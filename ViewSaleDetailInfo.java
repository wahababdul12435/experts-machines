package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import controller.ViewSaleDetail;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

public class ViewSaleDetailInfo {
    public String orderId;
    private String srNo;
    private String productId;
    private String productName;
    private String batchNo;
    private String batchExpiry;
    private String orderedQuantity;
    private String bonusQuantity;
    private String productPrice;
    private String discountGiven;

    private HBox operationsPane;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    public static ArrayList<Integer> companyIdArr = new ArrayList<>();
    public static ArrayList<String> companyNameArr = new ArrayList<>();
    public static ArrayList<String> productIdArr = new ArrayList<>();
    public static ArrayList<String> productNameArr = new ArrayList<>();
    public static ArrayList<String> batchIdArr = new ArrayList<>();
    public static ArrayList<String> batchNoArr = new ArrayList<>();
    public static ArrayList<String> batchExpiryArr = new ArrayList<>();
    public static ArrayList<String> orderedQuantityArr = new ArrayList<>();
    public static ArrayList<String> submissionQuantityArr = new ArrayList<>();
    public static ArrayList<String> bonusQuantityArr = new ArrayList<>();
//    public static ArrayList<String> unitArr = new ArrayList<>();
    public static ArrayList<String> productPriceArr = new ArrayList<>();
    public static ArrayList<String> finalPriceArr = new ArrayList<>();
    public static ArrayList<String> discountGivenArr = new ArrayList<>();

    public static TableView<ViewSaleDetailInfo> table_saledetaillog;

    public static JFXTextField txtProductName;
    public static JFXComboBox<String> txtBatchNo;
    public static JFXTextField txtQuantity;
//    public static JFXComboBox<String>  txtUnit;
    public static JFXTextField txtBonusPack;

    public static Button btnAdd;
    public static Button btnCancel;
    public static Button btnUpdate;

    public ViewSaleDetailInfo(String orderId) {
        this.orderId = orderId;
    }

    public ViewSaleDetailInfo(String srNo, String productName, String batchNo, String batchExpiry, String orderedQuantity, String bonusQuantity, String productPrice, String discountGiven) {
        this.srNo = srNo;
        this.productName = productName;
        this.batchNo = batchNo;
        this.batchExpiry = batchExpiry;
        this.orderedQuantity = orderedQuantity;
        this.bonusQuantity = bonusQuantity;
        this.productPrice = productPrice;
        this.discountGiven = discountGiven;

        this.btnEdit = new JFXButton();
        this.btnDelete = new JFXButton();
        this.btnEdit.setGraphic(createEditIcon());
        this.btnDelete.setGraphic(createDeleteIcon());
        this.btnEdit.getStyleClass().add("btn_icon");
        this.btnDelete.getStyleClass().add("btn_icon");
        this.operationsPane = new HBox(btnEdit, btnDelete);
        this.btnEdit.setOnAction((action)->editAddedRecord());
        this.btnDelete.setOnAction((action)->deleteAddedRecord());
    }

    private void editAddedRecord()
    {
        ViewSaleDetail.srNo = Integer.parseInt(srNo)-1;
        txtProductName.setText(productNameArr.get(Integer.parseInt(srNo)-1));
        txtBatchNo.getItems().clear();
        String productId = ViewSaleDetail.getProductId(ViewSaleDetail.completeProductData, txtProductName.getText());
        ArrayList<String> batchData = ViewSaleDetail.getProductsBatch(ViewSaleDetail.completeBatchData, productId);
        txtBatchNo.getItems().addAll(batchData);
        txtBatchNo.setValue(batchNoArr.get(Integer.parseInt(srNo)-1));
        txtQuantity.setText(orderedQuantityArr.get(Integer.parseInt(srNo)-1));
//        txtUnit.setValue(unitArr.get(Integer.parseInt(srNo)-1));
        txtBonusPack.setText(bonusQuantityArr.get(Integer.parseInt(srNo)-1));
        btnAdd.setVisible(false);
        btnCancel.setVisible(true);
        btnUpdate.setVisible(true);
    }

    private void deleteAddedRecord()
    {
        companyIdArr.remove(Integer.parseInt(srNo)-1);
        companyNameArr.remove(Integer.parseInt(srNo)-1);
        productIdArr.remove(Integer.parseInt(srNo)-1);
        productNameArr.remove(Integer.parseInt(srNo)-1);
        batchIdArr.remove(Integer.parseInt(srNo)-1);
        batchNoArr.remove(Integer.parseInt(srNo)-1);
        batchExpiryArr.remove(Integer.parseInt(srNo)-1);
        orderedQuantityArr.remove(Integer.parseInt(srNo)-1);
        submissionQuantityArr.remove(Integer.parseInt(srNo)-1);
        bonusQuantityArr.remove(Integer.parseInt(srNo)-1);
//        unitArr.remove(Integer.parseInt(srNo)-1);
        productPriceArr.remove(Integer.parseInt(srNo)-1);
        finalPriceArr.remove(Integer.parseInt(srNo)-1);
        discountGivenArr.remove(Integer.parseInt(srNo)-1);
        table_saledetaillog.setItems(ViewSaleDetail.parseUserList());
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    public String getBatchExpiry() {
        return batchExpiry;
    }

    public void setBatchExpiry(String batchExpiry) {
        this.batchExpiry = batchExpiry;
    }

    public String getOrderedQuantity() {
        return orderedQuantity;
    }

    public void setOrderedQuantity(String orderedQuantity) {
        this.orderedQuantity = orderedQuantity;
    }

    public String getBonusQuantity() {
        return bonusQuantity;
    }

    public void setBonusQuantity(String bonusQuantity) {
        this.bonusQuantity = bonusQuantity;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getDiscountGiven() {
        return discountGiven;
    }

    public void setDiscountGiven(String discountGiven) {
        this.discountGiven = discountGiven;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public ArrayList<String> getSaleBasicInfo(Statement stmt, Connection con)
    {
        ArrayList<String> saleBasicData = new ArrayList<String>();
        ArrayList<String> temp;
        ResultSet rs = null;
        String query = "SELECT `order_info`.`order_id`, `dealer_info`.`dealer_name`, `dealer_info`.`dealer_contact`, `order_info`.`final_price`, `order_info`.`discount`, `order_info`.`status`, `order_info`.`booking_user_id`, (SELECT `user_info`.`user_name` FROM `user_info` WHERE `order_info`.`booking_user_id` = `user_info`.`user_table_id`) as 'booking_user', `order_info`.`booking_date`, `order_info`.`booking_time`, `order_info`.`delivered_user_id`, (SELECT `user_info`.`user_name` FROM `user_info` WHERE `order_info`.`delivered_user_id` = `user_info`.`user_table_id`) as 'delivered_user', `order_info`.`delivered_date`, `order_info`.`delivered_time` FROM `order_info` LEFT OUTER JOIN `dealer_info` ON `dealer_info`.`dealer_table_id` = `order_info`.`dealer_id` WHERE `order_info`.`order_id` = '"+orderId+"'";
//        System.out.println(query);
        try {
            rs = stmt.executeQuery(query);
            while (rs.next())
            {
                saleBasicData.add(rs.getString(1)); // Invoice No
                saleBasicData.add(rs.getString(2)); // Dealer Name
                saleBasicData.add(rs.getString(3)); // Dealer Contact
                saleBasicData.add(rs.getString(4)); // Invoice Price
                saleBasicData.add(rs.getString(5)); // Discount Given
                saleBasicData.add(rs.getString(6)); // Status
                saleBasicData.add(rs.getString(8)+" ("+rs.getString(7)+")"); // Booking User
                saleBasicData.add(rs.getString(9)); // Booking Date
                saleBasicData.add(rs.getString(10)); // Booking Time
                saleBasicData.add(rs.getString(12)+" ("+rs.getString(11)+")"); // Delivered User
                saleBasicData.add(rs.getString(13)); // Delivered Date
                saleBasicData.add(rs.getString(14)); // Delivered Time
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return saleBasicData;
    }

    public ArrayList<ArrayList<String>> getSaleDetailInfo(Statement stmt, Connection con)
    {
        ArrayList<ArrayList<String>> saleData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        String query = "SELECT `company_info`.`company_table_id`, `company_info`.`company_name`, `order_info_detailed`.`product_id`, `product_info`.`product_name`, `order_info_detailed`.`batch_id`, `batchwise_stock`.`batch_no`, `batchwise_stock`.`batch_expiry`, `order_info_detailed`.`quantity`, `order_info_detailed`.`submission_quantity`, `order_info_detailed`.`bonus_quant`, `order_info_detailed`.`order_price`, `order_info_detailed`.`final_price`, `order_info_detailed`.`discount` FROM `order_info_detailed` INNER JOIN `order_info` ON `order_info`.`order_id` = `order_info_detailed`.`order_id` AND `order_info`.`order_id` = '"+orderId+"' LEFT OUTER JOIN `product_info` ON `product_info`.`product_table_id` = `order_info_detailed`.`product_id` LEFT OUTER JOIN `company_info` ON `company_info`.`company_table_id` = `product_info`.`company_id` LEFT OUTER JOIN `batchwise_stock` ON `batchwise_stock`.`batch_stock_id` = `order_info_detailed`.`batch_id` ORDER BY `order_info_detailed`.`product_id`";
        try {
            rs = stmt.executeQuery(query);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                if(rs.getString(1) != null)
                {
                    companyIdArr.add(Integer.parseInt(rs.getString(1)));
                    companyNameArr.add(rs.getString(2));
                }
                else
                {
                    companyIdArr.add(0);
                    companyNameArr.add("N/A");
                }

                temp.add(rs.getString(3)); // Product Id
                productIdArr.add(rs.getString(3));
                temp.add(rs.getString(4)); // Product Name
                productNameArr.add(rs.getString(4));
                temp.add(rs.getString(5)); // Batch Id
                batchIdArr.add(rs.getString(5));
                temp.add(rs.getString(6)); // Batch No
                batchNoArr.add(rs.getString(6));
                temp.add(rs.getString(7)); // Batch Expiry
                batchExpiryArr.add(rs.getString(7));
                temp.add(rs.getString(8)); // Qty
                orderedQuantityArr.add(rs.getString(8));
//                unitArr.add("Packet");
                temp.add(rs.getString(9)); // Submission Qty
                submissionQuantityArr.add(rs.getString(9));
                temp.add(rs.getString(10)); // Bonus Quantity
                bonusQuantityArr.add(rs.getString(10));
                temp.add(rs.getString(11)); // Product Price
                productPriceArr.add(rs.getString(11));
                temp.add(rs.getString(12)); // Final Price
                finalPriceArr.add(rs.getString(12));
                temp.add(rs.getString(13)); // Discount Given
                discountGivenArr.add(rs.getString(13));
                saleData.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return saleData;
    }

    public ArrayList<ArrayList<String>> loadSaleTable()
    {
        ArrayList<ArrayList<String>> saleData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        for(int i=0; i<productIdArr.size(); i++)
        {
            temp = new ArrayList<String>();
            temp.add(productNameArr.get(i));
            temp.add(batchNoArr.get(i));
            temp.add(batchExpiryArr.get(i));
            if(!orderedQuantityArr.get(i).equals(submissionQuantityArr.get(i)))
            {
                temp.add(orderedQuantityArr.get(i)+"\n(Qty in Stock: "+submissionQuantityArr.get(i)+")");
            }
            else
            {
                temp.add(orderedQuantityArr.get(i));
            }

            temp.add(bonusQuantityArr.get(i));
            temp.add(finalPriceArr.get(i));
            temp.add(discountGivenArr.get(i));
            saleData.add(temp);
        }
        return saleData;
    }

    public ArrayList<ArrayList<String>> loadInvoiceItems()
    {
        ArrayList<ArrayList<String>> saleData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ArrayList<Integer> tempCompanyId = companyIdArr;
        ArrayList<String> tempCompanyName = companyNameArr;
        ArrayList<String> tempProductId = productIdArr;
        ArrayList<String> tempProductName = productNameArr;
        ArrayList<String> tempBatchNo = batchNoArr;
        ArrayList<String> tempSubmissionQty = submissionQuantityArr;
        ArrayList<String> tempBonusQty = bonusQuantityArr;
        ArrayList<String> tempProductPrice = productPriceArr;
        ArrayList<String> tempDiscountGiven = discountGivenArr;
        ArrayList<String> tempFinalPrice = finalPriceArr;

        float companyTotalGross = 0;
        float companyTotalSalesTax = 0;
        float companyTotalDiscount = 0;
        float companyTotalAmount = 0;

        int x = 0;
        String stX = "";
        for (int i = 0; i < tempCompanyId.size(); i++) {
            for (int j = i+1; j < tempCompanyId.size(); j++) {
                if(tempCompanyId.get(i) > tempCompanyId.get(j)) {

                    x = tempCompanyId.get(i);
                    tempCompanyId.set(i, tempCompanyId.get(j));
                    tempCompanyId.set(j, x);

                    stX = tempCompanyName.get(i);
                    tempCompanyName.set(i, tempCompanyName.get(j));
                    tempCompanyName.set(j, stX);

                    stX = tempProductId.get(i);
                    tempProductId.set(i, tempProductId.get(j));
                    tempProductId.set(j, stX);

                    stX = tempProductName.get(i);
                    tempProductName.set(i, tempProductName.get(j));
                    tempProductName.set(j, stX);

                    stX = tempBatchNo.get(i);
                    tempBatchNo.set(i, tempBatchNo.get(j));
                    tempBatchNo.set(j, stX);

                    stX = tempSubmissionQty.get(i);
                    tempSubmissionQty.set(i, tempSubmissionQty.get(j));
                    tempSubmissionQty.set(j, stX);

                    stX = tempBonusQty.get(i);
                    tempBonusQty.set(i, tempBonusQty.get(j));
                    tempBonusQty.set(j, stX);

                    stX = tempProductPrice.get(i);
                    tempProductPrice.set(i, tempProductPrice.get(j));
                    tempProductPrice.set(j, stX);

                    stX = tempDiscountGiven.get(i);
                    tempDiscountGiven.set(i, tempDiscountGiven.get(j));
                    tempDiscountGiven.set(j, stX);

                    stX = tempFinalPrice.get(i);
                    tempFinalPrice.set(i, tempFinalPrice.get(j));
                    tempFinalPrice.set(j, stX);
                }
            }
        }

        int checkCompanyId = tempCompanyId.get(0);
        int num = 1;
        for(int i=0; i<tempCompanyId.size(); i++)
        {
            if(!tempSubmissionQty.get(i).equals("0"))
            {
                if(checkCompanyId == tempCompanyId.get(i))
                {
                    temp = new ArrayList<String>();
                    temp.add(String.valueOf(num));
                    temp.add(tempProductId.get(i));
                    if(tempProductName.get(i) != null)
                    {
                        temp.add(tempProductName.get(i));
                    }
                    else
                    {
                        temp.add("N/A");
                    }
                    temp.add("20");
                    temp.add(tempBatchNo.get(i));
                    String itemRate = String.valueOf(Float.parseFloat(tempProductPrice.get(i))/Float.parseFloat(tempSubmissionQty.get(i)));
                    temp.add(itemRate);
                    temp.add(tempSubmissionQty.get(i));
                    temp.add(tempBonusQty.get(i));
                    temp.add(tempProductPrice.get(i));
                    companyTotalGross += Float.parseFloat(tempProductPrice.get(i));
                    temp.add("0");
                    companyTotalSalesTax += 0;
                    temp.add(tempDiscountGiven.get(i));
                    companyTotalDiscount += Float.parseFloat(tempDiscountGiven.get(i));
                    temp.add(tempFinalPrice.get(i));
                    companyTotalAmount += Float.parseFloat(tempFinalPrice.get(i));
                    saleData.add(temp);
                    num++;
                }
                else
                {
                    checkCompanyId = tempCompanyId.get(i);
                    temp = new ArrayList<String>();
                    temp.add("");
                    temp.add("0");
                    temp.add("     ------------     "+tempCompanyName.get(i-1)+"     ------------     ");
                    temp.add("");
                    temp.add("");
                    temp.add("");
                    temp.add("");
                    temp.add("");
                    temp.add(String.valueOf(companyTotalGross));
                    temp.add(String.valueOf(companyTotalSalesTax));
                    temp.add(String.valueOf(companyTotalDiscount));
                    temp.add(String.valueOf(companyTotalAmount));
                    saleData.add(temp);

//                    ---------------------

                    temp = new ArrayList<String>();
                    companyTotalGross = 0;
                    companyTotalSalesTax = 0;
                    companyTotalDiscount = 0;
                    companyTotalAmount = 0;
                    temp.add(String.valueOf(num));
                    temp.add(tempProductId.get(i));
                    if(tempProductName.get(i) != null)
                    {
                        temp.add(tempProductName.get(i));
                    }
                    else
                    {
                        temp.add("N/A");
                    }
                    temp.add("20");
                    temp.add(tempBatchNo.get(i));
                    String itemRate = String.valueOf(Float.parseFloat(tempProductPrice.get(i))/Float.parseFloat(tempSubmissionQty.get(i)));
                    temp.add(itemRate);
                    temp.add(tempSubmissionQty.get(i));
                    temp.add(tempBonusQty.get(i));
                    temp.add(tempProductPrice.get(i));
                    companyTotalGross += Float.parseFloat(tempProductPrice.get(i));
                    temp.add("0");
                    companyTotalSalesTax += 0;
                    temp.add(tempDiscountGiven.get(i));
                    companyTotalDiscount += Float.parseFloat(tempDiscountGiven.get(i));
                    temp.add(tempFinalPrice.get(i));
                    companyTotalAmount += Float.parseFloat(tempFinalPrice.get(i));
                    saleData.add(temp);
                    num++;
                }
            }
        }
        temp = new ArrayList<String>();
        temp.add("");
        temp.add("0");
        temp.add("     ------------     "+tempCompanyName.get(tempCompanyName.size()-1)+"     ------------     ");
        temp.add("");
        temp.add("");
        temp.add("");
        temp.add("");
        temp.add("");
        temp.add(String.valueOf(companyTotalGross));
        temp.add(String.valueOf(companyTotalSalesTax));
        temp.add(String.valueOf(companyTotalDiscount));
        temp.add(String.valueOf(companyTotalAmount));
        saleData.add(temp);
        return saleData;
    }



    public String getBatchPolicy(Statement stmt, Connection con)
    {
        String batchPolicy = "";
        ArrayList<String> temp;
        ResultSet rs = null;
        String query = "SELECT `setting_value` FROM `system_settings` WHERE `setting_name` = 'batch_policy'";
//        System.out.println(query);
        try {
            rs = stmt.executeQuery(query);
            while (rs.next())
            {
                batchPolicy = rs.getString(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return batchPolicy;
    }

    public ArrayList<String> getInvoicePrints(Statement stmt, Connection con)
    {
        ArrayList<String> invoicePrintData = new ArrayList<String>();
        ArrayList<String> temp;
        ResultSet rs = null;
        String query = "SELECT `header_1`, `header_2`, `header_3`, `header_4`, `header_5`, `owner_name`, `father_name`, `owner_cnic`, `owner_country`, `business_name`, `business_address`, `footer_1`, `footer_2`, `footer_3`, `footer_4`, `status` FROM `sale_invoice_print_info`";
//        System.out.println(query);
        try {
            rs = stmt.executeQuery(query);
            while (rs.next())
            {
                invoicePrintData.add(rs.getString(1)); // Header 1
                invoicePrintData.add(rs.getString(2)); // Header 2
                invoicePrintData.add(rs.getString(3)); // Header 3
                invoicePrintData.add(rs.getString(4)); // Header 4
                invoicePrintData.add(rs.getString(5)); // Header 5
                invoicePrintData.add(rs.getString(6)); // Owner Name
                invoicePrintData.add(rs.getString(7)); // Father Name
                invoicePrintData.add(rs.getString(8)); // Owner CNIC
                invoicePrintData.add(rs.getString(9)); // Owner Country
                invoicePrintData.add(rs.getString(10)); // Business Name
                invoicePrintData.add(rs.getString(11)); // Business Address
                invoicePrintData.add(rs.getString(12)); // Footer 1
                invoicePrintData.add(rs.getString(13)); // Footer 2
                invoicePrintData.add(rs.getString(14)); // Footer 3
                invoicePrintData.add(rs.getString(15)); // Footer 4
                invoicePrintData.add(rs.getString(16)); // Status
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return invoicePrintData;
    }

    public void updateInvoice(Statement stmt, Connection con)
    {
        String currentDate = GlobalVariables.getStDate();
        String currentTime = GlobalVariables.getStTime();
        String currentUser = GlobalVariables.getUserId();
        String updateQuery = "INSERT INTO `order_info_detailed`(`order_id`, `product_id`, `batch_id`, `quantity`, `submission_quantity`, `unit`, `submission_unit`, `order_price`, `bonus_quant`, `discount`, `final_price`, `cmplt_returned_bit`, `returned_quant`, `returned_bonus_quant`) VALUES ";
        String deleteQuery = "DELETE FROM `order_info_detailed` WHERE `order_id` = '"+orderId+"'";
        String unit = "Packets";
        try {
            stmt.executeUpdate(deleteQuery);
            for(int i=0; i<productIdArr.size(); i++)
            {
                if(i == productIdArr.size()-1)
                    updateQuery += "('"+orderId+"','"+productIdArr.get(i)+"','"+batchIdArr.get(i)+"','"+orderedQuantityArr.get(i)+"','"+submissionQuantityArr.get(i)+"','"+unit+"','"+unit+"','"+productPriceArr.get(i)+"','"+bonusQuantityArr.get(i)+"','"+discountGivenArr.get(i)+"','"+finalPriceArr.get(i)+"','0','0','0') ";
                else
                    updateQuery += "('"+orderId+"','"+productIdArr.get(i)+"','"+batchIdArr.get(i)+"','"+orderedQuantityArr.get(i)+"','"+submissionQuantityArr.get(i)+"','"+unit+"','"+unit+"','"+productPriceArr.get(i)+"','"+bonusQuantityArr.get(i)+"','"+discountGivenArr.get(i)+"','"+finalPriceArr.get(i)+"','0','0','0'), ";
            }
//            updateQuery = "UPDATE `dealer_info` SET `dealer_id`='"+dealerId+"',`dealer_area_id`='"+areaId+"',`dealer_name`='"+dealerName+"',`dealer_contact`='"+dealerContact+"',`dealer_address`='"+dealerAddress+"',`dealer_type`='"+dealerType+"',`dealer_cnic`='"+dealerCnic+"',`dealer_lic_num`='"+dealerLicNum+"',`dealer_lic_exp`='"+dealerLicExp+"',`dealer_status`='"+dealerStatus+"', `update_user_id`='"+currentUser+"',`update_date`='"+currentDate+"',`update_time`='"+currentTime+"' WHERE `dealer_table_id` = '"+dealerTableId+"'";

//            System.out.println(updateQuery);
            stmt.executeUpdate(updateQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateBasic(Statement stmt, Connection con, String dealerId, String orderPrice, String discount, String waivedOffPrice, String finalPrice, String bookingUserId, String bookingDate, String bookingTime, String deliveredUserId, String deliveredDate, String deliveredTime, String orderStatus)
    {
        String currentDate = GlobalVariables.getStDate();
        String currentTime = GlobalVariables.getStTime();
        String currentUser = GlobalVariables.getUserId();
        String updateQuery;
        String unit = "";
        String productId = "";
        String quantity = "";
        String bonus = "";
//        String orderPrice = "0";
//        String discount = "0";
//        String finalPrice = "0";
        for(int i=0; i<productIdArr.size(); i++)
        {
            if(i == productIdArr.size()-1)
            {
                productId += productIdArr;
                quantity += submissionQuantityArr;
                bonus += bonusQuantityArr;
                unit += "Packets";
            }
            else
            {
                productId += productIdArr+"_-_";
                quantity += submissionQuantityArr+"_-_";
                bonus += bonusQuantityArr+"_-_";
                unit += "Packets_-_";
            }

        }
        try {
            updateQuery = "UPDATE `order_info` SET `dealer_id`='"+dealerId+"',`product_id`='"+productId+"',`quantity`='"+quantity+"',`unit`='"+unit+"',`order_price`='"+orderPrice+"',`bonus`='"+bonus+"',`discount`='"+discount+"',`waive_off_price`='"+waivedOffPrice+"',`final_price`='"+finalPrice+"',`order_success`=[value-11],`booking_latitude`=[value-12],`booking_longitude`=[value-13],`booking_area`=[value-14],`booking_date`=[value-15],`booking_time`=[value-16],`booking_user_id`=[value-17],`delivered_latitude`=[value-18],`delivered_longitude`=[value-19],`delivered_area`=[value-20],`delivered_date`=[value-21],`delivered_time`=[value-22],`delivered_user_id`=[value-23],`update_dates`=[value-24],`update_times`=[value-25],`update_user_id`=[value-26],`status`=[value-27] WHERE `order_id` = ''";
            stmt.executeUpdate(updateQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private Node createEditIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.EDIT);
        graphic.setFontSize(20.0);
        return graphic;
    }

    private Node createDeleteIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.TRASH);
        graphic.setFontSize(20.0);
        return graphic;
    }
}
