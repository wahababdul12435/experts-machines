/**
 * Created by Hp on 8/27/2019.
 */
function addToWishList(itemNo) {

    document.getElementById("addtowish_"+itemNo).hidden = true;
    document.getElementById("addedtowish_"+itemNo).hidden = false;

    // if(!loggedin)
    // {
    //     clickLogin('signup','signin');
    // }
    // else
    {
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp=new XMLHttpRequest();
        } else { // code for IE6, IE5
            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange=function() {
            if (this.readyState==4 && this.status==200) {
                // document.getElementById("txtHint").innerHTML=this.responseText;
            }
        };
        xmlhttp.open("GET","addtowishlist.php?wishitem="+itemNo+"&op=Add",true);
        xmlhttp.send();
    }
}

function removeFromWishList(itemNo) {

    document.getElementById("addtowish_"+itemNo).hidden = false;
    document.getElementById("addedtowish_"+itemNo).hidden = true;


    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function() {
        if (this.readyState==4 && this.status==200) {
            // document.getElementById("txtHint").innerHTML=this.responseText;

        }
    }
    xmlhttp.open("GET","addtowishlist.php?wishitem="+itemNo+"&op=Remove",true);
    xmlhttp.send();
}

function addToCart(itemNo) {
    document.getElementById("addtocart_"+itemNo).hidden = true;
    document.getElementById("addedtocart_"+itemNo).hidden = false;
    // var btnAddCart = document.getElementById("btnaddtocart_"+itemNo);
    // var btnAddedCart = document.getElementById("btnaddedtocart_"+itemNo);
    // if(btnAddCart || btnAddedCart)
    // {
    //     btnAddCart.hidden = true;
    //     btnAddedCart.hidden = false;
    // }
    // if(!loggedin)
    // {
    //     clickLogin('signup','signin');
    // }
    // else
    {
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp=new XMLHttpRequest();
        } else { // code for IE6, IE5
            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange=function() {
            if (this.readyState==4 && this.status==200) {
                // document.getElementById("txtHint").innerHTML=this.responseText;

            }
        }
        xmlhttp.open("GET","addtocart.php?cartitem="+itemNo+"&op=Add",true);
        xmlhttp.send();
    }
}

function removeFromCart(itemNo) {
    document.getElementById("addtocart_"+itemNo).hidden = false;
    document.getElementById("addedtocart_"+itemNo).hidden = true;

    // var btnAddCart = document.getElementById("btnaddtocart_"+itemNo);
    // var btnAddedCart = document.getElementById("btnaddedtocart_"+itemNo);
    // if(btnAddCart || btnAddedCart)
    // {
    //     window.alert('Yes');
    //     btnAddCart.hidden = false;
    //     btnAddedCart.hidden = true;
    // }

    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function() {
        if (this.readyState==4 && this.status==200) {
            // document.getElementById("txtHint").innerHTML=this.responseText;

        }
    }
    xmlhttp.open("GET","addtocart.php?cartitem="+itemNo+"&op=Remove",true);
    xmlhttp.send();
}