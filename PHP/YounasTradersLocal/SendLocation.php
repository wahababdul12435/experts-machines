<?php
/**
 * Created by PhpStorm.
 * User: Saffi Ullah 786
 * Date: 24-Mar-20
 * Time: 1:13 AM
 */

require_once "connection.php";
date_default_timezone_set("Asia/Karachi");
$date = date("d/M/Y");
$time = date("g:i A");

$type = $_GET["type"];
$softwareId = $_GET['softwareid'];
if($type == "TimeStamp")
{
    $userId = $_POST["user_id"];
    $latitude = $_POST["latitude"];
    $longitude = $_POST["longitude"];
    $locationName = $_POST["location_name"];
    $date = $_POST["date"];
    $time = $_POST["time"];

    $addTimeStampLocation = "INSERT INTO `mobile_gps_location`(`user_id`, `latitude`, `longitude`, `loc_name`, `date`, `time`, `software_lic_id`) VALUES ('$userId','$latitude','$longitude','$locationName','$date','$time', '$softwareId')";
    mysqli_query($con, $addTimeStampLocation);
    mysqli_close($con);
}
elseif($type == "DealerPinLoc")
{
    $dealerId = $_POST["dealer_id"];
    $latitude = $_POST["latitude"];
    $longitude = $_POST["longitude"];
    $locationName = $_POST["location_name"];
    $date = $_POST["date"];
    $time = $_POST["time"];
    $userId = $_POST["user_id"];

    $addDealerGPSLocation = "INSERT INTO `dealer_gps_location`(`dealer_id`, `latitude`, `longitude`, `loc_name`, `entry_date`, `entry_time`, `update_date`, `update_time`, `user_id`, `software_lic_id`) VALUES ('$dealerId','$latitude','$longitude','$locationName', '$date', '$time', '', '', '$userId', '$softwareId')";
    mysqli_query($con, $addDealerGPSLocation);
    mysqli_close($con);
}
else
{
    $userId = $_POST["user_id"];
    $latitude = $_POST["latitude"];
    $longitude = $_POST["longitude"];
    $locationName = $_POST["location_name"];

    $updateCurrLocation = "UPDATE `mobile_curr_locations` SET `latitude`='$latitude',`longitude`='$longitude',`location_name`='$locationName' WHERE `software_lic_id` = '$softwareId' AND `user_id` = '$userId'";
    mysqli_query($con, $updateCurrLocation);
    $num = mysqli_affected_rows($con);
    if($num == 0)
    {
        $addCurrLocation = "INSERT INTO `mobile_curr_locations`(`user_id`, `latitude`, `longitude`, `location_name`, `software_lic_id`) VALUES ('$userId','$latitude','$longitude','$locationName', '$softwareId')";
        mysqli_query($con, $addCurrLocation);
    }
    mysqli_close($con);
}

?>