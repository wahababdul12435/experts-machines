<?php
/**
 * Created by PhpStorm.
 * User: Saffi Ullah 786
 * Date: 27-Oct-19
 * Time: 11:23 PM
 */

require_once "connection.php";
require_once "UserDefinedFunctions.php";

$companyInfo = "SELECT `company_id`, `company_name` FROM `company_info`";
$companyInfo = mysqli_query($con, $companyInfo);
$companyCount = 0;
while($companyData = mysqli_fetch_array($companyInfo))
{
    $companyIDMain[$companyCount] = $companyData[0];
    $companyNameMain[$companyCount] = $companyData[1];
    $companyCount++;
}

$groupInfo = "SELECT * FROM `company_groups`";
$groupInfo = mysqli_query($con, $groupInfo);
$groupCount = 0;
while($groupData = mysqli_fetch_array($groupInfo))
{
    $groupIDMain[$groupCount] = $groupData[0];
    $companyIDGroupMain[$groupCount] = $groupData[1];
    $groupNameMain[$groupCount] = $groupData[2];
    $groupCount++;
}

$companyGroupsCount = array_count_values($companyIDGroupMain);

$productsInfo = "SELECT * FROM `product_info`";
$productsInfo = mysqli_query($con, $productsInfo);
$i=0;
while ($data = mysqli_fetch_array($productsInfo))
{
    $productID[$i] = $data[0];
    $companyID[$i] = $data[1];
    $companyIndex = array_search($companyID[$i], $companyIDMain);
    $companyName[$i] = $companyNameMain[$companyIndex];
    $groupID[$i] = $data[2];
    $groupIndex = array_search($groupID[$i], $groupIDMain);
    $groupName[$i] = $groupNameMain[$groupIndex];
    $productName[$i] = $data[3];
    $retailPrice[$i] = $data[4];
    $tradePrice[$i] = $data[5];
    $purchasePrice[$i] = $data[6];
    $purchaseDiscount[$i] = $data[7];
    $productStatus[$i] = $data[8];
    $i++;
}
?>

<html>
<head>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="css/font-awesome.min.css">
    <link type="text/css" rel="stylesheet" href="css/style.css">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>

    <style>
        .operations:hover
        {
            color: #D10024;
            cursor: pointer;
        }
        th
        {
            font-size: 15px;
        }
        td
        {
            font-size: 13px;
        }
    </style>
    <script>
        var productID = 0;
        var allCompaniesID = <?php echo json_encode($companyIDMain);?>;
    </script>
</head>
<body>

<?php
require_once "PopupModel.php";
?>
<div class="container">
    <div style="margin-top: 20px">
        <table id="SuppliersData" class="display">
            <thead>
            <div>
                <tr>
                    <th style="text-align: center; width: 2%">Product ID</th>
                    <th style="text-align: center; width: 2%">Company Name</th>
                    <th style="text-align: center; width: 2%">Group Name</th>
                    <th style="text-align: center; width: 2%">Name</th>
                    <th style="text-align: center; width: 2%">Retail Price</th>
                    <th style="text-align: center; width: 2%">Trade Price</th>
                    <th style="text-align: center; width: 2%">Purchase Price</th>
                    <th style="text-align: center; width: 2%">Purchase Discount</th>
                    <th style="text-align: center; width: 2%">Status</th>
                    <th style="text-align: center; width: 2%">Operations</th>
                </tr>
            </div>
            </thead>
            <tbody>
            <?php
            for($j=0; $j<$i; $j++)
            {
                ?>
                <tr>
                    <td style="text-align: center"><?php echo $productID[$j]; ?></td>
                    <td style="text-align: center"><?php echo $companyName[$j]; ?></td>
                    <td style="text-align: center"><?php echo $groupName[$j]; ?></td>
                    <td style="text-align: center"><?php echo $productName[$j]; ?></td>
                    <td style="text-align: center"><?php echo $retailPrice[$j]; ?></td>
                    <td style="text-align: center"><?php echo $tradePrice[$j]; ?></td>
                    <td style="text-align: center"><?php echo $purchasePrice[$j]; ?></td>
                    <td style="text-align: center"><?php echo $purchaseDiscount[$j]; ?></td>
                    <td style="text-align: center"><?php echo $productStatus[$j]; ?></td>
                    <td style="text-align: center">
                        <div style="margin-top: 10px">
                            <a data-toggle="modal" data-target="#myModalDel" onclick="delProduct('<?php echo $productID[$j]; ?>')">
                                <i class="fa-lg fa fa-trash operations" title="Delete"></i>&nbsp;&nbsp;
                            </a>
                            <a data-toggle="modal" data-target="#myModalEditProduct" onclick="editData('<?php echo $productID[$j]?>', '<?php echo $companyID[$j]; ?>', '<?php echo $groupID[$j]; ?>', '<?php echo $productName[$j]; ?>', '<?php echo $retailPrice[$j]; ?>', '<?php echo $tradePrice[$j]; ?>', '<?php echo $purchasePrice[$j]; ?>', '<?php echo $purchaseDiscount[$j]; ?>', '<?php echo $productStatus[$j]; ?>')">
                                <i class="fa-lg fa fa-edit operations" title="Edit"></i>
                            </a>
                        </div>
                    </td>
                </tr>

                <?php
            }
            ?>

            </tbody>
        </table>
    </div>
</div>

<script src="js/bootstrap.min.js"></script>
<script src="js/slick.min.js"></script>
<script src="js/nouislider.min.js"></script>
<script src="js/jquery.zoom.min.js"></script>
<script src="js/main.js"></script>

<script>
    $(document).ready(function() {
        $('#SuppliersData').DataTable(
            {
                // "Item No": [[ 0, "desc" ]]
            }
        );
    });

    function delProduct(givenID) {
        productID = givenID;
    }

    function delConfirmation(op) {
        if(op == 'yes')
        {
            window.location.href = 'SendData.php?table=product_info&op=del&id='+productID;
        }
        else
        {
            return;
        }
    }

    function editData(id, companyId, groupName, name, retailPrice, tradePrice, purchasePrice, purchaseDiscount, status) {
        document.getElementById('pro_id').value = id;
        document.getElementById('pro_companyid').value = companyId;
        document.getElementById('pro_groupname_'+companyId).style.display = 'block';
        document.getElementById('pro_groupname_'+companyId).value = groupName;
        document.getElementById('pro_name').value = name;
        document.getElementById('pro_retailprice').value = retailPrice;
        document.getElementById('pro_tradeprice').value = tradePrice;
        document.getElementById('pro_purchaseprice').value = purchasePrice;
        document.getElementById('pro_purchasediscount').value = purchaseDiscount;
        document.getElementById('pro_status').value = status;

        // window.alert(groupName)
    }

    function updateData(op)
    {
        if(op == 'yes')
        {
            comID = document.getElementById('pro_companyid').value;

            pro_id = document.getElementById('pro_id').value;
            pro_companyid = document.getElementById('pro_companyid').value;
            pro_groupid = document.getElementById('pro_groupname_'+comID).value;
            pro_name = document.getElementById('pro_name').value;
            pro_retailprice = document.getElementById('pro_retailprice').value;
            pro_tradeprice = document.getElementById('pro_tradeprice').value;
            pro_purchaseprice = document.getElementById('pro_purchaseprice').value;
            pro_purchasediscount = document.getElementById('pro_purchasediscount').value;
            pro_status = document.getElementById('pro_status').value;

            window.location.href = 'SendData.php?table=product_info&op=update&id='+pro_id+'&pro_companyid='+pro_companyid+'&pro_groupid='+pro_groupid+'&pro_name='+pro_name+'&pro_retailprice='+pro_retailprice+'&pro_tradeprice='+pro_tradeprice+'&pro_purchaseprice='+pro_purchaseprice+'&pro_purchasediscount='+pro_purchasediscount+'&pro_status='+pro_status;
        }
        else
        {

        }

    }

    function changeCompany() {
        changeComID = document.getElementById('pro_companyid').value;
        for(i=0; i<allCompaniesID.length; i++)
        {
            document.getElementById('pro_groupname_'+allCompaniesID[i]).style.display = 'none';
        }
        document.getElementById('pro_groupname_'+changeComID).style.display = 'block';
    }
</script>
</body>
</html>