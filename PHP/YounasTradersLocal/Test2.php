<?php
/**
 * Created by PhpStorm.
 * User: Saffi Ullah 786
 * Date: 26-Mar-20
 * Time: 11:40 PM
 */
require_once "connection.php";

$mobileCurrLoc = "SELECT `mobile_curr_locations`.*, `salesman_info`.`salesman_name` FROM `mobile_curr_locations` INNER JOIN `user_accounts` ON `user_accounts`.`user_id` = `mobile_curr_locations`.`user_id` INNER JOIN `salesman_info` ON `salesman_info`.`salesman_table_id` = `mobile_curr_locations`.`user_id` WHERE`user_accounts`.`current_status` = 'on' ORDER BY `user_accounts`.`user_id`";
$mobileCurrLoc = mysqli_query($con, $mobileCurrLoc);
$currActive=0;
while($currData = mysqli_fetch_array($mobileCurrLoc))
{
    $currUserId[$currActive] = $currData[0];
    $currLat[$currActive] = $currData[1];
    $currLng[$currActive] = $currData[2];
    $currLocName[$currActive] = $currData[3];
    $currUserName[$currActive] = $currData[4];
    $currActive++;
}

$timeStampLoc = "SELECT `mobile_gps_location`.* FROM `mobile_gps_location` INNER JOIN `user_accounts` ON `user_accounts`.`user_id` = `mobile_gps_location`.`user_id` WHERE`user_accounts`.`current_status` = 'on' AND `mobile_gps_location`.`date` = '20/Mar/2021' ORDER BY `user_accounts`.`user_id`";
$timeStampLoc = mysqli_query($con, $timeStampLoc);
$timeStampCount=0;
while($timeStampData = mysqli_fetch_array($timeStampLoc))
{
    $timeStampUserId[$timeStampCount] = $timeStampData[1];
    $timeStampLat[$timeStampCount] = $timeStampData[2];
    $timeStampLng[$timeStampCount] = $timeStampData[3];
    $timeStampLocName[$timeStampCount] = $timeStampData[4];
    $timeStampTime[$timeStampCount] = $timeStampData[6];
    $timeStampCount++;
}

$orderLoc = "SELECT `order_gps_location`.*, `dealer_info`.`dealer_name` FROM `order_gps_location` INNER JOIN `user_accounts` ON `user_accounts`.`user_id` = `order_gps_location`.`user_id` INNER JOIN `dealer_info` ON `dealer_info`.`dealer_id` = `order_gps_location`.`dealer_id` WHERE`user_accounts`.`current_status` = 'on' ORDER BY `user_accounts`.`user_id`";
$orderLoc = mysqli_query($con, $orderLoc);
$orderLocCount=0;
while($orderLocData = mysqli_fetch_array($orderLoc))
{
    $orderLocUserId[$orderLocCount] = $orderLocData[1];
    $orderLocLat[$orderLocCount] = $orderLocData[2];
    $orderLocLng[$orderLocCount] = $orderLocData[3];
    $orderLocName[$orderLocCount] = $orderLocData[4];
    $orderLocTime[$orderLocCount] = $orderLocData[6];
    $orderLocPrice[$orderLocCount] = $orderLocData[8];
    $orderLocType[$orderLocCount] = $orderLocData[9];
    $orderLocDealerName[$orderLocCount] = $orderLocData[10];
    $orderLocCount++;
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>My Google Map</title>
    <style>
        #map{
            height:600px;
            width:100%;
        }
    </style>
</head>
<body>
<div id="map"></div>
<script>
    var map;
    var markers = [];
    var markersCurrLocations = [[]];
    var markersNewLocations = [[]];
    var timeStampMarkers = [[]];
    var temp = [];
    var orderLocMarkers = [[]];
    var markerToMove = [];
    var positionOfMarker = [[]];
    var result = [[]];
    function initMap(){
        // Map options
        var options = {
            zoom:12,
            center:{lat:32.551158,lng:74.070952}
        }

        // New map
        map = new google.maps.Map(document.getElementById('map'), options);

        // Listen for click on map
        google.maps.event.addListener(map, 'click', function(event){
            // Add marker
            addMarker({coords:event.latLng});
        });

        iconCurrLoc = {
            url: "imgs/gps markers/curr_loc_1.png", // url
            scaledSize: new google.maps.Size(30, 30) // scaled size
        };
        iconDealerLoc = {
            url: "imgs/gps markers/dealer_loc_1.png", // url
            scaledSize: new google.maps.Size(30, 30) // scaled size
        };

        iconBooking = {
            url: "imgs/gps markers/booking_1.png", // url
            scaledSize: new google.maps.Size(30, 30) // scaled size
        };

        iconCashCollection = {
            url: "imgs/gps markers/cash_collection_1.png", // url
            scaledSize: new google.maps.Size(30, 30) // scaled size
        };

        iconDelivered = {
            url: "imgs/gps markers/delivered_1.png", // url
            scaledSize: new google.maps.Size(30, 30) // scaled size
        };

        iconTimeStamp = {
            url: "imgs/gps markers/time_stamp_1.png", // url
            scaledSize: new google.maps.Size(30, 30) // scaled size
        };

        // Array of markers
        // var markers = [
        //     {
        //         coords:{lat:32.551158,lng:74.070952},
        //         iconImage:icon,
        //         content:'<h1>Lynn MA</h1>'
        //     },
        //     {
        //         coords:{lat:42.8584,lng:-70.9300},
        //         content:'<h1>Amesbury MA</h1>'
        //     }
        // ];
        // markers[2] = {coords:{lat:42.7762,lng:-71.0773}};

        // Array of Current Location Markers
        currActiveMarkers = [];
        <?php
        for($curri=0; $curri<$currActive; $curri++)
        {
        ?>
        temp = [];
        currActiveMarkers[<?php echo $curri; ?>] = {coords:{lat:<?php echo $currLat[$curri]; ?>,lng:<?php echo $currLng[$curri]; ?>}, iconImage:iconCurrLoc, content:'<p><?php echo $currUserName[$curri]; ?></p><p><?php echo $currLocName[$curri]; ?></p>'};
        temp[0] = '<?php echo $currUserId[$curri]?>';
        temp[1] = <?php echo $currLat[$curri]?>;
        temp[2] = <?php echo $currLng[$curri]?>;
        temp[3] = '<?php echo $currLocName[$curri]?>';
        markersCurrLocations[<?php echo $curri; ?>] = temp;
        //window.alert(markersCurrLocations[<?php //echo $curri; ?>//][0]+" "+markersCurrLocations[<?php //echo $curri; ?>//][1]+" "+markersCurrLocations[<?php //echo $curri; ?>//][2]+" "+markersCurrLocations[<?php //echo $curri; ?>//][3]);
        <?php
        }
        ?>

        // Array of Time Stamp Location Markers
        <?php
        $curri = 0;
        $stampsCount = 0;
        for($timei = 0; $timei<$timeStampCount; $timei++)
        {
        $preUserId = $currUserId[$curri];
        $newUserId = $timeStampUserId[$timei];
        if($preUserId == $newUserId)
        {
        ?>
        temp = [];
        temp[<?php echo $stampsCount; ?>] = {
            coords: {
                lat:<?php echo $timeStampLat[$timei]; ?>,
                lng:<?php echo $timeStampLng[$timei]; ?>},
            iconImage: iconTimeStamp,
            content: '<p><?php echo $timeStampTime[$timei]; ?></p><p><?php echo $timeStampLocName[$timei]; ?>}</p>'
        };
        <?php
        $stampsCount++;
        }
        else
        {
        ?>
        timeStampMarkers[<?php echo $curri; ?>] = temp;
        temp = [];
        <?php
        if($curri < $currActive)
        {
            $curri++;
            $stampsCount = 0;
        }
        ?>
        temp[<?php echo $stampsCount; ?>] = {
            coords: {
                lat:<?php echo $timeStampLat[$timei]; ?>,
                lng:<?php echo $timeStampLng[$timei]; ?>},
            iconImage: iconTimeStamp,
            content: '<p><?php echo $timeStampTime[$timei]; ?></p><p><?php echo $timeStampLocName[$timei]; ?>}</p>'
        };
        <?php
        $stampsCount++;
        }
        }
        if($currActive > 0)
        {
        ?>
        timeStampMarkers[<?php echo $curri; ?>] = temp;
        <?php
        }
        ?>


        // Array of Order Location Markers

        temp = [];
        <?php
        $curri = 0;
        $orderCount = 0;
        for($orderi = 0; $orderi<$orderLocCount; $orderi++)
        {
        $preUserId = $currUserId[$curri];
        $newUserId = $orderLocUserId[$orderi];
        if($preUserId == $newUserId)
        {
        ?>
        temp[<?php echo $orderCount; ?>] = {
            coords: {
                lat:<?php echo $orderLocLat[$orderi]; ?>,
                lng:<?php echo $orderLocLng[$orderi]; ?>},
            <?php
            if($orderLocType[$orderi] == "Booking")
            {
            ?>
            iconImage: iconBooking,
            <?php
            }
            elseif ($orderLocType[$orderi] == "Collection")
            {
            ?>
            iconImage: iconCashCollection,
            <?php
            }
            else
            {
            ?>
            iconImage: iconDelivered,
            <?php
            }
            ?>
            content: '<p><?php echo $orderLocTime[$orderi]; ?></p><p><?php echo $orderLocName[$orderi]; ?></p><p><?php echo $orderLocDealerName[$orderi]; ?></p>'
        };
        <?php
        $orderCount++;
        }
        else
        {
        ?>
        orderLocMarkers[<?php echo $curri; ?>] = temp;
        temp = [];
        <?php
        if($curri < $currActive)
        {
            $curri++;
            $orderCount = 0;
        }
        ?>
        temp[<?php echo $orderCount; ?>] = {
            coords: {
                lat:<?php echo $orderLocLat[$orderi]; ?>,
                lng:<?php echo $orderLocLng[$orderi]; ?>},
            <?php
            if($orderLocType[$orderi] == "Booking")
            {
            ?>
            iconImage: iconBooking,
            <?php
            }
            elseif ($orderLocType[$orderi] == "Collection")
            {
            ?>
            iconImage: iconCashCollection,
            <?php
            }
            else
            {
            ?>
            iconImage: iconDelivered,
            <?php
            }
            ?>
            content: '<p><?php echo $orderLocTime[$orderi]; ?></p><p><?php echo $orderLocName[$orderi]; ?></p><p><?php echo $orderLocDealerName[$orderi]; ?></p>'
        };
        <?php
        $orderCount++;
        }
        }
        if($currActive > 0)
        {
        ?>
        orderLocMarkers[<?php echo $curri; ?>] = temp;
        <?php
        }
        ?>


        // Loop through Current markers
        for(var i = 0; i < currActiveMarkers.length; i++){
            addMarker(currActiveMarkers[i], i);
        }

        startMoving = false;


        <?php
        //        for($markerI=0; $markerI<$currActive; $markerI++)
        //        {
        ?>
        //temp =[];
        //destTemp = [];
        //markerToMove[<?php //echo $markerI; ?>//] = markers[<?php //echo $markerI; ?>//];
        //temp[0] = <?php //echo $currLat[$markerI]; ?>//;
        //temp[1] = <?php //echo $currLng[$markerI]; ?>//;
        //destTemp[0] = 32.651158;
        //destTemp[1] = 74.070952;
        //positionOfMarker[<?php //echo $markerI; ?>//] = temp;
        //result[<?php //echo $markerI; ?>//] = destTemp;
        <?php
        //        }
        ?>

        // window.alert(positionOfMarker.length);
        // result = [32.651158, 74.070952];

        doSetTimeout();
    }

    function addMarker(props, x){
        var marker = new google.maps.Marker({
            position:props.coords,
            map:map
            //icon:props.iconImage
        });
        if(x != null)
        {
            // markers.push(marker);
            markers[x] = marker;
        }

        // Check for customicon
        if(props.iconImage){
            // Set icon image
            marker.setIcon(props.iconImage);
        }

        // Check content
        if(props.content){
            var infoWindow = new google.maps.InfoWindow({
                content:props.content
            });

            marker.addListener('click', function(){
                infoWindow.open(map, marker);
                for(i=0; i<timeStampMarkers[x].length; i++)
                {
                    addMarker(timeStampMarkers[x][i]);
                }

                for(i=0; i<orderLocMarkers[x].length; i++)
                {
                    addMarker(orderLocMarkers[x][i]);
                }

            });
        }
    }

    // Sets the map on all markers in the array.
    function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    }

    // Removes the markers from the map, but keeps them in the array.
    function clearMarkers() {
        setMapOnAll(null);
    }

    // Shows any markers currently in the array.
    function showMarkers() {
        setMapOnAll(map);
    }

    var numDeltas = 100;
    var delay = 100; //milliseconds
    var i = 0;
    var deltaLat = [];
    var deltaLng = [];
    var numCheck = 0;

    function doSetTimeout() {
        // window.alert(numCheck);
        for(numX=0; numX<markersCurrLocations.length; numX++)
        {
            temp =[];
            destTemp = [];
            if(numCheck == 0)
            {
                markerToMove[numX] = markers[numX];
                temp[0] = markersCurrLocations[numX][1];
                temp[1] = markersCurrLocations[numX][2];
                positionOfMarker[numX] = temp;
                destTemp[0] = 32.651158;
                destTemp[1] = 74.070952;
            }
            else
            {
                destTemp[0] = 32.551158;
                destTemp[1] = 74.070952;
            }
            result[numX] = destTemp;
        }
        setTimeout(function() { transition() }, delay);
    }

    function transition(){
        i = 0;
        for(x=0; x<positionOfMarker.length; x++)
        {
            // window.alert(result[x][0]+" ---- "+result[x][1]);
            // deltaLat[x] = (result[x][0] - positionOfMarker[x][0])/numDeltas;
            // deltaLng[x] = (result[x][1] - positionOfMarker[x][1])/numDeltas;

            deltaLat[x] = (result[x][0] - positionOfMarker[x][0])/numDeltas;
            deltaLng[x] = (result[x][1] - positionOfMarker[x][1])/numDeltas;
        }
        moveMarker();
    }

    function moveMarker(){

        for(x=0; x<positionOfMarker.length; x++)
        {
            // window.alert(positionOfMarker[x][0]+" ---- "+positionOfMarker[x][1]);
            positionOfMarker[x][0] += deltaLat[x];
            positionOfMarker[x][1] += deltaLng[x];
            latlng = [];
            latlng[x] = new google.maps.LatLng(positionOfMarker[x][0], positionOfMarker[x][1]);
            markerToMove[x].setTitle("Latitude:"+positionOfMarker[x][0]+" | Longitude:"+positionOfMarker[x][1]);
            markerToMove[x].setPosition(latlng[x]);
        }
        if(i!=numDeltas){
            i++;
            setTimeout(moveMarker, delay);
        }
        else
        {
            numCheck = 1;
            temp = [];
            temp[0] = 32.651158;
            temp[1] = 74.070952;
            positionOfMarker[0] = temp;

            temp = [];
            temp[0] = 32.551158;
            temp[1] = 74.070952;
            positionOfMarker[1] = temp;
            doSetTimeout();
        }
    }

    function refreshCurrLocations() {
        markersNewLocations = [[]];
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp=new XMLHttpRequest();
        } else { // code for IE6, IE5
            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange=function() {
            if (this.readyState==4 && this.status==200) {
                // document.getElementById("txtHint").innerHTML=this.responseText;
                var data = JSON.parse(this.responseText);
                for(var refI = 0; refI < data.length; refI++) {
                    currUserId = data[refI].user_id;
                    currLatitude = data[refI].latitude;
                    currLongitude = data[refI].longitude;
                    currLocationName = '<p>'+data[refI].location_name+'</p>';
                    currUserName = '<p>'+data[refI].salesman_name+'</p>';
                    currContent = currUserName+currLocationName;
                    temp = [];
                    temp[0] = currUserId;
                    temp[1] = currLatitude;
                    temp[2] = currLongitude;
                    temp[3] = currContent;
                    markersNewLocations[refI] = temp;
                }
                doSetTimeout();
            }
        };
        xmlhttp.open("POST","GPSCurrLocRefresh.php",true);
        xmlhttp.send();
    }
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDDTQaCkiR1aqGImxpeFDqdip4ytAna7HM&callback=initMap">
</script>
</body>
</html>

