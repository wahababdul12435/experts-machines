<?php
/**
 * Created by PhpStorm.
 * User: Saffi Ullah 786
 * Date: 28-Oct-19
 * Time: 8:02 PM
 */

require_once "connection.php";

if(isset($_GET['table']) && isset($_GET['op']) && isset($_GET['id']))
{
    $tableName = $_GET['table'];
    $operation = $_GET['op'];
    $id = $_GET['id'];

    if($tableName == 'company_info')
    {
        if($operation == 'del')
        {
            $sqlDelete = "DELETE FROM `company_info` WHERE `company_id` = '$id'";
            if(mysqli_query($con, $sqlDelete))
            {
                header("Location: ViewCompany.php");
            }
            else
            {

            }
        }
        elseif ($operation == 'update')
        {
            $companyID = $_GET['id'];
            $companyName = $_GET['name'];
            $companyAddress = $_GET['address'];
            $companyContact = $_GET['contact'];
            $companyEmail = $_GET['email'];
            $companyStatus = $_GET['status'];

            $sqlUpdateCompany = "UPDATE `company_info` SET `company_name`='$companyName', `company_address`='$companyAddress', `company_contact`='$companyContact', `company_email`='$companyEmail', `company_status`='$companyStatus' WHERE `company_id` = '$companyID'";
//            echo $sqlUpdateCompany.'<br>';
            if(mysqli_query($con, $sqlUpdateCompany))
            {
                header("Location: ViewCompany.php");
            }
            else
            {

            }
        }
    }
    elseif ($tableName == "supplier_info")
    {
        if($operation == 'del')
        {
            $sqlDelete = "DELETE FROM `supplier_info` WHERE `supplier_id` = '$id'";
            if(mysqli_query($con, $sqlDelete))
            {
                header("Location: ViewSupplier.php");
            }
            else
            {

            }
        }
        elseif ($operation == 'update')
        {
            $supplierID = $_GET['id'];
            $supplierCompanyID = $_GET['companyid'];
            $supplierName = $_GET['name'];
            $supplierContact = $_GET['contact'];
            $supplierAddress = $_GET['address'];
            $supplierStatus = $_GET['status'];

            $sqlUpdateSupplier = "UPDATE `supplier_info` SET `company_id`='$supplierCompanyID',`supplier_name`='$supplierName',`supplier_contact`='$supplierContact',`supplier_address`='$supplierAddress',`supplier_status`='$supplierStatus' WHERE `supplier_id` = '$supplierID'";
//            echo $sqlUpdateCompany.'<br>';
            if(mysqli_query($con, $sqlUpdateSupplier))
            {
                header("Location: ViewSupplier.php");
            }
            else
            {

            }
        }
    }
    elseif ($tableName == "product_info")
    {
        if($operation == 'del')
        {
            $sqlDelete = "DELETE FROM `product_info` WHERE `product_id` = '$id'";
            if(mysqli_query($con, $sqlDelete))
            {
                header("Location: ViewProduct.php");
            }
            else
            {

            }
        }
        elseif ($operation == 'update')
        {
            $pro_id = $_GET['id'];
            $pro_companyid = $_GET['pro_companyid'];
            $pro_groupid = $_GET['pro_groupid'];
            $pro_name = $_GET['pro_name'];
            $pro_retailprice = $_GET['pro_retailprice'];
            $pro_tradeprice = $_GET['pro_tradeprice'];
            $pro_purchaseprice = $_GET['pro_purchaseprice'];
            $pro_purchasediscount = $_GET['pro_purchasediscount'];
            $pro_status = $_GET['pro_status'];

            $sqlUpdateProduct = "UPDATE `product_info` SET `company_id`='$pro_companyid',`group_id`='$pro_groupid',`product_name`='$pro_name',`retail_price`='$pro_retailprice',`trade_price`='$pro_tradeprice',`purchase_price`='$pro_purchaseprice',`purchase_discount`='$pro_purchasediscount',`product_status`='$pro_status' WHERE `product_id` = '$pro_id'";
//            echo $sqlUpdateProduct.'<br>';
            if(mysqli_query($con, $sqlUpdateProduct))
            {
                header("Location: ViewProduct.php");
            }
            else
            {

            }
        }
    }
    elseif ($tableName == "salesman_info")
    {
        if($operation == 'del')
        {
            $sqlDelete = "DELETE FROM `salesman_info` WHERE `salesman_id` = '$id'";
            if(mysqli_query($con, $sqlDelete))
            {
                header("Location: ViewSalesman.php");
            }
            else
            {

            }
        }
        elseif ($operation == 'update')
        {
            $salesmanId = $_GET['id'];
            $salesman_name = $_GET['salesman_name'];
            $salesman_contact = $_GET['salesman_contact'];
            $salesman_address = $_GET['salesman_address'];
            $salesman_cnic = $_GET['salesman_cnic'];
            $salesman_des_1 = $_GET['salesman_des_1'];
            $salesman_des_2 = $_GET['salesman_des_2'];
            $salesman_des_3 = $_GET['salesman_des_3'];
            $salesman_des_4 = $_GET['salesman_des_4'];
            $salesman_status = $_GET['salesman_status'];

            $salesmanDes = array();
            if($salesman_des_1 != 0)
            {
                array_push($salesmanDes, $salesman_des_1);
            }
            if($salesman_des_2 != 0)
            {
                array_push($salesmanDes, $salesman_des_2);
            }
            if($salesman_des_3 != 0)
            {
                array_push($salesmanDes, $salesman_des_3);
            }
            if($salesman_des_4 != 0)
            {
                array_push($salesmanDes, $salesman_des_4);
            }

            $sqlUpdateSalesman = "UPDATE `salesman_info` SET `salesman_name`='$salesman_name',`salesman_contact`='$salesman_contact',`salesman_address`='$salesman_address',`salesman_cnic`='$salesman_cnic',`salesman_status`='$salesman_status' WHERE `salesman_id` = '$salesmanId'";
//            echo $sqlUpdateProduct.'<br>';
            mysqli_query($con, $sqlUpdateSalesman);

            $getDesignationInfo = "SELECT * FROM `salesman_designated_areas` WHERE `salesman_id` = '$salesmanId'";
            $getDesignationInfo = mysqli_query($con, $getDesignationInfo);
            $i=0;
            while ($data = mysqli_fetch_array($getDesignationInfo))
            {
                $salDesID[$i] = $data[0];
                $salID[$i] = $data[1];
                $desAreaID[$i] = $data[2];
                $desStatus[$i] = $data[3];
                $i++;
            }

            if($i>sizeof($salesmanDes))
            {
//                echo "Gone <br>";
                $j=0;
                for($x=0; $x<sizeof($salesmanDes); $x++)
                {
//                    echo "Update <br>";
                    $updateDes = "UPDATE `salesman_designated_areas` SET `designated_area_id`='$salesmanDes[$x]',`designated_status`='$salesman_status' WHERE `salesman_designated_id` = '$salDesID[$x]'";
                    mysqli_query($con, $updateDes);
                    $j++;
                }
                for($j; $j<$i; $j++)
                {
//                    echo "Delete <br>";
                    $delDes = "DELETE FROM `salesman_designated_areas` WHERE `salesman_designated_id` = '$salDesID[$j]'";
                    mysqli_query($con, $delDes);
                }
            }
            elseif ($i<sizeof($salesmanDes))
            {
                $x=0;
                for($j=0; $j<$i; $j++)
                {
                    $updateDes = "UPDATE `salesman_designated_areas` SET `designated_area_id`='$salesmanDes[$j]',`designated_status`='$salesman_status' WHERE `salesman_designated_id` = '$salDesID[$j]'";
                    mysqli_query($con, $updateDes);
                    $x++;
                }
                for($x; $x<sizeof($salesmanDes); $x++)
                {
                    $insertDes = "INSERT INTO `salesman_designated_areas`(`salesman_id`, `designated_area_id`, `designated_status`) VALUES ('$salesmanId','$salesmanDes[$x]','$salesman_status')";
                    mysqli_query($con, $insertDes);
                }
            }
            else
            {
                for($j=0; $j<$i; $j++)
                {
                    $updateDes = "UPDATE `salesman_designated_areas` SET `designated_area_id`='$salesmanDes[$j]',`designated_status`='$salesman_status' WHERE `salesman_designated_id` = '$salDesID[$j]'";
                    mysqli_query($con, $updateDes);
                }
            }
        }
        header("Location: ViewSalesman.php");
    }
    elseif ($tableName == "dealer_info")
    {
        if($operation == 'del')
        {
            $sqlDelete = "DELETE FROM `dealer_info` WHERE `dealer_id` = '$id'";
            if(mysqli_query($con, $sqlDelete))
            {
                header("Location: ViewDealers.php");
            }
            else
            {

            }
        }
        elseif ($operation == 'update')
        {
            $dealer_id = $_GET['id'];
            $dealer_areaID = $_GET['dealer_areaname'];
            $dealer_name = $_GET['dealer_name'];
            $dealer_contact = $_GET['dealer_contact'];
            $dealer_address = $_GET['dealer_address'];
            $dealer_type = $_GET['dealer_type'];
            $dealer_cnic = $_GET['dealer_cnic'];
            $dealer_licnum = $_GET['dealer_licnum'];
            $dealer_licexp = $_GET['dealer_licexp'];
            $dealer_status = $_GET['dealer_status'];


            $sqlUpdateDealer = "UPDATE `dealer_info` SET `dealer_area_id`='$dealer_areaID',`dealer_name`='$dealer_name',`dealer_contact`='$dealer_contact',`dealer_address`='$dealer_address',`dealer_type`='$dealer_type',`dealer_cnic`='$dealer_cnic',`dealer_lic_num`='$dealer_licnum',`dealer_lic_exp`='$dealer_licexp',`dealer_status`='$dealer_status' WHERE `dealer_id` = '$dealer_id'";
//            echo $sqlUpdateProduct.'<br>';
            if(mysqli_query($con, $sqlUpdateDealer))
            {
                header("Location: ViewDealers.php");
            }
            else
            {

            }
        }
    }
    elseif ($tableName == "area_info")
    {
        if($operation == 'del')
        {
            $sqlDelete = "DELETE FROM `area_info` WHERE `area_id` = '$id'";
            if(mysqli_query($con, $sqlDelete))
            {
                header("Location: ViewAreas.php");
            }
            else
            {

            }
        }
        elseif ($operation == 'update')
        {
            $area_id = $_GET['id'];
            $area_areaname = $_GET['area_areaname'];
            $area_status = $_GET['area_status'];


            $sqlUpdateArea = "UPDATE `area_info` SET `area_name`='$area_areaname',`area_status`='$area_status' WHERE `area_id` = '$area_id'";
//            echo $sqlUpdateProduct.'<br>';
            if(mysqli_query($con, $sqlUpdateArea))
            {
                header("Location: ViewAreas.php");
            }
            else
            {

            }
        }
    }
    elseif ($tableName == "subarea_info")
    {
        if($operation == 'del')
        {
            $sqlDelete = "DELETE FROM `subarea_info` WHERE `subarea_id` = '$id'";
            if(mysqli_query($con, $sqlDelete))
            {
                header("Location: ViewSubareas.php");
            }
            else
            {

            }
        }
        elseif ($operation == 'update')
        {
            $subarea_id = $_GET['id'];
            $subarea_subareaname = $_GET['subarea_subareaname'];
            $subarea_status = $_GET['subarea_status'];


            $sqlUpdateSubArea = "UPDATE `subarea_info` SET `sub_area_name`='$subarea_subareaname',`subarea_status`='$subarea_status' WHERE `subarea_id` = '$subarea_id'";
//            echo $sqlUpdateProduct.'<br>';
            if(mysqli_query($con, $sqlUpdateSubArea))
            {
                header("Location: ViewSubareas.php");
            }
            else
            {

            }
        }
    }
    elseif ($tableName == "order_info")
    {
        $boxCount = 0;
        $packetsCount = 0;
        $totalOrderPrice = 0;
        $getDealerOrderDetails = "SELECT `order_info`.`dealer_id`, SUM(`order_info_detailed`.`quantity`), `order_info_detailed`.`unit`, SUM(`order_info_detailed`. `order_price`) FROM `order_info_detailed` INNER JOIN `order_info` ON `order_info`.`order_id` = `order_info_detailed`.`order_id` WHERE `order_info`.`order_id` = 2 GROUP BY`order_info_detailed`.`unit` ORDER BY `order_info_detailed`.`unit`";
        $getDealerOrderDetails = mysqli_query($con, $getDealerOrderDetails);
        while($dealerOrderInfo = mysqli_fetch_array($getDealerOrderDetails))
        {
            $dealerID = $dealerOrderInfo[0];
            if($dealerOrderInfo[2] == "Box")
            {
                $boxCount = $boxCount + intval($dealerOrderInfo[1]);
            }
            elseif ($dealerOrderInfo[2] == "Packets")
            {
                $packetsCount = $packetsCount + intval($dealerOrderInfo[1]);
            }
            $totalOrderPrice = $totalOrderPrice + floatval($dealerOrderInfo[3]);
            $totalOrderPrice = number_format($totalOrderPrice, 2);
        }

        if($operation == 'del')
        {
            $sqlDelete = "DELETE FROM `order_info` WHERE `order_id` = '$id'";
            if(mysqli_query($con, $sqlDelete))
            {
                $updateDealerOverall = "UPDATE `dealer_overall_record` SET `orders_given`=`orders_given`-1,`ordered_packets`=`ordered_packets`-'$packetsCount',`ordered_boxes`=`ordered_boxes`-'$boxCount',`order_price`=`order_price`-'$totalOrderPrice' WHERE `dealer_id` = '$dealerID'";
                mysqli_query($con, $updateDealerOverall);
                header("Location: ViewPendingOrders.php");
            }
            else
            {

            }
        }
        elseif ($operation == 'update')
        {
            $updateDealerOverall = "UPDATE `dealer_overall_record` SET `orders_given`=`orders_given`-1,`ordered_packets`=`ordered_packets`-'$packetsCount',`ordered_boxes`=`ordered_boxes`-'$boxCount',`order_price`=`order_price`-'$totalOrderPrice' WHERE `dealer_id` = '$dealerID'";
            mysqli_query($con, $updateDealerOverall);

            $dealerID = $_GET['dealerid'];
            $orderedItems = $_GET['ordereditems'];
            $orderedItems = str_replace(",","_-_",$orderedItems);
            $itemsQuantity = $_GET['itemsquantity'];
            $quantArr = explode("_-_", $itemsQuantity);
            $totalQuantity = 0;
            foreach ($quantArr as $quan)
            {
                $totalQuantity = $totalQuantity + intval($quan);
            }
            $boxCount = 0;
            $packetsCount = $totalQuantity;
            $itemsQuantity = str_replace(",","_-_",$itemsQuantity);
            $unit = "Packets";
            $orderPrice = $_GET['orderprice'];
            $bonus = $_GET['bonus'];
            $discount = $_GET['discount'];
            $totalPrice = $_GET['totalprice'];
            $orderDate = $_GET['date'];
            $orderTime = $_GET['time'];
            $orderDate = date("d/M/Y", strtotime($orderDate));
            $orderTime = date("g:i a", strtotime($orderTime));
            $salesmanID = $_GET['salesmanid'];
            $orderStatus = $_GET['status'];

            $sqlUpdateOrder = "UPDATE `order_info` SET `dealer_id`='$dealerID',`product_id`='$orderedItems',`quantity`='$itemsQuantity',`unit`='$unit',`order_price`=$orderPrice,`bonus`='$bonus',`discount`='$discount',`	`final_price`='$totalPrice',`date`='$orderDate',`time`='$orderTime',`salesman_id`='$salesmanID',`status`='$orderStatus' WHERE `order_id` = '$id'";
            if(mysqli_query($con, $sqlUpdateOrder))
            {
                $updateDealerOverall = "UPDATE `dealer_overall_record` SET `orders_given`=`orders_given`+1,`ordered_packets`=`ordered_packets`+'$packetsCount',`ordered_boxes`=`ordered_boxes`+'$boxCount',`order_price`=`order_price`+'$orderPrice' WHERE `dealer_id` = '$dealerID'";
                mysqli_query($con, $updateDealerOverall);
                header("Location: ViewPendingOrders.php");
            }
            else
            {

            }
        }
    }
}
?>