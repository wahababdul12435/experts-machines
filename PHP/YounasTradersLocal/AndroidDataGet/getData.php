<?php
/**
 * Created by PhpStorm.
 * User: Saffi Ullah 786
 * Date: 18-Jan-20
 * Time: 1:15 PM
 */

require_once "../connection.php";

if ($con->connect_error) {
    die("Connection failed: " . $con->connect_error);
}

$data = array();

$softwareId = $_GET['softwareid'];
$tableName = $_GET['tablename'];
if($tableName == 'area_info')
{
    $previousIds = array();
    $sql = "SELECT `software_area_id`, `city_table_id`, `area_name`, `area_status` FROM `area_info` WHERE `software_lic_id` = '$softwareId' AND `area_status` = 'Active'";
    $stmt = $con->prepare($sql);
    $stmt->execute();
    $stmt->bind_result($id, $cityId, $areaName, $areaStatus);
    while($stmt->fetch()){
        array_push($previousIds, $id);
        $temp = [
            'id'=>$id,
            'city_id'=>$cityId,
            'area_name'=>$areaName,
            'area_status'=>$areaStatus
        ];
        array_push($data, $temp);
    }
    echo print_r($previousIds).'<br>';
    if (in_array('2', $previousIds)) {
        echo "Yes";
    }
}
elseif ($tableName == 'dealer_info')
{
    $sql = "SELECT `software_dealer_id`, `dealer_area_id`, `dealer_name`, `dealer_phone`, `dealer_address`, `dealer_type`, `dealer_cnic`, `dealer_lic9_num`, `dealer_lic9_exp`, `dealer_status` FROM `dealer_info` WHERE `software_lic_id` = '$softwareId' AND `dealer_status` = 'Active'";
    $stmt = $con->prepare($sql);
    $stmt->execute();
    $stmt->bind_result($id, $areaId, $dealerName, $dealerContact, $dealerAddress, $dealerType, $dealerCnic, $dealerLicNum, $dealerLicExp, $dealerStatus);
    while($stmt->fetch()){
        $temp = [
            'id'=>$id,
            'areaid'=>$areaId,
            'dealer_name'=>$dealerName,
            'dealer_contact'=>$dealerContact,
            'dealer_address'=>$dealerAddress,
            'dealer_type'=>$dealerType,
            'dealer_cnic'=>$dealerCnic,
            'dealer_licnum'=>$dealerLicNum,
            'dealer_licexp'=>$dealerLicExp,
            'dealer_status'=>$dealerStatus
        ];
        array_push($data, $temp);
    }
    echo json_encode($data);
}
elseif ($tableName == 'product_info')
{
    $sql = "SELECT `software_product_id`, `product_name`, `trade_price`, `product_status`, `company_info`.`company_name` FROM `product_info` LEFT OUTER JOIN `company_info` ON `company_info`.`software_company_id` = `product_info`.`company_table_id` AND `company_info`.`company_status` != 'Deleted' WHERE `product_info`.`software_lic_id` = '$softwareId' AND `product_status` = 'Active'";
    $stmt = $con->prepare($sql);
    $stmt->execute();
    $stmt->bind_result($id, $productName, $finalPrice, $productStatus, $companyName);
    while($stmt->fetch()){
        $temp = [
            'id'=>$id,
            'product_name'=>$productName,
            'final_price'=>$finalPrice,
            'product_status'=>$productStatus,
            'company_name'=>$companyName
        ];
        array_push($data, $temp);
    }
    echo json_encode($data);
}
elseif ($tableName == 'invoices_data')
{
    $sql = "SELECT `order_info`.`software_order_id`, `order_info`.`dealer_id`, `order_info_detailed`.`product_table_id`, `order_info_detailed`.`submission_quantity`, `order_info_detailed`.`submission_unit`, `order_info_detailed`.`final_price`, `order_info`.`status` FROM `order_info_detailed` INNER JOIN `order_info` ON `order_info`.`software_order_id` = `order_info_detailed`.`software_order_id` AND `order_info`.`software_lic_id` = `order_info_detailed`.`software_lic_id` WHERE `order_info`.`software_lic_id` = '1' AND `order_info`.`status` = 'Invoiced'";
    $stmt = $con->prepare($sql);
    $stmt->execute();
    $stmt->bind_result($id, $dealerId, $productIds, $submissionQty, $submissionUnit, $invoicePrice, $orderStatus);
    while($stmt->fetch()){
        $temp = [
            'id'=>$id,
            'dealer_id'=>$dealerId,
            'product_ids'=>$productIds,
            'submission_qty'=>$submissionQty,
            'submission_unit'=>$submissionUnit,
            'invoice_price'=>$invoicePrice,
            'order_status'=>$orderStatus
        ];
        array_push($data, $temp);
    }
    echo json_encode($data);
}
elseif ($tableName == 'dealer_gps_location')
{
    $sql = "select `dealer_info`.`software_dealer_id` from `dealer_info` left outer join `dealer_gps_location` on `dealer_gps_location`.`dealer_id` = `dealer_info`.`software_dealer_id` where `dealer_info`.`software_lic_id` = '$softwareId' AND `dealer_gps_location`.`dealer_id` is null";
    $stmt = $con->prepare($sql);
    $stmt->execute();
    $stmt->bind_result($dealerId);
    while($stmt->fetch()){
        $temp = [
            'dealer_id'=>$dealerId
        ];
        array_push($data, $temp);
    }
    echo json_encode($data);
}
elseif ($tableName == 'dealer_pendings')
{
    $sql = "SELECT `dealer_id`, `pending_payments` FROM `dealer_payments` WHERE `software_lic_id` = '$softwareId' AND `payment_id` IN (SELECT MAX(`payment_id`) FROM `dealer_payments` GROUP BY `dealer_id`)";
    $stmt = $con->prepare($sql);
    $stmt->execute();
    $stmt->bind_result($dealerId, $pendingPayments);
    while($stmt->fetch()){
        $temp = [
            'dealer_id'=>$dealerId,
            'pending_payments'=>$pendingPayments
        ];
        array_push($data, $temp);
    }
    echo json_encode($data);
}
elseif ($tableName == 'batchwise_stock')
{
    $sql = "SELECT `software_batch_stock_id`, `product_table_id`, `batch_no`, `quantity`, `batch_expiry` FROM `batchwise_stock` WHERE `software_lic_id` = '%%' AND `mobile_sync` = '0' ORDER BY `product_table_id`";
    $stmt = $con->prepare($sql);
    $stmt->execute();
    $stmt->bind_result($batchStockId, $productId, $batchNo, $quantity, $batchExpiry);
    while($stmt->fetch()){
        $temp = [
            'batchstock_id'=>$batchStockId,
            'product_id'=>$productId,
            'batch_no'=>$batchNo,
            'quantity'=>$quantity,
            'batch_expiry'=>$batchExpiry
        ];
        array_push($data, $temp);
    }
    echo json_encode($data);
}
elseif($tableName == 'discount')
{
    $date = $_GET['date'];
    $sql = "SELECT `discount_policy`.`approval_id`, `discount_policy`.`dealer_table_id`, `company_info`.`company_name`, `discount_policy`.`product_table_id`, `discount_policy`.`sale_amount`, `discount_policy`.`discount_percent`, `discount_policy`.`start_date`, `discount_policy`.`end_date` FROM `discount_policy` LEFT OUTER JOIN `company_info` ON `company_info`.`software_company_id` = `discount_policy`.`company_table_id` WHERE `discount_policy`.`software_lic_id` = '%%' AND str_to_date(`start_date`, '%d/%b/%Y') <= str_to_date('$date', '%d/%b/%Y') AND str_to_date(`end_date`, '%d/%b/%Y') >= str_to_date('$date', '%d/%b/%Y') AND `policy_status` = 'Active'";
    $stmt = $con->prepare($sql);
    $stmt->execute();
    $stmt->bind_result($approvalId, $dealerId, $companyName, $productId, $saleAmount, $discount, $startDate, $endDate);
    while($stmt->fetch()){
        $temp = [
            'approval_id'=>$approvalId,
            'dealer_id'=>$dealerId,
            'company_name'=>$companyName,
            'product_id'=>$productId,
            'sale_amount'=>$saleAmount,
            'discount'=>$discount,
            'start_date'=>$startDate,
            'end_date'=>$endDate
        ];
        array_push($data, $temp);
    }
    echo json_encode($data);
}

elseif($tableName == 'bonus')
{
    $date = $_GET['date'];
    $sql = "SELECT `bonus_policy`.`approval_id`, `bonus_policy`.`dealer_table_id`, `company_info`.`company_name`, `bonus_policy`.`product_table_id`, `bonus_policy`.`quantity`, `bonus_policy`.`bonus_quant`, `bonus_policy`.`start_date`, `bonus_policy`.`end_date` FROM `bonus_policy` LEFT OUTER JOIN `company_info` ON `company_info`.`software_company_id` = `bonus_policy`.`company_table_id` WHERE `bonus_policy`.`software_lic_id` = '%%' AND str_to_date(`start_date`, '%d/%b/%Y') <= str_to_date('$date', '%d/%b/%Y') AND str_to_date(`end_date`, '%d/%b/%Y') >= str_to_date('$date', '%d/%b/%Y') AND `policy_status` = 'Active'";
    $stmt = $con->prepare($sql);
    $stmt->execute();
    $stmt->bind_result($approvalId, $dealerId, $companyName, $productId, $minQuantity, $bonusQuantity, $startDate, $endDate);
    while($stmt->fetch()){
        $temp = [
            'approval_id'=>$approvalId,
            'dealer_id'=>$dealerId,
            'company_name'=>$companyName,
            'product_id'=>$productId,
            'quantity'=>$minQuantity,
            'bonus'=>$bonusQuantity,
            'start_date'=>$startDate,
            'end_date'=>$endDate
        ];
        array_push($data, $temp);
    }
    echo json_encode($data);
}

//$heroes = array();
//$sql = "SELECT subarea_id, sub_area_name FROM subarea_info;";
//$stmt = $con->prepare($sql);
//$stmt->execute();
//$stmt->bind_result($id, $name);
//while($stmt->fetch()){
//    $temp = [
//        'id'=>$id,
//        'name'=>$name
//    ];
//    array_push($heroes, $temp);
//}
//echo json_encode($heroes);

?>