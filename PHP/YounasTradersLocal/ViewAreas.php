<?php
/**
 * Created by PhpStorm.
 * User: Saffi Ullah 786
 * Date: 07-Nov-19
 * Time: 6:42 PM
 */

require_once "connection.php";

$getSubareas = "SELECT `area_id`, COUNT(`subarea_id`) from `subarea_info` GROUP BY `area_id`";
$getSubareas = mysqli_query($con, $getSubareas);
$x = 0;
while($subData = mysqli_fetch_array($getSubareas))
{
    $areaIDSub[$x] = $subData[0];
    $subareasCount[$x] = $subData[1];
    $x++;
}

$areaInfo = "SELECT * FROM `area_info`";
$areaInfo = mysqli_query($con, $areaInfo);
$i=0;
while($areaData = mysqli_fetch_array($areaInfo))
{
    $areaID[$i] = $areaData[0];
    $areaName[$i] = $areaData[1];
    $areaStatus[$i] = $areaData[2];

    if(in_array($areaID[$i], $areaIDSub))
    {
        $index = array_search($areaID[$i], $areaIDSub);
        $totalSubAreas[$i] = $subareasCount[$index];
    }
    else
    {
        $totalSubAreas[$i] = 0;
    }

    $i++;
}
?>

<html>
<head>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="css/font-awesome.min.css">
    <link type="text/css" rel="stylesheet" href="css/style.css">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>

    <style>
        .operations:hover
        {
            color: #D10024;
            cursor: pointer;
        }
        th
        {
            font-size: 15px;
        }
        td
        {
            font-size: 13px;
        }
    </style>
    <script>
        var areaID = 0;
    </script>
</head>
<body>

<?php
require_once "PopupModel.php";
?>
<div class="container">
    <div style="margin-top: 20px">
        <table id="AreaData" class="display">
            <thead>
            <div>
                <tr>
                    <th style="text-align: center; width: 2%">Area ID</th>
                    <th style="text-align: center; width: 2%">Area Name</th>
                    <th style="text-align: center; width: 2%">Subareas</th>
                    <th style="text-align: center; width: 2%">Status</th>
                    <th style="text-align: center; width: 2%">Operations</th>
                </tr>
            </div>
            </thead>
            <tbody>
            <?php
            for($j=0; $j<$i; $j++)
            {
                ?>
                <tr>
                    <td style="text-align: center"><?php echo $areaID[$j]; ?></td>
                    <td style="text-align: center"><?php echo $areaName[$j]; ?></td>
                    <td style="text-align: center">
                        <?php
                        echo $totalSubAreas[$j];
                        ?>
                    </td>
                    <td style="text-align: center"><?php echo $areaStatus[$j]; ?></td>
                    <td style="text-align: center">
                        <div style="margin-top: 10px">
                            <a data-toggle="modal" data-target="#myModalDel" onclick="delArea('<?php echo $areaID[$j]; ?>')">
                                <i class="fa-lg fa fa-trash operations" title="Delete"></i>&nbsp;&nbsp;
                            </a>
                            <a data-toggle="modal" data-target="#myModalEditArea" onclick="editData('<?php echo $areaID[$j]; ?>', '<?php echo $areaName[$j]; ?>', '<?php echo $totalSubAreas[$j]; ?>', '<?php echo $areaStatus[$j]; ?>')">
                                <i class="fa-lg fa fa-edit operations" title="Edit"></i>
                            </a>
                        </div>
                    </td>
                </tr>

                <?php
            }
            ?>

            </tbody>
        </table>
    </div>
</div>

<script src="js/bootstrap.min.js"></script>
<script src="js/slick.min.js"></script>
<script src="js/nouislider.min.js"></script>
<script src="js/jquery.zoom.min.js"></script>
<script src="js/main.js"></script>

<script>
    $(document).ready(function() {
        $('#AreaData').DataTable(
            {
                // "Item No": [[ 0, "desc" ]]
            }
        );
    });

    function delArea(givenID) {
        areaID = givenID;
    }

    function delConfirmation(op) {
        if(op == 'yes')
        {
            window.location.href = 'SendData.php?table=area_info&op=del&id='+areaID;
        }
        else
        {
            return;
        }
    }

    function editData(id, areaname, subareacount, status) {
        document.getElementById('area_id').value = id;
        document.getElementById('area_areaname').value = areaname;
        document.getElementById('area_subareacount').value = subareacount;
        document.getElementById('area_status').value = status;
    }

    function updateData(op)
    {
        if(op == 'yes')
        {
            area_id = document.getElementById('area_id').value;
            area_areaname = document.getElementById('area_areaname').value;
            area_subareacount = document.getElementById('area_subareacount').value;
            area_status = document.getElementById('area_status').value;

            window.location.href = 'SendData.php?table=area_info&op=update&id='+area_id+'&area_areaname='+area_areaname+'&area_status='+area_status;
        }
        else
        {

        }
    }
</script>
</body>
</html>