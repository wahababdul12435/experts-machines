<?php
/**
 * Created by PhpStorm.
 * User: Saffi Ullah 786
 * Date: 27-Oct-19
 * Time: 11:44 PM
 */

require_once "connection.php";

$getAreasName = "SELECT `subarea_id`, `sub_area_name` FROM `subarea_info`";
$getAreasName = mysqli_query($con, $getAreasName);
$areasCount = 1;
$areaID[0] = 0;
$areaName[0] = "N/A";
while($areaData = mysqli_fetch_array($getAreasName))
{
    $areaID[$areasCount] = $areaData[0];
    $areaName[$areasCount] = $areaData[1];
    $areasCount++;
}

$getDesignatedAreas = "SELECT `salesman_id`, `designated_area_id` FROM `salesman_designated_areas`";
$getDesignatedAreas = mysqli_query($con, $getDesignatedAreas);
$desAreasCount = 0;
while($desAreaData = mysqli_fetch_array($getDesignatedAreas))
{
    $salesmanIDDesignated[$desAreasCount] = $desAreaData[0];
    $desAreaIDDesignated[$desAreasCount] = $desAreaData[1];
    $areaIndex = array_search($desAreaIDDesignated[$desAreasCount], $areaID);
    $desAreaNameID[$desAreasCount] = $areaID[$areaIndex];
    $desAreaName[$desAreasCount] = $areaName[$areaIndex];
    $desAreasCount++;
}

$salesmanInfo = "SELECT * FROM `salesman_info`";
$salesmanInfo = mysqli_query($con, $salesmanInfo);
$i=0;
while ($data = mysqli_fetch_array($salesmanInfo))
{
    $salesmanID[$i] = $data[0];
    $salesmanName[$i] = $data[1];
    $salesmanContact[$i] = $data[2];
    $salesmanAddress[$i] = $data[3];
    $salesmanCNIC[$i] = $data[4];
    $salesmanStatus[$i] = $data[5];
    $getAreasCount = "SELECT COUNT(`designated_area_id`) FROM `salesman_designated_areas` WHERE `salesman_id` = '$salesmanID[$i]'";
    $getAreasCount = mysqli_query($con, $getAreasCount);
    $designatedAreas[$i] = mysqli_fetch_array($getAreasCount)[0];

    $index = array_search($salesmanID[$i], $salesmanIDDesignated);
    $salesmanDesignatedAreasID[$i] = array();
    $salesmanDesignatedAreas[$i] = array();
    while($index<sizeof($salesmanIDDesignated) && $salesmanIDDesignated[$index] == $salesmanID[$i])
    {
        array_push($salesmanDesignatedAreasID[$i], $desAreaNameID[$index]);
        array_push($salesmanDesignatedAreas[$i], $desAreaName[$index]);
        $index++;
    }
    $i++;
}
?>

<html>
<head>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="css/font-awesome.min.css">
    <link type="text/css" rel="stylesheet" href="css/style.css">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>

    <style>
        .operations:hover
        {
            color: #D10024;
            cursor: pointer;
        }
        th
        {
            font-size: 15px;
        }
        td
        {
            font-size: 13px;
        }
        .show-hide-btns
        {
            color: blue;
        }
        .show-hide-btns:hover
        {
            cursor: pointer;
            text-decoration: underline;
        }
    </style>
    <script>
        var salesmanID = 0;
        salesmanDesAreasJSID = [];
        salesmanDesAreasJS = [];
    </script>
</head>
<body>

<?php
require_once "PopupModel.php";
?>
<div class="container">
    <div style="margin-top: 20px">
        <table id="SuppliersData" class="display">
            <thead>
            <div>
                <tr>
                    <th style="text-align: center; width: 2%">ID</th>
                    <th style="text-align: center; width: 2%">Name</th>
                    <th style="text-align: center; width: 2%">Contact</th>
                    <th style="text-align: center; width: 2%">Address</th>
                    <th style="text-align: center; width: 2%">CNIC</th>
                    <th style="text-align: center; width: 2%">Designated Areas</th>
                    <th style="text-align: center; width: 2%">Status</th>
                    <th style="text-align: center; width: 2%">Operations</th>
                </tr>
            </div>
            </thead>
            <tbody>
            <?php
            for($j=0; $j<$i; $j++)
            {
                ?>
                <tr>
                    <td style="text-align: center"><?php echo $salesmanID[$j]; ?></td>
                    <td style="text-align: center"><?php echo $salesmanName[$j]; ?></td>
                    <td style="text-align: center"><?php echo $salesmanContact[$j]; ?></td>
                    <td style="text-align: center"><?php echo $salesmanAddress[$j]; ?></td>
                    <td style="text-align: center"><?php echo $salesmanCNIC[$j]; ?></td>
                    <td style="text-align: center">
                        <span id="<?php echo 'showBtn_'.$j; ?>">
                            <?php echo $designatedAreas[$j]; ?><br>
                            <?php
                            if(sizeof($salesmanDesignatedAreas[$j]) > 0)
                            {
                                ?>
                                <span class="show-hide-btns" onclick="showAreas(<?php echo $j; ?>)">Show</span>
                                <?php
                            }
                            ?>
                        </span>
                        <div id="<?php echo 'areas_'.$j;?>" hidden>
                            <?php
                            for($desI=0; $desI<sizeof($salesmanDesignatedAreas[$j]); $desI++)
                            {
                                echo $salesmanDesignatedAreas[$j][$desI].'<br>';
                            }
                            ?>
                            <script>
                                salesmanDesAreasJS1 =  <?php echo json_encode($salesmanDesignatedAreas[$j]); ?>;
                                salesmanDesAreasJSID1 =  <?php echo json_encode($salesmanDesignatedAreasID[$j]); ?>;
                                // window.alert(salesmanDesAreasJS1.length);
                                salesmanDesAreasJSID.push(salesmanDesAreasJSID1);
                                salesmanDesAreasJS.push(salesmanDesAreasJS1);
                            </script>
                        </div>
                        <span class="show-hide-btns" onclick="hideAreas(<?php echo $j; ?>)" id="<?php echo 'hideBtn_'.$j; ?>" hidden>Hide</span>
                    </td>

                    <td style="text-align: center"><?php echo $salesmanStatus[$j]; ?></td>
                    <td style="text-align: center">
                        <div style="margin-top: 10px">
                            <a data-toggle="modal" data-target="#myModalDel" onclick="delSalesman('<?php echo $salesmanID[$j]; ?>')">
                                <i class="fa-lg fa fa-trash operations" title="Delete"></i>&nbsp;&nbsp;
                            </a>
                            <a data-toggle="modal" data-target="#myModalEditSalesman" onclick="editData('<?php echo $salesmanID[$j]?>', '<?php echo $salesmanName[$j];?>', '<?php echo $salesmanContact[$j]; ?>', '<?php echo $salesmanAddress[$j];?>', '<?php echo $salesmanCNIC[$j];?>', '<?php echo $j; ?>', '<?php echo $salesmanStatus[$j]; ?>')">
                                <i class="fa-lg fa fa-edit operations" title="Edit"></i>
                            </a>
                        </div>
                    </td>
                </tr>

                <?php
            }
            ?>

            </tbody>
        </table>
    </div>
</div>

<script src="js/bootstrap.min.js"></script>
<script src="js/slick.min.js"></script>
<script src="js/nouislider.min.js"></script>
<script src="js/jquery.zoom.min.js"></script>
<script src="js/main.js"></script>

<script>
    $(document).ready(function() {
        $('#SuppliersData').DataTable(
            {
                // "Item No": [[ 0, "desc" ]]
            }
        );
    } );

    function delSalesman(givenID) {
        salesmanID = givenID;
    }

    function delConfirmation(op) {
        if(op == 'yes')
        {
            window.location.href = 'SendData.php?table=salesman_info&op=del&id='+salesmanID;
        }
        else
        {
            return;
        }
    }

    function editData(id, name, contact, address, cnic, des, status) {
        document.getElementById('salesman_id').value = id;
        document.getElementById('salesman_name').value = name;
        document.getElementById('salesman_contact').value = contact;
        document.getElementById('salesman_address').value = address;
        document.getElementById('salesman_cnic').value = cnic;

        document.getElementById('salesman_des_1').value = 0;
        document.getElementById('salesman_des_2').value = 0;
        document.getElementById('salesman_des_3').value = 0;
        document.getElementById('salesman_des_4').value = 0;

        if(salesmanDesAreasJS[des].length == 4)
        {
            document.getElementById('salesman_des_1').value = salesmanDesAreasJSID[des][0];
            document.getElementById('salesman_des_2').value = salesmanDesAreasJSID[des][1];
            document.getElementById('salesman_des_3').value = salesmanDesAreasJSID[des][2];
            document.getElementById('salesman_des_4').value = salesmanDesAreasJSID[des][3];
        }
        else if(salesmanDesAreasJS[des].length == 3)
        {
            document.getElementById('salesman_des_1').value = salesmanDesAreasJSID[des][0];
            document.getElementById('salesman_des_2').value = salesmanDesAreasJSID[des][1];
            document.getElementById('salesman_des_3').value = salesmanDesAreasJSID[des][2];
        }
        else if(salesmanDesAreasJS[des].length == 2)
        {
            document.getElementById('salesman_des_1').value = salesmanDesAreasJSID[des][0];
            document.getElementById('salesman_des_2').value = salesmanDesAreasJSID[des][1];
        }
        else if(salesmanDesAreasJS[des].length == 1)
        {
            document.getElementById('salesman_des_1').value = salesmanDesAreasJSID[des][0];
        }

        document.getElementById('salesman_status').value = status;
    }

    function updateData(op)
    {
        if(op == 'yes')
        {
            salesman_id = document.getElementById('salesman_id').value;
            salesman_name = document.getElementById('salesman_name').value;
            salesman_contact = document.getElementById('salesman_contact').value;
            salesman_address = document.getElementById('salesman_address').value;
            salesman_cnic = document.getElementById('salesman_cnic').value;
            salesman_des_1 = document.getElementById('salesman_des_1').value;
            salesman_des_2 = document.getElementById('salesman_des_2').value;
            salesman_des_3 = document.getElementById('salesman_des_3').value;
            salesman_des_4 = document.getElementById('salesman_des_4').value;
            salesman_status = document.getElementById('salesman_status').value;

            window.location.href = 'SendData.php?table=salesman_info&op=update&id='+salesman_id+'&salesman_name='+salesman_name+'&salesman_contact='+salesman_contact+'&salesman_address='+salesman_address+'&salesman_cnic='+salesman_cnic+'&salesman_des_1='+salesman_des_1+'&salesman_des_2='+salesman_des_2+'&salesman_des_3='+salesman_des_3+'&salesman_des_4='+salesman_des_4+'&salesman_status='+salesman_status;
        }
        else
        {

        }
    }

    function showAreas(id) {
        document.getElementById('areas_'+id).hidden = false;
        document.getElementById('showBtn_'+id).hidden = true;
        document.getElementById('hideBtn_'+id).hidden = false;
    }

    function hideAreas(id) {
        document.getElementById('areas_'+id).hidden = true;
        document.getElementById('showBtn_'+id).hidden = false;
        document.getElementById('hideBtn_'+id).hidden = true;
    }
</script>
</body>
</html>