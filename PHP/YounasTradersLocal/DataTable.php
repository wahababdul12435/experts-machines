<?php

?>
<html>
<head>
    <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css"/>

    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div style="margin-top: 20px">
        <table id="NewPendingsData" class="display" width="100%">
            <thead>
            <div style="padding-top: 0%" id="div4">
                <tr>
                    <th style="text-align: center; width: 2%">Image</th>
                    <th style="text-align: center; width: 2%">Item No</th>
                    <th style="text-align: center; width: 2%">Category</th>
                    <th style="text-align: center; width: 2%">Item Name</th>
                    <th style="text-align: center; width: 2%">Quantity</th>
                    <th style="text-align: center; width: 2%">Return Days</th>
                    <th style="text-align: center; width: 2%">Warranty</th>
                    <th style="text-align: center; width: 2%">Price</th>
                </tr>
            </div>
            </thead>
            <tbody>
            <tr>
                <td style="text-align: center">1</td>
                <td style="text-align: center">2</td>
                <td style="text-align: center">3</td>
                <td style="text-align: center">4</td>
                <td style="text-align: center">5</td>
                <td style="text-align: center">6</td>
                <td style="text-align: center">7</td>
                <td style="text-align: center">8</td>
            </tr>
            <tr>
                <td style="text-align: center">1</td>
                <td style="text-align: center">2</td>
                <td style="text-align: center">3</td>
                <td style="text-align: center">4</td>
                <td style="text-align: center">5</td>
                <td style="text-align: center">6</td>
                <td style="text-align: center">7</td>
                <td style="text-align: center">8</td>
            </tr>
            <tr>
                <td style="text-align: center">1</td>
                <td style="text-align: center">2</td>
                <td style="text-align: center">3</td>
                <td style="text-align: center">4</td>
                <td style="text-align: center">5</td>
                <td style="text-align: center">6</td>
                <td style="text-align: center">7</td>
                <td style="text-align: center">8</td>
            </tr>

            </tbody>
        </table>
    </div>
</div>


<script src="js/bootstrap.min.js"></script>
<script src="js/slick.min.js"></script>
<script src="js/nouislider.min.js"></script>
<script src="js/jquery.zoom.min.js"></script>
<script src="js/main.js"></script>

<script>
    $(document).ready(function() {
        $('#NewPendingsData').DataTable(
            {
                "Item No": [[ 0, "desc" ]]
            }
        );
    } );
</script>
</body>
</html>
