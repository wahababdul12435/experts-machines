<?php
/**
 * Created by PhpStorm.
 * User: Saffi Ullah 786
 * Date: 11-Nov-19
 * Time: 11:20 PM
 */

require_once "connection.php";
//date_default_timezone_set("Asia/Karachi");
//$date = date("d/M/Y");
//$time = date("g:i A");

$type = $_GET['type'];
$softwareId = $_GET['softwareid'];


if($type == "Booking")
{
    $sent = false;
//    $dealerId = "6_--_7";
//    $productId = "466_-_470_--_4_-_107";
//    $quantity = "5_-_10_--_10_-_15";
//    $unit = "Packets_-_Packets_--_Packets_-_Packets";
//    $orderPrice = "290_-_150_--_290_-_150";
//    $bonus = "0_-_0_--_0_-_0";
//    $discount = "0.00_-_0.00_--_0.00_-_0.00";
//    $finalPrice = "290_-_150_--_290_-_150";
//    $latitude = "32.5709_--_32.5709";
//    $longitude = "74.0754_--_74.0754";
//    $orderPlaceArea = "Dhakki Chowk Gujrat Pakistan_--_Dhakki Chowk Gujrat Pakistan";
//    $date = "25/Mar/2021_--_25/Mar/2021";
//    $time = "01:18 pm_--_01:18 pm";
//    $salesmanId = "1_--_1";
//    $status = "Pending_--_Pending";

//    $dealerId = "2";
//    $productId = "466";
//    $quantity = "15";
//    $unit = "Packets";
//    $orderPrice = "2682.0";
//    $bonus = "0";
//    $discount = "0.00_-_0.00";
//    $finalPrice = "2682.0";
//    $latitude = "32.587898333333335";
//    $longitude = "74.14539833333333";
//    $orderPlaceArea = "School Road, Sook Kalan Gujrat, Punjab 50700, Pakistan";
//    $date = "21/Mar/2021";
//    $time = "01:18 pm";
//    $salesmanId = "1";
//    $status = "Pending";

    $dealerId = $_POST["dealer_id"];
    $productId = $_POST["product_id"];
    $quantity = $_POST["quantity"];
    $unit = $_POST["unit"];
    $orderPrice = $_POST["orderprice"];
    $bonus = $_POST["bonus"];
    $discount = $_POST["discount"];
    $finalPrice = $_POST["finalprice"];
    $latitude = $_POST["latitude"];
    $longitude = $_POST["longitude"];
    $orderPlaceArea = $_POST["order_place_area"];
    $date = $_POST["date"];
    $time = $_POST["time"];
    $salesmanId = $_POST["salesman_id"];
    $status = $_POST["status"];

    $dealerIdMain = explode("_--_", $dealerId);
    $productIdMain = explode("_--_", $productId);
    $quantityMain = explode("_--_", $quantity);
    $unitMain = explode("_--_", $unit);
    $orderPriceMain = explode("_--_", $orderPrice);
    $bonusMain = explode("_--_", $bonus);
    $discountMain = explode("_--_", $discount);
    $finalPriceMain = explode("_--_", $finalPrice);
    $latitudeMain = explode("_--_", $latitude);
    $longitudeMain = explode("_--_", $longitude);
    $orderPlaceAreaMain = explode("_--_", $orderPlaceArea);
    $dateMain = explode("_--_", $date);
    $timeMain = explode("_--_", $time);
    $salesmanIdMain = explode("_--_", $salesmanId);
    $statusMain = explode("_--_", $status);

    for($j=0; $j<sizeof($dealerIdMain); $j++)
    {
        $dealerId = $dealerIdMain[$j];
        $productId = $productIdMain[$j];
        $quantity = $quantityMain[$j];
        $unit = $unitMain[$j];
        $orderPrice = $orderPriceMain[$j];
        $bonus = $bonusMain[$j];
        $discount = $discountMain[$j];
        $finalPrice = $finalPriceMain[$j];
        $latitude = $latitudeMain[$j];
        $longitude = $longitudeMain[$j];
        $orderPlaceArea = $orderPlaceAreaMain[$j];
        $date = $dateMain[$j];
        $time = $timeMain[$j];
        $salesmanId = $salesmanIdMain[$j];
        $status = $statusMain[$j];


        $productIdArr = explode("_-_", $productId);
        $quantArr = explode("_-_", $quantity);
        $unitArr = explode("_-_", $unit);
        $orderPriceArr = explode("_-_", $orderPrice);
        $bonusArr = explode("_-_", $bonus);
        $discountArr = explode("_-_", $discount);
        $finalPriceArr = explode("_-_", $finalPrice);
        $boxCount = 0;
        $packetsCount = 0;

        $totalOrderPrice = 0;
        $totalDiscount = 0;
        $totalFinalPrice = 0;

        for($i=0; $i<sizeof($quantArr); $i++)
        {
            if($unitArr[$i] == "Box")
            {
                $boxCount = $boxCount + intval($quantArr[$i]);
            }
            elseif($unitArr[$i] == "Packets")
            {
                $packetsCount = $packetsCount + intval($quantArr[$i]);
            }
            $totalOrderPrice = $totalOrderPrice + $orderPriceArr[$i];
            $totalDiscount = $totalDiscount + $discountArr[$i];
            $totalFinalPrice = $totalFinalPrice + $finalPriceArr[$i];
        }
        $sqlInsertOrder = "INSERT INTO `order_info`(`dealer_id`, `product_id`, `quantity`, `unit`, `order_price`, `bonus`, `discount`, `waive_off_price`, `final_price`, `order_success`, `booking_latitude`, `booking_longitude`, `booking_area`, `booking_date`, `booking_time`, `booking_user_id`, `status`, `send_status_computer`, `software_lic_id`) VALUES ('$dealerId','$productId','$quantity','$unit', '$totalOrderPrice', '$bonus', '$totalDiscount', '0', '$totalFinalPrice', '1', '$latitude', '$longitude', '$orderPlaceArea','$date','$time', '$salesmanId','$status', 'Pending', '$softwareId')";
        if(mysqli_query($con, $sqlInsertOrder))
        {
            $sent = true;
        }
        else
        {
            $sent = false;
        }

        $orderId = "SELECT `order_id` FROM `order_info` WHERE `software_lic_id` = '$softwareId' AND `dealer_id` = '$dealerId' AND `booking_date` = '$date' AND `booking_time` = '$time'";
        $orderId = mysqli_query($con, $orderId);
        $orderId = mysqli_fetch_array($orderId)[0];

//     ----------------------------- Insert Order Detail ------------------------
//     ----------------------------- Insert Order Detail ------------------------

        $batchPolicy = "SELECT `setting_value` FROM `system_settings` WHERE `software_lic_id` = '$softwareId' AND `setting_name` = 'batch_policy'";
        $batchPolicy = mysqli_query($con, $batchPolicy);
        $batchPolicy = mysqli_fetch_array($batchPolicy)[0];
        for($i=0; $i<sizeof($productIdArr); $i++)
        {
            $orderedQty = $quantArr[$i];
            $submissionQty = 0;
            $detailInsertion = false;
            $totalOrderedQty = $quantArr[$i];
            $batchOrderedPrice = 0;
            $batchDiscountPrice = 0;
            $batchFinalPrice = 0;
            if($batchPolicy == "Expiry FIFO")
            {
                $batchResults = "SELECT `batch_stock_id`, `quantity`, `trade_price` FROM `batchwise_stock` WHERE `software_lic_id` = '$softwareId' AND `prod_id` = '$productIdArr[$i]' AND `quantity` > 0 ORDER BY str_to_date(`batch_expiry`, '%d/%b/%Y')";
            }
            elseif ($batchPolicy == "Expiry LIFO")
            {
                $batchResults = "SELECT `batch_stock_id`, `quantity`, `trade_price` FROM `batchwise_stock` WHERE `software_lic_id` = '$softwareId' AND `prod_id` = '$productIdArr[$i]' AND `quantity` > 0 ORDER BY str_to_date(`batch_expiry`, '%d/%b/%Y') DESC";
            }
            elseif ($batchPolicy == "Entry FIFO")
            {
                $batchResults = "SELECT `batch_stock_id`, `quantity`, `trade_price` FROM `batchwise_stock` WHERE `software_lic_id` = '$softwareId' AND `prod_id` = '$productIdArr[$i]' AND `quantity` > 0 ORDER BY str_to_date(`entry_date`, '%d/%b/%Y')";
            }
            else
            {
                $batchResults = "SELECT `batch_stock_id`, `quantity`, `trade_price` FROM `batchwise_stock` WHERE `software_lic_id` = '$softwareId' AND `prod_id` = '$productIdArr[$i]' AND `quantity` > 0 ORDER BY str_to_date(`entry_date`, '%d/%b/%Y') DESC";
            }

//        echo "Select Batch No: ".$batchResults.'<br>';
            $batchResults = mysqli_query($con, $batchResults);
            $totalBatches = mysqli_num_rows($batchResults);
//        echo "Mysqli Num Rows: ".$totalBatches.'<br>';
            while($batchData = mysqli_fetch_array($batchResults))
            {
                $totalBatches--;
                $batchNo = $batchData[0];
                $batchQty = $batchData[1];
                $batchPrice = $batchData[2];
                if($orderedQty <= $batchQty)
                {
                    $submissionQty = $orderedQty;
                    $orderedQty = $orderedQty - $submissionQty;
                    $totalOrderedQty = $totalOrderedQty - $submissionQty;
                }
                else
                {
                    $submissionQty = $batchQty;
                    $orderedQty = $orderedQty - $submissionQty;
                    $totalOrderedQty = $totalOrderedQty - $submissionQty;
                }
//                $batchOrderedPrice = $orderPriceArr[$i];
                $batchOrderedPrice = $batchPrice*$submissionQty;
//                $batchDiscountPrice = $discountArr[$i];
                $discountPercent = ($discountArr[$i]/$orderPriceArr[$i]) * 100;
//                $batchFinalPrice = $finalPriceArr[$i];
                $batchDiscountPrice = (($batchOrderedPrice/100) * $discountPercent);
                $batchFinalPrice = $batchOrderedPrice - $batchDiscountPrice;

                if($totalBatches == 0 && $submissionQty > 0)
                {
                    $totalOrderedQty = $totalOrderedQty + $submissionQty;
                    $insertOrderInfoDetailed = "INSERT INTO `order_info_detailed` (`order_id`, `product_id`, `batch_id`, `quantity`, `submission_quantity`, `unit`, `submission_unit`, `order_price`, `bonus_quant`, `discount`, `final_price`, `cmplt_returned_bit`, `returned_quant`, `returned_bonus_quant`, `software_lic_id`) VALUES ('$orderId', '$productIdArr[$i]', '$batchNo', '$totalOrderedQty', '$submissionQty', '$unitArr[$i]', '$unitArr[$i]', '$batchOrderedPrice', '$bonusArr[$i]', '$batchDiscountPrice', '$batchFinalPrice', '0', '0', '0', '$softwareId')";
//                echo "Total Batches = 0   ".$insertOrderInfoDetailed.'<br>';
                    if(mysqli_query($con, $insertOrderInfoDetailed))
                    {
//                    echo "Detail Insert 1".'<br>';
                    }
                }
                elseif($totalBatches != 0 && $submissionQty > 0)
                {
                    $insertOrderInfoDetailed = "INSERT INTO `order_info_detailed` (`order_id`, `product_id`, `batch_id`, `quantity`, `submission_quantity`, `unit`, `submission_unit`, `order_price`, `bonus_quant`, `discount`, `final_price`, `cmplt_returned_bit`, `returned_quant`, `returned_bonus_quant`, `software_lic_id`) VALUES ('$orderId', '$productIdArr[$i]', '$batchNo', '$submissionQty', '$submissionQty', '$unitArr[$i]', '$unitArr[$i]', '$batchOrderedPrice', '$bonusArr[$i]', '$batchDiscountPrice', '$batchFinalPrice', '0', '0', '0', '$softwareId')";
//                    echo "Total Batches != 0   ".$insertOrderInfoDetailed.'<br>';
                    if(mysqli_query($con, $insertOrderInfoDetailed))
                    {
//                    echo "Detail Insert 1".'<br>';
                    }
                }


                if($submissionQty > 0)
                {
                    $updateBatchStock = "UPDATE `batchwise_stock` SET `quantity` = `quantity` - '$submissionQty', `mobile_sync` = '0' WHERE `software_lic_id` = '$softwareId' AND `prod_id` = '$productIdArr[$i]' AND `batch_stock_id` = '$batchNo'";
//                echo "Update Batch Stock   ".$updateBatchStock.'<br>';
                    if(mysqli_query($con, $updateBatchStock))
                    {
//                    echo "Batch Stock Updated".'<br>';
                    }

                    $updateProductStock = "UPDATE `products_stock` SET `in_stock` = `in_stock` - '$submissionQty' WHERE `software_lic_id` = '$softwareId' AND `product_id` = '$productIdArr[$i]'";
//                echo "Update Product Stock   ".$updateProductStock.'<br>';
                    if(mysqli_query($con, $updateProductStock))
                    {
//                    echo "Product Stock Updated".'<br>';
                    }
                }
                $detailInsertion = true;
            }

            if(!$detailInsertion)
            {
                $batchOrderedPrice = $orderPriceArr[$i];
                $batchDiscountPrice = $discountArr[$i];
                $batchFinalPrice = $finalPriceArr[$i];
                $insertOrderInfoDetailed = "INSERT INTO `order_info_detailed`(`order_id`, `product_id`, `batch_id`, `quantity`, `submission_quantity`, `unit`, `submission_unit`, `order_price`, `bonus_quant`, `discount`, `final_price`, `cmplt_returned_bit`, `returned_quant`, `returned_bonus_quant`, `software_lic_id`) VALUES ('$orderId', '$productIdArr[$i]', '0', '$totalOrderedQty', '$totalOrderedQty', '$unitArr[$i]', '$unitArr[$i]', '$batchOrderedPrice', '$bonusArr[$i]', '$batchDiscountPrice', '$batchFinalPrice', '0', '0', '0', '$softwareId')";
//                echo "Detailed Insertion   ".$insertOrderInfoDetailed.'<br>';
                mysqli_query($con, $insertOrderInfoDetailed);
            }

        }

//     ----------------------------- Insert Order Detail ------------------------
//     ----------------------------- Insert Order Detail ------------------------

        $addOrderLocation = "INSERT INTO `order_gps_location`(`order_id`, `user_id`, `latitude`, `longitude`, `loc_name`, `date`, `time`, `dealer_id`, `price`, `type`, `software_lic_id`) VALUES ('$orderId', '$salesmanId','$latitude','$longitude','$orderPlaceArea','$date','$time','$dealerId','$finalPrice','Booking', '$softwareId')";
        mysqli_query($con, $addOrderLocation);

        $updateDealerOverall = "UPDATE `dealer_overall_record` SET `orders_given`=`orders_given`+1,`ordered_packets`=`ordered_packets`+'$packetsCount',`ordered_boxes`=`ordered_boxes`+'$boxCount',`order_price`=`order_price`+'$orderPrice' WHERE `software_lic_id` = '$softwareId' AND `dealer_id` = '$dealerId'";
        if(mysqli_query($con, $updateDealerOverall))
        {
            $num = mysqli_affected_rows($con);
            if($num == 0)
            {
                $addDealerOverall = "INSERT INTO `dealer_overall_record`(`dealer_id`, `orders_given`, `successful_orders`, `ordered_packets`, `submitted_packets`, `ordered_boxes`, `submitted_boxes`, `order_price`, `discount_price`, `invoiced_price`, `cash_collected`, `return_packets`, `return_boxes`, `return_price`, `cash_return`, `waived_off_price`, `pending_payments`, `software_lic_id`) VALUES ('$dealerId','1','0',$packetsCount,'0','$boxCount','0','$orderPrice','0','0','0','0','0','0','0','0','0', '$softwareId')";
                mysqli_query($con, $addDealerOverall);
            }
        }
    }
    if($sent)
    {
        echo "Order Sent";
    }
    else
    {
        echo "Order Not Sent";
    }
}
elseif ($type == "Delivered")
{
//    $orderId = "2";
//    $dealerId = "3";
//    $cashCollected = "5000";
//    $latitude = "32.587898333333335";
//    $longitude = "74.14539833333333";
//    $deliveryLocation = "Sook Kalan Road, Sook Kalan";
//    $date = "13/Apr/2020";
//    $time = "9:28 PM";
//    $userId = "1";
//    $status = "Delivered";


    $orderId = $_POST["order_id"];
    $dealerId = $_POST["dealer_id"];
    $cashCollected = $_POST["cash_collected"];
    $latitude = $_POST["latitude"];
    $longitude = $_POST["longitude"];
    $deliveryLocation = $_POST["location_name"];
    $date = $_POST["date"];
    $time = $_POST["time"];
    $userId = $_POST["user_id"];
    $status = $_POST["status"];

    $updateOrderStatus = "UPDATE `order_info` SET `status`='$status', `send_status_computer` = 'Pending' WHERE `software_lic_id` = '$softwareId' AND `order_id` = '$orderId'";
    mysqli_query($con, $updateOrderStatus);

    $addDeliveryLocation = "INSERT INTO `order_gps_location`(`order_id`, `user_id`, `latitude`, `longitude`, `loc_name`, `date`, `time`, `dealer_id`, `price`, `type`, `software_lic_id`) VALUES ('$orderId','$userId','$latitude','$longitude','$deliveryLocation','$date','$time','$dealerId','$cashCollected','Delivered', '$softwareId')";
    mysqli_query($con, $addDeliveryLocation);

    $orderDetail = "SELECT * FROM `order_info_detailed` WHERE `software_lic_id` = '$softwareId' AND `software_order_id` = '$orderId'";
    $orderDetail = mysqli_query($con, $orderDetail);
    $packetsCount = 0;
    $boxCount = 0;
    $orderSuccess = 1;
    $discount = 0;
    while($orderData = mysqli_fetch_array($orderDetail))
    {
        if(($orderData[5] != $orderData[6]) || ($orderData[7] != $orderData[8]))
        {
            $orderSuccess = 0;
        }
        if($orderData[8] == "Packets")
        {
            $packetsCount += $orderData[6];
        }
        else
        {
            $boxCount += $orderData[6];
        }
        $discount += ($orderData[9]/100)*$orderData[11];
    }

    $updateDealerOverall = "UPDATE `dealer_overall_record` SET `successful_orders`= `successful_orders` + '$orderSuccess', `submitted_packets` = `submitted_packets` + '$packetsCount', `submitted_boxes` = `submitted_boxes` + '$boxCount', `discount_price` = `discount_price` + '$discount', `cash_collected` = `cash_collected` + '$cashCollected', `pending_payments` = `invoiced_price` - (`waived_off_price` + `cash_collected`) WHERE `software_lic_id` = '$softwareId' AND `dealer_id` = '$dealerId'";
    if(mysqli_query($con, $updateDealerOverall))
    {
        echo 'Delivered Sent';
    }
    else
    {
        echo 'Delivered Not Sent';
    }
}
elseif ($type == "CashCollected")
{
//    $dealerId = "3";
//    $cashCollected = "5000";
//    $cashType = "Collection";
//    $latitude = "32.587898333333335";
//    $longitude = "32.587898333333335";
//    $deliveryLocation = "Sook Kalan Road, Sook Kalan";
//    $date = "13/Apr/2020";
//    $time = "9:28 PM";
//    $userId = "1";
//    $status = "Delivered";


    $dealerId = $_POST["dealer_id"];
    $cashCollected = $_POST["cash_collected"];
    $cashType = $_POST["cash_type"];
    $latitude = $_POST["latitude"];
    $longitude = $_POST["longitude"];
    $deliveryLocation = $_POST["location_name"];
    $date = $_POST["date"];
    $time = $_POST["time"];
    $userId = $_POST["user_id"];
    $status = $_POST["status"];

    $dateTemp = str_replace('/', '-', $date);
    $day = date('l', strtotime($dateTemp));

    $addCollection = "";
    $dealerPendingPayments = "SELECT `pending_payments` FROM `dealer_payments` WHERE `software_lic_id` = '$softwareId' AND `dealer_id` = '$dealerId' ORDER BY `payment_id` DESC LIMIT 1";
    $dealerPendingPayments = mysqli_query($con, $dealerPendingPayments);
    $dealerPendingPayments = mysqli_fetch_array($dealerPendingPayments)[0];

    if($cashType == "Collection")
    {
        $dealerPendingPayments = $dealerPendingPayments - $cashCollected;
        $addCollection = "INSERT INTO `dealer_payments`(`dealer_id`, `order_id`, `amount`, `cash_type`, `pending_payments`, `date`, `day`, `user_id`, `status`, `send_status_computer`, `software_lic_id`) VALUES ('$dealerId', '0', '$cashCollected','$cashType','$dealerPendingPayments','$date','$day','$userId','$status', 'Pending', '$softwareId')";
    }
    elseif($cashType == "Return")
    {
        $dealerPendingPayments = $dealerPendingPayments + $cashCollected;
        $addCollection = "INSERT INTO `dealer_payments`(`dealer_id`, `order_id`, `amount`, `cash_type`, `pending_payments`, `date`, `day`, `user_id`, `status`, `send_status_computer`, `software_lic_id`) VALUES ('$dealerId', '0', '$cashCollected','$cashType', '$dealerPendingPayments','$date','$day','$userId','$status', 'Pending', '$softwareId')";
    }
    echo $addCollection;

    if(mysqli_query($con, $addCollection))
    {
        echo "Collection Sent Successfully";
    }
    else
    {
        echo "Collection Not Sent";
    }

    $addDeliveryLocation = "INSERT INTO `order_gps_location`(`user_id`, `latitude`, `longitude`, `loc_name`, `date`, `time`, `dealer_id`, `price`, `type`, `software_lic_id`) VALUES ('$userId','$latitude','$longitude','$deliveryLocation','$date','$time','$dealerId','$cashCollected','Cash '.'$cashType', '$softwareId')";
    mysqli_query($con, $addDeliveryLocation);

    if($cashType == "Return")
    {
        $updateDealerOverall = "UPDATE `dealer_overall_record` SET `cash_return` = `cash_return` + '$cashCollected', `pending_payments` = `invoiced_price` - `waived_off_price` - `cash_return` WHERE `software_lic_id` = '$softwareId' AND `dealer_id` = '$dealerId'";
        mysqli_query($con, $updateDealerOverall);
    }
    else
    {
        $updateDealerOverall = "UPDATE `dealer_overall_record` SET `cash_collected` = `cash_collected` + '$cashCollected', `pending_payments` = `invoiced_price` - (`waived_off_price` + `cash_collected`) WHERE `software_lic_id` = '$softwareId' AND `dealer_id` = '$dealerId'";
        mysqli_query($con, $updateDealerOverall);
    }
}
elseif ($type == "Return")
{
//    $orderId = "0";
//    $dealerId = "1";
//    $productId = "4_-_107";
//    $quantity = "10_-_15";
//    $unit = "Packets_-_Packets";
//    $batchNo =  "5AQ157_-_PMT140";
//    $returnPrice = "100_-_160";
//    $totalReturnPrice = "260";
//    $returnReason = "Expiry_-_Expiry";
//    $cashReturn = "100";
//    $latitude = "32.587898333333335";
//    $longitude = "32.587898333333335";
//    $locationName = "Sook Kalan Road, Sook Kalan";
//    $date = "03/Jun/2020";
//    $time = "6:10 PM";
//    $userId = "1";
//    $status = "Return";

//    $orderId = "0";
//    $dealerId = "2";
//    $productId = "466";
//    $quantity = "10";
//    $unit = "Packets";
//    $batchNo =  "";
//    $returnPrice = "1788.0";
//    $totalReturnPrice = "1788";
//    $returnReason = "Expiry";
//    $cashReturn = "1000";
//    $latitude = "32.587898333333335";
//    $longitude = "32.587898333333335";
//    $locationName = "Sook Kalan Road, Sook Kalan";
//    $date = "20/Nov/2021";
//    $time = "6:10 PM";
//    $userId = "123";
//    $status = "Return";

    $orderId = $_POST["order_id"];
    $dealerId = $_POST["dealer_id"];
    $productId = $_POST["product_id"];
    $quantity = $_POST["quantity"];
    $unit = $_POST["unit"];
    $batchNo = $_POST["batch_no"];
    $returnPrice = $_POST["return_price"];
    $totalReturnPrice = $_POST["total_return_price"];
    $returnReason = $_POST["return_reason"];
    $cashReturn = $_POST["cash_return"];
    $latitude = $_POST["latitude"];
    $longitude = $_POST["longitude"];
    $locationName = $_POST["return_area"];
    $date = $_POST["date"];
    $time = $_POST["time"];
    $userId = $_POST["user_id"];
    $status = $_POST["status"];


    $productArr = explode("_-_", $productId);
    $batchArr = explode("_-_", $batchNo);
    $quantArr = explode("_-_", $quantity);
    $unitArr = explode("_-_", $unit);
    $returnPriceArr = explode("_-_", $returnPrice);
    $returnReasonArr = explode("_-_", $returnReason);
    $boxCount = 0;
    $packetsCount = 0;

    $dateTemp = str_replace('/', '-', $date);
    $day = date('l', strtotime($dateTemp));

//    echo sizeof($productArr).'<br>';
//    echo sizeof($batchArr).'<br>';
//    echo sizeof($quantArr).'<br>';
//    echo sizeof($unitArr).'<br>';
//    echo sizeof($returnPriceArr).'<br>';
//    echo sizeof($returnReasonArr).'<br>';

    for($i=0; $i<sizeof($productArr); $i++)
    {
        if($unitArr[$i] == "Box")
        {
            $boxCount = $boxCount + intval($quantArr[$i]);
        }
        elseif($unitArr[$i] == "Packets")
        {
            $packetsCount = $packetsCount + intval($quantArr[$i]);
        }
    }

    $updateDealerOverall = "UPDATE `dealer_overall_record` SET `return_packets` = `return_packets` + '$packetsCount', `return_boxes` = `return_boxes` + '$boxCount', `return_price`= `return_price` + '$totalReturnPrice', `cash_return`= `cash_return` + '$cashReturn', `pending_payments`= `invoiced_price` - `cash_collected` - `return_price` + `cash_return` - `waived_off_price` WHERE `software_lic_id` = '$softwareId' AND `dealer_id` = '$dealerId'";
    mysqli_query($con, $updateDealerOverall);

    $dealerPendingPayments = "SELECT `pending_payments` FROM `dealer_payments` WHERE `software_lic_id` = '$softwareId' AND `dealer_id` = '$dealerId' ORDER BY `payment_id` DESC LIMIT 1";
    $dealerPendingPayments = mysqli_query($con, $dealerPendingPayments);
    $dealerPendingPayments = mysqli_fetch_array($dealerPendingPayments)[0];
    $dealerPendingPayments = $dealerPendingPayments + $cashReturn;

    $insertDealerPayment = "INSERT INTO `dealer_payments`(`dealer_id`, `order_id`, `amount`, `cash_type`, `pending_payments`, `date`, `day`, `user_id`, `status`, `send_status_computer`, `software_lic_id`) VALUES ('$dealerId', '$orderId', '$cashReturn','Return','$dealerPendingPayments','$date','$day','$userId','$status', 'Pending', '$softwareId')";
    mysqli_query($con, $insertDealerPayment);

    $insertOrderGPS = "INSERT INTO `order_gps_location`(`order_id`, `user_id`, `latitude`, `longitude`, `loc_name`, `date`, `time`, `dealer_id`, `price`, `type`, `software_lic_id`) VALUES ('$orderId', '$userId', '$latitude', '$longitude', '$locationName', '$date', '$time', '$dealerId', '$totalReturnPrice', 'Return', '$softwareId')";
    mysqli_query($con, $insertOrderGPS);

    $insertOrderReturn = "INSERT INTO `order_return`(`dealer_id`, `return_total_price`, `return_gross_price`, `order_id`, `entry_date`, `entry_time`, `enter_user_id`, `comments`, `status`, `send_status_computer`, `software_lic_id`) VALUES ('$dealerId', '$totalReturnPrice', '$totalReturnPrice', '$orderId', '$date', '$time', '$userId', '', 'Active', 'Pending', '$softwareId')";
    if(mysqli_query($con, $insertOrderReturn))
    {
        echo "Return Sent";
    }
    else
    {
        echo "Return Not Sent";
    }

    $returnId = "SELECT `return_id` FROM `order_return` WHERE `software_lic_id` = '$softwareId' AND `dealer_id` = '$dealerId' AND `entry_date` = '$date' AND `entry_time` = '$time'";
    $returnId = mysqli_query($con, $returnId);
    $returnId = mysqli_fetch_array($returnId)[0];

    for($i=0; $i<sizeof($productArr); $i++)
    {
        $updateBatchStock = "UPDATE `batchwise_stock` SET `quantity` = `quantity` + '$quantArr[$i]' WHERE `software_lic_id` = '$softwareId' AND `prod_id` = '$productArr[$i]' AND `batch_no` = '$batchArr[$i]'";
        mysqli_query($con, $updateBatchStock);

        $insertOrderReturnDetail = "INSERT INTO `order_return_detail`(`return_id`, `prod_id`, `prod_batch`, `prod_quant`, `bonus_quant`, `discount_amount`, `total_amount`, `Unit`, `return_reason`, `software_lic_id`) VALUES ('$returnId', '$productArr[$i]', '$batchArr[$i]', '$quantArr[$i]', '0', '0', '$returnPriceArr[$i]', '$unitArr[$i]', '$returnReasonArr[$i]', '$softwareId')";
        mysqli_query($con, $insertOrderReturnDetail);

        $updateProductStock = "UPDATE `products_stock` SET `in_stock`= `in_stock` + '$quantArr[$i]' WHERE `software_lic_id` = '$softwareId' AND `product_id` = '$productArr[$i]'";
        mysqli_query($con, $updateProductStock);
    }
}

?>
