<?php
/**
 * Created by PhpStorm.
 * User: Saffi Ullah 786
 * Date: 29-Mar-20
 * Time: 12:45 AM
 */
require_once "connection.php";

$mobileCurrLoc = "SELECT `mobile_curr_locations`.*, `salesman_info`.`salesman_name` FROM `mobile_curr_locations` INNER JOIN `user_accounts` ON `user_accounts`.`user_id` = `mobile_curr_locations`.`user_id` INNER JOIN `salesman_info` ON `salesman_info`.`salesman_id` = `mobile_curr_locations`.`user_id` WHERE`user_accounts`.`current_status` = 'on' ORDER BY `user_accounts`.`user_id`";
$mobileCurrLoc = mysqli_query($con, $mobileCurrLoc);
$data = array();
while ($row = mysqli_fetch_object($mobileCurrLoc))
{
    array_push($data, $row);
}
echo json_encode($data);
exit();
?>