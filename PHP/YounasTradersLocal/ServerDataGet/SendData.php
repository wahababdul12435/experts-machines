<?php
/**
 * Created by PhpStorm.
 * User: Saffi Ullah 786
 * Date: 26-Sep-21
 * Time: 1:35 AM
 */
ini_set('display_errors', '1');
error_reporting(E_ALL);
require_once "../connection.php";

$operation = $_POST['operation'];
$serverId = $_POST["server_id"];
$softwareId = $_POST["software_id"];

$serverId = explode(",", $serverId);
$softwareId = explode(",", $softwareId);

$updateStatus = "";

$check = false;
if($operation == "Set new Invoice Status")
{
    for($i=0; $i<sizeof($serverId); $i++)
    {
        if($check)
            $updateStatus = $updateStatus."; ";
        $check = true;
        $updateStatus = $updateStatus."UPDATE `order_info` SET `software_order_id` = '$softwareId[$i]', `send_status_computer` = 'Sent' WHERE `order_id` = '$serverId[$i]'";
    }

    mysqli_multi_query($con, $updateStatus);
}
elseif($operation == "Set new Return Status")
{
    for($i=0; $i<sizeof($serverId); $i++)
    {
        if($check)
            $updateStatus = $updateStatus."; ";
        $check = true;
        $updateStatus = $updateStatus."UPDATE `order_return` SET `software_return_id` = '$softwareId[$i]', `send_status_computer` = 'Sent' WHERE `return_id` = '$serverId[$i]'";
    }

    mysqli_multi_query($con, $updateStatus);
}

?>