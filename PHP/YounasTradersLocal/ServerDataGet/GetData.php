<?php
/**
 * Created by PhpStorm.
 * User: Saffi Ullah 786
 * Date: 20-Sep-21
 * Time: 11:53 PM
 */

require_once "../connection.php";

$data = array();

$reqData = $_GET['required_data'];
$softwareLicId = $_GET['software_lic_id'];

if($reqData == "new_bookings_no")
{
    $sql = "SELECT COUNT(`order_id`) FROM `order_info` WHERE `send_status_computer` = 'Pending' AND `software_lic_id` = '$softwareLicId'";
    $stmt = $con->prepare($sql);
    $stmt->execute();
    $stmt->bind_result($totalOrders);
    while($stmt->fetch()){
        $temp = [
            'new_bookings'=>$totalOrders
        ];
        array_push($data, $temp);
    }
    echo json_encode($data);
}
elseif ($reqData == "new_bookings")
{
    $sql = "SELECT `order_id`, `dealer_id`, `product_id`, `quantity`, `unit`, `order_price`, `bonus`, `discount`, `waive_off_price`, `final_price`, `order_success`, `booking_latitude`, `booking_longitude`, `booking_area`, `booking_date`, `booking_time`, `booking_user_id` FROM `order_info` WHERE `send_status_computer` = 'Pending' AND `software_lic_id` = '$softwareLicId'";
    $stmt = $con->prepare($sql);
    $stmt->execute();
    $stmt->bind_result($orderId, $dealerId, $productId, $quantity, $unit, $orderPrice, $bonus, $discount, $waiveOff, $finalPrice, $orderSuccess, $bookingLatitude, $bookingLongitude, $bookingArea, $bookingDate, $bookingTime, $bookingUserId);
    while($stmt->fetch()){
        $temp = [
            'order_id'=>$orderId,
            'dealer_id'=>$dealerId,
            'product_id'=>$productId,
            'quantity'=>$quantity,
            'unit'=>$unit,
            'order_price'=>$orderPrice,
            'bonus'=>$bonus,
            'discount'=>$discount,
            'waive_off'=>$waiveOff,
            'final_price'=>$finalPrice,
            'order_success'=>$orderSuccess,
            'booking_latitude'=>$bookingLatitude,
            'booking_longitude'=>$bookingLongitude,
            'booking_area'=>$bookingArea,
            'booking_date'=>$bookingDate,
            'booking_time'=>$bookingTime,
            'booking_user_id'=>$bookingUserId
//            'comments'=>($comments == null)? "" : $comments,
//            'status'=>$status
        ];
        array_push($data, $temp);
    }
    echo json_encode($data);
}
elseif ($reqData == "new_bookings_detail")
{
    $sql = "SELECT `order_info_detailed`.`order_id`, `order_info_detailed`.`product_id`, `order_info_detailed`.`batch_id`, `order_info_detailed`.`quantity`, `order_info_detailed`.`unit`, `order_info_detailed`.`order_price`, `order_info_detailed`.`bonus_quant`, `order_info_detailed`.`discount`, `order_info_detailed`.`final_price` FROM `order_info_detailed` INNER JOIN `order_info` ON `order_info`.`order_id` = `order_info_detailed`.`order_id` AND `order_info`.`send_status_computer` = 'Pending' AND `order_info`.`software_lic_id` = '$softwareLicId'";
    $stmt = $con->prepare($sql);
    $stmt->execute();
    $stmt->bind_result($orderId, $productId, $batchId, $quantity, $unit, $orderPrice, $bonus, $discount, $finalPrice);
    while($stmt->fetch()){
        $temp = [
            'order_id'=>$orderId,
            'product_id'=>$productId,
            'batch_id'=>$batchId,
            'quantity'=>$quantity,
            'unit'=>$unit,
            'order_price'=>$orderPrice,
            'bonus'=>$bonus,
            'discount'=>$discount,
            'final_price'=>$finalPrice
//            'comments'=>($comments == null)? "" : $comments,
//            'status'=>$status
        ];
        array_push($data, $temp);
    }
    echo json_encode($data);
}
elseif($reqData == "new_dealer_finance_no")
{
    $sql = "SELECT COUNT(`payment_id`) FROM `dealer_payments` WHERE `send_status_computer` = 'Pending' AND `software_lic_id` = '$softwareLicId'";
    $stmt = $con->prepare($sql);
    $stmt->execute();
    $stmt->bind_result($totalNum);
    while($stmt->fetch()){
        $temp = [
            'new_dealer_finance'=>$totalNum
        ];
        array_push($data, $temp);
    }
    echo json_encode($data);
}
elseif ($reqData == "new_dealer_finance")
{
    $sql = "SELECT `payment_id`, `dealer_id`, `order_id`, `amount`, `cash_type`, `date`, `day`, `user_id`, `status` FROM `dealer_payments` WHERE `send_status_computer` = 'Pending' AND `software_lic_id` = '$softwareLicId'";
    $stmt = $con->prepare($sql);
    $stmt->execute();
    $stmt->bind_result($paymentId, $dealerId, $orderId, $amount, $cashType, $date, $day, $userId, $status);
    while($stmt->fetch()){
        $temp = [
            'payment_id'=>$paymentId,
            'dealer_id'=>$dealerId,
            'order_id'=>$orderId,
            'amount'=>$amount,
            'cash_type'=>$cashType,
            'date'=>$date,
            'day'=>$day,
            'user_id'=>$userId,
            'status'=>$status
        ];
        array_push($data, $temp);
    }
    echo json_encode($data);
}
elseif($reqData == "new_returns_no")
{
    $sql = "SELECT COUNT(`return_id`) FROM `order_return` WHERE `send_status_computer` = 'Pending' AND `software_lic_id` = '$softwareLicId'";
    $stmt = $con->prepare($sql);
    $stmt->execute();
    $stmt->bind_result($totalReturns);
    while($stmt->fetch()){
        $temp = [
            'new_returns_no'=>$totalReturns
        ];
        array_push($data, $temp);
    }
    echo json_encode($data);
}
elseif ($reqData == "new_returns")
{
    $sql = "SELECT `return_id`, `dealer_id`, `return_total_price`, `return_gross_price`, `order_id`, `entry_date`, `entry_time`, `enter_user_id` FROM `order_return` WHERE `send_status_computer` = 'Pending' AND `software_lic_id` = '$softwareLicId'";
    $stmt = $con->prepare($sql);
    $stmt->execute();
    $stmt->bind_result($returnId, $dealerId, $returnTotalPrice, $returnGrossPrice, $orderId, $entryDate, $entryTime, $enterUserId);
    while($stmt->fetch()){
        $temp = [
            'return_id'=>$returnId,
            'dealer_id'=>$dealerId,
            'return_total_price'=>$returnTotalPrice,
            'return_gross_price'=>$returnGrossPrice,
            'order_id'=>$orderId,
            'entry_date'=>$entryDate,
            'entry_time'=>$entryTime,
            'enter_user_id'=>$enterUserId
//            'comments'=>($comments == null)? "" : $comments,
//            'status'=>$status
        ];
        array_push($data, $temp);
    }
    echo json_encode($data);
}
elseif ($reqData == "new_returns_detail")
{
    $sql = "SELECT `order_return_detail`.`return_id`, `order_return_detail`.`prod_id`, `order_return_detail`.`prod_batch`, `order_return_detail`.`prod_quant`, `order_return_detail`.`bonus_quant`, `order_return_detail`.`discount_amount`, `order_return_detail`.`total_amount`, `order_return_detail`.`unit`, `order_return_detail`.`return_reason` FROM `order_return_detail` INNER JOIN `order_return` ON `order_return`.`return_id` = `order_return_detail`.`return_id` AND `order_return`.`send_status_computer` = 'Pending' AND `order_return`.`software_lic_id` = '$softwareLicId'";
    $stmt = $con->prepare($sql);
    $stmt->execute();
    $stmt->bind_result($returnId, $productId, $batchNo, $quantity, $bonusQty, $discount, $totalAmount, $unit, $returnReason);
    while($stmt->fetch()){
        $temp = [
            'return_id'=>$returnId,
            'product_id'=>$productId,
            'batch_no'=>$batchNo,
            'quantity'=>$quantity,
            'bonus_qty'=>$bonusQty,
            'discount'=>$discount,
            'total_amount'=>$totalAmount,
            'unit'=>$unit,
            'return_reason'=>$returnReason
//            'comments'=>($comments == null)? "" : $comments,
//            'status'=>$status
        ];
        array_push($data, $temp);
    }
    echo json_encode($data);
}
?>