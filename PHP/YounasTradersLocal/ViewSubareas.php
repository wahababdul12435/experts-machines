<?php
/**
 * Created by PhpStorm.
 * User: Saffi Ullah 786
 * Date: 28-Oct-19
 * Time: 12:30 AM
 */

require_once "connection.php";
$areaInfo = "SELECT * FROM `area_info`";
$areaInfo = mysqli_query($con, $areaInfo);
$x=0;
while($areaData = mysqli_fetch_array($areaInfo))
{
    $areaIDMain[$x] = $areaData[0];
    $areaNameMain[$x] = $areaData[1];
    $x++;
}


$subareasInfo = "SELECT * FROM `subarea_info`";
$subareasInfo = mysqli_query($con, $subareasInfo);
$i=0;
while ($data = mysqli_fetch_array($subareasInfo))
{
    $subareaID[$i] = $data[0];
    $areaID[$i] = $data[1];
    $areaIndex = array_search($areaID[$i], $areaIDMain);
    $areaName[$i] = $areaNameMain[$areaIndex];
    $subAreaName[$i] = $data[2];

    $salesmanAllocated = "SELECT COUNT(`salesman_id`) FROM `salesman_designated_areas` WHERE `designated_area_id` = '$subareaID[$i]'";
    $salesmanAllocated = mysqli_query($con, $salesmanAllocated);
    $salesmanAllocatedCount[$i] = mysqli_fetch_array($salesmanAllocated)[0];

    $getDealerCount = "SELECT COUNT(`dealer_id`) FROM `dealer_info` WHERE `dealer_area_id` = '$subareaID[$i]'";
    $getDealerCount = mysqli_query($con, $getDealerCount);
    $dealersCount[$i] = mysqli_fetch_array($getDealerCount)[0];

    $subAreaStatus[$i] = $data[3];
    $i++;
}
?>

<html>
<head>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="css/font-awesome.min.css">
    <link type="text/css" rel="stylesheet" href="css/style.css">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>

    <style>
        .operations:hover
        {
            color: #D10024;
            cursor: pointer;
        }
        th
        {
            font-size: 15px;
        }
        td
        {
            font-size: 13px;
        }
    </style>
    <script>
        var subareaID = 0;
    </script>
</head>
<body>

<?php
require_once "PopupModel.php";
?>
<div class="container">
    <div style="margin-top: 20px">
        <table id="SuppliersData" class="display">
            <thead>
            <div>
                <tr>
                    <th style="text-align: center; width: 2%">Sub Area ID</th>
                    <th style="text-align: center; width: 2%">Area Name</th>
                    <th style="text-align: center; width: 2%">Subarea Name</th>
                    <th style="text-align: center; width: 2%">Salesman Allocated</th>
                    <th style="text-align: center; width: 2%">Dealers Present</th>
                    <th style="text-align: center; width: 2%">Status</th>
                    <th style="text-align: center; width: 2%">Operations</th>
                </tr>
            </div>
            </thead>
            <tbody>
            <?php
            for($j=0; $j<$i; $j++)
            {
                ?>
                <tr>
                    <td style="text-align: center"><?php echo $subareaID[$j]; ?></td>
                    <td style="text-align: center"><?php echo $areaName[$j]; ?></td>
                    <td style="text-align: center"><?php echo $subAreaName[$j]; ?></td>
                    <td style="text-align: center"><?php echo $salesmanAllocatedCount[$j]; ?></td>
                    <td style="text-align: center"><?php echo $dealersCount[$j]; ?></td>
                    <td style="text-align: center"><?php echo $subAreaStatus[$j]; ?></td>
                    <td style="text-align: center">
                        <div style="margin-top: 10px">
                            <a data-toggle="modal" data-target="#myModalDel" onclick="delSubarea('<?php echo $subareaID[$j]; ?>')">
                                <i class="fa-lg fa fa-trash operations" title="Delete"></i>&nbsp;&nbsp;
                            </a>
                            <a data-toggle="modal" data-target="#myModalEditSubarea" onclick="editData('<?php echo $subareaID[$j]; ?>', '<?php echo $areaName[$j]; ?>', '<?php echo $subAreaName[$j]; ?>', '<?php echo $salesmanAllocatedCount[$j]; ?>', '<?php echo $dealersCount[$j]; ?>', '<?php echo $subAreaStatus[$j]; ?>')">
                                <i class="fa-lg fa fa-edit operations" title="Edit"></i>
                            </a>
                        </div>
                    </td>
                </tr>

                <?php
            }
            ?>

            </tbody>
        </table>
    </div>
</div>

<script src="js/bootstrap.min.js"></script>
<script src="js/slick.min.js"></script>
<script src="js/nouislider.min.js"></script>
<script src="js/jquery.zoom.min.js"></script>
<script src="js/main.js"></script>

<script>
    $(document).ready(function() {
        $('#SuppliersData').DataTable(
            {
                // "Item No": [[ 0, "desc" ]]
            }
        );
    });

    function delSubarea(givenID) {
        subareaID = givenID;
    }

    function delConfirmation(op) {
        if(op == 'yes')
        {
            window.location.href = 'SendData.php?table=subarea_info&op=del&id='+subareaID;
        }
        else
        {
            return;
        }
    }

    function editData(id, areaname, subareaname, subareaSalesman, subareaDealers, status) {
        document.getElementById('subarea_id').value = id;
        document.getElementById('subarea_areaname').value = areaname;
        document.getElementById('subarea_subareaname').value = subareaname;
        document.getElementById('subarea_salesman').value = subareaSalesman;
        document.getElementById('subarea_dealers').value = subareaDealers;
        document.getElementById('subarea_status').value = status;
    }

    function updateData(op)
    {
        if(op == 'yes')
        {
            subarea_id = document.getElementById('subarea_id').value;
            subarea_subareaname = document.getElementById('subarea_subareaname').value;
            subarea_status = document.getElementById('subarea_status').value;

            window.location.href = 'SendData.php?table=subarea_info&op=update&id='+subarea_id+'&subarea_subareaname='+subarea_subareaname+'&subarea_status='+subarea_status;
        }
        else
        {

        }
    }
</script>
</body>
</html>