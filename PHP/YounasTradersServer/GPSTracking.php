<?php
/**
 * Created by PhpStorm.
 * User: Saffi Ullah 786
 * Date: 26-Mar-20
 * Time: 11:40 PM
 */
require_once "connection.php";
$softwareId = $_GET['softwareid'];
$date = $_GET['date'];

$totalUsersCount = 0;
$gotCurrUser = false;

$mobileCurrLoc = "SELECT `mobile_curr_locations`.*, `user_info`.`user_name`, `user_accounts`.`current_status` FROM `mobile_curr_locations` INNER JOIN `user_accounts` ON `user_accounts`.`user_id` = `mobile_curr_locations`.`software_user_id` INNER JOIN `user_info` ON `user_info`.`user_table_id` = `mobile_curr_locations`.`software_user_id` WHERE `mobile_curr_locations`.`update_date` = '$date' AND `mobile_curr_locations`.`software_lic_id` = '$softwareId' GROUP BY `mobile_curr_locations`.`software_user_id` ORDER BY `user_accounts`.`user_id`";
//echo "Mobile Curr: ".$mobileCurrLoc.'<br>';
$mobileCurrLoc = mysqli_query($con, $mobileCurrLoc);
$currActive=0;
$currUserId = [];
while($currData = mysqli_fetch_array($mobileCurrLoc))
{
    $currUserId[$currActive] = $currData[1];
    $currLat[$currActive] = $currData[2];
    $currLng[$currActive] = $currData[3];
    $currLocName[$currActive] = $currData[4];
    $currUserName[$currActive] = $currData[8];
    $currUserStatus[$currActive] = $currData[9];
    $currActive++;
    $totalUsersCount++;
    $gotCurrUser = true;
}

$timeStampLoc = "SELECT `mobile_gps_location`.*, `user_info`.`user_name`, `user_accounts`.`current_status` FROM `mobile_gps_location` INNER JOIN `user_accounts` ON `user_accounts`.`user_id` = `mobile_gps_location`.`software_user_id` INNER JOIN `user_info` ON `user_info`.`user_table_id` = `mobile_gps_location`.`software_user_id` WHERE `mobile_gps_location`.`date` = '$date' AND `mobile_gps_location`.`software_lic_id` = '$softwareId' ORDER BY `user_accounts`.`user_id`";
//echo "Time Stamp: ".$timeStampLoc.'<br>';
$timeStampLoc = mysqli_query($con, $timeStampLoc);
$timeStampCount=0;
$preTimeStampUserId = "";
$firstIter = true;
while($timeStampData = mysqli_fetch_array($timeStampLoc))
{
    $timeStampUserId[$timeStampCount] = $timeStampData[1];
    $timeStampLat[$timeStampCount] = $timeStampData[2];
    $timeStampLng[$timeStampCount] = $timeStampData[3];
    $timeStampLocName[$timeStampCount] = $timeStampData[4];
    $timeStampTime[$timeStampCount] = $timeStampData[6];
    $timeStampUserName[$timeStampCount] = $timeStampData[8];
    $timeStampUserStatus[$timeStampCount] = $timeStampData[9];

    if($firstIter)
    {
        $preTimeStampUserId = $timeStampUserId[$timeStampCount];
        $firstIter = false;
    }

    if($preTimeStampUserId != $timeStampUserId[$timeStampCount])
    {
        if(!in_array($preTimeStampUserId, $currUserId))
        {
            $currUserId[$currActive] = $timeStampUserId[$timeStampCount-1];
            $currLat[$currActive] = $timeStampLat[$timeStampCount-1];
            $currLng[$currActive] = $timeStampLng[$timeStampCount-1];
            $currLocName[$currActive] = $timeStampLocName[$timeStampCount-1];
            $currUserName[$currActive] = $timeStampUserName[$timeStampCount-1];
            $currUserStatus[$currActive] = $timeStampUserStatus[$timeStampCount-1];
            $currActive++;
            $totalUsersCount++;
        }
        $preTimeStampUserId = $timeStampUserId[$timeStampCount];
    }

//    if($currActive == 0)
//    {
//        if($preTimeStampUserId != $timeStampUserId[$timeStampCount])
//        {
//            $preTimeStampUserId = $timeStampUserId[$timeStampCount];
//
//            $currUserId[$currActive] = $timeStampUserId[$timeStampCount-1];
//            $currLat[$currActive] = $timeStampLat[$timeStampCount-1];
//            $currLng[$currActive] = $timeStampLng[$timeStampCount-1];
//            $currLocName[$currActive] = $timeStampLocName[$timeStampCount-1];
//            $currUserName[$currActive] = $timeStampUserName[$timeStampCount-1];
//            $currUserStatus[$currActive] = $timeStampUserStatus[$timeStampCount-1];
//            $currActive++;
//            $totalUsersCount++;
//        }
//    }
//    else
//    {
//        if($preTimeStampUserId != $timeStampUserId[$timeStampCount])
//        {
//            if(!in_array($preTimeStampUserId, $currUserId))
//            {
//                $currUserId[$currActive] = $timeStampUserId[$timeStampCount-1];
//                $currLat[$currActive] = $timeStampLat[$timeStampCount-1];
//                $currLng[$currActive] = $timeStampLng[$timeStampCount-1];
//                $currLocName[$currActive] = $timeStampLocName[$timeStampCount-1];
//                $currUserName[$currActive] = $timeStampUserName[$timeStampCount-1];
//                $currUserStatus[$currActive] = $timeStampUserStatus[$timeStampCount-1];
//                $currActive++;
//                $totalUsersCount++;
//            }
//            $preTimeStampUserId = $timeStampUserId[$timeStampCount];
//        }
//    }
    $timeStampCount++;
}

if($timeStampCount > 0)
{
    if($currUserId == null)
    {
        $currUserId[$currActive] = $timeStampUserId[$timeStampCount-1];
        $currLat[$currActive] = $timeStampLat[$timeStampCount-1];
        $currLng[$currActive] = $timeStampLng[$timeStampCount-1];
        $currLocName[$currActive] = $timeStampLocName[$timeStampCount-1];
        $currUserName[$currActive] = $timeStampUserName[$timeStampCount-1];
        $currUserStatus[$currActive] = $timeStampUserStatus[$timeStampCount-1];
        $currActive++;
        $totalUsersCount++;
    }
    elseif(!in_array($preTimeStampUserId, $currUserId))
    {
        $currUserId[$currActive] = $timeStampUserId[$timeStampCount-1];
        $currLat[$currActive] = $timeStampLat[$timeStampCount-1];
        $currLng[$currActive] = $timeStampLng[$timeStampCount-1];
        $currLocName[$currActive] = $timeStampLocName[$timeStampCount-1];
        $currUserName[$currActive] = $timeStampUserName[$timeStampCount-1];
        $currUserStatus[$currActive] = $timeStampUserStatus[$timeStampCount-1];
        $currActive++;
        $totalUsersCount++;
    }
}

$orderLoc = "SELECT `order_gps_location`.*, `dealer_info`.`dealer_name` FROM `order_gps_location` INNER JOIN `user_accounts` ON `user_accounts`.`user_id` = `order_gps_location`.`user_id` INNER JOIN `dealer_info` ON `dealer_info`.`dealer_table_id` = `order_gps_location`.`dealer_id` WHERE `user_accounts`.`current_status` = 'on' AND `order_gps_location`.`software_lic_id` = '$softwareId' ORDER BY `user_accounts`.`user_id`";
//echo "Order Loc: ".$orderLoc.'<br>';
$orderLoc = mysqli_query($con, $orderLoc);
$orderLocCount=0;
while($orderLocData = mysqli_fetch_array($orderLoc))
{
    $orderLocUserId[$orderLocCount] = $orderLocData[3];
    $orderLocLat[$orderLocCount] = $orderLocData[4];
    $orderLocLng[$orderLocCount] = $orderLocData[5];
    $orderLocName[$orderLocCount] = $orderLocData[6];
    $orderLocTime[$orderLocCount] = $orderLocData[8];
    $orderLocPrice[$orderLocCount] = $orderLocData[10];
    $orderLocType[$orderLocCount] = $orderLocData[11];
    $orderLocDealerName[$orderLocCount] = $orderLocData[13];
    $orderLocCount++;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>My Google Map</title>
    <style>
        #map{
            height:600px;
            width:100%;
        }
    </style>
</head>
<body>
<div id="map"></div>
<script>
    var map;
    var markers = [];
    var infoMarkers = [];
    var infoMarkerCount = infoMarkers.length;
    var markersCurrLocations = [[]];
    var markersNewLocations = [[]];
    var timeStampMarkers = [[]];
    var temp = [];
    var orderLocMarkers = [[]];
    var markerToMove = [];
    var positionOfMarker = [[]];
    var result = [[]];
    var activeInfoWindow;
    var softwareId = '<?php echo $softwareId; ?>';
    var date = '<?php echo $date; ?>';
    var gotCurrUser = '<?php echo $gotCurrUser; ?>';
    function initMap(){
        // Map options
        var options = {
            zoom:12,
            center:{lat:32.551158,lng:74.070952}
        }

        // New map
        map = new google.maps.Map(document.getElementById('map'), options);

        // Listen for click on map
        google.maps.event.addListener(map, 'click', function(event){
            // Add marker
            // addMarker({coords:event.latLng});
        });

        iconCurrLoc = {
            url: "imgs/gps markers/curr_loc_1.png", // url
            scaledSize: new google.maps.Size(30, 30) // scaled size
        };
        iconDealerLoc = {
            url: "imgs/gps markers/dealer_loc_1.png", // url
            scaledSize: new google.maps.Size(30, 30) // scaled size
        };

        iconBooking = {
            url: "imgs/gps markers/booking_2.png", // url
            scaledSize: new google.maps.Size(20, 20) // scaled size
        };

        iconCashCollection = {
            url: "imgs/gps markers/cash_collection_1.png", // url
            scaledSize: new google.maps.Size(20, 20) // scaled size
        };

        iconDelivered = {
            url: "imgs/gps markers/delivered_2.png", // url
            scaledSize: new google.maps.Size(15, 15) // scaled size
        };

        iconTimeStamp = {
            url: "imgs/gps markers/time_stamp_2.png", // url
            scaledSize: new google.maps.Size(15, 15) // scaled size
        };

        // Array of markers
        // var markers = [
        //     {
        //         coords:{lat:32.551158,lng:74.070952},
        //         iconImage:icon,
        //         content:'<h1>Lynn MA</h1>'
        //     },
        //     {
        //         coords:{lat:42.8584,lng:-70.9300},
        //         content:'<h1>Amesbury MA</h1>'
        //     }
        // ];
        // markers[2] = {coords:{lat:42.7762,lng:-71.0773}};

        // Array of Current Location Markers
        currActiveMarkers = [];
        <?php
        for($curri=0; $curri<$currActive; $curri++)
        {
        ?>
        temp = [];
        // tempNew = [];
        currActiveMarkers[<?php echo $curri; ?>] = {coords:{lat:<?php echo $currLat[$curri]; ?>,lng:<?php echo $currLng[$curri]; ?>}, iconImage:iconCurrLoc, content:'<p><?php echo $currUserName[$curri]; ?></p><p><?php echo $currLocName[$curri]; ?></p><p><?php echo $currUserStatus[$curri]; ?></p>'};
        temp[0] = '<?php echo $currUserId[$curri]?>';
        //tempNew[0] = '<?php //echo $currUserId[$curri]?>//';
        temp[1] = <?php echo $currLat[$curri]?>;
        //tempNew[1] = <?php //echo $currLat[$curri]?>//;
        temp[2] = <?php echo $currLng[$curri]?>;
        //tempNew[2] = <?php //echo $currLng[$curri]?>//;
        temp[3] = '<?php echo $currLocName[$curri]?>';
        //tempNew[3] = '<?php //echo $currLocName[$curri]?>//';
        markersCurrLocations[<?php echo $curri; ?>] = temp;
        //markersNewLocations[<?php //echo $curri; ?>//] = tempNew;
        //window.alert(markersCurrLocations[<?php //echo $curri; ?>//][0]+" "+markersCurrLocations[<?php //echo $curri; ?>//][1]+" "+markersCurrLocations[<?php //echo $curri; ?>//][2]+" "+markersCurrLocations[<?php //echo $curri; ?>//][3]);
        <?php
        }
        ?>
        markersNewLocations = markersCurrLocations.map(function(arr) {
            return arr.slice();
        });

        temp = [];
        // Array of Time Stamp Location Markers
        <?php
        $curri = 0;
        $stampsCount = 0;
        for($timei = 0; $timei<$timeStampCount; $timei++)
        {
        $preUserId = $currUserId[$curri];
        $newUserId = $timeStampUserId[$timei];
        if($preUserId == $newUserId)
        {
        ?>
        temp[<?php echo $stampsCount; ?>] = {
            coords: {
                lat:<?php echo $timeStampLat[$timei]; ?>,
                lng:<?php echo $timeStampLng[$timei]; ?>},
            iconImage: iconTimeStamp,
            content: '<p><?php echo $timeStampTime[$timei]; ?></p><p><?php echo $timeStampLocName[$timei]; ?>}</p>'
        };
        <?php
        $stampsCount++;
        }
        else
        {
        ?>
        timeStampMarkers[<?php echo $curri; ?>] = temp;
        temp = [];
        <?php
        if($curri < $currActive)
        {
            $curri++;
            $stampsCount = 0;
        }
        ?>
        temp[<?php echo $stampsCount; ?>] = {
            coords: {
                lat:<?php echo $timeStampLat[$timei]; ?>,
                lng:<?php echo $timeStampLng[$timei]; ?>},
            iconImage: iconTimeStamp,
            content: '<p><?php echo $timeStampTime[$timei]; ?></p><p><?php echo $timeStampLocName[$timei]; ?>}</p>'
        };
        <?php
        $stampsCount++;
        }
        }
        if($currActive > 0)
        {
        ?>
        timeStampMarkers[<?php echo $curri; ?>] = temp;
        <?php
        }
        else
        {
            ?>
        timeStampMarkers[<?php echo $curri; ?>] = temp;
        <?php
        }
        ?>

        // Array of Order Location Markers

        temp = [];
        <?php
        $curri = 0;
        $orderCount = 0;
        for($orderi = 0; $orderi<$orderLocCount; $orderi++)
        {
        $preUserId = $currUserId[$curri];
        $newUserId = $orderLocUserId[$orderi];
        if($preUserId == $newUserId)
        {
        ?>
        temp[<?php echo $orderCount; ?>] = {
            coords: {
                lat:<?php echo $orderLocLat[$orderi]; ?>,
                lng:<?php echo $orderLocLng[$orderi]; ?>},
            <?php
            if($orderLocType[$orderi] == "Booking")
            {
            ?>
            iconImage: iconBooking,
            <?php
            }
            elseif ($orderLocType[$orderi] == "Collection")
            {
            ?>
            iconImage: iconCashCollection,
            <?php
            }
            else
            {
            ?>
            iconImage: iconDelivered,
            <?php
            }
            ?>
            content: '<p><?php echo $orderLocTime[$orderi]; ?></p><p><?php echo $orderLocName[$orderi]; ?></p><p><?php echo $orderLocDealerName[$orderi]; ?></p>'
        };
        <?php
        $orderCount++;
        }
        else
        {
        ?>
        orderLocMarkers[<?php echo $curri; ?>] = temp;
        temp = [];
        <?php
        if($curri < $currActive)
        {
            $curri++;
            $orderCount = 0;
        }
        ?>
        temp[<?php echo $orderCount; ?>] = {
            coords: {
                lat:<?php echo $orderLocLat[$orderi]; ?>,
                lng:<?php echo $orderLocLng[$orderi]; ?>},
            <?php
            if($orderLocType[$orderi] == "Booking")
            {
            ?>
            iconImage: iconBooking,
            <?php
            }
            elseif ($orderLocType[$orderi] == "Collection")
            {
            ?>
            iconImage: iconCashCollection,
            <?php
            }
            else
            {
            ?>
            iconImage: iconDelivered,
            <?php
            }
            ?>
            content: '<p><?php echo $orderLocTime[$orderi]; ?></p><p><?php echo $orderLocName[$orderi]; ?></p><p><?php echo $orderLocDealerName[$orderi]; ?></p>'
        };
        <?php
        $orderCount++;
        }
        }
        if($currActive > 0)
        {
        ?>
        orderLocMarkers[<?php echo $curri; ?>] = temp;
        <?php
        }
        ?>


        // Loop through Current markers
        for(var i = 0; i < currActiveMarkers.length; i++){
            addMarker(currActiveMarkers[i], i);
        }

        startMoving = false;

        <?php
//        for($markerI=0; $markerI<$currActive; $markerI++)
//        {
        ?>
        //temp =[];
        //destTemp = [];
        //markerToMove[<?php //echo $markerI; ?>//] = markers[<?php //echo $markerI; ?>//];
        //temp[0] = <?php //echo $currLat[$markerI]; ?>//;
        //temp[1] = <?php //echo $currLng[$markerI]; ?>//;
        //destTemp[0] = 32.651158;
        //destTemp[1] = 74.070952;
        //positionOfMarker[<?php //echo $markerI; ?>//] = temp;
        //result[<?php //echo $markerI; ?>//] = destTemp;
        <?php
//        }
        ?>

        // window.alert(positionOfMarker.length);
        // result = [32.651158, 74.070952];

        if(gotCurrUser)
        {
            doSetTimeout();
        }
    }

    function addMarker(props, x, type){
        var marker = new google.maps.Marker({
            position:props.coords,
            map:map
            //icon:props.iconImage
        });

        // Check for customicon
        if(props.iconImage){
            // Set icon image
            marker.setIcon(props.iconImage);
        }

        // Check content
        if(props.content){
            var infoWindow = new google.maps.InfoWindow({
                content:props.content
            });

            marker.addListener('click', function(){
                if (activeInfoWindow) { activeInfoWindow.close();}
                infoWindow.open(map, marker);
                activeInfoWindow = infoWindow;
                if(x != -1)
                {
                    for(infoI=0; infoI<infoMarkers.length; infoI++)
                    {
                        delMarker(infoMarkers[infoI]);
                        infoMarkerCount = infoMarkers.length;
                    }
                    infoMarkers = [];
                    infoMarkerCount = infoMarkers.length;
                    for(timeI=0; timeI<timeStampMarkers[x].length; timeI++)
                    {
                        addMarker(timeStampMarkers[x][timeI], -1, "Info");
                    }

                    for(orderI=0; orderI<orderLocMarkers[x].length; orderI++)
                    {
                        addMarker(orderLocMarkers[x][orderI], -1, "Info");
                    }
                }

            });
        }
        if(x != null && type == null)
        {
            // markers.push(marker);
            markers[x] = marker;
        }
        else
        {
            infoMarkers[infoMarkerCount] = marker;
            infoMarkerCount = infoMarkers.length;
        }
    }

    // Sets the map on all markers in the array.
    function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    }

    // Removes the markers from the map, but keeps them in the array.
    function clearMarkers() {
        setMapOnAll(null);
    }

    // Shows any markers currently in the array.
    function showMarkers() {
        setMapOnAll(map);
    }

    function delMarker(markerId, infoX) {
        markerId.setMap(null);
    }

    var numDeltas = 100;
    var delay = 100; //milliseconds
    var i = 0;
    var deltaLat = [];
    var deltaLng = [];

    function doSetTimeout() {
        for(numX=0; numX<markersCurrLocations.length; numX++)
        {
            temp =[];
            destTemp = [];
            markerToMove[numX] = markers[numX];
            // temp[0] = markersCurrLocations[numX][1];
            // temp[1] = markersCurrLocations[numX][2];
            // window.alert(markersNewLocations[numX][1]+"  Before  "+markersCurrLocations[numX][1]);
            // markersNewLocations[numX][1] = 32.651158;
            // markersNewLocations[numX][2] = 74.070952;
            // window.alert(markersNewLocations[numX][1]+"  After  "+markersCurrLocations[numX][1]);
            // positionOfMarker[numX] = temp;
        }
        // setTimeout(function() { transition() }, delay);
        transition();
    }

    function transition(){
        i = 0;
        deltaLat = [];
        deltaLng = [];
        for(x=0; x<markersCurrLocations.length; x++)
        {
            // window.alert(result[x][0]+" ---- "+result[x][1]);
            // deltaLat[x] = (result[x][0] - positionOfMarker[x][0])/numDeltas;
            // deltaLng[x] = (result[x][1] - positionOfMarker[x][1])/numDeltas;

            deltaLat[x] = (markersNewLocations[x][1] - markersCurrLocations[x][1])/numDeltas;
            deltaLng[x] = (markersNewLocations[x][2] - markersCurrLocations[x][2])/numDeltas;
        }
        moveMarker();
    }

    function moveMarker(){

        for(x=0; x<markersCurrLocations.length; x++)
        {
            // window.alert(markersCurrLocations[x][1]+" ---- "+markersCurrLocations[x][2]);
            markersCurrLocations[x][1] += deltaLat[x];
            markersCurrLocations[x][2] += deltaLng[x];
            latlng = [];
            latlng[x] = new google.maps.LatLng(markersCurrLocations[x][1], markersCurrLocations[x][2]);
            markerToMove[x].setTitle("Latitude:"+markersCurrLocations[x][1]+" | Longitude:"+markersCurrLocations[x][2]);
            markerToMove[x].setPosition(latlng[x]);
        }
        if(i!=numDeltas){
            i++;
            setTimeout(moveMarker, delay);
        }
        else
        {
            // window.alert("Yes");
            // markersCurrLocations = markersNewLocations.map(function(arr) {
            //     return arr.slice();
            // });
            refreshCurrLocations();
        }
    }

    function refreshCurrLocations() {
        markersNewLocations = [[]];
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp=new XMLHttpRequest();
        } else { // code for IE6, IE5
            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange=function() {
            if (this.readyState==4 && this.status==200) {
                // document.getElementById("txtHint").innerHTML=this.responseText;
                var data = JSON.parse(this.responseText);
                for(var refI = 0; refI < data.length; refI++) {
                    currUserId = data[refI].user_id;
                    currLatitude = data[refI].latitude;
                    currLongitude = data[refI].longitude;
                    currLocationName = '<p>'+data[refI].location_name+'</p>';
                    currUserName = '<p>'+data[refI].user_name+'</p>';
                    currUserSatus = '<p>'+data[refI].current_status+'</p>';
                    currContent = currUserName+currLocationName+currUserSatus;
                    temp = [];
                    temp[0] = currUserId;
                    temp[1] = currLatitude;
                    temp[2] = currLongitude;
                    temp[3] = currContent;
                    markersNewLocations[refI] = temp;
                    // window.alert(markersNewLocations[refI][1]+" ___---___ "+markersNewLocations[refI][2]);
                }
                doSetTimeout();
            }
        };
        xmlhttp.open("POST","GPSCurrLocRefresh.php?softwareid="+softwareId+"&date="+date,true);
        xmlhttp.send();
    }
</script>
<script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDml7laRW0mAK7sJpVAKCRKpW8bePIuKT4&callback=initMap" >
</script>
</body>
</html>

