<?php
/**
 * Created by PhpStorm.
 * User: Saffi Ullah 786
 * Date: 26-Sep-21
 * Time: 1:35 AM
 */
//ini_set('display_errors', '1');
//error_reporting(E_ALL);
require_once "../connection.php";

if(isset($_POST['operation']))
{
    $operation = $_POST['operation'];
}
else
{
    $operation = "";
}
if(isset($_POST['server_id']))
{
    $serverId = $_POST["server_id"];
}
else
{
    $serverId = "";
}
if(isset($_POST['software_id']))
{
    $softwareId = $_POST["software_id"];
}
else
{
    $softwareId = "0";
}

$updateStatus = "";

$check = false;
if($operation == "Send License Activation")
{
    $licenseNumber = $_POST['license_number'];
    $getLicNum = "SELECT `license_number_id` FROM `license_numbers` WHERE `license_number` = '$licenseNumber' AND `license_status` = 'Available'";

    $getLicNum = mysqli_query($con, $getLicNum);
    $getLicNum = mysqli_fetch_array($getLicNum)[0];
    if($getLicNum != "")
    {
        $updateQuery = "UPDATE `license_numbers` SET `license_status` = 'Activated' WHERE `license_number` = '$licenseNumber' AND `license_status` = 'Available'";

        if(mysqli_query($con, $updateQuery))
        {
            echo $getLicNum;
        }
        else
        {
            echo "0";
        }
    }
    else
    {
        echo "-1";
    }

}
elseif($operation == "Send Owner Business")
{
    $ownerName = $_POST['owner_name'];
    $ownerContact = $_POST['owner_contact'];
    $ownerAddress = $_POST['owner_address'];
    $ownerCNIC = $_POST['owner_CNIC'];
    $businessName = $_POST['business_name'];
    $businessContact = $_POST['business_contact'];
    $businessAddress = $_POST['business_address'];
    $businessNTN = $_POST['business_NTN'];
    $businessCategory = $_POST['business_category'];
    $workingDistrict = $_POST['working_district'];

//    $ownerName = "O Na"; //$_POST['owner_name'];
//    $ownerContact = "O Con"; //$_POST['owner_contact'];
//    $ownerAddress = "O Add"; //$_POST['owner_address'];
//    $ownerCNIC = "O CN"; //$_POST['owner_CNIC'];
//    $businessName = "B Na"; //$_POST['business_name'];
//    $businessContact = "B Con"; //$_POST['business_contact'];
//    $businessAddress = "B Add"; //$_POST['business_address'];
//    $businessNTN = "B NT"; //$_POST['business_NTN'];
//    $businessCategory = "B Cat"; //$_POST['business_category'];
//    $workingDistrict = "Wor Dis"; //$_POST['working_district'];


    $insertQuery = "INSERT INTO `owner_business_info` (`owner_name`, `owner_contact`, `owner_address`, `owner_cnic`, `business_name`, `business_contact`, `business_address`, `business_ntn`, `business_category`, `working_district`, `owner_status`, `software_lic_id`) VALUES ('$ownerName', '$ownerContact', '$ownerAddress', '$ownerCNIC', '$businessName', '$businessContact', '$businessAddress', '$businessNTN', '$businessCategory', '$workingDistrict', 'Active', '$softwareId')";
//    echo $insertQuery;
    if(mysqli_query($con, $insertQuery))
    {
        echo "True";
    }
    else
    {
        echo "False";
    }
}
elseif($operation == "Set new Invoice Status")
{
    $serverId = explode(",", $serverId);
    $softwareId = explode(",", $softwareId);

    for($i=0; $i<sizeof($serverId); $i++)
    {
        if($check)
            $updateStatus = $updateStatus."; ";
        $check = true;
        $updateStatus = $updateStatus."UPDATE `order_info` SET `software_order_id` = '$softwareId[$i]', `send_status_computer` = 'Sent' WHERE `order_id` = '$serverId[$i]'";
    }

    mysqli_multi_query($con, $updateStatus);
}
elseif($operation == "Set new Return Status")
{
    $serverId = explode(",", $serverId);
    $softwareId = explode(",", $softwareId);

    for($i=0; $i<sizeof($serverId); $i++)
    {
        if($check)
            $updateStatus = $updateStatus."; ";
        $check = true;
        $updateStatus = $updateStatus."UPDATE `order_return` SET `software_return_id` = '$softwareId[$i]', `send_status_computer` = 'Sent' WHERE `return_id` = '$serverId[$i]'";
    }

    mysqli_multi_query($con, $updateStatus);
}
elseif($operation == "Set new User Accounts Status")
{
    $serverId = explode(",", $serverId);
    $softwareId = explode(",", $softwareId);

    for($i=0; $i<sizeof($serverId); $i++)
    {
        if($check)
            $updateStatus = $updateStatus."; ";
        $check = true;
        $updateStatus = $updateStatus."UPDATE `user_accounts` SET `software_account_id` = '$softwareId[$i]', `send_status_computer` = 'Sent' WHERE `account_id` = '$serverId[$i]'";
    }

    mysqli_multi_query($con, $updateStatus);
}
elseif($operation == "Set new Dealer Finance")
{
    $serverId = explode(",", $serverId);
    $softwareId = explode(",", $softwareId);

    for($i=0; $i<sizeof($serverId); $i++)
    {
        if($check)
            $updateStatus = $updateStatus."; ";
        $check = true;
        $updateStatus = $updateStatus."UPDATE `dealer_payments` SET `software_payment_id` = '$softwareId[$i]', `send_status_computer` = 'Sent' WHERE `payment_id` = '$serverId[$i]'";
    }

    mysqli_multi_query($con, $updateStatus);
}
elseif($operation == "Set mobile gps Status")
{
    $serverId = explode(",", $serverId);

    for($i=0; $i<sizeof($serverId); $i++)
    {
        if($check)
            $updateStatus = $updateStatus."; ";
        $check = true;
        $updateStatus = $updateStatus."UPDATE `mobile_gps_location` SET `send_status_computer` = 'Sent' WHERE `mob_loc_id` = '$serverId[$i]'";
    }

    mysqli_multi_query($con, $updateStatus);
}
elseif($operation == "Set order gps Status")
{
    $serverId = explode(",", $serverId);

    for($i=0; $i<sizeof($serverId); $i++)
    {
        if($check)
            $updateStatus = $updateStatus."; ";
        $check = true;
        $updateStatus = $updateStatus."UPDATE `order_gps_location` SET `send_status_computer` = 'Sent' WHERE `order_loc_id` = '$serverId[$i]'";
    }

    mysqli_multi_query($con, $updateStatus);
}
elseif($operation == "sync_area_info")
{
    $previousAddedIds = array();
    $sqlSelectIds = "SELECT `software_area_id` FROM `area_info` WHERE `software_lic_id` = '$softwareId'";
    $sqlSelectIds = mysqli_query($con, $sqlSelectIds);
    while($addedId = mysqli_fetch_array($sqlSelectIds))
    {
        array_push($previousAddedIds, $addedId[0]);
    }

    $areaTableId = $_POST['area_table_id'];
    $areaId = $_POST['area_id'];
    $areaName = $_POST['area_name'];
    $areaAbbrev = $_POST['area_abbrev'];
    $cityTableId = $_POST['city_table_id'];
    $areaStatus = $_POST['area_status'];
    $creatingUserId = $_POST['creating_user_id'];
    $creatingDate = $_POST['creating_date'];
    $creatingTime = $_POST['creating_time'];
    $updateUserId = $_POST['update_user_id'];
    $updateDate = $_POST['update_date'];
    $updateTime = $_POST['update_time'];
    $server_sync = $_POST['server_sync'];

//    $areaTableId = "1/-_/2";
//    $areaId = "0/-_/0";
//    $areaName = "Test/-_/Test 1";
//    $areaAbbrev = "Test/-_/Test 1";
//    $cityTableId = "0/-_/0";
//    $areaStatus = "Active/-_/Active";
//    $creatingUserId = "1/-_/1";
//    $creatingDate = "26/Jan/2022/-_/27/Jan/2022";
//    $creatingTime = "09:18 PM/-_/12:17 AM";
//    $updateUserId = "1/-_/1";
//    $updateDate = "26/Jan/2022/-_/27/Jan/2022";
//    $updateTime = "09:18 PM/-_/12:17 AM";
//    $server_sync = "0/-_/0";

    $areaTableId = explode("/-_/", $areaTableId);
    $areaId = explode("/-_/", $areaId);
    $areaName = explode("/-_/", $areaName);
    $areaAbbrev = explode("/-_/", $areaAbbrev);
    $cityTableId = explode("/-_/", $cityTableId);
    $areaStatus = explode("/-_/", $areaStatus);
    $creatingUserId = explode("/-_/", $creatingUserId);
    $creatingDate = explode("/-_/", $creatingDate);
    $creatingTime = explode("/-_/", $creatingTime);
    $updateUserId = explode("/-_/", $updateUserId);
    $updateDate = explode("/-_/", $updateDate);
    $updateTime = explode("/-_/", $updateTime);
    $server_sync = explode("/-_/", $server_sync);

    $insertCheck = false;
    $updateCheck = false;
    $insertQuery = "INSERT INTO `area_info`(`software_area_id`, `area_id`, `area_name`, `area_abbrev`, `city_table_id`, `area_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`, `software_lic_id`) VALUES ";
    $updateQuery = "";
    $insertSuccess = "false";
    $updateSuccess = "false";
    for($i=0; $i<sizeof($areaTableId); $i++)
    {
        if($server_sync[$i] == "Insert" || $server_sync[$i] == "0" || $server_sync[$i] == "")
        {
            if(!in_array($areaTableId[$i], $previousAddedIds))
            {
                if($insertCheck)
                {
                    $insertQuery = $insertQuery.", ";
                }
                $insertCheck = true;
                $insertQuery = $insertQuery."('$areaTableId[$i]','$areaId[$i]','$areaName[$i]','$areaAbbrev[$i]','$cityTableId[$i]','$areaStatus[$i]','$creatingUserId[$i]','$creatingDate[$i]','$creatingTime[$i]','$updateUserId[$i]','$updateDate[$i]','$updateTime[$i]','$softwareId')";
            }
        }
        elseif ($server_sync[$i] == "Update")
        {
            if($updateCheck)
            {
                $updateCheck = $updateCheck."; ";
            }
            $updateCheck = true;
            $updateQuery = $updateQuery."UPDATE `area_info` SET `area_id`='$areaId[$i]', `area_name`='$areaName[$i]', `area_abbrev`='$areaAbbrev[$i]', `city_table_id`='$cityTableId[$i]', `area_status`='$areaStatus[$i]', `creating_user_id`='$creatingUserId[$i]', `creating_date`='$creatingDate[$i]', `creating_time`='$creatingTime[$i]', `update_user_id`='$updateUserId[$i]', `update_date`= '$updateDate[$i]', `update_time`= '$updateTime[$i]' WHERE `software_area_id` = '$areaTableId[$i]' AND `software_lic_id` = '$softwareId'";
        }
    }
//    echo $insertQuery.'<br>';
    if($insertCheck && mysqli_query($con, $insertQuery))
    {
        $insertSuccess = "true";
    }
//    echo $updateQuery.'<br>';
    if($updateCheck && mysqli_multi_query($con, $updateQuery))
    {
        $updateSuccess = "true";
    }

    echo "Insert:".$insertSuccess.":Update:".$updateSuccess;
}
elseif($operation == "sync_batchwise_stock")
{
    $previousAddedIds = array();
    $sqlSelectIds = "SELECT `software_batch_stock_id` FROM `batchwise_stock` WHERE `software_lic_id` = '$softwareId'";
    $sqlSelectIds = mysqli_query($con, $sqlSelectIds);
    while($addedId = mysqli_fetch_array($sqlSelectIds))
    {
        array_push($previousAddedIds, $addedId[0]);
    }

    $batch_stock_id = $_POST['batch_stock_id'];
    $product_table_id = $_POST['product_table_id'];
    $batch_no = $_POST['batch_no'];
    $quantity = $_POST['quantity'];
    $bonus = $_POST['bonus'];
    $retail_price = $_POST['retail_price'];
    $trade_price = $_POST['trade_price'];
    $purchase_price = $_POST['purchase_price'];
    $batch_expiry = $_POST['batch_expiry'];
    $entry_date = $_POST['entry_date'];
    $entry_time = $_POST['entry_time'];
    $update_date = $_POST['update_date'];
    $update_time = $_POST['update_time'];
    $server_sync = $_POST['server_sync'];
    $software_id = $_POST['software_id'];

    $batch_stock_id = explode("/-_/", $batch_stock_id);
    $product_table_id = explode("/-_/", $product_table_id);
    $batch_no = explode("/-_/", $batch_no);
    $quantity = explode("/-_/", $quantity);
    $bonus = explode("/-_/", $bonus);
    $retail_price = explode("/-_/", $retail_price);
    $trade_price = explode("/-_/", $trade_price);
    $purchase_price = explode("/-_/", $purchase_price);
    $batch_expiry = explode("/-_/", $batch_expiry);
    $entry_date = explode("/-_/", $entry_date);
    $entry_time = explode("/-_/", $entry_time);
    $update_date = explode("/-_/", $update_date);
    $update_time = explode("/-_/", $update_time);
    $server_sync = explode("/-_/", $server_sync);

    $insertCheck = false;
    $updateCheck = false;
    $insertQuery = "INSERT INTO `batchwise_stock`(`software_batch_stock_id`, `product_table_id`, `batch_no`, `quantity`, `bonus`, `retail_price`, `trade_price`, `purchase_price`, `batch_expiry`, `entry_date`, `entry_time`, `update_date`, `update_time`, `software_lic_id`) VALUES ";
    $updateQuery = "";
    $insertSuccess = "false";
    $updateSuccess = "false";
    for($i=0; $i<sizeof($batch_stock_id); $i++)
    {
        if($server_sync[$i] == "Insert" || $server_sync[$i] == "0" || $server_sync[$i] == "")
        {
            if(!in_array($batch_stock_id[$i], $previousAddedIds))
            {
                if($insertCheck)
                {
                    $insertQuery = $insertQuery.", ";
                }
                $insertCheck = true;
                $insertQuery = $insertQuery."('$batch_stock_id[$i]','$product_table_id[$i]','$batch_no[$i]','$quantity[$i]','$bonus[$i]','$retail_price[$i]','$trade_price[$i]','$purchase_price[$i]','$batch_expiry[$i]','$entry_date[$i]','$entry_time[$i]','$update_date[$i]','$update_time[$i]','$softwareId')";
            }
        }
        elseif ($server_sync[$i] == "Update")
        {
            if($updateCheck)
            {
                $updateCheck = $updateCheck."; ";
            }
            $updateCheck = true;
            $updateQuery = $updateQuery."UPDATE `batchwise_stock` SET `product_table_id`='$product_table_id[$i]',`batch_no`= '$batch_no[$i]',`quantity`= '$quantity[$i]',`bonus`= '$bonus[$i]',`retail_price`= '$retail_price[$i]',`trade_price`= '$trade_price[$i]',`purchase_price`= '$purchase_price[$i]',`batch_expiry`= '$batch_expiry[$i]',`entry_date`= '$entry_date[$i]',`entry_time`= '$entry_time[$i]',`update_date`= '$update_date[$i]',`update_time`= '$update_time[$i]' WHERE `software_batch_stock_id` = '$batch_stock_id[$i]' AND `software_lic_id` = '$softwareId'";
        }
    }
//    echo $insertQuery.'<br>';
    if($insertCheck && mysqli_query($con, $insertQuery))
    {
        $insertSuccess = "true";
    }
//    echo $updateQuery.'<br>';
    if($updateCheck && mysqli_multi_query($con, $updateQuery))
    {
        $updateSuccess = "true";
    }

    echo "Insert:".$insertSuccess.":Update:".$updateSuccess;
}
elseif($operation == "sync_bonus_policy")
{
    $previousAddedIds = array();
    $sqlSelectIds = "SELECT `software_bonus_id` FROM `bonus_policy` WHERE `software_lic_id` = '$softwareId'";
    $sqlSelectIds = mysqli_query($con, $sqlSelectIds);
    while($addedId = mysqli_fetch_array($sqlSelectIds))
    {
        array_push($previousAddedIds, $addedId[0]);
    }
   $bonus_policyID = $_POST['bonus_policyID'];
   $approval_id = $_POST['approval_id'];
   $dealer_table_id = $_POST['dealer_table_id'];
   $company_table_id = $_POST['company_table_id'];
   $product_table_id = $_POST['product_table_id'];
   $quantity = $_POST['quantity'];
   $bonus_quant = $_POST['bonus_quant'];
   $start_date = $_POST['start_date'];
   $end_date = $_POST['end_date'];
   $policy_status = $_POST['policy_status'];
   $entered_by = $_POST['entered_by'];
   $entry_date = $_POST['entry_date'];
   $entry_time = $_POST['entry_time'];
   $updated_by = $_POST['updated_by'];
   $update_date = $_POST['update_date'];
   $update_time = $_POST['update_time'];
   $server_sync = $_POST['server_sync'];

   $bonus_policyID = explode("/-_/", $bonus_policyID);
   $approval_id = explode("/-_/", $approval_id);
   $dealer_table_id = explode("/-_/", $dealer_table_id);
   $company_table_id = explode("/-_/", $company_table_id);
   $product_table_id = explode("/-_/", $product_table_id);
   $quantity = explode("/-_/", $quantity);
   $bonus_quant = explode("/-_/", $bonus_quant);
   $start_date = explode("/-_/", $start_date);
   $end_date = explode("/-_/", $end_date);
   $policy_status = explode("/-_/", $policy_status);
   $entered_by = explode("/-_/", $entered_by);
   $entry_date = explode("/-_/", $entry_date);
   $entry_time = explode("/-_/", $entry_time);
   $updated_by = explode("/-_/", $updated_by);
   $update_date = explode("/-_/", $update_date);
   $update_time = explode("/-_/", $update_time);
   $server_sync = explode("/-_/", $server_sync);

    $insertCheck = false;
    $updateCheck = false;
    $insertQuery = "INSERT INTO `bonus_policy`(`software_bonus_id`, `approval_id`, `dealer_table_id`, `company_table_id`, `product_table_id`, `quantity`, `bonus_quant`, `start_date`, `end_date`, `policy_status`, `entered_by`, `entry_date`, `entry_time`, `updated_by`, `update_date`, `update_time`, `software_lic_id`) VALUES ";
    $updateQuery = "";
    $insertSuccess = "false";
    $updateSuccess = "false";
    for($i=0; $i<sizeof($bonus_policyID); $i++)
    {
        if($server_sync[$i] == "Insert" || $server_sync[$i] == "0" || $server_sync[$i] == "")
        {
            if(!in_array($bonus_policyID[$i], $previousAddedIds))
            {
                if($insertCheck)
                {
                    $insertQuery = $insertQuery.", ";
                }
                $insertCheck = true;
                $insertQuery = $insertQuery."('$bonus_policyID[$i]','$approval_id[$i]','$dealer_table_id[$i]','$company_table_id[$i]','$product_table_id[$i]','$quantity[$i]','$bonus_quant[$i]','$start_date[$i]','$end_date[$i]','$policy_status[$i]','$entered_by[$i]','$entry_date[$i]','$entry_time[$i]','$updated_by[$i]','$update_date[$i]','$update_time[$i]','$softwareId')";
            }

        }
        elseif ($server_sync[$i] == "Update")
        {
            if($updateCheck)
            {
                $updateCheck = $updateCheck."; ";
            }
            $updateCheck = true;
            $updateQuery = $updateQuery."UPDATE `bonus_policy` SET `approval_id`= '$approval_id[$i]', `dealer_table_id`= '$dealer_table_id[$i]', `company_table_id`= '$company_table_id[$i]',`product_table_id`= '$product_table_id[$i]', `quantity`= '$quantity[$i]', `bonus_quant`= '$bonus_quant[$i]', `start_date`= '$start_date[$i]', `end_date`= '$end_date[$i]', `policy_status`= '$policy_status[$i]',`entered_by`= '$entered_by[$i]', `entry_date`= '$entry_date[$i]',`entry_time`= '$entry_time[$i]',`updated_by`= '$updated_by[$i]',`update_date`= '$update_date[$i]', `update_time`= '$update_time[$i]' WHERE `software_bonus_id` = '$bonus_policyID[$i]' AND `software_lic_id` = '$softwareId'";
        }
    }
//    echo $insertQuery.'<br>';
    if($insertCheck && mysqli_query($con, $insertQuery))
    {
        $insertSuccess = "true";
    }
//    echo $updateQuery.'<br>';
    if($updateCheck && mysqli_multi_query($con, $updateQuery))
    {
        $updateSuccess = "true";
    }

    echo "Insert:".$insertSuccess.":Update:".$updateSuccess;
}
elseif($operation == "sync_city_info")
{
    $previousAddedIds = array();
    $sqlSelectIds = "SELECT `software_city_id` FROM `city_info` WHERE `software_lic_id` = '$softwareId'";
    $sqlSelectIds = mysqli_query($con, $sqlSelectIds);
    while($addedId = mysqli_fetch_array($sqlSelectIds))
    {
        array_push($previousAddedIds, $addedId[0]);
    }
   $city_table_id = $_POST['city_table_id'];
   $city_id = $_POST['city_id'];
   $district_table_id = $_POST['district_table_id'];
   $city_name = $_POST['city_name'];
   $city_status = $_POST['city_status'];
   $creating_user_id = $_POST['creating_user_id'];
   $creating_date = $_POST['creating_date'];
   $creating_time = $_POST['creating_time'];
   $update_user_id = $_POST['update_user_id'];
   $update_date = $_POST['update_date'];
   $update_time = $_POST['update_time'];
   $server_sync = $_POST['server_sync'];

   $city_table_id = explode("/-_/", $city_table_id);
   $city_id = explode("/-_/", $city_id);
   $district_table_id = explode("/-_/", $district_table_id);
   $city_name = explode("/-_/", $city_name);
   $city_status = explode("/-_/", $city_status);
   $creating_user_id = explode("/-_/", $creating_user_id);
   $creating_date = explode("/-_/", $creating_date);
   $creating_time = explode("/-_/", $creating_time);
   $update_user_id = explode("/-_/", $update_user_id);
   $update_date = explode("/-_/", $update_date);
   $update_time = explode("/-_/", $update_time);
   $server_sync = explode("/-_/", $server_sync);

    $insertCheck = false;
    $updateCheck = false;
    $insertQuery = "INSERT INTO `city_info`(`software_city_id`, `city_id`, `district_table_id`, `city_name`, `city_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`, `software_lic_id`) VALUES ";
    $updateQuery = "";
    $insertSuccess = "false";
    $updateSuccess = "false";
    for($i=0; $i<sizeof($city_table_id); $i++)
    {
        if($server_sync[$i] == "Insert" || $server_sync[$i] == "0" || $server_sync[$i] == "")
        {
            if(!in_array($city_table_id[$i], $previousAddedIds))
            {
                if($insertCheck)
                {
                    $insertQuery = $insertQuery.", ";
                }
                $insertCheck = true;
                $insertQuery = $insertQuery."('$city_table_id[$i]','$city_id[$i]','$district_table_id[$i]','$city_name[$i]','$city_status[$i]','$creating_user_id[$i]','$creating_date[$i]','$creating_time[$i]','$update_user_id[$i]','$update_date[$i]','$update_time[$i]','$softwareId')";
            }

        }
        elseif ($server_sync[$i] == "Update")
        {
            if($updateCheck)
            {
                $updateCheck = $updateCheck."; ";
            }
            $updateCheck = true;
            $updateQuery = $updateQuery."UPDATE `city_info` SET `city_id`= '$city_id[$i]',`district_table_id`= '$district_table_id[$i]',`city_name`= '$city_name[$i]',`city_status`= '$city_status[$i]',`creating_user_id`= '$creating_user_id[$i]',`creating_date`= '$creating_date[$i]',`creating_time`= '$creating_time[$i]',`update_user_id`= '$update_user_id[$i]',`update_date`= '$update_date[$i]',`update_time`= '$update_time[$i]' WHERE `software_city_id` = '$city_table_id[$i]' AND `software_lic_id` = '$softwareId'";
        }
    }
//    echo $insertQuery.'<br>';
    if($insertCheck && mysqli_query($con, $insertQuery))
    {
        $insertSuccess = "true";
    }
//    echo $updateQuery.'<br>';
    if($updateCheck && mysqli_multi_query($con, $updateQuery))
    {
        $updateSuccess = "true";
    }

    echo "Insert:".$insertSuccess.":Update:".$updateSuccess;
}
elseif($operation == "sync_company_alternate_contacts")
{
    $previousAddedIds = array();
    $sqlSelectIds = "SELECT `software_contact_id` FROM `company_alternate_contacts` WHERE `software_lic_id` = '$softwareId'";
    $sqlSelectIds = mysqli_query($con, $sqlSelectIds);
    while($addedId = mysqli_fetch_array($sqlSelectIds))
    {
        array_push($previousAddedIds, $addedId[0]);
    }
    $contact_id = $_POST['contact_id'];
    $company_id = $_POST['company_id'];
    $contact_name = $_POST['contact_name'];
    $contact_number = $_POST['contact_number'];
    $server_sync = $_POST['server_sync'];

    $contact_id = explode("/-_/", $contact_id);
    $company_id = explode("/-_/", $company_id);
    $contact_name = explode("/-_/", $contact_name);
    $contact_number = explode("/-_/", $contact_number);
    $server_sync = explode("/-_/", $server_sync);

    $insertCheck = false;
    $updateCheck = false;
    $insertQuery = "INSERT INTO `company_alternate_contacts`(`software_contact_id`, `company_id`, `contact_name`, `contact_number`, `software_lic_id`) VALUES ";
    $updateQuery = "";
    $insertSuccess = "false";
    $updateSuccess = "false";
    for($i=0; $i<sizeof($contact_id); $i++)
    {
        if($server_sync[$i] == "Insert" || $server_sync[$i] == "0" || $server_sync[$i] == "")
        {
            if(!in_array($contact_id[$i], $previousAddedIds))
            {
                if($insertCheck)
                {
                    $insertQuery = $insertQuery.", ";
                }
                $insertCheck = true;
                $insertQuery = $insertQuery."('$contact_id[$i]','$company_id[$i]','$contact_name[$i]','$contact_number[$i]','$softwareId')";
            }

        }
        elseif ($server_sync[$i] == "Update")
        {
            if($updateCheck)
            {
                $updateCheck = $updateCheck."; ";
            }
            $updateCheck = true;
            $updateQuery = $updateQuery."UPDATE `company_alternate_contacts` SET `company_id`= '$company_id[$i]',`contact_name`= '$contact_name[$i]',`contact_number`= '$contact_number[$i]' WHERE `software_contact_id` = '$contact_id[$i]' AND `software_lic_id` = '$softwareId'";
        }
    }
//    echo $insertQuery.'<br>';
    if($insertCheck && mysqli_query($con, $insertQuery))
    {
        $insertSuccess = "true";
    }
//    echo $updateQuery.'<br>';
    if($updateCheck && mysqli_multi_query($con, $updateQuery))
    {
        $updateSuccess = "true";
    }

    echo "Insert:".$insertSuccess.":Update:".$updateSuccess;
}
elseif($operation == "sync_company_info")
{
    $previousAddedIds = array();
    $sqlSelectIds = "SELECT `software_company_id` FROM `company_info` WHERE `software_lic_id` = '$softwareId'";
    $sqlSelectIds = mysqli_query($con, $sqlSelectIds);
    while($addedId = mysqli_fetch_array($sqlSelectIds))
    {
        array_push($previousAddedIds, $addedId[0]);
    }
    $company_table_id = $_POST['company_table_id'];
    $company_id = $_POST['company_id'];
    $company_name = $_POST['company_name'];
    $company_address = $_POST['company_address'];
    $company_city = $_POST['company_city'];
    $company_contact = $_POST['company_contact'];
    $contact_Person = $_POST['contact_Person'];
    $company_email  = $_POST['company_email'];
    $company_image = $_POST['company_image'];
    $company_status = $_POST['company_status'];
    $creating_user_id = $_POST['creating_user_id'];
    $creating_date = $_POST['creating_date'];
    $creating_time = $_POST['creating_time'];
    $update_user_id = $_POST['update_user_id'];
    $update_date = $_POST['update_date'];
    $update_time = $_POST['update_time'];
    $server_sync = $_POST['server_sync'];

    $company_table_id = explode("/-_/", $company_table_id);
    $company_id = explode("/-_/", $company_id);
    $company_name = explode("/-_/", $company_name);
    $company_address = explode("/-_/", $company_address);
    $company_city = explode("/-_/", $company_city);
    $company_contact = explode("/-_/", $company_contact);
    $contact_Person = explode("/-_/", $contact_Person);
    $company_email  = explode("/-_/", $company_email );
    $company_image = explode("/-_/", $company_image);
    $company_status = explode("/-_/", $company_status);
    $creating_user_id = explode("/-_/", $creating_user_id);
    $creating_date = explode("/-_/", $creating_date);
    $creating_time = explode("/-_/", $creating_time);
    $update_user_id = explode("/-_/", $update_user_id);
    $update_date = explode("/-_/", $update_date);
    $update_time = explode("/-_/", $update_time);
    $server_sync = explode("/-_/", $server_sync);

    $insertCheck = false;
    $updateCheck = false;
    $insertQuery = "INSERT INTO `company_info`(`software_company_id`, `company_id`, `company_name`, `company_address`, `company_city`, `company_contact`, `contact_Person`, `company_email`, `company_image`, `company_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`, `software_lic_id`) VALUES ";
    $updateQuery = "";
    $insertSuccess = "false";
    $updateSuccess = "false";
    for($i=0; $i<sizeof($company_table_id); $i++)
    {
        if($server_sync[$i] == "Insert" || $server_sync[$i] == "0" || $server_sync[$i] == "")
        {
            if(!in_array($company_table_id[$i], $previousAddedIds))
            {
                if($insertCheck)
                {
                    $insertQuery = $insertQuery.", ";
                }
                $insertCheck = true;
                $insertQuery = $insertQuery."('$company_table_id[$i]','$company_id[$i]','$company_name[$i]','$company_address[$i]','$company_city[$i]','$company_contact[$i]', '$contact_Person[$i]', '$company_email[$i]', '$company_image[$i]', '$company_status[$i]', '$creating_user_id[$i]', '$creating_date[$i]', '$creating_time[$i]', '$update_user_id[$i]', '$update_date[$i]', '$update_time[$i]','$softwareId')";
            }

        }
        elseif ($server_sync[$i] == "Update")
        {
            if($updateCheck)
            {
                $updateCheck = $updateCheck."; ";
            }
            $updateCheck = true;
            $updateQuery = $updateQuery."UPDATE `company_info` SET `company_id`= '$company_id[$i]',`company_name`= '$company_name[$i]',`company_address`= '$company_address[$i]',`company_city`= '$company_city[$i]',`company_contact`= '$company_contact[$i]',`contact_Person`= '$contact_Person[$i]',`company_email`= '$company_email[$i]',`company_image`= '$company_image[$i]',`company_status`= '$company_status[$i]',`creating_user_id`= '$creating_user_id[$i]',`creating_date`= '$creating_date[$i]',`creating_time`= '$creating_time[$i]',`update_user_id`= '$update_user_id[$i]',`update_date`= '$update_date[$i]',`update_time`= '$update_time[$i]' WHERE `software_company_id` = '$company_table_id[$i]' AND `software_lic_id` = '$softwareId'";
        }
    }
//    echo $insertQuery.'<br>';
    if($insertCheck && mysqli_query($con, $insertQuery))
    {
        $insertSuccess = "true";
    }
//    echo $updateQuery.'<br>';
    if($updateCheck && mysqli_multi_query($con, $updateQuery))
    {
        $updateSuccess = "true";
    }

    echo "Insert:".$insertSuccess.":Update:".$updateSuccess;
}
elseif($operation == "sync_company_overall_record")
{
    $previousAddedIds = array();
    $sqlSelectIds = "SELECT `software_company_overall_id` FROM `company_overall_record` WHERE `software_lic_id` = '$softwareId'";
    $sqlSelectIds = mysqli_query($con, $sqlSelectIds);
    while($addedId = mysqli_fetch_array($sqlSelectIds))
    {
        array_push($previousAddedIds, $addedId[0]);
    }
    $company_overall_id = $_POST['company_overall_id'];
    $company_id = $_POST['company_id'];
    $orders_given = $_POST['orders_given'];
    $successful_orders = $_POST['successful_orders'];
    $ordered_packets = $_POST['ordered_packets'];
    $received_packets = $_POST['received_packets'];
    $ordered_boxes = $_POST['ordered_boxes'];
    $received_boxes = $_POST['received_boxes'];
    $order_price = $_POST['order_price'];
    $discount_price = $_POST['discount_price'];
    $invoiced_price = $_POST['invoiced_price'];
    $cash_sent = $_POST['cash_sent'];
    $return_packets = $_POST['return_packets'];
    $return_boxes = $_POST['return_boxes'];
    $return_price = $_POST['return_price'];
    $cash_return = $_POST['cash_return'];
    $waived_off_price = $_POST['waived_off_price'];
    $pending_payments = $_POST['pending_payments'];
    $server_sync = $_POST['server_sync'];

    $company_overall_id = explode("/-_/", $company_overall_id);
    $company_id = explode("/-_/", $company_id);
    $orders_given = explode("/-_/", $orders_given);
    $successful_orders = explode("/-_/", $successful_orders);
    $ordered_packets = explode("/-_/", $ordered_packets);
    $received_packets = explode("/-_/", $received_packets);
    $ordered_boxes = explode("/-_/", $ordered_boxes);
    $received_boxes = explode("/-_/", $received_boxes);
    $order_price = explode("/-_/", $order_price);
    $discount_price = explode("/-_/", $discount_price);
    $invoiced_price = explode("/-_/", $invoiced_price);
    $cash_sent = explode("/-_/", $cash_sent);
    $return_packets = explode("/-_/", $return_packets);
    $return_boxes = explode("/-_/", $return_boxes);
    $return_price = explode("/-_/", $return_price);
    $cash_return = explode("/-_/", $cash_return);
    $waived_off_price = explode("/-_/", $waived_off_price);
    $pending_payments = explode("/-_/", $pending_payments);
    $server_sync = explode("/-_/", $server_sync);

    $insertCheck = false;
    $updateCheck = false;
    $insertQuery = "INSERT INTO `company_overall_record`(`software_company_overall_id`, `company_id`, `orders_given`, `successful_orders`, `ordered_packets`, `received_packets`, `ordered_boxes`, `received_boxes`, `order_price`, `discount_price`, `invoiced_price`, `cash_sent`, `return_packets`, `return_boxes`, `return_price`, `cash_return`, `waived_off_price`, `pending_payments`, `software_lic_id`) VALUES ";
    $updateQuery = "";
    $insertSuccess = "false";
    $updateSuccess = "false";
    for($i=0; $i<sizeof($company_overall_id); $i++)
    {
        if($server_sync[$i] == "Insert" || $server_sync[$i] == "0" || $server_sync[$i] == "")
        {
            if(!in_array($company_overall_id[$i], $previousAddedIds))
            {
                if($insertCheck)
                {
                    $insertQuery = $insertQuery.", ";
                }
                $insertCheck = true;
                $insertQuery = $insertQuery."('$company_overall_id[$i]','$company_id[$i]', '$orders_given[$i]', '$successful_orders[$i]', '$ordered_packets[$i]', '$received_packets[$i]', '$ordered_boxes[$i]', '$received_boxes[$i]', '$order_price[$i]', '$discount_price[$i]', '$invoiced_price[$i]', '$cash_sent[$i]', '$return_packets[$i]', '$return_boxes[$i]', '$return_price[$i]', '$cash_return[$i]', '$waived_off_price[$i]', '$pending_payments[$i]', '$softwareId')";
            }

        }
        elseif ($server_sync[$i] == "Update")
        {
            if($updateCheck)
            {
                $updateCheck = $updateCheck."; ";
            }
            $updateCheck = true;
            $updateQuery = $updateQuery."UPDATE `company_overall_record` SET `company_id`= '$company_id[$i]',`orders_given`= '$orders_given[$i]',`successful_orders`= '$successful_orders[$i]', `ordered_packets`= '$ordered_packets[$i]',`received_packets`= '$received_packets[$i]',`ordered_boxes`= '$ordered_boxes[$i]',`received_boxes`= '$eceeived_boxes[$i]', `order_price`= '$order_price[$i]',`discount_price`= '$discount_price[$i]',`invoiced_price`= '$invoiced_price[$i]',`cash_sent`= '$cash_sent[$i]',`return_packets`= '$return_packets[$i]',`return_boxes`= '$return_boxes[$i]',`return_price`= '$return_price[$i]',`cash_return`= '$cash_return[$i]',`waived_off_price`= '$waived_off_price[$i]',`pending_payments`= '$pending_payments[$i]' WHERE `software_company_overall_id` = '$company_overall_id[$i]' AND `software_lic_id` = '$softwareId'";
        }
    }
//    echo $insertQuery.'<br>';
    if($insertCheck && mysqli_query($con, $insertQuery))
    {
        $insertSuccess = "true";
    }
//    echo $updateQuery.'<br>';
    if($updateCheck && mysqli_multi_query($con, $updateQuery))
    {
        $updateSuccess = "true";
    }

    echo "Insert:".$insertSuccess.":Update:".$updateSuccess;
}
elseif($operation == "sync_company_payments")
{
    $previousAddedIds = array();
    $sqlSelectIds = "SELECT `software_payment_id` FROM `company_payments` WHERE `software_lic_id` = '$softwareId'";
    $sqlSelectIds = mysqli_query($con, $sqlSelectIds);
    while($addedId = mysqli_fetch_array($sqlSelectIds))
    {
        array_push($previousAddedIds, $addedId[0]);
    }
    $payment_id = $_POST['payment_id'];
    $company_id = $_POST['company_id'];
    $invoice_id = $_POST['invoice_id'];
    $amount = $_POST['amount'];
    $cash_type = $_POST['cash_type'];
    $pending_payments = $_POST['pending_payments'];
    $date = $_POST['date'];
    $day = $_POST['day'];
    $user_id = $_POST['user_id'];
    $comments = $_POST['comments'];
    $status = $_POST['status'];
    $server_sync = $_POST['server_sync'];

    $payment_id = explode("/-_/", $payment_id);
    $company_id = explode("/-_/", $company_id);
    $invoice_id = explode("/-_/", $invoice_id);
    $amount = explode("/-_/", $amount);
    $cash_type = explode("/-_/", $cash_type);
    $pending_payments = explode("/-_/", $pending_payments);
    $date = explode("/-_/", $date);
    $day = explode("/-_/", $day);
    $user_id = explode("/-_/", $user_id);
    $comments = explode("/-_/", $comments);
    $status = explode("/-_/", $status);
    $server_sync = explode("/-_/", $server_sync);

    $insertCheck = false;
    $updateCheck = false;
    $insertQuery = "INSERT INTO `company_payments`(`software_payment_id`, `company_id`, `invoice_id`, `amount`, `cash_type`, `pending_payments`, `date`, `day`, `user_id`, `comments`, `status`, `software_lic_id`) VALUES ";
    $updateQuery = "";
    $insertSuccess = "false";
    $updateSuccess = "false";
    for($i=0; $i<sizeof($payment_id); $i++)
    {
        if($server_sync[$i] == "Insert" || $server_sync[$i] == "0" || $server_sync[$i] == "")
        {
            if(!in_array($payment_id[$i], $previousAddedIds))
            {
               if($insertCheck)
                {
                    $insertQuery = $insertQuery.", ";
                }
                $insertCheck = true;
                $insertQuery = $insertQuery."('$payment_id[$i]','$company_id[$i]', '$invoice_id[$i]', '$amount[$i]', '$cash_type[$i]', '$pending_payments[$i]', '$date[$i]', '$day[$i]', '$user_id[$i]', '$comments[$i]', '$status[$i]', '$softwareId')";
            }

        }
        elseif ($server_sync[$i] == "Update")
        {
            if($updateCheck)
            {
                $updateCheck = $updateCheck."; ";
            }
            $updateCheck = true;
            $updateQuery = $updateQuery."UPDATE `company_payments` SET `company_id`= '$company_id[$i]',`invoice_id`= '$invoice_id[$i]',`amount`= '$amount[$i]',`cash_type`= '$cash_type[$i]',`pending_payments`= '$pending_payments[$i]',`date`= '$date[$i]',`day`= '$day[$i]',`user_id`= '$user_id[$i]',`comments`= '$comments[$i]',`status`= '$status[$i]' WHERE `software_payment_id` = '$payment_id[$i]' AND `software_lic_id` = '$softwareId'";
        }
    }
//    echo $insertQuery.'<br>';
    if($insertCheck && mysqli_query($con, $insertQuery))
    {
        $insertSuccess = "true";
    }
//    echo $updateQuery.'<br>';
    if($updateCheck && mysqli_multi_query($con, $updateQuery))
    {
        $updateSuccess = "true";
    }

    echo "Insert:".$insertSuccess.":Update:".$updateSuccess;
}
elseif($operation == "sync_dealer_day_summary")
{
    $previousAddedIds = array();
    $sqlSelectIds = "SELECT `software_dealer_summary_id` FROM `dealer_day_summary` WHERE `software_lic_id` = '$softwareId'";
    $sqlSelectIds = mysqli_query($con, $sqlSelectIds);
    while($addedId = mysqli_fetch_array($sqlSelectIds))
    {
        array_push($previousAddedIds, $addedId[0]);
    }
    $dealer_summary_id  = $_POST['dealer_summary_id'];
    $dealer_id  = $_POST['dealer_id'];
    $date = $_POST['date'];
    $day = $_POST['day'];
    $booking_order = $_POST['booking_order'];
    $delivered_order = $_POST['delivered_order'];
    $returned_order = $_POST['returned_order'];
    $return_quantity = $_POST['return_quantity'];
    $return_price = $_POST['return_price'];
    $cash_collection = $_POST['cash_collection'];
    $cash_return = $_POST['cash_return'];
    $cash_waiveoff = $_POST['cash_waiveoff'];
    $server_sync = $_POST['server_sync'];

    $dealer_summary_id  = explode("/-_/", $dealer_summary_id);
    $dealer_id  = explode("/-_/", $dealer_id);
    $date = explode("/-_/", $date);
    $day = explode("/-_/", $day);
    $booking_order = explode("/-_/", $booking_order);
    $delivered_order = explode("/-_/", $delivered_order);
    $returned_order = explode("/-_/", $returned_order);
    $return_quantity = explode("/-_/", $return_quantity);
    $return_price = explode("/-_/", $return_price);
    $cash_collection = explode("/-_/", $cash_collection);
    $cash_return = explode("/-_/", $cash_return);
    $cash_waiveoff = explode("/-_/", $cash_waiveoff);
    $server_sync = explode("/-_/", $server_sync);

    $insertCheck = false;
    $updateCheck = false;
    $insertQuery = "INSERT INTO `dealer_day_summary`(`software_dealer_summary_id`, `dealer_id`, `date`, `day`, `booking_order`, `delivered_order`, `returned_order`, `return_quantity`, `return_price`, `cash_collection`, `cash_return`, `cash_waiveoff`, `software_lic_id`) VALUES ";
    $updateQuery = "";
    $insertSuccess = "false";
    $updateSuccess = "false";
    for($i=0; $i<sizeof($dealer_summary_id); $i++)
    {
        if($server_sync[$i] == "Insert" || $server_sync[$i] == "0" || $server_sync[$i] == "")
        {
            if(!in_array($dealer_summary_id[$i], $previousAddedIds))
            {
                if($insertCheck)
                {
                    $insertQuery = $insertQuery.", ";
                }
                $insertCheck = true;
                $insertQuery = $insertQuery."('$dealer_summary_id[$i]', '$dealer_id[$i]', '$date[$i]', '$day[$i]', '$booking_order[$i]', '$delivered_order[$i]', '$returned_order[$i]', '$return_quantity[$i]', '$return_price[$i]', '$cash_collection[$i]', '$cash_return[$i]', '$cash_waiveoff[$i]','$softwareId')";
            }

        }
        elseif ($server_sync[$i] == "Update")
        {
            if($updateCheck)
            {
                $updateCheck = $updateCheck."; ";
            }
            $updateCheck = true;
            $updateQuery = $updateQuery."UPDATE `dealer_day_summary` SET `dealer_id`= '$dealer_id[$i]',`date`= '$date[$i]',`day`= '$day[$i]',`booking_order`= '$booking_order[$i]',`delivered_order`= '$delivered_order[$i]',`returned_order`= '$returned_order[$i]',`return_quantity`= '$return_quantity[$i]',`return_price`= '$return_price[$i]',`cash_collection`= '$cash_collection[$i]',`cash_return`= '$cash_return[$i]',`cash_waiveoff`= '$cash_waiveoff[$i]' WHERE `software_dealer_summary_id` = '$dealer_summary_id[$i]' AND `software_lic_id` = '$softwareId'";
        }
    }
//    echo $insertQuery.'<br>';
    if($insertCheck && mysqli_query($con, $insertQuery))
    {
        $insertSuccess = "true";
    }
//    echo $updateQuery.'<br>';
    if($updateCheck && mysqli_multi_query($con, $updateQuery))
    {
        $updateSuccess = "true";
    }

    echo "Insert:".$insertSuccess.":Update:".$updateSuccess;
}
elseif($operation == "sync_dealer_gps_location")
{
    $previousAddedIds = array();
    $sqlSelectIds = "SELECT `software_dealer_loc_id` FROM `dealer_gps_location` WHERE `software_lic_id` = '$softwareId'";
    $sqlSelectIds = mysqli_query($con, $sqlSelectIds);
    while($addedId = mysqli_fetch_array($sqlSelectIds))
    {
        array_push($previousAddedIds, $addedId[0]);
    }
    $dealer_loc_id = $_POST['dealer_loc_id'];
    $dealer_id = $_POST['dealer_id'];
    $latitude = $_POST['latitude'];
    $longitude = $_POST['longitude'];
    $loc_name = $_POST['loc_name'];
    $entry_date = $_POST['entry_date'];
    $entry_time = $_POST['entry_time'];
    $update_date = $_POST['update_date'];
    $update_time = $_POST['update_time'];
    $user_id = $_POST['user_id'];
    $server_sync = $_POST['server_sync'];
    $software_id = $_POST['software_id'];

    $dealer_loc_id = explode("/-_/", $dealer_loc_id);
    $dealer_id = explode("/-_/", $dealer_id);
    $latitude = explode("/-_/", $latitude);
    $longitude = explode("/-_/", $longitude);
    $loc_name = explode("/-_/", $loc_name);
    $entry_date = explode("/-_/", $entry_date);
    $entry_time = explode("/-_/", $entry_time);
    $update_date = explode("/-_/", $update_date);
    $update_time = explode("/-_/", $update_time);
    $user_id = explode("/-_/", $user_id);
    $server_sync = explode("/-_/", $server_sync);
    $software_id = explode("/-_/", $software_id);

    $insertCheck = false;
    $updateCheck = false;
    $insertQuery = "INSERT INTO `dealer_gps_location`(`software_dealer_loc_id`, `dealer_id`, `latitude`, `longitude`, `loc_name`, `entry_date`, `entry_time`, `update_date`, `update_time`, `user_id`, `software_lic_id`) VALUES ";
    $updateQuery = "";
    $insertSuccess = "false";
    $updateSuccess = "false";
    for($i=0; $i<sizeof($dealer_loc_id); $i++)
    {
        if($server_sync[$i] == "Insert" || $server_sync[$i] == "0" || $server_sync[$i] == "")
        {
            if(!in_array($dealer_loc_id[$i], $previousAddedIds))
            {
                if($insertCheck)
                {
                    $insertQuery = $insertQuery.", ";
                }
                $insertCheck = true;
                $insertQuery = $insertQuery."('$dealer_loc_id[$i]', '$dealer_id[$i]', '$latitude[$i]', '$longitude[$i]', '$loc_name[$i]', '$entry_date[$i]', '$entry_time[$i]', '$update_date[$i]', '$update_time[$i]', '$user_id[$i]','$softwareId')";
            }

        }
        elseif ($server_sync[$i] == "Update")
        {
            if($updateCheck)
            {
                $updateCheck = $updateCheck."; ";
            }
            $updateCheck = true;
            $updateQuery = $updateQuery."UPDATE `dealer_gps_location` SET `dealer_id`= '$dealer_id[$i]',`latitude`= '$latitude[$i]',`longitude`= '$longitude[$i]',`loc_name`= '$loc_name[$i]',`entry_date`= '$entry_date[$i]',`entry_time`= '$entry_time[$i]',`update_date`= '$update_date[$i]',`update_time`= '$update_time[$i]',`user_id`= '$user_id[$i]' WHERE `software_dealer_loc_id` = '$dealer_loc_id[$i]' AND `software_lic_id` = '$softwareId'";
        }
    }
//    echo $insertQuery.'<br>';
    if($insertCheck && mysqli_query($con, $insertQuery))
    {
        $insertSuccess = "true";
    }
//    echo $updateQuery.'<br>';
    if($updateCheck && mysqli_multi_query($con, $updateQuery))
    {
        $updateSuccess = "true";
    }

    echo "Insert:".$insertSuccess.":Update:".$updateSuccess;
}
elseif($operation == "sync_dealer_info")
{
    $previousAddedIds = array();
    $sqlSelectIds = "SELECT `software_dealer_id` FROM `dealer_info` WHERE `software_lic_id` = '$softwareId'";
    $sqlSelectIds = mysqli_query($con, $sqlSelectIds);
    while($addedId = mysqli_fetch_array($sqlSelectIds))
    {
        array_push($previousAddedIds, $addedId[0]);
    }
    $dealer_table_id = $_POST['dealer_table_id'];
    $dealer_id = $_POST['dealer_id'];
    $dealer_area_id = $_POST['dealer_area_id'];
    $dealer_name = $_POST['dealer_name'];
    $dealer_contact_person = $_POST['dealer_contact_person'];
    $dealer_phone = $_POST['dealer_phone'];
    $dealer_fax = $_POST['dealer_fax'];
    $dealer_address = $_POST['dealer_address'];
    $dealer_address1 = $_POST['dealer_address1'];
    $dealer_type = $_POST['dealer_type'];
    $dealer_cnic = $_POST['dealer_cnic'];
    $dealer_ntn = $_POST['dealer_ntn'];
    $dealer_image = $_POST['dealer_image'];
    $dealer_lic9_num = $_POST['dealer_lic9_num'];
    $dealer_lic9_exp = $_POST['dealer_lic9_exp'];
    $dealer_lic10_num = $_POST['dealer_lic10_num'];
    $dealer_lic10_Exp = $_POST['dealer_lic10_Exp'];
    $dealer_lic11_num = $_POST['dealer_lic11_num'];
    $dealer_lic11_Exp = $_POST['dealer_lic11_Exp'];
    $dealer_credit_limit = $_POST['dealer_credit_limit'];
    $dealer_status = $_POST['dealer_status'];
    $creating_user_id = $_POST['creating_user_id'];
    $creating_date = $_POST['creating_date'];
    $creating_time = $_POST['creating_time'];
    $update_user_id = $_POST['update_user_id'];
    $update_date = $_POST['update_date'];
    $update_time = $_POST['update_time'];
    $server_sync = $_POST['server_sync'];

    $dealer_table_id = explode("/-_/", $dealer_table_id);
    $dealer_id = explode("/-_/", $dealer_id);
    $dealer_area_id = explode("/-_/", $dealer_area_id);
    $dealer_name = explode("/-_/", $dealer_name);
    $dealer_contact_person = explode("/-_/", $dealer_contact_person);
    $dealer_phone = explode("/-_/", $dealer_phone);
    $dealer_fax = explode("/-_/", $dealer_fax);
    $dealer_address = explode("/-_/", $dealer_address);
    $dealer_address1 = explode("/-_/", $dealer_address1);
    $dealer_type = explode("/-_/", $dealer_type);
    $dealer_cnic = explode("/-_/", $dealer_cnic);
    $dealer_ntn = explode("/-_/", $dealer_ntn);
    $dealer_image = explode("/-_/", $dealer_image);
    $dealer_lic9_num = explode("/-_/", $dealer_lic9_num);
    $dealer_lic9_exp = explode("/-_/", $dealer_lic9_exp);
    $dealer_lic10_num = explode("/-_/", $dealer_lic10_num);
    $dealer_lic10_Exp = explode("/-_/", $dealer_lic10_Exp);
    $dealer_lic11_num = explode("/-_/", $dealer_lic11_num);
    $dealer_lic11_Exp = explode("/-_/", $dealer_lic11_Exp);
    $dealer_credit_limit = explode("/-_/", $dealer_credit_limit);
    $dealer_status = explode("/-_/", $dealer_status);
    $creating_user_id = explode("/-_/", $creating_user_id);
    $creating_date = explode("/-_/", $creating_date);
    $creating_time = explode("/-_/", $creating_time);
    $update_user_id = explode("/-_/", $update_user_id);
    $update_date = explode("/-_/", $update_date);
    $update_time = explode("/-_/", $update_time);
    $server_sync = explode("/-_/", $server_sync);

    $insertCheck = false;
    $updateCheck = false;
    $insertQuery = "INSERT INTO `dealer_info`(`software_dealer_id`, `dealer_id`, `dealer_area_id`, `dealer_name`, `dealer_contact_person`, `dealer_phone`, `dealer_fax`, `dealer_address`, `dealer_address1`, `dealer_type`, `dealer_cnic`, `dealer_ntn`, `dealer_image`, `dealer_lic9_num`, `dealer_lic9_exp`, `dealer_lic10_num`, `dealer_lic10_Exp`, `dealer_lic11_num`, `dealer_lic11_Exp`, `dealer_credit_limit`, `dealer_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`, `software_lic_id`) VALUES ";
    $updateQuery = "";
    $insertSuccess = "false";
    $updateSuccess = "false";
    for($i=0; $i<sizeof($dealer_table_id); $i++)
    {
        if($server_sync[$i] == "Insert" || $server_sync[$i] == "0" || $server_sync[$i] == "")
        {
            if(!in_array($dealer_table_id[$i], $previousAddedIds))
            {
                if($insertCheck)
                {
                    $insertQuery = $insertQuery.", ";
                }
                $insertCheck = true;
                $insertQuery = $insertQuery."('$dealer_table_id[$i]', '$dealer_id[$i]', '$dealer_area_id[$i]', '$dealer_name[$i]', '$dealer_contact_person[$i]', '$dealer_phone[$i]', '$dealer_fax[$i]', '$dealer_address[$i]', '$dealer_address1[$i]', '$dealer_type[$i]', '$dealer_cnic[$i]', '$dealer_ntn[$i]', '$dealer_image[$i]', '$dealer_lic9_num[$i]', '$dealer_lic9_exp[$i]', '$dealer_lic10_num[$i]', '$dealer_lic10_Exp[$i]', '$dealer_lic11_num[$i]', '$dealer_lic11_Exp[$i]', '$dealer_credit_limit[$i]', '$dealer_status[$i]', '$creating_user_id[$i]', '$creating_date[$i]', '$creating_time[$i]', '$update_user_id[$i]', '$update_date[$i]', '$update_time[$i]','$softwareId')";
            }

        }
        elseif ($server_sync[$i] == "Update")
        {
            if($updateCheck)
            {
                $updateCheck = $updateCheck."; ";
            }
            $updateCheck = true;
            $updateQuery = $updateQuery."UPDATE `dealer_info` SET `dealer_id`='$dealer_id[$i]',`dealer_area_id`= '$dealer_area_id[$i]',`dealer_name`= '$dealer_name[$i]',`dealer_contact_person`= '$dealer_contact_person[$i]',`dealer_phone`= '$dealer_phone[$i]',`dealer_fax`= '$dealer_fax[$i]',`dealer_address`= '$dealer_address[$i]',`dealer_address1`= '$dealer_address1[$i]',`dealer_type`= '$dealer_type[$i]',`dealer_cnic`= '$dealer_cnic[$i]',`dealer_ntn`= '$dealer_ntn[$i]',`dealer_image`= '$dealer_image[$i]',`dealer_lic9_num`= '$dealer_lic9_num[$i]',`dealer_lic9_exp`= '$dealer_lic9_exp[$i]',`dealer_lic10_num`= '$dealer_lic10_num[$i]',`dealer_lic10_Exp`= '$dealer_lic10_Exp[$i]',`dealer_lic11_num`= '$dealer_lic11_num[$i]',`dealer_lic11_Exp`= '$dealer_lic11_Exp[$i]',`dealer_credit_limit`= '$dealer_credit_limit[$i]',`dealer_status`= '$dealer_status[$i]',`creating_user_id`= '$creating_user_id[$i]',`creating_date`= '$creating_date[$i]',`creating_time`= '$creating_time[$i]',`update_user_id`= '$update_user_id[$i]',`update_date`= '$update_date[$i]',`update_time`= '$update_time[$i]' WHERE `software_dealer_id` = '$dealer_table_id[$i]' AND `software_lic_id` = '$softwareId'";
        }
    }
//    echo $insertQuery.'<br>';
    if($insertCheck && mysqli_query($con, $insertQuery))
    {
        $insertSuccess = "true";
    }
//    echo $updateQuery.'<br>';
    if($updateCheck && mysqli_multi_query($con, $updateQuery))
    {
        $updateSuccess = "true";
    }

    echo "Insert:".$insertSuccess.":Update:".$updateSuccess;
}
elseif($operation == "sync_dealer_overall_record")
{
    $previousAddedIds = array();
    $sqlSelectIds = "SELECT `software_dealer_overall_id` FROM `dealer_overall_record` WHERE `software_lic_id` = '$softwareId'";
    $sqlSelectIds = mysqli_query($con, $sqlSelectIds);
    while($addedId = mysqli_fetch_array($sqlSelectIds))
    {
        array_push($previousAddedIds, $addedId[0]);
    }
    $dealer_overall_id = $_POST['dealer_overall_id'];
    $dealer_id = $_POST['dealer_id'];
    $orders_given = $_POST['orders_given'];
    $successful_orders = $_POST['successful_orders'];
    $ordered_packets = $_POST['ordered_packets'];
    $submitted_packets = $_POST['submitted_packets'];
    $ordered_boxes = $_POST['ordered_boxes'];
    $submitted_boxes = $_POST['submitted_boxes'];
    $order_price = $_POST['order_price'];
    $discount_price = $_POST['discount_price'];
    $invoiced_price = $_POST['invoiced_price'];
    $cash_collected = $_POST['cash_collected'];
    $return_packets = $_POST['return_packets'];
    $return_boxes = $_POST['return_boxes'];
    $return_price = $_POST['return_price'];
    $cash_return = $_POST['cash_return'];
    $waived_off_price = $_POST['waived_off_price'];
    $pending_payments = $_POST['pending_payments'];
    $server_sync = $_POST['server_sync'];

    $dealer_overall_id = explode("/-_/", $dealer_overall_id);
    $dealer_id = explode("/-_/", $dealer_id);
    $orders_given = explode("/-_/", $orders_given);
    $successful_orders = explode("/-_/", $successful_orders);
    $ordered_packets = explode("/-_/", $ordered_packets);
    $submitted_packets = explode("/-_/", $submitted_packets);
    $ordered_boxes = explode("/-_/", $ordered_boxes);
    $submitted_boxes = explode("/-_/", $submitted_boxes);
    $order_price = explode("/-_/", $order_price);
    $discount_price = explode("/-_/", $discount_price);
    $invoiced_price = explode("/-_/", $invoiced_price);
    $cash_collected = explode("/-_/", $cash_collected);
    $return_packets = explode("/-_/", $return_packets);
    $return_boxes = explode("/-_/", $return_boxes);
    $return_price = explode("/-_/", $return_price);
    $cash_return = explode("/-_/", $cash_return);
    $waived_off_price = explode("/-_/", $waived_off_price);
    $pending_payments = explode("/-_/", $pending_payments);
    $server_sync = explode("/-_/", $server_sync);

    $insertCheck = false;
    $updateCheck = false;
    $insertQuery = "INSERT INTO `dealer_overall_record`(`software_dealer_overall_id`, `dealer_id`, `orders_given`, `successful_orders`, `ordered_packets`, `submitted_packets`, `ordered_boxes`, `submitted_boxes`, `order_price`, `discount_price`, `invoiced_price`, `cash_collected`, `return_packets`, `return_boxes`, `return_price`, `cash_return`, `waived_off_price`, `pending_payments`, `software_lic_id`) VALUES ";
    $updateQuery = "";
    $insertSuccess = "false";
    $updateSuccess = "false";
    for($i=0; $i<sizeof($dealer_overall_id); $i++)
    {
        if($server_sync[$i] == "Insert" || $server_sync[$i] == "0" || $server_sync[$i] == "")
        {
            if(!in_array($dealer_overall_id[$i], $previousAddedIds))
            {
                if($insertCheck)
                {
                    $insertQuery = $insertQuery.", ";
                }
                $insertCheck = true;
                $insertQuery = $insertQuery."('$dealer_overall_id[$i]', '$dealer_id[$i]', '$orders_given[$i]', '$successful_orders[$i]', '$ordered_packets[$i]', '$submitted_packets[$i]', '$ordered_boxes[$i]', '$submitted_boxes[$i]', '$order_price[$i]', '$discount_price[$i]', '$invoiced_price[$i]', '$cash_collected[$i]', '$return_packets[$i]', '$return_boxes[$i]', '$return_price[$i]', '$cash_return[$i]', '$waived_off_price[$i]', '$pending_payments[$i]','$softwareId')";
            }

        }
        elseif ($server_sync[$i] == "Update")
        {
            if($updateCheck)
            {
                $updateCheck = $updateCheck."; ";
            }
            $updateCheck = true;
            $updateQuery = $updateQuery."UPDATE `dealer_overall_record` SET `dealer_id`= '$dealer_id[$i]',`orders_given`= '$orders_given[$i]',`successful_orders`= '$successful_orders[$i]',`ordered_packets`= '$ordered_packets[$i]',`submitted_packets`= '$submitted_packets[$i]',`ordered_boxes`= '$ordered_boxes[$i]',`submitted_boxes`= '$submitted_boxes[$i]',`order_price`= '$order_price[$i]',`discount_price`= '$discount_price[$i]',`invoiced_price`= '$invoiced_price[$i]',`cash_collected`= '$cash_collected[$i]',`return_packets`= '$return_packets[$i]',`return_boxes`= '$return_boxes[$i]',`return_price`= '$return_price[$i]',`cash_return`= '$cash_return[$i]',`waived_off_price`= '$waived_off_price[$i]',`pending_payments`= '$pending_payments[$i]' WHERE `software_dealer_overall_id` = '$dealer_overall_id[$i]' AND `software_lic_id` = '$softwareId'";
        }
    }
//    echo $insertQuery.'<br>';
    if($insertCheck && mysqli_query($con, $insertQuery))
    {
        $insertSuccess = "true";
    }
//    echo $updateQuery.'<br>';
    if($updateCheck && mysqli_multi_query($con, $updateQuery))
    {
        $updateSuccess = "true";
    }

    echo "Insert:".$insertSuccess.":Update:".$updateSuccess;
}
elseif($operation == "sync_dealer_payments")
{
    $previousAddedIds = array();
    $sqlSelectIds = "SELECT `software_payment_id` FROM `dealer_payments` WHERE `software_lic_id` = '$softwareId'";
    $sqlSelectIds = mysqli_query($con, $sqlSelectIds);
    while($addedId = mysqli_fetch_array($sqlSelectIds))
    {
        array_push($previousAddedIds, $addedId[0]);
    }
    $payment_id = $_POST['payment_id'];
    $dealer_id = $_POST['dealer_id'];
    $order_id = $_POST['order_id'];
    $amount = $_POST['amount'];
    $cash_type = $_POST['cash_type'];
    $pending_payments = $_POST['pending_payments'];
    $date = $_POST['date'];
    $day = $_POST['day'];
    $user_id = $_POST['user_id'];
    $comments = $_POST['comments'];
    $status = $_POST['status'];
    $server_id = $_POST['server_id'];
    $server_sync = $_POST['server_sync'];

    $payment_id = explode("/-_/", $payment_id);
    $dealer_id = explode("/-_/", $dealer_id);
    $order_id = explode("/-_/", $order_id);
    $amount = explode("/-_/", $amount);
    $cash_type = explode("/-_/", $cash_type);
    $pending_payments = explode("/-_/", $pending_payments);
    $date = explode("/-_/", $date);
    $day = explode("/-_/", $day);
    $user_id = explode("/-_/", $user_id);
    $comments = explode("/-_/", $comments);
    $status = explode("/-_/", $status);
    $server_id = explode("/-_/", $server_id);
    $server_sync = explode("/-_/", $server_sync);

    $insertCheck = false;
    $updateCheck = false;
    $insertQuery = "INSERT INTO `dealer_payments`(`software_payment_id`, `dealer_id`, `order_id`, `amount`, `cash_type`, `pending_payments`, `date`, `day`, `user_id`, `comments`, `status`, `software_lic_id`) VALUES ";
    $updateQuery = "";
    $insertSuccess = "false";
    $updateSuccess = "false";
    for($i=0; $i<sizeof($payment_id); $i++)
    {
        if($server_sync[$i] == "Insert" || $server_sync[$i] == "0" || $server_sync[$i] == "")
        {
            if(!in_array($payment_id[$i], $previousAddedIds))
            {
                if($insertCheck)
                {
                    $insertQuery = $insertQuery.", ";
                }
                $insertCheck = true;
                $insertQuery = $insertQuery."('$payment_id[$i]', '$dealer_id[$i]', '$order_id[$i]', '$amount[$i]', '$cash_type[$i]', '$pending_payments[$i]', '$date[$i]', '$day[$i]', '$user_id[$i]', '$comments[$i]', '$status[$i]','$softwareId')";
            }

        }
        elseif ($server_sync[$i] == "Update")
        {
            if($updateCheck)
            {
                $updateCheck = $updateCheck."; ";
            }
            $updateCheck = true;
            $updateQuery = $updateQuery."UPDATE `dealer_payments` SET `dealer_id`= '$dealer_id[$i]',`order_id`= '$order_id[$i]',`amount`= '$amount[$i]',`cash_type`= '$cash_type[$i]',`pending_payments`= '$pending_payments[$i]',`date`= '$date[$i]',`day`= '$day[$i]',`user_id`= '$user_id[$i]',`comments`= '$comments[$i]',`status`= '$status[$i]' WHERE `software_payment_id` = '$payment_id[$i]' AND `software_lic_id` = '$softwareId'";
        }
    }
//    echo $insertQuery.'<br>';
    if($insertCheck && mysqli_query($con, $insertQuery))
    {
        $insertSuccess = "true";
    }
//    echo $updateQuery.'<br>';
    if($updateCheck && mysqli_multi_query($con, $updateQuery))
    {
        $updateSuccess = "true";
    }

    echo "Insert:".$insertSuccess.":Update:".$updateSuccess;
}
elseif($operation == "sync_dealer_types")
{
    $previousAddedIds = array();
    $sqlSelectIds = "SELECT `software_dealer_type_id` FROM `dealer_types` WHERE `software_lic_id` = '$softwareId'";
    $sqlSelectIds = mysqli_query($con, $sqlSelectIds);
    while($addedId = mysqli_fetch_array($sqlSelectIds))
    {
        array_push($previousAddedIds, $addedId[0]);
    }
    $dealer_type_ID = $_POST['dealer_type_ID'];
    $typeID = $_POST['typeID'];
    $dealerType_name = $_POST['dealerType_name'];
    $typeStatus = $_POST['typeStatus'];
    $DeleteStatus = $_POST['DeleteStatus'];
    $server_sync = $_POST['server_sync'];

    $dealer_type_ID = explode("/-_/", $dealer_type_ID);
    $typeID = explode("/-_/", $typeID);
    $dealerType_name = explode("/-_/", $dealerType_name);
    $typeStatus = explode("/-_/", $typeStatus);
    $DeleteStatus = explode("/-_/", $DeleteStatus);
    $server_sync = explode("/-_/", $server_sync);

    $insertCheck = false;
    $updateCheck = false;
    $insertQuery = "INSERT INTO `dealer_types`(`software_dealer_type_id`, `typeID`, `dealerType_name`, `typeStatus`, `DeleteStatus`, `software_lic_id`) VALUES ";
    $updateQuery = "";
    $insertSuccess = "false";
    $updateSuccess = "false";
    for($i=0; $i<sizeof($dealer_type_ID); $i++)
    {
        if($server_sync[$i] == "Insert" || $server_sync[$i] == "0" || $server_sync[$i] == "")
        {
            if(!in_array($dealer_type_ID[$i], $previousAddedIds))
            {
                if($insertCheck)
                {
                    $insertQuery = $insertQuery.", ";
                }
                $insertCheck = true;
                $insertQuery = $insertQuery."('$dealer_type_ID[$i]','$typeID[$i]','$dealerType_name[$i]','$typeStatus[$i]','$DeleteStatus[$i]','$softwareId')";
            }

        }
        elseif ($server_sync[$i] == "Update")
        {
            if($updateCheck)
            {
                $updateCheck = $updateCheck."; ";
            }
            $updateCheck = true;
            $updateQuery = $updateQuery."UPDATE `dealer_types` SET `typeID`= '$typeID[$i]',`dealerType_name`= '$dealerType_name[$i]',`typeStatus`= '$typeStatus[$i]',`DeleteStatus`= '$DeleteStatus[$i]' WHERE `software_dealer_type_id` = '$dealer_type_ID[$i]' AND `software_lic_id` = '$softwareId'";
        }
    }
//    echo $insertQuery.'<br>';
    if($insertCheck && mysqli_query($con, $insertQuery))
    {
        $insertSuccess = "true";
    }
//    echo $updateQuery.'<br>';
    if($updateCheck && mysqli_multi_query($con, $updateQuery))
    {
        $updateSuccess = "true";
    }

    echo "Insert:".$insertSuccess.":Update:".$updateSuccess;
}
elseif($operation == "sync_discount_policy")
{
    $previousAddedIds = array();
    $sqlSelectIds = "SELECT `software_discount_id` FROM `discount_policy` WHERE `software_lic_id` = '$softwareId'";
    $sqlSelectIds = mysqli_query($con, $sqlSelectIds);
    while($addedId = mysqli_fetch_array($sqlSelectIds))
    {
        array_push($previousAddedIds, $addedId[0]);
    }
    $disc_policyID = $_POST['disc_policyID'];
    $approval_id = $_POST['approval_id'];
    $dealer_table_id = $_POST['dealer_table_id'];
    $company_table_id = $_POST['company_table_id'];
    $product_table_id = $_POST['product_table_id'];
    $sale_amount = $_POST['sale_amount'];
    $discount_percent = $_POST['discount_percent'];
    $start_date = $_POST['start_date'];
    $end_date = $_POST['end_date'];
    $policy_status = $_POST['policy_status'];
    $entered_by = $_POST['entered_by'];
    $entry_date = $_POST['entry_date'];
    $entry_time = $_POST['entry_time'];
    $updated_by = $_POST['updated_by'];
    $update_date = $_POST['update_date'];
    $update_time = $_POST['update_time'];
    $server_sync = $_POST['server_sync'];

    $disc_policyID = explode("/-_/", $disc_policyID);
    $approval_id = explode("/-_/", $approval_id);
    $dealer_table_id = explode("/-_/", $dealer_table_id);
    $company_table_id = explode("/-_/", $company_table_id);
    $product_table_id = explode("/-_/", $product_table_id);
    $sale_amount = explode("/-_/", $sale_amount);
    $discount_percent = explode("/-_/", $discount_percent);
    $start_date = explode("/-_/", $start_date);
    $end_date = explode("/-_/", $end_date);
    $policy_status = explode("/-_/", $policy_status);
    $entered_by = explode("/-_/", $entered_by);
    $entry_date = explode("/-_/", $entry_date);
    $entry_time = explode("/-_/", $entry_time);
    $updated_by = explode("/-_/", $updated_by);
    $update_date = explode("/-_/", $update_date);
    $update_time = explode("/-_/", $update_time);
    $server_sync = explode("/-_/", $server_sync);

    $insertCheck = false;
    $updateCheck = false;
    $insertQuery = "INSERT INTO `discount_policy`(`software_discount_id`, `approval_id`, `dealer_table_id`, `company_table_id`, `product_table_id`, `sale_amount`, `discount_percent`, `start_date`, `end_date`, `policy_status`, `entered_by`, `entry_date`, `entry_time`, `updated_by`, `update_date`, `update_time`, `software_lic_id`) VALUES ";
    $updateQuery = "";
    $insertSuccess = "false";
    $updateSuccess = "false";
    for($i=0; $i<sizeof($disc_policyID); $i++)
    {
        if($server_sync[$i] == "Insert" || $server_sync[$i] == "0" || $server_sync[$i] == "")
        {
            if(!in_array($disc_policyID[$i], $previousAddedIds))
            {
                if($insertCheck)
                {
                    $insertQuery = $insertQuery.", ";
                }
                $insertCheck = true;
                $insertQuery = $insertQuery."('$disc_policyID[$i]', '$approval_id[$i]', '$dealer_table_id[$i]', '$company_table_id[$i]', '$product_table_id[$i]', '$sale_amount[$i]', '$discount_percent[$i]', '$start_date[$i]', '$end_date[$i]', '$policy_status[$i]', '$entered_by[$i]', '$entry_date[$i]', '$entry_time[$i]', '$updated_by[$i]', '$update_date[$i]', '$update_time[$i]','$softwareId')";
            }
        }
        elseif ($server_sync[$i] == "Update")
        {
            if($updateCheck)
            {
                $updateCheck = $updateCheck."; ";
            }
            $updateCheck = true;
            $updateQuery = $updateQuery."UPDATE `discount_policy` SET `approval_id`= '$approval_id[$i]',`dealer_table_id`= '$dealer_table_id[$i]',`company_table_id`= '$company_table_id[$i]',`product_table_id`= '$product_table_id[$i]',`sale_amount`= '$sale_amount[$i]',`discount_percent`= '$discount_percent[$i]',`start_date`= '$start_date[$i]',`end_date`= '$end_date[$i]',`policy_status`= '$policy_status[$i]',`entered_by`= '$entered_by[$i]',`entry_date`= '$entry_date[$i]',`entry_time`= '$entry_time[$i]',`updated_by`= '$updated_by[$i]',`update_date`= '$update_date[$i]',`update_time`= '$update_time[$i]' WHERE `software_discount_id` = '$disc_policyID[$i]' AND `software_lic_id` = '$softwareId'";
        }
    }
//    echo $insertQuery.'<br>';
    if($insertCheck && mysqli_query($con, $insertQuery))
    {
        $insertSuccess = "true";
    }
//    echo $updateQuery.'<br>';
    if($updateCheck && mysqli_multi_query($con, $updateQuery))
    {
        $updateSuccess = "true";
    }

    echo "Insert:".$insertSuccess.":Update:".$updateSuccess;
}
elseif($operation == "sync_district_info")
{
    $previousAddedIds = array();
    $sqlSelectIds = "SELECT `software_district_id` FROM `district_info` WHERE `software_lic_id` = '$softwareId'";
    $sqlSelectIds = mysqli_query($con, $sqlSelectIds);
    while($addedId = mysqli_fetch_array($sqlSelectIds))
    {
        array_push($previousAddedIds, $addedId[0]);
    }
    $district_table_id = $_POST['district_table_id'];
    $district_id = $_POST['district_id'];
    $district_name = $_POST['district_name'];
    $district_status = $_POST['district_status'];
    $creating_user_id = $_POST['creating_user_id'];
    $creating_date = $_POST['creating_date'];
    $creating_time = $_POST['creating_time'];
    $update_user_id = $_POST['update_user_id'];
    $update_date = $_POST['update_date'];
    $update_time = $_POST['update_time'];
    $server_sync = $_POST['server_sync'];

//    $district_table_id = "1";
//    $district_id = "0";
//    $district_name = "Gujrat";
//    $district_status = "Active";
//    $creating_user_id = "1";
//    $creating_date = "29/Jan/2022";
//    $creating_time = "01:53 AM";
//    $update_user_id = "1";
//    $update_date = "29/Jan/2022";
//    $update_time = "01:53 AM";
//    $server_sync = "0";

    $district_table_id = explode("/-_/", $district_table_id);
    $district_id = explode("/-_/", $district_id);
    $district_name = explode("/-_/", $district_name);
    $district_status = explode("/-_/", $district_status);
    $creating_user_id = explode("/-_/", $creating_user_id);
    $creating_date = explode("/-_/", $creating_date);
    $creating_time = explode("/-_/", $creating_time);
    $update_user_id = explode("/-_/", $update_user_id);
    $update_date = explode("/-_/", $update_date);
    $update_time = explode("/-_/", $update_time);
    $server_sync = explode("/-_/", $server_sync);

    $insertCheck = false;
    $updateCheck = false;
    $insertQuery = "INSERT INTO `district_info`(`software_district_id`, `district_id`, `district_name`, `district_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`, `software_lic_id`) VALUES ";
    $updateQuery = "";
    $insertSuccess = "false";
    $updateSuccess = "false";
//    echo "Size: ".sizeof($district_table_id).'<br>';
    for($i=0; $i<sizeof($district_table_id); $i++)
    {
        if($server_sync[$i] == "Insert" || $server_sync[$i] == "0" || $server_sync[$i] == "")
        {
            if(!in_array($district_table_id[$i], $previousAddedIds))
            {
                if($insertCheck)
                {
                    $insertQuery = $insertQuery.", ";
                }
                $insertCheck = true;
                $insertQuery = $insertQuery."('$district_table_id[$i]', '$district_id[$i]', '$district_name[$i]', '$district_status[$i]', '$creating_user_id[$i]', '$creating_date[$i]', '$creating_time[$i]', '$update_user_id[$i]', '$update_date[$i]', '$update_time[$i]','$softwareId')";

            }
        }
        elseif ($server_sync[$i] == "Update")
        {
            if($updateCheck)
            {
                $updateCheck = $updateCheck."; ";
            }
            $updateCheck = true;
            $updateQuery = $updateQuery."UPDATE `district_info` SET `district_id`=[value-3],`district_name`=[value-4],`district_status`=[value-5],`creating_user_id`=[value-6],`creating_date`=[value-7],`creating_time`=[value-8],`update_user_id`=[value-9],`update_date`=[value-10],`update_time`=[value-11] WHERE `software_district_id` = '$areaTableId[$i]' AND `software_lic_id` = '$softwareId'";
        }
    }
//    echo $insertQuery.'<br>';
    if($insertCheck && mysqli_query($con, $insertQuery))
    {
        $insertSuccess = "true";
    }
//    echo $updateQuery.'<br>';
    if($updateCheck && mysqli_multi_query($con, $updateQuery))
    {
        $updateSuccess = "true";
    }

    echo "Insert:".$insertSuccess.":Update:".$updateSuccess;
}
elseif($operation == "sync_groups_info")
{
    $previousAddedIds = array();
    $sqlSelectIds = "SELECT `software_group_id` FROM `groups_info` WHERE `software_lic_id` = '$softwareId'";
    $sqlSelectIds = mysqli_query($con, $sqlSelectIds);
    while($addedId = mysqli_fetch_array($sqlSelectIds))
    {
        array_push($previousAddedIds, $addedId[0]);
    }
    $group_table_id = $_POST['group_table_id'];
    $group_id = $_POST['group_id'];
    $company_id = $_POST['company_id'];
    $group_name = $_POST['group_name'];
    $group_status = $_POST['group_status'];
    $creating_user_id = $_POST['creating_user_id'];
    $creating_date = $_POST['creating_date'];
    $creating_time = $_POST['creating_time'];
    $update_user_id = $_POST['update_user_id'];
    $update_date = $_POST['update_date'];
    $update_time = $_POST['update_time'];
    $server_sync = $_POST['server_sync'];

    $group_table_id = explode("/-_/", $group_table_id);
    $group_id = explode("/-_/", $group_id);
    $company_id = explode("/-_/", $company_id);
    $group_name = explode("/-_/", $group_name);
    $group_status = explode("/-_/", $group_status);
    $creating_user_id = explode("/-_/", $creating_user_id);
    $creating_date = explode("/-_/", $creating_date);
    $creating_time = explode("/-_/", $creating_time);
    $update_user_id = explode("/-_/", $update_user_id);
    $update_date = explode("/-_/", $update_date);
    $update_time = explode("/-_/", $update_time);
    $server_sync = explode("/-_/", $server_sync);

    $insertCheck = false;
    $updateCheck = false;
    $insertQuery = "INSERT INTO `groups_info`(`software_group_id`, `group_id`, `company_id`, `group_name`, `group_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`, `software_lic_id`) VALUES ";
    $updateQuery = "";
    $insertSuccess = "false";
    $updateSuccess = "false";
    for($i=0; $i<sizeof($group_table_id); $i++)
    {
        if($server_sync[$i] == "Insert" || $server_sync[$i] == "0" || $server_sync[$i] == "")
        {
            if(!in_array($group_table_id[$i], $previousAddedIds))
            {
                if($insertCheck)
                {
                    $insertQuery = $insertQuery.", ";
                }
                $insertCheck = true;
                $insertQuery = $insertQuery."('$group_table_id[$i]', '$group_id[$i]', '$company_id[$i]', '$group_name[$i]', '$group_status[$i]', '$creating_user_id[$i]', '$creating_date[$i]', '$creating_time[$i]', '$update_user_id[$i]', '$update_date[$i]', '$update_time[$i]', '$softwareId')";

            }
        }
        elseif ($server_sync[$i] == "Update")
        {
            if($updateCheck)
            {
                $updateCheck = $updateCheck."; ";
            }
            $updateCheck = true;
            $updateQuery = $updateQuery."UPDATE `groups_info` SET `group_id`= '$group_id[$i]',`company_id`= '$company_id[$i]',`group_name`= '$group_name[$i]',`group_status`= '$group_status[$i]',`creating_user_id`= '$creating_user_id[$i]',`creating_date`= '$creating_date[$i]',`creating_time`= '$creating_time[$i]',`update_user_id`= '$update_user_id[$i]',`update_date`= '$update_date[$i]',`update_time`= '$update_time[$i]' WHERE `software_group_id` = '$group_table_id[$i]' AND `software_lic_id` = '$softwareId'";
        }
    }
//    echo $insertQuery.'<br>';
    if($insertCheck && mysqli_query($con, $insertQuery))
    {
        $insertSuccess = "true";
    }
//    echo $updateQuery.'<br>';
    if($updateCheck && mysqli_multi_query($con, $updateQuery))
    {
        $updateSuccess = "true";
    }

    echo "Insert:".$insertSuccess.":Update:".$updateSuccess;
}
elseif($operation == "sync_mails_record")
{
    $previousAddedIds = array();
    $sqlSelectIds = "SELECT `software_mail_id` FROM `mails_record` WHERE `software_lic_id` = '$softwareId'";
    $sqlSelectIds = mysqli_query($con, $sqlSelectIds);
    while($addedId = mysqli_fetch_array($sqlSelectIds))
    {
        array_push($previousAddedIds, $addedId[0]);
    }
    $mail_id = $_POST['mail_id'];
    $from_email = $_POST['from_email'];
    $to_email = $_POST['to_email'];
    $title = $_POST['title'];
    $message = $_POST['message'];
    $attachment = $_POST['attachment'];
    $user_id = $_POST['user_id'];
    $date = $_POST['date'];
    $time = $_POST['time'];
    $status = $_POST['status'];
    $server_sync = $_POST['server_sync'];

    $mail_id = explode("/-_/", $mail_id);
    $from_email = explode("/-_/", $from_email);
    $to_email = explode("/-_/", $to_email);
    $title = explode("/-_/", $title);
    $message = explode("/-_/", $message);
    $attachment = explode("/-_/", $attachment);
    $user_id = explode("/-_/", $user_id);
    $date = explode("/-_/", $date);
    $time = explode("/-_/", $time);
    $status = explode("/-_/", $status);
    $server_sync = explode("/-_/", $server_sync);

    $insertCheck = false;
    $updateCheck = false;
    $insertQuery = "INSERT INTO `mails_record`(`software_mail_id`, `from_email`, `to_email`, `title`, `message`, `attachment`, `user_id`, `date`, `time`, `status`, `software_lic_id`) VALUES ";
    $updateQuery = "";
    $insertSuccess = "false";
    $updateSuccess = "false";
    for($i=0; $i<sizeof($mail_id); $i++)
    {
        if($server_sync[$i] == "Insert" || $server_sync[$i] == "0" || $server_sync[$i] == "")
        {
            if(!in_array($mail_id[$i], $previousAddedIds))
            {
                if($insertCheck)
                {
                    $insertQuery = $insertQuery.", ";
                }
                $insertCheck = true;
                $insertQuery = $insertQuery."('$mail_id[$i]', '$from_email[$i]', '$to_email[$i]', '$title[$i]', '$message[$i]', '$attachment[$i]', '$user_id[$i]', '$date[$i]', '$time[$i]', '$status[$i]', '$softwareId')";

            }
        }
        elseif ($server_sync[$i] == "Update")
        {
            if($updateCheck)
            {
                $updateCheck = $updateCheck."; ";
            }
            $updateCheck = true;
            $updateQuery = $updateQuery."UPDATE `mails_record` SET `from_email`= '$from_email[$i]',`to_email`= '$to_email[$i]',`title`= '$title[$i]',`message`= '$message[$i]',`attachment`= '$attachment[$i]',`user_id`= '$user_id[$i]',`date`= '$date[$i]',`time`= '$time[$i]',`status`= '$status[$i]' WHERE `software_mail_id` = '$mail_id[$i]' AND `software_lic_id` = '$softwareId'";
        }
    }
//    echo $insertQuery.'<br>';
    if($insertCheck && mysqli_query($con, $insertQuery))
    {
        $insertSuccess = "true";
    }
//    echo $updateQuery.'<br>';
    if($updateCheck && mysqli_multi_query($con, $updateQuery))
    {
        $updateSuccess = "true";
    }

    echo "Insert:".$insertSuccess.":Update:".$updateSuccess;
}
elseif($operation == "sync_order_info")
{
    $previousAddedIds = array();
    $sqlSelectIds = "SELECT `software_order_id` FROM `order_info` WHERE `software_lic_id` = '$softwareId'";
    $sqlSelectIds = mysqli_query($con, $sqlSelectIds);
    while($addedId = mysqli_fetch_array($sqlSelectIds))
    {
        array_push($previousAddedIds, $addedId[0]);
    }
    $order_id = $_POST['order_id'];
    $dealer_id = $_POST['dealer_id'];
    $product_id = $_POST['product_id'];
    $quantity = $_POST['quantity'];
    $unit = $_POST['unit'];
    $order_price = $_POST['order_price'];
    $bonus = $_POST['bonus'];
    $discount = $_POST['discount'];
    $waive_off_price = $_POST['waive_off_price'];
    $final_price = $_POST['final_price'];
    $order_success = $_POST['order_success'];
    $booking_latitude = $_POST['booking_latitude'];
    $booking_longitude = $_POST['booking_longitude'];
    $booking_area = $_POST['booking_area'];
    $booking_date = $_POST['booking_date'];
    $booking_time = $_POST['booking_time'];
    $booking_user_id = $_POST['booking_user_id'];
    $delivered_latitude = $_POST['delivered_latitude'];
    $delivered_longitude = $_POST['delivered_longitude'];
    $delivered_area = $_POST['delivered_area'];
    $delivered_date = $_POST['delivered_date'];
    $delivered_time = $_POST['delivered_time'];
    $delivered_user_id = $_POST['delivered_user_id'];
    $update_dates = $_POST['update_dates'];
    $update_times = $_POST['update_times'];
    $update_user_id = $_POST['update_user_id'];
    $comments = $_POST['comments'];
    $status = $_POST['status'];
    $server_sync = $_POST['server_sync'];

    $order_id = explode("/-_/", $order_id);
    $dealer_id = explode("/-_/", $dealer_id);
    $product_id = explode("/-_/", $product_id);
    $quantity = explode("/-_/", $quantity);
    $unit = explode("/-_/", $unit);
    $order_price = explode("/-_/", $order_price);
    $bonus = explode("/-_/", $bonus);
    $discount = explode("/-_/", $discount);
    $waive_off_price = explode("/-_/", $waive_off_price);
    $final_price = explode("/-_/", $final_price);
    $order_success = explode("/-_/", $order_success);
    $booking_latitude = explode("/-_/", $booking_latitude);
    $booking_longitude = explode("/-_/", $booking_longitude);
    $booking_area = explode("/-_/", $booking_area);
    $booking_date = explode("/-_/", $booking_date);
    $booking_time = explode("/-_/", $booking_time);
    $booking_user_id = explode("/-_/", $booking_user_id);
    $delivered_latitude = explode("/-_/", $delivered_latitude);
    $delivered_longitude = explode("/-_/", $delivered_longitude);
    $delivered_area = explode("/-_/", $delivered_area);
    $delivered_date = explode("/-_/", $delivered_date);
    $delivered_time = explode("/-_/", $delivered_time);
    $delivered_user_id = explode("/-_/", $delivered_user_id);
    $update_dates = explode("/-_/", $update_dates);
    $update_times = explode("/-_/", $update_times);
    $update_user_id = explode("/-_/", $update_user_id);
    $comments = explode("/-_/", $comments);
    $status = explode("/-_/", $status);
    $server_sync = explode("/-_/", $server_sync);

    $insertCheck = false;
    $updateCheck = false;
    $insertQuery = "INSERT INTO `order_info`(`software_order_id`, `dealer_id`, `product_id`, `quantity`, `unit`, `order_price`, `bonus`, `discount`, `waive_off_price`, `final_price`, `order_success`, `booking_latitude`, `booking_longitude`, `booking_area`, `booking_date`, `booking_time`, `booking_user_id`, `delivered_latitude`, `delivered_longitude`, `delivered_area`, `delivered_date`, `delivered_time`, `delivered_user_id`, `update_dates`, `update_times`, `update_user_id`, `comments`, `status`, `software_lic_id`) VALUES ";
    $updateQuery = "";
    $insertSuccess = "false";
    $updateSuccess = "false";
    for($i=0; $i<sizeof($order_id); $i++)
    {
        if($server_sync[$i] == "Insert" || $server_sync[$i] == "0" || $server_sync[$i] == "")
        {
            if(!in_array($order_id[$i], $previousAddedIds))
            {
                if($insertCheck)
                {
                    $insertQuery = $insertQuery.", ";
                }
                $insertCheck = true;
                $insertQuery = $insertQuery."('$order_id[$i]', '$dealer_id[$i]', '$product_id[$i]', '$quantity[$i]', '$unit[$i]', '$order_price[$i]', '$bonus[$i]', '$discount[$i]', '$waive_off_price[$i]', '$final_price[$i]', '$order_success[$i]', '$booking_latitude[$i]', '$booking_longitude[$i]', '$booking_area[$i]', '$booking_date[$i]', '$booking_time[$i]', '$booking_user_id[$i]', '$delivered_latitude[$i]', '$delivered_longitude[$i]', '$delivered_area[$i]', '$delivered_date[$i]', '$delivered_time[$i]', '$delivered_user_id[$i]', '$update_dates[$i]', '$update_times[$i]', '$update_user_id[$i]', '$comments[$i]', '$status[$i]', '$softwareId')";

            }
        }
        elseif ($server_sync[$i] == "Update")
        {
            if($updateCheck)
            {
                $updateCheck = $updateCheck."; ";
            }
            $updateCheck = true;
            $updateQuery = $updateQuery."UPDATE `order_info` SET `dealer_id`= '$dealer_id[$i]',`product_id`= '$product_id[$i]',`quantity`= '$quantity[$i]',`unit`= '$unit[$i]',`order_price`= '$order_price[$i]',`bonus`= '$bonus[$i]',`discount`= '$discount[$i]',`waive_off_price`= '$waive_off_price[$i]',`final_price`= '$final_price[$i]',`order_success`= '$order_success[$i]',`booking_latitude`= '$booking_latitude[$i]',`booking_longitude`= '$booking_longitude[$i]',`booking_area`= '$booking_area[$i]',`booking_date`= '$booking_date[$i]',`booking_time`= '$booking_time[$i]',`booking_user_id`= '$booking_user_id[$i]',`delivered_latitude`= '$delivered_latitude[$i]',`delivered_longitude`= '$delivered_longitude[$i]',`delivered_area`= '$delivered_area[$i]',`delivered_date`= '$delivered_date[$i]',`delivered_time`= '$delivered_time[$i]',`delivered_user_id`= '$delivered_user_id[$i]',`update_dates`= '$update_dates[$i]',`update_times`= '$update_times[$i]',`update_user_id`= '$update_user_id[$i]',`comments`= '$comments[$i]', `status`= '$status[$i]' WHERE `software_order_id` = '$order_id[$i]' AND `software_lic_id` = '$softwareId'";
        }
    }
//    echo $insertQuery.'<br>';
    if($insertCheck && mysqli_query($con, $insertQuery))
    {
        $insertSuccess = "true";
    }
//    echo $updateQuery.'<br>';
    if($updateCheck && mysqli_multi_query($con, $updateQuery))
    {
        $updateSuccess = "true";
    }

    echo "Insert:".$insertSuccess.":Update:".$updateSuccess;
}
elseif($operation == "sync_order_info_detailed")
{
    $previousAddedIds = array();
    $sqlSelectIds = "SELECT `software_detail_id` FROM `order_info_detailed` WHERE `software_lic_id` = '$softwareId'";
    $sqlSelectIds = mysqli_query($con, $sqlSelectIds);
    while($addedId = mysqli_fetch_array($sqlSelectIds))
    {
        array_push($previousAddedIds, $addedId[0]);
    }
    $detail_id = $_POST['detail_id'];
    $order_id = $_POST['order_id'];
    $product_table_id = $_POST['product_table_id'];
    $batch_id = $_POST['batch_id'];
    $quantity = $_POST['quantity'];
    $submission_quantity = $_POST['submission_quantity'];
    $unit = $_POST['unit'];
    $submission_unit = $_POST['submission_unit'];
    $order_price = $_POST['order_price'];
    $bonus_quant = $_POST['bonus_quant'];
    $discount = $_POST['discount'];
    $final_price = $_POST['final_price'];
    $cmplt_returned_bit = $_POST['cmplt_returned_bit'];
    $returned_quant = $_POST['returned_quant'];
    $returned_bonus_quant = $_POST['returned_bonus_quant'];
    $server_sync = $_POST['server_sync'];

    $detail_id = explode("/-_/", $detail_id);
    $order_id = explode("/-_/", $order_id);
    $product_table_id = explode("/-_/", $product_table_id);
    $batch_id = explode("/-_/", $batch_id);
    $quantity = explode("/-_/", $quantity);
    $submission_quantity = explode("/-_/", $submission_quantity);
    $unit = explode("/-_/", $unit);
    $submission_unit = explode("/-_/", $submission_unit);
    $order_price = explode("/-_/", $order_price);
    $bonus_quant = explode("/-_/", $bonus_quant);
    $discount = explode("/-_/", $discount);
    $final_price = explode("/-_/", $final_price);
    $cmplt_returned_bit = explode("/-_/", $cmplt_returned_bit);
    $returned_quant = explode("/-_/", $returned_quant);
    $returned_bonus_quant = explode("/-_/", $returned_bonus_quant);
    $server_sync = explode("/-_/", $server_sync);

    $insertCheck = false;
    $updateCheck = false;
    $insertQuery = "INSERT INTO `order_info_detailed`(`software_detail_id`, `software_order_id`, `order_id`, `product_table_id`, `batch_id`, `quantity`, `submission_quantity`, `unit`, `submission_unit`, `order_price`, `bonus_quant`, `discount`, `final_price`, `cmplt_returned_bit`, `returned_quant`, `returned_bonus_quant`, `software_lic_id`) VALUES ";
    $updateQuery = "";
    $insertSuccess = "false";
    $updateSuccess = "false";
    for($i=0; $i<sizeof($detail_id); $i++)
    {
        if($server_sync[$i] == "Insert" || $server_sync[$i] == "0" || $server_sync[$i] == "")
        {
            if(!in_array($detail_id[$i], $previousAddedIds))
            {
                if($insertCheck)
                {
                    $insertQuery = $insertQuery.", ";
                }
                $insertCheck = true;
                $insertQuery = $insertQuery."('$detail_id[$i]','$order_id[$i]', '$order_id[$i]', '$product_table_id[$i]', '$batch_id[$i]', '$quantity[$i]', '$submission_quantity[$i]', '$unit[$i]', '$submission_unit[$i]', '$order_price[$i]', '$bonus_quant[$i]', '$discount[$i]', '$final_price[$i]', '$cmplt_returned_bit[$i]', '$returned_quant[$i]', '$returned_bonus_quant[$i]','$softwareId')";

            }
        }
        elseif ($server_sync[$i] == "Update")
        {
            if($updateCheck)
            {
                $updateCheck = $updateCheck."; ";
            }
            $updateCheck = true;
            $updateQuery = $updateQuery."UPDATE `order_info_detailed` SET `software_order_id`= '$order_id[$i]',`order_id`= '$order_id[$i]',`product_table_id`= '$product_table_id[$i]',`batch_id`= '$batch_id[$i]',`quantity`= '$quantity[$i]',`submission_quantity`= '$submission_quantity[$i]',`unit`= '$unit[$i]',`submission_unit`= '$submission_unit[$i]',`order_price`= '$order_price[$i]',`bonus_quant`= '$bonus_quant[$i]',`discount`= '$discount[$i]',`final_price`= '$final_price[$i]',`cmplt_returned_bit`= '$cmplt_returned_bit[$i]',`returned_quant`= '$returned_quant[$i]',`returned_bonus_quant`= '$returned_bonus_quant[$i]' WHERE `software_detail_id` = '$detail_id[$i]' AND `software_lic_id` = '$softwareId'";
        }
    }
//    echo $insertQuery.'<br>';
    if($insertCheck && mysqli_query($con, $insertQuery))
    {
        $insertSuccess = "true";
    }
//    echo $updateQuery.'<br>';
    if($updateCheck && mysqli_multi_query($con, $updateQuery))
    {
        $updateSuccess = "true";
    }

    echo "Insert:".$insertSuccess.":Update:".$updateSuccess;
}
elseif($operation == "sync_order_return")
{
    $previousAddedIds = array();
    $sqlSelectIds = "SELECT `software_return_id` FROM `order_return` WHERE `software_lic_id` = '$softwareId'";
    $sqlSelectIds = mysqli_query($con, $sqlSelectIds);
    while($addedId = mysqli_fetch_array($sqlSelectIds))
    {
        array_push($previousAddedIds, $addedId[0]);
    }
    $return_id = $_POST['return_id'];
    $dealer_id = $_POST['dealer_id'];
    $return_total_price = $_POST['return_total_price'];
    $return_gross_price = $_POST['return_gross_price'];
    $order_id = $_POST['order_id'];
    $entry_date = $_POST['entry_date'];
    $entry_time = $_POST['entry_time'];
    $enter_user_id = $_POST['enter_user_id'];
    $update_date = $_POST['update_date'];
    $update_time = $_POST['update_time'];
    $update_user_id = $_POST['update_user_id'];
    $comments = $_POST['comments'];
    $status = $_POST['status'];
    $server_sync = $_POST['server_sync'];

    $return_id = explode("/-_/", $return_id);
    $dealer_id = explode("/-_/", $dealer_id);
    $return_total_price = explode("/-_/", $return_total_price);
    $return_gross_price = explode("/-_/", $return_gross_price);
    $order_id = explode("/-_/", $order_id);
    $entry_date = explode("/-_/", $entry_date);
    $entry_time = explode("/-_/", $entry_time);
    $enter_user_id = explode("/-_/", $enter_user_id);
    $update_date = explode("/-_/", $update_date);
    $update_time = explode("/-_/", $update_time);
    $update_user_id = explode("/-_/", $update_user_id);
    $comments = explode("/-_/", $comments);
    $status = explode("/-_/", $status);
    $server_sync = explode("/-_/", $server_sync);

    $insertCheck = false;
    $updateCheck = false;
    $insertQuery = "INSERT INTO `order_return`(`software_return_id`, `dealer_id`, `return_total_price`, `return_gross_price`, `order_id`, `entry_date`, `entry_time`, `enter_user_id`, `update_date`, `update_time`, `update_user_id`, `comments`, `status`, `software_lic_id`) VALUES ";
    $updateQuery = "";
    $insertSuccess = "false";
    $updateSuccess = "false";
    for($i=0; $i<sizeof($return_id); $i++)
    {
        if($server_sync[$i] == "Insert" || $server_sync[$i] == "0" || $server_sync[$i] == "")
        {
            if(!in_array($return_id[$i], $previousAddedIds))
            {
                if($insertCheck)
                {
                    $insertQuery = $insertQuery.", ";
                }
                $insertCheck = true;
                $insertQuery = $insertQuery."('$return_id[$i]', '$dealer_id[$i]', '$return_total_price[$i]', '$return_gross_price[$i]', '$order_id[$i]', '$entry_date[$i]', '$entry_time[$i]', '$enter_user_id[$i]', '$update_date[$i]', '$update_time[$i]', '$update_user_id[$i]', '$comments[$i]', '$status[$i]','$softwareId')";

            }
        }
        elseif ($server_sync[$i] == "Update")
        {
            if($updateCheck)
            {
                $updateCheck = $updateCheck."; ";
            }
            $updateCheck = true;
            $updateQuery = $updateQuery."UPDATE `order_return` SET `dealer_id`= '$dealer_id[$i]',`return_total_price`= '$return_total_price[$i]',`return_gross_price`= '$return_gross_price[$i]',`order_id`= '$order_id[$i]',`entry_date`= '$entry_date[$i]',`entry_time`= '$entry_time[$i]',`enter_user_id`= '$enter_user_id[$i]',`update_date`= '$update_date[$i]',`update_time`= '$update_time[$i]',`update_user_id`= '$update_user_id[$i]',`comments`= '$comments[$i]',`status`= '$status[$i]' WHERE `software_return_id` = '$return_id[$i]' AND `software_lic_id` = '$softwareId'";
        }
    }
//    echo $insertQuery.'<br>';
    if($insertCheck && mysqli_query($con, $insertQuery))
    {
        $insertSuccess = "true";
    }
//    echo $updateQuery.'<br>';
    if($updateCheck && mysqli_multi_query($con, $updateQuery))
    {
        $updateSuccess = "true";
    }

    echo "Insert:".$insertSuccess.":Update:".$updateSuccess;
}
elseif($operation == "sync_order_return_detail")
{
    $previousAddedIds = array();
    $sqlSelectIds = "SELECT `software_return_detail_id` FROM `order_return_detail` WHERE `software_lic_id` = '$softwareId'";
    $sqlSelectIds = mysqli_query($con, $sqlSelectIds);
    while($addedId = mysqli_fetch_array($sqlSelectIds))
    {
        array_push($previousAddedIds, $addedId[0]);
    }
    $return_detail_id = $_POST['return_detail_id'];
    $return_id = $_POST['return_id'];
    $prod_id = $_POST['prod_id'];
    $prod_batch = $_POST['prod_batch'];
    $prod_quant = $_POST['prod_quant'];
    $bonus_quant = $_POST['bonus_quant'];
    $discount_amount = $_POST['discount_amount'];
    $total_amount = $_POST['total_amount'];
    $unit = $_POST['unit'];
    $return_reason = $_POST['return_reason'];
    $server_sync = $_POST['server_sync'];

    $return_detail_id = explode("/-_/", $return_detail_id);
    $return_id = explode("/-_/", $return_id);
    $prod_id = explode("/-_/", $prod_id);
    $prod_batch = explode("/-_/", $prod_batch);
    $prod_quant = explode("/-_/", $prod_quant);
    $bonus_quant = explode("/-_/", $bonus_quant);
    $discount_amount = explode("/-_/", $discount_amount);
    $total_amount = explode("/-_/", $total_amount);
    $unit = explode("/-_/", $unit);
    $return_reason = explode("/-_/", $return_reason);
    $server_sync = explode("/-_/", $server_sync);

    $insertCheck = false;
    $updateCheck = false;
    $insertQuery = "INSERT INTO `order_return_detail`(`software_return_detail_id`, `return_id`, `prod_id`, `prod_batch`, `prod_quant`, `bonus_quant`, `discount_amount`, `total_amount`, `unit`, `return_reason`, `software_lic_id`) VALUES ";
    $updateQuery = "";
    $insertSuccess = "false";
    $updateSuccess = "false";
    for($i=0; $i<sizeof($return_detail_id); $i++)
    {
        if($server_sync[$i] == "Insert" || $server_sync[$i] == "0" || $server_sync[$i] == "")
        {
            if(!in_array($return_detail_id[$i], $previousAddedIds))
            {
                if($insertCheck)
                {
                    $insertQuery = $insertQuery.", ";
                }
                $insertCheck = true;
                $insertQuery = $insertQuery."('$return_detail_id[$i]', '$return_id[$i]', '$prod_id[$i]', '$prod_batch[$i]', '$prod_quant[$i]', '$bonus_quant[$i]', '$discount_amount[$i]', '$total_amount[$i]', '$unit[$i]', '$return_reason[$i]','$softwareId')";

            }
        }
        elseif ($server_sync[$i] == "Update")
        {
            if($updateCheck)
            {
                $updateCheck = $updateCheck."; ";
            }
            $updateCheck = true;
            $updateQuery = $updateQuery."UPDATE `order_return_detail` SET `return_id`= '$return_id[$i]',`prod_id`= '$prod_id[$i]',`prod_batch`= '$prod_batch[$i]',`prod_quant`= '$prod_quant[$i]',`bonus_quant`= '$bonus_quant[$i]',`discount_amount`= '$discount_amount[$i]',`total_amount`= '$total_amount[$i]',`unit`= '$unit[$i]',`return_reason`= '$return_reason[$i]' WHERE `software_return_detail_id` = '$return_detail_id[$i]' AND `software_lic_id` = '$softwareId'";
        }
    }
//    echo $insertQuery.'<br>';
    if($insertCheck && mysqli_query($con, $insertQuery))
    {
        $insertSuccess = "true";
    }
//    echo $updateQuery.'<br>';
    if($updateCheck && mysqli_multi_query($con, $updateQuery))
    {
        $updateSuccess = "true";
    }

    echo "Insert:".$insertSuccess.":Update:".$updateSuccess;
}
elseif($operation == "sync_order_status_info")
{
    $previousAddedIds = array();
    $sqlSelectIds = "SELECT `software_order_id` FROM `order_status_info` WHERE `software_lic_id` = '$softwareId'";
    $sqlSelectIds = mysqli_query($con, $sqlSelectIds);
    while($addedId = mysqli_fetch_array($sqlSelectIds))
    {
        array_push($previousAddedIds, $addedId[0]);
    }
    $order_status_id = $_POST['order_status_id'];
    $order_id = $_POST['order_id'];
    $booking_user_id = $_POST['booking_user_id'];
    $booking_date = $_POST['booking_date'];
    $booking_latitude = $_POST['booking_latitude'];
    $booking_longitude = $_POST['booking_longitude'];
    $booking_area = $_POST['booking_area'];
    $delivered_user_id = $_POST['delivered_user_id'];
    $delivered_date = $_POST['delivered_date'];
    $delivered_latitude = $_POST['delivered_latitude'];
    $delivered_longitude = $_POST['delivered_longitude'];
    $delivered_area = $_POST['delivered_area'];
    $server_sync = $_POST['server_sync'];

    $order_status_id = explode("/-_/", $order_status_id);
    $order_id = explode("/-_/", $order_id);
    $booking_user_id = explode("/-_/", $booking_user_id);
    $booking_date = explode("/-_/", $booking_date);
    $booking_latitude = explode("/-_/", $booking_latitude);
    $booking_longitude = explode("/-_/", $booking_longitude);
    $booking_area = explode("/-_/", $booking_area);
    $delivered_user_id = explode("/-_/", $delivered_user_id);
    $delivered_date = explode("/-_/", $delivered_date);
    $delivered_latitude = explode("/-_/", $delivered_latitude);
    $delivered_longitude = explode("/-_/", $delivered_longitude);
    $delivered_area = explode("/-_/", $delivered_area);
    $server_sync = explode("/-_/", $server_sync);

    $insertCheck = false;
    $updateCheck = false;
    $insertQuery = "INSERT INTO `order_status_info`(`software_order_id`, `order_id`, `booking_user_id`, `booking_date`, `booking_latitude`, `booking_longitude`, `booking_area`, `delivered_user_id`, `delivered_date`, `delivered_latitude`, `delivered_longitude`, `delivered_area`, `software_lic_id`) VALUES ";
    $updateQuery = "";
    $insertSuccess = "false";
    $updateSuccess = "false";
    for($i=0; $i<sizeof($order_status_id); $i++)
    {
        if($server_sync[$i] == "Insert" || $server_sync[$i] == "0" || $server_sync[$i] == "")
        {
            if(!in_array($order_status_id[$i], $previousAddedIds))
            {
                if($insertCheck)
                {
                    $insertQuery = $insertQuery.", ";
                }
                $insertCheck = true;
                $insertQuery = $insertQuery."('$order_status_id[$i]', '$order_id[$i]', '$booking_user_id[$i]', '$booking_date[$i]', '$booking_latitude[$i]', '$booking_longitude[$i]', '$booking_area[$i]', '$delivered_user_id[$i]', '$delivered_date[$i]', '$delivered_latitude[$i]', '$delivered_longitude[$i]', '$delivered_area[$i]','$softwareId')";

            }
        }
        elseif ($server_sync[$i] == "Update")
        {
            if($updateCheck)
            {
                $updateCheck = $updateCheck."; ";
            }
            $updateCheck = true;
            $updateQuery = $updateQuery."UPDATE `order_status_info` SET `order_id`= '$order_id[$i]',`booking_user_id`= '$booking_user_id[$i]',`booking_date`= '$booking_date[$i]',`booking_latitude`= '$booking_latitude[$i]',`booking_longitude`= '$booking_longitude[$i]',`booking_area`= '$booking_area[$i]',`delivered_user_id`= '$delivered_user_id[$i]',`delivered_date`= '$delivered_date[$i]',`delivered_latitude`= '$delivered_latitude[$i]',`delivered_longitude`= '$delivered_longitude[$i]',`delivered_area`= '$delivered_area[$i]' WHERE `software_order_id` = '$order_status_id[$i]' AND `software_lic_id` = '$softwareId'";
        }
    }
//    echo $insertQuery.'<br>';
    if($insertCheck && mysqli_query($con, $insertQuery))
    {
        $insertSuccess = "true";
    }
//    echo $updateQuery.'<br>';
    if($updateCheck && mysqli_multi_query($con, $updateQuery))
    {
        $updateSuccess = "true";
    }

    echo "Insert:".$insertSuccess.":Update:".$updateSuccess;
}
elseif($operation == "sync_products_stock")
{
    $previousAddedIds = array();
    $sqlSelectIds = "SELECT `software_product_stock_id` FROM `products_stock` WHERE `software_lic_id` = '$softwareId'";
    $sqlSelectIds = mysqli_query($con, $sqlSelectIds);
    while($addedId = mysqli_fetch_array($sqlSelectIds))
    {
        array_push($previousAddedIds, $addedId[0]);
    }
    $ProdStockID = $_POST['ProdStockID'];
    $product_table_id = $_POST['product_table_id'];
    $in_stock = $_POST['in_stock'];
    $min_stock = $_POST['min_stock'];
    $max_stock = $_POST['max_stock'];
    $bonus_quant = $_POST['bonus_quant'];
    $status = $_POST['status'];
    $server_sync = $_POST['server_sync'];

    $ProdStockID = explode("/-_/", $ProdStockID);
    $product_table_id = explode("/-_/", $product_table_id);
    $in_stock = explode("/-_/", $in_stock);
    $min_stock = explode("/-_/", $min_stock);
    $max_stock = explode("/-_/", $max_stock);
    $bonus_quant = explode("/-_/", $bonus_quant);
    $status = explode("/-_/", $status);
    $server_sync = explode("/-_/", $server_sync);

    $insertCheck = false;
    $updateCheck = false;
    $insertQuery = "INSERT INTO `products_stock`(`software_product_stock_id`, `product_table_id`, `in_stock`, `min_stock`, `max_stock`, `bonus_quant`, `status`, `software_lic_id`) VALUES ";
    $updateQuery = "";
    $insertSuccess = "false";
    $updateSuccess = "false";
    for($i=0; $i<sizeof($ProdStockID); $i++)
    {
        if($server_sync[$i] == "Insert" || $server_sync[$i] == "0" || $server_sync[$i] == "")
        {
            if(!in_array($ProdStockID[$i], $previousAddedIds))
            {
                if($insertCheck)
                {
                    $insertQuery = $insertQuery.", ";
                }
                $insertCheck = true;
                $insertQuery = $insertQuery."('$ProdStockID[$i]','$product_table_id[$i]','$in_stock[$i]','$min_stock[$i]','$max_stock[$i]','$bonus_quant[$i]','$status[$i]','$softwareId')";

            }
        }
        elseif ($server_sync[$i] == "Update")
        {
            if($updateCheck)
            {
                $updateCheck = $updateCheck."; ";
            }
            $updateCheck = true;
            $updateQuery = $updateQuery."UPDATE `products_stock` SET `product_table_id`= '$product_table_id[$i]',`in_stock`= '$in_stock[$i]',`min_stock`= '$min_stock[$i]',`max_stock`= '$max_stock[$i]',`bonus_quant`= '$bonus_quant[$i]',`status`=[value-8] WHERE `software_product_stock_id` = '$ProdStockID[$i]' AND `software_lic_id` = '$softwareId'";
        }
    }
//    echo $insertQuery.'<br>';
    if($insertCheck && mysqli_query($con, $insertQuery))
    {
        $insertSuccess = "true";
    }
//    echo $updateQuery.'<br>';
    if($updateCheck && mysqli_multi_query($con, $updateQuery))
    {
        $updateSuccess = "true";
    }

    echo "Insert:".$insertSuccess.":Update:".$updateSuccess;
}
elseif($operation == "sync_product_info")
{
    $previousAddedIds = array();
    $sqlSelectIds = "SELECT `software_product_id` FROM `product_info` WHERE `software_lic_id` = '$softwareId'";
    $sqlSelectIds = mysqli_query($con, $sqlSelectIds);
    while($addedId = mysqli_fetch_array($sqlSelectIds))
    {
        array_push($previousAddedIds, $addedId[0]);
    }
    $product_table_id = $_POST['product_table_id'];
    $product_id = $_POST['product_id'];
    $company_table_id = $_POST['company_table_id'];
    $group_table_id = $_POST['group_table_id'];
    $report_prodID = $_POST['report_prodID'];
    $old_prodID = $_POST['old_prodID'];
    $product_name = $_POST['product_name'];
    $product_type = $_POST['product_type'];
    $packSize = $_POST['packSize'];
    $carton_size = $_POST['carton_size'];
    $retail_price = $_POST['retail_price'];
    $trade_price = $_POST['trade_price'];
    $purchase_price = $_POST['purchase_price'];
    $purchase_discount = $_POST['purchase_discount'];
    $sales_tax = $_POST['sales_tax'];
    $hold_stock = $_POST['hold_stock'];
    $product_status = $_POST['product_status'];
    $creating_user_id = $_POST['creating_user_id'];
    $creating_date = $_POST['creating_date'];
    $creating_time = $_POST['creating_time'];
    $update_user_id = $_POST['update_user_id'];
    $update_date = $_POST['update_date'];
    $update_time = $_POST['update_time'];
    $server_sync = $_POST['server_sync'];

    $product_table_id = explode("/-_/", $product_table_id);
    $product_id = explode("/-_/", $product_id);
    $company_table_id = explode("/-_/", $company_table_id);
    $group_table_id = explode("/-_/", $group_table_id);
    $report_prodID = explode("/-_/", $report_prodID);
    $old_prodID = explode("/-_/", $old_prodID);
    $product_name = explode("/-_/", $product_name);
    $product_type = explode("/-_/", $product_type);
    $packSize = explode("/-_/", $packSize);
    $carton_size = explode("/-_/", $carton_size);
    $retail_price = explode("/-_/", $retail_price);
    $trade_price = explode("/-_/", $trade_price);
    $purchase_price = explode("/-_/", $purchase_price);
    $purchase_discount = explode("/-_/", $purchase_discount);
    $sales_tax = explode("/-_/", $sales_tax);
    $hold_stock = explode("/-_/", $hold_stock);
    $product_status = explode("/-_/", $product_status);
    $creating_user_id = explode("/-_/", $creating_user_id);
    $creating_date = explode("/-_/", $creating_date);
    $creating_time = explode("/-_/", $creating_time);
    $update_user_id = explode("/-_/", $update_user_id);
    $update_date = explode("/-_/", $update_date);
    $update_time = explode("/-_/", $update_time);
    $server_sync = explode("/-_/", $server_sync);

    $insertCheck = false;
    $updateCheck = false;
    $insertQuery = "INSERT INTO `product_info`(`software_product_id`, `product_id`, `company_table_id`, `group_table_id`, `report_prodID`, `old_prodID`, `product_name`, `product_type`, `packSize`, `carton_size`, `retail_price`, `trade_price`, `purchase_price`, `purchase_discount`, `sales_tax`, `hold_stock`, `product_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`, `software_lic_id`) VALUES ";
    $updateQuery = "";
    $insertSuccess = "false";
    $updateSuccess = "false";
    for($i=0; $i<sizeof($product_table_id); $i++)
    {
        if($server_sync[$i] == "Insert" || $server_sync[$i] == "0" || $server_sync[$i] == "")
        {
            if(!in_array($product_table_id[$i], $previousAddedIds))
            {
                if($insertCheck)
                {
                    $insertQuery = $insertQuery.", ";
                }
                $insertCheck = true;
                $insertQuery = $insertQuery."('$product_table_id[$i]', '$product_id[$i]', '$company_table_id[$i]', '$group_table_id[$i]', '$report_prodID[$i]', '$old_prodID[$i]', '$product_name[$i]', '$product_type[$i]', '$packSize[$i]', '$carton_size[$i]', '$retail_price[$i]', '$trade_price[$i]', '$purchase_price[$i]', '$purchase_discount[$i]', '$sales_tax[$i]', '$hold_stock[$i]', '$product_status[$i]', '$creating_user_id[$i]', '$creating_date[$i]', '$creating_time[$i]', '$update_user_id[$i]', '$update_date[$i]', '$update_time[$i]','$softwareId')";

            }
        }
        elseif ($server_sync[$i] == "Update")
        {
            if($updateCheck)
            {
                $updateCheck = $updateCheck."; ";
            }
            $updateCheck = true;
            $updateQuery = $updateQuery."UPDATE `product_info` SET `product_id`= '$product_id[$i]',`company_table_id`= '$company_table_id[$i]',`group_table_id`= '$group_table_id[$i]',`report_prodID`= '$report_prodID[$i]',`old_prodID`= '$old_prodID[$i]',`product_name`= '$product_name[$i]',`product_type`= '$product_type[$i]',`packSize`= '$packSize[$i]',`carton_size`= '$carton_size[$i]',`retail_price`= '$retail_price[$i]',`trade_price`= '$trade_price[$i]',`purchase_price`= '$purchase_price[$i]',`purchase_discount`= '$purchase_discount[$i]',`sales_tax`= '$sales_tax[$i]',`hold_stock`= '$hold_stock[$i]',`product_status`= '$product_status',`creating_user_id`= '$creating_user_id[$i]',`creating_date`= '$creating_date',`creating_time`= '$creating_time',`update_user_id`= '$update_user_id[$i]',`update_date`= '$update_date[$i]',`update_time`= '$update_time[$i]' WHERE `software_product_id` = '$product_table_id[$i]' AND `software_lic_id` = '$softwareId'";
        }
    }
//    echo $insertQuery.'<br>';
    if($insertCheck && mysqli_query($con, $insertQuery))
    {
        $insertSuccess = "true";
    }
//    echo $updateQuery.'<br>';
    if($updateCheck && mysqli_multi_query($con, $updateQuery))
    {
        $updateSuccess = "true";
    }

    echo "Insert:".$insertSuccess.":Update:".$updateSuccess;
}
elseif($operation == "sync_product_overall_record")
{
    $previousAddedIds = array();
    $sqlSelectIds = "SELECT `software_overall_record_id` FROM `product_overall_record` WHERE `software_lic_id` = '$softwareId'";
    $sqlSelectIds = mysqli_query($con, $sqlSelectIds);
    while($addedId = mysqli_fetch_array($sqlSelectIds))
    {
        array_push($previousAddedIds, $addedId[0]);
    }
    $overall_record_id = $_POST['overall_record_id'];
    $product_table_id = $_POST['product_table_id'];
    $prod_name = $_POST['prod_name'];
    $prod_batch = $_POST['prod_batch'];
    $quantity_sold = $_POST['quantity_sold'];
    $quantity_return = $_POST['quantity_return'];
    $trade_rate = $_POST['trade_rate'];
    $purch_rate = $_POST['purch_rate'];
    $retail_rate = $_POST['retail_rate'];
    $expired_quantity = $_POST['expired_quantity'];
    $damaged_quantity = $_POST['damaged_quantity'];
    $prod_status = $_POST['prod_status'];
    $server_sync = $_POST['server_sync'];

    $overall_record_id = explode("/-_/", $overall_record_id);
    $product_table_id = explode("/-_/", $product_table_id);
    $prod_name = explode("/-_/", $prod_name);
    $prod_batch = explode("/-_/", $prod_batch);
    $quantity_sold = explode("/-_/", $quantity_sold);
    $quantity_return = explode("/-_/", $quantity_return);
    $trade_rate = explode("/-_/", $trade_rate);
    $purch_rate = explode("/-_/", $purch_rate);
    $retail_rate = explode("/-_/", $retail_rate);
    $expired_quantity = explode("/-_/", $expired_quantity);
    $damaged_quantity = explode("/-_/", $damaged_quantity);
    $prod_status = explode("/-_/", $prod_status);
    $server_sync = explode("/-_/", $server_sync);

    $insertCheck = false;
    $updateCheck = false;
    $insertQuery = "INSERT INTO `product_overall_record`(`software_overall_record_id`, `product_table_id`, `prod_name`, `prod_batch`, `quantity_sold`, `quantity_return`, `trade_rate`, `purch_rate`, `retail_rate`, `expired_quantity`, `damaged_quantity`, `prod_status`, `software_lic_id`) VALUES ";
    $updateQuery = "";
    $insertSuccess = "false";
    $updateSuccess = "false";
    for($i=0; $i<sizeof($overall_record_id); $i++)
    {
        if($server_sync[$i] == "Insert" || $server_sync[$i] == "0" || $server_sync[$i] == "")
        {
            if(!in_array($overall_record_id[$i], $previousAddedIds))
            {
                if($insertCheck)
                {
                    $insertQuery = $insertQuery.", ";
                }
                $insertCheck = true;
                $insertQuery = $insertQuery."('$overall_record_id[$i]', '$product_table_id[$i]', '$prod_name[$i]', '$prod_batch[$i]', '$quantity_sold[$i]', '$quantity_return[$i]', '$trade_rate[$i]', '$purch_rate[$i]', '$retail_rate[$i]', '$expired_quantity[$i]', '$damaged_quantity[$i]', '$prod_status[$i]','$softwareId')";

            }
        }
        elseif ($server_sync[$i] == "Update")
        {
            if($updateCheck)
            {
                $updateCheck = $updateCheck."; ";
            }
            $updateCheck = true;
            $updateQuery = $updateQuery."UPDATE `product_overall_record` SET `product_table_id`= '$product_table_id[$i]',`prod_name`= '$prod_name[$i]',`prod_batch`= '$prod_batch[$i]',`quantity_sold`= '$quantity_sold[$i]',`quantity_return`= '$quantity_return[$i]',`trade_rate`= '$trade_rate[$i]',`purch_rate`= '$purch_rate[$i]',`retail_rate`= '$retail_rate[$i]',`expired_quantity`= '$expired_quantity[$i]',`damaged_quantity`= '$damaged_quantity[$i]',`prod_status`= '$prod_status[$i]' WHERE `software_overall_record_id` = '$overall_record_id[$i]' AND `software_lic_id` = '$softwareId'";
        }
    }
//    echo $insertQuery.'<br>';
    if($insertCheck && mysqli_query($con, $insertQuery))
    {
        $insertSuccess = "true";
    }
//    echo $updateQuery.'<br>';
    if($updateCheck && mysqli_multi_query($con, $updateQuery))
    {
        $updateSuccess = "true";
    }

    echo "Insert:".$insertSuccess.":Update:".$updateSuccess;
}
elseif($operation == "sync_purchase_info")
{
    $previousAddedIds = array();
    $sqlSelectIds = "SELECT `software_purchase_id` FROM `purchase_info` WHERE `software_lic_id` = '$softwareId'";
    $sqlSelectIds = mysqli_query($con, $sqlSelectIds);
    while($addedId = mysqli_fetch_array($sqlSelectIds))
    {
        array_push($previousAddedIds, $addedId[0]);
    }
    $purchase_id = $_POST['purchase_id'];
    $comp_id = $_POST['comp_id'];
    $invoice_num = $_POST['invoice_num'];
    $supplier_id = $_POST['supplier_id'];
    $purchase_date = $_POST['purchase_date'];
    $gross_amount = $_POST['gross_amount'];
    $disc_amount = $_POST['disc_amount'];
    $net_amount = $_POST['net_amount'];
    $enter_user_id = $_POST['enter_user_id'];
    $entry_date = $_POST['entry_date'];
    $entry_time = $_POST['entry_time'];
    $update_user_id = $_POST['update_user_id'];
    $update_date = $_POST['update_date'];
    $update_time = $_POST['update_time'];
    $comments = $_POST['comments'];
    $status = $_POST['status'];
    $server_sync = $_POST['server_sync'];

    $purchase_id = explode("/-_/", $purchase_id);
    $comp_id = explode("/-_/", $comp_id);
    $invoice_num = explode("/-_/", $invoice_num);
    $supplier_id = explode("/-_/", $supplier_id);
    $purchase_date = explode("/-_/", $purchase_date);
    $gross_amount = explode("/-_/", $gross_amount);
    $disc_amount = explode("/-_/", $disc_amount);
    $net_amount = explode("/-_/", $net_amount);
    $enter_user_id = explode("/-_/", $enter_user_id);
    $entry_date = explode("/-_/", $entry_date);
    $entry_time = explode("/-_/", $entry_time);
    $update_user_id = explode("/-_/", $update_user_id);
    $update_date = explode("/-_/", $update_date);
    $update_time = explode("/-_/", $update_time);
    $comments = explode("/-_/", $comments);
    $status = explode("/-_/", $status);
    $server_sync = explode("/-_/", $server_sync);

    $insertCheck = false;
    $updateCheck = false;
    $insertQuery = "INSERT INTO `purchase_info`(`software_purchase_id`, `comp_id`, `invoice_num`, `supplier_id`, `purchase_date`, `gross_amount`, `disc_amount`, `net_amount`, `enter_user_id`, `entry_date`, `entry_time`, `update_user_id`, `update_date`, `update_time`, `comments`, `status`, `software_lic_id`) VALUES ";
    $updateQuery = "";
    $insertSuccess = "false";
    $updateSuccess = "false";
    for($i=0; $i<sizeof($purchase_id); $i++)
    {
        if($server_sync[$i] == "Insert" || $server_sync[$i] == "0" || $server_sync[$i] == "")
        {
            if(!in_array($purchase_id[$i], $previousAddedIds))
            {
                if($insertCheck)
                {
                    $insertQuery = $insertQuery.", ";
                }
                $insertCheck = true;
                $insertQuery = $insertQuery."('$purchase_id[$i]', '$comp_id[$i]', '$invoice_num[$i]', '$supplier_id[$i]', '$purchase_date[$i]', '$gross_amount[$i]', '$disc_amount[$i]', '$net_amount[$i]', '$enter_user_id[$i]', '$entry_date[$i]', '$entry_time[$i]', '$update_user_id[$i]', '$update_date[$i]', '$update_time[$i]', '$comments[$i]', '$status[$i]','$softwareId')";

            }
        }
        elseif ($server_sync[$i] == "Update")
        {
            if($updateCheck)
            {
                $updateCheck = $updateCheck."; ";
            }
            $updateCheck = true;
            $updateQuery = $updateQuery."UPDATE `purchase_info` SET `comp_id`= '$comp_id[$i]',`invoice_num`= '$invoice_num[$i]',`supplier_id`= '$supplier_id[$i]',`purchase_date`= '$purchase_date[$i]',`gross_amount`= '$gross_amount[$i]',`disc_amount`= '$disc_amount[$i]',`net_amount`= '$net_amount[$i]',`enter_user_id`= '$enter_user_id[$i]',`entry_date`= '$entry_date[$i]',`entry_time`= '$entry_time[$i]',`update_user_id`= '$update_user_id[$i]',`update_date`= '$update_date[$i]',`update_time`= '$update_time[$i]',`comments`= '$comments[$i]',`status`= '$status[$i]' WHERE `software_purchase_id` = '$purchase_id[$i]' AND `software_lic_id` = '$softwareId'";
        }
    }
//    echo $insertQuery.'<br>';
    if($insertCheck && mysqli_query($con, $insertQuery))
    {
        $insertSuccess = "true";
    }
//    echo $updateQuery.'<br>';
    if($updateCheck && mysqli_multi_query($con, $updateQuery))
    {
        $updateSuccess = "true";
    }

    echo "Insert:".$insertSuccess.":Update:".$updateSuccess;
}
elseif($operation == "sync_purchase_info_detail")
{
    $previousAddedIds = array();
    $sqlSelectIds = "SELECT `software_purhchase_info_detail_id` FROM `purchase_info_detail` WHERE `software_lic_id` = '$softwareId'";
    $sqlSelectIds = mysqli_query($con, $sqlSelectIds);
    while($addedId = mysqli_fetch_array($sqlSelectIds))
    {
        array_push($previousAddedIds, $addedId[0]);
    }
    $purchase_info_detail_id = $_POST['purchase_info_detail_id'];
    $invoice_num = $_POST['invoice_num'];
    $purchase_id = $_POST['purchase_id'];
    $prod_id = $_POST['prod_id'];
    $discount = $_POST['discount'];
    $bonus_quant = $_POST['bonus_quant'];
    $recieve_quant = $_POST['recieve_quant'];
    $batch_no = $_POST['batch_no'];
    $expiry_date = $_POST['expiry_date'];
    $gross_amount = $_POST['gross_amount'];
    $disc_amount = $_POST['disc_amount'];
    $net_amount = $_POST['net_amount'];
    $invoice_date = $_POST['invoice_date'];
    $returnBit = $_POST['returnBit'];
    $returned_quant = $_POST['returned_quant'];
    $returned_bonus_quant = $_POST['returned_bonus_quant'];
    $server_sync = $_POST['server_sync'];

    $purchase_info_detail_id = explode("/-_/", $purchase_info_detail_id);
    $invoice_num = explode("/-_/", $invoice_num);
    $purchase_id = explode("/-_/", $purchase_id);
    $prod_id = explode("/-_/", $prod_id);
    $discount = explode("/-_/", $discount);
    $bonus_quant = explode("/-_/", $bonus_quant);
    $recieve_quant = explode("/-_/", $recieve_quant);
    $batch_no = explode("/-_/", $batch_no);
    $expiry_date = explode("/-_/", $expiry_date);
    $gross_amount = explode("/-_/", $gross_amount);
    $disc_amount = explode("/-_/", $disc_amount);
    $net_amount = explode("/-_/", $net_amount);
    $invoice_date = explode("/-_/", $invoice_date);
    $returnBit = explode("/-_/", $returnBit);
    $returned_quant = explode("/-_/", $returned_quant);
    $returned_bonus_quant = explode("/-_/", $returned_bonus_quant);
    $server_sync = explode("/-_/", $server_sync);

    $insertCheck = false;
    $updateCheck = false;
    $insertQuery = "INSERT INTO `purchase_info_detail`(`software_purhchase_info_detail_id`, `invoice_num`, `purchase_id`, `prod_id`, `discount`, `bonus_quant`, `recieve_quant`, `batch_no`, `expiry_date`, `gross_amount`, `disc_amount`, `net_amount`, `invoice_date`, `returnBit`, `returned_quant`, `returned_bonus_quant`, `software_lic_id`) VALUES ";
    $updateQuery = "";
    $insertSuccess = "false";
    $updateSuccess = "false";
    for($i=0; $i<sizeof($purchase_info_detail_id); $i++)
    {
        if($server_sync[$i] == "Insert" || $server_sync[$i] == "0" || $server_sync[$i] == "")
        {
            if(!in_array($purchase_info_detail_id[$i], $previousAddedIds))
            {
                if($insertCheck)
                {
                    $insertQuery = $insertQuery.", ";
                }
                $insertCheck = true;
                $insertQuery = $insertQuery."('$purchase_info_detail_id[$i]', '$invoice_num[$i]', '$purchase_id[$i]', '$prod_id[$i]', '$discount[$i]', '$bonus_quant[$i]', '$recieve_quant[$i]', '$batch_no[$i]', '$expiry_date[$i]', '$gross_amount[$i]', '$disc_amount[$i]', '$net_amount[$i]', '$invoice_date[$i]', '$returnBit[$i]', '$returned_quant[$i]', '$returned_bonus_quant[$i]','$softwareId')";

            }
        }
        elseif ($server_sync[$i] == "Update")
        {
            if($updateCheck)
            {
                $updateCheck = $updateCheck."; ";
            }
            $updateCheck = true;
            $updateQuery = $updateQuery."UPDATE `purchase_info_detail` SET `purchase_id`= '$purchase_id[$i]',`prod_id`= '$prod_id[$i]',`discount`= '$discount[$i]',`bonus_quant`= '$bonus_quant[$i]',`recieve_quant`= '$recieve_quant[$i]',`batch_no`= '$batch_no[$i]',`expiry_date`= '$expiry_date[$i]',`gross_amount`= '$gross_amount[$i]',`disc_amount`= '$disc_amount[$i]',`net_amount`= '$net_amount[$i]',`invoice_date`= '$invoice_date[$i]',`returnBit`= '$returnBit[$i]',`returned_quant`= '$returned_quant[$i]',`returned_bonus_quant`= '$returned_bonus_quant[$i]' WHERE `software_purhchase_info_detail_id` = '$purchase_info_detail_id[$i]' AND `software_lic_id` = '$softwareId'";
        }
    }
//    echo $insertQuery.'<br>';
    if($insertCheck && mysqli_query($con, $insertQuery))
    {
        $insertSuccess = "true";
    }
//    echo $updateQuery.'<br>';
    if($updateCheck && mysqli_multi_query($con, $updateQuery))
    {
        $updateSuccess = "true";
    }

    echo "Insert:".$insertSuccess.":Update:".$updateSuccess;
}
elseif($operation == "sync_purchase_order_detail")
{
    $previousAddedIds = array();
    $sqlSelectIds = "SELECT `software_order_detail_id` FROM `purchase_order_detail` WHERE `software_lic_id` = '$softwareId'";
    $sqlSelectIds = mysqli_query($con, $sqlSelectIds);
    while($addedId = mysqli_fetch_array($sqlSelectIds))
    {
        array_push($previousAddedIds, $addedId[0]);
    }
    $order_detail_tableID = $_POST['order_detail_tableID'];
    $product_table_id = $_POST['product_table_id'];
    $quantity_sold = $_POST['quantity_sold'];
    $quantity_instock = $_POST['quantity_instock'];
    $quantity_ordered = $_POST['quantity_ordered'];
    $purch_order_id = $_POST['purch_order_id'];
    $net_amount = $_POST['net_amount'];
    $server_sync = $_POST['server_sync'];

    $order_detail_tableID = explode("/-_/", $order_detail_tableID);
    $product_table_id = explode("/-_/", $product_table_id);
    $quantity_sold = explode("/-_/", $quantity_sold);
    $quantity_instock = explode("/-_/", $quantity_instock);
    $quantity_ordered = explode("/-_/", $quantity_ordered);
    $purch_order_id = explode("/-_/", $purch_order_id);
    $net_amount = explode("/-_/", $net_amount);
    $server_sync = explode("/-_/", $server_sync);

    $insertCheck = false;
    $updateCheck = false;
    $insertQuery = "INSERT INTO `purchase_order_detail`(`software_order_detail_id`, `product_table_id`, `quantity_sold`, `quantity_instock`, `quantity_ordered`, `purch_order_id`, `net_amount`, `software_lic_id`) VALUES ";
    $updateQuery = "";
    $insertSuccess = "false";
    $updateSuccess = "false";
    for($i=0; $i<sizeof($order_detail_tableID); $i++)
    {
        if($server_sync[$i] == "Insert" || $server_sync[$i] == "0" || $server_sync[$i] == "")
        {
            if(!in_array($order_detail_tableID[$i], $previousAddedIds))
            {
                if($insertCheck)
                {
                    $insertQuery = $insertQuery.", ";
                }
                $insertCheck = true;
                $insertQuery = $insertQuery."('$order_detail_tableID[$i]','$product_table_id[$i]','$quantity_sold[$i]','$quantity_instock[$i]','$quantity_ordered[$i]','$purch_order_id[$i]','$net_amount[$i]','$softwareId')";

            }
        }
        elseif ($server_sync[$i] == "Update")
        {
            if($updateCheck)
            {
                $updateCheck = $updateCheck."; ";
            }
            $updateCheck = true;
            $updateQuery = $updateQuery."UPDATE `purchase_order_detail` SET `product_table_id`= '$product_table_id[$i]',`quantity_sold`= '$quantity_sold[$i]',`quantity_instock`= '$quantity_instock[$i]',`quantity_ordered`= '$quantity_ordered[$i]',`purch_order_id`= '$purch_order_id[$i]',`net_amount`= '$net_amount[$i]' WHERE `software_order_detail_id` = '$order_detail_tableID[$i]' AND `software_lic_id` = '$softwareId'";
        }
    }
//    echo $insertQuery.'<br>';
    if($insertCheck && mysqli_query($con, $insertQuery))
    {
        $insertSuccess = "true";
    }
//    echo $updateQuery.'<br>';
    if($updateCheck && mysqli_multi_query($con, $updateQuery))
    {
        $updateSuccess = "true";
    }

    echo "Insert:".$insertSuccess.":Update:".$updateSuccess;
}
elseif($operation == "sync_purchase_order_info")
{
    $previousAddedIds = array();
    $sqlSelectIds = "SELECT `software_purchase_order_id` FROM `purchase_order_info` WHERE `software_lic_id` = '$softwareId'";
    $sqlSelectIds = mysqli_query($con, $sqlSelectIds);
    while($addedId = mysqli_fetch_array($sqlSelectIds))
    {
        array_push($previousAddedIds, $addedId[0]);
    }
    $purch_order_table_id = $_POST['purch_order_table_id'];
    $purch_order_id = $_POST['purch_order_id'];
    $purch_order_date = $_POST['purch_order_date'];
    $purch_order_amount = $_POST['purch_order_amount'];
    $numb_of_prod = $_POST['numb_of_prod'];
    $inventoryDays = $_POST['inventoryDays'];
    $comp_table_id = $_POST['comp_table_id'];
    $order_status = $_POST['order_status'];
    $creating_user_id = $_POST['creating_user_id'];
    $creating_date = $_POST['creating_date'];
    $creating_time = $_POST['creating_time'];
    $update_user_id = $_POST['update_user_id'];
    $update_date = $_POST['update_date'];
    $update_time = $_POST['update_time'];
    $server_sync = $_POST['server_sync'];

    $purch_order_table_id = explode("/-_/", $purch_order_table_id);
    $purch_order_id = explode("/-_/", $purch_order_id);
    $purch_order_date = explode("/-_/", $purch_order_date);
    $purch_order_amount = explode("/-_/", $purch_order_amount);
    $numb_of_prod = explode("/-_/", $numb_of_prod);
    $inventoryDays = explode("/-_/", $inventoryDays);
    $comp_table_id = explode("/-_/", $comp_table_id);
    $order_status = explode("/-_/", $order_status);
    $creating_user_id = explode("/-_/", $creating_user_id);
    $creating_date = explode("/-_/", $creating_date);
    $creating_time = explode("/-_/", $creating_time);
    $update_user_id = explode("/-_/", $update_user_id);
    $update_date = explode("/-_/", $update_date);
    $update_time = explode("/-_/", $update_time);
    $server_sync = explode("/-_/", $server_sync);

    $insertCheck = false;
    $updateCheck = false;
    $insertQuery = "INSERT INTO `purchase_order_info`(`software_purchase_order_id`, `purch_order_id`, `purch_order_date`, `purch_order_amount`, `numb_of_prod`, `inventoryDays`, `comp_table_id`, `order_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`, `software_lic_id`) VALUES ";
    $updateQuery = "";
    $insertSuccess = "false";
    $updateSuccess = "false";
    for($i=0; $i<sizeof($purch_order_table_id); $i++)
    {
        if($server_sync[$i] == "Insert" || $server_sync[$i] == "0" || $server_sync[$i] == "")
        {
            if(!in_array($purch_order_table_id[$i], $previousAddedIds))
            {
                if($insertCheck)
                {
                    $insertQuery = $insertQuery.", ";
                }
                $insertCheck = true;
                $insertQuery = $insertQuery."('$purch_order_table_id[$i]', '$purch_order_id[$i]', '$purch_order_date[$i]', '$purch_order_amount[$i]', '$numb_of_prod[$i]', '$inventoryDays[$i]', '$comp_table_id[$i]', '$order_status[$i]', '$creating_user_id[$i]', '$creating_date[$i]', '$creating_time[$i]', '$update_user_id[$i]', '$update_date[$i]', '$update_time[$i]','$softwareId')";

            }
        }
        elseif ($server_sync[$i] == "Update")
        {
            if($updateCheck)
            {
                $updateCheck = $updateCheck."; ";
            }
            $updateCheck = true;
            $updateQuery = $updateQuery."UPDATE `purchase_order_info` SET `purch_order_id`= '$purch_order_id[$i]',`purch_order_date`= '$purch_order_date[$i]',`purch_order_amount`= '$purch_order_amount[$i]',`numb_of_prod`= '$numb_of_prod[$i]',`inventoryDays`= '$inventoryDays[$i]',`comp_table_id`= '$comp_table_id[$i]',`order_status`= '$order_status[$i]',`creating_user_id`= '$creating_user_id[$i]',`creating_date`= '$creating_date[$i]',`creating_time`= '$creating_time[$i]',`update_user_id`= '$update_user_id[$i]',`update_date`= '$update_date[$i]',`update_time`= '$update_time[$i]' WHERE `software_purchase_order_id` = '$purch_order_table_id[$i]' AND `software_lic_id` = '$softwareId'";
        }
    }
//    echo $insertQuery.'<br>';
    if($insertCheck && mysqli_query($con, $insertQuery))
    {
        $insertSuccess = "true";
    }
//    echo $updateQuery.'<br>';
    if($updateCheck && mysqli_multi_query($con, $updateQuery))
    {
        $updateSuccess = "true";
    }

    echo "Insert:".$insertSuccess.":Update:".$updateSuccess;
}
elseif($operation == "sync_purchase_return")
{
    $previousAddedIds = array();
    $sqlSelectIds = "SELECT `software_purchase_return_id` FROM `purchase_return` WHERE `software_lic_id` = '$softwareId'";
    $sqlSelectIds = mysqli_query($con, $sqlSelectIds);
    while($addedId = mysqli_fetch_array($sqlSelectIds))
    {
        array_push($previousAddedIds, $addedId[0]);
    }
    $returnTable_id = $_POST['returnTable_id'];
    $return_id = $_POST['return_id'];
    $purchInvoNumb = $_POST['purchInvoNumb'];
    $supplier_id = $_POST['supplier_id'];
    $return_total_price = $_POST['return_total_price'];
    $return_gross_price = $_POST['return_gross_price'];
    $entry_date = $_POST['entry_date'];
    $entry_time = $_POST['entry_time'];
    $update_date = $_POST['update_date'];
    $update_time = $_POST['update_time'];
    $user_id = $_POST['user_id'];
    $comments = $_POST['comments'];
    $status = $_POST['status'];
    $server_sync = $_POST['server_sync'];

    $returnTable_id = explode("/-_/", $returnTable_id);
    $return_id = explode("/-_/", $return_id);
    $purchInvoNumb = explode("/-_/", $purchInvoNumb);
    $supplier_id = explode("/-_/", $supplier_id);
    $return_total_price = explode("/-_/", $return_total_price);
    $return_gross_price = explode("/-_/", $return_gross_price);
    $entry_date = explode("/-_/", $entry_date);
    $entry_time = explode("/-_/", $entry_time);
    $update_date = explode("/-_/", $update_date);
    $update_time = explode("/-_/", $update_time);
    $user_id = explode("/-_/", $user_id);
    $comments = explode("/-_/", $comments);
    $status = explode("/-_/", $status);
    $server_sync = explode("/-_/", $server_sync);

    $insertCheck = false;
    $updateCheck = false;
    $insertQuery = "INSERT INTO `purchase_return`(`software_purchase_return_id`, `return_id`, `purchInvoNumb`, `supplier_id`, `return_total_price`, `return_gross_price`, `entry_date`, `entry_time`, `update_date`, `update_time`, `user_id`, `comments`, `status`, `software_lic_id`) VALUES ";
    $updateQuery = "";
    $insertSuccess = "false";
    $updateSuccess = "false";
    for($i=0; $i<sizeof($returnTable_id); $i++)
    {
        if($server_sync[$i] == "Insert" || $server_sync[$i] == "0" || $server_sync[$i] == "")
        {
            if(!in_array($returnTable_id[$i], $previousAddedIds))
            {
                if($insertCheck)
                {
                    $insertQuery = $insertQuery.", ";
                }
                $insertCheck = true;
                $insertQuery = $insertQuery."('$returnTable_id[$i]', '$return_id[$i]', '$purchInvoNumb[$i]', '$supplier_id[$i]', '$return_total_price[$i]', '$return_gross_price[$i]', '$entry_date[$i]', '$entry_time[$i]', '$update_date[$i]', '$update_time[$i]', '$user_id[$i]', '$comments[$i]', '$status[$i]','$softwareId')";

            }
        }
        elseif ($server_sync[$i] == "Update")
        {
            if($updateCheck)
            {
                $updateCheck = $updateCheck."; ";
            }
            $updateCheck = true;
            $updateQuery = $updateQuery."UPDATE `purchase_return` SET `return_id`= '$return_id[$i]',`purchInvoNumb`= '$purchInvoNumb[$i]',`supplier_id`= '$supplier_id[$i]',`return_total_price`= '$return_total_price[$i]',`return_gross_price`= '$return_gross_price[$i]',`entry_date`= '$entry_date[$i]',`entry_time`= '$entry_time[$i]',`update_date`= '$update_date[$i]',`update_time`= '$update_time[$i]',`user_id`= '$user_id[$i]',`comments`= '$comments[$i]',`status`= '$status[$i]' WHERE `software_purchase_return_id` = '$returnTable_id[$i]' AND `software_lic_id` = '$softwareId'";
        }
    }
//    echo $insertQuery.'<br>';
    if($insertCheck && mysqli_query($con, $insertQuery))
    {
        $insertSuccess = "true";
    }
//    echo $updateQuery.'<br>';
    if($updateCheck && mysqli_multi_query($con, $updateQuery))
    {
        $updateSuccess = "true";
    }

    echo "Insert:".$insertSuccess.":Update:".$updateSuccess;
}
elseif($operation == "sync_purchase_return_detail")
{
    $previousAddedIds = array();
    $sqlSelectIds = "SELECT `software_return_detail_id` FROM `purchase_return_detail` WHERE `software_lic_id` = '$softwareId'";
    $sqlSelectIds = mysqli_query($con, $sqlSelectIds);
    while($addedId = mysqli_fetch_array($sqlSelectIds))
    {
        array_push($previousAddedIds, $addedId[0]);
    }
    $return_detail_id = $_POST['return_detail_id'];
    $return_id = $_POST['return_id'];
    $prod_id = $_POST['prod_id'];
    $prod_batch = $_POST['prod_batch'];
    $prod_quant = $_POST['prod_quant'];
    $bonus_quant = $_POST['bonus_quant'];
    $discount_amount = $_POST['discount_amount'];
    $total_amount = $_POST['total_amount'];
    $unit = $_POST['unit'];
    $return_reason = $_POST['return_reason'];
    $server_sync = $_POST['server_sync'];

    $return_detail_id = explode("/-_/", $return_detail_id);
    $return_id = explode("/-_/", $return_id);
    $prod_id = explode("/-_/", $prod_id);
    $prod_batch = explode("/-_/", $prod_batch);
    $prod_quant = explode("/-_/", $prod_quant);
    $bonus_quant = explode("/-_/", $bonus_quant);
    $discount_amount = explode("/-_/", $discount_amount);
    $total_amount = explode("/-_/", $total_amount);
    $unit = explode("/-_/", $unit);
    $return_reason = explode("/-_/", $return_reason);
    $server_sync = explode("/-_/", $server_sync);

    $insertCheck = false;
    $updateCheck = false;
    $insertQuery = "INSERT INTO `purchase_return_detail`(`software_return_detail_id`, `return_id`, `prod_id`, `prod_batch`, `prod_quant`, `bonus_quant`, `discount_amount`, `total_amount`, `unit`, `return_reason`, `software_lic_id`) VALUES ";
    $updateQuery = "";
    $insertSuccess = "false";
    $updateSuccess = "false";
    for($i=0; $i<sizeof($return_detail_id); $i++)
    {
        if($server_sync[$i] == "Insert" || $server_sync[$i] == "0" || $server_sync[$i] == "")
        {
            if(!in_array($return_detail_id[$i], $previousAddedIds))
            {
                if($insertCheck)
                {
                    $insertQuery = $insertQuery.", ";
                }
                $insertCheck = true;
                $insertQuery = $insertQuery."('$return_detail_id[$i]', '$return_id[$i]', '$prod_id[$i]', '$prod_batch[$i]', '$prod_quant[$i]', '$bonus_quant[$i]', '$discount_amount[$i]', '$total_amount[$i]', '$unit[$i]', '$return_reason[$i]','$softwareId')";

            }
        }
        elseif ($server_sync[$i] == "Update")
        {
            if($updateCheck)
            {
                $updateCheck = $updateCheck."; ";
            }
            $updateCheck = true;
            $updateQuery = $updateQuery."UPDATE `purchase_return_detail` SET `return_id`= '$return_id[$i]',`prod_id`= '$prod_id[$i]',`prod_batch`= '$prod_batch[$i]',`prod_quant`= '$prod_quant[$i]',`bonus_quant`= '$bonus_quant[$i]',`discount_amount`= '$discount_amount[$i]',`total_amount`= '$total_amount[$i]',`unit`= '$unit[$i]',`return_reason`= '$return_reason[$i]' WHERE `software_return_detail_id` = '$return_detail_id[$i]' AND `software_lic_id` = '$softwareId'";
        }
    }
//    echo $insertQuery.'<br>';
    if($insertCheck && mysqli_query($con, $insertQuery))
    {
        $insertSuccess = "true";
    }
//    echo $updateQuery.'<br>';
    if($updateCheck && mysqli_multi_query($con, $updateQuery))
    {
        $updateSuccess = "true";
    }

    echo "Insert:".$insertSuccess.":Update:".$updateSuccess;
}
elseif($operation == "sync_purchase_return_info_detail")
{
    $previousAddedIds = array();
    $sqlSelectIds = "SELECT `software_return_id` FROM `purchase_return_info_detail` WHERE `software_lic_id` = '$softwareId'";
    $sqlSelectIds = mysqli_query($con, $sqlSelectIds);
    while($addedId = mysqli_fetch_array($sqlSelectIds))
    {
        array_push($previousAddedIds, $addedId[0]);
    }
    $return_id = $_POST['return_id'];
    $supplier_id = $_POST['supplier_id'];
    $purch_invo_num = $_POST['purch_invo_num'];
    $return_date = $_POST['return_date'];
    $prod_id = $_POST['prod_id'];
    $return_quant = $_POST['return_quant'];
    $discount_percen = $_POST['discount_percen'];
    $bonus_quantity = $_POST['bonus_quantity'];
    $total_amount = $_POST['total_amount'];
    $batch_no = $_POST['batch_no'];
    $gross_amount = $_POST['gross_amount'];

    $return_id = explode("/-_/", $return_id);
    $supplier_id = explode("/-_/", $supplier_id);
    $purch_invo_num = explode("/-_/", $purch_invo_num);
    $return_date = explode("/-_/", $return_date);
    $prod_id = explode("/-_/", $prod_id);
    $return_quant = explode("/-_/", $return_quant);
    $discount_percen = explode("/-_/", $discount_percen);
    $bonus_quantity = explode("/-_/", $bonus_quantity);
    $total_amount = explode("/-_/", $total_amount);
    $batch_no = explode("/-_/", $batch_no);
    $gross_amount = explode("/-_/", $gross_amount);
    $server_sync = explode("/-_/", $server_sync);

    $insertCheck = false;
    $updateCheck = false;
    $insertQuery = "INSERT INTO `purchase_return_info_detail`(`software_return_id`, `supplier_id`, `purch_invo_num`, `return_date`, `prod_id`, `return_quant`, `discount_percent`, `bonus_quantity`, `total_amount`, `batch_no`, `gross_amount`, `software_lic_id`) VALUES ";
    $updateQuery = "";
    $insertSuccess = "false";
    $updateSuccess = "false";
    for($i=0; $i<sizeof($return_id); $i++)
    {
        if($server_sync[$i] == "Insert" || $server_sync[$i] == "0" || $server_sync[$i] == "")
        {
            if(!in_array($return_id[$i], $previousAddedIds))
            {
                if($insertCheck)
                {
                    $insertQuery = $insertQuery.", ";
                }
                $insertCheck = true;
                $insertQuery = $insertQuery."('$return_id[$i]', '$supplier_id[$i]', '$purch_invo_num[$i]', '$return_date[$i]', '$prod_id[$i]', '$return_quant[$i]', '$discount_percent[$i]', '$bonus_quantity[$i]', '$total_amount[$i]', '$batch_no[$i]', '$gross_amount[$i]','$softwareId')";

            }
        }
        elseif ($server_sync[$i] == "Update")
        {
            if($updateCheck)
            {
                $updateCheck = $updateCheck."; ";
            }
            $updateCheck = true;
            $updateQuery = $updateQuery."UPDATE `purchase_return_info_detail` SET `supplier_id`= '$supplier_id[$i]',`purch_invo_num`= '$purch_invo_num[$i]',`return_date`= '$return_date[$i]',`prod_id`= '$prod_id[$i]',`return_quant`= '$return_quant[$i]',`discount_percent`= '$discount_percent[$i]',`bonus_quantity`= '$bonus_quantity[$i]',`total_amount`= '$total_amount[$i]',`batch_no`= '$batch_no[$i]',`gross_amount`= '$gross_amount[$i]' WHERE `software_return_id` = '$return_id[$i]' AND `software_lic_id` = '$softwareId'";
        }
    }
//    echo $insertQuery.'<br>';
    if($insertCheck && mysqli_query($con, $insertQuery))
    {
        $insertSuccess = "true";
    }
//    echo $updateQuery.'<br>';
    if($updateCheck && mysqli_multi_query($con, $updateQuery))
    {
        $updateSuccess = "true";
    }

    echo "Insert:".$insertSuccess.":Update:".$updateSuccess;
}
elseif($operation == "sync_purch_return_info")
{
    $previousAddedIds = array();
    $sqlSelectIds = "SELECT `software_purch_return_no` FROM `purch_return_info` WHERE `software_lic_id` = '$softwareId'";
    $sqlSelectIds = mysqli_query($con, $sqlSelectIds);
    while($addedId = mysqli_fetch_array($sqlSelectIds))
    {
        array_push($previousAddedIds, $addedId[0]);
    }
    $purch_return_no = $_POST['purch_return_no'];
    $purch_invo_num = $_POST['purch_invo_num'];
    $return_date = $_POST['return_date'];
    $supplier_id = $_POST['supplier_id'];
    $total_amount = $_POST['total_amount'];
    $discount_amount = $_POST['discount_amount'];
    $server_sync = $_POST['server_sync'];

    $purch_return_no = explode("/-_/", $purch_return_no);
    $purch_invo_num = explode("/-_/", $purch_invo_num);
    $return_date = explode("/-_/", $return_date);
    $supplier_id = explode("/-_/", $supplier_id);
    $total_amount = explode("/-_/", $total_amount);
    $discount_amount = explode("/-_/", $discount_amount);
    $server_sync = explode("/-_/", $server_sync);

    $insertCheck = false;
    $updateCheck = false;
    $insertQuery = "INSERT INTO `purch_return_info`(`software_purch_return_no`, `purch_invo_num`, `return_date`, `supplier_id`, `total_amount`, `discount_amount`, `software_lic_id`) VALUES ";
    $updateQuery = "";
    $insertSuccess = "false";
    $updateSuccess = "false";
    for($i=0; $i<sizeof($purch_return_no); $i++)
    {
        if($server_sync[$i] == "Insert" || $server_sync[$i] == "0" || $server_sync[$i] == "")
        {
            if(!in_array($purch_return_no[$i], $previousAddedIds))
            {
                if($insertCheck)
                {
                    $insertQuery = $insertQuery.", ";
                }
                $insertCheck = true;
                $insertQuery = $insertQuery."('$purch_return_no[$i]','$purch_invo_num[$i]','$return_date[$i]','$supplier_id[$i]','$total_amount[$i]','$discount_amount[$i]','$softwareId')";

            }
        }
        elseif ($server_sync[$i] == "Update")
        {
            if($updateCheck)
            {
                $updateCheck = $updateCheck."; ";
            }
            $updateCheck = true;
            $updateQuery = $updateQuery."UPDATE `purch_return_info` SET `purch_invo_num`= '$purch_invo_num[$i]',`return_date`= '$return_date[$i]',`supplier_id`= '$supplier_id[$i]',`total_amount`= '$total_amount[$i]',`discount_amount`= '$discount_amount[$i]' WHERE `software_purch_return_no` = '$purch_return_no[$i]' AND `software_lic_id` = '$softwareId'";
        }
    }
//    echo $insertQuery.'<br>';
    if($insertCheck && mysqli_query($con, $insertQuery))
    {
        $insertSuccess = "true";
    }
//    echo $updateQuery.'<br>';
    if($updateCheck && mysqli_multi_query($con, $updateQuery))
    {
        $updateSuccess = "true";
    }

    echo "Insert:".$insertSuccess.":Update:".$updateSuccess;
}
elseif($operation == "sync_salesman_designated_cities")
{
    $previousAddedIds = array();
    $sqlSelectIds = "SELECT `software_salesman_designated_id` FROM `salesman_designated_cities` WHERE `software_lic_id` = '$softwareId'";
    $sqlSelectIds = mysqli_query($con, $sqlSelectIds);
    while($addedId = mysqli_fetch_array($sqlSelectIds))
    {
        array_push($previousAddedIds, $addedId[0]);
    }
    $salesman_designated_id = $_POST['salesman_designated_id'];
    $salesman_table_id = $_POST['salesman_table_id'];
    $designated_city_id = $_POST['designated_city_id'];
    $start_date = $_POST['start_date'];
    $end_date = $_POST['end_date'];
    $designated_status = $_POST['designated_status'];
    $server_sync = $_POST['server_sync'];

    $salesman_designated_id = explode("/-_/", $salesman_designated_id);
    $salesman_table_id = explode("/-_/", $salesman_table_id);
    $designated_city_id = explode("/-_/", $designated_city_id);
    $start_date = explode("/-_/", $start_date);
    $end_date = explode("/-_/", $end_date);
    $designated_status = explode("/-_/", $designated_status);
    $server_sync = explode("/-_/", $server_sync);

    $insertCheck = false;
    $updateCheck = false;
    $insertQuery = "INSERT INTO `salesman_designated_cities`(`software_salesman_designated_id`, `salesman_table_id`, `designated_city_id`, `start_date`, `end_date`, `designated_status`, `software_lic_id`) VALUES ";
    $updateQuery = "";
    $insertSuccess = "false";
    $updateSuccess = "false";
    for($i=0; $i<sizeof($salesman_designated_id); $i++)
    {
        if($server_sync[$i] == "Insert" || $server_sync[$i] == "0" || $server_sync[$i] == "")
        {
            if(!in_array($salesman_designated_id[$i], $previousAddedIds))
            {
                if($insertCheck)
                {
                    $insertQuery = $insertQuery.", ";
                }
                $insertCheck = true;
                $insertQuery = $insertQuery."('$salesman_designated_id[$i]','$salesman_table_id[$i]','$designated_city_id[$i]','$start_date[$i]','$end_date[$i]','$designated_status[$i]','$softwareId')";

            }
        }
        elseif ($server_sync[$i] == "Update")
        {
            if($updateCheck)
            {
                $updateCheck = $updateCheck."; ";
            }
            $updateCheck = true;
            $updateQuery = $updateQuery."UPDATE `salesman_designated_cities` SET `salesman_table_id`= '$salesman_table_id[$i]',`designated_city_id`= '$designated_city_id[$i]',`start_date`= '$start_date[$i]',`end_date`= '$end_date[$i]',`designated_status`= '$designated_status[$i]' WHERE `software_salesman_designated_id` = '$salesman_designated_id[$i]' AND `software_lic_id` = '$softwareId'";
        }
    }
//    echo $insertQuery.'<br>';
    if($insertCheck && mysqli_query($con, $insertQuery))
    {
        $insertSuccess = "true";
    }
//    echo $updateQuery.'<br>';
    if($updateCheck && mysqli_multi_query($con, $updateQuery))
    {
        $updateSuccess = "true";
    }

    echo "Insert:".$insertSuccess.":Update:".$updateSuccess;
}
elseif($operation == "sync_sale_invoice_print_info")
{
    $previousAddedIds = array();
    $sqlSelectIds = "SELECT `software_sale_print_id` FROM `sale_invoice_print_info` WHERE `software_lic_id` = '$softwareId'";
    $sqlSelectIds = mysqli_query($con, $sqlSelectIds);
    while($addedId = mysqli_fetch_array($sqlSelectIds))
    {
        array_push($previousAddedIds, $addedId[0]);
    }
    $sale_print_id = $_POST['sale_print_id'];
    $header_1 = $_POST['header_1'];
    $header_2 = $_POST['header_2'];
    $header_3 = $_POST['header_3'];
    $header_4 = $_POST['header_4'];
    $header_5 = $_POST['header_5'];
    $owner_ids = $_POST['owner_ids'];
    $owner_name = $_POST['owner_name'];
    $father_name = $_POST['father_name'];
    $owner_cnic = $_POST['owner_cnic'];
    $owner_country = $_POST['owner_country'];
    $business_name = $_POST['business_name'];
    $business_address = $_POST['business_address'];
    $business_city = $_POST['business_city'];
    $footer_1 = $_POST['footer_1'];
    $footer_2 = $_POST['footer_2'];
    $footer_3 = $_POST['footer_3'];
    $footer_4 = $_POST['footer_4'];
    $creating_user_id = $_POST['creating_user_id'];
    $creating_date = $_POST['creating_date'];
    $creating_time = $_POST['creating_time'];
    $update_user_id = $_POST['update_user_id'];
    $update_date = $_POST['update_date'];
    $update_time = $_POST['update_time'];
    $status = $_POST['status'];
    $server_sync = $_POST['server_sync'];

    $sale_print_id = explode("/-_/", $sale_print_id);
    $header_1 = explode("/-_/", $header_1);
    $header_2 = explode("/-_/", $header_2);
    $header_3 = explode("/-_/", $header_3);
    $header_4 = explode("/-_/", $header_4);
    $header_5 = explode("/-_/", $header_5);
    $owner_ids = explode("/-_/", $owner_ids);
    $owner_name = explode("/-_/", $owner_name);
    $father_name = explode("/-_/", $father_name);
    $owner_cnic = explode("/-_/", $owner_cnic);
    $owner_country = explode("/-_/", $owner_country);
    $business_name = explode("/-_/", $business_name);
    $business_address = explode("/-_/", $business_address);
    $business_city = explode("/-_/", $business_city);
    $footer_1 = explode("/-_/", $footer_1);
    $footer_2 = explode("/-_/", $footer_2);
    $footer_3 = explode("/-_/", $footer_3);
    $footer_4 = explode("/-_/", $footer_4);
    $creating_user_id = explode("/-_/", $creating_user_id);
    $creating_date = explode("/-_/", $creating_date);
    $creating_time = explode("/-_/", $creating_time);
    $update_user_id = explode("/-_/", $update_user_id);
    $update_date = explode("/-_/", $update_date);
    $update_time = explode("/-_/", $update_time);
    $status = explode("/-_/", $status);
    $server_sync = explode("/-_/", $server_sync);

    $insertCheck = false;
    $updateCheck = false;
    $insertQuery = "INSERT INTO `sale_invoice_print_info`(`software_sale_print_id`, `header_1`, `header_2`, `header_3`, `header_4`, `header_5`, `owner_ids`, `owner_name`, `father_name`, `owner_cnic`, `owner_country`, `business_name`, `business_address`, `business_city`, `footer_1`, `footer_2`, `footer_3`, `footer_4`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`, `status`, `software_lic_id`) VALUES ";
    $updateQuery = "";
    $insertSuccess = "false";
    $updateSuccess = "false";
    for($i=0; $i<sizeof($sale_print_id); $i++)
    {
        if($server_sync[$i] == "Insert" || $server_sync[$i] == "0" || $server_sync[$i] == "")
        {
            if(!in_array($sale_print_id[$i], $previousAddedIds))
            {
                if($insertCheck)
                {
                    $insertQuery = $insertQuery.", ";
                }
                $insertCheck = true;
                $insertQuery = $insertQuery."('$sale_print_id[$i]', '$header_1[$i]', '$header_2[$i]', '$header_3[$i]', '$header_4[$i]', '$header_5[$i]', '$owner_ids[$i]', '$owner_name[$i]', '$father_name[$i]', '$owner_cnic[$i]', '$owner_country[$i]', '$business_name[$i]', '$business_address[$i]', '$business_city[$i]', '$footer_1[$i]', '$footer_2[$i]', '$footer_3[$i]', '$footer_4[$i]', '$creating_user_id[$i]', '$creating_date[$i]', '$creating_time[$i]', '$update_user_id[$i]', '$update_date[$i]', '$update_time[$i]', '$status[$i]','$softwareId')";

            }
        }
        elseif ($server_sync[$i] == "Update")
        {
            if($updateCheck)
            {
                $updateCheck = $updateCheck."; ";
            }
            $updateCheck = true;
            $updateQuery = $updateQuery."UPDATE `sale_invoice_print_info` SET `header_1`= '$header_1[$i]',`header_2`= '$header_2[$i]',`header_3`= '$header_3[$i]',`header_4`= '$header_4[$i]',`header_5`= '$header_5[$i]',`owner_ids`= '$owner_ids[$i]',`owner_name`= '$owner_name[$i]',`father_name`= '$father_name[$i]',`owner_cnic`= '$owner_cnic[$i]',`owner_country`= '$owner_country[$i]',`business_name`= '$business_name[$i]',`business_address`= '$business_address[$i]',`business_city`= '$business_city[$i]',`footer_1`= '$footer_1[$i]',`footer_2`= '$footer_2[$i]',`footer_3`= '$footer_3[$i]',`footer_4`= '$footer_4[$i]',`creating_user_id`= '$creating_user_id[$i]',`creating_date`= '$creating_date[$i]',`creating_time`= '$creating_time[$i]',`update_user_id`= '$update_user_id[$i]',`update_date`= '$update_date[$i]',`update_time`= '$update_time[$i]',`status`= '$status[$i]' WHERE `software_sale_print_id` = '$sale_print_id[$i]' AND `software_lic_id` = '$softwareId'";
        }
    }
//    echo $insertQuery.'<br>';
    if($insertCheck && mysqli_query($con, $insertQuery))
    {
        $insertSuccess = "true";
    }
//    echo $updateQuery.'<br>';
    if($updateCheck && mysqli_multi_query($con, $updateQuery))
    {
        $updateSuccess = "true";
    }

    echo "Insert:".$insertSuccess.":Update:".$updateSuccess;
}
elseif($operation == "sync_supplier_company")
{
    $previousAddedIds = array();
    $sqlSelectIds = "SELECT `software_supplier_company_id` FROM `supplier_company` WHERE `software_lic_id` = '$softwareId'";
    $sqlSelectIds = mysqli_query($con, $sqlSelectIds);
    while($addedId = mysqli_fetch_array($sqlSelectIds))
    {
        array_push($previousAddedIds, $addedId[0]);
    }
    $supplier_company_id = $_POST['supplier_company_id'];
    $supplier_id = $_POST['supplier_id'];
    $company_id = $_POST['company_id'];
    $updated_date = $_POST['updated_date'];
    $user_id = $_POST['user_id'];
    $status = $_POST['status'];
    $server_sync = $_POST['server_sync'];

    $supplier_company_id = explode("/-_/", $supplier_company_id);
    $supplier_id = explode("/-_/", $supplier_id);
    $company_id = explode("/-_/", $company_id);
    $updated_date = explode("/-_/", $updated_date);
    $user_id = explode("/-_/", $user_id);
    $status = explode("/-_/", $status);
    $server_sync = explode("/-_/", $server_sync);

    $insertCheck = false;
    $updateCheck = false;
    $insertQuery = "INSERT INTO `supplier_company`(`software_supplier_company_id`, `supplier_id`, `company_id`, `updated_date`, `user_id`, `status`, `software_lic_id`) VALUES ";
    $updateQuery = "";
    $insertSuccess = "false";
    $updateSuccess = "false";
    for($i=0; $i<sizeof($supplier_company_id); $i++)
    {
        if($server_sync[$i] == "Insert" || $server_sync[$i] == "0" || $server_sync[$i] == "")
        {
            if(!in_array($supplier_company_id[$i], $previousAddedIds))
            {
                if($insertCheck)
                {
                    $insertQuery = $insertQuery.", ";
                }
                $insertCheck = true;
                $insertQuery = $insertQuery."('$supplier_company_id[$i]','$supplier_id[$i]','$company_id[$i]','$updated_date[$i]','$user_id[$i]','$status[$i]','$softwareId')";

            }
        }
        elseif ($server_sync[$i] == "Update")
        {
            if($updateCheck)
            {
                $updateCheck = $updateCheck."; ";
            }
            $updateCheck = true;
            $updateQuery = $updateQuery."UPDATE `supplier_company` SET `supplier_id`= '$supplier_id[$i]',`company_id`= '$company_id[$i]',`updated_date`= '$updated_date[$i]',`user_id`= '$user_id[$i]',`status`= '$status[$i]' WHERE `software_supplier_company_id` = '$supplier_company_id[$i]' AND `software_lic_id` = '$softwareId'";
        }
    }
//    echo $insertQuery.'<br>';
    if($insertCheck && mysqli_query($con, $insertQuery))
    {
        $insertSuccess = "true";
    }
//    echo $updateQuery.'<br>';
    if($updateCheck && mysqli_multi_query($con, $updateQuery))
    {
        $updateSuccess = "true";
    }

    echo "Insert:".$insertSuccess.":Update:".$updateSuccess;
}
elseif($operation == "sync_supplier_info")
{
    $previousAddedIds = array();
    $sqlSelectIds = "SELECT `software_supplier_id` FROM `supplier_info` WHERE `software_lic_id` = '$softwareId'";
    $sqlSelectIds = mysqli_query($con, $sqlSelectIds);
    while($addedId = mysqli_fetch_array($sqlSelectIds))
    {
        array_push($previousAddedIds, $addedId[0]);
    }
    $supplier_table_id = $_POST['supplier_table_id'];
    $supplier_id = $_POST['supplier_id'];
    $supplier_name = $_POST['supplier_name'];
    $supplier_email = $_POST['supplier_email'];
    $supplier_contact = $_POST['supplier_contact'];
    $contact_person = $_POST['contact_person'];
    $supplier_address = $_POST['supplier_address'];
    $supplier_city = $_POST['supplier_city'];
    $supplier_status = $_POST['supplier_status'];
    $creating_user_id = $_POST['creating_user_id'];
    $creating_date = $_POST['creating_date'];
    $creating_time = $_POST['creating_time'];
    $update_user_id = $_POST['update_user_id'];
    $update_date = $_POST['update_date'];
    $update_time = $_POST['update_time'];
    $server_sync = $_POST['server_sync'];

    $supplier_table_id = explode("/-_/", $supplier_table_id);
    $supplier_id = explode("/-_/", $supplier_id);
    $supplier_name = explode("/-_/", $supplier_name);
    $supplier_email = explode("/-_/", $supplier_email);
    $supplier_contact = explode("/-_/", $supplier_contact);
    $contact_person = explode("/-_/", $contact_person);
    $supplier_address = explode("/-_/", $supplier_address);
    $supplier_city = explode("/-_/", $supplier_city);
    $supplier_status = explode("/-_/", $supplier_status);
    $creating_user_id = explode("/-_/", $creating_user_id);
    $creating_date = explode("/-_/", $creating_date);
    $creating_time = explode("/-_/", $creating_time);
    $update_user_id = explode("/-_/", $update_user_id);
    $update_date = explode("/-_/", $update_date);
    $update_time = explode("/-_/", $update_time);
    $server_sync = explode("/-_/", $server_sync);

    $insertCheck = false;
    $updateCheck = false;
    $insertQuery = "INSERT INTO `supplier_info`(`software_supplier_id`, `supplier_id`, `supplier_name`, `supplier_email`, `supplier_contact`, `contact_person`, `supplier_address`, `supplier_city`, `supplier_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`, `software_lic_id`) VALUES ";
    $updateQuery = "";
    $insertSuccess = "false";
    $updateSuccess = "false";
    for($i=0; $i<sizeof($supplier_table_id); $i++)
    {
        if($server_sync[$i] == "Insert" || $server_sync[$i] == "0" || $server_sync[$i] == "")
        {
            if(!in_array($supplier_table_id[$i], $previousAddedIds))
            {
                if($insertCheck)
                {
                    $insertQuery = $insertQuery.", ";
                }
                $insertCheck = true;
                $insertQuery = $insertQuery."('$supplier_table_id[$i]', '$supplier_id[$i]', '$supplier_name[$i]', '$supplier_email[$i]', '$supplier_contact[$i]', '$contact_person[$i]', '$supplier_address[$i]', '$supplier_city[$i]', '$supplier_status[$i]', '$creating_user_id[$i]', '$creating_date[$i]', '$creating_time[$i]', '$update_user_id[$i]', '$update_date[$i]', '$update_time[$i]','$softwareId')";

            }
        }
        elseif ($server_sync[$i] == "Update")
        {
            if($updateCheck)
            {
                $updateCheck = $updateCheck."; ";
            }
            $updateCheck = true;
            $updateQuery = $updateQuery."UPDATE `supplier_info` SET `supplier_id`= '$supplier_id[$i]',`supplier_name`= '$supplier_name[$i]',`supplier_email`= '$supplier_email[$i]',`supplier_contact`= '$supplier_contact[$i]',`contact_person`= '$contact_Person[$i]',`supplier_address`= '$supplier_address[$i]',`supplier_city`= '$supplier_city[$i]',`supplier_status`= '$supplier_status[$i]',`creating_user_id`= '$creating_user_id[$i]',`creating_date`= '$creating_date[$i]',`creating_time`= '$creating_time[$i]',`update_user_id`= '$update_user_id[$i]',`update_date`= '$update_date[$i]',`update_time`= '$update_time[$i]' WHERE `software_supplier_id` = '$supplier_table_id[$i]' AND `software_lic_id` = '$softwareId'";
        }
    }
//    echo $insertQuery.'<br>';
    if($insertCheck && mysqli_query($con, $insertQuery))
    {
        $insertSuccess = "true";
    }
//    echo $updateQuery.'<br>';
    if($updateCheck && mysqli_multi_query($con, $updateQuery))
    {
        $updateSuccess = "true";
    }

    echo "Insert:".$insertSuccess.":Update:".$updateSuccess;
}
elseif($operation == "sync_sync_tracking")
{
    $previousAddedIds = array();
    $sqlSelectIds = "SELECT `software_sync_id` FROM `sync_tracking` WHERE `software_lic_id` = '$softwareId'";
    $sqlSelectIds = mysqli_query($con, $sqlSelectIds);
    while($addedId = mysqli_fetch_array($sqlSelectIds))
    {
        array_push($previousAddedIds, $addedId[0]);
    }
    $sync_id = $_POST['sync_id'];
    $user_id = $_POST['user_id'];
    $dealer_info = $_POST['dealer_info'];
    $area_info = $_POST['area_info'];
    $product_info = $_POST['product_info'];
    $discount_info = $_POST['discount_info'];
    $bonus_info = $_POST['bonus_info'];
    $server_sync = $_POST['server_sync'];

    $sync_id = explode("/-_/", $sync_id);
    $user_id = explode("/-_/", $user_id);
    $dealer_info = explode("/-_/", $dealer_info);
    $area_info = explode("/-_/", $area_info);
    $product_info = explode("/-_/", $product_info);
    $discount_info = explode("/-_/", $discount_info);
    $bonus_info = explode("/-_/", $bonus_info);
    $server_sync = explode("/-_/", $server_sync);

    $insertCheck = false;
    $updateCheck = false;
    $insertQuery = "INSERT INTO `sync_tracking`(`software_sync_id`, `user_id`, `dealer_info`, `area_info`, `product_info`, `discount_info`, `bonus_info`, `software_lic_id`) VALUES ";
    $updateQuery = "";
    $insertSuccess = "false";
    $updateSuccess = "false";
    for($i=0; $i<sizeof($sync_id); $i++)
    {
        if($server_sync[$i] == "Insert" || $server_sync[$i] == "0" || $server_sync[$i] == "")
        {
            if(!in_array($sync_id[$i], $previousAddedIds))
            {
                if($insertCheck)
                {
                    $insertQuery = $insertQuery.", ";
                }
                $insertCheck = true;
                $insertQuery = $insertQuery."('$sync_id[$i]', '$user_id[$i]', '$dealer_info[$i]', '$area_info[$i]', '$product_info[$i]', '$discount_info[$i]', '$bonus_info[$i]','$softwareId')";

            }
        }
        elseif ($server_sync[$i] == "Update")
        {
            if($updateCheck)
            {
                $updateCheck = $updateCheck."; ";
            }
            $updateCheck = true;
            $updateQuery = $updateQuery."UPDATE `sync_tracking` SET `user_id`= '$user_id[$i]',`dealer_info`= '$dealer_info[$i]',`area_info`= '$area_info[$i]',`product_info`= '$product_info[$i]',`discount_info`= '$discount_info[$i]',`bonus_info`= '$bonus_info[$i]' WHERE `software_data_id` = '$sync_id[$i]' AND `software_lic_id` = '$softwareId'";
        }
    }
//    echo $insertQuery.'<br>';
    if($insertCheck && mysqli_query($con, $insertQuery))
    {
        $insertSuccess = "true";
    }
//    echo $updateQuery.'<br>';
    if($updateCheck && mysqli_multi_query($con, $updateQuery))
    {
        $updateSuccess = "true";
    }

    echo "Insert:".$insertSuccess.":Update:".$updateSuccess;
}
elseif($operation == "sync_system_settings")
{
    $previousAddedIds = array();
    $sqlSelectIds = "SELECT `software_setting_id` FROM `system_settings` WHERE `software_lic_id` = '$softwareId'";
    $sqlSelectIds = mysqli_query($con, $sqlSelectIds);
    while($addedId = mysqli_fetch_array($sqlSelectIds))
    {
        array_push($previousAddedIds, $addedId[0]);
    }
    $setting_id = $_POST['setting_id'];
    $setting_name = $_POST['setting_name'];
    $setting_value = $_POST['setting_value'];
    $server_sync = $_POST['server_sync'];

    $setting_id = explode("/-_/", $setting_id);
    $setting_name = explode("/-_/", $setting_name);
    $setting_value = explode("/-_/", $setting_value);
    $server_sync = explode("/-_/", $server_sync);

    $insertCheck = false;
    $updateCheck = false;
    $insertQuery = "INSERT INTO `system_settings`(`software_setting_id`, `setting_name`, `setting_value`, `software_lic_id`) VALUES ";
    $updateQuery = "";
    $insertSuccess = "false";
    $updateSuccess = "false";
    for($i=0; $i<sizeof($setting_id); $i++)
    {
        if($server_sync[$i] == "Insert" || $server_sync[$i] == "0" || $server_sync[$i] == "")
        {
            if(!in_array($setting_id[$i], $previousAddedIds))
            {
                if($insertCheck)
                {
                    $insertQuery = $insertQuery.", ";
                }
                $insertQuery = $insertQuery."('$setting_id[$i]','$setting_name[$i]','$setting_value[$i]','$softwareId')";
                $insertCheck = true;

            }
        }
        elseif ($server_sync[$i] == "Update")
        {
            if($updateCheck)
            {
                $updateCheck = $updateCheck."; ";
            }
            $updateCheck = true;
            $updateQuery = $updateQuery."UPDATE `system_settings` SET `setting_name`= '$setting_name[$i]',`setting_value`= '$setting_value[$i]' WHERE `software_setting_id` = '$setting_id[$i]' AND `software_lic_id` = '$softwareId'";
        }
    }
//    echo $insertQuery.'<br>';
    if($insertCheck && mysqli_query($con, $insertQuery))
    {
        $insertSuccess = "true";
    }
//    echo $updateQuery.'<br>';
    if($updateCheck && mysqli_multi_query($con, $updateQuery))
    {
        $updateSuccess = "true";
    }

    echo "Insert:".$insertSuccess.":Update:".$updateSuccess;
}
elseif($operation == "sync_user_accounts")
{
    $previousAddedIds = array();
    $sqlSelectIds = "SELECT `software_account_id` FROM `user_accounts` WHERE `software_lic_id` = '$softwareId'";
    $sqlSelectIds = mysqli_query($con, $sqlSelectIds);
    while($addedId = mysqli_fetch_array($sqlSelectIds))
    {
        array_push($previousAddedIds, $addedId[0]);
    }
    $account_id = $_POST['account_id'];
    $user_name = $_POST['user_name'];
    $user_id = $_POST['user_id'];
    $user_password = $_POST['user_password'];
    $user_rights = $_POST['user_rights'];
    $reg_date = $_POST['reg_date'];
    $current_status = $_POST['current_status'];
    $user_status = $_POST['user_status'];
    $server_sync = $_POST['server_sync'];

    $account_id = explode("/-_/", $account_id);
    $user_name = explode("/-_/", $user_name);
    $user_id = explode("/-_/", $user_id);
    $user_password = explode("/-_/", $user_password);
    $user_rights = explode("/-_/", $user_rights);
    $reg_date = explode("/-_/", $reg_date);
    $current_status = explode("/-_/", $current_status);
    $user_status = explode("/-_/", $user_status);
    $server_sync = explode("/-_/", $server_sync);

    $insertCheck = false;
    $updateCheck = false;
    $insertQuery = "INSERT INTO `user_accounts`(`software_account_id`, `user_name`, `user_id`, `user_password`, `user_rights`, `reg_date`, `current_status`, `user_status`, `software_lic_id`) VALUES ";
    $updateQuery = "";
    $insertSuccess = "false";
    $updateSuccess = "false";
    for($i=0; $i<sizeof($account_id); $i++)
    {
        if($server_sync[$i] == "Insert" || $server_sync[$i] == "0" || $server_sync[$i] == "")
        {
        
            if(!in_array($account_id[$i], $previousAddedIds))
            {
                if($insertCheck)
                {
                    $insertQuery = $insertQuery.", ";
                }
                $insertQuery = $insertQuery."('$account_id[$i]', '$user_name[$i]', '$user_id[$i]', '$user_password[$i]', '$user_rights[$i]', '$reg_date[$i]', '$current_status[$i]', '$user_status[$i]','$softwareId')";
                $insertCheck = true;

            }
        }
        elseif ($server_sync[$i] == "Update")
        {
            if($updateCheck)
            {
                $updateCheck = $updateCheck."; ";
            }
            $updateCheck = true;
            $updateQuery = $updateQuery."UPDATE `user_accounts` SET `user_name`= '$user_name[$i]',`user_id`= '$user_id[$i]',`user_password`= '$user_password[$i]',`user_rights`= '$user_rights[$i]',`reg_date`= '$reg_date[$i]',`current_status`= '$current_status[$i]',`user_status`= '$user_status[$i]' WHERE `software_account_id` = '$account_id[$i]' AND `software_lic_id` = '$softwareId'";
        }
    }
//    echo $insertQuery.'<br>';
    if($insertCheck && mysqli_query($con, $insertQuery))
    {
        $insertSuccess = "true";
    }
//    echo $updateQuery.'<br>';
    if($updateCheck && mysqli_multi_query($con, $updateQuery))
    {
        $updateSuccess = "true";
    }

    echo "Insert:".$insertSuccess.":Update:".$updateSuccess;
}
elseif($operation == "sync_user_info")
{
    $previousAddedIds = array();
    $sqlSelectIds = "SELECT `software_user_id` FROM `user_info` WHERE `software_lic_id` = '$softwareId'";
    $sqlSelectIds = mysqli_query($con, $sqlSelectIds);
    while($addedId = mysqli_fetch_array($sqlSelectIds))
    {
        array_push($previousAddedIds, $addedId[0]);
    }
    $user_table_id = $_POST['user_table_id'];
    $user_id = $_POST['user_id'];
    $user_name = $_POST['user_name'];
    $user_contact = $_POST['user_contact'];
    $user_address = $_POST['user_address'];
    $user_cnic = $_POST['user_cnic'];
    $user_type = $_POST['user_type'];
    $user_image = $_POST['user_image'];
    $user_status = $_POST['user_status'];
    $creating_user_id = $_POST['creating_user_id'];
    $creating_date = $_POST['creating_date'];
    $creating_time = $_POST['creating_time'];
    $update_user_id = $_POST['update_user_id'];
    $update_date = $_POST['update_date'];
    $update_time = $_POST['update_time'];
    $server_sync = $_POST['server_sync'];

    $user_table_id = explode("/-_/", $user_table_id);
    $user_id = explode("/-_/", $user_id);
    $user_name = explode("/-_/", $user_name);
    $user_contact = explode("/-_/", $user_contact);
    $user_address = explode("/-_/", $user_address);
    $user_cnic = explode("/-_/", $user_cnic);
    $user_type = explode("/-_/", $user_type);
    $user_image = explode("/-_/", $user_image);
    $user_status = explode("/-_/", $user_status);
    $creating_user_id = explode("/-_/", $creating_user_id);
    $creating_date = explode("/-_/", $creating_date);
    $creating_time = explode("/-_/", $creating_time);
    $update_user_id = explode("/-_/", $update_user_id);
    $update_date = explode("/-_/", $update_date);
    $update_time = explode("/-_/", $update_time);
    $server_sync = explode("/-_/", $server_sync);

    $insertCheck = false;
    $updateCheck = false;
    $insertQuery = "INSERT INTO `user_info`(`software_user_id`, `user_id`, `user_name`, `user_contact`, `user_address`, `user_cnic`, `user_type`, `user_image`, `user_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`, `software_lic_id`) VALUES ";
    $updateQuery = "";
    $insertSuccess = "false";
    $updateSuccess = "false";
    for($i=0; $i<sizeof($user_table_id); $i++)
    {
        if($server_sync[$i] == "Insert" || $server_sync[$i] == "0" || $server_sync[$i] == "")
        {
    
            if(!in_array($user_table_id[$i], $previousAddedIds))
            {
                if($insertCheck)
                {
                    $insertQuery = $insertQuery.", ";
                }
                $insertQuery = $insertQuery."('$user_table_id[$i]', '$user_id[$i]', '$user_name[$i]', '$user_contact[$i]', '$user_address[$i]', '$user_cnic[$i]', '$user_type[$i]', '$user_image[$i]', '$user_status[$i]', '$creating_user_id[$i]', '$creating_date[$i]', '$creating_time[$i]', '$update_user_id[$i]', '$update_date[$i]', '$update_time[$i]', '$softwareId')";
                $insertCheck = true;
            }
        }
        elseif ($server_sync[$i] == "Update")
        {
            if($updateCheck)
            {
                $updateCheck = $updateCheck."; ";
            }
            $updateCheck = true;
            $updateQuery = $updateQuery."UPDATE `user_info` SET `user_id`= '$user_id[$i]',`user_name`= '$user_name[$i]',`user_contact`= '$user_contact[$i]',`user_address`= '$user_address[$i]',`user_cnic`= '$user_cnic[$i]',`user_type`= '$user_type[$i]',`user_image`= '$user_image[$i]',`user_status`= '$user_status[$i]',`creating_user_id`= '$creating_user_id[$i]',`creating_date`= '$creating_date[$i]',`creating_time`= '$creating_time[$i]',`update_user_id`= '$update_user_id[$i]',`update_date`= '$update_date[$i]',`update_time`= '$update_time[$i]' WHERE `software_user_id` = '$user_table_id[$i]' AND `software_lic_id` = '$softwareId'";
        }
    }
    // echo $insertQuery.'<br>';
    if($insertCheck && mysqli_query($con, $insertQuery))
    {
        $insertSuccess = "true";
    }
//    echo $updateQuery.'<br>';
    if($updateCheck && mysqli_multi_query($con, $updateQuery))
    {
        $updateSuccess = "true";
    }

    echo "Insert:".$insertSuccess.":Update:".$updateSuccess;
}
elseif($operation == "sync_user_rights")
{
    $previousAddedIds = array();
    $sqlSelectIds = "SELECT `software_rights_id` FROM `user_rights` WHERE `software_lic_id` = '$softwareId'";
    $sqlSelectIds = mysqli_query($con, $sqlSelectIds);
    while($addedId = mysqli_fetch_array($sqlSelectIds))
    {
        array_push($previousAddedIds, $addedId[0]);
    }
    $rights_id = $_POST['rights_id'];
    $user_id = $_POST['user_id'];
    $create_record = $_POST['create_record'];
    $read_record = $_POST['read_record'];
    $edit_record = $_POST['edit_record'];
    $delete_record = $_POST['delete_record'];
    $manage_stock = $_POST['manage_stock'];
    $manage_cash = $_POST['manage_cash'];
    $manage_settings = $_POST['manage_settings'];
    $mobile_app = $_POST['mobile_app'];
    $server_sync = $_POST['server_sync'];

    $rights_id = explode("/-_/", $rights_id);
    $user_id = explode("/-_/", $user_id);
    $create_record = explode("/-_/", $create_record);
    $read_record = explode("/-_/", $read_record);
    $edit_record = explode("/-_/", $edit_record);
    $delete_record = explode("/-_/", $delete_record);
    $manage_stock = explode("/-_/", $manage_stock);
    $manage_cash = explode("/-_/", $manage_cash);
    $manage_settings = explode("/-_/", $manage_settings);
    $mobile_app = explode("/-_/", $mobile_app);
    $server_sync = explode("/-_/", $server_sync);

    $insertCheck = false;
    $updateCheck = false;
    $insertQuery = "INSERT INTO `user_rights`(`software_rights_id`, `user_id`, `create_record`, `read_record`, `edit_record`, `delete_record`, `manage_stock`, `manage_cash`, `manage_settings`, `mobile_app`, `software_lic_id`) VALUES ";
    $updateQuery = "";
    $insertSuccess = "false";
    $updateSuccess = "false";
    for($i=0; $i<sizeof($rights_id); $i++)
    {
        if($server_sync[$i] == "Insert" || $server_sync[$i] == "0" || $server_sync[$i] == "")
        {
            if(!in_array($rights_id[$i], $previousAddedIds))
            {
                if($insertCheck)
                {
                    $insertQuery = $insertQuery.", ";
                }
				$insertCheck = true;
                $insertQuery = $insertQuery."('$rights_id[$i]', '$user_id[$i]', '$create_record[$i]', '$read_record[$i]', '$edit_record[$i]', '$delete_record[$i]', '$manage_stock[$i]', '$manage_cash[$i]', '$manage_settings[$i]', '$mobile_app[$i]', '$softwareId')";

            }
        }
        elseif ($server_sync[$i] == "Update")
        {
            if($updateCheck)
            {
                $updateCheck = $updateCheck."; ";
            }
            $updateCheck = true;
            $updateQuery = $updateQuery."UPDATE `user_rights` SET `user_id`= '$user_id[$i]',`create_record`= '$create_record[$i]',`read_record`= '$read_record[$i]',`edit_record`= '$edit_record[$i]',`delete_record`= '$delete_record[$i]',`manage_stock`= '$manage_stock[$i]',`manage_cash`= '$manage_cash[$i]',`manage_settings`= '$manage_settings[$i]',`mobile_app`= '$mobile_app[$i]' WHERE `software_rights_id` = '$rights_id[$i]' AND `software_lic_id` = '$softwareId'";
        }
    }
//    echo $insertQuery.'<br>';
    if($insertCheck && mysqli_query($con, $insertQuery))
    {
        $insertSuccess = "true";
    }
//    echo $updateQuery.'<br>';
    if($updateCheck && mysqli_multi_query($con, $updateQuery))
    {
        $updateSuccess = "true";
    }

    echo "Insert:".$insertSuccess.":Update:".$updateSuccess;
}

function preProcess($softwareIds)
{
    $requiredOps = [];
    for($i=0; $i<sizeof($softwareIds); $i++)
    {

    }
}

?>