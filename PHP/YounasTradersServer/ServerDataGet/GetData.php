<?php
/**
 * Created by PhpStorm.
 * User: Saffi Ullah 786
 * Date: 20-Sep-21
 * Time: 11:53 PM
 */

require_once "../connection.php";

$data = array();

$reqData = $_GET['required_data'];
$softwareLicId = $_GET['software_lic_id'];

if($reqData == "new_bookings_no")
{
    $sql = "SELECT COUNT(`order_id`) FROM `order_info` WHERE `send_status_computer` = 'Pending' AND `software_lic_id` = '$softwareLicId'";
    $stmt = $con->prepare($sql);
    $stmt->execute();
    $stmt->bind_result($totalOrders);
    while($stmt->fetch()){
        $temp = [
            'new_bookings'=>$totalOrders
        ];
        array_push($data, $temp);
    }
    echo json_encode($data);
}
elseif ($reqData == "new_bookings")
{
    $sql = "SELECT `order_id`, `dealer_id`, `product_id`, `quantity`, `unit`, `order_price`, `bonus`, `discount`, `waive_off_price`, `final_price`, `order_success`, `booking_latitude`, `booking_longitude`, `booking_area`, `booking_date`, `booking_time`, `booking_user_id` FROM `order_info` WHERE `send_status_computer` = 'Pending' AND `software_lic_id` = '$softwareLicId'";
    $stmt = $con->prepare($sql);
    $stmt->execute();
    $stmt->bind_result($orderId, $dealerId, $productId, $quantity, $unit, $orderPrice, $bonus, $discount, $waiveOff, $finalPrice, $orderSuccess, $bookingLatitude, $bookingLongitude, $bookingArea, $bookingDate, $bookingTime, $bookingUserId);
    while($stmt->fetch()){
        $temp = [
            'order_id'=>$orderId,
            'dealer_id'=>$dealerId,
            'product_id'=>$productId,
            'quantity'=>$quantity,
            'unit'=>$unit,
            'order_price'=>$orderPrice,
            'bonus'=>$bonus,
            'discount'=>$discount,
            'waive_off'=>$waiveOff,
            'final_price'=>$finalPrice,
            'order_success'=>$orderSuccess,
            'booking_latitude'=>$bookingLatitude,
            'booking_longitude'=>$bookingLongitude,
            'booking_area'=>$bookingArea,
            'booking_date'=>$bookingDate,
            'booking_time'=>$bookingTime,
            'booking_user_id'=>$bookingUserId
//            'comments'=>($comments == null)? "" : $comments,
//            'status'=>$status
        ];
        array_push($data, $temp);
    }
    echo json_encode($data);
}
elseif ($reqData == "new_bookings_detail")
{
    $sql = "SELECT `order_info_detailed`.`order_id`, `order_info_detailed`.`product_table_id`, `order_info_detailed`.`batch_id`, `order_info_detailed`.`quantity`, `order_info_detailed`.`unit`, `order_info_detailed`.`order_price`, `order_info_detailed`.`bonus_quant`, `order_info_detailed`.`discount`, `order_info_detailed`.`final_price` FROM `order_info_detailed` INNER JOIN `order_info` ON `order_info`.`order_id` = `order_info_detailed`.`order_id` AND `order_info`.`send_status_computer` = 'Pending' AND `order_info`.`software_lic_id` = '$softwareLicId'";
    $stmt = $con->prepare($sql);
    $stmt->execute();
    $stmt->bind_result($orderId, $productId, $batchId, $quantity, $unit, $orderPrice, $bonus, $discount, $finalPrice);
    while($stmt->fetch()){
        $temp = [
            'order_id'=>$orderId,
            'product_id'=>$productId,
            'batch_id'=>$batchId,
            'quantity'=>$quantity,
            'unit'=>$unit,
            'order_price'=>$orderPrice,
            'bonus'=>$bonus,
            'discount'=>$discount,
            'final_price'=>$finalPrice
//            'comments'=>($comments == null)? "" : $comments,
//            'status'=>$status
        ];
        array_push($data, $temp);
    }
    echo json_encode($data);
}
elseif($reqData == "new_dealer_finance_no")
{
    $sql = "SELECT COUNT(`payment_id`) FROM `dealer_payments` WHERE `send_status_computer` = 'Pending' AND `software_lic_id` = '$softwareLicId'";
    $stmt = $con->prepare($sql);
    $stmt->execute();
    $stmt->bind_result($totalNum);
    while($stmt->fetch()){
        $temp = [
            'new_dealer_finance'=>$totalNum
        ];
        array_push($data, $temp);
    }
    echo json_encode($data);
}
elseif ($reqData == "new_dealer_finance")
{
    $sql = "SELECT `payment_id`, `dealer_id`, `order_id`, `amount`, `cash_type`, `date`, `day`, `user_id`, `status` FROM `dealer_payments` WHERE `send_status_computer` = 'Pending' AND `software_lic_id` = '$softwareLicId'";
    $stmt = $con->prepare($sql);
    $stmt->execute();
    $stmt->bind_result($paymentId, $dealerId, $orderId, $amount, $cashType, $date, $day, $userId, $status);
    while($stmt->fetch()){
        $temp = [
            'payment_id'=>$paymentId,
            'dealer_id'=>$dealerId,
            'order_id'=>$orderId,
            'amount'=>$amount,
            'cash_type'=>$cashType,
            'date'=>$date,
            'day'=>$day,
            'user_id'=>$userId,
            'status'=>$status
        ];
        array_push($data, $temp);
    }
    echo json_encode($data);
}
elseif($reqData == "new_returns_no")
{
    $sql = "SELECT COUNT(`return_id`) FROM `order_return` WHERE `send_status_computer` = 'Pending' AND `software_lic_id` = '$softwareLicId'";
    $stmt = $con->prepare($sql);
    $stmt->execute();
    $stmt->bind_result($totalReturns);
    while($stmt->fetch()){
        $temp = [
            'new_returns_no'=>$totalReturns
        ];
        array_push($data, $temp);
    }
    echo json_encode($data);
}
elseif ($reqData == "new_returns")
{
    $sql = "SELECT `return_id`, `dealer_id`, `return_total_price`, `return_gross_price`, `order_id`, `entry_date`, `entry_time`, `enter_user_id` FROM `order_return` WHERE `send_status_computer` = 'Pending' AND `software_lic_id` = '$softwareLicId'";
    $stmt = $con->prepare($sql);
    $stmt->execute();
    $stmt->bind_result($returnId, $dealerId, $returnTotalPrice, $returnGrossPrice, $orderId, $entryDate, $entryTime, $enterUserId);
    while($stmt->fetch()){
        $temp = [
            'return_id'=>$returnId,
            'dealer_id'=>$dealerId,
            'return_total_price'=>$returnTotalPrice,
            'return_gross_price'=>$returnGrossPrice,
            'order_id'=>$orderId,
            'entry_date'=>$entryDate,
            'entry_time'=>$entryTime,
            'enter_user_id'=>$enterUserId
//            'comments'=>($comments == null)? "" : $comments,
//            'status'=>$status
        ];
        array_push($data, $temp);
    }
    echo json_encode($data);
}
elseif ($reqData == "new_returns_detail")
{
    $sql = "SELECT `order_return_detail`.`return_id`, `order_return_detail`.`prod_id`, `order_return_detail`.`prod_batch`, `order_return_detail`.`prod_quant`, `order_return_detail`.`bonus_quant`, `order_return_detail`.`discount_amount`, `order_return_detail`.`total_amount`, `order_return_detail`.`unit`, `order_return_detail`.`return_reason` FROM `order_return_detail` INNER JOIN `order_return` ON `order_return`.`return_id` = `order_return_detail`.`return_id` AND `order_return`.`send_status_computer` = 'Pending' AND `order_return`.`software_lic_id` = '$softwareLicId'";
    $stmt = $con->prepare($sql);
    $stmt->execute();
    $stmt->bind_result($returnId, $productId, $batchNo, $quantity, $bonusQty, $discount, $totalAmount, $unit, $returnReason);
    while($stmt->fetch()){
        $temp = [
            'return_id'=>$returnId,
            'product_id'=>$productId,
            'batch_no'=>$batchNo,
            'quantity'=>$quantity,
            'bonus_qty'=>$bonusQty,
            'discount'=>$discount,
            'total_amount'=>$totalAmount,
            'unit'=>$unit,
            'return_reason'=>$returnReason
//            'comments'=>($comments == null)? "" : $comments,
//            'status'=>$status
        ];
        array_push($data, $temp);
    }
    echo json_encode($data);
}
elseif($reqData == "new_user_accounts_no")
{
    $sql = "SELECT COUNT(`account_id`) FROM `user_accounts` WHERE `send_status_computer` = 'Pending' AND `software_lic_id` = '$softwareLicId'";
    $stmt = $con->prepare($sql);
    $stmt->execute();
    $stmt->bind_result($totalNewUserAccounts);
    while($stmt->fetch()){
        $temp = [
            'new_user_accounts_no'=>$totalNewUserAccounts
        ];
        array_push($data, $temp);
    }
    echo json_encode($data);
}
elseif ($reqData == "new_user_accounts")
{
    $sql = "SELECT `account_id`, `user_name`, `user_id`, `user_password`, `user_rights`, `reg_date`, `current_status`, `user_status` FROM `user_accounts` WHERE `send_status_computer` = 'Pending' AND `software_lic_id` = '$softwareLicId'";
    $stmt = $con->prepare($sql);
    $stmt->execute();
    $stmt->bind_result($accountId, $userName, $userId, $userPassword, $userRights, $regDate, $currentStatus, $userStatus);
    while($stmt->fetch()){
        $temp = [
            'account_id'=>$accountId,
            'user_name'=>$userName,
            'user_id'=>$userId,
            'user_password'=>$userPassword,
            'user_rights'=>$userRights,
            'reg_date'=>$regDate,
            'current_status'=>$currentStatus,
            'user_status'=>$userStatus
        ];
        array_push($data, $temp);
    }
    echo json_encode($data);
}
elseif ($reqData == "mobile_gps_locations")
{
    $sql = "SELECT `mob_loc_id`, `software_user_id`, `latitude`, `longitude`, `loc_name`, `date`, `time` FROM `mobile_gps_location` WHERE `send_status_computer` = 'Pending' AND `software_lic_id` = '$softwareLicId'";
    $stmt = $con->prepare($sql);
    $stmt->execute();
    $stmt->bind_result($mobLocId, $userId, $latitude, $longitude, $locationName, $date, $time);
    while($stmt->fetch()){
        $temp = [
            'mob_loc_id'=>$mobLocId,
            'software_user_id'=>$userId,
            'latitude'=>$latitude,
            'longitude'=>$longitude,
            'loc_name'=>$locationName,
            'date'=>$date,
            'time'=>$time
        ];
        array_push($data, $temp);
    }
    echo json_encode($data);
}
elseif ($reqData == "order_gps_locations")
{
    $sql = "SELECT `order_loc_id`, `order_id`, `user_id`, `latitude`, `longitude`, `loc_name`, `date`, `time`, `dealer_id`, `price`, `type` FROM `order_gps_location` WHERE `send_status_computer` = 'Pending' AND `software_lic_id` = '$softwareLicId'";
    $stmt = $con->prepare($sql);
    $stmt->execute();
    $stmt->bind_result($orderLocId, $orderId, $userId, $latitude, $longitude, $locationName, $date, $time, $dealerId, $price, $type);
    while($stmt->fetch()){
        $temp = [
            'order_loc_id'=>$orderLocId,
            'order_id'=>$orderId,
            'user_id'=>$userId,
            'latitude'=>$latitude,
            'longitude'=>$longitude,
            'loc_name'=>$locationName,
            'date'=>$date,
            'time'=>$time,
            'dealer_id'=>$dealerId,
            'price'=>$price,
            'type'=>$type
        ];
        array_push($data, $temp);
    }
    echo json_encode($data);
}
elseif ($reqData == "district_names")
{
    $sql = "SELECT `district_name` FROM `system_unique_district_info`";
    $stmt = $con->prepare($sql);
    $stmt->execute();
    $stmt->bind_result($districtNames);
    while($stmt->fetch()){
        $temp = [
            'district_name'=>$districtNames
        ];
        array_push($data, $temp);
    }
    echo json_encode($data);
}
elseif ($reqData == "companies_detail")
{
    $businessCategory = $_GET['business_category'];
    $sql = "SELECT `system_company_id`, `company_name`, `company_contact`, `company_address`, `company_email` FROM `system_unique_company_info` WHERE `business_category` = '$businessCategory'";
    $stmt = $con->prepare($sql);
    $stmt->execute();
    $stmt->bind_result($systemCompanyId, $companyName, $companyContact, $companyAddress, $companyEmail);
    while($stmt->fetch()){
        $temp = [
            'system_company_id'=>$systemCompanyId,
            'company_name'=>$companyName,
            'company_contact'=>$companyContact,
            'company_address'=>$companyAddress,
            'company_email'=>$companyEmail
        ];
        array_push($data, $temp);
    }
    echo json_encode($data);
}
elseif ($reqData == "products_detail")
{
    $companyIds = $_GET['company_ids'];
    $companyIds = explode(",", $companyIds);
    $firstIter = true;

    $sql = "SELECT `system_unique_company_info`.`company_name`, `system_unique_product_info`.`product_name`, `system_unique_product_info`.`product_type`, `system_unique_product_info`.`packSize`, `system_unique_product_info`.`carton_size`, `system_unique_product_info`.`purchase_price`, `system_unique_product_info`.`trade_price`, `system_unique_product_info`.`retail_price` FROM `system_unique_product_info` INNER JOIN `system_unique_company_info` ON `system_unique_company_info`.`system_company_id` = `system_unique_product_info`.`system_company_id` WHERE `system_unique_product_info`.`product_status` != 'Deleted'";
  
    if(sizeof($companyIds) > 0)
    {
      $sql .= " AND (";
    }
    for($i=0; $i<sizeof($companyIds); $i++)
    {
        if($firstIter)
        {
            $sql .= "`system_unique_product_info`.`system_company_id` = '$companyIds[$i]'";
            $firstIter = false;
        }
        else
        {
            $sql .= " OR `system_unique_product_info`.`system_company_id` = '$companyIds[$i]'";
        }
    }
    if(sizeof($companyIds) > 0)
    {
      $sql .= ")";
    }
    $sql .= " ORDER BY `system_unique_company_info`.`system_company_id`, `system_unique_product_info`.`product_name`";
    $stmt = $con->prepare($sql);
    $stmt->execute();
    $stmt->bind_result($companyName, $productName, $productType, $packSize, $cartonSize, $purchasePrice, $tradePrice, $retailPrice);
    while($stmt->fetch()){
        $temp = [
            'company_name'=>$companyName,
            'product_name'=>$productName,
            'product_type'=>$productType,
            'pack_size'=>$packSize,
            'carton_size'=>$cartonSize,
            'purchase_price'=>$purchasePrice,
            'trade_price'=>$tradePrice,
            'retail_price'=>$retailPrice
        ];
        array_push($data, $temp);
    }
    echo json_encode($data);
}
elseif ($reqData == "dealers_detail")
{
	$district = $_GET['district_name'];
	$businessCategory = $_GET['business_category'];
    $sql = "SELECT `system_unique_dealer_info`.`system_dealer_id`, `system_unique_dealer_info`.`dealer_name`, `system_unique_dealer_info`.`dealer_contact_person`, `system_unique_dealer_info`.`dealer_phone`, `system_unique_dealer_info`.`dealer_fax`, `system_unique_dealer_info`.`dealer_address`, `system_unique_dealer_info`.`dealer_address1`, `system_unique_dealer_info`.`dealer_type`, `system_unique_dealer_info`.`dealer_cnic`, `system_unique_dealer_info`.`dealer_ntn`, `system_unique_dealer_info`.`dealer_lic9_num`, `system_unique_dealer_info`.`dealer_lic9_exp`, `system_unique_dealer_info`.`dealer_lic10_num`, `system_unique_dealer_info`.`dealer_lic10_Exp`, `system_unique_dealer_info`.`dealer_lic11_num`, `system_unique_dealer_info`.`dealer_lic11_Exp`, `system_unique_dealer_info`.`latitude`, `system_unique_dealer_info`.`longitude`, `system_unique_dealer_info`.`loc_name`  FROM `system_unique_dealer_info` INNER JOIN `system_dealers_business_category` ON `system_dealers_business_category`.`system_dealer_id` = `system_unique_dealer_info`.`system_dealer_id` INNER JOIN `district_info` ON `district_info`.`district_table_id` = `system_unique_dealer_info`.`dealer_district_id` WHERE `district_info`.`district_name` = '$district' AND `system_dealers_business_category`.`business_category` = '$businessCategory' ORDER BY `system_unique_dealer_info`.`dealer_name`";
    $stmt = $con->prepare($sql);
    $stmt->execute();
    $stmt->bind_result($systemDealerId, $dealerName, $dealerContactPerson, $dealerPhone, $dealerFax, $dealerAddress, $dealerAddress1, $dealerType, $dealerCnic, $dealerNtn, $dealerLic9Num, $dealerLic9Exp, $dealerLic10Num, $dealerLic10Exp, $dealerLic11Num, $dealerLic11Exp, $latitude, $longitude, $locationName);
    while($stmt->fetch()){
        $temp = [
            'system_dealer_id'=>$systemDealerId,
            'dealer_name'=>$dealerName,
            'dealer_contact_person'=>$dealerContactPerson,
            'dealer_phone'=>$dealerPhone,
            'dealer_fax'=>$dealerFax,
            'dealer_address'=>$dealerAddress,
            'dealer_address1'=>$dealerAddress1,
            'dealer_type'=>$dealerType,
            'dealer_cnic'=>$dealerCnic,
            'dealer_ntn'=>$dealerNtn,
            'dealer_lic9_num'=>$dealerLic9Num,
            'dealer_lic9_exp'=>$dealerLic9Exp,
            'dealer_lic10_num'=>$dealerLic10Num,
            'dealer_lic10_exp'=>$dealerLic10Exp,
            'dealer_lic11_num'=>$dealerLic11Num,
            'dealer_lic11_exp'=>$dealerLic11Exp,
            'latitude'=>$latitude,
            'longitude'=>$longitude,
            'location_name'=>$locationName
        ];
        array_push($data, $temp);
    }
    echo json_encode($data);
}
?>