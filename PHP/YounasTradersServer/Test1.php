<?php
/**
 * Created by PhpStorm.
 * User: Saffi Ullah 786
 * Date: 26-Mar-20
 * Time: 11:40 PM
 */
require_once "connection.php";

$mobileCurrLoc = "SELECT `mobile_curr_locations`.*, `salesman_info`.`salesman_name` FROM `mobile_curr_locations` INNER JOIN `user_accounts` ON `user_accounts`.`user_id` = `mobile_curr_locations`.`user_id` INNER JOIN `salesman_info` ON `salesman_info`.`salesman_id` = `mobile_curr_locations`.`user_id` WHERE`user_accounts`.`current_status` = 'on' ORDER BY `user_accounts`.`user_id`";
$mobileCurrLoc = mysqli_query($con, $mobileCurrLoc);
$currActive=0;
while($currData = mysqli_fetch_array($mobileCurrLoc))
{
    $currUserId[$currActive] = $currData[0];
    $currLat[$currActive] = $currData[1];
    $currLng[$currActive] = $currData[2];
    $currLocName[$currActive] = $currData[3];
    $currUserName[$currActive] = $currData[4];
    $currActive++;
}

$timeStampLoc = "SELECT `mobile_gps_location`.* FROM `mobile_gps_location` INNER JOIN `user_accounts` ON `user_accounts`.`user_id` = `mobile_gps_location`.`user_id` WHERE`user_accounts`.`current_status` = 'on' ORDER BY `user_accounts`.`user_id`";
$timeStampLoc = mysqli_query($con, $timeStampLoc);
$timeStampCount=0;
while($timeStampData = mysqli_fetch_array($timeStampLoc))
{
    $timeStampUserId[$timeStampCount] = $timeStampData[1];
    $timeStampLat[$timeStampCount] = $timeStampData[2];
    $timeStampLng[$timeStampCount] = $timeStampData[3];
    $timeStampLocName[$timeStampCount] = $timeStampData[4];
    $timeStampTime[$timeStampCount] = $timeStampData[6];
    $timeStampCount++;
}

$orderLoc = "SELECT `order_gps_location`.*, `dealer_info`.`dealer_name` FROM `order_gps_location` INNER JOIN `user_accounts` ON `user_accounts`.`user_id` = `order_gps_location`.`user_id` INNER JOIN `dealer_info` ON `dealer_info`.`dealer_id` = `order_gps_location`.`dealer_id` WHERE`user_accounts`.`current_status` = 'on' ORDER BY `user_accounts`.`user_id`";
$orderLoc = mysqli_query($con, $orderLoc);
$orderLocCount=0;
while($orderLocData = mysqli_fetch_array($orderLoc))
{
    $orderLocUserId[$orderLocCount] = $orderLocData[1];
    $orderLocLat[$orderLocCount] = $orderLocData[2];
    $orderLocLng[$orderLocCount] = $orderLocData[3];
    $orderLocName[$orderLocCount] = $orderLocData[4];
    $orderLocTime[$orderLocCount] = $orderLocData[6];
    $orderLocPrice[$orderLocCount] = $orderLocData[8];
    $orderLocType[$orderLocCount] = $orderLocData[9];
    $orderLocDealerName[$orderLocCount] = $orderLocData[10];
    $orderLocCount++;
}


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>My Google Map</title>
    <style>
        #map{
            height:600px;
            width:100%;
        }
    </style>
</head>
<body>
<h1>My Google Map</h1>
<div id="map"></div>
<script>
    var map;
    var markers = [];
    var markersCurrLocations = [[]];
    var markersNewLocations = [[]];
    var timeStampMarkers = [[]];
    var orderLocMarkers = [[]];
    var markerToMove = [];
    var positionOfMarker = [[]];
    var result = [[]];
    function initMap(){
        // Map options
        var options = {
            zoom:12,
            center:{lat:32.551158,lng:74.070952}
        }

        // New map
        map = new google.maps.Map(document.getElementById('map'), options);

        // Listen for click on map
        google.maps.event.addListener(map, 'click', function(event){
            // Add marker
            addMarker({coords:event.latLng});
        });

        iconCurrLoc = {
            url: "imgs/gps markers/curr_loc_1.png", // url
            scaledSize: new google.maps.Size(30, 30) // scaled size
        };
        iconDealerLoc = {
            url: "imgs/gps markers/dealer_loc_1.png", // url
            scaledSize: new google.maps.Size(30, 30) // scaled size
        };

        iconBooking = {
            url: "imgs/gps markers/booking_1.png", // url
            scaledSize: new google.maps.Size(30, 30) // scaled size
        };

        iconCashCollection = {
            url: "imgs/gps markers/cash_collection_1.png", // url
            scaledSize: new google.maps.Size(30, 30) // scaled size
        };

        iconDelivered = {
            url: "imgs/gps markers/delivered_1.png", // url
            scaledSize: new google.maps.Size(30, 30) // scaled size
        };

        iconTimeStamp = {
            url: "imgs/gps markers/time_stamp_1.png", // url
            scaledSize: new google.maps.Size(30, 30) // scaled size
        };

        currActiveMarkers = [];
        <?php
        for($curri=0; $curri<$currActive; $curri++)
        {
        ?>
        temp = [];
        currActiveMarkers[<?php echo $curri; ?>] = {coords:{lat:<?php echo $currLat[$curri]; ?>,lng:<?php echo $currLng[$curri]; ?>}, iconImage:iconCurrLoc, content:'<p><?php echo $currUserName[$curri]; ?></p><p><?php echo $currLocName[$curri]; ?></p>'};
        <?php
        }
        ?>
        for(var x=0; x<currActiveMarkers.length; x++)
        {
            window.alert(currActiveMarkers[x]);
        }

        temp = [];
        // Array of Time Stamp Location Markers
        <?php
        $curri = 0;
        $stampsCount = 0;
        for($timei = 0; $timei<$timeStampCount; $timei++)
        {
            $preUserId = $currUserId[$curri];
            $newUserId = $timeStampUserId[$timei];
            if($preUserId == $newUserId)
            {
                ?>
                temp[<?php echo $stampsCount; ?>] = {
                coords: {
                    lat:<?php echo $timeStampLat[$timei]; ?>,
                    lng:<?php echo $timeStampLng[$timei]; ?>},
                 iconImage: iconTimeStamp,
                 content: '<p><?php echo $timeStampTime[$timei]; ?></p><p><?php echo $timeStampLocName[$timei]; ?>}</p>'
                };
        window.alert("Temp Var In: "+temp[<?php echo $stampsCount; ?>]);
                <?php
                $stampsCount++;
            }
            else
            {
                ?>
                timeStampMarkers[<?php echo $curri; ?>] = temp;
        window.alert("TimeStamp Marker In: "+timeStampMarkers[<?php echo $curri; ?>]);
                temp = [];
                <?php
                if($curri < $currActive)
                {
                    $curri++;
                    $stampsCount = 0;
                }
                ?>
                temp[<?php echo $stampsCount; ?>] = {
                    coords: {
                        lat:<?php echo $timeStampLat[$timei]; ?>,
                        lng:<?php echo $timeStampLng[$timei]; ?>},
                    iconImage: iconTimeStamp,
                    content: '<p><?php echo $timeStampTime[$timei]; ?></p><p><?php echo $timeStampLocName[$timei]; ?>}</p>'
                };
        window.alert("Temp Var In: "+temp[<?php echo $stampsCount; ?>]);
                <?php
                $stampsCount++;
            }
            ?>
        window.alert("Temp Var Out: "+temp[<?php echo $stampsCount-1; ?>]);
        <?php
        }
        if($currActive > 0)
        {
            ?>
            timeStampMarkers[<?php echo $curri; ?>] = temp;
        window.alert("TimeStamp Marker In: "+timeStampMarkers[<?php echo $curri; ?>]);
            <?php
        }
        ?>
        for(var x=0; x<timeStampMarkers.length; x++)
        {
            for(var y=0; y<timeStampMarkers[x].length; y++)
            {
                // window.alert(timeStampMarkers[x][y]);
            }
        }
        // Loop through markers
        for(var i = 0;i < currActiveMarkers.length;i++){
            // Add marker
            addMarker(currActiveMarkers[i], i);
        }

        // Add Marker Function
        function addMarker(props, x){
            var marker = new google.maps.Marker({
                position:props.coords,
                map:map,
                //icon:props.iconImage
            });

            // Check for customicon
            if(props.iconImage){
                // Set icon image
                marker.setIcon(props.iconImage);
            }

            // Check content
            if(props.content){
                var infoWindow = new google.maps.InfoWindow({
                    content:props.content
                });

                marker.addListener('click', function(){
                    infoWindow.open(map, marker);
                    for(i=0; i<timeStampMarkers[x].length; i++)
                    {
                        window.alert(i);
                        addMarker(timeStampMarkers[x][i]);
                    }
                    // addMarker(markers[2]);
                });
            }
        }
    }
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCf9uRA1pO0eWauuPVKjQmSujiChpBvtng&callback=initMap">
</script>
</body>
</html>

