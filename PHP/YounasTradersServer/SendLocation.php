<?php
/**
 * Created by PhpStorm.
 * User: Saffi Ullah 786
 * Date: 24-Mar-20
 * Time: 1:13 AM
 */

require_once "connection.php";
date_default_timezone_set("Asia/Karachi");
$date = date("d/M/Y");
$time = date("g:i A");

$type = $_GET["type"];
$softwareId = $_GET['softwareid'];
if($type == "TimeStamp")
{
    $userId = $_POST["user_id"];
    $latitude = $_POST["latitude"];
    $longitude = $_POST["longitude"];
    $locationName = $_POST["location_name"];
    $date = $_POST["date"];
    $time = $_POST["time"];

    $addTimeStampLocation = "INSERT INTO `mobile_gps_location`(`software_user_id`, `latitude`, `longitude`, `loc_name`, `date`, `time`, `software_lic_id`) VALUES ('$userId','$latitude','$longitude','$locationName','$date','$time', '$softwareId')";
    mysqli_query($con, $addTimeStampLocation);
    mysqli_close($con);
}
elseif($type == "DealerPinLoc")
{
    $dealerId = $_POST["dealer_id"];
    $latitude = $_POST["latitude"];
    $longitude = $_POST["longitude"];
    $locationName = $_POST["location_name"];
    $date = $_POST["date"];
    $time = $_POST["time"];
    $userId = $_POST["user_id"];

    $addDealerGPSLocation = "INSERT INTO `dealer_gps_location`(`dealer_id`, `latitude`, `longitude`, `loc_name`, `entry_date`, `entry_time`, `update_date`, `update_time`, `user_id`, `software_lic_id`) VALUES ('$dealerId','$latitude','$longitude','$locationName', '$date', '$time', '', '', '$userId', '$softwareId')";
    mysqli_query($con, $addDealerGPSLocation);
    mysqli_close($con);
}
elseif($type == "currentlocationtime")
{
    $userId = $_POST["user_id"];
    $date = $_POST["date"];
    $time = $_POST["time"];

//    $userId = "18";
//    $latitude = "31.5604343";
//    $longitude = "74.2995343";
//    $locationName = "Lahore";
//    $date = "02/Mar/2022";
//    $time = "11:10 PM";


    $updateCurrLocation = "UPDATE `mobile_curr_locations` SET `update_date`='$date',`update_time`='$time' WHERE `software_lic_id` = '$softwareId' AND `software_user_id` = '$userId'";
    mysqli_query($con, $updateCurrLocation);
}
else
{
    $userId = $_POST["user_id"];
    $latitude = $_POST["latitude"];
    $longitude = $_POST["longitude"];
    $locationName = $_POST["location_name"];
    $date = $_POST["date"];
    $time = $_POST["time"];

//    $userId = "18";
//    $latitude = "31.5604343";
//    $longitude = "74.2995343";
//    $locationName = "Lahore";
//    $date = "02/Mar/2022";
//    $time = "11:10 PM";


    $updateCurrLocation = "UPDATE `mobile_curr_locations` SET `latitude`='$latitude',`longitude`='$longitude',`location_name`='$locationName',`update_date`='$date',`update_time`='$time' WHERE `software_lic_id` = '$softwareId' AND `software_user_id` = '$userId'";
    mysqli_query($con, $updateCurrLocation);
    $num = mysqli_affected_rows($con);
    echo $num;
    if($num == 0)
    {
        $checkRowsCount = "SELECT COUNT(`curr_loc_id`) FROM `mobile_curr_locations` WHERE `software_lic_id` = '$softwareId' AND `software_user_id` = '$userId'";
        $checkRowsCount = mysqli_query($con, $checkRowsCount);
        $checkRowsCount = mysqli_fetch_array($checkRowsCount)[0];
        if($checkRowsCount > 0)
        {
            $deleteRows = "DELETE FROM `mobile_curr_locations` WHERE `software_lic_id` = '$softwareId' AND `software_user_id` = '$userId'";
            mysqli_query($con, $deleteRows);
        }
        $addCurrLocation = "INSERT INTO `mobile_curr_locations`(`software_user_id`, `latitude`, `longitude`, `location_name`, `update_date`, `update_time`, `software_lic_id`) VALUES ('$userId','$latitude','$longitude','$locationName', '$date', '$time', '$softwareId')";
        mysqli_query($con, $addCurrLocation);
    }

}

?>