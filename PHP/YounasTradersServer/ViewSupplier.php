<?php
/**
 * Created by PhpStorm.
 * User: Saffi Ullah 786
 * Date: 27-Oct-19
 * Time: 7:10 PM
 */
require_once "connection.php";

$getCompanyInfo = "SELECT `company_id`, `company_name` FROM `company_info`";
$getCompanyInfo = mysqli_query($con, $getCompanyInfo);
$comCount = 0;
while($comData = mysqli_fetch_array($getCompanyInfo))
{
    $comIDMain[$comCount] = $comData[0];
    $comNameMain[$comCount] = $comData[1];
    $comCount++;
}

$suppliersInfo = "SELECT * FROM `supplier_info`";
$suppliersInfo = mysqli_query($con, $suppliersInfo);
$i=0;
while ($data = mysqli_fetch_array($suppliersInfo))
{
    $supplierID[$i] = $data[0];
    $companyID[$i] = $data[1];
    $supplierName[$i] = $data[2];
    $supplierContact[$i] = $data[3];
    $supplierAddress[$i] = $data[4];
    $supplierStatus[$i] = $data[5];

    $comIndex = array_search($companyID[$i], $comIDMain);
    $companyName[$i] = $comNameMain[$comIndex];

    $i++;
}
?>

<html>
<head>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="css/font-awesome.min.css">
    <link type="text/css" rel="stylesheet" href="css/style.css">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>

    <style>
        .operations:hover
        {
            color: #D10024;
            cursor: pointer;
        }
        th
        {
            font-size: 15px;
        }
        td
        {
            font-size: 13px;
        }
    </style>
    <script>
        var supplierID = 0;
    </script>
</head>
<body>

<?php
require_once "PopupModel.php";
?>

<div class="container">
    <div style="margin-top: 20px">
        <table id="SuppliersData" class="display">
            <thead>
            <div>
                <tr>
                    <th style="text-align: center; width: 2%">ID</th>
                    <th style="text-align: center; width: 2%">Company Name</th>
                    <th style="text-align: center; width: 2%">Name</th>
                    <th style="text-align: center; width: 2%">Contact</th>
                    <th style="text-align: center; width: 2%">Address</th>
                    <th style="text-align: center; width: 2%">Status</th>
                    <th style="text-align: center; width: 2%">Operations</th>
                </tr>
            </div>
            </thead>
            <tbody>
            <?php
            for($j=0; $j<$i; $j++)
            {
                ?>
                <tr>
                    <td style="text-align: center"><?php echo $supplierID[$j]; ?></td>
                    <td style="text-align: center"><?php echo $companyName[$j]; ?></td>
                    <td style="text-align: center"><?php echo $supplierName[$j]; ?></td>
                    <td style="text-align: center"><?php echo $supplierContact[$j]; ?></td>
                    <td style="text-align: center"><?php echo $supplierAddress[$j]; ?></td>
                    <td style="text-align: center"><?php echo $supplierStatus[$j]; ?></td>
                    <td style="text-align: center">
                        <div style="margin-top: 10px">
                            <a data-toggle="modal" data-target="#myModalDel" onclick="delSupplier('<?php echo $supplierID[$j]; ?>')">
                                <i class="fa-lg fa fa-trash operations" title="Delete"></i>&nbsp;&nbsp;
                            </a>
                            <a data-toggle="modal" data-target="#myModalEditSupplier" onclick="editData('<?php echo $supplierID[$j]?>', '<?php echo $companyID[$j]; ?>', '<?php echo $supplierName[$j]; ?>', '<?php echo $supplierContact[$j]; ?>', '<?php echo $supplierAddress[$j]; ?>', '<?php echo $supplierStatus[$j]; ?>')">
                                <i class="fa-lg fa fa-edit operations" title="Edit"></i>
                            </a>
                        </div>
                    </td>
                </tr>

                <?php
            }
            ?>

            </tbody>
        </table>
    </div>
</div>

<script src="js/bootstrap.min.js"></script>
<script src="js/slick.min.js"></script>
<script src="js/nouislider.min.js"></script>
<script src="js/jquery.zoom.min.js"></script>
<script src="js/main.js"></script>

<script>
    $(document).ready(function() {
        $('#SuppliersData').DataTable(
            {
                // "Item No": [[ 0, "desc" ]]
            }
        );
    } );

    function delSupplier(givenID) {
        supplierID = givenID;
    }

    function delConfirmation(op) {
        if(op == 'yes')
        {
            window.location.href = 'SendData.php?table=supplier_info&op=del&id='+supplierID;
        }
        else
        {
            return;
        }
    }

    function editData(id, comId, name, contact, address, status) {
        document.getElementById('sup_id').value = id;
        document.getElementById('sup_companyname').value = comId;
        document.getElementById('sup_name').value = name;
        document.getElementById('sup_contact').value = contact;
        document.getElementById('sup_address').value = address;
        document.getElementById('sup_status').value = status;
    }

    function updateData(op)
    {
        if(op == 'yes')
        {
            sup_id = document.getElementById('sup_id').value;
            sup_companyid = document.getElementById('sup_companyname').value;
            sup_name = document.getElementById('sup_name').value;
            sup_contact = document.getElementById('sup_contact').value;
            sup_address = document.getElementById('sup_address').value;
            sup_status = document.getElementById('sup_status').value;

            window.location.href = 'SendData.php?table=supplier_info&op=update&id='+sup_id+'&companyid='+sup_companyid+'&name='+sup_name+'&contact='+sup_contact+'&address='+sup_address+'&status='+sup_status;
        }
        else
        {

        }

    }
</script>
</body>
</html>
