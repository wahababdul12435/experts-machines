package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.*;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.util.Duration;
import model.*;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class ViewArea implements Initializable {
    @FXML
    private TableView<ViewAreaInfo> table_viewarea;

    @FXML
    private TableColumn<ViewAreaInfo, String> sr_no;

    @FXML
    private TableColumn<ViewAreaInfo, String> area_id;

    @FXML
    private TableColumn<ViewAreaInfo, String> city_name;

    @FXML
    private TableColumn<ViewAreaInfo, String> area_name;

    @FXML
    private TableColumn<ViewAreaInfo, String> dealers_present;

    @FXML
    private TableColumn<ViewAreaInfo, String> area_status;

    @FXML
    private TableColumn<ViewAreaInfo, String> operations;

    @FXML
    private BorderPane menu_bar1;
    @FXML
    private Menu pending_orders;

    @FXML
    private StackPane stackPane;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private BorderPane nav_setup;
    @FXML    private AnchorPane inner_anchor;
    @FXML    private Pane btnpane;
    @FXML    private Button btn11;
    @FXML    private FontAwesomeIconView FA_Icon;

    @FXML
    private AnchorPane filter_pane;

    @FXML
    private HBox filter_hbox;

    @FXML
    private JFXTextField txt_area_id;

    @FXML
    private JFXTextField txt_area_name;

    @FXML
    private JFXComboBox<String> txt_city_name;

    @FXML
    private JFXComboBox<String> txt_area_status;

    @FXML
    private HBox summary_hbox;

    @FXML
    private Label lbl_total;

    @FXML
    private Label lbl_active;

    @FXML
    private Label lbl_inactive;

    public ObservableList<ViewAreaInfo> areasDetail;
    PendingOrders objPendingOrders;
    MysqlCon objMysqlCon;
    Statement objStmt;
    Connection objCon;
    private int summaryTotal;
    private int summaryActive;
    private int summaryInActive;

    private ArrayList<String> cityIds = new ArrayList<>();
    private ArrayList<String> cityNames = new ArrayList<>();

    public static String txtAreaId = "";
    public static String txtAreaName = "";
    public static String txtCityId = "All";
    public static String txtCityName = "";
    public static String txtAreaStatus = "";
    public static boolean filter;

    public static Button btnView;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnSetup.setStyle("-fx-background-color: #47ab1e");
        objMysqlCon = new MysqlCon();
        objStmt = objMysqlCon.stmt;
        objCon = objMysqlCon.con;
        ViewAreaInfo.stackPane = stackPane;

        AreaInfo objAreaInfo = new AreaInfo();
        cityIds = objAreaInfo.getSavedCityIds();
        cityNames = objAreaInfo.getSavedCityNames();
        txt_city_name.getItems().addAll(cityNames);

        inner_anchor.setLeftAnchor(inner_anchor, 280d);
        drawer.setSidePane(nav_setup);
        drawer.open();

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        area_id.setCellValueFactory(new PropertyValueFactory<>("areaId"));
        city_name.setCellValueFactory(new PropertyValueFactory<>("cityName"));
        area_name.setCellValueFactory(new PropertyValueFactory<>("areaName"));
        dealers_present.setCellValueFactory(new PropertyValueFactory<>("dealersPresent"));
        area_status.setCellValueFactory(new PropertyValueFactory<>("areaStatus"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_viewarea.setItems(parseUserList());
        lbl_total.setText("Areas\n"+summaryTotal);
        lbl_active.setText("Active\n"+summaryActive);
        lbl_inactive.setText("In Active\n"+summaryInActive);
    }

    private ObservableList<ViewAreaInfo> parseUserList(){
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        ViewAreaInfo objViewAreaInfo = new ViewAreaInfo();
        areasDetail = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> areaData;
        if(filter)
        {
            areaData  = objViewAreaInfo.getAreasSearch(objStmt, objCon, txtAreaId, txtAreaName, txtCityId, txtAreaStatus);
            txt_area_id.setText(txtAreaId);
            txt_area_name.setText(txtAreaName);
            txt_city_name.setValue(txtCityName);
            txt_area_status.setValue(txtAreaStatus);
            filter = false;
            filter_pane.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
        }
        else
        {
            areaData = objViewAreaInfo.getAreasInfo(objStmt, objCon);
        }
        for (int i = 0; i < areaData.size(); i++)
        {
            summaryTotal++;
            if(areaData.get(i).get(6).equals("Active"))
            {
                summaryActive++;
            }
            else if(areaData.get(i).get(6).equals("In Active"))
            {
                summaryInActive++;
            }
            areasDetail.add(new ViewAreaInfo(String.valueOf(i+1), areaData.get(i).get(0), ((areaData.get(i).get(1) == null) ? "N/A" : areaData.get(i).get(1)), areaData.get(i).get(2), ((areaData.get(i).get(3) == null) ? "N/A" : areaData.get(i).get(3)), areaData.get(i).get(4), areaData.get(i).get(5), areaData.get(i).get(6)));
        }
        return areasDetail;
    }

    @FXML
    void btnSearch(ActionEvent event) {
        txtAreaId = txt_area_id.getText();
        txtAreaName = txt_area_name.getText();
        txtCityName = txt_city_name.getValue();
        if(txt_city_name.getSelectionModel().getSelectedIndex() >= 1)
        {
            txtCityId = String.valueOf(cityIds.get(txt_city_name.getSelectionModel().getSelectedIndex()-1));
        }
        else
        {
            txtCityId = "All";
        }
        txtAreaStatus = txt_area_status.getValue();
        if(txtAreaStatus == null)
        {
            txtAreaStatus = "All";
        }
        filter = true;

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/view_area.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void btnReset(ActionEvent event) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/view_area.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void exportExcel(ActionEvent event) throws IOException {
        Workbook workbook = new HSSFWorkbook();
        Sheet spreadsheet = workbook.createSheet("data");
        Row row = spreadsheet.createRow(0);

        for (int j = 0; j < table_viewarea.getColumns().size()-1; j++) {
            row.createCell(j).setCellValue(table_viewarea.getColumns().get(j).getText());
        }

        for (int i = 0; i < table_viewarea.getItems().size(); i++) {
            row = spreadsheet.createRow(i + 1);
            for (int j = 0; j < table_viewarea.getColumns().size()-1; j++) {
                if(table_viewarea.getColumns().get(j).getCellData(i) != null) {
                    row.createCell(j).setCellValue(table_viewarea.getColumns().get(j).getCellData(i).toString());
                }
                else {
                    row.createCell(j).setCellValue("");
                }
            }
        }

        FileOutputStream fileOut = new FileOutputStream("data/View_Areas.xls");
        workbook.write(fileOut);
        fileOut.close();
    }

    public void follow() throws IOException {
        if (drawer.isOpened()) {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(-250);
            transition.setByY(0);
            transition.setCycleCount(1);
            transition.play();
            drawer.close();
            FA_Icon.setGlyphName("ANGLE_DOUBLE_RIGHT");
            FA_Icon.setGlyphSize(14);
            //inner_anchor.setRightAnchor(inner_anchor,1d);
            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(-90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration, inner_anchor);
            scale.setToX(1.1000);
            scale.setToY(1);

            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale, anchorePaneTransition, transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();

        } else {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(250);
            transition.setByY(0);
            transition.setCycleCount(1);
            FA_Icon.setGlyphName("ANGLE_DOUBLE_LEFT");
            FA_Icon.setGlyphSize(14);
            drawer.open();

            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration, inner_anchor);
            scale.setToX(0.99947777);
            scale.setToY(1);


            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale, anchorePaneTransition, transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();

        }
    }

    @FXML
    void addArea() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/enter_area.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
}
