package controller;

import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.transitions.hamburger.HamburgerBackArrowBasicTransition;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.util.StringConverter;
import model.DashboardReportInfo;
import model.GlobalVariables;
import model.MysqlCon;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;

public class DashboardReport implements Initializable {

    @FXML
    private BorderPane menu_bar;
    @FXML
    private JFXHamburger hamburger;
    @FXML
    private JFXDrawer drawer;
    @FXML
    private VBox Vbox_btns;
    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private JFXDatePicker txt_from_date;

    @FXML
    private JFXDatePicker txt_to_date;

    @FXML
    private VBox vbox_top_dealers;

    @FXML
    private PieChart cash_in_out_chart;

    @FXML
    private BarChart<String, String> city_booking_chart;

    @FXML
    private CategoryAxis cityNameAxis;

    @FXML
    private NumberAxis bookingPriceAxis;

    @FXML
    private VBox vbox_top_users;

    @FXML
    private Button btn11;
    @FXML
    private Pane btnpane;
    @FXML
    private FontAwesomeIconView FA_Icon;
    @FXML
    private BorderPane brdrpane_navreport;

    public static Button btnView;
    public static String txtFromDate;
    public static String txtToDate;
    public static boolean filter;

    DashboardReportInfo objDashboardReportInfo;
    ArrayList<ArrayList<String>> dealersData = new ArrayList<>();
    ArrayList<ArrayList<String>> usersData = new ArrayList<>();
    ArrayList<ArrayList<String>> cashInOutData = new ArrayList<>();
    ArrayList<ArrayList<String>> cityBookingData = new ArrayList<>();
    MysqlCon objMysqlCon;
    Statement objStmt;
    Connection objCon;
    private String stStartDate;
    private String stEndDate;
    SimpleDateFormat fmt = new SimpleDateFormat("dd/MMM/yyyy");

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnReport.setStyle("-fx-background-color: #47ab1e");
        objDashboardReportInfo = new DashboardReportInfo();
        objMysqlCon = new MysqlCon();
        objStmt = objMysqlCon.stmt;
        objCon = objMysqlCon.con;

        txt_from_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        txt_to_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        cityNameAxis.setLabel("City Names");
        bookingPriceAxis.setLabel("Booking Price");

        if(filter)
        {
            if (txtFromDate != null && !txtFromDate.equals("")) {
                txt_from_date.setValue(GlobalVariables.LOCAL_DATE(txtFromDate));
                stStartDate = txtFromDate;
            }
            if (txtToDate != null && !txtToDate.equals("")) {
                txt_to_date.setValue(GlobalVariables.LOCAL_DATE(txtToDate));
                stEndDate = txtToDate;
            }
            filter = false;
        }
        else
        {
            setDateRange();
        }

        dealersData = objDashboardReportInfo.getTopDealerDetail(objStmt, objCon, stStartDate, stEndDate);
        if(dealersData.size() == 0)
        {
            Label lblEmpty = new Label("No Data Found");
            vbox_top_dealers.getChildren().add(lblEmpty);
        }
        for(int j=0; j<dealersData.size(); j++)
        {
            vbox_top_dealers.getChildren().add(createDealerPane(dealersData.get(j).get(0), dealersData.get(j).get(1), dealersData.get(j).get(2), dealersData.get(j).get(3), dealersData.get(j).get(4)));
        }

        usersData = objDashboardReportInfo.getTopUsersDetail(objStmt, objCon, stStartDate, stEndDate);
        if(usersData.size() == 0)
        {
            Label lblEmpty = new Label("No Data Found");
            vbox_top_users.getChildren().add(lblEmpty);
        }
        for(int j=0; j<usersData.size(); j++)
        {
            vbox_top_users.getChildren().add(createUserPane(usersData.get(j).get(0), usersData.get(j).get(1), usersData.get(j).get(2), usersData.get(j).get(3), usersData.get(j).get(4)));
        }

        cashInOutData = objDashboardReportInfo.getCashInOutDetail(objStmt, objCon, stStartDate, stEndDate);
        ArrayList<PieChart.Data> slice2 = new ArrayList<>();
        for(int i=0; i<cashInOutData.size(); i++)
        {
            PieChart.Data slice22 = new PieChart.Data(cashInOutData.get(i).get(0), Double.parseDouble(cashInOutData.get(i).get(1)));
            slice2.add(slice22);
            cash_in_out_chart.getData().add(slice2.get(i));
        }

        cityBookingData = objDashboardReportInfo.getCityBookingDetail(objStmt, objCon, stStartDate, stEndDate);
        XYChart.Series dataSeries1 = new XYChart.Series();
        for(int i=0; i<cityBookingData.size(); i++)
        {
            dataSeries1.getData().add(new XYChart.Data(cityBookingData.get(i).get(0), Integer.parseInt(cityBookingData.get(i).get(1))));
        }
        city_booking_chart.getData().add(dataSeries1);




        btnpane.setOnMouseEntered(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(22);
                btnpane.setMinWidth(20.0);
                btn11.setMinWidth(20.0);

            }
        });
        btnpane.setOnMouseExited(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(14);
                btnpane.setMinWidth(10.0);
                btn11.setMinWidth(10.0);
            }
        });

        inner_anchor.setLeftAnchor(inner_anchor, 260d);
        drawer.setSidePane(brdrpane_navreport);
        drawer.open();
    }

    @FXML
    void btnSearch(ActionEvent event) {
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date selectedDate = null;
        if(txt_from_date.getValue() != null)
        {
            txtFromDate = txt_from_date.getValue().toString();
            try {
                selectedDate = sdf.parse(txtFromDate);
                txtFromDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if(txt_to_date.getValue() != null)
        {
            txtToDate = txt_to_date.getValue().toString();
            try {
                selectedDate = sdf.parse(txtToDate);
                txtToDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        filter = true;

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/dashboard_reports.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    public void follow() throws IOException
    {
        if (drawer.isOpened()) {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(-250);
            transition.setByY(0);
            transition.setCycleCount(1);
            transition.play();
            drawer.close();
            FA_Icon.setGlyphName("ANGLE_DOUBLE_RIGHT");
            FA_Icon.setGlyphSize(14);
            //inner_anchor.setRightAnchor(inner_anchor,1d);


            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(-90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(1.31017);
            scale.setToY(1);



            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();
            //
            //inner_anchor.setLeftAnchor(inner_anchor, 5d);
        } else {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(250);
            transition.setByY(0);
            transition.setCycleCount(1);
            FA_Icon.setGlyphName("ANGLE_DOUBLE_LEFT");
            FA_Icon.setGlyphSize(14);
            drawer.open();

            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(0.99947777);
            scale.setToY(1);



            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();

        }
    }

    private void setDateRange()
    {
        stEndDate = GlobalVariables.getStDate();
        Date endDate = null;
        try {
            endDate = fmt.parse(stEndDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(endDate);
        cal1.add(Calendar.DAY_OF_MONTH, -120);
        stStartDate = fmt.format(cal1.getTime());
        txt_from_date.setValue(GlobalVariables.LOCAL_DATE(stStartDate));
        txt_to_date.setValue(GlobalVariables.LOCAL_DATE(stEndDate));
    }

    private AnchorPane createDealerPane(String dealerId, String dealerName, String bookings, String bookingPrice, String imageUrl)
    {
        String css = "view/css/theme-green.css";
        AnchorPane objAnchorPane = new AnchorPane();
        objAnchorPane.getStylesheets().add(css);
        objAnchorPane.getStyleClass().add("users_pane");

        HBox objHbox;
        ImageView objImageView = new ImageView();
        File file = new File(imageUrl);
        boolean exists = file.exists();
        if(exists)
        {
            Image image = new Image(file.toURI().toString());
            objImageView.setImage(image);
        }
        else
        {
            file = new File("src/view/images/users/empty_user.jpg");
            Image image = new Image(file.toURI().toString());
            objImageView.setImage(image);
        }
        objImageView.setFitWidth(80);
        objImageView.setFitHeight(80);

        VBox objVBox;
        Label lblDealerName = new Label(dealerName);
        lblDealerName.setStyle("-fx-text-fill: #a4a4a4; -fx-font-style: italic");
        Separator sep1 = new Separator();
        Label lblBooking = new Label("Bookings: "+bookings);
        lblBooking.setStyle("-fx-text-fill: #a4a4a4; -fx-font-style: italic");
        Separator sep2 = new Separator();
        Label lblBookingPrice = new Label("Booking Price: Rs."+String.format("%,.0f", Float.parseFloat(bookingPrice)));
        lblBookingPrice.setStyle("-fx-text-fill: #a4a4a4; -fx-font-style: italic");
        objVBox = new VBox(lblDealerName, sep1, lblBooking, sep2, lblBookingPrice);
        objVBox.setPrefWidth(200);
        objVBox.setPadding(new Insets(0, 10, 0, 10));
        objVBox.setAlignment(Pos.CENTER_LEFT);

        objHbox = new HBox(objVBox,objImageView);
        objHbox.setPrefWidth(280);
        objHbox.setPrefHeight(100);
        objHbox.setAlignment(Pos.CENTER_RIGHT);
        objAnchorPane.setBottomAnchor(objHbox, 0.0);
        objAnchorPane.setTopAnchor(objHbox, 0.0);
        objAnchorPane.getChildren().addAll(objHbox);

        return objAnchorPane;
    }

    private AnchorPane createUserPane(String userId, String userName, String bookings, String bookingPrice, String imageUrl)
    {
        String css = "view/css/theme-green.css";
        AnchorPane objAnchorPane = new AnchorPane();
        objAnchorPane.getStylesheets().add(css);
        objAnchorPane.getStyleClass().add("users_pane");

        HBox objHbox;
        ImageView objImageView = new ImageView();
        File file = new File(imageUrl);
        boolean exists = file.exists();
        if(exists)
        {
            Image image = new Image(file.toURI().toString());
            objImageView.setImage(image);
        }
        else
        {
            file = new File("src/view/images/users/empty_user.jpg");
            Image image = new Image(file.toURI().toString());
            objImageView.setImage(image);
        }
        objImageView.setFitWidth(80);
        objImageView.setFitHeight(80);

        VBox objVBox;
        Label lblDealerName = new Label(userName);
        lblDealerName.setStyle("-fx-text-fill: #a4a4a4; -fx-font-style: italic");
        Separator sep1 = new Separator();
        Label lblBooking = new Label("Bookings: "+bookings);
        lblBooking.setStyle("-fx-text-fill: #a4a4a4; -fx-font-style: italic");
        Separator sep2 = new Separator();
        Label lblBookingPrice = new Label("Booking Price: Rs."+String.format("%,.0f", Float.parseFloat(bookingPrice)));
        lblBookingPrice.setStyle("-fx-text-fill: #a4a4a4; -fx-font-style: italic");
        objVBox = new VBox(lblDealerName, sep1, lblBooking, sep2, lblBookingPrice);
        objVBox.setPrefWidth(200);
        objVBox.setPadding(new Insets(0, 10, 0, 10));
        objVBox.setAlignment(Pos.CENTER_LEFT);

        objHbox = new HBox(objVBox, objImageView);
        objHbox.setPrefWidth(280);
        objHbox.setPrefHeight(100);
        objHbox.setAlignment(Pos.CENTER_RIGHT);
        objAnchorPane.setBottomAnchor(objHbox, 0.0);
        objAnchorPane.setTopAnchor(objHbox, 0.0);
        objAnchorPane.getChildren().addAll(objHbox);

        return objAnchorPane;
    }
}
