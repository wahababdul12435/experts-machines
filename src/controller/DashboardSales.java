package controller;

import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.transitions.hamburger.HamburgerBackArrowBasicTransition;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Bounds;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.*;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;
import model.MysqlCon;
import model.PurchaseInvoice;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;

public class DashboardSales implements Initializable {

    @FXML
    private BorderPane menu_bar;
    @FXML
    private JFXHamburger hamburger;
    @FXML
    private JFXDrawer drawer;

    @FXML
    private AnchorPane inner_anchor;
    @FXML
    private Button btn11;
    @FXML
    private Pane btnpane;
    @FXML
    private FontAwesomeIconView FA_Icon;
    @FXML
    private BorderPane brdrpane_navsale;
    @FXML
    private BarChart<?, ?> monthly_sale;

    @FXML
    private CategoryAxis month_axis;
    public static Button btnView;

    @FXML
    private NumberAxis sale_axis;
    ArrayList<String> prodctlist = new ArrayList<>();
    final static String[] month1 = new String[6];

    ArrayList<ArrayList<String>> compWise_monthSale = new ArrayList<ArrayList<String>>();
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle)
    {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnSale.setStyle("-fx-background-color: #47ab1e");
        btnpane.setOnMouseEntered(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(22);
                btnpane.setMinWidth(20.0);
                btn11.setMinWidth(20.0);

            }
        });
        btnpane.setOnMouseExited(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(14);
                btnpane.setMinWidth(10.0);
                btn11.setMinWidth(10.0);
            }
        });

        inner_anchor.setLeftAnchor(inner_anchor, 310d);
        drawer.setSidePane(brdrpane_navsale);
        drawer.open();
        String[] monthName = {"Jan", "Feb",
                "Mar", "Apr", "May", "Jun", "Jul",
                "Aug", "Sep", "Oct", "Nov",
                "Dec"};
        Date dt = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(dt);
        c.add(Calendar.MONTH, -1);
        month1[0] = monthName[c.get(Calendar.MONTH)];
        dt = c.getTime();
        int curryear = c.get(Calendar.YEAR);
        int strt =01;
        int endd =31;

        for(int y =1;y<6;y++)
        {
                c.setTime(dt);
            c.add(Calendar.MONTH, -1);
            month1[y] = monthName[c.get(Calendar.MONTH)];
            dt = c.getTime();
        }
        XYChart.Series series1 = new XYChart.Series();
        XYChart.Series series2 = new XYChart.Series();
        XYChart.Series series3 = new XYChart.Series();
        XYChart.Series series4 = new XYChart.Series();
        int[] comp_names = new int[4];
        comp_names[0] = 2;
        comp_names[1] = 3;
        comp_names[2] = 5;
        comp_names[3] = 6;

        for(int g =0;g<comp_names.length;g++)
        {
            ArrayList<String> saleValues = new ArrayList<>();
            for(int f=0;f<month1.length;f++)
            {
                float summ =0;
                float saleVal = 0;
                String startDate = strt+"-"+month1[f]+"-"+curryear;
                String endDate = endd+"-"+month1[f]+"-"+curryear;

                MysqlCon mysql_conn = new MysqlCon();
                Statement Stmnt_obj = mysql_conn.stmt;
                Connection conn_obj = mysql_conn.con;
                ResultSet res = null;
                try {
                    res = Stmnt_obj.executeQuery("select order_info_detailed.final_price,order_info.booking_date from ((order_info_detailed inner join product_info on product_info.product_table_id = order_info_detailed.product_table_id) inner join order_info on order_info.order_id = order_info_detailed.order_id) where product_info.company_table_id = '"+comp_names[g]+"'");
                    while (res.next()) {
                        String DateValue = res.getString(2);
                        String[] splitdate = DateValue.split("/");
                        String nsss = splitdate[0] + '-' + splitdate[1] + '-' + splitdate[2];
                        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
                        Date DBDate = dateFormat.parse(nsss);
                        Date strtDate = dateFormat.parse(startDate);
                        Date EndingDate = dateFormat.parse(endDate);

                        if (DBDate.after(strtDate) && DBDate.before(EndingDate)) {
                            String priceValue = res.getString(1);
                            saleVal = Float.parseFloat(priceValue);
                            summ = summ + saleVal;
                        }
                    }
                    saleValues.add(String.valueOf(summ));
                    conn_obj.close();
                } catch (SQLException | ParseException e) {
                    e.printStackTrace();
                }

            }
            compWise_monthSale.add(saleValues);
        }


        for(int month=0; month<month1.length;month++) {
            if(compWise_monthSale.get(0).size() > 0)
            {
                final XYChart.Data<String, Number> data = new XYChart.Data(month1[month], Float.parseFloat(compWise_monthSale.get(0).get(month)));
                series1.getData().add(data);
                final XYChart.Data<String, Number> data1 = new XYChart.Data(month1[month], Float.parseFloat(compWise_monthSale.get(1).get(month)));
                series2.getData().add(data1);
                final XYChart.Data<String, Number> data2 = new XYChart.Data(month1[month], Float.parseFloat(compWise_monthSale.get(2).get(month)));
                series3.getData().add(data2);
                final XYChart.Data<String, Number> data3 = new XYChart.Data(month1[month], Float.parseFloat(compWise_monthSale.get(3).get(month)));
                series4.getData().add(data3);

                data.nodeProperty().addListener(new ChangeListener<Node>() {
                    @Override
                    public void changed(ObservableValue<? extends Node> ov, Node oldNode, final Node node) {
                        if (node != null) {
                            displayLabelForData(data);
                        }
                    }
                });
            }
        }
        series1.setName("High Q");
        series2.setName("Vida");
        series3.setName("High Q");
        series4.setName("Vida");
        monthly_sale.getData().addAll(series1,series2,series3,series4);

        MysqlCon newobjMysqlCon1 = new MysqlCon();
        Statement newobjStmt1 = newobjMysqlCon1.stmt;
        Connection newobjCon1 = newobjMysqlCon1.con;
        ResultSet rs = null;
        try {
            rs = newobjStmt1.executeQuery("select product_name from product_info where product_status='Active'  order by  product_name ASC ");
            int i = 0;
            while (rs.next())
            {
                prodctlist.add(i,rs.getString(1));
                i++;
            }

            newobjCon1.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //PurchaseInvoice newobjsaleinvoice1 = new PurchaseInvoice();
        //newobjsaleinvoice1.insertdata_RowWise(newobjStmt1, newobjCon1, txt_purchInvNo.getText() ,purchid_intval,intvalue_prodID,floatvalue_discount,intvalue_prodBonus,intvalue_prodQuant,getbatch_tableview[i],expirydate_newformat,sumof_net_disc,fltval_discamount_perprod,fltval_netamount_perprod,newdates,returnedBit,rentrned_qantity,rentrned_Bonus_qantity);

    }

    private void displayLabelForData(XYChart.Data<String, Number> data) {
        final Node node = data.getNode();
        final DecimalFormat df = new DecimalFormat( "###,##0" );
        final Text dataText = new Text(df.format(data.getYValue()) + "");
        node.parentProperty().addListener(new ChangeListener<Parent>() {
            @Override
            public void changed(ObservableValue<? extends Parent> ov, Parent oldParent, Parent parent) {
                if(null!=parent)
                {
                    Group parentGroup = (Group) parent;
                    parentGroup.getChildren().add(dataText);
                }
            }
        });

        node.boundsInParentProperty().addListener(new ChangeListener<Bounds>() {
            @Override
            public void changed(ObservableValue<? extends Bounds> ov, Bounds oldBounds, Bounds bounds) {
                dataText.setLayoutX(
                        Math.round(
                                bounds.getMinX() + bounds.getWidth() / 2 - dataText.prefWidth(-1) / 2
                        )
                );
                dataText.setLayoutY(
                        Math.round(
                                bounds.getMinY() - dataText.prefHeight(-1) * 0.5
                        )
                );
            }
        });


    }
    public void follow() throws IOException
    {
    /*PathTransition transition;
    transition = new PathTransition();
    Polyline mpoly = new Polyline();
    mpoly.getPoints().addAll(new Double[]{
            0.0,0.0,
            0.0,0.0,
            -200.0,0.0});
    transition.setNode(btn11);
    transition.setDuration(Duration.seconds(2));
    transition.setPath(mpoly);
    transition.play();*/
        if (drawer.isOpened()) {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(-250);
            transition.setByY(0);
            transition.setCycleCount(1);
            transition.play();
            drawer.close();
            FA_Icon.setGlyphName("ANGLE_DOUBLE_RIGHT");
            FA_Icon.setGlyphSize(14);
            //inner_anchor.setRightAnchor(inner_anchor,1d);


            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(-90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(1.31017);
            scale.setToY(1);



            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();
            //
            //inner_anchor.setLeftAnchor(inner_anchor, 5d);
        } else {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(250);
            transition.setByY(0);
            transition.setCycleCount(1);
            FA_Icon.setGlyphName("ANGLE_DOUBLE_LEFT");
            FA_Icon.setGlyphSize(14);
            drawer.open();

            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(0.99947777);
            scale.setToY(1);



            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();

        }


    }
}
