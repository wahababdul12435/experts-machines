package controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.GlobalVariables;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class NavSetup implements Initializable {
    @FXML
    private VBox vbox_style;

    @FXML
    private Button btn_setup_dashboard;

    @FXML
    private Button btn_setup_company;

    @FXML
    private Button btn_setup_group;

    @FXML
    private Button btn_setup_supplier;

    @FXML
    private Button btn_setup_product;

    @FXML
    private Button btn_setup_salesman;

    @FXML
    private Button btn_setup_dealer;

    @FXML
    private Button btn_setup_district;

    @FXML
    private Button btn_setup_city;

    @FXML
    private Button btn_setup_area;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        DashboardSetup.btnView = btn_setup_dashboard;
        ViewCompany.btnView = btn_setup_company;
        EnterCompany.btnView = btn_setup_company;
        ViewGroups.btnView = btn_setup_group;
        EnterGroup.btnView = btn_setup_group;
        ViewSupplier.btnView = btn_setup_supplier;
        EnterSupplier.btnView = btn_setup_supplier;
        ViewProducts.btnView = btn_setup_product;
        EnterProduct.btnView = btn_setup_product;
        ViewUsers.btnView = btn_setup_salesman;
        EnterUser.btnView = btn_setup_salesman;
        ViewDealers.btnView = btn_setup_dealer;
        EnterDealer.btnView = btn_setup_dealer;
        ViewDistrict.btnView = btn_setup_district;
        EnterDistrict.btnView = btn_setup_district;
        ViewCity.btnView = btn_setup_city;
        EnterCity.btnView = btn_setup_city;
        ViewArea.btnView = btn_setup_area;
        EnterArea.btnView = btn_setup_area;
    }

    public void viewDashboard() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/dashboard_setup.fxml"));
//        Stage objstage = (Stage) vbox_style.getScene().getWindow();
//        GlobalVariables.baseScene = new Scene(root, GlobalVariables.SCREEN_WIDTH, GlobalVariables.SCREEN_HEIGHT);
//        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    public void viewPendings() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_pending_orders.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    public void batchslctionSetting() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/batch_selection_policy.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }



    public void addCompany() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/enter_company.fxml"));
//        Stage objstage = (Stage) vbox_style.getScene().getWindow();
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    public void viewCompanies() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_company.fxml"));

        GlobalVariables.setupNavigation = "company";
//        Stage objstage = (Stage) vbox_style.getScene().getWindow();
//        Scene objscene = new Scene(root, GlobalVariables.SCREEN_WIDTH, GlobalVariables.SCREEN_HEIGHT);
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    public void viewGroups() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_groups.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    public void viewDistrict() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_district.fxml"));
//        Stage objstage = (Stage) vbox_style.getScene().getWindow();
//        Scene objscene = new Scene(root, GlobalVariables.SCREEN_WIDTH, GlobalVariables.SCREEN_HEIGHT);
//        objstage.setScene(objscene);
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void addArea() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/enter_area.fxml"));
//        Stage objstage = (Stage) vbox_style.getScene().getWindow();
//        Scene objscene = new Scene(root, GlobalVariables.SCREEN_WIDTH, GlobalVariables.SCREEN_HEIGHT);
//        objstage.setScene(objscene);
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    public void viewCities() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_city.fxml"));
//        Stage objstage = (Stage) vbox_style.getScene().getWindow();
//        Scene objscene = new Scene(root, GlobalVariables.SCREEN_WIDTH, GlobalVariables.SCREEN_HEIGHT);
//        objstage.setScene(objscene);
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    public void viewAreas() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_area.fxml"));
//        Stage objstage = (Stage) vbox_style.getScene().getWindow();
//        Scene objscene = new Scene(root, GlobalVariables.SCREEN_WIDTH, GlobalVariables.SCREEN_HEIGHT);
//        objstage.setScene(objscene);
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void addDealer() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/enter_dealer.fxml"));
//        Stage objstage = (Stage) vbox_style.getScene().getWindow();
//        Scene objscene = new Scene(root, GlobalVariables.SCREEN_WIDTH, GlobalVariables.SCREEN_HEIGHT);
//        objstage.setScene(objscene);
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    public void viewDealers() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_dealers.fxml"));
//        Stage objstage = (Stage) vbox_style.getScene().getWindow();
//        Scene objscene = new Scene(root, GlobalVariables.SCREEN_WIDTH, GlobalVariables.SCREEN_HEIGHT);
//        objstage.setScene(objscene);
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void addSalesman() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/enter_salesman.fxml"));
//        Stage objstage = (Stage) vbox_style.getScene().getWindow();
//        Scene objscene = new Scene(root, GlobalVariables.SCREEN_WIDTH, GlobalVariables.SCREEN_HEIGHT);
//        objstage.setScene(objscene);
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    public void viewSalesman() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_users.fxml"));
//        Stage objstage = (Stage) vbox_style.getScene().getWindow();
//        Scene objscene = new Scene(root, GlobalVariables.SCREEN_WIDTH, GlobalVariables.SCREEN_HEIGHT);
//        objstage.setScene(objscene);
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void addSupplier() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/enter_supplier.fxml"));
//        Stage objstage = (Stage) vbox_style.getScene().getWindow();
//        Scene objscene = new Scene(root, GlobalVariables.SCREEN_WIDTH, GlobalVariables.SCREEN_HEIGHT);
//        objstage.setScene(objscene);
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    public void viewSuppliers() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_suppliers.fxml"));
//        Stage objstage = (Stage) vbox_style.getScene().getWindow();
//        Scene objscene = new Scene(root, GlobalVariables.SCREEN_WIDTH, GlobalVariables.SCREEN_HEIGHT);
//        objstage.setScene(objscene);
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void addProduct() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/enter_product.fxml"));
//        Stage objstage = (Stage) vbox_style.getScene().getWindow();
//        Scene objscene = new Scene(root, GlobalVariables.SCREEN_WIDTH, GlobalVariables.SCREEN_HEIGHT);
//        objstage.setScene(objscene);
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    public void viewProducts() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_products.fxml"));
//        Stage objstage = (Stage) vbox_style.getScene().getWindow();
//        Scene objscene = new Scene(root, GlobalVariables.SCREEN_WIDTH, GlobalVariables.SCREEN_HEIGHT);
//        objstage.setScene(objscene);
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    public void viewSystemAccounts() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/system_accounts.fxml"));
        Stage objstage = (Stage) vbox_style.getScene().getWindow();
        Scene objscene = new Scene(root, GlobalVariables.SCREEN_WIDTH, GlobalVariables.SCREEN_HEIGHT);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewDrawingSummary() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/drawing_summary.fxml"));
        Stage objstage = (Stage) vbox_style.getScene().getWindow();
        Scene objscene = new Scene(root, GlobalVariables.SCREEN_WIDTH, GlobalVariables.SCREEN_HEIGHT);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewCollectionSummary() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/collection_summary.fxml"));
        Stage objstage = (Stage) vbox_style.getScene().getWindow();
        Scene objscene = new Scene(root, GlobalVariables.SCREEN_WIDTH, GlobalVariables.SCREEN_HEIGHT);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewInvoicesDetail() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/invoices_detail.fxml"));
        Stage objstage = (Stage) vbox_style.getScene().getWindow();
        Scene objscene = new Scene(root, GlobalVariables.SCREEN_WIDTH, GlobalVariables.SCREEN_HEIGHT);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewGPSTracking() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/gps_tracking.fxml"));
        Stage objstage = (Stage) vbox_style.getScene().getWindow();
        Scene objscene = new Scene(root, GlobalVariables.SCREEN_WIDTH, GlobalVariables.SCREEN_HEIGHT);
        objstage.setScene(objscene);
    }
}
