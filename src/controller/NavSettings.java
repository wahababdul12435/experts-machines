package controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.GlobalVariables;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;


public class NavSettings implements Initializable {

    @FXML
    private VBox vbox_style;

    @FXML
    private Button btn_settings_dashboard;

    @FXML
    private Button btn_settings_myaccount;

    @FXML
    private Button btn_settings_usersaccount;

    @FXML
    private Button btn_settings_policies;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        DashboardSettings.btnView = btn_settings_dashboard;
        MyAccount.btnView = btn_settings_myaccount;
        UserAccounts.btnView = btn_settings_usersaccount;
        SystemSettings.btnView = btn_settings_policies;
    }

    public void settingsDashboard() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/dashboard_settings.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
    public void userAccounts() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/user_accounts.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
    public void systemSettings() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/system_settings.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
    public void myAccounts() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/my_account1.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

}
