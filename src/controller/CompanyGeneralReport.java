package controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.util.StringConverter;
import model.*;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

public class CompanyGeneralReport implements Initializable {

    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private TableView<CompanyGeneralReportInfo> table_companyreport;

    @FXML
    private TableColumn<CompanyGeneralReportInfo, String> sr_no;

    @FXML
    private TableColumn<CompanyGeneralReportInfo, String> company_id;

    @FXML
    private TableColumn<CompanyGeneralReportInfo, String> company_name;

    @FXML
    private TableColumn<CompanyGeneralReportInfo, String> orders_given;

    @FXML
    private TableColumn<CompanyGeneralReportInfo, String> orders_received;

    @FXML
    private TableColumn<CompanyGeneralReportInfo, String> ordered_items;

    @FXML
    private TableColumn<CompanyGeneralReportInfo, String> received_items;

    @FXML
    private TableColumn<CompanyGeneralReportInfo, String> returned_items;

    @FXML
    private TableColumn<CompanyGeneralReportInfo, String> return_price;

    @FXML
    private TableColumn<CompanyGeneralReportInfo, String> total_purchase;

    @FXML
    private TableColumn<CompanyGeneralReportInfo, String> discount_given;

    @FXML
    private TableColumn<CompanyGeneralReportInfo, String> cash_sent;

    @FXML
    private TableColumn<CompanyGeneralReportInfo, String> cash_return;

    @FXML
    private TableColumn<CompanyGeneralReportInfo, String> cash_waive_off;

    @FXML
    private TableColumn<CompanyGeneralReportInfo, String> cash_pending;

    @FXML
    private TableColumn<CompanyGeneralReportInfo, String> company_status;

    @FXML
    private TableColumn<CompanyGeneralReportInfo, String> operations;

    @FXML
    private AnchorPane filter_pane;

    @FXML
    private HBox filter_hbox;

    @FXML
    private JFXTextField txt_company_id;

    @FXML
    private JFXComboBox<String> txt_company_name;

    @FXML
    private JFXDatePicker txt_from_date;

    @FXML
    private JFXDatePicker txt_to_date;

    @FXML
    private HBox summary_hbox1;

    @FXML
    private Label lbl_orders_given;

    @FXML
    private Label lbl_orders_received;

    @FXML
    private Label lbl_ordered_items;

    @FXML
    private Label lbl_received_items;

    @FXML
    private Label lbl_returned_items;

    @FXML
    private Label lbl_return_price;

    @FXML
    private HBox summary_hbox2;

    @FXML
    private Label lbl_total_purchase;

    @FXML
    private Label lbl_cash_sent;

    @FXML
    private Label lbl_cash_return;

    @FXML
    private Label lbl_discount_given;

    @FXML
    private Label lbl_cash_waiveoff;

    @FXML
    private Label lbl_cash_pending;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    @FXML
    private BorderPane brdrpane_navfinances;

    @FXML
    private BorderPane menu_bar;

    MysqlCon objMysqlCon1;
    MysqlCon objMysqlCon2;
    MysqlCon objMysqlCon3;
    Statement objStmt1;
    Statement objStmt2;
    Statement objStmt3;
    Connection objCon;
    FilteredList<String> filteredItems;
    CompanyInfo objCompanyInfo;
    ArrayList<String> companyNames;

    float totalOrdersGiven = 0;
    float totalOrdersReceived = 0;
    float orderedItems = 0;
    float receivedItems = 0;
    float totalPurchase = 0;
    float discountGiven = 0;
    float itemsReturned = 0;
    float returnPrice = 0;
    float cashSent = 0;
    float cashReturn = 0;
    float cashWaiveOff = 0;
    float cashPending = 0;

    public static String txtCompanyId = "";
    public static String txtCompanyName = "";
    public static String txtFromDate = "";
    public static String txtToDate = "";
    public static boolean filter;
    public static String summary = "";

    public static MenuButton btnView;
    public ObservableList<CompanyGeneralReportInfo> companyReportDetail;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnReport.setStyle("-fx-background-color: #47ab1e");
        objMysqlCon1 = new MysqlCon();
        objMysqlCon2 = new MysqlCon();
        objMysqlCon3 = new MysqlCon();
        objStmt1 = objMysqlCon1.stmt;
        objStmt2 = objMysqlCon2.stmt;
        objStmt3 = objMysqlCon3.stmt;
        objCon = objMysqlCon1.con;

        objCompanyInfo = new CompanyInfo();
        ArrayList<ArrayList<String>> companyData = objCompanyInfo.getCompanyNames();
        companyNames = GlobalVariables.getArrayColumn(companyData, 1);
        txt_company_name.getItems().addAll(companyNames);

        inner_anchor.setLeftAnchor(inner_anchor, 280d);
        drawer.setSidePane(brdrpane_navfinances);
        drawer.open();

        txt_from_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        txt_to_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        company_id.setCellValueFactory(new PropertyValueFactory<>("companyId"));
        company_name.setCellValueFactory(new PropertyValueFactory<>("companyName"));
        orders_given.setCellValueFactory(new PropertyValueFactory<>("ordersGiven"));
        orders_received.setCellValueFactory(new PropertyValueFactory<>("ordersReceived"));
        ordered_items.setCellValueFactory(new PropertyValueFactory<>("orderedItems"));
        received_items.setCellValueFactory(new PropertyValueFactory<>("receivedItems"));
        total_purchase.setCellValueFactory(new PropertyValueFactory<>("totalPurchase"));
        discount_given.setCellValueFactory(new PropertyValueFactory<>("discountGiven"));
        returned_items.setCellValueFactory(new PropertyValueFactory<>("returnedItems"));
        return_price.setCellValueFactory(new PropertyValueFactory<>("returnPrice"));
        cash_sent.setCellValueFactory(new PropertyValueFactory<>("cashSent"));
        cash_return.setCellValueFactory(new PropertyValueFactory<>("cashReturn"));
        cash_waive_off.setCellValueFactory(new PropertyValueFactory<>("cashWaiveOff"));
        cash_pending.setCellValueFactory(new PropertyValueFactory<>("cashPending"));
        company_status.setCellValueFactory(new PropertyValueFactory<>("companyStatus"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_companyreport.setItems(parseUserList());
        lbl_orders_given.setText("Orders Given\n"+String.format("%,.0f", totalOrdersGiven));
        lbl_orders_received.setText("Orders Received\n"+String.format("%,.0f", totalOrdersReceived));
        lbl_ordered_items.setText("Ordered Items\n"+String.format("%,.0f", orderedItems));
        lbl_received_items.setText("Received Items\n"+String.format("%,.0f", receivedItems));
        lbl_total_purchase.setText("Total Purchase\n"+String.format("%,.0f", totalPurchase));
        lbl_discount_given.setText("Discount Given\n"+String.format("%,.0f", discountGiven));
        lbl_returned_items.setText("Items Returned\n"+String.format("%,.0f", itemsReturned));
        lbl_return_price.setText("Return Price\n"+String.format("%,.0f", returnPrice));
        lbl_cash_sent.setText("Cash Sent\n"+String.format("%,.0f", cashSent));
        lbl_cash_return.setText("Cash Returned\n"+String.format("%,.0f", cashReturn));
        lbl_cash_waiveoff.setText("Cash Waived Off\n"+String.format("%,.0f", cashWaiveOff));
        lbl_cash_pending.setText("Cash Pending\n"+String.format("%,.0f", cashPending));
        summary = "";
    }

    private ObservableList<CompanyGeneralReportInfo> parseUserList() {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        CompanyGeneralReportInfo objCompanyGeneralReportInfo = new CompanyGeneralReportInfo();
        companyReportDetail = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> companyGeneralReportData = null;
        if(filter)
        {
            companyGeneralReportData  = objCompanyGeneralReportInfo.getCompanyReportSearch(objStmt1, objStmt2, objStmt3, objCon, txtCompanyId, txtCompanyName, txtFromDate, txtToDate);
            txt_company_id.setText(txtCompanyId);
            txt_company_name.setValue(txtCompanyName);
            if (txtFromDate != null && !txtFromDate.equals("")) {
                txt_from_date.setValue(GlobalVariables.LOCAL_DATE(txtFromDate));
            }
            if (txtToDate != null && !txtToDate.equals("")) {
                txt_to_date.setValue(GlobalVariables.LOCAL_DATE(txtToDate));
                cash_pending.setText("Cash Pending\non\n"+txtToDate);
            }
            else
            {
                txtToDate = GlobalVariables.getStDate();
                cash_pending.setText("Cash Pending\non\n"+txtToDate);
            }
            filter = false;
            filter_pane.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
        }
        else
        {
            txtToDate = GlobalVariables.getStDate();
            cash_pending.setText("Cash Pending\non\n"+txtToDate);
            companyGeneralReportData = objCompanyGeneralReportInfo.getCompanyReportInfo(objStmt1, objStmt2, objStmt3, objCon, GlobalVariables.getStDate(), GlobalVariables.getStDate());
        }
        if (companyGeneralReportData != null) {
            for (int i = 0; i < companyGeneralReportData.size(); i++)
            {
                totalOrdersGiven += (companyGeneralReportData.get(i).get(3) != null) ? Float.parseFloat(companyGeneralReportData.get(i).get(3)) : 0;
                totalOrdersReceived += (companyGeneralReportData.get(i).get(4) != null) ? Float.parseFloat(companyGeneralReportData.get(i).get(4)) : 0;
                orderedItems += (companyGeneralReportData.get(i).get(5) != null) ? Float.parseFloat(companyGeneralReportData.get(i).get(5)) : 0;
                receivedItems += (companyGeneralReportData.get(i).get(6) != null) ? Float.parseFloat(companyGeneralReportData.get(i).get(6)) : 0;
                totalPurchase += (companyGeneralReportData.get(i).get(7) != null) ? Float.parseFloat(companyGeneralReportData.get(i).get(7)) : 0;
                discountGiven += (companyGeneralReportData.get(i).get(8) != null) ? Float.parseFloat(companyGeneralReportData.get(i).get(8)) : 0;
                itemsReturned += (companyGeneralReportData.get(i).get(9) != null) ? Float.parseFloat(companyGeneralReportData.get(i).get(9)) : 0;
                returnPrice += (companyGeneralReportData.get(i).get(10) != null) ? Float.parseFloat(companyGeneralReportData.get(i).get(10)) : 0;
                cashSent += (companyGeneralReportData.get(i).get(11) != null) ? Float.parseFloat(companyGeneralReportData.get(i).get(11)) : 0;
                cashReturn += (companyGeneralReportData.get(i).get(12) != null) ? Float.parseFloat(companyGeneralReportData.get(i).get(12)) : 0;
                cashWaiveOff += (companyGeneralReportData.get(i).get(13) != null) ? Float.parseFloat(companyGeneralReportData.get(i).get(13)) : 0;
                cashPending += (companyGeneralReportData.get(i).get(14) != null) ? Float.parseFloat(companyGeneralReportData.get(i).get(14)) : 0;
                companyReportDetail.add(new CompanyGeneralReportInfo(String.valueOf(i+1), companyGeneralReportData.get(i).get(0), ((companyGeneralReportData.get(i).get(1) == null) ? "N/A" : companyGeneralReportData.get(i).get(1)), companyGeneralReportData.get(i).get(2), ((companyGeneralReportData.get(i).get(3) == null) ? "N/A" : companyGeneralReportData.get(i).get(3)), ((companyGeneralReportData.get(i).get(4) == null) ? "0" : companyGeneralReportData.get(i).get(4)), ((companyGeneralReportData.get(i).get(5) == null) ? "0" : companyGeneralReportData.get(i).get(5)), ((companyGeneralReportData.get(i).get(6) == null) ? "0" : companyGeneralReportData.get(i).get(6)), ((companyGeneralReportData.get(i).get(7) == null) ? "0" : companyGeneralReportData.get(i).get(7)), ((companyGeneralReportData.get(i).get(8) == null) ? "0" : companyGeneralReportData.get(i).get(8)), ((companyGeneralReportData.get(i).get(9) == null) ? "0" : companyGeneralReportData.get(i).get(9)), ((companyGeneralReportData.get(i).get(10) == null) ? "0" : companyGeneralReportData.get(i).get(10)), ((companyGeneralReportData.get(i).get(11) == null) ? "0" : companyGeneralReportData.get(i).get(11)), ((companyGeneralReportData.get(i).get(12) == null) ? "0" : companyGeneralReportData.get(i).get(12)), ((companyGeneralReportData.get(i).get(13) == null) ? "0" : companyGeneralReportData.get(i).get(13)), ((companyGeneralReportData.get(i).get(14) == null) ? "0" : companyGeneralReportData.get(i).get(14)), ((companyGeneralReportData.get(i).get(15) == null) ? "0" : companyGeneralReportData.get(i).get(15))));
            }
        }
        return companyReportDetail;
    }

    @FXML
    void btnReset(ActionEvent event) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/company_general_report.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void btnSearch(ActionEvent event) {
        txtCompanyId = txt_company_id.getText();
        txtCompanyName = txt_company_name.getValue();
        if(txtCompanyName == null)
        {
            txtCompanyName = "All";
        }
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date selectedDate = null;
        if(txt_from_date.getValue() != null)
        {
            txtFromDate = txt_from_date.getValue().toString();
            try {
                selectedDate = sdf.parse(txtFromDate);
                txtFromDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        else
        {
            txtFromDate = "";
        }

        if(txt_to_date.getValue() != null)
        {
            txtToDate = txt_to_date.getValue().toString();
            try {
                selectedDate = sdf.parse(txtToDate);
                txtToDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        else
        {
            txtToDate = "";
        }
        filter = true;

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/company_general_report.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void follow(MouseEvent event) {

    }

    @FXML
    void showCashReturn(MouseEvent event) {

    }

    @FXML
    void showCashSent(MouseEvent event) {

    }

    @FXML
    void showDiscount(MouseEvent event) {

    }

    @FXML
    void showOrderedItems(MouseEvent event) {

    }

    @FXML
    void showOrdersGiven(MouseEvent event) {

    }

    @FXML
    void showOrdersReceived(MouseEvent event) {

    }

    @FXML
    void showPending(MouseEvent event) {

    }

    @FXML
    void showPurchase(MouseEvent event) {

    }

    @FXML
    void showReceivedItems(MouseEvent event) {

    }

    @FXML
    void showReturnPrice(MouseEvent event) {

    }

    @FXML
    void showReturnedItems(MouseEvent event) {

    }

    @FXML
    void showWaiveOff(MouseEvent event) {

    }
}
