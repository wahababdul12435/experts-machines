package controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.util.StringConverter;
import model.*;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

public class CompanyDetail implements Initializable {

    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private Label lbl_company_id;

    @FXML
    private Label lbl_company_name;

    @FXML
    private Label lbl_company_contact;

    @FXML
    private Label lbl_company_address;

    @FXML
    private Label lbl_company_person;

    @FXML
    private Label lbl_company_email;

    @FXML
    private Label lbl_company_created;

    @FXML
    private Label lbl_company_updated;

    @FXML
    private Label lbl_company_status;

    @FXML
    private ImageView img_user;

    @FXML
    private BarChart<Integer, String> cash_sent_chart;

    @FXML
    private CategoryAxis itemType;

    @FXML
    private NumberAxis numberAxis;

    @FXML
    private AnchorPane filter_pane;

    @FXML
    private JFXTextField txt_product_id;

    @FXML
    private JFXTextField txt_product_name;

    @FXML
    private JFXComboBox<String> txt_type;

    @FXML
    private JFXTextField txt_from_amount;

    @FXML
    private JFXTextField txt_to_amount;

    @FXML
    private JFXComboBox<String> txt_status;

    @FXML
    private TableView<CompanyDetailInfo> table_companycashlog;

    @FXML
    private TableColumn<CompanyDetailInfo, String> sr_no;

    @FXML
    private TableColumn<CompanyDetailInfo, String> product_id;

    @FXML
    private TableColumn<CompanyDetailInfo, String> product_name;

    @FXML
    private TableColumn<CompanyDetailInfo, String> type;

    @FXML
    private TableColumn<CompanyDetailInfo, String> retail_price;

    @FXML
    private TableColumn<CompanyDetailInfo, String> trade_price;

    @FXML
    private TableColumn<CompanyDetailInfo, String> product_status;

    @FXML
    private TableColumn<CompanyDetailInfo, String> operations;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    @FXML
    private BorderPane brdrpane_navfinances;

    @FXML
    private StackPane stackPane;

    @FXML
    private BorderPane menu_bar;

    private CompanyInfo objCompanyInfo;
    private CompanyDetailInfo objCompanyDetailInfo;
    private String currentCompanyId;
    private String selectedImagePath;
    private ArrayList<String> companyInfo;
    private ObservableList<CompanyDetailInfo> companyDetails;
    ArrayList<ArrayList<String>> typesAndCount = new ArrayList<>();
    ArrayList<Date> dates = new ArrayList<Date>();

    public static String txtProductId = "";
    public static String txtProductName = "";
    public static String txtType = "All";
    public static String txtFromAmount = "";
    public static String txtToAmount = "";
    public static String txtStatus = "All";

    public static boolean filter;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        CompanyDetailInfo.stackPane = stackPane;
        objCompanyInfo = new CompanyInfo();
        objCompanyDetailInfo = new CompanyDetailInfo();
        if(!ViewCompanyInfo.viewCompanyId.equals(""))
        {
            currentCompanyId = ViewCompanyInfo.viewCompanyId;
            ViewCompanyInfo.viewCompanyId = "";
        }
        companyInfo = objCompanyInfo.getCompanyDetail(currentCompanyId);
        lbl_company_id.setText(((companyInfo.get(0) == null) ? "N/A" : companyInfo.get(0)));
        lbl_company_name.setText(companyInfo.get(1));
        lbl_company_contact.setText(companyInfo.get(2));
        lbl_company_address.setText(companyInfo.get(3));
        lbl_company_person.setText((companyInfo.get(4) == null) ? "N/A" : companyInfo.get(4));
        lbl_company_email.setText(companyInfo.get(5));
        lbl_company_created.setText(companyInfo.get(6));
        lbl_company_updated.setText((companyInfo.get(7) == null) ? "N/A" : companyInfo.get(7));
        lbl_company_status.setText(companyInfo.get(8));
        if(companyInfo.get(9) != null && companyInfo.get(9) == "" )
        {
            selectedImagePath = companyInfo.get(9);
            File file = new File(selectedImagePath);
            boolean exists = file.exists();
            if(exists)
            {
                Image image = new Image(file.toURI().toString());
                img_user.setImage(image);
            }
        }

        ArrayList<String> temp = new ArrayList<>();
        temp.add("Tablets");
        temp.add("0");
        typesAndCount.add(temp);
        temp = new ArrayList<>();
        temp.add("Capsules");
        temp.add("0");
        typesAndCount.add(temp);
        temp = new ArrayList<>();
        temp.add("Injection");
        temp.add("0");
        typesAndCount.add(temp);
        temp = new ArrayList<>();
        temp.add("Syrup");
        temp.add("0");
        typesAndCount.add(temp);

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        product_id.setCellValueFactory(new PropertyValueFactory<>("productId"));
        product_name.setCellValueFactory(new PropertyValueFactory<>("productName"));
        type.setCellValueFactory(new PropertyValueFactory<>("productType"));
        retail_price.setCellValueFactory(new PropertyValueFactory<>("retailPrice"));
        trade_price.setCellValueFactory(new PropertyValueFactory<>("tradePrice"));
        product_status.setCellValueFactory(new PropertyValueFactory<>("productStatus"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_companycashlog.setItems(parseUserList());

        itemType.setLabel("Item Type");
        numberAxis.setLabel("No of Items");

        XYChart.Series dataSeries1 = new XYChart.Series();
        for(int i=0; i<typesAndCount.size(); i++)
        {
            dataSeries1.getData().add(new XYChart.Data((Integer.parseInt(typesAndCount.get(i).get(1))), typesAndCount.get(i).get(0)));
        }
        cash_sent_chart.getData().add(dataSeries1);
    }

    private ObservableList<CompanyDetailInfo> parseUserList(){
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        companyDetails = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> companyData;

        if(filter)
        {
            companyData = objCompanyDetailInfo.getCompanyDetailSearch(objStmt, objCon, currentCompanyId, txtProductId, txtProductName, txtType, txtFromAmount, txtToAmount, txtStatus);
            txt_product_id.setText(txtProductId);
            txt_product_name.setText(txtProductName);
            txt_type.setValue(txtType);
            txt_from_amount.setText(txtFromAmount);
            txt_to_amount.setText(txtToAmount);
            txt_status.setValue(txtStatus);
            filter = false;
            filter_pane.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
        }
        else
        {
            companyData = objCompanyDetailInfo.getCompanyDetail(objStmt, objCon, currentCompanyId);
        }

        for (int i = 0; i < companyData.size(); i++)
        {
            if(companyData.get(i).get(3).equals("Tablet"))
            {
                String value = String.valueOf(Integer.parseInt(typesAndCount.get(0).get(1)) + 1);
                typesAndCount.get(0).set(1, value);
            }
            else if(companyData.get(i).get(3).equals("Capsule"))
            {
                String value = String.valueOf(Integer.parseInt(typesAndCount.get(1).get(1)) + 1);
                typesAndCount.get(1).set(1, value);
            }
            else if(companyData.get(i).get(3).equals("Injection"))
            {
                String value = String.valueOf(Integer.parseInt(typesAndCount.get(2).get(1)) + 1);
                typesAndCount.get(2).set(1, value);
            }
            else if(companyData.get(i).get(3).equals("Syrup"))
            {
                String value = String.valueOf(Integer.parseInt(typesAndCount.get(3).get(1)) + 1);
                typesAndCount.get(3).set(1, value);
            }
            companyDetails.add(new CompanyDetailInfo(String.valueOf(i+1), companyData.get(i).get(0), companyData.get(i).get(1), companyData.get(i).get(2), companyData.get(i).get(3), companyData.get(i).get(4), companyData.get(i).get(5), companyData.get(i).get(6)));

        }
        return companyDetails;
    }

    @FXML
    void follow(MouseEvent event) {

    }

    @FXML
    void btnReset(ActionEvent event) {
        ViewCompanyInfo.viewCompanyId = currentCompanyId;
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/company_detail.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void btnSearch(ActionEvent event) {
        txtProductId = txt_product_id.getText();
        txtProductName = txt_product_name.getText();
        txtType = txt_type.getValue();
        if(txtType == null)
        {
            txtType = "All";
        }
        txtFromAmount = txt_from_amount.getText();
        txtToAmount = txt_to_amount.getText();
        txtStatus = txt_status.getValue();
        if(txtStatus == null)
        {
            txtStatus = "All";
        }
        filter = true;

        ViewCompanyInfo.viewCompanyId = currentCompanyId;
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/company_detail.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
}
