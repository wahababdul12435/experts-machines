package controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXDrawer;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.util.StringConverter;
import model.*;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

public class ViewPurchase implements Initializable {

    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private AnchorPane filter_pane;

    @FXML
    private JFXDatePicker txt_from_date;

    @FXML
    private JFXDatePicker txt_to_date;

    @FXML
    private JFXComboBox<String> txt_day;

    @FXML
    private JFXComboBox<String> txt_invoice_status;

    @FXML
    private HBox summary_hbox1;

    @FXML
    private Label lbl_invoices;

    @FXML
    private Label lbl_purchase_items;

    @FXML
    private Label lbl_bonus_items;

    @FXML
    private Label lbl_net_amount;

    @FXML
    private Label lbl_discount_given;

    @FXML
    private TableView<ViewPurchaseInfo> table_viewpurchaselog;

    @FXML
    private TableColumn<ViewPurchaseInfo, String> sr_no;

    @FXML
    private TableColumn<ViewPurchaseInfo, String> purchase_date;

    @FXML
    private TableColumn<ViewPurchaseInfo, String> purchase_day;

    @FXML
    private TableColumn<ViewPurchaseInfo, String> purchase_company;

    @FXML
    private TableColumn<ViewPurchaseInfo, String> invoice_no;

    @FXML
    private TableColumn<ViewPurchaseInfo, String> purchased_items;

    @FXML
    private TableColumn<ViewPurchaseInfo, String> bonus_items;

    @FXML
    private TableColumn<ViewPurchaseInfo, String> gross_amount;

    @FXML
    private TableColumn<ViewPurchaseInfo, String> discount;

    @FXML
    private TableColumn<ViewPurchaseInfo, String> net_amount;

    @FXML
    private TableColumn<ViewPurchaseInfo, String> supplier;

    @FXML
    private TableColumn<ViewPurchaseInfo, String> operations;

    @FXML
    private StackPane stackPane;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    @FXML
    private BorderPane brdrpane_navfinances;

    @FXML
    private BorderPane menu_bar;

    private ObservableList<ViewPurchaseInfo> purchaseDetails;
    private ViewPurchaseInfo objViewPurchaseInfo;
    ArrayList<ArrayList<String>> purchaseData;
    public ArrayList<InvoiceInfo> invoiceData;
    private static ViewSaleDetailInfo objViewSaleDetailInfo;
    private SaleInvoicePrintInfo objSaleInvoicePrintInfo;
    ArrayList<String> invoicePrintData;

    float totalInvoices = 0;
    float purchasedItems = 0;
    float bonusItems = 0;
    float netAmount = 0;
    float discountGiven = 0;

    public static String txtFromDate = "";
    public static String txtToDate = "";
    public static String txtDay = "All";
    public static String txtInvoiceStatus = "All";
    public static boolean filter;

    MysqlCon objMysqlCon1;
    MysqlCon objMysqlCon2;
    MysqlCon objMysqlCon3;
    Statement objStmt1;
    Statement objStmt2;
    Statement objStmt3;
    Connection objCon;

    public static Button btnView;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnPurchase.setStyle("-fx-background-color: #47ab1e");
        objMysqlCon1 = new MysqlCon();
        objMysqlCon2 = new MysqlCon();
        objMysqlCon3 = new MysqlCon();
        objStmt1 = objMysqlCon1.stmt;
        objStmt2 = objMysqlCon2.stmt;
        objStmt3 = objMysqlCon3.stmt;
        objCon = objMysqlCon1.con;
        objViewPurchaseInfo = new ViewPurchaseInfo();
        objSaleInvoicePrintInfo = new SaleInvoicePrintInfo();
        invoicePrintData = objSaleInvoicePrintInfo.getSaleInvoicePrintInfo(objStmt1, objCon);
        ViewPurchaseInfo.stackPane = stackPane;

        purchaseData = new ArrayList<>();
        invoiceData = new ArrayList<>();

        txt_from_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        txt_to_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        purchase_date.setCellValueFactory(new PropertyValueFactory<>("purchaseDate"));
        purchase_day.setCellValueFactory(new PropertyValueFactory<>("purchaseDay"));
        purchase_company.setCellValueFactory(new PropertyValueFactory<>("purchaseCompany"));
        invoice_no.setCellValueFactory(new PropertyValueFactory<>("invoiceNo"));
        purchased_items.setCellValueFactory(new PropertyValueFactory<>("purchasedItems"));
        bonus_items.setCellValueFactory(new PropertyValueFactory<>("bonusItems"));
        gross_amount.setCellValueFactory(new PropertyValueFactory<>("grossAmount"));
        discount.setCellValueFactory(new PropertyValueFactory<>("discount"));
        net_amount.setCellValueFactory(new PropertyValueFactory<>("netAmount"));
        supplier.setCellValueFactory(new PropertyValueFactory<>("supplier"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_viewpurchaselog.setItems(parseUserList());
        lbl_invoices.setText("Total Invoices\n"+String.format("%,.0f", totalInvoices));
        lbl_purchase_items.setText("Purchased Items\n"+String.format("%,.0f", purchasedItems));
        lbl_bonus_items.setText("Bonus Items\n"+String.format("%,.0f", bonusItems));
        lbl_net_amount.setText("Purchase Net Amount\n"+String.format("%,.0f", netAmount));
        lbl_discount_given.setText("Discount Given\n"+String.format("%,.0f", discountGiven));
    }

    private ObservableList<ViewPurchaseInfo> parseUserList() {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        purchaseDetails = FXCollections.observableArrayList();

        if (filter) {
            purchaseData = objViewPurchaseInfo.getPurchaseInfoSearch(objStmt1, objCon, txtFromDate, txtToDate, txtDay, txtInvoiceStatus);
            if (txtFromDate != null && !txtFromDate.equals("")) {
                txt_from_date.setValue(GlobalVariables.LOCAL_DATE(txtFromDate));
            }
            if (txtToDate != null && !txtToDate.equals("")) {
                txt_to_date.setValue(GlobalVariables.LOCAL_DATE(txtToDate));
            }
            txt_day.setValue(txtDay);
            txt_invoice_status.setValue(txtInvoiceStatus);
            filter = false;
            filter_pane.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
            txtFromDate = "";
            txtToDate = "";
            txtDay = "All";
            txtInvoiceStatus = "All";
        } else {
            purchaseData = objViewPurchaseInfo.getPurchaseInfo(objStmt1, objCon, GlobalVariables.getStDate(), GlobalVariables.getStDate());
        }

        for (int i = 0; i < purchaseData.size(); i++)
        {
            totalInvoices += (purchaseData.get(i).get(3) != null && !purchaseData.get(i).get(3).equals("-")) ? 1 : 0;
            purchasedItems += (purchaseData.get(i).get(4) != null && !purchaseData.get(i).get(4).equals("-")) ? Float.parseFloat(purchaseData.get(i).get(4)) : 0;
            bonusItems += (purchaseData.get(i).get(5) != null && !purchaseData.get(i).get(5).equals("-")) ? Float.parseFloat(purchaseData.get(i).get(5)) : 0;
            netAmount += (purchaseData.get(i).get(8) != null && !purchaseData.get(i).get(8).equals("-")) ? Float.parseFloat(purchaseData.get(i).get(8)) : 0;
            discountGiven += (purchaseData.get(i).get(7) != null && !purchaseData.get(i).get(7).equals("-")) ? Float.parseFloat(purchaseData.get(i).get(7)) : 0;
            purchaseDetails.add(new ViewPurchaseInfo(String.valueOf(i+1), ((purchaseData.get(i).get(0) == null || purchaseData.get(i).get(0).equals("")) ? "N/A" : purchaseData.get(i).get(0)), purchaseData.get(i).get(1), purchaseData.get(i).get(2), purchaseData.get(i).get(3), purchaseData.get(i).get(4), purchaseData.get(i).get(5), purchaseData.get(i).get(6), purchaseData.get(i).get(7), purchaseData.get(i).get(8), purchaseData.get(i).get(9), purchaseData.get(i).get(10), purchaseData.get(i).get(12)));
        }
        return purchaseDetails;
    }

    @FXML
    void btnReset(ActionEvent event) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/view_purchase.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void btnSearch(ActionEvent event) {
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date selectedDate = null;
        if(txt_from_date.getValue() != null)
        {
            txtFromDate = txt_from_date.getValue().toString();
            try {
                selectedDate = sdf.parse(txtFromDate);
                txtFromDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if(txt_to_date.getValue() != null)
        {
            txtToDate = txt_to_date.getValue().toString();
            try {
                selectedDate = sdf.parse(txtToDate);
                txtToDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        txtDay = txt_day.getValue();
        if(txtDay == null)
        {
            txtDay = "All";
        }
        txtInvoiceStatus = txt_invoice_status.getValue();
        if(txtInvoiceStatus == null)
        {
            txtInvoiceStatus = "All";
        }
        filter = true;

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/view_purchase.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void follow(MouseEvent event) {

    }

    @FXML
    void showBonusitems(MouseEvent event) {

    }

    @FXML
    void showDiscountGiven(MouseEvent event) {

    }

    @FXML
    void showInvoices(MouseEvent event) {

    }

    @FXML
    void showNetAmount(MouseEvent event) {

    }

    @FXML
    void showPurchasedItems(MouseEvent event) {

    }
}
