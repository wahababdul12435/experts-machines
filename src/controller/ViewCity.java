package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.*;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.util.Duration;
import model.*;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class ViewCity implements Initializable {
    @FXML
    private TableView<ViewCityInfo> table_viewcity;

    @FXML
    private TableColumn<ViewCityInfo, String> sr_no;

    @FXML
    private TableColumn<ViewCityInfo, String> city_id;

    @FXML
    private TableColumn<ViewCityInfo, String> district_name;

    @FXML
    private TableColumn<ViewCityInfo, String> city_name;

    @FXML
    private TableColumn<ViewCityInfo, String> areas;

    @FXML
    private TableColumn<ViewCityInfo, String> city_status;

    @FXML
    private TableColumn<ViewCityInfo, String> operations;

    @FXML
    private MenuBar menu_bar;

    @FXML
    private Menu pending_orders;

    @FXML
    private StackPane stackPane;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private BorderPane menu_bar1;

    @FXML
    private BorderPane nav_setup;

    @FXML    private AnchorPane inner_anchor;
    @FXML    private Pane btnpane;
    @FXML    private Button btn11;
    @FXML    private FontAwesomeIconView FA_Icon;

    @FXML
    private AnchorPane filter_pane;

    @FXML
    private HBox filter_hbox;

    @FXML
    private JFXTextField txt_city_id;

    @FXML
    private JFXTextField txt_city_name;

    @FXML
    private JFXComboBox<String> txt_district_name;

    @FXML
    private JFXComboBox<String> txt_city_status;

    @FXML
    private HBox summary_hbox;

    @FXML
    private Label lbl_total;

    @FXML
    private Label lbl_active;

    @FXML
    private Label lbl_inactive;


    public ObservableList<ViewCityInfo> citiesDetail;
    PendingOrders objPendingOrders;
    MysqlCon objMysqlCon;
    Statement objStmt;
    Connection objCon;
    private int summaryTotal;
    private int summaryActive;
    private int summaryInActive;

    private ArrayList<String> districtIds = new ArrayList<>();
    private ArrayList<String> districtNames = new ArrayList<>();

    public static String txtCityId = "";
    public static String txtCityName = "";
    public static String txtDistrictId = "";
    public static String txtDistrictName = "";
    public static String txtCityStatus = "";
    public static boolean filter;

    public static Button btnView;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnSetup.setStyle("-fx-background-color: #47ab1e");
        objMysqlCon = new MysqlCon();
        objStmt = objMysqlCon.stmt;
        objCon = objMysqlCon.con;
        ViewCityInfo.stackPane = stackPane;

        CityInfo objCityInfo = new CityInfo();
        districtIds = objCityInfo.getSavedDistrictIds();
        districtNames = objCityInfo.getSavedDistrictNames();
        txt_district_name.getItems().addAll(districtNames);

        inner_anchor.setLeftAnchor(inner_anchor, 280d);
        drawer.setSidePane(nav_setup);
        drawer.open();

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        city_id.setCellValueFactory(new PropertyValueFactory<>("cityId"));
        district_name.setCellValueFactory(new PropertyValueFactory<>("districtName"));
        city_name.setCellValueFactory(new PropertyValueFactory<>("cityName"));
        areas.setCellValueFactory(new PropertyValueFactory<>("areaNames"));
        city_status.setCellValueFactory(new PropertyValueFactory<>("cityStatus"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_viewcity.setItems(parseUserList());
        lbl_total.setText("Cities\n"+summaryTotal);
        lbl_active.setText("Active\n"+summaryActive);
        lbl_inactive.setText("In Active\n"+summaryInActive);
    }

    private ObservableList<ViewCityInfo> parseUserList(){
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        ViewCityInfo objViewCityInfo = new ViewCityInfo();
        citiesDetail = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> cityData;
        if(filter)
        {
            cityData  = objViewCityInfo.getCitiesSearch(objStmt, objCon, txtCityId, txtCityName, txtDistrictId, txtCityStatus);
            txt_city_id.setText(txtCityId);
            txt_city_name.setText(txtCityName);
            txt_district_name.setValue(txtDistrictName);
            txt_city_status.setValue(txtCityStatus);
            filter = false;
            filter_pane.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
        }
        else
        {
            cityData = objViewCityInfo.getCitiesInfo(objStmt, objCon);
        }
        for (int i = 0; i < cityData.size(); i++)
        {
            summaryTotal++;
            if(cityData.get(i).get(7).equals("Active"))
            {
                summaryActive++;
            }
            else if(cityData.get(i).get(7).equals("In Active"))
            {
                summaryInActive++;
            }
            citiesDetail.add(new ViewCityInfo(String.valueOf(i+1), cityData.get(i).get(0), ((cityData.get(i).get(1) == null) ? "N/A" : cityData.get(i).get(1)), cityData.get(i).get(2), ((cityData.get(i).get(3) == null) ? "N/A" : cityData.get(i).get(3)), cityData.get(i).get(4), cityData.get(i).get(5), ((cityData.get(i).get(6) == null) ? "N/A" : cityData.get(i).get(6)), cityData.get(i).get(7)));
        }
        return citiesDetail;
    }

    @FXML
    void btnSearch(ActionEvent event) {
        txtCityId = txt_city_id.getText();
        txtCityName = txt_city_name.getText();
        txtDistrictName = txt_district_name.getValue();
        if(txt_district_name.getSelectionModel().getSelectedIndex() >= 0)
        {
            txtDistrictId = String.valueOf(districtIds.get(txt_district_name.getSelectionModel().getSelectedIndex()));
        }
        txtCityStatus = txt_city_status.getValue();
        if(txtCityStatus == null)
        {
            txtCityStatus = "All";
        }
        filter = true;

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/view_city.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void btnReset(ActionEvent event) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/view_city.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void exportExcel(ActionEvent event) throws IOException {
        Workbook workbook = new HSSFWorkbook();
        Sheet spreadsheet = workbook.createSheet("data");
        Row row = spreadsheet.createRow(0);

        for (int j = 0; j < table_viewcity.getColumns().size()-1; j++) {
            row.createCell(j).setCellValue(table_viewcity.getColumns().get(j).getText());
        }

        for (int i = 0; i < table_viewcity.getItems().size(); i++) {
            row = spreadsheet.createRow(i + 1);
            for (int j = 0; j < table_viewcity.getColumns().size()-1; j++) {
                if(table_viewcity.getColumns().get(j).getCellData(i) != null) {
                    row.createCell(j).setCellValue(table_viewcity.getColumns().get(j).getCellData(i).toString());
                }
                else {
                    row.createCell(j).setCellValue("");
                }
            }
        }

        FileOutputStream fileOut = new FileOutputStream("data/View_Cities.xls");
        workbook.write(fileOut);
        fileOut.close();
    }

    public void follow() throws IOException {
        if (drawer.isOpened()) {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(-250);
            transition.setByY(0);
            transition.setCycleCount(1);
            transition.play();
            drawer.close();
            FA_Icon.setGlyphName("ANGLE_DOUBLE_RIGHT");
            FA_Icon.setGlyphSize(14);
            //inner_anchor.setRightAnchor(inner_anchor,1d);
            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(-90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration, inner_anchor);
            scale.setToX(1.1000);
            scale.setToY(1);

            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale, anchorePaneTransition, transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();

        } else {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(250);
            transition.setByY(0);
            transition.setCycleCount(1);
            FA_Icon.setGlyphName("ANGLE_DOUBLE_LEFT");
            FA_Icon.setGlyphSize(14);
            drawer.open();

            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration, inner_anchor);
            scale.setToX(0.99947777);
            scale.setToY(1);


            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale, anchorePaneTransition, transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();

        }
    }

    @FXML
    void addCity() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/enter_city.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
}
