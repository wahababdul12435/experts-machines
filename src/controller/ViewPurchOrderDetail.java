package controller;

import com.jfoenix.controls.*;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.InputMethodEvent;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.util.StringConverter;
import model.*;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;
import org.controlsfx.control.textfield.TextFields;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class ViewPurchOrderDetail implements Initializable {

    @FXML    private JFXTextField txt_order_id;
    @FXML    private JFXTextField txt_inventoryDays;
    @FXML    private JFXTextField txt_productQuantity;
    @FXML    private JFXTextField txt_PurchaseAmount;
    @FXML    private JFXComboBox<String> combo_company_name;
    @FXML    private TableView<ViewPurchOrderDetailInfo> table_saledetaillog;
    @FXML    private TableColumn<ViewPurchOrderDetailInfo, String> sr_no;
    @FXML    private TableColumn<ViewPurchOrderDetailInfo, String> product_id;
    @FXML    private TableColumn<ViewPurchOrderDetailInfo, String> product_name;
    @FXML    private TableColumn<ViewPurchOrderDetailInfo, String> carton_size;
    @FXML    private TableColumn<ViewPurchOrderDetailInfo, String> quantity_sold;
    @FXML    private TableColumn<ViewPurchOrderDetailInfo, String> quantity_Instock;
    @FXML    private TableColumn<ViewPurchOrderDetailInfo, String> quantity_order;
    @FXML    private TableColumn<ViewPurchOrderDetailInfo, String> purch_price;
    @FXML    private TableColumn<ViewPurchOrderDetailInfo, String> net_amount;
    @FXML    private JFXButton btn_print_invoice;
    @FXML    private JFXButton cancelBtn;

    private ObservableList<ViewPurchOrderDetailInfo> purchDetails;
    private ArrayList<ArrayList<String>> companiesData = new ArrayList<>();
    ArrayList<String> editData;
    public static int srNo;
    public static String purchOrderID;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        TopHeader.btnSale.setStyle("-fx-background-color: #47ab1e");

        UpdateProductInfo objUpdateProductInfo = new UpdateProductInfo();
        companiesData = objUpdateProductInfo.getCompaniesData();
        ArrayList<String> chosenCompanyNames = getArrayColumn(companiesData, 1);
        combo_company_name.getItems().addAll(chosenCompanyNames);

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        product_id.setCellValueFactory(new PropertyValueFactory<>("productID"));
        product_name.setCellValueFactory(new PropertyValueFactory<>("productName"));
        carton_size.setCellValueFactory(new PropertyValueFactory<>("cartonSize"));
        quantity_sold.setCellValueFactory(new PropertyValueFactory<>("soldQauntity"));
        quantity_Instock.setCellValueFactory(new PropertyValueFactory<>("quantityInstock"));
        quantity_order.setCellValueFactory(new PropertyValueFactory<>("orderQauntity"));
        purch_price.setCellValueFactory(new PropertyValueFactory<>("purchPrice"));
        net_amount.setCellValueFactory(new PropertyValueFactory<>("netAmount"));
        table_saledetaillog.setItems(parseUserList());

        PurchaseOrderInfo objpurchOrder = new PurchaseOrderInfo();
        MysqlCon objectsMysqlCon = new MysqlCon();
        Statement objectsStmt = objectsMysqlCon.stmt;
        Connection objectsCon = objectsMysqlCon.con;

        editData = objpurchOrder.getpurchOrder_edit(objectsStmt,objectsCon,purchOrderID);

        txt_order_id.setText(purchOrderID);
        txt_PurchaseAmount.setText(editData.get(0));
        txt_productQuantity.setText(editData.get(1));
        txt_inventoryDays.setText(editData.get(2));
        combo_company_name.setValue(editData.get(3));

    }

    public ObservableList<ViewPurchOrderDetailInfo> parseUserList() {
        purchDetails = FXCollections.observableArrayList();
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;

        ArrayList<ArrayList<String>> purchData;
        ViewPurchOrderDetailInfo objpurchorderDetail = new ViewPurchOrderDetailInfo();
        purchData = objpurchorderDetail.getOrderData(objStmt, objCon,purchOrderID);
        for (int i = 0; i < purchData.size(); i++)
        {
            purchDetails.add(new ViewPurchOrderDetailInfo(String.valueOf(i+1), purchData.get(i).get(0), purchData.get(i).get(1),  purchData.get(i).get(2), purchData.get(i).get(3), purchData.get(i).get(4),  purchData.get(i).get(5), purchData.get(i).get(6), purchData.get(i).get(7)));
        }

        return purchDetails;
    }

    public ArrayList<String> getArrayColumn(ArrayList<ArrayList<String>> citiesData, int columnIndex)
    {
        ArrayList<String> columnList = new ArrayList<>();
        for(ArrayList<String> row: citiesData) {
            columnList.add(row.get(columnIndex));
        }
        return columnList;
    }

    @FXML
    void cancelClicked(ActionEvent event) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/view_purchase_order.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

}
