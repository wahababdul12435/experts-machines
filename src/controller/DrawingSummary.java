package controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTreeView;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.util.Duration;
import model.*;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;
import org.controlsfx.control.CheckTreeView;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.ResourceBundle;

public class DrawingSummary implements Initializable {
    @FXML    private Button print_drawingsummary;
    @FXML    private TableView<DrawingSummaryInfo> Area_tableview;
    @FXML    private TableView<DrawingSummaryInfo> table_drawingsummary;
    @FXML    private TableColumn<DrawingSummaryInfo, String> sr_no;
    @FXML    private TableColumn<DrawingSummaryInfo, String> product_no;
    @FXML   private TableColumn<DrawingSummaryInfo, String> product_name;
    @FXML    private TableColumn<DrawingSummaryInfo, String> product_batch;
    @FXML    private TableColumn<DrawingSummaryInfo, String> ordered_quantity;
    @FXML    private TableColumn<DrawingSummaryInfo, String> bonusquantity;
    @FXML    private TableColumn<DrawingSummaryInfo, String> tradeprice;
    @FXML    private TableColumn<DrawingSummaryInfo, String> totalamount;
    @FXML    private TableColumn<DrawingSummaryInfo, String> operations;
    @FXML    private Button filter_drawingsummary;
    @FXML   private Button reset_drawingsummary;
    @FXML   private DatePicker datePick_summaryFrom;
    @FXML   private DatePicker datePick_summaryTo;
    @FXML   private TextField txt_InvoNum_To;
    @FXML   private TextField txt_InvoNum_From;
    @FXML    private TextField txt_totalNetAmount;
    @FXML    private TextField txt_totalQuantity;
    @FXML    private TextField txt_totalBonus;
    @FXML    private TextField txt_totalprods;
    @FXML    private AnchorPane inner_anchor;
    @FXML    private JFXComboBox<String> combo_invoice_status;
    @FXML    private JFXDrawer drawer;
    @FXML    private Button btn11;
    @FXML    private Pane btnpane;
    @FXML    private BorderPane nav_sales;
    @FXML    private BorderPane menu_bar1;
    @FXML    private CheckTreeView<String> treeview;
    @FXML    private FontAwesomeIconView FA_Icon;
    Alert a = new Alert(Alert.AlertType.NONE);
    @FXML
    private HBox menu_bar;

    public ObservableList<DrawingSummaryInfo> drawingSummary;

    public ArrayList<DrawingSummaryInfo> summaryData;




    ArrayList<String> Areaslist = new ArrayList<>();
    ArrayList<String> SubAreaslist = new ArrayList<>();
    ArrayList<String> getcheckedareas = new ArrayList<>();
    ArrayList<String> getsubareasID = new ArrayList<String>();
    int k =0;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        combo_invoice_status.setValue("Pending");
        MysqlCon newobjMysqlConnc = new MysqlCon();
        Statement objectStattmt = newobjMysqlConnc.stmt;
        Connection objectConnnec = newobjMysqlConnc.con;
        DrawingSummaryInfo objctdrwngSummary = new DrawingSummaryInfo();

        Areaslist = objctdrwngSummary.getAreas(objectStattmt,objectConnnec);

        CheckBoxTreeItem<String> root = new CheckBoxTreeItem<String>("Select All");
        for(int i=0;i<Areaslist.size();i++)
        {
            String[] splitAreas = Areaslist.get(i).split("--");

            CheckBoxTreeItem<String> node1 = new CheckBoxTreeItem<String>(splitAreas[1]);
            MysqlCon objMysqlConnc = new MysqlCon();
            Statement objctStattmt = objMysqlConnc.stmt;
            Connection objctConnnec = objMysqlConnc.con;
            DrawingSummaryInfo objctdrawngSummary = new DrawingSummaryInfo();

            int intval_Areaid = Integer.parseInt(splitAreas[0]);
            SubAreaslist = objctdrawngSummary.getSubAreas(objctStattmt,objctConnnec,intval_Areaid);

            for(int j=0;j<SubAreaslist.size();j++)
            {
                String[] splitSubAreas = SubAreaslist.get(j).split("--");
                CheckBoxTreeItem<String> node2 = new CheckBoxTreeItem<String>(splitSubAreas[1]);
                node1.setExpanded(false);
                node1.getChildren().add(node2);
            }
            root.getChildren().add(node1);
             treeview.setRoot(root);
             root.setExpanded(true);

            treeview.setShowRoot(true);
        }

        treeview.getCheckModel().getCheckedItems().addListener(new ListChangeListener<TreeItem<String>>() {
            @Override
            public void onChanged(Change<? extends TreeItem<String>> c) {
                //System.out.println(treeview.getCheckModel().getCheckedItems());
                String nval = treeview.getCheckModel().getCheckedItems().toString();
                String valueAndIndex = c.toString();
                String[] splitedvalueAndIndex = valueAndIndex.split(" ]] ");
                String[] splitedValue = splitedvalueAndIndex[0].split(": ");
                String[] getfunctionAndIndex = splitedvalueAndIndex[1].split(" at ");
                String[] getindx = getfunctionAndIndex[1].split(" ");
                String strngindex = getindx[0];
                int indexvalue= Integer.parseInt(strngindex);
                if(getfunctionAndIndex[0].equals("added"))
                {
                    getcheckedareas.add(splitedValue[1]);
                    /*if(getcheckedareas.size()>0) {
                        MysqlCon objMysqlCon = new MysqlCon();
                        Statement objctStmt = objMysqlCon.stmt;
                        Connection objctCnc = objMysqlCon.con;
                        DrawingSummaryInfo objctdrawngSummary = new DrawingSummaryInfo();
                        String checkAreasVal = getcheckedareas.get(k);
                        String subAreaIDs = objctdrawngSummary.getSubAreasCode(objctStmt, objctCnc, checkAreasVal);
                        if (subAreaIDs.equals("")) {
                        } else {
                            getsubareasID.add(subAreaIDs);
                        }
                    }*/
                    k++;
                }
                if(getfunctionAndIndex[0].equals("removed"))
                {
                    getcheckedareas.remove(indexvalue);
                    k--;
                }
                //String mvalu = getcheckedareas.get(1);
               }
        });

        summaryData = new ArrayList<>();
        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        product_no.setCellValueFactory(new PropertyValueFactory<>("productId"));
        product_name.setCellValueFactory(new PropertyValueFactory<>("productName"));
        product_batch.setCellValueFactory(new PropertyValueFactory<>("productBatch"));
        ordered_quantity.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        bonusquantity.setCellValueFactory(new PropertyValueFactory<>("bonusQuantity"));
        tradeprice.setCellValueFactory(new PropertyValueFactory<>("tradePrice"));
        totalamount.setCellValueFactory(new PropertyValueFactory<>("totalAmount"));

        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        try {
            table_drawingsummary.setItems(parseUserList());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        btnpane.setOnMouseEntered(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(22);
                btnpane.setMinWidth(20.0);
                btn11.setMinWidth(20.0);

            }
        });
        btnpane.setOnMouseExited(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(14);
                btnpane.setMinWidth(10.0);
                btn11.setMinWidth(10.0);
            }
        });
        inner_anchor.setLeftAnchor(inner_anchor, 260d);
        drawer.setSidePane(nav_sales);
        drawer.open();
    }

    public void follow() throws IOException
    {
    /*PathTransition transition;
    transition = new PathTransition();
    Polyline mpoly = new Polyline();
    mpoly.getPoints().addAll(new Double[]{
            0.0,0.0,
            0.0,0.0,
            -200.0,0.0});
    transition.setNode(btn11);
    transition.setDuration(Duration.seconds(2));
    transition.setPath(mpoly);
    transition.play();*/
        if (drawer.isOpened()) {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(-250);
            transition.setByY(0);
            transition.setCycleCount(1);
            transition.play();
            drawer.close();
            FA_Icon.setGlyphName("ANGLE_DOUBLE_RIGHT");
            FA_Icon.setGlyphSize(14);
            //inner_anchor.setRightAnchor(inner_anchor,1d);


            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(-90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(1.28000);
            scale.setToY(1);



            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();
            //
            //inner_anchor.setLeftAnchor(inner_anchor, 5d);
        } else {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(250);
            transition.setByY(0);
            transition.setCycleCount(1);
            FA_Icon.setGlyphName("ANGLE_DOUBLE_LEFT");
            FA_Icon.setGlyphSize(14);
            drawer.open();

            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(0.99947777);
            scale.setToY(1);



            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();

        }


    }

    ArrayList<ArrayList<String>> getINvodetails = new ArrayList<>();
    public void filterSummary() throws IOException, ParseException {
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String InvoToDate = "";
        String InvofromDate = "";
        if (datePick_summaryFrom.getValue() != null || datePick_summaryTo.getValue() != null) {
            InvofromDate = datePick_summaryFrom.getValue().toString();

            Date selectedDate = null;
            try {
                selectedDate = sdf.parse(InvofromDate);
                InvofromDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            InvoToDate = datePick_summaryTo.getValue().toString();
            Date selectedToDate = null;
            try {
                selectedToDate = sdf.parse(InvoToDate);
                InvoToDate = sdf2.format(selectedToDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        String invoFromVal = txt_InvoNum_From.getText();
        String invoToVal = txt_InvoNum_To.getText();
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        DrawingSummaryInfo objDrawingSummaryInfo = new DrawingSummaryInfo();
        drawingSummary = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> drawingSummaryData = objDrawingSummaryInfo.getDrawingSummary(objStmt, objCon,combo_invoice_status.getValue().toString(),invoFromVal,invoToVal,getcheckedareas,InvofromDate,InvoToDate);
        for (int i = 0; i < drawingSummaryData.size(); i++)
        {
            drawingSummary.add(new DrawingSummaryInfo(String.valueOf(i+1), drawingSummaryData.get(i).get(0), drawingSummaryData.get(i).get(1), drawingSummaryData.get(i).get(2), drawingSummaryData.get(i).get(3), drawingSummaryData.get(i).get(4)  , drawingSummaryData.get(i).get(5) , drawingSummaryData.get(i).get(6) ));
            summaryData.add(new DrawingSummaryInfo(String.valueOf(i+1), drawingSummaryData.get(i).get(0), drawingSummaryData.get(i).get(1), drawingSummaryData.get(i).get(2), drawingSummaryData.get(i).get(3), drawingSummaryData.get(i).get(4), drawingSummaryData.get(i).get(5) , drawingSummaryData.get(i).get(6)));
        }
        table_drawingsummary.setItems(drawingSummary);

        txt_InvoNum_From.setText("");
        txt_InvoNum_To.setText("");
        datePick_summaryTo.setValue(null);
        datePick_summaryFrom.setValue(null);
        treeview.getCheckModel().clearChecks();
    }

    private ObservableList<DrawingSummaryInfo> parseUserList() throws ParseException {


        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String InvoToDate = "";
        String InvofromDate = "";
        if (datePick_summaryFrom.getValue() != null || datePick_summaryTo.getValue() != null) {
            InvofromDate = datePick_summaryFrom.getValue().toString();

            Date selectedDate = null;
            try {
                selectedDate = sdf.parse(InvofromDate);
                InvofromDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            InvoToDate = datePick_summaryTo.getValue().toString();
            Date selectedToDate = null;
            try {
                selectedToDate = sdf.parse(InvoToDate);
                InvoToDate = sdf2.format(selectedToDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        String invoFromVal = txt_InvoNum_From.getText();
        String invoToVal = txt_InvoNum_To.getText();
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        DrawingSummaryInfo objDrawingSummaryInfo = new DrawingSummaryInfo();
        drawingSummary = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> drawingSummaryData = objDrawingSummaryInfo.getDrawingSummary(objStmt, objCon,combo_invoice_status.getValue().toString(),invoFromVal,invoToVal,getcheckedareas,InvofromDate,InvoToDate);
        for (int i = 0; i < drawingSummaryData.size(); i++)
        {
            drawingSummary.add(new DrawingSummaryInfo(String.valueOf(i+1), drawingSummaryData.get(i).get(0), drawingSummaryData.get(i).get(1), drawingSummaryData.get(i).get(2), drawingSummaryData.get(i).get(3), drawingSummaryData.get(i).get(4)  , drawingSummaryData.get(i).get(5) , drawingSummaryData.get(i).get(6) ));
            summaryData.add(new DrawingSummaryInfo(String.valueOf(i+1), drawingSummaryData.get(i).get(0), drawingSummaryData.get(i).get(1), drawingSummaryData.get(i).get(2), drawingSummaryData.get(i).get(3), drawingSummaryData.get(i).get(4), drawingSummaryData.get(i).get(5) , drawingSummaryData.get(i).get(6)));
        }

        Float sumval =0.0f;
        int totalBonus =0;
        int totalQuant = 0;
        int prodID = 0;
        for(int i = 0; i<drawingSummary.size();i++)
        {
            float newval = Float.parseFloat(drawingSummary.get(i).getTotalAmount());
            int BonusVal = Integer.parseInt(drawingSummary.get(i).getBonusQuantity());
            int QuantVal = Integer.parseInt(drawingSummary.get(i).getQuantity());
            sumval = sumval + newval;
            totalBonus = totalBonus + BonusVal;
            totalQuant = totalQuant + QuantVal;
            prodID = prodID + 1;
        }
        txt_totalNetAmount.setText(String.valueOf(sumval));
        txt_totalBonus.setText(String.valueOf(totalBonus));
        txt_totalQuantity.setText(String.valueOf(totalQuant));
        txt_totalprods.setText(String.valueOf(prodID));

        return drawingSummary;

    }


    public void resetsummary_click() throws IOException, ParseException {
        drawingSummary.removeAll();
        txt_InvoNum_From.setText("");
        txt_InvoNum_To.setText("");
        datePick_summaryTo.setValue(null);
        datePick_summaryFrom.setValue(null);
        String invoFromVal = txt_InvoNum_From.getText();
        String invoToVal = txt_InvoNum_To.getText();
        treeview.getCheckModel().clearChecks();
        summaryData.clear();
        table_drawingsummary.getItems().removeAll(drawingSummary);


        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String InvoToDate = "";
        String InvofromDate = "";
        if (datePick_summaryFrom.getValue() != null || datePick_summaryTo.getValue() != null) {
            InvofromDate = datePick_summaryFrom.getValue().toString();

            Date selectedDate = null;
            try {
                selectedDate = sdf.parse(InvofromDate);
                InvofromDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            InvoToDate = datePick_summaryTo.getValue().toString();
            Date selectedToDate = null;
            try {
                selectedToDate = sdf.parse(InvoToDate);
                InvoToDate = sdf2.format(selectedToDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }


        DrawingSummaryInfo objDrawingSummaryInfo = new DrawingSummaryInfo();
        MysqlCon objMysqlCon4 = new MysqlCon();
        Statement simple_statment = objMysqlCon4.stmt;
        Connection simple_connect = objMysqlCon4.con;
        ArrayList<ArrayList<String>> drawingSummaryData = objDrawingSummaryInfo.getDrawingSummary(simple_statment, simple_connect,combo_invoice_status.getValue().toString(),invoFromVal,invoToVal,getcheckedareas,InvofromDate,InvoToDate);
        for (int i = 0; i < drawingSummaryData.size(); i++)
        {
            drawingSummary.add(new DrawingSummaryInfo(String.valueOf(i+1), drawingSummaryData.get(i).get(0), drawingSummaryData.get(i).get(1), drawingSummaryData.get(i).get(2), drawingSummaryData.get(i).get(3), drawingSummaryData.get(i).get(4)  , drawingSummaryData.get(i).get(5) , drawingSummaryData.get(i).get(6) ));
            summaryData.add(new DrawingSummaryInfo(String.valueOf(i+1), drawingSummaryData.get(i).get(0), drawingSummaryData.get(i).get(1), drawingSummaryData.get(i).get(2), drawingSummaryData.get(i).get(3), drawingSummaryData.get(i).get(4), drawingSummaryData.get(i).get(5) , drawingSummaryData.get(i).get(6)));
        }
        table_drawingsummary.setItems(drawingSummary);
    }


    public void generateDrawingSummary(ArrayList<DrawingSummaryInfo> summaryList) throws JRException
    {
        InputStream objIO = DrawingSummary.class.getResourceAsStream("/reports/DrawingSummary.jrxml");
        JasperReport objReport = JasperCompileManager.compileReport(objIO);
        JasperPrint objPrint = JasperFillManager.fillReport(objReport, null, new JRBeanCollectionDataSource(summaryList));
        JasperViewer.viewReport(objPrint, false);
    }
    public void printDrawingSummary() throws JRException
    {
        generateDrawingSummary(summaryData);
    }


    public void generateDrawingSummary1(ArrayList<String> summaryList) throws JRException {
        InputStream objIO = DrawingSummary.class.getResourceAsStream("/reports/DrawingSummary.jrxml");
        JasperReport objReport = JasperCompileManager.compileReport(objIO);
        JasperPrint objPrint = JasperFillManager.fillReport(objReport, null, new JRBeanCollectionDataSource(summaryList));
        JasperViewer.viewReport(objPrint, false);
    }
    public ArrayList<String> summryData;
    public void printDrawingSummary1() throws JRException {

        generateDrawingSummary1(summryData);
    }


    public void viewHome() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/home.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }
    public void viewPendings() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_pending_orders.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }
    public void SaleInvoices() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/sale_invoice.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }
    public void addCompany() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/enter_company.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }
    @FXML
    public void viewCompanies() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_company.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }
    @FXML
    void addArea() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/enter_area.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }
    @FXML
    public void viewAreas() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_area.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }
    @FXML
    public void viewSubAreas() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_subarea.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }
    @FXML
    void addDealer() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/enter_dealer.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }
    @FXML
    public void viewDealers() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_dealers.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }
    @FXML
    void addSalesman() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/enter_salesman.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }
    @FXML
    public void viewSalesman() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_salesman.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }
    @FXML
    void addSupplier() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/enter_supplier.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }
    @FXML
    public void viewSuppliers() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_suppliers.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }
    @FXML
    void addProduct() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/enter_new_product.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }
    @FXML
    public void viewProducts() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_products.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }
    @FXML
    public void viewSystemAccounts() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/system_accounts.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }
    @FXML
    public void viewDrawingSummary() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/drawing_summary.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }
}
