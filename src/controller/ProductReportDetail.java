package controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXDrawer;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.util.StringConverter;
import model.*;

import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class ProductReportDetail implements Initializable {

    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private Label lbl_product_id;

    @FXML
    private Label lbl_product_name;

    @FXML
    private Label lbl_product_type;

    @FXML
    private Label lbl_retail_price;

    @FXML
    private Label lbl_company_name;

    @FXML
    private Label lbl_group_name;

    @FXML
    private Label lbl_dealer_created;

    @FXML
    private Label lbl_dealer_updated;

    @FXML
    private Label lbl_dealer_status;

    @FXML
    private AnchorPane filter_pane;

    @FXML
    private JFXDatePicker txt_from_date;

    @FXML
    private JFXDatePicker txt_to_date;

    @FXML
    private JFXComboBox<String> txt_day;

    @FXML
    private HBox summary_hbox1;

    @FXML
    private Label lbl_purchase_qty;

    @FXML
    private Label lbl_purchase_return;

    @FXML
    private Label lbl_sale_qty;

    @FXML
    private Label lbl_sale_return;

    @FXML
    private TableView<ProductReportDetailInfo> table_productreportlog;

    @FXML
    private TableColumn<ProductReportDetailInfo, String> sr_no;

    @FXML
    private TableColumn<ProductReportDetailInfo, String> date;

    @FXML
    private TableColumn<ProductReportDetailInfo, String> day;

    @FXML
    private TableColumn<ProductReportDetailInfo, String> purchase_qty;

    @FXML
    private TableColumn<ProductReportDetailInfo, String> purchase_amount;

    @FXML
    private TableColumn<ProductReportDetailInfo, String> purchase_return_qty;

    @FXML
    private TableColumn<ProductReportDetailInfo, String> purchase_return_amount;

    @FXML
    private TableColumn<ProductReportDetailInfo, String> sale_qty;

    @FXML
    private TableColumn<ProductReportDetailInfo, String> sale_amount;

    @FXML
    private TableColumn<ProductReportDetailInfo, String> sale_return_qty;

    @FXML
    private TableColumn<ProductReportDetailInfo, String> sale_return_amount;

    @FXML
    private TableColumn<ProductReportDetailInfo, String> operations;

    @FXML
    private StackPane stackPane;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    @FXML
    private BorderPane brdrpane_navfinances;

    @FXML
    private BorderPane menu_bar;

    private ProductInfo objProductInfo;
    private String currentProductId;
    private String selectedImagePath;
    private ArrayList<String> productInfo;
    private ObservableList<ProductReportDetailInfo> dealerDetails;
    private ProductReportDetailInfo objProductReportDetailInfo;
    ArrayList<ArrayList<String>> productReportData;
    private float purchaseQty = 0;
    private float purchaseReturn = 0;
    private float saleQty = 0;
    private float saleReturn = 0;

    public static String txtFromDate = "";
    public static String txtToDate = "";
    public static String txtDay = "All";
    public static boolean filter;

    MysqlCon objMysqlCon1;
    MysqlCon objMysqlCon2;
    MysqlCon objMysqlCon3;
    Statement objStmt1;
    Statement objStmt2;
    Statement objStmt3;
    Connection objCon;

    public static MenuButton btnView;
    SimpleDateFormat fmt = new SimpleDateFormat("dd/MMM/yyyy");

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnReport.setStyle("-fx-background-color: #47ab1e");
        objMysqlCon1 = new MysqlCon();
        objMysqlCon2 = new MysqlCon();
        objMysqlCon3 = new MysqlCon();
        objStmt1 = objMysqlCon1.stmt;
        objStmt2 = objMysqlCon2.stmt;
        objStmt3 = objMysqlCon3.stmt;
        objCon = objMysqlCon1.con;
        TopHeader.btnFinance.setStyle("-fx-background-color: #47ab1e");
        objProductInfo = new ProductInfo();
        objProductReportDetailInfo = new ProductReportDetailInfo();
        productReportData = new ArrayList<>();
        if(!ProductReportInfo.productReportId.equals(""))
        {
            currentProductId = ProductReportInfo.productReportId;
            ProductReportInfo.productReportId = "";
        }
        productInfo = objProductInfo.getProductDetail(currentProductId);
        lbl_product_id.setText(((productInfo.get(0) == null) ? "N/A" : productInfo.get(0)));
        lbl_product_name.setText(productInfo.get(1));
        lbl_product_type.setText(productInfo.get(2));
        lbl_retail_price.setText((productInfo.get(3) == null) ? "N/A" : productInfo.get(3));
        lbl_company_name.setText(productInfo.get(4));
        lbl_group_name.setText((productInfo.get(5) == null) ? "N/A" : productInfo.get(5));
        lbl_dealer_created.setText(productInfo.get(6));
        lbl_dealer_updated.setText((productInfo.get(7) == null) ? "N/A" : productInfo.get(7));
        lbl_dealer_status.setText(productInfo.get(8));

        txt_from_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        txt_to_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        date.setCellValueFactory(new PropertyValueFactory<>("date"));
        day.setCellValueFactory(new PropertyValueFactory<>("day"));
        purchase_qty.setCellValueFactory(new PropertyValueFactory<>("purchaseQty"));
        purchase_amount.setCellValueFactory(new PropertyValueFactory<>("purchaseAmount"));
        purchase_return_qty.setCellValueFactory(new PropertyValueFactory<>("purchaseReturn"));
        purchase_return_amount.setCellValueFactory(new PropertyValueFactory<>("purchaseReturnAmount"));
        sale_qty.setCellValueFactory(new PropertyValueFactory<>("saleQty"));
        sale_amount.setCellValueFactory(new PropertyValueFactory<>("saleAmount"));
        sale_return_qty.setCellValueFactory(new PropertyValueFactory<>("saleReturn"));
        sale_return_amount.setCellValueFactory(new PropertyValueFactory<>("saleReturnAmount"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_productreportlog.setItems(parseUserList());
        lbl_purchase_qty.setText("Purchase Qty\n"+String.format("%,.0f", purchaseQty));
        lbl_purchase_return.setText("Purchase Return\n"+String.format("%,.0f", purchaseReturn));
        lbl_sale_qty.setText("Sale Qty\n"+String.format("%,.0f", saleQty));
        lbl_sale_return.setText("Sale Return\n"+String.format("%,.0f", saleReturn));
    }

    private ObservableList<ProductReportDetailInfo> parseUserList() {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        dealerDetails = FXCollections.observableArrayList();

        if (filter) {
//            try {
//                productReportData = objProductReportDetailInfo.getDealerReportDetail(objStmt1, objStmt2, objStmt3, objCon, currentProductId, txtFromDate, txtToDate, txtDay);
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
            if (txtFromDate != null && !txtFromDate.equals("")) {
                txt_from_date.setValue(GlobalVariables.LOCAL_DATE(txtFromDate));
            }
            if (txtToDate != null && !txtToDate.equals("")) {
                txt_to_date.setValue(GlobalVariables.LOCAL_DATE(txtToDate));
            }
            txt_day.setValue(txtDay);
            filter = false;
            filter_pane.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
            txtFromDate = "";
            txtToDate = "";
            txtDay = "All";
        } else {
            try {
                productReportData = objProductReportDetailInfo.getDealerReportDetail(objStmt1, objStmt2, objStmt3, objCon, currentProductId);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }


        for (int i = 0; i < productReportData.size(); i++)
        {
            if(!productReportData.get(i).get(2).equals("-"))
            {
                purchaseQty += Integer.parseInt(productReportData.get(i).get(2));
            }
            if(!productReportData.get(i).get(4).equals("-"))
            {
                purchaseReturn += Integer.parseInt(productReportData.get(i).get(4));
            }
            if(!productReportData.get(i).get(6).equals("-"))
            {
                saleQty += Integer.parseInt(productReportData.get(i).get(6));
            }
            if(!productReportData.get(i).get(8).equals("-"))
            {
                saleReturn += Integer.parseInt(productReportData.get(i).get(8));
            }
            dealerDetails.add(new ProductReportDetailInfo(String.valueOf(i+1), ((productReportData.get(i).get(0) == null || productReportData.get(i).get(0).equals("")) ? "N/A" : productReportData.get(i).get(0)), productReportData.get(i).get(1), productReportData.get(i).get(2), productReportData.get(i).get(3), productReportData.get(i).get(4), productReportData.get(i).get(5), productReportData.get(i).get(6), productReportData.get(i).get(7), productReportData.get(i).get(8), productReportData.get(i).get(9)));
        }
        return dealerDetails;
    }

    @FXML
    void btnReset(ActionEvent event) {

    }

    @FXML
    void btnSearch(ActionEvent event) {

    }

    @FXML
    void follow(MouseEvent event) {

    }

    @FXML
    void showPurchaseQty(MouseEvent event) {

    }

    @FXML
    void showPurchaseReturn(MouseEvent event) {

    }

    @FXML
    void showSaleQty(MouseEvent event) {

    }

    @FXML
    void showSaleReturn(MouseEvent event) {

    }
}
