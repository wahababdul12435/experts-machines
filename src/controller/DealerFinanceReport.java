package controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import model.DealerFinanceReportInfo;
import model.GlobalVariables;
import model.MysqlCon;
import model.DealerFinanceReportInfo;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class DealerFinanceReport implements Initializable {
    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private TableView<DealerFinanceReportInfo> table_dealerfinance;

    @FXML
    private TableColumn<DealerFinanceReportInfo, String> sr_no;

    @FXML
    private TableColumn<DealerFinanceReportInfo, String> dealer_id;

    @FXML
    private TableColumn<DealerFinanceReportInfo, String> dealer_name;

    @FXML
    private TableColumn<DealerFinanceReportInfo, String> area_name;

    @FXML
    private TableColumn<DealerFinanceReportInfo, String> dealer_type;

    @FXML
    private TableColumn<DealerFinanceReportInfo, String> total_sale;

    @FXML
    private TableColumn<DealerFinanceReportInfo, String> cash_collection;

    @FXML
    private TableColumn<DealerFinanceReportInfo, String> cash_return;

    @FXML
    private TableColumn<DealerFinanceReportInfo, String> cash_discount;

    @FXML
    private TableColumn<DealerFinanceReportInfo, String> cash_waive_off;

    @FXML
    private TableColumn<DealerFinanceReportInfo, String> cash_pending;

    @FXML
    private TableColumn<DealerFinanceReportInfo, String> operations;

    @FXML
    private AnchorPane filter_pane;

    @FXML
    private HBox filter_hbox;

    @FXML
    private JFXTextField txt_dealer_id;

    @FXML
    private JFXTextField txt_dealer_name;

    @FXML
    private JFXTextField txt_area_name;

    @FXML
    private JFXComboBox<String> txt_dealer_type;

    @FXML
    private HBox summary_hbox1;

    @FXML
    private Label lbl_total_sale;

    @FXML
    private Label lbl_cash_collection;

    @FXML
    private Label lbl_cash_return;

    @FXML
    private HBox summary_hbox2;

    @FXML
    private Label lbl_cash_discount;

    @FXML
    private Label lbl_cash_waiveoff;

    @FXML
    private Label lbl_cash_pending;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    @FXML
    private BorderPane brdrpane_navfinances;

    @FXML
    private BorderPane menu_bar;

    MysqlCon objMysqlCon;
    Statement objStmt;
    Connection objCon;
    static float totalSale = 0;
    static float cashCollection = 0;
    static float cashReturn = 0;
    static float cashDiscount = 0;
    static float cashWaiveOff = 0;
    static float cashPending = 0;

    public static String txtDealerId = "";
    public static String txtDealerName = "";
    public static String txtAreaName = "";
    public static String txtDealerType = "";
    public static boolean filter;
    public static String summary = "";

    public static MenuButton btnView;
    public ObservableList<DealerFinanceReportInfo> dealersFinanceDetail;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnReport.setStyle("-fx-background-color: #47ab1e");
        objMysqlCon = new MysqlCon();
        objStmt = objMysqlCon.stmt;
        objCon = objMysqlCon.con;

        inner_anchor.setLeftAnchor(inner_anchor, 280d);
        drawer.setSidePane(brdrpane_navfinances);
        drawer.open();

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        dealer_id.setCellValueFactory(new PropertyValueFactory<>("dealerId"));
        dealer_name.setCellValueFactory(new PropertyValueFactory<>("dealerName"));
        area_name.setCellValueFactory(new PropertyValueFactory<>("areaName"));
        dealer_type.setCellValueFactory(new PropertyValueFactory<>("dealerType"));
        total_sale.setCellValueFactory(new PropertyValueFactory<>("totalSale"));
        cash_collection.setCellValueFactory(new PropertyValueFactory<>("cashCollection"));
        cash_return.setCellValueFactory(new PropertyValueFactory<>("cashReturn"));
        cash_discount.setCellValueFactory(new PropertyValueFactory<>("cashDiscount"));
        cash_waive_off.setCellValueFactory(new PropertyValueFactory<>("cashWaiveOff"));
        cash_pending.setCellValueFactory(new PropertyValueFactory<>("cashPending"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_dealerfinance.setItems(parseUserList());
        lbl_total_sale.setText("Sales\n"+String.format("%,.0f", totalSale));
        lbl_cash_collection.setText("Collection\n"+String.format("%,.0f", cashCollection));
        lbl_cash_return.setText("Return\n"+String.format("%,.0f", cashReturn));
        lbl_cash_discount.setText("Discount\n"+String.format("%,.0f", cashDiscount));
        lbl_cash_waiveoff.setText("Waive Off\n"+String.format("%,.0f", cashWaiveOff));
        lbl_cash_pending.setText("Pending\n"+String.format("%,.0f", cashPending));
        summary = "";
    }

    private ObservableList<DealerFinanceReportInfo> parseUserList() {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        DealerFinanceReportInfo objDealerFinanceReportInfo = new DealerFinanceReportInfo();
        dealersFinanceDetail = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> dealerFinanceData;
        if(filter)
        {
            dealerFinanceData  = objDealerFinanceReportInfo.getDealersFinanceSearch(objStmt, objCon, txtDealerId, txtDealerName, txtAreaName, txtDealerType);
            txt_dealer_id.setText(txtDealerId);
            txt_dealer_name.setText(txtDealerName);
            txt_area_name.setText(txtAreaName);
            txt_dealer_type.setValue(txtDealerType);
            filter = false;
            filter_pane.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
            totalSale = 0;
            cashCollection = 0;
            cashReturn = 0;
            cashDiscount = 0;
            cashWaiveOff = 0;
            cashPending = 0;
        }
        else if(summary.equals("Sale"))
        {
            dealerFinanceData = objDealerFinanceReportInfo.getDealersFinanceSummary(objStmt, objCon, summary);
            lbl_total_sale.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
        }
        else if(summary.equals("Collection"))
        {
            dealerFinanceData = objDealerFinanceReportInfo.getDealersFinanceSummary(objStmt, objCon, summary);
            lbl_cash_collection.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
        }
        else if(summary.equals("Return"))
        {
            dealerFinanceData = objDealerFinanceReportInfo.getDealersFinanceSummary(objStmt, objCon, summary);
            lbl_cash_return.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
        }
        else if(summary.equals("Discount"))
        {
            dealerFinanceData = objDealerFinanceReportInfo.getDealersFinanceSummary(objStmt, objCon, summary);
            lbl_cash_discount.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
        }
        else if(summary.equals("Pending"))
        {
            dealerFinanceData = objDealerFinanceReportInfo.getDealersFinanceSummary(objStmt, objCon, summary);
            lbl_cash_pending.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
        }
        else if(summary.equals("Waive Off"))
        {
            dealerFinanceData = objDealerFinanceReportInfo.getDealersFinanceSummary(objStmt, objCon, summary);
            lbl_cash_waiveoff.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
        }
        else
        {
            dealerFinanceData = objDealerFinanceReportInfo.getDealersFinanceInfo(objStmt, objCon);
            totalSale = 0;
            cashCollection = 0;
            cashReturn = 0;
            cashDiscount = 0;
            cashWaiveOff = 0;
            cashPending = 0;
        }
        for (int i = 0; i < dealerFinanceData.size(); i++)
        {
            if(summary.equals(""))
            {
                totalSale += (dealerFinanceData.get(i).get(5) != null) ? Float.parseFloat(dealerFinanceData.get(i).get(5)) : 0;
                cashCollection += (dealerFinanceData.get(i).get(6) != null) ? Float.parseFloat(dealerFinanceData.get(i).get(6)) : 0;
                cashReturn += (dealerFinanceData.get(i).get(7) != null) ? Float.parseFloat(dealerFinanceData.get(i).get(7)) : 0;
                cashDiscount += (dealerFinanceData.get(i).get(8) != null) ? Float.parseFloat(dealerFinanceData.get(i).get(8)) : 0;
                cashWaiveOff += (dealerFinanceData.get(i).get(9) != null) ? Float.parseFloat(dealerFinanceData.get(i).get(9)) : 0;
                cashPending += (dealerFinanceData.get(i).get(10) != null) ? Float.parseFloat(dealerFinanceData.get(i).get(10)) : 0;
            }
            dealersFinanceDetail.add(new DealerFinanceReportInfo(String.valueOf(i+1), dealerFinanceData.get(i).get(0), ((dealerFinanceData.get(i).get(1) == null) ? "N/A" : dealerFinanceData.get(i).get(1)), dealerFinanceData.get(i).get(2), ((dealerFinanceData.get(i).get(3) == null) ? "N/A" : dealerFinanceData.get(i).get(3)), ((dealerFinanceData.get(i).get(4) == null) ? "0" : dealerFinanceData.get(i).get(4)), ((dealerFinanceData.get(i).get(5) == null) ? "0" : dealerFinanceData.get(i).get(5)), ((dealerFinanceData.get(i).get(6) == null) ? "0" : dealerFinanceData.get(i).get(6)), ((dealerFinanceData.get(i).get(7) == null) ? "0" : dealerFinanceData.get(i).get(7)), ((dealerFinanceData.get(i).get(8) == null) ? "0" : dealerFinanceData.get(i).get(8)), ((dealerFinanceData.get(i).get(9) == null) ? "0" : dealerFinanceData.get(i).get(9)), ((dealerFinanceData.get(i).get(10) == null) ? "0" : dealerFinanceData.get(i).get(10))));
        }
        return dealersFinanceDetail;
    }

    @FXML
    void follow(MouseEvent event) {

    }

    @FXML
    void btnReset(ActionEvent event) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/dealer_finance.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void btnSearch(ActionEvent event) {
        txtDealerId = txt_dealer_id.getText();
        txtDealerName = txt_dealer_name.getText();
        txtAreaName = txt_area_name.getText();
        txtDealerType = txt_dealer_type.getValue();
        if(txtDealerType == null)
        {
            txtDealerType = "All";
        }
        filter = true;

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/dealer_finance.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void addCash(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/add_dealer_finance.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void showCollection(MouseEvent event) {
        summary = "Collection";

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/dealer_finance.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void showDiscount(MouseEvent event) {
        summary = "Discount";

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/dealer_finance.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void showPending(MouseEvent event) {
        summary = "Pending";

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/dealer_finance.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void showReturn(MouseEvent event) {
        summary = "Return";

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/dealer_finance.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void showSale(MouseEvent event) {
        summary = "Sale";

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/dealer_finance.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void showWaiveOff(MouseEvent event) {
        summary = "Waive Off";

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/dealer_finance.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
}
