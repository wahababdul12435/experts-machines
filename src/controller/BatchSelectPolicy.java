package controller;

import com.jfoenix.controls.JFXDrawer;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import model.BatchSelectionPolicy;
import model.MysqlCon;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ResourceBundle;


public class BatchSelectPolicy implements Initializable {

    Alert a = new Alert(Alert.AlertType.NONE);

    @FXML private CheckBox entry_fifo;
    @FXML private CheckBox entry_lifo;
    @FXML private CheckBox entry_filo;
    @FXML private CheckBox entry_lilo;
    @FXML private CheckBox expiry_fifo;
    @FXML private CheckBox expiry_lifo;
    @FXML private CheckBox expiry_filo;
    @FXML private CheckBox expiry_lilo;
    @FXML private CheckBox entrydate_selection;
    @FXML private CheckBox expirydate_selection;
    @FXML private Button btn_save;




    @FXML private Button cancelBtn;

    public void initialize(URL url, ResourceBundle resourceBundle)
    {


        entry_fifo.setDisable(true);
        entry_filo.setDisable(true);
        entry_lifo.setDisable(true);
        entry_lilo.setDisable(true);
        expiry_fifo.setDisable(true);
        expiry_filo.setDisable(true);
        expiry_lifo.setDisable(true);
        expiry_lilo.setDisable(true);

    }

   public void Select_entryDate() throws IOException
   {
       if(entrydate_selection.isSelected())
       {
           entry_fifo.setDisable(false);
           entry_filo.setDisable(false);
           entry_lifo.setDisable(false);
           entry_lilo.setDisable(false);

       }
       if(!entrydate_selection.isSelected())
       {
           entry_fifo.setDisable(true);
           entry_filo.setDisable(true);
           entry_lifo.setDisable(true);
           entry_lilo.setDisable(true);
       }

   }

    public void Select_expiryDate() throws IOException
    {
        if(expirydate_selection.isSelected())
        {
            expiry_fifo.setDisable(false);
            expiry_filo.setDisable(false);
            expiry_lifo.setDisable(false);
            expiry_lilo.setDisable(false);

        }
        if(!expirydate_selection.isSelected())
        {
            expiry_fifo.setDisable(true);
            expiry_filo.setDisable(true);
            expiry_lifo.setDisable(true);
            expiry_lilo.setDisable(true);
        }

    }
String policy_value = "";
    public void cancelClicked() throws IOException
    {

    }
    String getprevPolicy[] = new String[5];
    public void saveClicked() throws IOException {
        if(expiry_fifo.isSelected())
        {
            policy_value="EXPIRY FIFO";
        }
        if(expiry_lifo.isSelected())
        {
            policy_value="EXPIRY LIFO";
        }
        if(expiry_filo.isSelected())
        {
            policy_value="EXPIRY FILO";
        }
        if(expiry_lilo.isSelected())
        {
            policy_value="EXPIRY LILO";
        }
        if (entry_lilo.isSelected())
        {
            policy_value="ENTRY LILO";
        }
        if(entry_lifo.isSelected())
        {
            policy_value="ENTRY LIFO";
        }
        if(entry_filo.isSelected())
        {
            policy_value="ENTRY FILO";
        }
        if(entry_fifo.isSelected())
        {
            policy_value="ENTRY FIFO";
        }
        if(entrydate_selection.isSelected() )
        {
            if (entry_fifo.isSelected() || entry_filo.isSelected() || entry_lilo.isSelected() || entry_lifo.isSelected() )
            {
                BatchSelectionPolicy objBatchSelectionPolicy = new BatchSelectionPolicy();
                MysqlCon mysqlOBJct = new MysqlCon();
                Statement mystamtOBJct = mysqlOBJct.stmt;
                Connection mysqlConnobjct = mysqlOBJct.con;
                getprevPolicy = objBatchSelectionPolicy.getprev_batchslctionPolicy(mystamtOBJct,mysqlConnobjct,1);
                if(getprevPolicy[0]==null){}
                else
                {
                    MysqlCon objMysqlCon = new MysqlCon();
                    Statement objStmt = objMysqlCon.stmt;
                    Connection objCon = objMysqlCon.con;
                    objBatchSelectionPolicy.updateSelectedPolicy(objStmt, objCon, policy_value);
                }

            }
            else
            {
                a.setAlertType(Alert.AlertType.ERROR);
                a.setHeaderText("Checkbox not Selected!");
                a.setContentText("Please select any option.");
                a.show();
            }
        }

        else if( expirydate_selection.isSelected())
        {
            if (expiry_fifo.isSelected() || expiry_filo.isSelected() || expiry_lilo.isSelected() || expiry_lifo.isSelected())
            {
                BatchSelectionPolicy objBatchSelectionPolicy = new BatchSelectionPolicy();
                MysqlCon mysqlOBJct = new MysqlCon();
                Statement mystamtOBJct = mysqlOBJct.stmt;
                Connection mysqlConnobjct = mysqlOBJct.con;
                getprevPolicy = objBatchSelectionPolicy.getprev_batchslctionPolicy(mystamtOBJct,mysqlConnobjct,1);
                if(getprevPolicy[0]==null){}
                else
                {
                    MysqlCon objMysqlCon = new MysqlCon();
                    Statement objStmt = objMysqlCon.stmt;
                    Connection objCon = objMysqlCon.con;
                    objBatchSelectionPolicy.updateSelectedPolicy(objStmt, objCon, policy_value);
                }
            }
            else
            {
                a.setAlertType(Alert.AlertType.ERROR);
                a.setHeaderText("Checkbox not Selected!");
                a.setContentText("Please select any option.");
                a.show();
            }

        }
        else
        {
                a.setAlertType(Alert.AlertType.ERROR);
                a.setHeaderText("Checkbox not Selected!");
                a.setContentText("Please select any option.");
                a.show();

        }
        Stage stage = (Stage) btn_save.getScene().getWindow();
        // do what you have to do
        stage.close();
    }

    public void saveenterPress() throws IOException {
        if(expiry_fifo.isSelected())
        {
            policy_value="EXPIRY FIFO";
        }
        if(expiry_lifo.isSelected())
        {
            policy_value="EXPIRY LIFO";
        }
        if(expiry_filo.isSelected())
        {
            policy_value="EXPIRY FILO";
        }
        if(expiry_lilo.isSelected())
        {
            policy_value="EXPIRY LILO";
        }
        if (entry_lilo.isSelected())
        {
            policy_value="ENTRY LILO";
        }
        if(entry_lifo.isSelected())
        {
            policy_value="ENTRY LIFO";
        }
        if(entry_filo.isSelected())
        {
            policy_value="ENTRY FILO";
        }
        if(entry_fifo.isSelected())
        {
            policy_value="ENTRY FIFO";
        }

        BatchSelectionPolicy objBatchSelectionPolicy = new BatchSelectionPolicy();
        MysqlCon mysqlOBJct = new MysqlCon();
        Statement mystamtOBJct = mysqlOBJct.stmt;
        Connection mysqlConnobjct = mysqlOBJct.con;
        getprevPolicy = objBatchSelectionPolicy.getprev_batchslctionPolicy(mystamtOBJct,mysqlConnobjct,1);
        if(getprevPolicy[0]==null){}
        else
        {
            MysqlCon objMysqlCon = new MysqlCon();
            Statement objStmt = objMysqlCon.stmt;
            Connection objCon = objMysqlCon.con;
            objBatchSelectionPolicy.updateSelectedPolicy(objStmt, objCon, policy_value);
        }



    }
}
