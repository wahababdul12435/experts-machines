package controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXDrawer;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.util.StringConverter;
import model.*;
import model.ViewPurchaseOrderInfo;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

public class ViewPurchaseOrder implements Initializable {

    @FXML    private AnchorPane inner_anchor;
    @FXML    private AnchorPane filter_pane;
    @FXML    private JFXDatePicker datepick_from_date;
    @FXML    private JFXDatePicker datepick_to_date;
    @FXML    private JFXComboBox<String> combo_companyName;
    @FXML    private JFXComboBox<String> combo_order_status;
    @FXML    private HBox summary_hbox1;
    @FXML    private Label lbl_invoices;
    @FXML    private Label lbl_submitted_items;
    @FXML    private Label lbl_missed_items;
    @FXML    private HBox summary_hbox2;
    @FXML    private Label lbl_returned_items;
    @FXML    private Label lbl_missed_price;
    @FXML    private Label lbl_return_price;
    @FXML    private Label lbl_invoice_price;

    @FXML
    private Label lbl_discount_given;

    @FXML
    private TableView<ViewPurchaseOrderInfo> table_viewPurchOrderlog;

    @FXML    private TableColumn<ViewPurchaseOrderInfo, String> sr_no;
    @FXML    private TableColumn<ViewPurchaseOrderInfo, String> order_id;
    @FXML    private TableColumn<ViewPurchaseOrderInfo, String> order_date;
    @FXML    private TableColumn<ViewPurchaseOrderInfo, String> order_amount;
    @FXML    private TableColumn<ViewPurchaseOrderInfo, String> company_id;
    @FXML    private TableColumn<ViewPurchaseOrderInfo, String> order_status;
    @FXML    private TableColumn<ViewPurchaseOrderInfo, String> operations;
    @FXML    private StackPane stackPane;
    @FXML    private JFXDrawer drawer;
    @FXML    private Pane btnpane;
    @FXML    private Button btn11;
    @FXML    private FontAwesomeIconView FA_Icon;
    @FXML    private BorderPane brdrpane_navfinances;
    @FXML    private BorderPane menu_bar;

    private ObservableList<ViewPurchaseOrderInfo> purchDetails;
    private ViewSalesInfo objViewSalesInfo;
    ArrayList<ArrayList<String>> saleData;
    ArrayList<ArrayList<String>> purchData;
    public ArrayList<InvoiceInfo> invoiceData;
    private static ViewSaleDetailInfo objViewSaleDetailInfo;
    private SaleInvoicePrintInfo objSaleInvoicePrintInfo;
    ArrayList<String> invoicePrintData;

    int summaryActive = 0;
    int summaryInActive = 0;
    int summaryTotal = 0;
    float missedItems = 0;
    float returnedItems = 0;
    float missedPrice = 0;
    float returnPrice = 0;
    float invoicePrice = 0;
    float discountGiven = 0;
    float pendingInvoices = 0;

    public static String txtFromDate = "";
    public static String txtToDate = "";
    public static String txtDay = "All";
    public static String txtInvoiceStatus = "All";
    public static boolean filter;

    MysqlCon objMysqlCon1;
    MysqlCon objMysqlCon2;
    MysqlCon objMysqlCon3;
    Statement objStmt1;
    Statement objStmt2;
    Statement objStmt3;
    Connection objCon;

    public static Button btnView;
    SimpleDateFormat fmt = new SimpleDateFormat("dd/MMM/yyyy");

    @Override
    public void initialize(URL location, ResourceBundle resources) {
//        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnPurchase.setStyle("-fx-background-color: #47ab1e");
        objMysqlCon1 = new MysqlCon();
        objMysqlCon2 = new MysqlCon();
        objMysqlCon3 = new MysqlCon();
        objStmt1 = objMysqlCon1.stmt;
        objStmt2 = objMysqlCon2.stmt;
        objStmt3 = objMysqlCon3.stmt;
        objCon = objMysqlCon1.con;
        objViewSalesInfo = new ViewSalesInfo();
        objSaleInvoicePrintInfo = new SaleInvoicePrintInfo();
        invoicePrintData = objSaleInvoicePrintInfo.getSaleInvoicePrintInfo(objStmt1, objCon);
        ViewPurchaseOrderInfo.stackPane = stackPane;

        datepick_to_date.setValue(LocalDate.now());
        datepick_from_date.setValue(LocalDate.now());

        saleData = new ArrayList<>();
        invoiceData = new ArrayList<>();

        datepick_from_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        datepick_to_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        order_id.setCellValueFactory(new PropertyValueFactory<>("order_id"));
        company_id.setCellValueFactory(new PropertyValueFactory<>("company_id"));
        order_date.setCellValueFactory(new PropertyValueFactory<>("order_date"));
        order_amount.setCellValueFactory(new PropertyValueFactory<>("order_amount"));
        order_status.setCellValueFactory(new PropertyValueFactory<>("orderStatus"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_viewPurchOrderlog.setItems(parseUserList());
    }

    private ObservableList<ViewPurchaseOrderInfo> parseUserList(){
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        LocalDate datefromDate = datepick_from_date.getValue();
        String datepick_toDate = datepick_to_date.getValue().toString();
        String datepick_fromDate = datepick_to_date.getValue().toString();
        String comboCompanyName = combo_companyName.getValue();
        String comboStatus = combo_companyName.getValue();

        ViewPurchaseOrderInfo objViewProductsInfo = new ViewPurchaseOrderInfo();
        purchDetails = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> purchData;
        if(filter)
        {
            purchData  = objViewProductsInfo.getPurchaseOrderSearch(objStmt, objCon, datepick_fromDate, datepick_toDate, comboCompanyName, comboStatus);

            filter = false;
            filter_pane.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
        }
        else
        {
            purchData = objViewProductsInfo.getPurchaseOrder(objStmt, objCon);
        }
        for (int i = 0; i < purchData.size(); i++)
        {
            summaryTotal++;
            if(purchData.get(i).get(4).equals("Pending"))
            {
                summaryActive++;
            }
            else if(purchData.get(i).get(4).equals("Received"))
            {
                summaryInActive++;
            }
            ViewPurchaseOrderInfo objPurchOrder = new ViewPurchaseOrderInfo();

            purchDetails.add(new ViewPurchaseOrderInfo(String.valueOf(i+1), purchData.get(i).get(0), purchData.get(i).get(1),  purchData.get(i).get(2), purchData.get(i).get(3), purchData.get(i).get(4)));
        }
        return purchDetails;
    }

    @FXML
    void btnReset(ActionEvent event) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/view_sales.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void btnSearch(ActionEvent event) {
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date selectedDate = null;
        if(datepick_from_date.getValue() != null)
        {
            txtFromDate = datepick_to_date.getValue().toString();
            try {
                selectedDate = sdf.parse(txtFromDate);
                txtFromDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if(datepick_to_date.getValue() != null)
        {
            txtToDate = datepick_to_date.getValue().toString();
            try {
                selectedDate = sdf.parse(txtToDate);
                txtToDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        txtInvoiceStatus = combo_order_status.getValue();
        if(txtInvoiceStatus == null)
        {
            txtInvoiceStatus = "All";
        }
        filter = true;

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/view_sales.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void newPurchOrder(ActionEvent event) throws JRException {
        Parent parent = null;
        try {
            parent = FXMLLoader.load(getClass().getResource("../view/purchase_order.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(parent);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void follow(MouseEvent event) {

    }

    @FXML
    void showInvoicePrice(MouseEvent event) {

    }

    @FXML
    void showWPendingInvoices(MouseEvent event) {

    }

    @FXML
    void showInvoices(MouseEvent event) {

    }

    @FXML
    void showMissedItems(MouseEvent event) {

    }

    @FXML
    void showMissedPrice(MouseEvent event) {

    }

    @FXML
    void showOrderedItems(MouseEvent event) {

    }

    @FXML
    void showReturnedItems(MouseEvent event) {

    }

    @FXML
    void showSubmittedItems(MouseEvent event) {

    }

    @FXML
    void showWDiscountGiven(MouseEvent event) {

    }
}
