package controller;

import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.transitions.hamburger.HamburgerBackArrowBasicTransition;
import com.jfoenix.transitions.hamburger.HamburgerBasicCloseTransition;
import com.jfoenix.transitions.hamburger.HamburgerSlideCloseTransition;
import de.jensd.fx.glyphs.GlyphsDude;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.animation.ParallelTransition;
import javafx.animation.PathTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.shape.Polyline;
import javafx.stage.Stage;
import javafx.util.Duration;
import model.GlobalVariables;
import model.MysqlCon;
import model.PendingOrders;
import controller.TopHeader;
import org.controlsfx.glyphfont.FontAwesome;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ResourceBundle;

public class Home implements Initializable {

    @FXML
    private BorderPane menu_bar;
    @FXML
    private Button orders;
    @FXML
    private JFXDrawer drawer;
    @FXML
    private VBox Vbox_btns;
    @FXML
    private AnchorPane inner_anchor;
    @FXML
    private Button btn11;
    @FXML
    private Pane btnpane;
    @FXML
    private FontAwesomeIconView FA_Icon;
    @FXML
    private BorderPane Nav_home;

    PendingOrders objPendingOrders;
    MysqlCon objMysqlCon;
    Statement objStmt;
    Connection objCon;
    int unSeenOrders;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        unSeenOrders = 0;
        objMysqlCon = new MysqlCon();
        objStmt = objMysqlCon.stmt;
        objCon = objMysqlCon.con;
        objPendingOrders = new PendingOrders();
        unSeenOrders = objPendingOrders.getUnSeenOrders(objStmt, objCon);
        orders.setText("Pending Orders ("+unSeenOrders+")");

        btnpane.setOnMouseEntered(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(22);
                btnpane.setMinWidth(20.0);
                btn11.setMinWidth(20.0);

            }
        });
        btnpane.setOnMouseExited(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(14);
                btnpane.setMinWidth(10.0);
                btn11.setMinWidth(10.0);
            }
        });
        inner_anchor.setLeftAnchor(inner_anchor, 270d);
        drawer.setSidePane(Nav_home);
        drawer.open();
    }
    public void addtest() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/ordered_products_detail.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

public void follow() throws IOException
{
    /*PathTransition transition;
    transition = new PathTransition();
    Polyline mpoly = new Polyline();
    mpoly.getPoints().addAll(new Double[]{
            0.0,0.0,
            0.0,0.0,
            -200.0,0.0});
    transition.setNode(btn11);
    transition.setDuration(Duration.seconds(2));
    transition.setPath(mpoly);
    transition.play();*/
    if (drawer.isOpened()) {
        Duration duration = Duration.seconds(0.4);
        //Create new translate transition
        TranslateTransition transition = new TranslateTransition(duration, btnpane);
        transition.setByX(-250);
        transition.setByY(0);
        transition.setCycleCount(1);
        transition.play();
        drawer.close();
        FA_Icon.setGlyphName("ANGLE_DOUBLE_RIGHT");
        FA_Icon.setGlyphSize(14);
        //inner_anchor.setRightAnchor(inner_anchor,1d);


        TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
        anchorePaneTransition.setByX(-90);
        anchorePaneTransition.setByY(0);
        anchorePaneTransition.setCycleCount(1);

        ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
        scale.setToX(1.31017);
        scale.setToY(1);



        ParallelTransition pltTransition = new ParallelTransition();
        pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
        pltTransition.setCycleCount(1);
        pltTransition.play();
       //
        //inner_anchor.setLeftAnchor(inner_anchor, 5d);
    } else {
        Duration duration = Duration.seconds(0.4);
        //Create new translate transition
        TranslateTransition transition = new TranslateTransition(duration, btnpane);
        transition.setByX(250);
        transition.setByY(0);
        transition.setCycleCount(1);
        FA_Icon.setGlyphName("ANGLE_DOUBLE_LEFT");
        FA_Icon.setGlyphSize(14);
        drawer.open();

        TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
        anchorePaneTransition.setByX(90);
        anchorePaneTransition.setByY(0);
        anchorePaneTransition.setCycleCount(1);

        ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
        scale.setToX(0.99947777);
        scale.setToY(1);



        ParallelTransition pltTransition = new ParallelTransition();
        pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
        pltTransition.setCycleCount(1);
        pltTransition.play();

    }


}

    public void viewHome() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/home.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    public void viewPendings() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_pending_orders.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
    public  void viewbonuspolicies() throws IOException
    {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_bonus.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
    public  void viewdiscountpolicies() throws IOException
    {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_discount.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    public  void set_bonus_policy() throws IOException
    {
        Parent root = FXMLLoader.load(getClass().getResource("../view/bonus_policy.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
    public  void set_disc_policy() throws IOException
    {
        Parent root = FXMLLoader.load(getClass().getResource("../view/discount_policy.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
    //

    public void batchslctionSetting() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/batch_selection_policy.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
    public void printPageInfo() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/print_page.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
    public void viewInvoStockAdjust() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/adjust_invoice_stock.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }



    public void addCompany() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/enter_company.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    public void viewCompanies() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_company.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
    @FXML
    public void viewAreas() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_area.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    public void viewSubAreas() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_subarea.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
    @FXML
    public void viewDealers() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_dealers.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
    @FXML
    public void viewSalesman() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_salesman.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
    @FXML
    public void viewSuppliers() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_suppliers.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
    @FXML
    public void viewProducts() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_products.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    public void viewSystemAccounts() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/system_accounts.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    public void viewDrawingSummary() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/drawing_summary.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    public void viewCollectionSummary() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/collection_summary.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    public void viewInvoicesDetail() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/invoices_detail.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    public void viewGPSTracking() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/gps_tracking.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
}
