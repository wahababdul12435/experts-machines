package controller;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Worker;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.StackPane;

import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import model.*;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.view.JasperViewer;
import netscape.javascript.JSObject;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.util.*;

public class ViewPendingOrders implements Initializable {
    @FXML
    private TableView<ViewPendingOrdersInfo> table_viewpendingorders;

    @FXML
    private TableColumn<ViewPendingOrdersInfo, String> sr_no;

    @FXML
    private TableColumn<ViewPendingOrdersInfo, String> order_id;

    @FXML
    private TableColumn<ViewPendingOrdersInfo, String> dealer_name;

    @FXML
    private TableColumn<ViewPendingOrdersInfo, String> dealer_contact;

    @FXML
    private TableColumn<ViewPendingOrdersInfo, String> dealer_address;

    @FXML
    private TableColumn<ViewPendingOrdersInfo, String> ordered_item;

    @FXML
    private TableColumn<ViewPendingOrdersInfo, String> allocated_batch;

    @FXML
    private TableColumn<ViewPendingOrdersInfo, String> quantity;

    @FXML
    private TableColumn<ViewPendingOrdersInfo, String> unit;

    @FXML
    private TableColumn<ViewPendingOrdersInfo, String> total_price;

    @FXML
    private TableColumn<ViewPendingOrdersInfo, String> date;

    @FXML
    private TableColumn<ViewPendingOrdersInfo, String> time;

    @FXML
    private TableColumn<ViewPendingOrdersInfo, String> user_name;

    @FXML
    private TableColumn<ViewPendingOrdersInfo, String> order_status;

    @FXML
    private TableColumn<ViewPendingOrdersInfo, String> operations;

    @FXML
    private MenuBar menu_bar;

    @FXML
    private Menu pending_orders;

    @FXML
    private StackPane stackPane;


    public ObservableList<ViewPendingOrdersInfo> pendingOrdersDetail;
    PendingOrders objPendingOrders;
    MysqlCon objMysqlCon;
    Statement objStmt;
    Connection objCon;
    int unSeenOrders;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        unSeenOrders = 0;
        objMysqlCon = new MysqlCon();
        objStmt = objMysqlCon.stmt;
        objCon = objMysqlCon.con;
        objPendingOrders = new PendingOrders();
        unSeenOrders = objPendingOrders.getUnSeenOrders(objStmt, objCon);
        pending_orders.setText("Pending Orders (" + unSeenOrders + ")");
        ViewPendingOrdersInfo.stackPane = stackPane;

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        order_id.setCellValueFactory(new PropertyValueFactory<>("orderId"));
        dealer_name.setCellValueFactory(new PropertyValueFactory<>("dealerName"));
        dealer_contact.setCellValueFactory(new PropertyValueFactory<>("dealerContact"));
        dealer_address.setCellValueFactory(new PropertyValueFactory<>("dealerAddress"));
        ordered_item.setCellValueFactory(new PropertyValueFactory<>("orderedItem"));
        allocated_batch.setCellValueFactory(new PropertyValueFactory<>("allocatedBatch"));
        quantity.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        unit.setCellValueFactory(new PropertyValueFactory<>("unit"));
        total_price.setCellValueFactory(new PropertyValueFactory<>("totalFinalPrice"));
        date.setCellValueFactory(new PropertyValueFactory<>("date"));
        time.setCellValueFactory(new PropertyValueFactory<>("time"));
        user_name.setCellValueFactory(new PropertyValueFactory<>("userName"));
        order_status.setCellValueFactory(new PropertyValueFactory<>("orderStatus"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_viewpendingorders.setItems(parseUserList());

//        drawer.setSidePane(Vbox_btns);
//        HamburgerBackArrowBasicTransition transition = new HamburgerBackArrowBasicTransition(hamburger);
//        transition.setRate(-1);
//        hamburger.addEventHandler(MouseEvent.MOUSE_PRESSED, (e) -> {
//            transition.setRate(transition.getRate() * -1);
//            transition.play();
//
//            if (drawer.isOpened()) {
//                drawer.close();
//                inner_anchor.setLeftAnchor(inner_anchor, 5d);
//            } else {
//                drawer.open();
//                inner_anchor.setLeftAnchor(inner_anchor, 305d);
//            }
//        });
    }

    private ObservableList<ViewPendingOrdersInfo> parseUserList(){
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        ViewPendingOrdersInfo objViewPendingOrdersInfo = new ViewPendingOrdersInfo();
        pendingOrdersDetail = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> pendingOrdersData = objViewPendingOrdersInfo.getPendingOrdersInfo(objStmt, objCon);
        for (int i = 0; i < pendingOrdersData.size(); i++)
        {
//            System.out.println(String.valueOf(i+1)+"   ---   "+pendingOrdersData.get(i).get(0)+"   ---   "+pendingOrdersData.get(i).get(1)+"   ---   "+pendingOrdersData.get(i).get(2)+"   ---   "+pendingOrdersData.get(i).get(3)+"   ---   "+pendingOrdersData.get(i).get(4)+"   ---   "+pendingOrdersData.get(i).get(5)+"   ---   "+pendingOrdersData.get(i).get(6)+"   ---   "+pendingOrdersData.get(i).get(7)+"   ---   "+pendingOrdersData.get(i).get(8)+"   ---   "+pendingOrdersData.get(i).get(9)+"   ---   "+pendingOrdersData.get(i).get(10)+"   ---   "+pendingOrdersData.get(i).get(11)+"   ---   "+pendingOrdersData.get(i).get(12)+"   ---   "+pendingOrdersData.get(i).get(13)+"   ---   "+pendingOrdersData.get(i).get(14)+"   ---   "+pendingOrdersData.get(i).get(15)+"   ---   "+pendingOrdersData.get(i).get(16)+"   ---   "+pendingOrdersData.get(i).get(17));
            pendingOrdersDetail.add(new ViewPendingOrdersInfo(String.valueOf(i+1), pendingOrdersData.get(i).get(0), pendingOrdersData.get(i).get(1), pendingOrdersData.get(i).get(2), pendingOrdersData.get(i).get(3), pendingOrdersData.get(i).get(4), pendingOrdersData.get(i).get(5), pendingOrdersData.get(i).get(6), pendingOrdersData.get(i).get(7), pendingOrdersData.get(i).get(8), pendingOrdersData.get(i).get(9), pendingOrdersData.get(i).get(10), pendingOrdersData.get(i).get(11), pendingOrdersData.get(i).get(12), pendingOrdersData.get(i).get(13), pendingOrdersData.get(i).get(14), pendingOrdersData.get(i).get(15), pendingOrdersData.get(i).get(16), pendingOrdersData.get(i).get(17), pendingOrdersData.get(i).get(18)));
        }
        return pendingOrdersDetail;
    }


    public void viewHome() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/home.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

}
