package controller;

import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import model.GlobalVariables;
import model.MysqlCon;
import model.UserReportInfo;
import model.UserReportInfo;

import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class UserReport implements Initializable {

    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private TableView<UserReportInfo> table_userreport;

    @FXML
    private TableColumn<UserReportInfo, String> sr_no;

    @FXML
    private TableColumn<UserReportInfo, String> user_id;

    @FXML
    private TableColumn<UserReportInfo, String> user_name;

    @FXML
    private TableColumn<UserReportInfo, String> user_type;

    @FXML
    private TableColumn<UserReportInfo, String> orders_booked;

    @FXML
    private TableColumn<UserReportInfo, String> orders_delivered;

    @FXML
    private TableColumn<UserReportInfo, String> items_returned;

    @FXML
    private TableColumn<UserReportInfo, String> booking_price;

    @FXML
    private TableColumn<UserReportInfo, String> return_price;

    @FXML
    private TableColumn<UserReportInfo, String> cash_collection;

    @FXML
    private TableColumn<UserReportInfo, String> cash_returned;

    @FXML
    private TableColumn<UserReportInfo, String> cash_waiveoff;

    @FXML
    private TableColumn<UserReportInfo, String> days;

    @FXML
    private TableColumn<UserReportInfo, String> user_status;

    @FXML
    private TableColumn<UserReportInfo, String> operations;

    @FXML
    private AnchorPane filter_pane;

    @FXML
    private HBox filter_hbox;

    @FXML
    private JFXTextField txt_user_id;

    @FXML
    private JFXTextField txt_user_name;

    @FXML
    private HBox summary_hbox1;

    @FXML
    private Label lbl_total_users;

    @FXML
    private Label lbl_total_active;

    @FXML
    private Label lbl_total_inactive;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    @FXML
    private BorderPane brdrpane_navfinances;

    @FXML
    private BorderPane menu_bar;

    MysqlCon objMysqlCon1;
    Statement objStmt1;
    Connection objCon1;
    private static float totalUsers = 0;
    private static float totalActive = 0;
    private static float totalInactive = 0;

    public static String txtProductId = "";
    public static String txtProductName = "";
    public static String txtProductType = "";
    public static boolean filter;
    public static String summary = "";

    public static MenuButton btnView;
    public ObservableList<UserReportInfo> userReportDetail;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnReport.setStyle("-fx-background-color: #47ab1e");
        objMysqlCon1 = new MysqlCon();
        objStmt1 = objMysqlCon1.stmt;
        objCon1 = objMysqlCon1.con;

        inner_anchor.setLeftAnchor(inner_anchor, 280d);
        drawer.setSidePane(brdrpane_navfinances);
        drawer.open();

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        user_id.setCellValueFactory(new PropertyValueFactory<>("userId"));
        user_name.setCellValueFactory(new PropertyValueFactory<>("userName"));
        user_type.setCellValueFactory(new PropertyValueFactory<>("userType"));
        orders_booked.setCellValueFactory(new PropertyValueFactory<>("bookings"));
        orders_delivered.setCellValueFactory(new PropertyValueFactory<>("delivered"));
        items_returned.setCellValueFactory(new PropertyValueFactory<>("itemsReturned"));
        booking_price.setCellValueFactory(new PropertyValueFactory<>("bookingPrice"));
        return_price.setCellValueFactory(new PropertyValueFactory<>("returnPrice"));
        cash_collection.setCellValueFactory(new PropertyValueFactory<>("cashCollection"));
        cash_returned.setCellValueFactory(new PropertyValueFactory<>("cashReturned"));
        cash_waiveoff.setCellValueFactory(new PropertyValueFactory<>("cashWaivedOff"));
        days.setCellValueFactory(new PropertyValueFactory<>("days"));
        user_status.setCellValueFactory(new PropertyValueFactory<>("userStatus"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_userreport.setItems(parseUserList());
        lbl_total_users.setText("Users\n"+String.format("%,.0f", totalUsers));
        lbl_total_active.setText("Active\n"+String.format("%,.0f", totalActive));
        lbl_total_inactive.setText("In Active\n"+String.format("%,.0f", totalInactive));
        summary = "";
    }

    private ObservableList<UserReportInfo> parseUserList() {
        MysqlCon objMysqlCon2 = new MysqlCon();
        Statement objStmt2 = objMysqlCon2.stmt;
        Connection objCon2 = objMysqlCon2.con;
        UserReportInfo objUserReportInfo = new UserReportInfo();
        userReportDetail = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> userReportData;
        userReportData = objUserReportInfo.getUserReportInfo(GlobalVariables.objStmt, objStmt1, objStmt2, objCon1);
        for (int i = 0; i < userReportData.size(); i++)
        {
            if(summary.equals(""))
            {
                totalUsers++;
                if(userReportData.get(i).get(13).equals("Active"))
                {
                    totalActive++;
                }
                else
                {
                    totalInactive++;
                }
            }
            userReportDetail.add(new UserReportInfo(String.valueOf(i+1), userReportData.get(i).get(0), ((userReportData.get(i).get(1) == null) ? "N/A" : userReportData.get(i).get(1)), userReportData.get(i).get(2), ((userReportData.get(i).get(3) == null) ? "N/A" : userReportData.get(i).get(3)), ((userReportData.get(i).get(4) == null) ? "0" : userReportData.get(i).get(4)), ((userReportData.get(i).get(5) == null) ? "0" : userReportData.get(i).get(5)), ((userReportData.get(i).get(6) == null) ? "0" : userReportData.get(i).get(6)), ((userReportData.get(i).get(7) == null) ? "0" : String.format("%,.0f", Float.parseFloat(userReportData.get(i).get(7)))), ((userReportData.get(i).get(8) == null) ? "0" : String.format("%,.0f", Float.parseFloat(userReportData.get(i).get(8)))), ((userReportData.get(i).get(9) == null) ? "0" : String.format("%,.0f", Float.parseFloat(userReportData.get(i).get(9)))), ((userReportData.get(i).get(10) == null) ? "0" : String.format("%,.0f", Float.parseFloat(userReportData.get(i).get(10)))), ((userReportData.get(i).get(11) == null) ? "0" : String.format("%,.0f", Float.parseFloat(userReportData.get(i).get(11)))), ((userReportData.get(i).get(12) == null) || (userReportData.get(i).get(12).equals("N/A")) ? "0" : String.format("%,.0f", Float.parseFloat(userReportData.get(i).get(12)))), ((userReportData.get(i).get(13) == null) ? "0" : userReportData.get(i).get(13))));
        }
        return userReportDetail;
    }

    @FXML
    void addUser(ActionEvent event) {

    }

    @FXML
    void btnReset(ActionEvent event) {

    }

    @FXML
    void btnSearch(ActionEvent event) {

    }

    @FXML
    void follow(MouseEvent event) {

    }

    @FXML
    void showAllActive(MouseEvent event) {

    }

    @FXML
    void showAllInactive(MouseEvent event) {

    }

    @FXML
    void showAllUsers(MouseEvent event) {

    }


}
