package controller;

import com.jfoenix.controls.*;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.InputMethodEvent;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.util.StringConverter;
import model.*;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;
import org.controlsfx.control.textfield.TextFields;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class ViewSaleDetail implements Initializable {

    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private JFXTextField txt_invoice_no;

    @FXML
    private JFXTextField txt_dealer_name;

    @FXML
    private JFXTextField txt_dealer_contact;

    @FXML
    private JFXTextField txt_invoice_price;

    @FXML
    private JFXTextField txt_discount_given;

    @FXML
    private JFXComboBox<String> txt_invoice_status;

    @FXML
    private JFXComboBox<String> txt_booking_user;

    @FXML
    private JFXDatePicker txt_booking_date;

    @FXML
    private JFXTimePicker txt_booking_time;

    @FXML
    private JFXComboBox<String> txt_delviered_user;

    @FXML
    private JFXDatePicker txt_delivered_date;

    @FXML
    private JFXTimePicker txt_delivered_time;

    @FXML
    private ImageView img_user;


    @FXML
    private JFXTextField txt_product_name;

    @FXML
    private JFXComboBox<String> txt_product_batch;

    @FXML
    private JFXTextField txt_product_quantity;

//    @FXML
//    private JFXComboBox<String> txt_product_unit;

    @FXML
    private JFXTextField txt_product_bonus;

    @FXML
    private JFXButton btn_cancel_edit;

    @FXML
    private JFXButton btn_update_edit;

    @FXML
    private JFXButton btn_add;

    @FXML
    private HBox summary_hbox1;

    @FXML
    private Label lbl_products;

    @FXML
    private Label lbl_items_ordered;

    @FXML
    private Label lbl_items_missed;

    @FXML
    private Label lbl_near_expiry;

    @FXML
    private Label lbl_bonus_items;

    @FXML
    private Label lbl_error_product;


    @FXML
    private TableView<ViewSaleDetailInfo> table_saledetaillog;

    @FXML
    private TableColumn<ViewSaleDetailInfo, String> sr_no;

//    @FXML
//    private TableColumn<ViewSaleDetailInfo, String> product_id;

    @FXML
    private TableColumn<ViewSaleDetailInfo, String> product_name;

    @FXML
    private TableColumn<ViewSaleDetailInfo, String> batch_no;

    @FXML
    private TableColumn<ViewSaleDetailInfo, String> batch_expiry;

    @FXML
    private TableColumn<ViewSaleDetailInfo, String> ordered_quantity;

    @FXML
    private TableColumn<ViewSaleDetailInfo, String> bonus_quantity;

    @FXML
    private TableColumn<ViewSaleDetailInfo, String> product_price;

    @FXML
    private TableColumn<ViewSaleDetailInfo, String> discount_given;

    @FXML
    private TableColumn<ViewSaleDetailInfo, String> operations;

    @FXML
    private JFXButton btn_print_invoice;

    @FXML
    private JFXButton cancelBtn;

    @FXML
    private JFXButton saveBtn;

    @FXML
    private StackPane stackPane;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    @FXML
    private BorderPane brdrpane_navfinances;

    @FXML
    private BorderPane menu_bar;

    private static ObservableList<ViewSaleDetailInfo> saleDetails;
    private static ViewSaleDetailInfo objViewSaleDetailInfo;
    private SaleInvoicePrintInfo objSaleInvoicePrintInfo;
    private UserInfo objUserInfo;
    private ProductInfo objProductInfo;
    private DealerInfo objDealerInfo;
    private Bonus objBonus;
    private Discount objDiscount;
    ArrayList<String> saleBasicData;
    ArrayList<String> userNames;
    static ArrayList<ArrayList<String>> saleData;
    ArrayList<ArrayList<String>> usersData;
    ArrayList<String> invoicePrintData;
    public static ArrayList<ArrayList<String>> completeDealersData;
    public static ArrayList<ArrayList<String>> completeProductData;
    public static ArrayList<ArrayList<String>> productsCompaniesDetail;
    public static ArrayList<ArrayList<String>> completeBatchData;
    public static ArrayList<ArrayList<String>> bonusDetail;
    public static ArrayList<ArrayList<String>> discountDetail;
    static ArrayList<String> selectedBatchIds;
    static ArrayList<String> selectedBatchNos;
    static ArrayList<String> selectedBatchQty;
    private static String currentInvoiceId;
    private static String batchPolicy;
    private static String selectedBatch;
//    private static String selectedBatchQty;
    private static String selectedProductPrice;
    ArrayList<String> productNames;
    ArrayList<String> dealerNames;
    ArrayList<String> productQty;
    static SimpleDateFormat fmt = new SimpleDateFormat("dd/MMM/yyyy");

    static float totalProducts = 0;
    static float orderedItems = 0;
    static float missedItems = 0;
    static float nearExpiry = 0;
    static float bonusItems = 0;

    MysqlCon objMysqlCon1;
    MysqlCon objMysqlCon2;
    MysqlCon objMysqlCon3;
    static Statement objStmt1;
    Statement objStmt2;
    Statement objStmt3;
    Connection objCon;
    public ArrayList<InvoiceInfo> invoiceData;

    public static int srNo;
    public static Button btnView;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnSale.setStyle("-fx-background-color: #47ab1e");
        objMysqlCon1 = new MysqlCon();
        objMysqlCon2 = new MysqlCon();
        objMysqlCon3 = new MysqlCon();
        objStmt1 = objMysqlCon1.stmt;
        objStmt2 = objMysqlCon2.stmt;
        objStmt3 = objMysqlCon3.stmt;
        objCon = objMysqlCon1.con;
        objUserInfo = new UserInfo();
        objProductInfo = new ProductInfo();
        objDealerInfo = new DealerInfo();
        saleData = new ArrayList<>();

        if(!ViewSalesInfo.orderId.equals(""))
        {
            currentInvoiceId = ViewSalesInfo.orderId;
            ViewSalesInfo.orderId = "";
        }

        ViewSaleDetailInfo.productIdArr = new ArrayList<>();
        ViewSaleDetailInfo.productNameArr = new ArrayList<>();
        ViewSaleDetailInfo.batchIdArr = new ArrayList<>();
        ViewSaleDetailInfo.batchNoArr = new ArrayList<>();
        ViewSaleDetailInfo.batchExpiryArr = new ArrayList<>();
        ViewSaleDetailInfo.orderedQuantityArr = new ArrayList<>();
        ViewSaleDetailInfo.submissionQuantityArr = new ArrayList<>();
        ViewSaleDetailInfo.bonusQuantityArr = new ArrayList<>();
        ViewSaleDetailInfo.productPriceArr = new ArrayList<>();
        ViewSaleDetailInfo.finalPriceArr = new ArrayList<>();
        ViewSaleDetailInfo.discountGivenArr = new ArrayList<>();

        objViewSaleDetailInfo = new ViewSaleDetailInfo(currentInvoiceId);
        objViewSaleDetailInfo.getSaleDetailInfo(objStmt1, objCon);
        objSaleInvoicePrintInfo = new SaleInvoicePrintInfo();
        invoicePrintData = objSaleInvoicePrintInfo.getSaleInvoicePrintInfo(objStmt1, objCon);
        objBonus = new Bonus();
        objDiscount = new Discount();
        bonusDetail = objBonus.getCurrentBonusInfo(objStmt1, objCon);
        discountDetail = objDiscount.getCurrentDiscountInfo(objStmt1, objCon);

        invoiceData = new ArrayList<>();
        usersData = objUserInfo.getUserWithIds(objStmt1, objCon);
        userNames = GlobalVariables.getArrayColumn(usersData, 2);
        txt_booking_user.getItems().addAll(userNames);
        txt_delviered_user.getItems().addAll(userNames);
        batchPolicy = objViewSaleDetailInfo.getBatchPolicy(objStmt1, objCon);

        txt_booking_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        txt_delivered_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        ViewSaleDetailInfo.txtProductName = txt_product_name;
        ViewSaleDetailInfo.txtBatchNo = txt_product_batch;
        ViewSaleDetailInfo.txtQuantity = txt_product_quantity;
//        ViewSaleDetailInfo.txtUnit = txt_product_unit;
        ViewSaleDetailInfo.txtBonusPack = txt_product_bonus;
        ViewSaleDetailInfo.btnAdd = btn_add;
        ViewSaleDetailInfo.btnCancel = btn_cancel_edit;
        ViewSaleDetailInfo.btnUpdate = btn_update_edit;

        saleBasicData = objViewSaleDetailInfo.getSaleBasicInfo(objStmt1, objCon);
        txt_invoice_no.setText(saleBasicData.get(0));
        txt_dealer_name.setText(saleBasicData.get(1));
        txt_dealer_contact.setText(saleBasicData.get(2));
        txt_invoice_price.setText(saleBasicData.get(3));
        txt_discount_given.setText(saleBasicData.get(4));
        txt_invoice_status.setValue(saleBasicData.get(5));
        txt_booking_user.setValue(saleBasicData.get(6));

        completeDealersData = objDealerInfo.getDealersDetail(objStmt1, objCon);
        productsCompaniesDetail = objProductInfo.getCompanyProductsData(objStmt1, objCon);
        completeProductData = objProductInfo.getProductsDetail(objStmt1, objCon);
        productNames = GlobalVariables.getArrayColumn(completeProductData, 1);
        productQty = GlobalVariables.getArrayColumn(completeProductData, 2);
        TextFields.bindAutoCompletion(txt_product_name, productNames);
        completeBatchData = objProductInfo.getBatchesDetail(objStmt1, objCon);

        if(saleBasicData.get(7) != null)
        {
            txt_booking_date.setValue(GlobalVariables.LOCAL_DATE(saleBasicData.get(7)));
        }

        SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
        Date date = null;

        if(saleBasicData.get(8) != null)
        {
            try {
                date = parseFormat.parse(saleBasicData.get(8));
            } catch (ParseException e) {
                e.printStackTrace();
            }
//            System.out.println(parseFormat.format(date) + " = " + displayFormat.format(date));
//            System.out.println(LocalTime.parse(displayFormat.format(date)));
            txt_booking_time.setValue(LocalTime.parse(displayFormat.format(date)));
        }


        txt_delviered_user.setValue(saleBasicData.get(9));
        if(saleBasicData.get(10) != null)
        {
            txt_delivered_date.setValue(GlobalVariables.LOCAL_DATE(saleBasicData.get(10)));
        }


        if(saleBasicData.get(11) != null)
        {
            try {
                date = parseFormat.parse(saleBasicData.get(11));
            } catch (ParseException e) {
                e.printStackTrace();
            }
//            System.out.println(parseFormat.format(date) + " = " + displayFormat.format(date));
//            System.out.println(LocalTime.parse(displayFormat.format(date)));
            txt_delivered_time.setValue(LocalTime.parse(displayFormat.format(date)));
        }

        txt_product_name.focusedProperty().addListener(new ChangeListener<Boolean>()
        {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue)
            {
                if (newPropertyValue)
                {
//                    System.out.println("Textfield on focus");
                }
                else
                {
                    String prodId = getProductId(completeProductData, txt_product_name.getText());
                    if(prodId == null)
                    {
                        btn_add.setDisable(true);
                        lbl_error_product.setVisible(true);
                    }
                    else if(productAlreadyAdded(ViewSaleDetailInfo.productIdArr, prodId))
                    {
                        btn_add.setDisable(true);
                        lbl_error_product.setVisible(true);
                    }
                    else
                    {
                        txt_product_batch.getItems().clear();
                        txt_product_batch.getItems().addAll(getProductsBatch(completeBatchData, prodId));
                        txt_product_batch.setValue(selectedBatch);
                        lbl_error_product.setVisible(false);
                        btn_add.setDisable(false);
                    }

                }
            }
        });
        dealerNames = GlobalVariables.getArrayColumn(completeDealersData, 1);
        TextFields.bindAutoCompletion(txt_dealer_name, dealerNames);

        totalProducts = 0;
        orderedItems = 0;
        missedItems = 0;
        nearExpiry = 0;
        bonusItems = 0;

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
//        product_id.setCellValueFactory(new PropertyValueFactory<>("productId"));
        product_name.setCellValueFactory(new PropertyValueFactory<>("productName"));
        batch_no.setCellValueFactory(new PropertyValueFactory<>("batchNo"));
        batch_expiry.setCellValueFactory(new PropertyValueFactory<>("batchExpiry"));
        ordered_quantity.setCellValueFactory(new PropertyValueFactory<>("orderedQuantity"));
        bonus_quantity.setCellValueFactory(new PropertyValueFactory<>("bonusQuantity"));
        product_price.setCellValueFactory(new PropertyValueFactory<>("productPrice"));
        discount_given.setCellValueFactory(new PropertyValueFactory<>("discountGiven"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_saledetaillog.setItems(parseUserList());
        ViewSaleDetailInfo.table_saledetaillog = table_saledetaillog;
        lbl_products.setText("Products\n"+String.format("%,.0f", totalProducts));
        lbl_items_ordered.setText("Items Ordered\n"+String.format("%,.0f", orderedItems));
        lbl_items_missed.setText("Items Missed\n"+String.format("%,.0f", missedItems));
        lbl_near_expiry.setText("Near Expiry\n"+String.format("%,.0f", nearExpiry));
        lbl_bonus_items.setText("Bonus Items\n"+String.format("%,.0f", bonusItems));
    }

    public static ObservableList<ViewSaleDetailInfo> parseUserList() {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        saleDetails = FXCollections.observableArrayList();

        saleData = objViewSaleDetailInfo.loadSaleTable();

        for (int i = 0; i < saleData.size(); i++)
        {
            totalProducts += (saleData.get(i).get(0) != null && !saleData.get(i).get(0).equals("-")) ? 1 : 0;
//            orderedItems += (saleData.get(i).get(5) != null && !saleData.get(i).get(5).equals("-")) ? Float.parseFloat(saleData.get(i).get(5)) : 0;
//            missedItems += (saleData.get(i).get(7) != null && !saleData.get(i).get(7).equals("-")) ? Float.parseFloat(saleData.get(i).get(7)) : 0;
//            nearExpiry += (saleData.get(i).get(6) != null && !saleData.get(i).get(6).equals("-")) ? Float.parseFloat(saleData.get(i).get(6)) : 0;
            bonusItems += (saleData.get(i).get(4) != null && !saleData.get(i).get(4).equals("")) ? Float.parseFloat(saleData.get(i).get(4)) : 0;
            saleDetails.add(new ViewSaleDetailInfo(String.valueOf(i+1), saleData.get(i).get(0), saleData.get(i).get(1), saleData.get(i).get(2), saleData.get(i).get(3), saleData.get(i).get(4), saleData.get(i).get(5), saleData.get(i).get(6)));
        }
        return saleDetails;
    }

    @FXML
    void batchChange(ActionEvent event) {

    }

    @FXML
    void btnAdd(ActionEvent event) {
        String dealerName = txt_dealer_name.getText();
        String dealerId = GlobalVariables.getIdFrom2D(completeDealersData, dealerName, 1);
        String productName = txt_product_name.getText();
        String productId = GlobalVariables.getIdFrom2D(completeProductData, productName, 1);
        String companyId = GlobalVariables.getDataFrom2D(completeProductData, productName, 1, 4);
        String batchNo = txt_product_batch.getValue();
        String quantity = txt_product_quantity.getText();
        String bonus = txt_product_bonus.getText();
        selectedProductPrice = String.valueOf(Float.parseFloat(selectedProductPrice) * Float.parseFloat(quantity));
        bonus = getBonus(dealerId, companyId, productId, quantity);
        String discount = getDiscount(dealerId, companyId, productId, selectedProductPrice);
        String finalProductPrice = String.valueOf(Float.parseFloat(selectedProductPrice) - Float.parseFloat(discount));
        String qtyOfBatch = getBatchQty(selectedBatchNos, batchNo);
        String qtyOfProduct = getProductQty(completeProductData, productName);
        if(qtyOfBatch == null)
        {
            qtyOfBatch = "0";
        }
        if(qtyOfProduct == null)
        {
            qtyOfProduct = "0";
        }
        ViewSaleDetailInfo.productIdArr.add(getProductId(completeProductData, productName));
        ViewSaleDetailInfo.productNameArr.add(productName);
        ViewSaleDetailInfo.orderedQuantityArr.add(quantity);
        if(qtyOfBatch != null && !qtyOfBatch.equals("0"))
        {
            if(Integer.parseInt(quantity)<=Integer.parseInt(qtyOfBatch))
            {
                ViewSaleDetailInfo.submissionQuantityArr.add(quantity);
                qtyOfBatch = String.valueOf(Integer.parseInt(qtyOfBatch) - Integer.parseInt(quantity));
                setBatchQty(selectedBatchNos, batchNo, qtyOfBatch);
                qtyOfProduct = String.valueOf(Integer.parseInt(qtyOfProduct) - Integer.parseInt(quantity));
                setProductQty(completeProductData, productName, qtyOfProduct);
            }
            else
            {
                ViewSaleDetailInfo.submissionQuantityArr.add(qtyOfBatch);
                setBatchQty(selectedBatchNos, batchNo, "0");
                qtyOfProduct = String.valueOf(Integer.parseInt(qtyOfProduct) - Integer.parseInt(qtyOfBatch));
                setProductQty(completeProductData, productName, qtyOfProduct);
            }
            ViewSaleDetailInfo.batchIdArr.add(getBatchId(completeBatchData, batchNo));
            ViewSaleDetailInfo.batchNoArr.add(batchNo);
            ViewSaleDetailInfo.batchExpiryArr.add(getBatchExpiry(completeBatchData, batchNo));
        }
        else
        {
            if(Integer.parseInt(quantity)<=Integer.parseInt(qtyOfProduct))
            {
                ViewSaleDetailInfo.submissionQuantityArr.add(quantity);
                qtyOfProduct = String.valueOf(Integer.parseInt(qtyOfProduct) - Integer.parseInt(quantity));
                setProductQty(completeProductData, productName, qtyOfProduct);
            }
            else
            {
                ViewSaleDetailInfo.submissionQuantityArr.add(qtyOfProduct);
                setProductQty(completeProductData, productName, "0");
            }
            ViewSaleDetailInfo.batchIdArr.add("0");
            ViewSaleDetailInfo.batchNoArr.add("N/A");
            ViewSaleDetailInfo.batchExpiryArr.add("N/A");
        }

        ViewSaleDetailInfo.bonusQuantityArr.add(bonus);
        ViewSaleDetailInfo.productPriceArr.add(selectedProductPrice);
        ViewSaleDetailInfo.finalPriceArr.add(finalProductPrice);
        ViewSaleDetailInfo.discountGivenArr.add(discount);
        table_saledetaillog.setItems(parseUserList());
        ViewSaleDetailInfo.table_saledetaillog = table_saledetaillog;

        txt_product_name.clear();
        txt_product_batch.getItems().clear();
        txt_product_batch.setValue(null);
        txt_product_quantity.clear();
        txt_product_bonus.clear();
    }

    @FXML
    void btnCancelEdit(ActionEvent event) {
        txt_product_name.clear();
        txt_product_batch.getItems().clear();
        txt_product_batch.setValue(null);
        txt_product_quantity.clear();
        txt_product_bonus.clear();
        btn_add.setVisible(true);
        btn_cancel_edit.setVisible(false);
        btn_update_edit.setVisible(false);
    }

    @FXML
    void btnUpdateEdit(ActionEvent event) {
        String productName = txt_product_name.getText();
        String batchNo = txt_product_batch.getValue();
        String quantity = txt_product_quantity.getText();
        String bonus = txt_product_bonus.getText();
        bonus = "0";
        String qtyOfBatch = getBatchQty(selectedBatchNos, batchNo);
        String qtyOfProduct = getProductQty(completeProductData, productName);
        if(qtyOfBatch == null)
        {
            qtyOfBatch = "0";
        }
        if(qtyOfProduct == null)
        {
            qtyOfProduct = "0";
        }
        if(ViewSaleDetailInfo.preBatch != null && !ViewSaleDetailInfo.preBatch.equals("N/A"))
        {
            qtyOfBatch = String.valueOf(Integer.parseInt(qtyOfBatch) + Integer.parseInt(ViewSaleDetailInfo.preSubmissionQty));
            qtyOfProduct = String.valueOf(Integer.parseInt(qtyOfProduct) + Integer.parseInt(ViewSaleDetailInfo.preSubmissionQty));
        }
        else
        {
            qtyOfProduct = String.valueOf(Integer.parseInt(qtyOfProduct) + Integer.parseInt(ViewSaleDetailInfo.preSubmissionQty));
        }
        ViewSaleDetailInfo.productIdArr.set(srNo, getProductId(completeProductData, productName));
        ViewSaleDetailInfo.productNameArr.set(srNo, productName);
        ViewSaleDetailInfo.orderedQuantityArr.set(srNo, quantity);
        if(qtyOfBatch != null && !qtyOfBatch.equals("0"))
        {
            if(Integer.parseInt(quantity)<=Integer.parseInt(qtyOfBatch))
            {
                ViewSaleDetailInfo.submissionQuantityArr.set(srNo, quantity);
                qtyOfBatch = String.valueOf(Integer.parseInt(qtyOfBatch) - Integer.parseInt(quantity));
                setBatchQty(selectedBatchNos, batchNo, qtyOfBatch);
                qtyOfProduct = String.valueOf(Integer.parseInt(qtyOfProduct) - Integer.parseInt(quantity));
                setProductQty(completeProductData, productName, qtyOfProduct);
            }
            else
            {
                ViewSaleDetailInfo.submissionQuantityArr.set(srNo, qtyOfBatch);
                setBatchQty(selectedBatchNos, batchNo, "0");
                qtyOfProduct = String.valueOf(Integer.parseInt(qtyOfProduct) - Integer.parseInt(qtyOfBatch));
                setProductQty(completeProductData, productName, qtyOfProduct);
            }
            ViewSaleDetailInfo.batchIdArr.set(srNo, getBatchId(completeBatchData, batchNo));
            ViewSaleDetailInfo.batchNoArr.set(srNo, batchNo);
            ViewSaleDetailInfo.batchExpiryArr.set(srNo, getBatchExpiry(completeBatchData, batchNo));
        }
        else
        {
            if(Integer.parseInt(quantity)<=Integer.parseInt(qtyOfProduct))
            {
                ViewSaleDetailInfo.submissionQuantityArr.set(srNo, quantity);
                qtyOfProduct = String.valueOf(Integer.parseInt(qtyOfProduct) - Integer.parseInt(quantity));
                setProductQty(completeProductData, productName, qtyOfProduct);
            }
            else
            {
                ViewSaleDetailInfo.submissionQuantityArr.set(srNo, qtyOfProduct);
                setProductQty(completeProductData, productName, "0");
            }
            ViewSaleDetailInfo.batchIdArr.set(srNo, "0");
            ViewSaleDetailInfo.batchNoArr.set(srNo, "N/A");
            ViewSaleDetailInfo.batchExpiryArr.set(srNo, "N/A");
        }

        ViewSaleDetailInfo.bonusQuantityArr.set(srNo, bonus);
        ViewSaleDetailInfo.productPriceArr.set(srNo, selectedProductPrice);
        ViewSaleDetailInfo.finalPriceArr.set(srNo, selectedProductPrice);
        ViewSaleDetailInfo.discountGivenArr.set(srNo, "0");
        table_saledetaillog.setItems(parseUserList());
        ViewSaleDetailInfo.table_saledetaillog = table_saledetaillog;

        txt_product_name.clear();
        txt_product_batch.getItems().clear();
        txt_product_batch.setValue(null);
        txt_product_quantity.clear();
        txt_product_bonus.clear();
        btn_add.setVisible(true);
        btn_cancel_edit.setVisible(false);
        btn_update_edit.setVisible(false);
    }

    @FXML
    void printInvoice(ActionEvent event) throws JRException {
        invoiceData = new ArrayList<>();
        String orderId = txt_invoice_no.getText();
        String dealerId = txt_dealer_name.getText();
        String dealerName = txt_dealer_name.getText();
        String dealerLic = "Dealer Lic";
        String dealerAddress = "Dealer Address";
        String dealerContact = txt_dealer_contact.getText();
        String dealerNTN = "NTN";
        String dealerCnic = "CNIC";
        String dealerSalesTaxNo = "Sales Tax No";
        String invoiceDate = txt_booking_date.getValue().toString();
        String deliveredUser = txt_delviered_user.getValue();
        ArrayList<ArrayList<String>> getInvoiceDetail = objViewSaleDetailInfo.loadInvoiceItems();
        String totalQty = "1000";
        String orgPrice = "10,000";
        String salestax = "0.00";
        String totalBonus = "30";
        String totalDiscount = txt_discount_given.getText();
        String finalPrice = txt_invoice_price.getText();
        InvoicedItemsInfo objItemsInfos;
        List<InvoicedItemsInfo> itemsList = null;
        itemsList = new ArrayList<>();
        for (int i = 0; i < getInvoiceDetail.size(); i++)
        {
//            String[] itemsNo = new String[getInvoiceDetail.get(i).get(0).length];
//            itemsNo = getInvoiceDetail.get(i).get(0);
//            String[] itemsName = new String[getInvoiceDetail.get(i).get(1).length];
//            itemsName = getInvoiceDetail.get(i).get(1);
//            String[] packing = new String[getInvoiceDetail.get(i).get(2).length];
//            packing = getInvoiceDetail.get(i).get(2);
//            String[] batchNo = new String[getInvoiceDetail.get(i).get(3).length];
//            batchNo = getInvoiceDetail.get(i).get(3);
//            String[] itemRate = new String[getInvoiceDetail.get(i).get(4).length];
//            itemRate = getInvoiceDetail.get(i).get(4);
//            String[] quantity = new String[getInvoiceDetail.get(i).get(5).length];
//            quantity = getInvoiceDetail.get(i).get(5);
//            String[] bonusQty = new String[getInvoiceDetail.get(i).get(6).length];
//            bonusQty = getInvoiceDetail.get(i).get(6);
//            String[] grossAmount = new String[getInvoiceDetail.get(i).get(7).length];
//            grossAmount = getInvoiceDetail.get(i).get(7);
//            String[] salesTax = new String[getInvoiceDetail.get(i).get(8).length];
//            salesTax = getInvoiceDetail.get(i).get(8);
//            String[] discount = new String[getInvoiceDetail.get(i).get(9).length];
//            discount = getInvoiceDetail.get(i).get(9);
//            String[] totalAmount = new String[getInvoiceDetail.get(i).get(10).length];
//            totalAmount = getInvoiceDetail.get(i).get(10);

//            String[] quantity = new String[getInvoiceDetail.get(i).get(7).length];
//            quantity = getInvoiceDetail.get(i).get(7);
//            String[] unit = new String[getInvoiceDetail.get(i).get(8).length];
//            unit = getInvoiceDetail.get(i).get(8);
//            for(int j=0; j<itemsNo.length; j++)
//            {
                objItemsInfos = new InvoicedItemsInfo();
                objItemsInfos.setItemNo(getInvoiceDetail.get(i).get(0));
                objItemsInfos.setItemName(getInvoiceDetail.get(i).get(1));
                objItemsInfos.setPacking(getInvoiceDetail.get(i).get(2));
                objItemsInfos.setBatchNo(getInvoiceDetail.get(i).get(3));
                objItemsInfos.setItemRate(getInvoiceDetail.get(i).get(4));
                objItemsInfos.setQuantity(getInvoiceDetail.get(i).get(5));
                objItemsInfos.setBonusQty(getInvoiceDetail.get(i).get(6));
                objItemsInfos.setGrossAmount(getInvoiceDetail.get(i).get(7));
                objItemsInfos.setSalesTax(getInvoiceDetail.get(i).get(8));
                objItemsInfos.setDiscount(getInvoiceDetail.get(i).get(9));
                objItemsInfos.setTotalAmount(getInvoiceDetail.get(i).get(10));
                objItemsInfos.setTotalQty(totalQty);
                objItemsInfos.setTotalBonus(totalBonus);
                objItemsInfos.setOrgPrice(orgPrice);
                objItemsInfos.setTotalSalesTax(salestax);
                objItemsInfos.setTotalDiscount(totalDiscount);
                objItemsInfos.setFinalPrice(finalPrice);
                itemsList.add(objItemsInfos);
//            }
        }
        invoiceData.add(new InvoiceInfo(orderId, dealerId, dealerName, dealerLic, dealerAddress, dealerContact, dealerNTN, dealerCnic, dealerSalesTaxNo, invoiceDate, deliveredUser, itemsList, totalQty, orgPrice, salestax, totalBonus, totalDiscount, finalPrice, invoicePrintData.get(11)));
        InputStream objIO = ViewPendingOrders.class.getResourceAsStream("/reports/Invoice.jrxml");
        JasperReport objReport = JasperCompileManager.compileReport(objIO);
        JasperPrint objPrint = JasperFillManager.fillReport(objReport, null, new JRBeanCollectionDataSource(invoiceData));
        JasperViewer.viewReport(objPrint, false);
    }

    @FXML
    void cancelClicked(ActionEvent event) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/view_sales.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void follow(MouseEvent event) {

    }

    @FXML
    void productChange(KeyEvent event) {
        txt_product_batch.getItems().clear();
        txt_product_batch.setValue(null);
    }

    @FXML
    void saveClicked(ActionEvent event) {
        objViewSaleDetailInfo.updateInvoice(objStmt1, objCon);

        ViewSalesInfo.orderId = currentInvoiceId;
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/view_sale_detail.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void showBooked(MouseEvent event) {

    }

    @FXML
    void showBookingPrice(MouseEvent event) {

    }

    @FXML
    void showDelivered(MouseEvent event) {

    }

    @FXML
    void showDiscountGiven(MouseEvent event) {

    }

    @FXML
    void showReturned(MouseEvent event) {

    }

    @FXML
    void unitChange(ActionEvent event) {

    }

    public static String getProductId(ArrayList<ArrayList<String>> productsData, String productName)
    {
        for(ArrayList<String> row: productsData) {
            if(row.get(1).equals(productName))
            {
                selectedProductPrice = row.get(3);
//                addedRetailPrice = row.get(2);
                return row.get(0);
            }
        }
        return null;
    }

    public static String getProductQty(ArrayList<ArrayList<String>> productsData, String productName)
    {
        for(ArrayList<String> row: productsData) {
            if(row.get(1).equals(productName))
            {
                return row.get(2);
            }
        }
        return null;
    }

    public static void setProductQty(ArrayList<ArrayList<String>> productsData, String productName, String quantity)
    {
        int i=0;
        for(ArrayList<String> row: productsData) {
            if(row.get(1).equals(productName))
            {
                row.set(2, quantity);
                productsData.set(i, row);
            }
            i++;
        }
    }

    public String getBatchQty(ArrayList<String> selectedBatch, String batchNo)
    {
        for(int i=0; i<selectedBatch.size(); i++) {
            if(selectedBatch.get(i).equals(batchNo))
            {
                return selectedBatchQty.get(i);
            }
        }
        return null;
    }

    public void setBatchQty(ArrayList<String> selectedBatch, String batchNo, String quantity)
    {
        for(int i=0; i<selectedBatch.size(); i++) {
            if(selectedBatch.get(i).equals(batchNo))
            {
                selectedBatchQty.set(i, quantity);
            }
        }
    }

    public String getBatchId(ArrayList<ArrayList<String>> batchData, String batchNo)
    {
        for(ArrayList<String> row: batchData) {
            if(row.get(1).equals(batchNo))
            {
//                addedRetailPrice = row.get(2);
                return row.get(0);
            }
        }
        return null;
    }

    public String getBatchExpiry(ArrayList<ArrayList<String>> batchData, String batchNo)
    {
        for(ArrayList<String> row: batchData) {
            if(row.get(1).equals(batchNo))
            {
//                addedRetailPrice = row.get(2);
                return row.get(4);
            }
        }
        return null;
    }

    public static ArrayList<String> getProductsBatch(ArrayList<ArrayList<String>> batchData, String productId)
    {
        ArrayList<String> columnList = new ArrayList<>();
        ArrayList<String> stExpiryDate = new ArrayList<>();
        ArrayList<Date> expiryDate = new ArrayList<>();
        ArrayList<String> stEntryDate = new ArrayList<>();
        ArrayList<Date> entryDate = new ArrayList<>();
        selectedBatchIds = new ArrayList<>();
        selectedBatchNos = new ArrayList<>();
        selectedBatchQty = new ArrayList<>();
        Date entrySelectedDate;
        Date expirySelectedDate;
        int selectedIndex = 0;
        boolean expirySelection = false;
        // Batch Id          0
        // Batch No          1
        // Product Id        2
        // Batch Quantity    3
        // Batch Expiry      4
        // Entry Date        5
        if(batchData!=null)
        {
            for(ArrayList<String> row: batchData) {
                if(row.get(2).equals(productId))
                {
                    selectedBatchIds.add(row.get(0));
                    selectedBatchNos.add(row.get(1));
                    selectedBatchQty.add(row.get(3));
                    columnList.add(row.get(1));
                    stExpiryDate.add(row.get(4));
                    stEntryDate.add(row.get(5));
                    try {
                        expiryDate.add(fmt.parse(row.get(4)));
                        entryDate.add(fmt.parse(row.get(5)));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    if(batchPolicy.equals("Entry LIFO"))
                    {
                        // Entry select max date
                         entrySelectedDate = Collections.max(entryDate);
                         expirySelection = false;
                         selectedIndex = entryDate.indexOf(entrySelectedDate);
                    }
                    else if(batchPolicy.equals("Entry FIFO"))
                    {
                        // Entry select min date
                        entrySelectedDate = Collections.min(entryDate);
                        expirySelection = false;
                        selectedIndex = entryDate.indexOf(entrySelectedDate);
                    }
                    else if(batchPolicy.equals("Expiry LIFO"))
                    {
                        // Expiry select max date
                        expirySelectedDate = Collections.max(expiryDate);
                        expirySelection = true;
                        selectedIndex = entryDate.indexOf(expirySelectedDate);
                    }
                    else if(batchPolicy.equals("Expiry FIFO"))
                    {
                        // Expiry select min date
                        expirySelectedDate = Collections.min(expiryDate);
                        expirySelection = true;
                        selectedIndex = entryDate.indexOf(expirySelectedDate);
                    }
                    selectedBatch = columnList.get(selectedIndex);
                }
            }
        }
        return columnList;
    }

    private String getBonus(String dealerId, String companyId, String productId, String quantity)
    {
        String bonus = "0";
        if(quantity.equals(""))
        {
            quantity = "0";
        }

        for(int i=0; i<bonusDetail.size(); i++)
        {
            if(dealerId.equals(bonusDetail.get(i).get(0)) && (bonusDetail.get(i).get(1).equals("0") || bonusDetail.get(i).get(1).equals("null")) && (bonusDetail.get(i).get(2).equals("0") || bonusDetail.get(i).get(2).equals("null")))
            {
                if(bonusDetail.get(i).get(3).equals("0"))
                {
                    return bonusDetail.get(i).get(4);
                }
                else
                {
                    int num = Integer.valueOf(quantity)/Integer.valueOf(bonusDetail.get(i).get(3));
                    num = num*Integer.valueOf(bonusDetail.get(i).get(4));
                    return String.valueOf(num);
                }
            }
            else if(dealerId.equals(bonusDetail.get(i).get(0)) && (bonusDetail.get(i).get(1).equals("0") || bonusDetail.get(i).get(1).equals("null")) && productId.equals(bonusDetail.get(i).get(2)))
            {
                if(bonusDetail.get(i).get(3).equals("0"))
                {
                    return bonusDetail.get(i).get(4);
                }
                else
                {
                    int num = Integer.valueOf(quantity)/Integer.valueOf(bonusDetail.get(i).get(3));
                    num = num*Integer.valueOf(bonusDetail.get(i).get(4));
                    return String.valueOf(num);
                }
            }
            else if((bonusDetail.get(i).get(0).equals("0") || bonusDetail.get(i).get(0).equals("null")) && (bonusDetail.get(i).get(1).equals("0") || bonusDetail.get(i).get(1).equals("null")) && productId.equals(bonusDetail.get(i).get(2)))
            {
                if(bonusDetail.get(i).get(3).equals("0"))
                {
                    return bonusDetail.get(i).get(4);
                }
                else
                {
                    int num = Integer.valueOf(quantity)/Integer.valueOf(bonusDetail.get(i).get(3));
                    num = num*Integer.valueOf(bonusDetail.get(i).get(4));
                    return String.valueOf(num);
                }
            }
            else if((bonusDetail.get(i).get(0).equals("0") || bonusDetail.get(i).get(0).equals("null")) && !(bonusDetail.get(i).get(1).equals("0") || bonusDetail.get(i).get(1).equals("null")) && (bonusDetail.get(i).get(2).equals("0") || bonusDetail.get(i).get(2).equals("null")))
            {
                for(int j=0; j<productsCompaniesDetail.size(); j++)
                {
                    if(productsCompaniesDetail.get(j).get(1).equals(companyId) && companyId.equals(bonusDetail.get(i).get(1)))
                    {
                        if(bonusDetail.get(i).get(3).equals("0"))
                        {
                            return bonusDetail.get(i).get(4);
                        }
                        else
                        {
                            int num = Integer.valueOf(quantity)/Integer.valueOf(bonusDetail.get(i).get(3));
                            num = num*Integer.valueOf(bonusDetail.get(i).get(4));
                            return String.valueOf(num);
                        }
                    }
                }
            }
            else if(dealerId.equals(bonusDetail.get(i).get(0)) && !(bonusDetail.get(i).get(1).equals("0") || bonusDetail.get(i).get(1).equals("null")) && (bonusDetail.get(i).get(2).equals("0") || bonusDetail.get(i).get(2).equals("null")))
            {
                for(int j=0; j<productsCompaniesDetail.size(); j++)
                {
                    if(productsCompaniesDetail.get(j).get(1).equals(companyId) && companyId.equals(bonusDetail.get(i).get(1)))
                    {
                        if(bonusDetail.get(i).get(3).equals("0"))
                        {
                            return bonusDetail.get(i).get(4);
                        }
                        else
                        {
                            int num = Integer.valueOf(quantity)/Integer.valueOf(bonusDetail.get(i).get(3));
                            num = num*Integer.valueOf(bonusDetail.get(i).get(4));
                            return String.valueOf(num);
                        }
                    }
                }
            }
            else if((bonusDetail.get(i).get(0).equals("0") || bonusDetail.get(i).get(0).equals("null")) && !(bonusDetail.get(i).get(1).equals("0") || bonusDetail.get(i).get(1).equals("null")) && productId.equals(bonusDetail.get(i).get(2)))
            {
                for(int j=0; j<productsCompaniesDetail.size(); j++)
                {
                    if(productsCompaniesDetail.get(j).get(1).equals(companyId) && companyId.equals(bonusDetail.get(i).get(1)))
                    {
                        if(bonusDetail.get(i).get(3).equals("0"))
                        {
                            return bonusDetail.get(i).get(4);
                        }
                        else
                        {
                            int num = Integer.valueOf(quantity)/Integer.valueOf(bonusDetail.get(i).get(3));
                            num = num*Integer.valueOf(bonusDetail.get(i).get(4));
                            return String.valueOf(num);
                        }
                    }
                }
            }
            else if((bonusDetail.get(i).get(0).equals("0") || bonusDetail.get(i).get(0).equals("null")) && (bonusDetail.get(i).get(1).equals("0") || bonusDetail.get(i).get(1).equals("null")) && (bonusDetail.get(i).get(2).equals("0") || bonusDetail.get(i).get(2).equals("null")))
            {
                if(bonusDetail.get(i).get(3).equals("0"))
                {
                    return bonusDetail.get(i).get(4);
                }
                else
                {
                    int num = Integer.valueOf(quantity)/Integer.valueOf(bonusDetail.get(i).get(3));
                    num = num*Integer.valueOf(bonusDetail.get(i).get(4));
                    return String.valueOf(num);
                }
            }
        }
        return bonus;
    }

    private String getDiscount(String dealerId, String companyId, String productId, String saleAmount)
    {
        String discount = "0";
        if(saleAmount.equals(""))
        {
            saleAmount = "0";
        }

        for(int i=0; i<discountDetail.size(); i++)
        {
            if(dealerId.equals(discountDetail.get(i).get(0)) && (discountDetail.get(i).get(1).equals("0") || discountDetail.get(i).get(1).equals("null")) && (discountDetail.get(i).get(2).equals("0") || discountDetail.get(i).get(2).equals("null")))
            {
                if(Float.parseFloat(discountDetail.get(i).get(3)) == 0)
                {
                    float num = Float.parseFloat(saleAmount)/100;
                    num = num*Float.parseFloat(discountDetail.get(i).get(4));
                    return String.valueOf(num);
                }
                else
                {
                    float num = Float.parseFloat(saleAmount)/Float.parseFloat(discountDetail.get(i).get(3));
                    num = num*Float.parseFloat(discountDetail.get(i).get(4));
                    return String.valueOf(num);
                }
            }
            else if(dealerId.equals(discountDetail.get(i).get(0)) && (discountDetail.get(i).get(1).equals("0") || discountDetail.get(i).get(1).equals("null")) && productId.equals(discountDetail.get(i).get(2)))
            {
                if(Float.parseFloat(discountDetail.get(i).get(3)) == 0)
                {
                    float num = Float.parseFloat(saleAmount)/100;
                    num = num*Float.parseFloat(discountDetail.get(i).get(4));
                    return String.valueOf(num);
                }
                else
                {
                    float num = Float.parseFloat(saleAmount)/Float.parseFloat(discountDetail.get(i).get(3));
                    num = num*Float.parseFloat(discountDetail.get(i).get(4));
                    return String.valueOf(num);
                }
            }
            else if((discountDetail.get(i).get(0).equals("0") || discountDetail.get(i).get(0).equals("null")) && (discountDetail.get(i).get(1).equals("0") || discountDetail.get(i).get(1).equals("null")) && productId.equals(discountDetail.get(i).get(2)))
            {
                if(Float.parseFloat(discountDetail.get(i).get(3)) == 0)
                {
                    float num = Float.parseFloat(saleAmount)/100;
                    num = num*Float.parseFloat(discountDetail.get(i).get(4));
                    return String.valueOf(num);
                }
                else
                {
                    float num = Float.parseFloat(saleAmount)/Float.parseFloat(discountDetail.get(i).get(3));
                    num = num*Float.parseFloat(discountDetail.get(i).get(4));
                    return String.valueOf(num);
                }
            }
            else if((discountDetail.get(i).get(0).equals("0") || discountDetail.get(i).get(0).equals("null")) && !(discountDetail.get(i).get(1).equals("0") || discountDetail.get(i).get(1).equals("null")) && (discountDetail.get(i).get(2).equals("0") || discountDetail.get(i).get(2).equals("null")))
            {
                for(int j=0; j<productsCompaniesDetail.size(); j++)
                {
                    if(productsCompaniesDetail.get(j).get(1).equals(companyId) && companyId.equals(discountDetail.get(i).get(1)))
                    {
                        if(Float.parseFloat(discountDetail.get(i).get(3)) == 0)
                        {
                            float num = Float.parseFloat(saleAmount)/100;
                            num = num*Float.parseFloat(discountDetail.get(i).get(4));
                            return String.valueOf(num);
                        }
                        else
                        {
                            float num = Float.parseFloat(saleAmount)/Float.parseFloat(discountDetail.get(i).get(3));
                            num = num*Float.parseFloat(discountDetail.get(i).get(4));
                            return String.valueOf(num);
                        }
                    }
                }
            }
            else if(dealerId.equals(discountDetail.get(i).get(0)) && !(discountDetail.get(i).get(1).equals("0") || discountDetail.get(i).get(1).equals("null")) && (discountDetail.get(i).get(2).equals("0") || discountDetail.get(i).get(2).equals("null")))
            {
                for(int j=0; j<productsCompaniesDetail.size(); j++)
                {
                    if(productsCompaniesDetail.get(j).get(1).equals(companyId) && companyId.equals(discountDetail.get(i).get(1)))
                    {
                        if(Float.parseFloat(discountDetail.get(i).get(3)) == 0)
                        {
                            float num = Float.parseFloat(saleAmount)/100;
                            num = num*Float.parseFloat(discountDetail.get(i).get(4));
                            return String.valueOf(num);
                        }
                        else
                        {
                            float num = Float.parseFloat(saleAmount)/Float.parseFloat(discountDetail.get(i).get(3));
                            num = num*Float.parseFloat(discountDetail.get(i).get(4));
                            return String.valueOf(num);
                        }
                    }
                }
            }
            else if((discountDetail.get(i).get(0).equals("0") || discountDetail.get(i).get(0).equals("null")) && !(discountDetail.get(i).get(1).equals("0") || discountDetail.get(i).get(1).equals("null")) && productId.equals(discountDetail.get(i).get(2)))
            {
                for(int j=0; j<productsCompaniesDetail.size(); j++)
                {
                    if(productsCompaniesDetail.get(j).get(1).equals(companyId) && companyId.equals(discountDetail.get(i).get(1)))
                    {
                        if(Float.parseFloat(discountDetail.get(i).get(3)) == 0)
                        {
                            float num = Float.parseFloat(saleAmount)/100;
                            num = num*Float.parseFloat(discountDetail.get(i).get(4));
                            return String.valueOf(num);
                        }
                        else
                        {
                            float num = Float.parseFloat(saleAmount)/Float.parseFloat(discountDetail.get(i).get(3));
                            num = num*Float.parseFloat(discountDetail.get(i).get(4));
                            return String.valueOf(num);
                        }
                    }
                }
            }
            else if((discountDetail.get(i).get(0).equals("0") || discountDetail.get(i).get(0).equals("null")) && (discountDetail.get(i).get(1).equals("0") || discountDetail.get(i).get(1).equals("null")) && (discountDetail.get(i).get(2).equals("0") || discountDetail.get(i).get(2).equals("null")))
            {
                if(Float.parseFloat(discountDetail.get(i).get(3)) == 0)
                {
                    float num = Float.parseFloat(saleAmount)/100;
                    num = num*Float.parseFloat(discountDetail.get(i).get(4));
                    return String.valueOf(num);
                }
                else
                {
                    float num = Float.parseFloat(saleAmount)/Float.parseFloat(discountDetail.get(i).get(3));
                    num = num*Float.parseFloat(discountDetail.get(i).get(4));
                    return String.valueOf(num);
                }
            }
        }
        return discount;
    }

    private boolean productAlreadyAdded(ArrayList<String> addedProducts, String productId)
    {
        return addedProducts.contains(productId);
    }
}
