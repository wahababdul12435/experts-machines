package controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.util.Duration;
import model.GlobalVariables;
import model.MysqlCon;
import model.UserInfo;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class EnterUser implements Initializable {

    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private BorderPane menu_bar1;

    @FXML
    private BorderPane nav_setup;

    @FXML
    private JFXTextField txt_user_id;

    @FXML
    private JFXTextField txt_user_name;

    @FXML
    private Button cancelBtn;

    @FXML
    private Button saveBtn;

    @FXML
    private JFXTextField txt_user_contact;

    @FXML
    private JFXTextField txt_user_address;

    @FXML
    private JFXTextField txt_user_cnic;

    @FXML
    private JFXComboBox<String> txt_user_type;

    @FXML
    private JFXComboBox<String> txt_designated_city;

    @FXML
    private Button btn_add_user;

    @FXML    private Button btn_edit_cancel;
    @FXML    private Button btn_edit_update;

    @FXML
    private TableView<UserInfo> table_add_user;

    @FXML
    private TableColumn<UserInfo, String> sr_no;

    @FXML
    private TableColumn<UserInfo, String> user_id;

    @FXML
    private TableColumn<UserInfo, String> user_name;

    @FXML
    private TableColumn<UserInfo, String> user_contact;

    @FXML
    private TableColumn<UserInfo, String> user_address;

    @FXML
    private TableColumn<UserInfo, String> user_cnic;

    @FXML
    private TableColumn<UserInfo, String> user_type;

    @FXML
    private TableColumn<UserInfo, String> operations;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    public static ObservableList<UserInfo> userDetails;
    Alert alert = new Alert(Alert.AlertType.NONE);
    UserInfo objUserInfo;
    public static int srNo;
    public static Button btnView;

    public void initialize(URL url, ResourceBundle resourceBundle) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnSetup.setStyle("-fx-background-color: #47ab1e");
        objUserInfo = new UserInfo();
        btnpane.setOnMouseEntered(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(22);
                btnpane.setMinWidth(20.0);
                btn11.setMinWidth(20.0);

            }
        });
        btnpane.setOnMouseExited(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(14);
                btnpane.setMinWidth(10.0);
                btn11.setMinWidth(10.0);
            }
        });

        inner_anchor.setLeftAnchor(inner_anchor, 280d);
        drawer.setSidePane(nav_setup);
        drawer.open();

        ArrayList<String> chosenCityNames = objUserInfo.getSavedCityNames();
        txt_designated_city.getItems().addAll(chosenCityNames);

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        user_id.setCellValueFactory(new PropertyValueFactory<>("userId"));
        user_name.setCellValueFactory(new PropertyValueFactory<>("userName"));
        user_contact.setCellValueFactory(new PropertyValueFactory<>("userContact"));
        user_address.setCellValueFactory(new PropertyValueFactory<>("userAddress"));
        user_cnic.setCellValueFactory(new PropertyValueFactory<>("userCnic"));
        user_type.setCellValueFactory(new PropertyValueFactory<>("userType"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        UserInfo.table_add_user = table_add_user;
        table_add_user.setItems(parseUserList());
        UserInfo.txtUserId = txt_user_id;
        UserInfo.txtUserName = txt_user_name;
        UserInfo.txtUserContact = txt_user_contact;
        UserInfo.txtUserAddress = txt_user_address;
        UserInfo.txtUserCnic = txt_user_cnic;
        UserInfo.txtUserType = txt_user_type;
        UserInfo.txtUserCity = txt_designated_city;
        UserInfo.btnAdd = btn_add_user;
        UserInfo.btnCancel = btn_edit_cancel;
        UserInfo.btnUpdate = btn_edit_update;
    }

    public static ObservableList<UserInfo> parseUserList(){
        UserInfo objUserInfo = new UserInfo();
        userDetails = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> userData = objUserInfo.getAddedUser();
        for (int i = 0; i < userData.size(); i++)
        {
            userDetails.add(new UserInfo(String.valueOf(i+1), ((userData.get(i).get(0) == null || userData.get(i).get(0).equals("")) ? "N/A" : userData.get(i).get(0)), userData.get(i).get(1), userData.get(i).get(2), userData.get(i).get(3), userData.get(i).get(4), userData.get(i).get(5), userData.get(i).get(6)));
        }
        return userDetails;
    }

    @FXML
    void addUser(ActionEvent event) {
        String userId = txt_user_id.getText();
        String userName = txt_user_name.getText();
        String userContact = txt_user_contact.getText();
        String userAddress = txt_user_address.getText();
        String userCnic = txt_user_cnic.getText();
        String userType = txt_user_type.getValue();
        String cityId = String.valueOf(objUserInfo.getSavedCityIds().get(txt_designated_city.getSelectionModel().getSelectedIndex()));
        String cityName = txt_designated_city.getValue();
        String userStatus = "Active";

        String comp = objUserInfo.confirmNewId(userId);

        if(!comp.equals(userId) || comp.equals("")) {
            MysqlCon objMysqlCon = new MysqlCon();
            Statement objStmt = objMysqlCon.stmt;
            Connection objCon = objMysqlCon.con;
            UserInfo.userIdArr.add(userId);
            UserInfo.userNameArr.add(userName);
            UserInfo.userContactArr.add(userContact);
            UserInfo.userAddressArr.add(userAddress);
            UserInfo.userCnicArr.add(userCnic);
            UserInfo.userTypeArr.add(userType);
            UserInfo.userCityIdcArr.add(cityId);
            UserInfo.userCityNameArr.add(cityName);
            table_add_user.setItems(parseUserList());
            refreshControls();
        }
        else
        {
            alert.setAlertType(Alert.AlertType.ERROR);
            alert.setHeaderText("Error!");
            alert.setContentText("User ID Already Saved.");
            alert.show();
        }
    }

    @FXML
    void editCancel(ActionEvent event) {
        refreshControls();
        btn_add_user.setVisible(true);
        btn_edit_cancel.setVisible(false);
        btn_edit_update.setVisible(false);
    }

    @FXML
    void editUpdate(ActionEvent event) {
        String userId = txt_user_id.getText();
        String userName = txt_user_name.getText();
        String userContact = txt_user_contact.getText();
        String userAddress = txt_user_address.getText();
        String userCnic = txt_user_cnic.getText();
        String userType = txt_user_type.getValue();
        String cityId = String.valueOf(objUserInfo.getSavedCityIds().get(txt_designated_city.getSelectionModel().getSelectedIndex()));
        String cityName = txt_designated_city.getValue();
        String userStatus = "Active";

        String comp = objUserInfo.confirmNewId(userId);

        if(!comp.equals(userId) || comp.equals("")) {
            MysqlCon objMysqlCon = new MysqlCon();
            Statement objStmt = objMysqlCon.stmt;
            Connection objCon = objMysqlCon.con;
            UserInfo.userIdArr.set(srNo, userId);
            UserInfo.userNameArr.set(srNo, userName);
            UserInfo.userContactArr.set(srNo, userContact);
            UserInfo.userAddressArr.set(srNo, userAddress);
            UserInfo.userCnicArr.set(srNo, userCnic);
            UserInfo.userTypeArr.set(srNo, userType);
            UserInfo.userCityIdcArr.set(srNo, cityId);
            UserInfo.userCityNameArr.set(srNo, cityName);
            table_add_user.setItems(parseUserList());
            refreshControls();
        }
        else
        {
            alert.setAlertType(Alert.AlertType.ERROR);
            alert.setHeaderText("Error!");
            alert.setContentText("User ID Already Saved.");
            alert.show();
        }

        btn_add_user.setVisible(true);
        btn_edit_cancel.setVisible(false);
        btn_edit_update.setVisible(false);
    }

    @FXML
    void cancelClicked(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_users.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void follow(MouseEvent event) {
        if (drawer.isOpened()) {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(-250);
            transition.setByY(0);
            transition.setCycleCount(1);
            transition.play();
            drawer.close();
            FA_Icon.setGlyphName("ANGLE_DOUBLE_RIGHT");
            FA_Icon.setGlyphSize(14);
            //inner_anchor.setRightAnchor(inner_anchor,1d);
            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(-90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(1.1000);
            scale.setToY(1);

            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();

        }
        else {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(250);
            transition.setByY(0);
            transition.setCycleCount(1);
            FA_Icon.setGlyphName("ANGLE_DOUBLE_LEFT");
            FA_Icon.setGlyphSize(14);
            drawer.open();

            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(0.99947777);
            scale.setToY(1);

            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();
        }
    }

    @FXML
    void saveClicked(ActionEvent event) throws IOException {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        try {
            objUserInfo.insertUser(objStmt, objCon);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Parent root = FXMLLoader.load(getClass().getResource("../view/view_users.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    public void refreshControls()
    {
        this.txt_user_id.setText("");
        this.txt_user_name.setText("");
        this.txt_user_contact.setText("");
        this.txt_user_address.setText("");
        this.txt_user_cnic.setText("");
        this.txt_user_type.getSelectionModel().clearSelection();
        this.txt_designated_city.getSelectionModel().clearSelection();
    }

}
