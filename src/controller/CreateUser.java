package controller;

import com.jfoenix.controls.*;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import model.CreateUserInfo;
import model.GlobalVariables;
import model.MysqlCon;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class CreateUser implements Initializable {
    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private JFXButton btn_create_user;

    @FXML
    private JFXComboBox<String> txt_select_staff;

    @FXML
    private JFXTextField txt_user_name;

    @FXML
    private JFXTextField txt_password;

    @FXML
    private JFXTextField txt_re_password;

    @FXML
    private JFXCheckBox chk_insert;

    @FXML
    private JFXCheckBox chk_view;

    @FXML
    private JFXCheckBox chk_change;

    @FXML
    private JFXCheckBox chk_delete;

    @FXML
    private JFXCheckBox chk_manage_stock;

    @FXML
    private JFXCheckBox chk_manage_cash;

    @FXML
    private JFXCheckBox chk_manage_policies;

    @FXML
    private JFXCheckBox chk_mobile_app;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    @FXML
    private BorderPane brdrpane_navsetting;

    @FXML
    private BorderPane menu_bar;

    private CreateUserInfo objCreateUserInfo;
    private String userName;
    private String userId;
    private String password;
    ArrayList<ArrayList<String>> staffData;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        objCreateUserInfo = new CreateUserInfo();
        staffData = new ArrayList<ArrayList<String>>();
        staffData = objCreateUserInfo.getStaffData();
        txt_select_staff.getItems().addAll(getArrayColumn(staffData, 1));
    }

    @FXML
    void createNewUser(ActionEvent event) {
        if(authenticateInfo())
        {
            MysqlCon objMysqlCon = new MysqlCon();
            Statement objStmt = objMysqlCon.stmt;
            Connection objCon = objMysqlCon.con;
            String chkInsert = "0";
            String chkView = "0";
            String chkChange = "0";
            String chkDelete = "0";
            String chkStock = "0";
            String chkCash = "0";
            String chkSettings = "0";
            String chkMobile = "0";
            if(chk_insert.isSelected())
            {
                chkInsert = "1";
            }
            if(chk_view.isSelected())
            {
                chkView = "1";
            }
            if(chk_change.isSelected())
            {
                chkChange = "1";
            }
            if(chk_delete.isSelected())
            {
                chkDelete = "1";
            }
            if(chk_manage_stock.isSelected())
            {
                chkStock = "1";
            }
            if(chk_manage_cash.isSelected())
            {
                chkCash = "1";
            }
            if(chk_manage_policies.isSelected())
            {
                chkSettings = "1";
            }
            if(chk_mobile_app.isSelected())
            {
                chkMobile = "1";
            }

            try {
                objCreateUserInfo.insertUser(objStmt, objCon, userId, userName, password, chkInsert, chkView, chkChange, chkDelete, chkStock, chkCash, chkSettings, chkMobile);
            } catch (SQLException e) {
                e.printStackTrace();
            }

            Parent root = null;
            try {
                root = FXMLLoader.load(getClass().getResource("../view/user_accounts.fxml"));
            } catch (IOException e) {
                e.printStackTrace();
            }
            GlobalVariables.baseScene.setRoot(root);
            GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
        }
    }

    @FXML
    void follow(MouseEvent event) {

    }

    private boolean authenticateInfo()
    {
        userName = txt_user_name.getText().toString();
        password = txt_password.getText().toString();
        userId = getUserId(staffData, txt_select_staff.getValue());
        return userId != null && !userName.equals("") && password.equals(txt_re_password.getText().toString());
    }

    public ArrayList<String> getArrayColumn(ArrayList<ArrayList<String>> staffData, int columnIndex)
    {
        ArrayList<String> columnList = new ArrayList<>();
        for(ArrayList<String> row: staffData) {
            columnList.add(row.get(columnIndex));
        }
        return columnList;
    }

    public String getUserId(ArrayList<ArrayList<String>> staffData, String staffName)
    {
        for(ArrayList<String> row: staffData) {
            if(row.get(1).equals(staffName))
            {
                return row.get(0);
            }
        }
        return null;
    }
}
