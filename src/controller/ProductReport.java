package controller;

import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import model.MysqlCon;
import model.ProductReportInfo;

import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class ProductReport implements Initializable {

    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private TableView<ProductReportInfo> table_productreport;

    @FXML
    private TableColumn<ProductReportInfo, String> sr_no;

    @FXML
    private TableColumn<ProductReportInfo, String> product_id;

    @FXML
    private TableColumn<ProductReportInfo, String> product_name;

    @FXML
    private TableColumn<ProductReportInfo, String> product_type;

    @FXML
    private TableColumn<ProductReportInfo, String> company_name;

    @FXML
    private TableColumn<ProductReportInfo, String> total_sold;

    @FXML
    private TableColumn<ProductReportInfo, String> total_sale;

    @FXML
    private TableColumn<ProductReportInfo, String> in_stock;

    @FXML
    private TableColumn<ProductReportInfo, String> product_status;

    @FXML
    private TableColumn<ProductReportInfo, String> operations;

    @FXML
    private AnchorPane filter_pane;

    @FXML
    private HBox filter_hbox;

    @FXML
    private JFXTextField txt_product_id;

    @FXML
    private JFXTextField txt_product_name;

    @FXML
    private HBox summary_hbox1;

    @FXML
    private Label lbl_total_products;

    @FXML
    private Label lbl_total_sold;

    @FXML
    private Label lbl_total_sale;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    @FXML
    private BorderPane brdrpane_navfinances;

    @FXML
    private BorderPane menu_bar;

    MysqlCon objMysqlCon;
    Statement objStmt;
    Connection objCon;
    private static float totalProducts = 0;
    private static float totalSold = 0;
    private static float totalSale = 0;

    public static String txtProductId = "";
    public static String txtProductName = "";
    public static String txtProductType = "";
    public static boolean filter;
    public static String summary = "";

    public static MenuButton btnView;

    public ObservableList<ProductReportInfo> productReportDetail;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnReport.setStyle("-fx-background-color: #47ab1e");
        objMysqlCon = new MysqlCon();
        objStmt = objMysqlCon.stmt;
        objCon = objMysqlCon.con;

        inner_anchor.setLeftAnchor(inner_anchor, 280d);
        drawer.setSidePane(brdrpane_navfinances);
        drawer.open();

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        product_id.setCellValueFactory(new PropertyValueFactory<>("productId"));
        product_name.setCellValueFactory(new PropertyValueFactory<>("productName"));
        product_type.setCellValueFactory(new PropertyValueFactory<>("productType"));
        company_name.setCellValueFactory(new PropertyValueFactory<>("companyName"));
        total_sold.setCellValueFactory(new PropertyValueFactory<>("totalSold"));
        total_sale.setCellValueFactory(new PropertyValueFactory<>("totalSale"));
        in_stock.setCellValueFactory(new PropertyValueFactory<>("inStock"));
        product_status.setCellValueFactory(new PropertyValueFactory<>("status"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_productreport.setItems(parseUserList());
        lbl_total_products.setText("Products\n"+String.format("%,.0f", totalProducts));
        lbl_total_sold.setText("Sold\n"+String.format("%,.0f", totalSold));
        lbl_total_sale.setText("Sale\n"+String.format("%,.0f", totalSale));
        summary = "";

    }

    private ObservableList<ProductReportInfo> parseUserList() {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        ProductReportInfo objProductReportInfo = new ProductReportInfo();
        productReportDetail = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> productReportData;
        productReportData = objProductReportInfo.getProductReportInfo(objStmt, objCon);
        for (int i = 0; i < productReportData.size(); i++)
        {
            if(summary.equals(""))
            {
                totalProducts++;
                totalSold += (productReportData.get(i).get(5) != null) ? Float.parseFloat(productReportData.get(i).get(5)) : 0;
                totalSale += (productReportData.get(i).get(6) != null) ? Float.parseFloat(productReportData.get(i).get(6)) : 0;
            }
            productReportDetail.add(new ProductReportInfo(String.valueOf(i+1), productReportData.get(i).get(0), ((productReportData.get(i).get(1) == null) ? "N/A" : productReportData.get(i).get(1)), productReportData.get(i).get(2), ((productReportData.get(i).get(3) == null) ? "N/A" : productReportData.get(i).get(3)), ((productReportData.get(i).get(4) == null) ? "0" : productReportData.get(i).get(4)), ((productReportData.get(i).get(5) == null) ? "0" : productReportData.get(i).get(5)), ((productReportData.get(i).get(6) == null) ? "0" : productReportData.get(i).get(6)), ((productReportData.get(i).get(7) == null) ? "0" : productReportData.get(i).get(7)), ((productReportData.get(i).get(8) == null) ? "0" : productReportData.get(i).get(8))));
        }
        return productReportDetail;
    }

    @FXML
    void addCash(ActionEvent event) {

    }

    @FXML
    void btnReset(ActionEvent event) {

    }

    @FXML
    void btnSearch(ActionEvent event) {

    }

    @FXML
    void follow(MouseEvent event) {

    }

    @FXML
    void showCollection(MouseEvent event) {

    }

    @FXML
    void showReturn(MouseEvent event) {

    }

    @FXML
    void showSale(MouseEvent event) {

    }
}
