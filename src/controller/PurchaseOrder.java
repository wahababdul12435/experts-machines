package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.*;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.util.StringConverter;
import model.*;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.controlsfx.control.PropertySheet;

import java.awt.event.KeyEvent;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

public class PurchaseOrder implements Initializable {
    @FXML    private TableView<PurchaseOrderInfo> table_viewdealer;
    @FXML    private TableColumn<PurchaseOrderInfo, String> sr_no;
    @FXML    private TableColumn<PurchaseOrderInfo, String> product_id;
    @FXML    private TableColumn<PurchaseOrderInfo, String> product_name;
    @FXML    private TableColumn<PurchaseOrderInfo, String> carton_size;
    @FXML    private TableColumn<PurchaseOrderInfo, String> quantity_Instock;
    @FXML    private TableColumn<PurchaseOrderInfo, String> quantity_order;
    @FXML    private TableColumn<PurchaseOrderInfo, String> quantity_sold;
    @FXML    private TableColumn<PurchaseOrderInfo, String> purch_price;
    @FXML    private TableColumn<PurchaseOrderInfo, String> net_amount;
    @FXML    private AnchorPane inner_anchor;
    @FXML    private JFXDrawer drawer;
    @FXML    private BorderPane nav_setup;
    @FXML    private BorderPane menu_bar1;
    @FXML    private Pane btnpane;
    @FXML    private Button btn11;
    @FXML    private FontAwesomeIconView FA_Icon;
    @FXML    private AnchorPane filter_pane;
    @FXML    private HBox filter_hbox;
    @FXML    private JFXComboBox<String> combo_company_name;
    @FXML    private JFXTextField txt_inventoryDays;
    @FXML    private JFXTextField txt_order_id;
    @FXML    private JFXTextField txt_PurchaseAmount;
    @FXML    private JFXTextField txt_productQuantity;
    @FXML    private StackPane stackPane;
    private String selectedCompanyId;

    PendingOrders objPendingOrders;
    MysqlCon objMysqlCon;
    Statement objStmt;
    Connection objCon;
    int unSeenOrders;
    private int summaryTotal;
    private int summaryActive;
    private int summaryInActive;
    private ArrayList<ArrayList<String>> companiesData = new ArrayList<>();

    public static boolean filter;
    public static String EditValue;
    public static String orderId;

    private String selectedCityId;
    private String selectedAreaId;
    Alert a = new Alert(Alert.AlertType.NONE);

    private ArrayList<ArrayList<String>> citiesData = new ArrayList<>();
    private ArrayList<ArrayList<String>> areasData = new ArrayList<>();
    private ArrayList<String> options = new ArrayList<>();

    public static Button btnView;
    String purchOrder_id = "";
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        TopHeader.btnPurchase.setStyle("-fx-background-color: #47ab1e");
        MysqlCon newobjMysqlConnc = new MysqlCon();
        Statement objectStattmt = newobjMysqlConnc.stmt;
        Connection objectConnnec = newobjMysqlConnc.con;
        ResultSet rs3 = null;
        try {
            rs3 = objectStattmt.executeQuery("select company_name from company_info where company_status = 'Active' ");
            int i = 0;
            while (rs3.next()) {
                options.add(rs3.getString(1));
            }
            objectConnnec.close();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        UpdateProductInfo objUpdateProductInfo = new UpdateProductInfo();
        companiesData = objUpdateProductInfo.getCompaniesData();
        ArrayList<String> chosenCompanyNames = getArrayColumn(companiesData, 1);
        combo_company_name.getItems().addAll(chosenCompanyNames);


        if(EditValue!="Edit") {
            PurchaseOrderInfo objPurchOrderInfo = new PurchaseOrderInfo();

            MysqlCon mySQLobjc = new MysqlCon();
            Statement mysqlstatmenOBJ = mySQLobjc.stmt;
            Connection mysqlCnectOBJ = mySQLobjc.con;
            purchOrder_id = objPurchOrderInfo.getcurrentOrderID(mysqlstatmenOBJ, mysqlCnectOBJ);
            if (purchOrder_id.isEmpty()) {
                purchOrder_id = "1";
                txt_order_id.setText(purchOrder_id);
            } else {
                int purchidValue = Integer.parseInt(purchOrder_id);
                purchidValue++;
                txt_order_id.setText(String.valueOf(purchidValue));
            }
            unSeenOrders = 0;
            objMysqlCon = new MysqlCon();
            objStmt = objMysqlCon.stmt;
            objCon = objMysqlCon.con;
            ViewDealerInfo.stackPane = stackPane;

            sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
            product_id.setCellValueFactory(new PropertyValueFactory<>("productID"));
            product_name.setCellValueFactory(new PropertyValueFactory<>("productName"));
            carton_size.setCellValueFactory(new PropertyValueFactory<>("cartonSize"));
            quantity_sold.setCellValueFactory(new PropertyValueFactory<>("soldQauntity"));
            quantity_Instock.setCellValueFactory(new PropertyValueFactory<>("quantityInstock"));
            quantity_order.setCellValueFactory(new PropertyValueFactory<>("orderQauntity"));
            purch_price.setCellValueFactory(new PropertyValueFactory<>("purchPrice"));
            net_amount.setCellValueFactory(new PropertyValueFactory<>("netAmount"));

            try {
                table_viewdealer.setItems(parseUserList());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            float sumValue = 0.0f;
            for (int i = 0; i < table_viewdealer.getItems().size(); i++) {
                PurchaseOrderInfo newwobj = (PurchaseOrderInfo) table_viewdealer.getItems().get(i);
                float netValue = Float.parseFloat(newwobj.getNetAmount());
                sumValue = sumValue + netValue;
            }
            String strngTotalPurchAmount = String.valueOf(sumValue);
            txt_PurchaseAmount.setText(strngTotalPurchAmount);
            txt_productQuantity.setText(String.valueOf(table_viewdealer.getItems().size()));
            table_viewdealer.setEditable(true);
            table_viewdealer.getSelectionModel().cellSelectionEnabledProperty().set(true);
            quantity_order.getTableView().setEditable(true);
            quantity_order.setCellFactory(TextFieldTableCell.forTableColumn());
            quantity_order.setOnEditCommit(
                    new EventHandler<TableColumn.CellEditEvent<PurchaseOrderInfo, String>>() {
                        @Override
                        public void handle(TableColumn.CellEditEvent<PurchaseOrderInfo, String> t) {
                            ((PurchaseOrderInfo) t.getTableView().getItems().get(
                                    t.getTablePosition().getRow())
                            ).setOrderQauntity(t.getNewValue());
                            String mk = ((PurchaseOrderInfo) t.getTableView().getItems().get(
                                    t.getTablePosition().getRow())
                            ).getPurchPrice();
                            int quantities = Integer.parseInt(t.getNewValue());
                            float purchaseprice = Float.parseFloat(mk);
                            float totalamount = quantities * purchaseprice;
                            String netamounts = String.valueOf(totalamount);

                            ((PurchaseOrderInfo) t.getTableView().getItems().get(
                                    t.getTablePosition().getRow())
                            ).setNetAmount(netamounts);
                            float sumValue = 0.0f;
                            for (int i = 0; i < table_viewdealer.getItems().size(); i++) {
                                PurchaseOrderInfo newwobj = (PurchaseOrderInfo) table_viewdealer.getItems().get(i);
                                float netValue = Float.parseFloat(newwobj.getNetAmount());
                                sumValue = sumValue + netValue;
                            }
                            String strngTotalPurchAmount = String.valueOf(sumValue);
                            txt_PurchaseAmount.setText(strngTotalPurchAmount);
                            txt_productQuantity.setText(String.valueOf(table_viewdealer.getItems().size()));

                            table_viewdealer.refresh();
                        }
                    }
            );
        }
        else
        {
            combo_company_name.setDisable(true);
            txt_order_id.setDisable(true);
            PurchaseOrderInfo objpurchOrder = new PurchaseOrderInfo();
            MysqlCon objectsMysqlCon = new MysqlCon();
            Statement objectsStmt = objectsMysqlCon.stmt;
            Connection objectsCon = objectsMysqlCon.con;

            editData = objpurchOrder.getpurchOrder_edit(objectsStmt,objectsCon,orderId);

            txt_order_id.setText(orderId);
            txt_PurchaseAmount.setText(editData.get(0));
            txt_productQuantity.setText(editData.get(1));
            txt_inventoryDays.setText(editData.get(2));
            combo_company_name.setValue(editData.get(3));

            sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
            product_id.setCellValueFactory(new PropertyValueFactory<>("productID"));
            product_name.setCellValueFactory(new PropertyValueFactory<>("productName"));
            carton_size.setCellValueFactory(new PropertyValueFactory<>("cartonSize"));
            quantity_sold.setCellValueFactory(new PropertyValueFactory<>("soldQauntity"));
            quantity_Instock.setCellValueFactory(new PropertyValueFactory<>("quantityInstock"));
            quantity_order.setCellValueFactory(new PropertyValueFactory<>("orderQauntity"));
            purch_price.setCellValueFactory(new PropertyValueFactory<>("purchPrice"));
            net_amount.setCellValueFactory(new PropertyValueFactory<>("netAmount"));

            try {
                table_viewdealer.setItems(getEditData());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            float sumValue = 0.0f;
            for (int i = 0; i < table_viewdealer.getItems().size(); i++) {
                PurchaseOrderInfo newwobj = (PurchaseOrderInfo) table_viewdealer.getItems().get(i);
                float netValue = Float.parseFloat(newwobj.getNetAmount());
                sumValue = sumValue + netValue;
            }
            String strngTotalPurchAmount = String.valueOf(sumValue);
            txt_PurchaseAmount.setText(strngTotalPurchAmount);
            txt_productQuantity.setText(String.valueOf(table_viewdealer.getItems().size()));
            table_viewdealer.setEditable(true);
            table_viewdealer.getSelectionModel().cellSelectionEnabledProperty().set(true);
            quantity_order.getTableView().setEditable(true);
            quantity_order.setCellFactory(TextFieldTableCell.forTableColumn());
            quantity_order.setOnEditCommit(
                    new EventHandler<TableColumn.CellEditEvent<PurchaseOrderInfo, String>>() {
                        @Override
                        public void handle(TableColumn.CellEditEvent<PurchaseOrderInfo, String> t) {
                            ((PurchaseOrderInfo) t.getTableView().getItems().get(
                                    t.getTablePosition().getRow())
                            ).setOrderQauntity(t.getNewValue());
                            String mk = ((PurchaseOrderInfo) t.getTableView().getItems().get(
                                    t.getTablePosition().getRow())
                            ).getPurchPrice();
                            int quantities = Integer.parseInt(t.getNewValue());
                            float purchaseprice = Float.parseFloat(mk);
                            float totalamount = quantities * purchaseprice;
                            String netamounts = String.valueOf(totalamount);

                            ((PurchaseOrderInfo) t.getTableView().getItems().get(
                                    t.getTablePosition().getRow())
                            ).setNetAmount(netamounts);
                            float sumValue = 0.0f;
                            for (int i = 0; i < table_viewdealer.getItems().size(); i++) {
                                PurchaseOrderInfo newwobj = (PurchaseOrderInfo) table_viewdealer.getItems().get(i);
                                float netValue = Float.parseFloat(newwobj.getNetAmount());
                                sumValue = sumValue + netValue;
                            }
                            String strngTotalPurchAmount = String.valueOf(sumValue);
                            txt_PurchaseAmount.setText(strngTotalPurchAmount);
                            txt_productQuantity.setText(String.valueOf(table_viewdealer.getItems().size()));

                            table_viewdealer.refresh();
                        }
                    }
            );

        }
    }
    public ObservableList<PurchaseOrderInfo> purchOrdeDetails;
    ArrayList<String> editData;

    private ObservableList<PurchaseOrderInfo> parseUserList() throws ParseException {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        PurchaseOrderInfo objpurchOrder = new PurchaseOrderInfo();

        LocalDate now = LocalDate.now();
        LocalDate earlier = now.minusMonths(1);
        LocalDate startDate =  earlier.withDayOfMonth(1);
        LocalDate endDate = earlier.withDayOfMonth(earlier.lengthOfMonth());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MMM/yyyy");
        String sDate = formatter.format(startDate);
        String eDate = formatter.format(endDate);

        int companyID = 0;
        int invemtoryDays = 0;

            int index = combo_company_name.getSelectionModel().getSelectedIndex();
            if(index >=1)
            {
                selectedCompanyId = companiesData.get(index - 1).get(0);
                companyID = Integer.parseInt(selectedCompanyId);
            }
            if(companyID == 0)
            {
                selectedCompanyId = companiesData.get(0).get(1);
                combo_company_name.setValue(selectedCompanyId);
                companyID = Integer.parseInt(companiesData.get(0).get(0));

            }

        if(txt_inventoryDays.getText().isEmpty())
        {
            invemtoryDays = 45;
            txt_inventoryDays.setText("45");
        }
        else {
            invemtoryDays = Integer.parseInt(txt_inventoryDays.getText());
        }

        purchOrdeDetails = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> purchOrderData;
        purchOrderData  = objpurchOrder.getOrderData(objStmt, objCon,sDate,eDate,companyID,invemtoryDays);

        for (int i = 0; i < purchOrderData.size(); i++)
        {
            purchOrdeDetails.add(new PurchaseOrderInfo(String.valueOf(i+1), purchOrderData.get(i).get(0), ((purchOrderData.get(i).get(1) == null) ? "N/A" : purchOrderData.get(i).get(1)),purchOrderData.get(i).get(2),purchOrderData.get(i).get(3),purchOrderData.get(i).get(4),purchOrderData.get(i).get(5),purchOrderData.get(i).get(6),purchOrderData.get(i).get(7)));
        }
            return purchOrdeDetails;
    }
    private ObservableList<PurchaseOrderInfo> getEditData() throws ParseException {
        PurchaseOrderInfo objpurchOrder = new PurchaseOrderInfo();

        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        purchOrdeDetails = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> purchOrderData;
        purchOrderData  = objpurchOrder.getOrder_Edit(objStmt, objCon,orderId);


        for (int i = 0; i < purchOrderData.size(); i++)
        {
            purchOrdeDetails.add(new PurchaseOrderInfo(String.valueOf(i+1), purchOrderData.get(i).get(0), purchOrderData.get(i).get(1),purchOrderData.get(i).get(2),purchOrderData.get(i).get(3),purchOrderData.get(i).get(4),purchOrderData.get(i).get(5),purchOrderData.get(i).get(6),purchOrderData.get(i).get(7)));
        }
            return purchOrdeDetails;
    }

    public static final LocalDate LOCAL_DATE (String dateString) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MMM/yyyy");
        LocalDate localDate = LocalDate.parse(dateString, formatter);
        return localDate;
    }
    public ObservableList<PurchaseOrderInfo> createpurchOrder;
    @FXML
    void btncreateOrder(ActionEvent event) {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        PurchaseOrderInfo objpurchOrder = new PurchaseOrderInfo();

        LocalDate now = LocalDate.now();
        LocalDate earlier = now.minusMonths(1);
        LocalDate startDate =  earlier.withDayOfMonth(1);
        LocalDate endDate = earlier.withDayOfMonth(earlier.lengthOfMonth());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MMM/yyyy");
        String sDate = formatter.format(startDate);
        String eDate = formatter.format(endDate);

        int companyID = 0;
        int invemtoryDays = 0;

        int index = combo_company_name.getSelectionModel().getSelectedIndex();
        if(index >=1)
        {
            selectedCompanyId = companiesData.get(index - 1).get(0);
            companyID = Integer.parseInt(selectedCompanyId);
        }
        if(companyID == 0)
        {
            a.setAlertType(Alert.AlertType.ERROR);
            a.setHeaderText("Error!");
            a.setContentText("Please Select Company Name.");
            a.show();
            combo_company_name.requestFocus();
        }

        else if(txt_inventoryDays.getText().isEmpty())
        {
            a.setAlertType(Alert.AlertType.ERROR);
            a.setHeaderText("Error!");
            a.setContentText("Please Enter Inventory Days");
            a.show();
            txt_inventoryDays.requestFocus();
        }
        else {
            invemtoryDays = Integer.parseInt(txt_inventoryDays.getText());
        }

        createpurchOrder = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> purchOrderData;

        purchOrderData  = objpurchOrder.getOrderData(objStmt, objCon,sDate,eDate,companyID,invemtoryDays);
        for (int i = 0; i < purchOrderData.size(); i++)
        {
            createpurchOrder.add(new PurchaseOrderInfo(String.valueOf(i+1), purchOrderData.get(i).get(0), ((purchOrderData.get(i).get(1) == null) ? "N/A" : purchOrderData.get(i).get(1)),purchOrderData.get(i).get(2),purchOrderData.get(i).get(3),purchOrderData.get(i).get(4),purchOrderData.get(i).get(5),purchOrderData.get(i).get(6),purchOrderData.get(i).get(7)));
        }
        table_viewdealer.setItems(createpurchOrder);
        float sumValue = 0.0f;
        for(int i = 0; i<table_viewdealer.getItems().size();i++)
        {
            PurchaseOrderInfo newwobj = (PurchaseOrderInfo) table_viewdealer.getItems().get(i);
            float netValue = Float.parseFloat(newwobj.getNetAmount());
            sumValue = sumValue + netValue;
        }
        String strngTotalPurchAmount = String.valueOf(sumValue);
        txt_PurchaseAmount.setText(strngTotalPurchAmount);
        txt_productQuantity.setText(String.valueOf(table_viewdealer.getItems().size()));
    }

    @FXML
    void btnReset(ActionEvent event) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/view_dealers.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void exportExcel(ActionEvent event) throws IOException {
        Workbook workbook = new HSSFWorkbook();
        Sheet spreadsheet = workbook.createSheet("data");
        Row row = spreadsheet.createRow(0);

        for (int j = 0; j < table_viewdealer.getColumns().size()-1; j++) {
            row.createCell(j).setCellValue(table_viewdealer.getColumns().get(j).getText());
        }

        for (int i = 0; i < table_viewdealer.getItems().size(); i++) {
            row = spreadsheet.createRow(i + 1);
            for (int j = 0; j < table_viewdealer.getColumns().size()-1; j++) {
                if(table_viewdealer.getColumns().get(j).getCellData(i) != null) {
                    row.createCell(j).setCellValue(table_viewdealer.getColumns().get(j).getCellData(i).toString());
                }
                else {
                    row.createCell(j).setCellValue("");
                }
            }
        }

        FileOutputStream fileOut = new FileOutputStream("data/View_Dealers.xls");
        workbook.write(fileOut);
        fileOut.close();
    }

    public void follow() throws IOException
    {
        if (drawer.isOpened()) {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(-250);
            transition.setByY(0);
            transition.setCycleCount(1);
            transition.play();
            drawer.close();
            FA_Icon.setGlyphName("ANGLE_DOUBLE_RIGHT");
            FA_Icon.setGlyphSize(14);
            //inner_anchor.setRightAnchor(inner_anchor,1d);
            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(-90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(1.1000);
            scale.setToY(1);

            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();

        }
        else {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(250);
            transition.setByY(0);
            transition.setCycleCount(1);
            FA_Icon.setGlyphName("ANGLE_DOUBLE_LEFT");
            FA_Icon.setGlyphSize(14);
            drawer.open();

            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(0.99947777);
            scale.setToY(1);

            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();

        }
    }

    public ArrayList<String> getArrayColumn(ArrayList<ArrayList<String>> citiesData, int columnIndex)
    {
        ArrayList<String> columnList = new ArrayList<>();
        for(ArrayList<String> row: citiesData) {
            columnList.add(row.get(columnIndex));
        }
        return columnList;
    }

    @FXML
    void addDealer() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/enter_dealer.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
    String prodTableIDValue = "";
    String CompanyTableIDValue = "";
    public void savePurchOrder(ActionEvent actionEvent) {
        PurchaseOrderInfo obj_purchOrder = new PurchaseOrderInfo();
        SaleInvoice objsaleinvoice1 = new SaleInvoice();

        int counttableROWS = table_viewdealer.getItems().size();
        String[] getprodid_tableview = new String[counttableROWS];
        String[] getprodName_tableview = new String[counttableROWS];
        String[] getprodcartonSize_tableview = new String[counttableROWS];
        String[] getSoldQaunt_tableview = new String[counttableROWS];
        String[] getquantInStock_tableview = new String[counttableROWS];
        String[] getorderQuant_tableview = new String[counttableROWS];
        String[] getnetAmount_tableview = new String[counttableROWS];

        MysqlCon objMySql = new MysqlCon();
        Statement mysqlstatmentOBJ = objMySql.stmt;
        Connection mysqlConectOBJ = objMySql.con;
        CompanyTableIDValue = obj_purchOrder.getTablecompanyID(mysqlstatmentOBJ,mysqlConectOBJ,combo_company_name.getValue() );

        for(int i =0;i<counttableROWS;i++) //////////////////////////////////////START OF LOOP TO READ ALL ROWS OF TABLVIEW
        {
            PurchaseOrderInfo purchaseOrderObject = (PurchaseOrderInfo) table_viewdealer.getItems().get(i);
            getprodid_tableview[i] = purchaseOrderObject.getProductID();
            getprodName_tableview[i] = purchaseOrderObject.getProductName();
            getprodcartonSize_tableview[i] = purchaseOrderObject.getCartonSize();
            getSoldQaunt_tableview[i] = purchaseOrderObject.getSoldQauntity();
            getquantInStock_tableview[i] = purchaseOrderObject.getQuantityInstock();
            getorderQuant_tableview[i] = purchaseOrderObject.getOrderQauntity();
            getnetAmount_tableview[i] = purchaseOrderObject.getNetAmount();

            int intvalue_prodID = Integer.parseInt(getprodid_tableview[i]);
            int intvalue_quantity = Integer.parseInt(getorderQuant_tableview[i]);
            if(intvalue_quantity != 0) {
                MysqlCon mySQLobjc = new MysqlCon();
                Statement mysqlstatmenOBJ = mySQLobjc.stmt;
                Connection mysqlCnectOBJ = mySQLobjc.con;
                prodTableIDValue = objsaleinvoice1.getTableprodID(mysqlstatmenOBJ, mysqlCnectOBJ, intvalue_prodID);
                MysqlCon mySQLobj = new MysqlCon();
                Statement statmntObj = mySQLobj.stmt;
                Connection sqlConn = mySQLobj.con;
                if(EditValue != "Edit") {
                        obj_purchOrder.insertPurchOrderDetails(statmntObj, sqlConn, prodTableIDValue, getSoldQaunt_tableview[i], getquantInStock_tableview[i], getorderQuant_tableview[i], txt_order_id.getText(), getnetAmount_tableview[i]);
                }
                if(EditValue == "Edit")
                {
                    obj_purchOrder.updatePurchOrderDetails(statmntObj, sqlConn, prodTableIDValue, getSoldQaunt_tableview[i], getquantInStock_tableview[i], getorderQuant_tableview[i], txt_order_id.getText(), getnetAmount_tableview[i]);
                }
            }
        }

        MysqlCon objMysqlConct = new MysqlCon();
        Statement objStmt = objMysqlConct.stmt;
        Connection objContn = objMysqlConct.con;
        String creatingDate = GlobalVariables.getStDate();
        String creatingTime = GlobalVariables.getStTime();
        String creatingUser = GlobalVariables.getUserId();
        if(EditValue!="Edit") {
            obj_purchOrder.insertPurchOrderInfo(objStmt, objContn, txt_order_id.getText(), creatingDate, txt_PurchaseAmount.getText(), txt_productQuantity.getText(), txt_inventoryDays.getText(), CompanyTableIDValue, "Pending", creatingUser, creatingTime);
        }
        if(EditValue=="Edit") {
            obj_purchOrder.updatePurchOrderInfo(objStmt, objContn, txt_order_id.getText(), creatingDate, txt_PurchaseAmount.getText(), txt_productQuantity.getText(), txt_inventoryDays.getText(), CompanyTableIDValue, "Pending", creatingUser, creatingTime);
            EditValue = "not_edit";
        }
    }
}
