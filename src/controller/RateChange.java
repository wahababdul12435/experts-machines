package controller;

import com.jfoenix.controls.JFXButton;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Worker;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.StackPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import model.*;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.view.JasperViewer;
import netscape.javascript.JSObject;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;


public class RateChange implements Initializable
{

    @FXML    private JFXButton btn_cancel;
    @FXML    private TextField txt_Retail;
    @FXML    private TextField txt_trade;
    @FXML    private TextField txt_batch;
    @FXML    private TextField txt_Prodid;
    @FXML    private TextField txt_Prodname;
    @FXML    private TextField txt_purchase;
    @FXML    private Label lbl_Quant;
    @FXML    private TableView<RateChangeInfo> tableView_ProdRates;
    @FXML    private TableColumn<RateChangeInfo, String> sr_no;
    @FXML    private TableColumn<RateChangeInfo, String> product_batch;
    @FXML    private TableColumn<RateChangeInfo, String> product_retail;
    @FXML    private TableColumn<RateChangeInfo, String> product_trade;
    @FXML    private TableColumn<RateChangeInfo, String> product_purchase;
    @FXML    private TableColumn<RateChangeInfo, String> product_quantities;
    @FXML    private TableColumn<RateChangeInfo, String> operations;

    public static int product_ID;
    public static String retailRate;
    public static String tradeRate;
    public static String purchaseRate;
    public static String prodctsquantity;
    public static String prodctsbatch;

    String[] prodctRatelist = new String[1];
    String batch_policy;
    ArrayList<String> batchlist = new ArrayList<>();
    String getProductsbatch;
    Alert a = new Alert(Alert.AlertType.NONE);

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {


        if(product_ID != 0 )
        {
            PurchaseInvoice objpurchinvoice = new PurchaseInvoice();
            MysqlCon myobjectSQL = new MysqlCon();
            Statement mystatmnOBJ = myobjectSQL.stmt;
            Connection myCnnctionOBJ = myobjectSQL.con;

            batch_policy = objpurchinvoice.getbatchSelectionpolicy(mystatmnOBJ, myCnnctionOBJ);
            if (batch_policy.isEmpty()) {
            }
            else {
                String[] split_batchSelection = batch_policy.split(" ");

                MysqlCon myobjectmySQL = new MysqlCon();
                Statement statmnOBJ = myobjectmySQL.stmt;
                Connection CnnctionOBJ = myobjectmySQL.con;
                batchlist = objpurchinvoice.getprod_otherbatchs(statmnOBJ, CnnctionOBJ, product_ID);

                if (batchlist.isEmpty()) {}
                else {
                    String[] getall_dates = new String[20];
                    String[] getall_time = new String[20];
                    for (int f = 0; f < batchlist.size(); f++)
                    {
                        String[] split = batchlist.get(f).split("--");
                        if (split_batchSelection[0].equals("Entry")) {
                            getall_dates[f] = split[1];
                            getall_time[f] = split[2];
                        }
                        if (split_batchSelection[0].equals("Expiry")) {
                            getall_dates[f] = split[3];
                            getall_time[f] = split[2];
                        }

                    }
                    List<String> myList = new ArrayList<>();
                    for (int g = 0; g < batchlist.size(); g++) {
                        if (split_batchSelection[1].equals("FIFO") || split_batchSelection[1].equals("LILO")) {
                            String cmbine = getall_dates[g] + " " + getall_time[g];
                            myList.add(cmbine);
                            Collections.sort(myList);
                        }
                        if (split_batchSelection[1].equals("LIFO") || split_batchSelection[1].equals("FIFO")) {
                            String cmbine = getall_dates[g] + " " + getall_time[g];
                            myList.add(cmbine);
                            Collections.sort(myList, Collections.reverseOrder());
                        }
                    }
                    ///////////////////////////////////////////SELECT THE POLICY OF FIFO OR LIFO ETC
                    String cnvrtdatetime = String.valueOf(myList.get(0));
                    String[] splitDate_Time = cnvrtdatetime.split(" ");
                    String getscndbatchno = "";
                    if (split_batchSelection[0].equals("Entry")) {
                        MysqlCon mysqlOBJct = new MysqlCon();
                        Statement mystamtOBJct = mysqlOBJct.stmt;
                        Connection mysqlConnobjct = mysqlOBJct.con;
                        getscndbatchno = objpurchinvoice.getprodbatch_entry(mystamtOBJct, mysqlConnobjct, product_ID, splitDate_Time[0], splitDate_Time[1]);
                    }
                    if (split_batchSelection[0].equals("Expiry")) {
                        MysqlCon mysqlOBJct = new MysqlCon();
                        Statement mystamtOBJct = mysqlOBJct.stmt;
                        Connection mysqlConnobjct = mysqlOBJct.con;
                        getscndbatchno = objpurchinvoice.getprodbatch_expiry(mystamtOBJct, mysqlConnobjct, product_ID, splitDate_Time[0], splitDate_Time[1]);
                    }
                    if (getscndbatchno.isEmpty()) {
                    } else {
                        getProductsbatch = getscndbatchno;
                    }

                    String[] batch_quantity = getProductsbatch.split("-");
                    RateChangeInfo objratechange = new RateChangeInfo();
                    MysqlCon objMysqlCon = new MysqlCon();
                    Statement objStmt = objMysqlCon.stmt;
                    Connection objCon = objMysqlCon.con;
                    prodctRatelist = objratechange.get_prodDetails(objStmt, objCon, product_ID, batch_quantity[0]);

                    if(!prodctRatelist[0].isEmpty())
                    {
                        String[] splitRates = prodctRatelist[0].split("--");
                        txt_Retail.setText(splitRates[0]);
                        txt_trade.setText(splitRates[1]);
                        txt_purchase.setText(splitRates[2]);
                        lbl_Quant.setText(splitRates[3]);
                        txt_batch.setText(batch_quantity[0]);
                    }
                    MysqlCon obj_MysqlCon = new MysqlCon();
                    Statement obj_Stmt = obj_MysqlCon.stmt;
                    Connection obj_Con = obj_MysqlCon.con;
                    String prodctName = objratechange.get_prodNames(obj_Stmt, obj_Con, product_ID);
                    if(!prodctName.isEmpty())
                    {
                        String strPordID = String.valueOf(product_ID);
                        txt_Prodid.setText(strPordID);
                        txt_Prodname.setText(prodctName);
                    }

                    }
            }
            if(tradeRate != null)
            {
                if(txt_trade.getText().equals(tradeRate))
                {

                }
                else
                {
                    txt_trade.setText(tradeRate);
                    txt_batch.setText(prodctsbatch);
                    txt_purchase.setText(purchaseRate);
                    txt_Retail.setText(retailRate);
                    lbl_Quant.setText(prodctsquantity);
                }
            }

            sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
            product_batch.setCellValueFactory(new PropertyValueFactory<>("productBatch"));
            product_retail.setCellValueFactory(new PropertyValueFactory<>("productRetail"));
            product_trade.setCellValueFactory(new PropertyValueFactory<>("productTrade"));
            product_purchase.setCellValueFactory(new PropertyValueFactory<>("productPurchase"));
            product_quantities.setCellValueFactory(new PropertyValueFactory<>("productQuantities"));
            operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
            tableView_ProdRates.setItems(parseUserList());
        }
    }
    public ObservableList<RateChangeInfo> prodBatchDetails;
    private ArrayList<ArrayList<String>> prodBatchData;

    private ObservableList<RateChangeInfo> parseUserList(){
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        RateChangeInfo objratechangeInfo = new RateChangeInfo();
        prodBatchDetails = FXCollections.observableArrayList();
        prodBatchData = objratechangeInfo.getprodRateInfo(objStmt, objCon,product_ID);
        for (int i = 0; i < prodBatchData.size(); i++)
        {
            prodBatchDetails.add(new RateChangeInfo(String.valueOf(i+1), prodBatchData.get(i).get(0), ((prodBatchData.get(i).get(1) == null) ? "N/A" : prodBatchData.get(i).get(1)), prodBatchData.get(i).get(2), ((prodBatchData.get(i).get(3) == null) ? "N/A" : prodBatchData.get(i).get(3)),((prodBatchData.get(i).get(4) == null) ? "N/A" : prodBatchData.get(i).get(4))));
        }
        return prodBatchDetails;
    }

    public void viewbatchClicked() throws IOException
    {
        txt_Prodid.setText("asdc");
        txt_batch.setText("batchNumber");
        /*String batchNumber = "";
        batchNumber = RateChangeInfo.prodctsbatch;
        txt_batch.setText(batchNumber);
        txt_Retail.setText(RateChangeInfo.retailRate);
        txt_trade.setText(RateChangeInfo.tradeRate);
        txt_purchase.setText(RateChangeInfo.purchaseRate);
        lbl_Quant.setText(RateChangeInfo.prodctsquantity);*/


    }

    public void closewindow() throws IOException
    {
        PurchaseInvoice.changeRatePane.setVisible(false);
    }
}
