package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import model.*;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class UpdateProduct implements Initializable {
    @FXML    private Label label_update_product;
    @FXML    private JFXTextField txt_product_id;
    @FXML    private JFXTextField txt_product_name;
    @FXML    private JFXComboBox<String> txt_product_type;
    @FXML    private JFXComboBox<String> txt_product_status;
    @FXML    private JFXButton dialog_product_close;
    @FXML    private JFXButton dialog_product_update;
    @FXML    private JFXComboBox<String> txt_company_name;
    @FXML    private JFXComboBox<String> txt_company_group;
    @FXML    private JFXTextField txt_retail_price;
    @FXML    private JFXTextField txt_trade_price;
    @FXML    private JFXTextField txt_purchase_price;
    @FXML    private JFXTextField txt_purchase_discount;
    @FXML    private JFXTextField txt_final_price;
    @FXML    private JFXTextField txt_sales_tax;

    public static String productTableId ="";
    public static String productId ="";
    public static String productName ="";
    public static String productType ="";
    public static String companyName ="";
    public static String companyGroup ="";
    public static String purchasePrice ="";
    public static String retailPrice ="";
    public static String purchaseDiscount ="";
    public static String tradePrice ="";
    public static String salesTax ="";
    public static String productStatus ="";

    private String selectedCompanyId;
    private String selectedGroupId;

    private ArrayList<ArrayList<String>> companiesData = new ArrayList<>();
    private ArrayList<ArrayList<String>> groupsData = new ArrayList<>();
    private ArrayList<String> chosenGroupIds = new ArrayList<>();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ViewProductsInfo.lblUpdate = label_update_product;
        UpdateProductInfo objUpdateProductInfo = new UpdateProductInfo();
        companiesData = objUpdateProductInfo.getCompaniesData();
        groupsData = objUpdateProductInfo.getCompanyGroupsData();

        txt_product_id.setText(productId);
        txt_product_name.setText(productName);
        txt_product_type.setValue(productType);
        ArrayList<String> chosenCompanyNames = getArrayColumn(companiesData, 1);
        txt_company_name.getItems().addAll(chosenCompanyNames);
        txt_company_name.setValue(companyName);
        txt_company_group.getItems().addAll(getCompanyGroups(groupsData, getCompanyId(companiesData, companyName)));
        txt_company_group.setValue(companyGroup);
        getGroupId(groupsData, companyGroup, selectedCompanyId);
        txt_purchase_price.setText(purchasePrice);
        txt_retail_price.setText(retailPrice);
        txt_purchase_discount.setText(purchaseDiscount);
        txt_trade_price.setText(tradePrice);
        txt_product_status.setValue(productStatus);
        dialog_product_close.setOnAction((action)->closeDialog());
        dialog_product_update.setOnAction((action)->updateProduct());
    }

    public void closeDialog()
    {
        ViewProductsInfo.dialog.close();
        ViewProductsInfo.stackPane.setVisible(false);
    }

    public void updateProduct() {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        ProductInfo objProductInfo = new ProductInfo();
        productId = txt_product_id.getText();
        productName = txt_product_name.getText();
        productType = txt_product_type.getValue();
        retailPrice = txt_retail_price.getText();
        tradePrice = txt_trade_price.getText();
        purchasePrice = txt_purchase_price.getText();
        purchaseDiscount = txt_purchase_discount.getText();
        if(purchaseDiscount.equals("") || purchaseDiscount == null)
        {
            purchaseDiscount = "0";
        }
        productStatus = txt_product_status.getValue();
        objProductInfo.updateProduct(objStmt, objCon, productTableId, productId, productName, productType, selectedCompanyId, selectedGroupId, retailPrice, tradePrice, purchasePrice, purchaseDiscount, "0", productStatus);

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/view_products.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void companyChange(ActionEvent event) {
        int index = txt_company_name.getSelectionModel().getSelectedIndex();
        selectedCompanyId = companiesData.get(index).get(0);
        txt_company_group.getItems().clear();
        ArrayList<String> chosenGroupNames = getCompanyGroups(groupsData, selectedCompanyId);
        txt_company_group.getItems().addAll(chosenGroupNames);
        txt_company_group.setValue(chosenGroupNames.get(0));
        selectedGroupId = chosenGroupIds.get(0);
    }

    @FXML
    void groupChange(ActionEvent event) {
        int index = txt_company_group.getSelectionModel().getSelectedIndex();
        if(index >= 0)
        {
            selectedGroupId = chosenGroupIds.get(index);
        }
    }

    public ArrayList<String> getArrayColumn(ArrayList<ArrayList<String>> companiesData, int columnIndex)
    {
        ArrayList<String> columnList = new ArrayList<>();
        for(ArrayList<String> row: companiesData) {
            columnList.add(row.get(columnIndex));
        }
        return columnList;
    }

    public ArrayList<String> getCompanyGroups(ArrayList<ArrayList<String>> groupsData, String companyId)
    {
        ArrayList<String> columnList = new ArrayList<>();
        chosenGroupIds = new ArrayList<>();
        for(ArrayList<String> row: groupsData) {
            if(row.get(2).equals(companyId))
            {
                chosenGroupIds.add(row.get(0));
                columnList.add(row.get(1));
            }
        }
        return columnList;
    }

    public String getCompanyId(ArrayList<ArrayList<String>> companiesData, String companyName)
    {
        for(ArrayList<String> row: companiesData) {
            if(row.get(1).equals(companyName))
            {
                selectedCompanyId = row.get(0);
                return selectedCompanyId;
            }
        }
        return null;
    }

    public String getGroupId(ArrayList<ArrayList<String>> groupData, String groupName, String companyId)
    {
        for(ArrayList<String> row: groupData) {
            if(row.get(1).equals(groupName) && row.get(2).equals(companyId))
            {
                selectedGroupId = row.get(0);
                return selectedGroupId;
            }
        }
        return null;
    }
}
