package controller;

import com.jfoenix.controls.*;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.Duration;
import model.*;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class BonusPolicy implements Initializable {
    @FXML    private AnchorPane inner_anchor;
    @FXML    private JFXDrawer drawer;
    @FXML    private Button btn11;
    @FXML    private Pane btnpane;
    @FXML    private FontAwesomeIconView FA_Icon;
    @FXML    private BorderPane nav_setup;
    @FXML    private BorderPane menu_bar1;
    @FXML    private Button cancelBtn;
    @FXML    private TableView tableView_dealerType;
    @FXML    private TableView tableView_dealerName;
    @FXML    private JFXTextField txt_policyID;
    @FXML    private JFXTextField txt_dealerID;
    @FXML    private JFXTextField txt_dealerName;
    @FXML    private JFXTextField txt_prodID;
    @FXML    private JFXTextField txt_prodName;
    @FXML   private JFXTextField txt_packsize;
    @FXML    private JFXTextField txt_quantity;
    @FXML    private JFXTextField txt_bonus;
    @FXML    private JFXCheckBox chkbox_specficDealer;
    @FXML    private JFXCheckBox chkbox_Dealer_type;
    @FXML    private JFXDatePicker datepick_startdate;
    @FXML    private JFXDatePicker datepick_enddate;
    @FXML    private JFXComboBox<String> combo_dealertype;


    ObservableList<String> options = FXCollections.observableArrayList();
    Alert a = new Alert(Alert.AlertType.NONE);

    public void cancelClicked() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/home.fxml"));
        Stage objstage = (Stage) cancelBtn.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    public void initialize(URL location, ResourceBundle resources) {
        txt_dealerID.setVisible(false);
        txt_dealerName.setVisible(false);
        chkbox_specficDealer.setSelected(false);
        chkbox_Dealer_type.setSelected(true);
        combo_dealertype.setVisible(true);
        tableView_dealerName.setVisible(false);



        MysqlCon newobjMysqlConnc = new MysqlCon();
        Statement objectStattmt = newobjMysqlConnc.stmt;
        Connection objectConnnec = newobjMysqlConnc.con;
        ResultSet rs3 = null;
        try {
            rs3 = objectStattmt.executeQuery("select dealerType_name from dealer_types where DeleteStatus = 0");
            int i = 0;
            while (rs3.next()) {
                options.add(rs3.getString(1));
            }
            objectConnnec.close();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }

        combo_dealertype.getItems().addAll(options);

        txt_policyID.focusedProperty().addListener(new ChangeListener<Boolean>()
        {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue)
            {
                if(!txt_policyID.getText().isEmpty())
                {
                    if (newPropertyValue)
                    {
                        //System.out.println("Textfield on focus");
                    }
                    else {
                        String verifypolicyid = "";
                        MysqlCon newobjMysqlConnc = new MysqlCon();
                        Statement objectStattmt = newobjMysqlConnc.stmt;
                        Connection objectConnnec = newobjMysqlConnc.con;
                        ResultSet rs3 = null;
                        try {
                            rs3 = objectStattmt.executeQuery("select approval_id from bonus_policy ");
                            while (rs3.next()) {
                                verifypolicyid = (rs3.getString(1));
                            }
                            objectConnnec.close();
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                        if (!verifypolicyid.equals(txt_policyID.getText())) {
                            txt_prodID.requestFocus();
                        }
                        else
                        {
                            a.setAlertType(Alert.AlertType.ERROR);
                            a.setHeaderText("Policy ID already Exist");
                            a.setContentText("Please Enter new Policy ID");
                            a.show();
                            txt_policyID.requestFocus();
                        }
                    }
                }
            }
        });

        btnpane.setOnMouseEntered(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(22);
                btnpane.setMinWidth(20.0);
                btn11.setMinWidth(20.0);

            }
        });
        btnpane.setOnMouseExited(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(14);
                btnpane.setMinWidth(10.0);
                btn11.setMinWidth(10.0);
            }
        });

        inner_anchor.setLeftAnchor(inner_anchor, 260d);
        drawer.setSidePane(nav_setup);
        drawer.open();

        datepick_startdate.setValue(LocalDate.now());
        datepick_enddate.setValue(LocalDate.now());

        TableColumn approvalID_Col = new TableColumn("Approval ID");
        approvalID_Col.setMinWidth(80);
        TableColumn productID_COL = new TableColumn("Product ID");
        productID_COL.setMinWidth(70);
        TableColumn productName_COL = new TableColumn("Name");
        productName_COL.setMinWidth(140);
        TableColumn packsize_COL = new TableColumn("Pack Size");
        packsize_COL.setMinWidth(80);
        TableColumn dealertype_COL = new TableColumn("Dealer Type");
        dealertype_COL.setMinWidth(80);
        TableColumn productquantity_COL = new TableColumn("Quantity");
        productquantity_COL.setMinWidth(70);
        TableColumn productbonus_COL = new TableColumn("Bonus");
        productbonus_COL.setMinWidth(70);
        TableColumn startDate_COL = new TableColumn("Start Date");
        startDate_COL.setMinWidth(80);
        TableColumn endDate_COL = new TableColumn("End Date");
        endDate_COL.setMinWidth(80);
        TableColumn productEditCOL = new TableColumn("Actions");
        productEditCOL.setMinWidth(140);
        TableColumn firstEDITCol = new TableColumn("Edit");
        TableColumn secondDELETECol = new TableColumn("Delete");

        productEditCOL.getColumns().addAll(firstEDITCol, secondDELETECol);
        tableView_dealerType.getColumns().addAll(approvalID_Col, productID_COL, productName_COL, packsize_COL, dealertype_COL, productquantity_COL, productbonus_COL, startDate_COL, endDate_COL, productEditCOL);
        approvalID_Col.setCellValueFactory(new PropertyValueFactory<Bonus, String>("approvalids"));
        productID_COL.setCellValueFactory(new PropertyValueFactory<Bonus, String>("productIds"));
        productName_COL.setCellValueFactory(new PropertyValueFactory<Bonus, String>("productNames"));
        packsize_COL.setCellValueFactory(new PropertyValueFactory<Bonus, String>("packsizes"));
        dealertype_COL.setCellValueFactory(new PropertyValueFactory<Bonus, String>("bonusfor"));
        productquantity_COL.setCellValueFactory(new PropertyValueFactory<Bonus, String>("quantities"));
        productbonus_COL.setCellValueFactory(new PropertyValueFactory<Bonus, String>("bonuses"));
        startDate_COL.setCellValueFactory(new PropertyValueFactory<Bonus, String>("startdates"));
        endDate_COL.setCellValueFactory(new PropertyValueFactory<Bonus, String>("enddates"));

        //START of Creating Delete Button
        secondDELETECol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Bonus, Boolean>, ObservableValue>() {
            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<Bonus, Boolean> param) {
                return new SimpleBooleanProperty(param.getValue() != null);
            }

        });
        secondDELETECol.setCellFactory(new Callback<TableColumn<Bonus, Boolean>, TableCell<Bonus, Boolean>>() {
            @Override
            public TableCell<Bonus, Boolean> call(TableColumn<Bonus, Boolean> param) {
                return new BonusPolicy.ButtonCell(tableView_dealerType);
            }
        });
        //END of Creating Delete Button
        //START of Creating EDIT Button
        firstEDITCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Bonus, Boolean>, ObservableValue>() {
            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<Bonus, Boolean> param) {
                return new SimpleBooleanProperty(param.getValue() != null);
            }

        });
        firstEDITCol.setCellFactory(new Callback<TableColumn<Bonus, Boolean>, TableCell<Bonus, Boolean>>() {
            @Override
            public TableCell<Bonus, Boolean> call(TableColumn<Bonus, Boolean> param) {
                return new BonusPolicy.Editbtncell(tableView_dealerType);
            }
        });
        //END of Creating EDIT Button
        tableView_dealerType.setItems(data_dealerType);



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



        TableColumn approvalID_Col2 = new TableColumn("Approval ID");
        approvalID_Col2.setMinWidth(80);
        TableColumn productID_COL2 = new TableColumn("Product ID");
        productID_COL2.setMinWidth(70);
        TableColumn productName_COL2 = new TableColumn("Name");
        productName_COL2.setMinWidth(140);
        TableColumn packsize_COL2 = new TableColumn("Pack Size");
        packsize_COL2.setMinWidth(80);
        TableColumn dealerID_COL2 = new TableColumn("Dealer ID");
        dealerID_COL2.setMinWidth(80);
        TableColumn dealerName_COL2 = new TableColumn("Dealer Name");
        dealerName_COL2.setMinWidth(80);
        TableColumn productquantity_COL2 = new TableColumn("Quantity");
        productquantity_COL2.setMinWidth(70);
        TableColumn productbonus_COL2 = new TableColumn("Bonus");
        productbonus_COL2.setMinWidth(70);
        TableColumn startDate_COL2 = new TableColumn("Start Date");
        startDate_COL2.setMinWidth(80);
        TableColumn endDate_COL2 = new TableColumn("End Date");
        endDate_COL2.setMinWidth(80);
        TableColumn productEditCOL2 = new TableColumn("Actions");
        productEditCOL2.setMinWidth(140);
        TableColumn firstEDITCol2 = new TableColumn("Edit");
        TableColumn secondDELETECol2 = new TableColumn("Delete");

        productEditCOL2.getColumns().addAll(firstEDITCol2, secondDELETECol2);
        tableView_dealerName.getColumns().addAll(approvalID_Col2, productID_COL2, productName_COL2, packsize_COL2, dealerID_COL2, dealerName_COL2, productquantity_COL2, productbonus_COL2, startDate_COL2, endDate_COL2, productEditCOL2);
        approvalID_Col2.setCellValueFactory(new PropertyValueFactory<Bonus, String>("approvalids"));
        productID_COL2.setCellValueFactory(new PropertyValueFactory<Bonus, String>("productIds"));
        productName_COL2.setCellValueFactory(new PropertyValueFactory<Bonus, String>("productNames"));
        packsize_COL2.setCellValueFactory(new PropertyValueFactory<Bonus, String>("packsizes"));
        dealerID_COL2.setCellValueFactory(new PropertyValueFactory<Bonus, String>("dealerID"));
        dealerName_COL2.setCellValueFactory(new PropertyValueFactory<Bonus, String>("dealerName"));
        productquantity_COL2.setCellValueFactory(new PropertyValueFactory<Bonus, String>("quantities"));
        productbonus_COL2.setCellValueFactory(new PropertyValueFactory<Bonus, String>("bonuses"));
        startDate_COL2.setCellValueFactory(new PropertyValueFactory<Bonus, String>("startdates"));
        endDate_COL2.setCellValueFactory(new PropertyValueFactory<Bonus, String>("enddates"));

        //START of Creating Delete Button
        secondDELETECol2.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Bonus, Boolean>, ObservableValue>() {
            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<Bonus, Boolean> param) {
                return new SimpleBooleanProperty(param.getValue() != null);
            }

        });
        secondDELETECol2.setCellFactory(new Callback<TableColumn<Bonus, Boolean>, TableCell<Bonus, Boolean>>() {
            @Override
            public TableCell<Bonus, Boolean> call(TableColumn<Bonus, Boolean> param) {
                return new BonusPolicy.ButtonCell(tableView_dealerName);
            }
        });
        //END of Creating Delete Button
        //START of Creating EDIT Button
        firstEDITCol2.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Bonus, Boolean>, ObservableValue>() {
            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<Bonus, Boolean> param) {
                return new SimpleBooleanProperty(param.getValue() != null);
            }

        });
        firstEDITCol2.setCellFactory(new Callback<TableColumn<Bonus, Boolean>, TableCell<Bonus, Boolean>>() {
            @Override
            public TableCell<Bonus, Boolean> call(TableColumn<Bonus, Boolean> param) {
                return new BonusPolicy.Editbtncell(tableView_dealerName);
            }
        });
        //END of Creating EDIT Button
        tableView_dealerName.setItems(data_dealerName);

    }

    ObservableList<Bonus> data_dealerType = FXCollections.observableArrayList();
    ObservableList<Bonus> data_dealerName = FXCollections.observableArrayList();
    int rowvalueAfterEdit = 0;

    // START of Button Code for Editing a Row
    private class Editbtncell extends TableCell<Bonus, Boolean> {
        final Button btncell = new Button("Edit");

        public Editbtncell(final TableView view1) {
            btncell.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    rowvalueAfterEdit = getTableRow().getIndex();
                    Bonus newobj = (Bonus) tableView_dealerType.getItems().get(rowvalueAfterEdit);
                    txt_policyID.setText(newobj.getApprovalids());
                    txt_prodID.setText(newobj.getProductIds());
                    txt_prodName.setText(newobj.getProductNames());
                    txt_packsize.setText(newobj.getPacksizes());
                    txt_quantity.setText(newobj.getQuantities());
                    txt_bonus.setText(newobj.getBonuses());
                    String strng_strtdate = newobj.getStartdates();
                    String strng_enddate = newobj.getEnddates();

                }
            });
        }

        @Override
        protected void updateItem(Boolean n, boolean boln) {
            super.updateItem(n, boln);
            if (boln) {
                setGraphic(null);
            } else {
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setGraphic(btncell);
            }
        }
    }
    // END of Button Code for Editing a Row
    //START of Button Code for Deleting a Row
    private class ButtonCell extends TableCell<Bonus, Boolean> {

        final Button CellButton = new Button("Delete");

        public ButtonCell(final TableView view) {
            CellButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    int selectedIndex = getTableRow().getIndex();
                    data_dealerType.remove(selectedIndex);

                }
            });
        }

        @Override
        protected void updateItem(Boolean t, boolean bln) {
            super.updateItem(t, bln);
            if (bln) {
                setGraphic(null);
            } else {
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setGraphic(CellButton);
            }

        }
    }
    //END of Button Code for Deleting a Row

    private class Editbtncell2 extends TableCell<Bonus, Boolean> {
        final Button btncell = new Button("Edit");

        public Editbtncell2(final TableView view1) {
            btncell.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    rowvalueAfterEdit = getTableRow().getIndex();
                    Bonus newobj = (Bonus) tableView_dealerType.getItems().get(rowvalueAfterEdit);
                    txt_policyID.setText(newobj.getApprovalids());
                    txt_prodID.setText(newobj.getProductIds());
                    txt_prodName.setText(newobj.getProductNames());
                    txt_packsize.setText(newobj.getPacksizes());
                    txt_quantity.setText(newobj.getQuantities());
                    txt_bonus.setText(newobj.getBonuses());
                    String strng_strtdate = newobj.getStartdates();
                    String strng_enddate = newobj.getEnddates();

                }
            });
        }

        @Override
        protected void updateItem(Boolean n, boolean boln) {
            super.updateItem(n, boln);
            if (boln) {
                setGraphic(null);
            } else {
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setGraphic(btncell);
            }
        }
    }

    private class ButtonCell2 extends TableCell<Bonus, Boolean> {

        final Button CellButton = new Button("Delete");

        public ButtonCell2(final TableView view) {
            CellButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    int selectedIndex = getTableRow().getIndex();
                    data_dealerType.remove(selectedIndex);

                }
            });
        }

        @Override
        protected void updateItem(Boolean t, boolean bln) {
            super.updateItem(t, bln);
            if (bln) {
                setGraphic(null);
            } else {
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setGraphic(CellButton);
            }

        }
    }

    ArrayList<String> product_Details = new ArrayList<>();
    int alreadyentered = 0;
    String vrfyPolicyID="";
    public void enterpress_aprovalID(KeyEvent event) throws IOException
    {
        if (event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB) {
            if(txt_policyID.getText().isEmpty())
            { }
            else
            {
                MysqlCon newobjMysqlConnc = new MysqlCon();
                Statement objectStattmt = newobjMysqlConnc.stmt;
                Connection objectConnnec = newobjMysqlConnc.con;
                ResultSet rs3 = null;
                try {
                    rs3 = objectStattmt.executeQuery("select approval_id from bonus_policy ");
                    while (rs3.next()) {
                        vrfyPolicyID = (rs3.getString(1));
                    }


                    objectConnnec.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                if(txt_policyID.getText().equals(vrfyPolicyID))
                {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setHeaderText("ERROR!");
                    a.setContentText("Approval ID already saved. Please enter another ID.");
                    a.show();
                    txt_policyID.requestFocus();
                }
                else
                {
                    txt_prodID.requestFocus();
                }
                int count_tableROWS = tableView_dealerType.getItems().size();

                String[] gettableview_approvID = new String[count_tableROWS];
                for(int i =0;i<count_tableROWS;i++) //////////////////////////////////////START OF LOOP TO READ ALL ROWS OF TABLVIEW
                {
                    if(txt_policyID.getText().equals(gettableview_approvID[i]))
                    {
                        a.setAlertType(Alert.AlertType.ERROR);
                        a.setHeaderText("ERROR!");
                        a.setContentText("Approval ID already exist in Table. Please enter another ID.");
                        a.show();
                        txt_policyID.requestFocus();
                    }
                    else
                    {
                        txt_prodID.requestFocus();
                    }
                }

            }
        }
    }

    public void enterpressdealerID(KeyEvent event) throws IOException, ParseException {
        if (event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB) {
            if (!txt_dealerID.getText().isEmpty()) {
                tableView_dealerType.refresh();

                for (int i = 0; i < tableView_dealerType.getItems().size(); i++) {
                    Product newwobj = (Product) tableView_dealerType.getItems().get(i);
                    if (newwobj.getProductId().equals(txt_dealerID.getText())) {
                        alreadyentered = 1;
                    }
                }
                if (alreadyentered == 0) {
                    String dealerid = txt_dealerID.getText();
                    int intdealer_id = Integer.parseInt(dealerid);
                    int i = 0;
                    MysqlCon objMysqlCon1 = new MysqlCon();
                    Statement objStmt1 = objMysqlCon1.stmt;
                    Connection objCon1 = objMysqlCon1.con;
                    ResultSet rs = null;
                    try {
                        rs = objStmt1.executeQuery("select `dealer_name`  from `dealer_info`   where  dealer_ID = '" + intdealer_id + "' and dealer_status = 'Active' ");

                        while (rs.next()) {
                            i++;
                            txt_dealerName.setText(rs.getString(1));
                        }
                        if(i==0)
                        {
                            a.setAlertType(Alert.AlertType.ERROR);
                            a.setHeaderText("ERROR!");
                            a.setContentText("Dealer ID not Found.");
                            a.show();
                            txt_dealerID.requestFocus();
                        }
                        else
                        {
                            txt_quantity.requestFocus();
                        }

                        objCon1.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    if(i!=0)
                    {
                        txt_quantity.requestFocus();
                    }

                } else {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setHeaderText("ERROR!");
                    a.setContentText("Dealer ID Already Entered in Table.");
                    a.show();
                    txt_dealerID.requestFocus();
                    alreadyentered = 0;
                }
            } else {
                a.setAlertType(Alert.AlertType.ERROR);
                a.setHeaderText("Error!");
                a.setContentText("Dealer ID not entered.");
                a.show();
                txt_dealerID.requestFocus();
            }
        }
    }

    public void selectSpecificDealer() throws IOException
    {
        if(tableView_dealerType.getItems().isEmpty())
        {
            if(chkbox_specficDealer.isSelected())
            {
                txt_dealerID.setVisible(true);
                txt_dealerName.setVisible(true);
                tableView_dealerType.setVisible(false);
                tableView_dealerName.setVisible(true);
                chkbox_Dealer_type.setSelected(false);
                combo_dealertype.setVisible(false);
            }
            else
            {
                txt_dealerID.setVisible(false);
                txt_dealerName.setVisible(false);
                chkbox_Dealer_type.setSelected(true);
                combo_dealertype.setVisible(true);
                tableView_dealerType.setVisible(true);
                tableView_dealerName.setVisible(false);

            }
        }
        else
        {
            a.setAlertType(Alert.AlertType.ERROR);
            a.setHeaderText("ERROR!");
            a.setContentText("First Save Current Policy.");
            a.show();
            chkbox_specficDealer.setSelected(false);
            txt_prodID.requestFocus();
        }
    }

    public void enterpress_specificDealer(KeyEvent event) throws IOException
    {
        if(tableView_dealerType.getItems().isEmpty())
        {
            if(chkbox_specficDealer.isSelected())
            {
                txt_dealerID.setVisible(true);
                txt_dealerName.setVisible(true);
                tableView_dealerType.setVisible(false);
                tableView_dealerName.setVisible(true);
                chkbox_Dealer_type.setSelected(false);
                combo_dealertype.setVisible(false);
            }
            else
            {
                txt_dealerID.setVisible(false);
                txt_dealerName.setVisible(false);
                chkbox_Dealer_type.setSelected(true);
                combo_dealertype.setVisible(true);
                tableView_dealerType.setVisible(true);
                tableView_dealerName.setVisible(false);
            }
        }
        else
        {
            a.setAlertType(Alert.AlertType.ERROR);
            a.setHeaderText("ERROR!");
            a.setContentText("First Save Current Policy.");
            a.show();
            chkbox_specficDealer.setSelected(false);
            txt_prodID.requestFocus();
        }
    }

    public void selectDealerType() throws IOException
    {
        if(tableView_dealerName.getItems().isEmpty())
        {
            if(chkbox_Dealer_type.isSelected())
            {
                txt_dealerID.setVisible(false);
                txt_dealerName.setVisible(false);
                tableView_dealerType.setVisible(true);
                tableView_dealerName.setVisible(false);
                combo_dealertype.setVisible(true);
                chkbox_specficDealer.setSelected(false);
            }
            else
            {
                combo_dealertype.setVisible(false);
                txt_dealerID.setVisible(true);
                txt_dealerName.setVisible(true);
                chkbox_specficDealer.setSelected(true);
                tableView_dealerType.setVisible(false);
                tableView_dealerName.setVisible(true);
            }
        }
        else
        {
            a.setAlertType(Alert.AlertType.ERROR);
            a.setHeaderText("ERROR!");
            a.setContentText("First Save Current Policy.");
            a.show();
            chkbox_Dealer_type.setSelected(false);
            txt_prodID.requestFocus();
        }
    }

    public void enterpress_DealerType(KeyEvent event) throws IOException
    {
        if(tableView_dealerName.getItems().isEmpty())
        {
            if(chkbox_Dealer_type.isSelected())
            {
                txt_dealerID.setVisible(false);
                txt_dealerName.setVisible(false);
                tableView_dealerType.setVisible(true);
                tableView_dealerName.setVisible(false);
                combo_dealertype.setVisible(true);
                chkbox_specficDealer.setSelected(false);
            }
            else
            {
                combo_dealertype.setVisible(false);
                txt_dealerID.setVisible(true);
                txt_dealerName.setVisible(true);
                chkbox_specficDealer.setSelected(true);
                tableView_dealerType.setVisible(false);
                tableView_dealerName.setVisible(true);

            }
        }
        else
        {
            a.setAlertType(Alert.AlertType.ERROR);
            a.setHeaderText("ERROR!");
            a.setContentText("First Save Current Policy.");
            a.show();
            chkbox_Dealer_type.setSelected(false);
            txt_prodID.requestFocus();
        }
    }

    public void enterpress_prodID(KeyEvent event) throws IOException, ParseException {
        if (event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB) {
            if (!txt_prodID.getText().equals("")) {
                tableView_dealerType.refresh();

                for (int i = 0; i < tableView_dealerType.getItems().size(); i++) {
                    Product newwobj = (Product) tableView_dealerType.getItems().get(i);
                    if (newwobj.getProductId().equals(txt_prodID.getText())) {
                        alreadyentered = 1;
                    }
                }
                if (alreadyentered == 0) {
                    String product_ids = txt_prodID.getText();
                    int intprod_id = Integer.parseInt(product_ids);

                    MysqlCon objMysqlCon1 = new MysqlCon();
                    Statement objStmt1 = objMysqlCon1.stmt;
                    Connection objCon1 = objMysqlCon1.con;
                    ResultSet rs = null;
                    try {
                        rs = objStmt1.executeQuery("select `product_name` ,`packSize` from `product_info`   where  product_id = '" + intprod_id + "'  ");
                        int i = 0;
                        while (rs.next()) {
                            txt_prodName.setText(rs.getString(1));
                            txt_packsize.setText(rs.getString(2));
                        }

                        objCon1.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    if(txt_prodName.getText().isEmpty())
                    {
                        a.setAlertType(Alert.AlertType.ERROR);
                        a.setHeaderText("ERROR!");
                        a.setContentText("Product not Found.");
                        a.show();
                        txt_prodID.requestFocus();
                    }
                    else
                    {
                        combo_dealertype.requestFocus();
                    }

                } else {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setHeaderText("ERROR!");
                    a.setContentText("Product Already Entered in Invoice.");
                    a.show();
                    combo_dealertype.requestFocus();
                    alreadyentered = 0;
                }
            }
            else {
                a.setAlertType(Alert.AlertType.ERROR);
                a.setHeaderText("Error!");
                a.setContentText("Product ID not entered.");
                a.show();
                txt_prodID.requestFocus();
            }
        }
    }

    /////////////////////////////////////////////////////////////////////////// START of ENter Press on Button Add
    public void btnadd_enterpress(KeyEvent event) throws IOException {
        if (event.getCode() == KeyCode.ENTER) {
            if (txt_prodName.getText().isEmpty()) {
                txt_prodName.setText("ERROR");
            } else if (txt_prodID.getText().isEmpty()) {
                txt_prodID.setText("ERROR");
            } else {

                Bonus bon_obj = new Bonus();
                bon_obj.setApprovalids(txt_policyID.getText());
                bon_obj.setProductIds(txt_prodID.getText());
                bon_obj.setProductNames(txt_prodName.getText());
                bon_obj.setPacksizes(txt_packsize.getText());
                if(chkbox_specficDealer.isSelected())
                {
                    bon_obj.setDealerID(txt_dealerID.getText());
                    bon_obj.setDealerName(txt_dealerName.getText());
                    bon_obj.setBonusfor("null");
                }
                if(chkbox_Dealer_type.isSelected())
                {
                    bon_obj.setDealerID("0");
                    bon_obj.setDealerName("null");
                    bon_obj.setBonusfor(combo_dealertype.getValue());
                }
                bon_obj.setQuantities(txt_quantity.getText());
                bon_obj.setBonuses(txt_bonus.getText());
                //bon_obj.setStartdates(datepick_startdate.getValue().toString());
                //bon_obj.setEnddates(datepick_enddate.getValue().toString());
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MMM/yyyy");
                String startDateValue = (datepick_startdate.getValue()).format(formatter);
                bon_obj.setStartdates(startDateValue);

                String endDateValue = (datepick_enddate.getValue()).format(formatter);
                bon_obj.setEnddates(endDateValue);

                tableView_dealerType.getItems().add(bon_obj);
                txt_prodID.setText("");
                txt_prodName.setText("");
                txt_packsize.setText("");
                txt_dealerID.setText("");
                txt_dealerName.setText("");
                txt_bonus.setText("");
                txt_quantity.setText("");
                datepick_startdate.setValue(LocalDate.now());
                datepick_enddate.setValue(LocalDate.now());
                txt_prodID.requestFocus();

            }
        }

    }

    /////////////////////////////////////////////////////////////////////////// End of ENter Press on Button Add
    /////////////////////////////////////////////////////////////////////////// START of click on Button Add
    public void btnadd_clicks() throws IOException {

        Bonus bon_obj = new Bonus();
        bon_obj.setApprovalids(txt_policyID.getText());
        bon_obj.setProductIds(txt_prodID.getText());
        bon_obj.setProductNames(txt_prodName.getText());
        bon_obj.setPacksizes(txt_packsize.getText());
        if(chkbox_specficDealer.isSelected())
        {
            bon_obj.setDealerID(txt_dealerID.getText());
            bon_obj.setDealerName(txt_dealerName.getText());
            bon_obj.setBonusfor("null");
        }
        if(chkbox_Dealer_type.isSelected())
        {
            bon_obj.setDealerID("0");
            bon_obj.setDealerName("null");
            bon_obj.setBonusfor(combo_dealertype.getValue());
        }
        bon_obj.setQuantities(txt_quantity.getText());
        bon_obj.setBonuses(txt_bonus.getText());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MMM/yyyy");
        String startDateValue = (datepick_startdate.getValue()).format(formatter);
        bon_obj.setStartdates(startDateValue);
        String endDateValue = (datepick_enddate.getValue()).format(formatter);
        bon_obj.setEnddates(endDateValue);

        tableView_dealerName.getItems().add(bon_obj);
        txt_prodID.setText("");
        txt_prodName.setText("");
        txt_packsize.setText("");
        txt_dealerID.setText("");
        txt_dealerName.setText("");
        txt_bonus.setText("");
        txt_quantity.setText("");
        datepick_startdate.setValue(LocalDate.now());
        datepick_enddate.setValue(LocalDate.now());

        txt_prodID.requestFocus();
    }
    /////////////////////////////////////////////////////////////////////////// End of click on Button Add

    public void follow() throws IOException {
        if (drawer.isOpened()) {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(-250);
            transition.setByY(0);
            transition.setCycleCount(1);
            transition.play();
            drawer.close();
            FA_Icon.setGlyphName("ANGLE_DOUBLE_RIGHT");
            FA_Icon.setGlyphSize(14);
            //inner_anchor.setRightAnchor(inner_anchor,1d);


            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(-90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration, inner_anchor);
            scale.setToX(1.28000);
            scale.setToY(1);


            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale, anchorePaneTransition, transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();
            //
            //inner_anchor.setLeftAnchor(inner_anchor, 5d);
        } else {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(250);
            transition.setByY(0);
            transition.setCycleCount(1);
            FA_Icon.setGlyphName("ANGLE_DOUBLE_LEFT");
            FA_Icon.setGlyphSize(14);
            drawer.open();

            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration, inner_anchor);
            scale.setToX(0.99947777);
            scale.setToY(1);


            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale, anchorePaneTransition, transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();

        }


    }

    public boolean activ_notactiv(String st_date,String endDate,String curr_date) throws ParseException {
        DateFormat format = new SimpleDateFormat("dd/MMM/yyyy", Locale.ENGLISH);
        Date Sdate = format.parse(st_date);
        Date Edate = format.parse(endDate);
        Date Currdate = format.parse(curr_date);


        return Sdate.compareTo(Currdate) * Currdate.compareTo(Edate) >= 0;
    }
    public void save_policy() throws IOException, ParseException {
        int counttableROWS = 0;
        if(chkbox_specficDealer.isSelected())
        {
            counttableROWS = tableView_dealerName.getItems().size();
        }
        if(chkbox_Dealer_type.isSelected())
        {
            counttableROWS = tableView_dealerType.getItems().size();
        }

        String[] getapprovID_tableview = new String[counttableROWS];
        String[] getprodid_tableview = new String[counttableROWS];
        String[] getprodName_tableview = new String[counttableROWS];
        String[] getpacksize_tableview = new String[counttableROWS];
        String[] getbonusFor_tableview = new String[counttableROWS];
        String[] getdealerID_tableview = new String[counttableROWS];
        String[] getdealerName_tableview = new String[counttableROWS];
        String[] getprodQuant_tableview = new String[counttableROWS];
        String[] getprodBonus_tableview = new String[counttableROWS];
        String[] getstartDate_tableview = new String[counttableROWS];
        String[] getendDate_tableview = new String[counttableROWS];

        String verifypolicyid="";
        for(int i =0;i<counttableROWS;i++) //////////////////////////////////////START OF LOOP TO READ ALL ROWS OF TABLVIEW
        {
            Bonus bonus_Object = new Bonus();
            if(chkbox_specficDealer.isSelected())
            {
                bonus_Object = (Bonus) tableView_dealerName.getItems().get(i);
            }
            if(chkbox_Dealer_type.isSelected())
            {
                bonus_Object = (Bonus) tableView_dealerType.getItems().get(i);
            }

                getapprovID_tableview[i] = bonus_Object.getApprovalids();
                    getprodid_tableview[i] = bonus_Object.getProductIds();
                    getprodName_tableview[i] = bonus_Object.getProductNames();
                    getpacksize_tableview[i] = bonus_Object.getPacksizes();
                    getbonusFor_tableview[i] = bonus_Object.getBonusfor();
                    getdealerID_tableview[i] = bonus_Object.getDealerID();
                    getdealerName_tableview[i] = bonus_Object.getDealerName();
                    getprodQuant_tableview[i] = bonus_Object.getQuantities();
                    getprodBonus_tableview[i] = bonus_Object.getBonuses();
                    getstartDate_tableview[i] = bonus_Object.getStartdates();
                    getendDate_tableview[i] = bonus_Object.getEnddates();

                    String getuserID = GlobalVariables.getUserId();
                    String getcurrDate = GlobalVariables.getStDate();
                    String getcurrTime = GlobalVariables.getStTime();
                    String policy_Status = "";
                    boolean activ_or_not = activ_notactiv(getstartDate_tableview[i],getendDate_tableview[i],getcurrDate);
                    if(activ_or_not == true)
                    {
                        policy_Status= "Active";
                    }
                    else
                    {
                        policy_Status = "In Active";
                    }
            MysqlCon objMysqlConnct = new MysqlCon();
            Statement objctStatmt = objMysqlConnct.stmt;
            Connection objctConnc = objMysqlConnct.con;

            ResultSet rs = null;
            ResultSet rs1 = null;
            ResultSet rs2 = null;
            String dealertableID = "";
            String producttableID ="";
            String companytableID = "";
            try {
                rs = objctStatmt.executeQuery("SELECT dealer_table_id  FROM `dealer_info` WHERE dealer_id = '"+getdealerID_tableview[i]+"' ");
                while (rs.next())
                {
                     dealertableID = rs.getString(1);
                }
                rs1 = objctStatmt.executeQuery("SELECT product_table_id  FROM `product_info` WHERE product_id = '"+getprodid_tableview[i]+"' ");
                while (rs1.next()) {
                     producttableID = rs1.getString(1);
                }
                rs2 = objctStatmt.executeQuery("SELECT company_table_id  FROM `product_info` WHERE product_id = '"+getprodid_tableview[i]+"' ");
                while (rs2.next())
                {
                     companytableID = rs2.getString(1);
                }
//            objctConnc.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

                    MysqlCon objMysqlCon = new MysqlCon();
                    Statement objctStmt = objMysqlCon.stmt;
                    Connection objctCon = objMysqlCon.con;
                    try {
                        String insertQuery = "INSERT INTO `bonus_policy`( `approval_id`, `product_table_id`,  `dealer_table_id`,  `quantity`, `bonus_quant`, `start_date`, `end_date`,`policy_status`, `entered_by`, `entry_date`, `entry_time`, `updated_by`, `update_date`, `update_time`) VALUES ('" + getapprovID_tableview[i] + "','" + producttableID + "','" + dealertableID + "','" + getprodQuant_tableview[i] + "','" + getprodBonus_tableview[i] + "','" + getstartDate_tableview[i] + "','" + getendDate_tableview[i] + "','"+policy_Status+"','" + getuserID + "','" + getcurrDate + "','" + getcurrTime + "','" + getuserID + "','" + getcurrDate + "','" + getcurrTime + "')";
                        objctStmt.executeUpdate(insertQuery);
                        objctCon.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    txt_policyID.setText("");
                    txt_quantity.setText("");
                    txt_packsize.setText("");
                    txt_prodName.setText("");
                    txt_prodID.setText("");
                    txt_bonus.setText("");
                    datepick_startdate.setValue(LocalDate.now());
                    datepick_enddate.setValue(LocalDate.now());
                    txt_policyID.requestFocus();
                for ( int f = 0; f<tableView_dealerType.getItems().size(); f++) {
                    tableView_dealerType.getItems().clear();
                }
            for ( int f = 0; f<tableView_dealerName.getItems().size(); f++) {
                tableView_dealerName.getItems().clear();
            }

            }


    }
}
