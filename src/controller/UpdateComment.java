package controller;

import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;
import model.*;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ResourceBundle;

public class UpdateComment implements Initializable {

    @FXML
    private Label label_update;

    @FXML
    private JFXButton dialog_comment_close;

    @FXML
    private JFXButton dialog_comment_update;

    @FXML
    private TextArea txt_comment;

    public static String commentType;
    public static String orderId;
    public static String comment;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        txt_comment.setText(comment);
        dialog_comment_close.setOnAction((action)->closeDialog());
        dialog_comment_update.setOnAction((action)->updateCompany());
    }

    public void closeDialog()
    {
        if(commentType.equals("Sales"))
        {
            ViewSalesInfo.dialog.close();
            ViewSalesInfo.stackPane.setVisible(false);
        }
        else if(commentType.equals("Purchase"))
        {
            ViewPurchaseInfo.dialog.close();
            ViewPurchaseInfo.stackPane.setVisible(false);
        }
        else if(commentType.equals("Dealer Finance"))
        {
            DealerFinanceDetailReportInfo.dialog.close();
            DealerFinanceDetailReportInfo.stackPane.setVisible(false);
        }
        else if(commentType.equals("Company Finance"))
        {
            CompanyFinanceDetailReportInfo.dialog.close();
            CompanyFinanceDetailReportInfo.stackPane.setVisible(false);
        }
    }

    public void updateCompany() {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        String comment = txt_comment.getText();
        Parent root = null;
        if(commentType.equals("Sales"))
        {
            ViewSalesInfo objViewSalesInfo = new ViewSalesInfo();
            objViewSalesInfo.updateComment(objStmt, objCon, orderId, comment);

            try {
                root = FXMLLoader.load(getClass().getResource("../view/view_sales.fxml"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if(commentType.equals("Purchase"))
        {
            ViewPurchaseInfo objViewPurchaseInfo = new ViewPurchaseInfo();
            objViewPurchaseInfo.updateComment(objStmt, objCon, orderId, comment);
            root = null;
            try {
                root = FXMLLoader.load(getClass().getResource("../view/view_purchase.fxml"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if(commentType.equals("Dealer Finance"))
        {
            DealerFinanceDetailReportInfo objDealerFinanceDetailReportInfo = new DealerFinanceDetailReportInfo();
            objDealerFinanceDetailReportInfo.updateComment(objStmt, objCon, orderId, comment);
            root = null;
            try {
                root = FXMLLoader.load(getClass().getResource("../view/dealer_finance_report.fxml"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if(commentType.equals("Company Finance"))
        {
            CompanyFinanceDetailReportInfo objCompanyFinanceDetailReportInfo = new CompanyFinanceDetailReportInfo();
            objCompanyFinanceDetailReportInfo.updateComment(objStmt, objCon, orderId, comment);
            root = null;
            try {
                root = FXMLLoader.load(getClass().getResource("../view/company_finance_report.fxml"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
}
