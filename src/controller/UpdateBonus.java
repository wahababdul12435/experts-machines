package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import model.*;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;

public class UpdateBonus implements Initializable {
    @FXML    private Label label_update;
    @FXML    private JFXTextField txt_approval_id;
    @FXML    private JFXTextField txt_company_id;
    @FXML    private JFXTextField txt_company_Name;
    @FXML    private JFXTextField txt_dealerID;
    @FXML    private JFXTextField txt_dealerName;
    @FXML    private JFXTextField txt_productID;
    @FXML    private JFXTextField txt_productName;
    @FXML    private JFXTextField txt_quantity;
    @FXML    private JFXTextField txt_bonusQuantity;
    @FXML    private DatePicker datepick_startDate;
    @FXML    private DatePicker datepick_endDate;
    @FXML    private JFXComboBox<String> policyStatus;
    @FXML    private JFXButton dialog_company_close;
    @FXML    private JFXButton dialog_company_update;

    public static String bonusapprovalID ="";
    Alert a = new Alert(Alert.AlertType.NONE);

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ViewCompanyInfo.lblUpdate = label_update;
        int bonusapprovedID = Integer.parseInt(bonusapprovalID);

        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        ViewBonusInfo updatebonusinfo = new ViewBonusInfo();
        String policy_data = updatebonusinfo.get_bonuspolicy(objStmt, objCon, bonusapprovedID);

        String[] splitbonusData = policy_data.split("--");

        txt_company_id.setText(splitbonusData[8]);
        txt_company_Name.setText(splitbonusData[9]);
        txt_approval_id.setText(splitbonusData[0]);
        txt_quantity.setText(splitbonusData[1]);
        txt_bonusQuantity.setText(splitbonusData[2]);
        txt_dealerID.setText(splitbonusData[6]);
        txt_dealerName.setText(splitbonusData[7]);
        txt_productID.setText(splitbonusData[10]);
        txt_productName.setText(splitbonusData[11]);
        datepick_startDate.setValue(LOCAL_DATE(splitbonusData[3]));
        datepick_endDate.setValue(LOCAL_DATE(splitbonusData[4]));
        policyStatus.setValue(splitbonusData[5]);

        dialog_company_close.setOnAction((action)->closeDialog());
        dialog_company_update.setOnAction((action)->updateBonus());
    }
    public static final LocalDate LOCAL_DATE (String dateString) {
        LocalDate convertedDates = null;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String[] splitStartDate = dateString.split("/");
        Date date = null;
        try {
            date = new SimpleDateFormat("MMMM").parse(splitStartDate[1]);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int im = cal.get(Calendar.MONTH);
        im =+1;
        if(im<=9) {
            splitStartDate[1] = "0" + im;
        }
        String strtdate_value = splitStartDate[2]+"-"+splitStartDate[1]+"-"+splitStartDate[0];
        convertedDates = LocalDate.parse(strtdate_value, formatter);
        return convertedDates;
    }
    public void closeDialog()
    {
        ViewBonusInfo.dialog.close();
        ViewBonusInfo.stackPane.setVisible(false);
    }
    String companydetails_onIDEnter = "";
    public void enterpress_companyID(KeyEvent event) throws IOException, ParseException {
        if (event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB) {
            if(!txt_company_id.getText().equals(""))
            {
                int intdealer_id = Integer.parseInt(txt_company_id.getText());
                MysqlCon objMysqlCon = new MysqlCon();
                Statement objStmt = objMysqlCon.stmt;
                Connection objCon = objMysqlCon.con;
                ViewBonusInfo objectBonus = new ViewBonusInfo();
                companydetails_onIDEnter = objectBonus.get_dealerDetailswithIDs(objStmt, objCon, intdealer_id);
                if (companydetails_onIDEnter.equals("")) {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setHeaderText("Error!");
                    a.setContentText("Dealer ID not found.");
                    a.show();
                    txt_dealerID.requestFocus();
                    txt_dealerName.setText("");
                }
                else {
                    txt_company_Name.setText(companydetails_onIDEnter);

                }
            }
        }
    }
    String dealerdetails_onIDEnter = "";
    public void enterpress_dealerID(KeyEvent event) throws IOException, ParseException {
        if (event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB) {
            if(!txt_dealerID.getText().equals(""))
            {
                int intdealer_id = Integer.parseInt(txt_dealerID.getText());
                MysqlCon objMysqlCon = new MysqlCon();
                Statement objStmt = objMysqlCon.stmt;
                Connection objCon = objMysqlCon.con;
                ViewBonusInfo objectBonus = new ViewBonusInfo();
                dealerdetails_onIDEnter = objectBonus.get_dealerDetailswithIDs(objStmt, objCon, intdealer_id);
                if (dealerdetails_onIDEnter.equals("")) {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setHeaderText("Error!");
                    a.setContentText("Dealer ID not found.");
                    a.show();
                    txt_dealerID.requestFocus();
                    txt_dealerName.setText("");
                }
                else {
                    txt_dealerName.setText(dealerdetails_onIDEnter);

                }
            }
        }
    }
    String Productdetails_onIDEnter ="";
    public void enterpress_prodID(KeyEvent event) throws IOException, ParseException {
        if (event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB) {
            if(!txt_productID.getText().equals(""))
            {
                int intprod_id = Integer.parseInt(txt_productID.getText());
                MysqlCon objMysqlCon = new MysqlCon();
                Statement objStmt = objMysqlCon.stmt;
                Connection objCon = objMysqlCon.con;
                ViewBonusInfo object_bonus = new ViewBonusInfo();
                Productdetails_onIDEnter = object_bonus.get_productsDetailswithIDs(objStmt, objCon, intprod_id);
                if (Productdetails_onIDEnter == null) {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setHeaderText("Error!");
                    a.setContentText("Dealer ID not found.");
                    a.show();
                    txt_productID.requestFocus();
                    txt_productName.setText("");
                }
                else {
                    txt_productName.setText(Productdetails_onIDEnter);

                }
            }
        }
    }

    public void updateBonus() {

        int bonusapprovedID = Integer.parseInt(bonusapprovalID);

        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        ViewBonusInfo updatebonusinfo = new ViewBonusInfo();
        //updatebonusinfo.updatebonusdetails(objStmt, objCon, bonusapprovedID);

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/view_bonus.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
}
