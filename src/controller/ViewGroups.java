package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.util.Duration;
import model.*;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class ViewGroups implements Initializable {

    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private TableView<ViewGroupsInfo> table_viewgroup;

    @FXML
    private TableColumn<ViewGroupsInfo, String> sr_no;

    @FXML
    private TableColumn<ViewGroupsInfo, String> group_id;

    @FXML
    private TableColumn<ViewGroupsInfo, String> company_name;

    @FXML
    private TableColumn<ViewGroupsInfo, String> group_name;

    @FXML
    private TableColumn<ViewGroupsInfo, String> group_products;

    @FXML
    private TableColumn<ViewGroupsInfo, String> group_status;

    @FXML
    private TableColumn<ViewGroupsInfo, String> operations;

    @FXML
    private StackPane stackPane;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private BorderPane menu_bar1;

    @FXML
    private BorderPane nav_setup;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    @FXML
    private AnchorPane filter_pane;

    @FXML
    private HBox filter_hbox;

    @FXML
    private JFXTextField txt_group_id;

    @FXML
    private JFXComboBox<String> txt_company_name;

    @FXML
    private JFXTextField txt_group_name;

    @FXML
    private JFXComboBox<String> txt_group_status;

    @FXML
    private HBox summary_hbox;

    @FXML
    private Label lbl_total;

    @FXML
    private Label lbl_active;

    @FXML
    private Label lbl_inactive;

    private ArrayList<String> companyIds = new ArrayList<>();
    private ArrayList<String> companyNames = new ArrayList<>();

    public static String txtGroupId = "";
    public static String txtGroupName = "";
    public static String txtCompanyName = "";
    public static String txtCompanyId = "";
    public static String txtGroupStatus = "";
    public static boolean filter;

    public ObservableList<ViewGroupsInfo> groupDetails;
    PendingOrders objPendingOrders;
    MysqlCon objMysqlCon;
    Statement objStmt;
    Connection objCon;
    private int summaryTotal;
    private int summaryActive;
    private int summaryInActive;
    public static Button btnView;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnSetup.setStyle("-fx-background-color: #47ab1e");
        objMysqlCon = new MysqlCon();
        objStmt = objMysqlCon.stmt;
        objCon = objMysqlCon.con;
        ViewGroupsInfo.stackPane = stackPane;
        GroupInfo objGroupInfo = new GroupInfo();
        companyIds = objGroupInfo.getSavedCompanyIds();
        companyNames = objGroupInfo.getSavedCompanyNames();
        txt_company_name.getItems().addAll(companyNames);

        btnpane.setOnMouseEntered(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(22);
                btnpane.setMinWidth(20.0);
                btn11.setMinWidth(20.0);

            }
        });
        btnpane.setOnMouseExited(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(14);
                btnpane.setMinWidth(10.0);
                btn11.setMinWidth(10.0);
            }
        });

        inner_anchor.setLeftAnchor(inner_anchor, 280d);
        drawer.setSidePane(nav_setup);
        drawer.open();

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        group_id.setCellValueFactory(new PropertyValueFactory<>("groupId"));
        company_name.setCellValueFactory(new PropertyValueFactory<>("companyName"));
        group_name.setCellValueFactory(new PropertyValueFactory<>("groupName"));
        group_products.setCellValueFactory(new PropertyValueFactory<>("products"));
        group_status.setCellValueFactory(new PropertyValueFactory<>("groupStatus"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_viewgroup.setItems(parseUserList());
        lbl_total.setText("Groups\n"+summaryTotal);
        lbl_active.setText("Active\n"+summaryActive);
        lbl_inactive.setText("In Active\n"+summaryInActive);
    }

    private ObservableList<ViewGroupsInfo> parseUserList(){
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        ViewGroupsInfo objViewGroupsInfo = new ViewGroupsInfo();
        groupDetails = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> groupData;
        if(filter)
        {
            groupData  = objViewGroupsInfo.getGroupsSearch(objStmt, objCon, txtGroupId, txtGroupName, txtCompanyId, txtGroupStatus);
            txt_group_id.setText(txtGroupId);
            txt_group_name.setText(txtGroupName);
            txt_company_name.setValue(txtCompanyName);
            txt_group_status.setValue(txtGroupStatus);
            filter = false;
            filter_pane.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
        }
        else
        {
            groupData = objViewGroupsInfo.getGroupsInfo(objStmt, objCon);
        }
        for (int i = 0; i < groupData.size(); i++)
        {
            summaryTotal++;
            if(groupData.get(i).get(6).equals("Active"))
            {
                summaryActive++;
            }
            else if(groupData.get(i).get(6).equals("In Active"))
            {
                summaryInActive++;
            }
            groupDetails.add(new ViewGroupsInfo(String.valueOf(i+1), groupData.get(i).get(0), ((groupData.get(i).get(1) == null) ? "N/A" : groupData.get(i).get(1)), groupData.get(i).get(2), ((groupData.get(i).get(3) == null) ? "N/A" : groupData.get(i).get(3)), groupData.get(i).get(4), groupData.get(i).get(5), groupData.get(i).get(6)));
        }
        return groupDetails;
    }

    @FXML
    void btnSearch(ActionEvent event) {
        txtGroupId = txt_group_id.getText();
        txtGroupName = txt_group_name.getText();
        txtCompanyName = txt_company_name.getValue();
        if(txt_company_name.getSelectionModel().getSelectedIndex() >= 0)
        {
            txtCompanyId = String.valueOf(companyIds.get(txt_company_name.getSelectionModel().getSelectedIndex()));
        }
        txtGroupStatus = txt_group_status.getValue();
        if(txtGroupStatus == null)
        {
            txtGroupStatus = "All";
        }
        filter = true;

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/view_groups.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void btnReset(ActionEvent event) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/view_groups.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void exportExcel(ActionEvent event) throws IOException {
        Workbook workbook = new HSSFWorkbook();
        Sheet spreadsheet = workbook.createSheet("data");
        Row row = spreadsheet.createRow(0);

        for (int j = 0; j < table_viewgroup.getColumns().size()-1; j++) {
            row.createCell(j).setCellValue(table_viewgroup.getColumns().get(j).getText());
        }

        for (int i = 0; i < table_viewgroup.getItems().size(); i++) {
            row = spreadsheet.createRow(i + 1);
            for (int j = 0; j < table_viewgroup.getColumns().size()-1; j++) {
                if(table_viewgroup.getColumns().get(j).getCellData(i) != null) {
                    row.createCell(j).setCellValue(table_viewgroup.getColumns().get(j).getCellData(i).toString());
                }
                else {
                    row.createCell(j).setCellValue("");
                }
            }
        }

        FileOutputStream fileOut = new FileOutputStream("data/View_Groups.xls");
        workbook.write(fileOut);
        fileOut.close();
    }

    @FXML
    void addGroup(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/enter_group.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    public void follow() throws IOException
    {
        if (drawer.isOpened()) {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(-250);
            transition.setByY(0);
            transition.setCycleCount(1);
            transition.play();
            drawer.close();
            FA_Icon.setGlyphName("ANGLE_DOUBLE_RIGHT");
            FA_Icon.setGlyphSize(14);
            //inner_anchor.setRightAnchor(inner_anchor,1d);
            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(-90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(1.1000);
            scale.setToY(1);

            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();

        }
        else {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(250);
            transition.setByY(0);
            transition.setCycleCount(1);
            FA_Icon.setGlyphName("ANGLE_DOUBLE_LEFT");
            FA_Icon.setGlyphSize(14);
            drawer.open();

            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(0.99947777);
            scale.setToY(1);



            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();
        }
    }
}
