package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import model.*;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ResourceBundle;

public class UpdateCompany implements Initializable {
    @FXML
    private Label label_update;

    @FXML
    private JFXTextField txt_company_id;

    @FXML
    private JFXTextField txt_company_name;

    @FXML
    private JFXTextField txt_company_address;

    @FXML
    private JFXTextField txt_company_contact;

    @FXML
    private JFXTextField txt_company_email;

    @FXML
    private JFXComboBox<String> txt_company_status;

    @FXML
    private JFXButton dialog_company_close;

    @FXML
    private JFXButton dialog_company_update;

    public static String companyTableId ="";
    public static String companyId ="";
    public static String companyName ="";
    public static String companyAddress ="";
    public static String companyContact ="";
    public static String companyEmail ="";
    public static String companyStatus ="";

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ViewCompanyInfo.lblUpdate = label_update;
        txt_company_id.setText(companyId);
        txt_company_name.setText(companyName);
        txt_company_address.setText(companyAddress);
        txt_company_contact.setText(companyContact);
        txt_company_email.setText(companyEmail);
        txt_company_status.setValue(companyStatus);
        dialog_company_close.setOnAction((action)->closeDialog());
        dialog_company_update.setOnAction((action)->updateCompany());
    }

    public void closeDialog()
    {
        ViewCompanyInfo.dialog.close();
        ViewCompanyInfo.stackPane.setVisible(false);
    }

    public void updateCompany() {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        CompanyInfo objCompanyInfo = new CompanyInfo();
        companyId = txt_company_id.getText();
        companyName = txt_company_name.getText();
        companyAddress = txt_company_address.getText();
        companyContact = txt_company_contact.getText();
        companyEmail = txt_company_email.getText();
        companyStatus = txt_company_status.getValue();
        objCompanyInfo.updateCompany(objStmt, objCon, companyTableId, companyId, companyName, companyAddress, companyContact, companyEmail, companyStatus);

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/view_company.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
}
