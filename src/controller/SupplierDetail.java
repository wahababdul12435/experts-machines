package controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXDrawer;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;
import model.*;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

public class SupplierDetail implements Initializable {

    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private Label lbl_supplier_id;

    @FXML
    private Label lbl_supplier_name;

    @FXML
    private Label lbl_supplier_contact;

    @FXML
    private Label lbl_supplier_created;

    @FXML
    private Label lbl_supplier_updated;

    @FXML
    private Label lbl_supplier_status;

    @FXML
    private VBox summary_hbox;

    @FXML
    private Label lbl_orders_supplied;

    @FXML
    private Label lbl_orders_cost;

    @FXML
    private AnchorPane filter_pane;

    @FXML
    private JFXComboBox<String> txt_company_name;

    @FXML
    private JFXDatePicker txt_from_date;

    @FXML
    private JFXDatePicker txt_to_date;

    @FXML
    private TableView<SupplierDetailInfo> table_supplierdetails;

    @FXML
    private TableColumn<SupplierDetailInfo, String> sr_no;

    @FXML
    private TableColumn<SupplierDetailInfo, String> company_name;

    @FXML
    private TableColumn<SupplierDetailInfo, String> invoice_id;

    @FXML
    private TableColumn<SupplierDetailInfo, String> invoice_cost;

    @FXML
    private TableColumn<SupplierDetailInfo, String> supplied_date;

    @FXML
    private TableColumn<SupplierDetailInfo, String> operations;

    @FXML
    private StackPane stackPane;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    @FXML
    private BorderPane brdrpane_navfinances;

    @FXML
    private BorderPane menu_bar;

    private SupplierInfo objSupplierInfo;
    private SupplierDetailInfo objSupplierDetailInfo;
    private String currentSupplierId;
    private ArrayList<String> supplierInfo;
    private ObservableList<SupplierDetailInfo> supplierDetails;
    private ArrayList<ArrayList<String>> companiesData;

    private static String txtCompanyName = "All";
    private static String txtCompanyId = "All";
    private static String txtFromDate = "";
    private static String txtToDate = "";

    private int totalOrders = 0;
    private float totalOrdersCost = 0;

    public static boolean filter;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        if(!ViewSupplierInfo.viewSupplierId.equals(""))
        {
            currentSupplierId = ViewSupplierInfo.viewSupplierId;
            ViewSupplierInfo.viewSupplierId = "";
        }
        objSupplierDetailInfo = new SupplierDetailInfo();
        objSupplierInfo = new SupplierInfo();
        supplierInfo = objSupplierInfo.getSupplierDetail(currentSupplierId);
        lbl_supplier_id.setText(((supplierInfo.get(0) == null) ? "N/A" : supplierInfo.get(0)));
        lbl_supplier_name.setText(supplierInfo.get(1));
        lbl_supplier_contact.setText(((supplierInfo.get(2) == null) ? "N/A" : supplierInfo.get(2)));
        lbl_supplier_created.setText((supplierInfo.get(3) == null) ? "N/A" : supplierInfo.get(3));
        lbl_supplier_updated.setText((supplierInfo.get(4) == null) ? "N/A" : supplierInfo.get(4));
        lbl_supplier_status.setText(supplierInfo.get(5));

        companiesData = objSupplierDetailInfo.getCompaniesData();
        ArrayList<String> chosenCompanyName = getArrayColumn(companiesData, 1);
        txt_company_name.getItems().addAll(chosenCompanyName);

        txt_from_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        txt_to_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        company_name.setCellValueFactory(new PropertyValueFactory<>("companyName"));
        invoice_id.setCellValueFactory(new PropertyValueFactory<>("invoiceNo"));
        invoice_cost.setCellValueFactory(new PropertyValueFactory<>("invoiceCost"));
        supplied_date.setCellValueFactory(new PropertyValueFactory<>("suppliedDate"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_supplierdetails.setItems(parseUserList());
        lbl_orders_supplied.setText("Invoices Supplied\n"+totalOrders);
        lbl_orders_cost.setText("Total Invoices Cost\nRs. "+String.format("%,.0f", totalOrdersCost)+"/-");
    }

    private ObservableList<SupplierDetailInfo> parseUserList() {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        supplierDetails = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> supplierData;

        if(filter)
        {
            supplierData = objSupplierDetailInfo.getSupplierOrdersSearch(objStmt, objCon, currentSupplierId, txtCompanyId, txtFromDate, txtToDate);
            txt_company_name.setValue(txtCompanyName);
            if(txtFromDate != null && !txtFromDate.equals(""))
            {
                txt_from_date.setValue(GlobalVariables.LOCAL_DATE(txtFromDate));
            }
            if(txtToDate != null && !txtToDate.equals(""))
            {
                txt_to_date.setValue(GlobalVariables.LOCAL_DATE(txtToDate));
            }
            filter = false;
            filter_pane.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
        }
        else
        {
            supplierData = objSupplierDetailInfo.getSupplierOrders(objStmt, objCon, currentSupplierId);
        }

        for (int i = 0; i < supplierData.size(); i++)
        {
            totalOrders++;
            if(supplierData.get(i).get(2) != null && !supplierData.get(i).get(2).equals(""))
            {
                totalOrdersCost += Float.parseFloat(supplierData.get(i).get(2));
            }
            supplierDetails.add(new SupplierDetailInfo(String.valueOf(i+1), supplierData.get(i).get(0), supplierData.get(i).get(1), supplierData.get(i).get(2), supplierData.get(i).get(3)));
        }
        return supplierDetails;
    }

    @FXML
    void btnReset(ActionEvent event) {
        ViewSupplierInfo.viewSupplierId = currentSupplierId;
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/supplier_detail.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void btnSearch(ActionEvent event) {
        txtCompanyName = txt_company_name.getValue();
        if(txtCompanyName == null)
        {
            txtCompanyName = "All";
            txtCompanyId = "All";
        }
        else
        {
            txtCompanyId = getCompanyId(companiesData, txt_company_name.getValue());
        }

        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date selectedDate = null;
        if(txt_from_date.getValue() != null)
        {
            txtFromDate = txt_from_date.getValue().toString();
            try {
                selectedDate = sdf.parse(txtFromDate);
                txtFromDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if(txt_to_date.getValue() != null)
        {
            txtToDate = txt_to_date.getValue().toString();
            try {
                selectedDate = sdf.parse(txtToDate);
                txtToDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        filter = true;

        ViewSupplierInfo.viewSupplierId = currentSupplierId;
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/supplier_detail.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void follow(MouseEvent event) {

    }

    public ArrayList<String> getArrayColumn(ArrayList<ArrayList<String>> array, int columnIndex)
    {
        ArrayList<String> columnList = new ArrayList<>();
        for(ArrayList<String> row: array) {
            columnList.add(row.get(columnIndex));
        }
        return columnList;
    }

    public String getCompanyId(ArrayList<ArrayList<String>> companiesData, String companyName)
    {
        for(ArrayList<String> row: companiesData) {
            if(row.get(1).equals(companyName))
            {
                return row.get(0);
            }
        }
        return null;
    }
}
