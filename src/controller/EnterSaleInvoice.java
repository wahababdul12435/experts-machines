package controller;


import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.JFXDrawer;

import com.jfoenix.controls.JFXTextArea;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.print.*;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.StackPane;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.util.Callback;
import javafx.util.Duration;
import model.*;
import javafx.stage.Stage;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JRDesignQuery;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;
import org.controlsfx.control.textfield.AutoCompletionBinding;
import org.controlsfx.control.textfield.TextFields;

import javax.swing.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.util.*;

public class EnterSaleInvoice implements Initializable {

    @FXML   private TextField txt_dealerID;
    @FXML    private Label lbl_discAmount;
    @FXML    private Label lbl_totalAmount;
    @FXML    private Label lbl_retailPrice;
    @FXML    private Label lbl_purchPrice;
    @FXML    private Label lbl_tradePrice;
    @FXML    private Label lbl_dt;
    @FXML    private Label lbl_dealerType;
    @FXML    private Button btn_add;
    @FXML    private Button btn_saveinvoice;
    @FXML    private TextField txt_areaID;
    @FXML    private TextField txt_sale_invID;
    @FXML    private TextField txt_dealerName;
    @FXML    private TextField txt_totalNetAmount;
    @FXML    private TextField txt_dealerAdd;
    @FXML    private TextField txt_totaldiscAmount;
    @FXML    private TextField txt_dealerLicense;
    @FXML    private Label lbl_stockQaunt;
    @FXML    private TextField txt_prodID;
    @FXML    private Label lbl_stockBONUSQaunt;
    @FXML    private TextField txt_compID;
    @FXML    private TextField txt_batchno;
    @FXML    private TextField txt_prodName;
    @FXML    private Label lbl_bq;
    @FXML    private Label lbl_sq;
    @FXML    private Label lbl_da;
    @FXML    private Label lbl_ta;
    @FXML    private Label lbl_tp;
    @FXML    private Label lbl_pp;
    @FXML    private Label lbl_rp;
    @FXML    private Label lbl_ai;
    @FXML    private Label lbl_jobstatus;
    @FXML    private TextField txt_bonus;
    @FXML    private TextField txt_discount;
    @FXML    private TextField txt_quant;
    @FXML    private TextField txt_grossAmount;
    @FXML    private DatePicker datepick_saleinvoicedate;
    @FXML    private AnchorPane inner_anchor;
    @FXML    private JFXDrawer drawer;
    @FXML    private Button btn11;
    @FXML    private Pane btnpane;
    @FXML    private FontAwesomeIconView FA_Icon;
    @FXML    private BorderPane nav_sales;
    @FXML    private Button btn_print;
    @FXML    private JFXTextArea textArea;
    @FXML    private StackPane savePrintPane;
    public static Button btnView;

    VBox root = new VBox(5);
    VBox headers = new VBox(5);
    private AutoCompletionBinding<String> autoCompletionBinding;



    @FXML
    private Menu pending_orders;
    PendingOrders objPendingOrders;
    MysqlCon objMysqlCon;
    Statement objStmt;
    Connection objCon;
    int unSeenOrders;
    @FXML    private ComboBox combo_packSize;
    @FXML    private TableView tableView;

    int EDIT_CHK_Value = 0;
    Alert a = new Alert(Alert.AlertType.NONE);
    int useofSecndBatch = 0;

    String[] getlast_invonumb = new String[1];


    public void initialize(URL url, ResourceBundle resourceBundle) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnSale.setStyle("-fx-background-color: #47ab1e");
      /*  btn_print.setOnAction(new EventHandler <ActionEvent>()
        {
            public void handle(ActionEvent event)
            {
                //Get all Printers
                //ObservableSet<Printer> printers = Printer.getAllPrinters();

                //for(Printer printer : printers)
                //{
                 //   txt_dealerLicense.appendText(printer.getName()+"\n");
                //}
                Printer defaultprinter = Printer.getDefaultPrinter();

                if (defaultprinter != null)
                {
                    String name = defaultprinter.getName();
                    txt_dealerLicense.appendText("Default printer name: " + name);
                }
                else
                {
                    txt_dealerLicense.appendText("No printers installed.");
                }

            }
        });*/

      /*  btn_print.setOnAction(new EventHandler <ActionEvent>()
        {
            public void handle(ActionEvent event)
            {
                VBox root = new VBox(5);
                root.getChildren().addAll(txt_compID, textArea, txt_batchno, txt_dealerAdd);
                // Set the Size of the VBox
                root.setPrefSize(400, 300);

                // Set the Style-properties of the VBox
                root.setStyle("-fx-padding: 10;" +
                        "-fx-border-style: solid inside;" +
                        "-fx-border-width: 2;" +
                        "-fx-border-insets: 5;" +
                        "-fx-border-radius: 5;" +
                        "-fx-border-color: blue;");

                print(root);
            }
        });*/
        SaleInvoice.savePrintPane = savePrintPane;
        datepick_saleinvoicedate.setValue(LocalDate.now());
        txt_areaID.requestFocus();
        txt_discount.setText("0");
        txt_bonus.setText("0");

        ArrayList<String> newdealerlist = new ArrayList<>();
        MysqlCon newobjMysqlConnc = new MysqlCon();
        Statement objectStattmt = newobjMysqlConnc.stmt;
        Connection objectConnnec = newobjMysqlConnc.con;
        SaleInvoice objctsaleinvoice = new SaleInvoice();
        newdealerlist = objctsaleinvoice.get_AllDealers(objectStattmt,objectConnnec);
        String[] possiblee= new String[newdealerlist.size()];
        for(int i =0; i < newdealerlist.size();i++)
        {
            possiblee[i] = newdealerlist.get(i);
        }

        ArrayList<String> newprodlist = new ArrayList<>();
        MysqlCon objctsMysqlConnc = new MysqlCon();
        Statement objectStatmnt = objctsMysqlConnc.stmt;
        Connection objectConnnection = objctsMysqlConnc.con;
        newprodlist = objctsaleinvoice.get_Allprodcuts(objectStatmnt,objectConnnection);
        String[] Product_possiblee= new String[newprodlist.size()];
        for(int i =0; i < newprodlist.size();i++)
        {
            Product_possiblee[i] = newprodlist.get(i);
        }
        TextFields.bindAutoCompletion(txt_dealerName,possiblee);
        TextFields.bindAutoCompletion(txt_prodName,Product_possiblee);

        btnpane.setOnMouseEntered(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(22);
                btnpane.setMinWidth(20.0);
                btn11.setMinWidth(20.0);

            }
        });
        btnpane.setOnMouseExited(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(14);
                btnpane.setMinWidth(10.0);
                btn11.setMinWidth(10.0);
            }
        });

        inner_anchor.setLeftAnchor(inner_anchor, 260d);
        drawer.setSidePane(nav_sales);
        drawer.open();

        lbl_discAmount.setVisible(false);
        lbl_purchPrice.setVisible(false);
        lbl_retailPrice.setVisible(false);
        lbl_stockBONUSQaunt.setVisible(false);
        lbl_totalAmount.setVisible(false);
        lbl_tradePrice.setVisible(false);
        lbl_stockQaunt.setVisible(false);
        lbl_bq.setVisible(false);
        lbl_sq.setVisible(false);
        lbl_da.setVisible(false);
        lbl_ta.setVisible(false);
        lbl_tp.setVisible(false);
        lbl_pp.setVisible(false);
        lbl_rp.setVisible(false);
        lbl_dt.setVisible(false);
        lbl_dealerType.setVisible(false);
        lbl_jobstatus.setVisible(false);
        txt_areaID.setVisible(false);
        txt_compID.setVisible(false);
        MysqlCon newobjMysqlCon = new MysqlCon();
        Statement objectStmt = newobjMysqlCon.stmt;
        Connection objectCon = newobjMysqlCon.con;
        SaleInvoice objsaleinvoice = new SaleInvoice();

        getlast_invonumb = objsaleinvoice.get_lastinvnumber(objectStmt,objectCon);
        if(getlast_invonumb[0] == null)
        {
            txt_sale_invID.setText("1");
        }
        else
        {
            int cnvrtsalinvoint = Integer.parseInt(getlast_invonumb[0]);
            cnvrtsalinvoint = cnvrtsalinvoint+1;
            txt_sale_invID.setText(String.valueOf(cnvrtsalinvoint));
        }


        ObservableList<String> prodPacking = FXCollections.observableArrayList();
        prodPacking.add( "Box");
        prodPacking.add("Packs");
        combo_packSize.setItems(prodPacking);
        combo_packSize.setValue("Packs");
        TableColumn productcompidCOL = new TableColumn("Company ID");
        productcompidCOL.setMinWidth(100);
        productcompidCOL.setVisible(false);
        TableColumn productidCOL = new TableColumn("Product ID");
        productidCOL.setMinWidth(100);
        TableColumn productnameCOL = new TableColumn("Product Name");
        productnameCOL.setMinWidth(160);
        TableColumn productBatchCOL = new TableColumn("Batch No.");
        productBatchCOL.setMinWidth(100);
        TableColumn productquantCOL = new TableColumn("Quantity");
        productquantCOL.setMinWidth(100);
        TableColumn productDiscountCOL = new TableColumn("Discount");
        productDiscountCOL.setMinWidth(100);

        TableColumn productBonusCOL = new TableColumn("Bonus");
        productBonusCOL.setMinWidth(100);
        TableColumn productTotalCOL = new TableColumn("Total");
        productTotalCOL.setMinWidth(100);
        TableColumn productpacksizeCOL = new TableColumn("Pack Size");
        productpacksizeCOL.setMinWidth(10);
        productpacksizeCOL.setVisible(false);
        TableColumn productdiscAmountCOL = new TableColumn("Discount Amount");
        productdiscAmountCOL.setMinWidth(10);
        productdiscAmountCOL.setVisible(false);
        TableColumn productprodstockCOL = new TableColumn("Quantity in Stock");
        productprodstockCOL.setMinWidth(10);
        productprodstockCOL.setVisible(false);
        TableColumn productprodbonusstockCOL = new TableColumn("Bonus Quantity in Stock");
        productprodbonusstockCOL.setMinWidth(10);
        productprodbonusstockCOL.setVisible(false);
        TableColumn productEditCOL = new TableColumn("Actions");
        productEditCOL.setMinWidth(160);
        TableColumn firstEDITCol = new TableColumn("Edit");
        TableColumn secondDELETECol = new TableColumn("Delete");

        productEditCOL.getColumns().addAll(firstEDITCol, secondDELETECol);
        tableView.getColumns().addAll(productcompidCOL,productidCOL, productnameCOL,productBatchCOL, productquantCOL,productDiscountCOL,productBonusCOL,productTotalCOL,productpacksizeCOL,productdiscAmountCOL,productprodstockCOL,productprodbonusstockCOL,productEditCOL);
        productcompidCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("productCompId"));
        productidCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("productId"));
        productnameCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("productName"));
        productBatchCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("productBatch"));
        productquantCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("productQuant"));
        productDiscountCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("productDisc"));
        productBonusCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("productBonus"));
        productTotalCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("totalValue"));
        productpacksizeCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("packSize"));
        productdiscAmountCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("discAmount"));
        productprodstockCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("stockQuant"));
        productprodbonusstockCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("bonusstockQuant"));

        //START of Creating Delete Button
        secondDELETECol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Product,Boolean>, ObservableValue>() {
            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<Product, Boolean> param) {
                return new SimpleBooleanProperty(param.getValue()!=null);
            }

        });
        secondDELETECol.setCellFactory(new Callback<TableColumn<Product, Boolean>, TableCell<Product, Boolean>>() {
            @Override
            public TableCell<Product, Boolean> call(TableColumn<Product, Boolean> param) {
                return new ButtonCell(tableView);
            }
        });
        //END of Creating Delete Button
        //START of Creating EDIT Button
        firstEDITCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Product,Boolean>, ObservableValue>() {
            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<Product, Boolean> param) {
                return new SimpleBooleanProperty(param.getValue()!=null);
            }

        });
        firstEDITCol.setCellFactory(new Callback<TableColumn<Product, Boolean>, TableCell<Product, Boolean>>() {
            @Override
            public TableCell<Product, Boolean> call(TableColumn<Product, Boolean> param) {
                return new Editbtncell(tableView);
            }
        });
        //END of Creating EDIT Button
        tableView.setItems(data);
        btnpane.setOnMouseEntered(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                btnpane.setMinWidth(20.0);
                btn11.setMinWidth(20.0);

            }
        });
        btnpane.setOnMouseExited(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                btnpane.setMinWidth(10.0);
                btn11.setMinWidth(10.0);
            }
        });
    }
    public ArrayList<String> invoiceData;
    public  void viewsaleinvoice_report(ActionEvent event) throws IOException
    {
        try
        {
            /*MysqlCon objectsMysqlCon = new MysqlCon();
            Statement objectsStmt = objectsMysqlCon.stmt;
            Connection objectsCon = objectsMysqlCon.con;
            InputStream in = new FileInputStream(new File("C:\\wamp64\\www\\Distribution1\\experts-machines\\JavaFX\\Younas Traders\\src\\reports\\Invoice.jrxml"));
            JasperDesign jc = JRXmlLoader.load(in);
            String sql ="Select * from order_info";
            JRDesignQuery newQuery  = new JRDesignQuery();
            newQuery.setText(sql);
            jc.setQuery(newQuery);
            JasperReport jr = JasperCompileManager.compileReport(jc);
            JasperPrint jp = JasperFillManager.fillReport(jr,null,objectsCon);
            JasperViewer.viewReport(jp);*/
            InputStream objIO = ViewPendingOrders.class.getResourceAsStream("/reports/Invoice.jrxml");
            JasperReport objReport = JasperCompileManager.compileReport(objIO);
            JasperPrint objPrint = JasperFillManager.fillReport(objReport, null, new JRBeanCollectionDataSource(invoiceData));
            JasperViewer.viewReport(objPrint, false);
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null,e);
        }
    }
    public  void printDoc(ActionEvent event) throws IOException {

        if(tableView.getItems().isEmpty())
        {}
        else {
            //tableView.setPrefSize( 400, 400 );
            tableView.setMinHeight(300);
            tableView.setMinWidth(400);
            tableView.setMaxHeight(300);
            tableView.setMaxWidth(400);
            tableView.setPrefHeight(300);
            tableView.setPrefWidth(400);
            headers.getChildren().addAll(txt_prodName, lbl_discAmount);
            root.getChildren().addAll(headers,tableView);
            root.setPrefSize(450, 600);
            root.setStyle("-fx-padding: 10;" +
                    "-fx-border-style: solid inside;" +
                    "-fx-border-width: 2;" +
                    "-fx-border-insets: 5;" +
                    "-fx-border-radius: 5;" +
                    "-fx-border-color: blue;");
            print(root);

            root.getChildren().removeAll();
        }
    }

    private void print(Node node) throws IOException {
        PrinterJob printerJob = PrinterJob.createPrinterJob();
        JobSettings jobSettings = printerJob.getJobSettings();
        PageLayout pageLayout = jobSettings.getPageLayout();
        Printer printer = printerJob.getPrinter();
        pageLayout = printer.createPageLayout(Paper.A4, PageOrientation.PORTRAIT,Printer.MarginType.EQUAL);
        jobSettings.setPageLayout(pageLayout);

        // boolean prited = job.showPrintDialog(node.getScene().getWindow());
        //if(prited){}

        lbl_jobstatus.textProperty().unbind();
        lbl_jobstatus.setText("Creating a printer job...");
        PrinterJob job = PrinterJob.createPrinterJob();
        if (job != null)
        {
            lbl_jobstatus.textProperty().bind(job.jobStatusProperty().asString());
            boolean printed = job.printPage(node);
            if (printed)
            {
                job.endJob();
            }
            else
            {
                lbl_jobstatus.textProperty().unbind();
                lbl_jobstatus.setText("Printing failed.");
            }
        }
        else
        {
            lbl_jobstatus.setText("Could not create a printer job.");
        }
    }

    public void follow() throws IOException
    {
        if (drawer.isOpened()) {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(-250);
            transition.setByY(0);
            transition.setCycleCount(1);
            transition.play();
            drawer.close();
            FA_Icon.setGlyphName("ANGLE_DOUBLE_RIGHT");
            FA_Icon.setGlyphSize(14);
            //inner_anchor.setRightAnchor(inner_anchor,1d);


            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(-90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(1.28000);
            scale.setToY(1);



            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();
            //
            //inner_anchor.setLeftAnchor(inner_anchor, 5d);
        }
        else {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(250);
            transition.setByY(0);
            transition.setCycleCount(1);
            FA_Icon.setGlyphName("ANGLE_DOUBLE_LEFT");
            FA_Icon.setGlyphSize(14);
            drawer.open();

            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(0.99947777);
            scale.setToY(1);



            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();

        }


    }
    ObservableList<Product> data = FXCollections.observableArrayList();
    int rowvalueAfterEdit = 0;
    // START of Button Code for Editing a Row
    private class Editbtncell extends TableCell<Product,Boolean>{
        final Button btncell = new Button("Edit");

        public  Editbtncell(final TableView view1){
          btncell.setOnAction(new EventHandler<ActionEvent>() {
              @Override
              public void handle(ActionEvent event) {
                  rowvalueAfterEdit = getTableRow().getIndex();
                  Product newobj = (Product) tableView.getItems().get(rowvalueAfterEdit);
                  txt_compID.setText(newobj.getProductCompID());
                 txt_prodName.setText(newobj.getProductName());
                 txt_batchno.setText(newobj.getProductBatch());
                 txt_prodID.setText(newobj.getProductId());
                 txt_quant.setText(newobj.getProductQuant());
                 txt_bonus.setText(newobj.getProductBonus());
                 txt_discount.setText(newobj.getProductDisc());
                 lbl_totalAmount.setText(newobj.getTotalValue());
                  combo_packSize.setValue(newobj.getPackSize());
                    int prodID_forEdit = Integer.parseInt(txt_prodID.getText());
                  MysqlCon objMysqlCon1 = new MysqlCon();
                  Statement objStmt1 = objMysqlCon1.stmt;
                  Connection objCon1 = objMysqlCon1.con;
                  SaleInvoice objsaleinvoice1 = new SaleInvoice();
                  proddetails_onIDEnter = objsaleinvoice1.get_prodDetailswithIDs(objStmt1, objCon1, prodID_forEdit,txt_batchno.getText());
                  if(proddetails_onIDEnter[0]==null)
                  {
                      a.setAlertType(Alert.AlertType.ERROR);
                      a.setHeaderText("Error!");
                      a.setContentText("Product Code not Found.");
                      a.show();
                      txt_prodID.requestFocus();
                  }
                  String[] myArray = proddetails_onIDEnter[0].split("--");
                  txt_prodName.setText(myArray[0]);
                  lbl_retailPrice.setText(myArray[1]);
                  lbl_purchPrice.setText(myArray[2]);
                  lbl_tradePrice.setText(myArray[3]);
                  txt_compID.setText(myArray[4]);


                  MysqlCon objMysqlCon = new MysqlCon();
                  Statement objStmt = objMysqlCon.stmt;
                  Connection objCon = objMysqlCon.con;
                  SaleInvoice objsaleinvoice = new SaleInvoice();
                  prodquantinstock_onIDEnter = objsaleinvoice.gettotalprodstockquant_withIDs(objStmt, objCon,   prodID_forEdit);
                  if(prodquantinstock_onIDEnter[0]==null)
                  {
                      a.setAlertType(Alert.AlertType.ERROR);
                      a.setHeaderText("Error!");
                      a.setContentText("Stock Quantity is Zero");
                      a.show();
                      txt_prodID.requestFocus();
                  }
                  else
                  {
                      String[] prodStock_quantarr = prodquantinstock_onIDEnter[0].split("--");
                      lbl_stockQaunt.setText(prodStock_quantarr[0]);
                      lbl_stockBONUSQaunt.setText(prodStock_quantarr[1]);
                  }

                  txt_compID.setEditable(false);
                 txt_prodID.setEditable(false);
                  txt_prodName.setEditable(false);

                  if (discountPolicy_detail[0] == null) {
                  }
                  else
                  {
                      String[] mydiscountPolicy = discountPolicy_detail[0].split("-");

                      if(policy_Status.equals("Active") && txt_compID.getText().equals(mydiscountPolicy[1]))
                      {
                          txt_discount.setText(mydiscountPolicy[3]);
                          txt_discount.setDisable(true);
                      }

                      int intvalu_saleamount = Integer.parseInt(mydiscountPolicy[2]);

                      if(policy_Status=="Active" && intvalu_saleamount > 0)
                      {
                          saleamount_forDiscount = mydiscountPolicy[2];
                      }
                  }
                  if(txt_discount.isDisable() == true)
                  {

                      if(txt_bonus.isDisable() == true)
                      {
                          txt_quant.requestFocus();
                      }
                      else
                      {
                          txt_bonus.requestFocus();
                      }
                  }
                  else
                  {
                      txt_discount.requestFocus();
                  }
                  EDIT_CHK_Value = 1;
              }
          });
        }
        @Override
        protected  void updateItem(Boolean n, boolean boln){
            super.updateItem(n,boln);
            if(boln){
                setGraphic(null);
            }
            else
            {
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setGraphic(btncell);
            }
        }
    }
    // END of Button Code for Editing a Row
    //START of Button Code for Deleting a Row
    private class ButtonCell extends TableCell<Product, Boolean> {

        final Button CellButton = new Button("Delete");
        public ButtonCell(final TableView view) {
            CellButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    int selectedIndex = getTableRow().getIndex();
                    data.remove(selectedIndex);
                    ///////////////////////////START OF CALCULATIONS TO GET TOTAL amount and total Discount amount OF ALL PRODUCT ADDED IN TBALEVIEW
                    Float sumval =0.0f;
                    Float finalsumofDiscount2 = 0.0f;
                    Float nettotalamount = 0.0f;
                    for(int i = 0; i<tableView.getItems().size();i++)
                    {
                        Product newwobj = (Product) tableView.getItems().get(i);
                        float newval = Float.parseFloat(newwobj.getTotalValue());
                        float finalsumofDiscount = Float.parseFloat(newwobj.getDiscAmount());
                        finalsumofDiscount2 = finalsumofDiscount2 + finalsumofDiscount;
                        sumval = sumval + newval;
                        nettotalamount = sumval + finalsumofDiscount2;
                    }
                    txt_totalNetAmount.setText(String.valueOf(sumval));
                    txt_totaldiscAmount.setText(String.valueOf(finalsumofDiscount2));
                    txt_grossAmount.setText(String.valueOf(nettotalamount));
                    txt_prodID.requestFocus();
                    /////////////////////////////////END OF CALCULATIONS TO GET TOTAL amount and total Discount amount OF ALL PRODUCT ADDED IN TBALEVIEW
                }
            });
        }
        @Override
        protected void updateItem(Boolean t, boolean bln) {
            super.updateItem(t, bln);
            if (bln) {
                setGraphic(null);
            } else {
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setGraphic(CellButton);
            }

        }
    }
    //END of Button Code for Deleting a Row

    String getoverallrecord_dealer[] = new String[1];
    String getproduct_overallrecord[] = new String[1];
    String nxtBatch_Quantity[] = new String[10];
    String nxtBatch_BonusQuantity[] = new String[10];
    String scndbatchquant_variable;
    String  scndbatchBonus_variable;
    String batch_policy;
    String frstbatch_Qaunt;
    String frstbatch_BonusQaunt;
    String getfirst_batchQauntities[] = new String[1];
    String nxtbatch[] = new String[100];
    String getscndLast_batch[] = new String[1];
    String getscndbatchno_quantity[] = new String[1];
    int ordered_packets;
    int submitted_packets;
    int ordered_boxes;
    int submitted_boxes ;
    int prev_ordered_packets;
    int prev_submitted_packets;
    int prev_ordered_boxes;
    int prev_submitted_boxes ;
    int given_orders;
    float order_price ;
    float discount_price;
    float invoiced_price;
    float pending_payments;
    float discvalues;
    float grossamount;
    float discount_amount;
    float order_PriceValue;
    ArrayList<String> batchlist = new ArrayList<>();

    public void cclik() throws IOException
    {
        SaleInvoice objsaleinvoice = new SaleInvoice();
        objsaleinvoice.editClicked();
    }
    String prodTableIDValue = "";
    public void btnsaveinvoice_clicks() throws IOException, ParseException {

        int counttableROWS = tableView.getItems().size();
        String[] getprodid_tableview = new String[counttableROWS];
        String[] getprodCompid_tableview = new String[counttableROWS];
        String[] getprodQuant_tableview = new String[counttableROWS];
        String[] getprodBonus_tableview = new String[counttableROWS];
        String[] getprodDisc_tableview = new String[counttableROWS];
        String[] getpacksize_tableview = new String[counttableROWS];
        String[] getquant_inStock_tableview = new String[counttableROWS];
        String[] getbonusquant_inStock_tableview = new String[counttableROWS];
        String[] getprodTotal_table = new String[counttableROWS];
        String[] getdiscAmount_tableview = new String[counttableROWS];
        String[] getbatchnumber = new String[counttableROWS];

        for(int i =0;i<counttableROWS;i++) //////////////////////////////////////START OF LOOP TO READ ALL ROWS OF TABLVIEW
        {
            Product ProductObject = (Product) tableView.getItems().get(i);
            getprodid_tableview[i] = ProductObject.getProductId();
            getprodCompid_tableview[i] = ProductObject.getProductCompID();
            getprodQuant_tableview[i] = ProductObject.getProductQuant();
            getprodBonus_tableview[i] = ProductObject.getProductBonus();
            getprodDisc_tableview[i] = ProductObject.getProductDisc();
            getpacksize_tableview[i] = ProductObject.getPackSize();
            getquant_inStock_tableview[i] = ProductObject.getStockQuant();
            getbonusquant_inStock_tableview[i] = ProductObject.getBonusstockQuant();
            getprodTotal_table[i] = ProductObject.getTotalValue();
            getdiscAmount_tableview[i] = ProductObject.getDiscAmount();
            getbatchnumber[i] = ProductObject.getProductBatch();
            SaleInvoice objsaleinvoice1 = new SaleInvoice();

            int intvalue_prodID = Integer.parseInt(getprodid_tableview[i]);
            MysqlCon mySQLobjc = new MysqlCon();
            Statement mysqlstatmenOBJ = mySQLobjc.stmt;
            Connection mysqlCnectOBJ = mySQLobjc.con;
            prodTableIDValue = objsaleinvoice1.getTableprodID(mysqlstatmenOBJ,mysqlCnectOBJ,intvalue_prodID );
            int intvalueprodTableID = Integer.parseInt(prodTableIDValue);


            int intvalue_tableviewprodQuant = Integer.parseInt(getprodQuant_tableview[i]);
            int intvalue_tableviewprodBonus = Integer.parseInt(getprodBonus_tableview[i]);
            int intvalue_stockquantity = Integer.parseInt(getquant_inStock_tableview[i]);
            int intvalue_bonusstockquantity = Integer.parseInt(getbonusquant_inStock_tableview[i]);
            discvalues=Float.parseFloat(getprodDisc_tableview[i]);
            grossamount = Float.parseFloat(getprodTotal_table[i]);
            discount_amount = Float.parseFloat(getdiscAmount_tableview[i]);
            order_PriceValue = grossamount-discount_amount;


            MysqlCon myobj = new MysqlCon();
            Statement statmenOBJ = myobj.stmt;
            Connection CnectOBJ = myobj.con;
            getproduct_overallrecord = objsaleinvoice1.getprevious_productoverallRecord(statmenOBJ,CnectOBJ,intvalueprodTableID , getbatchnumber[i]);
            if(getproduct_overallrecord[0]==null)
            {
                MysqlCon myobject = new MysqlCon();
                Statement statmentOBJ = myobject.stmt;
                Connection CnectionOBJ = myobject.con;
                if(!proddetails_onIDEnter[0].isEmpty())
                {
                    String[] productsdetails = proddetails_onIDEnter[0].split("--");
                    objsaleinvoice1.insert_productoverallRecord(statmentOBJ,CnectionOBJ,intvalueprodTableID ,productsdetails[0], getbatchnumber[i],intvalue_tableviewprodQuant,intvalue_tableviewprodBonus,Float.parseFloat(productsdetails[3]),Float.parseFloat(productsdetails[2]),Float.parseFloat(productsdetails[1]),0,0);
                }
            }
            else
            {
                String[] splitoverallRecord = getproduct_overallrecord[0].split("--");
                int Quantity_Integervalue = Integer.parseInt(splitoverallRecord[1]);
                int totalquantity = Quantity_Integervalue + intvalue_tableviewprodQuant;
                MysqlCon my_sql_obj = new MysqlCon();
                Statement statment_obj = my_sql_obj.stmt;
                Connection Cnection_obj = my_sql_obj.con;
                objsaleinvoice1.UPDATE_productoverallRecord(statment_obj,Cnection_obj,intvalueprodTableID,totalquantity , getbatchnumber[i]);
            }
            if(getpacksize_tableview[i].equals("Packs"))
            {
                ordered_packets = ordered_packets + intvalue_tableviewprodQuant;
                submitted_packets = submitted_packets + intvalue_tableviewprodQuant;
            }
            if(getpacksize_tableview[i].equals("Box"))
            {
                ordered_boxes = ordered_boxes + intvalue_tableviewprodQuant;
                submitted_boxes = submitted_boxes + intvalue_tableviewprodQuant;
            }
             int subtracted_prodQuant = 0;
            int subtracted_prodBonus = 0;
            if(intvalue_tableviewprodQuant>0){
                subtracted_prodQuant = intvalue_stockquantity - intvalue_tableviewprodQuant;
            }
            if(intvalue_tableviewprodBonus>0){
                subtracted_prodBonus = intvalue_bonusstockquantity - intvalue_tableviewprodBonus;
            }

            Float prodtotalAmount = grossamount + discount_amount;
            int returnedBit = 0;
            int rentrned_qantity = 0;
            int rentrned_Bonus_qantity = 0;

                MysqlCon objectmySQL = new MysqlCon();
                Statement statmOBJ = objectmySQL.stmt;
                Connection CnnectOBJ = objectmySQL.con;
                getfirst_batchQauntities = objsaleinvoice1.getprodbatch_quantity(statmOBJ,CnnectOBJ,intvalueprodTableID , getbatchnumber[i]);
                if(getfirst_batchQauntities[0]==null)
                {}
                else
                {
                    String[] frstbatch_quantitiesarray = getfirst_batchQauntities[0].split("--");
                    frstbatch_Qaunt =  frstbatch_quantitiesarray[0];
                    frstbatch_BonusQaunt =  frstbatch_quantitiesarray[1];
                }
            float tradePrice_intval=0.0f;
            MysqlCon objMysqlCon = new MysqlCon();
            Statement objctStmt = objMysqlCon.stmt;
            Connection objctCon = objMysqlCon.con;
            SaleInvoice objsaleinvoice = new SaleInvoice();
            proddetails_onIDEnter = objsaleinvoice.get_prodDetailswithIDs(objctStmt, objctCon, intvalueprodTableID,getbatchnumber[i]);
            if (proddetails_onIDEnter[0] == null) {

            } else {
                String[] myArray = proddetails_onIDEnter[0].split("--");
                tradePrice_intval = Float.parseFloat(myArray[1]);

            }
                int intfirstbatchqaunt_val = Integer.parseInt(frstbatch_Qaunt);
                int intfirstbatchbonusqaunt_val = Integer.parseInt(frstbatch_BonusQaunt);

                float floatdiscount_percent = Float.parseFloat(getprodDisc_tableview[i]);

                /////////////////////////////////////////////////////////////////////////////// START OF CONDITION TO CHECK THE ENTERED QUANTITY IS GREATER THAN THE BATCH QUANTITY
                if(intvalue_tableviewprodQuant > intfirstbatchqaunt_val)
                {
                    ///////////////////////////////////////////////////////////////////////////////////GROSS AMOUNT IS OVERALL TOTAL AMOUNT
                    float grossamount_frstbatch =  intfirstbatchqaunt_val * tradePrice_intval;
                    float frstbatch_discAmount = (grossamount_frstbatch * floatdiscount_percent)/100;
                    ///////////////////////////////////////////////////////////////////////////////////NET AMOUNT IS FINAL AMOUNT AFTER DISCOUNT
                    float frstbatch_netamount = grossamount_frstbatch- frstbatch_discAmount;

                    int setzeroquantity_firstBatch = 0;
                    int setzerobonusquantity_firstBatch = 0;
                    SaleInvoice saleinvoices_obj = new SaleInvoice();
                    ////////////////////////////////////////////////////////////////////////////////////SET FIRST BATCH QUANTITY IN DB  = 0
                    MysqlCon objMysqlCon1 = new MysqlCon();
                    Statement objStmt1 = objMysqlCon1.stmt;
                    Connection objCon1 = objMysqlCon1.con;
                    objsaleinvoice1.BATCHquant_Zero(objStmt1, objCon1, intvalueprodTableID, setzeroquantity_firstBatch ,getbatchnumber[i]);


                    MysqlCon objMysqlConnction2 = new MysqlCon();
                    Statement objectsStatmt2 = objMysqlConnction2.stmt;
                    Connection objectsConntn2 = objMysqlConnction2.con;
                    int batchID_value1 = saleinvoices_obj.getBatchID(objectsStatmt2, objectsConntn2,intvalueprodTableID,getbatchnumber[i]);


                    MysqlCon objMysqlConct = new MysqlCon();
                    Statement objStmt = objMysqlConct.stmt;
                    Connection objContn = objMysqlConct.con;
                    saleinvoices_obj.insertsaleInvoiceInfodetails(objStmt, objContn,Integer.parseInt(txt_sale_invID.getText()),intvalueprodTableID,batchID_value1,intfirstbatchqaunt_val,intfirstbatchqaunt_val,getpacksize_tableview[i],getpacksize_tableview[i],grossamount_frstbatch,intvalue_tableviewprodBonus,discvalues,frstbatch_netamount,returnedBit,rentrned_qantity,rentrned_Bonus_qantity);

                    int remainQuant = intvalue_tableviewprodQuant - intfirstbatchqaunt_val;
                    int remainBonusQuant_afterFrstBatch = Integer.parseInt(txt_bonus.getText()) - intfirstbatchbonusqaunt_val;

                    /////////////////////////////////////////////////////////////////////////////////START OF FOR LOOP TO SAVE RECORD UNTIL ENTERED QUANTITY IS GREATER THAN BATCH QUANTITY
                    for(int j =0 ; remainQuant > 0;j++)
                    {
                        batchlist.clear();
                        ////////////////////////////////////////////////////GET NEXT BATCH IF ENTER QUANTITY GREATER THAN BATCH STOCK
                        MysqlCon myobjectSQL = new MysqlCon();
                        Statement mystatmnOBJ = myobjectSQL.stmt;
                        Connection myCnnctionOBJ = myobjectSQL.con;
                        batch_policy = objsaleinvoice1.getbatchSelectionpolicy(mystatmnOBJ,myCnnctionOBJ);
                        if(batch_policy.isEmpty())
                        {}
                       String[] split_batchSelection = batch_policy.split(" ");

                        ////////////////////////////////////////////////////GET NEXT BATCH IF ENTER QUANTITY GREATER THAN BATCH STOCK
                        MysqlCon myobjectmySQL = new MysqlCon();
                        Statement statmnOBJ = myobjectmySQL.stmt;
                        Connection CnnctionOBJ = myobjectmySQL.con;
                        batchlist = objsaleinvoice1.getprod_secondlastbatch(statmnOBJ,CnnctionOBJ,intvalueprodTableID);

                        if(batchlist.isEmpty())
                        {}
                        else
                        {
                            String[] getall_dates = new String[20];
                            String[] getall_time = new String[20];
                            for(int f = 0;f<batchlist.size();f++)
                            {
                                String[] split = batchlist.get(f).split("--");
                                if(split_batchSelection[0].equals("Entry"))
                                {
                                    getall_dates[f] = split[1];
                                    getall_time[f] = split[2];
                                }
                                if(split_batchSelection[0].equals("Expiry"))
                                {
                                    getall_dates[f] = split[3];
                                    getall_time[f] = split[2];
                                }

                            }
                            List<String> myList = new ArrayList<>();
                            for(int g = 0;g<batchlist.size();g++)
                            {
                                if(split_batchSelection[1].equals("FIFO") || split_batchSelection[1].equals("LILO"))
                                {
                                    String cmbine = getall_dates[g] +" "+ getall_time[g];
                                    myList.add(cmbine);
                                    Collections.sort(myList);
                                }
                                if(split_batchSelection[1].equals("LIFO") || split_batchSelection[1].equals("FIFO"))
                                {
                                    String cmbine = getall_dates[g] +" "+ getall_time[g];
                                    myList.add(cmbine);
                                    Collections.sort(myList,Collections.reverseOrder());
                                }

                            }
                            ///////////////////////////////////////////SELECT THE POLICY OF FIFO OR LIFO ETC
                                String cnvrtdatetime = String.valueOf(myList.get(0));
                                String[] splitDate_Time = cnvrtdatetime.split(" ");
                            String getscndbatchno="";
                            if(split_batchSelection[0].equals("Entry"))
                            {
                                MysqlCon mysqlOBJct = new MysqlCon();
                                Statement mystamtOBJct = mysqlOBJct.stmt;
                                Connection mysqlConnobjct = mysqlOBJct.con;
                                getscndbatchno = objsaleinvoice1.getprodbatch_entry(mystamtOBJct,mysqlConnobjct,intvalueprodTableID,splitDate_Time[0],splitDate_Time[1]);
                            }
                            if(split_batchSelection[0].equals("Expiry"))
                            {
                                MysqlCon mysqlOBJct = new MysqlCon();
                                Statement mystamtOBJct = mysqlOBJct.stmt;
                                Connection mysqlConnobjct = mysqlOBJct.con;
                                getscndbatchno = objsaleinvoice1.getprodbatch_expiry(mystamtOBJct,mysqlConnobjct,intvalueprodTableID,splitDate_Time[0],splitDate_Time[1]);
                            }
                            if(getscndbatchno.isEmpty()){}
                            else
                            {
                                nxtbatch[j] = getscndbatchno;
                            }

                        }
                        ////////////////////////////////////////////////////GET BATCH QUANTITY OF NEXT BATCH IF ENTER QUANTITY GREATER THAN BATCH STOCK
                        MysqlCon scndmysqlOBJ = new MysqlCon();
                        Statement scndmystamtOBJ = scndmysqlOBJ.stmt;
                        Connection scndmysqlConn = scndmysqlOBJ.con;
                        getscndbatchno_quantity = objsaleinvoice1.getprodbatch_quantity(scndmystamtOBJ,scndmysqlConn,intvalueprodTableID,nxtbatch[j]);
                        if(getscndbatchno_quantity[0]==null){}
                        else
                        {
                            String[] scndbatch_quantitiesarray = getscndbatchno_quantity[0].split("--");
                            nxtBatch_Quantity[j] = scndbatch_quantitiesarray[0];
                            nxtBatch_BonusQuantity[j] = scndbatch_quantitiesarray[1];
                        }

                        int  intnextbatchqaunt_val = Integer.parseInt(nxtBatch_Quantity[j]);
                        int intnextbatchbonusqaunt_val = Integer.parseInt(nxtBatch_BonusQuantity[j]);

                        remainQuant = remainQuant -   intnextbatchqaunt_val;

                        int sbtrctremainingquant_FrmscndbatchBONUS = remainBonusQuant_afterFrstBatch -   intnextbatchbonusqaunt_val;

                        if(remainQuant <=0)
                        {
                            int finalbatch_quantity = intnextbatchqaunt_val + remainQuant;
                            ///////////////////////////////////////////////////////////////////////////////////GROSS AMOUNT IS OVERALL TOTAL AMOUNT
                            float grossamount_finalbatch =  finalbatch_quantity * tradePrice_intval;
                            float finalbatch_discAmount = (grossamount_finalbatch * floatdiscount_percent)/100;
                            ///////////////////////////////////////////////////////////////////////////////////NET AMOUNT IS FINAL AMOUNT AFTER DISCOUNT
                            float finalbatch_netamount = grossamount_finalbatch- finalbatch_discAmount;
                            int remainingBatch_quant = intnextbatchqaunt_val - finalbatch_quantity;
                            int remainingBatch_Bonusquant = intnextbatchqaunt_val - finalbatch_quantity;

                            MysqlCon objMysqlConctn = new MysqlCon();
                            Statement statobj = objMysqlConctn.stmt;
                            Connection connobj = objMysqlConctn.con;
                            objsaleinvoice1.BATCHquant_Zero(statobj, connobj, intvalueprodTableID, remainingBatch_quant  ,nxtbatch[j]);

                            MysqlCon objMysqlConnction1 = new MysqlCon();
                            Statement objectsStatmt1 = objMysqlConnction1.stmt;
                            Connection objectsConntn1 = objMysqlConnction1.con;
                            int batchID_value = saleinvoices_obj.getBatchID(objectsStatmt1, objectsConntn1,intvalueprodTableID,getbatchnumber[i]);

                            MysqlCon objMysqlConnct = new MysqlCon();
                            Statement objStatmt = objMysqlConnct.stmt;
                            Connection objConntn = objMysqlConnct.con;
                            saleinvoices_obj.insertsaleInvoiceInfodetails(objStatmt, objConntn,Integer.parseInt(txt_sale_invID.getText()),intvalueprodTableID,batchID_value,finalbatch_quantity,finalbatch_quantity,getpacksize_tableview[i],getpacksize_tableview[i],grossamount_finalbatch,intvalue_tableviewprodBonus,discvalues,finalbatch_netamount,returnedBit,rentrned_qantity,rentrned_Bonus_qantity);

                        }
                        else
                        {

                            ///////////////////////////////////////////////////////////////////////////////////GROSS AMOUNT IS OVERALL TOTAL AMOUNT
                            float grossamount_fornextbatch =  intnextbatchqaunt_val * tradePrice_intval;
                            float fornextbatch_discAmount = (grossamount_fornextbatch * floatdiscount_percent)/100;
                            ///////////////////////////////////////////////////////////////////////////////////NET AMOUNT IS FINAL AMOUNT AFTER DISCOUNT
                            float fornextbatch_netamount = grossamount_fornextbatch- fornextbatch_discAmount;

                            MysqlCon objMysqlConctn = new MysqlCon();
                            Statement statobj = objMysqlConctn.stmt;
                            Connection connobj = objMysqlConctn.con;
                            objsaleinvoice1.BATCHquant_Zero(statobj, connobj, intvalueprodTableID, setzeroquantity_firstBatch ,nxtbatch[j]);
/////////////////////////////////////////////////////////////////////////////////////////////////////////

                            MysqlCon objMysqlConnction = new MysqlCon();
                            Statement objectsStatmt = objMysqlConnction.stmt;
                            Connection objectsConntn = objMysqlConnction.con;
                            int batchID_value = saleinvoices_obj.getBatchID(objectsStatmt, objectsConntn,intvalueprodTableID,nxtbatch[j]);

                            MysqlCon objMysqlConnct = new MysqlCon();
                            Statement objStatmt = objMysqlConnct.stmt;
                            Connection objConntn = objMysqlConnct.con;
                            saleinvoices_obj.insertsaleInvoiceInfodetails(objStatmt, objConntn,Integer.parseInt(txt_sale_invID.getText()),intvalueprodTableID,batchID_value,intnextbatchqaunt_val,intnextbatchqaunt_val,getpacksize_tableview[i],getpacksize_tableview[i],grossamount_fornextbatch,intvalue_tableviewprodBonus,discvalues,fornextbatch_netamount,returnedBit,rentrned_qantity,rentrned_Bonus_qantity);
                        }


                    }
                    /////////////////////////////////////////////////////////////////////////////////END OF FOR LOOP TO SAVE RECORD UNTIL ENTERED QUANTITY IS GREATER THAN BATCH QUANTITY
                    MysqlCon objMsqlCon1 = new MysqlCon();
                    Statement objctStmt1 = objMsqlCon1.stmt;
                    Connection objctCon1 = objMsqlCon1.con;
                    objsaleinvoice1.subtract_quantFROMStock(objctStmt1, objctCon1, intvalueprodTableID, subtracted_prodQuant ,subtracted_prodBonus);
                }
                /////////////////////////////////////////////////////////////////////////////// END OF CONDITION TO CHECK THE ENTERED QUANTITY IS GREATER THAN THE BATCH QUANTITY

                else
                {
                    int subtractedBatch_quatity = intfirstbatchqaunt_val - intvalue_tableviewprodQuant;
                    int subtractedBatch_Bonusquatity = intfirstbatchbonusqaunt_val - intvalue_tableviewprodBonus;

                    MysqlCon objMysqlCon1 = new MysqlCon();
                    Statement objStmt1 = objMysqlCon1.stmt;
                    Connection objCon1 = objMysqlCon1.con;
                    objsaleinvoice1.BATCHquant_Zero(objStmt1, objCon1, intvalueprodTableID, subtractedBatch_quatity ,getbatchnumber[i]);

                    SaleInvoice saleinvoices_obj = new SaleInvoice();
                    MysqlCon objMysqlConnction = new MysqlCon();
                    Statement objectsStatmt = objMysqlConnction.stmt;
                    Connection objectsConntn = objMysqlConnction.con;
                    int batchID_value = saleinvoices_obj.getBatchID(objectsStatmt, objectsConntn,intvalueprodTableID,getbatchnumber[i]);


                    MysqlCon objMysqlConct = new MysqlCon();
                    Statement objStmt = objMysqlConct.stmt;
                    Connection objContn = objMysqlConct.con;
                    saleinvoices_obj.insertsaleInvoiceInfodetails(objStmt, objContn,Integer.parseInt(txt_sale_invID.getText()),intvalueprodTableID,batchID_value,intvalue_tableviewprodQuant,intvalue_tableviewprodQuant,getpacksize_tableview[i],getpacksize_tableview[i],grossamount,intvalue_tableviewprodBonus,discvalues,order_PriceValue,returnedBit,rentrned_qantity,rentrned_Bonus_qantity);

                    MysqlCon objMsqlCon1 = new MysqlCon();
                    Statement objctStmt1 = objMsqlCon1.stmt;
                    Connection objctCon1 = objMsqlCon1.con;
                    objsaleinvoice1.subtract_quantFROMStock(objctStmt1, objctCon1, intvalueprodTableID, subtracted_prodQuant ,subtracted_prodBonus);
                }


        } ///////////////////////////////////////////////////////////////////////////////////END OF LOOP TO READ ALL ROWS OF TABLVIEW
        SaleInvoice saleinvoices_object = new SaleInvoice();
        MysqlCon mysqlOBJect = new MysqlCon();
        Statement mystatmntOBJ = mysqlOBJect.stmt;
        Connection mysqlConnect = mysqlOBJect.con;
        getoverallrecord_dealer = saleinvoices_object.getprevious_dealeroverallRecord(mystatmntOBJ,mysqlConnect,Integer.parseInt(txt_dealerID.getText()));
        float TotalGross_amount = Float.parseFloat(txt_totalNetAmount.getText());
        float Totaldisc_amount = Float.parseFloat(txt_totaldiscAmount.getText());
        float Totalnet_amount = Float.parseFloat(txt_grossAmount.getText());
        if(getoverallrecord_dealer[0]==null)
        {
            MysqlCon mysqlOBJects = new MysqlCon();
            Statement mystatmntOBJect = mysqlOBJects.stmt;
            Connection mysqlConnection = mysqlOBJects.con;
            saleinvoices_object.insertdealerOverallRecord(mystatmntOBJect,mysqlConnection,Integer.parseInt(txt_dealerID.getText()) , 1,0,ordered_packets,submitted_packets,ordered_boxes,submitted_boxes,TotalGross_amount,Totaldisc_amount,Totalnet_amount,0,0,Totalnet_amount);
        }
        else
        {
            String[] dealerOverAll_Records = getoverallrecord_dealer[0].split(",");
            given_orders = Integer.parseInt(dealerOverAll_Records[0]);
            prev_ordered_packets = Integer.parseInt(dealerOverAll_Records[1]);
            prev_submitted_packets = Integer.parseInt(dealerOverAll_Records[2]);
            prev_ordered_boxes = Integer.parseInt(dealerOverAll_Records[3]);
            prev_submitted_boxes = Integer.parseInt(dealerOverAll_Records[4]);
            order_price = Float.parseFloat(dealerOverAll_Records[5]);
            discount_price = Float.parseFloat(dealerOverAll_Records[6]);
            invoiced_price = Float.parseFloat(dealerOverAll_Records[7]);
            pending_payments = Float.parseFloat(dealerOverAll_Records[8]);
            given_orders = given_orders+1;
            discount_price = discount_price + Totaldisc_amount;
            order_price = order_price + TotalGross_amount;
            invoiced_price = invoiced_price + Totalnet_amount;
            pending_payments = pending_payments + Totalnet_amount;

            ordered_packets = ordered_packets + prev_ordered_packets;
            submitted_packets = submitted_packets + prev_submitted_packets;
            ordered_boxes = ordered_boxes + prev_ordered_boxes;
            submitted_boxes = submitted_boxes + prev_submitted_boxes;

            MysqlCon mysqlOBJects = new MysqlCon();
            Statement mystatmntOBJect = mysqlOBJects.stmt;
            Connection mysqlConnection = mysqlOBJects.con;
            saleinvoices_object.updatedealerOverallRecord(mystatmntOBJect,mysqlConnection,Integer.parseInt(txt_dealerID.getText()) ,given_orders,ordered_packets,submitted_packets,ordered_boxes,submitted_boxes,order_price,discount_price,invoiced_price,pending_payments);
        }
        String commasepratedarray_prodID = String.join("_-_",getprodid_tableview);
        String commasepratedarray_prodQuant = String.join("_-_",getprodQuant_tableview);
        String commasepratedarray_prodBonus = String.join("_-_",getprodBonus_tableview);
        String commasepratedarray_prodDisc = String.join("_-_",getprodDisc_tableview);
        String commasepratedarray_prodpacksize = String.join("_-_",getpacksize_tableview);
        Float getorderprice = Float.parseFloat(txt_totalNetAmount.getText());
        Float gettotalprice = Float.parseFloat(txt_grossAmount.getText());
        LocalDate getdates = datepick_saleinvoicedate.getValue();
        String newdates = getdates.format(DateTimeFormatter.ofPattern("dd/MMM/yyyy"));
        String update_date = getdates.format(DateTimeFormatter.ofPattern("dd/MMM/yyyy"));
        LocalTime gettime = LocalTime.now();
        String newtime = gettime.format(DateTimeFormatter.ofPattern("hh:mm a"));
        LocalTime getupdate_time = LocalTime.now();
        String update_time = getupdate_time.format(DateTimeFormatter.ofPattern("hh:mm a"));

        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        SaleInvoice saleinvo_obj = new SaleInvoice();
        saleinvo_obj.insertsaleInvoiceInfo(objStmt, objCon,txt_sale_invID.getText(),txt_dealerID.getText(),commasepratedarray_prodID,commasepratedarray_prodQuant,commasepratedarray_prodpacksize,gettotalprice,commasepratedarray_prodBonus,commasepratedarray_prodDisc,getorderprice,newdates,newtime,update_date,update_time);

        Parent root = FXMLLoader.load(getClass().getResource("../view/sale_invoice.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
    /////////////////////////////////////////////////////////////////////////// Start of Click on Button Add
    public void btnadd_clicks() throws IOException{
        if(Float.parseFloat(lbl_totalAmount.getText()) == 0.00f)
        {
            if(Integer.parseInt(txt_discount.getText())==0)
            {
                if (!txt_quant.getText().isEmpty())
                {
                    float getretail_floatval = Float.parseFloat(lbl_tradePrice.getText());
                    int intcnvrt_quant = Integer.parseInt(txt_quant.getText());

                    float totalquantity_price = intcnvrt_quant * getretail_floatval;
                    lbl_totalAmount.setText(String.valueOf(totalquantity_price));
                }
            }
            else
            {
                if (!txt_quant.getText().isEmpty() && Integer.parseInt(txt_discount.getText())!=0)
                {
                    float intgetretail_price = Integer.parseInt(lbl_tradePrice.getText());
                    int intcnvrt_quant = Integer.parseInt(txt_quant.getText());
                    float intdiscount_value = Integer.parseInt(txt_discount.getText());
                    float totalquantity_price = intcnvrt_quant * intgetretail_price;
                    float disc_firstVal = (totalquantity_price * intdiscount_value) / 100;
                    lbl_discAmount.setText(String.valueOf(disc_firstVal));
                    float final_discVal = totalquantity_price - disc_firstVal;
                    lbl_totalAmount.setText(String.valueOf(final_discVal));

                }
                else
                {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setHeaderText("Input Field Empty");
                    a.setContentText("Please Enter Quantity");
                    a.show();
                    txt_quant.requestFocus();
                }
            }
        }
        if(EDIT_CHK_Value == 1)
        {
            Product newobj = (Product) tableView.getItems().get(rowvalueAfterEdit);
            if(txt_quant.getText() == "")
            {
                txt_quant.setText("0");
            }
            if(txt_bonus.getText() == "")
            {
                txt_bonus.setText("0");
            }
            int prodQuant = Integer.parseInt(txt_quant.getText());
            float discValue = Float.parseFloat(txt_discount.getText());
            float prodRetail = Float.parseFloat(lbl_retailPrice.getText());
            float totalprice = prodQuant*prodRetail;
            float discountedValue = (totalprice * discValue)/100;
            float afterdiscValue = totalprice - discountedValue;
            lbl_totalAmount.setText(String.valueOf(afterdiscValue));
            lbl_discAmount.setText(String.valueOf(discountedValue));

            newobj.setProductBonus(txt_bonus.getText());
            newobj.setProductDisc(txt_discount.getText());
            newobj.setProductQuant(txt_quant.getText());
            newobj.setTotalValue(lbl_totalAmount.getText());
            newobj.setPackSize(combo_packSize.getValue().toString());
            newobj.setDiscAmount(lbl_discAmount.getText());
            newobj.setStockQuant(lbl_stockQaunt.getText());
            newobj.setBonusstockQuant(lbl_stockBONUSQaunt.getText());
            newobj.setProductBatch(txt_batchno.getText());

            tableView.refresh();
            txt_prodID.setEditable(true);
            txt_prodName.setEditable(true);
            txt_prodID.setText("");
            txt_prodName.setText("");
            txt_quant.setText("");
            txt_bonus.setText("");
            txt_discount.setText("");
            txt_compID.setText("");
            txt_batchno.setText("");
            combo_packSize.setValue("Packs");
            lbl_totalAmount.setText("0.00");
            lbl_tradePrice.setText("0.00");
            lbl_purchPrice.setText("0.00");
            lbl_retailPrice.setText("0.00");
            lbl_discAmount.setText("0.00");
            lbl_stockQaunt.setText("-");
            lbl_stockBONUSQaunt.setText("-");
            ///////////////////////////START OF CALCULATIONS TO GET TOTAL amount and total Discount amount OF ALL PRODUCT ADDED IN TBALEVIEW
            Float sumval =0.0f;
            Float finalsumofDiscount2 = 0.0f;
            Float nettotalamount = 0.0f;
            for(int i = 0; i<tableView.getItems().size();i++)
            {
                Product newwobj = (Product) tableView.getItems().get(i);
                float newval = Float.parseFloat(newwobj.getTotalValue());

                float finalsumofDiscount = Float.parseFloat(newwobj.getDiscAmount());
                finalsumofDiscount2 = finalsumofDiscount2 + finalsumofDiscount;
                sumval = sumval + newval;
                nettotalamount = sumval + finalsumofDiscount2;
            }
            txt_totalNetAmount.setText(String.valueOf(sumval));
            txt_totaldiscAmount.setText(String.valueOf(finalsumofDiscount2));
            txt_grossAmount.setText(String.valueOf(nettotalamount));
            /////////////////////////////////END OF CALCULATIONS TO GET TOTAL amount and total Discount amount OF ALL PRODUCT ADDED IN TBALEVIEW
            txt_prodID.requestFocus();
            EDIT_CHK_Value = 0;
        }
        else {
            if (txt_prodName.getText().isEmpty()) {
                txt_prodName.setText("ERROR");
            } else if (txt_prodID.getText().isEmpty() || txt_quant.getText().isEmpty()) {
                txt_prodID.setText("ERROR");
            } else if (txt_quant.getText().isEmpty()) {
                txt_quant.setText("ERROR");
            }
            else {
                if(txt_quant.getText() == "")
                {
                    txt_quant.setText("0");
                }
                if(txt_bonus.getText() == "")
                {
                    txt_bonus.setText("0");
                }
                int prodQuant = Integer.parseInt(txt_quant.getText());
                float discValue = Float.parseFloat(txt_discount.getText());
                float prodRetail = Float.parseFloat(lbl_retailPrice.getText());
                float totalprice = prodQuant*prodRetail;
                float discountedValue = (totalprice * discValue)/100;
                float afterdiscValue = totalprice - discountedValue;
                lbl_totalAmount.setText(String.valueOf(afterdiscValue));
                lbl_discAmount.setText(String.valueOf(discountedValue));
                Product objnew = new Product();
                objnew.setProductCompID(txt_compID.getText());
                objnew.setProductId(txt_prodID.getText());
                objnew.setProductName(txt_prodName.getText());
                objnew.setProductBatch(txt_batchno.getText());
                objnew.setProductQuant(txt_quant.getText());
                objnew.setProductBonus(txt_bonus.getText());
                objnew.setProductDisc(txt_discount.getText());
                objnew.setTotalValue(lbl_totalAmount.getText());
                objnew.setPackSize(combo_packSize.getValue().toString());
                objnew.setDiscAmount(lbl_discAmount.getText());
                objnew.setStockQuant(lbl_stockQaunt.getText());
                objnew.setBonusstockQuant(lbl_stockBONUSQaunt.getText());
                tableView.getItems().add(objnew);
                txt_prodID.setText("");
                txt_prodName.setText("");
                txt_quant.setText("");
                txt_bonus.setText("0");
                txt_discount.setText("0");
                txt_compID.setText("");
                txt_batchno.setText("");
                combo_packSize.setValue("Packs");
                lbl_totalAmount.setText("0.00");
                lbl_tradePrice.setText("0.00");
                lbl_purchPrice.setText("0.00");
                lbl_retailPrice.setText("0.00");
                lbl_discAmount.setText("0.00");
                lbl_stockQaunt.setText("-");
                lbl_stockBONUSQaunt.setText("-");
                ///////////////////////////START OF CALCULATIONS TO GET TOTAL amount and total Discount amount OF ALL PRODUCT ADDED IN TBALEVIEW
                Float sumval =0.0f;
                Float finalsumofDiscount2 = 0.0f;
                Float nettotalamount = 0.0f;
                for(int i = 0; i<tableView.getItems().size();i++)
                {
                    Product newwobj = (Product) tableView.getItems().get(i);
                    float newval = Float.parseFloat(newwobj.getTotalValue());
                    float finalsumofDiscount = Float.parseFloat(newwobj.getDiscAmount());
                    finalsumofDiscount2 = finalsumofDiscount2 + finalsumofDiscount;
                    sumval = sumval + newval;
                    nettotalamount = sumval + finalsumofDiscount2;
                }
                txt_totalNetAmount.setText(String.valueOf(sumval));
                txt_totaldiscAmount.setText(String.valueOf(finalsumofDiscount2));
                txt_grossAmount.setText(String.valueOf(nettotalamount));
                /////////////////////////////////END OF CALCULATIONS TO GET TOTAL amount and total Discount amount OF ALL PRODUCT ADDED IN TBALEVIEW

                txt_prodID.requestFocus();
                txt_discount.setDisable(false);
                txt_bonus.setDisable(false);
            }
        }
    }
    /////////////////////////////////////////////////////////////////////////// End of Click on Button Add
    /////////////////////////////////////////////////////////////////////////// START of ENter Press on Button Add
    public  void  btnadd_enterpress(KeyEvent event) throws  IOException     {
        if(event.getCode() == KeyCode.ENTER)
        {
            if(Float.parseFloat(lbl_totalAmount.getText()) == 0.00f)
            {
                if(Integer.parseInt(txt_discount.getText())==0)
                {
                    if (!txt_quant.getText().isEmpty())
                    {
                        float getretail_floatval = Float.parseFloat(lbl_tradePrice.getText());
                        int intcnvrt_quant = Integer.parseInt(txt_quant.getText());

                        float totalquantity_price = intcnvrt_quant * getretail_floatval;
                        lbl_totalAmount.setText(String.valueOf(totalquantity_price));
                    }
                }
                else
                {
                    if (!txt_quant.getText().isEmpty() && Integer.parseInt(txt_discount.getText())!=0)
                    {
                        float intgetretail_price = Integer.parseInt(lbl_tradePrice.getText());
                        int intcnvrt_quant = Integer.parseInt(txt_quant.getText());
                        float intdiscount_value = Integer.parseInt(txt_discount.getText());
                        float totalquantity_price = intcnvrt_quant * intgetretail_price;
                        float disc_firstVal = (totalquantity_price * intdiscount_value) / 100;
                        lbl_discAmount.setText(String.valueOf(disc_firstVal));
                        float final_discVal = totalquantity_price - disc_firstVal;
                        lbl_totalAmount.setText(String.valueOf(final_discVal));

                    }
                    else
                    {
                        a.setAlertType(Alert.AlertType.ERROR);
                        a.setHeaderText("Input Field Empty");
                        a.setContentText("Please Enter Quantity");
                        a.show();
                        txt_quant.requestFocus();
                    }
                }
            }
            if(EDIT_CHK_Value == 1)
            {
                if(txt_quant.getText() == "")
                {
                    txt_quant.setText("0");
                }
                if(txt_bonus.getText() == "")
                {
                    txt_bonus.setText("0");
                }
                int prodQuant = Integer.parseInt(txt_quant.getText());
                float discValue = Float.parseFloat(txt_discount.getText());
                float prodRetail = Float.parseFloat(lbl_retailPrice.getText());
                float totalprice = prodQuant*prodRetail;
                float discountedValue = (totalprice * discValue)/100;
                float afterdiscValue = totalprice - discountedValue;
                lbl_totalAmount.setText(String.valueOf(afterdiscValue));
                lbl_discAmount.setText(String.valueOf(discountedValue));
            Product newobj = (Product) tableView.getItems().get(rowvalueAfterEdit);
            newobj.setProductBonus(txt_bonus.getText());
            newobj.setProductDisc(txt_discount.getText());
            newobj.setProductQuant(txt_quant.getText());
            newobj.setTotalValue(lbl_totalAmount.getText());
            newobj.setPackSize(combo_packSize.getValue().toString());
            newobj.setDiscAmount(lbl_discAmount.getText());
            newobj.setStockQuant(lbl_stockQaunt.getText());
                newobj.setBonusstockQuant(lbl_stockBONUSQaunt.getText());
                newobj.setProductBatch(txt_batchno.getText());

            tableView.refresh();
            txt_prodID.setEditable(true);
            txt_prodName.setEditable(true);
            txt_prodID.setText("");
            txt_prodName.setText("");
            txt_quant.setText("");
            txt_bonus.setText("0");
            txt_discount.setText("0");
            txt_compID.setText("");
                txt_batchno.setText("");
            combo_packSize.setValue("Packs");
                lbl_totalAmount.setText("0.00");
                lbl_tradePrice.setText("0.00");
                lbl_purchPrice.setText("0.00");
                lbl_retailPrice.setText("0.00");
                lbl_discAmount.setText("0.00");
                lbl_stockQaunt.setText("-");
                lbl_stockBONUSQaunt.setText("-");

                ///////////////////////////START OF CALCULATIONS TO GET TOTAL amount and total Discount amount OF ALL PRODUCT ADDED IN TBALEVIEW
                Float sumval =0.0f;
                Float finalsumofDiscount2 = 0.0f;
                Float nettotalamount = 0.0f;
                for(int i = 0; i<tableView.getItems().size();i++)
                {
                    Product newwobj = (Product) tableView.getItems().get(i);
                    float newval = Float.parseFloat(newwobj.getTotalValue());
                    float finalsumofDiscount = Float.parseFloat(newwobj.getDiscAmount());
                    finalsumofDiscount2 = finalsumofDiscount2 + finalsumofDiscount;
                    sumval = sumval + newval;
                    nettotalamount = sumval + finalsumofDiscount2;
                }
                txt_totalNetAmount.setText(String.valueOf(sumval));
                txt_totaldiscAmount.setText(String.valueOf(finalsumofDiscount2));
                txt_grossAmount.setText(String.valueOf(nettotalamount));
                /////////////////////////////////END OF CALCULATIONS TO GET TOTAL amount and total Discount amount OF ALL PRODUCT ADDED IN TBALEVIEW
                txt_prodID.requestFocus();
                EDIT_CHK_Value=0;
            }
            else {
            if (txt_prodName.getText().isEmpty()) {
                txt_prodName.setText("ERROR");
            } else if (txt_prodID.getText().isEmpty() || txt_quant.getText().isEmpty()) {
                txt_prodID.setText("ERROR");
            } else if (txt_quant.getText().isEmpty()) {
                txt_quant.setText("ERROR");
            }
            /*if(txt_bonus.getText().isEmpty()){
                txt_bonus.setText("ERROR");
            }*/
            else {
                if(txt_quant.getText() == "")
                {
                    txt_quant.setText("0");
                }
                if(txt_bonus.getText() == "")
                {
                    txt_bonus.setText("0");
                }
                int prodQuant = Integer.parseInt(txt_quant.getText());
                float discValue = Float.parseFloat(txt_discount.getText());
                float prodRetail = Float.parseFloat(lbl_retailPrice.getText());
                float totalprice = prodQuant*prodRetail;
                float discountedValue = (totalprice * discValue)/100;
                float afterdiscValue = totalprice - discountedValue;
                lbl_totalAmount.setText(String.valueOf(afterdiscValue));
                lbl_discAmount.setText(String.valueOf(discountedValue));
                Product objnew = new Product();
                objnew.setProductCompID(txt_compID.getText());
                objnew.setProductId(txt_prodID.getText());
                objnew.setProductName(txt_prodName.getText());
                objnew.setProductBatch(txt_batchno.getText());
                objnew.setProductQuant(txt_quant.getText());
                objnew.setProductBonus(txt_bonus.getText());
                objnew.setProductDisc(txt_discount.getText());
                objnew.setTotalValue(lbl_totalAmount.getText());
                objnew.setPackSize(combo_packSize.getValue().toString());
                objnew.setDiscAmount(lbl_discAmount.getText());
                objnew.setStockQuant(lbl_stockQaunt.getText());
                objnew.setBonusstockQuant(lbl_stockBONUSQaunt.getText());

                tableView.getItems().add(objnew);
                txt_prodID.setText("");
                txt_prodName.setText("");
                txt_quant.setText("");
                txt_bonus.setText("0");
                txt_discount.setText("0");
                txt_compID.setText("");
                txt_batchno.setText("");
                combo_packSize.setValue("Packs");
                lbl_totalAmount.setText("0.00");
                lbl_tradePrice.setText("0.00");
                lbl_purchPrice.setText("0.00");
                lbl_retailPrice.setText("0.00");
                lbl_discAmount.setText("0.00");
                lbl_stockQaunt.setText("-");
                lbl_stockBONUSQaunt.setText("-");
                ///////////////////////////START OF CALCULATIONS TO GET TOTAL amount and total Discount amount OF ALL PRODUCT ADDED IN TBALEVIEW
                Float sumval =0.0f;
                Float finalsumofDiscount2 = 0.0f;
                Float nettotalamount = 0.0f;
                for(int i = 0; i<tableView.getItems().size();i++)
                {
                    Product newwobj = (Product) tableView.getItems().get(i);
                    float newval = Float.parseFloat(newwobj.getTotalValue());
                    float finalsumofDiscount = Float.parseFloat(newwobj.getDiscAmount());
                    finalsumofDiscount2 = finalsumofDiscount2 + finalsumofDiscount;
                    sumval = sumval + newval;
                    nettotalamount = sumval + finalsumofDiscount2;
                }
                txt_totalNetAmount.setText(String.valueOf(sumval));
                txt_totaldiscAmount.setText(String.valueOf(finalsumofDiscount2));
                txt_grossAmount.setText(String.valueOf(nettotalamount));
                /////////////////////////////////END OF CALCULATIONS TO GET TOTAL amount and total Discount amount OF ALL PRODUCT ADDED IN TBALEVIEW
                txt_prodID.requestFocus();
                txt_discount.setDisable(false);
                txt_bonus.setDisable(false);
            }
        }

        }

    }
    /////////////////////////////////////////////////////////////////////////// End of ENter Press on Button Add
    public  void  enterpress_txtdiscount() throws IOException, ParseException {
        txt_bonus.requestFocus();
    }
    public  void  bonustxt_enter(KeyEvent event) throws  IOException {
        if(event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB)
        {
            if(txt_bonus.getText().equals("") || txt_bonus.getText().isEmpty())
            {
                txt_bonus.setText("0");
                txt_quant.requestFocus();
            }
            else {

                if (Integer.parseInt(batchBonus_variable) > 0 )
                {
                    txt_quant.requestFocus();
                }
                else if(Integer.parseInt(txt_bonus.getText()) == 0)
                {
                    txt_quant.requestFocus();
                }
                else
                    {
                        a.setAlertType(Alert.AlertType.ERROR);
                        a.setHeaderText("ERROR!");
                        a.setContentText("Bonus not Available for this Batch");
                        a.show();
                        txt_bonus.requestFocus();

                }

            }
        }
    }

    public void keypress_AreaID(KeyEvent event) throws IOException{
        if (event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB) {
        txt_dealerID.requestFocus();
        }
    }
    public void enterpress_compID(KeyEvent event) throws IOException{
        if (event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB) {
            txt_prodID.requestFocus();
        }
    }

    /////////////////////////////////////////////////////////////////////////// START of ENter Press on Quantity
    String bonusPolicyStatus = "";
    public void enterpress_quant() throws IOException, ParseException {
        if(txt_discount.getText().isEmpty())
        {
            txt_discount.setText("0");
        }

        if(txt_discount.getText().equals("0"))
        {
            if (!txt_quant.getText().isEmpty() && !txt_quant.getText().equals("0"))
            {
                float getretail_floatval = Float.parseFloat(lbl_tradePrice.getText());
                int intcnvrt_quant = Integer.parseInt(txt_quant.getText());

                if(intcnvrt_quant>Integer.parseInt(lbl_stockQaunt.getText()))
                {
                        a.setAlertType(Alert.AlertType.ERROR);
                        a.setHeaderText("ERROR!");
                        a.setContentText("Entered Quantity is More Than Quantity in Stock");
                        a.show();
                        txt_quant.requestFocus();
                }

                else
                {
                    float totalquantity_price = intcnvrt_quant * getretail_floatval;
                    lbl_totalAmount.setText(String.valueOf(totalquantity_price));

                    if(bonusPolicy_detail[0] != null) {
                        String getcurrDate = GlobalVariables.getStDate();
                        String[] details_bonuspolicy = bonusPolicy_detail[0].split("--");
                        boolean activ_or_not = activ_notactiv(details_bonuspolicy[3], details_bonuspolicy[3], getcurrDate);
                        if (activ_or_not == true) {
                            bonusPolicyStatus = "Active";
                        } else {
                            bonusPolicyStatus = "In Active";
                        }
                        if (details_bonuspolicy[6].equals(bonusPolicyStatus) && bonusPolicyStatus.equals("Active"))
                        {
                            if( details_bonuspolicy[7].equals(txt_dealerID.getText()))
                            {
                                int quantity_inPolicy = Integer.parseInt(details_bonuspolicy[2]);
                                int Bonus_inPolicy = Integer.parseInt(details_bonuspolicy[3]);
                                int quantity_intxtbox = Integer.parseInt(txt_quant.getText());
                                if(quantity_intxtbox >= quantity_inPolicy)
                                {
                                    int dividedQuantity = quantity_intxtbox/quantity_inPolicy;
                                    int totalQuantity = Bonus_inPolicy * dividedQuantity;
                                    txt_bonus.setText(String.valueOf(totalQuantity));
                                    txt_bonus.setDisable(true);
                                }
                            }

                        }
                        else if(!details_bonuspolicy[6].equals(bonusPolicyStatus))
                        {
                            SaleInvoice objectssaleinvoice = new SaleInvoice();
                            MysqlCon mysqlOBJects = new MysqlCon();
                            Statement mystatmntOBJect = mysqlOBJects.stmt;
                            Connection mysqlConnection = mysqlOBJects.con;
                            int intvalu_approvID = Integer.parseInt(details_bonuspolicy[0]);
                            objectssaleinvoice.UPDATE_BonuspolicyStatus(mystatmntOBJect,mysqlConnection,intvalu_approvID,txt_prodID.getText(),policy_Status);
                        }
                    }

                    btn_add.requestFocus();
                }

            }
        }
        else
        {
            if (!txt_quant.getText().isEmpty() && !txt_quant.getText().equals("0"))
            {
                float intgetretail_price = Float.parseFloat(lbl_tradePrice.getText());
                int intcnvrt_quant = Integer.parseInt(txt_quant.getText());
                float intdiscount_value = Float.parseFloat(txt_discount.getText());

                if(intcnvrt_quant>Integer.parseInt(batchquant_variable))
                {
                        a.setAlertType(Alert.AlertType.ERROR);
                        a.setHeaderText("ERROR!");
                        a.setContentText("Entered Quantity is More Than Stock Quantity");
                        a.show();
                        txt_quant.requestFocus();
                }

                else
                {
                    float totalquantity_price = intcnvrt_quant * intgetretail_price;
                    float disc_firstVal = (totalquantity_price * intdiscount_value) / 100;
                    lbl_discAmount.setText(String.valueOf(disc_firstVal));
                    float final_discVal = totalquantity_price - disc_firstVal;
                    lbl_totalAmount.setText(String.valueOf(final_discVal));

                    if(!bonusPolicy_detail[0].isEmpty()) {
                        String getcurrDate = GlobalVariables.getStDate();
                        String[] details_bonuspolicy = bonusPolicy_detail[0].split("--");
                        boolean activ_or_not = activ_notactiv(details_bonuspolicy[3], details_bonuspolicy[4], getcurrDate);
                        if (activ_or_not == true) {
                            bonusPolicyStatus = "Active";
                        } else {
                            bonusPolicyStatus = "In Active";
                        }

                        if (details_bonuspolicy[6].equals(bonusPolicyStatus) && bonusPolicyStatus.equals("Active"))
                        {
                            if(details_bonuspolicy[1].equals("null") && details_bonuspolicy[7].equals(txt_dealerID.getText()))
                            {
                                int quantity_inPolicy = Integer.parseInt(details_bonuspolicy[2]);
                                int quantity_intxtbox = Integer.parseInt(txt_quant.getText());
                                if(quantity_intxtbox >= quantity_inPolicy)
                                {
                                    txt_bonus.setText(details_bonuspolicy[3]);
                                    txt_bonus.setDisable(true);
                                }
                            }
                            if(details_bonuspolicy[1].equals(lbl_dealerType.getText()) )
                            {
                                int quantity_inPolicy = Integer.parseInt(details_bonuspolicy[2]);
                                int quantity_intxtbox = Integer.parseInt(txt_quant.getText());
                                if(quantity_intxtbox >= quantity_inPolicy)
                                {
                                    txt_bonus.setText(details_bonuspolicy[3]);
                                    txt_bonus.setDisable(true);
                                }
                            }


                        }
                        else if(!details_bonuspolicy[5].equals(bonusPolicyStatus))
                        {
                            SaleInvoice objectssaleinvoice = new SaleInvoice();
                            MysqlCon mysqlOBJects = new MysqlCon();
                            Statement mystatmntOBJect = mysqlOBJects.stmt;
                            Connection mysqlConnection = mysqlOBJects.con;
                            int intvalu_approvID = Integer.parseInt(details_bonuspolicy[0]);
                            objectssaleinvoice.UPDATE_BonuspolicyStatus(mystatmntOBJect,mysqlConnection,intvalu_approvID,txt_prodID.getText(),policy_Status);
                        }
                    }

                    btn_add.requestFocus();
                }
            }
            else
            {
                a.setAlertType(Alert.AlertType.ERROR);
                a.setHeaderText("Invalid Input!");
                a.setContentText("Please Enter Quantity");
                a.show();
                txt_quant.requestFocus();
            }
        }

    }
    /////////////////////////////////////////////////////////////////////////// END of ENter Press on Quantity
    public boolean activ_notactiv(String st_date,String endDate,String curr_date) throws ParseException {
        DateFormat format = new SimpleDateFormat("dd/MMM/yyyy", Locale.ENGLISH);
        Date Sdate = format.parse(st_date);
        Date Edate = format.parse(endDate);
        Date Currdate = format.parse(curr_date);


        return Sdate.compareTo(Currdate) * Currdate.compareTo(Edate) >= 0;
    }
    /////////////////////////////////////////////////////////////////////////// START of Enter Press on Dealer ID
    String dealerdetails_onIDEnter[] = new String[1];
    String discountPolicy_detail[] = new String[1];
    String policy_Status = "";
    String saleamount_forDiscount = "";
    public void enterpress_dealerID(KeyEvent event) throws IOException, ParseException {
        if (event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB) {
            if(!txt_dealerID.getText().equals(""))
            {
                String dealer_ids = txt_dealerID.getText();
                int intdealer_id = Integer.parseInt(dealer_ids);

                MysqlCon objectsMysqlCon = new MysqlCon();
                Statement objectsStmt = objectsMysqlCon.stmt;
                Connection objectsCon = objectsMysqlCon.con;
                SaleInvoice objectssaleinvoice = new SaleInvoice();
                discountPolicy_detail = objectssaleinvoice.getdiscount_policy(objectsStmt, objectsCon, intdealer_id);
                String getcurrDate = GlobalVariables.getStDate();
                if (discountPolicy_detail[0] == null) {

                }
                else
                {
                    String[] mydiscountPolicy = discountPolicy_detail[0].split("-");
                    boolean activ_or_not = activ_notactiv(mydiscountPolicy[4],mydiscountPolicy[5],getcurrDate);
                    if(activ_or_not == true)
                    {
                        policy_Status= "Active";
                    }
                    else
                    {
                        policy_Status = "In Active";
                    }
                    if(mydiscountPolicy[6].equals(policy_Status))
                    {}
                    else
                    {
                        MysqlCon mysqlOBJects = new MysqlCon();
                        Statement mystatmntOBJect = mysqlOBJects.stmt;
                        Connection mysqlConnection = mysqlOBJects.con;
                        int intvalu_approvID = Integer.parseInt(mydiscountPolicy[0]);
                        objectssaleinvoice.UPDATE_discountpolicyStatus(mystatmntOBJect,mysqlConnection,intvalu_approvID,intdealer_id,policy_Status);

                    }
                    int intvalu_saleamount = Integer.parseInt(mydiscountPolicy[2]);
                    if(policy_Status=="Active" && intvalu_saleamount == 0)
                    {
                        //txt_discount.setText(mydiscountPolicy[3]);
                        //txt_discount.setDisable(true);
                    }
                    if(policy_Status=="Active" && intvalu_saleamount > 0)
                    {
                        saleamount_forDiscount = mydiscountPolicy[2];
                    }
                }

                MysqlCon objMysqlCon = new MysqlCon();
                Statement objStmt = objMysqlCon.stmt;
                Connection objCon = objMysqlCon.con;
                SaleInvoice objsaleinvoice = new SaleInvoice();
                dealerdetails_onIDEnter = objsaleinvoice.get_dealerDetailswithIDs(objStmt, objCon, intdealer_id);
                if (dealerdetails_onIDEnter[0] == null) {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setHeaderText("Error!");
                    a.setContentText("Dealer ID not found.");
                    a.show();
                    txt_dealerID.requestFocus();
                    txt_dealerName.setText("");
                    txt_dealerAdd.setText("");
                    txt_dealerLicense.setText("");
                }
                else {
                    String[] myArray = dealerdetails_onIDEnter[0].split("--");
                    txt_dealerName.setText(myArray[0]);
                    txt_dealerAdd.setText(myArray[1]);
                    txt_areaID.setText(myArray[2]);
                    if(!myArray[3].equals("0"))
                    {
                        txt_dealerLicense.setText(myArray[3]);
                    }
                    if(!myArray[4].equals("0"))
                    {
                        txt_dealerLicense.setText(myArray[4]);
                    }
                    if(!myArray[5].equals("0"))
                    {
                        txt_dealerLicense.setText(myArray[5]);
                    }
                    lbl_dealerType.setText(myArray[4]);
                    txt_prodID.requestFocus();
                }
            }
            else
            {
                a.setAlertType(Alert.AlertType.ERROR);
                a.setHeaderText("Error!");
                a.setContentText("Please Enter Dealer ID.");
                a.show();
                txt_dealerID.requestFocus();
            }
        }
    }
    /////////////////////////////////////////////////////////////////////////// End of Enter Press on Dealer ID
    /////////////////////////////////////////////////////////////////////////// START of ENter Press on Dealer Name
        String dealerdetails_onEnterNamebox[] = new String[1];
        public void enterpress_dealerName(KeyEvent event) throws IOException, ParseException {
            if (event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB)
            {
                if(!txt_dealerName.getText().equals("")){
                String dealer_name = txt_dealerName.getText();

                MysqlCon objMysqlCon = new MysqlCon();
                Statement objStmt = objMysqlCon.stmt;
                Connection objCon = objMysqlCon.con;
                SaleInvoice objsaleinvoice = new SaleInvoice();
                dealerdetails_onEnterNamebox = objsaleinvoice.get_dealerDetailswithname(objStmt, objCon, dealer_name);
                if (dealerdetails_onEnterNamebox[0] == null) {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setHeaderText("Error!");
                    a.setContentText("Dealer Name not Found.");
                    a.show();
                    txt_dealerName.requestFocus();
                    txt_dealerID.setText("");
                    txt_dealerAdd.setText("");
                    txt_dealerLicense.setText("");

                }
                else {
                    String[] myArray = dealerdetails_onEnterNamebox[0].split("--");
                    txt_areaID.setText(myArray[1]);
                    txt_dealerID.setText(myArray[0]);

                    txt_dealerLicense.setText(myArray[3]);
                    txt_dealerAdd.setText(myArray[2]);
                    txt_prodID.requestFocus();
                }
                int dealerid_intvalue = Integer.parseInt(txt_dealerID.getText());

                    MysqlCon objectsMysqlCon = new MysqlCon();
                    Statement objectsStmt = objectsMysqlCon.stmt;
                    Connection objectsCon = objectsMysqlCon.con;
                    SaleInvoice objectssaleinvoice = new SaleInvoice();
                    discountPolicy_detail = objectssaleinvoice.getdiscount_policy(objectsStmt, objectsCon, dealerid_intvalue);
                    String getcurrDate = GlobalVariables.getStDate();
                    if (discountPolicy_detail[0] == null) {

                    }
                    else
                    {
                        String[] mydiscountPolicy = discountPolicy_detail[0].split("-");
                        boolean activ_or_not = activ_notactiv(mydiscountPolicy[4],mydiscountPolicy[5],getcurrDate);
                        if(activ_or_not == true)
                        {
                            policy_Status= "Active";
                        }
                        else
                        {
                            policy_Status = "In Active";
                        }
                        if(mydiscountPolicy[6].equals(policy_Status))
                        {}
                        else
                        {
                            MysqlCon mysqlOBJects = new MysqlCon();
                            Statement mystatmntOBJect = mysqlOBJects.stmt;
                            Connection mysqlConnection = mysqlOBJects.con;
                            int intvalu_approvID = Integer.parseInt(mydiscountPolicy[0]);
                            objectssaleinvoice.UPDATE_discountpolicyStatus(mystatmntOBJect,mysqlConnection,intvalu_approvID,dealerid_intvalue,policy_Status);

                        }
                        int intvalu_saleamount = Integer.parseInt(mydiscountPolicy[2]);
                        if(policy_Status=="Active" && intvalu_saleamount == 0)
                        {
                            //txt_discount.setText(mydiscountPolicy[3]);
                            //txt_discount.setDisable(true);
                        }
                        if(policy_Status=="Active" && intvalu_saleamount > 0)
                        {
                            saleamount_forDiscount = mydiscountPolicy[2];
                        }
                    }

            }
        }
    }
    /////////////////////////////////////////////////////////////////////////// END of ENter Press on Dealer Name
    /////////////////////////////////////////////////////////////////////////// START of Enter Press on Product ID
    String proddetails_onIDEnter[] = new String[1];
    String prodquantinstock_onIDEnter[] = new String[1];
    String getbatchno[] = new String[1];
    String getbatchno_quantity[] = new String[1];
    String batchquant_variable;
    String batchBonus_variable;
    int alreadyentered=0;
    ArrayList<String> frstTimebatchlist = new ArrayList<>();
    String bonusPolicy_detail[] = new String[1];
    String StrngproductTable_id = "";

    public void enterpress_prodID(KeyEvent event) throws IOException, ParseException {
        if (event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB)
        {
            if(!txt_prodID.getText().equals(""))
            {
            tableView.refresh();

            for (int i = 0; i < tableView.getItems().size(); i++) {
                Product newwobj = (Product) tableView.getItems().get(i);
                if (newwobj.getProductId().equals(txt_prodID.getText())) {
                    alreadyentered = 1;
                }
            }
            if (alreadyentered == 0)
            {
                SaleInvoice objsaleinvoice1 = new SaleInvoice();

                String product_ids = txt_prodID.getText();
                int intprod_id = Integer.parseInt(product_ids);
                MysqlCon myobjctMYSQL = new MysqlCon();
                Statement mystatmentsOBJ = myobjctMYSQL.stmt;
                Connection myConectionsOBJ = myobjctMYSQL.con;
                StrngproductTable_id = objsaleinvoice1.getTableprodID(mystatmentsOBJ, myConectionsOBJ,intprod_id);
                int intprodTable_id = Integer.parseInt(StrngproductTable_id);

                int dealerid_intValue = Integer.parseInt(txt_dealerID.getText());
                String getcurrDate = GlobalVariables.getStDate();
                SaleInvoice objectssaleinvoice = new SaleInvoice();


                MysqlCon myobjctSQL = new MysqlCon();
                Statement mystatmentOBJ = myobjctSQL.stmt;
                Connection myConectionOBJ = myobjctSQL.con;
                bonusPolicy_detail = objsaleinvoice1.get_bonuspolicy(mystatmentOBJ, myConectionOBJ,intprodTable_id);

                        MysqlCon myobjectSQL = new MysqlCon();
                        Statement mystatmnOBJ = myobjectSQL.stmt;
                        Connection myCnnctionOBJ = myobjectSQL.con;
                        batch_policy = objsaleinvoice1.getbatchSelectionpolicy(mystatmnOBJ, myCnnctionOBJ);
                        if (batch_policy.isEmpty()) {
                        }
                        String[] split_batchSelection = batch_policy.split(" ");

                        ////////////////////////////////////////////////////GET NEXT BATCH IF ENTER QUANTITY GREATER THAN BATCH STOCK
                        MysqlCon myobjectmySQL = new MysqlCon();
                        Statement statmnOBJ = myobjectmySQL.stmt;
                        Connection CnnctionOBJ = myobjectmySQL.con;
                        frstTimebatchlist = objsaleinvoice1.getprod_secondlastbatch(statmnOBJ, CnnctionOBJ, intprodTable_id);

                        if (frstTimebatchlist.isEmpty()) {
                            a.setAlertType(Alert.AlertType.ERROR);
                            a.setHeaderText("Error!");
                            a.setContentText("No Batch Found!");
                            a.show();
                            txt_prodID.requestFocus();
                        }
                        else
                            {
                                String[] getall_dates = new String[20];
                                String[] getall_time = new String[20];
                                for (int f = 0; f < frstTimebatchlist.size(); f++)
                                {
                                    String[] split = frstTimebatchlist.get(f).split("--");
                                    if (split_batchSelection[0].equals("Entry")) {
                                        getall_dates[f] = split[1];
                                        getall_time[f] = split[2];
                                    }
                                    if (split_batchSelection[0].equals("Expiry")) {
                                        getall_dates[f] = split[3];
                                        getall_time[f] = split[2];
                                    }

                                }
                                List<LocalDateTime> mydateList = new ArrayList<>();
                                List<String> myList = new ArrayList<>();
                                for (int g = 0; g < frstTimebatchlist.size(); g++) {
                                    if (split_batchSelection[1].equals("FIFO") || split_batchSelection[1].equals("LILO"))
                                    {
                                        String[] splitdate = getall_dates[g].split("/");
                                        String cmbine = getall_dates[g] + " " + getall_time[g];
                                        myList.add(cmbine);
                                        Collections.sort(myList, Collections.reverseOrder());

                                    }
                                    if (split_batchSelection[1].equals("LIFO") || split_batchSelection[1].equals("FILO")) {
                                        String[] splitdate = getall_dates[g].split("/");
                                        String cmbine = getall_dates[g] + " " + getall_time[g];
                                        myList.add(cmbine);
                                        Collections.sort(myList);
                                    }
                                }
                                int batchQuant_INtvalue = 0;
                                String getscndbatchno = "";
                                String getscndbatchnoQuant = "";
                                String prodBatchNum = "";
                                int var_i = 0;
                                do {
                                    String cnvrtdatetime = String.valueOf(myList.get(var_i));
                                    String[] splitDate_Time = cnvrtdatetime.split(" ");


                                    if (split_batchSelection[0].equals("Entry")) {
                                        MysqlCon mysqlOBJct = new MysqlCon();
                                        Statement mystamtOBJct = mysqlOBJct.stmt;
                                        Connection mysqlConnobjct = mysqlOBJct.con;
                                        getscndbatchno = objsaleinvoice1.getprodbatch_entry(mystamtOBJct, mysqlConnobjct, intprodTable_id, splitDate_Time[0], splitDate_Time[1]);
                                        String[] Splitscndbatch = getscndbatchno.split("-");
                                        getscndbatchnoQuant = Splitscndbatch[1];
                                        prodBatchNum= Splitscndbatch[0];
                                    }
                                    if (split_batchSelection[0].equals("Expiry")) {
                                        MysqlCon mysqlOBJct = new MysqlCon();
                                        Statement mystamtOBJct = mysqlOBJct.stmt;
                                        Connection mysqlConnobjct = mysqlOBJct.con;
                                        getscndbatchno = objsaleinvoice1.getprodbatch_expiry(mystamtOBJct, mysqlConnobjct, intprodTable_id, splitDate_Time[0], splitDate_Time[1]);
                                        String[] Splitscndbatch = getscndbatchno.split("-");
                                        getscndbatchnoQuant = Splitscndbatch[1];
                                        prodBatchNum= Splitscndbatch[0];
                                    }
                                    batchQuant_INtvalue = Integer.parseInt(getscndbatchnoQuant);
                                    var_i++;
                                }
                                while(batchQuant_INtvalue<0);
                                if(batchQuant_INtvalue == 0)
                                {
                                    a.setAlertType(Alert.AlertType.ERROR);
                                    a.setHeaderText("Error!");
                                    a.setContentText("Stock of this product is Null.");
                                    a.show();
                                    txt_prodID.requestFocus();
                                }
                                else
                                {
                                    if (getscndbatchno == null) {
                                        a.setAlertType(Alert.AlertType.ERROR);
                                        a.setHeaderText("Error!");
                                        a.setContentText("No Batch of this Product was Found in Stock.");
                                        a.show();
                                        txt_prodID.requestFocus();

                                    }
                                    else {
                                        txt_batchno.setText(prodBatchNum);

                                        MysqlCon objMysqlCon = new MysqlCon();
                                        Statement objStmt = objMysqlCon.stmt;
                                        Connection objCon = objMysqlCon.con;
                                        SaleInvoice objsaleinvoice = new SaleInvoice();
                                        prodquantinstock_onIDEnter = objsaleinvoice.gettotalprodstockquant_withIDs(objStmt, objCon, intprodTable_id);
                                        if (prodquantinstock_onIDEnter[0] == null)
                                        {
                                            a.setAlertType(Alert.AlertType.ERROR);
                                            a.setHeaderText("Error!");
                                            a.setContentText("Stock Quantity is Zero");
                                            a.show();
                                            txt_prodID.requestFocus();
                                        }
                                        else
                                            {
                                                MysqlCon objMysqlCon1 = new MysqlCon();
                                                Statement objStmt1 = objMysqlCon1.stmt;
                                                Connection objCon1 = objMysqlCon1.con;
                                                proddetails_onIDEnter = objsaleinvoice1.get_prodDetailswithIDs(objStmt1, objCon1, intprodTable_id, txt_batchno.getText());
                                                if (proddetails_onIDEnter[0] == null) {
                                                    a.setAlertType(Alert.AlertType.ERROR);
                                                    a.setHeaderText("Error!");
                                                    a.setContentText("Product Code not Found.");
                                                    a.show();
                                                    txt_prodID.requestFocus();
                                                } else
                                                {
                                                String[] prodStock_quantarr = prodquantinstock_onIDEnter[0].split("--");
                                                lbl_stockQaunt.setText(prodStock_quantarr[0]);
                                                lbl_stockBONUSQaunt.setText(prodStock_quantarr[1]);

                                                String[] myArray = proddetails_onIDEnter[0].split("--");
                                                txt_prodName.setText(myArray[0]);
                                                lbl_retailPrice.setText(myArray[1]);
                                                lbl_purchPrice.setText(myArray[2]);
                                                lbl_tradePrice.setText(myArray[3]);
                                                txt_compID.setText(myArray[4]);

                                                    if (discountPolicy_detail[0] == null) {

                                                    }
                                                    else
                                                    {
                                                        String[] mydiscountPolicy = discountPolicy_detail[0].split("-");

                                                        if(policy_Status.equals("Active") && txt_compID.getText().equals(mydiscountPolicy[1]))
                                                        {
                                                            txt_discount.setText(mydiscountPolicy[3]);
                                                            txt_discount.setDisable(true);
                                                        }

                                                        int intvalu_saleamount = Integer.parseInt(mydiscountPolicy[2]);

                                                        if(policy_Status=="Active" && intvalu_saleamount > 0)
                                                        {
                                                            saleamount_forDiscount = mydiscountPolicy[2];
                                                        }
                                                    }

                                                    if(txt_discount.isDisable() == true)
                                                    {

                                                        if(txt_bonus.isDisable() == true)
                                                        {
                                                            txt_quant.requestFocus();
                                                        }
                                                        else
                                                        {
                                                            txt_bonus.requestFocus();
                                                        }
                                                    }
                                                    else
                                                    {
                                                        txt_discount.requestFocus();
                                                    }

                                                MysqlCon mysqlOBJ = new MysqlCon();
                                                Statement mystamtOBJ = mysqlOBJ.stmt;
                                                Connection mysqlConn = mysqlOBJ.con;
                                                getbatchno_quantity = objsaleinvoice.getprodbatch_quantity(mystamtOBJ, mysqlConn, intprodTable_id, txt_batchno.getText());
                                                if (getbatchno_quantity[0] == null) {
                                                } else {
                                                    String[] batch_quantityarray = getbatchno_quantity[0].split("--");
                                                    batchquant_variable = batch_quantityarray[0];
                                                    batchBonus_variable = batch_quantityarray[1];

                                                }
                                        }

                                    }
                                }
                                }
                            }
            }
            else {
                a.setAlertType(Alert.AlertType.ERROR);
                a.setHeaderText("ERROR!");
                a.setContentText("Product Already Entered in Invoice.");
                a.show();
                txt_prodID.requestFocus();
                alreadyentered = 0;
            }
        }
            else
            {
                a.setAlertType(Alert.AlertType.ERROR);
                a.setHeaderText("Error!");
                a.setContentText("Product ID not entered.");
                a.show();
                txt_prodID.requestFocus();
            }
    }
    }
    /////////////////////////////////////////////////////////////////////////// END of Enter Press on Product ID


    /////////////////////////////////////////////////////////////////////////// START of ENter Press on Product Name
    String proddetails_onprodNameEnter[] = new String[1];
    public void enterpress_prodName(KeyEvent event) throws IOException {
        if(event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB) {
            if(!txt_prodName.getText().equals("")) {
                tableView.refresh();

                for (int i = 0; i < tableView.getItems().size(); i++) {
                    Product newwobj = (Product) tableView.getItems().get(i);
                    if (newwobj.getProductName().equals(txt_prodName.getText())) {
                        alreadyentered = 1;

                    }
                }
                if (alreadyentered == 0)
                {
                    String product_name = txt_prodName.getText();
                MysqlCon objMysqlCon = new MysqlCon();
                Statement objStmt = objMysqlCon.stmt;
                Connection objCon = objMysqlCon.con;
                SaleInvoice objsaleinvoice = new SaleInvoice();
                proddetails_onprodNameEnter = objsaleinvoice.get_prodDetailswithname(objStmt, objCon, product_name);
                if (proddetails_onprodNameEnter[0] == null) {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setHeaderText("Error!");
                    a.setContentText("Product Name not Found.");
                    a.show();
                    txt_prodName.requestFocus();
                } else {
                    String[] myArray = proddetails_onprodNameEnter[0].split("--");
                    txt_compID.setText(myArray[1]);
                    txt_prodID.setText(myArray[0]);


                    int intprod_id = Integer.parseInt(txt_prodID.getText());

                    MysqlCon newobjMysqlCon1 = new MysqlCon();
                    Statement newobjStmt1 = newobjMysqlCon1.stmt;
                    Connection newobjCon1 = newobjMysqlCon1.con;
                    SaleInvoice newobjsaleinvoice1 = new SaleInvoice();
                    frstTimebatchlist = newobjsaleinvoice1.getprod_secondlastbatch(newobjStmt1, newobjCon1, intprod_id);

                    if (frstTimebatchlist.isEmpty()) {
                    } else {
                        MysqlCon myobjectSQL = new MysqlCon();
                        Statement mystatmnOBJ = myobjectSQL.stmt;
                        Connection myCnnctionOBJ = myobjectSQL.con;
                        batch_policy = newobjsaleinvoice1.getbatchSelectionpolicy(mystatmnOBJ, myCnnctionOBJ);
                        if (batch_policy.isEmpty()) {
                        } else {

                            String[] split_batchSelection = batch_policy.split(" ");

                            String[] getall_dates = new String[20];
                            String[] getall_time = new String[20];
                            for (int f = 0; f < frstTimebatchlist.size(); f++) {
                                String[] split = frstTimebatchlist.get(f).split("--");
                                if (split_batchSelection[0].equals("Entry")) {
                                    getall_dates[f] = split[1];
                                    getall_time[f] = split[2];
                                }
                                if (split_batchSelection[0].equals("Expiry")) {
                                    getall_dates[f] = split[3];
                                    getall_time[f] = split[2];
                                }

                            }
                            List<LocalDateTime> mydateList = new ArrayList<>();
                            List<String> myList = new ArrayList<>();
                            for (int g = 0; g < frstTimebatchlist.size(); g++) {
                                if (split_batchSelection[1].equals("FIFO") || split_batchSelection[1].equals("LILO")) {
                                    String[] splitdate = getall_dates[g].split("/");
                                    String cmbine = getall_dates[g] + " " + getall_time[g];
                                    myList.add(cmbine);
                                    Collections.sort(myList);
                                }
                                if (split_batchSelection[1].equals("LIFO") || split_batchSelection[1].equals("FILO")) {
                                    String[] splitdate = getall_dates[g].split("/");
                                    String cmbine = getall_dates[g] + " " + getall_time[g];
                                    myList.add(cmbine);
                                    Collections.sort(myList, Collections.reverseOrder());
                                }
                            }

                            String cnvrtdatetime = String.valueOf(myList.get(0));
                            String[] splitDate_Time = cnvrtdatetime.split(" ");
                            String getscndbatchno = "";
                            if (split_batchSelection[0].equals("Entry")) {
                                MysqlCon mysqlOBJct = new MysqlCon();
                                Statement mystamtOBJct = mysqlOBJct.stmt;
                                Connection mysqlConnobjct = mysqlOBJct.con;
                                getscndbatchno = newobjsaleinvoice1.getprodbatch_entry(mystamtOBJct, mysqlConnobjct, intprod_id, splitDate_Time[0], splitDate_Time[1]);
                            }
                            if (split_batchSelection[0].equals("Expiry")) {
                                MysqlCon mysqlOBJct = new MysqlCon();
                                Statement mystamtOBJct = mysqlOBJct.stmt;
                                Connection mysqlConnobjct = mysqlOBJct.con;
                                getscndbatchno = newobjsaleinvoice1.getprodbatch_expiry(mystamtOBJct, mysqlConnobjct, intprod_id, splitDate_Time[0], splitDate_Time[1]);
                            }


                            if (getscndbatchno == null) {
                                a.setAlertType(Alert.AlertType.ERROR);
                                a.setHeaderText("Error!");
                                a.setContentText("Batch of this Product was not Found in Stock.");
                                a.show();
                                txt_prodID.requestFocus();

                            } else {
                                txt_batchno.setText(getscndbatchno);

                                MysqlCon objctMysqlCon = new MysqlCon();
                                Statement objctStmt = objctMysqlCon.stmt;
                                Connection objctCon = objctMysqlCon.con;
                                prodquantinstock_onIDEnter = objsaleinvoice.gettotalprodstockquant_withIDs(objctStmt, objctCon, intprod_id);
                                if (prodquantinstock_onIDEnter[0] == null) {
                                    a.setAlertType(Alert.AlertType.ERROR);
                                    a.setHeaderText("Error!");
                                    a.setContentText("Stock Quantity is Zero");
                                    a.show();
                                    txt_prodID.requestFocus();
                                } else {
                                    String[] prodStock_quantarr = prodquantinstock_onIDEnter[0].split("--");
                                    lbl_stockQaunt.setText(prodStock_quantarr[0]);
                                    lbl_stockBONUSQaunt.setText(prodStock_quantarr[1]);


                                    MysqlCon objMysqlCon1 = new MysqlCon();
                                    Statement objStmt1 = objMysqlCon1.stmt;
                                    Connection objCon1 = objMysqlCon1.con;
                                    proddetails_onIDEnter = objsaleinvoice.get_prodDetailswithIDs(objStmt1, objCon1, intprod_id, txt_batchno.getText());
                                    if (proddetails_onIDEnter[0] == null) {
                                        a.setAlertType(Alert.AlertType.ERROR);
                                        a.setHeaderText("Error!");
                                        a.setContentText("Product not Found.");
                                        a.show();
                                        txt_prodID.requestFocus();
                                    } else {

                                        String[] mydetailArray = proddetails_onIDEnter[0].split("--");
                                        lbl_retailPrice.setText(mydetailArray[1]);
                                        lbl_purchPrice.setText(mydetailArray[2]);
                                        lbl_tradePrice.setText(mydetailArray[3]);
                                    }



                                        MysqlCon mysqlOBJ = new MysqlCon();
                                    Statement mystamtOBJ = mysqlOBJ.stmt;
                                    Connection mysqlConn = mysqlOBJ.con;
                                    getbatchno_quantity = objsaleinvoice.getprodbatch_quantity(mystamtOBJ, mysqlConn, intprod_id, txt_batchno.getText());
                                    if (getbatchno_quantity[0] == null) {
                                        a.setAlertType(Alert.AlertType.ERROR);
                                        a.setHeaderText("Error!");
                                        a.setContentText("Batch Qauntity is Nill of this Product.");
                                        a.show();
                                        txt_prodName.requestFocus();
                                    }
                                    else
                                        {
                                        String[] batch_quantityarray = getbatchno_quantity[0].split("--");
                                        batchquant_variable = batch_quantityarray[0];
                                        batchBonus_variable = batch_quantityarray[1];


                                        }
                                }
                            }
                        }
                    }
                    if (discountPolicy_detail[0] == null) {

                    }
                    else
                    {
                        String[] mydiscountPolicy = discountPolicy_detail[0].split("-");

                        if(policy_Status.equals("Active") && txt_compID.getText().equals(mydiscountPolicy[1]))
                        {
                            txt_discount.setText(mydiscountPolicy[3]);
                            txt_discount.setDisable(true);
                        }

                        int intvalu_saleamount = Integer.parseInt(mydiscountPolicy[2]);

                        if(policy_Status=="Active" && intvalu_saleamount > 0)
                        {
                            saleamount_forDiscount = mydiscountPolicy[2];
                        }
                    }

                    if(txt_discount.isDisable() == true)
                    {

                        if(txt_bonus.isDisable() == true)
                        {
                            txt_quant.requestFocus();
                        }
                        else
                        {
                            txt_bonus.requestFocus();
                        }
                    }
                    else
                    {
                        txt_discount.requestFocus();
                    }
                }
            }
                else {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setHeaderText("ERROR!");
                    a.setContentText("Product Already Entered in Invoice.");
                    a.show();
                    txt_prodID.requestFocus();
                    alreadyentered = 0;
                }
            }

            else
            {
                a.setAlertType(Alert.AlertType.ERROR);
                a.setHeaderText("Error!");
                a.setContentText("Product Name not entered.");
                a.show();
                txt_prodName.requestFocus();
            }
        }
    }
    /////////////////////////////////////////////////////////////////////////// END of Enter Press on Product Name


}
