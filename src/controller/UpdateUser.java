package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import model.*;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class UpdateUser implements Initializable {
    @FXML
    private Label label_update_user;

    @FXML
    private JFXTextField txt_user_id;

    @FXML
    private JFXTextField txt_user_name;

    @FXML
    private JFXComboBox<String> txt_user_type;

    @FXML
    private JFXComboBox<String> txt_user_status;

    @FXML
    private JFXButton dialog_user_close;

    @FXML
    private JFXButton dialog_user_update;

    @FXML
    private Label label_update1;

    @FXML
    private Label label_update11;

    @FXML
    private JFXTextField txt_user_contact;

    @FXML
    private JFXTextField txt_user_address;

    @FXML
    private JFXTextField txt_user_cnic;

    @FXML
    private JFXComboBox<String> txt_user_city;

    @FXML
    private TableView<UpdateUserInfo> table_designatedareas;

    @FXML
    private TableColumn<UpdateUserInfo, String> sr_no;

    @FXML
    private TableColumn<UpdateUserInfo, String> designated_city;

    @FXML
    private TableColumn<UpdateUserInfo, String> operations;

    @FXML
    private JFXButton add_city;


    public static String userId ="";
    public static String userTableId ="";
    public static String userName ="";
    public static String userType ="";
    public static String userContact ="";
    public static String userAddress ="";
    public static String userCnic ="";
    public static String userStatus ="";
    public static ArrayList<String> userCityIds = new ArrayList<>();
    public static ArrayList<String> userCityNames = new ArrayList<>();

    public static ObservableList<UpdateUserInfo> userCityDetail;

    private ArrayList<ArrayList<String>> citiesData = new ArrayList<>();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ViewUsersInfo.lblUpdate = label_update_user;
        UpdateUserInfo objUpdateUserInfo = new UpdateUserInfo();
        citiesData = objUpdateUserInfo.getCitiesData();

        txt_user_id.setText(userId);
        txt_user_name.setText(userName);
        txt_user_type.setValue(userType);
        txt_user_address.setText(userAddress);
        txt_user_cnic.setText(userCnic);
        txt_user_status.setValue(userStatus);
        dialog_user_close.setOnAction((action)->closeDialog());
        dialog_user_update.setOnAction((action)->updateUser());

        ArrayList<String> chosenCityNames = getArrayColumn(citiesData, 1);
        txt_user_city.getItems().addAll(chosenCityNames);

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        designated_city.setCellValueFactory(new PropertyValueFactory<>("userSubarea"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_designatedareas.setItems(parseUserList());
        UpdateUserInfo.table_designatedareas = table_designatedareas;
    }

    public static ObservableList<UpdateUserInfo> parseUserList() {
        userCityDetail = FXCollections.observableArrayList();
        for (int i = 0; i < userCityNames.size(); i++)
        {
            userCityDetail.add(new UpdateUserInfo(String.valueOf(i+1), userCityNames.get(i)));
        }
        return userCityDetail;
    }

    @FXML
    void addCity(ActionEvent event) {
        String addCityName = txt_user_city.getValue();
        userCityNames.add(addCityName);
        userCityIds.add(getCityId(citiesData, addCityName));
        table_designatedareas.setItems(parseUserList());
    }

    public void closeDialog()
    {
        ViewUsersInfo.dialog.close();
        ViewUsersInfo.stackPane.setVisible(false);
    }

    public void updateUser() {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        UserInfo objUserInfo = new UserInfo();
        userId = txt_user_id.getText();
        userName = txt_user_name.getText();
        userContact = txt_user_contact.getText();
        userAddress = txt_user_address.getText();
        userCnic = txt_user_cnic.getText();
        userType = txt_user_type.getValue();
        userStatus = txt_user_status.getValue();
        objUserInfo.updateUser(objStmt, objCon, userTableId, userId, userName, userContact, userAddress, userCnic, userType, userStatus, userCityIds);

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/view_users.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    public ArrayList<String> getArrayColumn(ArrayList<ArrayList<String>> array, int columnIndex)
    {
        ArrayList<String> columnList = new ArrayList<>();
        for(ArrayList<String> row: array) {
            columnList.add(row.get(columnIndex));
        }
        return columnList;
    }

//    public ArrayList<String> getAreaSubarea(ArrayList<ArrayList<String>> subcitiesData, String companyId)
//    {
//        ArrayList<String> columnList = new ArrayList<>();
//        chosenSubareaIds = new ArrayList<>();
//        for(ArrayList<String> row: subcitiesData) {
//            if(row.get(2).equals(companyId))
//            {
//                chosenSubareaIds.add(row.get(0));
//                columnList.add(row.get(1));
//            }
//        }
//        return columnList;
//    }

    public String getCityId(ArrayList<ArrayList<String>> citiesData, String cityName)
    {
        for(ArrayList<String> row: citiesData) {
            if(row.get(1).equals(cityName))
            {
                return row.get(0);
            }
        }
        return null;
    }

    public String findIdInSubarea(ArrayList<ArrayList<String>> subcitiesData, String subareaName, String areaId)
    {
        for (int i = 0 ; i < subcitiesData.size(); i++)
            if (subcitiesData.get(i).get(1).equals(subareaName) && subcitiesData.get(i).get(2).equals(areaId))
            {
                return subcitiesData.get(i).get(0);
            }
        return null;
    }
}
