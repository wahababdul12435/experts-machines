package controller;

import com.jfoenix.controls.JFXDrawer;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.util.Callback;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.util.Duration;
import model.*;
import org.controlsfx.control.textfield.TextFields;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;


import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.UnaryOperator;

public class EnterPuchaseInvoice implements Initializable  {
    @FXML    private TextField txt_supplierID;
    @FXML    private Label lbl_discAmount;
    @FXML    private Label lbl_totalAmount;
    @FXML    private Label lbl_retailPrice;
    @FXML    private Label lbl_purchPrice;
    @FXML    private Label lbl_tradePrice;
    @FXML    private TextField txt_prodID;
    @FXML    private TextField txt_supplierName;
    @FXML    private TextField txt_compID;
    @FXML    private TextField txt_prodName;
    @FXML    private Button btn_add;
    @FXML    private TextField txt_bonus;
    @FXML    private  TextField txt_purchInvNo;
    @FXML    private TextField txt_discount;
    @FXML    private TextField txt_quant;
    @FXML    private MenuBar menu_bar;
    @FXML    private DatePicker datepick_invoicedate;
    @FXML    private DatePicker datepick_expiry;
    @FXML    private TableView tableView;
    @FXML    private TextField txt_Batchno;
    @FXML    private ComboBox combo_packSize;
    @FXML    private  Label lbl_stockQaunt;
    @FXML    private  Label lbl_stockBONUSQaunt;
    @FXML    private  Label lbl_RP;
    @FXML    private  Label lbl_PP;
    @FXML    private  Label lbl_TP;
    @FXML    private  Label lbl_SQ;
    @FXML    private  Label lbl_BQ;
    @FXML    private  Label lbl_DA;
    @FXML    private  Label lbl_TA;
    @FXML    private TextField txt_purchID;
    @FXML    private TextField txt_totaldiscAmount;
    @FXML    private TextField txt_grossAmount;
    @FXML    private TextField txt_totalAmount;
    @FXML    private BorderPane nav_purch;
    @FXML    private Pane btnpane;
    @FXML    private JFXDrawer drawer;
    @FXML    private FontAwesomeIconView FA_Icon;
    @FXML    private AnchorPane purch_window;
    @FXML    private AnchorPane inner_anchor;
    @FXML    private Button btn11;
    @FXML    private StackPane changeRatePane;

    PendingOrders objPendingOrders;
    MysqlCon objMysqlCon;
    Statement objStmt;
    Connection objCon;
    int unSeenOrders;
    public static Button btnView;

    String[] getlast_purchID = new String[1];
    public void initialize(URL url, ResourceBundle resourceBundle){
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnPurchase.setStyle("-fx-background-color: #47ab1e");
        PurchaseInvoice.changeRatePane = changeRatePane;
        lbl_discAmount.setVisible(false);
        lbl_purchPrice.setVisible(false);
        lbl_retailPrice.setVisible(false);
        lbl_stockBONUSQaunt.setVisible(false);
        lbl_totalAmount.setVisible(false);
        lbl_tradePrice.setVisible(false);
        lbl_stockQaunt.setVisible(false);
        lbl_BQ.setVisible(false);
        lbl_SQ.setVisible(false);
        lbl_TP.setVisible(false);
        lbl_PP.setVisible(false);
        lbl_RP.setVisible(false);
        lbl_DA.setVisible(false);
        lbl_TA.setVisible(false);
        combo_packSize.setVisible(false);
        txt_compID.setVisible(false);

        ArrayList<String> newSupplist = new ArrayList<>();
        MysqlCon newobjMysqlConnc = new MysqlCon();
        Statement objectStattmt = newobjMysqlConnc.stmt;
        Connection objectConnnec = newobjMysqlConnc.con;
        PurchaseInvoice prchInvo_Obj = new PurchaseInvoice();
        newSupplist = prchInvo_Obj.get_AllSuppliers(objectStattmt,objectConnnec);
        String[] possiblee= new String[newSupplist.size()];
        for(int i =0; i < newSupplist.size();i++)
        {
            possiblee[i] = newSupplist.get(i);
        }
        ArrayList<String> newprodlist = new ArrayList<>();
        MysqlCon objctsMysqlConnc = new MysqlCon();
        Statement objectStatmnt = objctsMysqlConnc.stmt;
        Connection objectConnnection = objctsMysqlConnc.con;
        newprodlist = prchInvo_Obj.get_Allprodcuts(objectStatmnt,objectConnnection);
        String[] Product_possiblee= new String[newprodlist.size()];
        for(int i =0; i < newprodlist.size();i++)
        {
            Product_possiblee[i] = newprodlist.get(i);
        }
        TextFields.bindAutoCompletion(txt_supplierName,possiblee);
        TextFields.bindAutoCompletion(txt_prodName,Product_possiblee);

        datepick_invoicedate.setValue(LocalDate.now());
        datepick_expiry.setValue(LocalDate.now());

        btnpane.setOnMouseEntered(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(22);
                btnpane.setMinWidth(20.0);
                btn11.setMinWidth(20.0);

            }
        });
        btnpane.setOnMouseExited(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(14);
                btnpane.setMinWidth(10.0);
                btn11.setMinWidth(10.0);
            }
        });

        inner_anchor.setLeftAnchor(inner_anchor, 260d);
        drawer.setSidePane(nav_purch);
        drawer.open();

        MysqlCon objectMysqlCon = new MysqlCon();
        Statement objectStmt = objectMysqlCon.stmt;
        Connection objectCon = objectMysqlCon.con;
        PurchaseInvoice objsaleinvoice = new PurchaseInvoice();

        getlast_purchID = objsaleinvoice.getlatest_purchID(objectStmt,objectCon);
        int purchID_cnvrtint=0;
        if(getlast_purchID[0]==null)
        {
            getlast_purchID[0] = "0";
        }

            purchID_cnvrtint = Integer.parseInt(getlast_purchID[0]);
            purchID_cnvrtint = purchID_cnvrtint+1;
            txt_purchID.setText(String.valueOf(purchID_cnvrtint));



        ObservableList<String> prodPacking = FXCollections.observableArrayList();
        prodPacking.add( "Box");
        prodPacking.add("Packs");
        combo_packSize.setItems(prodPacking);
        combo_packSize.setValue("Packs");

        TableColumn productcompIDCOL = new TableColumn("Company ID");
        productcompIDCOL.setMinWidth(150);
        productcompIDCOL.setVisible(false);
        TableColumn productIDCOL = new TableColumn("Product ID");
        productIDCOL.setMinWidth(150);
        productIDCOL.setVisible(false);
        TableColumn productnameCOL = new TableColumn("Product Name");
        productnameCOL.setMinWidth(160);
        TableColumn productbatchCOL = new TableColumn("Batch No.");
        productbatchCOL.setMinWidth(100);
        TableColumn productquantCOL = new TableColumn("Quantity");
        productquantCOL.setMinWidth(100);
        TableColumn productDiscountCOL = new TableColumn("Discount");
        productDiscountCOL.setMinWidth(100);
        TableColumn productBonusCOL = new TableColumn("Bonus");
        productBonusCOL.setMinWidth(100);
        TableColumn productexpiryCOL = new TableColumn("Expiry");
        productexpiryCOL.setMinWidth(120);
        TableColumn productTotalCOL = new TableColumn("Total");
        productTotalCOL.setMinWidth(100);
        TableColumn productpacksizeCOL = new TableColumn("Pack Size");
        productpacksizeCOL.setMinWidth(10);
        productpacksizeCOL.setVisible(false);
        TableColumn productdiscAmountCOL = new TableColumn("Discount Amount");
        productdiscAmountCOL.setMinWidth(10);
        productdiscAmountCOL.setVisible(false);
        TableColumn productprodstockCOL = new TableColumn("Quantity in Stock");
        productprodstockCOL.setMinWidth(10);
        productprodstockCOL.setVisible(false);
        TableColumn productprodbonusstockCOL = new TableColumn("Bonus Quantity in Stock");
        productprodbonusstockCOL.setMinWidth(10);
        productprodbonusstockCOL.setVisible(false);
        TableColumn productEditCOL = new TableColumn("Actions");
        productEditCOL.setMinWidth(200);
        TableColumn firstEDITCol = new TableColumn("Edit");
        TableColumn secondDELETECol = new TableColumn("Delete");

        productEditCOL.getColumns().addAll(firstEDITCol, secondDELETECol);

        tableView.getColumns().addAll(productcompIDCOL,productIDCOL, productnameCOL,productbatchCOL, productquantCOL,productDiscountCOL,productBonusCOL,productexpiryCOL,productTotalCOL,productpacksizeCOL,productdiscAmountCOL,productprodstockCOL,productprodbonusstockCOL,productEditCOL);

        productcompIDCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("productCompID"));
        productIDCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("productID"));
        productnameCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("productName"));
        productbatchCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("productBatch"));
        productquantCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("productQuant"));
        productDiscountCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("productDisc"));
        productBonusCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("productBonus"));
        productexpiryCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("productExpiry"));
        productTotalCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("totalValue"));
        productpacksizeCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("packSize"));
        productdiscAmountCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("discAmount"));
        productprodstockCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("stockQuant"));
        productprodbonusstockCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("bonusstockQuant"));
        //START of Creating Delete Button
        secondDELETECol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Product,Boolean>, ObservableValue>() {
            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<Product, Boolean> param) {
                return new SimpleBooleanProperty(param.getValue()!=null);
            }

        });
        secondDELETECol.setCellFactory(new Callback<TableColumn<Product, Boolean>, TableCell<Product, Boolean>>() {
            @Override
            public TableCell<Product, Boolean> call(TableColumn<Product, Boolean> param) {
                return new ButtonCell(tableView);
            }
        });
        //END of Creating Delete Button
        //START of Creating EDIT Button
        firstEDITCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Product,Boolean>, ObservableValue>() {
            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<Product, Boolean> param) {
                return new SimpleBooleanProperty(param.getValue()!=null);
            }

        });
        firstEDITCol.setCellFactory(new Callback<TableColumn<Product, Boolean>, TableCell<Product, Boolean>>() {
            @Override
            public TableCell<Product, Boolean> call(TableColumn<Product, Boolean> param) {
                return new Editbtncell(tableView);
            }
        });
        //END of Creating EDIT Button

        tableView.setItems(data);
    }

    public void follow() throws IOException
    {
    /*PathTransition transition;
    transition = new PathTransition();
    Polyline mpoly = new Polyline();
    mpoly.getPoints().addAll(new Double[]{
            0.0,0.0,
            0.0,0.0,
            -200.0,0.0});
    transition.setNode(btn11);
    transition.setDuration(Duration.seconds(2));
    transition.setPath(mpoly);
    transition.play();*/
        if (drawer.isOpened()) {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(-250);
            transition.setByY(0);
            transition.setCycleCount(1);
            transition.play();
            drawer.close();
            FA_Icon.setGlyphName("ANGLE_DOUBLE_RIGHT");
            FA_Icon.setGlyphSize(14);
            //inner_anchor.setRightAnchor(inner_anchor,1d);


            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(-90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(1.31017);
            scale.setToY(1);



            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();
            //
            //inner_anchor.setLeftAnchor(inner_anchor, 5d);
        } else {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(250);
            transition.setByY(0);
            transition.setCycleCount(1);
            FA_Icon.setGlyphName("ANGLE_DOUBLE_LEFT");
            FA_Icon.setGlyphSize(14);
            drawer.open();

            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(0.99947777);
            scale.setToY(1);



            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();

        }


    }

    int EDIT_CHK_Value = 0;
    Alert a = new Alert(AlertType.NONE);
    ObservableList<Product> data = FXCollections.observableArrayList();
    int rowvalueAfterEdit = 0;
    /////////////////////////////////////////////////////insertnewbatch_ifnotavlbl////////////////////////// START of Button Code for Editing a Row
    private class Editbtncell extends TableCell<Product,Boolean>{
        final Button btncell = new Button("Edit");

        public  Editbtncell(final TableView view1){
            btncell.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    rowvalueAfterEdit = getTableRow().getIndex();
                    Product newobj = (Product) tableView.getItems().get(rowvalueAfterEdit);
                    txt_prodID.setText(newobj.getProductId());
                    txt_compID.setText(newobj.getProductCompID());
                    txt_prodName.setText(newobj.getProductName());
                    txt_Batchno.setText(newobj.getProductBatch());
                    txt_quant.setText(newobj.getProductQuant());
                    txt_bonus.setText(newobj.getProductBonus());
                    txt_discount.setText(newobj.getProductDisc());
                    //datepick_expiry.setValue(newobj.getProductExpiry().t);
                    lbl_totalAmount.setText(newobj.getTotalValue());
                    String prodrates_onEditclick[] = new String[1];
                    String comp_ids = txt_compID.getText();
                    String product_ids = txt_prodID.getText();


                    int intprod_id = Integer.parseInt(product_ids);

                    MysqlCon objMysqlCon = new MysqlCon();
                    Statement objStmt = objMysqlCon.stmt;
                    Connection objCon = objMysqlCon.con;
                    PurchaseInvoice objpurchinvoice = new PurchaseInvoice();
                    proddetails_onIDEnter = objpurchinvoice.get_prodDetailswithIDs(objStmt, objCon, intprod_id);
                    if (proddetails_onIDEnter[0] == null) {
                    } else {
                        String[] myArray = proddetails_onIDEnter[0].split("--");
                        lbl_retailPrice.setText(myArray[1]);
                        lbl_purchPrice.setText(myArray[2]);
                        lbl_tradePrice.setText(myArray[3]);
                        txt_compID.setText(myArray[4]);
                    }
                    txt_prodID.setEditable(false);
                    txt_prodName.setEditable(false);
                    EDIT_CHK_Value = 1;
                }
            });
        }
        @Override
        protected  void updateItem(Boolean n, boolean boln){
            super.updateItem(n,boln);
            if(boln){
                setGraphic(null);
            }
            else
            {
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setGraphic(btncell);
            }
        }
    }
    ///////////////////////////////////////////////////////////////////////////// END of Button Code for Editing a Row
    ////////////////////////////////////////////////////////////////////////////START of Button Code for Deleting a Row
    private class ButtonCell extends TableCell<Product, Boolean> {

        final Button CellButton = new Button("Delete");
        public ButtonCell(final TableView view) {
            CellButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    int selectedIndex = getTableRow().getIndex();
                    data.remove(selectedIndex);
                }
            });
        }
        @Override
        protected void updateItem(Boolean t, boolean bln) {
            super.updateItem(t, bln);
            if (bln) {
                setGraphic(null);
            } else {
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setGraphic(CellButton);
            }

        }
    }
    ///////////////////////////////////////////////////////////////////////////END of Button Code for Deleting a Row
    String[] getprevbatchwise_stockquant = new String[5];
    String[] getprev_stockquant = new String[5];
    String newdates;
    String newtimes;
    String getCompanyID;
    String proddetails[] = new String[1];
    public void savepurchinvo_btnclick() throws IOException{
        int counttableROWS = tableView.getItems().size();
        String[] getprodid_tableview = new String[counttableROWS];
        String[] getprodCompid_tableview = new String[counttableROWS];
        String[] getprodQuant_tableview = new String[counttableROWS];
        String[] getprodBonus_tableview = new String[counttableROWS];
        String[] getprodDisc_tableview = new String[counttableROWS];
        String[] getpacksize_tableview = new String[counttableROWS];
        String[] getquant_inStock_tableview = new String[counttableROWS];
        String[] getbonusquant_inStock_tableview = new String[counttableROWS];
        String[] getbatch_tableview = new String[counttableROWS];
        String[] getexpirydate = new String[counttableROWS];
        String[] getnetamount_perProd = new String[counttableROWS];
        String[] getgrossamount_perProd = new String[counttableROWS];
        String[] getdiscamount_perProd = new String[counttableROWS];
        int purchid_intval = Integer.parseInt(txt_purchID.getText());
        int suplierid_intval = Integer.parseInt(txt_supplierID.getText());
        Float netamount_floatval = Float.parseFloat(txt_totalAmount.getText());
        Float grossamount_floatval = Float.parseFloat(txt_grossAmount.getText());
        Float discountamount_floatval = Float.parseFloat(txt_totaldiscAmount.getText());
        for(int i =0;i<counttableROWS;i++)
        {
            Product ProductObject = (Product) tableView.getItems().get(i);
            getprodid_tableview[i] = ProductObject.getProductId();
            getprodCompid_tableview[i] = ProductObject.getProductCompID();
            getprodQuant_tableview[i] = ProductObject.getProductQuant();
            getprodBonus_tableview[i] = ProductObject.getProductBonus();
            getprodDisc_tableview[i] = ProductObject.getProductDisc();
            getpacksize_tableview[i] = ProductObject.getPackSize();
            getnetamount_perProd[i] = ProductObject.getTotalValue();
            getdiscamount_perProd[i]= ProductObject.getDiscAmount();
            float fltval_netamount_perprod = Float.parseFloat(getnetamount_perProd[i]);
            float fltval_discamount_perprod = Float.parseFloat(getdiscamount_perProd[i]);
            float sumof_net_disc = fltval_netamount_perprod+ fltval_discamount_perprod;

            getquant_inStock_tableview[i] = ProductObject.getStockQuant();
            getbonusquant_inStock_tableview[i] = ProductObject.getBonusstockQuant();
            getbatch_tableview[i] = ProductObject.getProductBatch();
            getexpirydate[i] = ProductObject.getProductExpiry();
            String[] breakexpiryDate_array = getexpirydate[i].split("-");
            int yearval = Integer.parseInt(breakexpiryDate_array[0]);

            int monthval = Integer.parseInt(breakexpiryDate_array[1]);
            int dateval = Integer.parseInt(breakexpiryDate_array[2]);
            int returnedBit = 0;
            datepick_expiry.setValue(LocalDate.of(yearval, monthval, dateval));
            LocalDate getexpirydates = datepick_expiry.getValue();
            String expirydate_newformat = getexpirydates.format(DateTimeFormatter.ofPattern("dd/MMM/yyyy"));

            int intvalue_prodID = Integer.parseInt(getprodid_tableview[i]);
            int intvalue_compID = Integer.parseInt(getprodCompid_tableview[i]);
            int intvalue_prodQuant = Integer.parseInt(getprodQuant_tableview[i]);
            int intvalue_prodBonus = Integer.parseInt(getprodBonus_tableview[i]);
            int intvalue_stockquantity = Integer.parseInt(getquant_inStock_tableview[i]);
            int intvalue_bonusstockquantity = Integer.parseInt(getbonusquant_inStock_tableview[i]);
            float floatvalue_discount = Float.parseFloat(getprodDisc_tableview[i]);
            LocalDate getdates = datepick_invoicedate.getValue();
            LocalTime gettimess = LocalTime.now();
            newtimes = gettimess.format(DateTimeFormatter.ofPattern("hh:mm:ss.SSS"));
            newdates = getdates.format(DateTimeFormatter.ofPattern("dd/MMM/yyyy"));
            int rentrned_qantity = 0;
            int rentrned_Bonus_qantity = 0;
            ///////////////////////////////////////INSERT DATA IN PURCHASE_INFO_DETAIL TABLE
            MysqlCon newobjMysqlCon1 = new MysqlCon();
            Statement newobjStmt1 = newobjMysqlCon1.stmt;
            Connection newobjCon1 = newobjMysqlCon1.con;
            PurchaseInvoice newobjsaleinvoice1 = new PurchaseInvoice();
            newobjsaleinvoice1.insertdata_RowWise(newobjStmt1, newobjCon1, txt_purchInvNo.getText() ,purchid_intval,intvalue_prodID,floatvalue_discount,intvalue_prodBonus,intvalue_prodQuant,getbatch_tableview[i],expirydate_newformat,sumof_net_disc,fltval_discamount_perprod,fltval_netamount_perprod,newdates,returnedBit,rentrned_qantity,rentrned_Bonus_qantity);

            ///////////////////////////////////////GET QUANTITY OF BATCH IF AVAILABLE IN batchwise STOCK TABLE
            MysqlCon objMysqlConnection = new MysqlCon();
            Statement objStatmt = objMysqlConnection.stmt;
            Connection objConnect1 = objMysqlConnection.con;
            PurchaseInvoice getbatchquant_object = new PurchaseInvoice();
            getprevbatchwise_stockquant =  getbatchquant_object.batch_quant(objStatmt, objConnect1, intvalue_prodID ,getbatch_tableview[i]);

            if(getprevbatchwise_stockquant[0] == null)
            {
                PurchaseInvoice addbatchquant_object = new PurchaseInvoice();
                MysqlCon MysqlConction = new MysqlCon();
                Statement Statmtobj1 = MysqlConction.stmt;
                Connection Connectobj1 = MysqlConction.con;
                proddetails = addbatchquant_object.get_prodDetailswithIDs(Statmtobj1,Connectobj1,intvalue_prodID);
                String[] SplitprodDetails = proddetails[0].split("--");
                //////add new batch in batchwise_stock in this condition
                MysqlCon MysqlConnection = new MysqlCon();
                Statement Statmt1 = MysqlConnection.stmt;
                Connection Connect1 = MysqlConnection.con;
                addbatchquant_object.insertnewbatch_ifnotavlbl(Statmt1, Connect1, intvalue_prodID ,getbatch_tableview[i],intvalue_prodQuant,intvalue_prodBonus,SplitprodDetails[1],SplitprodDetails[3],SplitprodDetails[2],expirydate_newformat,newdates,newtimes);
            }
            else {
                String[] myArray = getprevbatchwise_stockquant[0].split("--");
                int specificbatch_quant = Integer.parseInt( myArray[0]);
                int specificbatch_bonus = Integer.parseInt( myArray[1]);

                int addition_prodQuant = 0;
                int addition_prodBonus = 0;
                addition_prodBonus = intvalue_prodBonus + specificbatch_bonus;
                addition_prodQuant = intvalue_prodQuant + specificbatch_quant;
                ///////////////////////////////////////UPDATE batchwise STOCK TABLE AFTER ADDING NEW PURCHASE
                MysqlCon objectMysqlCon1 = new MysqlCon();
                Statement objectStmt1 = objectMysqlCon1.stmt;
                Connection objectCon1 = objectMysqlCon1.con;
                PurchaseInvoice objectpurchinvoice1 = new PurchaseInvoice();
                objectpurchinvoice1.Addition_BatchStock(objectStmt1, objectCon1, intvalue_prodID, addition_prodQuant ,addition_prodBonus,getbatch_tableview[i]);
            }

            ///////////////////////////////////////GET QUANTITY OF PRODUCT IF AVAILABLE IN PRODUCT STOCK TABLE
            MysqlCon objMysqlConnects = new MysqlCon();
            Statement objStatmts = objMysqlConnects.stmt;
            Connection objConnects = objMysqlConnects.con;
            PurchaseInvoice getprodquant_objects = new PurchaseInvoice();
            getprev_stockquant =  getprodquant_objects.getprodstockquant_withprodID(objStatmts, objConnects, intvalue_prodID );
            String product_status = "Active";
            if(getprev_stockquant[0] == null)
            {
                //////add PRODUCT QUANTITY ENTRY in PRODUCTS_STOCK in this condition
                MysqlCon MysqlConnections = new MysqlCon();
                Statement Statmts1 = MysqlConnections.stmt;
                Connection Connects1 = MysqlConnections.con;
                PurchaseInvoice addquant_object = new PurchaseInvoice();
                addquant_object.insertprodQUANT_ifnotavlbl(Statmts1, Connects1, intvalue_prodID ,intvalue_prodQuant,intvalue_prodBonus,product_status);
            }
            else
            {
                ///////////////////////////////////////UPDATE PRODUCT STOCK TABLE AFTER ADDING NEW PURCHASE
                int subtracted_prodQuant = 0;
                int subtracted_prodBonus = 0;
                if(intvalue_prodQuant>=0){
                    subtracted_prodQuant = intvalue_stockquantity + intvalue_prodQuant;
                }
                if(intvalue_prodBonus>=0){
                    subtracted_prodBonus = intvalue_bonusstockquantity + intvalue_prodBonus;
                }
                MysqlCon objMysqlCon1 = new MysqlCon();
                Statement objStmt1 = objMysqlCon1.stmt;
                Connection objCon1 = objMysqlCon1.con;
                PurchaseInvoice objsaleinvoice1 = new PurchaseInvoice();
                objsaleinvoice1.addtition_quantFROMStock(objStmt1, objCon1, intvalue_prodID, subtracted_prodQuant ,subtracted_prodBonus);
            }

        }
        LocalDate getdates = datepick_invoicedate.getValue();
        String newdate = getdates.format(DateTimeFormatter.ofPattern("dd/MMM/yyyy"));

        LocalTime gettime = LocalTime.now();
        String newtime = gettime.format(DateTimeFormatter.ofPattern("hh:mm a"));
        int userID = 1;
        ///////////////////////////////////////ADD PRODUCT PURCHASE IN PURCHASE_INFO TABLE AFTER ADDING NEW PURCHASE
        MysqlCon objMysqlConn = new MysqlCon();
        Statement objStatmt = objMysqlConn.stmt;
        Connection objConn = objMysqlConn.con;
        PurchaseInvoice purch_Object = new PurchaseInvoice();
        purch_Object.insertpurchInvoiceInfo(objStatmt, objConn,purchid_intval,Integer.parseInt(getCompanyID),txt_purchInvNo.getText(),suplierid_intval,newdates,grossamount_floatval,discountamount_floatval,netamount_floatval,newdate,newtime,newdate,newtime,userID);



        Parent root = FXMLLoader.load(getClass().getResource("../view/purch_invoice.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);

    }
    //////////////////////////////////////////////////////////////////////////////////START of Add Button Click Code
    public void  addbtn_click() throws IOException{
        if(lbl_totalAmount.getText() == "0.00")
        {
            if(txt_discount.getText()=="0")
            {
                if (txt_quant.getText() != "")
                {
                    float getretail_floatval = Float.parseFloat(lbl_tradePrice.getText());
                    int intcnvrt_quant = Integer.parseInt(txt_quant.getText());

                    float totalquantity_price = intcnvrt_quant * getretail_floatval;
                    lbl_totalAmount.setText(String.valueOf(totalquantity_price));
                    btn_add.requestFocus();
                }
            }
            else
            {
                if (!txt_quant.getText().isEmpty() && txt_discount.getText()!="0")
                {
                    float intgetretail_price = Integer.parseInt(lbl_tradePrice.getText());
                    int intcnvrt_quant = Integer.parseInt(txt_quant.getText());
                    float intdiscount_value = Integer.parseInt(txt_discount.getText());
                    float totalquantity_price = intcnvrt_quant * intgetretail_price;
                    float disc_firstVal = (totalquantity_price * intdiscount_value) / 100;
                    lbl_discAmount.setText(String.valueOf(disc_firstVal));
                    float final_discVal = totalquantity_price - disc_firstVal;
                    lbl_totalAmount.setText(String.valueOf(final_discVal));
                    btn_add.requestFocus();
                }
                else
                {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setHeaderText("Input Field Empty");
                    a.setContentText("Please Enter Quantity");
                    a.show();
                    txt_quant.requestFocus();
                }
            }
        }
        if(EDIT_CHK_Value == 1)
        {
            Product newobj = (Product) tableView.getItems().get(rowvalueAfterEdit);

            newobj.setProductBatch(txt_Batchno.getText());
            newobj.setProductBonus(txt_bonus.getText());
            newobj.setProductDisc(txt_discount.getText());
            newobj.setProductQuant(txt_quant.getText());
            newobj.setProductExpiry(datepick_expiry.getValue().toString());

            newobj.setTotalValue(lbl_totalAmount.getText());
            newobj.setPackSize(combo_packSize.getValue().toString());
            newobj.setDiscAmount(lbl_discAmount.getText());
            newobj.setStockQuant(lbl_stockQaunt.getText());
            newobj.setBonusstockQuant(lbl_stockBONUSQaunt.getText());
            tableView.refresh();
            txt_prodID.setEditable(true);
            txt_prodName.setEditable(true);
            txt_prodID.setText("");
            txt_prodName.setText("");
            txt_quant.setText("");
            txt_bonus.setText("");
            txt_Batchno.setText("");
            txt_discount.setText("");
            txt_compID.setText("");
            combo_packSize.setValue("Packs");
            lbl_totalAmount.setText("0.00");
            lbl_tradePrice.setText("0.00");
            lbl_purchPrice.setText("0.00");
            lbl_retailPrice.setText("0.00");
            lbl_discAmount.setText("0.00");
            datepick_expiry.setValue(LocalDate.now());
            lbl_stockQaunt.setText("-");
            lbl_stockBONUSQaunt.setText("-");
            ///////////////////////////START OF CALCULATIONS TO GET TOTAL amount and total Discount amount OF ALL PRODUCT ADDED IN TBALEVIEW
            Float sumval =0.0f;
            Float finalsumofDiscount2 = 0.0f;
            Float nettotalamount = 0.0f;
            for(int i = 0; i<tableView.getItems().size();i++)
            {
                Product newwobj = (Product) tableView.getItems().get(i);
                float newval = Float.parseFloat(newwobj.getTotalValue());
                float finalsumofDiscount = Float.parseFloat(newwobj.getDiscAmount());
                finalsumofDiscount2 = finalsumofDiscount2 + finalsumofDiscount;
                sumval = sumval + newval;
                nettotalamount = sumval + finalsumofDiscount2;
            }
            txt_totalAmount.setText(String.valueOf(sumval));
            txt_totaldiscAmount.setText(String.valueOf(finalsumofDiscount2));
            txt_grossAmount.setText(String.valueOf(nettotalamount));
            txt_compID.requestFocus();
            /////////////////////////////////END OF CALCULATIONS TO GET TOTAL amount and total Discount amount OF ALL PRODUCT ADDED IN TBALEVIEW
            //txt_quant1.setText(String.valueOf(sumval));
            txt_prodID.requestFocus();
            EDIT_CHK_Value = 0;
        }
        else {
            if (txt_prodName.getText().isEmpty()) {
                txt_prodName.setText("ERROR");
            } else if (txt_prodID.getText().isEmpty() || txt_quant.getText().isEmpty()) {
                txt_prodID.setText("ERROR");
            } else if (txt_quant.getText().isEmpty()) {
                txt_quant.setText("ERROR");
            }
            /*if(txt_bonus.getText().isEmpty()){
                txt_bonus.setText("ERROR");
            }*/
            else {
                getCompanyID = txt_compID.getText();
                Product objnew = new Product();
                objnew.setProductId(txt_prodID.getText());
                objnew.setProductCompID(txt_compID.getText());
                objnew.setProductName(txt_prodName.getText());
                objnew.setProductBatch(txt_Batchno.getText());
                objnew.setProductQuant(txt_quant.getText());
                objnew.setProductBonus(txt_bonus.getText());
                objnew.setProductDisc(txt_discount.getText());
                objnew.setProductExpiry(datepick_expiry.getValue().toString());
                objnew.setTotalValue(lbl_totalAmount.getText());
                objnew.setPackSize(combo_packSize.getValue().toString());
                objnew.setDiscAmount(lbl_discAmount.getText());
                objnew.setStockQuant(lbl_stockQaunt.getText());
                objnew.setBonusstockQuant(lbl_stockBONUSQaunt.getText());

                tableView.getItems().add(objnew);
                txt_prodID.setText("");
                txt_prodName.setText("");
                txt_quant.setText("");
                txt_bonus.setText("0");
                txt_discount.setText("0");
                txt_compID.setText("");
                txt_Batchno.setText("");
                combo_packSize.setValue("Packs");
                lbl_totalAmount.setText("0.00");
                lbl_tradePrice.setText("0.00");
                lbl_purchPrice.setText("0.00");
                lbl_retailPrice.setText("0.00");
                lbl_discAmount.setText("0.00");
                datepick_expiry.setValue(LocalDate.now());
                lbl_stockQaunt.setText("-");
                lbl_stockBONUSQaunt.setText("-");

                ///////////////////////////START OF CALCULATIONS TO GET TOTAL amount and total Discount amount OF ALL PRODUCT ADDED IN TBALEVIEW
                Float sumval =0.0f;
                Float finalsumofDiscount2 = 0.0f;
                Float nettotalamount = 0.0f;
                for(int i = 0; i<tableView.getItems().size();i++)
                {
                    Product newwobj = (Product) tableView.getItems().get(i);
                    float newval = Float.parseFloat(newwobj.getTotalValue());
                    float finalsumofDiscount = Float.parseFloat(newwobj.getDiscAmount());
                    finalsumofDiscount2 = finalsumofDiscount2 + finalsumofDiscount;
                    sumval = sumval + newval;
                    nettotalamount = sumval + finalsumofDiscount2;
                }
                txt_totalAmount.setText(String.valueOf(sumval));
                txt_totaldiscAmount.setText(String.valueOf(finalsumofDiscount2));
                txt_grossAmount.setText(String.valueOf(nettotalamount));
                txt_compID.requestFocus();
                /////////////////////////////////END OF CALCULATIONS TO GET TOTAL amount and total Discount amount OF ALL PRODUCT ADDED IN TBALEVIEW
                //txt_quant1.setText(String.valueOf(sumval));
                txt_prodID.requestFocus();

            }
        }
    }
   /////////////////////////////////////////////////////////////////////////////////////END of Add Button Click Code
    public void  changeRate_click() {
        int selectedProdID = 0;
        String selectedBatch = "";
        if(!txt_prodID.getText().isEmpty())
        {
            selectedProdID = Integer.parseInt(txt_prodID.getText());
        }
        else
        {
            selectedProdID = 0;
        }
        if(!txt_prodID.getText().isEmpty())
        {
            selectedBatch = txt_Batchno.getText();
        }
        else
        {
            selectedBatch = null;
        }


        PurchaseInvoice objpurchInvo = new PurchaseInvoice();
        objpurchInvo.rateChange(selectedProdID);
    }
   //////////////////////////////////////////////////////////////////////////////////START of Add Button ENTER Press Code
   public void  addbtn_enterpress(KeyEvent event) throws IOException {
       if (event.getCode() == KeyCode.ENTER ) {
           if (lbl_totalAmount.getText() == "0.00") {
               if (txt_discount.getText() == "0") {
                   if (txt_quant.getText() != "") {
                       float getretail_floatval = Float.parseFloat(lbl_tradePrice.getText());
                       int intcnvrt_quant = Integer.parseInt(txt_quant.getText());

                       float totalquantity_price = intcnvrt_quant * getretail_floatval;
                       lbl_totalAmount.setText(String.valueOf(totalquantity_price));
                       btn_add.requestFocus();
                   }
               } else {
                   if (!txt_quant.getText().isEmpty() && txt_discount.getText() != "0") {
                       float intgetretail_price = Integer.parseInt(lbl_tradePrice.getText());
                       int intcnvrt_quant = Integer.parseInt(txt_quant.getText());
                       float intdiscount_value = Integer.parseInt(txt_discount.getText());
                       float totalquantity_price = intcnvrt_quant * intgetretail_price;
                       float disc_firstVal = (totalquantity_price * intdiscount_value) / 100;
                       lbl_discAmount.setText(String.valueOf(disc_firstVal));
                       float final_discVal = totalquantity_price - disc_firstVal;
                       lbl_totalAmount.setText(String.valueOf(final_discVal));
                       btn_add.requestFocus();
                   } else {
                       a.setAlertType(Alert.AlertType.ERROR);
                       a.setHeaderText("Input Field Empty");
                       a.setContentText("Please Enter Quantity");
                       a.show();
                       txt_quant.requestFocus();
                   }
               }
           }
           if (EDIT_CHK_Value == 1) {
               Product newobj = (Product) tableView.getItems().get(rowvalueAfterEdit);

               newobj.setProductBatch(txt_Batchno.getText());
               newobj.setProductBonus(txt_bonus.getText());
               newobj.setProductDisc(txt_discount.getText());
               newobj.setProductQuant(txt_quant.getText());
               newobj.setProductExpiry(datepick_expiry.getValue().toString());
               newobj.setTotalValue(lbl_totalAmount.getText());
               newobj.setPackSize(combo_packSize.getValue().toString());
               newobj.setDiscAmount(lbl_discAmount.getText());
               newobj.setStockQuant(lbl_stockQaunt.getText());
               newobj.setBonusstockQuant(lbl_stockBONUSQaunt.getText());
               tableView.refresh();
               txt_prodID.setEditable(true);
               txt_prodName.setEditable(true);
               txt_prodID.setText("");
               txt_prodName.setText("");
               txt_quant.setText("");
               txt_bonus.setText("");
               txt_Batchno.setText("");
               txt_discount.setText("");
               txt_compID.setText("");
               combo_packSize.setValue("Packs");
               lbl_totalAmount.setText("0.00");
               lbl_tradePrice.setText("0.00");
               lbl_purchPrice.setText("0.00");
               lbl_retailPrice.setText("0.00");
               lbl_discAmount.setText("0.00");
               datepick_expiry.setValue(LocalDate.now());
               lbl_stockQaunt.setText("-");
               lbl_stockBONUSQaunt.setText("-");
               ///////////////////////////START OF CALCULATIONS TO GET TOTAL amount and total Discount amount OF ALL PRODUCT ADDED IN TBALEVIEW
               Float sumval = 0.0f;
               Float finalsumofDiscount2 = 0.0f;
               Float nettotalamount = 0.0f;
               for (int i = 0; i < tableView.getItems().size(); i++) {
                   Product newwobj = (Product) tableView.getItems().get(i);
                   float newval = Float.parseFloat(newwobj.getTotalValue());
                   float finalsumofDiscount = Float.parseFloat(newwobj.getDiscAmount());
                   finalsumofDiscount2 = finalsumofDiscount2 + finalsumofDiscount;
                   sumval = sumval + newval;
                   nettotalamount = sumval + finalsumofDiscount2;
               }
               txt_totalAmount.setText(String.valueOf(sumval));
               txt_totaldiscAmount.setText(String.valueOf(finalsumofDiscount2));
               txt_grossAmount.setText(String.valueOf(nettotalamount));
               txt_compID.requestFocus();
               /////////////////////////////////END OF CALCULATIONS TO GET TOTAL amount and total Discount amount OF ALL PRODUCT ADDED IN TBALEVIEW
               //txt_quant1.setText(String.valueOf(sumval));
               txt_prodID.requestFocus();
               EDIT_CHK_Value = 0;
           } else {
               if (txt_prodName.getText().isEmpty()) {
                   txt_prodName.setText("ERROR");
               } else if (txt_prodID.getText().isEmpty() || txt_quant.getText().isEmpty()) {
                   txt_prodID.setText("ERROR");
               } else if (txt_quant.getText().isEmpty()) {
                   txt_quant.setText("ERROR");
               }
            /*if(txt_bonus.getText().isEmpty()){
                txt_bonus.setText("ERROR");
            }*/
               else {
                   Product objnew = new Product();
                   getCompanyID = txt_compID.getText();
                   objnew.setProductId(txt_prodID.getText());
                   objnew.setProductCompID(txt_compID.getText());
                   objnew.setProductName(txt_prodName.getText());
                   objnew.setProductBatch(txt_Batchno.getText());
                   objnew.setProductQuant(txt_quant.getText());
                   objnew.setProductBonus(txt_bonus.getText());
                   objnew.setProductDisc(txt_discount.getText());
                   objnew.setProductExpiry(datepick_expiry.getValue().toString());
                   objnew.setTotalValue(lbl_totalAmount.getText());
                   objnew.setPackSize(combo_packSize.getValue().toString());
                   objnew.setDiscAmount(lbl_discAmount.getText());
                   objnew.setStockQuant(lbl_stockQaunt.getText());
                   objnew.setBonusstockQuant(lbl_stockBONUSQaunt.getText());

                   tableView.getItems().add(objnew);
                   txt_prodID.setText("");
                   txt_prodName.setText("");
                   txt_quant.setText("");
                   txt_bonus.setText("0");
                   txt_discount.setText("0");
                   txt_compID.setText("");
                   txt_Batchno.setText("");
                   combo_packSize.setValue("Packs");
                   lbl_totalAmount.setText("0.00");
                   lbl_tradePrice.setText("0.00");
                   lbl_purchPrice.setText("0.00");
                   lbl_retailPrice.setText("0.00");
                   lbl_discAmount.setText("0.00");
                   datepick_expiry.setValue(LocalDate.now());
                   lbl_stockQaunt.setText("-");
                   lbl_stockBONUSQaunt.setText("-");

                   ///////////////////////////START OF CALCULATIONS TO GET TOTAL amount and total Discount amount OF ALL PRODUCT ADDED IN TBALEVIEW
                   Float sumval = 0.0f;
                   Float finalsumofDiscount2 = 0.0f;
                   Float nettotalamount = 0.0f;
                   for (int i = 0; i < tableView.getItems().size(); i++) {
                       Product newwobj = (Product) tableView.getItems().get(i);
                       float newval = Float.parseFloat(newwobj.getTotalValue());
                       float finalsumofDiscount = Float.parseFloat(newwobj.getDiscAmount());
                       finalsumofDiscount2 = finalsumofDiscount2 + finalsumofDiscount;
                       sumval = sumval + newval;
                       nettotalamount = sumval + finalsumofDiscount2;
                   }
                   txt_totalAmount.setText(String.valueOf(sumval));
                   txt_totaldiscAmount.setText(String.valueOf(finalsumofDiscount2));
                   txt_grossAmount.setText(String.valueOf(nettotalamount));
                   txt_compID.requestFocus();
                   /////////////////////////////////END OF CALCULATIONS TO GET TOTAL amount and total Discount amount OF ALL PRODUCT ADDED IN TBALEVIEW
                   //txt_quant1.setText(String.valueOf(sumval));
                   txt_prodID.requestFocus();

               }
           }
       }
   }
    /////////////////////////////////////////////////////////////////////////////////////END of Add Button ENTER Press Code
    /////////////////////////////////////////////////////////////////////////// START of ENter Press on Quantity
    public void enterpress_quant(KeyEvent event) throws IOException{
        if(event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB)
        {
            if(txt_discount.getText().isEmpty())
            {
                txt_discount.setText("0");
            }
            if(txt_bonus.getText().isEmpty())
            {
                txt_bonus.setText("0");
            }
            int getdisc_intval = Integer.parseInt(txt_discount.getText());
            if (getdisc_intval == 0 ) {
                if (!txt_quant.getText().isEmpty()) {
                    float intgetpurch_price = Float.parseFloat(lbl_purchPrice.getText());
                    int intcnvrt_quant = Integer.parseInt(txt_quant.getText());

                    float totalquantity_price = intcnvrt_quant * intgetpurch_price;
                    lbl_totalAmount.setText(String.valueOf(totalquantity_price));
                    datepick_expiry.requestFocus();
                }
                else
                {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setHeaderText("Error!");
                    a.setContentText("Please Enter Value Greater than ");
                    a.show();
                    txt_quant.requestFocus();
                }
            }
            else {
                if (!txt_quant.getText().isEmpty() && !txt_discount.getText().isEmpty())
                {
                    if(!txt_quant.getText().equals("0") && txt_discount.getText().equals("0"))
                    {
                        float intgetpurch_price = Float.parseFloat(lbl_purchPrice.getText());
                        int intcnvrt_quant = Integer.parseInt(txt_quant.getText());
                        float intdiscount_value = Float.parseFloat(txt_discount.getText());
                        float totalquantity_price = intcnvrt_quant * intgetpurch_price;
                        float disc_firstVal = (totalquantity_price * intdiscount_value) / 100;
                        lbl_discAmount.setText(String.valueOf(disc_firstVal));
                        float final_discVal = totalquantity_price - disc_firstVal;
                        lbl_totalAmount.setText(String.valueOf(final_discVal));
                        datepick_expiry.requestFocus();

                    }

                }
            }
        }

    }
    /////////////////////////////////////////////////////////////////////////// END of ENter Press on Quantity
    /////////////////////////////////////////////////////////////////////////// START of ENter Press on Expirydate
    public void keypress_onExpirydate(KeyEvent event) throws IOException
    {
        if(event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB) {
            btn_add.requestFocus();
        }
    }
    /////////////////////////////////////////////////////////////////////////// END of ENter Press on Expirydate
    /////////////////////////////////////////////////////////////////////////// START of Enter Press on Supplier ID
    String[] suppdetails_onIDEnter = new String[1];
    public void keypress_SuppID(KeyEvent event) throws IOException{
        if(event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB) {
            String txtbx_suppID = txt_supplierID.getText();

            MysqlCon objMysqlCon = new MysqlCon();
            Statement objStmt = objMysqlCon.stmt;
            Connection objCon = objMysqlCon.con;
            PurchaseInvoice objpurchinvoice = new PurchaseInvoice();
            suppdetails_onIDEnter = objpurchinvoice.get_SuppDetails(objStmt, objCon, txtbx_suppID);
            if (suppdetails_onIDEnter[0] == null) {
                a.setAlertType(AlertType.ERROR);
                a.setHeaderText("Error!");
                a.setContentText("Supplier ID not found.");
                a.show();
                txt_purchInvNo.requestFocus();
            } else {
                String[] myArray = suppdetails_onIDEnter[0].split("--");
                txt_supplierName.setText(myArray[0]);
                txt_purchInvNo.requestFocus();
            }
        }
    }
    /////////////////////////////////////////////////////////////////////////// END of Enter Press on Supplier ID
    /////////////////////////////////////////////////////////////////////////// START of Enter Press on Purchase Invoice
    String alreadyExist_purchID[] = new String[5];
    public void purchInvo_enterpress(KeyEvent event) throws IOException{
        if(event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB)
        {
            if(txt_purchInvNo.getText().isEmpty())
            {
                a.setAlertType(AlertType.ERROR);
                a.setHeaderText("Input Field Empty");
                a.setContentText("Please Enter Invoice Number.");
                a.show();
                txt_purchInvNo.requestFocus();
            }
            else
            {

                MysqlCon objectMysqlCon = new MysqlCon();
                Statement objectStmt = objectMysqlCon.stmt;
                Connection objectCon = objectMysqlCon.con;
                PurchaseInvoice objectpurchinvoice = new PurchaseInvoice();

                alreadyExist_purchID = objectpurchinvoice.cnfrmInvoNum_already(objectStmt, objectCon,   txt_purchInvNo.getText());
                if(alreadyExist_purchID[0]==null){
                    txt_prodID.requestFocus();
                }
                else
                {
                    a.setAlertType(AlertType.ERROR);
                    a.setHeaderText("ERROR!");
                    a.setContentText("Purchase Invoice Number Already in Records.");
                    a.show();
                    txt_purchInvNo.requestFocus();
                }

            }

        }
    }
    /////////////////////////////////////////////////////////////////////////// END of Enter Press on Purchase Invoice
    /////////////////////////////////////////////////////////////////////////// START of Enter Press on Discount
    public  void  enterpress_txtdiscount(KeyEvent event) throws IOException{
        if(event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB)
        {
            txt_quant.requestFocus();
        }
    }
    /////////////////////////////////////////////////////////////////////////// END of Enter Press on Discount
    /////////////////////////////////////////////////////////////////////////// START of Enter Press on Bonus
    public  void  bonustxt_enter(KeyEvent event) throws  IOException {
        if(event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB) {

               txt_discount.requestFocus();
        }
    }
    /////////////////////////////////////////////////////////////////////////// End of Enter Press on Bonus
    /////////////////////////////////////////////////////////////////////////// START of Enter Press on Product ID
    String prodquantinstock_prodIDEnter[] = new String[1];
    String proddetails_onIDEnter[] = new String[1];
    String proddetails_withID[] = new String[1];
    String batch_policy;
    ArrayList<String> batchlist = new ArrayList<>();
    String getProductsbatch;
    public void enterpress_prodID(KeyEvent event) throws IOException{
        if(event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB) {

            if (txt_prodID.getText().isEmpty() ) {
                a.setAlertType(AlertType.ERROR);
                a.setHeaderText("Input Field Empty");
                a.setContentText("Please Enter Product ID");
                a.show();
                txt_prodID.requestFocus();
            }

            else
            {
                int intprod_id = Integer.parseInt(txt_prodID.getText());
                PurchaseInvoice objpurchinvoice = new PurchaseInvoice();
                MysqlCon myobjectSQL = new MysqlCon();
                Statement mystatmnOBJ = myobjectSQL.stmt;
                Connection myCnnctionOBJ = myobjectSQL.con;

                batch_policy = objpurchinvoice.getbatchSelectionpolicy(mystatmnOBJ, myCnnctionOBJ);
                if (batch_policy.isEmpty()) {
                }
                else {
                    String[] split_batchSelection = batch_policy.split(" ");

                    MysqlCon myobjectmySQL = new MysqlCon();
                    Statement statmnOBJ = myobjectmySQL.stmt;
                    Connection CnnctionOBJ = myobjectmySQL.con;
                    batchlist = objpurchinvoice.getprod_otherbatchs(statmnOBJ, CnnctionOBJ, intprod_id);

                    if (batchlist.isEmpty()) {
                        MysqlCon myobjtmySQL = new MysqlCon();
                        Statement statmnttOBJ = myobjtmySQL.stmt;
                        Connection ConnctionOBJ = myobjtmySQL.con;
                        proddetails_withID = objpurchinvoice.get_prodDetailswithIDs(statmnttOBJ, ConnctionOBJ, intprod_id);
                        if (proddetails_withID[0] == null) {
                            a.setAlertType(AlertType.ERROR);
                            a.setHeaderText("Incorrect Value");
                            a.setContentText("Product ID not Found.");
                            a.show();
                            txt_prodID.clear();
                            txt_prodID.requestFocus();
                        } else {
                            String[] myArray = proddetails_withID[0].split("--");
                            txt_prodName.setText(myArray[0]);
                            lbl_retailPrice.setText(myArray[1]);
                            lbl_purchPrice.setText(myArray[2]);
                            lbl_tradePrice.setText(myArray[3]);
                            txt_compID.setText(myArray[4]);
                            txt_Batchno.requestFocus();
                        }
                    }
                    else
                        {
                        String[] getall_dates = new String[20];
                        String[] getall_time = new String[20];
                        for (int f = 0; f < batchlist.size(); f++) {
                            String[] split = batchlist.get(f).split("--");
                            if (split_batchSelection[0].equals("Entry")) {
                                getall_dates[f] = split[1];
                                getall_time[f] = split[2];
                            }
                            if (split_batchSelection[0].equals("Expiry")) {
                                getall_dates[f] = split[3];
                                getall_time[f] = split[2];
                            }

                        }
                        List<String> myList = new ArrayList<>();
                        for (int g = 0; g < batchlist.size(); g++) {
                            if (split_batchSelection[1].equals("FIFO") || split_batchSelection[1].equals("LILO")) {
                                String cmbine = getall_dates[g] + " " + getall_time[g];
                                myList.add(cmbine);
                                Collections.sort(myList);
                            }
                            if (split_batchSelection[1].equals("LIFO") || split_batchSelection[1].equals("FIFO")) {
                                String cmbine = getall_dates[g] + " " + getall_time[g];
                                myList.add(cmbine);
                                Collections.sort(myList, Collections.reverseOrder());
                            }
                        }
                        ///////////////////////////////////////////SELECT THE POLICY OF FIFO OR LIFO ETC
                        String cnvrtdatetime = String.valueOf(myList.get(0));
                        String[] splitDate_Time = cnvrtdatetime.split(" ");
                        String getscndbatchno = "";
                        if (split_batchSelection[0].equals("Entry")) {
                            MysqlCon mysqlOBJct = new MysqlCon();
                            Statement mystamtOBJct = mysqlOBJct.stmt;
                            Connection mysqlConnobjct = mysqlOBJct.con;
                            getscndbatchno = objpurchinvoice.getprodbatch_entry(mystamtOBJct, mysqlConnobjct, intprod_id, splitDate_Time[0], splitDate_Time[1]);
                        }
                        if (split_batchSelection[0].equals("Expiry")) {
                            MysqlCon mysqlOBJct = new MysqlCon();
                            Statement mystamtOBJct = mysqlOBJct.stmt;
                            Connection mysqlConnobjct = mysqlOBJct.con;
                            getscndbatchno = objpurchinvoice.getprodbatch_expiry(mystamtOBJct, mysqlConnobjct, intprod_id, splitDate_Time[0], splitDate_Time[1]);
                        }
                        if (getscndbatchno.isEmpty()) {
                        } else {
                            getProductsbatch = getscndbatchno;
                        }

                        String[] batch_quantity = getProductsbatch.split("-");

                        MysqlCon objMysqlCon = new MysqlCon();
                        Statement objStmt = objMysqlCon.stmt;
                        Connection objCon = objMysqlCon.con;
                        proddetails_onIDEnter = objpurchinvoice.get_prodDetailswithID_batch(objStmt, objCon, intprod_id, batch_quantity[0]);
                        if (proddetails_onIDEnter[0] == null) {
                            a.setAlertType(AlertType.ERROR);
                            a.setHeaderText("Incorrect Value");
                            a.setContentText("Product ID not Found.");
                            a.show();
                            txt_prodID.clear();
                            txt_prodID.requestFocus();
                        } else {
                            String[] myArray = proddetails_onIDEnter[0].split("--");
                            txt_prodName.setText(myArray[0]);
                            lbl_retailPrice.setText(myArray[1]);
                            lbl_purchPrice.setText(myArray[2]);
                            lbl_tradePrice.setText(myArray[3]);
                            txt_compID.setText(myArray[4]);
                            txt_Batchno.requestFocus();


                            MysqlCon objectMysqlCon = new MysqlCon();
                            Statement objectStmt = objectMysqlCon.stmt;
                            Connection objectCon = objectMysqlCon.con;
                            PurchaseInvoice objectsaleinvoice = new PurchaseInvoice();

                            prodquantinstock_prodIDEnter = objectsaleinvoice.getprodstockquant_withprodID(objectStmt, objectCon, intprod_id);
                            if (prodquantinstock_prodIDEnter[0] == null) {
                                lbl_stockQaunt.setText("0");
                                lbl_stockBONUSQaunt.setText("0");
                            } else {
                                String[] prodStock_quantarr = prodquantinstock_prodIDEnter[0].split("--");
                                lbl_stockQaunt.setText(prodStock_quantarr[0]);
                                lbl_stockBONUSQaunt.setText(prodStock_quantarr[1]);
                            }
                        }
                    }


                }
            }
        }
    }
    /////////////////////////////////////////////////////////////////////////// END of Enter Press on Product ID
    /////////////////////////////////////////////////////////////////////////// START of ENter Press on Product Name
    String proddetails_onprodNameEnter[] = new String[1];
    public void enterpress_prodName(KeyEvent event) throws IOException {
        if(event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB) {
            if(!txt_prodName.getText().equals(""))
            {
                    String product_name = txt_prodName.getText();
                    MysqlCon objMysqlCon = new MysqlCon();
                    Statement objStmt = objMysqlCon.stmt;
                    Connection objCon = objMysqlCon.con;
                    SaleInvoice objsaleinvoice = new SaleInvoice();
                    proddetails_onprodNameEnter = objsaleinvoice.get_prodDetailswithname(objStmt, objCon, product_name);
                    if (proddetails_onprodNameEnter[0] == null) {
                        a.setAlertType(Alert.AlertType.ERROR);
                        a.setHeaderText("Error!");
                        a.setContentText("Wrong Product Name.");
                        a.show();
                        txt_prodName.clear();
                        txt_prodName.requestFocus();
                    }
                    else {
                        String[] myArray = proddetails_onprodNameEnter[0].split("--");
                        txt_compID.setText(myArray[1]);
                        txt_prodID.setText(myArray[0]);
                        lbl_retailPrice.setText(myArray[2]);
                        lbl_purchPrice.setText(myArray[3]);
                        lbl_tradePrice.setText(myArray[4]);

                        int intprod_id = Integer.parseInt(txt_prodID.getText());

                        MysqlCon objectMysqlCon = new MysqlCon();
                        Statement objectStmt = objectMysqlCon.stmt;
                        Connection objectCon = objectMysqlCon.con;
                        PurchaseInvoice objectsaleinvoice = new PurchaseInvoice();

                        prodquantinstock_prodIDEnter = objectsaleinvoice.getprodstockquant_withprodID(objectStmt, objectCon,   intprod_id);
                        if(prodquantinstock_prodIDEnter[0]==null){
                            lbl_stockQaunt.setText("0");
                            lbl_stockBONUSQaunt.setText("0");
                        }
                        else
                        {
                            String[] prodStock_quantarr = prodquantinstock_prodIDEnter[0].split("--");
                            lbl_stockQaunt.setText(prodStock_quantarr[0]);
                            lbl_stockBONUSQaunt.setText(prodStock_quantarr[1]);
                        }

                    }

                txt_prodName.requestFocus();
            }

            else
            {
                a.setAlertType(Alert.AlertType.ERROR);
                a.setHeaderText("Error!");
                a.setContentText("Product Name not entered.");
                a.show();
                txt_prodName.requestFocus();
            }
        }
    }
    /////////////////////////////////////////////////////////////////////////// END of Enter Press on Product Name

    /////////////////////////////////////////////////////////////////////////// START of ENter Press on Supplier Name
    String Supdetails_onSUPPNameEnter[] = new String[1];
    public void enterpress_SupplierName(KeyEvent event) throws IOException {
        if(event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB) {
            if(!txt_supplierName.getText().equals(""))
            {
                String Supp_name = txt_supplierName.getText();
                MysqlCon objMysqlCon = new MysqlCon();
                Statement objStmt = objMysqlCon.stmt;
                Connection objCon = objMysqlCon.con;
                PurchaseInvoice prchInvoObj = new PurchaseInvoice();
                Supdetails_onSUPPNameEnter = prchInvoObj.getSuppDetails_SupName(objStmt, objCon, Supp_name);
                if (Supdetails_onSUPPNameEnter[0] == null) {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setHeaderText("Error!");
                    a.setContentText("Wrong Supplier Name.");
                    a.show();
                    txt_supplierName.clear();
                    txt_supplierName.requestFocus();
                }
                else {
                    txt_supplierID.setText(Supdetails_onSUPPNameEnter[0]);
                    /*
                    String[] myArray = proddetails_onSUPPNameEnter[0].split("--");
                    txt_compID.setText(myArray[1]);
                    lbl_retailPrice.setText(myArray[2]);
                    lbl_purchPrice.setText(myArray[3]);
                    lbl_tradePrice.setText(myArray[4]);*/
                }

                txt_purchInvNo.requestFocus();
            }

            else
            {
                a.setAlertType(Alert.AlertType.ERROR);
                a.setHeaderText("Error!");
                a.setContentText("Product Name not entered.");
                a.show();
                txt_prodName.requestFocus();
            }
        }
    }
    /////////////////////////////////////////////////////////////////////////// END of Enter Press on Supplier Name

/////////////////////////////////////////////////////////////////////////// START of KEYPress on Batch No
    public void toupperCase(KeyEvent event) throws IOException{
        txt_Batchno.setTextFormatter(new TextFormatter<>((change) -> {
            change.setText(change.getText().toUpperCase());
            return change;
        }));
        if(event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB) {
            txt_bonus.requestFocus();
        }
    }
    /////////////////////////////////////////////////////////////////////////// END of KEYPress on Batch No
}
