package controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.util.StringConverter;
import model.*;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

public class DealerReportDetail implements Initializable {

    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private Label lbl_dealer_id;

    @FXML
    private Label lbl_dealer_name;

    @FXML
    private Label lbl_dealer_contact;

    @FXML
    private Label lbl_dealer_area;

    @FXML
    private Label lbl_dealer_address;

    @FXML
    private Label lbl_dealer_type;

    @FXML
    private Label lbl_dealer_cnic;

    @FXML
    private Label lbl_dealer_licnum;

    @FXML
    private Label lbl_dealer_licexp;

    @FXML
    private Label lbl_dealer_created;

    @FXML
    private Label lbl_dealer_updated;

    @FXML
    private Label lbl_dealer_status;

    @FXML
    private ImageView img_user;

    @FXML
    private AnchorPane filter_pane;

    @FXML
    private JFXDatePicker txt_from_date;

    @FXML
    private JFXDatePicker txt_to_date;

    @FXML
    private JFXComboBox<String> txt_day;

    @FXML
    private HBox summary_hbox1;

    @FXML
    private Label lbl_booked;

    @FXML
    private Label lbl_delivered;

    @FXML
    private Label lbl_returned;

    @FXML
    private Label lbl_booking_price;

    @FXML
    private Label lbl_discount_given;

    @FXML
    private Label lbl_items_returned;

    @FXML
    private HBox summary_hbox2;

    @FXML
    private Label lbl_return_price;

    @FXML
    private Label lbl_cash_collection;

    @FXML
    private Label lbl_cash_return;

    @FXML
    private Label lbl_cash_waiveoff;

    @FXML
    private Label lbl_cash_pending;
    @FXML
    private TableView<DealerReportDetailInfo> table_dealerreportlog;

    @FXML
    private TableColumn<DealerReportDetailInfo, String> sr_no;

    @FXML
    private TableColumn<DealerReportDetailInfo, String> date;

    @FXML
    private TableColumn<DealerReportDetailInfo, String> day;

    @FXML
    private TableColumn<DealerReportDetailInfo, String> booking_invoice;

    @FXML
    private TableColumn<DealerReportDetailInfo, String> delivered_invoice;

    @FXML
    private TableColumn<DealerReportDetailInfo, String> returned_invoice;

    @FXML
    private TableColumn<DealerReportDetailInfo, String> booking_price;

    @FXML
    private TableColumn<DealerReportDetailInfo, String> discount_given;

    @FXML
    private TableColumn<DealerReportDetailInfo, String> quantity_returned;

    @FXML
    private TableColumn<DealerReportDetailInfo, String> return_price;

    @FXML
    private TableColumn<DealerReportDetailInfo, String> cash_collection;

    @FXML
    private TableColumn<DealerReportDetailInfo, String> cash_return;

    @FXML
    private TableColumn<DealerReportDetailInfo, String> cash_waive_off;

    @FXML
    private TableColumn<DealerReportDetailInfo, String> cash_pending;

    @FXML
    private TableColumn<DealerReportDetailInfo, String> operations;

    @FXML
    private StackPane stackPane;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    @FXML
    private BorderPane brdrpane_navfinances;

    @FXML
    private BorderPane menu_bar;

    private DealerInfo objDealerInfo;
    private String currentDealerId;
    private String selectedImagePath;
    private ArrayList<String> dealerInfo;
    private ObservableList<DealerReportDetailInfo> dealerDetails;
    private DealerReportDetailInfo objDealerReportDetailInfo;
    ArrayList<ArrayList<String>> dealerReportData;

    static float totalBooked = 0;
    static float totalDelivered = 0;
    static float totalReturned = 0;
    static float bookingPrice = 0;
    static float discountGiven = 0;
    static float itemsReturned = 0;
    static float returnPrice = 0;
    static float cashCollection = 0;
    static float cashReturn = 0;
    static float cashWaiveOff = 0;
    static float cashPending = 0;

    public static String txtFromDate = "";
    public static String txtToDate = "";
    public static String txtDay = "All";
    public static boolean filter;

    MysqlCon objMysqlCon1;
    MysqlCon objMysqlCon2;
    MysqlCon objMysqlCon3;
    Statement objStmt1;
    Statement objStmt2;
    Statement objStmt3;
    Connection objCon;

    public static MenuButton btnView;
    SimpleDateFormat fmt = new SimpleDateFormat("dd/MMM/yyyy");

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnFinance.setStyle("-fx-background-color: #47ab1e");
        objMysqlCon1 = new MysqlCon();
        objMysqlCon2 = new MysqlCon();
        objMysqlCon3 = new MysqlCon();
        objStmt1 = objMysqlCon1.stmt;
        objStmt2 = objMysqlCon2.stmt;
        objStmt3 = objMysqlCon3.stmt;
        objCon = objMysqlCon1.con;
        objDealerInfo = new DealerInfo();
        objDealerReportDetailInfo = new DealerReportDetailInfo();
        dealerReportData = new ArrayList<>();
        if(!DealerGeneralReportInfo.dealerReportId.equals(""))
        {
            currentDealerId = DealerGeneralReportInfo.dealerReportId;
            DealerGeneralReportInfo.dealerReportId = "";
        }
        dealerInfo = objDealerInfo.getDealerDetail(currentDealerId);
        lbl_dealer_id.setText(((dealerInfo.get(0) == null) ? "N/A" : dealerInfo.get(0)));
        lbl_dealer_name.setText(dealerInfo.get(1));
        lbl_dealer_contact.setText(dealerInfo.get(2));
        lbl_dealer_area.setText((dealerInfo.get(3) == null) ? "N/A" : dealerInfo.get(3));
        lbl_dealer_address.setText(dealerInfo.get(4));
        lbl_dealer_type.setText((dealerInfo.get(5) == null) ? "N/A" : dealerInfo.get(5));
        lbl_dealer_cnic.setText(dealerInfo.get(6));
        lbl_dealer_licnum.setText((dealerInfo.get(7) == null) ? "N/A" : dealerInfo.get(7));
        lbl_dealer_licexp.setText((dealerInfo.get(8) == null) ? "N/A" : dealerInfo.get(8));
        lbl_dealer_created.setText(dealerInfo.get(9));
        lbl_dealer_updated.setText((dealerInfo.get(10) == null) ? "N/A" : dealerInfo.get(10));
        lbl_dealer_status.setText(dealerInfo.get(11));
        if(!dealerInfo.get(12).equals(""))
        {
            selectedImagePath = dealerInfo.get(12);
            File file = new File(selectedImagePath);
            boolean exists = file.exists();
            if(exists)
            {
                Image image = new Image(file.toURI().toString());
                img_user.setImage(image);
            }
        }

        txt_from_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        txt_to_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        date.setCellValueFactory(new PropertyValueFactory<>("date"));
        day.setCellValueFactory(new PropertyValueFactory<>("day"));
        booking_invoice.setCellValueFactory(new PropertyValueFactory<>("bookingInvoice"));
        delivered_invoice.setCellValueFactory(new PropertyValueFactory<>("deliveredInvoice"));
        returned_invoice.setCellValueFactory(new PropertyValueFactory<>("returnedInvoice"));
        booking_price.setCellValueFactory(new PropertyValueFactory<>("bookingPrice"));
        discount_given.setCellValueFactory(new PropertyValueFactory<>("discountGiven"));
        quantity_returned.setCellValueFactory(new PropertyValueFactory<>("quantityReturned"));
        return_price.setCellValueFactory(new PropertyValueFactory<>("returnPrice"));
        cash_collection.setCellValueFactory(new PropertyValueFactory<>("cashCollection"));
        cash_return.setCellValueFactory(new PropertyValueFactory<>("cashReturn"));
        cash_waive_off.setCellValueFactory(new PropertyValueFactory<>("cashWaiveOff"));
        cash_pending.setCellValueFactory(new PropertyValueFactory<>("cashPending"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_dealerreportlog.setItems(parseUserList());
        lbl_booked.setText("Booked\n"+String.format("%,.0f", totalBooked));
        lbl_delivered.setText("Delivered\n"+String.format("%,.0f", totalDelivered));
        lbl_returned.setText("Returned\n"+String.format("%,.0f", totalReturned));
        lbl_booking_price.setText("Booking Price\n"+String.format("%,.0f", bookingPrice));
        lbl_discount_given.setText("Discount Given\n"+String.format("%,.0f", discountGiven));
        lbl_items_returned.setText("Items Returned\n"+String.format("%,.0f", itemsReturned));
        lbl_return_price.setText("Return Price\n"+String.format("%,.0f", returnPrice));
        lbl_cash_collection.setText("Cash Collection\n"+String.format("%,.0f", cashCollection));
        lbl_cash_return.setText("Cash Return\n"+String.format("%,.0f", cashReturn));
        lbl_cash_waiveoff.setText("Cash Waived Off\n"+String.format("%,.0f", cashWaiveOff));
        lbl_cash_pending.setText("Cash Pending\n"+String.format("%,.0f", cashPending));
    }

    private ObservableList<DealerReportDetailInfo> parseUserList() {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        dealerDetails = FXCollections.observableArrayList();

        if (filter) {
            try {
                dealerReportData = objDealerReportDetailInfo.getDealerReportDetailSearch(objStmt1, objStmt2, objStmt3, objCon, currentDealerId, txtFromDate, txtToDate, txtDay);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (txtFromDate != null && !txtFromDate.equals("")) {
                txt_from_date.setValue(GlobalVariables.LOCAL_DATE(txtFromDate));
            }
            if (txtToDate != null && !txtToDate.equals("")) {
                txt_to_date.setValue(GlobalVariables.LOCAL_DATE(txtToDate));
            }
            txt_day.setValue(txtDay);
            filter = false;
            filter_pane.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
            txtFromDate = "";
            txtToDate = "";
            txtDay = "All";
        } else {
            try {
                dealerReportData = objDealerReportDetailInfo.getDealerReportDetail(objStmt1, objStmt2, objStmt3, objCon, currentDealerId);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }


        for (int i = 0; i < dealerReportData.size(); i++)
        {
            if(dealerReportData.get(i).get(2) != null && !dealerReportData.get(i).get(2).equals("-"))
            {
                String[] bookings = dealerReportData.get(i).get(2).split("\n");
                totalBooked += bookings.length;
            }
            if(dealerReportData.get(i).get(3) != null && !dealerReportData.get(i).get(3).equals("-"))
            {
                String[] deliveries = dealerReportData.get(i).get(3).split("\n");
                totalDelivered += deliveries.length;
            }
            if(dealerReportData.get(i).get(4) != null && !dealerReportData.get(i).get(4).equals("-"))
            {
                String[] returns = dealerReportData.get(i).get(4).split("\n");
                totalReturned += returns.length;
            }
            bookingPrice += (dealerReportData.get(i).get(5) != null && !dealerReportData.get(i).get(5).equals("-")) ? Float.parseFloat(dealerReportData.get(i).get(5)) : 0;
            discountGiven += (dealerReportData.get(i).get(6) != null && !dealerReportData.get(i).get(6).equals("-")) ? Float.parseFloat(dealerReportData.get(i).get(6)) : 0;
            itemsReturned += (dealerReportData.get(i).get(7) != null && !dealerReportData.get(i).get(7).equals("-")) ? Float.parseFloat(dealerReportData.get(i).get(7)) : 0;
            returnPrice += (dealerReportData.get(i).get(8) != null && !dealerReportData.get(i).get(8).equals("-")) ? Float.parseFloat(dealerReportData.get(i).get(8)) : 0;
            cashCollection += (dealerReportData.get(i).get(9) != null && !dealerReportData.get(i).get(9).equals("-")) ? Float.parseFloat(dealerReportData.get(i).get(9)) : 0;
            cashReturn += (dealerReportData.get(i).get(10) != null && !dealerReportData.get(i).get(10).equals("-")) ? Float.parseFloat(dealerReportData.get(i).get(10)) : 0;
            cashWaiveOff += (dealerReportData.get(i).get(11) != null && !dealerReportData.get(i).get(11).equals("-")) ? Float.parseFloat(dealerReportData.get(i).get(11)) : 0;
            cashPending += (dealerReportData.get(i).get(12) != null && !dealerReportData.get(i).get(12).equals("-")) ? Float.parseFloat(dealerReportData.get(i).get(12)) : 0;
            dealerDetails.add(new DealerReportDetailInfo(String.valueOf(i+1), ((dealerReportData.get(i).get(0) == null || dealerReportData.get(i).get(0).equals("")) ? "N/A" : dealerReportData.get(i).get(0)), dealerReportData.get(i).get(1), dealerReportData.get(i).get(2), dealerReportData.get(i).get(3), dealerReportData.get(i).get(4), dealerReportData.get(i).get(5), dealerReportData.get(i).get(6), dealerReportData.get(i).get(7), dealerReportData.get(i).get(8), dealerReportData.get(i).get(9), dealerReportData.get(i).get(10), dealerReportData.get(i).get(11), dealerReportData.get(i).get(12)));
        }
        return dealerDetails;
    }

    @FXML
    void btnReset(ActionEvent event) {
        DealerGeneralReportInfo.dealerReportId = currentDealerId;
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/dealer_report_detail.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void btnSearch(ActionEvent event) {
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date selectedDate = null;
        if(txt_from_date.getValue() != null)
        {
            txtFromDate = txt_from_date.getValue().toString();
            try {
                selectedDate = sdf.parse(txtFromDate);
                txtFromDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if(txt_to_date.getValue() != null)
        {
            txtToDate = txt_to_date.getValue().toString();
            try {
                selectedDate = sdf.parse(txtToDate);
                txtToDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        txtDay = txt_day.getValue();
        if(txtDay == null)
        {
            txtDay = "All";
        }
        filter = true;

        DealerGeneralReportInfo.dealerReportId = currentDealerId;
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/dealer_report_detail.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void follow(MouseEvent event) {

    }


    @FXML
    void showBooked(MouseEvent event) {

    }

    @FXML
    void showBookingPrice(MouseEvent event) {

    }

    @FXML
    void showCollection(MouseEvent event) {

    }

    @FXML
    void showDelivered(MouseEvent event) {

    }

    @FXML
    void showDiscountGiven(MouseEvent event) {

    }

    @FXML
    void showItemsReturned(MouseEvent event) {

    }

    @FXML
    void showPending(MouseEvent event) {

    }

    @FXML
    void showReturn(MouseEvent event) {

    }

    @FXML
    void showReturnPrice(MouseEvent event) {

    }

    @FXML
    void showReturned(MouseEvent event) {

    }

    @FXML
    void showWaiveOff(MouseEvent event) {

    }
}
