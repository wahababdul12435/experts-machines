package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import model.GlobalVariables;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;


public class NavLive implements Initializable {

    @FXML
    private AnchorPane rootpane;

    @FXML
    private VBox vbox_style;

    @FXML
    private Button btn_live_dashboard;

    @FXML
    private Button btn_stock;

    @FXML
    private Button btn_expired_items;

    @FXML
    private Button btn_google_map;

    @FXML
    private Button btn_mail;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        DashboardLive.btnView = btn_live_dashboard;
        ViewStock.btnView = btn_stock;
        SendMail.btnView = btn_mail;
    }

    @FXML
    void liveDashboard(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/dashboard_live.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void viewStock(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_stock.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void mail(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/send_mail.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void viewExpiredItems(ActionEvent event) throws IOException {

    }

    @FXML
    void googleMap(ActionEvent event) throws IOException {

    }
}
