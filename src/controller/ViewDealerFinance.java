package controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.util.StringConverter;
import model.*;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

public class ViewDealerFinance implements Initializable {

    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private TableView<ViewDealerFinanceInfo> table_dealerfinancelog;

    @FXML
    private TableColumn<ViewDealerFinanceInfo, String> sr_no;

    @FXML
    private TableColumn<ViewDealerFinanceInfo, String> dealer_name;

    @FXML
    private TableColumn<ViewDealerFinanceInfo, String> payment_date;

    @FXML
    private TableColumn<ViewDealerFinanceInfo, String> payment_day;

    @FXML
    private TableColumn<ViewDealerFinanceInfo, String> dealer_contact;

    @FXML
    private TableColumn<ViewDealerFinanceInfo, String> area_name;

    @FXML
    private TableColumn<ViewDealerFinanceInfo, String> dealer_type;

    @FXML
    private TableColumn<ViewDealerFinanceInfo, String> cash_collection;

    @FXML
    private TableColumn<ViewDealerFinanceInfo, String> cash_return;

    @FXML
    private TableColumn<ViewDealerFinanceInfo, String> cash_waived_off;

    @FXML
    private TableColumn<ViewDealerFinanceInfo, String> cash_pending;

    @FXML
    private TableColumn<ViewDealerFinanceInfo, String> user_name;

    @FXML
    private TableColumn<ViewDealerFinanceInfo, String> operations;

    @FXML
    private AnchorPane filter_pane;

    @FXML
    private HBox filter_hbox1;

    @FXML
    private JFXTextField txt_dealer_id;

    @FXML
    private JFXTextField txt_dealer_name;

    @FXML
    private JFXTextField txt_area_name;

    @FXML
    private JFXComboBox<String> txt_dealer_type;

    @FXML
    private HBox filter_hbox2;

    @FXML
    private JFXDatePicker txt_from_date;

    @FXML
    private JFXDatePicker txt_to_date;

    @FXML
    private JFXComboBox<String> txt_day;

    @FXML
    private JFXComboBox<String> txt_cash_type;

    @FXML
    private StackPane stackPane;

    @FXML
    private HBox summary_hbox1;

    @FXML
    private Label lbl_cash_collection;

    @FXML
    private Label lbl_cash_return;

    @FXML
    private Label lbl_cash_waived_off;

    @FXML
    private Label lbl_cash_pending;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    @FXML
    private BorderPane brdrpane_navfinances;

    @FXML
    private BorderPane menu_bar;

    private ObservableList<ViewDealerFinanceInfo> paymentDetails;
    private ViewDealerFinanceInfo objViewDealerFinanceInfo;
    ArrayList<ArrayList<String>> paymentData;

    float totalCollection = 0;
    float totalReturn = 0;
    float totalWaivedOff = 0;
    float totalPending = 0;

    public static String txtDealerId = "";
    public static String txtDealerName = "";
    public static String txtAreaName = "";
    public static String txtDealerType = "All";
    public static String txtFromDate = "";
    public static String txtToDate = "";
    public static String txtDay = "All";
    public static String txtCashType = "All";
    public static boolean filter;

    private String summary = "";

    MysqlCon objMysqlCon1;
    Statement objStmt1;
    Connection objCon;

    public static Button btnView;
    SimpleDateFormat fmt = new SimpleDateFormat("dd/MMM/yyyy");

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnFinance.setStyle("-fx-background-color: #47ab1e");
        objMysqlCon1 = new MysqlCon();
        objStmt1 = objMysqlCon1.stmt;
        objCon = objMysqlCon1.con;
        objViewDealerFinanceInfo = new ViewDealerFinanceInfo();
        ViewDealerFinanceInfo.stackPane = stackPane;

        paymentData = new ArrayList<>();

        txt_from_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        txt_to_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        payment_date.setCellValueFactory(new PropertyValueFactory<>("paymentDate"));
        payment_day.setCellValueFactory(new PropertyValueFactory<>("paymentDay"));
        dealer_name.setCellValueFactory(new PropertyValueFactory<>("dealerName"));
        dealer_contact.setCellValueFactory(new PropertyValueFactory<>("dealerContact"));
        area_name.setCellValueFactory(new PropertyValueFactory<>("areaName"));
        dealer_type.setCellValueFactory(new PropertyValueFactory<>("dealerType"));
        cash_collection.setCellValueFactory(new PropertyValueFactory<>("cashCollection"));
        cash_return.setCellValueFactory(new PropertyValueFactory<>("cashReturn"));
        cash_waived_off.setCellValueFactory(new PropertyValueFactory<>("cashWaivedOff"));
        cash_pending.setCellValueFactory(new PropertyValueFactory<>("cashPending"));
        user_name.setCellValueFactory(new PropertyValueFactory<>("userName"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_dealerfinancelog.setItems(parseUserList());
        lbl_cash_collection.setText("Collection\n"+String.format("%,.0f", totalCollection));
        lbl_cash_return.setText("Return\n"+String.format("%,.0f", totalReturn));
        lbl_cash_waived_off.setText("Waived Off\n"+String.format("%,.0f", totalWaivedOff));
        lbl_cash_pending.setText("Pending\n"+String.format("%,.0f", totalPending));
    }

    private ObservableList<ViewDealerFinanceInfo> parseUserList() {

        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        paymentDetails = FXCollections.observableArrayList();

        if (filter) {
            paymentData = objViewDealerFinanceInfo.getDealerFinanceInfoSearch(objStmt1, objCon, txtDealerId, txtDealerName, txtAreaName, txtDealerType, txtFromDate, txtToDate, txtDay, txtCashType);
            txt_dealer_id.setText(txtDealerId);
            txt_dealer_name.setText(txtDealerName);
            txt_area_name.setText(txtAreaName);
            txt_dealer_type.setValue(txtDealerType);
            if (txtFromDate != null && !txtFromDate.equals("")) {
                txt_from_date.setValue(GlobalVariables.LOCAL_DATE(txtFromDate));
            }
            if (txtToDate != null && !txtToDate.equals("")) {
                txt_to_date.setValue(GlobalVariables.LOCAL_DATE(txtToDate));
            }
            txt_day.setValue(txtDay);
            txt_cash_type.setValue(txtCashType);
            filter = false;
            filter_pane.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
            txtFromDate = "";
            txtToDate = "";
            txtDay = "All";
            txtCashType = "All";
        } else if(summary.equals("")){
            paymentData = objViewDealerFinanceInfo.getDealerFinanceInfo(objStmt1, objCon, GlobalVariables.getStDate(), GlobalVariables.getStDate());
        }

        if (txtToDate != null && !txtToDate.equals("")) {
            txt_to_date.setValue(GlobalVariables.LOCAL_DATE(txtToDate));
            cash_pending.setText("Cash Pending\non\n"+txtToDate);
        }
        else
        {
            txtToDate = GlobalVariables.getStDate();
            cash_pending.setText("Cash Pending\non\n"+txtToDate);
        }
        int num = 1;
        for (int i = 0; i < paymentData.size(); i++)
        {
            totalCollection += (paymentData.get(i).get(8) != null && !paymentData.get(i).get(8).equals("-")) ? Float.parseFloat(paymentData.get(i).get(8)) : 0;
            totalReturn += (paymentData.get(i).get(9) != null && !paymentData.get(i).get(9).equals("-")) ? Float.parseFloat(paymentData.get(i).get(9)) : 0;
            totalWaivedOff += (paymentData.get(i).get(10) != null && !paymentData.get(i).get(10).equals("-")) ? Float.parseFloat(paymentData.get(i).get(10)) : 0;
            totalPending += (paymentData.get(i).get(11) != null && !paymentData.get(i).get(11).equals("-")) ? Float.parseFloat(paymentData.get(i).get(11)) : 0;
            if(summary.equals("Collection") && !paymentData.get(i).get(8).equals("-"))
            {
                paymentDetails.add(new ViewDealerFinanceInfo(String.valueOf(num), ((paymentData.get(i).get(0) == null || paymentData.get(i).get(0).equals("")) ? "N/A" : paymentData.get(i).get(0)), paymentData.get(i).get(1), paymentData.get(i).get(2), paymentData.get(i).get(3), paymentData.get(i).get(4), paymentData.get(i).get(5), paymentData.get(i).get(6), paymentData.get(i).get(7), paymentData.get(i).get(8), paymentData.get(i).get(9), paymentData.get(i).get(10), paymentData.get(i).get(11), paymentData.get(i).get(12), paymentData.get(i).get(13)));
                num++;
            }
            else if(summary.equals("Return") && !paymentData.get(i).get(9).equals("-"))
            {
                paymentDetails.add(new ViewDealerFinanceInfo(String.valueOf(num), ((paymentData.get(i).get(0) == null || paymentData.get(i).get(0).equals("")) ? "N/A" : paymentData.get(i).get(0)), paymentData.get(i).get(1), paymentData.get(i).get(2), paymentData.get(i).get(3), paymentData.get(i).get(4), paymentData.get(i).get(5), paymentData.get(i).get(6), paymentData.get(i).get(7), paymentData.get(i).get(8), paymentData.get(i).get(9), paymentData.get(i).get(10), paymentData.get(i).get(11), paymentData.get(i).get(12), paymentData.get(i).get(13)));
                num++;
            }
            else if(summary.equals("Waived Off") && !paymentData.get(i).get(10).equals("-"))
            {
                paymentDetails.add(new ViewDealerFinanceInfo(String.valueOf(num), ((paymentData.get(i).get(0) == null || paymentData.get(i).get(0).equals("")) ? "N/A" : paymentData.get(i).get(0)), paymentData.get(i).get(1), paymentData.get(i).get(2), paymentData.get(i).get(3), paymentData.get(i).get(4), paymentData.get(i).get(5), paymentData.get(i).get(6), paymentData.get(i).get(7), paymentData.get(i).get(8), paymentData.get(i).get(9), paymentData.get(i).get(10), paymentData.get(i).get(11), paymentData.get(i).get(12), paymentData.get(i).get(13)));
                num++;
            }
            else if(summary.equals("Pending") && !paymentData.get(i).get(11).equals("0"))
            {
                paymentDetails.add(new ViewDealerFinanceInfo(String.valueOf(num), ((paymentData.get(i).get(0) == null || paymentData.get(i).get(0).equals("")) ? "N/A" : paymentData.get(i).get(0)), paymentData.get(i).get(1), paymentData.get(i).get(2), paymentData.get(i).get(3), paymentData.get(i).get(4), paymentData.get(i).get(5), paymentData.get(i).get(6), paymentData.get(i).get(7), paymentData.get(i).get(8), paymentData.get(i).get(9), paymentData.get(i).get(10), paymentData.get(i).get(11), paymentData.get(i).get(12), paymentData.get(i).get(13)));
                num++;
            }
            else if(summary.equals(""))
            {
                paymentDetails.add(new ViewDealerFinanceInfo(String.valueOf(i+1), ((paymentData.get(i).get(0) == null || paymentData.get(i).get(0).equals("")) ? "N/A" : paymentData.get(i).get(0)), paymentData.get(i).get(1), paymentData.get(i).get(2), paymentData.get(i).get(3), paymentData.get(i).get(4), paymentData.get(i).get(5), paymentData.get(i).get(6), paymentData.get(i).get(7), paymentData.get(i).get(8), paymentData.get(i).get(9), paymentData.get(i).get(10), paymentData.get(i).get(11), paymentData.get(i).get(12), paymentData.get(i).get(13)));
            }
        }
        return paymentDetails;
    }

    @FXML
    void btnReset(ActionEvent event) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/view_dealer_finance.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void btnSearch(ActionEvent event) {
        txtDealerId = txt_dealer_id.getText();
        txtDealerName = txt_dealer_name.getText();
        txtAreaName = txt_area_name.getText();
        txtDealerType = txt_dealer_type.getValue();
        if(txtDealerType == null)
        {
            txtDealerType = "All";
        }
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date selectedDate = null;
        if(txt_from_date.getValue() != null)
        {
            txtFromDate = txt_from_date.getValue().toString();
            try {
                selectedDate = sdf.parse(txtFromDate);
                txtFromDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if(txt_to_date.getValue() != null)
        {
            txtToDate = txt_to_date.getValue().toString();
            try {
                selectedDate = sdf.parse(txtToDate);
                txtToDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        txtDay = txt_day.getValue();
        if(txtDay == null)
        {
            txtDay = "All";
        }
        txtCashType = txt_cash_type.getValue();
        if(txtCashType == null)
        {
            txtCashType = "All";
        }
        filter = true;

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/view_dealer_finance.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }


    @FXML
    void addCash(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/add_dealer_finance.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void follow(MouseEvent event) {

    }

    @FXML
    void showCollection(MouseEvent event) {
        summary = "Collection";
        table_dealerfinancelog.setItems(parseUserList());
    }

    @FXML
    void showReturn(MouseEvent event) {
        summary = "Return";
        table_dealerfinancelog.setItems(parseUserList());
    }

    @FXML
    void showWaiveOff(MouseEvent event) {
        summary = "Waived Off";
        table_dealerfinancelog.setItems(parseUserList());
    }

    @FXML
    void showPending(MouseEvent event) {
        summary = "Pending";
        table_dealerfinancelog.setItems(parseUserList());
    }
}
