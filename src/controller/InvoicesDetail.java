package controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.transitions.hamburger.HamburgerBackArrowBasicTransition;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.util.Duration;
import model.*;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;
import org.controlsfx.control.CheckTreeView;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

public class InvoicesDetail implements Initializable {

    @FXML    private Button save;
    @FXML    private TableView<InvoicesDetailInfo> table_invoicesdetail;
    @FXML    private TableColumn<InvoicesDetailInfo, String> sr_no;
    @FXML    private TableColumn<InvoicesDetailInfo, String> order_id;
    @FXML    private TableColumn<InvoicesDetailInfo, String> dealer_id;
    @FXML    private TableColumn<InvoicesDetailInfo, String> deale_name;
    @FXML    private TableColumn<InvoicesDetailInfo, String> final_price;
    @FXML    private TableColumn<InvoicesDetailInfo, String> date;
    @FXML    private TableColumn<InvoicesDetailInfo, String> operations;

    @FXML   private DatePicker datePick_summaryFrom;
    @FXML   private DatePicker datePick_summaryTo;
    @FXML   private TextField txt_InvoNum_To;
    @FXML   private TextField txt_InvoNum_From;
    @FXML    private BorderPane menu_bar;
    @FXML    private JFXDrawer drawer;
    @FXML    private BorderPane nav_sales;
    @FXML    private AnchorPane inner_anchor;
    @FXML    private JFXComboBox<String> combo_invoice_status;
    @FXML    private Button btn11;
    @FXML    private Pane btnpane;
    @FXML    private CheckTreeView<String> treeview;
    @FXML    private FontAwesomeIconView FA_Icon;

    public ObservableList<InvoicesDetailInfo> invoicesDetailList;
    InvoiceInfo objInvoiceInfo;
    public ArrayList<InvoiceInfo> invoiceData;
    ArrayList<String> Areaslist = new ArrayList<>();
    ArrayList<String> SubAreaslist = new ArrayList<>();
    ArrayList<String> getcheckedareas = new ArrayList<>();
    int k =0;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        MysqlCon newobjMysqlConnc = new MysqlCon();
        Statement objectStattmt = newobjMysqlConnc.stmt;
        Connection objectConnnec = newobjMysqlConnc.con;
        DrawingSummaryInfo objctdrwngSummary = new DrawingSummaryInfo();

        Areaslist = objctdrwngSummary.getAreas(objectStattmt,objectConnnec);

        CheckBoxTreeItem<String> root = new CheckBoxTreeItem<String>("Select All");
        for(int i=0;i<Areaslist.size();i++)
        {
            String[] splitAreas = Areaslist.get(i).split("--");

            CheckBoxTreeItem<String> node1 = new CheckBoxTreeItem<String>(splitAreas[1]);
            MysqlCon objMysqlConnc = new MysqlCon();
            Statement objctStattmt = objMysqlConnc.stmt;
            Connection objctConnnec = objMysqlConnc.con;
            DrawingSummaryInfo objctdrawngSummary = new DrawingSummaryInfo();

            int intval_Areaid = Integer.parseInt(splitAreas[0]);
            SubAreaslist = objctdrawngSummary.getSubAreas(objctStattmt,objctConnnec,intval_Areaid);

            for(int j=0;j<SubAreaslist.size();j++)
            {
                String[] splitSubAreas = SubAreaslist.get(j).split("--");
                CheckBoxTreeItem<String> node2 = new CheckBoxTreeItem<String>(splitSubAreas[1]);
                node1.setExpanded(false);
                node1.getChildren().add(node2);
            }
            root.getChildren().add(node1);
            treeview.setRoot(root);
            root.setExpanded(true);

            treeview.setShowRoot(true);
        }

        treeview.getCheckModel().getCheckedItems().addListener(new ListChangeListener<TreeItem<String>>() {
            @Override
            public void onChanged(Change<? extends TreeItem<String>> c) {
                //System.out.println(treeview.getCheckModel().getCheckedItems());
                String nval = treeview.getCheckModel().getCheckedItems().toString();
                String valueAndIndex = c.toString();
                String[] splitedvalueAndIndex = valueAndIndex.split(" ]] ");
                String[] splitedValue = splitedvalueAndIndex[0].split(": ");
                String[] getfunctionAndIndex = splitedvalueAndIndex[1].split(" at ");
                String[] getindx = getfunctionAndIndex[1].split(" ");
                String strngindex = getindx[0];
                int indexvalue= Integer.parseInt(strngindex);
                if(getfunctionAndIndex[0].equals("added"))
                {
                    getcheckedareas.add(splitedValue[1]);
                    k++;
                }
                if(getfunctionAndIndex[0].equals("removed"))
                {
                    getcheckedareas.remove(indexvalue);
                    k--;
                }
            }
        });
        combo_invoice_status.setValue("Pending");
        objInvoiceInfo = new InvoiceInfo();
        invoiceData = new ArrayList<>();
        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        order_id.setCellValueFactory(new PropertyValueFactory<>("orderId"));
        dealer_id.setCellValueFactory(new PropertyValueFactory<>("dealerId"));
        deale_name.setCellValueFactory(new PropertyValueFactory<>("dealerName"));
        final_price.setCellValueFactory(new PropertyValueFactory<>("finalPrice"));
        date.setCellValueFactory(new PropertyValueFactory<>("date"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        try {
            table_invoicesdetail.setItems(parseUserList());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        btnpane.setOnMouseEntered(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(22);
                btnpane.setMinWidth(20.0);
                btn11.setMinWidth(20.0);

            }
        });
        btnpane.setOnMouseExited(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(14);
                btnpane.setMinWidth(10.0);
                btn11.setMinWidth(10.0);
            }
        });
        inner_anchor.setLeftAnchor(inner_anchor, 260d);
        drawer.setSidePane(nav_sales);
        drawer.open();

    }

    public void follow() throws IOException
    {
    /*PathTransition transition;
    transition = new PathTransition();
    Polyline mpoly = new Polyline();
    mpoly.getPoints().addAll(new Double[]{
            0.0,0.0,
            0.0,0.0,
            -200.0,0.0});
    transition.setNode(btn11);
    transition.setDuration(Duration.seconds(2));
    transition.setPath(mpoly);
    transition.play();*/
        if (drawer.isOpened()) {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(-250);
            transition.setByY(0);
            transition.setCycleCount(1);
            transition.play();
            drawer.close();
            FA_Icon.setGlyphName("ANGLE_DOUBLE_RIGHT");
            FA_Icon.setGlyphSize(14);
            //inner_anchor.setRightAnchor(inner_anchor,1d);


            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(-90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(1.28000);
            scale.setToY(1);



            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();
            //
            //inner_anchor.setLeftAnchor(inner_anchor, 5d);
        } else {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(250);
            transition.setByY(0);
            transition.setCycleCount(1);
            FA_Icon.setGlyphName("ANGLE_DOUBLE_LEFT");
            FA_Icon.setGlyphSize(14);
            drawer.open();

            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(0.99947777);
            scale.setToY(1);



            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();

        }


    }

    private ObservableList<InvoicesDetailInfo> parseUserList() throws ParseException {
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String InvoToDate = "";
        String InvofromDate = "";
        if (datePick_summaryFrom.getValue() != null || datePick_summaryTo.getValue() != null) {
            InvofromDate = datePick_summaryFrom.getValue().toString();

            Date selectedDate = null;
            try {
                selectedDate = sdf.parse(InvofromDate);
                InvofromDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            InvoToDate = datePick_summaryTo.getValue().toString();
            Date selectedToDate = null;
            try {
                selectedToDate = sdf.parse(InvoToDate);
                InvoToDate = sdf2.format(selectedToDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        String invoFromVal = txt_InvoNum_From.getText();
        String invoToVal = txt_InvoNum_To.getText();

        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        InvoicesDetailInfo objInvoicesDetailInfo = new InvoicesDetailInfo();
        invoicesDetailList = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> invoicesData = objInvoicesDetailInfo.getInvoicesDetail(objStmt, objCon,combo_invoice_status.getValue().toString() , invoFromVal, invoToVal,getcheckedareas,InvofromDate,InvoToDate );
        for (int i = 0; i < invoicesData.size(); i++)
        {
            invoicesDetailList.add(new InvoicesDetailInfo(String.valueOf(i+1), invoicesData.get(i).get(0), invoicesData.get(i).get(1), invoicesData.get(i).get(2), invoicesData.get(i).get(4), invoicesData.get(i).get(5)));
        }
        return invoicesDetailList;
    }

    public void filterSummary() throws IOException, ParseException {
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String InvoToDate = "";
        String InvofromDate = "";
        if (datePick_summaryFrom.getValue() != null || datePick_summaryTo.getValue() != null) {
            InvofromDate = datePick_summaryFrom.getValue().toString();

            Date selectedDate = null;
            try {
                selectedDate = sdf.parse(InvofromDate);
                InvofromDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            InvoToDate = datePick_summaryTo.getValue().toString();
            Date selectedToDate = null;
            try {
                selectedToDate = sdf.parse(InvoToDate);
                InvoToDate = sdf2.format(selectedToDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        String invoFromVal = txt_InvoNum_From.getText();
        String invoToVal = txt_InvoNum_To.getText();

        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        InvoicesDetailInfo objInvoicesDetailInfo = new InvoicesDetailInfo();
        invoicesDetailList = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> invoicesData = objInvoicesDetailInfo.getInvoicesDetail(objStmt, objCon,combo_invoice_status.getValue().toString() , invoFromVal, invoToVal,getcheckedareas,InvofromDate,InvoToDate );
        for (int i = 0; i < invoicesData.size(); i++)
        {
            invoicesDetailList.add(new InvoicesDetailInfo(String.valueOf(i+1), invoicesData.get(i).get(0), invoicesData.get(i).get(1), invoicesData.get(i).get(2), invoicesData.get(i).get(4), invoicesData.get(i).get(5)));
        }
        table_invoicesdetail.setItems(invoicesDetailList);

        txt_InvoNum_From.setText("");
        txt_InvoNum_To.setText("");
        datePick_summaryTo.setValue(null);
        datePick_summaryFrom.setValue(null);
        treeview.getCheckModel().clearChecks();
    }

    public void resetsummary_click() throws IOException, ParseException {
        txt_InvoNum_From.setText("");
        txt_InvoNum_To.setText("");
        datePick_summaryTo.setValue(null);
        datePick_summaryFrom.setValue(null);
        treeview.getCheckModel().clearChecks();
        invoicesDetailList.clear();
        table_invoicesdetail.getItems().removeAll(invoicesDetailList);


        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String InvoToDate = "";
        String InvofromDate = "";
        if (datePick_summaryFrom.getValue() != null || datePick_summaryTo.getValue() != null) {
            InvofromDate = datePick_summaryFrom.getValue().toString();

            Date selectedDate = null;
            try {
                selectedDate = sdf.parse(InvofromDate);
                InvofromDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            InvoToDate = datePick_summaryTo.getValue().toString();
            Date selectedToDate = null;
            try {
                selectedToDate = sdf.parse(InvoToDate);
                InvoToDate = sdf2.format(selectedToDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        String invoFromVal = txt_InvoNum_From.getText();
        String invoToVal = txt_InvoNum_To.getText();

        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        InvoicesDetailInfo objInvoicesDetailInfo = new InvoicesDetailInfo();
        invoicesDetailList = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> invoicesData = objInvoicesDetailInfo.getInvoicesDetail(objStmt, objCon,combo_invoice_status.getValue().toString() , invoFromVal, invoToVal,getcheckedareas,InvofromDate,InvoToDate );
        for (int i = 0; i < invoicesData.size(); i++)
        {
            invoicesDetailList.add(new InvoicesDetailInfo(String.valueOf(i+1), invoicesData.get(i).get(0), invoicesData.get(i).get(1), invoicesData.get(i).get(2), invoicesData.get(i).get(4), invoicesData.get(i).get(5)));
        }
        table_invoicesdetail.setItems(invoicesDetailList);
    }

    @FXML
    public void generateInvoice() throws JRException {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        ArrayList<ArrayList<String[]>> getInvoiceDetail = objInvoiceInfo.getInvoiceInfo(objStmt, objCon);
        InvoicedItemsInfo objItemsInfos;
        List<InvoicedItemsInfo> itemsList;
        for (int i = 0; i < getInvoiceDetail.size(); i++)
        {
            String[] itemsNo = new String[getInvoiceDetail.get(i).get(5).length];
            itemsNo = getInvoiceDetail.get(i).get(5);
            String[] itemsName = new String[getInvoiceDetail.get(i).get(6).length];
            itemsName = getInvoiceDetail.get(i).get(6);
            String[] quantity = new String[getInvoiceDetail.get(i).get(7).length];
            quantity = getInvoiceDetail.get(i).get(7);
            String[] unit = new String[getInvoiceDetail.get(i).get(8).length];
            unit = getInvoiceDetail.get(i).get(8);
            itemsList = new ArrayList<>();
            for(int j=0; j<itemsNo.length; j++)
            {
                objItemsInfos = new InvoicedItemsInfo();
                objItemsInfos.setItemNo(itemsNo[j]);
                objItemsInfos.setItemName(itemsName[j]);
                objItemsInfos.setQuantity(quantity[j]);
                objItemsInfos.setUnit(unit[j]);
                itemsList.add(objItemsInfos);
            }
            invoiceData.add(new InvoiceInfo(String.join(",",getInvoiceDetail.get(i).get(0)), String.join(",",getInvoiceDetail.get(i).get(1)), String.join(",",getInvoiceDetail.get(i).get(2)), String.join(",",getInvoiceDetail.get(i).get(3)), String.join(",",getInvoiceDetail.get(i).get(4)), itemsList, String.join(",",getInvoiceDetail.get(i).get(9)), String.join(",",getInvoiceDetail.get(i).get(10)), String.join(",",getInvoiceDetail.get(i).get(11)), String.join(",",getInvoiceDetail.get(i).get(12))));
        }
        InputStream objIO = ViewPendingOrders.class.getResourceAsStream("/reports/Invoice.jrxml");
        JasperReport objReport = JasperCompileManager.compileReport(objIO);
        JasperPrint objPrint = JasperFillManager.fillReport(objReport, null, new JRBeanCollectionDataSource(invoiceData));
        JasperViewer.viewReport(objPrint, false);
    }

    public void viewHome() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/home.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    public void viewPendings() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_pending_orders.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    public void SaleInvoices() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/sale_invoice.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    public void addCompany() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/enter_company.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewCompanies() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_company.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    void addArea() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/enter_area.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewAreas() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_area.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewSubAreas() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_subarea.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    void addDealer() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/enter_dealer.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewDealers() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_dealers.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    void addSalesman() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/enter_salesman.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewSalesman() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_salesman.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    void addSupplier() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/enter_supplier.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewSuppliers() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_suppliers.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    void addProduct() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/enter_new_product.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewProducts() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_products.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewSystemAccounts() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/system_accounts.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewDrawingSummary() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/drawing_summary.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }
}