package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXProgressBar;
import com.jfoenix.controls.JFXTextField;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import model.GlobalVariables;

import javax.mail.*;
import javax.naming.AuthenticationException;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;

public class MailLogin {

    @FXML
    private Label label_update;

    @FXML
    private JFXButton dialog_email_close;

    @FXML
    private JFXButton dialog_email_login;

    @FXML
    private JFXTextField txt_email_id;

    @FXML
    private JFXPasswordField txt_password;

    @FXML
    private JFXProgressBar progress_bar;

    public static StackPane objStackPane;
    public static Label lblEmail;

    @FXML
    void cancelEmailLogin(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/dashboard_live.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void emailLogin(ActionEvent event) throws IOException {
        progress_bar.setVisible(true);
        thread.start();
    }

    Thread thread = new Thread(){
        public void run(){

            //setup mail server
            Properties props = System.getProperties();
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", GlobalVariables.host);
            props.put("mail.smtp.port", "587");

            String txtEmail = txt_email_id.getText();
            String txtPassword = txt_password.getText();

            Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator(){
                @Override
                protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(txtEmail, txtPassword);
                }
            });

            Transport transport;
            try {
                transport = session.getTransport("smtp");
                transport.connect("smtp.gmail.com", txtEmail, txtPassword);
                transport.close();
//                System.out.println("1");

                GlobalVariables.email = txtEmail;
                GlobalVariables.password = txtPassword;

//                Parent root = null;
//                try {
//                    root = FXMLLoader.load(getClass().getResource("../view/dashboard_live.fxml"));
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                GlobalVariables.baseScene.setRoot(root);
//                GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);

                Platform.runLater(new Runnable(){
                    @Override
                    public void run() {
                        SendMail.dialog.close();
                        objStackPane.setVisible(false);
                        lblEmail.setText("("+GlobalVariables.email+")");
                        GlobalVariables.showNotification(1, "Success", "Login Successfully");
                    }
                    // do your GUI stuff here
                });

                //Authentication success
            } catch (NoSuchProviderException e) {
//            e.printStackTrace();
//                System.out.println("2");
                Platform.runLater(new Runnable(){
                    @Override
                    public void run() {
                        SendMail.dialog.close();
                        objStackPane.setVisible(false);
                        GlobalVariables.showNotification(-1, "Unable to Login", "Please check Email, Password and Internet Connection");
                        Parent root = null;
                        try {
                            root = FXMLLoader.load(getClass().getResource("../view/dashboard_live.fxml"));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        GlobalVariables.baseScene.setRoot(root);
                        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
                    }
                    // do your GUI stuff here
                });
            } catch (MessagingException e) {
//            e.printStackTrace();
//                System.out.println("3");
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        SendMail.dialog.close();
                        objStackPane.setVisible(false);
                        GlobalVariables.showNotification(-1, "Unable to Login", "Please check Email, Password and Internet Connection");
                        Parent root = null;
                        try {
                            root = FXMLLoader.load(getClass().getResource("../view/dashboard_live.fxml"));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        GlobalVariables.baseScene.setRoot(root);
                        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
                    }
                    // do your GUI stuff here
                });
            }
        }
    };
}
