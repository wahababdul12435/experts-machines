package controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.util.Duration;
import model.*;
import model.PrintPageInfo;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class PrintPage implements Initializable {

    @FXML    private BorderPane menu_bar1;
    @FXML    private BorderPane nav_setup;
    @FXML    private JFXTextField txt_heading1;
    @FXML    private JFXTextField txt_heading2;
    @FXML    private JFXTextField txt_heading3;
    @FXML    private JFXTextField txt_heading4;
    @FXML    private JFXTextField txt_heading5;
    @FXML    private JFXTextField txt_footer1;
    @FXML    private JFXTextField txt_footer2;
    @FXML    private JFXTextField txt_footer3;
    @FXML    private JFXTextField txt_footer4;

    @FXML    private JFXTextField txt_ownerid;
    @FXML    private JFXTextField txt_owner_name;
    @FXML    private JFXTextField txt_father_name;
    @FXML    private JFXTextField txt_owner_cnic;
    @FXML    private JFXTextField txt_business_name;
    @FXML    private JFXTextField txt_owner_country;
    @FXML    private JFXTextField txt_business_address;
    @FXML    private JFXTextField txt_business_city;

    @FXML    private Button cancelBtn;
    @FXML    private Button saveBtn;
    @FXML    private Button btn_add_supplier;
    @FXML   private Button btn_edit_cancel;
    @FXML   private Button btn_edit_update;
    @FXML    private TableView<PrintPageInfo> table_printpagedetails;
    @FXML    private TableColumn<PrintPageInfo, String> sr_no;
    @FXML    private TableColumn<PrintPageInfo, String> owner_id;
    @FXML    private TableColumn<PrintPageInfo, String> owner_name;
    @FXML    private TableColumn<PrintPageInfo, String> father_name;
    @FXML    private TableColumn<PrintPageInfo, String> owner_country;
    @FXML    private TableColumn<PrintPageInfo, String> business_name;
    @FXML    private TableColumn<PrintPageInfo, String> business_address;
    @FXML    private TableColumn<PrintPageInfo, String> operations;
    @FXML    private JFXDrawer drawer;
    @FXML    private Pane btnpane;
    @FXML    private Button btn11;
    @FXML    private FontAwesomeIconView FA_Icon;
    @FXML    private AnchorPane inner_anchor;

    public static ObservableList<PrintPageInfo> pageprintDetails;
    Alert alert = new Alert(Alert.AlertType.NONE);
    PrintPageInfo objPrintPageInfo;
    public static int srNo;

    public void initialize(URL url, ResourceBundle resourceBundle) {
        objPrintPageInfo = new PrintPageInfo();
        btnpane.setOnMouseEntered(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(22);
                btnpane.setMinWidth(20.0);
                btn11.setMinWidth(20.0);

            }
        });
        btnpane.setOnMouseExited(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(14);
                btnpane.setMinWidth(10.0);
                btn11.setMinWidth(10.0);
            }
        });

        inner_anchor.setLeftAnchor(inner_anchor, 280d);
        drawer.setSidePane(nav_setup);
        drawer.open();


        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        owner_id.setCellValueFactory(new PropertyValueFactory<>("ownerId"));
        owner_name.setCellValueFactory(new PropertyValueFactory<>("ownerName"));
        father_name.setCellValueFactory(new PropertyValueFactory<>("fatherName"));
        business_name.setCellValueFactory(new PropertyValueFactory<>("businessName"));
        business_address.setCellValueFactory(new PropertyValueFactory<>("businessAddress"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        PrintPageInfo.table_addsupplier = table_printpagedetails;
        table_printpagedetails.setItems(parseUserList());

        PrintPageInfo.txtownerId = txt_ownerid;
        PrintPageInfo.txtownerName = txt_owner_name;
        PrintPageInfo.txtfatherName = txt_father_name;
        PrintPageInfo.txtownerCNIC = txt_owner_cnic;
        PrintPageInfo.txtownerCountry = txt_owner_country;
        PrintPageInfo.txtbusinessName = txt_business_name;
        PrintPageInfo.txtbusinessAddress = txt_business_address;
        PrintPageInfo.txtbusinessCity = txt_business_city;

        PrintPageInfo.btnAdd = btn_add_supplier;
        PrintPageInfo.btnCancel = btn_edit_cancel;
        PrintPageInfo.btnUpdate = btn_edit_update;
    }

    public static ObservableList<PrintPageInfo> parseUserList(){
        PrintPageInfo objPrintPageInfo = new PrintPageInfo();
        pageprintDetails = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> supplierData = objPrintPageInfo.getAddedPageInfo();
        for (int i = 0; i < supplierData.size(); i++)
        {
            pageprintDetails.add(new PrintPageInfo(String.valueOf(i+1), ((supplierData.get(i).get(0) == null || supplierData.get(i).get(0).equals("")) ? "N/A" : supplierData.get(i).get(0)), supplierData.get(i).get(1), supplierData.get(i).get(2), supplierData.get(i).get(3), supplierData.get(i).get(4)));
        }
        return pageprintDetails;
    }

    @FXML
    void addPageInfo(ActionEvent event) {
        String Str_ownerid = txt_ownerid.getText();
        String Str_ownerName = txt_owner_name.getText();
        String Str_fatherName = txt_father_name.getText();
        String Str_ownerCNIC = txt_owner_cnic.getText();
        String Str_ownerCountry = txt_owner_country.getText();
        String Str_businessName = txt_business_name.getText();
        String Str_businessAddress = txt_business_address.getText();
        String Str_businessCity = txt_business_city.getText();
        String Str_header1 = txt_heading1.getText();
        String Str_header2 = txt_heading1.getText();
        String Str_header3 = txt_heading1.getText();
        String Str_header4 = txt_heading1.getText();
        String Str_header5 = txt_heading1.getText();
        String Str_footer1 = txt_business_name.getText();
        String Str_footer2 = txt_business_address.getText();
        String Str_footer3 = txt_business_city.getText();
        String Str_footer4 = txt_business_city.getText();

        String comp = objPrintPageInfo.confirmNewId(Str_ownerid);

        if(!comp.equals(Str_ownerid) || comp.equals("")) {
            PrintPageInfo.ownerIdArr.add(Str_ownerid);
            PrintPageInfo.ownerNameArr.add(Str_ownerName);
            PrintPageInfo.fatherNameArr.add(Str_fatherName);
            PrintPageInfo.ownerCNICArr.add(Str_ownerCNIC);
            PrintPageInfo.ownerCountryArr.add(Str_ownerCountry);
            PrintPageInfo.businessNameArr.add(Str_businessName);
            PrintPageInfo.businessAddressArr.add(Str_businessAddress);
            PrintPageInfo.businessCityArr.add(Str_businessCity);
            PrintPageInfo.headerArr1.add(Str_header1);
            PrintPageInfo.headerArr2.add(Str_header2);
            PrintPageInfo.headerArr3.add(Str_header3);
            PrintPageInfo.headerArr4.add(Str_header4);
            PrintPageInfo.headerArr5.add(Str_header5);
            PrintPageInfo.footerArr1.add(Str_footer1);
            PrintPageInfo.footerArr2.add(Str_footer2);
            PrintPageInfo.footerArr3.add(Str_footer3);
            PrintPageInfo.footerArr4.add(Str_footer4);
            table_printpagedetails.setItems(parseUserList());
            refreshControls();
        }
        else
        {
            alert.setAlertType(Alert.AlertType.ERROR);
            alert.setHeaderText("Error!");
            alert.setContentText("Owner ID Already Saved.");
            alert.show();
        }
    }

    @FXML
    void editCancel(ActionEvent event) {
        refreshControls();
        btn_add_supplier.setVisible(true);
        btn_edit_cancel.setVisible(false);
        btn_edit_update.setVisible(false);
    }

    @FXML
    void editUpdate(ActionEvent event) {
        String Str_ownerid = txt_ownerid.getText();
        String Str_ownerName = txt_owner_name.getText();
        String Str_fatherName = txt_father_name.getText();
        String Str_ownerCNIC = txt_owner_cnic.getText();
        String Str_ownerCountry = txt_owner_country.getText();
        String Str_businessName = txt_business_name.getText();
        String Str_businessAddress = txt_business_address.getText();
        String Str_businessCity = txt_business_city.getText();
        String Str_supplierStatus = "Active";

        String comp = objPrintPageInfo.confirmNewId(Str_ownerid);

        if(!comp.equals(Str_ownerid) || comp.equals("")) {
            PrintPageInfo.ownerIdArr.add(Str_ownerid);
            PrintPageInfo.ownerNameArr.add(Str_ownerName);
            PrintPageInfo.fatherNameArr.add(Str_fatherName);
            PrintPageInfo.ownerCNICArr.add(Str_ownerCNIC);
            PrintPageInfo.ownerCountryArr.add(Str_ownerCountry);
            PrintPageInfo.businessNameArr.add(Str_businessName);
            PrintPageInfo.businessAddressArr.add(Str_businessAddress);
            PrintPageInfo.businessCityArr.add(Str_businessCity);
            table_printpagedetails.setItems(parseUserList());
            refreshControls();
        }
        else
        {
            alert.setAlertType(Alert.AlertType.ERROR);
            alert.setHeaderText("Error!");
            alert.setContentText("Supplier ID Already Saved.");
            alert.show();
        }

        btn_add_supplier.setVisible(true);
        btn_edit_cancel.setVisible(false);
        btn_edit_update.setVisible(false);
    }

    @FXML
    void cancelClicked(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/system_settings.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void follow(MouseEvent event) {
        if (drawer.isOpened()) {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(-250);
            transition.setByY(0);
            transition.setCycleCount(1);
            transition.play();
            drawer.close();
            FA_Icon.setGlyphName("ANGLE_DOUBLE_RIGHT");
            FA_Icon.setGlyphSize(14);
            //inner_anchor.setRightAnchor(inner_anchor,1d);
            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(-90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(1.1000);
            scale.setToY(1);

            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();

        }
        else {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(250);
            transition.setByY(0);
            transition.setCycleCount(1);
            FA_Icon.setGlyphName("ANGLE_DOUBLE_LEFT");
            FA_Icon.setGlyphSize(14);
            drawer.open();

            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(0.99947777);
            scale.setToY(1);

            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();
        }
    }

    @FXML
    void saveClicked(ActionEvent event) throws IOException {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        try {
            objPrintPageInfo.insertPageinfo(objStmt, objCon);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Parent root = FXMLLoader.load(getClass().getResource("../view/print_page.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    public void refreshControls()
    {
        this.txt_ownerid.setText("");
        this.txt_owner_name.setText("");
        this.txt_father_name.setText("");
        this.txt_owner_cnic.setText("");
        this.txt_owner_country.setText("");
        this.txt_business_name.setText("");
        this.txt_business_address.setText("");
        this.txt_business_city.setText("");
        this.txt_heading1.setText("");
        this.txt_heading2.setText("");
        this.txt_heading3.setText("");
        this.txt_heading4.setText("");
        this.txt_heading5.setText("");
        this.txt_footer1.setText("");
        this.txt_footer2.setText("");
        this.txt_footer3.setText("");
        this.txt_footer4.setText("");
    }

}
