package controller;

import com.jfoenix.controls.*;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.Duration;
import model.*;
import org.controlsfx.control.textfield.TextFields;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class DiscountPolicy implements Initializable {
    private int discount = 0;

    @FXML    private AnchorPane inner_anchor;
    @FXML    private JFXDrawer drawer;
    @FXML    private Button btn11;
    @FXML    private Pane btnpane;
    @FXML    private FontAwesomeIconView FA_Icon;
    @FXML    private BorderPane nav_setup;
    @FXML    private BorderPane menu_bar1;
    @FXML    private Button cancelBtn;
    @FXML    private TableView tableView;
    @FXML    private JFXTextField txt_policyID;
    @FXML    private JFXTextField txt_dealername;
    @FXML    private JFXCheckBox chkbx_discDealer;
    @FXML    private JFXCheckBox chkbx_discSaleAmount;
    @FXML    private JFXTextField txt_saleamount;
    @FXML    private JFXTextField txt_dealerid;
    @FXML    private JFXTextField txt_Compname;
    @FXML    private JFXTextField txt_Compid;
    @FXML    private JFXDatePicker datepick_startdate;
    @FXML    private JFXDatePicker datepick_enddate;
    @FXML    private JFXComboBox<String> combo_dealertype;
    @FXML private Slider slider_discount;
    @FXML private TextField discount_display;
    //@FXML    private Button cancelBtn;
    Alert a = new Alert(Alert.AlertType.NONE);

    public void cancelClicked() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/home.fxml"));
        Stage objstage = (Stage) cancelBtn.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    ObservableList<Discount> data = FXCollections.observableArrayList();
    int rowvalueAfterEdit = 0;
    public void initialize(URL location, ResourceBundle resources) {
        chkbx_discDealer.setSelected(true);
        discount_display.focusedProperty().addListener(new ChangeListener<Boolean>()
        {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue)
            {
                if (newPropertyValue)
                {
                    //System.out.println("Textfield on focus");
                }
                else {
                    double txt_toDouble = Double.parseDouble(discount_display.getText());
                        slider_discount.setValue(txt_toDouble);
                }
            }
        });

        txt_dealerid.focusedProperty().addListener(new ChangeListener<Boolean>()
        {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue)
            {
                if (newPropertyValue)
                {
                    //System.out.println("Textfield on focus");
                }
                else {
                    if(!txt_dealerid.getText().isEmpty())
                    {
                        MysqlCon newobjMysqlConnc = new MysqlCon();
                        Statement objectStattmt = newobjMysqlConnc.stmt;
                        Connection objectConnnec = newobjMysqlConnc.con;
                        ResultSet rs3 = null;
                        try {
                            rs3 = objectStattmt.executeQuery("select dealer_name from dealer_info where dealer_id = '"+txt_dealerid.getText()+"' and dealer_status = 'Active'");
                            while (rs3.next()) {
                                txt_dealername.setText(rs3.getString(1));
                            }
                            objectConnnec.close();
                            slider_discount.requestFocus();
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
        txt_policyID.focusedProperty().addListener(new ChangeListener<Boolean>()
        {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue)
            {

                    if (newPropertyValue)
                    {
                        //System.out.println("Textfield on focus");
                    }
                    else {
                        if(!txt_policyID.getText().isEmpty())
                        {
                            String verifypolicyid = "";
                            MysqlCon newobjMysqlConnc = new MysqlCon();
                            Statement objectStattmt = newobjMysqlConnc.stmt;
                            Connection objectConnnec = newobjMysqlConnc.con;
                            ResultSet rs3 = null;
                            try {
                                rs3 = objectStattmt.executeQuery("select approval_id from discount_policy ");
                                while (rs3.next()) {
                                    verifypolicyid = (rs3.getString(1));
                                }
                                objectConnnec.close();
                            } catch (SQLException e) {
                                e.printStackTrace();
                            }
                            if (!verifypolicyid.equals(txt_policyID.getText())) {
                                txt_dealerid.requestFocus();
                            }
                            else
                            {
                                a.setAlertType(Alert.AlertType.ERROR);
                                a.setHeaderText("Policy ID already Exist");
                                a.setContentText("Please Enter new Policy ID");
                                a.show();
                                txt_policyID.requestFocus();
                            }
                        }
                    }
                }

        });

        ArrayList<String> newComplist = new ArrayList<>();
        Discount objctsaleinvoice = new Discount();
        MysqlCon objctsMysqlConnc = new MysqlCon();
        Statement objectStatmnt = objctsMysqlConnc.stmt;
        Connection objectConnnection = objctsMysqlConnc.con;
        newComplist = objctsaleinvoice.get_AllCompanies(objectStatmnt,objectConnnection);
        String[] possiblee= new String[newComplist.size()];
        for(int i =0; i < newComplist.size();i++)
        {
            possiblee[i] = newComplist.get(i);
        }
        TextFields.bindAutoCompletion(txt_Compname,possiblee);

        datepick_startdate.setValue(LocalDate.now());
        datepick_enddate.setValue(LocalDate.now());

        TableColumn approvalID_Col = new TableColumn("Approval ID");
        approvalID_Col.setMinWidth(100);
        TableColumn dealerID_COL = new TableColumn("Dealer ID");
        dealerID_COL.setMinWidth(100);
        TableColumn dealerName_COL = new TableColumn("Dealer Name");
        dealerName_COL.setMinWidth(100);
        TableColumn companyID_COL = new TableColumn("Company ID");
        companyID_COL.setMinWidth(100);
        TableColumn companyName_COL = new TableColumn("Company Name");
        companyName_COL.setMinWidth(100);
        TableColumn discount_COL = new TableColumn("Discount");
        discount_COL.setMinWidth(80);
        TableColumn startDate_COL = new TableColumn("Start Date");
        startDate_COL.setMinWidth(100);
        TableColumn endDate_COL = new TableColumn("End Date");
        endDate_COL.setMinWidth(100);
        TableColumn productEditCOL = new TableColumn("Actions");
        productEditCOL.setMinWidth(140);
        TableColumn firstEDITCol = new TableColumn("Edit");
        TableColumn secondDELETECol = new TableColumn("Delete");

        productEditCOL.getColumns().addAll(firstEDITCol, secondDELETECol);
        tableView.getColumns().addAll(approvalID_Col, dealerID_COL, dealerName_COL,companyID_COL,companyName_COL, discount_COL,  startDate_COL, endDate_COL, productEditCOL);
        approvalID_Col.setCellValueFactory(new PropertyValueFactory<Discount, String>("approvalids"));
        dealerID_COL.setCellValueFactory(new PropertyValueFactory<Discount, String>("dealerIds"));
        dealerName_COL.setCellValueFactory(new PropertyValueFactory<Discount, String>("dealerNames"));
        companyID_COL.setCellValueFactory(new PropertyValueFactory<Discount, String>("companyId"));
        companyName_COL.setCellValueFactory(new PropertyValueFactory<Discount, String>("companyName"));
        discount_COL.setCellValueFactory(new PropertyValueFactory<Discount, String>("discount_percent"));
        startDate_COL.setCellValueFactory(new PropertyValueFactory<Discount, String>("startdates"));
        endDate_COL.setCellValueFactory(new PropertyValueFactory<Discount, String>("enddates"));

        //START of Creating Delete Button
        secondDELETECol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Discount, Boolean>, ObservableValue>() {
            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<Discount, Boolean> param) {
                return new SimpleBooleanProperty(param.getValue() != null);
            }

        });
        secondDELETECol.setCellFactory(new Callback<TableColumn<Discount, Boolean>, TableCell<Discount, Boolean>>() {
            @Override
            public TableCell<Discount, Boolean> call(TableColumn<Discount, Boolean> param) {
                return new DiscountPolicy.ButtonCell(tableView);
            }
        });
        //END of Creating Delete Button
        //START of Creating EDIT Button
        firstEDITCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Discount, Boolean>, ObservableValue>() {
            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<Discount, Boolean> param) {
                return new SimpleBooleanProperty(param.getValue() != null);
            }

        });
        firstEDITCol.setCellFactory(new Callback<TableColumn<Discount, Boolean>, TableCell<Discount, Boolean>>() {
            @Override
            public TableCell<Discount, Boolean> call(TableColumn<Discount, Boolean> param) {
                return new DiscountPolicy.Editbtncell(tableView);
            }
        });
        //END of Creating EDIT Button
        tableView.setItems(data);
    }
    // START of Button Code for Editing a Row
    private class Editbtncell extends TableCell<Discount, Boolean> {
        final Button btncell = new Button("Edit");

        public Editbtncell(final TableView view1) {
            btncell.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    rowvalueAfterEdit = getTableRow().getIndex();
                    Discount newobj = (Discount) tableView.getItems().get(rowvalueAfterEdit);
                    txt_policyID.setText(newobj.getApprovalids());
                    txt_dealerid.setText(newobj.getDealerIds());
                    txt_Compid.setText(newobj.getCompanyId());
                    txt_dealername.setText(newobj.getDealerNames());
                    discount_display.setText(newobj.getDiscount_percent());
                    txt_saleamount.setText(newobj.getSaleamount());
                    String strng_strtdate = newobj.getStartdates();
                    String strng_enddate = newobj.getEnddates();

                }
            });
        }

        @Override
        protected void updateItem(Boolean n, boolean boln) {
            super.updateItem(n, boln);
            if (boln) {
                setGraphic(null);
            } else {
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setGraphic(btncell);
            }
        }
    }
    // END of Button Code for Editing a Row
    //START of Button Code for Deleting a Row
    private class ButtonCell extends TableCell<Discount, Boolean> {

        final Button CellButton = new Button("Delete");

        public ButtonCell(final TableView view) {
            CellButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    int selectedIndex = getTableRow().getIndex();
                    data.remove(selectedIndex);

                }
            });
        }

        @Override
        protected void updateItem(Boolean t, boolean bln) {
            super.updateItem(t, bln);
            if (bln) {
                setGraphic(null);
            } else {
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setGraphic(CellButton);
            }

        }
    }
    //END of Button Code for Deleting a Row
    public void keypress_policyID(KeyEvent event) throws IOException
    {
        if (event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB)
        {
            if(!txt_policyID.getText().isEmpty())
            {
                String verifypolicyid = "";
                MysqlCon newobjMysqlConnc = new MysqlCon();
                Statement objectStattmt = newobjMysqlConnc.stmt;
                Connection objectConnnec = newobjMysqlConnc.con;
                ResultSet rs3 = null;
                try {
                    rs3 = objectStattmt.executeQuery("select approval_id from discount_policy ");
                    while (rs3.next()) {
                        verifypolicyid = (rs3.getString(1));
                    }
                    objectConnnec.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                if (!verifypolicyid.equals(txt_policyID.getText())) {
                    txt_dealerid.requestFocus();
                }
                else
                {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setHeaderText("Policy ID already Exist");
                    a.setContentText("Please Enter new Policy ID");
                    a.show();
                    txt_policyID.requestFocus();
                }
            }
        }

    }
    public boolean activ_notactiv(String st_date,String endDate,String curr_date) throws ParseException {
        DateFormat format = new SimpleDateFormat("dd/MMM/yyyy", Locale.ENGLISH);
        Date Sdate = format.parse(st_date);
        Date Edate = format.parse(endDate);
        Date Currdate = format.parse(curr_date);
        return Sdate.compareTo(Currdate) * Currdate.compareTo(Edate) >= 0;
    }
    public void save_policy() throws IOException, ParseException {

            int counttableROWS = tableView.getItems().size();

            String[] getapprovID_tableview = new String[counttableROWS];
            String[] getdealerid_tableview = new String[counttableROWS];
            String[] getdealerName_tableview = new String[counttableROWS];
            String[] getcompanyid_tableview = new String[counttableROWS];
            String[] getcompanyname_tableview = new String[counttableROWS];
            String[] getsaleamount_tableview = new String[counttableROWS];
            String[] getdiscount_tableview = new String[counttableROWS];
            String[] getstartDate_tableview = new String[counttableROWS];
            String[] getendDate_tableview = new String[counttableROWS];

            for(int i =0;i<counttableROWS;i++) //////////////////////////////////////START OF LOOP TO READ ALL ROWS OF TABLVIEW
            {
                Discount disc_Object = (Discount) tableView.getItems().get(i);
                getapprovID_tableview[i] = disc_Object.getApprovalids();
                getdealerid_tableview[i] = disc_Object.getDealerIds();
                getdealerName_tableview[i] = disc_Object.getDealerNames();
                getcompanyid_tableview[i] =  disc_Object.getCompanyId();
                getcompanyname_tableview[i] =  disc_Object.getCompanyName();
                getsaleamount_tableview[i] = disc_Object.getSaleamount();
                getdiscount_tableview[i] = disc_Object.getDiscount_percent();
                getstartDate_tableview[i] = disc_Object.getStartdates();
                getendDate_tableview[i] = disc_Object.getEnddates();
                String getuserID = GlobalVariables.getUserId();
                String getcurrDate = GlobalVariables.getStDate();
                String getcurrTime = GlobalVariables.getStTime();
                String discountValue = "";
                String[] splitval = getdiscount_tableview[i].split("%");
                discountValue = splitval[0];
                String policy_Status = "";
                boolean activ_or_not = activ_notactiv(getstartDate_tableview[i],getendDate_tableview[i],getcurrDate);
                if(activ_or_not == true)
                {
                    policy_Status= "Active";
                }
                else
                {
                    policy_Status = "InActive";
                }
                if(getsaleamount_tableview[i] == null)
                {
                    getsaleamount_tableview[i] = "0.0";
                }
                MysqlCon objMysqlConnct = new MysqlCon();
                Statement objctStatmt = objMysqlConnct.stmt;
                Connection objctConnc = objMysqlConnct.con;
                ResultSet rs = null;
                ResultSet rs1 = null;
                ResultSet rs2 = null;
                String dealertableID = "";
                String producttableID ="";
                String companytableID = "";

                try {
                    rs = objctStatmt.executeQuery("SELECT dealer_table_id  FROM `dealer_info` WHERE dealer_id = '"+getdealerid_tableview[i]+"' ");
                    while (rs.next())
                    {
                        dealertableID = rs.getString(1);
                    }

                    rs2 = objctStatmt.executeQuery("SELECT company_table_id  FROM `company_info` WHERE company_id = '"+getcompanyid_tableview[i]+"' ");
                    while (rs2.next())
                    {
                        companytableID = rs2.getString(1);
                    }
//            objctConnc.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }

                MysqlCon objMysqlCon = new MysqlCon();
                Statement objctStmt = objMysqlCon.stmt;
                Connection objctCon = objMysqlCon.con;
                try {
                    String insertQuery = "INSERT INTO `discount_policy`( `approval_id`, `dealer_table_id`,`company_table_id`, `sale_amount`, `discount_percent`, `start_date`, `end_date`, `policy_status`, `entered_by`, `entry_date`, `entry_time`, `updated_by`, `update_date`, `update_time`) VALUES ('" + getapprovID_tableview[i] + "','" + dealertableID + "','"+companytableID+"','" + getsaleamount_tableview[i] + "','" + discountValue + "','" + getstartDate_tableview[i] + "','" + getendDate_tableview[i] + "','"+policy_Status+"','" + getuserID + "','" + getcurrDate + "','" + getcurrTime + "','" + getuserID + "','" + getcurrDate + "','" + getcurrTime + "')";
                    objctStmt.executeUpdate(insertQuery);
                    objctCon.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                double txt_toDouble = 0.0;
                slider_discount.setValue(txt_toDouble);
                txt_policyID.setText("");
                txt_dealerid.setText("");
                txt_dealername.setText("");
                txt_saleamount.setText("");
                for ( int f = 0; f<tableView.getItems().size(); f++) {
                    tableView.getItems().clear();
                }
                datepick_startdate.setValue(LocalDate.now());
                datepick_enddate.setValue(LocalDate.now());
                txt_policyID.requestFocus();
            }
    }

    public void show_saleamount(KeyEvent event) throws IOException {
        if (event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.SPACE) {
            if(chkbx_discSaleAmount.isSelected())
            {
                txt_saleamount.setVisible(false);
            }
            else
            {
                txt_saleamount.setVisible(true);
                chkbx_discDealer.setSelected(false);
            }
        }
    }
    public void disc_onDealer_keypress(KeyEvent event) throws IOException {
        if (event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.SPACE) {
            if(chkbx_discDealer.isSelected())
            {
                //txt_saleamount.setVisible(false);
            }
            else
            {
                txt_saleamount.setVisible(false);
                chkbx_discSaleAmount.setSelected(false);
            }
        }
    }

    public void disc_onDealer_click() throws IOException
    {
        if(chkbx_discDealer.isSelected())
        {
            txt_saleamount.setVisible(false);
            chkbx_discSaleAmount.setSelected(false);
        }
        else
        {
            //txt_saleamount.setVisible(false);
        }
    }
    public void setDiscount_onAmount() throws IOException
    {
        if(chkbx_discSaleAmount.isSelected())
        {
            txt_saleamount.setVisible(true);
            chkbx_discDealer.setSelected(false);
        }
        else
        {
            chkbx_discDealer.setSelected(true);
            txt_saleamount.setVisible(false);
        }
    }
    /////////////////////////////////////////////////////////////////////////// START of ENter Press on Button Add
    public void btnadd_enterpress(KeyEvent event) throws IOException {
        if (event.getCode() == KeyCode.ENTER) {
            if (txt_policyID.getText().isEmpty()) {
                a.setAlertType(Alert.AlertType.ERROR);
                a.setHeaderText("ERROR!");
                a.setContentText("Enter Approval ID.");
                a.show();
                txt_policyID.requestFocus();
            } else if (txt_dealerid.getText().isEmpty()) {
                a.setAlertType(Alert.AlertType.ERROR);
                a.setHeaderText("ERROR!");
                a.setContentText("Enter Dealer ID.");
                a.show();
                txt_dealerid.requestFocus();
            } else {

                Discount bon_obj = new Discount();
                bon_obj.setApprovalids(txt_policyID.getText());
                bon_obj.setDealerIds(txt_dealerid.getText());
                bon_obj.setDealerNames(txt_dealername.getText());
                bon_obj.setCompanyId(txt_Compid.getText());
                bon_obj.setCompanyName(txt_Compname.getText());
                if(chkbx_discSaleAmount.isSelected())
                {
                    bon_obj.setSaleamount(txt_saleamount.getText());
                    bon_obj.setDiscount_percent(discount_display.getText());
                }
                if(chkbx_discDealer.isSelected())
                {
                    bon_obj.setDiscount_percent(discount_display.getText());
                }
                bon_obj.setStartdates(datepick_startdate.getValue().toString());
                bon_obj.setEnddates(datepick_enddate.getValue().toString());

                tableView.getItems().add(bon_obj);
                txt_dealerid.setText("");
                txt_dealername.setText("");
                txt_Compid.setText("");
                txt_Compname.setText("");
                txt_policyID.setText("");
                txt_saleamount.setText("0");

                txt_policyID.requestFocus();

            }
        }

    }

    /////////////////////////////////////////////////////////////////////////// End of ENter Press on Button Add


    public void enterpressCompID(KeyEvent event) throws IOException, ParseException
    {
        if (event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB) {
            if (!txt_Compid.getText().isEmpty()) {
                tableView.refresh();
                int i=0;
                String companyid = txt_Compid.getText();
                int intcompany_id = Integer.parseInt(companyid);
                MysqlCon objMysqlCon1 = new MysqlCon();
                Statement objStmt1 = objMysqlCon1.stmt;
                Connection objCon1 = objMysqlCon1.con;
                ResultSet rs = null;
                try {
                    rs = objStmt1.executeQuery("select `company_name`  from `company_info`  where  company_id = '" + intcompany_id + "'  and company_status = 'Active' ");

                    while (rs.next()) {
                        i++;
                        txt_Compname.setText(rs.getString(1));
                    }
                    if(i==0)
                    {
                        a.setAlertType(Alert.AlertType.ERROR);
                        a.setHeaderText("ERROR!");
                        a.setContentText("Company ID not Found.");
                        a.show();
                        txt_Compid.requestFocus();
                    }
                    else
                    {
                        slider_discount.requestFocus();
                    }

                    objCon1.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    int alreadyentered = 0;
    public void enterpressdealerID(KeyEvent event) throws IOException, ParseException {
        if (event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB) {
            if (!txt_dealerid.getText().isEmpty()) {
                tableView.refresh();

                for (int i = 0; i < tableView.getItems().size(); i++) {
                    Product newwobj = (Product) tableView.getItems().get(i);
                    if (newwobj.getProductId().equals(txt_dealerid.getText())) {
                        alreadyentered = 1;
                    }
                }
                if (alreadyentered == 0) {
                    String dealerid = txt_dealerid.getText();
                    int intdealer_id = Integer.parseInt(dealerid);
                    int i = 0;
                    MysqlCon objMysqlCon1 = new MysqlCon();
                    Statement objStmt1 = objMysqlCon1.stmt;
                    Connection objCon1 = objMysqlCon1.con;
                    ResultSet rs = null;
                    try {
                        rs = objStmt1.executeQuery("select `dealer_name`  from `dealer_info`   where  dealer_ID = '" + intdealer_id + "' and dealer_status = 'Active' ");

                        while (rs.next()) {
                           i++;
                           txt_dealername.setText(rs.getString(1));
                        }
                        if(i==0)
                        {
                            a.setAlertType(Alert.AlertType.ERROR);
                            a.setHeaderText("ERROR!");
                            a.setContentText("Dealer ID not Found.");
                            a.show();
                            txt_dealerid.requestFocus();
                        }
                        else
                        {
                            txt_Compid.requestFocus();
                        }

                        objCon1.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    if(i!=0)
                    {
                        txt_Compid.requestFocus();
                    }

                } else {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setHeaderText("ERROR!");
                    a.setContentText("Dealer ID Already Entered in Table.");
                    a.show();
                    txt_dealerid.requestFocus();
                    alreadyentered = 0;
                }
            } else {
                a.setAlertType(Alert.AlertType.ERROR);
                a.setHeaderText("Error!");
                a.setContentText("Dealer ID not entered.");
                a.show();
                txt_dealerid.requestFocus();
            }
        }
    }

   /* int companyalreadyentered = 0;
    public void enterpresscompanyID(KeyEvent event) throws IOException, ParseException {
        if (event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB) {
            if (!txt_Compid.getText().equals("")) {
                tableView.refresh();

                for (int i = 0; i < tableView.getItems().size(); i++) {
                    Product newwobj = (Product) tableView.getItems().get(i);
                    if (newwobj.getProductId().equals(txt_Compid.getText())) {
                        companyalreadyentered = 1;
                    }
                }
                if (companyalreadyentered == 0) {
                    String compid = txt_Compid.getText();
                    int intComp_id = Integer.parseInt(compid);

                    MysqlCon objMysqlCon1 = new MysqlCon();
                    Statement objStmt1 = objMysqlCon1.stmt;
                    Connection objCon1 = objMysqlCon1.con;
                    ResultSet rs = null;
                    try {
                        rs = objStmt1.executeQuery("select `company_name`  from `company_info`   where  company_id = '" + intComp_id + "'  ");
                        int i = 0;
                        while (rs.next()) {
                            txt_dealername.setText(rs.getString(1));
                        }

                        objCon1.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    txt_Compid.requestFocus();

                } else {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setHeaderText("ERROR!");
                    a.setContentText("Dealer ID Already Entered in Table.");
                    a.show();
                    txt_dealerid.requestFocus();
                    alreadyentered = 0;
                }
            } else {
                a.setAlertType(Alert.AlertType.ERROR);
                a.setHeaderText("Error!");
                a.setContentText("Dealer ID not entered.");
                a.show();
                txt_dealerid.requestFocus();
            }
        }
    }*/

    /////////////////////////////////////////////////////////////////////////// START of click on Button Add
    public void btnadd_clicks() throws IOException {
            if (txt_policyID.getText().isEmpty()) {
                a.setAlertType(Alert.AlertType.ERROR);
                a.setHeaderText("ERROR!");
                a.setContentText("Enter Approval ID.");
                a.show();
                txt_policyID.requestFocus();
            } else {
                Discount disc_obj = new Discount();
                disc_obj.setApprovalids(txt_policyID.getText());
                disc_obj.setDealerIds(txt_dealerid.getText());
                disc_obj.setDealerNames(txt_dealername.getText());
                disc_obj.setCompanyId(txt_Compid.getText());
                disc_obj.setCompanyName(txt_Compname.getText());
                if(chkbx_discSaleAmount.isSelected())
                {
                    disc_obj.setSaleamount(txt_saleamount.getText());
                    disc_obj.setDiscount_percent(discount_display.getText());
                }
                if(chkbx_discDealer.isSelected())
                {
                    disc_obj.setDiscount_percent(discount_display.getText());
                }
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MMM/yyyy");
                String startDateValue = (datepick_startdate.getValue()).format(formatter);
                disc_obj.setStartdates(startDateValue);

                String endDateValue = (datepick_enddate.getValue()).format(formatter);
                disc_obj.setEnddates(endDateValue);


                tableView.getItems().add(disc_obj);
                txt_dealerid.setText("");
                txt_dealername.setText("");
                txt_Compid.setText("");
                txt_Compname.setText("");
                txt_policyID.setText("");

                txt_policyID.requestFocus();

            }
        }

    /////////////////////////////////////////////////////////////////////////// End of click on Button Add

    public void follow() throws IOException {
        if (drawer.isOpened()) {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(-250);
            transition.setByY(0);
            transition.setCycleCount(1);
            transition.play();
            drawer.close();
            FA_Icon.setGlyphName("ANGLE_DOUBLE_RIGHT");
            FA_Icon.setGlyphSize(14);
            //inner_anchor.setRightAnchor(inner_anchor,1d);


            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(-90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration, inner_anchor);
            scale.setToX(1.28000);
            scale.setToY(1);


            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale, anchorePaneTransition, transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();
            //
            //inner_anchor.setLeftAnchor(inner_anchor, 5d);
        } else {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(250);
            transition.setByY(0);
            transition.setCycleCount(1);
            FA_Icon.setGlyphName("ANGLE_DOUBLE_LEFT");
            FA_Icon.setGlyphSize(14);
            drawer.open();

            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration, inner_anchor);
            scale.setToX(0.99947777);
            scale.setToY(1);


            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale, anchorePaneTransition, transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();

        }


    }
    public int getDiscount() {
        return discount;
    }
    public void keypress_setDiscount(KeyEvent ec)
    {
        if (ec.getCode() == KeyCode.RIGHT || ec.getCode() == KeyCode.LEFT)
        {
            int slid = (int) slider_discount.getValue();
            this.discount = slid;
            discount_display.setText(String.valueOf(slid)+"%");
        }
    }
    public void setDiscount() {
        int slid = (int) slider_discount.getValue();
        this.discount = slid;
        discount_display.setText(String.valueOf(slid)+"%");
    }
}
