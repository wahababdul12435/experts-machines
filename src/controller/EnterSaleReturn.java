package controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDrawer;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.util.Callback;
import javafx.util.Duration;
import model.*;
import javafx.scene.web.WebView;
import model.SaleReturn;
import model.Product;
import javafx.stage.Stage;
import org.controlsfx.control.textfield.TextFields;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.text.SimpleDateFormat;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;


public class EnterSaleReturn implements Initializable {

    @FXML
    private TextField txt_dealerID;
    @FXML
    private Label lbl_discAmount;
    @FXML
    private Label lbl_totalAmount;
    @FXML
    private Label lbl_retailPrice;
    @FXML
    private Label lbl_purchPrice;
    @FXML
    private Label lbl_tradePrice;
    @FXML
    private Label lbl_pendingBalnce;
    @FXML
    private JFXComboBox<String> cmbobox_batch;
    @FXML
    private Button btn_add;
    @FXML
    private TextField txt_areaID;
    @FXML
    private TextField txt_sale_invID;
    @FXML
    private TextField txt_dealerName;
    @FXML
    private TextField txt_totalNetAmount;
    @FXML
    private TextField txt_dealerAdd;
    @FXML
    private TextField txt_totaldiscAmount;
    @FXML
    private Label lbl_stockQaunt;
    @FXML
    private MenuBar menu_bar;
    @FXML
    private Label lbl_bq;
    @FXML
    private Label lbl_sq;
    @FXML
    private Label lbl_da;
    @FXML
    private Label lbl_ta;
    @FXML
    private Label lbl_tp;
    @FXML
    private Label lbl_pp;
    @FXML
    private Label lbl_rp;
    @FXML
    private Label lbl_ai;
    @FXML
    private Label lbl_ci;
    @FXML
    private Label lbl_id;
    @FXML
    private TextField txt_prodID;
    @FXML
    private Label lbl_stockBONUSQaunt;
    @FXML
    private TextField txt_compID;
    @FXML
    private TextField txt_dealerLicense;
    @FXML
    private TextField txt_prodName;
    @FXML
    private TextField txt_returnNo;
    @FXML
    private TextField txt_bonus;
    @FXML
    private TextField txt_discount;
    @FXML
    private TextField txt_quant;
    @FXML
    private TextField txt_grossAmount;
    @FXML
    private TextField txt_batchNO;
    @FXML
    private AnchorPane inner_anchor;
    @FXML
    private Button btn11;
    @FXML
    private Pane btnpane;
    @FXML
    private FontAwesomeIconView FA_Icon;
    @FXML
    private BorderPane nav_sales;
    @FXML
    private JFXDrawer drawer;
    @FXML
    private DatePicker datepick_curntDate;
    @FXML
    private DatePicker datepick_saleinvoicedate;
    @FXML
    private ComboBox combo_packSize;
    @FXML
    private TableView tableView;

    Alert a = new Alert(Alert.AlertType.NONE);
    public static Button btnView;

    String[] getlast_returnNum = new String[5];
    public void initialize(URL url, ResourceBundle resourceBundle) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnSale.setStyle("-fx-background-color: #47ab1e");
        cmbobox_batch.setVisible(false);
        datepick_curntDate.setValue(LocalDate.now());
        btnpane.setOnMouseEntered(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(22);
                btnpane.setMinWidth(20.0);
                btn11.setMinWidth(20.0);

            }
        });
        btnpane.setOnMouseExited(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(14);
                btnpane.setMinWidth(10.0);
                btn11.setMinWidth(10.0);
            }
        });

        inner_anchor.setLeftAnchor(inner_anchor, 260d);
        drawer.setSidePane(nav_sales);
        drawer.open();
        /*txt_sale_invID.requestFocus();
        lbl_discAmount.setVisible(false);
        lbl_purchPrice.setVisible(false);
        lbl_retailPrice.setVisible(false);
        lbl_stockBONUSQaunt.setVisible(false);
        lbl_totalAmount.setVisible(false);
        lbl_tradePrice.setVisible(false);
        lbl_stockQaunt.setVisible(false);
        lbl_bq.setVisible(false);
        lbl_sq.setVisible(false);
        lbl_da.setVisible(false);
        lbl_ta.setVisible(false);
        lbl_tp.setVisible(false);
        lbl_pp.setVisible(false);
        lbl_rp.setVisible(false);
        lbl_ai.setVisible(false);
        lbl_ci.setVisible(false);
        txt_areaID.setVisible(false);
        txt_compID.setVisible(false);
        lbl_id.setVisible(false);
        datepick_saleinvoicedate.setVisible(false);*/

        ArrayList<String> newdealerlist = new ArrayList<>();
        MysqlCon newobjMysqlConnc = new MysqlCon();
        Statement objectStattmt = newobjMysqlConnc.stmt;
        Connection objectConnnec = newobjMysqlConnc.con;
        SaleInvoice objctsaleinvoice = new SaleInvoice();
        newdealerlist = objctsaleinvoice.get_AllDealers(objectStattmt,objectConnnec);
        String[] possiblee= new String[newdealerlist.size()];
        for(int i =0; i < newdealerlist.size();i++)
        {
            possiblee[i] = newdealerlist.get(i);
        }

        ArrayList<String> newprodlist = new ArrayList<>();
        MysqlCon objctsMysqlConnc = new MysqlCon();
        Statement objectStatmnt = objctsMysqlConnc.stmt;
        Connection objectConnnection = objctsMysqlConnc.con;
        newprodlist = objctsaleinvoice.get_Allprodcuts(objectStatmnt,objectConnnection);
        String[] Product_possiblee= new String[newprodlist.size()];
        for(int i =0; i < newprodlist.size();i++)
        {
            Product_possiblee[i] = newprodlist.get(i);
        }
        TextFields.bindAutoCompletion(txt_dealerName,possiblee);
        TextFields.bindAutoCompletion(txt_prodName,Product_possiblee);

        MysqlCon newobjMysqlCon = new MysqlCon();
        Statement objectStmt = newobjMysqlCon.stmt;
        Connection objectCon = newobjMysqlCon.con;
        SaleReturn objsalereturn = new SaleReturn();

        getlast_returnNum = objsalereturn.get_lastreturnnumber(objectStmt,objectCon);
        if(getlast_returnNum[0] == null)
        {
            txt_returnNo.setText("1");
        }
        else
            {
            int cnvrtsalinvoint = Integer.parseInt(getlast_returnNum[0]);
            cnvrtsalinvoint = cnvrtsalinvoint + 1;
            txt_returnNo.setText(String.valueOf(cnvrtsalinvoint));
        }
        ObservableList<String> prodPacking = FXCollections.observableArrayList();
        prodPacking.add( "Box");
        prodPacking.add("Packs");
        combo_packSize.setItems(prodPacking);
        combo_packSize.setValue("Packs");
        TableColumn productcompidCOL = new TableColumn("Company ID");
        productcompidCOL.setMinWidth(100);
        productcompidCOL.setVisible(false);
        TableColumn productidCOL = new TableColumn("Product ID");
        productidCOL.setMinWidth(100);
        TableColumn productnameCOL = new TableColumn("Product Name");
        productnameCOL.setMinWidth(200);
        TableColumn productBatchCOL = new TableColumn("Batch No.");
        productBatchCOL.setMinWidth(100);
        TableColumn productquantCOL = new TableColumn("Product Quantity");
        productquantCOL.setMinWidth(150);
        TableColumn productDiscountCOL = new TableColumn("Discount");
        productDiscountCOL.setMinWidth(100);

        TableColumn productBonusCOL = new TableColumn("Product Bonus");
        productBonusCOL.setMinWidth(150);
        TableColumn productTotalCOL = new TableColumn("Total");
        productTotalCOL.setMinWidth(100);
        TableColumn productpacksizeCOL = new TableColumn("Pack Size");
        productpacksizeCOL.setMinWidth(10);
        productpacksizeCOL.setVisible(false);
        TableColumn productdiscAmountCOL = new TableColumn("Discount Amount");
        productdiscAmountCOL.setMinWidth(10);
        productdiscAmountCOL.setVisible(false);
        TableColumn productprodstockCOL = new TableColumn("Quantity in Stock");
        productprodstockCOL.setMinWidth(10);
        productprodstockCOL.setVisible(false);
        TableColumn productprodbonusstockCOL = new TableColumn("Bonus Quantity in Stock");
        productprodbonusstockCOL.setMinWidth(10);
        productprodbonusstockCOL.setVisible(false);
        TableColumn productSecondBatchCOL = new TableColumn("Second Batch Bit");
        productSecondBatchCOL.setMinWidth(10);
        productSecondBatchCOL.setVisible(false);
        TableColumn productEditCOL = new TableColumn("Actions");
        productEditCOL.setMinWidth(200);
        TableColumn firstEDITCol = new TableColumn("Edit");
        TableColumn secondDELETECol = new TableColumn("Delete");

        productEditCOL.getColumns().addAll(firstEDITCol, secondDELETECol);
        tableView.getColumns().addAll(productcompidCOL,productidCOL, productnameCOL,productBatchCOL, productquantCOL,productDiscountCOL,productBonusCOL,productTotalCOL,productpacksizeCOL,productdiscAmountCOL,productprodstockCOL,productSecondBatchCOL,productprodbonusstockCOL,productEditCOL);
        productcompidCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("productCompId"));
        productidCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("productId"));
        productnameCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("productName"));
        productBatchCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("productBatch"));
        productquantCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("productQuant"));
        productDiscountCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("productDisc"));
        productBonusCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("productBonus"));
        productTotalCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("totalValue"));
        productpacksizeCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("packSize"));
        productdiscAmountCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("discAmount"));
        productprodstockCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("stockQuant"));
        productprodbonusstockCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("bonusstockQuant"));
        productSecondBatchCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("secondbatchBit"));
        //START of Creating Delete Button
        secondDELETECol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Product,Boolean>, ObservableValue>() {
            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<Product, Boolean> param) {
                return new SimpleBooleanProperty(param.getValue()!=null);
            }

        });
        secondDELETECol.setCellFactory(new Callback<TableColumn<Product, Boolean>, TableCell<Product, Boolean>>() {
            @Override
            public TableCell<Product, Boolean> call(TableColumn<Product, Boolean> param) {
                return new ButtonCell(tableView);
            }
        });
        //END of Creating Delete Button
        //START of Creating EDIT Button
        firstEDITCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Product,Boolean>, ObservableValue>() {
            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<Product, Boolean> param) {
                return new SimpleBooleanProperty(param.getValue()!=null);
            }

        });
        firstEDITCol.setCellFactory(new Callback<TableColumn<Product, Boolean>, TableCell<Product, Boolean>>() {
            @Override
            public TableCell<Product, Boolean> call(TableColumn<Product, Boolean> param) {
                return new Editbtncell(tableView);
            }
        });
        //END of Creating EDIT Button
        tableView.setItems(data);
        txt_sale_invID.requestFocus();
    }
    public void follow() throws IOException
    {
        if (drawer.isOpened()) {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(-250);
            transition.setByY(0);
            transition.setCycleCount(1);
            transition.play();
            drawer.close();
            FA_Icon.setGlyphName("ANGLE_DOUBLE_RIGHT");
            FA_Icon.setGlyphSize(14);
            //inner_anchor.setRightAnchor(inner_anchor,1d);


            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(-90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(1.28000);
            scale.setToY(1);



            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();
            //
            //inner_anchor.setLeftAnchor(inner_anchor, 5d);
        } else {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(250);
            transition.setByY(0);
            transition.setCycleCount(1);
            FA_Icon.setGlyphName("ANGLE_DOUBLE_LEFT");
            FA_Icon.setGlyphSize(14);
            drawer.open();

            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(0.99947777);
            scale.setToY(1);



            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();

        }


    }
    int EDIT_CHK_Value = 0;
    ObservableList<Product> data = FXCollections.observableArrayList();
    int rowvalueAfterEdit = 0;
    // START of Button Code for Editing a Row
    private class Editbtncell extends TableCell<Product,Boolean>{
        final Button btncell = new Button("Edit");

        public  Editbtncell(final TableView view1){
            btncell.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    rowvalueAfterEdit = getTableRow().getIndex();
                    Product newobj = (Product) tableView.getItems().get(rowvalueAfterEdit);
                    txt_compID.setText(newobj.getProductCompID());
                    txt_prodName.setText(newobj.getProductName());
                    txt_prodID.setText(newobj.getProductId());
                    txt_quant.setText(newobj.getProductQuant());
                    txt_bonus.setText(newobj.getProductBonus());
                    txt_batchNO.setText(newobj.getProductBatch());
                    txt_discount.setText(newobj.getProductDisc());
                    lbl_totalAmount.setText(newobj.getTotalValue());
                    combo_packSize.setValue(newobj.getPackSize());
                    int productID_foredt = Integer.parseInt(txt_prodID.getText());
                    MysqlCon objMysqlCon1 = new MysqlCon();
                    Statement objStmt1 = objMysqlCon1.stmt;
                    Connection objCon1 = objMysqlCon1.con;
                    SaleReturn objsalereturn1 = new SaleReturn();
                    proddetails_onIDEnter = objsalereturn1.get_prodDetailswithIDs(objStmt1, objCon1, productID_foredt);
                    if (proddetails_onIDEnter[0] == null) {
                    }
                    else
                    {
                        String[] myArray = proddetails_onIDEnter[0].split("--");
                        txt_prodName.setText(myArray[0]);
                        lbl_retailPrice.setText(myArray[1]);
                        lbl_purchPrice.setText(myArray[2]);
                        lbl_tradePrice.setText(myArray[3]);
                        txt_compID.setText(myArray[4]);

                    }
                    MysqlCon objMysqlCon = new MysqlCon();
                    Statement objStmt = objMysqlCon.stmt;
                    Connection objCon = objMysqlCon.con;
                    SaleInvoice objsaleinvoice = new SaleInvoice();
                    prodquantinstock_onIDEnter = objsaleinvoice.gettotalprodstockquant_withIDs(objStmt, objCon,   productID_foredt);
                    if(prodquantinstock_onIDEnter[0]==null)
                    {
                        a.setAlertType(Alert.AlertType.ERROR);
                        a.setHeaderText("Error!");
                        a.setContentText("Stock Quantity is Zero");
                        a.show();
                        txt_prodID.requestFocus();
                    }
                    else
                    {
                        String[] prodStock_quantarr = prodquantinstock_onIDEnter[0].split("--");
                        lbl_stockQaunt.setText(prodStock_quantarr[0]);
                        lbl_stockBONUSQaunt.setText(prodStock_quantarr[1]);}
                    txt_compID.setEditable(false);
                    txt_prodID.setEditable(false);
                    txt_prodName.setEditable(false);
                    EDIT_CHK_Value = 1;
                    txt_bonus.requestFocus();
                }
            });
        }
        @Override
        protected  void updateItem(Boolean n, boolean boln){
            super.updateItem(n,boln);
            if(boln){
                setGraphic(null);
            }
            else
            {
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setGraphic(btncell);
            }
        }
    }
    // END of Button Code for Editing a Row
    //START of Button Code for Deleting a Row
    private class ButtonCell extends TableCell<Product, Boolean> {

        final Button CellButton = new Button("Delete");
        public ButtonCell(final TableView view) {
            CellButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    int selectedIndex = getTableRow().getIndex();
                    data.remove(selectedIndex);
                    ///////////////////////////START OF CALCULATIONS TO GET TOTAL amount and total Discount amount OF ALL PRODUCT ADDED IN TBALEVIEW
                    Float sumval =0.0f;
                    Float finalsumofDiscount2 = 0.0f;
                    Float nettotalamount = 0.0f;
                    for(int i = 0; i<tableView.getItems().size();i++)
                    {
                        Product newwobj = (Product) tableView.getItems().get(i);
                        float newval = Float.parseFloat(newwobj.getTotalValue());
                        float finalsumofDiscount = Float.parseFloat(newwobj.getDiscAmount());
                        finalsumofDiscount2 = finalsumofDiscount2 + finalsumofDiscount;
                        sumval = sumval + newval;
                        nettotalamount = sumval + finalsumofDiscount2;
                    }
                    txt_totalNetAmount.setText(String.valueOf(sumval));
                    txt_totaldiscAmount.setText(String.valueOf(finalsumofDiscount2));
                    txt_grossAmount.setText(String.valueOf(nettotalamount));
                    txt_compID.requestFocus();
                    /////////////////////////////////END OF CALCULATIONS TO GET TOTAL amount and total Discount amount OF ALL PRODUCT ADDED IN TBALEVIEW
                }
            });
        }
        @Override
        protected void updateItem(Boolean t, boolean bln) {
            super.updateItem(t, bln);
            if (bln) {
                setGraphic(null);
            } else {
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setGraphic(CellButton);
            }

        }
    }
    //END of Button Code for Deleting a Row
    String whetherinvoIDEntered="n";
    String[] invoicedetails = new String[1];
    String[] dealdetails = new String[1];
    public void txtBoxsale_invoID_Enter(KeyEvent event) throws IOException, ParseException {
        if(event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB) {
            if (txt_sale_invID.getText().isEmpty() && txt_dealerID.getText().isEmpty()) {
                a.setAlertType(Alert.AlertType.ERROR);
                a.setHeaderText("Error!");
                a.setContentText("Please enter Either Dealer ID or Invoice No.");
                a.show();
                txt_dealerID.requestFocus();
            }
            else if (txt_sale_invID.getText().isEmpty())
            {
                txt_prodID.requestFocus();
            }
            else {
                if (Integer.parseInt(txt_sale_invID.getText()) == 0) {
                    txt_prodID.requestFocus();
                } else {

                    String txtValue_invoNo = txt_sale_invID.getText();
                    int intvalue_invoNo = Integer.parseInt(txtValue_invoNo);
                    MysqlCon objMysqlCon = new MysqlCon();
                    Statement objStmt = objMysqlCon.stmt;
                    Connection objCon = objMysqlCon.con;
                    SaleReturn objpurchinvoice = new SaleReturn();
                    invoicedetails = objpurchinvoice.getsaleInvodetails(objStmt, objCon, intvalue_invoNo);

                    if (invoicedetails[0] == null) {
                    } else {
                        String[] myArray = invoicedetails[0].split("--");
                        txt_dealerID.setText(myArray[0]);
                        String[] splitdate = myArray[1].split("/");
                        String nsss = splitdate[0] +'-' + splitdate[1] +'-'+ splitdate[2];
                        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
                        Date varDate=dateFormat.parse(nsss);
                        dateFormat=new SimpleDateFormat("dd-MM-yyyy");
                        String mydate = dateFormat.format(varDate);

                        String[] sdate = mydate.split("-");
                        int yyear = Integer.parseInt(sdate[2]);
                        int montth = Integer.parseInt(sdate[1]);
                        int ddate = Integer.parseInt(sdate[0]);

                        datepick_saleinvoicedate.setValue(LocalDate.of(yyear,montth,ddate));

                        txt_prodID.requestFocus();
                        whetherinvoIDEntered = "y";
                    }

                    String txtValue_dealrID = txt_dealerID.getText();
                    int intvalue_txtbxdealerID = Integer.parseInt(txtValue_dealrID);

                    MysqlCon objMysqlConnection = new MysqlCon();
                    Statement objectsStmt = objMysqlConnection.stmt;
                    Connection objectsCon = objMysqlConnection.con;
                    SaleReturn objectspurchinvoice = new SaleReturn();
                    dealdetails = objectspurchinvoice.getdealersdetails(objectsStmt, objectsCon, intvalue_txtbxdealerID);

                    if (dealdetails[0] == null) {
                    } else {
                        String[] dealerdetailArray = dealdetails[0].split("--");
                        txt_dealerName.setText(dealerdetailArray[0]);
                        txt_dealerAdd.setText(dealerdetailArray[1]);
                        txt_areaID.setText(dealerdetailArray[2]);
                        txt_dealerLicense.setText(dealerdetailArray[3]);
                        txt_prodID.requestFocus();
                        whetherinvoIDEntered = "y";
                    }
                }
            }
        }
    }

    String[] getbatch_quantfrmOrder = new String[5];
    String[] cnfrmExistncOfBatch = new String[5];
    public void toupperCase(KeyEvent event) throws IOException{
            txt_batchNO.setTextFormatter(new TextFormatter<>((change) -> {
            change.setText(change.getText().toUpperCase());
            return change;
        }));
        if(event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB)
        {
            if(whetherinvoIDEntered == "y")
            {
                int proddID = Integer.parseInt(txt_prodID.getText());
                SaleReturn newsaleretrnobject = new SaleReturn();

                MysqlCon mysqlConctOBJECT = new MysqlCon();
                Statement stamt_OBJ = mysqlConctOBJECT.stmt;
                Connection Conn_OBJ = mysqlConctOBJECT.con;
                cnfrmExistncOfBatch = newsaleretrnobject.cnfrmBatch_exist_inDB(stamt_OBJ, Conn_OBJ, proddID, txt_batchNO.getText());
                if (cnfrmExistncOfBatch[0] == null) {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setHeaderText("Error!");
                    a.setContentText("Batch not Available in Stock.");
                    a.show();
                    txt_batchNO.requestFocus();
                }
                else
                    {
                MysqlCon scndmysqlOBJECT = new MysqlCon();
                Statement secndmystamtOBJ = scndmysqlOBJECT.stmt;
                Connection secndmysqlConn = scndmysqlOBJECT.con;
                getbatch_quantfrmOrder = newsaleretrnobject.getbatch_quantFrmORDER(secndmystamtOBJ, secndmysqlConn, proddID, txt_batchNO.getText());
                if (getbatch_quantfrmOrder[0] == null) {
                    txt_discount.requestFocus();
                }
                        else
                            {
                    String[] batch_quantfrmORDER = getbatch_quantfrmOrder[0].split("--");
                    txt_quant.setText(batch_quantfrmORDER[0]);
                    txt_bonus.setText(batch_quantfrmORDER[1]);
                    txt_discount.setText(batch_quantfrmORDER[2]);
                    txt_discount.requestFocus();
                }
            }
            }
            else
            {
                int proddID = Integer.parseInt(txt_prodID.getText());
                SaleReturn newsaleretrnobject = new SaleReturn();

                MysqlCon mysqlConctOBJECT = new MysqlCon();
                Statement stamt_OBJ = mysqlConctOBJECT.stmt;
                Connection Conn_OBJ = mysqlConctOBJECT.con;
                cnfrmExistncOfBatch = newsaleretrnobject.cnfrmBatch_exist_inDB(stamt_OBJ, Conn_OBJ, proddID, txt_batchNO.getText());
                if (cnfrmExistncOfBatch[0] == null)
                {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setHeaderText("Error!");
                    a.setContentText("Batch not Available in Stock.");
                    a.show();
                    txt_batchNO.requestFocus();
                }
                else
                {
                    txt_discount.requestFocus();
                }
            }


        }
    }

    String[] getbatchno_quantity = new String[5];
    String[] getoverallrecord_dealer = new String[5];
    String batchquant_variable = "";
    String batchBonus_variable = "";
    int intbatchqaunt_val ;
    int ordered_packets;
    int submitted_packets;
    int ordered_boxes;
    int submitted_boxes ;
    int prev_ordered_packets;
    int prev_submitted_packets;
    int prev_ordered_boxes;
    int prev_submitted_boxes ;
    float order_price ;
    float discount_price;
    float invoiced_price;
    float pending_payments;
    public void btnsaveinvoice_clicks() throws IOException
    {

        int counttableROWS = tableView.getItems().size();
        if(counttableROWS == 0)
        {}
        else {
            String[] getprodid_tableview = new String[counttableROWS];
            String[] getprodCompid_tableview = new String[counttableROWS];
            String[] getprodQuant_tableview = new String[counttableROWS];
            String[] getprodBonus_tableview = new String[counttableROWS];
            String[] getprodDisc_tableview = new String[counttableROWS];
            String[] getpacksize_tableview = new String[counttableROWS];
            String[] getquant_inStock_tableview = new String[counttableROWS];
            String[] getbonusquant_inStock_tableview = new String[counttableROWS];
            String[] getprodTotal_table = new String[counttableROWS];
            String[] getdiscAmount_tableview = new String[counttableROWS];
            String[] getbatchnumber = new String[counttableROWS];


            for (int i = 0; i < counttableROWS; i++)//////////////////////////////////////START OF LOOP TO READ ALL ROWS OF TABLEVIEW
            {
                Product ProductObject = (Product) tableView.getItems().get(i);
                getprodid_tableview[i] = ProductObject.getProductId();
                getprodCompid_tableview[i] = ProductObject.getProductCompID();
                getprodQuant_tableview[i] = ProductObject.getProductQuant();
                getprodBonus_tableview[i] = ProductObject.getProductBonus();
                getprodDisc_tableview[i] = ProductObject.getProductDisc();
                getpacksize_tableview[i] = ProductObject.getPackSize();
                getquant_inStock_tableview[i] = ProductObject.getStockQuant();
                getbonusquant_inStock_tableview[i] = ProductObject.getBonusstockQuant();
                getprodTotal_table[i] = ProductObject.getTotalValue();
                getdiscAmount_tableview[i] = ProductObject.getDiscAmount();
                getbatchnumber[i] = ProductObject.getProductBatch();

                int intvalue_prodID = Integer.parseInt(getprodid_tableview[i]);
                int intvalue_prodQuant = Integer.parseInt(getprodQuant_tableview[i]);
                int intvalue_prodBonus = Integer.parseInt(getprodBonus_tableview[i]);
                int intvalue_stockquantity = Integer.parseInt(getquant_inStock_tableview[i]);
                int intvalue_bonusstockquantity = Integer.parseInt(getbonusquant_inStock_tableview[i]);
                float floatvalue_prodtotalamount = Float.parseFloat(ProductObject.getTotalValue());
                float flaotvalue_prodDiscamount = Float.parseFloat(ProductObject.getDiscAmount());
                float discvalues = Float.parseFloat(getprodDisc_tableview[i]);
                float grossamount = Float.parseFloat(getprodTotal_table[i]);
                float discount_amount = Float.parseFloat(getdiscAmount_tableview[i]);
                float order_PriceValue = grossamount - discount_amount;
                if (txt_sale_invID.getText().isEmpty()) {
                    txt_sale_invID.setText("0");
                }

                if(getpacksize_tableview[i].equals("Packs"))
                {
                    ordered_packets = ordered_packets + intvalue_prodQuant;
                    submitted_packets = submitted_packets + intvalue_prodQuant;
                }
                if(getpacksize_tableview[i].equals("Box"))
                {
                    ordered_boxes = ordered_boxes + intvalue_prodQuant;
                    submitted_boxes = submitted_boxes + intvalue_prodQuant;
                }

                MysqlCon mysqlOBJect = new MysqlCon();
                Statement mystatmntOBJ = mysqlOBJect.stmt;
                Connection mysqlConnect = mysqlOBJect.con;
                SaleInvoice objsaleinvoice1 = new SaleInvoice();
                getoverallrecord_dealer = objsaleinvoice1.getprevious_dealeroverallRecord(mystatmntOBJ, mysqlConnect, Integer.parseInt(txt_dealerID.getText()));
                if (getoverallrecord_dealer[0] == null) {

                } else {
                    String[] dealerOverAll_Records = getoverallrecord_dealer[0].split(",");
                    ordered_packets = Integer.parseInt(dealerOverAll_Records[1]);
                    submitted_packets = Integer.parseInt(dealerOverAll_Records[2]);
                    ordered_boxes = Integer.parseInt(dealerOverAll_Records[3]);
                    submitted_boxes = Integer.parseInt(dealerOverAll_Records[4]);
                    order_price = Float.parseFloat(dealerOverAll_Records[5]);
                    discount_price = Float.parseFloat(dealerOverAll_Records[6]);
                    invoiced_price = Float.parseFloat(dealerOverAll_Records[7]);
                    pending_payments = Float.parseFloat(dealerOverAll_Records[8]);

                    if (getpacksize_tableview[i].equals("Packs")) {
                        ordered_packets = ordered_packets - intvalue_prodQuant;
                        submitted_packets = submitted_packets - intvalue_prodQuant;
                    }
                    if (getpacksize_tableview[i].equals("Box")) {
                        ordered_boxes = ordered_boxes - intvalue_prodQuant;
                        submitted_boxes = submitted_boxes - intvalue_prodQuant;
                    }
                    discount_price = discount_price - discount_amount;
                    order_price = order_price - grossamount;
                    invoiced_price = invoiced_price - order_PriceValue;
                    pending_payments = pending_payments - order_PriceValue;
                }
                ////////////////////////////////////////////////////////////////////////////////////////START OF CALCULATION FOR ADDING QUANTITIES OF TOTAL STOCK
                int sum_prodQuant = 0;
                int sum_prodBonus = 0;
                if (intvalue_prodQuant > 0) {
                    sum_prodQuant = intvalue_stockquantity + intvalue_prodQuant;
                }
                if (intvalue_prodQuant > 0) {
                    sum_prodBonus = intvalue_bonusstockquantity + intvalue_prodBonus;
                }
                ////////////////////////////////////////////////////////////////////////////////////////END OF CALCULATION FOR ADDING QUANTITIES OF TOTAL STOCK
                ////////////////////////////////////////////////////////////////////////////////////////START --> GET BATCH QUANTITIES OF PRODUCT
                SaleReturn newsaleretrn_obj = new SaleReturn();
                MysqlCon scndmysqlOBJ = new MysqlCon();
                Statement scndmystamtOBJ = scndmysqlOBJ.stmt;
                Connection scndmysqlConn = scndmysqlOBJ.con;
                getbatchno_quantity = newsaleretrn_obj.getprodbatch_quantity(scndmystamtOBJ, scndmysqlConn, intvalue_prodID, getbatchnumber[i]);
                if (getbatchno_quantity[0] == null) {
                } else {
                    String[] scndbatch_quantitiesarray = getbatchno_quantity[0].split("--");
                    batchquant_variable = scndbatch_quantitiesarray[0];
                    batchBonus_variable = scndbatch_quantitiesarray[1];
                }
                ////////////////////////////////////////////////////////////////////////////////////////END --> GET BATCH QUANTITIES OF PRODUCT
                ////////////////////////////////////////////////////////////////////////////////////////START OF CALCULATION FOR ADDING QUANTITIES OF BATCH
                int intSecondbatchqaunt_val = Integer.parseInt(batchquant_variable);
                int intSecondbatchbonusqaunt_val = Integer.parseInt(batchBonus_variable);

                int sumbatch_quant = 0;
                int sumbatch_Bonusquant = 0;
                if (intvalue_prodQuant > 0) {
                    sumbatch_quant = intSecondbatchqaunt_val + intvalue_prodQuant;
                }
                if (intvalue_prodQuant > 0) {
                    sumbatch_Bonusquant = intSecondbatchbonusqaunt_val + intvalue_prodBonus;
                }
                ////////////////////////////////////////////////////////////////////////////////////////END OF CALCULATION FOR ADDING QUANTITIES OF BATCH

                MysqlCon MysqlConnctionObject = new MysqlCon();
                Statement StatmntObject = MysqlConnctionObject.stmt;
                Connection ConnctnObject = MysqlConnctionObject.con;

                if (whetherinvoIDEntered == "y") {////////////////////////////////////////////////////////////////////////////////////////START OF CONDITION TO CHECK IF WHOLE QUANTITY IS RETURNED
                    int get_secndreferenceQuantity = Integer.parseInt(getprodQuant_tableview[i]);
                    int get_secndreferenceBonusQuantity = Integer.parseInt(getprodBonus_tableview[i]);
                    if (getQuantityAs_reference == get_secndreferenceQuantity && getBonusQuantityAs_reference == get_secndreferenceBonusQuantity) {
                        newsaleretrn_obj.changeretrnBit(StatmntObject, ConnctnObject, getbatchnumber[i], intvalue_prodID, Integer.parseInt(txt_sale_invID.getText()));
                    } else {
                        newsaleretrn_obj.enterretrnrtrnquant(StatmntObject, ConnctnObject, getbatchnumber[i], intvalue_prodID, Integer.parseInt(txt_sale_invID.getText()), get_secndreferenceQuantity, get_secndreferenceBonusQuantity);
                    }

                    whetherinvoIDEntered = "n";
                }////////////////////////////////////////////////////////////////////////////////////////END OF CONDITION TO CHECK IF WHOLE QUANTITY IS RETURNED

                MysqlCon objMynewsqlCon = new MysqlCon();
                Statement newobjStmt = objMynewsqlCon.stmt;
                Connection newobjCon = objMynewsqlCon.con;
                newsaleretrn_obj.insertReturnDetails(newobjStmt, newobjCon, Integer.parseInt(txt_returnNo.getText()), intvalue_prodID, getbatchnumber[i], intvalue_prodQuant, intvalue_prodBonus, flaotvalue_prodDiscamount, floatvalue_prodtotalamount, getpacksize_tableview[i]);
                /////////////////////////////////////////////////////////////////////////////////////// Start of UPDATE PRODUCT TOTAL STOCK AFTER RETURN
                MysqlCon objMysqlCon1 = new MysqlCon();
                Statement objStmt1 = objMysqlCon1.stmt;
                Connection objCon1 = objMysqlCon1.con;
                newsaleretrn_obj.addQuantinStock(objStmt1, objCon1, intvalue_prodID, sum_prodQuant, sum_prodBonus);
                /////////////////////////////////////////////////////////////////////////////////////// End of UPDATE PRODUCT TOTAL STOCK AFTER RETURN
/////////////////////////////////////////////////////////////////////////////////////// Start of UPDATE PRODUCT BATCH STOCK AFTER RETURN
                MysqlCon objMysqlCons = new MysqlCon();
                Statement objStments = objMysqlCons.stmt;
                Connection objConctins = objMysqlCons.con;
                newsaleretrn_obj.addQuant_inbatchStock(objStments, objConctins, intvalue_prodID, getbatchnumber[i], sumbatch_quant, sumbatch_Bonusquant);
                /////////////////////////////////////////////////////////////////////////////////////// End of UPDATE PRODUCT BATCH STOCK AFTER RETURN
            } //////////////////////////////////////END OF LOOP TO READ ALL ROWS OF TABLVIEW
            SaleReturn saleretrn_Obj = new SaleReturn();
            MysqlCon mysqlOBJect = new MysqlCon();
            Statement mystatmntOBJ = mysqlOBJect.stmt;
            Connection mysqlConnect = mysqlOBJect.con;
            getoverallrecord_dealer = saleretrn_Obj.getprevious_dealeroverallRecord(mystatmntOBJ,mysqlConnect,Integer.parseInt(txt_dealerID.getText()));
            float TotalGross_amount = Float.parseFloat(txt_totalNetAmount.getText());
            float Totaldisc_amount = Float.parseFloat(txt_totaldiscAmount.getText());
            float Totalnet_amount = Float.parseFloat(txt_grossAmount.getText());
            if(getoverallrecord_dealer[0]==null)
            {
                MysqlCon mysqlOBJects = new MysqlCon();
                Statement mystatmntOBJect = mysqlOBJects.stmt;
                Connection mysqlConnection = mysqlOBJects.con;
                saleretrn_Obj.insertdealerOverallRecord(mystatmntOBJect,mysqlConnection,Integer.parseInt(txt_dealerID.getText()) , 1,0,ordered_packets,submitted_packets,ordered_boxes,submitted_boxes,TotalGross_amount,Totaldisc_amount,Totalnet_amount,0,Totalnet_amount);
            }
            else
            {
                String[] dealerOverAll_Records = getoverallrecord_dealer[0].split(",");
                prev_ordered_packets = Integer.parseInt(dealerOverAll_Records[0]);
                prev_submitted_packets = Integer.parseInt(dealerOverAll_Records[1]);
                prev_ordered_boxes = Integer.parseInt(dealerOverAll_Records[2]);
                prev_submitted_boxes = Integer.parseInt(dealerOverAll_Records[3]);
                order_price = Float.parseFloat(dealerOverAll_Records[4]);
                discount_price = Float.parseFloat(dealerOverAll_Records[5]);
                invoiced_price = Float.parseFloat(dealerOverAll_Records[6]);
                pending_payments = Float.parseFloat(dealerOverAll_Records[7]);

                ordered_packets = prev_ordered_packets - ordered_packets;
                submitted_packets = prev_submitted_packets - submitted_packets;
                ordered_boxes =prev_ordered_boxes-  ordered_boxes ;
                submitted_boxes =prev_submitted_boxes -  submitted_boxes;

                discount_price = discount_price - Totaldisc_amount;
                order_price = order_price - TotalGross_amount;
                invoiced_price = invoiced_price - Totalnet_amount;
                pending_payments = pending_payments - Totalnet_amount;

                MysqlCon mysqlOBJects = new MysqlCon();
                Statement mystatmntOBJect = mysqlOBJects.stmt;
                Connection mysqlConnection = mysqlOBJects.con;
                saleretrn_Obj.updatedealerOverallRecord(mystatmntOBJect,mysqlConnection,Integer.parseInt(txt_dealerID.getText()) ,ordered_packets,submitted_packets,ordered_boxes,submitted_boxes,order_price,discount_price,invoiced_price,pending_payments);
            }

            Float getorderprice = Float.parseFloat(txt_totalNetAmount.getText());
            Float gettotalprice = Float.parseFloat(txt_grossAmount.getText());

            LocalDate getdates = datepick_saleinvoicedate.getValue();
            String newdates = getdates.format(DateTimeFormatter.ofPattern("dd/MMM/yyyy"));
            LocalTime gettime = LocalTime.now();
            String newtime = gettime.format(DateTimeFormatter.ofPattern("hh:mm a"));

            MysqlCon objMysqlCon = new MysqlCon();
            Statement objStmt = objMysqlCon.stmt;
            Connection objCon = objMysqlCon.con;
            SaleReturn saleretrn_obj = new SaleReturn();
            saleretrn_obj.insertreturnInvoiceInfo(objStmt, objCon, Integer.parseInt(txt_returnNo.getText()), Integer.parseInt(txt_dealerID.getText()), getorderprice, gettotalprice, Integer.parseInt(txt_sale_invID.getText()), newdates,newtime,newdates ,newtime ,1);
            Parent root = FXMLLoader.load(getClass().getResource("../view/sale_return.fxml"));
            GlobalVariables.baseScene.setRoot(root);
            GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
        }
    }
    String dealerdetails_onEnterNamebox[] = new String[1];
    public void dealerName_enterpress(KeyEvent event) throws IOException
    {
        if (event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB)
        {
            if(!txt_dealerName.getText().equals("")){
                String dealer_name = txt_dealerName.getText();

                MysqlCon objMysqlCon = new MysqlCon();
                Statement objStmt = objMysqlCon.stmt;
                Connection objCon = objMysqlCon.con;
                SaleInvoice objsaleinvoice = new SaleInvoice();
                dealerdetails_onEnterNamebox = objsaleinvoice.get_dealerDetailswithname(objStmt, objCon, dealer_name);
                if (dealerdetails_onEnterNamebox[0] == null) {

                } else {
                    String[] myArray = dealerdetails_onEnterNamebox[0].split("--");
                    txt_areaID.setText(myArray[1]);
                    txt_dealerID.setText(myArray[0]);
                    txt_dealerLicense.setText(myArray[3]);
                    txt_dealerAdd.setText(myArray[2]);
                    txt_prodID.requestFocus();
                }
            }
        }

    }
    /////////////////////////////////////////////////////////////////////////// START of Enter Press on Dealer ID
    String dealerdetails_onIDEnter[] = new String[1];
    String dealer_overallRec;
    public void enterpress_dealerID() throws IOException{
        SaleReturn objsalereturn = new SaleReturn();
        String dealer_ids = txt_dealerID.getText();
        int intdealer_id = Integer.parseInt(dealer_ids);

        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        dealerdetails_onIDEnter = objsalereturn.getdealerDetailswithIDs(objStmt, objCon,  intdealer_id);
        if(dealerdetails_onIDEnter[0] == null){

        }
        else {
            String[] myArray = dealerdetails_onIDEnter[0].split("--");
            txt_dealerName.setText(myArray[0]);
            txt_dealerAdd.setText(myArray[1]);
            txt_dealerLicense.setText(myArray[2]);
            txt_areaID.setText(myArray[3]);
            txt_sale_invID.requestFocus();
        }
        MysqlCon MysqlCon_object = new MysqlCon();
        Statement Stmt_object = MysqlCon_object.stmt;
        Connection Con_object = MysqlCon_object.con;
        dealer_overallRec = objsalereturn.getdealer_overallRecord(Stmt_object, Con_object,  intdealer_id);
        if(dealer_overallRec.isEmpty()){

        }
        else
        {
            lbl_pendingBalnce.setText(dealer_overallRec);
        }
    }
    /////////////////////////////////////////////////////////////////////////// End of Enter Press on Dealer ID


    /////////////////////////////////////////////////////////////////////////// START of ENter Press on Button Add
    public  void  btnadd_enterpress(KeyEvent event) throws  IOException     {
        if(event.getCode() == KeyCode.ENTER)
        {
            if(EDIT_CHK_Value == 1)
            {
                if(txt_quant.getText() == "")
                {
                    txt_quant.setText("0");
                }
                if(txt_bonus.getText() == "")
                {
                    txt_bonus.setText("0");
                }
                int prodQuant = Integer.parseInt(txt_quant.getText());
                float discValue = Float.parseFloat(txt_discount.getText());
                float prodRetail = Float.parseFloat(lbl_retailPrice.getText());
                float totalprice = prodQuant*prodRetail;
                float discountedValue = (totalprice * discValue)/100;
                float afterdiscValue = totalprice - discountedValue;
                lbl_totalAmount.setText(String.valueOf(afterdiscValue));
                lbl_discAmount.setText(String.valueOf(discountedValue));

                Product newobj = (Product) tableView.getItems().get(rowvalueAfterEdit);
                newobj.setProductCompID(txt_compID.getText());
                newobj.setProductId(txt_prodID.getText());
                newobj.setProductName(txt_prodName.getText());
                newobj.setProductBatch(txt_batchNO.getText());
                newobj.setProductQuant(txt_quant.getText());
                newobj.setProductBonus(txt_bonus.getText());
                newobj.setProductDisc(txt_discount.getText());
                newobj.setTotalValue(lbl_totalAmount.getText());
                newobj.setPackSize(combo_packSize.getValue().toString());
                newobj.setDiscAmount(lbl_discAmount.getText());
                newobj.setStockQuant(lbl_stockQaunt.getText());
                newobj.setBonusstockQuant(lbl_stockBONUSQaunt.getText());
                //objnew.setSecondbatchBit(String.valueOf(useofSecndBatch));
                tableView.refresh();
                txt_prodID.setEditable(true);
                txt_prodName.setEditable(true);
                txt_prodID.setText("");
                txt_prodName.setText("");
                txt_quant.setText("");
                txt_bonus.setText("0");
                txt_discount.setText("0");
                txt_compID.setText("");
                txt_batchNO.setText("");
                combo_packSize.setValue("Packs");
                lbl_totalAmount.setText("0.00");
                lbl_tradePrice.setText("0.00");
                lbl_purchPrice.setText("0.00");
                lbl_retailPrice.setText("0.00");
                lbl_discAmount.setText("0.00");
                lbl_stockQaunt.setText("-");
                lbl_stockBONUSQaunt.setText("-");

                ///////////////////////////START OF CALCULATIONS TO GET TOTAL amount and total Discount amount OF ALL PRODUCT ADDED IN TBALEVIEW
                Float sumval =0.0f;
                Float finalsumofDiscount2 = 0.0f;
                Float nettotalamount = 0.0f;
                for(int i = 0; i<tableView.getItems().size();i++)
                {
                    Product newwobj = (Product) tableView.getItems().get(i);
                    float newval = Float.parseFloat(newwobj.getTotalValue());
                    float finalsumofDiscount = Float.parseFloat(newwobj.getDiscAmount());
                    finalsumofDiscount2 = finalsumofDiscount2 + finalsumofDiscount;
                    sumval = sumval + newval;
                    nettotalamount = sumval + finalsumofDiscount2;
                }
                txt_totalNetAmount.setText(String.valueOf(sumval));
                txt_totaldiscAmount.setText(String.valueOf(finalsumofDiscount2));
                txt_grossAmount.setText(String.valueOf(nettotalamount));
                /////////////////////////////////END OF CALCULATIONS TO GET TOTAL amount and total Discount amount OF ALL PRODUCT ADDED IN TBALEVIEW
                txt_prodID.requestFocus();
            }
            else {
                if (txt_prodName.getText().isEmpty()) {
                    txt_prodName.setText("ERROR");
                } else if (txt_prodID.getText().isEmpty() || txt_quant.getText().isEmpty()) {
                    txt_prodID.setText("ERROR");
                } else if (txt_quant.getText().isEmpty()) {
                    txt_quant.setText("ERROR");
                }
            /*if(txt_bonus.getText().isEmpty()){
                txt_bonus.setText("ERROR");
            }*/
                else {
                    if(txt_quant.getText() == "")
                    {
                        txt_quant.setText("0");
                    }
                    if(txt_bonus.getText() == "")
                    {
                        txt_bonus.setText("0");
                    }
                    int prodQuant = Integer.parseInt(txt_quant.getText());
                    float discValue = Float.parseFloat(txt_discount.getText());
                    float prodRetail = Float.parseFloat(lbl_retailPrice.getText());
                    float totalprice = prodQuant*prodRetail;
                    float discountedValue = (totalprice * discValue)/100;
                    float afterdiscValue = totalprice - discountedValue;
                    lbl_totalAmount.setText(String.valueOf(afterdiscValue));
                    lbl_discAmount.setText(String.valueOf(discountedValue));

                    Product objnew = new Product();
                    objnew.setProductCompID(txt_compID.getText());
                    objnew.setProductId(txt_prodID.getText());
                    objnew.setProductName(txt_prodName.getText());
                    objnew.setProductBatch(txt_batchNO.getText());
                    objnew.setProductQuant(txt_quant.getText());
                    objnew.setProductBonus(txt_bonus.getText());
                    objnew.setProductDisc(txt_discount.getText());
                    objnew.setTotalValue(lbl_totalAmount.getText());
                    objnew.setPackSize(combo_packSize.getValue().toString());
                    objnew.setDiscAmount(lbl_discAmount.getText());
                    objnew.setStockQuant(lbl_stockQaunt.getText());
                    objnew.setBonusstockQuant(lbl_stockBONUSQaunt.getText());
                    //objnew.setSecondbatchBit(String.valueOf(useofSecndBatch));
                    tableView.getItems().add(objnew);
                    txt_prodID.setText("");
                    txt_prodName.setText("");
                    txt_quant.setText("");
                    txt_bonus.setText("0");
                    txt_discount.setText("0");
                    txt_compID.setText("");
                    txt_batchNO.setText("");
                    combo_packSize.setValue("Packs");
                    lbl_totalAmount.setText("0.00");
                    lbl_tradePrice.setText("0.00");
                    lbl_purchPrice.setText("0.00");
                    lbl_retailPrice.setText("0.00");
                    lbl_discAmount.setText("0.00");
                    lbl_stockQaunt.setText("-");
                    lbl_stockBONUSQaunt.setText("-");
                    ///////////////////////////START OF CALCULATIONS TO GET TOTAL amount and total Discount amount OF ALL PRODUCT ADDED IN TBALEVIEW
                    Float sumval =0.0f;
                    Float finalsumofDiscount2 = 0.0f;
                    Float nettotalamount = 0.0f;
                    for(int i = 0; i<tableView.getItems().size();i++)
                    {
                        Product newwobj = (Product) tableView.getItems().get(i);
                        float newval = Float.parseFloat(newwobj.getTotalValue());
                        float finalsumofDiscount = Float.parseFloat(newwobj.getDiscAmount());
                        finalsumofDiscount2 = finalsumofDiscount2 + finalsumofDiscount;
                        sumval = sumval + newval;
                        nettotalamount = sumval + finalsumofDiscount2;
                    }
                    txt_totalNetAmount.setText(String.valueOf(sumval));
                    txt_totaldiscAmount.setText(String.valueOf(finalsumofDiscount2));
                    txt_grossAmount.setText(String.valueOf(nettotalamount));
                    /////////////////////////////////END OF CALCULATIONS TO GET TOTAL amount and total Discount amount OF ALL PRODUCT ADDED IN TBALEVIEW
                    txt_prodID.requestFocus();

                }
            }

        }

    }
    /////////////////////////////////////////////////////////////////////////// End of ENter Press on Button Add
    /////////////////////////////////////////////////////////////////////////// Start of Click on Button Add
    public void btnadd_clicks() throws IOException{
        if(Float.parseFloat(lbl_totalAmount.getText()) == 0.00f)
        {
            if(Integer.parseInt(txt_discount.getText())==0)
            {
                if (!txt_quant.getText().isEmpty())
                {
                    float getretail_floatval = Float.parseFloat(lbl_tradePrice.getText());
                    int intcnvrt_quant = Integer.parseInt(txt_quant.getText());

                    float totalquantity_price = intcnvrt_quant * getretail_floatval;
                    lbl_totalAmount.setText(String.valueOf(totalquantity_price));

                }
            }
            else
            {
                if (!txt_quant.getText().isEmpty() && Integer.parseInt(txt_discount.getText())!=0)
                {
                    float intgetretail_price = Integer.parseInt(lbl_tradePrice.getText());
                    int intcnvrt_quant = Integer.parseInt(txt_quant.getText());
                    float intdiscount_value = Integer.parseInt(txt_discount.getText());
                    float totalquantity_price = intcnvrt_quant * intgetretail_price;
                    float disc_firstVal = (totalquantity_price * intdiscount_value) / 100;
                    lbl_discAmount.setText(String.valueOf(disc_firstVal));
                    float final_discVal = totalquantity_price - disc_firstVal;
                    lbl_totalAmount.setText(String.valueOf(final_discVal));

                }
                else
                {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setHeaderText("Input Field Empty");
                    a.setContentText("Please Enter Quantity");
                    a.show();
                    txt_quant.requestFocus();
                }
            }
        }
        if(EDIT_CHK_Value == 1)
        {
            Product newobj = (Product) tableView.getItems().get(rowvalueAfterEdit);
            if(txt_quant.getText() == "")
            {
                txt_quant.setText("0");
            }
            if(txt_bonus.getText() == "")
            {
                txt_bonus.setText("0");
            }
            newobj.setProductCompID(txt_compID.getText());
            newobj.setProductId(txt_prodID.getText());
            newobj.setProductName(txt_prodName.getText());
            newobj.setProductBatch(txt_batchNO.getText());
            newobj.setProductQuant(txt_quant.getText());
            newobj.setProductBonus(txt_bonus.getText());
            newobj.setProductDisc(txt_discount.getText());
            newobj.setTotalValue(lbl_totalAmount.getText());
            newobj.setPackSize(combo_packSize.getValue().toString());
            newobj.setDiscAmount(lbl_discAmount.getText());
            newobj.setStockQuant(lbl_stockQaunt.getText());
            newobj.setBonusstockQuant(lbl_stockBONUSQaunt.getText());
            //newobj.setSecondbatchBit(String.valueOf(useofSecndBatch));
            tableView.refresh();
            txt_prodID.setEditable(true);
            txt_prodName.setEditable(true);
            txt_prodID.setText("");
            txt_prodName.setText("");
            txt_quant.setText("");
            txt_bonus.setText("");
            txt_discount.setText("");
            txt_compID.setText("");
            txt_batchNO.setText("");
            combo_packSize.setValue("Packs");
            lbl_totalAmount.setText("0.00");
            lbl_tradePrice.setText("0.00");
            lbl_purchPrice.setText("0.00");
            lbl_retailPrice.setText("0.00");
            lbl_discAmount.setText("0.00");
            lbl_stockQaunt.setText("-");
            lbl_stockBONUSQaunt.setText("-");
            ///////////////////////////START OF CALCULATIONS TO GET TOTAL amount and total Discount amount OF ALL PRODUCT ADDED IN TBALEVIEW
            Float sumval =0.0f;
            Float finalsumofDiscount2 = 0.0f;
            Float nettotalamount = 0.0f;
            for(int i = 0; i<tableView.getItems().size();i++)
            {
                Product newwobj = (Product) tableView.getItems().get(i);
                float newval = Float.parseFloat(newwobj.getTotalValue());
                float finalsumofDiscount = Float.parseFloat(newwobj.getDiscAmount());
                finalsumofDiscount2 = finalsumofDiscount2 + finalsumofDiscount;
                sumval = sumval + newval;
                nettotalamount = sumval + finalsumofDiscount2;
            }
            txt_totalNetAmount.setText(String.valueOf(sumval));
            txt_totaldiscAmount.setText(String.valueOf(finalsumofDiscount2));
            txt_grossAmount.setText(String.valueOf(nettotalamount));
            /////////////////////////////////END OF CALCULATIONS TO GET TOTAL amount and total Discount amount OF ALL PRODUCT ADDED IN TBALEVIEW
            txt_prodID.requestFocus();
            EDIT_CHK_Value = 0;
        }
        else {
            if (txt_prodName.getText().isEmpty()) {
                txt_prodName.setText("ERROR");
            } else if (txt_prodID.getText().isEmpty() || txt_quant.getText().isEmpty()) {
                txt_prodID.setText("ERROR");
            } else if (txt_quant.getText().isEmpty()) {
                txt_quant.setText("ERROR");
            }
            else {
                if(txt_quant.getText() == "")
                {
                    txt_quant.setText("0");
                }
                if(txt_bonus.getText() == "")
                {
                    txt_bonus.setText("0");
                }
                Product objnew = new Product();
                objnew.setProductCompID(txt_compID.getText());
                objnew.setProductId(txt_prodID.getText());
                objnew.setProductName(txt_prodName.getText());
                objnew.setProductQuant(txt_quant.getText());
                objnew.setProductBatch(txt_batchNO.getText());
                objnew.setProductBonus(txt_bonus.getText());
                objnew.setProductDisc(txt_discount.getText());
                objnew.setTotalValue(lbl_totalAmount.getText());
                objnew.setPackSize(combo_packSize.getValue().toString());
                objnew.setDiscAmount(lbl_discAmount.getText());
                objnew.setStockQuant(lbl_stockQaunt.getText());
                objnew.setBonusstockQuant(lbl_stockBONUSQaunt.getText());

                tableView.getItems().add(objnew);
                txt_prodID.setText("");
                txt_prodName.setText("");
                txt_quant.setText("");
                txt_bonus.setText("0");
                txt_discount.setText("0");
                txt_compID.setText("");
                txt_batchNO.setText("");
                combo_packSize.setValue("Packs");
                lbl_totalAmount.setText("0.00");
                lbl_tradePrice.setText("0.00");
                lbl_purchPrice.setText("0.00");
                lbl_retailPrice.setText("0.00");
                lbl_discAmount.setText("0.00");
                lbl_stockQaunt.setText("-");
                lbl_stockBONUSQaunt.setText("-");
                ///////////////////////////START OF CALCULATIONS TO GET TOTAL amount and total Discount amount OF ALL PRODUCT ADDED IN TBALEVIEW
                Float sumval =0.0f;
                Float finalsumofDiscount2 = 0.0f;
                Float nettotalamount = 0.0f;
                for(int i = 0; i<tableView.getItems().size();i++)
                {
                    Product newwobj = (Product) tableView.getItems().get(i);
                    float newval = Float.parseFloat(newwobj.getTotalValue());
                    float finalsumofDiscount = Float.parseFloat(newwobj.getDiscAmount());
                    finalsumofDiscount2 = finalsumofDiscount2 + finalsumofDiscount;
                    sumval = sumval + newval;
                    nettotalamount = sumval + finalsumofDiscount2;
                }
                txt_totalNetAmount.setText(String.valueOf(sumval));
                txt_totaldiscAmount.setText(String.valueOf(finalsumofDiscount2));
                txt_grossAmount.setText(String.valueOf(nettotalamount));
                /////////////////////////////////END OF CALCULATIONS TO GET TOTAL amount and total Discount amount OF ALL PRODUCT ADDED IN TBALEVIEW
                txt_prodID.requestFocus();

            }
        }
    }
    /////////////////////////////////////////////////////////////////////////// End of Click on Button Add


    /////////////////////////////////////////////////////////////////////////// START of Enter Press on Product ID
    String proddetails_onIDEnter[] = new String[1];
    String prodquantinstock_onIDEnter[] = new String[1];
    String cnfrmprodID_inInvoice[] =new String[1];
    String getbatchfromOrderTable[] = new String[1];
    int getQuantityAs_reference = 0;
    int getBonusQuantityAs_reference = 0;
    ArrayList<String> prod_batchlist = new ArrayList<>();
    String getbatch_rates[] = new String[1];
    public void enterpress_prodID(KeyEvent event) throws IOException{
        if(event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB) {
            if (!txt_prodID.getText().isEmpty()) {
                String product_ids = txt_prodID.getText();
                int intprod_id = Integer.parseInt(product_ids);
                if (txt_sale_invID.getText().isEmpty()) {
                    MysqlCon objMysqlCon1 = new MysqlCon();
                    Statement objStmt1 = objMysqlCon1.stmt;
                    Connection objCon1 = objMysqlCon1.con;
                    SaleReturn objsalereturn1 = new SaleReturn();
                    proddetails_onIDEnter = objsalereturn1.get_prodDetailswithIDs(objStmt1, objCon1, intprod_id);
                    if (proddetails_onIDEnter[0] == null) {
                        a.setAlertType(Alert.AlertType.ERROR);
                        a.setHeaderText("Error!");
                        a.setContentText("Product ID is not Correct");
                        a.show();
                        txt_prodID.requestFocus();
                    } else {
                        String[] myArray = proddetails_onIDEnter[0].split("--");
                        txt_prodName.setText(myArray[0]);
                        lbl_retailPrice.setText(myArray[1]);
                        lbl_purchPrice.setText(myArray[2]);
                        lbl_tradePrice.setText(myArray[3]);
                        txt_compID.setText(myArray[4]);

                        MysqlCon objMysqlCon = new MysqlCon();
                        Statement objStmnt = objMysqlCon.stmt;
                        Connection objCons = objMysqlCon.con;
                        SaleReturn objctsalereturn = new SaleReturn();

                        prodquantinstock_onIDEnter = objctsalereturn.getprodstockquant_withIDs(objStmnt, objCons, intprod_id);
                        if (prodquantinstock_onIDEnter[0] == null) {
                        } else {
                            String[] prodStock_quantarr = prodquantinstock_onIDEnter[0].split("--");
                            lbl_stockQaunt.setText(prodStock_quantarr[0]);
                            lbl_stockBONUSQaunt.setText(prodStock_quantarr[1]);
                            txt_batchNO.requestFocus();
                        }
                        MysqlCon objctsMysqlCon = new MysqlCon();
                        Statement objctsStmnt = objctsMysqlCon.stmt;
                        Connection objctsCons = objctsMysqlCon.con;
                        SaleReturn objects_salereturn = new SaleReturn();

                        prod_batchlist = objects_salereturn.getprod_batchs(objctsStmnt, objctsCons, intprod_id);
                        if (prod_batchlist.isEmpty()) {
                        } else {
                            String[] Batch_possiblee = new String[prod_batchlist.size()];
                            ObservableList<String> productBatches = FXCollections.observableArrayList();
                            for (int i = 0; i < prod_batchlist.size(); i++) {
                                Batch_possiblee[i] = prod_batchlist.get(i);
                                productBatches.add(prod_batchlist.get(i));
                            }

                            TextFields.bindAutoCompletion(txt_batchNO, Batch_possiblee);
                            cmbobox_batch.setItems(productBatches);
                        }
                    }

                } else {
                    String saleinvoice_ids = txt_sale_invID.getText();
                    int intsaleInvoice_id = Integer.parseInt(saleinvoice_ids);

                    MysqlCon objMysqlCon1 = new MysqlCon();
                    Statement objStmt1 = objMysqlCon1.stmt;
                    Connection objCon1 = objMysqlCon1.con;
                    SaleReturn objsalereturn1 = new SaleReturn();
                    proddetails_onIDEnter = objsalereturn1.get_prodDetailswithIDs(objStmt1, objCon1, intprod_id);
                    if (proddetails_onIDEnter[0] == null) {
                        a.setAlertType(Alert.AlertType.ERROR);
                        a.setHeaderText("Error!");
                        a.setContentText("Product ID is not Correct");
                        a.show();
                        txt_prodID.requestFocus();
                    } else {
                        String[] myArray = proddetails_onIDEnter[0].split("--");
                        txt_prodName.setText(myArray[0]);
                        lbl_retailPrice.setText(myArray[1]);
                        lbl_purchPrice.setText(myArray[2]);
                        lbl_tradePrice.setText(myArray[3]);
                        txt_compID.setText(myArray[4]);

                        MysqlCon objMysqlCon = new MysqlCon();
                        Statement objStmnt = objMysqlCon.stmt;
                        Connection objCons = objMysqlCon.con;
                        SaleReturn objctsalereturn = new SaleReturn();

                        prodquantinstock_onIDEnter = objctsalereturn.getprodstockquant_withIDs(objStmnt, objCons, intprod_id);
                        if (prodquantinstock_onIDEnter[0] == null) {
                        } else {
                            String[] prodStock_quantarr = prodquantinstock_onIDEnter[0].split("--");
                            lbl_stockQaunt.setText(prodStock_quantarr[0]);
                            lbl_stockBONUSQaunt.setText(prodStock_quantarr[1]);
                        }


                        MysqlCon objMysqlCons = new MysqlCon();
                        Statement objStmt = objMysqlCons.stmt;
                        Connection objCon = objMysqlCons.con;
                        SaleReturn objsalereturn = new SaleReturn();

                        cnfrmprodID_inInvoice = objsalereturn.cnfrmprodID_inSaleInvoice(objStmt, objCon, intsaleInvoice_id, intprod_id);
                        if (cnfrmprodID_inInvoice[0] == null) {
                            txt_batchNO.requestFocus();

                        } else {

                            MysqlCon Object_mysqlCON = new MysqlCon();
                            Statement getstmntObj = Object_mysqlCON.stmt;
                            Connection getconectionObj = Object_mysqlCON.con;
                            SaleReturn getsalereturnOBJ = new SaleReturn();
                            getbatchfromOrderTable = getsalereturnOBJ.getexistingBatchandDetails(getstmntObj, getconectionObj, intprod_id, intsaleInvoice_id);
                            if (getbatchfromOrderTable[0] == null) {

                            } else {

                                String[] prodbatchANDQauntity = getbatchfromOrderTable[0].split("--");
                                if (prodbatchANDQauntity[0].equals("1")) {
                                    a.setAlertType(Alert.AlertType.ERROR);
                                    a.setHeaderText("Error");
                                    a.setContentText("Already Returned");
                                    a.show();
                                } else {
                                    txt_batchNO.setText(prodbatchANDQauntity[3]);
                                    txt_quant.setText(prodbatchANDQauntity[4]);
                                    txt_discount.setText(prodbatchANDQauntity[1]);
                                    txt_bonus.setText(prodbatchANDQauntity[2]);
                                    getQuantityAs_reference = Integer.parseInt(txt_quant.getText());
                                    getBonusQuantityAs_reference = Integer.parseInt(txt_bonus.getText());
                                    txt_batchNO.requestFocus();
                                    MysqlCon ObjectmysqlCON = new MysqlCon();
                                    Statement getstmnt_Obj = ObjectmysqlCON.stmt;
                                    Connection getconection_Obj = ObjectmysqlCON.con;
                                    SaleReturn getsalereturn_OBJ = new SaleReturn();
                                    getbatch_rates = getsalereturnOBJ.get_prodDetailswithIDs_Batch(getstmnt_Obj, getconection_Obj, intprod_id, txt_batchNO.getText());

                                    String[] batchRates_Array = getbatch_rates[0].split("--");
                                    txt_prodName.setText(batchRates_Array[0]);
                                    lbl_retailPrice.setText(batchRates_Array[1]);
                                    lbl_purchPrice.setText(batchRates_Array[2]);
                                    lbl_tradePrice.setText(batchRates_Array[3]);
                                    txt_compID.setText(batchRates_Array[4]);
                                }
                            }
                        }


                    }
                }
            }
            else
            {
                a.setAlertType(Alert.AlertType.ERROR);
                a.setHeaderText("Error!");
                a.setContentText("PLease enter Product ID.");
                a.show();
                txt_prodID.requestFocus();
            }
        }
    }
    /////////////////////////////////////////////////////////////////////////// END of Enter Press on Product ID
    /////////////////////////////////////////////////////////////////////////// START of ENter Press on Product Name
    String proddetails_onprodNameEnter[] = new String[1];
    public void enterpress_prodName(KeyEvent event) throws IOException {
        if(event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB) {

            if(!txt_prodName.getText().equals("")){
                if(txt_sale_invID.getText().isEmpty()) {
                    String product_name = txt_prodName.getText();
                    MysqlCon objMysqlCon = new MysqlCon();
                    Statement objStmt = objMysqlCon.stmt;
                    Connection objCon = objMysqlCon.con;
                    SaleInvoice objsaleinvoice = new SaleInvoice();
                    proddetails_onprodNameEnter = objsaleinvoice.get_prodDetailswithname(objStmt, objCon, product_name);
                    if (proddetails_onprodNameEnter[0] == null) {
                        a.setAlertType(Alert.AlertType.ERROR);
                        a.setHeaderText("Error!");
                        a.setContentText("Wrong Product Name.");
                        a.show();
                        txt_prodName.requestFocus();
                    } else {
                        String[] myArray = proddetails_onprodNameEnter[0].split("--");
                        txt_compID.setText(myArray[1]);
                        txt_prodID.setText(myArray[0]);
                        lbl_retailPrice.setText(myArray[2]);
                        lbl_purchPrice.setText(myArray[3]);
                        lbl_tradePrice.setText(myArray[4]);

                        txt_discount.requestFocus();
                    }
                    int intval_prodIDS = Integer.parseInt(txt_prodID.getText());

                    MysqlCon objctsMysqlCon = new MysqlCon();
                    Statement objctsStmnt = objctsMysqlCon.stmt;
                    Connection objctsCons = objctsMysqlCon.con;
                    SaleReturn objects_salereturn = new SaleReturn();
                    //
                    prod_batchlist = objects_salereturn.getprod_batchs(objctsStmnt, objctsCons, intval_prodIDS);
                    if (prod_batchlist.isEmpty()) {
                    } else {
                        String[] Batch_possiblee = new String[prod_batchlist.size()];
                        ObservableList<String> productBatches = FXCollections.observableArrayList();
                        for(int i =0;i<prod_batchlist.size();i++)
                        {
                            Batch_possiblee[i] = prod_batchlist.get(i);
                            productBatches.add(prod_batchlist.get(i));
                        }

                        TextFields.bindAutoCompletion(txt_batchNO,Batch_possiblee);
                        cmbobox_batch.setItems(productBatches);
                    }
                }
                else
                {
                    String product_name = txt_prodName.getText();
                    MysqlCon objMysqlCon = new MysqlCon();
                    Statement objStmt = objMysqlCon.stmt;
                    Connection objCon = objMysqlCon.con;
                    SaleInvoice objsaleinvoice = new SaleInvoice();
                    proddetails_onprodNameEnter = objsaleinvoice.get_prodDetailswithname(objStmt, objCon, product_name);
                    if (proddetails_onprodNameEnter[0] == null) {
                        a.setAlertType(Alert.AlertType.ERROR);
                        a.setHeaderText("Error!");
                        a.setContentText("Wrong Product Name.");
                        a.show();
                        txt_prodName.requestFocus();
                    } else {
                        String[] myArray = proddetails_onprodNameEnter[0].split("--");
                        txt_compID.setText(myArray[1]);
                        txt_prodID.setText(myArray[0]);
                        lbl_retailPrice.setText(myArray[2]);
                        lbl_purchPrice.setText(myArray[3]);
                        lbl_tradePrice.setText(myArray[4]);

                        txt_discount.requestFocus();


                    String saleinvoice_ids = txt_sale_invID.getText();
                    int intsaleInvoice_id = Integer.parseInt(saleinvoice_ids);

                        String prd_ids = txt_prodID.getText();
                        int intprd_id = Integer.parseInt(prd_ids);

                        MysqlCon mysqlobj = new MysqlCon();
                        Statement statement_obj = mysqlobj.stmt;
                        Connection connection_obj = mysqlobj.con;
                        SaleReturn saleReturn_Obj = new SaleReturn();

                        prod_batchlist = saleReturn_Obj.getprod_batchs(statement_obj, connection_obj, intprd_id);
                        if (prod_batchlist.isEmpty()) {
                        } else {
                            ObservableList<String> productBatches = FXCollections.observableArrayList();
                            for (int i = 0; i < prod_batchlist.size(); i++) {
                                productBatches.add(prod_batchlist.get(i));
                            }
                            cmbobox_batch.setItems(productBatches);
                        }

                    MysqlCon objcMysqlCons = new MysqlCon();
                    Statement objcStmt = objcMysqlCons.stmt;
                    Connection objcCon = objcMysqlCons.con;
                    SaleReturn objcsalereturn = new SaleReturn();

                    cnfrmprodID_inInvoice = objcsalereturn.cnfrmprodID_inSaleInvoice(objcStmt, objcCon,intsaleInvoice_id, intprd_id);
                    if (cnfrmprodID_inInvoice[0] == null)
                    {
                        txt_batchNO.requestFocus();

                    }
                    else
                    {

                        MysqlCon Object_mysqlCON = new MysqlCon();
                        Statement getstmntObj = Object_mysqlCON.stmt;
                        Connection getconectionObj = Object_mysqlCON.con;
                        SaleReturn getsalereturnOBJ = new SaleReturn();
                        getbatchfromOrderTable = getsalereturnOBJ.getexistingBatchandDetails(getstmntObj, getconectionObj, intprd_id, intsaleInvoice_id);
                        if (getbatchfromOrderTable[0] == null) {

                        } else {

                            String[] prodbatchANDQauntity = getbatchfromOrderTable[0].split("--");
                            if (prodbatchANDQauntity[0].equals("1") ) {
                                a.setAlertType(Alert.AlertType.ERROR);
                                a.setHeaderText("Error");
                                a.setContentText("Already Returned");
                                a.show();
                            } else {
                                cmbobox_batch.getSelectionModel().select(prodbatchANDQauntity[3]);
                                txt_batchNO.setText(prodbatchANDQauntity[3]);
                                txt_quant.setText(prodbatchANDQauntity[4]);
                                txt_discount.setText(prodbatchANDQauntity[1]);
                                txt_bonus.setText(prodbatchANDQauntity[2]);
                                getQuantityAs_reference = Integer.parseInt(txt_quant.getText());
                                getBonusQuantityAs_reference = Integer.parseInt(txt_bonus.getText());
                                txt_batchNO.requestFocus();
                            }
                        }
                    }
                    }
                }
            }

            else
            {
                a.setAlertType(Alert.AlertType.ERROR);
                a.setHeaderText("Error!");
                a.setContentText("Product Name not entered.");
                a.show();
                txt_prodName.requestFocus();
            }
        }
    }
    /////////////////////////////////////////////////////////////////////////// END of Enter Press on Product Name

    /////////////////////////////////////////////////////////////////////////// START of ENter Press on Quantity
    public void enterpress_quant(KeyEvent event) throws IOException {
        if(event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB)
        {
        if (txt_discount.getText() == "0") {
            if (txt_quant.getText() != "") {
                float getretail_floatval = Float.parseFloat(lbl_tradePrice.getText());
                int intcnvrt_quant = Integer.parseInt(txt_quant.getText());

                float totalquantity_price = intcnvrt_quant * getretail_floatval;
                lbl_totalAmount.setText(String.valueOf(totalquantity_price));
                btn_add.requestFocus();
            }
        } else {
            if (!txt_quant.getText().isEmpty() && txt_discount.getText() != "0") {
                float intgetretail_price = Float.parseFloat(lbl_tradePrice.getText());
                int intcnvrt_quant = Integer.parseInt(txt_quant.getText());
                float intdiscount_value = Float.parseFloat(txt_discount.getText());
                float totalquantity_price = intcnvrt_quant * intgetretail_price;
                float disc_firstVal = (totalquantity_price * intdiscount_value) / 100;
                lbl_discAmount.setText(String.valueOf(disc_firstVal));
                float final_discVal = totalquantity_price - disc_firstVal;
                lbl_totalAmount.setText(String.valueOf(final_discVal));
                btn_add.requestFocus();
            } else {
                a.setAlertType(Alert.AlertType.ERROR);
                a.setHeaderText("Input Field Empty");
                a.setContentText("Please Enter Quantity");
                a.show();
                txt_quant.requestFocus();
            }
        }
    }
    }
    /////////////////////////////////////////////////////////////////////////// END of ENter Press on Quantity


    public  void  enterpress_txtdiscount() throws IOException{
        txt_bonus.requestFocus();
    }
    public  void  bonustxt_enter() throws  IOException {
        txt_quant.requestFocus();
    }


    public void viewHome() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/home.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }
    @FXML
    public void viewAreas() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_area.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }
    @FXML
    public void viewSubAreas() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_subarea.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }
    @FXML
    public void viewDealers() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_dealers.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }
    @FXML
    public void viewProducts() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_products.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }
    @FXML
    public void viewSalesman() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_salesman.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }
    @FXML
    public void viewSuppliers() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_suppliers.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewCompanies() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_company.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }
}
