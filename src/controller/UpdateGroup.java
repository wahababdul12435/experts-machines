package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import model.*;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class UpdateGroup implements Initializable {

    @FXML
    private Label label_update;

    @FXML
    private JFXTextField txt_group_id;

    @FXML
    private JFXComboBox<String> txt_company_name;

    @FXML
    private JFXTextField txt_group_name;

    @FXML
    private JFXComboBox<String> txt_group_status;

    @FXML
    private JFXButton dialog_group_close;

    @FXML
    private JFXButton dialog_group_update;

    public static String groupTableId ="";
    public static String groupId ="";
    public static String companyId ="";
    public static String companyName ="";
    public static String groupName ="";
    public static String groupStatus ="";

    private String selectedCompanyId;
    private ArrayList<ArrayList<String>> companiesData = new ArrayList<>();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ViewGroupsInfo.lblUpdate = label_update;
        UpdateGroupInfo objUpdateGroupInfo = new UpdateGroupInfo();
        companiesData = objUpdateGroupInfo.getCompaniesData();
        ArrayList<String> chosenCompanytNames = getArrayColumn(companiesData, 1);
        txt_company_name.getItems().addAll(chosenCompanytNames);
        txt_group_id.setText(groupId);
        txt_group_name.setText(groupName);
        txt_company_name.setValue(companyName);
        txt_group_status.setValue(groupStatus);
        selectedCompanyId = companyId;
        dialog_group_close.setOnAction((action)->closeDialog());
        dialog_group_update.setOnAction((action)->updateGroup());
    }

    public void closeDialog()
    {
        ViewGroupsInfo.dialog.close();
        ViewGroupsInfo.stackPane.setVisible(false);
    }

    public void updateGroup() {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        GroupInfo objGroupInfo = new GroupInfo();
        groupId = txt_group_id.getText();
        groupName = txt_group_name.getText();
        companyName = txt_company_name.getValue();
        groupStatus = txt_group_status.getValue();
        objGroupInfo.updateGroup(objStmt, objCon, groupTableId, groupId, groupName, selectedCompanyId, groupStatus);

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/view_groups.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void companyChange(ActionEvent event) {
        int index = txt_company_name.getSelectionModel().getSelectedIndex();
        selectedCompanyId = companiesData.get(index).get(0);
    }

    public ArrayList<String> getArrayColumn(ArrayList<ArrayList<String>> companiesData, int columnIndex)
    {
        ArrayList<String> columnList = new ArrayList<>();
        for(ArrayList<String> row: companiesData) {
            columnList.add(row.get(columnIndex));
        }
        return columnList;
    }

}
