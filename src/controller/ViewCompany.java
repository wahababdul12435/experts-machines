package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.transitions.hamburger.HamburgerBackArrowBasicTransition;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.util.Duration;
import model.*;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class ViewCompany implements Initializable {

    @FXML    private MenuBar menu_bar;
    @FXML    private Menu pending_orders;
    @FXML    private TableView<ViewCompanyInfo> table_viewcompany;
    @FXML    private TableColumn<ViewCompanyInfo, String> sr_no;
    @FXML    private TableColumn<ViewCompanyInfo, String> company_id;
    @FXML    private TableColumn<ViewCompanyInfo, String> company_name;
    @FXML    private TableColumn<ViewCompanyInfo, String> company_address;
    @FXML    private TableColumn<ViewCompanyInfo, String> company_contact;
    @FXML    private TableColumn<ViewCompanyInfo, String> company_email;
    @FXML    private TableColumn<ViewCompanyInfo, String> company_status;
    @FXML    private TableColumn<ViewCompanyInfo, String> operations;
    @FXML    private StackPane stackPane;
    @FXML    private BorderPane menu_bar1;
    @FXML    private AnchorPane inner_anchor;
    @FXML    private BorderPane nav_setup;
    @FXML    private Pane btnpane;
    @FXML    private Button btn11;
    @FXML    private FontAwesomeIconView FA_Icon;
    @FXML    private JFXDrawer drawer;
    @FXML    private AnchorPane filter_pane;
    @FXML    private HBox filter_hbox;
    @FXML    private JFXTextField txt_company_id;
    @FXML    private JFXTextField txt_company_name;
    @FXML    private JFXTextField txt_company_address;
    @FXML    private JFXTextField txt_company_city;
    @FXML   private JFXTextField txt_company_contact;
    @FXML    private JFXTextField txt_company_email;
    @FXML    private JFXComboBox<String> txt_company_status;
    @FXML    private HBox summary_hbox;
    @FXML    private Label lbl_total;
    @FXML    private Label lbl_active;
    @FXML    private Label lbl_inactive;


    PendingOrders objPendingOrders;
    MysqlCon objMysqlCon;
    Statement objStmt;
    Connection objCon;
    int unSeenOrders;
    private int summaryTotal;
    private int summaryActive;
    private int summaryInActive;
    public ObservableList<ViewCompanyInfo> companyDetails;
    private ArrayList<ArrayList<String>> companyData;

    public static String txtCompanyId;
    public static String txtCompanyName;
    public static String txtCompanyAddress;
    public static String txtCompanyCity;
    public static String txtCompanyContact;
    public static String txtCompanyEmail;
    public static String txtCompanyStatus;
    public static boolean filter;

    public static Button btnView;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnSetup.setStyle("-fx-background-color: #47ab1e");
        unSeenOrders = 0;
        objMysqlCon = new MysqlCon();
        objStmt = objMysqlCon.stmt;
        objCon = objMysqlCon.con;
        objPendingOrders = new PendingOrders();
        unSeenOrders = objPendingOrders.getUnSeenOrders(objStmt, objCon);
        ViewCompanyInfo.stackPane = stackPane;

        btnpane.setOnMouseEntered(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(22);
                btnpane.setMinWidth(20.0);
                btn11.setMinWidth(20.0);

            }
        });
        btnpane.setOnMouseExited(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(14);
                btnpane.setMinWidth(10.0);
                btn11.setMinWidth(10.0);
            }
        });

        inner_anchor.setLeftAnchor(inner_anchor, 280d);
        drawer.setSidePane(nav_setup);
        drawer.open();

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        company_id.setCellValueFactory(new PropertyValueFactory<>("companyId"));
        company_name.setCellValueFactory(new PropertyValueFactory<>("companyName"));
        company_address.setCellValueFactory(new PropertyValueFactory<>("companyAddress"));
        company_contact.setCellValueFactory(new PropertyValueFactory<>("companyContact"));
        company_email.setCellValueFactory(new PropertyValueFactory<>("companyEmail"));
        company_status.setCellValueFactory(new PropertyValueFactory<>("companyStatus"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_viewcompany.setItems(parseUserList());
        lbl_total.setText("Companies\n"+summaryTotal);
        lbl_active.setText("Active\n"+summaryActive);
        lbl_inactive.setText("In Active\n"+summaryInActive);
    }

    private ObservableList<ViewCompanyInfo> parseUserList(){
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        ViewCompanyInfo objViewCompanyInfo = new ViewCompanyInfo();
        companyDetails = FXCollections.observableArrayList();
        if(filter)
        {
            companyData  = objViewCompanyInfo.getCompaniesSearch(objStmt, objCon, txtCompanyId, txtCompanyName, txtCompanyAddress, txtCompanyCity, txtCompanyContact, txtCompanyEmail, txtCompanyStatus);
            txt_company_id.setText(txtCompanyId);
            txt_company_name.setText(txtCompanyName);
            txt_company_address.setText(txtCompanyAddress);
            txt_company_city.setText(txtCompanyCity);
            txt_company_contact.setText(txtCompanyContact);
            txt_company_email.setText(txtCompanyEmail);
            txt_company_status.setValue(txtCompanyStatus);
            filter = false;
            filter_pane.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
        }
        else
        {
            companyData = objViewCompanyInfo.getCompaniesInfo(objStmt, objCon);
        }

        for (int i = 0; i < companyData.size(); i++)
        {
            summaryTotal++;
            if(companyData.get(i).get(6).equals("Active"))
            {
                summaryActive++;
            }
            else if(companyData.get(i).get(6).equals("In Active"))
            {
                summaryInActive++;
            }
            companyDetails.add(new ViewCompanyInfo(String.valueOf(i+1), companyData.get(i).get(0), ((companyData.get(i).get(1) == null) ? "N/A" : companyData.get(i).get(1)), companyData.get(i).get(2), ((companyData.get(i).get(3) == null) ? "N/A" : companyData.get(i).get(3)), ((companyData.get(i).get(4) == null) ? "N/A" : companyData.get(i).get(4)), ((companyData.get(i).get(5) == null) ? "N/A" : companyData.get(i).get(5)), companyData.get(i).get(6)));
        }
        return companyDetails;
    }

    @FXML
    void btnSearch(ActionEvent event) {
        txtCompanyId = txt_company_id.getText();
        txtCompanyName = txt_company_name.getText();
        txtCompanyAddress = txt_company_address.getText();
        txtCompanyCity = txt_company_city.getText();
        txtCompanyContact = txt_company_contact.getText();
        txtCompanyEmail = txt_company_email.getText();
        txtCompanyStatus = txt_company_status.getValue();
        if(txtCompanyStatus == null)
        {
            txtCompanyStatus = "All";
        }
        filter = true;

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/view_company.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void btnReset(ActionEvent event) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/view_company.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void exportExcel(ActionEvent event) throws IOException {
        Workbook workbook = new HSSFWorkbook();
        Sheet spreadsheet = workbook.createSheet("data");
        Row row = spreadsheet.createRow(0);

        for (int j = 0; j < table_viewcompany.getColumns().size()-1; j++) {
            row.createCell(j).setCellValue(table_viewcompany.getColumns().get(j).getText());
        }

        for (int i = 0; i < table_viewcompany.getItems().size(); i++) {
            row = spreadsheet.createRow(i + 1);
            for (int j = 0; j < table_viewcompany.getColumns().size()-1; j++) {
                if(table_viewcompany.getColumns().get(j).getCellData(i) != null) {
                    row.createCell(j).setCellValue(table_viewcompany.getColumns().get(j).getCellData(i).toString());
                }
                else {
                    row.createCell(j).setCellValue("");
                }
            }
        }

        FileOutputStream fileOut = new FileOutputStream("data/View_Company.xls");
        workbook.write(fileOut);
        fileOut.close();
    }

    public void follow() throws IOException
    {
        if (drawer.isOpened()) {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(-250);
            transition.setByY(0);
            transition.setCycleCount(1);
            transition.play();
            drawer.close();
            FA_Icon.setGlyphName("ANGLE_DOUBLE_RIGHT");
            FA_Icon.setGlyphSize(14);
            //inner_anchor.setRightAnchor(inner_anchor,1d);
            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(-90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(1.1000);
            scale.setToY(1);

            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();

        }
        else {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(250);
            transition.setByY(0);
            transition.setCycleCount(1);
            FA_Icon.setGlyphName("ANGLE_DOUBLE_LEFT");
            FA_Icon.setGlyphSize(14);
            drawer.open();

            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(0.99947777);
            scale.setToY(1);

            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();
        }
    }

    public void addCompany() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/enter_company.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
}
