package controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import model.CompanyReportInfo;
import model.GlobalVariables;
import model.MysqlCon;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class CompanyReport implements Initializable {
    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private TableView<CompanyReportInfo> table_companyreport;

    @FXML
    private TableColumn<CompanyReportInfo, String> sr_no;

    @FXML
    private TableColumn<CompanyReportInfo, String> company_id;

    @FXML
    private TableColumn<CompanyReportInfo, String> company_name;

    @FXML
    private TableColumn<CompanyReportInfo, String> orders;

    @FXML
    private TableColumn<CompanyReportInfo, String> ordered_items;

    @FXML
    private TableColumn<CompanyReportInfo, String> received_items;

    @FXML
    private TableColumn<CompanyReportInfo, String> returned_items;

    @FXML
    private TableColumn<CompanyReportInfo, String> total_purchase;

    @FXML
    private TableColumn<CompanyReportInfo, String> cash_sent;

    @FXML
    private TableColumn<CompanyReportInfo, String> cash_return;

    @FXML
    private TableColumn<CompanyReportInfo, String> cash_discount;

    @FXML
    private TableColumn<CompanyReportInfo, String> cash_waive_off;

    @FXML
    private TableColumn<CompanyReportInfo, String> cash_pending;

    @FXML
    private TableColumn<CompanyReportInfo, String> operations;

    @FXML
    private AnchorPane filter_pane;

    @FXML
    private HBox filter_hbox;

    @FXML
    private JFXTextField txt_company_id;

    @FXML
    private JFXTextField txt_company_name;

    @FXML
    private JFXTextField txt_area_name;

    @FXML
    private JFXComboBox<String> txt_dealer_type;

    @FXML
    private HBox summary_hbox1;

    @FXML
    private Label lbl_total_purchase;

    @FXML
    private Label lbl_cash_sent;

    @FXML
    private Label lbl_cash_return;

    @FXML
    private HBox summary_hbox2;

    @FXML
    private Label lbl_cash_discount;

    @FXML
    private Label lbl_cash_waiveoff;

    @FXML
    private Label lbl_cash_pending;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    @FXML
    private BorderPane brdrpane_navfinances;

    @FXML
    private BorderPane menu_bar;

    MysqlCon objMysqlCon;
    Statement objStmt;
    Connection objCon;
    static float totalPurchase = 0;
    static float cashSent = 0;
    static float cashReturn = 0;
    static float cashDiscount = 0;
    static float cashWaiveOff = 0;
    static float cashPending = 0;

    public static String txtDealerId = "";
    public static String txtDealerName = "";
    public static String txtAreaName = "";
    public static String txtDealerType = "";
    public static boolean filter;
    public static String summary = "";

    public static MenuButton btnView;

    public ObservableList<CompanyReportInfo> companyReportDetail;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnReport.setStyle("-fx-background-color: #47ab1e");
        objMysqlCon = new MysqlCon();
        objStmt = objMysqlCon.stmt;
        objCon = objMysqlCon.con;

        inner_anchor.setLeftAnchor(inner_anchor, 280d);
        drawer.setSidePane(brdrpane_navfinances);
        drawer.open();

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        company_id.setCellValueFactory(new PropertyValueFactory<>("companyId"));
        company_name.setCellValueFactory(new PropertyValueFactory<>("companyName"));
        orders.setCellValueFactory(new PropertyValueFactory<>("orders"));
        ordered_items.setCellValueFactory(new PropertyValueFactory<>("orderedItems"));
        received_items.setCellValueFactory(new PropertyValueFactory<>("receivedItems"));
        returned_items.setCellValueFactory(new PropertyValueFactory<>("returnedItems"));
        total_purchase.setCellValueFactory(new PropertyValueFactory<>("totalPurchase"));
        cash_sent.setCellValueFactory(new PropertyValueFactory<>("cashSent"));
        cash_return.setCellValueFactory(new PropertyValueFactory<>("cashReturn"));
        cash_discount.setCellValueFactory(new PropertyValueFactory<>("cashDiscount"));
        cash_waive_off.setCellValueFactory(new PropertyValueFactory<>("cashWaiveOff"));
        cash_pending.setCellValueFactory(new PropertyValueFactory<>("cashPending"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_companyreport.setItems(parseUserList());
        lbl_total_purchase.setText("Purchase\n"+String.format("%,.0f", totalPurchase));
        lbl_cash_sent.setText("Sent\n"+String.format("%,.0f", cashSent));
        lbl_cash_return.setText("Return\n"+String.format("%,.0f", cashReturn));
        lbl_cash_discount.setText("Discount\n"+String.format("%,.0f", cashDiscount));
        lbl_cash_waiveoff.setText("Waive Off\n"+String.format("%,.0f", cashWaiveOff));
        lbl_cash_pending.setText("Pending\n"+String.format("%,.0f", cashPending));
        summary = "";
    }

    private ObservableList<CompanyReportInfo> parseUserList() {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        CompanyReportInfo objCompanyReportInfo = new CompanyReportInfo();
        companyReportDetail = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> dealerReportData;
        if(filter)
        {
            dealerReportData  = objCompanyReportInfo.getCompanyReportSearch(objStmt, objCon, txtDealerId, txtDealerName, txtAreaName, txtDealerType);
            txt_company_id.setText(txtDealerId);
            txt_company_name.setText(txtDealerName);
            txt_area_name.setText(txtAreaName);
            txt_dealer_type.setValue(txtDealerType);
            filter = false;
            filter_pane.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
            totalPurchase = 0;
            cashSent = 0;
            cashReturn = 0;
            cashDiscount = 0;
            cashWaiveOff = 0;
            cashPending = 0;
        }
        else if(summary.equals("Sale"))
        {
            dealerReportData = objCompanyReportInfo.getCompanyReportSummary(objStmt, objCon, summary);
            lbl_total_purchase.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
        }
        else if(summary.equals("Collection"))
        {
            dealerReportData = objCompanyReportInfo.getCompanyReportSummary(objStmt, objCon, summary);
            lbl_cash_sent.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
        }
        else if(summary.equals("Return"))
        {
            dealerReportData = objCompanyReportInfo.getCompanyReportSummary(objStmt, objCon, summary);
            lbl_cash_return.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
        }
        else if(summary.equals("Discount"))
        {
            dealerReportData = objCompanyReportInfo.getCompanyReportSummary(objStmt, objCon, summary);
            lbl_cash_discount.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
        }
        else if(summary.equals("Pending"))
        {
            dealerReportData = objCompanyReportInfo.getCompanyReportSummary(objStmt, objCon, summary);
            lbl_cash_pending.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
        }
        else if(summary.equals("Waive Off"))
        {
            dealerReportData = objCompanyReportInfo.getCompanyReportSummary(objStmt, objCon, summary);
            lbl_cash_waiveoff.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
        }
        else
        {
            dealerReportData = objCompanyReportInfo.getCompanyReportInfo(objStmt, objCon);
            totalPurchase = 0;
            cashSent = 0;
            cashReturn = 0;
            cashDiscount = 0;
            cashWaiveOff = 0;
            cashPending = 0;
        }
        for (int i = 0; i < dealerReportData.size(); i++)
        {
            if(summary.equals(""))
            {
                totalPurchase += (dealerReportData.get(i).get(7) != null) ? Float.parseFloat(dealerReportData.get(i).get(7)) : 0;
                cashSent += (dealerReportData.get(i).get(8) != null) ? Float.parseFloat(dealerReportData.get(i).get(8)) : 0;
                cashReturn += (dealerReportData.get(i).get(9) != null) ? Float.parseFloat(dealerReportData.get(i).get(9)) : 0;
                cashDiscount += (dealerReportData.get(i).get(10) != null) ? Float.parseFloat(dealerReportData.get(i).get(10)) : 0;
                cashWaiveOff += (dealerReportData.get(i).get(11) != null) ? Float.parseFloat(dealerReportData.get(i).get(11)) : 0;
                cashPending += (dealerReportData.get(i).get(12) != null) ? Float.parseFloat(dealerReportData.get(i).get(12)) : 0;
            }
            companyReportDetail.add(new CompanyReportInfo(String.valueOf(i+1), dealerReportData.get(i).get(0), ((dealerReportData.get(i).get(1) == null) ? "N/A" : dealerReportData.get(i).get(1)), dealerReportData.get(i).get(2), ((dealerReportData.get(i).get(3) == null) ? "N/A" : dealerReportData.get(i).get(3)), ((dealerReportData.get(i).get(4) == null) ? "0" : dealerReportData.get(i).get(4)), ((dealerReportData.get(i).get(5) == null) ? "0" : dealerReportData.get(i).get(5)), ((dealerReportData.get(i).get(6) == null) ? "0" : dealerReportData.get(i).get(6)), ((dealerReportData.get(i).get(7) == null) ? "0" : dealerReportData.get(i).get(7)), ((dealerReportData.get(i).get(8) == null) ? "0" : dealerReportData.get(i).get(8)), ((dealerReportData.get(i).get(9) == null) ? "0" : dealerReportData.get(i).get(9)), ((dealerReportData.get(i).get(10) == null) ? "0" : dealerReportData.get(i).get(10)), ((dealerReportData.get(i).get(11) == null) ? "0" : dealerReportData.get(i).get(11)), ((dealerReportData.get(i).get(12) == null) ? "0" : dealerReportData.get(i).get(12))));
        }
        return companyReportDetail;
    }

    @FXML
    void follow(MouseEvent event) {

    }

    @FXML
    void btnReset(ActionEvent event) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/dealer_finance.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void btnSearch(ActionEvent event) {
        txtDealerId = txt_company_id.getText();
        txtDealerName = txt_company_name.getText();
        filter = true;

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/dealer_finance.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void addCash(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/add_dealer_finance.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void showCollection(MouseEvent event) {
        summary = "Collection";

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/dealer_finance.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void showDiscount(MouseEvent event) {
        summary = "Discount";

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/dealer_finance.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void showPending(MouseEvent event) {
        summary = "Pending";

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/dealer_finance.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void showReturn(MouseEvent event) {
        summary = "Return";

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/dealer_finance.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void showSale(MouseEvent event) {
        summary = "Sale";

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/dealer_finance.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void showWaiveOff(MouseEvent event) {
        summary = "Waive Off";

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/dealer_finance.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
}
