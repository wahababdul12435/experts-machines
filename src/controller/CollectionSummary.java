package controller;

import com.jfoenix.controls.*;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import com.jfoenix.transitions.hamburger.HamburgerBackArrowBasicTransition;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.Duration;
import model.*;
import model.CollectionSummaryInfo;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;
import org.controlsfx.control.CheckTreeView;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

public class CollectionSummary implements Initializable {
    @FXML    private Button print_collectionsummary;
    @FXML    private TableView<CollectionSummaryInfo> table_collectionsummary;
    @FXML    private TableColumn<CollectionSummaryInfo, String> sr_no;
    @FXML    private TableColumn<CollectionSummaryInfo, String> dealer_id;
    @FXML    private TableColumn<CollectionSummaryInfo, String> order_id;
    @FXML    private TableColumn<CollectionSummaryInfo, String> dealer_name;
    @FXML    private TableColumn<CollectionSummaryInfo, String> dealer_address;
    @FXML    private TableColumn<CollectionSummaryInfo, String> dealer_contact;
    @FXML    private TableColumn<CollectionSummaryInfo, String> old_payments;
    @FXML    private TableColumn<CollectionSummaryInfo, String> new_payments;
    @FXML    private TableColumn<CollectionSummaryInfo, String> total_payments;
    @FXML    private TableColumn<CollectionSummaryInfo, String> operations;
    @FXML    private Button filter_collectionsummary;
    @FXML    private Button reset_collectionsummary;
    @FXML   private DatePicker datePick_summaryFrom;
    @FXML   private DatePicker datePick_summaryTo;
    @FXML   private TextField txt_InvoNum_To;
    @FXML   private TextField txt_InvoNum_From;
    @FXML    private BorderPane menu_bar;
    @FXML    private JFXDrawer drawer;
    @FXML    private Button btn11;
    @FXML    private FontAwesomeIconView FA_Icon;
    @FXML    private AnchorPane inner_anchor;
    @FXML    private Pane btnpane;
    @FXML    private BorderPane nav_sales;
    @FXML    private CheckTreeView<String> treeview;
    @FXML    private JFXComboBox<String> combo_invoice_status;
    @FXML    private StackPane stackPane;

    public ObservableList<CollectionSummaryInfo> collectionSummary;

    public ArrayList<CollectionSummaryInfo> summaryData;
    ArrayList<String> Areaslist = new ArrayList<>();
    ArrayList<String> SubAreaslist = new ArrayList<>();
    ArrayList<String> getcheckedareas = new ArrayList<>();
    int k =0;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        CollectionSummaryInfo.stackPane = stackPane;
        combo_invoice_status.setValue("Pending");

        summaryData = new ArrayList<>();
        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        dealer_id.setCellValueFactory(new PropertyValueFactory<>("dealerId"));
        order_id.setCellValueFactory(new PropertyValueFactory<>("orderId"));
        dealer_name.setCellValueFactory(new PropertyValueFactory<>("dealerName"));
        dealer_address.setCellValueFactory(new PropertyValueFactory<>("dealerAddress"));
        dealer_contact.setCellValueFactory(new PropertyValueFactory<>("dealerContact"));
        old_payments.setCellValueFactory(new PropertyValueFactory<>("oldPayment"));
        new_payments.setCellValueFactory(new PropertyValueFactory<>("newPayment"));
        total_payments.setCellValueFactory(new PropertyValueFactory<>("totalPayment"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_collectionsummary.setItems(parseUserList());

        btnpane.setOnMouseEntered(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(22);
                btnpane.setMinWidth(20.0);
                btn11.setMinWidth(20.0);

            }
        });
        btnpane.setOnMouseExited(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(14);
                btnpane.setMinWidth(10.0);
                btn11.setMinWidth(10.0);
            }
        });
        inner_anchor.setLeftAnchor(inner_anchor, 260d);
        drawer.setSidePane(nav_sales);
        drawer.open();

        MysqlCon newobjMysqlConnc = new MysqlCon();
        Statement objectStattmt = newobjMysqlConnc.stmt;
        Connection objectConnnec = newobjMysqlConnc.con;
        DrawingSummaryInfo objctdrwngSummary = new DrawingSummaryInfo();

        Areaslist = objctdrwngSummary.getAreas(objectStattmt,objectConnnec);

        CheckBoxTreeItem<String> root = new CheckBoxTreeItem<String>("Select All");
        for(int i=0;i<Areaslist.size();i++)
        {
            String[] splitAreas = Areaslist.get(i).split("--");

            CheckBoxTreeItem<String> node1 = new CheckBoxTreeItem<String>(splitAreas[1]);
            MysqlCon objMysqlConnc = new MysqlCon();
            Statement objctStattmt = objMysqlConnc.stmt;
            Connection objctConnnec = objMysqlConnc.con;
            DrawingSummaryInfo objctdrawngSummary = new DrawingSummaryInfo();

            int intval_Areaid = Integer.parseInt(splitAreas[0]);
            SubAreaslist = objctdrawngSummary.getSubAreas(objctStattmt,objctConnnec,intval_Areaid);

            for(int j=0;j<SubAreaslist.size();j++)
            {
                String[] splitSubAreas = SubAreaslist.get(j).split("--");
                CheckBoxTreeItem<String> node2 = new CheckBoxTreeItem<String>(splitSubAreas[1]);
                node1.setExpanded(false);
                node1.getChildren().add(node2);
            }
            root.getChildren().add(node1);
            treeview.setRoot(root);
            root.setExpanded(true);

            treeview.setShowRoot(true);
        }

       treeview.getCheckModel().getCheckedItems().addListener(new ListChangeListener<TreeItem<String>>() {
            @Override
            public void onChanged(Change<? extends TreeItem<String>> c) {
                String nval = treeview.getCheckModel().getCheckedItems().toString();
                String valueAndIndex = c.toString();
                String[] splitedvalueAndIndex = valueAndIndex.split(" ]] ");
                String[] splitedValue = splitedvalueAndIndex[0].split(": ");
                String[] getfunctionAndIndex = splitedvalueAndIndex[1].split(" at ");
                String[] getindx = getfunctionAndIndex[1].split(" ");
                String strngindex = getindx[0];
                int indexvalue= Integer.parseInt(strngindex);
                if(getfunctionAndIndex[0].equals("added"))
                {
                    getcheckedareas.add(splitedValue[1]);
                    k++;
                }
                if(getfunctionAndIndex[0].equals("removed"))
                {
                    getcheckedareas.remove(indexvalue);
                    k--;
                }
            }
        });
    }

    public void follow() throws IOException
    {
    /*PathTransition transition;
    transition = new PathTransition();
    Polyline mpoly = new Polyline();
    mpoly.getPoints().addAll(new Double[]{
            0.0,0.0,
            0.0,0.0,
            -200.0,0.0});
    transition.setNode(btn11);
    transition.setDuration(Duration.seconds(2));
    transition.setPath(mpoly);
    transition.play();*/
        if (drawer.isOpened()) {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(-250);
            transition.setByY(0);
            transition.setCycleCount(1);
            transition.play();
            drawer.close();
            FA_Icon.setGlyphName("ANGLE_DOUBLE_RIGHT");
            FA_Icon.setGlyphSize(14);
            //inner_anchor.setRightAnchor(inner_anchor,1d);


            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(-90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(1.28000);
            scale.setToY(1);



            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();
            //
            //inner_anchor.setLeftAnchor(inner_anchor, 5d);
        } else {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(250);
            transition.setByY(0);
            transition.setCycleCount(1);
            FA_Icon.setGlyphName("ANGLE_DOUBLE_LEFT");
            FA_Icon.setGlyphSize(14);
            drawer.open();

            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(0.99947777);
            scale.setToY(1);



            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();

        }


    }

    private ObservableList<CollectionSummaryInfo> parseUserList(){
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        CollectionSummaryInfo objCollectionSummaryInfo = new CollectionSummaryInfo();
        collectionSummary = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> collectionSummaryData = objCollectionSummaryInfo.getCollectionSummary(objStmt, objCon);
        for (int i = 0; i < collectionSummaryData.size(); i++)
        {
            collectionSummary.add(new CollectionSummaryInfo(String.valueOf(i+1), collectionSummaryData.get(i).get(0), collectionSummaryData.get(i).get(1), collectionSummaryData.get(i).get(2), collectionSummaryData.get(i).get(3), collectionSummaryData.get(i).get(4), collectionSummaryData.get(i).get(5), collectionSummaryData.get(i).get(6), collectionSummaryData.get(i).get(7)));
            summaryData.add(new CollectionSummaryInfo(String.valueOf(i+1), collectionSummaryData.get(i).get(0), collectionSummaryData.get(i).get(1), collectionSummaryData.get(i).get(2), collectionSummaryData.get(i).get(3), collectionSummaryData.get(i).get(4), collectionSummaryData.get(i).get(5), collectionSummaryData.get(i).get(6), collectionSummaryData.get(i).get(7)));
        }
        return collectionSummary;
    }
    public void ResetSummary() throws IOException, ParseException
    {
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String InvoToDate = "";
        String InvofromDate = "";
        if (datePick_summaryFrom.getValue() != null || datePick_summaryTo.getValue() != null) {
            InvofromDate = datePick_summaryFrom.getValue().toString();

            Date selectedDate = null;
            try {
                selectedDate = sdf.parse(InvofromDate);
                InvofromDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            InvoToDate = datePick_summaryTo.getValue().toString();
            Date selectedToDate = null;
            try {
                selectedToDate = sdf.parse(InvoToDate);
                InvoToDate = sdf2.format(selectedToDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        String invoFromVal = txt_InvoNum_From.getText();
        String invoToVal = txt_InvoNum_To.getText();

        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        CollectionSummaryInfo objCollectionSummaryInfo = new CollectionSummaryInfo();
        collectionSummary = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> collectionSummaryData = objCollectionSummaryInfo.getCollectionSummary_withFilter(objStmt, objCon,combo_invoice_status.getValue().toString(),invoFromVal,invoToVal,getcheckedareas,InvofromDate,InvoToDate);
        for (int i = 0; i < collectionSummaryData.size(); i++)
        {
            collectionSummary.add(new CollectionSummaryInfo(String.valueOf(i+1), collectionSummaryData.get(i).get(0), collectionSummaryData.get(i).get(1), collectionSummaryData.get(i).get(2), collectionSummaryData.get(i).get(3), collectionSummaryData.get(i).get(4), collectionSummaryData.get(i).get(5), collectionSummaryData.get(i).get(6), collectionSummaryData.get(i).get(7)));
            summaryData.add(new CollectionSummaryInfo(String.valueOf(i+1), collectionSummaryData.get(i).get(0), collectionSummaryData.get(i).get(1), collectionSummaryData.get(i).get(2), collectionSummaryData.get(i).get(3), collectionSummaryData.get(i).get(4), collectionSummaryData.get(i).get(5), collectionSummaryData.get(i).get(6), collectionSummaryData.get(i).get(7)));
        }
        table_collectionsummary.setItems(collectionSummary);

        txt_InvoNum_From.setText("");
        txt_InvoNum_To.setText("");
        datePick_summaryTo.setValue(null);
        datePick_summaryFrom.setValue(null);
        treeview.getCheckModel().clearChecks();

    }

    public void filterSummary() throws IOException, ParseException
    {
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String InvoToDate = "";
        String InvofromDate = "";
        if (datePick_summaryFrom.getValue() != null || datePick_summaryTo.getValue() != null) {
            InvofromDate = datePick_summaryFrom.getValue().toString();

            Date selectedDate = null;
            try {
                selectedDate = sdf.parse(InvofromDate);
                InvofromDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            InvoToDate = datePick_summaryTo.getValue().toString();
            Date selectedToDate = null;
            try {
                selectedToDate = sdf.parse(InvoToDate);
                InvoToDate = sdf2.format(selectedToDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        String invoFromVal = txt_InvoNum_From.getText();
        String invoToVal = txt_InvoNum_To.getText();

        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        CollectionSummaryInfo objCollectionSummaryInfo = new CollectionSummaryInfo();
        collectionSummary = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> collectionSummaryData = objCollectionSummaryInfo.getCollectionSummary_withFilter(objStmt, objCon,combo_invoice_status.getValue().toString(),invoFromVal,invoToVal,getcheckedareas,InvofromDate,InvoToDate);
        for (int i = 0; i < collectionSummaryData.size(); i++)
        {
            collectionSummary.add(new CollectionSummaryInfo(String.valueOf(i+1), collectionSummaryData.get(i).get(0), collectionSummaryData.get(i).get(1), collectionSummaryData.get(i).get(2), collectionSummaryData.get(i).get(3), collectionSummaryData.get(i).get(4), collectionSummaryData.get(i).get(5), collectionSummaryData.get(i).get(6), collectionSummaryData.get(i).get(7)));
            summaryData.add(new CollectionSummaryInfo(String.valueOf(i+1), collectionSummaryData.get(i).get(0), collectionSummaryData.get(i).get(1), collectionSummaryData.get(i).get(2), collectionSummaryData.get(i).get(3), collectionSummaryData.get(i).get(4), collectionSummaryData.get(i).get(5), collectionSummaryData.get(i).get(6), collectionSummaryData.get(i).get(7)));
        }
        table_collectionsummary.setItems(collectionSummary);

        txt_InvoNum_From.setText("");
        txt_InvoNum_To.setText("");
        datePick_summaryTo.setValue(null);
        datePick_summaryFrom.setValue(null);
        treeview.getCheckModel().clearChecks();

    }

    public void generateCollectionSummary(ArrayList<CollectionSummaryInfo> summaryList) throws JRException {
        InputStream objIO = CollectionSummary.class.getResourceAsStream("/reports/CollectionSummary.jrxml");
        JasperReport objReport = JasperCompileManager.compileReport(objIO);
        JasperPrint objPrint = JasperFillManager.fillReport(objReport, null, new JRBeanCollectionDataSource(summaryList));
        JasperViewer.viewReport(objPrint, false);
    }

    public void printCollectionSummary() throws JRException {
        generateCollectionSummary(summaryData);
    }

}
