package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.util.Duration;
import model.*;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class ViewProducts implements Initializable {
    @FXML    private TableView<ViewProductsInfo> table_viewproducts;
    @FXML    private TableColumn<ViewProductsInfo, String> sr_no;
    @FXML    private TableColumn<ViewProductsInfo, String> product_id;
    @FXML    private TableColumn<ViewProductsInfo, String> company_name;
    @FXML    private TableColumn<ViewProductsInfo, String> company_group;
    @FXML    private TableColumn<ViewProductsInfo, String> product_name;
    @FXML    private TableColumn<ViewProductsInfo, String> product_type;
    @FXML    private TableColumn<ViewProductsInfo, String> retial_price;
    @FXML    private TableColumn<ViewProductsInfo, String> trade_price;
    @FXML    private TableColumn<ViewProductsInfo, String> purchase_price;
    @FXML    private TableColumn<ViewProductsInfo, String> purchase_discount;
    @FXML    private TableColumn<ViewProductsInfo, String> status;
    @FXML    private TableColumn<ViewProductsInfo, String> operations;

    @FXML    private BorderPane menu_bar1;
    @FXML    private AnchorPane inner_anchor;
    @FXML    private BorderPane nav_setup;
    @FXML    private Pane btnpane;
    @FXML    private Button btn11;
    @FXML    private FontAwesomeIconView FA_Icon;
    @FXML    private JFXDrawer drawer;

    @FXML    private AnchorPane filter_pane;
    @FXML    private HBox filter_hbox;
    @FXML    private JFXTextField txt_product_id;
    @FXML    private JFXTextField txt_product_name;
    @FXML    private JFXComboBox<String> txt_product_type;
    @FXML    private JFXComboBox<String> txt_company_name;
    @FXML    private JFXComboBox<String> txt_group_name;
    @FXML    private JFXComboBox<String> txt_product_status;
    @FXML    private StackPane stackPane;
    @FXML    private HBox summary_hbox;
    @FXML    private Label lbl_total;
    @FXML    private Label lbl_active;
    @FXML    private Label lbl_inactive;

    public ObservableList<ViewProductsInfo> productsDetail;
    PendingOrders objPendingOrders;
    MysqlCon objMysqlCon;
    Statement objStmt;
    Connection objCon;
    private int summaryTotal;
    private int summaryActive;
    private int summaryInActive;

    private String selectedCompanyId;
    private String selectedGroupId;

    private ArrayList<ArrayList<String>> companiesData = new ArrayList<>();
    private ArrayList<ArrayList<String>> groupsData = new ArrayList<>();
    private ArrayList<String> chosenGroupIds = new ArrayList<>();

    public static String txtProductId = "";
    public static String txtProductName = "";
    public static String txtProductType = "All";
    public static String txtCompanyId = "All";
    public static String txtCompanyName = "";
    public static String txtGroupId = "All";
    public static String txtGroupName = "";
    public static String txtProductStatus = "";
    public static boolean filter;

    public static Button btnView;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnSetup.setStyle("-fx-background-color: #47ab1e");
        objMysqlCon = new MysqlCon();
        objStmt = objMysqlCon.stmt;
        objCon = objMysqlCon.con;
        ViewProductsInfo.stackPane = stackPane;

        UpdateProductInfo objUpdateProductInfo = new UpdateProductInfo();
        companiesData = objUpdateProductInfo.getCompaniesData();
        groupsData = objUpdateProductInfo.getCompanyGroupsData();
        ArrayList<String> chosenCompanyNames = getArrayColumn(companiesData, 1);
        txt_company_name.getItems().addAll(chosenCompanyNames);

        btnpane.setOnMouseEntered(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(22);
                btnpane.setMinWidth(20.0);
                btn11.setMinWidth(20.0);

            }
        });
        btnpane.setOnMouseExited(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(14);
                btnpane.setMinWidth(10.0);
                btn11.setMinWidth(10.0);
            }
        });
        inner_anchor.setLeftAnchor(inner_anchor, 270d);
        drawer.setSidePane(nav_setup);
        drawer.open();

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        product_id.setCellValueFactory(new PropertyValueFactory<>("productId"));
        company_name.setCellValueFactory(new PropertyValueFactory<>("companyName"));
        company_group.setCellValueFactory(new PropertyValueFactory<>("companyGroup"));
        product_name.setCellValueFactory(new PropertyValueFactory<>("productName"));
        product_type.setCellValueFactory(new PropertyValueFactory<>("productType"));
        retial_price.setCellValueFactory(new PropertyValueFactory<>("retialPrice"));
        trade_price.setCellValueFactory(new PropertyValueFactory<>("tradePrice"));
        purchase_price.setCellValueFactory(new PropertyValueFactory<>("purchasePrice"));
        purchase_discount.setCellValueFactory(new PropertyValueFactory<>("purchaseDiscount"));
        status.setCellValueFactory(new PropertyValueFactory<>("productStatus"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_viewproducts.setItems(parseUserList());
        lbl_total.setText("Products\n"+summaryTotal);
        lbl_active.setText("Active\n"+summaryActive);
        lbl_inactive.setText("In Active\n"+summaryInActive);
    }

    public void follow() throws IOException
    {
        if (drawer.isOpened()) {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(-250);
            transition.setByY(0);
            transition.setCycleCount(1);
            transition.play();
            drawer.close();
            FA_Icon.setGlyphName("ANGLE_DOUBLE_RIGHT");
            FA_Icon.setGlyphSize(14);
            //inner_anchor.setRightAnchor(inner_anchor,1d);

            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(-90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(1.1000);
            scale.setToY(1);

            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();
            //
            //inner_anchor.setLeftAnchor(inner_anchor, 5d);
        } else {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(250);
            transition.setByY(0);
            transition.setCycleCount(1);
            FA_Icon.setGlyphName("ANGLE_DOUBLE_LEFT");
            FA_Icon.setGlyphSize(14);
            drawer.open();

            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(0.99947777);
            scale.setToY(1);

            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();
        }
    }

    private ObservableList<ViewProductsInfo> parseUserList(){
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        ViewProductsInfo objViewProductsInfo = new ViewProductsInfo();
        productsDetail = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> productsData;
        if(filter)
        {
            productsData  = objViewProductsInfo.getProductsSearch(objStmt, objCon, txtProductId, txtProductName, txtProductType, txtCompanyId, txtGroupId, txtProductStatus);
            txt_product_id.setText(txtProductId);
            txt_product_name.setText(txtProductName);
            txt_product_type.setValue(txtProductType);
            txt_company_name.setValue(txtCompanyName);
            int index = txt_company_name.getSelectionModel().getSelectedIndex();
            if(index >=1)
            {
                selectedCompanyId = companiesData.get(index - 1).get(0);
                txtCompanyId = selectedCompanyId;
                txt_group_name.getItems().clear();
                ArrayList<String> chosenGroupNames = getCompanyGroups(groupsData, selectedCompanyId);
                txt_group_name.getItems().addAll("All");
                txt_group_name.getItems().addAll(chosenGroupNames);
                txt_group_name.setValue(txtGroupName);
            }
            txt_product_status.setValue(txtProductStatus);
            filter = false;
            filter_pane.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
        }
        else
        {
            productsData = objViewProductsInfo.getProductsInfo(objStmt, objCon);
        }
        for (int i = 0; i < productsData.size(); i++)
        {
            summaryTotal++;
            if(productsData.get(i).get(10).equals("Active"))
            {
                summaryActive++;
            }
            else if(productsData.get(i).get(10).equals("In Active"))
            {
                summaryInActive++;
            }
            productsDetail.add(new ViewProductsInfo(String.valueOf(i+1), productsData.get(i).get(0), ((productsData.get(i).get(1) == null) ? "N/A" : productsData.get(i).get(1)), ((productsData.get(i).get(2) == null) ? "N/A" : productsData.get(i).get(2)), ((productsData.get(i).get(3) == null) ? "N/A" : productsData.get(i).get(3)), productsData.get(i).get(4), productsData.get(i).get(5), productsData.get(i).get(6), productsData.get(i).get(7), productsData.get(i).get(8), productsData.get(i).get(9), productsData.get(i).get(10)));
        }
        return productsDetail;
    }

    @FXML
    void btnSearch(ActionEvent event) {
        txtProductId = txt_product_id.getText();
        txtProductName = txt_product_name.getText();
        txtProductType = txt_product_type.getValue();
        if(txtProductType == null)
        {
            txtProductType = "All";
        }
        txtCompanyName = txt_company_name.getValue();
        txtGroupName = txt_group_name.getValue();
        txtProductStatus = txt_product_status.getValue();
        if(txtProductStatus == null)
        {
            txtProductStatus = "All";
        }
        filter = true;

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/view_products.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void btnReset(ActionEvent event) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/view_products.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void exportExcel(ActionEvent event) throws IOException {
        Workbook workbook = new HSSFWorkbook();
        Sheet spreadsheet = workbook.createSheet("data");
        Row row = spreadsheet.createRow(0);

        for (int j = 0; j < table_viewproducts.getColumns().size()-1; j++) {
            row.createCell(j).setCellValue(table_viewproducts.getColumns().get(j).getText());
        }

        for (int i = 0; i < table_viewproducts.getItems().size(); i++) {
            row = spreadsheet.createRow(i + 1);
            for (int j = 0; j < table_viewproducts.getColumns().size()-1; j++) {
                if(table_viewproducts.getColumns().get(j).getCellData(i) != null) {
                    row.createCell(j).setCellValue(table_viewproducts.getColumns().get(j).getCellData(i).toString());
                }
                else {
                    row.createCell(j).setCellValue("");
                }
            }
        }

        FileOutputStream fileOut = new FileOutputStream("data/View_Products.xls");
        workbook.write(fileOut);
        fileOut.close();
    }

    public ArrayList<String> getArrayColumn(ArrayList<ArrayList<String>> companiesData, int columnIndex)
    {
        ArrayList<String> columnList = new ArrayList<>();
        for(ArrayList<String> row: companiesData) {
            columnList.add(row.get(columnIndex));
        }
        return columnList;
    }

    public ArrayList<String> getCompanyGroups(ArrayList<ArrayList<String>> groupsData, String companyId)
    {
        ArrayList<String> columnList = new ArrayList<>();
        chosenGroupIds = new ArrayList<>();
        for(ArrayList<String> row: groupsData) {
            if(row.get(2).equals(companyId))
            {
                chosenGroupIds.add(row.get(0));
                columnList.add(row.get(1));
            }
        }
        return columnList;
    }

    public String getCompanyId(ArrayList<ArrayList<String>> companiesData, String companyName)
    {
        for(ArrayList<String> row: companiesData) {
            if(row.get(1).equals(companyName))
            {
                selectedCompanyId = row.get(0);
                return selectedCompanyId;
            }
        }
        return null;
    }

    @FXML
    void companyChange(ActionEvent event) {
        int index = txt_company_name.getSelectionModel().getSelectedIndex();
        if(index >=1)
        {
            selectedCompanyId = companiesData.get(index-1).get(0);
            txtCompanyId = selectedCompanyId;
            txt_group_name.getItems().clear();
            ArrayList<String> chosenGroupNames = getCompanyGroups(groupsData, selectedCompanyId);
            if(chosenGroupNames.size() > 0)
            {
                txt_group_name.getItems().addAll("All");
                txt_group_name.getItems().addAll(chosenGroupNames);
                txt_group_name.setValue("All");
            }
            txtGroupId = "All";
//            if(chosenGroupNames.size() >= 0)
//            {
//                txt_group_name.setValue(chosenGroupNames.get(0));
//            }
//            selectedGroupId = chosenGroupIds.get(0);
//            txtGroupId = selectedGroupId;
        }
        else
        {
            txtCompanyId = "All";
            txtGroupId = "All";
            txt_group_name.getItems().clear();
            txt_group_name.getItems().addAll("All");
            txt_group_name.setValue("All");
        }
    }

    @FXML
    void groupChange(ActionEvent event) {
        int index = txt_group_name.getSelectionModel().getSelectedIndex();
        if(index >=1)
        {
            selectedGroupId = chosenGroupIds.get(index-1);
            txtGroupId = selectedGroupId;
        }
        else
        {
            txtGroupId = "All";
        }
    }

    @FXML
    void addProduct() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/enter_product.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
}
