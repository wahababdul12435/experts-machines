package controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.util.Duration;
import model.*;
import model.CityInfo;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class EnterCity implements Initializable {

    @FXML
    private BorderPane menu_bar1;

    @FXML
    private BorderPane nav_setup;

    @FXML
    private JFXTextField txt_city_id;

    @FXML
    private JFXTextField txt_city_name;

    @FXML
    private JFXComboBox<String> txt_district_name;

    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private Button cancelBtn;

    @FXML
    private Button saveBtn;

    @FXML
    private Button btn_add_city;

    @FXML    private Button btn_edit_cancel;
    @FXML    private Button btn_edit_update;

    @FXML
    private TableView<CityInfo> table_add_city;

    @FXML
    private TableColumn<CityInfo, String> sr_no;

    @FXML
    private TableColumn<CityInfo, String> city_id;

    @FXML
    private TableColumn<CityInfo, String> city_name;

    @FXML
    private TableColumn<CityInfo, String> district_name;

    @FXML
    private TableColumn<CityInfo, String> operations;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    public static ObservableList<CityInfo> cityDetails;
    Alert alert = new Alert(Alert.AlertType.NONE);
    CityInfo objCityInfo;
    public static int srNo;
    public static Button btnView;

    public void initialize(URL url, ResourceBundle resourceBundle) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnSetup.setStyle("-fx-background-color: #47ab1e");
        objCityInfo = new CityInfo();
        btnpane.setOnMouseEntered(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(22);
                btnpane.setMinWidth(20.0);
                btn11.setMinWidth(20.0);

            }
        });
        btnpane.setOnMouseExited(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(14);
                btnpane.setMinWidth(10.0);
                btn11.setMinWidth(10.0);
            }
        });

        inner_anchor.setLeftAnchor(inner_anchor, 280d);
        drawer.setSidePane(nav_setup);
        drawer.open();

        ArrayList<String> chosenDistrictNames = objCityInfo.getSavedDistrictNames();
        txt_district_name.getItems().addAll(chosenDistrictNames);

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        city_id.setCellValueFactory(new PropertyValueFactory<>("cityId"));
        city_name.setCellValueFactory(new PropertyValueFactory<>("cityName"));
        district_name.setCellValueFactory(new PropertyValueFactory<>("districtName"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        CityInfo.table_add_city = table_add_city;
        table_add_city.setItems(parseUserList());
        CityInfo.txtCityId = txt_city_id;
        CityInfo.txtCityName = txt_city_name;
        CityInfo.txtDistrictName = txt_district_name;
        CityInfo.btnAdd = btn_add_city;
        CityInfo.btnCancel = btn_edit_cancel;
        CityInfo.btnUpdate = btn_edit_update;
    }

    public static ObservableList<CityInfo> parseUserList(){
        CityInfo objCityInfo = new CityInfo();
        cityDetails = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> cityData = objCityInfo.getAddedCity();
        for (int i = 0; i < cityData.size(); i++)
        {
            cityDetails.add(new CityInfo(String.valueOf(i+1), ((cityData.get(i).get(0) == null || cityData.get(i).get(0).equals("")) ? "N/A" : cityData.get(i).get(0)), cityData.get(i).get(1), cityData.get(i).get(2)));
        }
        return cityDetails;
    }

    @FXML
    void addCity(ActionEvent event) {
        String cityId = txt_city_id.getText();
        String cityName = txt_city_name.getText();
        String districtId = String.valueOf(objCityInfo.getSavedDistrictIds().get(txt_district_name.getSelectionModel().getSelectedIndex()));
        String districtName = txt_district_name.getValue();
        String cityStatus = "Active";

        String comp = objCityInfo.confirmNewId(cityId);

        if(!comp.equals(cityId) || comp.equals("")) {
            MysqlCon objMysqlCon = new MysqlCon();
            Statement objStmt = objMysqlCon.stmt;
            Connection objCon = objMysqlCon.con;
            CityInfo.cityIdArr.add(cityId);
            CityInfo.cityNameArr.add(cityName);
            CityInfo.districtIdArr.add(districtId);
            CityInfo.districtNameArr.add(districtName);
            table_add_city.setItems(parseUserList());
            refreshControls();
        }
        else
        {
            alert.setAlertType(Alert.AlertType.ERROR);
            alert.setHeaderText("Error!");
            alert.setContentText("City ID Already Saved.");
            alert.show();
        }
    }

    @FXML
    void editCancel(ActionEvent event) {
        refreshControls();
        btn_add_city.setVisible(true);
        btn_edit_cancel.setVisible(false);
        btn_edit_update.setVisible(false);
    }

    @FXML
    void editUpdate(ActionEvent event) {
        String cityId = txt_city_id.getText();
        String cityName = txt_city_name.getText();
        String districtId = String.valueOf(objCityInfo.getSavedDistrictIds().get(txt_district_name.getSelectionModel().getSelectedIndex()));
        String districtName = txt_district_name.getValue();
        String cityStatus = "Active";

        String comp = objCityInfo.confirmNewId(cityId);

        if(!comp.equals(cityId) || comp.equals("")) {
            MysqlCon objMysqlCon = new MysqlCon();
            Statement objStmt = objMysqlCon.stmt;
            Connection objCon = objMysqlCon.con;
            CityInfo.cityIdArr.set(srNo, cityId);
            CityInfo.cityNameArr.set(srNo, cityName);
            CityInfo.districtIdArr.set(srNo, districtId);
            CityInfo.districtNameArr.set(srNo, districtName);
            table_add_city.setItems(parseUserList());
            refreshControls();
        }
        else
        {
            alert.setAlertType(Alert.AlertType.ERROR);
            alert.setHeaderText("Error!");
            alert.setContentText("City ID Already Saved.");
            alert.show();
        }

        btn_add_city.setVisible(true);
        btn_edit_cancel.setVisible(false);
        btn_edit_update.setVisible(false);
    }

    @FXML
    void cancelClicked(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_city.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void follow(MouseEvent event) {
        if (drawer.isOpened()) {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(-250);
            transition.setByY(0);
            transition.setCycleCount(1);
            transition.play();
            drawer.close();
            FA_Icon.setGlyphName("ANGLE_DOUBLE_RIGHT");
            FA_Icon.setGlyphSize(14);
            //inner_anchor.setRightAnchor(inner_anchor,1d);
            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(-90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(1.1000);
            scale.setToY(1);

            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();
        }
        else {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(250);
            transition.setByY(0);
            transition.setCycleCount(1);
            FA_Icon.setGlyphName("ANGLE_DOUBLE_LEFT");
            FA_Icon.setGlyphSize(14);
            drawer.open();

            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(0.99947777);
            scale.setToY(1);

            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();
        }
    }

    @FXML
    void saveClicked(ActionEvent event) throws IOException {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        try {
            objCityInfo.insertCity(objStmt, objCon);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Parent root = FXMLLoader.load(getClass().getResource("../view/view_city.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    public void refreshControls()
    {
        this.txt_city_id.setText("");
        this.txt_city_name.setText("");
        this.txt_district_name.getSelectionModel().clearSelection();
    }

}
