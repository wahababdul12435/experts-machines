package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import model.*;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class UpdateCity implements Initializable {

    @FXML
    private Label label_update;

    @FXML
    private JFXTextField txt_city_id;

    @FXML
    private JFXTextField txt_city_name;

    @FXML
    private JFXComboBox<String> txt_district_name;

    @FXML
    private JFXComboBox<String> txt_city_status;

    @FXML
    private JFXButton dialog_city_close;

    @FXML
    private JFXButton dialog_city_update;

    public static String cityTableId = "";
    public static String cityId = "";
    public static String cityName = "";
    public static String districtId = "";
    public static String districtName = "";
    public static String cityStatus = "";

    private String selectedDistrictId;
    private ArrayList<ArrayList<String>> districtsData = new ArrayList<>();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ViewCityInfo.lblUpdate = label_update;
        UpdateCityInfo objUpdateCityInfo = new UpdateCityInfo();
        districtsData = objUpdateCityInfo.getDistrictsData();
        ArrayList<String> chosenDistrictNames = getArrayColumn(districtsData, 1);
        txt_district_name.getItems().addAll(chosenDistrictNames);
        txt_city_id.setText(cityId);
        txt_city_name.setText(cityName);
        txt_district_name.setValue(districtName);
        txt_city_status.setValue(cityStatus);
        selectedDistrictId = districtId;
        dialog_city_close.setOnAction((action)->closeDialog());
        dialog_city_update.setOnAction((action)->updateCity());
    }

    public void closeDialog()
    {
        ViewCityInfo.dialog.close();
        ViewCityInfo.stackPane.setVisible(false);
    }

    public void updateCity() {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        CityInfo objCityInfo = new CityInfo();
        cityId = txt_city_id.getText();
        cityName = txt_city_name.getText();
        districtName = txt_district_name.getValue();
        cityStatus = txt_city_status.getValue();
        objCityInfo.updateCity(objStmt, objCon, cityTableId, cityId, cityName, selectedDistrictId, cityStatus);

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/view_city.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void districtChange(ActionEvent event) {
        int index = txt_district_name.getSelectionModel().getSelectedIndex();
        selectedDistrictId = districtsData.get(index).get(0);
    }

    public ArrayList<String> getArrayColumn(ArrayList<ArrayList<String>> companiesData, int columnIndex)
    {
        ArrayList<String> columnList = new ArrayList<>();
        for(ArrayList<String> row: companiesData) {
            columnList.add(row.get(columnIndex));
        }
        return columnList;
    }

}
