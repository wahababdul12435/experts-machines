package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import model.*;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;

public class UpdateDiscount implements Initializable {
    @FXML    private Label label_update;
    @FXML    private JFXTextField txt_approval_id;
    @FXML    private JFXTextField txt_company_id;
    @FXML    private JFXTextField txt_company_Name;
    @FXML    private JFXTextField txt_saleamount;
    @FXML    private JFXTextField txt_discPercent;
    @FXML    private JFXTextField txt_dealerID;
    @FXML    private JFXTextField txt_dealerName;
    @FXML    private JFXTextField txt_productID;
    @FXML    private JFXTextField txt_productName;
    @FXML    private DatePicker datepick_startDate;
    @FXML    private DatePicker datepick_endDate;
    @FXML    private JFXComboBox<String> policyStatus;
    @FXML    private JFXButton dialog_company_close;
    @FXML    private JFXButton dialog_company_update;

    public static String discountapprovalID ="";
    Alert a = new Alert(Alert.AlertType.NONE);

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ViewCompanyInfo.lblUpdate = label_update;
        int discountapprovedID = Integer.parseInt(discountapprovalID);

        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        ViewDiscountInfo updatediscinfo = new ViewDiscountInfo();
        String policy_data = updatediscinfo.get_discountpolicy(objStmt, objCon, discountapprovedID);

        String[] splitbonusData = policy_data.split("--");
            txt_company_id.setText(splitbonusData[8]);
            txt_company_Name.setText(splitbonusData[9]);
        txt_approval_id.setText(splitbonusData[0]);
        txt_discPercent.setText(splitbonusData[2]);
        txt_saleamount.setText(splitbonusData[1]);
            txt_dealerID.setText(splitbonusData[6]);
            txt_dealerName.setText(splitbonusData[7]);
            txt_productID.setText(splitbonusData[10]);

            txt_productName.setText(splitbonusData[11]);

        datepick_startDate.setValue(LOCAL_DATE(splitbonusData[3]));
        datepick_endDate.setValue(LOCAL_DATE(splitbonusData[4]));
        policyStatus.setValue(splitbonusData[5]);
        txt_approval_id.setDisable(true);
        dialog_company_close.setOnAction((action)->closeDialog());
        dialog_company_update.setOnAction((action)->updateDiscount());
    }
    public static final LocalDate LOCAL_DATE (String dateString) {
        LocalDate convertedDates = null;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String[] splitStartDate = dateString.split("/");
        Date date = null;
        try {
            date = new SimpleDateFormat("MMMM").parse(splitStartDate[1]);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int im = cal.get(Calendar.MONTH);
        im =+1;
        if(im<=9) {
            splitStartDate[1] = "0" + im;
        }
        String strtdate_value = splitStartDate[2]+"-"+splitStartDate[1]+"-"+splitStartDate[0];
        convertedDates = LocalDate.parse(strtdate_value, formatter);
        return convertedDates;
    }
    public void closeDialog()
    {
        ViewDiscountInfo.dialog.close();
        ViewDiscountInfo.stackPane.setVisible(false);
    }
    String dealerdetails_onIDEnter = "";
    public void enterpress_dealerID(KeyEvent event) throws IOException, ParseException {
        if (event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB) {
            if(!txt_dealerID.getText().equals(""))
            {
                int intdealer_id = Integer.parseInt(txt_dealerID.getText());
                MysqlCon objMysqlCon = new MysqlCon();
                Statement objStmt = objMysqlCon.stmt;
                Connection objCon = objMysqlCon.con;
                ViewBonusInfo objectBonus = new ViewBonusInfo();
                dealerdetails_onIDEnter = objectBonus.get_dealerDetailswithIDs(objStmt, objCon, intdealer_id);
                if (dealerdetails_onIDEnter.equals("")) {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setHeaderText("Error!");
                    a.setContentText("Dealer ID not found.");
                    a.show();
                    txt_dealerID.requestFocus();
                    txt_dealerName.setText("");
                }
                else {
                    txt_dealerName.setText(dealerdetails_onIDEnter);

                }
            }
        }
    }
    String Productdetails_onIDEnter ="";
    public void enterpress_prodID(KeyEvent event) throws IOException, ParseException {
        if (event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB) {
            if(!txt_productID.getText().equals(""))
            {
                int intprod_id = Integer.parseInt(txt_productID.getText());
                MysqlCon objMysqlCon = new MysqlCon();
                Statement objStmt = objMysqlCon.stmt;
                Connection objCon = objMysqlCon.con;
                ViewBonusInfo object_bonus = new ViewBonusInfo();
                Productdetails_onIDEnter = object_bonus.get_productsDetailswithIDs(objStmt, objCon, intprod_id);
                if (Productdetails_onIDEnter == null) {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setHeaderText("Error!");
                    a.setContentText("Dealer ID not found.");
                    a.show();
                    txt_productID.requestFocus();
                    txt_productName.setText("");
                }
                else {
                    txt_productName.setText(Productdetails_onIDEnter);

                }
            }
        }
    }

    public void updateDiscount() {

        MysqlCon objMysqlConnct = new MysqlCon();
        Statement objctStatmt = objMysqlConnct.stmt;
        Connection objctConnc = objMysqlConnct.con;
        ResultSet rs = null;
        ResultSet rs1 = null;
        ResultSet rs2 = null;
        String dealertableID = "";
        String producttableID ="";
        String companytableID = "";
        try {
            if(!txt_dealerID.getText().isEmpty()) {
                rs = objctStatmt.executeQuery("SELECT dealer_table_id  FROM `dealer_info` WHERE dealer_id = '" + txt_dealerID.getText() + "' ");
                while (rs.next()) {
                    dealertableID = rs.getString(1);
                }
            }
            if(!txt_productID.getText().isEmpty()) {
                rs1 = objctStatmt.executeQuery("SELECT product_table_id  FROM `product_info` WHERE product_id = '" + txt_productID.getText() + "' ");
                while (rs1.next()) {
                    producttableID = rs1.getString(1);
                }
            }
            if(!txt_productID.getText().isEmpty()) {
                rs2 = objctStatmt.executeQuery("SELECT company_table_id  FROM `company_info` WHERE company_id = '" + txt_company_id.getText() + "' ");
                while (rs2.next()) {
                    companytableID = rs2.getString(1);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

            MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        ViewDiscountInfo objDiscInfo = new ViewDiscountInfo();
        discountapprovalID = txt_approval_id.getText();

        String startingDate = "";
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        startingDate = datepick_startDate.getValue().toString();
        Date selectedDate = null;
        try {
            selectedDate = sdf.parse(startingDate);
            startingDate = sdf2.format(selectedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String endingDate = "";
        endingDate = datepick_startDate.getValue().toString();
        Date selected_Date = null;
        try {
            selected_Date = sdf.parse(endingDate);
            endingDate = sdf2.format(selected_Date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        int intdealerID = 0;
        int intcompanyID = 0;
        int intproductID = 0;
        if(!dealertableID.equals("")) {
            intdealerID = Integer.parseInt(dealertableID);
        }
        if(!companytableID.equals("")) {
             intcompanyID = Integer.parseInt(companytableID);
        }
        if(!producttableID.equals("")) {
            intproductID = Integer.parseInt(producttableID);
        }

        objDiscInfo.updateDiscountpolicy(objStmt, objCon,txt_approval_id.getText(),intdealerID , intcompanyID, intproductID, txt_saleamount.getText(), txt_discPercent.getText(), startingDate, endingDate,policyStatus.getValue().toString());

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/view_discount.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
}
