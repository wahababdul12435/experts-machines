package controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import model.DealerReportInfo;
import model.GlobalVariables;
import model.MysqlCon;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class DealerReport implements Initializable {
    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private TableView<DealerReportInfo> table_dealerfinance;

    @FXML
    private TableColumn<DealerReportInfo, String> sr_no;

    @FXML
    private TableColumn<DealerReportInfo, String> dealer_id;

    @FXML
    private TableColumn<DealerReportInfo, String> dealer_name;

    @FXML
    private TableColumn<DealerReportInfo, String> area_name;

    @FXML
    private TableColumn<DealerReportInfo, String> dealer_type;

    @FXML
    private TableColumn<DealerReportInfo, String> orders;

    @FXML
    private TableColumn<DealerReportInfo, String> ordered_items;

    @FXML
    private TableColumn<DealerReportInfo, String> submitted_items;

    @FXML
    private TableColumn<DealerReportInfo, String> returned_items;

    @FXML
    private TableColumn<DealerReportInfo, String> total_sale;

    @FXML
    private TableColumn<DealerReportInfo, String> cash_collection;

    @FXML
    private TableColumn<DealerReportInfo, String> cash_return;

    @FXML
    private TableColumn<DealerReportInfo, String> cash_discount;

    @FXML
    private TableColumn<DealerReportInfo, String> cash_waive_off;

    @FXML
    private TableColumn<DealerReportInfo, String> cash_pending;

    @FXML
    private TableColumn<DealerReportInfo, String> operations;

    @FXML
    private AnchorPane filter_pane;

    @FXML
    private HBox filter_hbox;

    @FXML
    private JFXTextField txt_dealer_id;

    @FXML
    private JFXTextField txt_dealer_name;

    @FXML
    private JFXTextField txt_area_name;

    @FXML
    private JFXComboBox<String> txt_dealer_type;

    @FXML
    private HBox summary_hbox1;

    @FXML
    private Label lbl_total_sale;

    @FXML
    private Label lbl_cash_collection;

    @FXML
    private Label lbl_cash_return;

    @FXML
    private HBox summary_hbox2;

    @FXML
    private Label lbl_cash_discount;

    @FXML
    private Label lbl_cash_waiveoff;

    @FXML
    private Label lbl_cash_pending;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    @FXML
    private BorderPane brdrpane_navfinances;

    @FXML
    private BorderPane menu_bar;

    MysqlCon objMysqlCon;
    Statement objStmt;
    Connection objCon;
    DealerReport objDealerFinance;
    static float totalSale = 0;
    static float cashCollection = 0;
    static float cashReturn = 0;
    static float cashDiscount = 0;
    static float cashWaiveOff = 0;
    static float cashPending = 0;

    public static String txtDealerId = "";
    public static String txtDealerName = "";
    public static String txtAreaName = "";
    public static String txtDealerType = "";
    public static boolean filter;
    public static String summary = "";

    public static MenuButton btnView;
    public ObservableList<DealerReportInfo> dealersReportDetail;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnReport.setStyle("-fx-background-color: #47ab1e");
        objDealerFinance = new DealerReport();
        objMysqlCon = new MysqlCon();
        objStmt = objMysqlCon.stmt;
        objCon = objMysqlCon.con;

        inner_anchor.setLeftAnchor(inner_anchor, 280d);
        drawer.setSidePane(brdrpane_navfinances);
        drawer.open();

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        dealer_id.setCellValueFactory(new PropertyValueFactory<>("dealerId"));
        dealer_name.setCellValueFactory(new PropertyValueFactory<>("dealerName"));
        area_name.setCellValueFactory(new PropertyValueFactory<>("areaName"));
        dealer_type.setCellValueFactory(new PropertyValueFactory<>("dealerType"));
        orders.setCellValueFactory(new PropertyValueFactory<>("orders"));
        ordered_items.setCellValueFactory(new PropertyValueFactory<>("orderedItems"));
        submitted_items.setCellValueFactory(new PropertyValueFactory<>("submittedItems"));
        returned_items.setCellValueFactory(new PropertyValueFactory<>("returnedItems"));
        total_sale.setCellValueFactory(new PropertyValueFactory<>("totalSale"));
        cash_collection.setCellValueFactory(new PropertyValueFactory<>("cashCollection"));
        cash_return.setCellValueFactory(new PropertyValueFactory<>("cashReturn"));
        cash_discount.setCellValueFactory(new PropertyValueFactory<>("cashDiscount"));
        cash_waive_off.setCellValueFactory(new PropertyValueFactory<>("cashWaiveOff"));
        cash_pending.setCellValueFactory(new PropertyValueFactory<>("cashPending"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_dealerfinance.setItems(parseUserList());
        lbl_total_sale.setText("Sales\n"+String.format("%,.0f", totalSale));
        lbl_cash_collection.setText("Collection\n"+String.format("%,.0f", cashCollection));
        lbl_cash_return.setText("Return\n"+String.format("%,.0f", cashReturn));
        lbl_cash_discount.setText("Discount\n"+String.format("%,.0f", cashDiscount));
        lbl_cash_waiveoff.setText("Waive Off\n"+String.format("%,.0f", cashWaiveOff));
        lbl_cash_pending.setText("Pending\n"+String.format("%,.0f", cashPending));
        summary = "";
    }

    private ObservableList<DealerReportInfo> parseUserList() {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        DealerReportInfo objDealerReportInfo = new DealerReportInfo();
        dealersReportDetail = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> dealerReportData;
        if(filter)
        {
            dealerReportData  = objDealerReportInfo.getDealersReportSearch(objStmt, objCon, txtDealerId, txtDealerName, txtAreaName, txtDealerType);
            txt_dealer_id.setText(txtDealerId);
            txt_dealer_name.setText(txtDealerName);
            txt_area_name.setText(txtAreaName);
            txt_dealer_type.setValue(txtDealerType);
            filter = false;
            filter_pane.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
            totalSale = 0;
            cashCollection = 0;
            cashReturn = 0;
            cashDiscount = 0;
            cashWaiveOff = 0;
            cashPending = 0;
        }
        else if(summary.equals("Sale"))
        {
            dealerReportData = objDealerReportInfo.getDealersReportSummary(objStmt, objCon, summary);
            lbl_total_sale.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
        }
        else if(summary.equals("Collection"))
        {
            dealerReportData = objDealerReportInfo.getDealersReportSummary(objStmt, objCon, summary);
            lbl_cash_collection.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
        }
        else if(summary.equals("Return"))
        {
            dealerReportData = objDealerReportInfo.getDealersReportSummary(objStmt, objCon, summary);
            lbl_cash_return.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
        }
        else if(summary.equals("Discount"))
        {
            dealerReportData = objDealerReportInfo.getDealersReportSummary(objStmt, objCon, summary);
            lbl_cash_discount.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
        }
        else if(summary.equals("Pending"))
        {
            dealerReportData = objDealerReportInfo.getDealersReportSummary(objStmt, objCon, summary);
            lbl_cash_pending.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
        }
        else if(summary.equals("Waive Off"))
        {
            dealerReportData = objDealerReportInfo.getDealersReportSummary(objStmt, objCon, summary);
            lbl_cash_waiveoff.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
        }
        else
        {
            dealerReportData = objDealerReportInfo.getDealersReportInfo(objStmt, objCon);
            totalSale = 0;
            cashCollection = 0;
            cashReturn = 0;
            cashDiscount = 0;
            cashWaiveOff = 0;
            cashPending = 0;
        }
        for (int i = 0; i < dealerReportData.size(); i++)
        {
            if(summary.equals(""))
            {
                totalSale += (dealerReportData.get(i).get(5) != null) ? Float.parseFloat(dealerReportData.get(i).get(5)) : 0;
                cashCollection += (dealerReportData.get(i).get(6) != null) ? Float.parseFloat(dealerReportData.get(i).get(6)) : 0;
                cashReturn += (dealerReportData.get(i).get(7) != null) ? Float.parseFloat(dealerReportData.get(i).get(7)) : 0;
                cashDiscount += (dealerReportData.get(i).get(8) != null) ? Float.parseFloat(dealerReportData.get(i).get(8)) : 0;
                cashWaiveOff += (dealerReportData.get(i).get(9) != null) ? Float.parseFloat(dealerReportData.get(i).get(9)) : 0;
                cashPending += (dealerReportData.get(i).get(10) != null) ? Float.parseFloat(dealerReportData.get(i).get(10)) : 0;
            }
            dealersReportDetail.add(new DealerReportInfo(String.valueOf(i+1), dealerReportData.get(i).get(0), ((dealerReportData.get(i).get(1) == null) ? "N/A" : dealerReportData.get(i).get(1)), dealerReportData.get(i).get(2), ((dealerReportData.get(i).get(3) == null) ? "N/A" : dealerReportData.get(i).get(3)), ((dealerReportData.get(i).get(4) == null) ? "0" : dealerReportData.get(i).get(4)), ((dealerReportData.get(i).get(5) == null) ? "0" : dealerReportData.get(i).get(5)), ((dealerReportData.get(i).get(6) == null) ? "0" : dealerReportData.get(i).get(6)), ((dealerReportData.get(i).get(7) == null) ? "0" : dealerReportData.get(i).get(7)), ((dealerReportData.get(i).get(8) == null) ? "0" : dealerReportData.get(i).get(8)), ((dealerReportData.get(i).get(9) == null) ? "0" : dealerReportData.get(i).get(9)), ((dealerReportData.get(i).get(10) == null) ? "0" : dealerReportData.get(i).get(10)), ((dealerReportData.get(i).get(11) == null) ? "0" : dealerReportData.get(i).get(11)), ((dealerReportData.get(i).get(12) == null) ? "0" : dealerReportData.get(i).get(12)), ((dealerReportData.get(i).get(13) == null) ? "0" : dealerReportData.get(i).get(13)), ((dealerReportData.get(i).get(14) == null) ? "0" : dealerReportData.get(i).get(14))));
        }
        return dealersReportDetail;
    }

    @FXML
    void follow(MouseEvent event) {

    }

    @FXML
    void btnReset(ActionEvent event) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/dealer_finance.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void btnSearch(ActionEvent event) {
        txtDealerId = txt_dealer_id.getText();
        txtDealerName = txt_dealer_name.getText();
        txtAreaName = txt_area_name.getText();
        txtDealerType = txt_dealer_type.getValue();
        if(txtDealerType == null)
        {
            txtDealerType = "All";
        }
        filter = true;

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/dealer_finance.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

//    @FXML
//    void addCash(ActionEvent event) throws IOException {
//        Parent root = FXMLLoader.load(getClass().getResource("../view/add_dealer_finance.fxml"));
//        GlobalVariables.baseScene.setRoot(root);
//        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
//    }

    @FXML
    void showCollection(MouseEvent event) {
        summary = "Collection";

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/dealer_finance.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void showDiscount(MouseEvent event) {
        summary = "Discount";

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/dealer_finance.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void showPending(MouseEvent event) {
        summary = "Pending";

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/dealer_finance.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void showReturn(MouseEvent event) {
        summary = "Return";

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/dealer_finance.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void showSale(MouseEvent event) {
        summary = "Sale";

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/dealer_finance.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void showWaiveOff(MouseEvent event) {
        summary = "Waive Off";

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/dealer_finance.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
}
