package controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.util.Duration;
import model.*;
import model.SupplierInfo;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class EnterSupplier implements Initializable {

    @FXML
    private BorderPane menu_bar1;

    @FXML
    private BorderPane nav_setup;

    @FXML
    private JFXTextField txt_supplier_id;

    @FXML
    private JFXTextField txt_supplier_name;

    @FXML
    private JFXTextField txt_supplier_contact;

    @FXML
    private JFXTextField txt_supplier_address;

    @FXML
    private JFXTextField txt_supplier_email;

    @FXML
    private JFXTextField txt_contact_person;

    @FXML
    private JFXTextField txt_supplier_city;

    @FXML
    private Button cancelBtn;

    @FXML
    private Button saveBtn;

    @FXML
    private JFXComboBox<String> txt_company_name;

    @FXML
    private Button btn_add_supplier;

    @FXML    private Button btn_edit_cancel;
    @FXML    private Button btn_edit_update;

    @FXML
    private TableView<SupplierInfo> table_addsupplier;

    @FXML
    private TableColumn<SupplierInfo, String> sr_no;

    @FXML
    private TableColumn<SupplierInfo, String> supplier_id;

    @FXML
    private TableColumn<SupplierInfo, String> supplier_name;

    @FXML
    private TableColumn<SupplierInfo, String> supplier_contact;

    @FXML
    private TableColumn<SupplierInfo, String> supplier_address;

    @FXML
    private TableColumn<SupplierInfo, String> supplier_company;

    @FXML
    private TableColumn<SupplierInfo, String> operations;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    @FXML
    private AnchorPane inner_anchor;

    public static ObservableList<SupplierInfo> supplierDetails;
    Alert alert = new Alert(Alert.AlertType.NONE);
    SupplierInfo objSupplierInfo;
    public static int srNo;
    public static Button btnView;

    public void initialize(URL url, ResourceBundle resourceBundle) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnSetup.setStyle("-fx-background-color: #47ab1e");
        objSupplierInfo = new SupplierInfo();
        btnpane.setOnMouseEntered(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(22);
                btnpane.setMinWidth(20.0);
                btn11.setMinWidth(20.0);

            }
        });
        btnpane.setOnMouseExited(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(14);
                btnpane.setMinWidth(10.0);
                btn11.setMinWidth(10.0);
            }
        });

        inner_anchor.setLeftAnchor(inner_anchor, 280d);
        drawer.setSidePane(nav_setup);
        drawer.open();

        ArrayList<String> chosenCompanyNames = objSupplierInfo.getSavedCompanyNames();
        txt_company_name.getItems().addAll(chosenCompanyNames);

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        supplier_id.setCellValueFactory(new PropertyValueFactory<>("supplierId"));
        supplier_name.setCellValueFactory(new PropertyValueFactory<>("supplierName"));
        supplier_contact.setCellValueFactory(new PropertyValueFactory<>("supplierContact"));
        supplier_address.setCellValueFactory(new PropertyValueFactory<>("supplierAddress"));
        supplier_company.setCellValueFactory(new PropertyValueFactory<>("supplierCompany"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        SupplierInfo.table_addsupplier = table_addsupplier;
        table_addsupplier.setItems(parseUserList());
        SupplierInfo.txtSupplierId = txt_supplier_id;
        SupplierInfo.txtSupplierName = txt_supplier_name;
        SupplierInfo.txtSupplierContact = txt_supplier_contact;
        SupplierInfo.txtSupplierAddress = txt_supplier_address;
        SupplierInfo.txtCompanyName = txt_company_name;
        SupplierInfo.txtSupplierEmail = txt_supplier_email;
        SupplierInfo.txtContactPerson = txt_contact_person;
        SupplierInfo.txtSupplierCity = txt_supplier_city;
        SupplierInfo.btnAdd = btn_add_supplier;
        SupplierInfo.btnCancel = btn_edit_cancel;
        SupplierInfo.btnUpdate = btn_edit_update;
    }

    public static ObservableList<SupplierInfo> parseUserList(){
        SupplierInfo objSupplierInfo = new SupplierInfo();
        supplierDetails = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> supplierData = objSupplierInfo.getAddedSuppliers();
        for (int i = 0; i < supplierData.size(); i++)
        {
            supplierDetails.add(new SupplierInfo(String.valueOf(i+1), ((supplierData.get(i).get(0) == null || supplierData.get(i).get(0).equals("")) ? "N/A" : supplierData.get(i).get(0)), supplierData.get(i).get(1), supplierData.get(i).get(2), supplierData.get(i).get(3), supplierData.get(i).get(4)));
        }
        return supplierDetails;
    }

    @FXML
    void addSupplier(ActionEvent event) {
        String supplierId = txt_supplier_id.getText();
        String supplierName = txt_supplier_name.getText();
        String supplierContact = txt_supplier_contact.getText();
        String supplierAddress = txt_supplier_address.getText();
        String supplierEmail = txt_supplier_email.getText();
        String contactPerson = txt_contact_person.getText();
        String supplierCity = txt_supplier_city.getText();
        String companyId = String.valueOf(objSupplierInfo.getSavedCompanyIds().get(txt_company_name.getSelectionModel().getSelectedIndex()));
        String companyName = txt_company_name.getValue();
        String supplierStatus = "Active";

        String comp = objSupplierInfo.confirmNewId(supplierId);

        if(!comp.equals(supplierId) || comp.equals("")) {
            MysqlCon objMysqlCon = new MysqlCon();
            Statement objStmt = objMysqlCon.stmt;
            Connection objCon = objMysqlCon.con;
            SupplierInfo.supplierIdArr.add(supplierId);
            SupplierInfo.supplierNameArr.add(supplierName);
            SupplierInfo.supplierContactArr.add(supplierContact);
            SupplierInfo.supplierAddressArr.add(supplierAddress);
            SupplierInfo.companyIdArr.add(companyId);
            SupplierInfo.companyNameArr.add(companyName);
            SupplierInfo.supplierEmailArr.add(supplierEmail);
            SupplierInfo.contactPersonArr.add(contactPerson);
            SupplierInfo.supplierCityArr.add(supplierCity);
            table_addsupplier.setItems(parseUserList());
            refreshControls();
        }
        else
        {
            alert.setAlertType(Alert.AlertType.ERROR);
            alert.setHeaderText("Error!");
            alert.setContentText("Supplier ID Already Saved.");
            alert.show();
        }
    }

    @FXML
    void editCancel(ActionEvent event) {
        refreshControls();
        btn_add_supplier.setVisible(true);
        btn_edit_cancel.setVisible(false);
        btn_edit_update.setVisible(false);
    }

    @FXML
    void editUpdate(ActionEvent event) {
        String supplierId = txt_supplier_id.getText();
        String supplierName = txt_supplier_name.getText();
        String supplierContact = txt_supplier_contact.getText();
        String supplierAddress = txt_supplier_address.getText();
        String supplierEmail = txt_supplier_email.getText();
        String contactPerson = txt_contact_person.getText();
        String supplierCity = txt_supplier_city.getText();
        String companyId = String.valueOf(objSupplierInfo.getSavedCompanyIds().get(txt_company_name.getSelectionModel().getSelectedIndex()));
        String companyName = txt_company_name.getValue();
        String supplierStatus = "Active";

        String comp = objSupplierInfo.confirmNewId(supplierId);

        if(!comp.equals(supplierId) || comp.equals("")) {
            MysqlCon objMysqlCon = new MysqlCon();
            Statement objStmt = objMysqlCon.stmt;
            Connection objCon = objMysqlCon.con;
            SupplierInfo.supplierIdArr.set(srNo, supplierId);
            SupplierInfo.supplierNameArr.set(srNo, supplierName);
            SupplierInfo.supplierContactArr.set(srNo, supplierContact);
            SupplierInfo.supplierAddressArr.set(srNo, supplierAddress);
            SupplierInfo.companyIdArr.set(srNo, companyId);
            SupplierInfo.companyNameArr.set(srNo, companyName);
            SupplierInfo.supplierEmailArr.set(srNo, supplierEmail);
            SupplierInfo.contactPersonArr.set(srNo, contactPerson);
            SupplierInfo.supplierCityArr.set(srNo, supplierCity);
            table_addsupplier.setItems(parseUserList());
            refreshControls();
        }
        else
        {
            alert.setAlertType(Alert.AlertType.ERROR);
            alert.setHeaderText("Error!");
            alert.setContentText("Supplier ID Already Saved.");
            alert.show();
        }

        btn_add_supplier.setVisible(true);
        btn_edit_cancel.setVisible(false);
        btn_edit_update.setVisible(false);
    }

    @FXML
    void cancelClicked(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_suppliers.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void follow(MouseEvent event) {
        if (drawer.isOpened()) {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(-250);
            transition.setByY(0);
            transition.setCycleCount(1);
            transition.play();
            drawer.close();
            FA_Icon.setGlyphName("ANGLE_DOUBLE_RIGHT");
            FA_Icon.setGlyphSize(14);
            //inner_anchor.setRightAnchor(inner_anchor,1d);
            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(-90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(1.1000);
            scale.setToY(1);

            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();

        }
        else {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(250);
            transition.setByY(0);
            transition.setCycleCount(1);
            FA_Icon.setGlyphName("ANGLE_DOUBLE_LEFT");
            FA_Icon.setGlyphSize(14);
            drawer.open();

            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(0.99947777);
            scale.setToY(1);

            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();
        }
    }

    @FXML
    void saveClicked(ActionEvent event) throws IOException {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        try {
            objSupplierInfo.insertSupplier(objStmt, objCon);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Parent root = FXMLLoader.load(getClass().getResource("../view/view_suppliers.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    public void refreshControls()
    {
        this.txt_supplier_id.setText("");
        this.txt_supplier_name.setText("");
        this.txt_supplier_contact.setText("");
        this.txt_supplier_address.setText("");
        this.txt_supplier_email.setText("");
        this.txt_contact_person.setText("");
        this.txt_supplier_city.setText("");
        this.txt_company_name.getSelectionModel().clearSelection();
    }

}
