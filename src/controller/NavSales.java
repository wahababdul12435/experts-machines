package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.FetchServerData;
import model.GlobalVariables;
import model.SaleInvoice;
import model.SaleReturn;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;


public class NavSales implements Initializable {

    @FXML
    private VBox Vbox_btns;

    @FXML
    private Button btn_sale_dashboard;

    @FXML
    private Button btn_view_sales;

    @FXML
    private Button btn_create_invoice;

    @FXML
    private Button btn_view_return;

    @FXML
    private Button btn_sale_return;

    @FXML
    private Button btn_stock_transfer_invoice;

    @FXML
    private Button btn_create_estimate_invoice;

    FetchServerData objFetchServerData;
    ArrayList<ArrayList<String>> newBookings;
    ArrayList<ArrayList<String>> newBookingsDetail;
    ArrayList<ArrayList<String>> newReturns;
    ArrayList<ArrayList<String>> newReturnsDetail;
    SaleInvoice objSaleInvoice;
    SaleReturn objSaleReturn;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        DashboardSales.btnView = btn_sale_dashboard;
        ViewSales.btnView = btn_view_sales;
        ViewSaleDetail.btnView = btn_view_sales;
        ViewSalesReturn.btnView = btn_view_return;
        ViewSaleReturnDetail.btnView = btn_view_return;
        EnterSaleInvoice.btnView = btn_create_invoice;
        EnterSaleReturn.btnView = btn_sale_return;
        GlobalVariables.btnNavSaleOrder = btn_view_sales;
        GlobalVariables.btnNavSaleReturn = btn_view_return;
        if(GlobalVariables.findNewBookings > 0)
        {
            btn_view_sales.setText("View Sales ("+GlobalVariables.findNewBookings+")");
        }
        else
        {
            btn_view_sales.setText("View Sales");
        }

        if(GlobalVariables.findNewReturns > 0)
        {
            btn_view_return.setText("View Returns ("+GlobalVariables.findNewReturns+")");
        }
        else
        {
            btn_view_return.setText("View Returns");
        }
    }

    public void viewSales(ActionEvent event) throws IOException {
        if(GlobalVariables.findNewBookings > 0)
        {
            newBookings = importNewBookings();
            newBookingsDetail = importNewBookingsDetail();
//            System.out.println("New Booking Detail: "+newBookingsDetail);
            objSaleInvoice = new SaleInvoice();
            if(newBookings != null)
            {
                objSaleInvoice.insertNewServerSales(newBookings, newBookingsDetail);
            }
        }
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_sales.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
    public void SaleInvoices(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/sale_invoice.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
    public void viewReturn(ActionEvent event) throws IOException {
        if(GlobalVariables.findNewReturns > 0)
        {
            newReturns = importNewReturns();
            newReturnsDetail = importNewReturnsDetail();
//            System.out.println("New Booking Detail: "+newBookingsDetail);
            objSaleReturn = new SaleReturn();
            if(newReturns != null)
            {
                objSaleReturn.insertNewServerReturns(newReturns, newReturnsDetail);
            }
        }
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_sales_return.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
    public void salereturnbtnclick(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/sale_return.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
    public void SaleDashboard(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/dashboard_sales.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    public ArrayList<ArrayList<String>> importNewBookings()
    {
        objFetchServerData = new FetchServerData("new_bookings");
        return objFetchServerData.getNewBookings();
    }
    public ArrayList<ArrayList<String>> importNewBookingsDetail()
    {
        objFetchServerData = new FetchServerData("new_bookings_detail");
        return objFetchServerData.getNewBookingsDetail();
    }
    public ArrayList<ArrayList<String>> importNewReturns()
    {
        objFetchServerData = new FetchServerData("new_returns");
        return objFetchServerData.getNewReturns();
    }
    public ArrayList<ArrayList<String>> importNewReturnsDetail()
    {
        objFetchServerData = new FetchServerData("new_returns_detail");
        return objFetchServerData.getNewReturnsDetail();
    }
}
