package controller;

import com.jfoenix.controls.*;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.FileChooser;
import model.CompanyInfo;
import model.GlobalVariables;
import model.SendMailInfo;
import model.MysqlCon;
import org.controlsfx.control.textfield.TextFields;

import javax.mail.Session;
import java.io.File;
import java.io.IOException;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;
import java.util.ResourceBundle;

public class SendMail implements Initializable {

    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private Label lbl_login_as;

    @FXML
    private AnchorPane notification;

    @FXML
    private JFXButton btn_save_mail;

    @FXML
    private JFXButton btn_update_edit;

    @FXML
    private JFXButton btn_cancel_edit;

    @FXML
    private JFXTextArea txt_message;

    @FXML
    private Label lbl_file_path;

    @FXML
    private JFXTextField txt_to;

    @FXML
    private JFXTextField txt_title;

    @FXML
    private TableView<SendMailInfo> table_new_mails;

    @FXML
    private TableColumn<SendMailInfo, String> sr_no;

    @FXML
    private TableColumn<SendMailInfo, String> to;

    @FXML
    private TableColumn<SendMailInfo, String> title;

    @FXML
    private TableColumn<SendMailInfo, String> message;

    @FXML
    private TableColumn<SendMailInfo, String> attachment;

    @FXML
    private TableColumn<SendMailInfo, String> operations;

    @FXML
    private JFXButton btn_cancel_mail;

    @FXML
    private JFXButton btn_send_mail;

    @FXML
    private StackPane stackPane;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private BorderPane menu_bar1;

    @FXML
    private BorderPane nav_setup;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    public static ObservableList<SendMailInfo> mailsDetails;
    public static int srNo;
    public static Button btnView;
    FileChooser fil_chooser = new FileChooser();
    public static JFXDialog dialog;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnLive.setStyle("-fx-background-color: #47ab1e");
        SendMailInfo objSendMailInfo = new SendMailInfo();
        MailLogin.objStackPane = stackPane;
        MailLogin.lblEmail = lbl_login_as;
        btnpane.setOnMouseEntered(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(22);
                btnpane.setMinWidth(20.0);
                btn11.setMinWidth(20.0);

            }
        });
        btnpane.setOnMouseExited(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(14);
                btnpane.setMinWidth(10.0);
                btn11.setMinWidth(10.0);
            }
        });

        if(GlobalVariables.email.equals(""))
        {
            Parent parent = null;
            try {
                parent = FXMLLoader.load(getClass().getResource("../view/mail_login.fxml"));
            } catch (IOException e) {
                e.printStackTrace();
            }
            JFXDialogLayout dialogLayout = new JFXDialogLayout();
            dialogLayout.setStyle("-fx-padding: -20 -10 -20 -10;");
            dialogLayout.setBody(parent);
            dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

            stackPane.setVisible(true);
            dialog.show();
            dialog.setOverlayClose(false);
        }
        else
        {
            lbl_login_as.setText("("+GlobalVariables.email+")");
        }

        inner_anchor.setLeftAnchor(inner_anchor, 280d);
        drawer.setSidePane(nav_setup);
        drawer.open();
        ArrayList<String> newCitylist = new ArrayList<>();
        MysqlCon objctsMysqlConnc = new MysqlCon();
        Statement objectStatmnt = objctsMysqlConnc.stmt;
        Connection objectConnnection = objctsMysqlConnc.con;
//        newCitylist = objSendMailInfo.get_AllCities(objectStatmnt,objectConnnection);
//        String[] City_possiblee= new String[newCitylist.size()];
//        for(int i =0; i < newCitylist.size();i++)
//        {
//            City_possiblee[i] = newCitylist.get(i);
//        }
//        TextFields.bindAutoCompletion(txt_city,City_possiblee);

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        to.setCellValueFactory(new PropertyValueFactory<>("to"));
        title.setCellValueFactory(new PropertyValueFactory<>("title"));
        message.setCellValueFactory(new PropertyValueFactory<>("message"));
        attachment.setCellValueFactory(new PropertyValueFactory<>("attachment"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_new_mails.setItems(parseUserList());
        SendMailInfo.table_new_mails = table_new_mails;
        SendMailInfo.txtTo = txt_to;
        SendMailInfo.txtTitle = txt_title;
        SendMailInfo.txtMessage = txt_message;
        SendMailInfo.txtAttachment = lbl_file_path;
        SendMailInfo.btnAdd = btn_save_mail;
        SendMailInfo.btnCancel = btn_cancel_edit;
        SendMailInfo.btnUpdate = btn_update_edit;
    }

    public static ObservableList<SendMailInfo> parseUserList() {
        SendMailInfo objSendMailInfo = new SendMailInfo();
        mailsDetails = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> mailsData = objSendMailInfo.getAddedMails();
        for (int i = 0; i < mailsData.size(); i++)
        {
            mailsDetails.add(new SendMailInfo(String.valueOf(i+1), mailsData.get(i).get(0), mailsData.get(i).get(1), mailsData.get(i).get(2), mailsData.get(i).get(3)));
        }
        return mailsDetails;
    }


    @FXML
    void addAttachment(ActionEvent event) {
        File file = fil_chooser.showOpenDialog(GlobalVariables.baseStage);
        if (file != null) {
            lbl_file_path.setText(file.getAbsolutePath());
        }
    }

    @FXML
    void cancelEdit(ActionEvent event) {
        clearAllFields();
    }

    @FXML
    void updateEdit(ActionEvent event) {
        String to = txt_to.getText();
        String title = txt_title.getText();
        String message = txt_message.getText();
        String filePath = lbl_file_path.getText();
        String filePathSplit[] = filePath.split("\\\\");
        String attachment = filePathSplit[filePathSplit.length-1];

        if(!to.equals(""))
        {
            SendMailInfo.toArr.set(srNo, to);
            SendMailInfo.titleArr.set(srNo, title);
            SendMailInfo.messageArr.set(srNo, message);
            SendMailInfo.filePathArr.set(srNo, filePath);
            SendMailInfo.attachmentArr.set(srNo, attachment);

            table_new_mails.setItems(parseUserList());
            clearAllFields();
        }
        else
        {
            GlobalVariables.showAlert("Error", "Please Enter Sender Email");
        }

        btn_save_mail.setVisible(true);
        btn_cancel_edit.setVisible(false);
        btn_update_edit.setVisible(false);
    }

    @FXML
    void cancelClicked(ActionEvent event) {

    }

    @FXML
    void follow(MouseEvent event) {

    }

    @FXML
    void saveMail(ActionEvent event) {
        String to = txt_to.getText();
        String title = txt_title.getText();
        String message = txt_message.getText();
        String filePath = lbl_file_path.getText();
        String filePathSplit[] = filePath.split("\\\\");
        String attachment = filePathSplit[filePathSplit.length-1];

        if(!to.equals(""))
        {
            SendMailInfo.toArr.add(to);
            SendMailInfo.titleArr.add(title);
            SendMailInfo.messageArr.add(message);
            SendMailInfo.filePathArr.add(filePath);
            SendMailInfo.attachmentArr.add(attachment);

            table_new_mails.setItems(parseUserList());
            clearAllFields();
        }
        else
        {
            GlobalVariables.showAlert("Error", "Please Enter Sender Email");
        }

    }

    @FXML
    void sendMails(ActionEvent event) {
        for(int i=0; i<SendMailInfo.toArr.size(); i++)
        {
            SendMailInfo.sendMail(SendMailInfo.toArr.get(i), SendMailInfo.titleArr.get(i), SendMailInfo.messageArr.get(i), SendMailInfo.filePathArr.get(i), SendMailInfo.attachmentArr.get(i));
        }
    }

    private void clearAllFields()
    {
        txt_to.clear();
        txt_title.clear();
        txt_message.clear();
        lbl_file_path.setText("");
    }
}
