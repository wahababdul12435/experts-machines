package controller;

import com.jfoenix.controls.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import model.*;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class UpdatePendingOrder implements Initializable {
    @FXML
    private Label label_update_order;

    @FXML
    private JFXTextField order_id;

    @FXML
    private JFXTextField customer_name;

    @FXML
    private JFXComboBox<String> order_status;

    @FXML
    private JFXButton dialog_order_close;

    @FXML
    private JFXButton dialog_order_update;

    @FXML
    private Label label_update1;

    @FXML
    private Label label_update11;

    @FXML
    private JFXTextField customer_contact;

    @FXML
    private JFXTextField customer_address;

    @FXML
    private JFXComboBox<String> product_name;

    @FXML
    private JFXComboBox<String> product_unit;

    @FXML
    private TableView<UpdatePendingOrderInfo> table_ordered_products;

    @FXML
    private TableColumn<UpdatePendingOrderInfo, String> sr_no;

    @FXML
    private TableColumn<UpdatePendingOrderInfo, String> ordered_product_name;

    @FXML
    private TableColumn<UpdatePendingOrderInfo, String> ordered_product_batch;

    @FXML
    private TableColumn<UpdatePendingOrderInfo, String> ordered_quantity;

    @FXML
    private TableColumn<UpdatePendingOrderInfo, String> ordered_unit;

    @FXML
    private TableColumn<UpdatePendingOrderInfo, String> ordered_item_price;

    @FXML
    private TableColumn<UpdatePendingOrderInfo, String> ordered_bonus;

    @FXML
    private TableColumn<UpdatePendingOrderInfo, String> ordered_discount;

    @FXML
    private TableColumn<UpdatePendingOrderInfo, String> ordered_total_price;

    @FXML
    private TableColumn<UpdatePendingOrderInfo, String> operations;

    @FXML
    private JFXButton add_product;

    @FXML
    private JFXComboBox<String> product_batch;

    @FXML
    private JFXTextField product_quantity;

    @FXML
    private JFXTextField product_discount;

    @FXML
    private JFXTextField total_order_price;

    @FXML
    private JFXTextField total_final_price;

    @FXML
    private JFXTextField total_discount_price;

    @FXML
    private JFXTextField product_bonus;

    @FXML
    private JFXTextField total_bonus_packs;

    @FXML
    private JFXDatePicker booking_date;

    @FXML
    private JFXTimePicker booking_time;

    private String addedProductIs = "";
    private String addedRetailPrice = "0";
    float newFinalPrice = 0.0f;

    public static String orderId = "";
    public static String dealerId = "";
    public static String dealerName = "";
    public static String dealerContact = "";
    public static String dealerAddress = "";
    public static String bookingDate = "02/Aug/2020";
    public static String bookingTime = "10:30 PM";
    public static String orderStatus = "";
    public static ArrayList<String> productsId = new ArrayList<>();
    public static ArrayList<String> productsName = new ArrayList<>();
    public static ArrayList<String> batch = new ArrayList<>();
    public static ArrayList<String> quantity = new ArrayList<>();
    public static ArrayList<String> unit = new ArrayList<>();
    public static ArrayList<String> productsPrice = new ArrayList<>();
    public static ArrayList<String> productsBonus = new ArrayList<>();
    public static ArrayList<String> productsDiscount = new ArrayList<>();
    public static ArrayList<String> productsFinalPrice = new ArrayList<>();

    public static float totalOrderPrice = 0.0f;
    public static Integer totalBonus = 0;
    public static float totalDiscount = 0.0f;
    public static float totalFinalPrice = 0.0f;

    private String dashedProductId = "";
    private String dashedQuantity = "";
    private String dashedUnit = "";
    private String dashedBonus = "";
    private String dashedDiscount = "";


    public static ObservableList<UpdatePendingOrderInfo> orderDetails;

    private ArrayList<ArrayList<String>> productsData = new ArrayList<>();
    private ArrayList<ArrayList<String>> batchData = new ArrayList<>();
    private ArrayList<String> chosenBatchNos = new ArrayList<>();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ViewPendingOrdersInfo.lblUpdate = label_update_order;
        UpdatePendingOrderInfo objUpdatePendingOrderInfo = new UpdatePendingOrderInfo();
        productsData = objUpdatePendingOrderInfo.getProductsData();
        batchData = objUpdatePendingOrderInfo.getBatchData();

        order_id.setText(orderId);
        customer_name.setText(dealerName);
        customer_contact.setText(dealerContact);
        customer_address.setText(dealerAddress);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MMM/yyyy");
        LocalDate localDate = LocalDate.parse(bookingDate , formatter);
        booking_date.setConverter(new StringConverter<LocalDate>()
        {
            private DateTimeFormatter dateTimeFormatter=DateTimeFormatter.ofPattern("dd/MMM/yyyy");

            @Override
            public String toString(LocalDate localDate)
            {
                if(localDate==null)
                    return "";
                return dateTimeFormatter.format(localDate);
            }

            @Override
            public LocalDate fromString(String dateString)
            {
                if(dateString==null || dateString.trim().isEmpty())
                {
                    return null;
                }
                return LocalDate.parse(dateString,dateTimeFormatter);
            }
        });
        booking_date.setValue(localDate);
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("hh:mm a");
        LocalTime localtime = LocalTime.parse(bookingTime, timeFormatter);
        booking_time.setValue(localtime);
        order_status.setValue(orderStatus);
        dialog_order_close.setOnAction((action)->closeDialog());
        dialog_order_update.setOnAction((action)->updateOrder());

        ArrayList<String> chosesProductsName = getArrayColumn(productsData, 1);
        product_name.getItems().addAll(chosesProductsName);

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        ordered_product_name.setCellValueFactory(new PropertyValueFactory<>("productName"));
        ordered_product_batch.setCellValueFactory(new PropertyValueFactory<>("batch"));
        ordered_quantity.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        ordered_unit.setCellValueFactory(new PropertyValueFactory<>("unit"));
        ordered_item_price.setCellValueFactory(new PropertyValueFactory<>("orderItemPrice"));
        ordered_bonus.setCellValueFactory(new PropertyValueFactory<>("orderItemBonus"));
        ordered_discount.setCellValueFactory(new PropertyValueFactory<>("orderItemDiscount"));
        ordered_total_price.setCellValueFactory(new PropertyValueFactory<>("orderItemTotalPrice"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_ordered_products.setItems(parseUserList());
        UpdatePendingOrderInfo.table_ordered_products = table_ordered_products;
        UpdatePendingOrderInfo.total_order_price = total_order_price;
        UpdatePendingOrderInfo.total_bonus_packs = total_bonus_packs;
        UpdatePendingOrderInfo.total_discount_price = total_discount_price;
        UpdatePendingOrderInfo.total_final_price = total_final_price;
        calculatePrices();
        setAllPrices();
    }

    public static ObservableList<UpdatePendingOrderInfo> parseUserList(){
        orderDetails = FXCollections.observableArrayList();
        for (int i = 0; i < productsName.size(); i++)
        {
            orderDetails.add(new UpdatePendingOrderInfo(String.valueOf(i+1), productsName.get(i), batch.get(i), quantity.get(i), unit.get(i), productsPrice.get(i), productsBonus.get(i), productsDiscount.get(i), productsFinalPrice.get(i)));
        }
        return orderDetails;
    }

    public void closeDialog()
    {
        ViewPendingOrdersInfo.dialog.close();
        ViewPendingOrdersInfo.stackPane.setVisible(false);
    }

    public void updateOrder() {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        InvoiceInfo objInvoiceInfo = new InvoiceInfo();
        orderId = order_id.getText();
        dashedProductId = getDashedString(productsId);
        dashedQuantity = getDashedString(quantity);
        dashedUnit = getDashedString(unit);
        dashedBonus = getDashedString(productsBonus);
        dashedDiscount = getDashedString(productsDiscount);
        bookingDate = booking_date.getValue().toString();
        bookingTime = booking_time.getValue().toString();
        orderStatus = order_status.getValue();
        objInvoiceInfo.updateInvoice(objStmt, objCon, orderId, dealerId, dashedProductId, productsId, batch, dashedQuantity, quantity, dashedUnit, unit, String.valueOf(totalOrderPrice), productsPrice, dashedBonus, productsBonus, dashedDiscount, productsDiscount, String.valueOf(totalFinalPrice), productsFinalPrice, GlobalVariables.getStDate(), GlobalVariables.getStTime(), orderStatus);

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/view_pending_orders.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Stage objstage = (Stage) dialog_order_close.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    void addPrduct(ActionEvent event) {
        addedProductIs = getProductId(productsData, product_name.getValue());
        productsId.add(addedProductIs);
        productsName.add(product_name.getValue());
        batch.add(product_batch.getValue());
        quantity.add(product_quantity.getText());
        unit.add(product_unit.getValue());
        productsPrice.add(addedRetailPrice);
        productsBonus.add(product_bonus.getText());
        productsDiscount.add(product_discount.getText());
        newFinalPrice = Float.parseFloat(addedRetailPrice) - (Float.parseFloat(addedRetailPrice)/100 * Float.parseFloat(product_discount.getText()));
        productsFinalPrice.add(String.valueOf(newFinalPrice));
        table_ordered_products.setItems(parseUserList());
        calculatePrices();
        setAllPrices();
    }

    @FXML
    void batchChange(ActionEvent event) {

    }

    @FXML
    void productChange(ActionEvent event) {
        product_batch.getItems().clear();
        product_batch.getItems().addAll(getProductsBatch(batchData, getProductId(productsData, product_name.getValue())));
    }

    @FXML
    void unitChange(ActionEvent event) {

    }

    public ArrayList<String> getArrayColumn(ArrayList<ArrayList<String>> array, int columnIndex)
    {
        ArrayList<String> columnList = new ArrayList<>();
        for(ArrayList<String> row: array) {
            columnList.add(row.get(columnIndex));
        }
        return columnList;
    }

    public ArrayList<String> getProductsBatch(ArrayList<ArrayList<String>> batchData, String productId)
    {
        ArrayList<String> columnList = new ArrayList<>();
        chosenBatchNos = new ArrayList<>();
        for(ArrayList<String> row: batchData) {
            if(row.get(2).equals(productId))
            {
                chosenBatchNos.add(row.get(0));
                columnList.add(row.get(1));
            }
        }
        return columnList;
    }

    public String getProductId(ArrayList<ArrayList<String>> productsData, String productName)
    {
        for(ArrayList<String> row: productsData) {
            if(row.get(1).equals(productName))
            {
                addedRetailPrice = row.get(2);
                return row.get(0);
            }
        }
        return null;
    }

    public String findIdInSubarea(ArrayList<ArrayList<String>> batchData, String subareaName, String areaId)
    {
        for (int i = 0 ; i < batchData.size(); i++)
            if (batchData.get(i).get(1).equals(subareaName) && batchData.get(i).get(2).equals(areaId))
            {
                return batchData.get(i).get(0);
            }
        return null;
    }

    public static void calculatePrices()
    {
        totalOrderPrice = 0.0f;
        totalBonus = 0;
        totalDiscount = 0.0f;
        totalFinalPrice = 0.0f;
        for (int i = 0; i < productsId.size(); i++)
        {
            totalOrderPrice = totalOrderPrice + Float.parseFloat(productsPrice.get(i));
            totalBonus = totalBonus + Integer.parseInt(productsBonus.get(i));
            totalDiscount = totalDiscount + Float.parseFloat(productsPrice.get(i)) - Float.parseFloat(productsFinalPrice.get(i));
            totalFinalPrice = totalFinalPrice + Float.parseFloat(productsFinalPrice.get(i));
        }
    }

    public void setAllPrices()
    {
        DecimalFormat df = new DecimalFormat("0.00");
        total_order_price.setText(df.format(totalOrderPrice));
        total_bonus_packs.setText(String.valueOf(totalBonus));
        total_discount_price.setText(df.format(totalDiscount));
        total_final_price.setText(df.format(totalFinalPrice));
    }

    public String getDashedString(ArrayList<String> arr)
    {
        String dashedString = "";
        boolean iter = false;
        for(String data: arr)
        {
            if(iter)
            {
                dashedString += "_-_"+data;
            }
            else
            {
                iter = true;
                dashedString = data;
            }
        }
        return dashedString;
    }
}
