package controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTreeView;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.util.Duration;
import model.AdjustInvoiceStockInfo;
import model.GlobalVariables;
import model.MysqlCon;
import model.SaleInvoice;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;
import org.controlsfx.control.CheckTreeView;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.ResourceBundle;

public class AdjustInvoiceStock implements Initializable {
    @FXML    private Button print_drawingsummary;
    @FXML    private TableView<AdjustInvoiceStockInfo> Area_tableview;
    @FXML    private TableView<AdjustInvoiceStockInfo> table_drawingsummary;
    @FXML    private TableColumn<AdjustInvoiceStockInfo, String> sr_no;
    @FXML    private TableColumn<AdjustInvoiceStockInfo, String> product_no;
    @FXML    private TableColumn<AdjustInvoiceStockInfo, String> product_name;
    @FXML    private TableColumn<AdjustInvoiceStockInfo, String> product_batch;
    @FXML    private TableColumn<AdjustInvoiceStockInfo, String> ordered_quantity;
    @FXML    private TableColumn<AdjustInvoiceStockInfo, String> in_stock;
    @FXML    private TableColumn<AdjustInvoiceStockInfo, String> deficiency;
    @FXML    private TableColumn<AdjustInvoiceStockInfo, String> operations;
    @FXML   private Button reset_drawingsummary;
    @FXML    private JFXComboBox<String> combo_invoice_status;
    @FXML   private DatePicker datePick_summaryFrom;
    @FXML   private DatePicker datePick_summaryTo;
    @FXML   private TextField txt_InvoNum_To;
    @FXML   private TextField txt_InvoNum_From;


    @FXML    private AnchorPane inner_anchor;
    @FXML    private JFXDrawer drawer;
    @FXML    private Button btn11;
    @FXML    private Pane btnpane;
    @FXML    private BorderPane nav_sales;
    @FXML    private CheckTreeView<String> treeview;
    @FXML    private VBox Vbox_btns;
    @FXML    private FontAwesomeIconView FA_Icon;

    Alert a = new Alert(Alert.AlertType.NONE);
    @FXML
    private HBox menu_bar;

    public ObservableList<AdjustInvoiceStockInfo> drawingSummary;

    public ArrayList<AdjustInvoiceStockInfo> summaryData;


    ArrayList<String> Areaslist = new ArrayList<>();
    ArrayList<String> SubAreaslist = new ArrayList<>();
    ArrayList<String> getcheckedareas = new ArrayList<>();
    ArrayList<String> getsubareasID = new ArrayList<String>();
    int k =0;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        combo_invoice_status.setValue("Pending");
        MysqlCon newobjMysqlConnc = new MysqlCon();
        Statement objectStattmt = newobjMysqlConnc.stmt;
        Connection objectConnnec = newobjMysqlConnc.con;
        AdjustInvoiceStockInfo objctdrwngSummary = new AdjustInvoiceStockInfo();

        Areaslist = objctdrwngSummary.getAreas(objectStattmt,objectConnnec);

        CheckBoxTreeItem<String> root = new CheckBoxTreeItem<String>("Select All");
        for(int i=0;i<Areaslist.size();i++)
        {
            String[] splitAreas = Areaslist.get(i).split("--");

            CheckBoxTreeItem<String> node1 = new CheckBoxTreeItem<String>(splitAreas[1]);
            MysqlCon objMysqlConnc = new MysqlCon();
            Statement objctStattmt = objMysqlConnc.stmt;
            Connection objctConnnec = objMysqlConnc.con;
            AdjustInvoiceStockInfo objctdrawngSummary = new AdjustInvoiceStockInfo();

            int intval_Areaid = Integer.parseInt(splitAreas[0]);
            SubAreaslist = objctdrawngSummary.getSubAreas(objctStattmt,objctConnnec,intval_Areaid);

            for(int j=0;j<SubAreaslist.size();j++)
            {
                String[] splitSubAreas = SubAreaslist.get(j).split("--");
                CheckBoxTreeItem<String> node2 = new CheckBoxTreeItem<String>(splitSubAreas[1]);
                node1.setExpanded(false);
                node1.getChildren().add(node2);
            }
            root.getChildren().add(node1);
            treeview.setRoot(root);
            root.setExpanded(true);

            treeview.setShowRoot(true);
        }

        treeview.getCheckModel().getCheckedItems().addListener(new ListChangeListener<TreeItem<String>>() {
            @Override
            public void onChanged(Change<? extends TreeItem<String>> c) {
//                System.out.println(treeview.getCheckModel().getCheckedItems());
                String nval = treeview.getCheckModel().getCheckedItems().toString();
                String valueAndIndex = c.toString();
                String[] splitedvalueAndIndex = valueAndIndex.split(" ]] ");
                String[] splitedValue = splitedvalueAndIndex[0].split(": ");
                String[] getfunctionAndIndex = splitedvalueAndIndex[1].split(" at ");
                String strngindex = getfunctionAndIndex[1].substring(0,1);
                int indexvalue= Integer.parseInt(strngindex);
                if(getfunctionAndIndex[0].equals("added"))
                {
                    getcheckedareas.add(splitedValue[1]);
                    /*if(getcheckedareas.size()>0) {
                        MysqlCon objMysqlCon = new MysqlCon();
                        Statement objctStmt = objMysqlCon.stmt;
                        Connection objctCnc = objMysqlCon.con;
                        AdjustInvoiceStockInfo objctdrawngSummary = new AdjustInvoiceStockInfo();
                        String checkAreasVal = getcheckedareas.get(k);
                        String subAreaIDs = objctdrawngSummary.getSubAreasCode(objctStmt, objctCnc, checkAreasVal);
                        if (subAreaIDs.equals("")) {
                        } else {
                            getsubareasID.add(subAreaIDs);
                        }
                    }*/
                    k++;
                }
                if(getfunctionAndIndex[0].equals("removed"))
                {
                    getcheckedareas.remove(indexvalue);
                    k--;
                }
            }
        });


        summaryData = new ArrayList<>();
        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        product_no.setCellValueFactory(new PropertyValueFactory<>("productId"));
        product_name.setCellValueFactory(new PropertyValueFactory<>("productName"));
        product_batch.setCellValueFactory(new PropertyValueFactory<>("productBatch"));
        ordered_quantity.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        in_stock.setCellValueFactory(new PropertyValueFactory<>("itemsInStock"));
        deficiency.setCellValueFactory(new PropertyValueFactory<>("deficiency"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_drawingsummary.setItems(parseUserList());

        btnpane.setOnMouseEntered(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(22);
                btnpane.setMinWidth(20.0);
                btn11.setMinWidth(20.0);

            }
        });
        btnpane.setOnMouseExited(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(14);
                btnpane.setMinWidth(10.0);
                btn11.setMinWidth(10.0);
            }
        });
        inner_anchor.setLeftAnchor(inner_anchor, 290d);
        drawer.setSidePane(nav_sales);
        drawer.open();
    }

    public void follow() throws IOException
    {
    /*PathTransition transition;
    transition = new PathTransition();
    Polyline mpoly = new Polyline();
    mpoly.getPoints().addAll(new Double[]{
            0.0,0.0,
            0.0,0.0,
            -200.0,0.0});
    transition.setNode(btn11);
    transition.setDuration(Duration.seconds(2));
    transition.setPath(mpoly);
    transition.play();*/
        if (drawer.isOpened()) {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(-250);
            transition.setByY(0);
            transition.setCycleCount(1);
            transition.play();
            drawer.close();
            FA_Icon.setGlyphName("ANGLE_DOUBLE_RIGHT");
            FA_Icon.setGlyphSize(14);
            //inner_anchor.setRightAnchor(inner_anchor,1d);


            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(-90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(1.28000);
            scale.setToY(1);



            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();
            //
            //inner_anchor.setLeftAnchor(inner_anchor, 5d);
        } else {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(250);
            transition.setByY(0);
            transition.setCycleCount(1);
            FA_Icon.setGlyphName("ANGLE_DOUBLE_LEFT");
            FA_Icon.setGlyphSize(14);
            drawer.open();

            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(0.99947777);
            scale.setToY(1);



            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();

        }


    }

    ArrayList<ArrayList<String>> getINvodetails = new ArrayList<>();
    public void filterFrom_SelectedArea() throws IOException, ParseException {
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String InvoToDate = "";
        String InvofromDate = "";
        if (datePick_summaryFrom.getValue() != null || datePick_summaryTo.getValue() != null) {
            InvofromDate = datePick_summaryFrom.getValue().toString();

            Date selectedDate = null;
            try {
                selectedDate = sdf.parse(InvofromDate);
                InvofromDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            InvoToDate = datePick_summaryTo.getValue().toString();
            Date selectedToDate = null;
            try {
                selectedToDate = sdf.parse(InvoToDate);
                InvoToDate = sdf2.format(selectedToDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        String invoFromVal = txt_InvoNum_From.getText();
        String invoToVal = txt_InvoNum_To.getText();
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        AdjustInvoiceStockInfo objAdjustInvoiceStockInfo = new AdjustInvoiceStockInfo();
        drawingSummary= FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> drawingSummaryData = objAdjustInvoiceStockInfo.getSummarydetails(objStmt, objCon, InvofromDate,InvoToDate,invoFromVal,invoToVal,getcheckedareas,combo_invoice_status.getValue().toString());
        for (int i = 0; i < drawingSummaryData.size(); i++)
        {
            drawingSummary.add(new AdjustInvoiceStockInfo(String.valueOf(i+1), drawingSummaryData.get(i).get(0), drawingSummaryData.get(i).get(1), drawingSummaryData.get(i).get(2), drawingSummaryData.get(i).get(3), drawingSummaryData.get(i).get(4)  , drawingSummaryData.get(i).get(5)));
            summaryData.add(new AdjustInvoiceStockInfo(String.valueOf(i+1), drawingSummaryData.get(i).get(0), drawingSummaryData.get(i).get(1), drawingSummaryData.get(i).get(2), drawingSummaryData.get(i).get(3), drawingSummaryData.get(i).get(4), drawingSummaryData.get(i).get(5)));
        }
        table_drawingsummary.setItems(drawingSummary);

        txt_InvoNum_From.setText("");
        txt_InvoNum_To.setText("");
        datePick_summaryTo.setValue(null);
        datePick_summaryFrom.setValue(null);
        treeview.getCheckModel().clearChecks();

    }
    private ObservableList<AdjustInvoiceStockInfo> parseUserList(){
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String InvoToDate = "";
        String InvofromDate = "";
        if (datePick_summaryFrom.getValue() != null || datePick_summaryTo.getValue() != null) {
            InvofromDate = datePick_summaryFrom.getValue().toString();

            Date selectedDate = null;
            try {
                selectedDate = sdf.parse(InvofromDate);
                InvofromDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            InvoToDate = datePick_summaryTo.getValue().toString();
            Date selectedToDate = null;
            try {
                selectedToDate = sdf.parse(InvoToDate);
                InvoToDate = sdf2.format(selectedToDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        String invoFromVal = txt_InvoNum_From.getText();
        String invoToVal = txt_InvoNum_To.getText();

        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        AdjustInvoiceStockInfo objAdjustInvoiceStockInfo = new AdjustInvoiceStockInfo();
        drawingSummary= FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> drawingSummaryData = objAdjustInvoiceStockInfo.getSummarydetails(objStmt, objCon, InvofromDate,InvoToDate,invoFromVal,invoToVal,getcheckedareas,combo_invoice_status.getValue().toString());
        for (int i = 0; i < drawingSummaryData.size(); i++)
        {
            drawingSummary.add(new AdjustInvoiceStockInfo(String.valueOf(i+1), drawingSummaryData.get(i).get(0), drawingSummaryData.get(i).get(1), drawingSummaryData.get(i).get(2), drawingSummaryData.get(i).get(3), drawingSummaryData.get(i).get(4)  , drawingSummaryData.get(i).get(5)));
            summaryData.add(new AdjustInvoiceStockInfo(String.valueOf(i+1), drawingSummaryData.get(i).get(0), drawingSummaryData.get(i).get(1), drawingSummaryData.get(i).get(2), drawingSummaryData.get(i).get(3), drawingSummaryData.get(i).get(4), drawingSummaryData.get(i).get(5)));
        }
        return drawingSummary;
    }

    public void resetsummary_click() throws IOException
    {
        drawingSummary.removeAll();
        combo_invoice_status.setValue("Pending");

        summaryData.clear();
        table_drawingsummary.getItems().removeAll(drawingSummary);
        AdjustInvoiceStockInfo objAdjustInvoiceStockInfo = new AdjustInvoiceStockInfo();
        MysqlCon objMysqlCon4 = new MysqlCon();
        Statement simple_statment = objMysqlCon4.stmt;
        Connection simple_connect = objMysqlCon4.con;
        ArrayList<ArrayList<String>> drawingSummaryData = objAdjustInvoiceStockInfo.getSummarybyOnlyStatus(simple_statment, simple_connect,combo_invoice_status.getValue().toString());

        for (int i = 0; i < drawingSummaryData.size(); i++)
        {
            drawingSummary.add(new AdjustInvoiceStockInfo(String.valueOf(i+1), drawingSummaryData.get(i).get(0), drawingSummaryData.get(i).get(1), drawingSummaryData.get(i).get(2), drawingSummaryData.get(i).get(3), drawingSummaryData.get(i).get(4) , drawingSummaryData.get(i).get(5)));
            summaryData.add(new AdjustInvoiceStockInfo(String.valueOf(i+1), drawingSummaryData.get(i).get(0), drawingSummaryData.get(i).get(1), drawingSummaryData.get(i).get(2), drawingSummaryData.get(i).get(3), drawingSummaryData.get(i).get(4) , drawingSummaryData.get(i).get(5)));
        }
        table_drawingsummary.setItems(drawingSummary);
    }



}
