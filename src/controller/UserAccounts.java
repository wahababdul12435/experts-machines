package controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import model.*;
import model.UserAccountsInfo;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class UserAccounts implements Initializable {
    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private TableView<UserAccountsInfo> table_viewuseraccounts;

    @FXML
    private TableColumn<UserAccountsInfo, String> sr_no;

    @FXML
    private TableColumn<UserAccountsInfo, String> user_id;

    @FXML
    private TableColumn<UserAccountsInfo, String> user_name;

    @FXML
    private TableColumn<UserAccountsInfo, String> right_insert;

    @FXML
    private TableColumn<UserAccountsInfo, String> right_view;

    @FXML
    private TableColumn<UserAccountsInfo, String> right_change;

    @FXML
    private TableColumn<UserAccountsInfo, String> right_delete;

    @FXML
    private TableColumn<UserAccountsInfo, String> right_stock;

    @FXML
    private TableColumn<UserAccountsInfo, String> right_cash;

    @FXML
    private TableColumn<UserAccountsInfo, String> right_settings;

    @FXML
    private TableColumn<UserAccountsInfo, String> mobile_app;

    @FXML
    private TableColumn<UserAccountsInfo, String> user_status;

    @FXML
    private TableColumn<UserAccountsInfo, String> operations;

    @FXML
    private AnchorPane filter_pane;

    @FXML
    private HBox filter_hbox;

    @FXML
    private JFXTextField txt_user_id;

    @FXML
    private JFXTextField txt_user_name;

    @FXML
    private JFXComboBox<String> txt_user_type;

    @FXML
    private JFXComboBox<String> txt_user_status;

    @FXML
    private HBox summary_hbox;

    @FXML
    private Label lbl_total;

    @FXML
    private Label lbl_active;

    @FXML
    private Label lbl_inactive;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    @FXML
    private BorderPane brdrpane_navsetting;

    @FXML
    private BorderPane menu_bar;

    public ObservableList<UserAccountsInfo> usersDetail;
    private ArrayList<String> userType;
    MysqlCon objMysqlCon;
    Statement objStmt;
    Connection objCon;
    UserAccountsInfo objUserAccountsInfo;
    private int summaryTotal;
    private int summaryActive;
    private int summaryInActive;

    public static String txtUserId = "";
    public static String txtUsername = "";
    public static String txtUserType = "All";
    public static String txtUserStatus = "";
    public static boolean filter;
    public static Button btnView;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnSettings.setStyle("-fx-background-color: #47ab1e");
        objUserAccountsInfo = new UserAccountsInfo();
        objMysqlCon = new MysqlCon();
        objStmt = objMysqlCon.stmt;
        objCon = objMysqlCon.con;
        userType = objUserAccountsInfo.getUserTypes(objStmt, objCon);
        txt_user_type.getItems().addAll(userType);

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        user_id.setCellValueFactory(new PropertyValueFactory<>("userId"));
        user_name.setCellValueFactory(new PropertyValueFactory<>("userName"));
        right_insert.setCellValueFactory(new PropertyValueFactory<>("rightInsert"));
        right_view.setCellValueFactory(new PropertyValueFactory<>("rightView"));
        right_change.setCellValueFactory(new PropertyValueFactory<>("rightChange"));
        right_delete.setCellValueFactory(new PropertyValueFactory<>("rightDelete"));
        right_stock.setCellValueFactory(new PropertyValueFactory<>("rightStock"));
        right_cash.setCellValueFactory(new PropertyValueFactory<>("rightCash"));
        right_settings.setCellValueFactory(new PropertyValueFactory<>("rightSettings"));
        mobile_app.setCellValueFactory(new PropertyValueFactory<>("mobileApp"));
        user_status.setCellValueFactory(new PropertyValueFactory<>("userStatus"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_viewuseraccounts.setItems(parseUserList());
        lbl_total.setText("User Acccounts\n"+summaryTotal);
        lbl_active.setText("Active\n"+summaryActive);
        lbl_inactive.setText("In Active\n"+summaryInActive);
    }

    private ObservableList<UserAccountsInfo> parseUserList(){
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        usersDetail = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> usersData;
        if(filter)
        {
            usersData  = objUserAccountsInfo.getUsersAccountInfoSearch(objStmt, objCon, txtUserId, txtUsername, txtUserType, txtUserStatus);
            txt_user_id.setText(txtUserId);
            txt_user_name.setText(txtUsername);
            txt_user_type.setValue(txtUserType);
            txt_user_status.setValue(txtUserStatus);
            filter = false;
            filter_pane.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
        }
        else
        {
            usersData  = objUserAccountsInfo.getUsersAccountInfo(objStmt, objCon);
        }
        for (int i = 0; i < usersData.size(); i++)
        {
            summaryTotal++;
            if(usersData.get(i).get(10).equals("Active"))
            {
                summaryActive++;
            }
            else if(usersData.get(i).get(10).equals("In Active"))
            {
                summaryInActive++;
            }
            usersDetail.add(new UserAccountsInfo(String.valueOf(i+1), usersData.get(i).get(0), ((usersData.get(i).get(1) == null) ? "N/A" : usersData.get(i).get(1)), usersData.get(i).get(2), ((usersData.get(i).get(3).equals("1")) ? "Yes" : "-"), ((usersData.get(i).get(4).equals("1")) ? "Yes" : "-"), ((usersData.get(i).get(5).equals("1")) ? "Yes" : "-"), ((usersData.get(i).get(6).equals("1")) ? "Yes" : "-"), ((usersData.get(i).get(7).equals("1")) ? "Yes" : "-"), ((usersData.get(i).get(8).equals("1")) ? "Yes" : "-"), ((usersData.get(i).get(9).equals("1")) ? "Yes" : "-"), ((usersData.get(i).get(10).equals("1")) ? "Yes" : "-"), usersData.get(i).get(11)));
        }
        return usersDetail;
    }

    @FXML
    void follow(MouseEvent event) {

    }

    @FXML
    void btnSearch(ActionEvent event) {
        txtUserId = txt_user_id.getText();
        txtUsername = txt_user_name.getText();
        txtUserType = txt_user_type.getValue();
        txtUserStatus = txt_user_status.getValue();
        if(txtUserStatus == null)
        {
            txtUserStatus = "All";
        }
        filter = true;

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/user_accounts.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void btnReset(ActionEvent event) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/user_accounts.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void createNewUser(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/create_user.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
}
