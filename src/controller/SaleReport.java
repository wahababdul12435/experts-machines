package controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXDrawer;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.util.StringConverter;
import model.*;

import java.io.File;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class SaleReport implements Initializable {

    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private AnchorPane filter_pane;

    @FXML
    private JFXDatePicker txt_from_date;

    @FXML
    private JFXDatePicker txt_to_date;

    @FXML
    private JFXComboBox<String> txt_day;

    @FXML
    private HBox summary_hbox1;

    @FXML
    private Label lbl_booking;

    @FXML
    private Label lbl_delivered;

    @FXML
    private Label lbl_items_returned;

    @FXML
    private Label lbl_booking_price;

    @FXML
    private HBox summary_hbox2;

    @FXML
    private Label lbl_return_price;

    @FXML
    private Label lbl_cash_collection;

    @FXML
    private Label lbl_cash_return;

    @FXML
    private Label lbl_waive_off;

    @FXML
    private TableView<SaleReportInfo> table_salereportlog;

    @FXML
    private TableColumn<SaleReportInfo, String> sr_no;

    @FXML
    private TableColumn<SaleReportInfo, String> date;

    @FXML
    private TableColumn<SaleReportInfo, String> day;

    @FXML
    private TableColumn<SaleReportInfo, String> bookings;

    @FXML
    private TableColumn<SaleReportInfo, String> delivered;

    @FXML
    private TableColumn<SaleReportInfo, String> items_returned;

    @FXML
    private TableColumn<SaleReportInfo, String> booking_price;

    @FXML
    private TableColumn<SaleReportInfo, String> return_price;

    @FXML
    private TableColumn<SaleReportInfo, String> cash_collection;

    @FXML
    private TableColumn<SaleReportInfo, String> cash_return;

    @FXML
    private TableColumn<SaleReportInfo, String> cash_waive_off;

    @FXML
    private TableColumn<SaleReportInfo, String> operations;

    @FXML
    private StackPane stackPane;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    @FXML
    private BorderPane brdrpane_navfinances;

    @FXML
    private BorderPane menu_bar;

    private String currentUserId;
    private String selectedImagePath;
    private ArrayList<String> userInfo;
    private ObservableList<SaleReportInfo> userDetails;
    private SaleReportInfo objSaleReportInfo;
    ArrayList<ArrayList<String>> saleReportData;
    private float bookingsCount = 0;
    private float deliveredCount = 0;
    private float itemsReturned = 0;
    private float bookingPrice = 0;
    private float returnPrice = 0;
    private float cashCollection = 0;
    private float cashReturn = 0;
    private float cashWaiveOff = 0;

    public static String txtFromDate = "";
    public static String txtToDate = "";
    public static String txtDay = "All";
    public static boolean filter;

    MysqlCon objMysqlCon1;
    MysqlCon objMysqlCon2;
    MysqlCon objMysqlCon3;
    Statement objStmt1;
    Statement objStmt2;
    Statement objStmt3;
    Connection objCon;

    public static MenuButton btnView;
    SimpleDateFormat fmt = new SimpleDateFormat("dd/MMM/yyyy");

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        objMysqlCon1 = new MysqlCon();
        objMysqlCon2 = new MysqlCon();
        objMysqlCon3 = new MysqlCon();
        objStmt1 = objMysqlCon1.stmt;
        objStmt2 = objMysqlCon2.stmt;
        objStmt3 = objMysqlCon3.stmt;
        objCon = objMysqlCon1.con;
        TopHeader.btnReport.setStyle("-fx-background-color: #47ab1e");
        objSaleReportInfo = new SaleReportInfo();
        saleReportData = new ArrayList<>();

        txt_from_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        txt_to_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        date.setCellValueFactory(new PropertyValueFactory<>("date"));
        day.setCellValueFactory(new PropertyValueFactory<>("day"));
        bookings.setCellValueFactory(new PropertyValueFactory<>("bookings"));
        delivered.setCellValueFactory(new PropertyValueFactory<>("delivered"));
        items_returned.setCellValueFactory(new PropertyValueFactory<>("itemsReturned"));
        booking_price.setCellValueFactory(new PropertyValueFactory<>("bookingPrice"));
        return_price.setCellValueFactory(new PropertyValueFactory<>("returnPrice"));
        cash_collection.setCellValueFactory(new PropertyValueFactory<>("cashCollection"));
        cash_return.setCellValueFactory(new PropertyValueFactory<>("cashReturn"));
        cash_waive_off.setCellValueFactory(new PropertyValueFactory<>("cashWaiveOff"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_salereportlog.setItems(parseUserList());
        lbl_booking.setText("Orders Booked\n"+String.format("%,.0f", bookingsCount));
        lbl_delivered.setText("Orders Delivered\n"+String.format("%,.0f", deliveredCount));
        lbl_items_returned.setText("Items Returned\n"+String.format("%,.0f", itemsReturned));
        lbl_booking_price.setText("Booking Price\n"+String.format("%,.0f", bookingPrice));
        lbl_return_price.setText("Return Price\n"+String.format("%,.0f", returnPrice));
        lbl_cash_collection.setText("Cash Collection\n"+String.format("%,.0f", cashCollection));
        lbl_cash_return.setText("Cash Returned\n"+String.format("%,.0f", cashReturn));
        lbl_waive_off.setText("Cash Waive off\n"+String.format("%,.0f", cashWaiveOff));
    }

    private ObservableList<SaleReportInfo> parseUserList() {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        userDetails = FXCollections.observableArrayList();

        if (filter) {
//            try {
//                saleReportData = objSaleReportInfo.getDealerReportDetailSearch(objStmt1, objStmt2, objStmt3, objCon, currentDealerId, txtFromDate, txtToDate, txtDay);
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
            if (txtFromDate != null && !txtFromDate.equals("")) {
                txt_from_date.setValue(GlobalVariables.LOCAL_DATE(txtFromDate));
            }
            if (txtToDate != null && !txtToDate.equals("")) {
                txt_to_date.setValue(GlobalVariables.LOCAL_DATE(txtToDate));
            }
            txt_day.setValue(txtDay);
            filter = false;
            filter_pane.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
            txtFromDate = "";
            txtToDate = "";
            txtDay = "All";
        } else {
            try {
                saleReportData = objSaleReportInfo.getSaleReportDetail(objStmt1, objStmt2, objStmt3, objCon);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }


        for (int i = 0; i < saleReportData.size(); i++)
        {
            if(!saleReportData.get(i).get(2).equals("-"))
            {
                bookingsCount += Float.parseFloat(saleReportData.get(i).get(2));
            }
            if(!saleReportData.get(i).get(3).equals("-"))
            {
                deliveredCount += Float.parseFloat(saleReportData.get(i).get(3));
            }
            if(!saleReportData.get(i).get(4).equals("-"))
            {
                itemsReturned += Float.parseFloat(saleReportData.get(i).get(4));
            }
            if(!saleReportData.get(i).get(5).equals("-"))
            {
                bookingPrice += Float.parseFloat(saleReportData.get(i).get(5));
            }
            if(!saleReportData.get(i).get(6).equals("-"))
            {
                returnPrice += Float.parseFloat(saleReportData.get(i).get(6));
            }
            if(!saleReportData.get(i).get(7).equals("-"))
            {
                cashCollection += Float.parseFloat(saleReportData.get(i).get(7));
            }
            if(!saleReportData.get(i).get(8).equals("-"))
            {
                cashReturn += Float.parseFloat(saleReportData.get(i).get(8));
            }
            if(!saleReportData.get(i).get(9).equals("-"))
            {
                cashWaiveOff += Float.parseFloat(saleReportData.get(i).get(9));
            }
            userDetails.add(new SaleReportInfo(String.valueOf(i+1), ((saleReportData.get(i).get(0) == null || saleReportData.get(i).get(0).equals("")) ? "N/A" : saleReportData.get(i).get(0)), saleReportData.get(i).get(1), saleReportData.get(i).get(2), saleReportData.get(i).get(3), saleReportData.get(i).get(4), saleReportData.get(i).get(5), saleReportData.get(i).get(6), saleReportData.get(i).get(7), saleReportData.get(i).get(8), saleReportData.get(i).get(9)));
        }
        return userDetails;
    }


    @FXML
    void btnReset(ActionEvent event) {

    }

    @FXML
    void btnSearch(ActionEvent event) {

    }

    @FXML
    void follow(MouseEvent event) {

    }

    @FXML
    void showBookingPrice(MouseEvent event) {

    }

    @FXML
    void showBookings(MouseEvent event) {

    }

    @FXML
    void showCashCollection(MouseEvent event) {

    }

    @FXML
    void showCashReturn(MouseEvent event) {

    }

    @FXML
    void showCashWaiveOff(MouseEvent event) {

    }

    @FXML
    void showDelivered(MouseEvent event) {

    }

    @FXML
    void showItemsReturned(MouseEvent event) {

    }

    @FXML
    void showReturnPrice(MouseEvent event) {

    }
}
