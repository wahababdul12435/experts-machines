package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import controller.UpdateDealerFinance;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

public class DealerDayReportInfo {
    private String srNo;
    private String paymentId;
    private String dealerId;
    private String dealerName;
    private String date;
    private String day;
    private String bookingInvoice;
    private String deliveredInvoice;
    private String returnedInvoice;
    private String bookingPrice;
    private String discountGiven;
    private String quantityReturned;
    private String returnPrice;
    private String cashCollection;
    private String cashReturn;
    private String cashWaiveOff;
    private String cashPending;

    private HBox operationsPane;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    SimpleDateFormat fmt = new SimpleDateFormat("dd/MMM/yyyy");

    public static StackPane stackPane;
    public static JFXDialog dialog;

    public DealerDayReportInfo() {
        this.srNo = "";
//        this.paymentId = "";
//        this.dealerId = "";
//        this.dealerName = "";
        this.date = "";
        this.day = "";
        this.bookingInvoice = "";
        this.deliveredInvoice = "";
        this.returnedInvoice = "";
        this.bookingPrice = "";
        this.discountGiven = "";
        this.quantityReturned = "";
        this.returnPrice = "";
        this.cashCollection = "";
        this.cashReturn = "";
        this.cashWaiveOff = "";
        this.cashPending = "";
    }

    public DealerDayReportInfo(String srNo, String date, String day, String bookingInvoice, String deliveredInvoice, String returnedInvoice, String bookingPrice, String discountGiven, String quantityReturned, String returnPrice, String cashCollection, String cashReturn, String cashWaiveOff, String cashPending) {
        this.srNo = srNo;
//        this.paymentId = paymentId;
//        this.dealerId = dealerId;
//        this.dealerName = dealerName;
        this.date = date;
        this.day = day;
        this.bookingInvoice = bookingInvoice;
        this.deliveredInvoice = deliveredInvoice;
        this.returnedInvoice = returnedInvoice;
        this.bookingPrice = (!bookingPrice.equals("-")) ? String.format("%,.0f", Float.parseFloat(bookingPrice)) : "-";
        this.discountGiven = (!discountGiven.equals("-")) ? String.format("%,.0f", Float.parseFloat(discountGiven)) : "-";
        this.quantityReturned = (!quantityReturned.equals("-")) ? String.format("%,.0f", Float.parseFloat(quantityReturned)) : "-";
        this.returnPrice = (!returnPrice.equals("-")) ? String.format("%,.0f", Float.parseFloat(returnPrice)) : "-";
        this.cashCollection = (!cashCollection.equals("-")) ? String.format("%,.0f", Float.parseFloat(cashCollection)) : "-";
        this.cashReturn = (!cashReturn.equals("-")) ? String.format("%,.0f", Float.parseFloat(cashReturn)) : "-";
        this.cashWaiveOff = (!cashWaiveOff.equals("-")) ? String.format("%,.0f", Float.parseFloat(cashWaiveOff)) : "-";
        this.cashPending = (!cashPending.equals("-")) ? String.format("%,.0f", Float.parseFloat(cashPending)) : "-";

        this.btnEdit = new JFXButton();
        this.btnDelete = new JFXButton();
        this.btnEdit.setGraphic(createEditIcon());
        this.btnDelete.setGraphic(createDeleteIcon());
        this.btnEdit.getStyleClass().add("btn_icon");
        this.btnDelete.getStyleClass().add("btn_icon");
        this.operationsPane = new HBox(btnEdit, btnDelete);
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getDealerId() {
        return dealerId;
    }

    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getBookingInvoice() {
        return bookingInvoice;
    }

    public void setBookingInvoice(String bookingInvoice) {
        this.bookingInvoice = bookingInvoice;
    }

    public String getDeliveredInvoice() {
        return deliveredInvoice;
    }

    public void setDeliveredInvoice(String deliveredInvoice) {
        this.deliveredInvoice = deliveredInvoice;
    }

    public String getReturnedInvoice() {
        return returnedInvoice;
    }

    public void setReturnedInvoice(String returnedInvoice) {
        this.returnedInvoice = returnedInvoice;
    }

    public String getBookingPrice() {
        return bookingPrice;
    }

    public void setBookingPrice(String bookingPrice) {
        this.bookingPrice = bookingPrice;
    }

    public String getDiscountGiven() {
        return discountGiven;
    }

    public void setDiscountGiven(String discountGiven) {
        this.discountGiven = discountGiven;
    }

    public String getCashPending() {
        return cashPending;
    }

    public void setCashPending(String cashPending) {
        this.cashPending = cashPending;
    }

    public String getQuantityReturned() {
        return quantityReturned;
    }

    public void setQuantityReturned(String quantityReturned) {
        this.quantityReturned = quantityReturned;
    }

    public String getReturnPrice() {
        return returnPrice;
    }

    public void setReturnPrice(String returnPrice) {
        this.returnPrice = returnPrice;
    }

    public String getCashCollection() {
        return cashCollection;
    }

    public void setCashCollection(String cashCollection) {
        this.cashCollection = cashCollection;
    }

    public String getCashReturn() {
        return cashReturn;
    }

    public void setCashReturn(String cashReturn) {
        this.cashReturn = cashReturn;
    }

    public String getCashWaiveOff() {
        return cashWaiveOff;
    }

    public void setCashWaiveOff(String cashWaiveOff) {
        this.cashWaiveOff = cashWaiveOff;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public static StackPane getStackPane() {
        return stackPane;
    }

    public static void setStackPane(StackPane stackPane) {
        DealerDayReportInfo.stackPane = stackPane;
    }

    public static JFXDialog getDialog() {
        return dialog;
    }

    public static void setDialog(JFXDialog dialog) {
        DealerDayReportInfo.dialog = dialog;
    }

    public ArrayList<ArrayList<String>> getDealerReportDetail(Statement stmt1, Statement stmt2, Statement stmt3, Connection con) throws ParseException {
        ArrayList<ArrayList<String>> dealersReportData1 = new ArrayList<ArrayList<String>>();
        ArrayList<ArrayList<String>> dealersReportData2 = new ArrayList<ArrayList<String>>();
        ArrayList<ArrayList<String>> dealersReportData3 = new ArrayList<ArrayList<String>>();
        ArrayList<ArrayList<String>> dealersReportData4 = new ArrayList<ArrayList<String>>();
        ArrayList<ArrayList<String>> dealersReportData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs1 = null;
        ResultSet rs2 = null;
        ResultSet rs3 = null;
        int bookingCount = 0;
        int deliveredCount = 0;
        int returnedCount = 0;
        try {
            rs1 = stmt1.executeQuery("SELECT `order_id`, `booking_date`, `delivered_date`, `final_price`, `discount`, `status` FROM `order_info` WHERE `status` != 'Deleted' ORDER BY str_to_date(`order_info`.`booking_date`, '%d/%b/%Y') DESC");
            while (rs1.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs1.getString(1)); // Order Id
                temp.add(rs1.getString(2)); // Booking Date
                temp.add(rs1.getString(4)); // Booking Price
                temp.add(rs1.getString(5)); // Discount Given
                dealersReportData1.add(temp);
                temp = new ArrayList<String>();
                temp.add(rs1.getString(1)); // Order Id
                temp.add(rs1.getString(3)); // Delivery Date
                temp.add(rs1.getString(4)); // Status
                dealersReportData2.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            rs2 = stmt2.executeQuery("SELECT `order_return`.`entry_date`, SUM(`order_return_detail`.`prod_quant`) as 'return_quantity', SUM(`order_return_detail`.`total_amount`) as 'return_amount' FROM `order_return` INNER JOIN `order_return_detail` ON `order_return_detail`.`return_id` = `order_return`.`return_id` WHERE `order_return`.`status` != 'Deleted' GROUP BY `order_return`.`entry_date` ORDER BY str_to_date(`order_return`.`entry_date`, '%d/%b/%Y') DESC");
            while (rs2.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs2.getString(1)); // Return Date
                temp.add(rs2.getString(2)); // Return Quantity
                temp.add(rs2.getString(3)); // Return Price
                dealersReportData3.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            rs3 = stmt3.executeQuery("SELECT `dealer_payments`.`payment_id`, `dealer_payments`.`date`, `dealer_payments`.`amount`, `dealer_payments`.`cash_type`, `dealer_payments`.`pending_payments` FROM `dealer_payments` WHERE `dealer_payments`.`status` != 'Deleted' ORDER BY str_to_date(`date`, '%d/%b/%Y') DESC");
            while (rs3.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs3.getString(1)); // Payment Id
                temp.add(rs3.getString(2)); // Payment Date
                temp.add(rs3.getString(3)); // Amount
                temp.add(rs3.getString(4)); // Payment Type
                temp.add(rs3.getString(5)); // Pending Payment
                dealersReportData4.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        String stDateBooking = "";
        String stDateDelivered = "";
        String stDateReturn = "";
        String stDatePayment = "";
        Date dateBooking = null;
        Date dateDelivered = null;
        Date dateReturn = null;
        Date datePayment = null;
        ArrayList<Date> objDatesArray;
        int day;
        String stDay = "";
        boolean newDataAdd = true;

        while (dealersReportData1.size() > 0 || dealersReportData2.size() > 0 || dealersReportData3.size() > 0 || dealersReportData4.size() > 0)
        {
            temp = new ArrayList<>();
            objDatesArray = new ArrayList<>();
            if(dealersReportData1.size() > 0)
            {
                stDateBooking = dealersReportData1.get(0).get(1);
                dateBooking = fmt.parse(stDateBooking);
                objDatesArray.add(dateBooking);
            }
            if(dealersReportData2.size() > 0)
            {
                stDateDelivered = dealersReportData2.get(0).get(1);
                if(stDateDelivered != null && !stDateDelivered.equals(""))
                {
                    dateDelivered = fmt.parse(stDateDelivered);
                    objDatesArray.add(dateDelivered);
                }
                else
                {
                    dealersReportData2.remove(0);
                }
            }
            if(dealersReportData3.size() > 0)
            {
                stDateReturn = dealersReportData3.get(0).get(0);
                dateReturn = fmt.parse(stDateReturn);
                objDatesArray.add(dateReturn);
            }
            if(dealersReportData4.size() > 0)
            {
                stDatePayment = dealersReportData4.get(0).get(1);
                datePayment = fmt.parse(stDatePayment);
                objDatesArray.add(datePayment);
            }
            Date maxDate = Collections.max(objDatesArray);
            day = maxDate.getDay();
            if(day == 0)
                stDay = "Sunday";
            else if(day == 1)
                stDay = "Monday";
            else if(day == 2)
                stDay = "Tuesday";
            else if(day == 3)
                stDay = "Wednesday";
            else if(day == 4)
                stDay = "Thursday";
            else if(day == 5)
                stDay = "Friday";
            else if(day == 6)
                stDay = "Saturday";
            if(dateBooking != null && maxDate.equals(dateBooking) && dealersReportData1.size() > 0)
            {
                if(dealersReportData.size() > 0 && dealersReportData.get(dealersReportData.size()-1).get(0).equals(stDateBooking))
                {
                    newDataAdd = false;
                    if(dealersReportData.get(dealersReportData.size()-1).get(2).equals("-"))
                    {
                        dealersReportData.get(dealersReportData.size()-1).set(2, "1");
                        dealersReportData.get(dealersReportData.size()-1).set(5, dealersReportData1.get(0).get(2));
                        dealersReportData.get(dealersReportData.size()-1).set(6, dealersReportData1.get(0).get(3));
                    }
                    else
                    {
                        int value = Integer.parseInt(dealersReportData.get(dealersReportData.size()-1).get(2));
                        value++;
                        String value1 = dealersReportData.get(dealersReportData.size()-1).get(5);
                        String value2 = dealersReportData.get(dealersReportData.size()-1).get(6);
                        dealersReportData.get(dealersReportData.size()-1).set(2, String.valueOf(value));
                        dealersReportData.get(dealersReportData.size()-1).set(5, String.valueOf(Float.parseFloat(value1)+Float.parseFloat(dealersReportData1.get(0).get(2))));
                        dealersReportData.get(dealersReportData.size()-1).set(6, String.valueOf(Float.parseFloat(value2)+Float.parseFloat(dealersReportData1.get(0).get(3))));
                    }
                }
                else
                {
                    newDataAdd = true;
                    temp.add(stDateBooking);
                    temp.add(stDay);
                    temp.add("1");
                    temp.add("-");
                    temp.add("-");
                    temp.add(dealersReportData1.get(0).get(2));
                    temp.add(dealersReportData1.get(0).get(3));
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                }
                dealersReportData1.remove(0);
            }
            else if(dateDelivered != null && maxDate.equals(dateDelivered) && dealersReportData2.size() > 0)
            {
                if(dealersReportData.size() > 0 && dealersReportData.get(dealersReportData.size()-1).get(0).equals(stDateDelivered))
                {
                    newDataAdd = false;
                    if(dealersReportData2.get(0).get(2).equals("Delivered"))
                    {
                        if(dealersReportData.get(dealersReportData.size()-1).get(3).equals("-"))
                        {
                            dealersReportData.get(dealersReportData.size()-1).set(3, "1");
                        }
                        else
                        {
                            int value = Integer.parseInt(dealersReportData.get(dealersReportData.size()-1).get(3));
                            value++;
                            dealersReportData.get(dealersReportData.size()-1).set(3, String.valueOf(value));
                        }
                    }
                    else if(dealersReportData2.get(0).get(2).equals("Returned"))
                    {
                        if(dealersReportData.get(dealersReportData.size()-1).get(4).equals("-"))
                        {
                            dealersReportData.get(dealersReportData.size()-1).set(4, "1");
                        }
                        else
                        {
                            int value = Integer.parseInt(dealersReportData.get(dealersReportData.size()-1).get(4));
                            value++;
                            dealersReportData.get(dealersReportData.size()-1).set(4, String.valueOf(value));
                        }
                    }
                }
                else
                {
                    newDataAdd = true;
                    temp.add(stDateDelivered);
                    temp.add(stDay);
                    temp.add("-");
                    if(dealersReportData2.get(0).get(2).equals("Delivered"))
                    {
                        temp.add("1");
                        temp.add("-");
                    }
                    else if(dealersReportData2.get(0).get(2).equals("Returned"))
                    {
                        temp.add("-");
                        temp.add("1");
                    }
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                }

                dealersReportData2.remove(0);
            }
            else if(dateReturn != null && maxDate.equals(dateReturn) && dealersReportData3.size() > 0)
            {
                if(dealersReportData.size() > 0 && dealersReportData.get(dealersReportData.size()-1).get(0).equals(stDateReturn))
                {
                    newDataAdd = false;
                    if(dealersReportData.get(dealersReportData.size()-1).get(7).equals("-"))
                    {
                        dealersReportData.get(dealersReportData.size()-1).set(7, dealersReportData3.get(0).get(1));
                        dealersReportData.get(dealersReportData.size()-1).set(8, dealersReportData3.get(0).get(2));
                    }
                    else
                    {
                        String value1 = dealersReportData.get(dealersReportData.size()-1).get(7);
                        String value2 = dealersReportData.get(dealersReportData.size()-1).get(8);
                        dealersReportData.get(dealersReportData.size()-1).set(7, String.valueOf(Integer.parseInt(value1)+Integer.parseInt(dealersReportData3.get(0).get(1))));
                        dealersReportData.get(dealersReportData.size()-1).set(8, String.valueOf(Integer.parseInt(value2)+Integer.parseInt(dealersReportData3.get(0).get(2))));
                    }
                }
                else
                {
                    newDataAdd = true;
                    temp.add(stDateReturn);
                    temp.add(stDay);
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add(dealersReportData3.get(0).get(1));
                    temp.add(dealersReportData3.get(0).get(2));
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                }
                dealersReportData3.remove(0);
            }
            else if (datePayment != null && maxDate.equals(datePayment) && dealersReportData4.size() > 0)
            {
                if(dealersReportData.size() > 0 && dealersReportData.get(dealersReportData.size()-1).get(0).equals(stDatePayment))
                {
                    newDataAdd = false;
                    if(dealersReportData4.get(0).get(3).equals("Collection"))
                    {
                        if(dealersReportData.get(dealersReportData.size()-1).get(7).equals("-"))
                        {
                            dealersReportData.get(dealersReportData.size()-1).set(7, dealersReportData4.get(0).get(2));
                        }
                        else
                        {
                            String value = dealersReportData.get(dealersReportData.size()-1).get(7);
                            dealersReportData.get(dealersReportData.size()-1).set(7, String.valueOf(Float.parseFloat(value)+Float.parseFloat(dealersReportData4.get(0).get(2))));
                        }
                    }
                    else if (dealersReportData4.get(0).get(3).equals("Return"))
                    {
                        if(dealersReportData.get(dealersReportData.size()-1).get(8).equals("-"))
                        {
                            dealersReportData.get(dealersReportData.size()-1).set(8, dealersReportData4.get(0).get(2));
                        }
                        else
                        {
                            String value = dealersReportData.get(dealersReportData.size()-1).get(8);
                            dealersReportData.get(dealersReportData.size()-1).set(8, String.valueOf(Float.parseFloat(value)+Float.parseFloat(dealersReportData4.get(0).get(2))));
                        }
                    }
                    else if (dealersReportData4.get(0).get(3).equals("Waive Off"))
                    {
                        if(dealersReportData.get(dealersReportData.size()-1).get(9).equals("-"))
                        {
                            dealersReportData.get(dealersReportData.size()-1).set(9, dealersReportData4.get(0).get(2));
                        }
                        else
                        {
                            String value = dealersReportData.get(dealersReportData.size()-1).get(9);
                            dealersReportData.get(dealersReportData.size()-1).set(9, String.valueOf(Float.parseFloat(value)+Float.parseFloat(dealersReportData4.get(0).get(2))));
                        }
                    }
                    if(dealersReportData.get(dealersReportData.size()-1).get(12).equals("-"))
                    {
                        dealersReportData.get(dealersReportData.size()-1).set(12, dealersReportData4.get(0).get(4));
                    }
                    else
                    {
                        String value = dealersReportData.get(dealersReportData.size()-1).get(12);
                        dealersReportData.get(dealersReportData.size()-1).set(12, String.valueOf(Float.parseFloat(value)+Float.parseFloat(dealersReportData4.get(0).get(2))));
                    }
                }
                else
                {
                    newDataAdd = true;
                    temp.add(stDatePayment);
                    temp.add(stDay);
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    if(dealersReportData4.get(0).get(3).equals("Collection"))
                    {
                        temp.add(dealersReportData4.get(0).get(2));
                        temp.add("-");
                        temp.add("-");
                    }
                    else if (dealersReportData4.get(0).get(3).equals("Return"))
                    {
                        temp.add("-");
                        temp.add(dealersReportData4.get(0).get(2));
                        temp.add("-");
                    }
                    else if (dealersReportData4.get(0).get(3).equals("Waive Off"))
                    {
                        temp.add("-");
                        temp.add("-");
                        temp.add(dealersReportData4.get(0).get(2));
                    }
                    temp.add(dealersReportData4.get(0).get(4));
                }
                dealersReportData4.remove(0);
            }
            if(newDataAdd)
            {
                dealersReportData.add(temp);
            }
            else
            {
                newDataAdd = true;
            }
        }

        return dealersReportData;
    }

    public ArrayList<ArrayList<String>> getDealerReportDetailSearch(Statement stmt1, Statement stmt2, Statement stmt3, Connection con, String fromDate, String toDate, String day) throws ParseException {
        ArrayList<ArrayList<String>> dealersReportData1 = new ArrayList<ArrayList<String>>();
        ArrayList<ArrayList<String>> dealersReportData2 = new ArrayList<ArrayList<String>>();
        ArrayList<ArrayList<String>> dealersReportData3 = new ArrayList<ArrayList<String>>();
        ArrayList<ArrayList<String>> dealersReportData4 = new ArrayList<ArrayList<String>>();
        ArrayList<ArrayList<String>> dealersReportData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs1 = null;
        ResultSet rs2 = null;
        ResultSet rs3 = null;
        String searchQuery1;
        String searchQuery2;
        String searchQuery3;
        int multipleSearch = 0;
        searchQuery1 = "SELECT `order_id`, `booking_date`, `delivered_date`, `final_price`, `discount`, `status` FROM `order_info` WHERE ";
        searchQuery2 = "SELECT `order_return`.`entry_date`, SUM(`order_return_detail`.`prod_quant`) as 'return_quantity', SUM(`order_return_detail`.`total_amount`) as 'return_amount' FROM `order_return` INNER JOIN `order_return_detail` ON `order_return_detail`.`return_id` = `order_return`.`return_id` WHERE ";
        searchQuery3 = "SELECT `dealer_payments`.`payment_id`, `dealer_payments`.`date`, `dealer_payments`.`amount`, `dealer_payments`.`cash_type`, `dealer_payments`.`pending_payments` FROM `dealer_payments` WHERE ";
        if(!fromDate.equals(""))
        {
            searchQuery1 += "str_to_date(`booking_date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') ";
            searchQuery2 += "str_to_date(`order_return`.`entry_date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') ";
            searchQuery3 += "str_to_date(`dealer_payments`.`date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') ";
            multipleSearch++;
        }
        if(!toDate.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery1 += " AND ";
                searchQuery2 += " AND ";
                searchQuery3 += " AND ";
            }
            searchQuery1 += "str_to_date(`booking_date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y') ";
            searchQuery2 += "str_to_date(`order_return`.`entry_date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y') ";
            searchQuery3 += "str_to_date(`dealer_payments`.`date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y') ";
            multipleSearch++;
        }
        if(multipleSearch >= 1)
        {
            searchQuery1 += " AND ";
            searchQuery2 += " AND ";
            searchQuery3 += " AND ";
        }
        searchQuery1 += "`status` != 'Deleted' ORDER BY str_to_date(`order_info`.`booking_date`, '%d/%b/%Y') DESC";
        try {
            rs1 = stmt1.executeQuery(searchQuery1);
            while (rs1.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs1.getString(1)); // Order Id
                temp.add(rs1.getString(2)); // Booking Date
                temp.add(rs1.getString(4)); // Booking Price
                temp.add(rs1.getString(5)); // Discount Given
                dealersReportData1.add(temp);
                temp = new ArrayList<String>();
                temp.add(rs1.getString(1)); // Order Id
                temp.add(rs1.getString(3)); // Delivery Date
                temp.add(rs1.getString(4)); // Status
                dealersReportData2.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        searchQuery2 += "`order_return`.`status` != 'Deleted' GROUP BY `order_return`.`entry_date` ORDER BY str_to_date(`order_return`.`entry_date`, '%d/%b/%Y') DESC";
        try {
            rs2 = stmt2.executeQuery(searchQuery2);
            while (rs2.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs2.getString(1)); // Return Date
                temp.add(rs2.getString(2)); // Return Quantity
                temp.add(rs2.getString(3)); // Return Price
                dealersReportData3.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            searchQuery3 += "`dealer_payments`.`status` != 'Deleted' ORDER BY str_to_date(`date`, '%d/%b/%Y') DESC";
            rs3 = stmt3.executeQuery(searchQuery3);
            while (rs3.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs3.getString(1)); // Payment Id
                temp.add(rs3.getString(2)); // Payment Date
                temp.add(rs3.getString(3)); // Amount
                temp.add(rs3.getString(4)); // Payment Type
                temp.add(rs3.getString(5)); // Pending Payment
                dealersReportData4.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        String stDateBooking = "";
        String stDateDelivered = "";
        String stDateReturn = "";
        String stDatePayment = "";
        Date dateBooking = null;
        Date dateDelivered = null;
        Date dateReturn = null;
        Date datePayment = null;
        ArrayList<Date> objDatesArray;
        int intDay;
        String stDay = "";
        boolean newDataAdd = true;

        while (dealersReportData1.size() > 0 || dealersReportData2.size() > 0 || dealersReportData3.size() > 0 || dealersReportData4.size() > 0)
        {
            temp = new ArrayList<>();
            objDatesArray = new ArrayList<>();
            if(dealersReportData1.size() > 0)
            {
                stDateBooking = dealersReportData1.get(0).get(1);
                dateBooking = fmt.parse(stDateBooking);
                objDatesArray.add(dateBooking);
            }
            if(dealersReportData2.size() > 0)
            {
                stDateDelivered = dealersReportData2.get(0).get(1);
                if(stDateDelivered != null && !stDateDelivered.equals(""))
                {
                    dateDelivered = fmt.parse(stDateDelivered);
                    objDatesArray.add(dateDelivered);
                }
                else
                {
                    dealersReportData2.remove(0);
                }
            }
            if(dealersReportData3.size() > 0)
            {
                stDateReturn = dealersReportData3.get(0).get(0);
                dateReturn = fmt.parse(stDateReturn);
                objDatesArray.add(dateReturn);
            }
            if(dealersReportData4.size() > 0)
            {
                stDatePayment = dealersReportData4.get(0).get(1);
                datePayment = fmt.parse(stDatePayment);
                objDatesArray.add(datePayment);
            }
            Date maxDate = Collections.max(objDatesArray);
            intDay = maxDate.getDay();
            if(intDay == 0)
                stDay = "Sunday";
            else if(intDay == 1)
                stDay = "Monday";
            else if(intDay == 2)
                stDay = "Tuesday";
            else if(intDay == 3)
                stDay = "Wednesday";
            else if(intDay == 4)
                stDay = "Thursday";
            else if(intDay == 5)
                stDay = "Friday";
            else if(intDay == 6)
                stDay = "Saturday";
            if(dateBooking != null && maxDate.equals(dateBooking) && dealersReportData1.size() > 0)
            {
                if(dealersReportData.size() > 0 && dealersReportData.get(dealersReportData.size()-1).get(0).equals(stDateBooking))
                {
                    newDataAdd = false;
                    if(dealersReportData.get(dealersReportData.size()-1).get(2).equals("-"))
                    {
                        dealersReportData.get(dealersReportData.size()-1).set(2, "1");
                        dealersReportData.get(dealersReportData.size()-1).set(5, dealersReportData1.get(0).get(2));
                        dealersReportData.get(dealersReportData.size()-1).set(6, dealersReportData1.get(0).get(3));
                    }
                    else
                    {
                        int value = Integer.parseInt(dealersReportData.get(dealersReportData.size()-1).get(2));
                        value++;
                        String value1 = dealersReportData.get(dealersReportData.size()-1).get(5);
                        String value2 = dealersReportData.get(dealersReportData.size()-1).get(6);
                        dealersReportData.get(dealersReportData.size()-1).set(2, String.valueOf(value));
                        dealersReportData.get(dealersReportData.size()-1).set(5, String.valueOf(Float.parseFloat(value1)+Float.parseFloat(dealersReportData1.get(0).get(2))));
                        dealersReportData.get(dealersReportData.size()-1).set(6, String.valueOf(Float.parseFloat(value2)+Float.parseFloat(dealersReportData1.get(0).get(3))));
                    }
                }
                else
                {
                    newDataAdd = true;
                    temp.add(stDateBooking);
                    temp.add(stDay);
                    temp.add("1");
                    temp.add("-");
                    temp.add("-");
                    temp.add(dealersReportData1.get(0).get(2));
                    temp.add(dealersReportData1.get(0).get(3));
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                }
                dealersReportData1.remove(0);
            }
            else if(dateDelivered != null && maxDate.equals(dateDelivered) && dealersReportData2.size() > 0)
            {
                if(dealersReportData.size() > 0 && dealersReportData.get(dealersReportData.size()-1).get(0).equals(stDateDelivered))
                {
                    newDataAdd = false;
                    if(dealersReportData2.get(0).get(2).equals("Delivered"))
                    {
                        if(dealersReportData.get(dealersReportData.size()-1).get(3).equals("-"))
                        {
                            dealersReportData.get(dealersReportData.size()-1).set(3, "1");
                        }
                        else
                        {
                            int value = Integer.parseInt(dealersReportData.get(dealersReportData.size()-1).get(3));
                            value++;
                            dealersReportData.get(dealersReportData.size()-1).set(3, String.valueOf(value));
                        }
                    }
                    else if(dealersReportData2.get(0).get(2).equals("Returned"))
                    {
                        if(dealersReportData.get(dealersReportData.size()-1).get(4).equals("-"))
                        {
                            dealersReportData.get(dealersReportData.size()-1).set(4, "1");
                        }
                        else
                        {
                            int value = Integer.parseInt(dealersReportData.get(dealersReportData.size()-1).get(4));
                            value++;
                            dealersReportData.get(dealersReportData.size()-1).set(4, String.valueOf(value));
                        }
                    }
                }
                else
                {
                    newDataAdd = true;
                    temp.add(stDateDelivered);
                    temp.add(stDay);
                    temp.add("-");
                    if(dealersReportData2.get(0).get(2).equals("Delivered"))
                    {
                        temp.add("1");
                        temp.add("-");
                    }
                    else if(dealersReportData2.get(0).get(2).equals("Returned"))
                    {
                        temp.add("-");
                        temp.add("1");
                    }
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                }

                dealersReportData2.remove(0);
            }
            else if(dateReturn != null && maxDate.equals(dateReturn) && dealersReportData3.size() > 0)
            {
                if(dealersReportData.size() > 0 && dealersReportData.get(dealersReportData.size()-1).get(0).equals(stDateReturn))
                {
                    newDataAdd = false;
                    if(dealersReportData.get(dealersReportData.size()-1).get(7).equals("-"))
                    {
                        dealersReportData.get(dealersReportData.size()-1).set(7, dealersReportData3.get(0).get(1));
                        dealersReportData.get(dealersReportData.size()-1).set(8, dealersReportData3.get(0).get(2));
                    }
                    else
                    {
                        String value1 = dealersReportData.get(dealersReportData.size()-1).get(7);
                        String value2 = dealersReportData.get(dealersReportData.size()-1).get(8);
                        dealersReportData.get(dealersReportData.size()-1).set(7, String.valueOf(Integer.parseInt(value1)+Integer.parseInt(dealersReportData3.get(0).get(1))));
                        dealersReportData.get(dealersReportData.size()-1).set(8, String.valueOf(Integer.parseInt(value2)+Integer.parseInt(dealersReportData3.get(0).get(2))));
                    }
                }
                else
                {
                    newDataAdd = true;
                    temp.add(stDateReturn);
                    temp.add(stDay);
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add(dealersReportData3.get(0).get(1));
                    temp.add(dealersReportData3.get(0).get(2));
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                }
                dealersReportData3.remove(0);
            }
            else if (datePayment != null && maxDate.equals(datePayment) && dealersReportData4.size() > 0)
            {
                if(dealersReportData.size() > 0 && dealersReportData.get(dealersReportData.size()-1).get(0).equals(stDatePayment))
                {
                    newDataAdd = false;
                    if(dealersReportData4.get(0).get(3).equals("Collection"))
                    {
                        if(dealersReportData.get(dealersReportData.size()-1).get(7).equals("-"))
                        {
                            dealersReportData.get(dealersReportData.size()-1).set(7, dealersReportData4.get(0).get(2));
                        }
                        else
                        {
                            String value = dealersReportData.get(dealersReportData.size()-1).get(7);
                            dealersReportData.get(dealersReportData.size()-1).set(7, String.valueOf(Integer.parseInt(value)+Integer.parseInt(dealersReportData4.get(0).get(2))));
                        }
                    }
                    else if (dealersReportData4.get(0).get(3).equals("Return"))
                    {
                        if(dealersReportData.get(dealersReportData.size()-1).get(8).equals("-"))
                        {
                            dealersReportData.get(dealersReportData.size()-1).set(8, dealersReportData4.get(0).get(2));
                        }
                        else
                        {
                            String value = dealersReportData.get(dealersReportData.size()-1).get(8);
                            dealersReportData.get(dealersReportData.size()-1).set(8, String.valueOf(Integer.parseInt(value)+Integer.parseInt(dealersReportData4.get(0).get(2))));
                        }
                    }
                    else if (dealersReportData4.get(0).get(3).equals("Waive Off"))
                    {
                        if(dealersReportData.get(dealersReportData.size()-1).get(9).equals("-"))
                        {
                            dealersReportData.get(dealersReportData.size()-1).set(9, dealersReportData4.get(0).get(2));
                        }
                        else
                        {
                            String value = dealersReportData.get(dealersReportData.size()-1).get(9);
                            dealersReportData.get(dealersReportData.size()-1).set(9, String.valueOf(Integer.parseInt(value)+Integer.parseInt(dealersReportData4.get(0).get(2))));
                        }
                    }
                    if(dealersReportData.get(dealersReportData.size()-1).get(12).equals("-"))
                    {
                        dealersReportData.get(dealersReportData.size()-1).set(12, dealersReportData4.get(0).get(4));
                    }
                    else
                    {
                        String value = dealersReportData.get(dealersReportData.size()-1).get(12);
                        dealersReportData.get(dealersReportData.size()-1).set(12, String.valueOf(Float.parseFloat(value)+Float.parseFloat(dealersReportData4.get(0).get(2))));
                    }
                }
                else
                {
                    newDataAdd = true;
                    temp.add(stDatePayment);
                    temp.add(stDay);
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    if(dealersReportData4.get(0).get(3).equals("Collection"))
                    {
                        temp.add(dealersReportData4.get(0).get(2));
                        temp.add("-");
                        temp.add("-");
                    }
                    else if (dealersReportData4.get(0).get(3).equals("Return"))
                    {
                        temp.add("-");
                        temp.add(dealersReportData4.get(0).get(2));
                        temp.add("-");
                    }
                    else if (dealersReportData4.get(0).get(3).equals("Waive Off"))
                    {
                        temp.add("-");
                        temp.add("-");
                        temp.add(dealersReportData4.get(0).get(2));
                    }
                    temp.add(dealersReportData4.get(0).get(4));
                }
                dealersReportData4.remove(0);
            }
            if(newDataAdd && stDay.equals(day))
            {
                dealersReportData.add(temp);
            }
            else if(newDataAdd && day.equals("All"))
            {
                dealersReportData.add(temp);
            }
            else
            {
                newDataAdd = true;
            }
        }

        return dealersReportData;
    }

    private Node createEditIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.EDIT);
        graphic.setFontSize(20.0);
        return graphic;
    }

    private Node createDeleteIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.TRASH);
        graphic.setFontSize(20.0);
        return graphic;
    }


}
