package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import controller.EnterCompany;
import controller.ViewCompany;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class CompanyInfo {
    private String srNo;
    private String id;
    private String name;
    private int totalProducts;
    private String groupsCount;

    public static ArrayList<String> idArr = new ArrayList<>();
    public static ArrayList<String> nameArr = new ArrayList<>();
    public static ArrayList<String> companyAddressArr = new ArrayList<>();
    public static ArrayList<String> companyContactArr = new ArrayList<>();
    public static ArrayList<String> companyEmailArr = new ArrayList<>();
    public static ArrayList<String> companyCityArr = new ArrayList<>();
    public static ArrayList<String> contactNumbertArr = new ArrayList<>();
    public static ArrayList<String> groupsCountArr = new ArrayList<>();
    public static ArrayList<ArrayList<String>> groupNamesArr = new ArrayList<>();

    public static TableView<CompanyInfo> table_addcompany;

    public static JFXTextField txtCompanyId;
    public static JFXTextField txtCity;
    public static JFXTextField txtCompanyAddress;
    public static JFXTextField txtContactPerson;
    public static JFXTextField txtCompanyEmail;
    public static JFXTextField txtCompanyName;
    public static JFXTextField txtContactNum;
    public static JFXTextField txtCompGroup1;
    public static JFXTextField txtCompGroup2;
    public static JFXTextField txtCompGroup3;
    public static JFXTextField txtCompGroup4;
    public static JFXTextField txtCompGroup5;
    public static Button btn1;
    public static Button btn2;
    public static Button btn3;
    public static Button btn4;
    public static Button btn5;

    public static Button btnAdd;
    public static Button btnCancel;
    public static Button btnUpdate;

    private HBox operationsPane;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    MysqlCon objMysqlCon;
    Statement objStmt;
    Connection objCon;

    ArrayList<String> citieslist = new ArrayList<>();
    public ArrayList<String> get_AllCities(Statement stmt2, Connection con2)
    {
        ResultSet rs = null;
        try {
            rs = stmt2.executeQuery("select city_name from city_info where city_status!='Deleted'  order by  city_name ASC ");
            int i=0;
            while (rs.next() )
            {
                int mval = 0;
                citieslist.add(mval, rs.getString(1));
                mval++;
            }

//            con2.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return citieslist;
    }
    public CompanyInfo()
    {
        id = "";
        name = "";

        objMysqlCon = new MysqlCon();
        objStmt = objMysqlCon.stmt;
        objCon = objMysqlCon.con;
    }

    public CompanyInfo(String companyID, String companyName)
    {
        this.id = companyID;
        this.name = companyName;
    }

    public CompanyInfo(String companyName, int products)
    {
        this.totalProducts = products;
        this.name = companyName;
    }

    public CompanyInfo(String srNo, String id, String name, String groupsCount) {
        this.srNo = srNo;
        this.id = id;
        this.name = name;
        this.groupsCount = groupsCount;

        this.btnEdit = new JFXButton("Edit");
        this.btnDelete = new JFXButton("Delete");
        this.btnEdit.setOnAction((action)->editAddedRecord());
        this.btnDelete.setOnAction((action)->deleteAddedRecord());
        this.operationsPane = new HBox(btnEdit, btnDelete);
    }

    private void deleteAddedRecord()
    {
        idArr.remove(Integer.parseInt(srNo)-1);
        nameArr.remove(Integer.parseInt(srNo)-1);
        companyAddressArr.remove(Integer.parseInt(srNo)-1);
        companyContactArr.remove(Integer.parseInt(srNo)-1);
        companyEmailArr.remove(Integer.parseInt(srNo)-1);
        companyCityArr.remove(Integer.parseInt(srNo)-1);
        contactNumbertArr.remove(Integer.parseInt(srNo)-1);
        groupsCountArr.remove(Integer.parseInt(srNo)-1);
        groupNamesArr.remove(Integer.parseInt(srNo)-1);

        table_addcompany.setItems(EnterCompany.parseUserList());
    }

    public void editAddedRecord()
    {
        EnterCompany.srNo = Integer.parseInt(srNo)-1;
        txtCompanyId.setText(idArr.get(Integer.parseInt(srNo)-1));
        txtCity.setText(companyCityArr.get(Integer.parseInt(srNo)-1));
        txtCompanyAddress.setText(companyAddressArr.get(Integer.parseInt(srNo)-1));
        txtContactPerson.setText(companyContactArr.get(Integer.parseInt(srNo)-1));
        txtCompanyEmail.setText(companyEmailArr.get(Integer.parseInt(srNo)-1));
        txtCompanyName.setText(nameArr.get(Integer.parseInt(srNo)-1));
        txtContactNum.setText(contactNumbertArr.get(Integer.parseInt(srNo)-1));
        if(Integer.parseInt(groupsCountArr.get(Integer.parseInt(srNo)-1)) >= 5)
        {
            txtCompGroup5.setText(groupNamesArr.get(Integer.parseInt(srNo)-1).get(4));
            txtCompGroup5.setVisible(true);
            btn5.setVisible(true);
        }
        if(Integer.parseInt(groupsCountArr.get(Integer.parseInt(srNo)-1)) >= 4)
        {
            txtCompGroup4.setText(groupNamesArr.get(Integer.parseInt(srNo)-1).get(3));
            txtCompGroup4.setVisible(true);
            btn4.setVisible(true);
        }
        if(Integer.parseInt(groupsCountArr.get(Integer.parseInt(srNo)-1)) >= 3)
        {
            txtCompGroup3.setText(groupNamesArr.get(Integer.parseInt(srNo)-1).get(2));
            txtCompGroup3.setVisible(true);
            btn3.setVisible(true);
        }
        if(Integer.parseInt(groupsCountArr.get(Integer.parseInt(srNo)-1)) >= 2)
        {
            txtCompGroup2.setText(groupNamesArr.get(Integer.parseInt(srNo)-1).get(1));
            txtCompGroup2.setVisible(true);
            btn2.setVisible(true);
        }
        if(Integer.parseInt(groupsCountArr.get(Integer.parseInt(srNo)-1)) >= 1)
        {
            txtCompGroup1.setText(groupNamesArr.get(Integer.parseInt(srNo)-1).get(0));
            txtCompGroup1.setVisible(true);
            btn1.setVisible(true);
        }
        btnAdd.setVisible(false);
        btnCancel.setVisible(true);
        btnUpdate.setVisible(true);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTotalProducts() {
        return totalProducts;
    }

    public void setTotalProducts(int totalProducts) {
        this.totalProducts = totalProducts;
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getGroupsCount() {
        return groupsCount;
    }

    public void setGroupsCount(String groupsCount) {
        this.groupsCount = groupsCount;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public void insertCompany(Statement stmt, Connection con)
    {
        String currentDate = GlobalVariables.getStDate();
        String currentTime = GlobalVariables.getStTime();
        String currentUser = GlobalVariables.getUserId();
        String insertGroupQuery;
        String companyTableId;
        String insertOverall;
        String insertCompanyQuery;
        try {
            for(int i=0; i<idArr.size(); i++)
            {
                if(idArr.get(i).equals("") || idArr.get(i) == null)
                {
                    insertCompanyQuery = "INSERT INTO `company_info`(`company_name`, `company_address`, `company_city`, `company_contact`, `contact_Person`, `company_email`, `company_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`) VALUES ('"+nameArr.get(i)+"','"+companyAddressArr.get(i)+"','"+companyCityArr.get(i)+"','"+contactNumbertArr.get(i)+"','"+companyContactArr.get(i)+"','"+companyEmailArr.get(i)+"','Active' ,'"+currentUser+"','"+currentDate+"','"+currentTime+"','"+currentUser+"','"+currentDate+"','"+currentTime+"')";
                }
                else
                {
                    insertCompanyQuery = "INSERT INTO `company_info`(`company_id`,`company_name`, `company_address`, `company_city`, `company_contact`, `contact_Person`, `company_email`, `company_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`) VALUES ('"+idArr.get(i)+"','"+nameArr.get(i)+"','"+companyAddressArr.get(i)+"','"+companyCityArr.get(i)+"','"+contactNumbertArr.get(i)+"','"+companyContactArr.get(i)+"','"+companyEmailArr.get(i)+"','Active' ,'"+currentUser+"','"+currentDate+"','"+currentTime+"','"+currentUser+"','"+currentDate+"','"+currentTime+"')";
                }
                stmt.executeUpdate(insertCompanyQuery);
                companyTableId = getCompanyTableId(stmt, con, nameArr.get(i), contactNumbertArr.get(i));
                insertOverall = "INSERT INTO `company_overall_record`(`company_id`, `orders_given`, `successful_orders`, `ordered_packets`, `received_packets`, `ordered_boxes`, `received_boxes`, `order_price`, `discount_price`, `invoiced_price`, `cash_sent`, `return_packets`, `return_boxes`, `return_price`, `cash_return`, `waived_off_price`, `pending_payments`) VALUES ('"+companyTableId+"','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0')";
                stmt.executeUpdate(insertOverall);

                insertGroupQuery = "INSERT INTO `groups_info`(`company_id`, `group_name`, `group_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`) VALUES ";
                for(int j=0; j<groupNamesArr.get(i).size(); j++)
                {
                    if(j>0)
                    {
                        insertGroupQuery += ",";
                    }
                    insertGroupQuery += "('"+companyTableId+"','"+groupNamesArr.get(i).get(j)+"','Active','"+currentUser+"','"+currentDate+"','"+currentTime+"','"+currentUser+"','"+currentDate+"','"+currentTime+"')";
                }
                if(Integer.parseInt(groupsCountArr.get(i)) > 0)
                {
                    stmt.executeUpdate(insertGroupQuery);
                }
            }
            String title = "Successfull";
            String message = "Record Inserted";
            GlobalVariables.showNotification(1, title, message);
        } catch (SQLException e) {
            String title = "Error";
            String message = "Unable to insert";
            GlobalVariables.showNotification(-1, title, message);
//            e.printStackTrace();
        }
        refreshAllArrays();
    }

    private void refreshAllArrays()
    {
        idArr = new ArrayList<>();
        nameArr = new ArrayList<>();
        companyAddressArr = new ArrayList<>();
        companyContactArr = new ArrayList<>();
        companyEmailArr = new ArrayList<>();
        companyCityArr = new ArrayList<>();
        contactNumbertArr = new ArrayList<>();
        groupsCountArr = new ArrayList<>();
        groupNamesArr = new ArrayList<>();
    }

    public ArrayList<String> getCompanyDetail(String companyId)
    {
        ArrayList<String> companyInfo = new ArrayList<>();
        ResultSet rs = null;
        try {
            rs = objStmt.executeQuery("SELECT `company_id`, `company_name`, `company_contact`, `company_address`, `contact_Person`, `company_email`, `creating_date`, `update_date`, `company_status`, `company_image` FROM `company_info` WHERE `company_table_id` = '"+companyId+"'");
            while (rs.next())
            {
                /*if(rs.getString(1) == null)
                {
                    companyInfo.add("0");
                }
                else {
                    companyInfo.add(rs.getString(1)); // Company Id
                }*/
                companyInfo.add(rs.getString(1)); // Company Id
                companyInfo.add(rs.getString(2)); // Name
                companyInfo.add(rs.getString(3)); // Contact
                companyInfo.add(rs.getString(4)); // Address
                companyInfo.add(rs.getString(5)); // Contact Peson
                companyInfo.add(rs.getString(6)); // Email
                companyInfo.add(rs.getString(7)); // Created
                companyInfo.add(rs.getString(8)); // Updated
                companyInfo.add(rs.getString(9)); // Status
                companyInfo.add(rs.getString(10)); // Image
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return companyInfo;
    }

    public ArrayList<ArrayList<String>>  getCompanyNames()
    {
        ArrayList<ArrayList<String>> companyInfo = new ArrayList<>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            rs = objStmt.executeQuery("SELECT `company_table_id`, `company_name` FROM `company_info` WHERE `company_status` != 'Deleted'");
            while (rs.next())
            {
                temp = new ArrayList<>();
                temp.add(rs.getString(1)); // Id
                temp.add(rs.getString(2)); // Name
                companyInfo.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return companyInfo;
    }

    String compID = new String();
    public String confirmNewId(Statement stmt3, Connection con3 ,String companID)
    {
        ResultSet rs3 = null;
        try {
            rs3 = stmt3.executeQuery("select company_id from company_info where company_id = '"+companID+"' and company_status != 'Deleted'");
            while (rs3.next())
            {
                compID = rs3.getString(1) ;
            }
//            con3.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return compID;
    }

    public void updateCompany(Statement stmt, Connection con, String companyTableId, String companyId, String companyName, String companyAddress, String companyContact, String companyEmail, String companyStatus)
    {
        String currentDate = GlobalVariables.getStDate();
        String currentTime = GlobalVariables.getStTime();
        String currentUser = GlobalVariables.getUserId();
        String updateQuery;
        try {
            if(companyId.equals("") || companyId.equals("N/A"))
            {
                updateQuery = "UPDATE `company_info` SET `company_id`= NULL,`company_name`='"+companyName+"',`company_address`='"+companyAddress+"',`company_contact`='"+companyContact+"',`company_email`='"+companyEmail+"',`company_status`='"+companyStatus+"',`update_user_id`='"+currentUser+"',`update_date`='"+currentDate+"',`update_time`='"+currentTime+"' WHERE `company_table_id` = '"+companyTableId+"'";
            }
            else
            {
                updateQuery = "UPDATE `company_info` SET `company_id`='"+companyId+"',`company_name`='"+companyName+"',`company_address`='"+companyAddress+"',`company_contact`='"+companyContact+"',`company_email`='"+companyEmail+"',`company_status`='"+companyStatus+"',`update_user_id`='"+currentUser+"',`update_date`='"+currentDate+"',`update_time`='"+currentTime+"' WHERE `company_table_id` = '"+companyTableId+"'";
            }
            stmt.executeUpdate(updateQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<ArrayList<String>> getAddedCompanies()
    {
        ArrayList<ArrayList<String>> companyData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        for(int i=0; i<idArr.size(); i++)
        {
            temp = new ArrayList<String>();
            temp.add(idArr.get(i));
            temp.add(nameArr.get(i));
            temp.add(groupsCountArr.get(i));
            companyData.add(temp);
        }
        return companyData;
    }

    /*public String[] viewCompanies(Statement stmt, Connection con, int limit)
    {
        String[] companiesData;
        if(limit != -1)
        {
            companiesData = new String[limit];
        }
        else
        {
            limit = 100;
            companiesData = new String[limit];
        }
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("select comp_id,company_name,company_address,company_contact,company_email,company_status from company_info");
            int i = 0;
            while (rs.next() && i < limit)
            {
//                System.out.println(rs.getInt(1) + "  " + rs.getString(2) + "  " + rs.getString(3));
                companiesData[i] = rs.getString(1)+"_*_"+rs.getString(2)+"_*_"+rs.getString(3)+"_*_"+rs.getString(4)+"_*_"+rs.getString(5) +"_*_"+rs.getString(6);
                i++;
            }

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return companiesData;
    }*/

    public String getCompanyTableId(Statement stmt3, Connection con3 , String companyName, String contactNumber)
    {
        ResultSet rs3 = null;
        try {
            rs3 = stmt3.executeQuery("select company_table_id from company_info where company_name = '"+companyName+"' AND company_contact = '"+contactNumber+"' and company_status != 'Deleted'");
            while (rs3.next())
            {
                compID = rs3.getString(1) ;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return compID;
    }
}
