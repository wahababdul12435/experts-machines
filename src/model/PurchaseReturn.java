package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class PurchaseReturn {
    int limit = 5;
    String[] getreturnnumber = new String[1];
    public String[] get_lastreturnnumber(Statement stmt3, Connection con3)
    {
        ResultSet rs3 = null;
        try {
            rs3 = stmt3.executeQuery("select purch_return_no from purch_return_info order by purch_return_no ASC LIMIT 1");
            int i = 0;
            while (rs3.next() && i < limit)
            {

                getreturnnumber[i] = rs3.getString(1) ;
                i++;
            }
            con3.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return getreturnnumber;
    }

    ArrayList<String>  batchlist = new ArrayList<>();
    public ArrayList<String> getprod_Allbatch(Statement stmt2, Connection con2, int product_IDS)
    {
        ResultSet rs = null;
        try {
            rs = stmt2.executeQuery("select batch_no from batchwise_stock where prod_id ='"+product_IDS+"' order by entry_date , entry_time ASC ");
            int i = 0;
            while (rs.next() && i < limit)
            {
                batchlist.add(i,rs.getString(1));
                i++;
            }

            con2.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return batchlist;
    }


    public String[] getprodbatch_quantity(Statement stmt2, Connection con2, int product_IDS,String batch_product)
    {
        String[] prodbatch_quantity = new String[1];
        ResultSet rs = null;
        try {
            rs = stmt2.executeQuery("select quantity,bonus from batchwise_stock where prod_id ='"+product_IDS+"' and batch_no='"+batch_product+"' ");
            int i = 0;
            while (rs.next() && i < limit)
            {

                prodbatch_quantity[i] = rs.getString(1)  + "--" + rs.getString(2);
            }

            con2.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return prodbatch_quantity;
    }

    public void updatereturnBit(Statement stmt, Connection con,int prodID , int prodQuant, int prodbonus , String invoNumber){
        try {
            int tinyint = 1;
            String insertQuery = "UPDATE `purchase_info_detail` SET `returnBit`='"+tinyint+"',`returned_quant`='"+prodQuant+"' ,`returned_bonus_quant`='"+prodbonus+"' WHERE `invoice_num` = '"+invoNumber+"'  and  `prod_id` = '"+prodID+"'";
            stmt.executeUpdate(insertQuery);
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    String[] prod_details = new String[1];
    public String[] get_prodDetailswithIDs(Statement stmt1, Connection con1, int prods_Ids)
    {
        ResultSet rs = null;
        try {
            rs = stmt1.executeQuery("select product_name,retail_price,purchase_price,trade_price,company_id from product_info where  product_id = '"+prods_Ids+"'");
            int i = 0;
            while (rs.next() && i < limit)
            {

                prod_details[i] = rs.getString(1) + "--" + rs.getString(2) + "--" + rs.getString(3) + "--" + rs.getString(4)+"--"+ rs.getString(5);
            }

            con1.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return prod_details;
    }

    String[] purchInvo_details = new String[1];
    public String[] getpprchaseDetails(Statement stmt, Connection con , int product_Ids,String purchinvoicNum)
    {
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("select discount,bonus_quant,recieve_quant,batch_no,expiry_date from purchase_info_detail where prod_id = '"+product_Ids+"' and invoice_num = '"+purchinvoicNum+"'");
            int i = 0;
            while (rs.next() && i < limit)
            {
                purchInvo_details[i] = rs.getString(1) + "--" + rs.getString(2) + "--" + rs.getString(3) + "--" + rs.getString(4) + "--" + rs.getString(5);
            }

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return purchInvo_details;
    }

    String[] prod_stockquant = new String[1];
    public String[] getprodstockquant_withIDs(Statement stmt, Connection con , int product_Ids)
    {
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("select in_stock,bonus_quant from products_stock where product_id = '"+product_Ids+"' and status = 'Active'");
            int i = 0;
            while (rs.next() && i < limit)
            {
                prod_stockquant[i] = rs.getString(1) + "--" + rs.getString(2) ;
            }

            con.close();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return prod_stockquant;
    }

    String[] prod_batch = new String[1];
    public String[] getprod_batch(Statement stmt2, Connection con2, int product_IDS)
    {
        ResultSet rs = null;
        try {
            rs = stmt2.executeQuery("select batch_no from batchwise_stock where prod_id ='"+product_IDS+"' order by entry_date, entry_time ASC LIMIT 1 ");
            int i = 0;
            while (rs.next() && i < limit)
            {

                prod_batch[i] = rs.getString(1) ;
            }

            con2.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return prod_batch;
    }

    String invodetailsArray[] =new String[1];
    public String[] getinvodetails(Statement stmnt,Connection conn,String invoicenumbers)
    {
        ResultSet rs = null;
        try {
            rs = stmnt.executeQuery("select purchase_id,supplier_id,purchase_date from purchase_info where invoice_num ='"+invoicenumbers+"'  ");
            int i = 0;
            while (rs.next() && i < limit)
            {

                invodetailsArray[i] = rs.getString(1) +"--" + rs.getString(2)  +"--" + rs.getString(3) ;
            }
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return invodetailsArray;
    }

    String suppliersdetails[] =new String[1];
    public String[] getdealersname(Statement stmnt,Connection conn,String suplr_ID)
    {
        ResultSet rs = null;
        try {
            rs = stmnt.executeQuery("select supplier_name from supplier_info where supplier_id ='"+suplr_ID+"'  ");
            int i = 0;
            while (rs.next() && i < limit)
            {
                suppliersdetails[i] = rs.getString(1) ;
            }
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return suppliersdetails;
    }

    public void UPDATE_batchStock(Statement stmt, Connection con,int prodID , int prodQuant, int bonusquantity,  String batchnumber)
    {
        try {
            String insertQuery = "UPDATE `batchwise_stock` SET `quantity`='"+prodQuant+"' , `bonus` = '"+bonusquantity+"' WHERE `prod_id` = '"+prodID+"' and `batch_no` = '"+batchnumber+"' ";
            stmt.executeUpdate(insertQuery);
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void insertpurchaseInvoiceInfo(Statement stmt, Connection con, int retrnID, String rtrnInvNum, int suppliersID, float rtrn_totalAmount,float rtrn_grossamount, String rtrnDate, String rtrnTime, String updateDate, String updateTime, int usersID,String rtrn_status )
    {
        String orderplace = "Shadiwal";
        String invoice_status = "Pending";
        String UserIDs = GlobalVariables.getUserId();
        try {
            String insertQuery = "INSERT INTO `purchase_return` ( `return_id`, `purchInvoNumb`, `supplier_id`, `return_total_price`, `return_gross_price`, `entry_date`, `entry_time`, `update_date`, `update_time`, `user_id`, `status`) VALUES ('"+retrnID+"','"+rtrnInvNum+"','"+suppliersID+"','"+rtrn_totalAmount+"','"+rtrn_grossamount+"','"+rtrnDate+"' ,'"+rtrnTime+"' ,'"+updateDate+"','"+updateTime+"','"+usersID+"','"+rtrn_status+"')";
            stmt.executeUpdate(insertQuery);
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void insertPurchReturnInfodetails(Statement stmt, Connection con, int return_id, int prodID,String batchnumbers, int quant, int bon_quant,float discounts, float totalAmount,String  units,String returnReason)
    {
        try {
            String insertQuery = "INSERT INTO `purchase_return_detail` (`return_id`, `prod_id`, `prod_batch`, `prod_quant`, `bonus_quant`, `discount_amount`, `total_amount`, `unit`,`return_reason` ) VALUES ('"+return_id+"','"+prodID+"','"+batchnumbers+"','"+quant+"','"+bon_quant+"','"+discounts+"','"+totalAmount+"' ,'"+units+"','"+returnReason+"' )";
            stmt.executeUpdate(insertQuery);
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void subtract_quantFROMStock(Statement stmt, Connection con,int prodID , int prodQuant, int prodbonus)
    {
        try {
            String insertQuery = "UPDATE `products_stock` SET `in_stock`='"+prodQuant+"',`bonus_quant`='"+prodbonus+"' WHERE `product_id` = '"+prodID+"'";
            stmt.executeUpdate(insertQuery);
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
