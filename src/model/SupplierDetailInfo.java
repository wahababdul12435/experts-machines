package model;

import com.jfoenix.controls.JFXButton;
import javafx.scene.layout.HBox;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class SupplierDetailInfo {
    private String srNo;
    private String companyName;
    private String invoiceNo;
    private String invoiceCost;
    private String suppliedDate;

    private HBox operationsPane;
    private JFXButton btnView;

    public SupplierDetailInfo() {
    }

    public SupplierDetailInfo(String srNo, String companyName, String invoiceNo, String invoiceCost, String suppliedDate) {
        this.srNo = srNo;
        this.companyName = companyName;
        this.invoiceNo = invoiceNo;
        this.invoiceCost = invoiceCost;
        this.suppliedDate = suppliedDate;
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getInvoiceCost() {
        return invoiceCost;
    }

    public void setInvoiceCost(String invoiceCost) {
        this.invoiceCost = invoiceCost;
    }

    public String getSuppliedDate() {
        return suppliedDate;
    }

    public void setSuppliedDate(String suppliedDate) {
        this.suppliedDate = suppliedDate;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnView() {
        return btnView;
    }

    public void setBtnView(JFXButton btnView) {
        this.btnView = btnView;
    }

    public ArrayList<ArrayList<String>> getSupplierOrders(Statement stmt, Connection con, String supplierId)
    {
        ArrayList<ArrayList<String>> suppliedOrderData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT `company_info`.`company_name`, `purchase_info`.`invoice_num`, `purchase_info`.`net_amount`, `purchase_info`.`entry_date` FROM `purchase_info` INNER JOIN `company_info` ON `company_info`.`company_table_id` = `purchase_info`.`comp_id` WHERE `purchase_info`.`supplier_id` = '"+supplierId+"'");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2));
                temp.add(rs.getString(3));
                temp.add(rs.getString(4));
                suppliedOrderData.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return suppliedOrderData;
    }

    public ArrayList<ArrayList<String>> getSupplierOrdersSearch(Statement stmt, Connection con, String supplierId, String companyName, String fromDate, String toDate)
    {
        ArrayList<ArrayList<String>> suppliedOrderData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        String searchQuery;
        int multipleSearch = 0;
        searchQuery = "SELECT `company_info`.`company_name`, `purchase_info`.`invoice_num`, `purchase_info`.`net_amount`, `purchase_info`.`entry_date` FROM `purchase_info` INNER JOIN `company_info` ON `company_info`.`company_table_id` = `purchase_info`.`comp_id` WHERE ";
        if(!companyName.equals("All"))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`purchase_info`.`comp_id` = '"+companyName+"'";
            multipleSearch++;
        }
        if(!fromDate.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "str_to_date(`purchase_info`.`entry_date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y')";
            multipleSearch++;
        }
        if(!toDate.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "str_to_date(`purchase_info`.`entry_date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y')";
            multipleSearch++;
        }
        if(multipleSearch >= 1)
        {
            searchQuery += " AND ";
        }
        searchQuery += "`purchase_info`.`supplier_id` = '"+supplierId+"'";
        try {
            rs = stmt.executeQuery(searchQuery);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2));
                temp.add(rs.getString(3));
                temp.add(rs.getString(4));
                suppliedOrderData.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return suppliedOrderData;
    }

    public ArrayList<ArrayList<String>> getCompaniesData()
    {
        ArrayList<ArrayList<String>> companiesData = new ArrayList<>();
        MysqlCon objMysqlCon = new MysqlCon();
        Statement stmt = objMysqlCon.stmt;
        Connection con = objMysqlCon.con;
        ResultSet rs = null;
        ArrayList<String> temp;
        try {
            rs = stmt.executeQuery("SELECT `company_table_id`, `company_name` FROM `company_info` WHERE `company_status` != 'Deleted'");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2));
                companiesData.add(temp);
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return companiesData;
    }
}
