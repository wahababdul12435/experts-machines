package model;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import controller.SendMail;
import javafx.application.Platform;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FetchServerData{
    String dataTableName;
    String softwareLicId;
    String newBookingsNo;
    String newReturnsNo;
    String newDealerFinanceNo;
    ArrayList<ArrayList<String>> newBookings;
    ArrayList<ArrayList<String>> newBookingsDetail;
    ArrayList<ArrayList<String>> newReturns;
    ArrayList<ArrayList<String>> newReturnsDetail;
    ArrayList<ArrayList<String>> newDealerFinance;
    String webUrl = "https://younastraders.000webhostapp.com/ProfitFlow/ServerDataGet/GetData.php?software_lic_id="+GlobalVariables.softwareLicId+"&required_data=";

    public FetchServerData(String dataTableName)
    {
        this.dataTableName = dataTableName;
        String jsonString;
        if(dataTableName.equals("new_bookings_no"))
        {
            String fetchedOrder = fetchServerDataNo();
            if(fetchedOrder != null && !fetchedOrder.equals(""))
            {
                loadIntoListView(fetchedOrder);
            }
        }
        else if(dataTableName.equals("new_bookings"))
        {
            String fetchedOrder = fetchServerData();
            if(fetchedOrder != null && !fetchedOrder.equals(""))
            {
                loadIntoListView(fetchedOrder);
            }
        }
        else if(dataTableName.equals("new_bookings_detail"))
        {
            String fetchedOrder = fetchServerData();
            if(fetchedOrder != null && !fetchedOrder.equals(""))
            {
                loadIntoListView(fetchedOrder);
            }
        }
        else if(dataTableName.equals("new_returns"))
        {
            String fetchedOrder = fetchServerData();
            if(fetchedOrder != null && !fetchedOrder.equals(""))
            {
                loadIntoListView(fetchedOrder);
            }
        }
        else if(dataTableName.equals("new_returns_detail"))
        {
            String fetchedOrder = fetchServerData();
            if(fetchedOrder != null && !fetchedOrder.equals(""))
            {
                loadIntoListView(fetchedOrder);
            }
        }
        else if(dataTableName.equals("new_dealer_finance_no"))
        {
            String fetchedOrder = fetchServerDataNo();
            if(fetchedOrder != null && !fetchedOrder.equals(""))
            {
                loadIntoListView(fetchedOrder);
            }
        }
        else if(dataTableName.equals("new_dealer_finance"))
        {
            String fetchedOrder = fetchServerData();
            if(fetchedOrder != null && !fetchedOrder.equals(""))
            {
                loadIntoListView(fetchedOrder);
            }
        }
        else if(dataTableName.equals("new_returns_no"))
        {
            String fetchedOrder = fetchServerDataNo();
            if(fetchedOrder != null && !fetchedOrder.equals(""))
            {
                loadIntoListView(fetchedOrder);
            }
        }
    }

    public String getNewBookingsNo() {
        return newBookingsNo;
    }

    public String getNewDealerFinanceNo() {
        return newDealerFinanceNo;
    }

    public ArrayList<ArrayList<String>> getNewBookings() {
        return newBookings;
    }

    public ArrayList<ArrayList<String>> getNewBookingsDetail() {
        return newBookingsDetail;
    }

    public ArrayList<ArrayList<String>> getNewDealerFinance() {
        return newDealerFinance;
    }

    public String getNewReturnsNo() {
        return newReturnsNo;
    }

    public ArrayList<ArrayList<String>> getNewReturns() {
        return newReturns;
    }

    public ArrayList<ArrayList<String>> getNewReturnsDetail() {
        return newReturnsDetail;
    }

    private String fetchServerDataNo()
    {
        webUrl += dataTableName;
        try
        {
            URL url = new URL(webUrl);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setConnectTimeout(2 * 1000);
            con.connect();
            if (con.getResponseCode() == 200)
            {
                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String json;
                while ((json = bufferedReader.readLine()) != null)
                {
                    sb.append(json + "\n");
                }
                GlobalVariables.isConnectedToServer = true;
                if(GlobalVariables.connectionCheck)
                {
                    GlobalVariables.connectionCheck = false;
                    GlobalVariables.connectionErrorShow = true;
                    Platform.runLater(new Runnable(){
                        @Override
                        public void run() {
                            GlobalVariables.lblConnectionStatus.setText("Connected");
                            GlobalVariables.sepUsers.setVisible(true);
                            GlobalVariables.lblUsersActive.setVisible(true);
                            GlobalVariables.lblUsersActive.setManaged(true);
                            String title = "Connected";
                            String message = "Connected to Server";
                            GlobalVariables.showNotification(1, title, message);
                        }
                        // do your GUI stuff here
                    });
                }
                return sb.toString().trim();
            }
            else
            {
                GlobalVariables.isConnectedToServer = false;
                if(!GlobalVariables.connectionCheck)
                {
                    GlobalVariables.connectionCheck = true;
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            GlobalVariables.lblConnectionStatus.setText("Not Connected");
                            GlobalVariables.sepUsers.setVisible(false);
                            GlobalVariables.lblUsersActive.setVisible(false);
                            GlobalVariables.lblUsersActive.setManaged(false);
                            String title = "Not Connected";
                            String message = "Unable to Connect to Server";
                            GlobalVariables.showNotification(-1, title, message);
                        }
                        // do your GUI stuff here
                    });
                }
                return null;
            }
        }
        catch (Exception e)
        {
            GlobalVariables.isConnectedToServer = false;
            if(GlobalVariables.connectionErrorShow)
            {
                GlobalVariables.connectionErrorShow = false;
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        GlobalVariables.lblConnectionStatus.setText("Not Connected");
                        GlobalVariables.sepUsers.setVisible(false);
                        GlobalVariables.lblUsersActive.setVisible(false);
                        GlobalVariables.lblUsersActive.setManaged(false);
                        String title = "Not Connected";
                        String message = "Unable to Connect to Server";
                        GlobalVariables.showNotification(-1, title, message);
                    }
                    // do your GUI stuff here
                });
            }
            return null;
        }
    }

    private String fetchServerData()
    {
        webUrl += dataTableName;
        try
        {
            URL url = new URL(webUrl);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setConnectTimeout(2 * 1000);
            con.connect();
            if (con.getResponseCode() == 200)
            {
                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String json;
                while ((json = bufferedReader.readLine()) != null)
                {
                    sb.append(json + "\n");
                }
                GlobalVariables.isConnectedToServer = true;
                if(GlobalVariables.connectionCheck)
                {
                    GlobalVariables.connectionCheck = false;
                    Platform.runLater(new Runnable(){
                        @Override
                        public void run() {
                            GlobalVariables.lblConnectionStatus.setText("Connected");
                            GlobalVariables.sepUsers.setVisible(true);
                            GlobalVariables.lblUsersActive.setVisible(true);
                            GlobalVariables.lblUsersActive.setManaged(true);
                            String title = "Connected";
                            String message = "Connected to Server";
                            GlobalVariables.showNotification(1, title, message);
                        }
                        // do your GUI stuff here
                    });
                }
                return sb.toString().trim();
            }
            else
            {
                GlobalVariables.isConnectedToServer = false;
                if(!GlobalVariables.connectionCheck)
                {
                    GlobalVariables.connectionCheck = true;
                    Platform.runLater(new Runnable(){
                        @Override
                        public void run() {
                            GlobalVariables.lblConnectionStatus.setText("Not Connected");
                            GlobalVariables.sepUsers.setVisible(false);
                            GlobalVariables.lblUsersActive.setVisible(false);
                            GlobalVariables.lblUsersActive.setManaged(false);
                            String title = "Not Connected";
                            String message = "Unable to Connect to Server";
                            GlobalVariables.showNotification(-1, title, message);
                        }
                        // do your GUI stuff here
                    });
                }
                return null;
            }
        }
        catch (Exception e)
        {
            GlobalVariables.isConnectedToServer = false;
            Platform.runLater(new Runnable(){
                @Override
                public void run() {
                    GlobalVariables.lblConnectionStatus.setText("Not Connected");
                    GlobalVariables.sepUsers.setVisible(false);
                    GlobalVariables.lblUsersActive.setVisible(false);
                    GlobalVariables.lblUsersActive.setManaged(false);
                    String title = "Not Connected";
                    String message = "Unable to Connect to Server";
                    GlobalVariables.showNotification(-1, title, message);
                }
                // do your GUI stuff here
            });
            return null;
        }
    }

    private void loadIntoListView(String json) throws JSONException {
        JSONArray jsonArray = new JSONArray(json);
        if (dataTableName.equals("new_bookings_no")) {
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                newBookingsNo = String.valueOf(obj.getInt("new_bookings"));
            }
        }
        else if (dataTableName.equals("new_bookings")) {
            newBookings = new ArrayList<>();
            ArrayList<String> temp;
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                temp = new ArrayList<>();
                temp.add(String.valueOf(obj.getInt("order_id")));
                temp.add(String.valueOf(obj.getInt("dealer_id")));
                temp.add(obj.getString("product_id"));
                temp.add(obj.getString("quantity"));
                temp.add(obj.getString("unit"));
                temp.add(String.valueOf(obj.getFloat("order_price")));
                temp.add(obj.getString("bonus"));
                temp.add(String.valueOf(obj.getFloat("discount")));
                temp.add(String.valueOf(obj.getFloat("waive_off")));
                temp.add(obj.getString("final_price"));
                temp.add(String.valueOf(obj.getInt("order_success")));
                temp.add(obj.getString("booking_latitude"));
                temp.add(obj.getString("booking_longitude"));
                temp.add(obj.getString("booking_area"));
                temp.add(obj.getString("booking_date"));
                temp.add(obj.getString("booking_time"));
                temp.add(String.valueOf(obj.getInt("booking_user_id")));
//                temp.add(obj.getString("delivered_latitude"));
//                temp.add(obj.getString("delivered_longitude"));
//                temp.add(obj.getString("delivered_area"));
//                temp.add(obj.getString("delivered_date"));
//                temp.add(obj.getString("delivered_time"));
//                temp.add(String.valueOf(obj.getInt("delivered_user_id")));
//                temp.add(obj.getString("update_date"));
//                temp.add(obj.getString("update_time"));
//                temp.add(String.valueOf(obj.getInt("update_user_id")));
//                temp.add(obj.getString("comments"));
//                temp.add(obj.getString("status"));
                newBookings.add(temp);
            }
        }
        else if (dataTableName.equals("new_bookings_detail")) {
            newBookingsDetail = new ArrayList<>();
            ArrayList<String> temp;
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                temp = new ArrayList<>();
                temp.add(String.valueOf(obj.getInt("order_id")));
                temp.add(String.valueOf(obj.getInt("product_id")));
                temp.add(String.valueOf(obj.getInt("batch_id")));
                temp.add(String.valueOf(obj.getInt("quantity")));
                temp.add(obj.getString("unit"));
                temp.add(String.valueOf(obj.getFloat("order_price")));
                temp.add(String.valueOf(obj.getInt("bonus")));
                temp.add(String.valueOf(obj.getFloat("discount")));
                temp.add(String.valueOf(obj.getFloat("final_price")));
                newBookingsDetail.add(temp);
            }
        }
        else if (dataTableName.equals("new_dealer_finance_no")) {
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                newDealerFinanceNo = String.valueOf(obj.getInt("new_dealer_finance"));
            }
        }
        else if (dataTableName.equals("new_dealer_finance")) {
            newDealerFinance = new ArrayList<>();
            ArrayList<String> temp;
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                temp = new ArrayList<>();
                temp.add(String.valueOf(obj.getInt("payment_id")));
                temp.add(String.valueOf(obj.getInt("dealer_id")));
                temp.add(String.valueOf(obj.getInt("order_id")));
                temp.add(String.valueOf(obj.getFloat("amount")));
                temp.add(obj.getString("cash_type"));
                temp.add(obj.getString("date"));
                temp.add(obj.getString("day"));
                temp.add(String.valueOf(obj.getInt("user_id")));
                temp.add(obj.getString("status"));
                newDealerFinance.add(temp);
            }
        }
        else if (dataTableName.equals("new_returns_no")) {
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                newReturnsNo = String.valueOf(obj.getInt("new_returns_no"));
            }
        }
        else if (dataTableName.equals("new_returns")) {
            newReturns = new ArrayList<>();
            ArrayList<String> temp;
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                temp = new ArrayList<>();
                temp.add(String.valueOf(obj.getInt("return_id")));
                temp.add(String.valueOf(obj.getInt("dealer_id")));
                temp.add(obj.getString("return_total_price"));
                temp.add(obj.getString("return_gross_price"));
                temp.add(String.valueOf(obj.getInt("order_id")));
                temp.add(obj.getString("entry_date"));
                temp.add(obj.getString("entry_time"));
                temp.add(String.valueOf(obj.getInt("enter_user_id")));
                newReturns.add(temp);
            }
        }
        else if (dataTableName.equals("new_returns_detail")) {
            newReturnsDetail = new ArrayList<>();
            ArrayList<String> temp;
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                temp = new ArrayList<>();
                temp.add(String.valueOf(obj.getInt("return_id")));
                temp.add(String.valueOf(obj.getInt("product_id")));
                temp.add(obj.getString("batch_no"));
                temp.add(String.valueOf(obj.getInt("quantity")));
                temp.add(String.valueOf(obj.getInt("bonus_qty")));
                temp.add(String.valueOf(obj.getFloat("discount")));
                temp.add(String.valueOf(obj.getFloat("total_amount")));
                temp.add(obj.getString("unit"));
                temp.add(obj.getString("return_reason"));
                newReturnsDetail.add(temp);
            }
        }
    }


}
