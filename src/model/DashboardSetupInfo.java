package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DashboardSetupInfo {
    MysqlCon objMysqlCon;
    Statement objStmt;
    Connection objCon;
    
    public DashboardSetupInfo()
    {
        objMysqlCon = new MysqlCon();
        objStmt = objMysqlCon.stmt;
        objCon = objMysqlCon.con;
    }

    public ArrayList<ArrayList<String>> getCompanyGroups()
    {
        ArrayList<ArrayList<String>> chartData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            rs = objStmt.executeQuery("SELECT `company_info`.`company_name`, COUNT(`groups_info`.`company_id`) FROM `company_info` LEFT OUTER JOIN `groups_info` ON `groups_info`.`company_id` = `company_info`.`company_table_id` AND `groups_info`.`group_status` != 'Deleted' WHERE `company_info`.`company_status` != 'Deleted' GROUP BY `company_info`.`company_table_id`");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2));
                chartData.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return chartData;
    }

    public ArrayList<ArrayList<String>> getCompanyProducts()
    {
        ArrayList<ArrayList<String>> chartData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            rs = objStmt.executeQuery("SELECT `company_info`.`company_name`, COUNT(`product_info`.`company_table_id`) FROM `company_info` LEFT OUTER JOIN `product_info` ON `product_info`.`company_table_id` = `company_info`.`company_table_id` AND `product_info`.`product_status` != 'Deleted' WHERE `company_info`.`company_status` != 'Deleted' GROUP BY `company_info`.`company_table_id`");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2));
                chartData.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return chartData;
    }

    public ArrayList<ArrayList<String>> getCityDealers()
    {
        ArrayList<ArrayList<String>> chartData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            rs = objStmt.executeQuery("SELECT `city_info`.`city_name`, COUNT(`dealer_info`.`dealer_table_id`) FROM `city_info` LEFT OUTER JOIN `area_info` ON `city_info`.`city_table_id` = `area_info`.`city_table_id` AND `area_info`.`area_status` != 'Deleted' LEFT OUTER JOIN `dealer_info` ON `dealer_info`.`dealer_area_id` = `area_info`.`area_table_id` AND `dealer_info`.`dealer_status` != 'Deleted' WHERE `city_info`.`city_status` != 'Deleted' GROUP BY `city_info`.`city_table_id`");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2));
                chartData.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return chartData;
    }

    public ArrayList<ArrayList<String>> getCityAreas()
    {
        ArrayList<ArrayList<String>> chartData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            rs = objStmt.executeQuery("SELECT `city_info`.`city_name`, COUNT(`area_info`.`city_table_id`) FROM `city_info` LEFT OUTER JOIN `area_info` ON `city_info`.`city_table_id` = `area_info`.`city_table_id` AND `area_info`.`area_status` != 'Deleted' WHERE `city_info`.`city_status` != 'Deleted' GROUP BY `city_info`.`city_table_id`");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2));
                chartData.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return chartData;
    }
}
