package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import controller.CompanyFinanceDetailReport;
import controller.CompanyFinanceReport;
import controller.UpdateComment;
import controller.UpdateCompanyFinance;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class CompanyFinanceDetailReportInfo {
    private String srNo;
    private String paymentId;
    private String companyId;
    private String companyName;
    private String date;
    private String day;
    private String cashSent;
    private String cashReturn;
    private String cashWaiveOff;
    private String comment;

    private HBox operationsPane;
    private JFXButton btnComment;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    public static StackPane stackPane;
    public static JFXDialog dialog;

    public CompanyFinanceDetailReportInfo() {
        this.srNo = "";
        this.paymentId = "";
        this.companyId = "";
        this.companyName = "";
        this.date = "";
        this.day = "";
        this.cashSent = "";
        this.cashReturn = "";
        this.cashWaiveOff = "";
        this.comment = "";
    }

    public CompanyFinanceDetailReportInfo(String srNo, String paymentId, String companyId, String companyName, String date, String day, String cashSent, String cashReturn, String cashWaiveOff, String comment) {
        this.srNo = srNo;
        this.paymentId = paymentId;
        this.companyId = companyId;
        this.companyName = companyName;
        this.date = date;
        this.day = day;
        this.cashSent = cashSent;
        this.cashReturn = cashReturn;
        this.cashWaiveOff = cashWaiveOff;
        this.comment = comment;

        this.btnComment = new JFXButton();
        this.btnEdit = new JFXButton();
        this.btnDelete = new JFXButton();
        this.btnComment.setGraphic(GlobalVariables.createCommentIcon());
        this.btnEdit.setGraphic(GlobalVariables.createEditIcon());
        this.btnDelete.setGraphic(GlobalVariables.createDeleteIcon());
        this.btnComment.getStyleClass().add("btn_icon");
        this.btnEdit.getStyleClass().add("btn_icon");
        this.btnDelete.getStyleClass().add("btn_icon");
        this.btnComment.setOnAction((action)->commentClicked());
        this.btnEdit.setOnAction(event -> editClicked());
        this.btnDelete.setOnAction(event -> deleteCompanyFinance(this.paymentId));
        this.operationsPane = new HBox(btnComment, btnEdit, btnDelete);
    }

    private void commentClicked()
    {
        Parent parent = null;
        try {
            UpdateComment.commentType = "Company Finance";
            UpdateComment.orderId = this.paymentId;
            UpdateComment.comment = this.comment;
            parent = FXMLLoader.load(getClass().getResource("../view/update_comment.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setStyle("-fx-padding: -20 -10 -20 -10;");
        dialogLayout.setBody(parent);
        dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        stackPane.setVisible(true);
        dialog.show();
        dialog.setOverlayClose(false);
    }

    public void deleteCompanyFinance(String paymentId) {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        try {
            String updateQuery = "UPDATE `company_payments` SET `status`= 'Deleted' WHERE `payment_id` = '"+paymentId+"'";
            objStmt.executeUpdate(updateQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        CompanyFinanceReportInfo.companyFinanceId = companyId;
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/company_finance_detail_report.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    public void editClicked()
    {
        Parent parent = null;
        CompanyFinanceReportInfo.companyFinanceId = companyId;
        try {
            UpdateCompanyFinance.paymentId = paymentId;
            UpdateCompanyFinance.companyId = companyId;
            UpdateCompanyFinance.companyName = companyName;
            UpdateCompanyFinance.date = date;
            if(!cashSent.equals("-"))
            {
                UpdateCompanyFinance.preCashAmount = cashSent;
                UpdateCompanyFinance.cashAmount = cashSent;
                UpdateCompanyFinance.preCashType = "Sent";
                UpdateCompanyFinance.cashType = "Sent";
            }
            else if(!cashReturn.equals("-"))
            {
                UpdateCompanyFinance.preCashAmount = cashReturn;
                UpdateCompanyFinance.cashAmount = cashReturn;
                UpdateCompanyFinance.preCashType = "Return";
                UpdateCompanyFinance.cashType = "Return";
            }
            else if(!cashWaiveOff.equals("-"))
            {
                UpdateCompanyFinance.preCashAmount = cashWaiveOff;
                UpdateCompanyFinance.cashAmount = cashWaiveOff;
                UpdateCompanyFinance.preCashType = "Waive Off";
                UpdateCompanyFinance.cashType = "Waive Off";
            }

            parent = FXMLLoader.load(getClass().getResource("../view/update_company_finance.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setStyle("-fx-padding: -20 -10 -20 -10;");
        dialogLayout.setBody(parent);
        dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        stackPane.setVisible(true);
        dialog.show();
        dialog.setOverlayClose(false);
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getCashSent() {
        return cashSent;
    }

    public void setCashSent(String cashSent) {
        this.cashSent = cashSent;
    }

    public String getCashReturn() {
        return cashReturn;
    }

    public void setCashReturn(String cashReturn) {
        this.cashReturn = cashReturn;
    }

    public String getCashWaiveOff() {
        return cashWaiveOff;
    }

    public void setCashWaiveOff(String cashWaiveOff) {
        this.cashWaiveOff = cashWaiveOff;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public static StackPane getStackPane() {
        return stackPane;
    }

    public static void setStackPane(StackPane stackPane) {
        CompanyFinanceDetailReportInfo.stackPane = stackPane;
    }

    public static JFXDialog getDialog() {
        return dialog;
    }

    public static void setDialog(JFXDialog dialog) {
        CompanyFinanceDetailReportInfo.dialog = dialog;
    }

    public ArrayList<ArrayList<String>> getCompanyFinanceDetail(Statement stmt, Connection con, String companyId)
    {
        ArrayList<ArrayList<String>> companyFinanceData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT `company_payments`.`payment_id`, `company_payments`.`company_id`, `company_info`.`company_name`, `company_payments`.`date`, `company_payments`.`day`, `company_payments`.`amount`, `company_payments`.`cash_type`, `company_payments`.`comments`  FROM `company_payments` INNER JOIN `company_info` ON `company_info`.`company_table_id` = `company_payments`.`company_id` WHERE `company_payments`.`company_id` = '"+companyId+"' AND `company_payments`.`status` != 'Deleted' ORDER BY str_to_date(`company_payments`.`date`, '%d/%b/%Y') DESC");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Payment Id
                temp.add(rs.getString(2)); // Company Id
                temp.add(rs.getString(3)); // Company Name
                temp.add(rs.getString(4)); // Date
                temp.add(rs.getString(5)); // Day
                temp.add(rs.getString(6)); // Cash
                temp.add(rs.getString(7)); // Cash Type
                temp.add(rs.getString(8)); // Comments
                companyFinanceData.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return companyFinanceData;
    }

    public ArrayList<ArrayList<String>> getCompanyFinanceDetailSearch(Statement stmt, Connection con, String companyId, String fromDate, String toDate, String day, String fromAmount, String toAmount, String type)
    {
        ArrayList<ArrayList<String>> companyFinanceData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        String searchQuery;
        int multipleSearch = 0;
        searchQuery = "SELECT `company_payments`.`payment_id`, `company_payments`.`company_id`, `company_info`.`company_name`, `company_payments`.`date`, `company_payments`.`day`, `company_payments`.`amount`, `company_payments`.`cash_type`, `company_payments`.`comments` FROM `company_payments` INNER JOIN `company_info` ON `company_info`.`company_table_id` = `company_payments`.`company_id` WHERE ";
        if(!fromDate.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "str_to_date(`date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y')";
            multipleSearch++;
        }
        if(!toDate.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "str_to_date(`date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y')";
            multipleSearch++;
        }
        if(!day.equals("All"))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`company_payments`.`day` = '"+day+"'";
            multipleSearch++;
        }
        if(!fromAmount.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`company_payments`.`amount` >= '"+fromAmount+"'";
            multipleSearch++;
        }
        if(!toAmount.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`company_payments`.`amount` <= '"+toAmount+"'";
            multipleSearch++;
        }
        if(!type.equals("All"))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`company_payments`.`cash_type` = '"+type+"'";
            multipleSearch++;
        }
        if(multipleSearch >= 1)
        {
            searchQuery += " AND ";
        }
        searchQuery += "`company_payments`.`company_id` = '"+companyId+"' AND `company_payments`.`status` != 'Deleted' ORDER BY str_to_date(`company_payments`.`date`, '%d/%b/%Y') DESC";
        try {
            rs = stmt.executeQuery(searchQuery);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Payment Id
                temp.add(rs.getString(2)); // company Id
                temp.add(rs.getString(3)); // company Name
                temp.add(rs.getString(4)); // Date
                temp.add(rs.getString(5)); // Day
                temp.add(rs.getString(6)); // Cash
                temp.add(rs.getString(7)); // Cash Type
                temp.add(rs.getString(8)); // Comments
                companyFinanceData.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return companyFinanceData;
    }

    public void updateCompanyFinance(Statement stmt, Connection con, String paymentId, String companyId, String preCashAmount, String preCashType, String date, String day, String amount, String cashType)
    {
        String currentDate = GlobalVariables.getStDate();
        String currentTime = GlobalVariables.getStTime();
        String currentUser = GlobalVariables.getUserId();
        String updateQuery = null;
        try {
            if(preCashType.equals("Sent"))
            {
                updateQuery = "UPDATE `company_overall_record` SET `cash_sent`= `cash_sent` - '"+preCashAmount+"', `pending_payments` = `pending_payments` + '"+preCashAmount+"' WHERE `company_id` = '"+companyId+"'";
            }
            else if(preCashType.equals("Return"))
            {
                updateQuery = "UPDATE `company_overall_record` SET `cash_return`= `cash_return` - '"+preCashAmount+"', `pending_payments` = `pending_payments` - '"+preCashAmount+"' WHERE `company_id` = '"+companyId+"'";
            }
            else if(preCashType.equals("Waive Off"))
            {
                updateQuery = "UPDATE `company_overall_record` SET `waived_off_price`= `waived_off_price` - '"+preCashAmount+"', `pending_payments` = `pending_payments` + '"+preCashAmount+"' WHERE `company_id` = '"+companyId+"'";
            }
            stmt.executeUpdate(updateQuery);

            updateQuery = "UPDATE `company_payments` SET `date`='"+date+"', `day`='"+day+"', `amount`='"+amount+"',`cash_type`='"+cashType+"', `user_id`='"+currentUser+"' WHERE `payment_id` = '"+paymentId+"'";
            stmt.executeUpdate(updateQuery);

            if(cashType.equals("Sent"))
            {
                updateQuery = "UPDATE `company_overall_record` SET `cash_sent`= `cash_sent` + '"+amount+"', `pending_payments` = `pending_payments` - '"+amount+"' WHERE `company_id` = '"+companyId+"'";
            }
            else if(cashType.equals("Return"))
            {
                updateQuery = "UPDATE `company_overall_record` SET `cash_return`= `cash_return` + '"+amount+"', `pending_payments` = `pending_payments` + '"+amount+"' WHERE `company_id` = '"+companyId+"'";
            }
            else if(cashType.equals("Waive Off"))
            {
                updateQuery = "UPDATE `company_overall_record` SET `waived_off_price`= `waived_off_price` + '"+amount+"', `pending_payments` = `pending_payments` - '"+amount+"' WHERE `company_id` = '"+companyId+"'";
            }
            stmt.executeUpdate(updateQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateComment(Statement stmt, Connection con, String paymentId, String comment)
    {
        String currentDate = GlobalVariables.getStDate();
        String currentTime = GlobalVariables.getStTime();
        String currentUser = GlobalVariables.getUserId();
        String updateQuery = "UPDATE `company_payments` SET `comments` = '"+comment+"' WHERE `payment_id` = '"+paymentId+"'";
        try {
            stmt.executeUpdate(updateQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private Node createEditIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.EDIT);
        graphic.setFontSize(20.0);
        return graphic;
    }

    private Node createDeleteIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.TRASH);
        graphic.setFontSize(20.0);
        return graphic;
    }
}
