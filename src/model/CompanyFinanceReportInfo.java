package model;

import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.HBox;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class CompanyFinanceReportInfo {
    private String srNo;
    private String companyTableId;
    private String companyId;
    private String companyName;
    private String companyCity;
    private String companyContact;
    private String totalPurchase;
    private String cashSent;
    private String cashReturn;
    private String cashDiscount;
    private String cashWaiveOff;
    private String cashPending;

    private HBox operationsPane;
    private JFXButton btnView;
    private JFXButton btnEdit;

    public static String companyFinanceId;

    public CompanyFinanceReportInfo() {
    }

    public CompanyFinanceReportInfo(String srNo, String companyTableId, String companyId, String companyName, String companyCity, String companyContact, String totalPurchase, String cashSent, String cashReturn, String cashDiscount, String cashWaiveOff, String cashPending) {
        this.srNo = srNo;
        this.companyTableId = companyTableId;
        this.companyId = companyId;
        this.companyName = companyName;
        this.companyCity = companyCity;
        this.companyContact = companyContact;
        this.totalPurchase = totalPurchase;
        this.cashSent = cashSent;
        this.cashReturn = cashReturn;
        this.cashDiscount = cashDiscount;
        this.cashWaiveOff = cashWaiveOff;
        this.cashPending = cashPending;

        this.btnView = new JFXButton();
        this.btnView.setGraphic(createViewIcon());
        this.btnView.getStyleClass().add("btn_icon");
        this.btnEdit = new JFXButton();
        this.btnEdit.setGraphic(createEditIcon());
        this.btnEdit.getStyleClass().add("btn_icon");
        this.operationsPane = new HBox(btnView, btnEdit);

        this.btnView.setOnAction((event)->viewCompanyDetail());
        this.btnEdit.setOnAction((event)->editClicked());
    }

    public void viewCompanyDetail()
    {
        Parent parent = null;
        try {
            companyFinanceId = companyTableId;
            parent = FXMLLoader.load(getClass().getResource("../view/company_finance_detail_report.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(parent);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    public void editClicked()
    {
        Parent parent = null;
        try {
            companyFinanceId = companyTableId;
            parent = FXMLLoader.load(getClass().getResource("../view/add_company_finance.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(parent);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getCompanyTableId() {
        return companyTableId;
    }

    public void setCompanyTableId(String companyTableId) {
        this.companyTableId = companyTableId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyCity() {
        return companyCity;
    }

    public void setCompanyCity(String companyCity) {
        this.companyCity = companyCity;
    }

    public String getCompanyContact() {
        return companyContact;
    }

    public void setCompanyContact(String companyContact) {
        this.companyContact = companyContact;
    }

    public String getTotalPurchase() {
        return totalPurchase;
    }

    public void setTotalPurchase(String totalPurchase) {
        this.totalPurchase = totalPurchase;
    }

    public String getCashSent() {
        return cashSent;
    }

    public void setCashSent(String cashSent) {
        this.cashSent = cashSent;
    }

    public String getCashReturn() {
        return cashReturn;
    }

    public void setCashReturn(String cashReturn) {
        this.cashReturn = cashReturn;
    }

    public String getCashDiscount() {
        return cashDiscount;
    }

    public void setCashDiscount(String cashDiscount) {
        this.cashDiscount = cashDiscount;
    }

    public String getCashWaiveOff() {
        return cashWaiveOff;
    }

    public void setCashWaiveOff(String cashWaiveOff) {
        this.cashWaiveOff = cashWaiveOff;
    }

    public String getCashPending() {
        return cashPending;
    }

    public void setCashPending(String cashPending) {
        this.cashPending = cashPending;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public ArrayList<ArrayList<String>> getCompanyFinanceInfo(Statement stmt, Connection con)
    {
        ArrayList<ArrayList<String>> companyFinanceData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT `company_info`.`company_table_id`, `company_info`.`company_id`, `company_info`.`company_name`, `company_info`.`company_city`, `company_info`.`company_contact`, `company_overall_record`.`invoiced_price`, `company_overall_record`.`cash_sent`, `company_overall_record`.`cash_return`, `company_overall_record`.`discount_price`, `company_overall_record`.`waived_off_price`, `company_overall_record`.`pending_payments` FROM `company_info` LEFT OUTER JOIN `company_overall_record` ON `company_info`.`company_table_id` = `company_overall_record`.`company_id` WHERE `company_info`.`company_status` != 'Deleted'");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Table Id
                temp.add(rs.getString(2)); // Id
                temp.add(rs.getString(3)); // Name
                temp.add(rs.getString(4)); // City
                temp.add(rs.getString(5)); // Contact
                temp.add(rs.getString(6)); // Purchase
                temp.add(rs.getString(7)); // Cash Sent
                temp.add(rs.getString(8)); // Return
                temp.add(rs.getString(9)); // Discount
                temp.add(rs.getString(10)); // Waive Off
                temp.add(rs.getString(11)); // Pending Payment
                companyFinanceData.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return companyFinanceData;
    }

    public ArrayList<ArrayList<String>> getCompaniesFinanceSearch(Statement stmt, Connection con, String companyId, String companyName, String companyContact, String companyStatus)
    {
        ArrayList<ArrayList<String>> companyFinanceData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        String searchQuery;
        int multipleSearch = 0;
        searchQuery = "SELECT `company_info`.`company_table_id`, `company_info`.`company_id`, `company_info`.`company_name`, `company_info`.`company_city`, `company_info`.`company_contact`, `company_overall_record`.`invoiced_price`, `company_overall_record`.`cash_sent`, `company_overall_record`.`cash_return`, `company_overall_record`.`discount_price`, `company_overall_record`.`waived_off_price`, `company_overall_record`.`pending_payments` FROM `company_info` LEFT OUTER JOIN `company_overall_record` ON `company_info`.`company_table_id` = `company_overall_record`.`company_id` WHERE ";
        if(!companyId.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`company_info`.`company_id` = '"+companyId+"'";
            multipleSearch++;
        }
        if(!companyName.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`company_info`.`company_name` = '"+companyName+"'";
            multipleSearch++;
        }
        if(!companyContact.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`company_info`.`company_contact` = '"+companyContact+"'";
            multipleSearch++;
        }
        if(!companyStatus.equals("All"))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`company_info`.`company_status` = '"+companyStatus+"'";
            multipleSearch++;
        }
        if(multipleSearch >= 1)
        {
            searchQuery += " AND ";
        }
        searchQuery += "`company_info`.`company_status` != 'Deleted'";
        try {
            rs = stmt.executeQuery(searchQuery);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Table Id
                temp.add(rs.getString(2)); // Id
                temp.add(rs.getString(3)); // Name
                temp.add(rs.getString(4)); // City
                temp.add(rs.getString(5)); // Contact
                temp.add(rs.getString(6)); // Purchase
                temp.add(rs.getString(7)); // Cash Sent
                temp.add(rs.getString(8)); // Return
                temp.add(rs.getString(9)); // Discount
                temp.add(rs.getString(10)); // Waive Off
                temp.add(rs.getString(11)); // Pending Payment
                companyFinanceData.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return companyFinanceData;
    }

    public ArrayList<ArrayList<String>> getCompaniesFinanceSummary(Statement stmt, Connection con, String summaryBase)
    {
        ArrayList<ArrayList<String>> companyFinanceData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        String searchQuery;
        searchQuery = "SELECT `company_info`.`company_table_id`, `company_info`.`company_id`, `company_info`.`company_name`, `company_info`.`company_city`, `company_info`.`company_contact`, `company_overall_record`.`invoiced_price`, `company_overall_record`.`cash_sent`, `company_overall_record`.`cash_return`, `company_overall_record`.`discount_price`, `company_overall_record`.`waived_off_price`, `company_overall_record`.`pending_payments` FROM `company_info` LEFT OUTER JOIN `company_overall_record` ON `company_info`.`company_table_id` = `company_overall_record`.`company_id` WHERE `company_info`.`company_status` != 'Deleted'";
        if(!summaryBase.equals(""))
        {
            searchQuery += " AND ";
            if(summaryBase.equals("Pending"))
                searchQuery += "`company_overall_record`.`pending_payments` != '0'";
            else if(summaryBase.equals("Waive Off"))
                searchQuery += "`company_overall_record`.`waived_off_price` != '0'";
            else if(summaryBase.equals("Sale"))
                searchQuery += "`company_overall_record`.`invoiced_price` != '0'";
            else if(summaryBase.equals("Sent"))
                searchQuery += "`company_overall_record`.`cash_sent` != '0'";
            else if(summaryBase.equals("Return"))
                searchQuery += "`company_overall_record`.`cash_return` != '0'";
            else if(summaryBase.equals("Discount"))
                searchQuery += "`company_overall_record`.`discount_price` != '0'";
        }
        try {
            rs = stmt.executeQuery(searchQuery);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Table Id
                temp.add(rs.getString(2)); // Id
                temp.add(rs.getString(3)); // Name
                temp.add(rs.getString(4)); // City
                temp.add(rs.getString(5)); // Contact
                temp.add(rs.getString(6)); // Purchase
                temp.add(rs.getString(7)); // Cash Sent
                temp.add(rs.getString(8)); // Return
                temp.add(rs.getString(9)); // Discount
                temp.add(rs.getString(10)); // Waive Off
                temp.add(rs.getString(11)); // Pending Payment
                companyFinanceData.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return companyFinanceData;
    }

    private Node createEditIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.EDIT);
        graphic.setFontSize(20.0);
        return graphic;
    }

    private Node createViewIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.EYE);
        graphic.setFontSize(20.0);
        return graphic;
    }
}
