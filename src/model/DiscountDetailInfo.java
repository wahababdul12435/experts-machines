package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DiscountDetailInfo {

    private String sr_No;
    private String dealers_id;
    private String dealers_name;
    private String company_id;
    private String company_name;
    private String sale_amount;
    private String discount;

    public DiscountDetailInfo()
    {    }

    /*public DiscountDetailInfo(String sr_No, String dealers_id, String dealers_name, String company_id, String company_name, String sale_amount, String discount) {
        this.sr_No = sr_No;
        this.dealers_id = dealers_id;
        this.dealers_name = dealers_name;
        this.company_id = company_id;
        this.company_name = company_name;
        this.sale_amount = sale_amount;
        this.discount = discount;
    }

    public DiscountDetailInfo(String sr_No, String dealers_id, String dealers_name, String company_id, String company_name, String sale_amount, String discount) {
        this.sr_No = sr_No;
        this.dealers_id = dealers_id;
        this.dealers_name = dealers_name;
        this.company_id = company_id;
        this.company_name = company_name;
        this.sale_amount = sale_amount;
        this.discount = discount;
    }
*/
    public DiscountDetailInfo(String sr_No, String dealers_id, String dealers_name, String company_id, String company_name, String sale_amount, String discount) {
    this.sr_No=sr_No;
    this.dealers_id = dealers_id;
    this.dealers_name = dealers_name;
    this.company_id = company_id;
    this.company_name = company_name;
    this.sale_amount = sale_amount;
    this.discount = discount;

    }

    public String getSr_No() {
        return sr_No;
    }

    public void setSr_No(String sr_No) {
        this.sr_No = sr_No;
    }

    public String getDealers_id() {
        return dealers_id;
    }

    public void setDealers_id(String dealers_id) {
        this.dealers_id = dealers_id;
    }

    public String getDealers_name() {
        return dealers_name;
    }

    public void setDealers_name(String dealers_name) {
        this.dealers_name = dealers_name;
    }

    public String getCompany_id() {
        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getSale_amount() {
        return sale_amount;
    }

    public void setSale_amount(String sale_amount) {
        this.sale_amount = sale_amount;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }



    public ArrayList<ArrayList<String>> getDiscountPolicydetails(Statement stmt, Connection con, String policy_approvalID)
    {
        ArrayList<ArrayList<String>> discountPolicyData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        ResultSet rs1 = null;
        ResultSet rs2 = null;
        try {
            rs = stmt.executeQuery("SELECT `discount_policy`.`approval_id`, `dealer_info`.`dealer_id`, `company_info`.`company_id`, `discount_policy`.`sale_amount`, `discount_policy`.`discount_percent`, `discount_policy`.`start_date`, `discount_policy`.`end_date`, `discount_policy`.`policy_status`  FROM `discount_policy` INNER JOIN `dealer_info`  ON dealer_info.dealer_table_id = `discount_policy`.`dealer_table_id` INNER JOIN `company_info` on `discount_policy`.`company_table_id` = `company_info`.`company_table_id` WHERE policy_status != 'Deleted' and approval_id = '"+policy_approvalID+"' ");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2));
                String dealer_id = rs.getString(2);

                MysqlCon objMysqlCon = new MysqlCon();
                Statement objStmt = objMysqlCon.stmt;
                Connection objCon = objMysqlCon.con;

                rs1 = objStmt.executeQuery("SELECT dealer_name from dealer_info where dealer_id = '"+dealer_id+"'");
                while (rs1.next())
                {
                    temp.add(rs1.getString(1));
                }
                temp.add(rs.getString(3));
                String company_id = rs.getString(3);
                rs2 = objStmt.executeQuery("SELECT company_name from company_info where company_id = '"+company_id+"'");
                while (rs2.next())
                {
                    temp.add(rs2.getString(1));
                }

                temp.add(rs.getString(4));
                temp.add(rs.getString(5));
                temp.add(rs.getString(6));
                temp.add(rs.getString(7));
                temp.add(rs.getString(8));
                discountPolicyData.add(temp);
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return discountPolicyData;
    }

}
