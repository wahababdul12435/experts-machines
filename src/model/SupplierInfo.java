package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import controller.EnterSupplier;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class SupplierInfo {
    private String srNo;
    private String supplierId;
    private String supplierName;
    private String supplierContact;
    private String supplierAddress;
    private String supplierCompany;
    private String supplierEmail;
    private String contactPerson;
    private String supplierCity;

    private HBox operationsPane;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    public static ArrayList<String> supplierIdArr = new ArrayList<>();
    public static ArrayList<String> supplierNameArr = new ArrayList<>();
    public static ArrayList<String> supplierContactArr = new ArrayList<>();
    public static ArrayList<String> supplierAddressArr = new ArrayList<>();
    public static ArrayList<String> companyIdArr = new ArrayList<>();
    public static ArrayList<String> companyNameArr = new ArrayList<>();
    public static ArrayList<String> supplierEmailArr = new ArrayList<>();
    public static ArrayList<String> contactPersonArr = new ArrayList<>();
    public static ArrayList<String> supplierCityArr = new ArrayList<>();

    public static TableView<SupplierInfo> table_addsupplier;

    public static JFXTextField txtSupplierId;
    public static JFXTextField txtSupplierName;
    public static JFXTextField txtSupplierContact;
    public static JFXTextField txtSupplierAddress;
    public static JFXComboBox<String> txtCompanyName;
    public static JFXTextField txtSupplierEmail;
    public static JFXTextField txtContactPerson;
    public static JFXTextField txtSupplierCity;

    public static Button btnAdd;
    public static Button btnCancel;
    public static Button btnUpdate;

    private ArrayList<String> savedCompanyIds;
    private ArrayList<String> savedCompanyNames;

    MysqlCon objMysqlCon;
    Statement objStmt;
    Connection objCon;

    public SupplierInfo() {
        srNo = "";
        supplierId = "";
        supplierName = "";
        supplierContact = "";
        supplierAddress = "";
        supplierCompany = "";
        supplierEmail = "";
        contactPerson = "";
        supplierCity = "";

        objMysqlCon = new MysqlCon();
        objStmt = objMysqlCon.stmt;
        objCon = objMysqlCon.con;

        getCompaniesInfo(objStmt, objCon);
    }

    public SupplierInfo(String srNo, String supplierId, String supplierName, String supplierContact, String supplierAddress, String supplierCompany) {
        this.srNo = srNo;
        this.supplierId = supplierId;
        this.supplierName = supplierName;
        this.supplierContact = supplierContact;
        this.supplierAddress = supplierAddress;
        this.supplierCompany = supplierCompany;

        this.btnEdit = new JFXButton("Edit");
        this.btnDelete = new JFXButton("Delete");
        this.btnEdit.setOnAction((action)->editAddedRecord());
        this.btnDelete.setOnAction((action)->deleteAddedRecord());
        this.operationsPane = new HBox(btnEdit, btnDelete);
    }

    private void deleteAddedRecord()
    {
        supplierIdArr.remove(Integer.parseInt(srNo)-1);
        supplierNameArr.remove(Integer.parseInt(srNo)-1);
        supplierContactArr.remove(Integer.parseInt(srNo)-1);
        supplierAddressArr.remove(Integer.parseInt(srNo)-1);
        companyIdArr.remove(Integer.parseInt(srNo)-1);
        companyNameArr.remove(Integer.parseInt(srNo)-1);
        supplierEmailArr.remove(Integer.parseInt(srNo)-1);
        contactPersonArr.remove(Integer.parseInt(srNo)-1);
        supplierCityArr.remove(Integer.parseInt(srNo)-1);
        table_addsupplier.setItems(EnterSupplier.parseUserList());
    }

    public void editAddedRecord()
    {
        EnterSupplier.srNo = Integer.parseInt(srNo)-1;
        txtSupplierId.setText(supplierIdArr.get(Integer.parseInt(srNo)-1));
        txtSupplierName.setText(supplierNameArr.get(Integer.parseInt(srNo)-1));
        txtSupplierContact.setText(supplierContactArr.get(Integer.parseInt(srNo)-1));
        txtSupplierAddress.setText(supplierAddressArr.get(Integer.parseInt(srNo)-1));
        txtCompanyName.setValue(companyNameArr.get(Integer.parseInt(srNo)-1));
        txtSupplierEmail.setText(supplierEmailArr.get(Integer.parseInt(srNo)-1));
        txtContactPerson.setText(contactPersonArr.get(Integer.parseInt(srNo)-1));
        txtSupplierCity.setText(supplierCityArr.get(Integer.parseInt(srNo)-1));

        btnAdd.setVisible(false);
        btnCancel.setVisible(true);
        btnUpdate.setVisible(true);
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getSupplierContact() {
        return supplierContact;
    }

    public void setSupplierContact(String supplierContact) {
        this.supplierContact = supplierContact;
    }

    public String getSupplierAddress() {
        return supplierAddress;
    }

    public void setSupplierAddress(String supplierAddress) {
        this.supplierAddress = supplierAddress;
    }

    public String getSupplierCompany() {
        return supplierCompany;
    }

    public void setSupplierCompany(String supplierCompany) {
        this.supplierCompany = supplierCompany;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public ArrayList<String> getSavedCompanyIds() {
        return savedCompanyIds;
    }

    public void setSavedCompanyIds(ArrayList<String> savedCompanyIds) {
        this.savedCompanyIds = savedCompanyIds;
    }

    public ArrayList<String> getSavedCompanyNames() {
        return savedCompanyNames;
    }

    public void setSavedCompanyNames(ArrayList<String> savedCompanyNames) {
        this.savedCompanyNames = savedCompanyNames;
    }

    public String getSupplierEmail() {
        return supplierEmail;
    }

    public void setSupplierEmail(String supplierEmail) {
        this.supplierEmail = supplierEmail;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getSupplierCity() {
        return supplierCity;
    }

    public void setSupplierCity(String supplierCity) {
        this.supplierCity = supplierCity;
    }

    public ArrayList<String> getSupplierDetail(String supplierId)
    {
        ArrayList<String> supplierInfo = new ArrayList<>();
        ResultSet rs = null;
        try {
            rs = GlobalVariables.objStmt.executeQuery("SELECT `supplier_id`, `supplier_name`, `supplier_contact`, `creating_date`, `update_date`, `supplier_status` FROM `supplier_info` WHERE `supplier_table_id` = '"+supplierId+"'");
            while (rs.next())
            {
                supplierInfo.add(rs.getString(1)); // Id
                supplierInfo.add(rs.getString(2)); // Name
                supplierInfo.add(rs.getString(3)); // Contact
                supplierInfo.add(rs.getString(4)); // Created
                supplierInfo.add(rs.getString(5)); // Updated
                supplierInfo.add(rs.getString(6)); // Status
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return supplierInfo;
    }

    public void insertSupplier(Statement stmt, Connection con) throws SQLException {
        String currentDate = GlobalVariables.getStDate();
        String currentTime = GlobalVariables.getStTime();
        String currentUser = GlobalVariables.getUserId();
        boolean withId = false;
        boolean withoutId = false;
        String insertQuery;
        String insertSupplierCompany;
        String supplierTableId;
        int insertInfo = 1;

        for(int i=0; i<supplierIdArr.size(); i++)
        {
            if(supplierIdArr.get(i).equals("") || supplierIdArr.get(i) == null)
            {
                insertQuery = "INSERT INTO `supplier_info`(`supplier_name`, `supplier_email`, `supplier_contact`, `contact_person`, `supplier_address`, `supplier_city`, `supplier_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`) VALUES ('"+supplierNameArr.get(i)+"','"+supplierEmailArr.get(i)+"','"+supplierContactArr.get(i)+"','"+contactPersonArr.get(i)+"','"+supplierAddressArr.get(i)+"','"+supplierCityArr.get(i)+"','Active','"+currentUser+"','"+currentDate+"','"+currentTime+"','"+currentUser+"','"+currentDate+"','"+currentTime+"')";
            }
            else
            {
                insertQuery = "INSERT INTO `supplier_info`(`supplier_id`, `supplier_name`, `supplier_email`, `supplier_contact`, `contact_person`, `supplier_address`, `supplier_city`, `supplier_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`) VALUES ('"+supplierIdArr.get(i)+"','"+supplierNameArr.get(i)+"','"+supplierEmailArr.get(i)+"','"+supplierContactArr.get(i)+"','"+contactPersonArr.get(i)+"','"+supplierAddressArr.get(i)+"','"+supplierCityArr.get(i)+"','Active','"+currentUser+"','"+currentDate+"','"+currentTime+"','"+currentUser+"','"+currentDate+"','"+currentTime+"')";
            }
            if(stmt.executeUpdate(insertQuery) == -1)
            {
                insertInfo = -1;
            }
            supplierTableId = getSupplierTableId(stmt, con, supplierNameArr.get(i), supplierContactArr.get(i));
            insertSupplierCompany = "INSERT INTO `supplier_company`(`supplier_id`, `company_id`, `updated_date`, `user_id`, `status`) VALUES ('"+supplierTableId+"','"+companyIdArr.get(i)+"','"+currentDate+"','"+currentUser+"','Active')";
            if(stmt.executeUpdate(insertSupplierCompany) == -1)
            {
                insertInfo = -1;
            }
        }
        if(insertInfo == -1)
        {
            String title = "Error";
            String message = "Unable to insert";
            GlobalVariables.showNotification(-1, title, message);
        }
        else
        {
            String title = "Successfull";
            String message = "Record Inserted";
            GlobalVariables.showNotification(1, title, message);
        }
        refreshAllArrays();
    }

    private void refreshAllArrays()
    {
        supplierIdArr = new ArrayList<>();
        supplierNameArr = new ArrayList<>();
        supplierContactArr = new ArrayList<>();
        supplierAddressArr = new ArrayList<>();
        companyIdArr = new ArrayList<>();
        companyNameArr = new ArrayList<>();
        supplierEmailArr = new ArrayList<>();
        contactPersonArr = new ArrayList<>();
        supplierCityArr = new ArrayList<>();
    }

    public void updateSupplier(Statement stmt, Connection con, String supplierTableId, String supplierId, String supplierName, String supplierAddress, String supplierCity, String supplierEmail, String contactPerson, String supplierContact, ArrayList<String> companyIds, String supplierStatus)
    {
        String currentDate = GlobalVariables.getStDate();
        String currentTime = GlobalVariables.getStTime();
        String currentUser = GlobalVariables.getUserId();
        String updateQuery;
        try {
            if(supplierId.equals("") || supplierId.equals("N/A"))
            {
                updateQuery = "UPDATE `supplier_info` SET `supplier_id`= NULL, `supplier_name`='"+supplierName+"',`supplier_email`='"+supplierEmail+"',`supplier_contact`='"+supplierContact+"',`contact_person`='"+contactPerson+"',`supplier_address`='"+supplierAddress+"',`supplier_city`='"+supplierCity+"',`supplier_status`='"+supplierStatus+"',`update_user_id`='"+currentUser+"',`update_date`='"+currentDate+"',`update_time`='"+currentTime+"' WHERE `supplier_table_id` = '"+supplierTableId+"'";
            }
            else
            {
                updateQuery = "UPDATE `supplier_info` SET `supplier_id`='"+supplierId+"',`supplier_name`='"+supplierName+"',`supplier_email`='"+supplierEmail+"',`supplier_contact`='"+supplierContact+"',`contact_person`='"+contactPerson+"',`supplier_address`='"+supplierAddress+"',`supplier_city`='"+supplierCity+"',`supplier_status`='"+supplierStatus+"',`update_user_id`='"+currentUser+"',`update_date`='"+currentDate+"',`update_time`='"+currentTime+"' WHERE `supplier_table_id` = '"+supplierTableId+"'";
            }
            stmt.executeUpdate(updateQuery);
            String deleteCompany = "DELETE FROM `supplier_company` WHERE `supplier_id` = '"+supplierTableId+"'";
            stmt.executeUpdate(deleteCompany);
            String supplierCompany = "INSERT INTO `supplier_company`(`supplier_id`, `company_id`, `updated_date`, `user_id`, `status`) VALUES ";
            for(int i=0; i<companyIds.size(); i++)
            {
                if(i>0)
                {
                    supplierCompany += ", ";
                }
                supplierCompany += "('"+supplierTableId+"','"+companyIds.get(i)+"','"+currentDate+"','"+currentUser+"', '"+supplierStatus+"')";
            }
            if(companyIds.size() > 0)
            {
                stmt.executeUpdate(supplierCompany);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<ArrayList<String>> getAddedSuppliers()
    {
        ArrayList<ArrayList<String>> supplierData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        for(int i=0; i<supplierIdArr.size(); i++)
        {
            temp = new ArrayList<String>();
            temp.add(supplierIdArr.get(i));
            temp.add(supplierNameArr.get(i));
            temp.add(supplierContactArr.get(i));
            temp.add(supplierAddressArr.get(i));
            temp.add(companyNameArr.get(i));
            supplierData.add(temp);
        }
        return supplierData;
    }

    String checkSupplierId = new String();
    public String confirmNewId(String supplierId)
    {
        ResultSet rs3 = null;
        try {
            rs3 = objStmt.executeQuery("SELECT `supplier_id` FROM `supplier_info` WHERE `supplier_id` = '"+supplierId+"' AND `supplier_status` != 'Deleted'");
            while (rs3.next())
            {
                checkSupplierId = rs3.getString(1) ;
            }
//            objCon.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return checkSupplierId;
    }

    public void getCompaniesInfo(Statement stmt, Connection con)
    {
        ArrayList<String> tempIds;
        ArrayList<String> tempNames;
        tempIds = new ArrayList<String>();
        tempNames = new ArrayList<String>();
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT `company_table_id`, `company_name` FROM `company_info` WHERE `company_status` != 'Deleted' ORDER BY `company_table_id`");
            while (rs.next())
            {
                tempIds.add(rs.getString(1));
                tempNames.add(rs.getString(2)+" ("+rs.getString(1)+")");
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        setSavedCompanyIds(tempIds);
        setSavedCompanyNames(tempNames);
    }

    public ArrayList<ArrayList<String>> getSuppliersNames()
    {
        ArrayList<ArrayList<String>> supplierData = new ArrayList<>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            rs = objStmt.executeQuery("SELECT `supplier_table_id`, `supplier_name` FROM `supplier_info` WHERE `supplier_status` != 'Deleted'");
            while (rs.next())
            {
                temp = new ArrayList<>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2));
                supplierData.add(temp);
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return supplierData;
    }

    public String getSupplierTableId(Statement stmt3, Connection con3 , String supplierName, String contactNumber)
    {
        ResultSet rs3 = null;
        try {
            rs3 = stmt3.executeQuery("SELECT `supplier_table_id` FROM `supplier_info` WHERE `supplier_name` = '"+supplierName+"' AND `supplier_contact` = '"+contactNumber+"' AND `supplier_status` != 'Deleted'");
            while (rs3.next())
            {
                checkSupplierId = rs3.getString(1) ;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return checkSupplierId;
    }
}
