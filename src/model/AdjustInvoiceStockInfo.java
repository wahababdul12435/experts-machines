package model;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;

public class AdjustInvoiceStockInfo {
    private String srNo;
    private String productId;
    private String productName;
    private  String productBatch;
    private String quantity;
    private String itemsInStock;
    private String deficiency;
    private HBox operationsPane;
    private Button btnView;
    public static String productIdDetail;
    Alert a = new Alert(Alert.AlertType.NONE);
    public AdjustInvoiceStockInfo() {
        this.srNo = "0";
        this.productId = "0";
        this.productName = "";
        this.productBatch="";
        this.quantity = "0";
        this.itemsInStock = "0";
        this.deficiency = "0";
        this.operationsPane = new HBox();
    }

    public AdjustInvoiceStockInfo(String srNo, String productId, String productName, String productBatch, String quantity, String itemsInStock, String deficiency) {
        this.srNo = srNo;
        this.productId = productId;
        this.productName = productName;
        this.productBatch = productBatch;
        this.quantity = quantity;
        this.itemsInStock = itemsInStock;
        this.deficiency = deficiency;
        btnView = new Button("Details");
        btnView.setOnAction(event -> {
            try {
                goToOrderedProductDetail(productId);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        this.operationsPane = new HBox(btnView);
    }

    public void goToOrderedProductDetail(String productIdDet) throws IOException {
        productIdDetail = productIdDet;
        Parent root = FXMLLoader.load(getClass().getResource("../view/ordered_products_detail.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);

    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }
    public String getProductBatch() {
        return productBatch;
    }

    public void setProductBatch(String productName) {
        this.productBatch = productBatch;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getItemsInStock() {
        return itemsInStock;
    }

    public void setItemsInStock(String itemsInStock) {
        this.itemsInStock = itemsInStock;
    }

    public String getDeficiency() {
        return deficiency;
    }

    public void setDeficiency(String deficiency) {
        this.deficiency = deficiency;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public Button getBtnView() {
        return btnView;
    }

    public void setBtnView(Button btnView) {
        this.btnView = btnView;
    }

    ArrayList<String> DealersTable_subareaID= new ArrayList<>();
    ArrayList<String> getareasCodes = new ArrayList<>();
    ArrayList<String> getSubareasCodes = new ArrayList<>();
    ArrayList<String> areawiseSearch_SubareaID = new ArrayList<>();
    ArrayList<String> areawiseSearch_batches = new ArrayList<>();
    ArrayList<String> areawiseSearch_productIds= new ArrayList<>();
    ArrayList<String> areawiseSearch_quantities= new ArrayList<>();
    ArrayList<String> areawiseSearch_dates= new ArrayList<>();
    ArrayList<String> areawiseSearch_dealerIDs= new ArrayList<>();
    ArrayList<String> areawiseSearch_orderIDS= new ArrayList<>();
    public ArrayList<ArrayList<String>> getSummarydetails(Statement stmt, Connection con,String datefrom,String dateTo,String invoicefrom,String invoiceto,ArrayList<String> chkd_areas,String invo_status)
    {
        String getsummaryQuery = "SELECT `order_info`.`dealer_id`, `dealer_info`.`dealer_area_id`, `order_info`.`booking_date` , `order_info`.`order_id` ,`order_info_detailed`.`product_table_id`, `batchwise_stock`.`batch_no` , `order_info_detailed`.`quantity` FROM `order_info` INNER JOIN `order_info_detailed` on `order_info`.`order_id` = `order_info_detailed`.`order_id` INNER JOIN `dealer_info` on `order_info`.`dealer_id` = `dealer_info`.`dealer_table_id` inner join `batchwise_stock` ON `order_info_detailed`.`batch_id` = `batchwise_stock`.`batch_stock_id` WHERE `order_info`.`status` = '"+invo_status+"'";
        if(!invoicefrom.isEmpty() || !invoiceto.isEmpty())
        {
            getsummaryQuery += "and order_info.order_id >='"+invoicefrom+"' and order_info.order_id <='"+invoiceto+"'";
        }
        if (!datefrom.isEmpty() || !dateTo.isEmpty())
        {
            getsummaryQuery += " and str_to_date(`order_info`.`booking_date`, '%d/%b/%Y') >= str_to_date('"+datefrom+"', '%d/%b/%Y') and str_to_date(`order_info`.`booking_date`, '%d/%b/%Y') <= str_to_date('"+dateTo+"', '%d/%b/%Y')";
        }
        if(!chkd_areas.isEmpty())
        {
            ResultSet rset = null;
            for(int i =0;i<chkd_areas.size();i++)
            {
                try {
                    rset = stmt.executeQuery("select subarea_id from subarea_info where subarea_status ='Active' and sub_area_name='"+chkd_areas.get(i)+"' ");

                    while (rset.next() )
                    {
                        getSubareasCodes.add(rset.getString(1));
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }

        ArrayList<ArrayList<String>> drawingsummary_withfilters = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ArrayList<ArrayList<String>> inStockData = null;
        int k=0;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery(getsummaryQuery);
            while (rs.next())
            {
                areawiseSearch_dealerIDs.add(rs.getString(1));
                areawiseSearch_SubareaID.add(rs.getString(2));
                areawiseSearch_dates.add(rs.getString(3));
                areawiseSearch_orderIDS.add(rs.getString(4));
                areawiseSearch_productIds.add(rs.getString(5));
                areawiseSearch_batches.add(rs.getString(6));
                areawiseSearch_quantities.add(rs.getString(7));


                temp = new ArrayList<String>();
                if(chkd_areas.isEmpty())
                {
                    temp.add(areawiseSearch_dates.get(k));
                    temp.add(areawiseSearch_orderIDS.get(k));
                    temp.add(areawiseSearch_productIds.get(k));
                    temp.add(areawiseSearch_batches.get(k));
                    temp.add(areawiseSearch_quantities.get(k));
                    drawingsummary_withfilters.add(temp);
                }

                for(int i=0;i<getSubareasCodes.size();i++)
                {
                    if(( getSubareasCodes.get(i).equals(areawiseSearch_SubareaID.get(k))))
                    {
                        temp.add(areawiseSearch_dates.get(k));
                        temp.add(areawiseSearch_orderIDS.get(k));
                        temp.add(areawiseSearch_productIds.get(k));
                        temp.add(areawiseSearch_batches.get(k));
                        temp.add(areawiseSearch_quantities.get(k));
                        drawingsummary_withfilters.add(temp);
                    }
                }
                k++;
            }

            if(drawingsummary_withfilters.isEmpty())
            {
                a.setAlertType(Alert.AlertType.ERROR);
                a.setHeaderText("No Record Found");
                a.setContentText("No Invoice was Created ");

                a.show();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
//        System.out.println(drawingSummary);
        ArrayList<String> uniqueProductIds = new ArrayList<String>();
        ArrayList<String> uniquebatch = new ArrayList<String>();
        ArrayList<String> uniqueProductNames = new ArrayList<String>();
        ArrayList<String> uniqueProductIdsQuantities = new ArrayList<String>();
        for(ArrayList<String> row: drawingsummary_withfilters) {
            if(!uniquebatch.contains(row.get(3)))
            {
                uniquebatch.add(row.get(3));
                uniqueProductNames.add("");
                uniqueProductIds.add(row.get(2));
            }
        }
//        System.out.println(uniqueProductIds);
        int currQuant;
        for(String row: uniquebatch) {
            currQuant = 0;
            for(int i=0; i<drawingsummary_withfilters.size(); i++)
            {
                if(drawingsummary_withfilters.get(i).get(3).equals(row))
                {
                    currQuant += Integer.parseInt(drawingsummary_withfilters.get(i).get(4));
                }
            }
            uniqueProductIdsQuantities.add(String.valueOf(currQuant));
        }

        ArrayList<ArrayList<String>> drawingSummaryData = new ArrayList<ArrayList<String>>();
        ArrayList<ArrayList<String>> getprodDetails = new ArrayList<ArrayList<String>>();
        inStockData = new ArrayList<>();
        ResultSet rs1 = null;
        ResultSet rs2 = null;
        int productIndex;
        try {
            for(int c =0;c<uniqueProductIds.size();c++)
            {
                rs1 = stmt.executeQuery("SELECT `product_id`, `product_name` FROM `product_info` where `product_id` = '" + uniqueProductIds.get(c) + "'");
                while (rs1.next()) {
                    temp = new ArrayList<String>();
                    temp.add(rs1.getString(1));
                    temp.add(rs1.getString(2));
                    getprodDetails.add(temp);

                }


                for (int a = 0; a < getprodDetails.size(); a++) {
                    for (int b = 0; b < uniqueProductIds.size(); b++) {
                        String strngprodDetails = getprodDetails.get(a).get(0);
                        String strnguniqueID = uniqueProductIds.get(b);

                        if (strngprodDetails.equals(strnguniqueID)) {
                            uniqueProductNames.set(b, getprodDetails.get(a).get(1));
                        }

                    }
                }
                rs2 = stmt.executeQuery("SELECT `product_id`, `in_stock` FROM `products_stock` where `product_id` = '" + uniqueProductIds.get(c) + "'");

                while (rs2.next()) {
                    temp = new ArrayList<String>();
                    temp.add(rs2.getString(1));
                    temp.add(rs2.getString(2));
                    inStockData.add(temp);
                }
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
//        System.out.println(uniqueProductNames);
//        System.out.println(uniqueProductIdsQuantities);

        for (int i=0; i<uniqueProductIds.size(); i++)
        {
            temp = new ArrayList<String>();
            temp.add(uniqueProductIds.get(i));
            temp.add(uniqueProductNames.get(i));
            temp.add(uniquebatch.get(i));
            temp.add(uniqueProductIdsQuantities.get(i));
            int indexOfStock = findIndex(uniqueProductIds.get(i), inStockData);
            int dif = 0;
            if(indexOfStock >= 0)
            {
                temp.add(inStockData.get(indexOfStock).get(1));
                dif = Integer.parseInt(inStockData.get(indexOfStock).get(1)) - Integer.parseInt(uniqueProductIdsQuantities.get(i));
            }
            else
            {
                temp.add("0");
            }
            temp.add(String.valueOf(dif));
            drawingSummaryData.add(temp);
        }

        return drawingSummaryData;
    }
    public ArrayList<ArrayList<String>> getSummarybyOnlyStatus(Statement stmt, Connection con,String invo_status)
    {

        ArrayList<ArrayList<String>> drawingsummary_withfilters = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ArrayList<ArrayList<String>> inStockData = null;
        int k=0;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT `order_info`.`dealer_id`, `dealer_info`.`dealer_subarea_id`, `order_info`.`booking_date` , `order_info`.`order_id` ,`order_info_detailed`.`product_table_id`, `order_info_detailed`.`batch_number` , `order_info_detailed`.`quantity` FROM `order_info` INNER JOIN `order_info_detailed` on `order_info`.`order_id` = `order_info_detailed`.`order_id`  INNER JOIN `dealer_info` on `order_info`.`dealer_id` = `dealer_info`.`dealer_table_id`  WHERE `order_info`.`status` = '"+invo_status+"'");
            while (rs.next())
            {
                areawiseSearch_dealerIDs.add(rs.getString(1));
                areawiseSearch_SubareaID.add(rs.getString(2));
                areawiseSearch_dates.add( rs.getString(3));
                areawiseSearch_orderIDS.add(rs.getString(4));
                areawiseSearch_productIds.add(rs.getString(5));
                areawiseSearch_batches.add(rs.getString(6));
                areawiseSearch_quantities.add(rs.getString(7));

                temp = new ArrayList<String>();
                String[] splitdate = areawiseSearch_dates.get(k).split("/");
                String nsss = splitdate[0] +'-' + splitdate[1] +'-'+ splitdate[2];
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
                Date varDate=dateFormat.parse(nsss);
                dateFormat=new SimpleDateFormat("yyyy-MM-dd");
                String mydate = dateFormat.format(varDate);
                LocalDate invoicedate = LocalDate.parse(mydate);
                temp.add(areawiseSearch_dates.get(k));
                temp.add(areawiseSearch_orderIDS.get(k));
                temp.add(areawiseSearch_productIds.get(k));
                temp.add(areawiseSearch_batches.get(k));
                temp.add(areawiseSearch_quantities.get(k));
                drawingsummary_withfilters.add(temp);
                k++;
            }

            if(drawingsummary_withfilters.isEmpty())
            {
                a.setAlertType(Alert.AlertType.ERROR);
                a.setHeaderText("Error!");
                a.setContentText("No Invoice111 was Found.");

                a.show();
            }
        } catch (SQLException | ParseException e) {
            e.printStackTrace();
        }
//        System.out.println(drawingSummary);
        ArrayList<String> uniqueProductIds = new ArrayList<String>();
        ArrayList<String> uniquebatch = new ArrayList<String>();
        ArrayList<String> uniqueProductNames = new ArrayList<String>();
        ArrayList<String> uniqueProductIdsQuantities = new ArrayList<String>();
        for(ArrayList<String> row: drawingsummary_withfilters) {
            if(!uniquebatch.contains(row.get(3)))
            {
                uniquebatch.add(row.get(3));
                uniqueProductNames.add("");
                uniqueProductIds.add(row.get(2));
            }
        }
//        System.out.println(uniqueProductIds);
        int currQuant;
        for(String row: uniquebatch) {
            currQuant = 0;
            for(int i=0; i<drawingsummary_withfilters.size(); i++)
            {
                if(drawingsummary_withfilters.get(i).get(3).equals(row))
                {
                    currQuant += Integer.parseInt(drawingsummary_withfilters.get(i).get(4));
                }
            }
            uniqueProductIdsQuantities.add(String.valueOf(currQuant));
        }

        ArrayList<ArrayList<String>> drawingSummaryData = new ArrayList<ArrayList<String>>();
        ArrayList<ArrayList<String>> getprodDetails = new ArrayList<ArrayList<String>>();
        inStockData = new ArrayList<>();
        ResultSet rs1 = null;
        ResultSet rs2 = null;
        int productIndex;
        try {
            for(int c =0;c<uniqueProductIds.size();c++)
            {
                rs1 = stmt.executeQuery("SELECT `product_id`, `product_name` FROM `product_info` where `product_id` = '" + uniqueProductIds.get(c) + "'");
                while (rs1.next()) {
                    temp = new ArrayList<String>();
                    temp.add(rs1.getString(1));
                    temp.add(rs1.getString(2));
                    getprodDetails.add(temp);

                }


                for (int a = 0; a < getprodDetails.size(); a++) {
                    for (int b = 0; b < uniqueProductIds.size(); b++) {
                        String strngprodDetails = getprodDetails.get(a).get(0);
                        String strnguniqueID = uniqueProductIds.get(b);

                        if (strngprodDetails.equals(strnguniqueID)) {
                            uniqueProductNames.set(b, getprodDetails.get(a).get(1));
                        }

                    }
                }
                rs2 = stmt.executeQuery("SELECT `product_id`, `in_stock` FROM `products_stock` where `product_id` = '" + uniqueProductIds.get(c) + "'");

                while (rs2.next()) {
                    temp = new ArrayList<String>();
                    temp.add(rs2.getString(1));
                    temp.add(rs2.getString(2));
                    inStockData.add(temp);
                }
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
//        System.out.println(uniqueProductNames);
//        System.out.println(uniqueProductIdsQuantities);

        for (int i=0; i<uniqueProductIds.size(); i++)
        {
            temp = new ArrayList<String>();
            temp.add(uniqueProductIds.get(i));
            temp.add(uniqueProductNames.get(i));
            temp.add(uniquebatch.get(i));
            temp.add(uniqueProductIdsQuantities.get(i));
            int indexOfStock = findIndex(uniqueProductIds.get(i), inStockData);
            int dif = 0;
            if(indexOfStock >= 0)
            {
                temp.add(inStockData.get(indexOfStock).get(1));
                dif = Integer.parseInt(inStockData.get(indexOfStock).get(1)) - Integer.parseInt(uniqueProductIdsQuantities.get(i));
            }
            else
            {
                temp.add("0");
            }
            temp.add(String.valueOf(dif));
            drawingSummaryData.add(temp);
        }

        return drawingSummaryData;
    }
    public ArrayList<ArrayList<String>> areawiseSearch_getSummarybydates(Statement stmt, Connection con,LocalDate datefrom,LocalDate dateTo,ArrayList<String> chkd_subareas,String invo_status)
    {
        ResultSet rset = null;
        for(int i =0;i<chkd_subareas.size();i++)
        {
            try {
                rset = stmt.executeQuery("select subarea_id from subarea_info where subarea_status ='Active' and sub_area_name='"+chkd_subareas.get(i)+"' ");

                while (rset.next() )
                {
                    getSubareasCodes.add(rset.getString(1));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        ArrayList<ArrayList<String>> drawingsummary_withfilters = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ArrayList<ArrayList<String>> inStockData = null;
        int k=0;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT `order_info`.`dealer_id`, `dealer_info`.`dealer_subarea_id`, `order_info`.`booking_date` , `order_info`.`order_id` ,`order_info_detailed`.`product_table_id`, `order_info_detailed`.`batch_number` , `order_info_detailed`.`quantity` FROM `order_info` INNER JOIN `order_info_detailed` on `order_info`.`order_id` = `order_info_detailed`.`order_id`  INNER JOIN `dealer_info` on `order_info`.`dealer_id` = `dealer_info`.`dealer_table_id`  WHERE `order_info`.`status` = '"+invo_status+"'");
            while (rs.next())
            {
                areawiseSearch_dealerIDs.add(rs.getString(1));
                areawiseSearch_SubareaID.add(rs.getString(2));
                areawiseSearch_dates.add( rs.getString(3));
                areawiseSearch_orderIDS.add(rs.getString(4));
                areawiseSearch_productIds.add(rs.getString(5));
                areawiseSearch_batches.add(rs.getString(6));
                areawiseSearch_quantities.add(rs.getString(7));

                temp = new ArrayList<String>();
                String[] splitdate = areawiseSearch_dates.get(k).split("/");
                String nsss = splitdate[0] +'-' + splitdate[1] +'-'+ splitdate[2];
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
                Date varDate=dateFormat.parse(nsss);
                dateFormat=new SimpleDateFormat("yyyy-MM-dd");
                String mydate = dateFormat.format(varDate);
                LocalDate invoicedate = LocalDate.parse(mydate);
                for(int i=0;i<getSubareasCodes.size();i++)
                {
                    if((invoicedate.isEqual(datefrom) || invoicedate.isEqual(dateTo))&& getSubareasCodes.get(i).equals(areawiseSearch_SubareaID.get(k)))
                    {
                        temp.add(areawiseSearch_dates.get(k));
                        temp.add(areawiseSearch_orderIDS.get(k));
                        temp.add(areawiseSearch_productIds.get(k));
                        temp.add(areawiseSearch_batches.get(k));
                        temp.add(areawiseSearch_quantities.get(k));
                        drawingsummary_withfilters.add(temp);
                    }

                    if((invoicedate.isAfter(datefrom) && invoicedate.isBefore(dateTo))&& getSubareasCodes.get(i).equals(areawiseSearch_SubareaID.get(k)) )
                    {
                        temp.add(areawiseSearch_dates.get(k));
                        temp.add(areawiseSearch_orderIDS.get(k));
                        temp.add(areawiseSearch_productIds.get(k));
                        temp.add(areawiseSearch_batches.get(k));
                        temp.add(areawiseSearch_quantities.get(k));
                        drawingsummary_withfilters.add(temp);
                    }
                }
                k++;
            }

            if(drawingsummary_withfilters.isEmpty())
            {
                a.setAlertType(Alert.AlertType.ERROR);
                a.setHeaderText("No Record Found");
                a.setContentText("No Invoice was Created on "+dateTo+" ");

                a.show();
            }
        } catch (SQLException | ParseException e) {
            e.printStackTrace();
        }
//        System.out.println(drawingSummary);
        ArrayList<String> uniqueProductIds = new ArrayList<String>();
        ArrayList<String> uniquebatch = new ArrayList<String>();
        ArrayList<String> uniqueProductNames = new ArrayList<String>();
        ArrayList<String> uniqueProductIdsQuantities = new ArrayList<String>();
        for(ArrayList<String> row: drawingsummary_withfilters) {
            if(!uniquebatch.contains(row.get(3)))
            {
                uniquebatch.add(row.get(3));
                uniqueProductNames.add("");
                uniqueProductIds.add(row.get(2));
            }
        }
//        System.out.println(uniqueProductIds);
        int currQuant;
        for(String row: uniquebatch) {
            currQuant = 0;
            for(int i=0; i<drawingsummary_withfilters.size(); i++)
            {
                if(drawingsummary_withfilters.get(i).get(3).equals(row))
                {
                    currQuant += Integer.parseInt(drawingsummary_withfilters.get(i).get(4));
                }
            }
            uniqueProductIdsQuantities.add(String.valueOf(currQuant));
        }

        ArrayList<ArrayList<String>> drawingSummaryData = new ArrayList<ArrayList<String>>();
        ArrayList<ArrayList<String>> getprodDetails = new ArrayList<ArrayList<String>>();
        inStockData = new ArrayList<>();
        ResultSet rs1 = null;
        ResultSet rs2 = null;
        int productIndex;
        try {
            for(int c =0;c<uniqueProductIds.size();c++)
            {
                rs1 = stmt.executeQuery("SELECT `product_id`, `product_name` FROM `product_info` where `product_id` = '" + uniqueProductIds.get(c) + "'");
                while (rs1.next()) {
                    temp = new ArrayList<String>();
                    temp.add(rs1.getString(1));
                    temp.add(rs1.getString(2));
                    getprodDetails.add(temp);

                }


                for (int a = 0; a < getprodDetails.size(); a++) {
                    for (int b = 0; b < uniqueProductIds.size(); b++) {
                        String strngprodDetails = getprodDetails.get(a).get(0);
                        String strnguniqueID = uniqueProductIds.get(b);

                        if (strngprodDetails.equals(strnguniqueID)) {
                            uniqueProductNames.set(b, getprodDetails.get(a).get(1));
                        }

                    }
                }
                rs2 = stmt.executeQuery("SELECT `product_id`, `in_stock` FROM `products_stock` where `product_id` = '" + uniqueProductIds.get(c) + "'");

                while (rs2.next()) {
                    temp = new ArrayList<String>();
                    temp.add(rs2.getString(1));
                    temp.add(rs2.getString(2));
                    inStockData.add(temp);
                }
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
//        System.out.println(uniqueProductNames);
//        System.out.println(uniqueProductIdsQuantities);

        for (int i=0; i<uniqueProductIds.size(); i++)
        {
            temp = new ArrayList<String>();
            temp.add(uniqueProductIds.get(i));
            temp.add(uniqueProductNames.get(i));
            temp.add(uniquebatch.get(i));
            temp.add(uniqueProductIdsQuantities.get(i));
            int indexOfStock = findIndex(uniqueProductIds.get(i), inStockData);
            int dif = 0;
            if(indexOfStock >= 0)
            {
                temp.add(inStockData.get(indexOfStock).get(1));
                dif = Integer.parseInt(inStockData.get(indexOfStock).get(1)) - Integer.parseInt(uniqueProductIdsQuantities.get(i));
            }
            else
            {
                temp.add("0");
            }
            temp.add(String.valueOf(dif));
            drawingSummaryData.add(temp);
        }

        return drawingSummaryData;
    }
    public ArrayList<ArrayList<String>> getSummaryOnlybyArea(Statement stmt, Connection con,ArrayList<String> chkd_subareas,String invo_status)
    {
        ResultSet rset = null;
        for(int i =0;i<chkd_subareas.size();i++)
        {
            try {
                rset = stmt.executeQuery("select subarea_id from subarea_info where subarea_status ='Active' and sub_area_name='"+chkd_subareas.get(i)+"' ");

                while (rset.next() )
                {
                    getSubareasCodes.add(rset.getString(1));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        ArrayList<ArrayList<String>> drawingsummary_withfilters = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ArrayList<ArrayList<String>> inStockData = null;
        int k=0;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT `order_info`.`dealer_id`, `dealer_info`.`dealer_subarea_id`, `order_info`.`booking_date` , `order_info`.`order_id` ,`order_info_detailed`.`product_table_id`, `order_info_detailed`.`batch_number` , `order_info_detailed`.`quantity` FROM `order_info` INNER JOIN `order_info_detailed` on `order_info`.`order_id` = `order_info_detailed`.`order_id`  INNER JOIN `dealer_info` on `order_info`.`dealer_id` = `dealer_info`.`dealer_id`  WHERE `order_info`.`status` = '"+invo_status+"'");
            while (rs.next())
            {
                areawiseSearch_dealerIDs.add(rs.getString(1));
                areawiseSearch_SubareaID.add(rs.getString(2));
                areawiseSearch_dates.add( rs.getString(3));
                areawiseSearch_orderIDS.add(rs.getString(4));
                areawiseSearch_productIds.add(rs.getString(5));
                areawiseSearch_batches.add(rs.getString(6));
                areawiseSearch_quantities.add(rs.getString(7));

                temp = new ArrayList<String>();
                String[] splitdate = areawiseSearch_dates.get(k).split("/");
                String nsss = splitdate[0] +'-' + splitdate[1] +'-'+ splitdate[2];
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
                Date varDate=dateFormat.parse(nsss);
                dateFormat=new SimpleDateFormat("yyyy-MM-dd");
                String mydate = dateFormat.format(varDate);
                LocalDate invoicedate = LocalDate.parse(mydate);
                for(int i=0;i<getSubareasCodes.size();i++)
                {
                    if(( getSubareasCodes.get(i).equals(areawiseSearch_SubareaID.get(k))))
                    {
                        temp.add(areawiseSearch_dates.get(k));
                        temp.add(areawiseSearch_orderIDS.get(k));
                        temp.add(areawiseSearch_productIds.get(k));
                        temp.add(areawiseSearch_batches.get(k));
                        temp.add(areawiseSearch_quantities.get(k));
                        drawingsummary_withfilters.add(temp);
                    }

                }

                k++;
            }

            if(drawingsummary_withfilters.isEmpty())
            {
                a.setAlertType(Alert.AlertType.ERROR);
                a.setHeaderText("No Record Found");
                a.setContentText("No Invoice122 was Created.");

                a.show();
            }
        } catch (SQLException | ParseException e) {
            e.printStackTrace();
        }
//        System.out.println(drawingSummary);
        ArrayList<String> uniqueProductIds = new ArrayList<String>();
        ArrayList<String> uniquebatch = new ArrayList<String>();
        ArrayList<String> uniqueProductNames = new ArrayList<String>();
        ArrayList<String> uniqueProductIdsQuantities = new ArrayList<String>();
        for(ArrayList<String> row: drawingsummary_withfilters) {
            if(!uniquebatch.contains(row.get(3)))
            {
                uniquebatch.add(row.get(3));
                uniqueProductNames.add("");
                uniqueProductIds.add(row.get(2));
            }
        }
//        System.out.println(uniqueProductIds);
        int currQuant;
        for(String row: uniquebatch) {
            currQuant = 0;
            for(int i=0; i<drawingsummary_withfilters.size(); i++)
            {
                if(drawingsummary_withfilters.get(i).get(3).equals(row))
                {
                    currQuant += Integer.parseInt(drawingsummary_withfilters.get(i).get(4));
                }
            }
            uniqueProductIdsQuantities.add(String.valueOf(currQuant));
        }

        ArrayList<ArrayList<String>> drawingSummaryData = new ArrayList<ArrayList<String>>();
        ArrayList<ArrayList<String>> getprodDetails = new ArrayList<ArrayList<String>>();
        inStockData = new ArrayList<>();
        ResultSet rs1 = null;
        ResultSet rs2 = null;
        int productIndex;
        try {
            for(int c =0;c<uniqueProductIds.size();c++)
            {
                rs1 = stmt.executeQuery("SELECT `product_id`, `product_name` FROM `product_info` where `product_id` = '" + uniqueProductIds.get(c) + "'");
                while (rs1.next()) {
                    temp = new ArrayList<String>();
                    temp.add(rs1.getString(1));
                    temp.add(rs1.getString(2));
                    getprodDetails.add(temp);

                }


                for (int a = 0; a < getprodDetails.size(); a++) {
                    for (int b = 0; b < uniqueProductIds.size(); b++) {
                        String strngprodDetails = getprodDetails.get(a).get(0);
                        String strnguniqueID = uniqueProductIds.get(b);

                        if (strngprodDetails.equals(strnguniqueID)) {
                            uniqueProductNames.set(b, getprodDetails.get(a).get(1));
                        }

                    }
                }
                rs2 = stmt.executeQuery("SELECT `product_id`, `in_stock` FROM `products_stock` where `product_id` = '" + uniqueProductIds.get(c) + "'");

                while (rs2.next()) {
                    temp = new ArrayList<String>();
                    temp.add(rs2.getString(1));
                    temp.add(rs2.getString(2));
                    inStockData.add(temp);
                }
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
//        System.out.println(uniqueProductNames);
//        System.out.println(uniqueProductIdsQuantities);

        for (int i=0; i<uniqueProductIds.size(); i++)
        {
            temp = new ArrayList<String>();
            temp.add(uniqueProductIds.get(i));
            temp.add(uniqueProductNames.get(i));
            temp.add(uniquebatch.get(i));
            temp.add(uniqueProductIdsQuantities.get(i));
            int indexOfStock = findIndex(uniqueProductIds.get(i), inStockData);
            int dif = 0;
            if(indexOfStock >= 0)
            {
                temp.add(inStockData.get(indexOfStock).get(1));
                dif = Integer.parseInt(inStockData.get(indexOfStock).get(1)) - Integer.parseInt(uniqueProductIdsQuantities.get(i));
            }
            else
            {
                temp.add("0");
            }
            temp.add(String.valueOf(dif));
            drawingSummaryData.add(temp);
        }

        return drawingSummaryData;
    }
    public ArrayList<ArrayList<String>> getSummary_invoiceSearch(Statement stmt, Connection con,int invoicefrom,int invoiceto,String invo_status)
    {

        ArrayList<ArrayList<String>> drawingsummary_withfilters = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ArrayList<ArrayList<String>> inStockData = null;
        int k=0;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT `order_info`.`dealer_id`, `dealer_info`.`dealer_subarea_id`, `order_info`.`booking_date` , `order_info`.`order_id` ,`order_info_detailed`.`product_table_id`, `order_info_detailed`.`batch_number` , `order_info_detailed`.`quantity` FROM `order_info` INNER JOIN `order_info_detailed` on `order_info`.`order_id` = `order_info_detailed`.`order_id`  INNER JOIN `dealer_info` on `order_info`.`dealer_id` = `dealer_info`.`dealer_table_id`  WHERE `order_info`.`status` = '"+invo_status+"' and order_info.order_id >='"+invoicefrom+"' and order_info.order_id <='"+invoiceto+"' ");
            while (rs.next())
            {
                areawiseSearch_dealerIDs.add(rs.getString(1));
                areawiseSearch_SubareaID.add(rs.getString(2));
                areawiseSearch_dates.add( rs.getString(3));
                areawiseSearch_orderIDS.add(rs.getString(4));
                areawiseSearch_productIds.add(rs.getString(5));
                areawiseSearch_batches.add(rs.getString(6));
                areawiseSearch_quantities.add(rs.getString(7));

                temp = new ArrayList<String>();
                String[] splitdate = areawiseSearch_dates.get(k).split("/");
                String nsss = splitdate[0] +'-' + splitdate[1] +'-'+ splitdate[2];
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
                Date varDate=dateFormat.parse(nsss);
                dateFormat=new SimpleDateFormat("yyyy-MM-dd");
                String mydate = dateFormat.format(varDate);
                LocalDate invoicedate = LocalDate.parse(mydate);

                        temp.add(areawiseSearch_dates.get(k));
                        temp.add(areawiseSearch_orderIDS.get(k));
                        temp.add(areawiseSearch_productIds.get(k));
                        temp.add(areawiseSearch_batches.get(k));
                        temp.add(areawiseSearch_quantities.get(k));
                        drawingsummary_withfilters.add(temp);


                k++;
            }

            if(drawingsummary_withfilters.isEmpty())
            {
                a.setAlertType(Alert.AlertType.ERROR);
                a.setHeaderText("Error!");
                a.setContentText("No Invoice was Found.");

                a.show();
            }
        } catch (SQLException | ParseException e) {
            e.printStackTrace();
        }
//        System.out.println(drawingSummary);
        ArrayList<String> uniqueProductIds = new ArrayList<String>();
        ArrayList<String> uniquebatch = new ArrayList<String>();
        ArrayList<String> uniqueProductNames = new ArrayList<String>();
        ArrayList<String> uniqueProductIdsQuantities = new ArrayList<String>();
        for(ArrayList<String> row: drawingsummary_withfilters) {
            if(!uniquebatch.contains(row.get(3)))
            {
                uniquebatch.add(row.get(3));
                uniqueProductNames.add("");
                uniqueProductIds.add(row.get(2));
            }
        }
//        System.out.println(uniqueProductIds);
        int currQuant;
        for(String row: uniquebatch) {
            currQuant = 0;
            for(int i=0; i<drawingsummary_withfilters.size(); i++)
            {
                if(drawingsummary_withfilters.get(i).get(3).equals(row))
                {
                    currQuant += Integer.parseInt(drawingsummary_withfilters.get(i).get(4));
                }
            }
            uniqueProductIdsQuantities.add(String.valueOf(currQuant));
        }

        ArrayList<ArrayList<String>> drawingSummaryData = new ArrayList<ArrayList<String>>();
        ArrayList<ArrayList<String>> getprodDetails = new ArrayList<ArrayList<String>>();
        inStockData = new ArrayList<>();
        ResultSet rs1 = null;
        ResultSet rs2 = null;
        int productIndex;
        try {
            for(int c =0;c<uniqueProductIds.size();c++)
            {
                rs1 = stmt.executeQuery("SELECT `product_id`, `product_name` FROM `product_info` where `product_id` = '" + uniqueProductIds.get(c) + "'");
                while (rs1.next()) {
                    temp = new ArrayList<String>();
                    temp.add(rs1.getString(1));
                    temp.add(rs1.getString(2));
                    getprodDetails.add(temp);

                }


                for (int a = 0; a < getprodDetails.size(); a++) {
                    for (int b = 0; b < uniqueProductIds.size(); b++) {
                        String strngprodDetails = getprodDetails.get(a).get(0);
                        String strnguniqueID = uniqueProductIds.get(b);

                        if (strngprodDetails.equals(strnguniqueID)) {
                            uniqueProductNames.set(b, getprodDetails.get(a).get(1));
                        }

                    }
                }
                rs2 = stmt.executeQuery("SELECT `product_id`, `in_stock` FROM `products_stock` where `product_id` = '" + uniqueProductIds.get(c) + "'");

                while (rs2.next()) {
                    temp = new ArrayList<String>();
                    temp.add(rs2.getString(1));
                    temp.add(rs2.getString(2));
                    inStockData.add(temp);
                }
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
//        System.out.println(uniqueProductNames);
//        System.out.println(uniqueProductIdsQuantities);

        for (int i=0; i<uniqueProductIds.size(); i++)
        {
            temp = new ArrayList<String>();
            temp.add(uniqueProductIds.get(i));
            temp.add(uniqueProductNames.get(i));
            temp.add(uniquebatch.get(i));
            temp.add(uniqueProductIdsQuantities.get(i));
            int indexOfStock = findIndex(uniqueProductIds.get(i), inStockData);
            int dif = 0;
            if(indexOfStock >= 0)
            {
                temp.add(inStockData.get(indexOfStock).get(1));
                dif = Integer.parseInt(inStockData.get(indexOfStock).get(1)) - Integer.parseInt(uniqueProductIdsQuantities.get(i));
            }
            else
            {
                temp.add("0");
            }
            temp.add(String.valueOf(dif));
            drawingSummaryData.add(temp);
        }

        return drawingSummaryData;
    }
    public ArrayList<ArrayList<String>> areawiseSearch_invoiceSearch_getSummary(Statement stmt, Connection con,int invoicefrom,int invoiceto,ArrayList<String> chkd_areas,String invo_status)
    {
        ResultSet rset = null;
        for(int i =0;i<chkd_areas.size();i++)
        {
            try {
                rset = stmt.executeQuery("select subarea_id from subarea_info where subarea_status ='Active' and sub_area_name='"+chkd_areas.get(i)+"' ");
                while (rset.next() )
                {
                    getSubareasCodes.add(rset.getString(1));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        ArrayList<ArrayList<String>> drawingsummary_withfilters = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ArrayList<ArrayList<String>> inStockData = null;
        int k=0;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT `order_info`.`dealer_id`,`dealer_info`.`dealer_subarea_id`, `order_info`.`booking_date` , `order_info`.`order_id` ,`order_info_detailed`.`product_table_id`, `order_info_detailed`.`batch_number` , `order_info_detailed`.`quantity` FROM `order_info` INNER JOIN `order_info_detailed` on `order_info`.`order_id` = `order_info_detailed`.`order_id`  INNER JOIN `dealer_info` on `order_info`.`dealer_id` = `dealer_info`.`dealer_table_id`  WHERE `order_info`.`status` = '"+invo_status+"' and order_info.order_id >='"+invoicefrom+"' and order_info.order_id <='"+invoiceto+"'");
            while (rs.next())
            {
                areawiseSearch_dealerIDs.add(rs.getString(1));
                areawiseSearch_SubareaID.add(rs.getString(2));
                areawiseSearch_dates.add(rs.getString(3));
                areawiseSearch_orderIDS.add(rs.getString(4));
                areawiseSearch_productIds.add(rs.getString(5));
                areawiseSearch_batches.add(rs.getString(6));
                areawiseSearch_quantities.add(rs.getString(7));

                temp = new ArrayList<String>();
                String[] splitdate = areawiseSearch_dates.get(k).split("/");
                String nsss = splitdate[0] +'-' + splitdate[1] +'-'+ splitdate[2];
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
                Date varDate=dateFormat.parse(nsss);
                dateFormat=new SimpleDateFormat("yyyy-MM-dd");
                String mydate = dateFormat.format(varDate);
                LocalDate invoicedate = LocalDate.parse(mydate);
                for(int i=0;i<getSubareasCodes.size();i++)
                {
                    if(( getSubareasCodes.get(i).equals(areawiseSearch_SubareaID.get(k))))
                    {
                        temp.add(areawiseSearch_dates.get(k));
                        temp.add(areawiseSearch_orderIDS.get(k));
                        temp.add(areawiseSearch_productIds.get(k));
                        temp.add(areawiseSearch_batches.get(k));
                        temp.add(areawiseSearch_quantities.get(k));
                        drawingsummary_withfilters.add(temp);
                    }
                }
                k++;
            }

            if(drawingsummary_withfilters.isEmpty())
            {
                a.setAlertType(Alert.AlertType.ERROR);
                a.setHeaderText("No Record Found");
                a.setContentText("No Invoice was Created ");

                a.show();
            }
        } catch (SQLException | ParseException e) {
            e.printStackTrace();
        }
//        System.out.println(drawingSummary);
        ArrayList<String> uniqueProductIds = new ArrayList<String>();
        ArrayList<String> uniquebatch = new ArrayList<String>();
        ArrayList<String> uniqueProductNames = new ArrayList<String>();
        ArrayList<String> uniqueProductIdsQuantities = new ArrayList<String>();
        for(ArrayList<String> row: drawingsummary_withfilters) {
            if(!uniquebatch.contains(row.get(3)))
            {
                uniquebatch.add(row.get(3));
                uniqueProductNames.add("");
                uniqueProductIds.add(row.get(2));
            }
        }
//        System.out.println(uniqueProductIds);
        int currQuant;
        for(String row: uniquebatch) {
            currQuant = 0;
            for(int i=0; i<drawingsummary_withfilters.size(); i++)
            {
                if(drawingsummary_withfilters.get(i).get(3).equals(row))
                {
                    currQuant += Integer.parseInt(drawingsummary_withfilters.get(i).get(4));
                }
            }
            uniqueProductIdsQuantities.add(String.valueOf(currQuant));
        }

        ArrayList<ArrayList<String>> drawingSummaryData = new ArrayList<ArrayList<String>>();
        ArrayList<ArrayList<String>> getprodDetails = new ArrayList<ArrayList<String>>();
        inStockData = new ArrayList<>();
        ResultSet rs1 = null;
        ResultSet rs2 = null;
        int productIndex;
        try {
            for(int c =0;c<uniqueProductIds.size();c++)
            {
                rs1 = stmt.executeQuery("SELECT `product_id`, `product_name` FROM `product_info` where `product_id` = '" + uniqueProductIds.get(c) + "'");
                while (rs1.next()) {
                    temp = new ArrayList<String>();
                    temp.add(rs1.getString(1));
                    temp.add(rs1.getString(2));
                    getprodDetails.add(temp);

                }


                for (int a = 0; a < getprodDetails.size(); a++) {
                    for (int b = 0; b < uniqueProductIds.size(); b++) {
                        String strngprodDetails = getprodDetails.get(a).get(0);
                        String strnguniqueID = uniqueProductIds.get(b);

                        if (strngprodDetails.equals(strnguniqueID)) {
                            uniqueProductNames.set(b, getprodDetails.get(a).get(1));
                        }

                    }
                }
                rs2 = stmt.executeQuery("SELECT `product_id`, `in_stock` FROM `products_stock` where `product_id` = '" + uniqueProductIds.get(c) + "'");

                while (rs2.next()) {
                    temp = new ArrayList<String>();
                    temp.add(rs2.getString(1));
                    temp.add(rs2.getString(2));
                    inStockData.add(temp);
                }
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
//        System.out.println(uniqueProductNames);
//        System.out.println(uniqueProductIdsQuantities);

        for (int i=0; i<uniqueProductIds.size(); i++)
        {
            temp = new ArrayList<String>();
            temp.add(uniqueProductIds.get(i));
            temp.add(uniqueProductNames.get(i));
            temp.add(uniquebatch.get(i));
            temp.add(uniqueProductIdsQuantities.get(i));
            int indexOfStock = findIndex(uniqueProductIds.get(i), inStockData);
            int dif = 0;
            if(indexOfStock >= 0)
            {
                temp.add(inStockData.get(indexOfStock).get(1));
                dif = Integer.parseInt(inStockData.get(indexOfStock).get(1)) - Integer.parseInt(uniqueProductIdsQuantities.get(i));
            }
            else
            {
                temp.add("0");
            }
            temp.add(String.valueOf(dif));
            drawingSummaryData.add(temp);
        }

        return drawingSummaryData;
    }



    ArrayList<String> getallareas = new ArrayList<>();
    ArrayList<String> getallsubareas = new ArrayList<>();

    public ArrayList<String> getAreas(Statement stmt3, Connection con3)
    {
        ResultSet rs3 = null;
        try {
            rs3 = stmt3.executeQuery("select area_table_id,area_name from area_info where area_status ='Active'");

            while (rs3.next() )
            {

                getallareas.add(rs3.getString(1) + "--" +rs3.getString(2));

            }
            con3.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return getallareas;
    }
    public ArrayList<String> getSubAreas(Statement stmt3, Connection con3,int areasID)
    {
        ResultSet rs3 = null;
        try {
            rs3 = stmt3.executeQuery("select subarea_id,sub_area_name from subarea_info where subarea_status ='Active' and area_id='"+areasID+"' ");

            while (rs3.next() )
            {

                getallsubareas.add(rs3.getString(1) + "--" +rs3.getString(2));

            }
            con3.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return getallsubareas;
    }

  /* public String getSubAreasCode(Statement stmt3, Connection con3,String areasNames)
    {
        ResultSet rs3 = null;
        try {
            rs3 = stmt3.executeQuery("select subarea_id from subarea_info where subarea_status ='Active' and sub_area_name='"+areasNames+"' ");

            while (rs3.next() )
            {

                getsubareasCodes=(rs3.getString(1));

            }
            con3.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return getsubareasCodes;
    }*/


    /*
    public ArrayList<ArrayList<String>> getSummary(Statement stmt, Connection con,int invoFrom,int invoTo)
    {
        ArrayList<ArrayList<String>> drawingsummary_withfilters = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        String[] productIds;
        String[] quantity;
        ArrayList<ArrayList<String>> inStockData = null;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT `product_id`, `quantity` FROM `order_info` WHERE `order_id` >= '"+invoFrom+"' and `order_id` <= '"+invoTo+"'");
            while (rs.next())
            {
                productIds = rs.getString(1).split("_-_");
                if(productIds.length > 1)
                {
                    quantity = rs.getString(2).split("_-_");
                    for(int i=0; i<productIds.length; i++)
                    {
                        temp = new ArrayList<String>();
                        temp.add(productIds[i]);
                        temp.add(quantity[i]);
                        drawingsummary_withfilters.add(temp);
                    }
                }
                else
                {
                    temp = new ArrayList<String>();
                    temp.add(rs.getString(1));
                    temp.add(rs.getString(2));
                    drawingsummary_withfilters.add(temp);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
//        System.out.println(drawingSummary);
        ArrayList<String> uniqueProductIds = new ArrayList<String>();
        ArrayList<String> uniqueProductNames = new ArrayList<String>();
        ArrayList<String> uniqueProductIdsQuantities = new ArrayList<String>();
        for(ArrayList<String> row: drawingsummary_withfilters) {
            if(!uniqueProductIds.contains(row.get(0)))
            {
                uniqueProductIds.add(row.get(0));
                uniqueProductNames.add("");
            }
        }
//        System.out.println(uniqueProductIds);
        int currQuant;
        for(String row: uniqueProductIds) {
            currQuant = 0;
            for(int i=0; i<drawingsummary_withfilters.size(); i++)
            {
                if(drawingsummary_withfilters.get(i).get(0).equals(row))
                {
                    currQuant += Integer.parseInt(drawingsummary_withfilters.get(i).get(1));
                }
            }
            uniqueProductIdsQuantities.add(String.valueOf(currQuant));
        }

        ArrayList<ArrayList<String>> drawingSummaryData = new ArrayList<ArrayList<String>>();
        ResultSet rs1 = null;
        ResultSet rs2 = null;
        int productIndex;
        try {
            rs1 = stmt.executeQuery("SELECT `product_id`, `product_name` FROM `product_info`");
            while (rs1.next())
            {
                productIndex = uniqueProductIds.indexOf(rs1.getString(1));
                if(productIndex>=0)
                {
                    uniqueProductNames.set(productIndex, rs1.getString(2));
                }
            }
            rs2 = stmt.executeQuery("SELECT `product_id`, `in_stock` FROM `products_stock`");
            inStockData = new ArrayList<>();
            while (rs2.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs2.getString(1));
                temp.add(rs2.getString(2));
                inStockData.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
//        System.out.println(uniqueProductNames);
//        System.out.println(uniqueProductIdsQuantities);

        for (int i=0; i<uniqueProductIds.size(); i++)
        {
            temp = new ArrayList<String>();
            temp.add(uniqueProductIds.get(i));
            temp.add(uniqueProductNames.get(i));
            temp.add(uniqueProductIdsQuantities.get(i));
            int indexOfStock = findIndex(uniqueProductIds.get(i), inStockData);
            int dif = 0;
            if(indexOfStock >= 0)
            {
                temp.add(inStockData.get(indexOfStock).get(1));
                dif = Integer.parseInt(inStockData.get(indexOfStock).get(1)) - Integer.parseInt(uniqueProductIdsQuantities.get(i));
            }
            else
            {
                temp.add("0");
            }
            temp.add(String.valueOf(dif));
            drawingSummaryData.add(temp);
        }

        return drawingSummaryData;
    }*/

    public int findIndex(String val, ArrayList<ArrayList<String>> arr)
    {
        for (int i = 0 ; i < arr.size(); i++)
            if ( arr.get(i).get(0).equals(val))
            {
                return i;
            }
        return -1;
    }
}
