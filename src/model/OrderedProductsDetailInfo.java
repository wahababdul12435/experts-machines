package model;

import controller.OrderedProductsDetail;
import javafx.collections.FXCollections;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import org.omg.PortableInterceptor.INACTIVE;

import java.awt.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

public class OrderedProductsDetailInfo {
    private String srNo;
    private String orderId;
    private String dealerId;
    private String dealerName;
    private ComboBox<String> batchnumber;
    private String orderedQty;
    private String orderedUnit;
//    private String submissionQty;
    private TextField txtSubmissionQty;
    private ComboBox<String> comboSubmissionUnit;
//    private String submissionUnit;
    private String orgPrice;
    private String discount;
    private String finalPrice;
    private String date;
    private Button btnChange;
    private Button btnSave;
    private Button btnCancel;
    private HBox operationsPane;
    Alert a = new Alert(Alert.AlertType.NONE);

    public OrderedProductsDetailInfo() {
    }

    public OrderedProductsDetailInfo(String srNo, String orderId, String dealerId, String dealerName, String batchnumbers,String orderedQty, String orderedUnit, String submissionQty, String submissionUnit, String orgPrice, String discount, String finalPrice, String date, String txt) {
        int limit=50;
        /////////////////////POPULATE COMBOBOX OF BATCH
        ArrayList<String> batchList = new ArrayList<>();
            ResultSet rs = null;
        MysqlCon newobjMysqlConnc = new MysqlCon();
        Statement objectStattmt = newobjMysqlConnc.stmt;
        Connection objectConnnec = newobjMysqlConnc.con;
        try {
            rs = objectStattmt.executeQuery("select batch_no, quantity from batchwise_stock where prod_id ='"+OrderedProductsDetail.productIdDetail+"' and  quantity>0");
            int i = 1;
            batchList.add(0,"Batch -- Quantity");
            while (rs.next() && i < limit)
            {
                int val1 = Integer.parseInt(rs.getString(2));

                batchList.add(i,rs.getString(1) + " -- " + rs.getString(2));
                i++;
            }

            objectConnnec.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        this.srNo = srNo;
        this.orderId = orderId;
        this.dealerId = dealerId;
        this.dealerName = dealerName;
        this.batchnumber = new ComboBox<>();
        this.batchnumber.setItems(FXCollections.observableArrayList(batchList));
        this.batchnumber.getSelectionModel().select(batchnumbers);
        this.batchnumber.setDisable(true);
        this.orderedQty = orderedQty;
        this.orderedUnit = orderedUnit;
        this.txtSubmissionQty = new TextField();
        this.txtSubmissionQty.setText(submissionQty);
        this.txtSubmissionQty.setDisable(true);
        this.comboSubmissionUnit = new ComboBox<>();
        this.comboSubmissionUnit.setItems(FXCollections.observableArrayList("Packets", "Box"));
        this.comboSubmissionUnit.getSelectionModel().select(submissionUnit);
        this.comboSubmissionUnit.setDisable(true);
        this.orgPrice = orgPrice;
        this.discount = discount;
        this.finalPrice = finalPrice;
        this.date = date;
        this.btnChange = new Button(txt);
        this.btnSave = new Button("Save");
        this.btnCancel = new Button("Cancel");
        this.btnChange.setOnAction(event -> onChangeClick());
        this.batchnumber.setOnAction(event -> cnfrm_batchSelected());
        this.btnSave.setOnAction(event -> {
            try {
                saveChange(batchnumbers,submissionQty);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        });
        this.btnCancel.setOnAction(event -> cancelChange());

        this.operationsPane = new HBox(btnChange);
    }
    private void cnfrm_batchSelected()
    {
        if(this.batchnumber.getValue().equals("Batch -- Quantity"))
        {
            a.setAlertType(Alert.AlertType.ERROR);
            a.setHeaderText("Error!");
            a.setContentText("Please Select Correct Batch.");
            a.show();
            this.batchnumber.requestFocus();
        }
    }
    private void  batchnum_changed(String prevbatch,String prevSaved_quantity) {

        String[] breakbatch_N_quantity = this.batchnumber.getValue().split(" -- ");

        if (breakbatch_N_quantity[0].equals(prevbatch)) {

        }
        else
            {
            /////////////////////////////////////////////////////////////////////GET THE QUANITY OF THE SELECTED PRODUCT
            String[] PREV_batchquant = new String[1];
                String[] next_batchquant = new String[1];
                String[] prod_stockquant = new String[1];
            MysqlCon myssqlcon = new MysqlCon();
            Statement objctstatmnt = myssqlcon.stmt;
            Connection objctconnc = myssqlcon.con;
                ResultSet rs3 = null;
                try
                {
                    ///////////////////////////////////////BATCH QUANITY OF NEXT BATCH
                    rs3 = objctstatmnt.executeQuery("select quantity,bonus from batchwise_stock where prod_id = '"+ OrderedProductsDetail.productIdDetail+"' and batch_no = '"+breakbatch_N_quantity[0]+"' ");
                    int i = 0;
                    while (rs3.next() && i < 2) {
                        next_batchquant[i] = rs3.getString(1) + "--" + rs3.getString(2);
                    }

                    objctconnc.close();
                }
                catch (SQLException e) {
                    e.printStackTrace();
                }
                MysqlCon myssqlconnect = new MysqlCon();
                Statement objstatmnt = myssqlconnect.stmt;
                Connection objconnc = myssqlconnect.con;
                ResultSet rs2 = null;
                try
                {
                    ///////////////////////////////////////BATCH QUANITY OF PREVIOUS BATCH
                    rs2 = objstatmnt.executeQuery("select quantity,bonus from batchwise_stock where prod_id = '"+ OrderedProductsDetail.productIdDetail+"' and batch_no = '"+prevbatch+"' ");
                    int j = 0;
                    while (rs2.next() && j < 2) {
                        PREV_batchquant[j] = rs2.getString(1) + "--" + rs2.getString(2);
                    }

                    objconnc.close();
                }
                catch (SQLException e) {
                    e.printStackTrace();
                }
                MysqlCon myssqlconObj = new MysqlCon();
                Statement objectstatmnt = myssqlconObj.stmt;
                Connection objectconnc = myssqlconObj.con;
            ResultSet rs = null;

            try {
                /////////////////////////////////////GET QUANITY FROM STOCK TABLE OF THE SELECTED PRODUCT
                rs = objectstatmnt.executeQuery("select in_stock,bonus_quant from products_stock where product_id = '"+ OrderedProductsDetail.productIdDetail+"' and status = 'Active'");


                int k = 0;
                while (rs.next() && k < 2) {
                    prod_stockquant[k] = rs.getString(1) + "--" + rs.getString(2);
                }

                objectconnc.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            String[] split_next_batchquant = next_batchquant[0].split("--");
                String[] split_PREV_batchquant = PREV_batchquant[0].split("--");
                String[] split_prod_stockquant = prod_stockquant[0].split("--");
            int intval_newquantity = Integer.parseInt(this.txtSubmissionQty.getText());
            int intval_nextbatchDBQuant = Integer.parseInt(split_next_batchquant[0]);
               int intval_PREVbatchDBQuant = Integer.parseInt(split_PREV_batchquant[0]);
               int intval_ProdDBQuant = Integer.parseInt(split_prod_stockquant[0]);
                int intval_prevSavequant = Integer.parseInt(prevSaved_quantity);


            if(intval_nextbatchDBQuant>= intval_newquantity)
            {
                String gettradeprice = null;
                MysqlCon myssqlconObject = new MysqlCon();
                Statement objectstatment = myssqlconObject.stmt;
                Connection objectconnction = myssqlconObject.con;
                ResultSet rs4 = null;

                try {
                    /////////////////////////////////////GET QUANITY FROM STOCK TABLE OF THE SELECTED PRODUCT
                    rs4 = objectstatment.executeQuery("select trade_price from product_info where product_id = '"+ OrderedProductsDetail.productIdDetail+"' and product_status = 'Active'");

                    gettradeprice = rs4.getString(1);
                    objectconnction.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                float totalamount = intval_newquantity * Integer.parseInt(gettradeprice);
                float disc = Integer.parseInt(this.discount);
                float discAmount = (disc*totalamount)/100;
                float netamount = totalamount - discAmount;

                intval_PREVbatchDBQuant = intval_PREVbatchDBQuant+intval_prevSavequant;
                intval_nextbatchDBQuant = intval_nextbatchDBQuant-intval_newquantity;
                intval_ProdDBQuant = intval_ProdDBQuant+intval_prevSavequant;
                intval_ProdDBQuant = intval_ProdDBQuant-intval_newquantity;
            MysqlCon MysqlConn = new MysqlCon();
            Statement statmntObj = MysqlConn.stmt;
            Connection COnnOBJ = MysqlConn.con;
            try {
                statmntObj.executeUpdate("UPDATE `batchwise_stock` SET `quantity`= '" + intval_nextbatchDBQuant + "' WHERE `batch_no` = '" + breakbatch_N_quantity[0] + "' AND `prod_id` = '" + OrderedProductsDetail.productIdDetail + "'");

                COnnOBJ.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

                MysqlCon Mysqlobj = new MysqlCon();
                Statement statmentObj = Mysqlobj.stmt;
                Connection COnnectOBJ = Mysqlobj.con;
                try {
                    statmentObj.executeUpdate("UPDATE `batchwise_stock` SET `quantity`= '" + intval_PREVbatchDBQuant + "' WHERE `batch_no` = '" + prevbatch + "' AND `prod_id` = '" + OrderedProductsDetail.productIdDetail + "'");

                    COnnectOBJ.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }


                MysqlCon MysqlCon = new MysqlCon();
                Statement objectsStmt = MysqlCon.stmt;
                Connection objectsCon = MysqlCon.con;
                try {
                    objectsStmt.executeUpdate("UPDATE `products_stock` SET `in_stock`= '" + intval_ProdDBQuant + "' where `product_id` = '" + OrderedProductsDetail.productIdDetail + "'");

                    objectsCon.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }

                MysqlCon objMysqlCon = new MysqlCon();
                Statement objStmt = objMysqlCon.stmt;
                Connection objCon = objMysqlCon.con;
                try {
                    objStmt.executeUpdate("UPDATE `order_info_detailed` SET `batch_number`= '" + breakbatch_N_quantity[0] + "' , order_price = '"+totalamount+"' , final_price = '"+netamount+"' WHERE `order_id` = '" + this.orderId + "' AND `product_id` = '" + OrderedProductsDetail.productIdDetail + "'");

                    objCon.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            else
            {
                a.setAlertType(Alert.AlertType.ERROR);
                a.setHeaderText("Error!");
                a.setContentText("Not Enough Quantity in Stock.");
                a.show();
                this.txtSubmissionQty.requestFocus();
            }
        }


    }
    private void onChangeClick()
    {
        this.txtSubmissionQty.setDisable(false);
        this.comboSubmissionUnit.setDisable(false);
        this.batchnumber.setDisable(false);
        this.operationsPane.getChildren().clear();
        this.operationsPane.getChildren().addAll(btnSave, btnCancel);
        this.operationsPane.setSpacing(10);
    }

    private void cancelChange()
    {
        this.txtSubmissionQty.setDisable(true);
        this.comboSubmissionUnit.setDisable(true);
        this.batchnumber.setDisable(true);
        this.operationsPane.getChildren().clear();
        this.operationsPane.getChildren().addAll(btnChange);
    }

    private void saveChange(String Batchs, String savedquantity) throws ParseException {
        if(!this.batchnumber.getValue().equals("Batch -- Quantity"))
        {
        batchnum_changed(Batchs,savedquantity);
            String[] batchquants = new String[1];
            String[] prod_stockquant = new String[1];
            MysqlCon myssqlcon = new MysqlCon();
            Statement objctstatmnt = myssqlcon.stmt;
            Connection objctconnc = myssqlcon.con;
            ResultSet rs3 = null;
            try
            {
                ///////////////////////////////////////BATCH QUANITY OF NEXT BATCH
                rs3 = objctstatmnt.executeQuery("select quantity,bonus from batchwise_stock where prod_id = '"+ OrderedProductsDetail.productIdDetail+"' and batch_no = '"+Batchs+"' ");
                int i = 0;
                while (rs3.next() && i < 2) {
                    batchquants[i] = rs3.getString(1) + "--" + rs3.getString(2);
                }

                objctconnc.close();
            }
            catch (SQLException e) {
                e.printStackTrace();
            }

            MysqlCon myssqlconObj = new MysqlCon();
            Statement objectstatmnt = myssqlconObj.stmt;
            Connection objectconnc = myssqlconObj.con;
            ResultSet rs = null;

            try {
                /////////////////////////////////////GET QUANITY FROM STOCK TABLE OF THE SELECTED PRODUCT
                rs = objectstatmnt.executeQuery("select in_stock,bonus_quant from products_stock where product_id = '"+ OrderedProductsDetail.productIdDetail+"' and status = 'Active'");


                int k = 0;
                while (rs.next() && k < 2) {
                    prod_stockquant[k] = rs.getString(1) + "--" + rs.getString(2);
                }

                objectconnc.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

            String[] split_PREV_batchquant = batchquants[0].split("--");
            String[] split_prod_stockquant = prod_stockquant[0].split("--");
            int intval_newquantity = Integer.parseInt(this.txtSubmissionQty.getText());
            int intval_batchDBQuant = Integer.parseInt(split_PREV_batchquant[0]);
            int intval_ProdDBQuant = Integer.parseInt(split_prod_stockquant[0]);
            int intval_prevSavequant = Integer.parseInt(savedquantity);


            if(intval_batchDBQuant>= intval_newquantity)
            {
                String gettradeprice = null;
                int intprodID_val = Integer.parseInt(OrderedProductsDetail.productIdDetail);
                MysqlCon myssqlconObject = new MysqlCon();
                Statement objectstatment = myssqlconObject.stmt;
                Connection objectconnction = myssqlconObject.con;
                ResultSet rs4 = null;

                try {
                    /////////////////////////////////////GET QUANITY FROM STOCK TABLE OF THE SELECTED PRODUCT
                    rs4 = objectstatment.executeQuery("select `trade_price` from `batchwise_stock` where `prod_id` = '"+intprodID_val+"' and `batch_no` = '"+Batchs+"' ");
                    while (rs4.next())
                    {
                        gettradeprice = rs4.getString(1);
                    }
                    objectconnction.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                float totalamount = intval_newquantity * Float.parseFloat(gettradeprice);
                float disc = Integer.parseInt(this.discount);
                float discAmount = (disc*totalamount)/100;
                float netamount = totalamount - discAmount;

                intval_batchDBQuant = intval_batchDBQuant + intval_prevSavequant;
            intval_ProdDBQuant = intval_ProdDBQuant + intval_prevSavequant;
            intval_batchDBQuant = intval_batchDBQuant - intval_newquantity;
            intval_ProdDBQuant = intval_ProdDBQuant - intval_newquantity;


                MysqlCon Mysqlobj = new MysqlCon();
                Statement statmentObj = Mysqlobj.stmt;
                Connection COnnectOBJ = Mysqlobj.con;
                try {
                    statmentObj.executeUpdate("UPDATE `batchwise_stock` SET `quantity`= '" + intval_batchDBQuant + "' WHERE `batch_no` = '" + Batchs + "' AND `prod_id` = '" + OrderedProductsDetail.productIdDetail + "'");

                    COnnectOBJ.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }


                MysqlCon MysqlCon = new MysqlCon();
                Statement objectsStmt = MysqlCon.stmt;
                Connection objectsCon = MysqlCon.con;
                try {
                    objectsStmt.executeUpdate("UPDATE `products_stock` SET `in_stock`= '" + intval_ProdDBQuant + "' where `product_id` = '" + OrderedProductsDetail.productIdDetail + "'");

                    objectsCon.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }

        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        ArrayList<String> temp;
        try {
            objStmt.executeUpdate("UPDATE `order_info_detailed` SET `submission_quantity`= '" + this.txtSubmissionQty.getText() + "', `submission_unit`='" + this.comboSubmissionUnit.getValue() + "' WHERE `order_id` = '" + this.orderId + "' AND `product_id` = '" + OrderedProductsDetail.productIdDetail + "'");
            try {
                goToOrderedProductsDetail();
            } catch (IOException e) {
                e.printStackTrace();
            }
            objCon.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

                LocalDate update_date = LocalDate.now();
                String updatedDates = update_date.format(DateTimeFormatter.ofPattern("dd/MMM/yyyy"));
                LocalTime getupdate_time = LocalTime.now();
                String updated_time = getupdate_time.format(DateTimeFormatter.ofPattern("hh:mm a"));


                MysqlCon updateobjMysqlCon = new MysqlCon();
                Statement updateobjStmt = updateobjMysqlCon.stmt;
                Connection udateobjCon = updateobjMysqlCon.con;

                try {
                    updateobjStmt.executeUpdate("UPDATE `order_info` SET `update_dates`= '" + updatedDates + "', `update_times`='" + updated_time + "' WHERE `order_id` = '" + this.orderId + "' ");
                    try {
                        goToOrderedProductsDetail();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    udateobjCon.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
    }}
        else
        {
            a.setAlertType(Alert.AlertType.ERROR);
            a.setHeaderText("Error!");
            a.setContentText("Please Select Correct Batch.");
            a.show();
            this.batchnumber.requestFocus();
        }

    }

    public void goToOrderedProductsDetail() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/ordered_products_detail.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getDealerId() {
        return dealerId;
    }

    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public ComboBox<String> getBatchnumber() {
        return batchnumber;
    }

    public void setBatchnumber(ComboBox<String> batchnumber) {
        this.batchnumber = batchnumber;
    }

    public String getOrderedQty() {
        return orderedQty;
    }

    public void setOrderedQty(String orderedQty) {
        this.orderedQty = orderedQty;
    }

    public String getOrderedUnit() {
        return orderedUnit;
    }

    public void setOrderedUnit(String orderedUnit) {
        this.orderedUnit = orderedUnit;
    }

    public TextField getTxtSubmissionQty() {
        return txtSubmissionQty;
    }

    public void setTxtSubmissionQty(TextField txtSubmissionQty) {
        this.txtSubmissionQty = txtSubmissionQty;
    }

    public ComboBox<String> getComboSubmissionUnit() {
        return comboSubmissionUnit;
    }

    public void setComboSubmissionUnit(ComboBox<String> comboSubmissionUnit) {
        this.comboSubmissionUnit = comboSubmissionUnit;
    }

    //    public String getSubmissionQty() {
//        return submissionQty;
//    }
//
//    public void setSubmissionQty(String submissionQty) {
//        this.submissionQty = submissionQty;
//    }

//    public String getSubmissionUnit() {
//        return submissionUnit;
//    }
//
//    public void setSubmissionUnit(String submissionUnit) {
//        this.submissionUnit = submissionUnit;
//    }

    public String getOrgPrice() {
        return orgPrice;
    }

    public void setOrgPrice(String orgPrice) {
        this.orgPrice = orgPrice;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(String finalPrice) {
        this.finalPrice = finalPrice;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Button getBtnChange() {
        return btnChange;
    }

    public void setBtnChange(Button btnChange) {
        this.btnChange = btnChange;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public Button getBtnSave() {
        return btnSave;
    }

    public void setBtnSave(Button btnSave) {
        this.btnSave = btnSave;
    }

    public Button getBtnCancel() {
        return btnCancel;
    }

    public void setBtnCancel(Button btnCancel) {
        this.btnCancel = btnCancel;
    }



    public ArrayList<ArrayList<String>> getOrderedProductDetail(Statement stmt, Connection con, String productId)
    {
        ArrayList<ArrayList<String>> orderedProductDetail = new ArrayList<ArrayList<String>>();
        ResultSet rs = null;
        ArrayList<String> temp;
        try {
            rs = stmt.executeQuery("SELECT `order_info`.`order_id`, `order_info`.`dealer_id`, `dealer_info`.`dealer_name`,`batchwise_stock`.`batch_no`, `order_info_detailed`.`quantity`, `order_info_detailed`.`unit`, `order_info_detailed`.`submission_quantity`, `order_info_detailed`.`submission_unit`, `order_info_detailed`.`order_price`, `order_info_detailed`.`discount`, `order_info_detailed`.`final_price`, `order_info`.`booking_date` from `order_info` INNER JOIN `order_info_detailed` ON `order_info_detailed`.`order_id` = `order_info`.`order_id` INNER JOIN `dealer_info` ON `order_info`.`dealer_id` = `dealer_info`.`dealer_id` INNER JOIN `product_info` ON `product_info`.`product_id` = `order_info_detailed`.`product_table_id` inner join `batchwise_stock` ON `order_info_detailed`.`batch_id` = `batchwise_stock`.`batch_stock_id` WHERE `order_info`.`status` = 'Pending' AND `order_info_detailed`.`product_table_id` = '"+productId+"'");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2));
                temp.add(rs.getString(3));
                temp.add(rs.getString(4));
                temp.add(rs.getString(5));
                temp.add(rs.getString(6));
                temp.add(rs.getString(7));
                temp.add(rs.getString(8));
                temp.add(rs.getString(9));
                temp.add(rs.getString(10));
                temp.add(rs.getString(11));
                temp.add(rs.getString(12));
                orderedProductDetail.add(temp);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Collections.sort(orderedProductDetail, new Comparator<ArrayList<String>>() {
            @Override
            public int compare(ArrayList<String> o1, ArrayList<String> o2) {
                return o1.get(0).compareTo(o2.get(0));
            }
        });
        return orderedProductDetail;
    }
}
