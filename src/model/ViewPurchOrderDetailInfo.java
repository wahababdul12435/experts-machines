package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import controller.ViewPurchOrderDetail;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class ViewPurchOrderDetailInfo {
    private String srNo;
    private String productID;
    private String productName;
    private String cartonSize;
    private String soldQauntity;
    private String quantityInstock;
    private String orderQauntity;
    private String purchPrice;
    private String netAmount;

    public static Button btnAdd;
    public static Button btnCancel;
    public static Button btnUpdate;

    public ViewPurchOrderDetailInfo()
    {
        this.srNo = "";
        this.productID = "";
        this.productName = "";
        this.cartonSize = "";
        this.soldQauntity = "";
        this.quantityInstock = "";
        this.orderQauntity = "";
        this.purchPrice = "";
        this.netAmount = "";

    }
    public ViewPurchOrderDetailInfo(String srNo, String productID, String productName,String cartonSize, String soldQauntity , String quantityInstock,String orderQauntity,String purchPrice,String netAmount) {
        this.srNo = srNo;
        this.productID = productID;
        this.productName = productName;
        this.cartonSize = cartonSize;
        this.soldQauntity = soldQauntity;
        this.quantityInstock = quantityInstock;
        this.orderQauntity = orderQauntity;
        this.purchPrice = purchPrice;
        this.netAmount = netAmount;

    }

    public ArrayList<ArrayList<String>> getOrderData(Statement stmt, Connection con, String ordersID)
    {
        ArrayList<ArrayList<String>> orderData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        ResultSet rs1 = null;
        try {
            rs = stmt.executeQuery("SELECT `product_info`.`product_id`,product_info.product_name,product_info.carton_size, purchase_order_detail.quantity_sold, purchase_order_detail.quantity_instock, purchase_order_detail.quantity_ordered ,product_info.purchase_price FROM `product_info`  inner JOIN `purchase_order_detail` ON purchase_order_detail.product_table_id = `product_info`.`product_table_id`  where purchase_order_detail.purch_order_id = '"+ordersID+"'");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1));//product id
                temp.add(rs.getString(2));//product name
                temp.add(rs.getString(3));//carton size
                String soldQuantity = rs.getString(4);//sold quantity
                if(soldQuantity == null)
                {
                    soldQuantity = "0";
                }
                temp.add(soldQuantity);
                String instockQuant = rs.getString(5);//in stock
                if(instockQuant==null)
                {
                    instockQuant = "0";
                }
                temp.add(instockQuant);
                int purch_orderQuantity = Integer.parseInt(rs.getString(6));
                String str_purchOrderQuant = String.valueOf(purch_orderQuantity);
                temp.add(str_purchOrderQuant);//quantity ordered
                temp.add(rs.getString(7));//purchase price
                float price = Float.parseFloat(rs.getString(6));
                float netAmounts = price * purch_orderQuantity;
                String strngNetAmount = String.valueOf(netAmounts);
                temp.add(strngNetAmount);//net amount
                orderData.add(temp);
            }
            /*if(orderData.isEmpty())
            {
                ResultSet result = null;
                try {
                    result = stmt.executeQuery("SELECT  `product_info`.product_id, product_info.product_name, product_info.carton_size, products_stock.in_stock,product_info.purchase_price  FROM   product_info INNER JOIN products_stock ON products_stock.product_table_id = product_info.product_table_id where  product_info.company_table_id = '" + int_compID + "'    ");
                    while (result.next()) {
                        temp = new ArrayList<String>();
                        temp.add(result.getString(1));//product id
                        temp.add(result.getString(2));//product name
                        temp.add(result.getString(3));//carton size
                        temp.add("0");//sold quantity
                        String in_stock = result.getString(4);//in stock
                        if(in_stock == null)
                        {
                            in_stock = "0";
                        }
                        int int_valueInstock = Integer.parseInt(in_stock);
                        float netAmounts = 0.0f;
                        if(int_valueInstock == 0)
                        {
                            temp.add(result.getString(3));
                        }
                        else {
                            temp.add("0");//ordered quantity
                        }
                        int intValue_cartonsize = Integer.parseInt(result.getString(3));

                        temp.add(result.getString(5));//purchase price
                        float price = Float.parseFloat(result.getString(5));

                        if(int_valueInstock == 0)
                        {
                            netAmounts =  intValue_cartonsize * price;
                        }
                        else
                        {
                            netAmounts = price * 0;
                        }
                        String strngNetAmount = String.valueOf(netAmounts);
                        temp.add(strngNetAmount);//net amount
                        orderData.add(temp);
                    }
                }
                catch (SQLException e) {
                    e.printStackTrace();
                }
            }*/

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return orderData;
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getProductID() {
        return productID;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getCartonSize() {
        return cartonSize;
    }

    public void setCartonSize(String cartonSize) {
        this.cartonSize = cartonSize;
    }

    public String getSoldQauntity() {
        return soldQauntity;
    }

    public void setSoldQauntity(String soldQauntity) {
        this.soldQauntity = soldQauntity;
    }

    public String getQuantityInstock() {
        return quantityInstock;
    }

    public void setQuantityInstock(String quantityInstock) {
        this.quantityInstock = quantityInstock;
    }

    public String getOrderQauntity() {
        return orderQauntity;
    }

    public void setOrderQauntity(String orderQauntity) {
        this.orderQauntity = orderQauntity;
    }

    public String getPurchPrice() {
        return purchPrice;
    }

    public void setPurchPrice(String purchPrice) {
        this.purchPrice = purchPrice;
    }

    public String getNetAmount() {
        return netAmount;
    }

    public void setNetAmount(String netAmount) {
        this.netAmount = netAmount;
    }

    private Node createEditIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.EDIT);
        graphic.setFontSize(20.0);
        return graphic;
    }

    private Node createDeleteIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.TRASH);
        graphic.setFontSize(20.0);
        return graphic;
    }
}
