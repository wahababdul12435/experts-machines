package model;

import com.jfoenix.controls.JFXButton;
import javafx.scene.Node;
import javafx.scene.layout.HBox;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

public class ProductReportDetailInfo {
    private String srNo;
    private String date;
    private String day;
    private String purchaseQty;
    private String purchaseAmount;
    private String purchaseReturn;
    private String purchaseReturnAmount;
    private String saleQty;
    private String saleAmount;
    private String saleReturn;
    private String saleReturnAmount;

    private HBox operationsPane;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    SimpleDateFormat fmt = new SimpleDateFormat("dd/MMM/yyyy");

    public ProductReportDetailInfo() {
    }

    public ProductReportDetailInfo(String srNo, String date, String day, String purchaseQty, String purchaseAmount, String purchaseReturn, String purchaseReturnAmount, String saleQty, String saleAmount, String saleReturn, String saleReturnAmount) {
        this.srNo = srNo;
        this.date = date;
        this.day = day;
        this.purchaseQty = purchaseQty;
        this.purchaseAmount = purchaseAmount;
        this.purchaseReturn = purchaseReturn;
        this.purchaseReturnAmount = purchaseReturnAmount;
        this.saleQty = saleQty;
        this.saleAmount = saleAmount;
        this.saleReturn = saleReturn;
        this.saleReturnAmount = saleReturnAmount;

        this.btnEdit = new JFXButton();
        this.btnDelete = new JFXButton();
        this.btnEdit.setGraphic(createEditIcon());
        this.btnDelete.setGraphic(createDeleteIcon());
        this.btnEdit.getStyleClass().add("btn_icon");
        this.btnDelete.getStyleClass().add("btn_icon");
        this.operationsPane = new HBox(btnEdit, btnDelete);
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getPurchaseQty() {
        return purchaseQty;
    }

    public void setPurchaseQty(String purchaseQty) {
        this.purchaseQty = purchaseQty;
    }

    public String getPurchaseAmount() {
        return purchaseAmount;
    }

    public void setPurchaseAmount(String purchaseAmount) {
        this.purchaseAmount = purchaseAmount;
    }

    public String getPurchaseReturn() {
        return purchaseReturn;
    }

    public void setPurchaseReturn(String purchaseReturn) {
        this.purchaseReturn = purchaseReturn;
    }

    public String getPurchaseReturnAmount() {
        return purchaseReturnAmount;
    }

    public void setPurchaseReturnAmount(String purchaseReturnAmount) {
        this.purchaseReturnAmount = purchaseReturnAmount;
    }

    public String getSaleQty() {
        return saleQty;
    }

    public void setSaleQty(String saleQty) {
        this.saleQty = saleQty;
    }

    public String getSaleAmount() {
        return saleAmount;
    }

    public void setSaleAmount(String saleAmount) {
        this.saleAmount = saleAmount;
    }

    public String getSaleReturn() {
        return saleReturn;
    }

    public void setSaleReturn(String saleReturn) {
        this.saleReturn = saleReturn;
    }

    public String getSaleReturnAmount() {
        return saleReturnAmount;
    }

    public void setSaleReturnAmount(String saleReturnAmount) {
        this.saleReturnAmount = saleReturnAmount;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public ArrayList<ArrayList<String>> getDealerReportDetail(Statement stmt1, Statement stmt2, Statement stmt3, Connection con, String productId) throws ParseException {
        ArrayList<ArrayList<String>> productReportData1 = new ArrayList<ArrayList<String>>();
        ArrayList<ArrayList<String>> productReportData2 = new ArrayList<ArrayList<String>>();
        ArrayList<ArrayList<String>> productReportData3 = new ArrayList<ArrayList<String>>();
        ArrayList<ArrayList<String>> productReportData4 = new ArrayList<ArrayList<String>>();
        ArrayList<ArrayList<String>> productReportData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs1 = null;
        ResultSet rs2 = null;
        ResultSet rs3 = null;
        try {
            rs1 = stmt1.executeQuery("SELECT `purchase_info`.`purchase_date`, `purchase_info_detail`.`recieve_quant`, `purchase_info_detail`.`net_amount` FROM `purchase_info_detail` INNER JOIN `purchase_info` ON `purchase_info`.`purchase_id` = `purchase_info_detail`.`purchase_id` WHERE `purchase_info_detail`.`prod_id` = '"+productId+"' ORDER BY str_to_date(`purchase_info`.`purchase_date`, '%d/%b/%Y') DESC");
            while (rs1.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs1.getString(1)); // Purchase Date
                temp.add(rs1.getString(2)); // Purchase Qty
                temp.add(rs1.getString(3)); // Purchase Amount
                productReportData1.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            rs2 = stmt2.executeQuery("SELECT `purchase_return`.`entry_date`, `purchase_return_detail`.`prod_quant`, `purchase_return_detail`.`total_amount` FROM `purchase_return_detail` INNER JOIN `purchase_return` ON `purchase_return`.`return_id` = `purchase_return_detail`.`return_id` WHERE `purchase_return_detail`.`prod_id` = '"+productId+"' ORDER BY str_to_date(`purchase_return`.`entry_date`, '%d/%b/%Y') DESC");
            while (rs2.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs2.getString(1)); // Purchase Return Date
                temp.add(rs2.getString(2)); // Purchase Return Quantity
                temp.add(rs2.getString(3)); // Purchase Return Amount
                productReportData2.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            rs3 = stmt3.executeQuery("SELECT `order_info`.`booking_date`, `order_info_detailed`.`submission_quantity`, `order_info_detailed`.`final_price` FROM `order_info_detailed` INNER JOIN `order_info` ON `order_info`.`order_id` = `order_info_detailed`.`order_id` WHERE `order_info_detailed`.`product_table_id` = '"+productId+"' AND `order_info_detailed`.`submission_quantity` != '0' ORDER BY str_to_date(`order_info`.`booking_date`, '%d/%b/%Y') DESC");
            while (rs3.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs3.getString(1)); // Sale Date
                temp.add(rs3.getString(2)); // Sale Quantity
                temp.add(rs3.getString(3)); // Sale Amount
                productReportData3.add(temp);
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            rs3 = stmt3.executeQuery("SELECT `order_return`.`entry_date`, `order_return_detail`.`prod_quant`, `order_return_detail`.`total_amount` FROM `order_return_detail` INNER JOIN `order_return` ON `order_return`.`return_id` = `order_return_detail`.`return_id` WHERE `order_return_detail`.`prod_id` = '"+productId+"' ORDER BY str_to_date(`order_return`.`entry_date`, '%d/%b/%Y') DESC");
            while (rs2.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs3.getString(1)); // Sale Return Date
                temp.add(rs3.getString(2)); // Sale Return Quantity
                temp.add(rs3.getString(3)); // Sale Return Amount
                productReportData4.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        String stDatePurchase = "";
        String stDatePurchaseReturn = "";
        String stDateSale = "";
        String stDateSaleReturn = "";
        Date datePurchase = null;
        Date datePurchaseReturn = null;
        Date dateSale = null;
        Date dateSaleReturn = null;
        ArrayList<Date> objDatesArray;
        int day;
        String stDay = "";
        boolean newDataAdd = true;

        while (productReportData1.size() > 0 || productReportData2.size() > 0 || productReportData3.size() > 0 || productReportData4.size() > 0)
        {
            temp = new ArrayList<>();
            objDatesArray = new ArrayList<>();
            if(productReportData1.size() > 0)
            {
                stDatePurchase = productReportData1.get(0).get(0);
                datePurchase = fmt.parse(stDatePurchase);
                objDatesArray.add(datePurchase);
            }
            if(productReportData2.size() > 0)
            {
                stDatePurchaseReturn = productReportData2.get(0).get(0);
                datePurchaseReturn = fmt.parse(stDatePurchaseReturn);
                objDatesArray.add(datePurchaseReturn);
            }
            if(productReportData3.size() > 0)
            {
                stDateSale = productReportData3.get(0).get(0);
                dateSale = fmt.parse(stDateSale);
                objDatesArray.add(dateSale);
            }
            if(productReportData4.size() > 0)
            {
                stDateSaleReturn = productReportData4.get(0).get(1);
                dateSaleReturn = fmt.parse(stDateSaleReturn);
                objDatesArray.add(dateSaleReturn);
            }
            Date maxDate = Collections.max(objDatesArray);
            day = maxDate.getDay();
            if(day == 0)
                stDay = "Sunday";
            else if(day == 1)
                stDay = "Monday";
            else if(day == 2)
                stDay = "Tuesday";
            else if(day == 3)
                stDay = "Wednesday";
            else if(day == 4)
                stDay = "Thursday";
            else if(day == 5)
                stDay = "Friday";
            else if(day == 6)
                stDay = "Saturday";
            if(datePurchase != null && maxDate.equals(datePurchase) && productReportData1.size() > 0)
            {
                if(productReportData.size() > 0 && productReportData.get(productReportData.size()-1).get(0).equals(stDatePurchase))
                {
                    newDataAdd = false;
                    if(productReportData.get(productReportData.size()-1).get(2).equals("-"))
                    {
                        productReportData.get(productReportData.size()-1).set(2, productReportData1.get(0).get(1));
                        productReportData.get(productReportData.size()-1).set(3, productReportData1.get(0).get(2));
                    }
                    else
                    {
                        String value1 = productReportData.get(productReportData.size()-1).get(2);
                        String value2 = productReportData.get(productReportData.size()-1).get(3);
                        productReportData.get(productReportData.size()-1).set(2, String.valueOf(Integer.parseInt(value1)+Integer.parseInt(productReportData1.get(0).get(1))));
                        productReportData.get(productReportData.size()-1).set(3, String.valueOf(Integer.parseInt(value2)+Integer.parseInt(productReportData1.get(0).get(2))));
                    }
                }
                else
                {
                    newDataAdd = true;
                    temp.add(stDatePurchase);
                    temp.add(stDay);
                    temp.add(productReportData1.get(0).get(1));
                    temp.add(productReportData1.get(0).get(2));
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                }
                productReportData1.remove(0);
            }
            else if(datePurchaseReturn != null && maxDate.equals(datePurchaseReturn) && productReportData2.size() > 0)
            {
                if(productReportData.size() > 0 && productReportData.get(productReportData.size()-1).get(0).equals(stDatePurchaseReturn))
                {
                    newDataAdd = false;
                    if(productReportData.get(productReportData.size()-1).get(4).equals("-"))
                    {
                        productReportData.get(productReportData.size()-1).set(4, productReportData2.get(0).get(1));
                        productReportData.get(productReportData.size()-1).set(5, productReportData2.get(0).get(2));
                    }
                    else
                    {
                        String value1 = productReportData.get(productReportData.size()-1).get(4);
                        String value2 = productReportData.get(productReportData.size()-1).get(5);
                        productReportData.get(productReportData.size()-1).set(4, String.valueOf(Integer.parseInt(value1)+Integer.parseInt(productReportData2.get(0).get(1))));
                        productReportData.get(productReportData.size()-1).set(5, String.valueOf(Integer.parseInt(value2)+Integer.parseInt(productReportData2.get(0).get(2))));
                    }
                }
                else
                {
                    newDataAdd = true;
                    temp.add(stDatePurchaseReturn);
                    temp.add(stDay);
                    temp.add("-");
                    temp.add("-");
                    temp.add(productReportData2.get(0).get(1));
                    temp.add(productReportData2.get(0).get(2));
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                }
                productReportData2.remove(0);
            }
            else if(dateSale != null && maxDate.equals(dateSale) && productReportData3.size() > 0)
            {
                if(productReportData.size() > 0 && productReportData.get(productReportData.size()-1).get(0).equals(stDateSale))
                {
                    newDataAdd = false;
                    if(productReportData.get(productReportData.size()-1).get(6).equals("-"))
                    {
                        productReportData.get(productReportData.size()-1).set(6, productReportData3.get(0).get(1));
                        productReportData.get(productReportData.size()-1).set(7, productReportData3.get(0).get(2));
                    }
                    else
                    {
                        String value1 = productReportData.get(productReportData.size()-1).get(6);
                        String value2 = productReportData.get(productReportData.size()-1).get(7);
                        productReportData.get(productReportData.size()-1).set(6, String.valueOf(Integer.parseInt(value1)+Integer.parseInt(productReportData3.get(0).get(1))));
                        productReportData.get(productReportData.size()-1).set(7, String.valueOf(Integer.parseInt(value2)+Integer.parseInt(productReportData3.get(0).get(2))));
                    }
                }
                else
                {
                    newDataAdd = true;
                    temp.add(stDateSale);
                    temp.add(stDay);
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add(productReportData3.get(0).get(1));
                    temp.add(productReportData3.get(0).get(2));
                    temp.add("-");
                    temp.add("-");
                }
                productReportData3.remove(0);
            }
            else if (dateSaleReturn != null && maxDate.equals(dateSaleReturn) && productReportData4.size() > 0)
            {
                if(productReportData.size() > 0 && productReportData.get(productReportData.size()-1).get(0).equals(stDateSaleReturn))
                {
                    newDataAdd = false;
                    if(productReportData.get(productReportData.size()-1).get(8).equals("-"))
                    {
                        productReportData.get(productReportData.size()-1).set(8, productReportData4.get(0).get(1));
                        productReportData.get(productReportData.size()-1).set(9, productReportData4.get(0).get(2));
                    }
                    else
                    {
                        String value1 = productReportData.get(productReportData.size()-1).get(8);
                        String value2 = productReportData.get(productReportData.size()-1).get(9);
                        productReportData.get(productReportData.size()-1).set(8, String.valueOf(Integer.parseInt(value1)+Integer.parseInt(productReportData4.get(0).get(1))));
                        productReportData.get(productReportData.size()-1).set(9, String.valueOf(Integer.parseInt(value2)+Integer.parseInt(productReportData4.get(0).get(2))));
                    }
                }
                else
                {
                    newDataAdd = true;
                    temp.add(stDateSaleReturn);
                    temp.add(stDay);
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add(productReportData4.get(0).get(1));
                    temp.add(productReportData4.get(0).get(2));
                }
                productReportData4.remove(0);
            }
            if(newDataAdd)
            {
                productReportData.add(temp);
            }
            else
            {
                newDataAdd = true;
            }
        }

        return productReportData;
    }

    private Node createEditIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.EDIT);
        graphic.setFontSize(20.0);
        return graphic;
    }

    private Node createDeleteIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.TRASH);
        graphic.setFontSize(20.0);
        return graphic;
    }
}
