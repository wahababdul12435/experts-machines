package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import controller.AddCompanyFinance;
import controller.AddCompanyFinance;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class AddCompanyFinanceInfo {
    private String srNo;
    private String companyId;
    private String companyName;
    private String companyContact;
    private String addedDate;
    private String addedDay;
    private String amount;
    private String type;
    private String pending;

    private HBox operationsPane;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    public static ArrayList<String> selectedCompanyArr = new ArrayList<>();
    public static ArrayList<String> companyIdArr = new ArrayList<>();
    public static ArrayList<String> companyNameArr = new ArrayList<>();
    public static ArrayList<String> companyContactArr = new ArrayList<>();
    public static ArrayList<String> companyAddressArr = new ArrayList<>();
    public static ArrayList<String> companyCnicArr = new ArrayList<>();
    public static ArrayList<String> companyImageArr = new ArrayList<>();
    public static ArrayList<String> companyPendingAmountArr = new ArrayList<>();
    public static ArrayList<String> amountArr = new ArrayList<>();
    public static ArrayList<String> typeArr = new ArrayList<>();
    public static ArrayList<String> addedDateArr = new ArrayList<>();
    public static ArrayList<String> addedDayArr = new ArrayList<>();
    public static ArrayList<String> pendingArr = new ArrayList<>();

    public static TableView<AddCompanyFinanceInfo> table_addcompanycash;

    public static JFXComboBox<String> txtSelectCompany;
    public static JFXTextField txtCompanyName;
    public static ImageView txtCompanyImage;
    public static JFXTextField txtCompanyContact;
    public static JFXTextField txtCompanyAddress;
    public static JFXTextField txtCompanyCnic;
    public static JFXTextField txtCompanyPendingAmount;
    public static JFXTextField txtAmount;
    public static JFXComboBox<String> txtType;
    public static JFXDatePicker txtAddedDate;

    public static Button btnAdd;
    public static Button btnCancel;
    public static Button btnUpdate;

    MysqlCon objMysqlCon = new MysqlCon();
    Statement objStmt = objMysqlCon.stmt;
    Connection objCon = objMysqlCon.con;

    public AddCompanyFinanceInfo() {
        srNo = "";
        companyId = "";
        companyName = "";
        companyContact = "";

    }

    public AddCompanyFinanceInfo(String srNo, String companyId, String companyName, String companyContact, String addedDate, String addedDay, String amount, String type, String pending) {
        this.srNo = srNo;
        this.companyId = companyId;
        this.companyName = companyName;
        this.companyContact = companyContact;
        this.addedDate = addedDate;
        this.addedDay = addedDay;
        this.amount = amount;
        this.type = type;
        this.pending = pending;

        this.btnEdit = new JFXButton("");
        this.btnDelete = new JFXButton("");
        this.btnEdit.setGraphic(createEditIcon());
        this.btnDelete.setGraphic(createDeleteIcon());
        this.btnEdit.getStyleClass().add("btn_icon");
        this.btnDelete.getStyleClass().add("btn_icon");
        this.btnEdit.setOnAction((action)->editAddedRecord());
        this.btnDelete.setOnAction((action)->deleteAddedRecord());
        this.operationsPane = new HBox(btnEdit, btnDelete);
    }

    private void deleteAddedRecord()
    {
        selectedCompanyArr.remove(Integer.parseInt(srNo)-1);
        companyIdArr.remove(Integer.parseInt(srNo)-1);
        companyNameArr.remove(Integer.parseInt(srNo)-1);
        companyContactArr.remove(Integer.parseInt(srNo)-1);
        companyAddressArr.remove(Integer.parseInt(srNo)-1);
        companyCnicArr.remove(Integer.parseInt(srNo)-1);
        companyImageArr.remove(Integer.parseInt(srNo)-1);
        companyPendingAmountArr.remove(Integer.parseInt(srNo)-1);
        amountArr.remove(Integer.parseInt(srNo)-1);
        typeArr.remove(Integer.parseInt(srNo)-1);
        addedDateArr.remove(Integer.parseInt(srNo)-1);
        addedDayArr.remove(Integer.parseInt(srNo)-1);
        pendingArr.remove(Integer.parseInt(srNo)-1);
        table_addcompanycash.setItems(AddCompanyFinance.parseUserList());
    }

    public void editAddedRecord()
    {
        AddCompanyFinance.srNo = Integer.parseInt(srNo)-1;
        File file = new File(companyImageArr.get(Integer.parseInt(srNo)-1));
        boolean exists = file.exists();
        if(exists)
        {
            Image image = new Image(file.toURI().toString());
            txtCompanyImage.setImage(image);
        }
        txtCompanyName.setText(companyNameArr.get(Integer.parseInt(srNo)-1));
        txtCompanyContact.setText(companyContactArr.get(Integer.parseInt(srNo)-1));
        txtCompanyAddress.setText(companyAddressArr.get(Integer.parseInt(srNo)-1));
        txtCompanyCnic.setText(companyCnicArr.get(Integer.parseInt(srNo)-1));
        txtCompanyPendingAmount.setText(companyPendingAmountArr.get(Integer.parseInt(srNo)-1));
        txtAmount.setText(amountArr.get(Integer.parseInt(srNo)-1));
        txtType.setValue(typeArr.get(Integer.parseInt(srNo)-1));
        txtAddedDate.setValue(LOCAL_DATE(addedDateArr.get(Integer.parseInt(srNo)-1)));

        btnAdd.setVisible(false);
        btnCancel.setVisible(true);
        btnUpdate.setVisible(true);
    }

    public ArrayList<ArrayList<String>> getAddedCash()
    {
        ArrayList<ArrayList<String>> companyData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        for(int i=0; i<companyIdArr.size(); i++)
        {
            temp = new ArrayList<String>();
            temp.add(companyIdArr.get(i));
            temp.add(companyNameArr.get(i));
            temp.add(companyContactArr.get(i));
            temp.add(addedDateArr.get(i));
            temp.add(addedDayArr.get(i));
            temp.add(amountArr.get(i));
            temp.add(typeArr.get(i));
            temp.add(pendingArr.get(i));
            companyData.add(temp);
        }
        return companyData;
    }

    public static final LocalDate LOCAL_DATE (String dateString) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MMM/yyyy");
        LocalDate localDate = LocalDate.parse(dateString, formatter);
        return localDate;
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyContact() {
        return companyContact;
    }

    public void setCompanyContact(String companyContact) {
        this.companyContact = companyContact;
    }

    public String getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(String addedDate) {
        this.addedDate = addedDate;
    }

    public String getAddedDay() {
        return addedDay;
    }

    public void setAddedDay(String addedDay) {
        this.addedDay = addedDay;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPending() {
        return pending;
    }

    public void setPending(String pending) {
        this.pending = pending;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public ArrayList<ArrayList<String>> getCompaniesInfo(Statement stmt, Connection con)
    {
        ArrayList<ArrayList<String>> companiesData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT `company_info`.`company_table_id`, `company_info`.`company_name`, `company_info`.`company_contact`, `company_info`.`company_address`, `company_info`.`company_email`, `company_info`.`company_image`, `company_overall_record`.`pending_payments` FROM `company_info` LEFT OUTER JOIN `company_overall_record` ON `company_info`.`company_table_id` = `company_overall_record`.`company_id` WHERE `company_info`.`company_status` != 'Deleted'");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Table id
                temp.add(rs.getString(2)+" ("+rs.getString(4)+" - "+rs.getString(3)+")"); // name
                temp.add(rs.getString(2)); // name
                temp.add(rs.getString(3)); // contact
                temp.add(rs.getString(4)); // address
                temp.add(rs.getString(5)); // Email
                if(rs.getString(6) != null)
                {
                    temp.add(rs.getString(6)); // Image
                }
                else
                {
                    temp.add(""); // Image
                }

                temp.add(rs.getString(7)); // Pending Payments
                companiesData.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return companiesData;
    }

    public void insertCompanyCash(Statement stmt, Connection con) throws SQLException {
        String currentDate = GlobalVariables.getStDate();
        String currentTime = GlobalVariables.getStTime();
        String currentUser = GlobalVariables.getUserId();
        String insertCompanyPayment;
        String updateOverAll;
        String orderId = "0";
        String cashStatus = "Cash";
        float pendingPayment = 0;

        for(int i=0; i<companyIdArr.size(); i++)
        {
            ResultSet rs = null;
            try {
                rs = stmt.executeQuery("SELECT `pending_payments` FROM `company_payments` WHERE `company_id` = '"+companyIdArr.get(i)+"' ORDER BY `payment_id` DESC LIMIT 1");
                if(rs.next())
                {
                    pendingPayment = rs.getFloat(1);
                }
                else
                {
                    pendingPayment = 0;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

            if(typeArr.get(i).equals("Sent"))
            {
                pendingPayment = pendingPayment - Float.parseFloat(amountArr.get(i));
                updateOverAll = "UPDATE `company_overall_record` SET `cash_sent` = `cash_sent` + '"+amountArr.get(i)+"', `pending_payments` = `pending_payments` - '"+amountArr.get(i)+"' WHERE `company_id` = '"+companyIdArr.get(i)+"'";
            }
            else if(typeArr.get(i).equals("Received"))
            {
                pendingPayment = pendingPayment + Float.parseFloat(amountArr.get(i));
                updateOverAll = "UPDATE `company_overall_record` SET `cash_return` = `cash_return` + '"+amountArr.get(i)+"', `pending_payments` = `pending_payments` + '"+amountArr.get(i)+"' WHERE `company_id` = '"+companyIdArr.get(i)+"'";
            }
            else
            {
                pendingPayment = pendingPayment - Float.parseFloat(amountArr.get(i));
                updateOverAll = "UPDATE `company_overall_record` SET `waived_off_price` = `waived_off_price` + '"+amountArr.get(i)+"', `pending_payments` = `pending_payments` - '"+amountArr.get(i)+"' WHERE `company_id` = '"+companyIdArr.get(i)+"'";
            }
            insertCompanyPayment = "INSERT INTO `company_payments`(`company_id`, `invoice_id`, `amount`, `cash_type`, `pending_payments`, `date`, `day`, `user_id`, `status`) VALUES ('"+companyIdArr.get(i)+"','"+orderId+"','"+amountArr.get(i)+"','"+typeArr.get(i)+"','"+pendingPayment+"','"+addedDateArr.get(i)+"','"+addedDayArr.get(i)+"','"+currentUser+"','"+cashStatus+"')";
            int insertInfo = -1;
            if(stmt.executeUpdate(insertCompanyPayment) != -1)
            {
                insertInfo = 1;
            }
            if(stmt.executeUpdate(updateOverAll) != -1)
            {
                insertInfo = 1;
            }
            if(insertInfo == -1)
            {
                String title = "Error";
                String message = "Unable to insert";
                GlobalVariables.showNotification(-1, title, message);
            }
            else
            {
                String title = "Successfull";
                String message = "Record Inserted";
                GlobalVariables.showNotification(1, title, message);
            }
        }
        refreshAllArrays();
    }

    private void refreshAllArrays()
    {
        selectedCompanyArr = new ArrayList<>();
        companyIdArr = new ArrayList<>();
        companyNameArr = new ArrayList<>();
        companyContactArr = new ArrayList<>();
        companyAddressArr = new ArrayList<>();
        companyCnicArr = new ArrayList<>();
        companyImageArr = new ArrayList<>();
        companyPendingAmountArr = new ArrayList<>();
        amountArr = new ArrayList<>();
        typeArr = new ArrayList<>();
        addedDateArr = new ArrayList<>();
        addedDayArr = new ArrayList<>();
        pendingArr = new ArrayList<>();
    }

    private Node createEditIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.EDIT);
        graphic.setFontSize(20.0);
        return graphic;
    }

    private Node createDeleteIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.TRASH);
        graphic.setFontSize(20.0);
        return graphic;
    }
}
