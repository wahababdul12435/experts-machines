package model;

import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.Button;

public  class Product {
        String productCompID;
        String productId;
        String productName;
        String productBatch;
        String productQuant;
        String productBonus;
        String productDisc;
        String productExpiry;
        String totalValue;
        String packSize;
        String discAmount;
        String stockQuant;
        String bonusstockQuant;

       //private Button deleteButton;

public Product()
{}

    public Product(String prodcompid,String prodid, String prodName,String prodbatch, String prodquant, String prodbonus,String proddisc,String prodexpiry,String totalval, String pcksize,String discountamount, String stockquantity, String bonusquantstock) {
        this.productCompID = prodcompid;
        this.productId =prodid;
        this.productName = prodName;
        this.productBatch = prodbatch;
        this.productQuant = prodquant;
        this.productBonus = prodbonus;
        this.productDisc = proddisc;
        this.productExpiry = prodexpiry;
        this.totalValue = totalval;
        this.packSize = pcksize;
        this.discAmount = discountamount;
        this.stockQuant = stockquantity;
        this.bonusstockQuant = bonusquantstock;

}

    public String getProductCompID() {
        return productCompID;
    }
    public void setProductCompID(String prodid) {
        this.productCompID=prodid;
    }

    public String getProductId() {
        return productId;
    }
    public void setProductId(String prodid) {
        this.productId=prodid;
    }

    public String getProductName() {
        return productName;
    }
    public void setProductName(String prodid) {
        this.productName=prodid;
    }

    public String getProductBatch() {
        return productBatch;
    }
    public void setProductBatch(String prodid) {
        this.productBatch=prodid;
    }

    public String getProductQuant() {
        return productQuant;
    }
    public void setProductQuant(String prodid) {
        this.productQuant=prodid;
    }

    public String getProductBonus() {
        return productBonus;
    }
    public void setProductBonus(String prodid) {
        this.productBonus=prodid;
    }

    public  String getProductDisc(){
    return productDisc;
    }
    public void setProductDisc(String prodid) {
        this.productDisc = prodid;
    }

    public  String getProductExpiry(){
        return productExpiry;
    }
    public void setProductExpiry(String prodid) {
        this.productExpiry = prodid;
    }

    public String getTotalValue() {
        return totalValue;
    }
    public void setTotalValue(String prodid) {
        this.totalValue=prodid;
    }

    public String getPackSize() {
        return packSize;
    }
    public void setPackSize(String prodid) {
        this.packSize=prodid;
    }

    public String getDiscAmount() {
        return discAmount;
    }
    public void setDiscAmount(String prodid) {
        this.discAmount=prodid;
    }

    public String getStockQuant() {
        return stockQuant;
    }
    public void setStockQuant(String prodid) {
        this.stockQuant=prodid;
    }

    public String getBonusstockQuant() {
        return bonusstockQuant;
    }
    public void setBonusstockQuant(String prodid) {
        this.bonusstockQuant=prodid;
    }



    /*public Button getDeleteButton() {
        return deleteButton;
    }
    public void setDeleteButton(Button delbutn) {
        this.deleteButton=delbutn;
    }*/
}
