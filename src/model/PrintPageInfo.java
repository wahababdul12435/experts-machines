package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import controller.EnterSupplier;
import controller.PrintPage;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class PrintPageInfo {
    private String srNo;
    private String ownerId;
    private String ownerName;
    private String fatherName;
    private String businessName;
    private String businessAddress;

    private HBox operationsPane;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    public static ArrayList<String> ownerIdArr = new ArrayList<>();
    public static ArrayList<String> ownerNameArr = new ArrayList<>();
    public static ArrayList<String> fatherNameArr = new ArrayList<>();
    public static ArrayList<String> ownerCNICArr = new ArrayList<>();
    public static ArrayList<String> ownerCountryArr = new ArrayList<>();
    public static ArrayList<String> businessNameArr = new ArrayList<>();
    public static ArrayList<String> businessAddressArr = new ArrayList<>();
    public static ArrayList<String> businessCityArr = new ArrayList<>();
    public static ArrayList<String> headerArr1 = new ArrayList<>();
    public static ArrayList<String> headerArr2 = new ArrayList<>();
    public static ArrayList<String> headerArr3 = new ArrayList<>();
    public static ArrayList<String> headerArr4 = new ArrayList<>();
    public static ArrayList<String> headerArr5 = new ArrayList<>();
    public static ArrayList<String> footerArr1 = new ArrayList<>();
    public static ArrayList<String> footerArr2 = new ArrayList<>();
    public static ArrayList<String> footerArr3 = new ArrayList<>();
    public static ArrayList<String> footerArr4 = new ArrayList<>();

    public static TableView<PrintPageInfo> table_addsupplier;

    public static JFXTextField txtownerId;
    public static JFXTextField txtownerName;
    public static JFXTextField txtfatherName;
    public static JFXTextField txtownerCNIC;
    public static JFXTextField txtownerCountry;
    public static JFXTextField txtbusinessName;
    public static JFXTextField txtbusinessAddress;
    public static JFXTextField txtbusinessCity;
    public static JFXTextField txtheader1;
    public static JFXTextField txtheader2;
    public static JFXTextField txtheader3;
    public static JFXTextField txtheader4;
    public static JFXTextField txtheader5;
    public static JFXTextField txtfooter1;
    public static JFXTextField txtfooter2;
    public static JFXTextField txtfooter3;
    public static JFXTextField txtfooter4;

    public static Button btnAdd;
    public static Button btnCancel;
    public static Button btnUpdate;

    private ArrayList<String> savedCompanyIds;
    private ArrayList<String> savedCompanyNames;

    MysqlCon objMysqlCon = new MysqlCon();
    Statement objStmt = objMysqlCon.stmt;
    Connection objCon = objMysqlCon.con;

    public PrintPageInfo() {
        srNo = "";
        ownerName = "";
        fatherName = "";
        businessName = "";
        businessAddress = "";

    }

    public PrintPageInfo(String srNo, String ownerId, String ownerName, String fatherName, String businessName, String businessAddress) {
        this.srNo = srNo;
        this.ownerId = ownerId;
        this.ownerName = ownerName;
        this.fatherName = fatherName;
        this.businessName = businessName;
        this.businessAddress = businessAddress;
        this.btnEdit = new JFXButton("Edit");
        this.btnDelete = new JFXButton("Delete");
        this.btnEdit.setOnAction((action)->editAddedRecord());
        this.btnDelete.setOnAction((action)->deleteAddedRecord());
        this.operationsPane = new HBox(btnEdit, btnDelete);

    }

    private void deleteAddedRecord()
    {
        ownerIdArr.remove(Integer.parseInt(srNo)-1);
        ownerNameArr.remove(Integer.parseInt(srNo)-1);
        fatherNameArr.remove(Integer.parseInt(srNo)-1);
        ownerCNICArr.remove(Integer.parseInt(srNo)-1);
        ownerCountryArr.remove(Integer.parseInt(srNo)-1);
        businessNameArr.remove(Integer.parseInt(srNo)-1);
        businessAddressArr.remove(Integer.parseInt(srNo)-1);
        businessCityArr.remove(Integer.parseInt(srNo)-1);
        headerArr1.remove(Integer.parseInt(srNo)-1);
        headerArr2.remove(Integer.parseInt(srNo)-1);
        headerArr3.remove(Integer.parseInt(srNo)-1);
        headerArr4.remove(Integer.parseInt(srNo)-1);
        headerArr5.remove(Integer.parseInt(srNo)-1);
        footerArr1.remove(Integer.parseInt(srNo)-1);
        footerArr2.remove(Integer.parseInt(srNo)-1);
        footerArr3.remove(Integer.parseInt(srNo)-1);
        footerArr4.remove(Integer.parseInt(srNo)-1);
        table_addsupplier.setItems(PrintPage.parseUserList());
    }

    public void editAddedRecord()
    {
        EnterSupplier.srNo = Integer.parseInt(srNo)-1;
        txtownerId.setText(ownerIdArr.get(Integer.parseInt(srNo)-1));
        txtownerName.setText(ownerNameArr.get(Integer.parseInt(srNo)-1));
        txtfatherName.setText(fatherNameArr.get(Integer.parseInt(srNo)-1));
        txtownerCNIC.setText(ownerCNICArr.get(Integer.parseInt(srNo)-1));
        txtownerCountry.setText(businessNameArr.get(Integer.parseInt(srNo)-1));
        txtbusinessName.setText(businessAddressArr.get(Integer.parseInt(srNo)-1));
        txtbusinessAddress.setText(businessCityArr.get(Integer.parseInt(srNo)-1));
        txtheader1.setText(headerArr1.get(Integer.parseInt(srNo)-1));
        txtheader2.setText(headerArr2.get(Integer.parseInt(srNo)-1));
        txtheader3.setText(headerArr3.get(Integer.parseInt(srNo)-1));
        txtheader4.setText(headerArr4.get(Integer.parseInt(srNo)-1));
        txtheader5.setText(headerArr5.get(Integer.parseInt(srNo)-1));
        txtfooter1.setText(footerArr1.get(Integer.parseInt(srNo)-1));
        txtfooter2.setText(footerArr2.get(Integer.parseInt(srNo)-1));
        txtfooter3.setText(footerArr3.get(Integer.parseInt(srNo)-1));
        txtfooter4.setText(footerArr4.get(Integer.parseInt(srNo)-1));

        btnAdd.setVisible(false);
        btnCancel.setVisible(true);
        btnUpdate.setVisible(true);
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }


    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(String businessAddress) {
        this.businessAddress = businessAddress;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public ArrayList<String> getSavedCompanyIds() {
        return savedCompanyIds;
    }

    public void setSavedCompanyIds(ArrayList<String> savedCompanyIds) {
        this.savedCompanyIds = savedCompanyIds;
    }

    public ArrayList<String> getSavedCompanyNames() {
        return savedCompanyNames;
    }

    public void setSavedCompanyNames(ArrayList<String> savedCompanyNames) {
        this.savedCompanyNames = savedCompanyNames;
    }


    public ArrayList<String> getSupplierDetail(String supplierId)
    {
        ArrayList<String> PrintPageInfo = new ArrayList<>();
        ResultSet rs = null;
        try {
            rs = GlobalVariables.objStmt.executeQuery("SELECT `supplier_id`, `supplier_name`, `supplier_contact`, `creating_date`, `update_date`, `supplier_status` FROM `supplier_info` WHERE `supplier_table_id` = '"+supplierId+"'");
            while (rs.next())
            {
                PrintPageInfo.add(rs.getString(1)); // Id
                PrintPageInfo.add(rs.getString(2)); // Name
                PrintPageInfo.add(rs.getString(3)); // Contact
                PrintPageInfo.add(rs.getString(4)); // Created
                PrintPageInfo.add(rs.getString(5)); // Updated
                PrintPageInfo.add(rs.getString(6)); // Status
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return PrintPageInfo;
    }

    public void insertPageinfo(Statement stmt, Connection con) throws SQLException {
        String currentDate = GlobalVariables.getStDate();
        String currentTime = GlobalVariables.getStTime();
        String currentUser = GlobalVariables.getUserId();
        String ownerstatus = "Active";
        boolean withId = false;
        boolean withoutId = false;
        String insertQuery;
        String insertSupplierCompany;
        String supplierTableId;

        for(int i=0; i<ownerIdArr.size(); i++)
        {
            if(ownerIdArr.get(i).equals("") || ownerIdArr.get(i) == null)
            {
                insertQuery = "INSERT INTO `sale_invoice_print_info`(`header_1`, `header_2`, `header_3`, `header_4`, `header_5`,  `owner_name`, `father_name`, `owner_cnic`, `owner_country`, `business_name`, `business_address`, `business_city`, `footer_1`, `footer_2`, `footer_3`, `footer_4`,`creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`, `status`) VALUES ('"+headerArr1.get(i)+"','"+headerArr2.get(i)+"','"+headerArr3.get(i)+"','"+headerArr4.get(i)+"','"+headerArr5.get(i)+"','"+ownerNameArr.get(i)+"','"+fatherNameArr.get(i)+"','"+ownerCNICArr.get(i)+"','"+ownerCountryArr.get(i)+"','"+businessNameArr.get(i)+"','"+businessAddressArr.get(i)+"','"+businessCityArr.get(i)+"','"+footerArr1.get(i)+"','"+footerArr2.get(i)+"','"+footerArr3.get(i)+"','"+footerArr4.get(i)+"','"+currentUser+"','"+currentDate+"','"+currentTime+"','"+currentUser+"','"+currentDate+"','"+currentTime+"','"+ownerstatus+"')";
            }
            else
            {
                insertQuery = "INSERT INTO `sale_invoice_print_info`( `header_1`, `header_2`, `header_3`, `header_4`, `header_5`, `owner_ids`, `owner_name`, `father_name`, `owner_cnic`, `owner_country`, `business_name`, `business_address`, `business_city`, `footer_1`, `footer_2`, `footer_3`, `footer_4`,`creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`, `status`) VALUES ('"+headerArr1.get(i)+"','"+headerArr2.get(i)+"','"+headerArr3.get(i)+"','"+headerArr4.get(i)+"','"+headerArr5.get(i)+"','"+ownerIdArr.get(i)+"','"+ownerNameArr.get(i)+"','"+fatherNameArr.get(i)+"','"+ownerCNICArr.get(i)+"','"+ownerCountryArr.get(i)+"','"+businessNameArr.get(i)+"','"+businessAddressArr.get(i)+"','"+businessCityArr+"','"+footerArr1.get(i)+"','"+footerArr2.get(i)+"','"+footerArr3.get(i)+"','"+footerArr4.get(i)+"','"+currentUser+"','"+currentDate+"','"+currentTime+"','"+currentUser+"','"+currentDate+"','"+currentTime+"','"+ownerstatus+"')";
            }
            stmt.executeUpdate(insertQuery);
        }
        refreshAllArrays();
    }

    private void refreshAllArrays()
    {
        ownerIdArr = new ArrayList<>();
        ownerNameArr = new ArrayList<>();
        fatherNameArr = new ArrayList<>();
        ownerCNICArr = new ArrayList<>();
        ownerCountryArr = new ArrayList<>();
        businessNameArr = new ArrayList<>();
        businessAddressArr = new ArrayList<>();
        businessCityArr = new ArrayList<>();
        headerArr1 = new ArrayList<>();
        headerArr2 = new ArrayList<>();
        headerArr3 = new ArrayList<>();
        headerArr4 = new ArrayList<>();
        headerArr5 = new ArrayList<>();
        footerArr1 = new ArrayList<>();
        footerArr2 = new ArrayList<>();
        footerArr3 = new ArrayList<>();
        footerArr4 = new ArrayList<>();
    }

    public void updateSupplier(Statement stmt, Connection con, String supplierTableId, String supplierId, String supplierName, String supplierAddress, String supplierCity, String supplierEmail, String contactPerson, String supplierContact, ArrayList<String> companyIds, String supplierStatus)
    {
        String currentDate = GlobalVariables.getStDate();
        String currentTime = GlobalVariables.getStTime();
        String currentUser = GlobalVariables.getUserId();
        String updateQuery;
        try {
            if(supplierId.equals("") || supplierId.equals("N/A"))
            {
                updateQuery = "UPDATE `supplier_info` SET `supplier_id`= NULL, `supplier_name`='"+supplierName+"',`supplier_email`='"+supplierEmail+"',`supplier_contact`='"+supplierContact+"',`contact_person`='"+contactPerson+"',`supplier_address`='"+supplierAddress+"',`supplier_city`='"+supplierCity+"',`supplier_status`='"+supplierStatus+"',`update_user_id`='"+currentUser+"',`update_date`='"+currentDate+"',`update_time`='"+currentTime+"' WHERE `supplier_table_id` = '"+supplierTableId+"'";
            }
            else
            {
                updateQuery = "UPDATE `supplier_info` SET `supplier_id`='"+supplierId+"',`supplier_name`='"+supplierName+"',`supplier_email`='"+supplierEmail+"',`supplier_contact`='"+supplierContact+"',`contact_person`='"+contactPerson+"',`supplier_address`='"+supplierAddress+"',`supplier_city`='"+supplierCity+"',`supplier_status`='"+supplierStatus+"',`update_user_id`='"+currentUser+"',`update_date`='"+currentDate+"',`update_time`='"+currentTime+"' WHERE `supplier_table_id` = '"+supplierTableId+"'";
            }
            stmt.executeUpdate(updateQuery);
            String deleteCompany = "DELETE FROM `supplier_company` WHERE `supplier_id` = '"+supplierTableId+"'";
            stmt.executeUpdate(deleteCompany);
            String supplierCompany = "INSERT INTO `supplier_company`(`supplier_id`, `company_id`, `updated_date`, `user_id`, `status`) VALUES ";
            for(int i=0; i<companyIds.size(); i++)
            {
                if(i>0)
                {
                    supplierCompany += ", ";
                }
                supplierCompany += "('"+supplierTableId+"','"+companyIds.get(i)+"','"+currentDate+"','"+currentUser+"', '"+supplierStatus+"')";
            }
            if(companyIds.size() > 0)
            {
                stmt.executeUpdate(supplierCompany);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<ArrayList<String>> getAddedPageInfo()
    {
        ArrayList<ArrayList<String>> PrintPageData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        for(int i=0; i<ownerIdArr.size(); i++)
        {
            temp = new ArrayList<String>();
            temp.add(ownerIdArr.get(i));
            temp.add(ownerNameArr.get(i));
            temp.add(fatherNameArr.get(i));
            temp.add(ownerCountryArr.get(i));
            temp.add(businessNameArr.get(i));
            temp.add(businessAddressArr.get(i));
            PrintPageData.add(temp);
        }
        return PrintPageData;
    }

    String checkId = new String();
    public String confirmNewId(String ownersId)
    {
        ResultSet rs3 = null;
        try {
            rs3 = objStmt.executeQuery("SELECT `owner_ids` FROM `sale_invoice_print_info` WHERE `owner_ids` = '"+ownersId+"' AND `status` != 'Deleted'");
            while (rs3.next())
            {
                checkId = rs3.getString(1) ;
            }
//            objCon.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return checkId;
    }

    public void getCitiesInfo(Statement stmt, Connection con)
    {
        ArrayList<String> tempIds;
        ArrayList<String> tempNames;
        tempIds = new ArrayList<String>();
        tempNames = new ArrayList<String>();
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT `city_table_id`, `city_name` FROM `city_info` WHERE `city_status` != 'Deleted' ORDER BY `city_table_id`");
            while (rs.next())
            {
                tempIds.add(rs.getString(1));
                tempNames.add(rs.getString(2)+" ("+rs.getString(1)+")");
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        setSavedCompanyIds(tempIds);
        setSavedCompanyNames(tempNames);
    }

    public String getSupplierTableId(Statement stmt3, Connection con3 , String supplierName, String contactNumber)
    {
        ResultSet rs3 = null;
        try {
            rs3 = stmt3.executeQuery("SELECT `supplier_table_id` FROM `supplier_info` WHERE `supplier_name` = '"+supplierName+"' AND `supplier_contact` = '"+contactNumber+"' AND `supplier_status` != 'Deleted'");
            while (rs3.next())
            {
                checkId = rs3.getString(1) ;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return checkId;
    }
}
