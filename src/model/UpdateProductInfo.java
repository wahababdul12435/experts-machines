package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class UpdateProductInfo {
    public ArrayList<ArrayList<String>> companiesData = new ArrayList<>();
    public ArrayList<ArrayList<String>> companyGroupsData = new ArrayList<>();

    public ArrayList<ArrayList<String>> getCompaniesData()
    {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement stmt = objMysqlCon.stmt;
        Connection con = objMysqlCon.con;
        ResultSet rs = null;
        ArrayList<String> temp;
        try {
            rs = stmt.executeQuery("SELECT `company_table_id`, `company_name` FROM `company_info`");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2));
                companiesData.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return companiesData;
    }

    public ArrayList<ArrayList<String>> getCompanyGroupsData()
    {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement stmt = objMysqlCon.stmt;
        Connection con = objMysqlCon.con;
        ResultSet rs = null;
        ArrayList<String> temp;
        try {
            rs = stmt.executeQuery("SELECT `group_table_id`, `group_name`, `company_id` FROM `groups_info`");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2));
                temp.add(rs.getString(3));
                companyGroupsData.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return companyGroupsData;
    }
}
