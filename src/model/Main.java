package model;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.awt.*;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{

        Parent root = FXMLLoader.load(getClass().getResource("../view/login.fxml"));
        primaryStage.setTitle("Profit Flow");
        primaryStage.setX(GlobalVariables.boundsX);
        primaryStage.setY(GlobalVariables.boundsY);
        GlobalVariables.baseScene = new Scene(root, GlobalVariables.SCREEN_WIDTH, GlobalVariables.SCREEN_HEIGHT);
        primaryStage.setScene(GlobalVariables.baseScene);
        primaryStage.setMaximized(true);
        primaryStage.show();
        GlobalVariables.baseStage = primaryStage;
    }


    public static void main(String[] args) {
        launch(args);
    }
}
