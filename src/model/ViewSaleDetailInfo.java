package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import controller.ViewSaleDetail;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class ViewSaleDetailInfo {
    public String orderId;
    private String srNo;
    private String productId;
    private String productName;
    private String batchNo;
    private Label batchExpiry;
    private Label orderedQuantity;
    private String bonusQuantity;
    private String productPrice;
    private String discountGiven;

    private HBox operationsPane;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    public static ArrayList<String> preProductIdArr = new ArrayList<>();
    public static ArrayList<String> productIdArr = new ArrayList<>();
    public static ArrayList<String> productNameArr = new ArrayList<>();
    public static ArrayList<String> batchIdArr = new ArrayList<>();
    public static ArrayList<String> preBatchNoArr = new ArrayList<>();
    public static ArrayList<String> batchNoArr = new ArrayList<>();
    public static ArrayList<String> batchExpiryArr = new ArrayList<>();
    public static ArrayList<String> orderedQuantityArr = new ArrayList<>();
    public static ArrayList<String> preSubmissionQuantityArr = new ArrayList<>();
    public static ArrayList<String> submissionQuantityArr = new ArrayList<>();
    public static ArrayList<String> bonusQuantityArr = new ArrayList<>();
//    public static ArrayList<String> unitArr = new ArrayList<>();
    public static ArrayList<String> productPriceArr = new ArrayList<>();
    public static ArrayList<String> finalPriceArr = new ArrayList<>();
    public static ArrayList<String> discountGivenArr = new ArrayList<>();
    public static String preBatch;
    public static String preSubmissionQty;

    public static TableView<ViewSaleDetailInfo> table_saledetaillog;

    public static JFXTextField txtProductName;
    public static JFXComboBox<String> txtBatchNo;
    public static JFXTextField txtQuantity;
//    public static JFXComboBox<String>  txtUnit;
    public static JFXTextField txtBonusPack;

    public static Button btnAdd;
    public static Button btnCancel;
    public static Button btnUpdate;

    public ViewSaleDetailInfo(String orderId) {
        this.orderId = orderId;
    }

    public ViewSaleDetailInfo(String srNo, String productName, String batchNo, String batchExpiry, String orderedQuantity, String bonusQuantity, String productPrice, String discountGiven) {
        this.srNo = srNo;
        this.productName = productName;
        this.batchNo = batchNo;

        if(batchExpiry != null && !batchExpiry.equals("") && !batchExpiry.equals("N/A"))
        {
            String stEndDate = batchExpiry;
            String stStartDate = GlobalVariables.getStDate();

            Date endDate = null;
            Date startDate = null;
            try {
                startDate = GlobalVariables.fmt.parse(stStartDate);
                endDate = GlobalVariables.fmt.parse(stEndDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            long diff = endDate.getTime() - startDate.getTime();
            diff = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);

            if(diff >= 180)
            {
                this.batchExpiry = new Label(batchExpiry);
            }
            else if(diff < 0)
            {
                this.batchExpiry = new Label(batchExpiry+"\n(EXPIRED)");
                this.batchExpiry.setStyle("-fx-background-color: orange;");
            }
            else
            {
                this.batchExpiry = new Label(batchExpiry+"\n(Days Left: "+diff+")");
                this.batchExpiry.setStyle("-fx-background-color: yellow;");
            }
        }
        else
        {
            this.batchExpiry = new Label("N/A");
        }

        this.batchExpiry.setPrefWidth(100);
        this.batchExpiry.setAlignment(Pos.CENTER);

        this.orderedQuantity = new Label (orderedQuantity);

        this.bonusQuantity = bonusQuantity;
        this.productPrice = productPrice;
        this.discountGiven = discountGiven;

        this.btnEdit = new JFXButton();
        this.btnDelete = new JFXButton();
        this.btnEdit.setGraphic(createEditIcon());
        this.btnDelete.setGraphic(createDeleteIcon());
        this.btnEdit.getStyleClass().add("btn_icon");
        this.btnDelete.getStyleClass().add("btn_icon");
        this.operationsPane = new HBox(btnEdit, btnDelete);
        this.btnEdit.setOnAction((action)->editAddedRecord());
        this.btnDelete.setOnAction((action)->deleteAddedRecord());
    }

    private void editAddedRecord()
    {
        ViewSaleDetail.srNo = Integer.parseInt(srNo)-1;
        txtProductName.setText(productNameArr.get(Integer.parseInt(srNo)-1));
        txtBatchNo.getItems().clear();
        String productId = ViewSaleDetail.getProductId(ViewSaleDetail.completeProductData, txtProductName.getText());
        ArrayList<String> batchData = ViewSaleDetail.getProductsBatch(ViewSaleDetail.completeBatchData, productId);
        txtBatchNo.getItems().addAll(batchData);
        txtBatchNo.setValue(batchNoArr.get(Integer.parseInt(srNo)-1));
        preBatch = batchNoArr.get(Integer.parseInt(srNo)-1);
        txtQuantity.setText(orderedQuantityArr.get(Integer.parseInt(srNo)-1));
        preSubmissionQty = submissionQuantityArr.get(Integer.parseInt(srNo)-1);
//        txtUnit.setValue(unitArr.get(Integer.parseInt(srNo)-1));
        txtBonusPack.setText(bonusQuantityArr.get(Integer.parseInt(srNo)-1));
        btnAdd.setVisible(false);
        btnCancel.setVisible(true);
        btnUpdate.setVisible(true);
    }

    private void deleteAddedRecord()
    {
        productIdArr.remove(Integer.parseInt(srNo)-1);
        productNameArr.remove(Integer.parseInt(srNo)-1);
        batchIdArr.remove(Integer.parseInt(srNo)-1);
        batchNoArr.remove(Integer.parseInt(srNo)-1);
        batchExpiryArr.remove(Integer.parseInt(srNo)-1);
        orderedQuantityArr.remove(Integer.parseInt(srNo)-1);
        submissionQuantityArr.remove(Integer.parseInt(srNo)-1);
        bonusQuantityArr.remove(Integer.parseInt(srNo)-1);
//        unitArr.remove(Integer.parseInt(srNo)-1);
        productPriceArr.remove(Integer.parseInt(srNo)-1);
        finalPriceArr.remove(Integer.parseInt(srNo)-1);
        discountGivenArr.remove(Integer.parseInt(srNo)-1);
        table_saledetaillog.setItems(ViewSaleDetail.parseUserList());
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    public Label getBatchExpiry() {
        return batchExpiry;
    }

    public void setBatchExpiry(Label batchExpiry) {
        this.batchExpiry = batchExpiry;
    }

    public Label getOrderedQuantity() {
        return orderedQuantity;
    }

    public void setOrderedQuantity(Label orderedQuantity) {
        this.orderedQuantity = orderedQuantity;
    }

    public String getBonusQuantity() {
        return bonusQuantity;
    }

    public void setBonusQuantity(String bonusQuantity) {
        this.bonusQuantity = bonusQuantity;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getDiscountGiven() {
        return discountGiven;
    }

    public void setDiscountGiven(String discountGiven) {
        this.discountGiven = discountGiven;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public ArrayList<String> getSaleBasicInfo(Statement stmt, Connection con)
    {
        ArrayList<String> saleBasicData = new ArrayList<String>();
        ArrayList<String> temp;
        ResultSet rs = null;
        String query = "SELECT `order_info`.`order_id`, `dealer_info`.`dealer_name`, `dealer_info`.`dealer_phone`, `order_info`.`final_price`, `order_info`.`discount`, `order_info`.`status`, `order_info`.`booking_user_id`, (SELECT `user_info`.`user_name` FROM `user_info` WHERE `order_info`.`booking_user_id` = `user_info`.`user_table_id`) as 'booking_user', `order_info`.`booking_date`, `order_info`.`booking_time`, `order_info`.`delivered_user_id`, (SELECT `user_info`.`user_name` FROM `user_info` WHERE `order_info`.`delivered_user_id` = `user_info`.`user_table_id`) as 'delivered_user', `order_info`.`delivered_date`, `order_info`.`delivered_time` FROM `order_info` LEFT OUTER JOIN `dealer_info` ON `dealer_info`.`dealer_table_id` = `order_info`.`dealer_id` WHERE `order_info`.`order_id` = '"+orderId+"'";
//        System.out.println(query);
        try {
            rs = stmt.executeQuery(query);
            while (rs.next())
            {
                saleBasicData.add(rs.getString(1)); // Invoice No
                saleBasicData.add(rs.getString(2)); // Dealer Name
                saleBasicData.add(rs.getString(3)); // Dealer Contact
                saleBasicData.add(rs.getString(4)); // Invoice Price
                saleBasicData.add(rs.getString(5)); // Discount Given
                saleBasicData.add(rs.getString(6)); // Status
                saleBasicData.add(rs.getString(8)+" ("+rs.getString(7)+")"); // Booking User
                saleBasicData.add(rs.getString(9)); // Booking Date
                saleBasicData.add(rs.getString(10)); // Booking Time
                saleBasicData.add(rs.getString(12)+" ("+rs.getString(11)+")"); // Delivered User
                saleBasicData.add(rs.getString(13)); // Delivered Date
                saleBasicData.add(rs.getString(14)); // Delivered Time
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return saleBasicData;
    }

    public ArrayList<ArrayList<String>> getSaleDetailInfo(Statement stmt, Connection con)
    {
        ArrayList<ArrayList<String>> saleData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        String query = "SELECT `order_info_detailed`.`product_table_id`, `product_info`.`product_name`, `order_info_detailed`.`batch_id`, `batchwise_stock`.`batch_no`, `batchwise_stock`.`batch_expiry`, `order_info_detailed`.`quantity`, `order_info_detailed`.`submission_quantity`, `order_info_detailed`.`bonus_quant`, `order_info_detailed`.`order_price`, `order_info_detailed`.`final_price`, `order_info_detailed`.`discount` FROM `order_info_detailed` INNER JOIN `order_info` ON `order_info`.`order_id` = `order_info_detailed`.`order_id` AND `order_info`.`order_id` = '"+orderId+"' LEFT OUTER JOIN `product_info` ON `product_info`.`product_table_id` = `order_info_detailed`.`product_table_id` LEFT OUTER JOIN `batchwise_stock` ON `batchwise_stock`.`batch_stock_id` = `order_info_detailed`.`batch_id` ORDER BY `order_info_detailed`.`product_table_id`";
        try {
            rs = stmt.executeQuery(query);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Product Id
                productIdArr.add(rs.getString(1));
                temp.add(rs.getString(2)); // Product Name
                productNameArr.add(rs.getString(2));
                temp.add(rs.getString(3)); // Batch Id
                batchIdArr.add(rs.getString(3));
                temp.add(rs.getString(4)); // Batch No
                batchNoArr.add(rs.getString(4));
                temp.add(rs.getString(5)); // Batch Expiry
                batchExpiryArr.add(rs.getString(5));
                temp.add(rs.getString(6)); // Qty
                orderedQuantityArr.add(rs.getString(6));
//                unitArr.add("Packet");
                temp.add(rs.getString(7)); // Submission Qty
                submissionQuantityArr.add(rs.getString(7));
                temp.add(rs.getString(8)); // Bonus Quantity
                bonusQuantityArr.add(rs.getString(8));
                temp.add(rs.getString(9)); // Product Price
                productPriceArr.add(rs.getString(9));
                temp.add(rs.getString(10)); // Final Price
                finalPriceArr.add(rs.getString(10));
                temp.add(rs.getString(11)); // Discount Given
                discountGivenArr.add(rs.getString(11));
                saleData.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return saleData;
    }

    public ArrayList<ArrayList<String>> loadSaleTable()
    {
        ArrayList<ArrayList<String>> saleData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        for(int i=0; i<productIdArr.size(); i++)
        {
            temp = new ArrayList<String>();
            temp.add(productNameArr.get(i));
            temp.add(batchNoArr.get(i));
            temp.add(batchExpiryArr.get(i));


            if(!orderedQuantityArr.get(i).equals(submissionQuantityArr.get(i)))
            {
                temp.add(orderedQuantityArr.get(i)+"\n(Qty in Stock: "+submissionQuantityArr.get(i)+")");
            }
            else
            {
                temp.add(orderedQuantityArr.get(i));
            }

            temp.add(bonusQuantityArr.get(i));
            temp.add(finalPriceArr.get(i));
            temp.add(discountGivenArr.get(i));
            saleData.add(temp);
        }
        return saleData;
    }

    public ArrayList<ArrayList<String>> loadInvoiceItems()
    {
        ArrayList<ArrayList<String>> saleData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        for(int i=0; i<productIdArr.size(); i++)
        {
            if(!submissionQuantityArr.get(i).equals("0"))
            {
                temp = new ArrayList<String>();
                temp.add(productIdArr.get(i));
                if(productNameArr.get(i) != null)
                {
                    temp.add(productNameArr.get(i));
                }
                else
                {
                    temp.add("N/A");
                }
                temp.add("20");
                temp.add(batchNoArr.get(i));
                String itemRate = String.valueOf(Float.parseFloat(productPriceArr.get(i))/Float.parseFloat(submissionQuantityArr.get(i)));
                temp.add(itemRate);
                temp.add(submissionQuantityArr.get(i));
                temp.add(bonusQuantityArr.get(i));
                temp.add(productPriceArr.get(i));
                temp.add("0");
                temp.add(discountGivenArr.get(i));
                temp.add(finalPriceArr.get(i));
                saleData.add(temp);
            }
        }
        return saleData;
    }

    public String getBatchPolicy(Statement stmt, Connection con)
    {
        String batchPolicy = "";
        ArrayList<String> temp;
        ResultSet rs = null;
        String query = "SELECT `setting_value` FROM `system_settings` WHERE `setting_name` = 'batch_policy'";
//        System.out.println(query);
        try {
            rs = stmt.executeQuery(query);
            while (rs.next())
            {
                batchPolicy = rs.getString(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return batchPolicy;
    }

    public void updateInvoice(Statement stmt, Connection con)
    {
        String currentDate = GlobalVariables.getStDate();
        String currentTime = GlobalVariables.getStTime();
        String currentUser = GlobalVariables.getUserId();
        String updateProductStock = "";
        String updateBatchStock = "";
        String updateQuery = "INSERT INTO `order_info_detailed`(`order_id`, `product_id`, `batch_id`, `quantity`, `submission_quantity`, `unit`, `submission_unit`, `order_price`, `bonus_quant`, `discount`, `final_price`, `cmplt_returned_bit`, `returned_quant`, `returned_bonus_quant`) VALUES ";
        String deleteQuery = "DELETE FROM `order_info_detailed` WHERE `order_id` = '"+orderId+"'";
        String unit = "Packets";
        try {
            stmt.executeUpdate(deleteQuery);
            for(int i=0; i<preProductIdArr.size(); i++)
            {
                updateProductStock = "UPDATE `products_stock` SET `in_stock` = `in_stock` + '"+preSubmissionQuantityArr.get(i)+"' WHERE `product_id` = '"+preProductIdArr.get(i)+"'; ";
                updateBatchStock = "UPDATE `batchwise_stock` SET `quantity` = `quantity` + '"+preSubmissionQuantityArr.get(i)+"' WHERE `prod_id` = '"+preProductIdArr.get(i)+"' AND `batch_no` = '"+preBatchNoArr.get(i)+"';";
                stmt.executeUpdate(updateProductStock);
                stmt.executeUpdate(updateBatchStock);
            }
            for(int i=0; i<productIdArr.size(); i++)
            {
                if(i == productIdArr.size()-1)
                    updateQuery += "('"+orderId+"','"+productIdArr.get(i)+"','"+batchIdArr.get(i)+"','"+orderedQuantityArr.get(i)+"','"+submissionQuantityArr.get(i)+"','"+unit+"','"+unit+"','"+productPriceArr.get(i)+"','"+bonusQuantityArr.get(i)+"','"+discountGivenArr.get(i)+"','"+finalPriceArr.get(i)+"','0','0','0') ";
                else
                    updateQuery += "('"+orderId+"','"+productIdArr.get(i)+"','"+batchIdArr.get(i)+"','"+orderedQuantityArr.get(i)+"','"+submissionQuantityArr.get(i)+"','"+unit+"','"+unit+"','"+productPriceArr.get(i)+"','"+bonusQuantityArr.get(i)+"','"+discountGivenArr.get(i)+"','"+finalPriceArr.get(i)+"','0','0','0'), ";

                updateProductStock = "UPDATE `products_stock` SET `in_stock` = `in_stock` - '"+submissionQuantityArr.get(i)+"' WHERE `product_id` = '"+productIdArr.get(i)+"'; ";
                updateBatchStock = "UPDATE `batchwise_stock` SET `quantity` = `quantity` - '"+submissionQuantityArr.get(i)+"' WHERE `prod_id` = '"+productIdArr.get(i)+"' AND `batch_no` = '"+batchNoArr.get(i)+"';";
                stmt.executeUpdate(updateProductStock);
                stmt.executeUpdate(updateBatchStock);
            }
//            updateQuery = "UPDATE `dealer_info` SET `dealer_id`='"+dealerId+"',`dealer_area_id`='"+areaId+"',`dealer_name`='"+dealerName+"',`dealer_contact`='"+dealerContact+"',`dealer_address`='"+dealerAddress+"',`dealer_type`='"+dealerType+"',`dealer_cnic`='"+dealerCnic+"',`dealer_lic_num`='"+dealerLicNum+"',`dealer_lic_exp`='"+dealerLicExp+"',`dealer_status`='"+dealerStatus+"', `update_user_id`='"+currentUser+"',`update_date`='"+currentDate+"',`update_time`='"+currentTime+"' WHERE `dealer_table_id` = '"+dealerTableId+"'";

//            System.out.println(updateQuery);
            stmt.executeUpdate(updateQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateBasic(Statement stmt, Connection con, String dealerId, String orderPrice, String discount, String waivedOffPrice, String finalPrice, String bookingUserId, String bookingDate, String bookingTime, String deliveredUserId, String deliveredDate, String deliveredTime, String orderStatus)
    {
        String currentDate = GlobalVariables.getStDate();
        String currentTime = GlobalVariables.getStTime();
        String currentUser = GlobalVariables.getUserId();
        String updateQuery;
        String unit = "";
        String productId = "";
        String quantity = "";
        String bonus = "";
//        String orderPrice = "0";
//        String discount = "0";
//        String finalPrice = "0";
        for(int i=0; i<productIdArr.size(); i++)
        {
            if(i == productIdArr.size()-1)
            {
                productId += productIdArr;
                quantity += submissionQuantityArr;
                bonus += bonusQuantityArr;
                unit += "Packets";
            }
            else
            {
                productId += productIdArr+"_-_";
                quantity += submissionQuantityArr+"_-_";
                bonus += bonusQuantityArr+"_-_";
                unit += "Packets_-_";
            }

        }
        try {
            updateQuery = "UPDATE `order_info` SET `dealer_id`='"+dealerId+"',`product_id`='"+productId+"',`quantity`='"+quantity+"',`unit`='"+unit+"',`order_price`='"+orderPrice+"',`bonus`='"+bonus+"',`discount`='"+discount+"',`waive_off_price`='"+waivedOffPrice+"',`final_price`='"+finalPrice+"',`order_success`=[value-11],`booking_latitude`=[value-12],`booking_longitude`=[value-13],`booking_area`=[value-14],`booking_date`=[value-15],`booking_time`=[value-16],`booking_user_id`=[value-17],`delivered_latitude`=[value-18],`delivered_longitude`=[value-19],`delivered_area`=[value-20],`delivered_date`=[value-21],`delivered_time`=[value-22],`delivered_user_id`=[value-23],`update_dates`=[value-24],`update_times`=[value-25],`update_user_id`=[value-26],`status`=[value-27] WHERE `order_id` = ''";
            stmt.executeUpdate(updateQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private Node createEditIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.EDIT);
        graphic.setFontSize(20.0);
        return graphic;
    }

    private Node createDeleteIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.TRASH);
        graphic.setFontSize(20.0);
        return graphic;
    }
}
