package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import controller.DeleteWindow;
import controller.UpdateArea;
import controller.UpdateCity;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ViewAreaInfo {
    private String srNo;
    private String areaTableId;
    private String areaId;
    private String cityId;
    private String cityName;
    private String areaName;
    private String dealersPresent;
    private String areaStatus;

    private HBox operationsPane;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    public static StackPane stackPane;
    public static JFXDialog dialog;
    public static Label lblUpdate;

    public ViewAreaInfo()
    {
        this.srNo = "";
        this.areaTableId = "";
        this.areaId = "";
        this.cityId = "";
        this.cityName = "";
        this.areaName = "";
        this.dealersPresent = "";
        this.areaStatus = "";

        this.btnEdit = new JFXButton("Edit");
        this.btnDelete = new JFXButton("Delete");
        this.operationsPane = new HBox(btnEdit, btnDelete);
        lblUpdate = new Label("");
    }

    public ViewAreaInfo(String srNo, String areaTableId, String areaId, String cityId, String cityName, String areaName, String dealersPresent, String areaStatus) {
        this.srNo = srNo;
        this.areaTableId = areaTableId;
        this.areaId = areaId;
        this.cityId = cityId;
        this.cityName = cityName;
        this.areaName = areaName;
        this.dealersPresent = dealersPresent;
        this.areaStatus = areaStatus;

        this.btnEdit = new JFXButton();
        this.btnDelete = new JFXButton();
        this.btnEdit.setGraphic(createEditIcon());
        this.btnDelete.setGraphic(createDeleteIcon());
        this.btnEdit.getStyleClass().add("btn_icon");
        this.btnDelete.getStyleClass().add("btn_icon");
        this.operationsPane = new HBox(btnEdit, btnDelete);
        lblUpdate = new Label("");

        this.btnEdit.setOnAction((event)->editClicked());
        this.btnDelete.setOnAction(event -> {
            try {
                deleteArea(this.areaTableId);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void editClicked()
    {
        Parent parent = null;
        try {
            UpdateArea.areaTableId = areaTableId;
            UpdateArea.areaId = areaId;
            UpdateArea.areaName = areaName;
            UpdateArea.cityId = cityId;
            UpdateArea.cityName = cityName;
            UpdateArea.areaStatus = areaStatus;
            parent = FXMLLoader.load(getClass().getResource("../view/update_area.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setStyle("-fx-padding: -20 -10 -20 -10;");
        dialogLayout.setHeading(lblUpdate);
        dialogLayout.setBody(parent);
        dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        stackPane.setVisible(true);
        dialog.show();
        dialog.setOverlayClose(false);
    }

    public void deleteArea(String areaTableId) throws IOException {
        DeleteWindow.sceneWindow = "ViewArea";
        DeleteWindow.deletionId = areaTableId;
        Parent parent = null;
        try {
            parent = FXMLLoader.load(getClass().getResource("../view/delete_window.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setStyle("-fx-padding: -40 -20 -40 -20;");
        dialogLayout.setBody(parent);
        dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        stackPane.setVisible(true);
        dialog.show();
        dialog.setOverlayClose(false);
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getAreaTableId() {
        return areaTableId;
    }

    public void setAreaTableId(String areaTableId) {
        this.areaTableId = areaTableId;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getDealersPresent() {
        return dealersPresent;
    }

    public void setDealersPresent(String dealersPresent) {
        this.dealersPresent = dealersPresent;
    }

    public String getAreaStatus() {
        return areaStatus;
    }

    public void setAreaStatus(String areaStatus) {
        this.areaStatus = areaStatus;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public ArrayList<ArrayList<String>> getAreasInfo(Statement stmt, Connection con)
    {
        ArrayList<ArrayList<String>> areaData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT `area_info`.`area_table_id`, `area_info`.`area_id`, `area_info`.`city_table_id`, `city_info`.`city_name`, `area_info`.`area_name`, COUNT(`dealer_info`.`dealer_table_id`) as 'dealers_present', `area_info`.`area_status` FROM `area_info` LEFT OUTER JOIN `city_info` ON `area_info`.`city_table_id` = `city_info`.`city_table_id` LEFT OUTER JOIN `dealer_info` ON `dealer_info`.`dealer_area_id` = `area_info`.`area_table_id` WHERE `area_info`.`area_status` != 'Deleted' GROUP BY `area_info`.`area_table_id`");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2));
                temp.add(rs.getString(3));
                temp.add(rs.getString(4));
                temp.add(rs.getString(5));
                temp.add(rs.getString(6));
                temp.add(rs.getString(7));
                areaData.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return areaData;
    }

    public ArrayList<ArrayList<String>> getAreasSearch(Statement stmt, Connection con, String areaId, String areaName, String cityId, String areaStatus)
    {
        ArrayList<ArrayList<String>> areaData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        String searchQuery;
        int multipleSearch = 0;
        searchQuery = "SELECT `area_info`.`area_table_id`, `area_info`.`area_id`, `area_info`.`city_table_id`, `city_info`.`city_name`, `area_info`.`area_name`, COUNT(`dealer_info`.`dealer_table_id`) as 'dealers_present', `area_info`.`area_status` FROM `area_info` LEFT OUTER JOIN `city_info` ON `area_info`.`city_table_id` = `city_info`.`city_table_id` LEFT OUTER JOIN `dealer_info` ON `dealer_info`.`dealer_area_id` = `area_info`.`area_table_id` WHERE ";
        if(!areaId.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`area_info`.`area_id` = '"+areaId+"'";
            multipleSearch++;
        }
        if(!areaName.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`area_info`.`area_name` = '"+areaName+"'";
            multipleSearch++;
        }
        if(!cityId.equals("All"))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`area_info`.`city_table_id` = '"+cityId+"'";
            multipleSearch++;
        }
        if(!areaStatus.equals("All"))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`area_info`.`area_status` = '"+areaStatus+"'";
            multipleSearch++;
        }
        if(multipleSearch >= 1)
        {
            searchQuery += " AND ";
        }
        searchQuery += "`area_info`.`area_status` != 'Deleted' GROUP BY `area_info`.`area_table_id`";
        try {
            rs = stmt.executeQuery(searchQuery);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2));
                temp.add(rs.getString(3));
                temp.add(rs.getString(4));
                temp.add(rs.getString(5));
                temp.add(rs.getString(6));
                temp.add(rs.getString(7));
                areaData.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return areaData;
    }

    public ArrayList<String> getAreasSummary(Statement stmt, Connection con)
    {
        ArrayList<String> groupData = new ArrayList<String>();
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT COUNT(`area_table_id`) FROM `area_info` WHERE `area_status` != 'Deleted'");
            while (rs.next())
            {
                groupData.add(rs.getString(1));
            }
            rs = stmt.executeQuery("SELECT COUNT(`area_table_id`) FROM `area_info` WHERE `area_status` = 'Active'");
            while (rs.next())
            {
                groupData.add(rs.getString(1));
            }
            rs = stmt.executeQuery("SELECT COUNT(`area_table_id`) FROM `area_info` WHERE `area_status` = 'In Active'");
            while (rs.next())
            {
                groupData.add(rs.getString(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return groupData;
    }

    private Node createEditIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.EDIT);
        graphic.setFontSize(20.0);
        return graphic;
    }

    private Node createDeleteIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.TRASH);
        graphic.setFontSize(20.0);
        return graphic;
    }
}
