package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import controller.UpdateCompanyFinance;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class CompanyDetailInfo {
    private String srNo;
    private String productTableId;
    private String productId;
    private String productName;
    private String productType;
    private String retailPrice;
    private String tradePrice;
    private String productStatus;

    private HBox operationsPane;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    public static StackPane stackPane;
    public static JFXDialog dialog;

    public CompanyDetailInfo() {
    }

    public CompanyDetailInfo(String srNo, String productTableId, String productId, String productName, String productType, String retailPrice, String tradePrice, String productStatus) {
        this.srNo = srNo;
        this.productTableId = productTableId;
        this.productId = productId;
        this.productName = productName;
        this.productType = productType;
        this.retailPrice = retailPrice;
        this.tradePrice = tradePrice;
        this.productStatus = productStatus;

        this.btnEdit = new JFXButton();
        this.btnDelete = new JFXButton();
        this.btnEdit.setGraphic(createEditIcon());
        this.btnDelete.setGraphic(createDeleteIcon());
        this.btnEdit.getStyleClass().add("btn_icon");
        this.btnDelete.getStyleClass().add("btn_icon");
//        this.btnEdit.setOnAction(event -> editClicked());
//        this.btnDelete.setOnAction(event -> deleteDealerFinance(this.paymentId));
        this.operationsPane = new HBox(btnEdit, btnDelete);
    }



//    public void deleteDealerFinance(String paymentId) {
//        MysqlCon objMysqlCon = new MysqlCon();
//        Statement objStmt = objMysqlCon.stmt;
//        Connection objCon = objMysqlCon.con;
//        try {
//            String updateQuery = "UPDATE `dealer_payments` SET `status`= 'Deleted' WHERE `payment_id` = '"+paymentId+"'";
//            objStmt.executeUpdate(updateQuery);
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        CompanyFinanceInfo.companyFinanceId = companyId;
//        Parent root = null;
//        try {
//            root = FXMLLoader.load(getClass().getResource("../view/company_finance_detail.fxml"));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        GlobalVariables.baseScene.setRoot(root);
//        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
//    }
//
//    public void editClicked()
//    {
//        Parent parent = null;
//        CompanyFinanceInfo.companyFinanceId = companyId;
//        try {
//            UpdateCompanyFinance.paymentId = paymentId;
//            UpdateCompanyFinance.companyId = companyId;
//            UpdateCompanyFinance.companyName = companyName;
//            UpdateCompanyFinance.date = date;
//            if(!cashSent.equals("-"))
//            {
//                UpdateCompanyFinance.preCashAmount = cashSent;
//                UpdateCompanyFinance.cashAmount = cashSent;
//                UpdateCompanyFinance.preCashType = "Sent";
//                UpdateCompanyFinance.cashType = "Sent";
//            }
//            else if(!cashReturn.equals("-"))
//            {
//                UpdateCompanyFinance.preCashAmount = cashReturn;
//                UpdateCompanyFinance.cashAmount = cashReturn;
//                UpdateCompanyFinance.preCashType = "Return";
//                UpdateCompanyFinance.cashType = "Return";
//            }
//            else if(!cashWaiveOff.equals("-"))
//            {
//                UpdateCompanyFinance.preCashAmount = cashWaiveOff;
//                UpdateCompanyFinance.cashAmount = cashWaiveOff;
//                UpdateCompanyFinance.preCashType = "Waive Off";
//                UpdateCompanyFinance.cashType = "Waive Off";
//            }
//
//            parent = FXMLLoader.load(getClass().getResource("../view/update_company_finance.fxml"));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        JFXDialogLayout dialogLayout = new JFXDialogLayout();
//        dialogLayout.setStyle("-fx-padding: -20 -10 -20 -10;");
//        dialogLayout.setBody(parent);
//        dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);
//
//        stackPane.setVisible(true);
//        dialog.show();
//        dialog.setOverlayClose(false);
//    }


    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getProductTableId() {
        return productTableId;
    }

    public void setProductTableId(String productTableId) {
        this.productTableId = productTableId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getRetailPrice() {
        return retailPrice;
    }

    public void setRetailPrice(String retailPrice) {
        this.retailPrice = retailPrice;
    }

    public String getTradePrice() {
        return tradePrice;
    }

    public void setTradePrice(String tradePrice) {
        this.tradePrice = tradePrice;
    }

    public String getProductStatus() {
        return productStatus;
    }

    public void setProductStatus(String productStatus) {
        this.productStatus = productStatus;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public ArrayList<ArrayList<String>> getCompanyDetail(Statement stmt, Connection con, String companyId)
    {
        ArrayList<ArrayList<String>> companyProductsData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        String query = "SELECT `product_table_id`, `product_id`, `product_name`, `product_type`, `retail_price`, `trade_price`, `product_status` FROM `product_info` WHERE `company_id` = '"+companyId+"' AND `product_status` != 'Deleted'";
        try {
            rs = stmt.executeQuery(query);
//            System.out.println(query);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Product Tb Id
                temp.add(rs.getString(2)); // Product Id
                temp.add(rs.getString(3)); // Product Name
                temp.add(rs.getString(4)); // Type
                temp.add(rs.getString(5)); // Retail Price
                temp.add(rs.getString(6)); // Trade Price
                temp.add(rs.getString(7)); // Product Status
                companyProductsData.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return companyProductsData;
    }

    public ArrayList<ArrayList<String>> getCompanyDetailSearch(Statement stmt, Connection con, String companyId, String productId, String productName, String type, String fromAmount, String toAmount, String status)
    {
        ArrayList<ArrayList<String>> companyProductsData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        String searchQuery;
        int multipleSearch = 0;
        searchQuery = "SELECT `product_table_id`, `product_id`, `product_name`, `product_type`, `retail_price`, `trade_price`, `product_status` FROM `product_info` WHERE ";
        if(!productId.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`product_id` = '"+productId+"'";
            multipleSearch++;
        }
        if(!productName.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`product_name` = '"+productName+"'";
            multipleSearch++;
        }
        if(!type.equals("All"))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`product_type` = '"+type+"'";
            multipleSearch++;
        }
        if(!fromAmount.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`retail_price` >= '"+fromAmount+"'";
            multipleSearch++;
        }
        if(!toAmount.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`retail_price` <= '"+toAmount+"'";
            multipleSearch++;
        }
        if(!status.equals("All"))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`product_status` = '"+status+"'";
            multipleSearch++;
        }
        if(multipleSearch >= 1)
        {
            searchQuery += " AND ";
        }
        searchQuery += "`company_id` = '"+companyId+"' AND `product_status` != 'Deleted'";
        try {
            rs = stmt.executeQuery(searchQuery);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Product Tb Id
                temp.add(rs.getString(2)); // Product Id
                temp.add(rs.getString(3)); // Product Name
                temp.add(rs.getString(4)); // Type
                temp.add(rs.getString(5)); // Retail Price
                temp.add(rs.getString(6)); // Trade Price
                temp.add(rs.getString(7)); // Product Status
                companyProductsData.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return companyProductsData;
    }

    public void updateCompanyFinance(Statement stmt, Connection con, String paymentId, String companyId, String preCashAmount, String preCashType, String date, String day, String amount, String cashType)
    {
        String currentDate = GlobalVariables.getStDate();
        String currentTime = GlobalVariables.getStTime();
        String currentUser = GlobalVariables.getUserId();
        String updateQuery = null;
        try {
            if(preCashType.equals("Sent"))
            {
                updateQuery = "UPDATE `company_overall_record` SET `cash_sent`= `cash_sent` - '"+preCashAmount+"', `pending_payments` = `pending_payments` + '"+preCashAmount+"' WHERE `company_id` = '"+companyId+"'";
            }
            else if(preCashType.equals("Return"))
            {
                updateQuery = "UPDATE `company_overall_record` SET `cash_return`= `cash_return` - '"+preCashAmount+"', `pending_payments` = `pending_payments` - '"+preCashAmount+"' WHERE `company_id` = '"+companyId+"'";
            }
            else if(preCashType.equals("Waive Off"))
            {
                updateQuery = "UPDATE `company_overall_record` SET `waived_off_price`= `waived_off_price` - '"+preCashAmount+"', `pending_payments` = `pending_payments` + '"+preCashAmount+"' WHERE `company_id` = '"+companyId+"'";
            }
            stmt.executeUpdate(updateQuery);

            updateQuery = "UPDATE `company_payments` SET `date`='"+date+"', `day`='"+day+"', `amount`='"+amount+"',`cash_type`='"+cashType+"', `user_id`='"+currentUser+"' WHERE `payment_id` = '"+paymentId+"'";
            stmt.executeUpdate(updateQuery);

            if(cashType.equals("Sent"))
            {
                updateQuery = "UPDATE `company_overall_record` SET `cash_sent`= `cash_sent` + '"+amount+"', `pending_payments` = `pending_payments` - '"+amount+"' WHERE `company_id` = '"+companyId+"'";
            }
            else if(cashType.equals("Return"))
            {
                updateQuery = "UPDATE `company_overall_record` SET `cash_return`= `cash_return` + '"+amount+"', `pending_payments` = `pending_payments` + '"+amount+"' WHERE `company_id` = '"+companyId+"'";
            }
            else if(cashType.equals("Waive Off"))
            {
                updateQuery = "UPDATE `company_overall_record` SET `waived_off_price`= `waived_off_price` + '"+amount+"', `pending_payments` = `pending_payments` - '"+amount+"' WHERE `company_id` = '"+companyId+"'";
            }
            stmt.executeUpdate(updateQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private Node createEditIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.EDIT);
        graphic.setFontSize(20.0);
        return graphic;
    }

    private Node createDeleteIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.TRASH);
        graphic.setFontSize(20.0);
        return graphic;
    }
}
