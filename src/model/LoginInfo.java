package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class LoginInfo {

    MysqlCon objMysqlCon;
    Statement objStmt;
    Connection objCon;

    public LoginInfo()
    {
        objMysqlCon = new MysqlCon();
        objStmt = objMysqlCon.stmt;
        objCon = objMysqlCon.con;
    }

    public ArrayList<String> checkLogin(String userName, String password)
    {
        ArrayList<String> userDetail = new ArrayList<>();
        ResultSet rs = null;
        String query = "SELECT `user_accounts`.`user_id`, `user_info`.`user_image` FROM `user_accounts` INNER JOIN `user_info` ON `user_info`.`user_table_id` = `user_accounts`.`user_id` WHERE `user_accounts`.`user_name` = '"+userName +"' AND `user_accounts`.`user_password` = '"+password+"'";
        try {
            rs = objStmt.executeQuery(query);
            if(rs.next())
            {
                userDetail.add(rs.getString(1)); // User Id
                userDetail.add(rs.getString(2)); // User Image
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userDetail;
    }
}
