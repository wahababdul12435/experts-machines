package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import controller.EnterProduct;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class ProductInfo {
    private String srNo;
    private String productId;
    private String companyName;
    private String groupName;
    private String productName;
    private String productType;
    private String purchasePrice;
    private String retailPrice;

    private HBox operationsPane;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    public static ArrayList<String> productIdArr = new ArrayList<>();
    public static ArrayList<String> companyIdArr = new ArrayList<>();
    public static ArrayList<String> companyNameArr = new ArrayList<>();
    public static ArrayList<String> groupIdArr = new ArrayList<>();
    public static ArrayList<String> groupNameArr = new ArrayList<>();
    public static ArrayList<String> productNameArr = new ArrayList<>();
    public static ArrayList<String> productTypeArr = new ArrayList<>();
    public static ArrayList<String> retailPriceArr = new ArrayList<>();
    public static ArrayList<String> tradePriceArr = new ArrayList<>();
    public static ArrayList<String> purchasePriceArr = new ArrayList<>();
    public static ArrayList<String> purchaseDiscountArr = new ArrayList<>();
    public static ArrayList<String> productPackingArr = new ArrayList<>();
    public static ArrayList<String> cartonPackingArr = new ArrayList<>();
    public static ArrayList<String> salesTaxArr = new ArrayList<>();
    public static ArrayList<String> holdStockArr = new ArrayList<>();
    public static ArrayList<String> maxStockArr = new ArrayList<>();
    public static ArrayList<String> minStockArr = new ArrayList<>();

    public static TableView<ProductInfo> table_add_product;

    public static JFXTextField txtProductId;
    public static JFXComboBox<String> txtCompanyName;
    public static JFXComboBox<String> txtGroupName;
    public static JFXTextField txtProductName;
    public static JFXComboBox<String> txtProductType;
    public static JFXTextField txtRetailPrice;
    public static JFXTextField txtTradePrice;
    public static JFXTextField txtPurchasePrice;
    public static JFXTextField txtPurchaseDiscount;
    public static JFXTextField txtProductPacking;
    public static JFXTextField txtCartonPacking;
    public static JFXTextField txtSalesTax;
    public static JFXTextField txtHoldStock;
    public static JFXTextField txtMaxStock;
    public static JFXTextField txtMinStock;

    public static Button btnAdd;
    public static Button btnCancel;
    public static Button btnUpdate;

    public ArrayList<ArrayList<String>> companiesData = new ArrayList<>();
    public ArrayList<ArrayList<String>> groupsData = new ArrayList<>();


    MysqlCon objMysqlCon = new MysqlCon();
    Statement objStmt = objMysqlCon.stmt;
    Connection objCon = objMysqlCon.con;

    public ProductInfo() {
        srNo = "";
        productId = "";
        companyName = "";
        groupName = "";
        productName = "";
        productType = "";
        purchasePrice = "";
        retailPrice = "";
    }

    public ProductInfo(String srNo, String productId, String companyName, String groupName, String productName, String productType, String purchasePrice, String retailPrice) {
        this.srNo = srNo;
        this.productId = productId;
        this.companyName = companyName;
        this.groupName = groupName;
        this.productName = productName;
        this.productType = productType;
        this.purchasePrice = purchasePrice;
        this.retailPrice = retailPrice;

        this.btnEdit = new JFXButton("Edit");
        this.btnDelete = new JFXButton("Delete");
        this.btnEdit.setOnAction((action)->editAddedRecord());
        this.btnDelete.setOnAction((action)->deleteAddedRecord());
        this.operationsPane = new HBox(btnEdit, btnDelete);
    }

    private void deleteAddedRecord()
    {
        productIdArr.remove(Integer.parseInt(srNo)-1);
        companyIdArr.remove(Integer.parseInt(srNo)-1);
        companyNameArr.remove(Integer.parseInt(srNo)-1);
        groupIdArr.remove(Integer.parseInt(srNo)-1);
        groupNameArr.remove(Integer.parseInt(srNo)-1);
        productNameArr.remove(Integer.parseInt(srNo)-1);
        productTypeArr.remove(Integer.parseInt(srNo)-1);
        retailPriceArr.remove(Integer.parseInt(srNo)-1);
        tradePriceArr.remove(Integer.parseInt(srNo)-1);
        purchasePriceArr.remove(Integer.parseInt(srNo)-1);
        purchaseDiscountArr.remove(Integer.parseInt(srNo)-1);
        productPackingArr.remove(Integer.parseInt(srNo)-1);
        cartonPackingArr.remove(Integer.parseInt(srNo)-1);
        salesTaxArr.remove(Integer.parseInt(srNo)-1);
        holdStockArr.remove(Integer.parseInt(srNo)-1);
        maxStockArr.remove(Integer.parseInt(srNo)-1);
        minStockArr.remove(Integer.parseInt(srNo)-1);
        table_add_product.setItems(EnterProduct.parseUserList());
    }

    public void editAddedRecord()
    {
        EnterProduct.srNo = Integer.parseInt(srNo)-1;
        txtProductId.setText(productIdArr.get(Integer.parseInt(srNo)-1));
        txtCompanyName.setValue(companyNameArr.get(Integer.parseInt(srNo)-1));
        txtGroupName.setValue(groupNameArr.get(Integer.parseInt(srNo)-1));
        txtProductName.setText(productNameArr.get(Integer.parseInt(srNo)-1));
        txtProductType.setValue(productTypeArr.get(Integer.parseInt(srNo)-1));
        txtRetailPrice.setText(retailPriceArr.get(Integer.parseInt(srNo)-1));
        txtTradePrice.setText(tradePriceArr.get(Integer.parseInt(srNo)-1));
        txtPurchasePrice.setText(purchasePriceArr.get(Integer.parseInt(srNo)-1));
        txtPurchaseDiscount.setText(purchaseDiscountArr.get(Integer.parseInt(srNo)-1));
        txtProductPacking.setText(productPackingArr.get(Integer.parseInt(srNo)-1));
        txtSalesTax.setText(salesTaxArr.get(Integer.parseInt(srNo)-1));
        txtHoldStock.setText(holdStockArr.get(Integer.parseInt(srNo)-1));
        txtMaxStock.setText(maxStockArr.get(Integer.parseInt(srNo)-1));
        txtMinStock.setText(minStockArr.get(Integer.parseInt(srNo)-1));
        btnAdd.setVisible(false);
        btnCancel.setVisible(true);
        btnUpdate.setVisible(true);
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(String purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public String getRetailPrice() {
        return retailPrice;
    }

    public void setRetailPrice(String retailPrice) {
        this.retailPrice = retailPrice;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public void insertProduct(Statement stmt, Connection con, String prodctID , String companyID, String groupID, String productName, String productType, String retailPrice, String tradePrice, String purchasePrice, String purchaseDiscount, String productStatus)
    {
        try {
            float purchasePriceFloat = Float.parseFloat(purchasePrice);
            float purchaseDiscountFloat = Float.parseFloat(purchaseDiscount);
            float finalPrice = purchasePriceFloat - ((purchasePriceFloat/100) * purchaseDiscountFloat);
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
            LocalDateTime now = LocalDateTime.now();
            String currentDate = dtf.format(now);
            DateTimeFormatter dtf1 = DateTimeFormatter.ofPattern("HH:mm:ss");
            LocalDateTime nowtime = LocalDateTime.now();
            String currentTime = dtf1.format(nowtime);
            int currentUser = Integer.parseInt(GlobalVariables.getUserId());
            ResultSet rs = null;
            String insertQuery = "INSERT INTO `product_info`(`products_ID`,`company_id`, `group_id`, `product_name`, `product_type`, `retail_price`, `trade_price`, `purchase_price`, `purchase_discount`, `final_price`, `product_status` , `entry_date`, `entry_time`, `update_date`, `update_time`, `user_id`) VALUES ('"+prodctID+"','"+companyID+"','"+groupID+"','"+productName+"','"+productType+"','"+retailPrice+"','"+tradePrice+"','"+purchasePrice+"','"+purchaseDiscount+"','"+finalPrice+"','"+productStatus+"' ,'"+currentDate+"','"+currentTime+"','"+currentDate+"', '"+currentTime+"' ,'"+currentUser+"')";
            stmt.executeUpdate(insertQuery);

            String insertProductStock = "INSERT INTO `products_stock`(`product_id`, `in_stock`, `bonus_quant`, `status`) VALUES ('"+prodctID+"','0','0','"+productStatus+"')";
            stmt.executeUpdate(insertProductStock);
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertProduct(Statement stmt, Connection con) throws SQLException {
        String currentDate = GlobalVariables.getStDate();
        String currentTime = GlobalVariables.getStTime();
        String currentUser = GlobalVariables.getUserId();
        boolean withId = false;
        boolean withoutId = false;
        String insertQuery;
        String insertProductStock;
        String productTableId;
        int insertInfo = 1;

        for(int i=0; i<productIdArr.size(); i++)
        {
            if(productIdArr.get(i) == null || productIdArr.get(i).equals(""))
            {
                if(groupIdArr.get(i) == null || groupIdArr.get(i).equals(""))
                {
                    insertQuery = "INSERT INTO `product_info`(`company_table_id`, `product_name`, `product_type`, `packSize`,`carton_size`, `retail_price`, `trade_price`, `purchase_price`, `purchase_discount`, `sales_tax`, `product_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`) VALUES ('"+companyIdArr.get(i)+"','"+productNameArr.get(i)+"','"+productTypeArr.get(i)+"','"+productPackingArr.get(i)+"','"+cartonPackingArr.get(i)+"','"+retailPriceArr.get(i)+"','"+tradePriceArr.get(i)+"','"+purchasePriceArr.get(i)+"','"+purchaseDiscountArr.get(i)+"','"+salesTaxArr.get(i)+"','Active','"+currentUser+"','"+currentDate+"','"+currentTime+"','"+currentUser+"','"+currentDate+"','"+currentTime+"')";
                }
                else
                {
                    insertQuery = "INSERT INTO `product_info`(`company_table_id`, `group_id`, `product_name`, `product_type`,`packSize`,`carton_size`, `retail_price`, `trade_price`, `purchase_price`, `purchase_discount`, `sales_tax`, `product_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`) VALUES ('"+companyIdArr.get(i)+"','"+groupIdArr.get(i)+"','"+productNameArr.get(i)+"','"+productTypeArr.get(i)+"','"+productPackingArr.get(i)+"','"+cartonPackingArr.get(i)+"','"+retailPriceArr.get(i)+"','"+tradePriceArr.get(i)+"','"+purchasePriceArr.get(i)+"','"+purchaseDiscountArr.get(i)+"','"+salesTaxArr.get(i)+"','Active','"+currentUser+"','"+currentDate+"','"+currentTime+"','"+currentUser+"','"+currentDate+"','"+currentTime+"')";
                }

            }
            else
            {
                if(groupIdArr.get(i) == null || groupIdArr.get(i).equals(""))
                {
                    insertQuery = "INSERT INTO `product_info`(`product_id`, `company_table_id`, `product_name`, `product_type`,`packSize`,`carton_size`, `retail_price`, `trade_price`, `purchase_price`, `purchase_discount`, `sales_tax`, `product_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`) VALUES ('"+productIdArr.get(i)+"','"+companyIdArr.get(i)+"','"+productNameArr.get(i)+"','"+productTypeArr.get(i)+"','"+productPackingArr.get(i)+"','"+cartonPackingArr.get(i)+"','"+retailPriceArr.get(i)+"','"+tradePriceArr.get(i)+"','"+purchasePriceArr.get(i)+"','"+purchaseDiscountArr.get(i)+"','"+salesTaxArr.get(i)+"','Active','"+currentUser+"','"+currentDate+"','"+currentTime+"','"+currentUser+"','"+currentDate+"','"+currentTime+"')";
                }
                else
                {

                    insertQuery = "INSERT INTO `product_info`(`product_id`, `company_table_id`, `group_id`, `product_name`, `product_type`,`packSize`,`carton_size`, `retail_price`, `trade_price`, `purchase_price`, `purchase_discount`, `sales_tax`, `product_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`) VALUES ('"+productIdArr.get(i)+"','"+companyIdArr.get(i)+"','"+groupIdArr.get(i)+"','"+productNameArr.get(i)+"','"+productTypeArr.get(i)+"','"+productPackingArr.get(i)+"','"+cartonPackingArr.get(i)+"','"+retailPriceArr.get(i)+"','"+tradePriceArr.get(i)+"','"+purchasePriceArr.get(i)+"','"+purchaseDiscountArr.get(i)+"','"+salesTaxArr.get(i)+"','Active','"+currentUser+"','"+currentDate+"','"+currentTime+"','"+currentUser+"','"+currentDate+"','"+currentTime+"')";
                }
            }
            if(stmt.executeUpdate(insertQuery) == -1)
            {
                insertInfo = -1;
            }
            productTableId = getProductTableId(stmt, con, productNameArr.get(i), companyIdArr.get(i), currentDate);
            insertProductStock = "INSERT INTO `products_stock`(`product_id`, `in_stock`, `min_stock`, `max_stock`, `bonus_quant`, `status`) VALUES ('"+productTableId+"','0','0','0','0','Active')";
            if(stmt.executeUpdate(insertProductStock) == -1)
            {
                insertInfo = -1;
            }
        }
        if(insertInfo == -1)
        {
            String title = "Error";
            String message = "Unable to insert";
            GlobalVariables.showNotification(-1, title, message);
        }
        else
        {
            String title = "Successfull";
            String message = "Record Inserted";
            GlobalVariables.showNotification(1, title, message);
        }
        refreshAllArrays();
    }

    private void refreshAllArrays()
    {
        productIdArr = new ArrayList<>();
        companyIdArr = new ArrayList<>();
        companyNameArr = new ArrayList<>();
        groupIdArr = new ArrayList<>();
        groupNameArr = new ArrayList<>();
        productNameArr = new ArrayList<>();
        productTypeArr = new ArrayList<>();
        retailPriceArr = new ArrayList<>();
        tradePriceArr = new ArrayList<>();
        purchasePriceArr = new ArrayList<>();
        purchaseDiscountArr = new ArrayList<>();
        productPackingArr = new ArrayList<>();
        salesTaxArr = new ArrayList<>();
        holdStockArr = new ArrayList<>();
        maxStockArr = new ArrayList<>();
        minStockArr = new ArrayList<>();
    }


    public void updateProduct(Statement stmt, Connection con, String productTableId, String productId, String productName, String productType, String companyId, String groupId, String retailPrice, String tradePrice, String purchasePrice, String purchaseDiscount, String salesTax, String productStatus)
    {
        String updateQuery;
        try {
            if(productId.equals("") || productId.equals("N/A"))
            {
                updateQuery = "UPDATE `product_info` SET `product_id`= NULL, `company_id`='"+companyId+"',`group_id`='"+groupId+"',`product_name`='"+productName+"', `product_type`='"+productType+"',`retail_price`='"+retailPrice+"',`trade_price`='"+tradePrice+"',`purchase_price`='"+purchasePrice+"',`purchase_discount`='"+purchaseDiscount+"',`sales_tax`='"+salesTax+"',`product_status`='"+productStatus+"',`update_date`='"+GlobalVariables.getStDate()+"',`update_time`='"+GlobalVariables.getStTime()+"',`update_user_id`='"+GlobalVariables.getUserId()+"' WHERE `product_table_id` = '"+productTableId+"'";
            }
            else
            {
                updateQuery = "UPDATE `product_info` SET `product_id`= '"+productId+"', `company_id`='"+companyId+"',`group_id`='"+groupId+"',`product_name`='"+productName+"', `product_type`='"+productType+"',`retail_price`='"+retailPrice+"',`trade_price`='"+tradePrice+"',`purchase_price`='"+purchasePrice+"',`purchase_discount`='"+purchaseDiscount+"',`sales_tax`='"+salesTax+"',`product_status`='"+productStatus+"',`update_date`='"+GlobalVariables.getStDate()+"',`update_time`='"+GlobalVariables.getStTime()+"',`update_user_id`='"+GlobalVariables.getUserId()+"' WHERE `product_table_id` = '"+productTableId+"'";
            }

//            System.out.println(updateQuery);
            stmt.executeUpdate(updateQuery);
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<ArrayList<String>> getAddedProducts()
    {
        ArrayList<ArrayList<String>> productData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        for(int i=0; i<productIdArr.size(); i++)
        {
            temp = new ArrayList<String>();
            temp.add(productIdArr.get(i));
            temp.add(companyNameArr.get(i));
            temp.add(groupNameArr.get(i));
            temp.add(productNameArr.get(i));
            temp.add(productTypeArr.get(i));
            temp.add(purchasePriceArr.get(i));
            temp.add(retailPriceArr.get(i));
            productData.add(temp);
        }
        return productData;
    }

    public ArrayList<String> getProductDetail(String productId)
    {
        ArrayList<String> productInfo = new ArrayList<>();
        ResultSet rs = null;
        try {
            rs = objStmt.executeQuery("SELECT `product_info`.`product_id`, `product_info`.`product_name`, `product_info`.`product_type`, `product_info`.`retail_price`, `company_info`.`company_name`, `groups_info`.`group_name`, `product_info`.`creating_date`, `product_info`.`update_date`, `product_info`.`product_status` FROM `product_info` LEFT OUTER JOIN `company_info` ON `company_info`.`company_table_id` = `product_info`.`company_table_id` LEFT OUTER JOIN `groups_info` ON `groups_info`.`group_table_id` = `product_info`.`group_table_id` WHERE `product_info`.`product_table_id` = '"+productId+"'");
            while (rs.next())
            {
                productInfo.add(rs.getString(1)); // Product Id
                productInfo.add(rs.getString(2)); // Name
                productInfo.add(rs.getString(3)); // Type
                productInfo.add(rs.getString(4)); // Retail Price
                productInfo.add(rs.getString(5)); // Company Name
                productInfo.add(rs.getString(6)); // Group Name
                productInfo.add(rs.getString(7)); // Create
                productInfo.add(rs.getString(8)); // Update
                productInfo.add(rs.getString(9)); // Status
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return productInfo;
    }

    String checkProductId = new String();
    public String confirmNewId(String productId)
    {
        ResultSet rs3 = null;
        try {
            rs3 = objStmt.executeQuery("SELECT `product_id` FROM `product_info` WHERE `product_id` = '"+productId+"' AND `product_status` != 'Deleted'");
            while (rs3.next())
            {
                checkProductId = rs3.getString(1) ;
            }
//            objCon.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return checkProductId;
    }

    public ArrayList<ArrayList<String>> getCompaniesData()
    {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement stmt = objMysqlCon.stmt;
        Connection con = objMysqlCon.con;
        ResultSet rs = null;
        ArrayList<String> temp;
        try {
            rs = stmt.executeQuery("SELECT `company_table_id`, `company_name` FROM `company_info` WHERE `company_status` != 'Deleted'");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2));
                companiesData.add(temp);
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return companiesData;
    }

    public ArrayList<ArrayList<String>> getGroupsData()
    {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement stmt = objMysqlCon.stmt;
        Connection con = objMysqlCon.con;
        ResultSet rs = null;
        ArrayList<String> temp;
        try {
            rs = stmt.executeQuery("SELECT `group_table_id`, `group_name`, `company_id` FROM `groups_info` WHERE `group_status` != 'Deleted'");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2));
                temp.add(rs.getString(3));
                groupsData.add(temp);
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return groupsData;
    }

    public String getProductTableId(Statement stmt3, Connection con3 , String productName, String companyId, String creatingDate)
    {
        ResultSet rs3 = null;
        try {
            String getIdStatment = "SELECT `product_table_id` FROM `product_info` WHERE `product_name` = '"+productName+"' AND `company_table_id` = '"+companyId+"' AND `creating_date` = '"+creatingDate+"' AND `product_status` != 'Deleted'";
            rs3 = stmt3.executeQuery(getIdStatment);
            while (rs3.next())
            {
                checkProductId = rs3.getString(1) ;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return checkProductId;
    }

    public ArrayList<ArrayList<String>> getProductsDetail(Statement stmt, Connection con)
    {
        ArrayList<ArrayList<String>> batchData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT `product_info`.`product_table_id`, `product_info`.`product_name`, `products_stock`.`in_stock`, `product_info`.`trade_price`, `product_info`.`company_table_id` FROM `product_info` LEFT OUTER JOIN `products_stock` ON `products_stock`.`product_table_id` = `product_info`.`product_table_id` WHERE `product_info`.`product_status` != 'Deleted'");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Product Id
                temp.add(rs.getString(2)); // Product Name
                temp.add(rs.getString(3)); // Product Quantity
                temp.add(rs.getString(4)); // Product Price
                temp.add(rs.getString(5)); // Company Id
                batchData.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return batchData;
    }

    public ArrayList<ArrayList<String>> getCompanyProductsData(Statement stmt, Connection con)
    {
        ArrayList<ArrayList<String>> companyProductData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT `product_table_id`, `company_id` FROM `product_info` WHERE `product_status` != 'Deleted'");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Product Id
                temp.add(rs.getString(2)); // Company Id
                companyProductData.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return companyProductData;
    }

    public ArrayList<ArrayList<String>> getCompanyProductsDetail(Statement stmt, Connection con, String companyId)
    {
        ArrayList<ArrayList<String>> batchData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT `product_info`.`product_table_id`, `product_info`.`product_name`, `products_stock`.`in_stock`, `product_info`.`trade_price` FROM `product_info` LEFT OUTER JOIN `products_stock` ON `products_stock`.`product_table_id` = `product_info`.`product_table_id` WHERE `product_info`.`product_status` != 'Deleted' AND `product_info`.`company_table_id` = '"+companyId+"'");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Product Id
                temp.add(rs.getString(2)); // Product Name
                temp.add(rs.getString(3)); // Product Quantity
                temp.add(rs.getString(4)); // Product Price
                batchData.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return batchData;
    }

    public ArrayList<ArrayList<String>> getBatchesDetail(Statement stmt, Connection con)
    {
        ArrayList<ArrayList<String>> batchData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT `batch_stock_id`, `batch_no`, `prod_id`, `quantity`, `batch_expiry`, `entry_date` FROM `batchwise_stock` WHERE `quantity` > '0' ORDER BY `prod_id`");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Batch Id
                temp.add(rs.getString(2)); // Batch No  +" - ("+rs.getString(5)+")"
                temp.add(rs.getString(3)); // Product Id
                temp.add(rs.getString(4)); // Batch Quantity
                temp.add(rs.getString(5)); // Batch Expiry
                temp.add(rs.getString(6)); // Entry Date
                batchData.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return batchData;
    }

    public ArrayList<ArrayList<String>> getAllBatchesDetail(Statement stmt, Connection con)
    {
        ArrayList<ArrayList<String>> batchData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT `batch_stock_id`, `batch_no`, `prod_id`, `trade_price`  FROM `batchwise_stock` ORDER BY `prod_id`");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Batch Id
                temp.add(rs.getString(2)); // Batch No
                temp.add(rs.getString(3)); // Product Id
                temp.add(rs.getString(4)); // Trade Price
                batchData.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return batchData;
    }
}
