package model;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class DrawingSummaryInfo {
    private String srNo;
    private String productId;
    private String productName;
    private  String productBatch;
    private String quantity;
    private String bonusQuantity;
    private String tradePrice;
    private String totalAmount;

    private HBox operationsPane;
    private Button btnView;
    public static String productIdDetail;
    Alert a = new Alert(Alert.AlertType.NONE);
    public DrawingSummaryInfo() {
        this.srNo = "0";
        this.productId = "0";
        this.productName = "";
        this.productBatch="";
        this.quantity = "0";
        this.bonusQuantity = "0";
        this.tradePrice = "0.0";
        this.totalAmount = "0.0";
        this.operationsPane = new HBox();
    }

    public DrawingSummaryInfo(String srNo, String productIds, String productNames, String productBatchs, String quantities , String bonusQuantities, String tradeprices, String totalamounts ) {
        this.srNo = srNo;
        this.productId = productIds;
        this.productName = productNames;
        this.productBatch = productBatchs;
        this.quantity = quantities;
        this.bonusQuantity = bonusQuantities;
        this.tradePrice = tradeprices;
        this.totalAmount = totalamounts;

        btnView = new Button("Details");
        btnView.setOnAction(event -> {
            try {
                goToOrderedProductDetail(productIds);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        this.operationsPane = new HBox(btnView);
    }

    public void goToOrderedProductDetail(String productIdDet) throws IOException {
        productIdDetail = productIdDet;
        Parent root = FXMLLoader.load(getClass().getResource("../view/ordered_products_detail.fxml"));
        Stage objstage = (Stage) operationsPane.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }
    int limit=50;
    ArrayList<String> prodctlist = new ArrayList<>();
    public ArrayList<String> getprodcuts(Statement stmt2, Connection con2)
    {
        ResultSet rs = null;
        try {
            rs = stmt2.executeQuery("SELECT  `order_info`.`dealer_id`  , `dealer_info`.`dealer_subarea_id` , `order_info`.`booking_date`  , `order_info`.`order_id` ,`order_info_detailed`.`product_table_id`, `order_info_detailed`.`batch_number` , `order_info_detailed`.`submission_quantity` FROM `order_info` INNER JOIN `order_info_detailed` on `order_info`.`order_id` = `order_info_detailed`.`order_id`  INNER JOIN `dealer_info` on `order_info`.`dealer_id`  =`dealer_info`.`dealer_id` WHERE `order_info`.`status` = 'Pending'");
            int i = 0;
            while (rs.next() && i < limit)
            {
                prodctlist.add(i,rs.getString(1));
                i++;
            }

            con2.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return prodctlist;
    }


    public String getSrNo() {
        return srNo;
    }
    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }
    public String getProductId() {
        return productId;
    }
    public void setProductId(String productIds) {
        this.productId = productIds;
    }
    public String getProductName() {
        return productName;
    }
    public void setProductName(String productNames) {
        this.productName = productNames;
    }
    public String getProductBatch() {
        return productBatch;
    }
    public void setProductBatch(String productBatchs) {
        this.productBatch = productBatchs;
    }
    public String getQuantity() {
        return quantity;
    }
    public void setQuantity(String quantities) {
        this.quantity = quantities;
    }
    public String getBonusQuantity() {
        return bonusQuantity;
    }
    public void setBonusQuantity(String bonusQuantities) {
        this.bonusQuantity = bonusQuantities;
    }
    public String getTradePrice() {
        return tradePrice;
    }
    public void setTradePrice(String tradeprices) {
        this.tradePrice = tradeprices;
    }
    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalamounts) {
        this.totalAmount = totalamounts;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public Button getBtnView() {
        return btnView;
    }


    public void setBtnView(Button btnView) {
        this.btnView = btnView;
    }

    ArrayList<String> DealersTable_subareaID= new ArrayList<>();
    ArrayList<String> getsubareasCodes = new ArrayList<>();
    ArrayList<String> areawiseSearch_DealersTable_subareaID = new ArrayList<>();
    ArrayList<String> areawiseSearch_batches = new ArrayList<>();
    ArrayList<String> areawiseSearch_productIds= new ArrayList<>();
    ArrayList<String> areawiseSearch_quantities= new ArrayList<>();
    ArrayList<String> areawiseSearch_dates= new ArrayList<>();
    ArrayList<String> areawiseSearch_dealerIDs= new ArrayList<>();
    ArrayList<String> areawiseSearch_orderIDS= new ArrayList<>();
    ArrayList<String> areawiseSearch_bonusQaunt = new ArrayList<>();
    ArrayList<String> areawiseSearch_getallsubareas = new ArrayList<>();

    public ArrayList<ArrayList<String>> areawiseSearch_getSummarybyboth(Statement stmt, Connection con,int invoFrom,int invoTo ,LocalDate datefrom,LocalDate dateTo,ArrayList<String> chkd_subareas)
    {
    ResultSet rs3 = null;
        for(int i =0;i<chkd_subareas.size();i++)
        {
            try {
                rs3 = stmt.executeQuery("select subarea_id from subarea_info where subarea_status ='Active' and sub_area_name='"+chkd_subareas.get(i)+"' ");

                while (rs3.next() )
                {
                    getsubareasCodes.add(rs3.getString(1));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        ArrayList<ArrayList<String>> drawingsummary_withfilters = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ArrayList<ArrayList<String>> inStockData = null;
        int k=0;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT  `order_info`.`dealer_id`  , `dealer_info`.`dealer_subarea_id` , `order_info`.`booking_date`  , `order_info`.`order_id` ,`order_info_detailed`.`product_table_id`, `order_info_detailed`.`batch_number` , `order_info_detailed`.`submission_quantity` , `order_info_detailed`.`bonus_quant` FROM `order_info` INNER JOIN `order_info_detailed` on `order_info`.`order_id` = `order_info_detailed`.`order_id`  INNER JOIN `dealer_info` on `order_info`.`dealer_id`  =`dealer_info`.`dealer_id` WHERE `order_info`.`status` = 'Pending' and  `order_info`.`order_id` >= '"+invoFrom+"' and `order_info`.`order_id` <= '"+invoTo+"'");

            while (rs.next())
            {
                areawiseSearch_dealerIDs.add(rs.getString(1));
                areawiseSearch_DealersTable_subareaID.add(rs.getString(2));
                areawiseSearch_dates.add( rs.getString(3));
                areawiseSearch_orderIDS.add(rs.getString(4));
                areawiseSearch_productIds.add(rs.getString(5));
                areawiseSearch_batches.add(rs.getString(6));
                areawiseSearch_quantities.add(rs.getString(7));
                areawiseSearch_bonusQaunt.add(rs.getString(8));

                temp = new ArrayList<String>();

                String[] splitdate = areawiseSearch_dates.get(k).split("/");
                String nsss = splitdate[0] +'-' + splitdate[1] +'-'+ splitdate[2];
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
                Date varDate=dateFormat.parse(nsss);
                dateFormat=new SimpleDateFormat("yyyy-MM-dd");
                String mydate = dateFormat.format(varDate);
                LocalDate invoicedate = LocalDate.parse(mydate);
                for(int i=0;i<getsubareasCodes.size();i++)
                {
                if((invoicedate.isEqual(datefrom) || invoicedate.isEqual(dateTo))&& getsubareasCodes.get(i).equals(areawiseSearch_DealersTable_subareaID.get(k)))
                {
                    temp.add(areawiseSearch_dates.get(k));
                    temp.add(areawiseSearch_orderIDS.get(k));
                    temp.add(areawiseSearch_productIds.get(k));
                    temp.add(areawiseSearch_batches.get(k));
                    temp.add(areawiseSearch_quantities.get(k));
                    temp.add(areawiseSearch_bonusQaunt.get(k));
                    drawingsummary_withfilters.add(temp);
                }

                if((invoicedate.isAfter(datefrom) && invoicedate.isBefore(dateTo))&& getsubareasCodes.get(i).equals(areawiseSearch_DealersTable_subareaID.get(k)) )
                {
                    temp.add(areawiseSearch_dates.get(k));
                    temp.add(areawiseSearch_orderIDS.get(k));
                    temp.add(areawiseSearch_productIds.get(k));
                    temp.add(areawiseSearch_batches.get(k));
                    temp.add(areawiseSearch_quantities.get(k));
                    temp.add(areawiseSearch_bonusQaunt.get(k));
                    drawingsummary_withfilters.add(temp);
                }
                }
                k++;
            }

        } catch (SQLException | ParseException e) {
            e.printStackTrace();
        }

        ArrayList<String> uniqueProductIds = new ArrayList<String>();
        ArrayList<String> uniquebatch = new ArrayList<String>();
        ArrayList<String> uniqueProductNames = new ArrayList<String>();
        ArrayList<String> uniqueProductIdsQuantities = new ArrayList<String>();
        ArrayList<String> uniqueProductIdsBonusQuant = new ArrayList<String>();
        for(ArrayList<String> row: drawingsummary_withfilters) {
            if(!uniquebatch.contains(row.get(3)))
            {
                uniquebatch.add(row.get(3));
                uniqueProductNames.add("");
                uniqueProductIds.add(row.get(2));
            }
        }
//        System.out.println(uniqueProductIds);  !uniqueProductIds.contains(row.get(0))
        int currQuant;
        int currBonusQuant;
        for(String row: uniquebatch) {
            currQuant = 0;
            currBonusQuant = 0;
            for(int i=0; i<drawingsummary_withfilters.size(); i++)
            {
                if(drawingsummary_withfilters.get(i).get(3).equals(row))
                {
                    currQuant += Integer.parseInt(drawingsummary_withfilters.get(i).get(4));
                    currBonusQuant += Integer.parseInt(drawingsummary_withfilters.get(i).get(5));
                }
            }
            uniqueProductIdsQuantities.add(String.valueOf(currQuant));
            uniqueProductIdsBonusQuant.add(String.valueOf(currBonusQuant));
        }

        ArrayList<ArrayList<String>> drawingSummaryData = new ArrayList<ArrayList<String>>();
        ArrayList<ArrayList<String>> getprodDetails = new ArrayList<ArrayList<String>>();
        inStockData = new ArrayList<>();
        ResultSet rs1 = null;
        ResultSet rs2 = null;
        int productIndex;

        try {
            for(int c =0;c<uniqueProductIds.size();c++) {
                rs1 = stmt.executeQuery("SELECT `product_id`, `product_name`  , `trade_price` FROM `product_info` where `product_id` = '" + uniqueProductIds.get(c) + "'");
                while (rs1.next()) {
                    temp = new ArrayList<String>();
                    temp.add(rs1.getString(1));
                    temp.add(rs1.getString(2));
                    temp.add(rs1.getString(3));
                    getprodDetails.add(temp);

                }


                for (int a = 0; a < getprodDetails.size(); a++) {
                    for (int b = 0; b < uniqueProductIds.size(); b++) {
                        String strngprodDetails = getprodDetails.get(a).get(0);
                        String strnguniqueID = uniqueProductIds.get(b);

                        if (strngprodDetails.equals(strnguniqueID)) {
                            uniqueProductNames.set(b, getprodDetails.get(a).get(1));
                        }

                    }
                }
                rs2 = stmt.executeQuery("SELECT `product_id` , `trade_price` FROM `product_info` where `product_id` = '" + uniqueProductIds.get(c) + "'");

                while (rs2.next()) {
                    temp = new ArrayList<String>();
                    temp.add(rs2.getString(1));
                    temp.add(rs2.getString(2));
                    inStockData.add(temp);
                }
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }


        for (int i=0; i<uniqueProductIds.size(); i++)
        {
            temp = new ArrayList<String>();
            temp.add(uniqueProductIds.get(i));
            temp.add(uniqueProductNames.get(i));
            temp.add(uniquebatch.get(i));
            temp.add(uniqueProductIdsQuantities.get(i));
            temp.add(uniqueProductIdsBonusQuant.get(i));
            temp.add(inStockData.get(i).get(1));
            int indexOfStock = findIndex(uniqueProductIds.get(i), inStockData);
            float multi = 0;
            if(indexOfStock >= 0)
            {
                multi = Float.parseFloat(inStockData.get(indexOfStock).get((1))) * Integer.parseInt(uniqueProductIdsQuantities.get(i));
            }
            else
            {
                temp.add("0");
            }
            temp.add(String.valueOf(multi));
            drawingSummaryData.add(temp);
        }

        return drawingSummaryData;
    }
    public ArrayList<ArrayList<String>> areawiseSearch_getSummarybyinvoice(Statement stmt, Connection con,int invoFrom,int invoTo ,ArrayList<String> chkd_subareas)
    {
        ResultSet rset = null;
        for(int i =0;i<chkd_subareas.size();i++)
        {
            try {
                rset = stmt.executeQuery("select subarea_id from subarea_info where subarea_status ='Active' and sub_area_name='"+chkd_subareas.get(i)+"' ");

                while (rset.next() )
                {
                    getsubareasCodes.add(rset.getString(1));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        ArrayList<ArrayList<String>> drawingsummary_withfilters = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ArrayList<ArrayList<String>> inStockData = null;
        int k=0;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT  `order_info`.`dealer_id`  , `dealer_info`.`dealer_subarea_id` , `order_info`.`booking_date` , `order_info`.`order_id` ,`order_info_detailed`.`product_table_id`, `order_info_detailed`.`batch_number` , `order_info_detailed`.`submission_quantity` , `order_info_detailed`.`bonus_quant` FROM `order_info` INNER JOIN `order_info_detailed` on `order_info`.`order_id` = `order_info_detailed`.`order_id`  INNER JOIN `dealer_info` on `order_info`.`dealer_id`  =`dealer_info`.`dealer_id` WHERE `order_info`.`status` = 'Pending' and  `order_info`.`order_id` >= '"+invoFrom+"' and `order_info`.`order_id` <= '"+invoTo+"'");

            while (rs.next())
            {
                areawiseSearch_dealerIDs.add(rs.getString(1));
                areawiseSearch_DealersTable_subareaID.add(rs.getString(2));
                areawiseSearch_dates.add( rs.getString(3));
                areawiseSearch_orderIDS.add(rs.getString(4));
                areawiseSearch_productIds.add(rs.getString(5));
                areawiseSearch_batches.add(rs.getString(6));
                areawiseSearch_quantities.add(rs.getString(7));
                areawiseSearch_bonusQaunt.add(rs.getString(8));
                temp = new ArrayList<String>();

                for(int i=0;i<getsubareasCodes.size();i++)
                {
                    if(getsubareasCodes.get(i).equals(areawiseSearch_DealersTable_subareaID.get(k)))
                    {
                        temp.add(areawiseSearch_dates.get(k));
                        temp.add(areawiseSearch_orderIDS.get(k));
                        temp.add(areawiseSearch_productIds.get(k));
                        temp.add(areawiseSearch_batches.get(k));
                        temp.add(areawiseSearch_quantities.get(k));
                        temp.add(areawiseSearch_bonusQaunt.get(k));
                        drawingsummary_withfilters.add(temp);
                    }
                }

                k++;
            }
            if(drawingsummary_withfilters.isEmpty())
            {
                a.setAlertType(Alert.AlertType.ERROR);
                a.setHeaderText("No Record Found");
                a.setContentText("No Invoice Found.");
                a.show();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
//        System.out.println(drawingSummary);
        ArrayList<String> uniqueProductIds = new ArrayList<String>();
        ArrayList<String> uniquebatch = new ArrayList<String>();
        ArrayList<String> uniqueProductNames = new ArrayList<String>();
        ArrayList<String> uniqueProductIdsQuantities = new ArrayList<String>();
        ArrayList<String> uniqueProductIdsBonusQuant = new ArrayList<String>();
        for(ArrayList<String> row: drawingsummary_withfilters) {
            if(!uniquebatch.contains(row.get(3)))
            {
                uniquebatch.add(row.get(3));
                uniqueProductNames.add("");
                uniqueProductIds.add(row.get(2));
            }
        }
//        System.out.println(uniqueProductIds);
        int currQuant;
        int currBonusQuant;
        for(String row: uniquebatch) {
            currQuant = 0;
            currBonusQuant = 0;
            for(int i=0; i<drawingsummary_withfilters.size(); i++)
            {
                if(drawingsummary_withfilters.get(i).get(3).equals(row))
                {
                    currQuant += Integer.parseInt(drawingsummary_withfilters.get(i).get(4));
                    currBonusQuant += Integer.parseInt(drawingsummary_withfilters.get(i).get(5));
                }
            }
            uniqueProductIdsQuantities.add(String.valueOf(currQuant));
            uniqueProductIdsBonusQuant.add(String.valueOf(currBonusQuant));
        }

        ArrayList<ArrayList<String>> drawingSummaryData = new ArrayList<ArrayList<String>>();
        ArrayList<ArrayList<String>> getprodDetails = new ArrayList<ArrayList<String>>();
        inStockData = new ArrayList<>();
        ResultSet rs1 = null;
        ResultSet rs2 = null;
        int productIndex;
        try {
            for(int c =0;c<uniqueProductIds.size();c++) {
            rs1 = stmt.executeQuery("SELECT `product_id`, `product_name` , `trade_price` FROM `product_info` where `product_id` = '" + uniqueProductIds.get(c) + "'");
            while (rs1.next()) {
                temp = new ArrayList<String>();
                temp.add(rs1.getString(1));
                temp.add(rs1.getString(2));
                getprodDetails.add(temp);

            }


            for (int a = 0; a < getprodDetails.size(); a++) {
                for (int b = 0; b < uniqueProductIds.size(); b++) {
                    String strngprodDetails = getprodDetails.get(a).get(0);
                    String strnguniqueID = uniqueProductIds.get(b);

                    if (strngprodDetails.equals(strnguniqueID)) {
                        uniqueProductNames.set(b, getprodDetails.get(a).get(1));
                    }

                }
            }
            rs2 = stmt.executeQuery("SELECT `product_id`, `trade_price` FROM `product_info` where `product_id` = '" + uniqueProductIds.get(c) + "'");

            while (rs2.next()) {
                temp = new ArrayList<String>();
                temp.add(rs2.getString(1));
                temp.add(rs2.getString(2));
                inStockData.add(temp);
            }

        }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
//        System.out.println(uniqueProductNames);
//        System.out.println(uniqueProductIdsQuantities);



        for (int i=0; i<uniqueProductIds.size(); i++)
        {
            temp = new ArrayList<String>();
            temp.add(uniqueProductIds.get(i));
            temp.add(uniqueProductNames.get(i));
            temp.add(uniquebatch.get(i));
            temp.add(uniqueProductIdsQuantities.get(i));
            temp.add(uniqueProductIdsBonusQuant.get(i));
            temp.add(inStockData.get(i).get(1));
            int indexOfStock = findIndex(uniqueProductIds.get(i), inStockData);
            float multi = 0;
            if(indexOfStock >= 0)
            {
                multi = Float.parseFloat(inStockData.get(indexOfStock).get((1))) * Integer.parseInt(uniqueProductIdsQuantities.get(i));
            }
            else
            {
                temp.add("0");
            }
            temp.add(String.valueOf(multi));
            drawingSummaryData.add(temp);
        }

        return drawingSummaryData;
    }
    public ArrayList<ArrayList<String>> areawiseSearch_getSummarybydates(Statement stmt, Connection con,LocalDate datefrom,LocalDate dateTo,ArrayList<String> chkd_subareas)
    {
        ResultSet rset = null;
        for(int i =0;i<chkd_subareas.size();i++)
        {
            try {
                rset = stmt.executeQuery("select subarea_id from subarea_info where subarea_status ='Active' and sub_area_name='"+chkd_subareas.get(i)+"' ");

                while (rset.next() )
                {
                    getsubareasCodes.add(rset.getString(1));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        ArrayList<ArrayList<String>> drawingsummary_withfilters = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ArrayList<ArrayList<String>> inStockData = null;
        int k=0;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT  `order_info`.`dealer_id`  , `dealer_info`.`dealer_subarea_id` ,  `order_info`.`booking_date` , `order_info`.`order_id` ,`order_info_detailed`.`product_table_id`, `order_info_detailed`.`batch_number` , `order_info_detailed`.`submission_quantity` , `order_info_detailed`.`bonus_quant` FROM `order_info` INNER JOIN `order_info_detailed` on `order_info`.`order_id` = `order_info_detailed`.`order_id`  INNER JOIN `dealer_info` on `order_info`.`dealer_id`  =`dealer_info`.`dealer_id`  WHERE `order_info`.`status` = 'Pending'");
            while (rs.next())
            {
                areawiseSearch_dealerIDs.add(rs.getString(1));
                areawiseSearch_DealersTable_subareaID.add(rs.getString(2));
                areawiseSearch_dates.add( rs.getString(3));
                areawiseSearch_orderIDS.add(rs.getString(4));
                areawiseSearch_productIds.add(rs.getString(5));
                areawiseSearch_batches.add(rs.getString(6));
                areawiseSearch_quantities.add(rs.getString(7));
                areawiseSearch_bonusQaunt.add(rs.getString(8));

                temp = new ArrayList<String>();
                String[] splitdate = areawiseSearch_dates.get(k).split("/");
                String nsss = splitdate[0] +'-' + splitdate[1] +'-'+ splitdate[2];
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
                Date varDate=dateFormat.parse(nsss);
                dateFormat=new SimpleDateFormat("yyyy-MM-dd");
                String mydate = dateFormat.format(varDate);
                LocalDate invoicedate = LocalDate.parse(mydate);
                for(int i=0;i<getsubareasCodes.size();i++)
                {
                    if((invoicedate.isEqual(datefrom) || invoicedate.isEqual(dateTo))&& getsubareasCodes.get(i).equals(areawiseSearch_DealersTable_subareaID.get(k)))
                    {
                        temp.add(areawiseSearch_dates.get(k));
                        temp.add(areawiseSearch_orderIDS.get(k));
                        temp.add(areawiseSearch_productIds.get(k));
                        temp.add(areawiseSearch_batches.get(k));
                        temp.add(areawiseSearch_quantities.get(k));
                        temp.add(areawiseSearch_bonusQaunt.get(k));
                        drawingsummary_withfilters.add(temp);
                    }

                    if((invoicedate.isAfter(datefrom) && invoicedate.isBefore(dateTo))&& getsubareasCodes.get(i).equals(areawiseSearch_DealersTable_subareaID.get(k)) )
                    {
                        temp.add(areawiseSearch_dates.get(k));
                        temp.add(areawiseSearch_orderIDS.get(k));
                        temp.add(areawiseSearch_productIds.get(k));
                        temp.add(areawiseSearch_batches.get(k));
                        temp.add(areawiseSearch_quantities.get(k));
                        temp.add(areawiseSearch_bonusQaunt.get(k));
                        drawingsummary_withfilters.add(temp);
                    }
                }
                k++;
            }

            if(drawingsummary_withfilters.isEmpty())
            {
                a.setAlertType(Alert.AlertType.ERROR);
                a.setHeaderText("No Record Found");
                a.setContentText("No Invoice was Created Between these Dates");
                a.show();
            }
        } catch (SQLException | ParseException e) {
            e.printStackTrace();
        }
//        System.out.println(drawingSummary);
        ArrayList<String> uniqueProductIds = new ArrayList<String>();
        ArrayList<String> uniquebatch = new ArrayList<String>();
        ArrayList<String> uniqueProductNames = new ArrayList<String>();
        ArrayList<String> uniqueProductIdsQuantities = new ArrayList<String>();
        ArrayList<String> uniqueProductIdsBonusQuant = new ArrayList<String>();
        for(ArrayList<String> row: drawingsummary_withfilters) {
            if(!uniquebatch.contains(row.get(3)))
            {
                uniquebatch.add(row.get(3));
                uniqueProductNames.add("");
                uniqueProductIds.add(row.get(2));
            }
        }
//        System.out.println(uniqueProductIds);
        int currQuant;
        int currBonusQuant;
        for(String row: uniquebatch) {
            currQuant = 0;
            currBonusQuant = 0;
            for(int i=0; i<drawingsummary_withfilters.size(); i++)
            {
                if(drawingsummary_withfilters.get(i).get(3).equals(row))
                {
                    currQuant += Integer.parseInt(drawingsummary_withfilters.get(i).get(4));
                    currBonusQuant += Integer.parseInt(drawingsummary_withfilters.get(i).get(5));
                }
            }
            uniqueProductIdsQuantities.add(String.valueOf(currQuant));
            uniqueProductIdsBonusQuant.add(String.valueOf(currBonusQuant));
        }

        ArrayList<ArrayList<String>> drawingSummaryData = new ArrayList<ArrayList<String>>();
        ArrayList<ArrayList<String>> getprodDetails = new ArrayList<ArrayList<String>>();
        inStockData = new ArrayList<>();
        ResultSet rs1 = null;
        ResultSet rs2 = null;
        int productIndex;
        try
        {
            inStockData = new ArrayList<>();
            for(int c =0;c<uniqueProductIds.size();c++)
            {
                rs1 = stmt.executeQuery("SELECT `product_id`, `product_name`  , `trade_price` FROM `product_info` where `product_id` = '" + uniqueProductIds.get(c) + "'");
                while (rs1.next()) {
                    temp = new ArrayList<String>();
                    temp.add(rs1.getString(1));
                    temp.add(rs1.getString(2));
                    temp.add(rs1.getString(3));
                    getprodDetails.add(temp);

                }


                for (int a = 0; a < getprodDetails.size(); a++) {
                    for (int b = 0; b < uniqueProductIds.size(); b++) {
                        String strngprodDetails = getprodDetails.get(a).get(0);
                        String strnguniqueID = uniqueProductIds.get(b);

                        if (strngprodDetails.equals(strnguniqueID)) {
                            uniqueProductNames.set(b, getprodDetails.get(a).get(1));
                        }

                    }
                }
                rs2 = stmt.executeQuery("SELECT `product_id`, `trade_price` FROM `product_info` where `product_id` = '" + uniqueProductIds.get(c) + "'");

                while (rs2.next()) {
                    temp = new ArrayList<String>();
                    temp.add(rs2.getString(1));
                    temp.add(rs2.getString(2));
                    inStockData.add(temp);
                }
        }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
//        System.out.println(uniqueProductNames);
//        System.out.println(uniqueProductIdsQuantities);

        for (int i=0; i<uniqueProductIds.size(); i++)
        {
            temp = new ArrayList<String>();
            temp.add(uniqueProductIds.get(i));
            temp.add(uniqueProductNames.get(i));
            temp.add(uniquebatch.get(i));
            temp.add(uniqueProductIdsQuantities.get(i));
            temp.add(uniqueProductIdsBonusQuant.get(i));
            temp.add(inStockData.get(i).get(1));
            int indexOfStock = findIndex(uniqueProductIds.get(i), inStockData);
            float multi = 0;
            if(indexOfStock >= 0)
            {
                multi = Float.parseFloat(inStockData.get(indexOfStock).get((1))) * Integer.parseInt(uniqueProductIdsQuantities.get(i));
            }
            else
            {
                temp.add("0");
            }
            temp.add(String.valueOf(multi));
            drawingSummaryData.add(temp);
        }

        return drawingSummaryData;
    }
    public ArrayList<ArrayList<String>> areawiseSearch_ggetDrawingSummary(Statement stmt, Connection con,ArrayList<String> chkd_subareas)
    {
        ResultSet rset = null;
        for(int i =0;i<chkd_subareas.size();i++)
        {
            try {
                rset = stmt.executeQuery("select subarea_id from subarea_info where subarea_status ='Active' and sub_area_name='"+chkd_subareas.get(i)+"' ");

                while (rset.next() )
                {
                    getsubareasCodes.add(rset.getString(1));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        ArrayList<ArrayList<String>> drawingsummary_withfilters = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ArrayList<ArrayList<String>> inStockData = null;
        int k=0;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT  `order_info`.`dealer_id`  , `dealer_info`.`dealer_subarea_id` ,  `order_info`.`booking_date` , `order_info`.`order_id` ,`order_info_detailed`.`product_table_id`, `order_info_detailed`.`batch_number` , `order_info_detailed`.`submission_quantity` , `order_info_detailed`.`bonus_quant` FROM `order_info` INNER JOIN `order_info_detailed` on `order_info`.`order_id` = `order_info_detailed`.`order_id`  INNER JOIN `dealer_info` on `order_info`.`dealer_id`  =`dealer_info`.`dealer_id`  WHERE `order_info`.`status` = 'Pending'");
            while (rs.next())
            {
                areawiseSearch_dealerIDs.add(rs.getString(1));
                areawiseSearch_DealersTable_subareaID.add(rs.getString(2));
                areawiseSearch_dates.add( rs.getString(3));
                areawiseSearch_orderIDS.add(rs.getString(4));
                areawiseSearch_productIds.add(rs.getString(5));
                areawiseSearch_batches.add(rs.getString(6));
                areawiseSearch_quantities.add(rs.getString(7));
                areawiseSearch_bonusQaunt.add(rs.getString(8));
                temp = new ArrayList<String>();
                String[] splitdate = areawiseSearch_dates.get(k).split("/");
                String nsss = splitdate[0] +'-' + splitdate[1] +'-'+ splitdate[2];
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
                Date varDate=dateFormat.parse(nsss);
                dateFormat=new SimpleDateFormat("yyyy-MM-dd");
                String mydate = dateFormat.format(varDate);
                for(int i=0;i<getsubareasCodes.size();i++)
                {
                    if(getsubareasCodes.get(i).equals(areawiseSearch_DealersTable_subareaID.get(k)))
                    {
                        temp.add(areawiseSearch_dates.get(k));
                        temp.add(areawiseSearch_orderIDS.get(k));
                        temp.add(areawiseSearch_productIds.get(k));
                        temp.add(areawiseSearch_batches.get(k));
                        temp.add(areawiseSearch_quantities.get(k));
                        temp.add(areawiseSearch_bonusQaunt.get(k));
                        drawingsummary_withfilters.add(temp);
                    }
                }
                k++;
            }

            if(drawingsummary_withfilters.isEmpty())
            {
                a.setAlertType(Alert.AlertType.ERROR);
                a.setHeaderText("No Record Found");
                a.setContentText("No Invoice was Found.");
                a.show();
            }
        } catch (SQLException | ParseException e) {
            e.printStackTrace();
        }
//        System.out.println(drawingSummary);
        ArrayList<String> uniqueProductIds = new ArrayList<String>();
        ArrayList<String> uniquebatch = new ArrayList<String>();
        ArrayList<String> uniqueProductNames = new ArrayList<String>();
        ArrayList<String> uniqueProductIdsQuantities = new ArrayList<String>();
        ArrayList<String> uniqueProductIdsBonusQuant = new ArrayList<String>();
        for(ArrayList<String> row: drawingsummary_withfilters) {
            if(!uniquebatch.contains(row.get(3)))
            {
                uniquebatch.add(row.get(3));
                uniqueProductNames.add("");
                uniqueProductIds.add(row.get(2));
            }
        }
//        System.out.println(uniqueProductIds);
        int currQuant;
        int currBonusQuant;
        for(String row: uniquebatch) {
            currQuant = 0;
            currBonusQuant = 0;
            for(int i=0; i<drawingsummary_withfilters.size(); i++)
            {
                if(drawingsummary_withfilters.get(i).get(3).equals(row))
                {
                    currQuant += Integer.parseInt(drawingsummary_withfilters.get(i).get(4));
                    currBonusQuant += Integer.parseInt(drawingsummary_withfilters.get(i).get(5));
                }
            }
            uniqueProductIdsQuantities.add(String.valueOf(currQuant));
            uniqueProductIdsBonusQuant.add(String.valueOf(currBonusQuant));
        }

        ArrayList<ArrayList<String>> drawingSummaryData = new ArrayList<ArrayList<String>>();
        ArrayList<ArrayList<String>> getprodDetails = new ArrayList<ArrayList<String>>();


        inStockData = new ArrayList<>();
        ResultSet rs1 = null;
        ResultSet rs2 = null;
        int productIndex;
        try {
            inStockData = new ArrayList<>();
            for(int c =0;c<uniqueProductIds.size();c++)
            {
                rs1 = stmt.executeQuery("SELECT `product_id`, `product_name`  , `trade_price` FROM `product_info` where `product_id` = '" + uniqueProductIds.get(c) + "'");
                while (rs1.next()) {
                    temp = new ArrayList<String>();
                    temp.add(rs1.getString(1));
                    temp.add(rs1.getString(2));
                    temp.add(rs1.getString(3));
                    getprodDetails.add(temp);

                }


                for (int a = 0; a < getprodDetails.size(); a++) {
                    for (int b = 0; b < uniqueProductIds.size(); b++) {
                        String strngprodDetails = getprodDetails.get(a).get(0);
                        String strnguniqueID = uniqueProductIds.get(b);

                        if (strngprodDetails.equals(strnguniqueID)) {
                            uniqueProductNames.set(b, getprodDetails.get(a).get(1));
                        }

                    }
                }
                rs2 = stmt.executeQuery("SELECT `product_id` , `trade_price` FROM `product_info` where `product_id` = '" + uniqueProductIds.get(c) + "'");

                while (rs2.next()) {
                    temp = new ArrayList<String>();
                    temp.add(rs2.getString(1));
                    temp.add(rs2.getString(2));
                    inStockData.add(temp);
                }
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
//        System.out.println(uniqueProductNames);
//        System.out.println(uniqueProductIdsQuantities);

        for (int i=0; i<uniqueProductIds.size(); i++)
        {
            temp = new ArrayList<String>();
            temp.add(uniqueProductIds.get(i));
            temp.add(uniqueProductNames.get(i));
            temp.add(uniquebatch.get(i));
            temp.add(uniqueProductIdsQuantities.get(i));
            temp.add(uniqueProductIdsBonusQuant.get(i));
            temp.add(inStockData.get(i).get(1));
            int indexOfStock = findIndex(uniqueProductIds.get(i), inStockData);
            float multi = 0;
            if(indexOfStock >= 0)
            {
                multi = Float.parseFloat(inStockData.get(indexOfStock).get((1))) * Integer.parseInt(uniqueProductIdsQuantities.get(i));
            }
            else
            {
                temp.add("0");
            }
            temp.add(String.valueOf(multi));

            drawingSummaryData.add(temp);
        }

        return drawingSummaryData;
    }

    ArrayList<String> batches = new ArrayList<>();
    ArrayList<String> productIds= new ArrayList<>();
    ArrayList<String> quantities= new ArrayList<>();
    ArrayList<String> dates= new ArrayList<>();
    ArrayList<String> Bonus_quants= new ArrayList<>();
    ArrayList<String> orderIDS= new ArrayList<>();
    ArrayList<String> getallareas = new ArrayList<>();
    ArrayList<String> getallsubareas = new ArrayList<>();
    ArrayList<String> getSubareasCodes = new ArrayList<>();
    ArrayList<String> dealers_id = new ArrayList<>();
    ArrayList<String> subareas_id = new ArrayList<>();
    String batch_policy = "";
    ArrayList<String> batchlist = new ArrayList<>();
    String currentBatch = "";
    public ArrayList<ArrayList<String>> getDrawingSummary(Statement stmt, Connection con,String invo_status,String invoiceFrom,String invoiceTo,ArrayList<String> chkd_subareas,String datefrom,String dateTo) throws ParseException {
        ArrayList<ArrayList<String>> drawingsummary_withfilters = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ArrayList<ArrayList<String>> inStockData = null;
        int k = 0;


        String MyQuery = "SELECT  `order_info`.`dealer_id`, `dealer_info`.`dealer_subarea_id`,`order_info`.`booking_date` , `order_info`.`order_id` ,`order_info_detailed`.`product_table_id`, `order_info_detailed`.`batch_number` , `order_info_detailed`.`submission_quantity` , `order_info_detailed`.`bonus_quant` FROM `order_info` INNER JOIN `order_info_detailed` on `order_info`.`order_id` = `order_info_detailed`.`order_id` INNER JOIN `dealer_info` on `order_info`.`dealer_id` = `dealer_info`.`dealer_id`  WHERE `order_info`.`status` = '" + invo_status + "' ";
        if (!invoiceFrom.isEmpty() || !invoiceTo.isEmpty())
        {
            MyQuery += "and order_info.order_id >='"+invoiceFrom+"' and order_info.order_id <='"+invoiceTo+"'";
        }
        if (!datefrom.isEmpty() || !dateTo.isEmpty())
        {
            MyQuery += " and str_to_date(`order_info`.`booking_date`, '%d/%b/%Y') >= str_to_date('"+datefrom+"', '%d/%b/%Y') and str_to_date(`order_info`.`booking_date`, '%d/%b/%Y') <= str_to_date('"+dateTo+"', '%d/%b/%Y')";
        }
        if(!chkd_subareas.isEmpty())
        {
            ResultSet rset = null;
            for(int i =0;i<chkd_subareas.size();i++)
            {
                try {
                    rset = stmt.executeQuery("select subarea_id from subarea_info where subarea_status ='Active' and sub_area_name='"+chkd_subareas.get(i)+"' ");

                    while (rset.next() )
                    {
                        getSubareasCodes.add(rset.getString(1));
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery(MyQuery);

            while (rs.next())
            {
                dealers_id.add(rs.getString(1));
                subareas_id.add(rs.getString(2));
                dates.add( rs.getString(3));
                orderIDS.add(rs.getString(4));
                productIds.add(rs.getString(5));
                batches.add(rs.getString(6));
                quantities.add(rs.getString(7));
                Bonus_quants.add(rs.getString(8));

                temp = new ArrayList<String>();

                if(chkd_subareas.isEmpty())
                {
                    temp.add(dates.get(k));
                    temp.add(orderIDS.get(k));
                    temp.add(productIds.get(k));
                    temp.add(batches.get(k));
                    temp.add(quantities.get(k));
                    temp.add(Bonus_quants.get(k));
                    drawingsummary_withfilters.add(temp);
                }
                for(int i=0;i<getSubareasCodes.size();i++)
                {
                    if(( getSubareasCodes.get(i).equals(subareas_id.get(k)))) {
                        temp.add(dates.get(k));
                        temp.add(orderIDS.get(k));
                        temp.add(productIds.get(k));
                        temp.add(batches.get(k));
                        temp.add(quantities.get(k));
                        temp.add(Bonus_quants.get(k));
                        drawingsummary_withfilters.add(temp);
                    }
                }
                k++;
            }
            if(drawingsummary_withfilters.isEmpty())
            {
                a.setAlertType(Alert.AlertType.ERROR);
                a.setHeaderText("No Record Found");
                a.setContentText("No Invoice was Created Between these Dates");
                a.show();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> uniqueProductIds = new ArrayList<String>();
        ArrayList<String> uniquebatch = new ArrayList<String>();
        ArrayList<String> uniqueProductNames = new ArrayList<String>();
        ArrayList<String> uniqueProductIdsQuantities = new ArrayList<String>();
        ArrayList<String> uniqueProductIdsBonusQuant = new ArrayList<String>();
        ArrayList<String> uniqueRate = new ArrayList<String>();
        for(ArrayList<String> row: drawingsummary_withfilters) {
            if(!uniquebatch.contains(row.get(3)))
            {
                uniquebatch.add(row.get(3));
                uniqueProductNames.add("");
                uniqueProductIds.add(row.get(2));
                uniqueRate.add("");
            }
        }
        int currQuant;
        int currBonusQuant;
        for(String row: uniquebatch) {
            currQuant = 0;
            currBonusQuant = 0;
            for(int i=0; i<drawingsummary_withfilters.size(); i++)
            {
                if(drawingsummary_withfilters.get(i).get(3).equals(row))
                {
                    currQuant += Integer.parseInt(drawingsummary_withfilters.get(i).get(4));
                    currBonusQuant += Integer.parseInt(drawingsummary_withfilters.get(i).get(5));
                }
            }
            uniqueProductIdsQuantities.add(String.valueOf(currQuant));
            uniqueProductIdsBonusQuant.add(String.valueOf(currBonusQuant));
        }

        ArrayList<ArrayList<String>> drawingSummaryData = new ArrayList<ArrayList<String>>();
        ArrayList<ArrayList<String>> getprodDetails = new ArrayList<ArrayList<String>>();
        inStockData = new ArrayList<>();
        ResultSet rs1 = null;
        ResultSet rs2 = null;
        int productIndex;
        try {
            inStockData = new ArrayList<>();
            for(int c =0;c<uniqueProductIds.size();c++) {

                rs1 = stmt.executeQuery("SELECT `product_info`.`product_id`, `product_info`.`product_name`, `batchwise_stock`.`trade_price` FROM `product_info` INNER JOIN `batchwise_stock` on `product_info`.`product_id` = `batchwise_stock`.`product_table_id` where `batchwise_stock`.`product_table_id` =  '" + uniqueProductIds.get(c) + "' and `batchwise_stock`.`batch_no` = '"+uniquebatch.get(c)+"' ");
                while (rs1.next()) {
                    temp = new ArrayList<String>();
                    temp.add(rs1.getString(1));
                    temp.add(rs1.getString(2));
                    temp.add(rs1.getString(3));
                    getprodDetails.add(temp);

                }


                for (int a = 0; a < getprodDetails.size(); a++) {
                    for (int b = 0; b < uniqueProductIds.size(); b++) {
                        String strngprodDetails = getprodDetails.get(a).get(0);
                        String strnguniqueID = uniqueProductIds.get(b);

                        if (strngprodDetails.equals(strnguniqueID)) {
                            uniqueProductNames.set(b, getprodDetails.get(a).get(1));
                        }

                    }
                }
                for (int a = 0; a < getprodDetails.size(); a++) {
                    for (int b = 0; b < uniqueProductIds.size(); b++) {
                        String strngprodDetails = getprodDetails.get(a).get(0);
                        String strnguniqueID = uniqueProductIds.get(b);

                        if (strngprodDetails.equals(strnguniqueID)) {
                            uniqueRate.set(b, getprodDetails.get(a).get(2));
                        }

                    }
                }
                rs2 = stmt.executeQuery("SELECT `product_id`, `in_stock` FROM `products_stock` where `product_id` = '" + uniqueProductIds.get(c) + "'");

                while (rs2.next()) {
                    temp = new ArrayList<String>();
                    temp.add(rs2.getString(1));
                    temp.add(rs2.getString(2));
                    inStockData.add(temp);
                }
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
//        System.out.println(uniqueProductNames);
//        System.out.println(uniqueProductIdsQuantities);

        for (int i=0; i<uniqueProductIds.size(); i++)
        {
            temp = new ArrayList<String>();
            temp.add(uniqueProductIds.get(i));
            temp.add(uniqueProductNames.get(i));
            temp.add(uniquebatch.get(i));
            temp.add(uniqueProductIdsQuantities.get(i));
            temp.add(uniqueProductIdsBonusQuant.get(i));
            temp.add(uniqueRate.get(i));
            int indexOfStock = findIndex(uniqueProductIds.get(i), inStockData);
            float multi = 0;
            if(indexOfStock >= 0)
            {
                multi = Float.parseFloat(uniqueRate.get(i)) * Integer.parseInt(uniqueProductIdsQuantities.get(i));
            }
            else
            {
                temp.add("0");
            }
            temp.add(String.valueOf(multi));
            drawingSummaryData.add(temp);
        }

        return drawingSummaryData;
    }

    public ArrayList<String> getAreas(Statement stmt3, Connection con3)
    {
        ResultSet rs3 = null;
        try {
            rs3 = stmt3.executeQuery("select area_id,area_name from area_info where area_status ='Active'");

            while (rs3.next() )
            {

                getallareas.add(rs3.getString(1) + "--" +rs3.getString(2));

            }
            con3.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return getallareas;
    }
    public ArrayList<String> getSubAreas(Statement stmt3, Connection con3,int areasID)
    {
        ResultSet rs3 = null;
        try {
            rs3 = stmt3.executeQuery("select subarea_id,sub_area_name from subarea_info where subarea_status ='Active' and area_id='"+areasID+"' ");

            while (rs3.next() )
            {
                getallsubareas.add(rs3.getString(1) + "--" +rs3.getString(2));
            }
            con3.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return getallsubareas;
    }

    public int findIndex(String val, ArrayList<ArrayList<String>> arr)
    {
        for (int i = 0 ; i < arr.size(); i++)
            if ( arr.get(i).get(0).equals(val))
            {
                return i;
            }
        return -1;
    }
}
