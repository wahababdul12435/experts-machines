package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class SaleReturn {
    int limit = 5;
    String[] Invo_details = new String[1];
    public String[] getsaleInvodetails(Statement stmt, Connection con , int invoiceid)
    {
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("select dealer_id,booking_date from order_info where order_id ='"+invoiceid+"' and status='Pending' ");
            int i = 0;
            while (rs.next() && i < limit)
            {
                Invo_details[i] = rs.getString(1) + "--" + rs.getString(2);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Invo_details;
    }


    String[] proddetails = new String[1];
    public String[] get_prodDetailswithIDs_Batch(Statement stmt1, Connection con1, int prods_Ids,String getbatch)
    {
        ResultSet rs = null;
        try {
            rs = stmt1.executeQuery("select `product_info`.`product_name`, `batchwise_stock`.`retail_price` , `batchwise_stock`.`purchase_price`, `batchwise_stock`.`trade_price`, `product_info`.`company_table_id` from `product_info`  INNER JOIN `batchwise_stock` on `batchwise_stock`.`product_table_id` = `product_info`.`product_id` where  product_id = '"+prods_Ids+"' and `batchwise_stock`.`batch_no` = '"+getbatch+"' ");
            int i = 0;
            while (rs.next() && i < limit)
            {
                proddetails[i] = rs.getString(1) + "--" + rs.getString(2) + "--" + rs.getString(3) + "--" + rs.getString(4)+"--"+ rs.getString(5);
            }

            con1.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return proddetails;
    }


    String[] prodbatch_quantity = new String[1];
    public String[] getprodbatch_quantity(Statement stmt2, Connection con2, int product_IDS,String batch_product)
    {
        ResultSet rs = null;
        try {
            rs = stmt2.executeQuery("select quantity,bonus from batchwise_stock where prod_id ='"+product_IDS+"' and batch_no='"+batch_product+"' ");
            int i = 0;
            while (rs.next() && i < limit)
            {

                prodbatch_quantity[i] = rs.getString(1)  + "--" + rs.getString(2);
            }

            con2.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return prodbatch_quantity;
    }

    String[] cnfrmBatch_exist = new String[1];
    public String[] cnfrmBatch_exist_inDB(Statement stmt2, Connection con2, int product_IDS,String batch_product)
    {
        ResultSet rs = null;
        try {
            rs = stmt2.executeQuery("select quantity,bonus from batchwise_stock   where prod_id ='"+product_IDS+"' and batch_no='"+batch_product+"' ");
            int i = 0;
            while (rs.next() && i < limit)
            {

                cnfrmBatch_exist[i] = rs.getString(1)  + "--" + rs.getString(2);
            }

            con2.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cnfrmBatch_exist;
    }

    String[] getbatchquants = new String[1];
    public String[] getbatch_quantFrmORDER(Statement stmt2, Connection con2, int product_IDS,String batch_product)
    {
        ResultSet rs = null;
        try {
            rs = stmt2.executeQuery("select quantity,bonus_quant,discount from order_info_detailed where product_id ='"+product_IDS+"' and batch_number='"+batch_product+"' ");
            int i = 0;
            while (rs.next() && i < limit)
            {

                getbatchquants[i] = rs.getString(1)  + "--" + rs.getString(2)  + "--" + rs.getString(3);
            }

            con2.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return getbatchquants;
    }

    String[] getbatchofProd = new String[5];
    public String[] getexistingBatchandDetails(Statement stmt, Connection con ,int productIds, int invoiceid)
    {
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("select cmplt_returned_bit,discount,bonus_quant,batch_number,quantity from order_info_detailed where product_id ='"+productIds+"' and order_id ='"+invoiceid+"' ");
            int i = 0;
            while (rs.next())
            {
                getbatchofProd[i] = rs.getString(1) + "--" + rs.getString(2) + "--" + rs.getString(3) + "--" + rs.getString(4) + "--" + rs.getString(5);
                i++;
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return getbatchofProd;
    }



    String[] getreturnnumber = new String[1];
    public String[] get_lastreturnnumber(Statement stmt3, Connection con3)
    {
        ResultSet rs3 = null;
        try {
            rs3 = stmt3.executeQuery("select return_id from order_return order by return_id DESC LIMIT 1");
            int i = 0;
            while (rs3.next() && i < limit)
            {

                getreturnnumber[i] = rs3.getString(1) ;
                i++;
            }
            con3.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return getreturnnumber;
    }
    public void addQuantinStock(Statement stmt, Connection con,int prodID , int prodQuant, int prodbonus){
        try {
            String insertQuery = "UPDATE `products_stock` SET `in_stock`='"+prodQuant+"',`bonus_quant`='"+prodbonus+"' WHERE `product_id` = '"+prodID+"'";
            stmt.executeUpdate(insertQuery);
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void addQuant_inbatchStock(Statement stmt, Connection con,int prodID ,String Batchnumber, int prodQuant, int prodbonus){
        try {
            String insertQuery = "UPDATE `batchwise_stock` SET `quantity`='"+prodQuant+"',`bonus`='"+prodbonus+"' WHERE `prod_id` = '"+prodID+"' and `batch_no` = '"+Batchnumber+"'";
            stmt.executeUpdate(insertQuery);
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void changeretrnBit(Statement stmt, Connection con,String Batchnumbers,int ProductsIDs, int OrderInfoID)
    {
        int rtnbit = 1;
        try {
            String insertQuery = "UPDATE `order_info_detailed` SET cmplt_returned_bit = '"+rtnbit+"' WHERE `batch_number`='"+Batchnumbers+"' and  `order_id`='"+OrderInfoID+"' and `product_id` = '"+ProductsIDs+"'";
            stmt.executeUpdate(insertQuery);
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void enterretrnrtrnquant(Statement stmt, Connection con,String batchnumbers,int ProductsIDs, int OrderInfoID,int rtnrndquant,int rtrndbonusquant)
    {
        try {
            String insertQuery = "UPDATE `order_info_detailed` SET returned_quant = '"+rtnrndquant+"' , returned_bonus_quant = '"+rtrndbonusquant+"' WHERE `batch_number`='"+batchnumbers+"' and `order_id`='"+OrderInfoID+"' and `product_id` = '"+ProductsIDs+"'";
            stmt.executeUpdate(insertQuery);
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void insertReturnDetails(Statement stmt, Connection con, int retrn_ids,  int productsID,String productsBatchs, int productsQuants,int bonusQaunts, float discAmounts, float totalAmounts,String packUnit)
    {
        String retrnReason = "Expiry";
        try {
            String insertQuery = "INSERT INTO `order_return_detail` (`return_id`, `prod_id`, `prod_batch`, `prod_quant`, `bonus_quant`, `discount_amount` , `total_amount` , `Unit`, `return_reason`) VALUES ('"+retrn_ids+"','"+productsID+"','"+productsBatchs+"','"+productsQuants+"','"+bonusQaunts+"','"+discAmounts+"','"+totalAmounts+"' ,'"+packUnit+"','"+retrnReason+"')";
            stmt.executeUpdate(insertQuery);
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void insertreturnInvoiceInfo(Statement stmt, Connection con, int retrn_id, int dealer_id, float retrn_totalprice, float retrn_grossprice,int invoNumb, String currntDate, String currnttime, String updateDate, String updatetime, int user_id)
    {
       String returnstatus = "Active";
        try {
            String insertQuery = "INSERT INTO `order_return` (`return_id`, `dealer_id`, `return_total_price`, `return_gross_price`, `order_id`, `entry_date` , `entry_time` , `update_date` , `update_time` , `user_id`, `status`) VALUES ('"+retrn_id+"','"+dealer_id+"','"+retrn_totalprice+"','"+retrn_grossprice+"','"+invoNumb+"','"+currntDate+"' ,'"+currnttime+"', '"+updateDate+"','"+updatetime+"','"+user_id+"','"+returnstatus+"')";
            stmt.executeUpdate(insertQuery);
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    ArrayList<String> prod_batchs = new ArrayList<>();
    public  ArrayList<String> getprod_batchs(Statement stmt2, Connection con2, int product_IDS)
    {
        ResultSet rs = null;
        try {
            rs = stmt2.executeQuery("select batch_no from batchwise_stock where prod_id ='"+product_IDS+"' and quantity>0");
            int i = 0;
            while (rs.next() && i < limit)
            {

                prod_batchs.add( rs.getString(1));
                i++;
            }

            con2.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return prod_batchs;
    }

    String[] prod_details = new String[1];
    public String[] get_prodDetailswithIDs(Statement stmt1, Connection con1 , int prods_Ids)
    {
        ResultSet rs = null;
        try {
            rs = stmt1.executeQuery("select product_name,retail_price,purchase_price,trade_price,company_id  from product_info where  product_id = '"+prods_Ids+"'");
            int i = 0;
            while (rs.next() && i < limit)
            {

                prod_details[i] = rs.getString(1) + "--" + rs.getString(2) + "--" + rs.getString(3) + "--" + rs.getString(4) +"--"+rs.getString(5);
            }

            con1.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return prod_details;
    }

    String[] prod_ids = new String[1];
    public String[] cnfrmprodID_inSaleInvoice(Statement stmt, Connection con ,int saleinvo, int product_Ids)
    {
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("select product_id from order_info_detailed where product_id = '"+product_Ids+"' and order_id = '"+saleinvo+"' ");
            int i = 0;
            while (rs.next() && i < limit)
            {
                prod_ids[i] = rs.getString(1) ;
            }

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return prod_ids;
    }
     String[] prod_stockquant = new String[1];
    public String[] getprodstockquant_withIDs(Statement stmt, Connection con , int product_Ids)
    {
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("select in_stock,bonus_quant from products_stock where product_id = '"+product_Ids+"' and status = 'Active'");
            int i = 0;
            while (rs.next() && i < limit)
            {
                prod_stockquant[i] = rs.getString(1) + "--" + rs.getString(2) ;
            }

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return prod_stockquant;
    }

    String[] dealers_details = new String[1];
    public String[] getdealersdetails(Statement stmt, Connection con , int dealersID)
    {
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("select dealer_name,dealer_address,dealer_area_id,dealer_lic_num from dealer_info where dealer_id ='"+dealersID+"' ");
            int i = 0;
            while (rs.next() && i < limit)
            {
                dealers_details[i] = rs.getString(1) + "--" + rs.getString(2) + "--" + rs.getString(3) + "--" + rs.getString(4);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dealers_details;
    }

    String[] deleardetails = new String[1];
    public String[] getdealerDetailswithIDs(Statement stmt, Connection con, int dealer_Ids )
    {
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("select dealer_name,dealer_address,dealer_lic_num,dealer_area_id from dealer_info where dealer_id ='"+dealer_Ids+"' ");
            int i = 0;
            while (rs.next() && i < limit)
            {

                deleardetails[i] = rs.getString(1) + "--" + rs.getString(2) + "--"+ rs.getString(3) + "--"+ rs.getString(4) ;
            }

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return deleardetails;

    }

    String[] prevOverallRecord = new String[1];
    public String[] getprevious_dealeroverallRecord(Statement stmt3, Connection con3,int dealerID )
    {
        ResultSet rs3 = null;
        try {
            rs3 = stmt3.executeQuery("select ordered_packets,submitted_packets,ordered_boxes,submitted_boxes,order_price,discount_price,invoiced_price,pending_payments from dealer_overall_record where dealer_id= '"+dealerID+"' ");
            int i = 0;
            while (rs3.next() && i < limit)
            {

                prevOverallRecord[i] = rs3.getString(1) +","+rs3.getString(2) +","+rs3.getString(3)+","+rs3.getString(4) +","+rs3.getString(5)+","+rs3.getString(6)+","+rs3.getString(7)+","+rs3.getString(8);
                i++;
            }
            con3.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return prevOverallRecord;
    }

    public void updatedealerOverallRecord(Statement stmt, Connection con,  int dealersid, int ordered_packets,int submitted_packets,int ordered_boxes, int submitted_boxes , float order_price,float discount_price, float invoiced_price, float pending_payments)
    {

        try {
            String insertQuery = "UPDATE `dealer_overall_record` SET  `ordered_packets` = '"+ordered_packets+"'  , `submitted_packets` = '"+submitted_packets+"'  , `ordered_boxes` = '"+ordered_boxes+"'  , `submitted_boxes` = '"+submitted_boxes+"'  , `order_price` = '"+order_price+"'  ,`discount_price` = '"+discount_price+"'  , `invoiced_price` = '"+invoiced_price+"'  , `pending_payments` = '"+pending_payments+"' where `dealer_id` = '"+dealersid+"' ";
            stmt.executeUpdate(insertQuery);
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertdealerOverallRecord(Statement stmt, Connection con, int dealersid, int ordergiven,int successful_orders, int ordered_packets,int submitted_packets,int ordered_boxes, int submitted_boxes , float order_price,float discount_price, float invoiced_price,float waived_off_price, float pending_payments)
    {

        try {
            String insertQuery = "INSERT INTO `dealer_overall_record` (`dealer_id`, `orders_given`, `successful_orders`, `ordered_packets`, `submitted_packets`, `ordered_boxes`, `submitted_boxes`, `order_price`,`discount_price`, `invoiced_price`, `waived_off_price`, `pending_payments` ) VALUES ('"+dealersid+"','"+ordergiven+"','"+successful_orders+"','"+ordered_packets+"','"+submitted_packets+"','"+ordered_boxes+"','"+submitted_boxes+"' ,'"+order_price+"','"+discount_price+"' ,'"+invoiced_price+"','"+waived_off_price+"','"+pending_payments+"' )";
            stmt.executeUpdate(insertQuery);
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    String delearoverallRecord;
    public String getdealer_overallRecord(Statement stmt, Connection con, int dealer_Ids )
    {
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("select pending_payments  from dealer_overall_record where dealer_id ='"+dealer_Ids+"' ");
            int i = 0;
            while (rs.next() && i < limit)
            {

                delearoverallRecord = rs.getString(1) ;
            }

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return delearoverallRecord;

    }

    public void insertNewServerReturns(ArrayList<ArrayList<String>> returnData, ArrayList<ArrayList<String>> returnDetailData)
    {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        String status = "Active";
        boolean check = false;
        ArrayList<String> dealerIds = new ArrayList<>();
        ArrayList<String> returnTotalPrice = new ArrayList<>();
        ArrayList<String> entryDate = new ArrayList<>();
        ArrayList<String> entryTime = new ArrayList<>();
        ArrayList<String> serverReturnIds = new ArrayList<>();
        ArrayList<String> returnIds = new ArrayList<>();
        int num = 0;
        try {
            String insertQuery = "INSERT INTO `order_return`(`dealer_id`, `return_total_price`, `return_gross_price`, `order_id`, `entry_date`, `entry_time`, `enter_user_id`, `status`) VALUES ";
            for(int i=0; i<returnData.size(); i++)
            {
                serverReturnIds.add(returnData.get(i).get(0));
                if(i == returnData.size()-1)
                {
                    insertQuery += "('"+returnData.get(i).get(1)+"','"+returnData.get(i).get(2)+"','"+returnData.get(i).get(3)+"','"+returnData.get(i).get(4)+"' ,'"+returnData.get(i).get(5)+"' ,'"+returnData.get(i).get(6)+"','"+returnData.get(i).get(7)+"','"+status+"') ";
                }
                else
                {
                    insertQuery += "('"+returnData.get(i).get(1)+"','"+returnData.get(i).get(2)+"','"+returnData.get(i).get(3)+"','"+returnData.get(i).get(4)+"' ,'"+returnData.get(i).get(5)+"' ,'"+returnData.get(i).get(6)+"','"+returnData.get(i).get(7)+"','"+status+"'), ";
                }
                dealerIds.add(returnData.get(i).get(1));
                returnTotalPrice.add(returnData.get(i).get(2));
                entryDate.add(returnData.get(i).get(5));
                entryTime.add(returnData.get(i).get(6));
            }
            objStmt.executeUpdate(insertQuery);

            ResultSet rs = null;
            for(int i=0; i<dealerIds.size(); i++)
            {
                String query = "SELECT `return_id` FROM `order_return` WHERE `dealer_id` = '"+dealerIds.get(i)+"' AND `return_total_price` = '"+returnTotalPrice.get(i)+"' AND `entry_date` = '"+entryDate.get(i)+"' AND `entry_time` = '"+entryTime.get(i)+"' LIMIT 1";
                rs = objStmt.executeQuery(query);
                while (rs.next())
                {
                    returnIds.add(rs.getString(1));
                }
            }

            for(int i=0; i<returnIds.size(); i++)
            {
                check = false;
                String query = "INSERT INTO `order_return_detail`(`return_id`, `prod_id`, `prod_batch`, `prod_quant`, `bonus_quant`, `discount_amount`, `total_amount`, `unit`, `return_reason`) VALUES ";
                while (returnDetailData.size() > num && serverReturnIds.get(i).equals(returnDetailData.get(num).get(0)))
                {
                    if(check)
                        query += ", ";
                    query += "('"+returnIds.get(i)+"','"+returnDetailData.get(num).get(1)+"','"+returnDetailData.get(num).get(2)+"','"+returnDetailData.get(num).get(3)+"','"+returnDetailData.get(num).get(4)+"','"+returnDetailData.get(num).get(5)+"','"+returnDetailData.get(num).get(6)+"','"+returnDetailData.get(num).get(7)+"','"+returnDetailData.get(num).get(8)+"')";
                    num++;
                    check = true;
                }
                if(check)
                {
                    objStmt.executeUpdate(query);
                }
            }
            SendServerThread objSendServerThread = new SendServerThread(serverReturnIds, returnIds, "Set new Return Status");
            objSendServerThread.start();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
