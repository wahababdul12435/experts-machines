package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import controller.DeleteWindow;
import controller.UpdateCompany;
import controller.UpdateGroup;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;

public class ViewGroupsInfo {
    private String srNo;
    private String groupTableId;
    private String groupId;
    private String companyId;
    private String companyName;
    private String groupName;
    private String products;
    private String groupStatus;

    private HBox operationsPane;
    private JFXButton btnView;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    public static StackPane stackPane;
    public static JFXDialog dialog;
    public static Label lblUpdate;

    public static String viewGroupId;

    public ViewGroupsInfo()
    {
        this.srNo = "";
        this.groupTableId = "";
        this.groupId = "";
        this.companyId = "";
        this.companyName = "";
        this.groupName = "";
        this.products = "";
        this.groupStatus = "";

        this.btnView = new JFXButton();
        this.btnEdit = new JFXButton();
        this.btnDelete = new JFXButton();
        this.btnView.setGraphic(createViewIcon());
        this.btnEdit.setGraphic(createEditIcon());
        this.btnDelete.setGraphic(createDeleteIcon());
        this.btnView.getStyleClass().add("btn_icon");
        this.btnEdit.getStyleClass().add("btn_icon");
        this.btnDelete.getStyleClass().add("btn_icon");
        this.operationsPane = new HBox(btnView, btnEdit, btnDelete);
        lblUpdate = new Label("");
    }

    public ViewGroupsInfo(String srNo, String groupTableId, String groupId, String companyId, String companyName, String groupName, String products, String groupStatus) {
        this.srNo = srNo;
        this.groupTableId = groupTableId;
        this.groupId = groupId;
        this.companyId = companyId;
        this.companyName = companyName;
        this.groupName = groupName;
        this.products = products;
        this.groupStatus = groupStatus;

        this.btnView = new JFXButton();
        this.btnEdit = new JFXButton();
        this.btnDelete = new JFXButton();
        this.btnView.setGraphic(createViewIcon());
        this.btnEdit.setGraphic(createEditIcon());
        this.btnDelete.setGraphic(createDeleteIcon());
        this.btnView.getStyleClass().add("btn_icon");
        this.btnEdit.getStyleClass().add("btn_icon");
        this.btnDelete.getStyleClass().add("btn_icon");
        this.operationsPane = new HBox(btnView, btnEdit, btnDelete);
        lblUpdate = new Label("");

        this.btnView.setOnAction((action)->viewClicked());
        this.btnEdit.setOnAction((action)->editClicked());
        this.btnDelete.setOnAction(event -> {
            try {
                deleteGroup(this.groupTableId);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void viewClicked()
    {
        Parent parent = null;
        viewGroupId = this.groupTableId;
        try {
            parent = FXMLLoader.load(getClass().getResource("../view/group_detail.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(parent);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    public void deleteGroup(String groupTableId) throws IOException {
        DeleteWindow.sceneWindow = "ViewGroup";
        DeleteWindow.deletionId = groupTableId;
        Parent parent = null;
        try {
            parent = FXMLLoader.load(getClass().getResource("../view/delete_window.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setStyle("-fx-padding: -40 -20 -40 -20;");
        dialogLayout.setBody(parent);
        dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        stackPane.setVisible(true);
        dialog.show();
        dialog.setOverlayClose(false);
    }

    public void editClicked()
    {
        Parent parent = null;
        try {
            UpdateGroup.groupTableId = groupTableId;
            UpdateGroup.groupId = groupId;
            UpdateGroup.companyId = companyId;
            UpdateGroup.companyName = companyName;
            UpdateGroup.groupName = groupName;
            UpdateGroup.groupStatus = groupStatus;
            parent = FXMLLoader.load(getClass().getResource("../view/update_group.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setStyle("-fx-padding: -20 -10 -20 -10;");
        dialogLayout.setHeading(lblUpdate);
        dialogLayout.setBody(parent);
        dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        stackPane.setVisible(true);
        dialog.show();
        dialog.setOverlayClose(false);
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getGroupTableId() {
        return groupTableId;
    }

    public void setGroupTableId(String groupTableId) {
        this.groupTableId = groupTableId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getProducts() {
        return products;
    }

    public void setProducts(String products) {
        this.products = products;
    }

    public String getGroupStatus() {
        return groupStatus;
    }

    public void setGroupStatus(String groupStatus) {
        this.groupStatus = groupStatus;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public ArrayList<ArrayList<String>> getGroupsInfo(Statement stmt, Connection con)
    {
        ArrayList<ArrayList<String>> groupData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT `groups_info`.`group_table_id`, `groups_info`.`group_id`, `groups_info`.`company_id`, `company_info`.`company_name`, `groups_info`.`group_name`, COUNT(`product_info`.`product_table_id`) as 'total_products', `groups_info`.`group_status` FROM `groups_info` LEFT OUTER JOIN `company_info` ON `groups_info`.`company_id` = `company_info`.`company_table_id` LEFT OUTER JOIN `product_info` ON `product_info`.`group_table_id` = `groups_info`.`group_table_id` WHERE `groups_info`.`group_status` != 'Deleted' GROUP BY `groups_info`.`group_table_id`");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2));
                temp.add(rs.getString(3));
                temp.add(rs.getString(4));
                temp.add(rs.getString(5));
                temp.add(rs.getString(6));
                temp.add(rs.getString(7));
                groupData.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return groupData;
    }

    public ArrayList<ArrayList<String>> getGroupsSearch(Statement stmt, Connection con, String groupId, String groupName, String companyId, String groupStatus)
    {
        ArrayList<ArrayList<String>> groupData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        String searchQuery;
        int multipleSearch = 0;
        searchQuery = "SELECT `groups_info`.`group_table_id`, `groups_info`.`group_id`, `groups_info`.`company_id`, `company_info`.`company_name`, `groups_info`.`group_name`, COUNT(`product_info`.`product_table_id`) as 'total_products', `groups_info`.`group_status` FROM `groups_info` LEFT OUTER JOIN `company_info` ON `groups_info`.`company_id` = `company_info`.`company_table_id` LEFT OUTER JOIN `product_info` ON `product_info`.`group_table_id` = `groups_info`.`group_table_id` WHERE ";
        if(!groupId.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`groups_info`.`group_id` = '"+groupId+"'";
            multipleSearch++;
        }
        if(!groupName.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`groups_info`.`group_name` = '"+groupName+"'";
            multipleSearch++;
        }
        if(!companyId.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`groups_info`.`company_id` = '"+companyId+"'";
            multipleSearch++;
        }
        if(!groupStatus.equals("All"))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`groups_info`.`group_status` = '"+groupStatus+"'";
            multipleSearch++;
        }
        if(multipleSearch >= 1)
        {
            searchQuery += " AND ";
        }
        searchQuery += "`groups_info`.`group_status` != 'Deleted' GROUP BY `groups_info`.`group_table_id`";
        try {
            rs = stmt.executeQuery(searchQuery);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2));
                temp.add(rs.getString(3));
                temp.add(rs.getString(4));
                temp.add(rs.getString(5));
                temp.add(rs.getString(6));
                temp.add(rs.getString(7));
                groupData.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return groupData;
    }

    public ArrayList<String> getGroupsSummary(Statement stmt, Connection con)
    {
        ArrayList<String> groupData = new ArrayList<String>();
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT COUNT(`group_table_id`) FROM `groups_info` WHERE `group_status` != 'Deleted'");
            while (rs.next())
            {
                groupData.add(rs.getString(1));
            }
            rs = stmt.executeQuery("SELECT COUNT(`group_table_id`) FROM `groups_info` WHERE `group_status` = 'Active'");
            while (rs.next())
            {
                groupData.add(rs.getString(1));
            }
            rs = stmt.executeQuery("SELECT COUNT(`group_table_id`) FROM `groups_info` WHERE `group_status` = 'In Active'");
            while (rs.next())
            {
                groupData.add(rs.getString(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return groupData;
    }

    public void deleteCompany(String companyTableId) throws IOException {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        try {
            PreparedStatement updateEXP = objCon.prepareStatement("DELETE FROM `company_info` WHERE `company_table_id` = '"+companyTableId+"'");
            updateEXP.executeUpdate();
            objCon.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_company.fxml"));
        Stage objstage = (Stage) operationsPane.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    private Node createViewIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.EYE);
        graphic.setFontSize(20.0);
        return graphic;
    }

    private Node createEditIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.EDIT);
        graphic.setFontSize(20.0);
        return graphic;
    }

    private Node createDeleteIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.TRASH);
        graphic.setFontSize(20.0);
        return graphic;
    }
}
