package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import controller.EnterDistrict;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DistrictInfo {
    private String srNo;
    private String districtId;
    private String districtName;

    private HBox operationsPane;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    public static ArrayList<String> districtIdArr = new ArrayList<>();
    public static ArrayList<String> districtNameArr = new ArrayList<>();

    public static TableView<DistrictInfo> table_add_district;

    public static JFXTextField txtDistrictId;
    public static JFXTextField txtDistrictName;

    public static Button btnAdd;
    public static Button btnCancel;
    public static Button btnUpdate;

    MysqlCon objMysqlCon = new MysqlCon();
    Statement objStmt = objMysqlCon.stmt;
    Connection objCon = objMysqlCon.con;

    public DistrictInfo() {
        this.srNo = "";
        this.districtId = "";
        this.districtName = "";
    }

    public DistrictInfo(String srNo, String districtId, String districtName) {
        this.srNo = srNo;
        this.districtId = districtId;
        this.districtName = districtName;

        this.btnEdit = new JFXButton("Edit");
        this.btnDelete = new JFXButton("Delete");
        this.btnEdit.setOnAction((action)->editAddedRecord());
        this.btnDelete.setOnAction((action)->deleteAddedRecord());
        this.operationsPane = new HBox(btnEdit, btnDelete);
    }

    private void deleteAddedRecord()
    {
        districtIdArr.remove(Integer.parseInt(srNo)-1);
        districtNameArr.remove(Integer.parseInt(srNo)-1);
        table_add_district.setItems(EnterDistrict.parseUserList());
    }

    public void editAddedRecord()
    {
        EnterDistrict.srNo = Integer.parseInt(srNo)-1;
        txtDistrictId.setText(districtIdArr.get(Integer.parseInt(srNo)-1));
        txtDistrictName.setText(districtNameArr.get(Integer.parseInt(srNo)-1));
        btnAdd.setVisible(false);
        btnCancel.setVisible(true);
        btnUpdate.setVisible(true);
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getDistrictId() {
        return districtId;
    }

    public void setDistrictId(String districtId) {
        this.districtId = districtId;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public void insertDistrict(Statement stmt, Connection con) throws SQLException {
        String currentDate = GlobalVariables.getStDate();
        String currentTime = GlobalVariables.getStTime();
        String currentUser = GlobalVariables.getUserId();
        boolean withId = false;
        boolean withoutId = false;
        String insertQueryWithId = "INSERT INTO `district_info`(`district_id`, `district_name`, `district_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`) VALUES ";
        String insertQueryWithoutId = "INSERT INTO `district_info`(`district_name`, `district_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`) VALUES ";

        for(int i=0; i<districtIdArr.size(); i++)
        {
            if(districtIdArr.get(i).equals("") || districtIdArr.get(i) == null)
            {
                if(withoutId)
                {
                    insertQueryWithoutId += ", ";
                }
                withoutId = true;
                insertQueryWithoutId += "('"+districtNameArr.get(i)+"','Active','"+currentUser+"','"+currentDate+"','"+currentTime+"','"+currentUser+"','"+currentDate+"','"+currentTime+"')";
            }
            else
            {
                if(withId)
                {
                    insertQueryWithId += ", ";
                }
                withId = true;
                insertQueryWithId += "('"+districtIdArr.get(i)+"','"+districtNameArr.get(i)+"','Active','"+currentUser+"','"+currentDate+"','"+currentTime+"','"+currentUser+"','"+currentDate+"','"+currentTime+"')";
            }
        }
        int insertInfo = 1;
        if(withId)
        {
            if(stmt.executeUpdate(insertQueryWithId) == -1)
            {
                insertInfo = -1;
            }
        }
        if(withoutId)
        {
            if(stmt.executeUpdate(insertQueryWithoutId) == -1)
            {
                insertInfo = -1;
            }
        }

        if(insertInfo == -1)
        {
            String title = "Error";
            String message = "Unable to insert";
            GlobalVariables.showNotification(-1, title, message);
        }
        else
        {
            String title = "Successfull";
            String message = "Record Inserted";
            GlobalVariables.showNotification(1, title, message);
        }
        refreshAllArrays();
    }

    private void refreshAllArrays()
    {
        districtIdArr = new ArrayList<>();
        districtNameArr = new ArrayList<>();
    }

    public void updateDistrict(Statement stmt, Connection con, String districtTableId, String districtId, String districtName, String districtStatus)
    {
        String currentDate = GlobalVariables.getStDate();
        String currentTime = GlobalVariables.getStTime();
        String currentUser = GlobalVariables.getUserId();
        String updateQuery;
        try {
            if(districtId.equals("") || districtId.equals("N/A"))
            {
                updateQuery = "UPDATE `district_info` SET `district_id`= NULL,`district_name`='"+districtName+"',`district_status`='"+districtStatus+"', `update_user_id`='"+currentUser+"',`update_date`='"+currentDate+"',`update_time`='"+currentTime+"' WHERE `district_table_id` = '"+districtTableId+"'";
            }
            else
            {
                updateQuery = "UPDATE `district_info` SET `district_id`='"+districtId+"',`district_name`='"+districtName+"',`district_status`='"+districtStatus+"', `update_user_id`='"+currentUser+"',`update_date`='"+currentDate+"',`update_time`='"+currentTime+"' WHERE `district_table_id` = '"+districtTableId+"'";
            }

            stmt.executeUpdate(updateQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<ArrayList<String>> getAddedDistrict()
    {
        ArrayList<ArrayList<String>> districtData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        for(int i=0; i<districtIdArr.size(); i++)
        {
            temp = new ArrayList<String>();
            temp.add(districtIdArr.get(i));
            temp.add(districtNameArr.get(i));
            districtData.add(temp);
        }
        return districtData;
    }

    String checkDistrictId = new String();
    public String confirmNewId(String districtId)
    {
        ResultSet rs3 = null;
        try {
            rs3 = objStmt.executeQuery("SELECT `district_id` FROM `district_info` WHERE `district_id` = '"+districtId+"' AND `district_status` != 'Deleted'");
            while (rs3.next())
            {
                checkDistrictId = rs3.getString(1) ;
            }
//            objCon.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return checkDistrictId;
    }

    public ArrayList<ArrayList<String>> getDistrictsWithIds(Statement stmt, Connection con)
    {
        ArrayList<ArrayList<String>> districtData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT `district_table_id`, `district_id`, `district_name` FROM `district_info` WHERE `district_status` != 'Deleted'");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Table id
                temp.add(rs.getString(2)); // id
                if(rs.getString(2) != null && !rs.getString(2).equals(""))
                {
                    temp.add(rs.getString(3)+" ("+rs.getString(2)+")"); // name
                }
                else
                {
                    temp.add(rs.getString(3)); // name
                }
                districtData.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return districtData;
    }
}
