package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class MyAccountInfo {

    public MyAccountInfo()
    {

    }

    public ArrayList<String> getCompleteUserDetail(Statement stmt, Connection con, String userId)
    {
        ArrayList<String> userDetail = new ArrayList<String>();
        ResultSet rs = null;
        String query = "SELECT `user_accounts`.`user_name`, `user_accounts`.`user_password`, `user_info`.`user_name`, `user_info`.`user_contact`, `user_info`.`user_address`, `user_info`.`user_cnic`, `user_info`.`user_type`, `user_info`.`user_image`, `user_info`.`creating_date`, `user_info`.`user_status`, `user_rights`.`create_record`, `user_rights`.`read_record`, `user_rights`.`edit_record`, `user_rights`.`delete_record`, `user_rights`.`manage_stock`, `user_rights`.`manage_cash`, `user_rights`.`manage_settings`, `user_rights`.`mobile_app` FROM `user_accounts` INNER JOIN `user_info` ON `user_info`.`user_table_id` = `user_accounts`.`user_id` LEFT OUTER JOIN `user_rights` ON `user_accounts`.`user_id` = `user_rights`.`user_id` WHERE `user_accounts`.`user_id` = '"+userId+"'";
        try {
            rs = stmt.executeQuery(query);
            if(rs.next())
            {
                userDetail.add(rs.getString(1)); // user_name
                userDetail.add(rs.getString(2)); // password
                userDetail.add(rs.getString(3)); // name
                userDetail.add(rs.getString(4)); // contact
                userDetail.add(rs.getString(5)); // address
                userDetail.add(rs.getString(6)); // cnic
                userDetail.add(rs.getString(7)); // type
                userDetail.add(rs.getString(8)); // Image
                userDetail.add(rs.getString(9)); // reg date
                userDetail.add(rs.getString(10)); // status;
                userDetail.add(rs.getString(11) == null? "0" : rs.getString(11)); // create record
                userDetail.add(rs.getString(12) == null? "0" : rs.getString(12)); // read record
                userDetail.add(rs.getString(13) == null? "0" : rs.getString(13)); // edit record
                userDetail.add(rs.getString(14) == null? "0" : rs.getString(14)); // delete record
                userDetail.add(rs.getString(15) == null? "0" : rs.getString(15)); // booking
                userDetail.add(rs.getString(16) == null? "0" : rs.getString(16)); // cash
                userDetail.add(rs.getString(17) == null? "0" : rs.getString(17)); // policies
                userDetail.add(rs.getString(18) == null? "0" : rs.getString(18)); // mobile app
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userDetail;
    }

    public void updateAccountInfo(Statement stmt, Connection con, String userId, String userName, String password, String name, String contact, String address, String cnic, String userImage, String rightInsert, String rightView, String rightChange, String rightDelete, String rightStock, String rightCash, String rightSettings, String rightMobile)
    {
        String currentDate = GlobalVariables.getStDate();
        String currentTime = GlobalVariables.getStTime();
        String currentUser = GlobalVariables.getUserId();
        String updateQuery;
        try {
            updateQuery = "UPDATE `user_accounts` SET `user_name`= '"+userName+"', `user_password`='"+password+"' WHERE `user_id` = '"+userId+"'";
            stmt.executeUpdate(updateQuery);
            updateQuery = "UPDATE `user_info` SET `user_name`='"+name+"', `user_contact`='"+contact+"', `user_address`='"+address+"', `user_cnic`='"+cnic+"', `user_image`='"+userImage+"',  `update_user_id`= '"+currentUser+"', `update_date`='"+currentDate+"', `update_time`= '"+currentTime+"' WHERE `user_table_id` = '"+userId+"'";
            stmt.executeUpdate(updateQuery);
            updateQuery = "UPDATE `user_rights` SET `create_record`= '"+rightInsert+"',`read_record`='"+rightView+"',`edit_record`='"+rightChange+"',`delete_record`='"+rightDelete+"',`manage_stock`='"+rightStock+"',`manage_cash`='"+rightCash+"',`manage_settings`='"+rightSettings+"',`mobile_app`='"+rightMobile+"' WHERE `user_id` = '"+userId+"'";
            stmt.executeUpdate(updateQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
