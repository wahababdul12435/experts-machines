package model;

import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.HBox;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class CompanyReportInfo {
    private String srNo;
    private String companyTableId;
    private String companyId;
    private String companyName;
    private String orders;
    private String orderedItems;
    private String receivedItems;
    private String returnedItems;
    private String totalPurchase;
    private String cashSent;
    private String cashReturn;
    private String cashDiscount;
    private String cashWaiveOff;
    private String cashPending;

    private HBox operationsPane;
    private JFXButton btnView;
    private JFXButton btnEdit;

    public static String companyReportId;

    public CompanyReportInfo() {
    }

    public CompanyReportInfo(String srNo, String companyTableId, String companyId, String companyName, String orders, String orderedItems, String receivedItems, String returnedItems, String totalPurchase, String cashSent, String cashReturn, String cashDiscount, String cashWaiveOff, String cashPending) {
        this.srNo = srNo;
        this.companyTableId = companyTableId;
        this.companyId = companyId;
        this.companyName = companyName;
        this.orders = orders;
        this.orderedItems = orderedItems;
        this.receivedItems = receivedItems;
        this.returnedItems = returnedItems;
        this.totalPurchase = totalPurchase;
        this.cashSent = cashSent;
        this.cashReturn = cashReturn;
        this.cashDiscount = cashDiscount;
        this.cashWaiveOff = cashWaiveOff;
        this.cashPending = cashPending;

        this.btnView = new JFXButton();
        this.btnView.setGraphic(createViewIcon());
        this.btnView.getStyleClass().add("btn_icon");
        this.btnEdit = new JFXButton();
        this.btnEdit.setGraphic(createEditIcon());
        this.btnEdit.getStyleClass().add("btn_icon");
        this.operationsPane = new HBox(btnView, btnEdit);

        this.btnView.setOnAction((event)->viewCompanyDetail());
        this.btnEdit.setOnAction((event)->editClicked());
    }

    public void viewCompanyDetail()
    {
        Parent parent = null;
        try {
            companyReportId = companyTableId;
            parent = FXMLLoader.load(getClass().getResource("../view/company_report_detail.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(parent);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
    
    public void editClicked()
    {
        Parent parent = null;
        try {
            companyReportId = companyTableId;
            parent = FXMLLoader.load(getClass().getResource("../view/add_dealer_finance.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(parent);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getCompanyTableId() {
        return companyTableId;
    }

    public void setCompanyTableId(String dealerTableId) {
        this.companyTableId = dealerTableId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String dealerId) {
        this.companyId = dealerId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String dealerName) {
        this.companyName = dealerName;
    }

    public String getOrders() {
        return orders;
    }

    public void setOrders(String orders) {
        this.orders = orders;
    }

    public String getOrderedItems() {
        return orderedItems;
    }

    public void setOrderedItems(String orderedItems) {
        this.orderedItems = orderedItems;
    }

    public String getReceivedItems() {
        return receivedItems;
    }

    public void setReceivedItems(String receivedItems) {
        this.receivedItems = receivedItems;
    }

    public String getReturnedItems() {
        return returnedItems;
    }

    public void setReturnedItems(String returnedItems) {
        this.returnedItems = returnedItems;
    }

    public String getTotalPurchase() {
        return totalPurchase;
    }

    public void setTotalPurchase(String totalPurchase) {
        this.totalPurchase = totalPurchase;
    }

    public String getCashSent() {
        return cashSent;
    }

    public void setCashSent(String cashSent) {
        this.cashSent = cashSent;
    }

    public String getCashReturn() {
        return cashReturn;
    }

    public void setCashReturn(String cashReturn) {
        this.cashReturn = cashReturn;
    }

    public String getCashDiscount() {
        return cashDiscount;
    }

    public void setCashDiscount(String cashDiscount) {
        this.cashDiscount = cashDiscount;
    }

    public String getCashWaiveOff() {
        return cashWaiveOff;
    }

    public void setCashWaiveOff(String cashWaiveOff) {
        this.cashWaiveOff = cashWaiveOff;
    }

    public String getCashPending() {
        return cashPending;
    }

    public void setCashPending(String cashPending) {
        this.cashPending = cashPending;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public ArrayList<ArrayList<String>> getCompanyReportInfo(Statement stmt, Connection con)
    {
        ArrayList<ArrayList<String>> dealerReportData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT `company_info`.`company_table_id`, `company_info`.`company_id`, `company_info`.`company_name`, `company_overall_record`.`orders_given`, `company_overall_record`.`ordered_packets`, `company_overall_record`.`received_packets`, `company_overall_record`.`return_packets`, `company_overall_record`.`invoiced_price`, `company_overall_record`.`cash_sent`, `company_overall_record`.`cash_return`, `company_overall_record`.`discount_price`, `company_overall_record`.`waived_off_price`, `company_overall_record`.`pending_payments` FROM `company_info` LEFT OUTER JOIN `company_overall_record` ON `company_info`.`company_table_id` = `company_overall_record`.`company_id` WHERE `company_info`.`company_status` != 'Deleted'");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Table Id
                temp.add(rs.getString(2)); // Id
                temp.add(rs.getString(3)); // Name
                temp.add(rs.getString(4)); // Order
                temp.add(rs.getString(5)); // Ordered Items
                temp.add(rs.getString(6)); // Received Items
                temp.add(rs.getString(7)); // Returned Items
                temp.add(rs.getString(8)); // Purchase
                temp.add(rs.getString(9)); // Sent
                temp.add(rs.getString(10)); // Return
                temp.add(rs.getString(11)); // Discount
                temp.add(rs.getString(12)); // Waive Off
                temp.add(rs.getString(13)); // Pending Payment
                dealerReportData.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dealerReportData;
    }

    public ArrayList<ArrayList<String>> getCompanyReportSearch(Statement stmt, Connection con, String dealerId, String dealerName, String areaName, String dealerType)
    {
        ArrayList<ArrayList<String>> dealerReportData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        String searchQuery;
        int multipleSearch = 0;
        searchQuery = "SELECT `dealer_info`.`dealer_table_id`, `dealer_info`.`dealer_id`, `dealer_info`.`dealer_name`, `area_info`.`area_name`, `dealer_info`.`dealer_type`, `dealer_overall_record`.`orders_given`, `dealer_overall_record`.`ordered_packets`, `dealer_overall_record`.`submitted_packets`, `dealer_overall_record`.`return_packets`, `dealer_overall_record`.`invoiced_price`, `dealer_overall_record`.`cash_collected`, `dealer_overall_record`.`cash_return`, `dealer_overall_record`.`discount_price`, `dealer_overall_record`.`waived_off_price`, `dealer_overall_record`.`pending_payments` FROM `dealer_info` LEFT OUTER JOIN `area_info` ON `dealer_info`.`dealer_area_id` = `area_info`.`area_table_id` LEFT OUTER JOIN `dealer_overall_record` ON `dealer_info`.`dealer_table_id` = `dealer_overall_record`.`dealer_id` WHERE ";
        if(!dealerId.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`dealer_info`.`dealer_id` = '"+dealerId+"'";
            multipleSearch++;
        }
        if(!dealerName.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`dealer_info`.`dealer_name` = '"+dealerName+"'";
            multipleSearch++;
        }
        if(!areaName.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`area_info`.`area_name` = '"+areaName+"'";
            multipleSearch++;
        }
        if(!dealerType.equals("All"))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`dealer_info`.`dealer_type` = '"+dealerType+"'";
            multipleSearch++;
        }
        if(multipleSearch >= 1)
        {
            searchQuery += " AND ";
        }
        searchQuery += "`dealer_info`.`dealer_status` != 'Deleted'";
        try {
            rs = stmt.executeQuery(searchQuery);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Table Id
                temp.add(rs.getString(2)); // Id
                temp.add(rs.getString(3)); // Name
                temp.add(rs.getString(4)); // area
                temp.add(rs.getString(5)); // Dealer Type
                temp.add(rs.getString(6)); // Order
                temp.add(rs.getString(7)); // Ordered Items
                temp.add(rs.getString(8)); // Submitted Items
                temp.add(rs.getString(9)); // Returned Items
                temp.add(rs.getString(10)); // Sale
                temp.add(rs.getString(11)); // Collected
                temp.add(rs.getString(12)); // Return
                temp.add(rs.getString(13)); // Discount
                temp.add(rs.getString(14)); // Waive Off
                temp.add(rs.getString(15)); // Pending Payment
                dealerReportData.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dealerReportData;
    }

    public ArrayList<ArrayList<String>> getCompanyReportSummary(Statement stmt, Connection con, String summaryBase)
    {
        ArrayList<ArrayList<String>> dealerReportData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        String searchQuery;
        searchQuery = "SELECT `dealer_info`.`dealer_table_id`, `dealer_info`.`dealer_id`, `dealer_info`.`dealer_name`, `area_info`.`area_name`, `dealer_info`.`dealer_type`, `dealer_overall_record`.`orders_given`, `dealer_overall_record`.`ordered_packets`, `dealer_overall_record`.`submitted_packets`, `dealer_overall_record`.`return_packets`, `dealer_overall_record`.`invoiced_price`, `dealer_overall_record`.`cash_collected`, `dealer_overall_record`.`cash_return`, `dealer_overall_record`.`discount_price`, `dealer_overall_record`.`waived_off_price`, `dealer_overall_record`.`pending_payments` FROM `dealer_info` LEFT OUTER JOIN `area_info` ON `dealer_info`.`dealer_area_id` = `area_info`.`area_table_id` LEFT OUTER JOIN `dealer_overall_record` ON `dealer_info`.`dealer_table_id` = `dealer_overall_record`.`dealer_id` WHERE `dealer_info`.`dealer_status` != 'Deleted'";
        if(!summaryBase.equals(""))
        {
            searchQuery += " AND ";
            if(summaryBase.equals("Pending"))
                searchQuery += "`dealer_overall_record`.`pending_payments` != '0'";
            else if(summaryBase.equals("Waive Off"))
                searchQuery += "`dealer_overall_record`.`waived_off_price` != '0'";
            else if(summaryBase.equals("Sale"))
                searchQuery += "`dealer_overall_record`.`invoiced_price` != '0'";
            else if(summaryBase.equals("Collection"))
                searchQuery += "`dealer_overall_record`.`cash_collected` != '0'";
            else if(summaryBase.equals("Return"))
                searchQuery += "`dealer_overall_record`.`cash_return` != '0'";
            else if(summaryBase.equals("Discount"))
                searchQuery += "`dealer_overall_record`.`discount_price` != '0'";
        }
        try {
            rs = stmt.executeQuery(searchQuery);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Table Id
                temp.add(rs.getString(2)); // Id
                temp.add(rs.getString(3)); // Name
                temp.add(rs.getString(4)); // area
                temp.add(rs.getString(5)); // Dealer Type
                temp.add(rs.getString(6)); // Order
                temp.add(rs.getString(7)); // Ordered Items
                temp.add(rs.getString(8)); // Submitted Items
                temp.add(rs.getString(9)); // Returned Items
                temp.add(rs.getString(10)); // Sale
                temp.add(rs.getString(11)); // Collected
                temp.add(rs.getString(12)); // Return
                temp.add(rs.getString(13)); // Discount
                temp.add(rs.getString(14)); // Waive Off
                temp.add(rs.getString(15)); // Pending Payment
                dealerReportData.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dealerReportData;
    }

    private Node createEditIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.EDIT);
        graphic.setFontSize(20.0);
        return graphic;
    }

    private Node createViewIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.EYE);
        graphic.setFontSize(20.0);
        return graphic;
    }
}
