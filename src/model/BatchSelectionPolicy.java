package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class BatchSelectionPolicy {

    public void updateSelectedPolicy(Statement stmt, Connection con,String policy_value){
        try {
            String insertQuery = "UPDATE `system_settings` SET `setting_value`='"+policy_value+"' WHERE `setting_id` = 1";
            stmt.executeUpdate(insertQuery);
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    String[] selectBatchPolicy = new String[1];
    public String[] getprev_batchslctionPolicy(Statement stmt3, Connection con3,int policyID )
    {
        ResultSet rs3 = null;
        try {
            rs3 = stmt3.executeQuery("select setting_value from system_settings where setting_id= '"+policyID+"' ");
            int i = 0;
            while (rs3.next() && i < 5)
            {
                selectBatchPolicy[i] = rs3.getString(1);
            }
            con3.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return selectBatchPolicy;
    }
   
       
}
