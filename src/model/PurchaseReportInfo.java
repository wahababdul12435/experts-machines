package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import javafx.scene.Node;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

public class PurchaseReportInfo {
    private String srNo;
    private String date;
    private String day;
    private String purchaseInvoice;
    private String purchasePrice;
    private String quantityReturned;
    private String returnPrice;
    private String cashCollection;
    private String cashReturn;
    private String cashWaiveOff;

    private HBox operationsPane;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    SimpleDateFormat fmt = new SimpleDateFormat("dd/MMM/yyyy");

    public static StackPane stackPane;
    public static JFXDialog dialog;

    public PurchaseReportInfo() {
        this.srNo = "";
//        this.paymentId = "";
//        this.dealerId = "";
//        this.dealerName = "";
        this.date = "";
        this.day = "";
        this.purchaseInvoice = "";
        this.quantityReturned = "";
        this.returnPrice = "";
        this.cashCollection = "";
        this.cashReturn = "";
        this.cashWaiveOff = "";
    }

    public PurchaseReportInfo(String srNo, String date, String day, String purchaseInvoice, String purchasePrice, String quantityReturned, String returnPrice, String cashCollection, String cashReturn, String cashWaiveOff) {
        this.srNo = srNo;
//        this.paymentId = paymentId;
//        this.dealerId = dealerId;
//        this.dealerName = dealerName;
        this.date = date;
        this.day = day;
        this.purchaseInvoice = purchaseInvoice;
        this.purchasePrice = purchasePrice;
        this.quantityReturned = quantityReturned;
        this.returnPrice = returnPrice;
        this.cashCollection = cashCollection;
        this.cashReturn = cashReturn;
        this.cashWaiveOff = cashWaiveOff;

        this.btnEdit = new JFXButton();
        this.btnDelete = new JFXButton();
        this.btnEdit.setGraphic(createEditIcon());
        this.btnDelete.setGraphic(createDeleteIcon());
        this.btnEdit.getStyleClass().add("btn_icon");
        this.btnDelete.getStyleClass().add("btn_icon");
        this.operationsPane = new HBox(btnEdit, btnDelete);
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getPurchaseInvoice() {
        return purchaseInvoice;
    }

    public void setPurchaseInvoice(String purchaseInvoice) {
        this.purchaseInvoice = purchaseInvoice;
    }

    public String getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(String purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public String getQuantityReturned() {
        return quantityReturned;
    }

    public void setQuantityReturned(String quantityReturned) {
        this.quantityReturned = quantityReturned;
    }

    public String getReturnPrice() {
        return returnPrice;
    }

    public void setReturnPrice(String returnPrice) {
        this.returnPrice = returnPrice;
    }

    public String getCashCollection() {
        return cashCollection;
    }

    public void setCashCollection(String cashCollection) {
        this.cashCollection = cashCollection;
    }

    public String getCashReturn() {
        return cashReturn;
    }

    public void setCashReturn(String cashReturn) {
        this.cashReturn = cashReturn;
    }

    public String getCashWaiveOff() {
        return cashWaiveOff;
    }

    public void setCashWaiveOff(String cashWaiveOff) {
        this.cashWaiveOff = cashWaiveOff;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public static StackPane getStackPane() {
        return stackPane;
    }

    public static void setStackPane(StackPane stackPane) {
        PurchaseReportInfo.stackPane = stackPane;
    }

    public static JFXDialog getDialog() {
        return dialog;
    }

    public static void setDialog(JFXDialog dialog) {
        PurchaseReportInfo.dialog = dialog;
    }

    public ArrayList<ArrayList<String>> getPurchaseReportDetail(Statement stmt1, Statement stmt2, Statement stmt3, Connection con) throws ParseException {
        ArrayList<ArrayList<String>> purchaseReportData1 = new ArrayList<ArrayList<String>>();
        ArrayList<ArrayList<String>> purchaseReportData2 = new ArrayList<ArrayList<String>>();
        ArrayList<ArrayList<String>> purchaseReportData3 = new ArrayList<ArrayList<String>>();
        ArrayList<ArrayList<String>> purchaseReportData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs1 = null;
        ResultSet rs2 = null;
        ResultSet rs3 = null;
        try {
            rs1 = stmt1.executeQuery("SELECT COUNT(`invoice_num`), SUM(`net_amount`), `purchase_date` FROM `purchase_info` WHERE `status` != 'Deleted' GROUP BY `purchase_date` ORDER BY str_to_date(`purchase_date`, '%d/%b/%Y') DESC");
            while (rs1.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs1.getString(1)); // Invoice Num
                temp.add(rs1.getString(2)); // Purchase Price
                temp.add(rs1.getString(3)); // Purchase Date
                purchaseReportData1.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            rs2 = stmt2.executeQuery("SELECT `purchase_return`.`entry_date`, SUM(`purchase_return_detail`.`prod_quant`) as 'return_quantity', SUM(`purchase_return_detail`.`total_amount`) as 'return_amount' FROM `purchase_return` INNER JOIN `purchase_return_detail` ON `purchase_return_detail`.`return_id` = `purchase_return`.`return_id` WHERE `purchase_return`.`status` != 'Deleted' GROUP BY `purchase_return`.`entry_date` ORDER BY str_to_date(`purchase_return`.`entry_date`, '%d/%b/%Y') DESC");
            while (rs2.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs2.getString(1)); // Return Date
                temp.add(rs2.getString(2)); // Return Quantity
                temp.add(rs2.getString(3)); // Return Price
                purchaseReportData2.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            rs3 = stmt3.executeQuery("SELECT `company_payments`.`payment_id`, `company_payments`.`date`, `company_payments`.`amount`, `company_payments`.`cash_type` FROM `company_payments` WHERE `company_payments`.`status` != 'Deleted' ORDER BY str_to_date(`date`, '%d/%b/%Y') DESC");
            while (rs3.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs3.getString(1)); // Payment Id
                temp.add(rs3.getString(2)); // Payment Date
                temp.add(rs3.getString(3)); // Amount
                temp.add(rs3.getString(4)); // Payment Type
                purchaseReportData3.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        String stDatePurchase = "";
        String stDateReturn = "";
        String stDatePayment = "";
        Date datePurchase = null;
        Date dateReturn = null;
        Date datePayment = null;
        ArrayList<Date> objDatesArray;
        int day;
        String stDay = "";
        boolean newDataAdd = true;

        while (purchaseReportData1.size() > 0 || purchaseReportData2.size() > 0 || purchaseReportData3.size() > 0)
        {
            temp = new ArrayList<>();
            objDatesArray = new ArrayList<>();
            if(purchaseReportData1.size() > 0)
            {
                stDatePurchase = purchaseReportData1.get(0).get(2);
                datePurchase = fmt.parse(stDatePurchase);
                objDatesArray.add(datePurchase);
            }
            if(purchaseReportData2.size() > 0)
            {
                stDateReturn = purchaseReportData2.get(0).get(0);
                dateReturn = fmt.parse(stDateReturn);
                objDatesArray.add(dateReturn);
            }
            if(purchaseReportData3.size() > 0)
            {
                stDatePayment = purchaseReportData3.get(0).get(1);
                datePayment = fmt.parse(stDatePayment);
                objDatesArray.add(datePayment);
            }
            Date maxDate = Collections.max(objDatesArray);
            day = maxDate.getDay();
            if(day == 0)
                stDay = "Sunday";
            else if(day == 1)
                stDay = "Monday";
            else if(day == 2)
                stDay = "Tuesday";
            else if(day == 3)
                stDay = "Wednesday";
            else if(day == 4)
                stDay = "Thursday";
            else if(day == 5)
                stDay = "Friday";
            else if(day == 6)
                stDay = "Saturday";
            if(datePurchase != null && maxDate.equals(datePurchase) && purchaseReportData1.size() > 0)
            {
                if(purchaseReportData.size() > 0 && purchaseReportData.get(purchaseReportData.size()-1).get(0).equals(stDatePurchase))
                {
                    newDataAdd = false;
                    if(purchaseReportData.get(purchaseReportData.size()-1).get(2).equals("-"))
                    {
                        purchaseReportData.get(purchaseReportData.size()-1).set(2, purchaseReportData1.get(0).get(0));
                        purchaseReportData.get(purchaseReportData.size()-1).set(3, purchaseReportData1.get(0).get(1));
                    }
                    else
                    {
                        String value1 = purchaseReportData.get(purchaseReportData.size()-1).get(2);
                        String value2 = purchaseReportData.get(purchaseReportData.size()-1).get(3);
                        purchaseReportData.get(purchaseReportData.size()-1).set(2, String.valueOf(Integer.parseInt(value1)+Integer.parseInt(purchaseReportData1.get(0).get(0))));
                        purchaseReportData.get(purchaseReportData.size()-1).set(3, String.valueOf(Integer.parseInt(value2)+Integer.parseInt(purchaseReportData1.get(0).get(1))));
                    }
                }
                else
                {
                    newDataAdd = true;
                    temp.add(stDatePurchase);
                    temp.add(stDay);
                    temp.add(purchaseReportData1.get(0).get(0));
                    temp.add(purchaseReportData1.get(0).get(1));
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                }
                purchaseReportData1.remove(0);
            }
            else if(dateReturn != null && maxDate.equals(dateReturn) && purchaseReportData2.size() > 0)
            {
                if(purchaseReportData.size() > 0 && purchaseReportData.get(purchaseReportData.size()-1).get(0).equals(stDateReturn))
                {
                    newDataAdd = false;
                    if(purchaseReportData.get(purchaseReportData.size()-1).get(4).equals("-"))
                    {
                        purchaseReportData.get(purchaseReportData.size()-1).set(4, purchaseReportData2.get(0).get(1));
                        purchaseReportData.get(purchaseReportData.size()-1).set(5, purchaseReportData2.get(0).get(2));
                    }
                    else
                    {
                        String value1 = purchaseReportData.get(purchaseReportData.size()-1).get(4);
                        String value2 = purchaseReportData.get(purchaseReportData.size()-1).get(5);
                        purchaseReportData.get(purchaseReportData.size()-1).set(4, String.valueOf(Integer.parseInt(value1)+Integer.parseInt(purchaseReportData2.get(0).get(1))));
                        purchaseReportData.get(purchaseReportData.size()-1).set(5, String.valueOf(Integer.parseInt(value2)+Integer.parseInt(purchaseReportData2.get(0).get(2))));
                    }
                }
                else
                {
                    newDataAdd = true;
                    temp.add(stDateReturn);
                    temp.add(stDay);
                    temp.add("-");
                    temp.add("-");
                    temp.add(purchaseReportData2.get(0).get(1));
                    temp.add(purchaseReportData2.get(0).get(2));
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                }
                purchaseReportData2.remove(0);
            }
            else if (datePayment != null && maxDate.equals(datePayment) && purchaseReportData3.size() > 0)
            {
                if(purchaseReportData.size() > 0 && purchaseReportData.get(purchaseReportData.size()-1).get(0).equals(stDatePayment))
                {
                    newDataAdd = false;
                    if(purchaseReportData3.get(0).get(3).equals("Sent"))
                    {
                        if(purchaseReportData.get(purchaseReportData.size()-1).get(6).equals("-"))
                        {
                            purchaseReportData.get(purchaseReportData.size()-1).set(6, purchaseReportData3.get(0).get(2));
                        }
                        else
                        {
                            String value = purchaseReportData.get(purchaseReportData.size()-1).get(6);
                            purchaseReportData.get(purchaseReportData.size()-1).set(6, String.valueOf(Integer.parseInt(value)+Integer.parseInt(purchaseReportData3.get(0).get(2))));
                        }
                    }
                    else if (purchaseReportData3.get(0).get(3).equals("Received"))
                    {
                        if(purchaseReportData.get(purchaseReportData.size()-1).get(7).equals("-"))
                        {
                            purchaseReportData.get(purchaseReportData.size()-1).set(7, purchaseReportData3.get(0).get(2));
                        }
                        else
                        {
                            String value = purchaseReportData.get(purchaseReportData.size()-1).get(7);
                            purchaseReportData.get(purchaseReportData.size()-1).set(7, String.valueOf(Integer.parseInt(value)+Integer.parseInt(purchaseReportData3.get(0).get(2))));
                        }
                    }
                    else if (purchaseReportData3.get(0).get(3).equals("Waive Off"))
                    {
                        if(purchaseReportData.get(purchaseReportData.size()-1).get(8).equals("-"))
                        {
                            purchaseReportData.get(purchaseReportData.size()-1).set(8, purchaseReportData3.get(0).get(2));
                        }
                        else
                        {
                            String value = purchaseReportData.get(purchaseReportData.size()-1).get(8);
                            purchaseReportData.get(purchaseReportData.size()-1).set(8, String.valueOf(Integer.parseInt(value)+Integer.parseInt(purchaseReportData3.get(0).get(2))));
                        }
                    }
                }
                else
                {
                    newDataAdd = true;
                    temp.add(stDatePayment);
                    temp.add(stDay);
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    if(purchaseReportData3.get(0).get(3).equals("Sent"))
                    {
                        temp.add(purchaseReportData3.get(0).get(2));
                        temp.add("-");
                        temp.add("-");
                    }
                    else if (purchaseReportData3.get(0).get(3).equals("Received"))
                    {
                        temp.add("-");
                        temp.add(purchaseReportData3.get(0).get(2));
                        temp.add("-");
                    }
                    else if (purchaseReportData3.get(0).get(3).equals("Waive Off"))
                    {
                        temp.add("-");
                        temp.add("-");
                        temp.add(purchaseReportData3.get(0).get(2));
                    }
                }
                purchaseReportData3.remove(0);
            }
            if(newDataAdd)
            {
                purchaseReportData.add(temp);
            }
            else
            {
                newDataAdd = true;
            }
        }
        return purchaseReportData;
    }

    public ArrayList<ArrayList<String>> getDealerReportDetailSearch(Statement stmt1, Statement stmt2, Statement stmt3, Connection con, String dealerId, String fromDate, String toDate, String day) throws ParseException {
        ArrayList<ArrayList<String>> purchaseReportData1 = new ArrayList<ArrayList<String>>();
        ArrayList<ArrayList<String>> purchaseReportData2 = new ArrayList<ArrayList<String>>();
        ArrayList<ArrayList<String>> purchaseReportData3 = new ArrayList<ArrayList<String>>();
        ArrayList<ArrayList<String>> purchaseReportData4 = new ArrayList<ArrayList<String>>();
        ArrayList<ArrayList<String>> purchaseReportData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs1 = null;
        ResultSet rs2 = null;
        ResultSet rs3 = null;
        String searchQuery1;
        String searchQuery2;
        String searchQuery3;
        int multipleSearch = 0;
        searchQuery1 = "SELECT `order_id`, `booking_date`, `delivered_date`, `status` FROM `order_info` WHERE ";
        searchQuery2 = "SELECT `order_return`.`entry_date`, SUM(`order_return_detail`.`prod_quant`) as 'return_quantity', SUM(`order_return_detail`.`total_amount`) as 'return_amount' FROM `order_return` INNER JOIN `order_return_detail` ON `order_return_detail`.`return_id` = `order_return`.`return_id` WHERE ";
        searchQuery3 = "SELECT `dealer_payments`.`payment_id`, `dealer_payments`.`date`, `dealer_payments`.`amount`, `dealer_payments`.`cash_type` FROM `dealer_payments` WHERE ";
        if(!fromDate.equals(""))
        {
            searchQuery1 += "str_to_date(`booking_date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y')";
            searchQuery2 += "str_to_date(`order_return`.`entry_date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y')";
            searchQuery3 += "str_to_date(`dealer_payments`.`date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y')";
            multipleSearch++;
        }
        if(!toDate.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery1 += " AND ";
                searchQuery2 += " AND ";
                searchQuery3 += " AND ";
            }
            searchQuery1 += "str_to_date(`booking_date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y')";
            searchQuery2 += "str_to_date(`order_return`.`entry_date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y')";
            searchQuery3 += "str_to_date(`dealer_payments`.`date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y')";
            multipleSearch++;
        }
        if(multipleSearch >= 1)
        {
            searchQuery1 += " AND ";
        }
        searchQuery1 += "`dealer_id` = '"+dealerId+"' ORDER BY `order_id` DESC";
        try {
            rs1 = stmt1.executeQuery(searchQuery1);
            while (rs1.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs1.getString(1)); // Order Id
                temp.add(rs1.getString(2)); // Booking Date
                purchaseReportData1.add(temp);
                temp = new ArrayList<String>();
                temp.add(rs1.getString(1)); // Order Id
                temp.add(rs1.getString(3)); // Delivery Date
                temp.add(rs1.getString(4)); // Status
                purchaseReportData2.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        searchQuery2 += "`order_return`.`dealer_id` = '"+dealerId+"' GROUP BY `order_return`.`entry_date` ORDER BY str_to_date(`order_return`.`entry_date`, '%d/%b/%Y') DESC";
        try {
            rs2 = stmt2.executeQuery(searchQuery2);
            while (rs2.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs2.getString(1)); // Return Date
                temp.add(rs2.getString(2)); // Return Quantity
                temp.add(rs2.getString(3)); // Return Price
                purchaseReportData3.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            searchQuery3 += "`dealer_payments`.`dealer_id` = '"+dealerId+"' AND `dealer_payments`.`status` != 'Deleted' ORDER BY str_to_date(`date`, '%d/%b/%Y') DESC";
            rs3 = stmt3.executeQuery(searchQuery3);
            while (rs3.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs3.getString(1)); // Payment Id
                temp.add(rs3.getString(2)); // Payment Date
                temp.add(rs3.getString(3)); // Amount
                temp.add(rs3.getString(4)); // Payment Type
                purchaseReportData4.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        String stDatePurchase = "";
        String stDateDelivered = "";
        String stDateReturn = "";
        String stDatePayment = "";
        Date datePurchase = null;
        Date dateDelivered = null;
        Date dateReturn = null;
        Date datePayment = null;
        ArrayList<Date> objDatesArray;
        int intDay;
        String stDay = "";
        boolean newDataAdd = true;

        while (purchaseReportData1.size() > 0 || purchaseReportData2.size() > 0 || purchaseReportData3.size() > 0 || purchaseReportData4.size() > 0)
        {
            temp = new ArrayList<>();
            objDatesArray = new ArrayList<>();
            if(purchaseReportData1.size() > 0)
            {
                stDatePurchase = purchaseReportData1.get(0).get(1);
                datePurchase = fmt.parse(stDatePurchase);
                objDatesArray.add(datePurchase);
            }
            if(purchaseReportData2.size() > 0)
            {
                stDateDelivered = purchaseReportData2.get(0).get(1);
                if(stDateDelivered != null && !stDateDelivered.equals(""))
                {
                    dateDelivered = fmt.parse(stDateDelivered);
                    objDatesArray.add(dateDelivered);
                }
                else
                {
                    purchaseReportData2.remove(0);
                }
            }
            if(purchaseReportData3.size() > 0)
            {
                stDateReturn = purchaseReportData3.get(0).get(0);
                dateReturn = fmt.parse(stDateReturn);
                objDatesArray.add(dateReturn);
            }
            if(purchaseReportData4.size() > 0)
            {
                stDatePayment = purchaseReportData4.get(0).get(1);
                datePayment = fmt.parse(stDatePayment);
                objDatesArray.add(datePayment);
            }
            Date maxDate = Collections.max(objDatesArray);
            intDay = maxDate.getDay();
            if(intDay == 0)
                stDay = "Sunday";
            else if(intDay == 1)
                stDay = "Monday";
            else if(intDay == 2)
                stDay = "Tuesday";
            else if(intDay == 3)
                stDay = "Wednesday";
            else if(intDay == 4)
                stDay = "Thursday";
            else if(intDay == 5)
                stDay = "Friday";
            else if(intDay == 6)
                stDay = "Saturday";
            if(datePurchase != null && maxDate.equals(datePurchase) && purchaseReportData1.size() > 0)
            {
                if(purchaseReportData.size() > 0 && purchaseReportData.get(purchaseReportData.size()-1).get(0).equals(stDatePurchase))
                {
                    newDataAdd = false;
                    if(purchaseReportData.get(purchaseReportData.size()-1).get(2).equals("-"))
                    {
                        purchaseReportData.get(purchaseReportData.size()-1).set(2, purchaseReportData1.get(0).get(0));
                    }
                    else
                    {
                        String value = purchaseReportData.get(purchaseReportData.size()-1).get(2);
                        purchaseReportData.get(purchaseReportData.size()-1).set(2, value+"\n"+purchaseReportData1.get(0).get(0));
                    }
                }
                else
                {
                    if(!day.equals("All"))
                    {
                        if(day.equals(stDay))
                        {
                            newDataAdd = true;
                            temp.add(stDatePurchase);
                            temp.add(stDay);
                            temp.add(purchaseReportData1.get(0).get(0));
                            temp.add("-");
                            temp.add("-");
                            temp.add("-");
                            temp.add("-");
                            temp.add("-");
                            temp.add("-");
                            temp.add("-");
                        }
                    }
                    else
                    {
                        newDataAdd = true;
                        temp.add(stDatePurchase);
                        temp.add(stDay);
                        temp.add(purchaseReportData1.get(0).get(0));
                        temp.add("-");
                        temp.add("-");
                        temp.add("-");
                        temp.add("-");
                        temp.add("-");
                        temp.add("-");
                        temp.add("-");
                    }

                }
                purchaseReportData1.remove(0);
            }
            else if(dateDelivered != null && maxDate.equals(dateDelivered) && purchaseReportData2.size() > 0)
            {
                if(purchaseReportData.size() > 0 && purchaseReportData.get(purchaseReportData.size()-1).get(0).equals(stDateDelivered))
                {
                    newDataAdd = false;
                    if(purchaseReportData2.get(0).get(2).equals("Delivered"))
                    {
                        if(purchaseReportData.get(purchaseReportData.size()-1).get(3).equals("-"))
                        {
                            purchaseReportData.get(purchaseReportData.size()-1).set(3, purchaseReportData2.get(0).get(0));
                        }
                        else
                        {
                            String value = purchaseReportData.get(purchaseReportData.size()-1).get(3);
                            purchaseReportData.get(purchaseReportData.size()-1).set(3, value+"\n"+purchaseReportData2.get(0).get(0));
                        }
                    }
                    else if(purchaseReportData2.get(0).get(2).equals("Returned"))
                    {
                        if(purchaseReportData.get(purchaseReportData.size()-1).get(4).equals("-"))
                        {
                            purchaseReportData.get(purchaseReportData.size()-1).set(4, purchaseReportData2.get(0).get(0));
                        }
                        else
                        {
                            String value = purchaseReportData.get(purchaseReportData.size()-1).get(4);
                            purchaseReportData.get(purchaseReportData.size()-1).set(4, value+"\n"+purchaseReportData2.get(0).get(0));
                        }
                    }
                }
                else
                {
                    if(!day.equals("All"))
                    {
                        if(day.equals(stDay))
                        {
                            newDataAdd = true;
                            temp.add(stDateDelivered);
                            temp.add(stDay);
                            temp.add("-");
                            if(purchaseReportData2.get(0).get(2).equals("Delivered"))
                            {
                                temp.add(purchaseReportData2.get(0).get(0));
                                temp.add("-");
                            }
                            else if(purchaseReportData2.get(0).get(2).equals("Returned"))
                            {
                                temp.add("-");
                                temp.add(purchaseReportData2.get(0).get(0));
                            }
                            temp.add("-");
                            temp.add("-");
                            temp.add("-");
                            temp.add("-");
                            temp.add("-");
                        }
                    }
                    else
                    {
                        newDataAdd = true;
                        temp.add(stDateDelivered);
                        temp.add(stDay);
                        temp.add("-");
                        if(purchaseReportData2.get(0).get(2).equals("Delivered"))
                        {
                            temp.add(purchaseReportData2.get(0).get(0));
                            temp.add("-");
                        }
                        else if(purchaseReportData2.get(0).get(2).equals("Returned"))
                        {
                            temp.add("-");
                            temp.add(purchaseReportData2.get(0).get(0));
                        }
                        temp.add("-");
                        temp.add("-");
                        temp.add("-");
                        temp.add("-");
                        temp.add("-");
                    }

                }

                purchaseReportData2.remove(0);
            }
            else if(dateReturn != null && maxDate.equals(dateReturn) && purchaseReportData3.size() > 0)
            {
                if(purchaseReportData.size() > 0 && purchaseReportData.get(purchaseReportData.size()-1).get(0).equals(stDateReturn))
                {
                    newDataAdd = false;
                    if(purchaseReportData.get(purchaseReportData.size()-1).get(5).equals("-"))
                    {
                        purchaseReportData.get(purchaseReportData.size()-1).set(5, purchaseReportData3.get(0).get(1));
                        purchaseReportData.get(purchaseReportData.size()-1).set(6, purchaseReportData3.get(0).get(2));
                    }
                    else
                    {
                        String value1 = purchaseReportData.get(purchaseReportData.size()-1).get(5);
                        String value2 = purchaseReportData.get(purchaseReportData.size()-1).get(6);
                        purchaseReportData.get(purchaseReportData.size()-1).set(5, String.valueOf(Integer.parseInt(value1)+Integer.parseInt(purchaseReportData3.get(0).get(1))));
                        purchaseReportData.get(purchaseReportData.size()-1).set(6, String.valueOf(Integer.parseInt(value2)+Integer.parseInt(purchaseReportData3.get(0).get(2))));
                    }
                }
                else
                {
                    if(!day.equals("All"))
                    {
                        if(day.equals(stDay))
                        {
                            newDataAdd = true;
                            temp.add(stDateReturn);
                            temp.add(stDay);
                            temp.add("-");
                            temp.add("-");
                            temp.add("-");
                            temp.add(purchaseReportData3.get(0).get(1));
                            temp.add(purchaseReportData3.get(0).get(2));
                            temp.add("-");
                            temp.add("-");
                            temp.add("-");
                        }
                    }
                    else
                    {
                        newDataAdd = true;
                        temp.add(stDateReturn);
                        temp.add(stDay);
                        temp.add("-");
                        temp.add("-");
                        temp.add("-");
                        temp.add(purchaseReportData3.get(0).get(1));
                        temp.add(purchaseReportData3.get(0).get(2));
                        temp.add("-");
                        temp.add("-");
                        temp.add("-");
                    }
                }
                purchaseReportData3.remove(0);
            }
            else if (datePayment != null && maxDate.equals(datePayment) && purchaseReportData4.size() > 0)
            {
                if(purchaseReportData.size() > 0 && purchaseReportData.get(purchaseReportData.size()-1).get(0).equals(stDatePayment))
                {
                    newDataAdd = false;
                    if(purchaseReportData4.get(0).get(3).equals("Collection"))
                    {
                        if(purchaseReportData.get(purchaseReportData.size()-1).get(7).equals("-"))
                        {
                            purchaseReportData.get(purchaseReportData.size()-1).set(7, purchaseReportData4.get(0).get(2));
                        }
                        else
                        {
                            String value = purchaseReportData.get(purchaseReportData.size()-1).get(7);
                            purchaseReportData.get(purchaseReportData.size()-1).set(7, String.valueOf(Integer.parseInt(value)+Integer.parseInt(purchaseReportData4.get(0).get(2))));
                        }
                    }
                    else if (purchaseReportData4.get(0).get(3).equals("Received"))
                    {
                        if(purchaseReportData.get(purchaseReportData.size()-1).get(8).equals("-"))
                        {
                            purchaseReportData.get(purchaseReportData.size()-1).set(8, purchaseReportData4.get(0).get(2));
                        }
                        else
                        {
                            String value = purchaseReportData.get(purchaseReportData.size()-1).get(8);
                            purchaseReportData.get(purchaseReportData.size()-1).set(8, String.valueOf(Integer.parseInt(value)+Integer.parseInt(purchaseReportData4.get(0).get(2))));
                        }
                    }
                    else if (purchaseReportData4.get(0).get(3).equals("Waive Off"))
                    {
                        if(purchaseReportData.get(purchaseReportData.size()-1).get(9).equals("-"))
                        {
                            purchaseReportData.get(purchaseReportData.size()-1).set(9, purchaseReportData4.get(0).get(2));
                        }
                        else
                        {
                            String value = purchaseReportData.get(purchaseReportData.size()-1).get(9);
                            purchaseReportData.get(purchaseReportData.size()-1).set(9, String.valueOf(Integer.parseInt(value)+Integer.parseInt(purchaseReportData4.get(0).get(2))));
                        }
                    }
                }
                else
                {
                    if(!day.equals("All"))
                    {
                        if(day.equals(stDay))
                        {
                            newDataAdd = true;
                            temp.add(stDatePayment);
                            temp.add(stDay);
                            temp.add("-");
                            temp.add("-");
                            temp.add("-");
                            temp.add("-");
                            temp.add("-");
                            if(purchaseReportData4.get(0).get(3).equals("Collection"))
                            {
                                temp.add(purchaseReportData4.get(0).get(2));
                                temp.add("-");
                                temp.add("-");
                            }
                            else if (purchaseReportData4.get(0).get(3).equals("Received"))
                            {
                                temp.add("-");
                                temp.add(purchaseReportData4.get(0).get(2));
                                temp.add("-");
                            }
                            else if (purchaseReportData4.get(0).get(3).equals("Waive Off"))
                            {
                                temp.add("-");
                                temp.add("-");
                                temp.add(purchaseReportData4.get(0).get(2));
                            }
                        }
                    }
                    else
                    {
                        newDataAdd = true;
                        temp.add(stDatePayment);
                        temp.add(stDay);
                        temp.add("-");
                        temp.add("-");
                        temp.add("-");
                        temp.add("-");
                        temp.add("-");
                        if(purchaseReportData4.get(0).get(3).equals("Collection"))
                        {
                            temp.add(purchaseReportData4.get(0).get(2));
                            temp.add("-");
                            temp.add("-");
                        }
                        else if (purchaseReportData4.get(0).get(3).equals("Received"))
                        {
                            temp.add("-");
                            temp.add(purchaseReportData4.get(0).get(2));
                            temp.add("-");
                        }
                        else if (purchaseReportData4.get(0).get(3).equals("Waive Off"))
                        {
                            temp.add("-");
                            temp.add("-");
                            temp.add(purchaseReportData4.get(0).get(2));
                        }
                    }

                }
                purchaseReportData4.remove(0);
            }
            if(newDataAdd && stDay.equals(day))
            {
                purchaseReportData.add(temp);
            }
            else
            {
                newDataAdd = true;
            }
        }

        return purchaseReportData;
    }

    public ArrayList<ArrayList<String>> getDealerFinanceDetailSearch(Statement stmt, Connection con, String dealerId, String fromDate, String toDate, String day, String fromAmount, String toAmount, String type)
    {
        ArrayList<ArrayList<String>> dealersFinanceData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        String searchQuery;
        int multipleSearch = 0;
        searchQuery = "SELECT `dealer_payments`.`payment_id`, `dealer_payments`.`dealer_id`, `dealer_info`.`dealer_name`, `dealer_payments`.`date`, `dealer_payments`.`day`, `dealer_payments`.`amount`, `dealer_payments`.`cash_type` FROM `dealer_payments` INNER JOIN `dealer_info` ON `dealer_info`.`dealer_table_id` = `dealer_payments`.`dealer_id` WHERE ";
        if(!fromDate.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "str_to_date(`date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y')";
            multipleSearch++;
        }
        if(!toDate.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "str_to_date(`date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y')";
            multipleSearch++;
        }
        if(!day.equals("All"))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`dealer_payments`.`day` = '"+day+"'";
            multipleSearch++;
        }
        if(!fromAmount.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`dealer_payments`.`amount` >= '"+fromAmount+"'";
            multipleSearch++;
        }
        if(!toAmount.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`dealer_payments`.`amount` <= '"+toAmount+"'";
            multipleSearch++;
        }
        if(!type.equals("All"))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`dealer_payments`.`cash_type` = '"+type+"'";
            multipleSearch++;
        }
        if(multipleSearch >= 1)
        {
            searchQuery += " AND ";
        }
        searchQuery += "`dealer_payments`.`dealer_id` = '"+dealerId+"' AND `dealer_payments`.`status` != 'Deleted' ORDER BY str_to_date(`date`, '%d/%b/%Y') DESC";
        try {
            rs = stmt.executeQuery(searchQuery);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Payment Id
                temp.add(rs.getString(2)); // Dealer Id
                temp.add(rs.getString(3)); // Dealer Name
                temp.add(rs.getString(4)); // Date
                temp.add(rs.getString(5)); // Day
                temp.add(rs.getString(6)); // Cash
                temp.add(rs.getString(7)); // Cash Type
                dealersFinanceData.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dealersFinanceData;
    }

    public void updateDealerFinance(Statement stmt, Connection con, String paymentId, String dealerId, String preCashAmount, String preCashType, String date, String day, String amount, String cashType)
    {
        String currentDate = GlobalVariables.getStDate();
        String currentTime = GlobalVariables.getStTime();
        String currentUser = GlobalVariables.getUserId();
        String updateQuery = null;
        try {
            if(preCashType.equals("Collection"))
            {
                updateQuery = "UPDATE `dealer_overall_record` SET `cash_collected`= `cash_collected` - '"+preCashAmount+"', `pending_payments` = `pending_payments` + '"+preCashAmount+"' WHERE `dealer_id` = '"+dealerId+"'";
            }
            else if(preCashType.equals("Received"))
            {
                updateQuery = "UPDATE `dealer_overall_record` SET `cash_return`= `cash_return` - '"+preCashAmount+"', `pending_payments` = `pending_payments` - '"+preCashAmount+"' WHERE `dealer_id` = '"+dealerId+"'";
            }
            else if(preCashType.equals("Waive Off"))
            {
                updateQuery = "UPDATE `dealer_overall_record` SET `waived_off_price`= `waived_off_price` - '"+preCashAmount+"', `pending_payments` = `pending_payments` + '"+preCashAmount+"' WHERE `dealer_id` = '"+dealerId+"'";
            }
            stmt.executeUpdate(updateQuery);

            updateQuery = "UPDATE `dealer_payments` SET `date`='"+date+"', `day`='"+day+"', `amount`='"+amount+"',`cash_type`='"+cashType+"', `user_id`='"+currentUser+"' WHERE `payment_id` = '"+paymentId+"'";
            stmt.executeUpdate(updateQuery);

            if(cashType.equals("Collection"))
            {
                updateQuery = "UPDATE `dealer_overall_record` SET `cash_collected`= `cash_collected` + '"+amount+"', `pending_payments` = `pending_payments` - '"+amount+"' WHERE `dealer_id` = '"+dealerId+"'";
            }
            else if(cashType.equals("Received"))
            {
                updateQuery = "UPDATE `dealer_overall_record` SET `cash_return`= `cash_return` + '"+amount+"', `pending_payments` = `pending_payments` + '"+amount+"' WHERE `dealer_id` = '"+dealerId+"'";
            }
            else if(cashType.equals("Waive Off"))
            {
                updateQuery = "UPDATE `dealer_overall_record` SET `waived_off_price`= `waived_off_price` + '"+amount+"', `pending_payments` = `pending_payments` - '"+amount+"' WHERE `dealer_id` = '"+dealerId+"'";
            }
            stmt.executeUpdate(updateQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private Node createEditIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.EDIT);
        graphic.setFontSize(20.0);
        return graphic;
    }

    private Node createDeleteIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.TRASH);
        graphic.setFontSize(20.0);
        return graphic;
    }


}
