package model;

import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.HBox;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class CompanyGeneralReportInfo {
    private String srNo;
    private String companyTableId;
    private String companyId;
    private String companyName;
    private String ordersGiven;
    private String ordersReceived;
    private String orderedItems;
    private String receivedItems;
    private String totalPurchase;
    private String discountGiven;
    private String returnedItems;
    private String returnPrice;
    private String cashSent;
    private String cashReturn;
    private String cashWaiveOff;
    private String cashPending;
    private String companyStatus;

    private HBox operationsPane;
    private JFXButton btnView;
    private JFXButton btnEdit;

    public static String companyReportId;

    public CompanyGeneralReportInfo() {
    }

    public CompanyGeneralReportInfo(String srNo, String companyTableId, String companyId, String companyName, String ordersGiven, String ordersReceived, String orderedItems, String receivedItems, String totalPurchase, String discountGiven, String returnedItems, String returnPrice, String cashSent, String cashReturn, String cashWaiveOff, String cashPending, String companyStatus) {
        this.srNo = srNo;
        this.companyTableId = companyTableId;
        this.companyId = companyId;
        this.companyName = companyName;
        this.ordersGiven = String.format("%,.0f", Float.parseFloat(ordersGiven));
        this.ordersReceived = String.format("%,.0f", Float.parseFloat(ordersReceived));
        this.orderedItems = String.format("%,.0f", Float.parseFloat(orderedItems));
        this.receivedItems = String.format("%,.0f", Float.parseFloat(receivedItems));
        this.totalPurchase = String.format("%,.0f", Float.parseFloat(totalPurchase));
        this.discountGiven = String.format("%,.0f", Float.parseFloat(discountGiven));
        this.returnedItems = String.format("%,.0f", Float.parseFloat(returnedItems));
        this.returnPrice = String.format("%,.0f", Float.parseFloat(returnPrice));
        this.cashSent = String.format("%,.0f", Float.parseFloat(cashSent));
        this.cashReturn = String.format("%,.0f", Float.parseFloat(cashReturn));
        this.cashWaiveOff = String.format("%,.0f", Float.parseFloat(cashWaiveOff));
        this.cashPending = String.format("%,.0f", Float.parseFloat(cashPending));
        this.companyStatus = companyStatus;

        this.btnView = new JFXButton();
        this.btnView.setGraphic(createViewIcon());
        this.btnView.getStyleClass().add("btn_icon");
        this.btnEdit = new JFXButton();
        this.btnEdit.setGraphic(createEditIcon());
        this.btnEdit.getStyleClass().add("btn_icon");
        this.operationsPane = new HBox(btnView, btnEdit);

        this.btnView.setOnAction((event)->viewCompanyDetail());
    }

    public void viewCompanyDetail()
    {
        Parent parent = null;
        try {
            companyReportId = companyTableId;
            parent = FXMLLoader.load(getClass().getResource("../view/company_report_detail.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(parent);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getCompanyTableId() {
        return companyTableId;
    }

    public void setCompanyTableId(String companyTableId) {
        this.companyTableId = companyTableId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getOrdersGiven() {
        return ordersGiven;
    }

    public void setOrdersGiven(String ordersGiven) {
        this.ordersGiven = ordersGiven;
    }

    public String getOrdersReceived() {
        return ordersReceived;
    }

    public void setOrdersReceived(String ordersReceived) {
        this.ordersReceived = ordersReceived;
    }

    public String getOrderedItems() {
        return orderedItems;
    }

    public void setOrderedItems(String orderedItems) {
        this.orderedItems = orderedItems;
    }

    public String getReceivedItems() {
        return receivedItems;
    }

    public void setReceivedItems(String receivedItems) {
        this.receivedItems = receivedItems;
    }

    public String getReturnedItems() {
        return returnedItems;
    }

    public void setReturnedItems(String returnedItems) {
        this.returnedItems = returnedItems;
    }

    public String getReturnPrice() {
        return returnPrice;
    }

    public void setReturnPrice(String returnPrice) {
        this.returnPrice = returnPrice;
    }

    public String getTotalPurchase() {
        return totalPurchase;
    }

    public void setTotalPurchase(String totalPurchase) {
        this.totalPurchase = totalPurchase;
    }

    public String getDiscountGiven() {
        return discountGiven;
    }

    public void setDiscountGiven(String discountGiven) {
        this.discountGiven = discountGiven;
    }

    public String getCashSent() {
        return cashSent;
    }

    public void setCashSent(String cashSent) {
        this.cashSent = cashSent;
    }

    public String getCashReturn() {
        return cashReturn;
    }

    public void setCashReturn(String cashReturn) {
        this.cashReturn = cashReturn;
    }

    public String getCashWaiveOff() {
        return cashWaiveOff;
    }

    public void setCashWaiveOff(String cashWaiveOff) {
        this.cashWaiveOff = cashWaiveOff;
    }

    public String getCashPending() {
        return cashPending;
    }

    public void setCashPending(String cashPending) {
        this.cashPending = cashPending;
    }

    public String getCompanyStatus() {
        return companyStatus;
    }

    public void setCompanyStatus(String companyStatus) {
        this.companyStatus = companyStatus;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnView() {
        return btnView;
    }

    public void setBtnView(JFXButton btnView) {
        this.btnView = btnView;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public ArrayList<ArrayList<String>> getCompanyReportInfo(Statement stmt1, Statement stmt2, Statement stmt3, Connection con, String fromDate, String toDate)
    {
        ArrayList<ArrayList<String>> companyReportData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs1 = null;
        ResultSet rs2 = null;
        ResultSet rs3 = null;
        try {
            rs1 = stmt1.executeQuery("SELECT `company_info`.`company_table_id`, `company_info`.`company_id`, `company_info`.`company_name`, SUM(case when `purchase_info`.`comp_id` = `company_info`.`company_table_id` AND `purchase_info`.`status` != 'Deleted' then 1 else 0 end) as 'orders_purchase', (SELECT COALESCE(SUM(`purchase_info_detail`.`recieve_quant`),0) FROM `purchase_info_detail` LEFT OUTER JOIN `purchase_info` ON `purchase_info`.`purchase_id` = `purchase_info_detail`.`purchase_id` AND `purchase_info`.`status` != 'Deleted' AND str_to_date(`purchase_info`.`purchase_date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') AND str_to_date(`purchase_info`.`purchase_date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y') WHERE `purchase_info`.`comp_id` = `company_info`.`company_table_id`) as 'items_purchased', SUM(case when `purchase_info`.`comp_id` = `company_info`.`company_table_id` AND `purchase_info`.`status` != 'Deleted' then `purchase_info`.`net_amount` else 0 end) as 'purchase_price', SUM(case when `purchase_info`.`comp_id` = `company_info`.`company_table_id` AND `purchase_info`.`status` != 'Deleted' then `purchase_info`.`disc_amount` else 0 end) as 'discount_given' FROM `company_info` LEFT OUTER JOIN `purchase_info` ON `purchase_info`.`comp_id` = `company_info`.`company_table_id` AND str_to_date(`purchase_info`.`purchase_date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') AND str_to_date(`purchase_info`.`purchase_date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y') WHERE `company_info`.`company_status` != 'Deleted' GROUP BY `company_info`.`company_table_id`");
            rs2 = stmt2.executeQuery("SELECT `company_info`.`company_table_id`, SUM(`purchase_return_detail`.`prod_quant`) as 'items_returned', ROUND((SELECT COALESCE(SUM(`purchase_return`.`return_total_price`),0) FROM `purchase_return` WHERE `purchase_return`.`company_id` = `company_info`.`company_table_id` AND `purchase_return`.`status` != 'Deleted' AND str_to_date(`purchase_return`.`entry_date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') AND str_to_date(`purchase_return`.`entry_date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y')), 2) as 'return_price' FROM `company_info` LEFT OUTER JOIN `purchase_return` ON `purchase_return`.`company_id` = `company_info`.`company_table_id` AND `purchase_return`.`status` != 'Deleted' LEFT OUTER JOIN `purchase_return_detail` ON `purchase_return_detail`.`return_id` = `purchase_return`.`return_id` AND `purchase_return`.`status` != 'Deleted' AND str_to_date(`purchase_return`.`entry_date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') AND str_to_date(`purchase_return`.`entry_date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y') WHERE `company_info`.`company_status` != 'Deleted' GROUP BY `company_info`.`company_table_id`");
            rs3 = stmt3.executeQuery("SELECT `company_info`.`company_table_id`, ROUND((SELECT COALESCE(SUM(`amount`),0) FROM `company_payments` WHERE `cash_type` = 'Sent' AND `company_payments`.`status` != 'Deleted' AND `company_payments`.`company_id` = `company_info`.`company_table_id` AND str_to_date(`company_payments`.`date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') AND str_to_date(`company_payments`.`date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y')), 2) as 'cash_sent', ROUND((SELECT COALESCE(SUM(`amount`),0) FROM `company_payments` WHERE `cash_type` = 'Return' AND `company_payments`.`status` != 'Deleted' AND `company_payments`.`company_id` = `company_info`.`company_table_id` AND str_to_date(`company_payments`.`date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') AND str_to_date(`company_payments`.`date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y')), 2) as 'cash_return', ROUND((SELECT COALESCE(SUM(`amount`),0) FROM `company_payments` WHERE `cash_type` = 'Waived Off' AND `company_payments`.`status` != 'Deleted' AND `company_payments`.`company_id` = `company_info`.`company_table_id` AND str_to_date(`company_payments`.`date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') AND str_to_date(`company_payments`.`date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y')), 2) as 'cash_waivedoff', (SELECT `pending_payments` FROM `company_payments` WHERE `company_payments`.`status` != 'Deleted' AND `company_payments`.`company_id` = `company_info`.`company_table_id` AND str_to_date(`company_payments`.`date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') AND str_to_date(`company_payments`.`date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y') ORDER BY `payment_id` DESC LIMIT 1) as 'pending_payments', `company_info`.`company_status` FROM `company_info` WHERE `company_info`.`company_status` != 'Deleted' GROUP BY `company_info`.`company_table_id`");
            while (rs1.next() && rs2.next() && rs3.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs1.getString(1)); // Table Id
                temp.add(rs1.getString(2)); // Id
                temp.add(rs1.getString(3)); // Name
                temp.add(rs1.getString(4)); // Orders Given
                temp.add(rs1.getString(4)); // Orders Received
                temp.add(rs1.getString(5)); // Ordered Items
                temp.add(rs1.getString(5)); // Received Items
                temp.add(rs1.getString(6)); // Total Purchase
                temp.add(rs1.getString(7)); // Discount Given
                temp.add(rs2.getString(2)); // Returned
                temp.add(rs2.getString(3)); // Return Price
                temp.add(rs3.getString(2)); // Cash Sent
                temp.add(rs3.getString(3)); // Cash Returned
                temp.add(rs3.getString(4)); // Cash Waived Off
                temp.add(rs3.getString(5)); // Pending Payments
                temp.add(rs3.getString(6)); // Company Status
                companyReportData.add(temp);
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return companyReportData;
    }

    public ArrayList<ArrayList<String>> getCompanyReportSearch(Statement stmt1, Statement stmt2, Statement stmt3, Connection con, String companyId, String companyName, String fromDate, String toDate)
    {
        ArrayList<ArrayList<String>> companyReportData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs1 = null;
        ResultSet rs2 = null;
        ResultSet rs3 = null;
        String searchQuery1;
        String searchQuery2;
        String searchQuery3;
        int multipleSearch = 0;
        searchQuery1 = "SELECT `company_info`.`company_table_id`, `company_info`.`company_id`, `company_info`.`company_name`, SUM(case when `purchase_info`.`comp_id` = `company_info`.`company_table_id` AND `purchase_info`.`status` != 'Deleted' then 1 else 0 end) as 'orders_purchase', (SELECT COALESCE(SUM(`purchase_info_detail`.`recieve_quant`),0) FROM `purchase_info_detail` LEFT OUTER JOIN `purchase_info` ON `purchase_info`.`purchase_id` = `purchase_info_detail`.`purchase_id` AND `purchase_info`.`status` != 'Deleted' ";
        searchQuery2 = "SELECT `company_info`.`company_table_id`, SUM(`purchase_return_detail`.`prod_quant`) as 'items_returned', ROUND((SELECT COALESCE(SUM(`purchase_return`.`return_total_price`),0) FROM `purchase_return` WHERE `purchase_return`.`company_id` = `company_info`.`company_table_id` AND `purchase_return`.`status` != 'Deleted' ";
        searchQuery3 = "SELECT `company_info`.`company_table_id`, ROUND((SELECT COALESCE(SUM(`amount`),0) FROM `company_payments` WHERE `cash_type` = 'Sent' AND `company_payments`.`status` != 'Deleted' AND `company_payments`.`company_id` = `company_info`.`company_table_id` ";
        if(!fromDate.equals(""))
        {
            searchQuery1 += "AND str_to_date(`purchase_info`.`purchase_date`, '%d/%b/%Y') >= str_to_date('01/Jan/2021', '%d/%b/%Y') ";
            searchQuery2 += "AND str_to_date(`purchase_return`.`entry_date`, '%d/%b/%Y') >= str_to_date('01/Jan/2021', '%d/%b/%Y') ";
            searchQuery3 += "AND str_to_date(`company_payments`.`date`, '%d/%b/%Y') >= str_to_date('01/Jan/2021', '%d/%b/%Y') ";
        }
        if(!toDate.equals(""))
        {
            searchQuery1 += "AND str_to_date(`purchase_info`.`purchase_date`, '%d/%b/%Y') <= str_to_date('30/Jun/2021', '%d/%b/%Y') ";
            searchQuery2 += "AND str_to_date(`purchase_return`.`entry_date`, '%d/%b/%Y') <= str_to_date('30/Jun/2021', '%d/%b/%Y') ";
            searchQuery3 += "AND str_to_date(`company_payments`.`date`, '%d/%b/%Y') <= str_to_date('30/Jun/2021', '%d/%b/%Y') ";
        }
        searchQuery1 += "WHERE `purchase_info`.`comp_id` = `company_info`.`company_table_id`) as 'items_purchased', SUM(case when `purchase_info`.`comp_id` = `company_info`.`company_table_id` AND `purchase_info`.`status` != 'Deleted' then `purchase_info`.`net_amount` else 0 end) as 'purchase_price', SUM(case when `purchase_info`.`comp_id` = `company_info`.`company_table_id` AND `purchase_info`.`status` != 'Deleted' then `purchase_info`.`disc_amount` else 0 end) as 'discount_given' FROM `company_info` LEFT OUTER JOIN `purchase_info` ON `purchase_info`.`comp_id` = `company_info`.`company_table_id` ";
        searchQuery2 += "), 2) as 'return_price' FROM `company_info` LEFT OUTER JOIN `purchase_return` ON `purchase_return`.`company_id` = `company_info`.`company_table_id` AND `purchase_return`.`status` != 'Deleted' LEFT OUTER JOIN `purchase_return_detail` ON `purchase_return_detail`.`return_id` = `purchase_return`.`return_id` AND `purchase_return`.`status` != 'Deleted' ";
        searchQuery3 += "), 2) as 'cash_sent', ROUND((SELECT COALESCE(SUM(`amount`),0) FROM `company_payments` WHERE `cash_type` = 'Return' AND `company_payments`.`status` != 'Deleted' AND `company_payments`.`company_id` = `company_info`.`company_table_id` ";
        if(!fromDate.equals(""))
        {
            searchQuery1 += "AND str_to_date(`purchase_info`.`purchase_date`, '%d/%b/%Y') >= str_to_date('01/Jan/2021', '%d/%b/%Y') ";
            searchQuery2 += "AND str_to_date(`purchase_return`.`entry_date`, '%d/%b/%Y') >= str_to_date('01/Jan/2021', '%d/%b/%Y') ";
            searchQuery3 += "AND str_to_date(`company_payments`.`date`, '%d/%b/%Y') >= str_to_date('01/Jan/2021', '%d/%b/%Y') ";
        }
        if(!toDate.equals(""))
        {
            searchQuery1 += "AND str_to_date(`purchase_info`.`purchase_date`, '%d/%b/%Y') <= str_to_date('30/Jun/2021', '%d/%b/%Y') ";
            searchQuery2 += "AND str_to_date(`purchase_return`.`entry_date`, '%d/%b/%Y') <= str_to_date('30/Jun/2021', '%d/%b/%Y') ";
            searchQuery3 += "AND str_to_date(`company_payments`.`date`, '%d/%b/%Y') <= str_to_date('30/Jun/2021', '%d/%b/%Y') ";
        }
        searchQuery3 += "), 2) as 'cash_return', ROUND((SELECT COALESCE(SUM(`amount`),0) FROM `company_payments` WHERE `cash_type` = 'Waived Off' AND `company_payments`.`status` != 'Deleted' AND `company_payments`.`company_id` = `company_info`.`company_table_id` ";
        if(!fromDate.equals(""))
        {
            searchQuery3 += "AND str_to_date(`company_payments`.`date`, '%d/%b/%Y') >= str_to_date('01/Jan/2021', '%d/%b/%Y') ";
        }
        if(!toDate.equals(""))
        {
            searchQuery3 += "AND str_to_date(`company_payments`.`date`, '%d/%b/%Y') <= str_to_date('30/Jun/2021', '%d/%b/%Y')";
        }
        searchQuery3 += "), 2) as 'cash_waivedoff', (SELECT `pending_payments` FROM `company_payments` WHERE `company_payments`.`status` != 'Deleted' AND `company_payments`.`company_id` = `company_info`.`company_table_id` ";

        if(!fromDate.equals(""))
        {
            searchQuery3 += "AND str_to_date(`company_payments`.`date`, '%d/%b/%Y') >= str_to_date('01/Jan/2021', '%d/%b/%Y') ";
        }
        if(!toDate.equals(""))
        {
            searchQuery3 += "AND str_to_date(`company_payments`.`date`, '%d/%b/%Y') <= str_to_date('30/Jun/2021', '%d/%b/%Y') ";
        }
        searchQuery3 += "ORDER BY `payment_id` DESC LIMIT 1) as 'pending_payments', `company_info`.`company_status` FROM `company_info` WHERE ";

        searchQuery1 += "WHERE ";
        searchQuery2 += "WHERE ";
        if(!companyId.equals(""))
        {
            searchQuery1 += "`company_info`.`company_id` = '"+companyId+"' AND ";
            searchQuery2 += "`company_info`.`company_id` = '"+companyId+"' AND ";
            searchQuery3 += "`company_info`.`company_id` = '"+companyId+"' AND ";
        }
        if(!companyName.equals("All"))
        {
            searchQuery1 += "`company_info`.`company_name` = '"+companyName+"' AND ";
            searchQuery2 += "`company_info`.`company_name` = '"+companyName+"' AND ";
            searchQuery3 += "`company_info`.`company_name` = '"+companyName+"' AND ";
        }
        searchQuery1 += "`company_info`.`company_status` != 'Deleted' GROUP BY `company_info`.`company_table_id`";
        searchQuery2 += "`company_info`.`company_status` != 'Deleted' GROUP BY `company_info`.`company_table_id`";
        searchQuery3 += "`company_info`.`company_status` != 'Deleted' GROUP BY `company_info`.`company_table_id`";
        try {
            rs1 = stmt1.executeQuery(searchQuery1);
            rs2 = stmt2.executeQuery(searchQuery2);
            rs3 = stmt3.executeQuery(searchQuery3);
            if(!companyId.equals(""))
            {

            }
            while (rs1.next() && rs2.next() && rs3.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs1.getString(1)); // Table Id
                temp.add(rs1.getString(2)); // Id
                temp.add(rs1.getString(3)); // Name
                temp.add(rs1.getString(4)); // Orders Given
                temp.add(rs1.getString(4)); // Orders Received
                temp.add(rs1.getString(5)); // Ordered Items
                temp.add(rs1.getString(5)); // Received Items
                temp.add(rs1.getString(6)); // Total Purchase
                temp.add(rs1.getString(7)); // Discount Given
                temp.add(rs2.getString(2)); // Returned
                temp.add(rs2.getString(3)); // Return Price
                temp.add(rs3.getString(2)); // Cash Sent
                temp.add(rs3.getString(3)); // Cash Returned
                temp.add(rs3.getString(4)); // Cash Waived Off
                temp.add(rs3.getString(5)); // Pending Payments
                temp.add(rs3.getString(6)); // Company Status
                companyReportData.add(temp);
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return companyReportData;
    }

    private Node createEditIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.EDIT);
        graphic.setFontSize(20.0);
        return graphic;
    }

    private Node createViewIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.EYE);
        graphic.setFontSize(20.0);
        return graphic;
    }
}
