package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.JFXTextField;
import controller.DeleteWindow;
import controller.UpdateCompany;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import javax.xml.soap.Text;
import java.awt.*;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;

public class ViewCompanyInfo {
    private String srNo;
    private String companyTableId;
    private String companyId;
    private String companyName;
    private String companyAddress;
    private String companyContact;
    private String companyEmail;
    private String companyStatus;

    private HBox operationsPane;
    private JFXButton btnView;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    public static StackPane stackPane;
    public static JFXDialog dialog;
    public static Label lblUpdate;

    public static String viewCompanyId;

    GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");

    public ViewCompanyInfo()
    {
        this.srNo = "";
        this.companyTableId = "";
        this.companyId = "";
        this.companyName = "";
        this.companyAddress = "";
        this.companyContact = "";
        this.companyEmail = "";
        this.companyStatus = "";
        this.btnView = new JFXButton();
        this.btnEdit = new JFXButton();
        this.btnDelete = new JFXButton();
        this.btnView.setGraphic(createViewIcon());
        this.btnEdit.setGraphic(createEditIcon());
        this.btnDelete.setGraphic(createDeleteIcon());
        this.btnView.getStyleClass().add("btn_icon");
        this.btnEdit.getStyleClass().add("btn_icon");
        this.btnDelete.getStyleClass().add("btn_icon");
        this.operationsPane = new HBox(btnView, btnEdit, btnDelete);
        lblUpdate = new Label("");
    }

    public ViewCompanyInfo(String srNo, String companyTableId, String companyId, String companyName, String companyAddress, String companyContact, String companyEmail, String companyStatus) {
        this.srNo = srNo;
        this.companyTableId = companyTableId;
        this.companyId = companyId;
        this.companyName = companyName;
        this.companyAddress = companyAddress;
        this.companyContact = companyContact;
        this.companyEmail = companyEmail;
        this.companyStatus = companyStatus;

        this.btnView = new JFXButton();
        this.btnEdit = new JFXButton();
        this.btnDelete = new JFXButton();
        this.btnView.setGraphic(createViewIcon());
        this.btnEdit.setGraphic(createEditIcon());
        this.btnDelete.setGraphic(createDeleteIcon());
        this.btnView.getStyleClass().add("btn_icon");
        this.btnEdit.getStyleClass().add("btn_icon");
        this.btnDelete.getStyleClass().add("btn_icon");
        this.operationsPane = new HBox(btnView, btnEdit, btnDelete);
        lblUpdate = new Label("");

        this.btnView.setOnAction((action)->viewClicked());
        this.btnEdit.setOnAction((action)->editClicked());
        this.btnDelete.setOnAction(event -> {
            try {
                deleteCompany(this.companyTableId);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void deleteCompany(String companyTableId) throws IOException {
        DeleteWindow.sceneWindow = "ViewCompany";
        DeleteWindow.deletionId = companyTableId;
        Parent parent = null;
        try {
            parent = FXMLLoader.load(getClass().getResource("../view/delete_window.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setStyle("-fx-padding: -40 -20 -40 -20;");
        dialogLayout.setBody(parent);
        dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        stackPane.setVisible(true);
        dialog.show();
        dialog.setOverlayClose(false);
    }

    public void viewClicked()
    {
        Parent parent = null;
        viewCompanyId = this.companyTableId;
        try {
            parent = FXMLLoader.load(getClass().getResource("../view/company_detail.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(parent);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    public void editClicked()
    {
        Parent parent = null;
        try {
            UpdateCompany.companyTableId = companyTableId;
            UpdateCompany.companyId = companyId;
            UpdateCompany.companyName = companyName;
            UpdateCompany.companyAddress = companyAddress;
            UpdateCompany.companyContact = companyContact;
            UpdateCompany.companyEmail = companyEmail;
            UpdateCompany.companyStatus = companyStatus;
            parent = FXMLLoader.load(getClass().getResource("../view/update_company.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setStyle("-fx-padding: -20 -10 -20 -10;");
        dialogLayout.setHeading(lblUpdate);
        dialogLayout.setBody(parent);
        dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        stackPane.setVisible(true);
        dialog.show();
        dialog.setOverlayClose(false);
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getCompanyTableId() {
        return companyTableId;
    }

    public void setCompanyTableId(String companyTableId) {
        this.companyTableId = companyTableId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getCompanyContact() {
        return companyContact;
    }

    public void setCompanyContact(String companyContact) {
        this.companyContact = companyContact;
    }

    public String getCompanyEmail() {
        return companyEmail;
    }

    public void setCompanyEmail(String companyEmail) {
        this.companyEmail = companyEmail;
    }

    public String getCompanyStatus() {
        return companyStatus;
    }

    public void setCompanyStatus(String companyStatus) {
        this.companyStatus = companyStatus;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public ArrayList<ArrayList<String>> getCompaniesInfo(Statement stmt, Connection con)
    {
        ArrayList<ArrayList<String>> companyData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT company_table_id, company_id, company_name,company_address,company_contact,company_email,company_status  FROM `company_info` WHERE company_status != 'Deleted' ORDER BY company_table_id");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2));
                temp.add(rs.getString(3));
                temp.add(rs.getString(4));
                temp.add(rs.getString(5));
                temp.add(rs.getString(6));
                temp.add(rs.getString(7));
                companyData.add(temp);
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return companyData;
    }

    public ArrayList<ArrayList<String>> getCompaniesSearch(Statement stmt, Connection con, String companyId, String companyName, String companyAddress, String companyCity, String companyContact, String companyEmail, String companyStatus)
    {
        ArrayList<ArrayList<String>> companyData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        String searchQuery;
        int multipleSearch = 0;
        searchQuery = "SELECT `company_table_id`, `company_id`, `company_name`, `company_address`, `company_contact`, `company_email`, `company_status`  FROM `company_info` WHERE ";
        if(!companyId.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`company_id` = '"+companyId+"'";
            multipleSearch++;
        }
        if(!companyName.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`company_name` = '"+companyName+"'";
            multipleSearch++;
        }
        if(!companyAddress.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`company_address` = '"+companyAddress+"'";
            multipleSearch++;
        }
        if(!companyCity.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`company_city` = '"+companyCity+"'";
            multipleSearch++;
        }
        if(!companyContact.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`company_contact` = '"+companyContact+"'";
            multipleSearch++;
        }
        if(!companyEmail.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`company_email` = '"+companyEmail+"'";
            multipleSearch++;
        }
        if(!companyStatus.equals("All"))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`company_status` = '"+companyStatus+"'";
            multipleSearch++;
        }
        if(multipleSearch >= 1)
        {
            searchQuery += " AND ";
        }
        searchQuery += "`company_status` != 'Deleted' ORDER BY `company_table_id`";
        try {
            rs = stmt.executeQuery(searchQuery);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2));
                temp.add(rs.getString(3));
                temp.add(rs.getString(4));
                temp.add(rs.getString(5));
                temp.add(rs.getString(6));
                temp.add(rs.getString(7));
                companyData.add(temp);
            }

//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return companyData;
    }

    public ArrayList<String> getCompaniesSummary(Statement stmt, Connection con)
    {
        ArrayList<String> companyData = new ArrayList<String>();
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT COUNT(`company_table_id`) FROM `company_info` WHERE `company_status` != 'Deleted'");
            while (rs.next())
            {
                companyData.add(rs.getString(1));
            }
            rs = stmt.executeQuery("SELECT COUNT(`company_table_id`) FROM `company_info` WHERE `company_status` = 'Active'");
            while (rs.next())
            {
                companyData.add(rs.getString(1));
            }
            rs = stmt.executeQuery("SELECT COUNT(`company_table_id`) FROM `company_info` WHERE `company_status` = 'In Active'");
            while (rs.next())
            {
                companyData.add(rs.getString(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return companyData;
    }

    private Node createViewIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.EYE);
        graphic.setFontSize(20.0);
        return graphic;
    }

    private Node createEditIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.EDIT);
        graphic.setFontSize(20.0);
        return graphic;
    }

    private Node createDeleteIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.TRASH);
        graphic.setFontSize(20.0);
        return graphic;
    }
}
