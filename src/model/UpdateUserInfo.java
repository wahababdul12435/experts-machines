package model;

import com.jfoenix.controls.JFXButton;
import controller.UpdateUser;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class UpdateUserInfo {
    private String srNo;
    private String userSubarea;

    private HBox operationsPane;
    private JFXButton btnDelete;

    public static TableView<UpdateUserInfo> table_designatedareas;

    public ArrayList<ArrayList<String>> areasData = new ArrayList<>();
    public ArrayList<ArrayList<String>> subareasData = new ArrayList<>();

    public UpdateUserInfo() {
        this.srNo = "";
        this.userSubarea = "";
        this.btnDelete = new JFXButton("Delete");
        this.operationsPane = new HBox(btnDelete);
    }

    public UpdateUserInfo(String srNo, String userSubarea) {
        this.srNo = srNo;
        this.userSubarea = userSubarea;

        this.btnDelete = new JFXButton("Delete");
        this.btnDelete.setOnAction((action)->deleteDesignatedCity());
        this.operationsPane = new HBox(btnDelete);
    }

    public void deleteDesignatedCity()
    {
        UpdateUser.userCityNames.remove(Integer.parseInt(srNo)-1);
        UpdateUser.userCityIds.remove(Integer.parseInt(srNo)-1);
        table_designatedareas.setItems(UpdateUser.parseUserList());
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getUserSubarea() {
        return userSubarea;
    }

    public void setUserSubarea(String userSubarea) {
        this.userSubarea = userSubarea;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public ArrayList<ArrayList<String>> getCitiesData()
    {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement stmt = objMysqlCon.stmt;
        Connection con = objMysqlCon.con;
        ResultSet rs = null;
        ArrayList<String> temp;
        try {
            rs = stmt.executeQuery("SELECT `city_table_id`, `city_name` FROM `city_info` WHERE `city_status` != 'Deleted'");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2));
                areasData.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return areasData;
    }
}
