package model;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class SendServerData {
    String webURL = "https://younastraders.000webhostapp.com/ProfitFlow/ServerDataGet/SendData.php";
    String response = "";

    public String setNewDataStatus(String serverId, String softwareId, String operation)
    {
        try {
            URL objurl = new URL(webURL);
            HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
            objhttpurlconnection.setConnectTimeout(5 * 1000);
            objhttpurlconnection.connect();
            if (objhttpurlconnection.getResponseCode() == 200)
            {
                objhttpurlconnection.disconnect();
                objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("operation", "UTF-8") +"="+URLEncoder.encode(operation, "UTF-8")+"&"+
                        URLEncoder.encode("server_id", "UTF-8") +"="+URLEncoder.encode(serverId, "UTF-8")+"&"+
                        URLEncoder.encode("software_id", "UTF-8") +"="+URLEncoder.encode(softwareId, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                String orderResponse;
                while ((orderResponse = bufferedReader.readLine()) != null)
                {
                    response += orderResponse;
                }
                objhttpurlconnection.disconnect();
            }
            else
            {
                response = "Unable to Connect To Server";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }
}
