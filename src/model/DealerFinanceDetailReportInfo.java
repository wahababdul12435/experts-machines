package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import controller.DealerFinanceDetailReport;
import controller.UpdateComment;
import controller.UpdateDealerFinance;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DealerFinanceDetailReportInfo {
    private String srNo;
    private String paymentId;
    private String dealerId;
    private String dealerName;
    private String date;
    private String day;
    private String cashCollection;
    private String cashReturn;
    private String cashWaiveOff;
    private String comment;

    private HBox operationsPane;
    private JFXButton btnComment;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    public static StackPane stackPane;
    public static JFXDialog dialog;

    public DealerFinanceDetailReportInfo() {
        this.srNo = "";
        this.paymentId = "";
        this.dealerId = "";
        this.dealerName = "";
        this.date = "";
        this.day = "";
        this.cashCollection = "";
        this.cashReturn = "";
        this.cashWaiveOff = "";
        this.comment = "";
    }

    public DealerFinanceDetailReportInfo(String srNo, String paymentId, String dealerId, String dealerName, String date, String day, String cashCollection, String cashReturn, String cashWaiveOff, String comment) {
        this.srNo = srNo;
        this.paymentId = paymentId;
        this.dealerId = dealerId;
        this.dealerName = dealerName;
        this.date = date;
        this.day = day;
        this.cashCollection = cashCollection;
        this.cashReturn = cashReturn;
        this.cashWaiveOff = cashWaiveOff;
        this.comment = comment;

        this.btnComment = new JFXButton();
        this.btnEdit = new JFXButton();
        this.btnDelete = new JFXButton();
        this.btnComment.setGraphic(GlobalVariables.createCommentIcon());
        this.btnEdit.setGraphic(GlobalVariables.createEditIcon());
        this.btnDelete.setGraphic(GlobalVariables.createDeleteIcon());
        this.btnComment.getStyleClass().add("btn_icon");
        this.btnEdit.getStyleClass().add("btn_icon");
        this.btnDelete.getStyleClass().add("btn_icon");
        this.operationsPane = new HBox(btnComment, btnEdit, btnDelete);
        this.btnComment.setOnAction((action)->commentClicked());
        this.btnDelete.setOnAction(event -> deleteDealerFinance(this.paymentId));
    }

    private void commentClicked()
    {
        Parent parent = null;
        try {
            UpdateComment.commentType = "Dealer Finance";
            UpdateComment.orderId = this.paymentId;
            UpdateComment.comment = this.comment;
            parent = FXMLLoader.load(getClass().getResource("../view/update_comment.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setStyle("-fx-padding: -20 -10 -20 -10;");
        dialogLayout.setBody(parent);
        dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        stackPane.setVisible(true);
        dialog.show();
        dialog.setOverlayClose(false);
    }

    public void deleteDealerFinance(String paymentId) {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        try {
            String updateQuery = "UPDATE `dealer_payments` SET `status`= 'Deleted' WHERE `payment_id` = '"+paymentId+"'";
            objStmt.executeUpdate(updateQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        DealerFinanceReportInfo.dealerFinanceId = dealerId;
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/dealer_finance_detail_report.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    public void editClicked()
    {
        Parent parent = null;
        DealerFinanceReportInfo.dealerFinanceId = dealerId;
        try {
            UpdateDealerFinance.paymentId = paymentId;
            UpdateDealerFinance.dealerId = dealerId;
            UpdateDealerFinance.dealerName = dealerName;
            UpdateDealerFinance.date = date;
            if(!cashCollection.equals("-"))
            {
                UpdateDealerFinance.preCashAmount = cashCollection;
                UpdateDealerFinance.cashAmount = cashCollection;
                UpdateDealerFinance.preCashType = "Collection";
                UpdateDealerFinance.cashType = "Collection";
            }
            else if(!cashReturn.equals("-"))
            {
                UpdateDealerFinance.preCashAmount = cashReturn;
                UpdateDealerFinance.cashAmount = cashReturn;
                UpdateDealerFinance.preCashType = "Return";
                UpdateDealerFinance.cashType = "Return";
            }
            else if(!cashWaiveOff.equals("-"))
            {
                UpdateDealerFinance.preCashAmount = cashWaiveOff;
                UpdateDealerFinance.cashAmount = cashWaiveOff;
                UpdateDealerFinance.preCashType = "Waive Off";
                UpdateDealerFinance.cashType = "Waive Off";
            }

            parent = FXMLLoader.load(getClass().getResource("../view/update_dealer_finance.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setStyle("-fx-padding: -20 -10 -20 -10;");
        dialogLayout.setBody(parent);
        dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        stackPane.setVisible(true);
        dialog.show();
        dialog.setOverlayClose(false);
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getDealerId() {
        return dealerId;
    }

    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getCashCollection() {
        return cashCollection;
    }

    public void setCashCollection(String cashCollection) {
        this.cashCollection = cashCollection;
    }

    public String getCashReturn() {
        return cashReturn;
    }

    public void setCashReturn(String cashReturn) {
        this.cashReturn = cashReturn;
    }

    public String getCashWaiveOff() {
        return cashWaiveOff;
    }

    public void setCashWaiveOff(String cashWaiveOff) {
        this.cashWaiveOff = cashWaiveOff;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public static StackPane getStackPane() {
        return stackPane;
    }

    public static void setStackPane(StackPane stackPane) {
        DealerFinanceDetailReportInfo.stackPane = stackPane;
    }

    public static JFXDialog getDialog() {
        return dialog;
    }

    public static void setDialog(JFXDialog dialog) {
        DealerFinanceDetailReportInfo.dialog = dialog;
    }

    public ArrayList<ArrayList<String>> getDealerFinanceDetail(Statement stmt, Connection con, String dealerId)
    {
        ArrayList<ArrayList<String>> dealersFinanceData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT `dealer_payments`.`payment_id`, `dealer_payments`.`dealer_id`, `dealer_info`.`dealer_name`, `dealer_payments`.`date`, `dealer_payments`.`day`, `dealer_payments`.`amount`, `dealer_payments`.`cash_type`, `dealer_payments`.`comments` FROM `dealer_payments` INNER JOIN `dealer_info` ON `dealer_info`.`dealer_table_id` = `dealer_payments`.`dealer_id` WHERE `dealer_payments`.`dealer_id` = '"+dealerId+"' AND `dealer_payments`.`status` != 'Deleted' ORDER BY str_to_date(`date`, '%d/%b/%Y') DESC");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Payment Id
                temp.add(rs.getString(2)); // Dealer Id
                temp.add(rs.getString(3)); // Dealer Name
                temp.add(rs.getString(4)); // Date
                temp.add(rs.getString(5)); // Day
                temp.add(rs.getString(6)); // Cash
                temp.add(rs.getString(7)); // Cash Type
                temp.add(rs.getString(8)); // Comments
                dealersFinanceData.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dealersFinanceData;
    }

    public ArrayList<ArrayList<String>> getDealerFinanceDetailSearch(Statement stmt, Connection con, String dealerId, String fromDate, String toDate, String day, String fromAmount, String toAmount, String type)
    {
        ArrayList<ArrayList<String>> dealersFinanceData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        String searchQuery;
        int multipleSearch = 0;
        searchQuery = "SELECT `dealer_payments`.`payment_id`, `dealer_payments`.`dealer_id`, `dealer_info`.`dealer_name`, `dealer_payments`.`date`, `dealer_payments`.`day`, `dealer_payments`.`amount`, `dealer_payments`.`cash_type`, `dealer_payments`.`comments` FROM `dealer_payments` INNER JOIN `dealer_info` ON `dealer_info`.`dealer_table_id` = `dealer_payments`.`dealer_id` WHERE ";
        if(!fromDate.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "str_to_date(`date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y')";
            multipleSearch++;
        }
        if(!toDate.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "str_to_date(`date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y')";
            multipleSearch++;
        }
        if(!day.equals("All"))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`dealer_payments`.`day` = '"+day+"'";
            multipleSearch++;
        }
        if(!fromAmount.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`dealer_payments`.`amount` >= '"+fromAmount+"'";
            multipleSearch++;
        }
        if(!toAmount.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`dealer_payments`.`amount` <= '"+toAmount+"'";
            multipleSearch++;
        }
        if(!type.equals("All"))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`dealer_payments`.`cash_type` = '"+type+"'";
            multipleSearch++;
        }
        if(multipleSearch >= 1)
        {
            searchQuery += " AND ";
        }
        searchQuery += "`dealer_payments`.`dealer_id` = '"+dealerId+"' AND `dealer_payments`.`status` != 'Deleted' ORDER BY str_to_date(`date`, '%d/%b/%Y') DESC";
        try {
            rs = stmt.executeQuery(searchQuery);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Payment Id
                temp.add(rs.getString(2)); // Dealer Id
                temp.add(rs.getString(3)); // Dealer Name
                temp.add(rs.getString(4)); // Date
                temp.add(rs.getString(5)); // Day
                temp.add(rs.getString(6)); // Cash
                temp.add(rs.getString(7)); // Cash Type
                temp.add(rs.getString(8)); // Comments
                dealersFinanceData.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dealersFinanceData;
    }

    public void updateDealerFinance(Statement stmt, Connection con, String paymentId, String dealerId, String preCashAmount, String preCashType, String date, String day, String amount, String cashType)
    {
        String currentDate = GlobalVariables.getStDate();
        String currentTime = GlobalVariables.getStTime();
        String currentUser = GlobalVariables.getUserId();
        String updateQuery = null;
        try {
            if(preCashType.equals("Collection"))
            {
                updateQuery = "UPDATE `dealer_overall_record` SET `cash_collected`= `cash_collected` - '"+preCashAmount+"', `pending_payments` = `pending_payments` + '"+preCashAmount+"' WHERE `dealer_id` = '"+dealerId+"'";
            }
            else if(preCashType.equals("Return"))
            {
                updateQuery = "UPDATE `dealer_overall_record` SET `cash_return`= `cash_return` - '"+preCashAmount+"', `pending_payments` = `pending_payments` - '"+preCashAmount+"' WHERE `dealer_id` = '"+dealerId+"'";
            }
            else if(preCashType.equals("Waive Off"))
            {
                updateQuery = "UPDATE `dealer_overall_record` SET `waived_off_price`= `waived_off_price` - '"+preCashAmount+"', `pending_payments` = `pending_payments` + '"+preCashAmount+"' WHERE `dealer_id` = '"+dealerId+"'";
            }
            stmt.executeUpdate(updateQuery);

            updateQuery = "UPDATE `dealer_payments` SET `date`='"+date+"', `day`='"+day+"', `amount`='"+amount+"',`cash_type`='"+cashType+"', `user_id`='"+currentUser+"' WHERE `payment_id` = '"+paymentId+"'";
            stmt.executeUpdate(updateQuery);

            if(cashType.equals("Collection"))
            {
                updateQuery = "UPDATE `dealer_overall_record` SET `cash_collected`= `cash_collected` + '"+amount+"', `pending_payments` = `pending_payments` - '"+amount+"' WHERE `dealer_id` = '"+dealerId+"'";
            }
            else if(cashType.equals("Return"))
            {
                updateQuery = "UPDATE `dealer_overall_record` SET `cash_return`= `cash_return` + '"+amount+"', `pending_payments` = `pending_payments` + '"+amount+"' WHERE `dealer_id` = '"+dealerId+"'";
            }
            else if(cashType.equals("Waive Off"))
            {
                updateQuery = "UPDATE `dealer_overall_record` SET `waived_off_price`= `waived_off_price` + '"+amount+"', `pending_payments` = `pending_payments` - '"+amount+"' WHERE `dealer_id` = '"+dealerId+"'";
            }
            stmt.executeUpdate(updateQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateComment(Statement stmt, Connection con, String paymentId, String comment)
    {
        String currentDate = GlobalVariables.getStDate();
        String currentTime = GlobalVariables.getStTime();
        String currentUser = GlobalVariables.getUserId();
        String updateQuery = "UPDATE `dealer_payments` SET `comments` = '"+comment+"' WHERE `payment_id` = '"+paymentId+"'";
        try {
            stmt.executeUpdate(updateQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private Node createEditIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.EDIT);
        graphic.setFontSize(20.0);
        return graphic;
    }

    private Node createDeleteIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.TRASH);
        graphic.setFontSize(20.0);
        return graphic;
    }
}
