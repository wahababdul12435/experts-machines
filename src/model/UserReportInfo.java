package model;

import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.HBox;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class UserReportInfo {
    private String srNo;
    private String userTableId;
    private String userId;
    private String userName;
    private String userType;
    private String bookings;
    private String delivered;
    private String itemsReturned;
    private String bookingPrice;
    private String returnPrice;
    private String cashCollection;
    private String cashReturned;
    private String cashWaivedOff;
    private String days;
    private String userStatus;

    private HBox operationsPane;
    private JFXButton btnView;
    private JFXButton btnEdit;

    public static String userReportId;

    public UserReportInfo() {
    }

    public UserReportInfo(String srNo, String userTableId, String userId, String userName, String userType, String bookings, String delivered, String itemsReturned, String bookingPrice, String returnPrice, String cashCollection, String cashReturned, String cashWaivedOff, String days, String userStatus) {
        this.srNo = srNo;
        this.userTableId = userTableId;
        this.userId = userId;
        this.userName = userName;
        this.userType = userType;
        this.bookings = bookings;
        this.delivered = delivered;
        this.itemsReturned = itemsReturned;
        this.bookingPrice = bookingPrice;
        this.returnPrice = returnPrice;
        this.cashCollection = cashCollection;
        this.cashReturned = cashReturned;
        this.cashWaivedOff = cashWaivedOff;
        this.days = days;
        this.userStatus = userStatus;

        this.btnView = new JFXButton();
        this.btnView.setGraphic(createViewIcon());
        this.btnView.getStyleClass().add("btn_icon");
        this.btnEdit = new JFXButton();
        this.btnEdit.setGraphic(createEditIcon());
        this.btnEdit.getStyleClass().add("btn_icon");
        this.operationsPane = new HBox(btnView, btnEdit);

        this.btnView.setOnAction((event)->viewUserDetail());
    }

    public void viewUserDetail()
    {
        Parent parent = null;
        try {
            userReportId = userTableId;
            parent = FXMLLoader.load(getClass().getResource("../view/user_report_detail.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(parent);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getUserTableId() {
        return userTableId;
    }

    public void setUserTableId(String userTableId) {
        this.userTableId = userTableId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getBookings() {
        return bookings;
    }

    public void setBookings(String bookings) {
        this.bookings = bookings;
    }

    public String getDelivered() {
        return delivered;
    }

    public void setDelivered(String delivered) {
        this.delivered = delivered;
    }

    public String getItemsReturned() {
        return itemsReturned;
    }

    public void setItemsReturned(String itemsReturned) {
        this.itemsReturned = itemsReturned;
    }

    public String getBookingPrice() {
        return bookingPrice;
    }

    public void setBookingPrice(String bookingPrice) {
        this.bookingPrice = bookingPrice;
    }

    public String getReturnPrice() {
        return returnPrice;
    }

    public void setReturnPrice(String returnPrice) {
        this.returnPrice = returnPrice;
    }

    public String getCashCollection() {
        return cashCollection;
    }

    public void setCashCollection(String cashCollection) {
        this.cashCollection = cashCollection;
    }

    public String getCashReturned() {
        return cashReturned;
    }

    public void setCashReturned(String cashReturned) {
        this.cashReturned = cashReturned;
    }

    public String getCashWaivedOff() {
        return cashWaivedOff;
    }

    public void setCashWaivedOff(String cashWaivedOff) {
        this.cashWaivedOff = cashWaivedOff;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnView() {
        return btnView;
    }

    public void setBtnView(JFXButton btnView) {
        this.btnView = btnView;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public ArrayList<ArrayList<String>> getUserReportInfo(Statement stmt1, Statement stmt2, Statement stmt3, Connection con)
    {
        ArrayList<ArrayList<String>> productReportData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs1 = null;
        ResultSet rs2 = null;
        ResultSet rs3 = null;
        try {
            rs1 = stmt1.executeQuery("SELECT `user_info`.`user_table_id`, `user_info`.`user_id`, `user_info`.`user_name`, `user_info`.`user_type`, SUM(case when `order_info`.`booking_user_id` = `user_info`.`user_table_id` AND `order_info`.`status` != 'Deleted' then 1 else 0 end) as 'total_booking', SUM(case when `order_info`.`delivered_user_id` = `user_info`.`user_table_id` AND `order_info`.`status` = 'Delivered' then 1 else 0 end) as 'total_delivered' FROM `user_info` LEFT OUTER JOIN `order_info` ON `order_info`.`booking_user_id` = `user_info`.`user_table_id` OR `order_info`.`delivered_user_id` = `user_info`.`user_table_id` WHERE `user_info`.`user_status` != 'Deleted' GROUP BY `user_info`.`user_table_id`");
            rs2 = stmt2.executeQuery("SELECT `user_info`.`user_table_id`, COALESCE(SUM(`order_return_detail`.`prod_quant`),0) as 'items_returned', ROUND((SELECT COALESCE(SUM(`order_info`.`final_price`),0) FROM `order_info` WHERE `order_info`.`booking_user_id` = `user_info`.`user_table_id` AND `order_info`.`status` != 'Deleted'), 2) as 'booking_price', ROUND((SELECT COALESCE(SUM(`order_return`.`return_total_price`),0) FROM `order_return` WHERE `order_return`.`enter_user_id` = `user_info`.`user_table_id` AND `order_return`.`status` != 'Deleted'), 2) as 'return_price' FROM `user_info` LEFT OUTER JOIN `order_return` ON `order_return`.`enter_user_id` = `user_info`.`user_table_id` LEFT OUTER JOIN `order_return_detail` ON `order_return_detail`.`return_id` = `order_return`.`return_id` WHERE `user_info`.`user_status` != 'Deleted' GROUP BY `user_info`.`user_table_id`");
            rs3 = stmt3.executeQuery("SELECT `user_info`.`user_table_id`, ROUND((SELECT COALESCE(SUM(`amount`),0) FROM `dealer_payments` WHERE `cash_type` = 'Collection' AND `dealer_payments`.`status` != 'Deleted' AND `dealer_payments`.`user_id` = `user_info`.`user_table_id`), 2) as 'cash_collection', ROUND((SELECT COALESCE(SUM(`amount`),0) FROM `dealer_payments` WHERE `cash_type` = 'Return' AND `dealer_payments`.`status` != 'Deleted' AND `dealer_payments`.`user_id` = `user_info`.`user_table_id`), 2) as 'cash_return', ROUND((SELECT COALESCE(SUM(`amount`),0) FROM `dealer_payments` WHERE `cash_type` = 'Waived Off' AND `dealer_payments`.`status` != 'Deleted' AND `dealer_payments`.`user_id` = `user_info`.`user_table_id`), 2) as 'cash_waivedoff', `user_info`.`creating_date`, `user_info`.`user_status` FROM `user_info` WHERE `user_info`.`user_status` != 'Deleted' GROUP BY `user_info`.`user_table_id`");
            while (rs1.next() && rs2.next() && rs3.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs1.getString(1)); // Table Id
                temp.add(rs1.getString(2)); // Id
                temp.add(rs1.getString(3)); // Name
                temp.add(rs1.getString(4)); // Type
                temp.add(rs1.getString(5)); // Booking
                temp.add(rs1.getString(6)); // Delivered
                temp.add(rs2.getString(2)); // Items Returned
                temp.add(rs2.getString(3)); // Booking Price
                temp.add(rs2.getString(4)); // Return Price
                temp.add(rs3.getString(2)); // Cash Collection
                temp.add(rs3.getString(3)); // Cash Returned
                temp.add(rs3.getString(4)); // Cash Waived Off

                if(rs3.getString(5) != null && !rs3.getString(5).equals(""))
                {
                    String stEndDate = GlobalVariables.getStDate();
                    String stStartDate = rs3.getString(5);

                    Date endDate = null;
                    Date startDate = null;
                    SimpleDateFormat fmt = new SimpleDateFormat("dd/MMM/yyyy");
                    try {
                        startDate = fmt.parse(stStartDate);
                        endDate = fmt.parse(stEndDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    long diff = endDate.getTime() - startDate.getTime();
                    diff = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);

                    temp.add(String.valueOf(diff)); // Days
                }
                else
                {
                    temp.add(String.valueOf("N/A")); // Days
                }

                temp.add(rs3.getString(6)); // User Status
                productReportData.add(temp);
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return productReportData;
    }

    private Node createEditIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.EDIT);
        graphic.setFontSize(20.0);
        return graphic;
    }

    private Node createViewIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.EYE);
        graphic.setFontSize(20.0);
        return graphic;
    }
}
