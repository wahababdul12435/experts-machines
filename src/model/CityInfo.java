package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import controller.EnterCity;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class CityInfo {
    private String srNo;
    private String cityId;
    private String cityName;
    private String districtName;

    private HBox operationsPane;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    public static ArrayList<String> cityIdArr = new ArrayList<>();
    public static ArrayList<String> cityNameArr = new ArrayList<>();
    public static ArrayList<String> districtIdArr = new ArrayList<>();
    public static ArrayList<String> districtNameArr = new ArrayList<>();

    public static TableView<CityInfo> table_add_city;

    public static JFXTextField txtCityId;
    public static JFXTextField txtCityName;
    public static JFXComboBox<String> txtDistrictName;

    public static Button btnAdd;
    public static Button btnCancel;
    public static Button btnUpdate;

    private ArrayList<String> savedDistrictIds;
    private ArrayList<String> savedDistrictNames;

    MysqlCon objMysqlCon = new MysqlCon();
    Statement objStmt = objMysqlCon.stmt;
    Connection objCon = objMysqlCon.con;

    public CityInfo() {
        this.srNo = "";
        this.cityId = "";
        this.cityName = "";
        this.districtName = "";

        getDistrictsInfo(objStmt, objCon);
    }

    public CityInfo(String srNo, String cityId, String cityName, String districtName) {
        this.srNo = srNo;
        this.cityId = cityId;
        this.cityName = cityName;
        this.districtName = districtName;

        this.btnEdit = new JFXButton("Edit");
        this.btnDelete = new JFXButton("Delete");
        this.btnEdit.setOnAction((action)->editAddedRecord());
        this.btnDelete.setOnAction((action)->deleteAddedRecord());
        this.operationsPane = new HBox(btnEdit, btnDelete);
    }

    private void deleteAddedRecord()
    {
        cityIdArr.remove(Integer.parseInt(srNo)-1);
        cityNameArr.remove(Integer.parseInt(srNo)-1);
        districtIdArr.remove(Integer.parseInt(srNo)-1);
        districtNameArr.remove(Integer.parseInt(srNo)-1);
        table_add_city.setItems(EnterCity.parseUserList());
    }

    public void editAddedRecord()
    {
        EnterCity.srNo = Integer.parseInt(srNo)-1;
        txtCityId.setText(cityIdArr.get(Integer.parseInt(srNo)-1));
        txtCityName.setText(cityNameArr.get(Integer.parseInt(srNo)-1));
        txtDistrictName.setValue(districtNameArr.get(Integer.parseInt(srNo)-1));
        btnAdd.setVisible(false);
        btnCancel.setVisible(true);
        btnUpdate.setVisible(true);
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public ArrayList<String> getSavedDistrictIds() {
        return savedDistrictIds;
    }

    public void setSavedDistrictIds(ArrayList<String> savedDistrictIds) {
        this.savedDistrictIds = savedDistrictIds;
    }

    public ArrayList<String> getSavedDistrictNames() {
        return savedDistrictNames;
    }

    public void setSavedDistrictNames(ArrayList<String> savedDistrictNames) {
        this.savedDistrictNames = savedDistrictNames;
    }

    public void insertCity(Statement stmt, Connection con) throws SQLException {
        String currentDate = GlobalVariables.getStDate();
        String currentTime = GlobalVariables.getStTime();
        String currentUser = GlobalVariables.getUserId();
        boolean withId = false;
        boolean withoutId = false;
        String insertQueryWithId = "INSERT INTO `city_info`(`city_id`, `district_table_id`, `city_name`, `city_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`) VALUES ";
        String insertQueryWithoutId = "INSERT INTO `city_info`(`district_table_id`, `city_name`, `city_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`) VALUES ";

        for(int i=0; i<cityIdArr.size(); i++)
        {
            if(cityIdArr.get(i).equals("") || cityIdArr.get(i) == null)
            {
                if(withoutId)
                {
                    insertQueryWithoutId += ", ";
                }
                withoutId = true;
                insertQueryWithoutId += "('"+districtIdArr.get(i)+"','"+cityNameArr.get(i)+"','Active','"+currentUser+"','"+currentDate+"','"+currentTime+"','"+currentUser+"','"+currentDate+"','"+currentTime+"')";
            }
            else
            {
                if(withId)
                {
                    insertQueryWithId += ", ";
                }
                withId = true;
                insertQueryWithId += "('"+cityIdArr.get(i)+"','"+districtIdArr.get(i)+"','"+cityNameArr.get(i)+"','Active','"+currentUser+"','"+currentDate+"','"+currentTime+"','"+currentUser+"','"+currentDate+"','"+currentTime+"')";
            }
        }
        int insertInfo = 1;
        if(withId)
        {
            if(stmt.executeUpdate(insertQueryWithId) == -1)
            {
                insertInfo = -1;
            }
        }
        if(withoutId)
        {
            if(stmt.executeUpdate(insertQueryWithoutId) == -1)
            {
                insertInfo = -1;
            }
        }
        if(insertInfo == -1)
        {
            String title = "Error";
            String message = "Unable to insert";
            GlobalVariables.showNotification(-1, title, message);
        }
        else
        {
            String title = "Successfull";
            String message = "Record Inserted";
            GlobalVariables.showNotification(1, title, message);
        }
        refreshAllArrays();
    }

    private void refreshAllArrays()
    {
        cityIdArr = new ArrayList<>();
        cityNameArr = new ArrayList<>();
        districtIdArr = new ArrayList<>();
        districtNameArr = new ArrayList<>();
    }

    public void updateCity(Statement stmt, Connection con, String cityTableId, String cityId, String cityName, String districtId, String cityStatus)
    {
        String currentDate = GlobalVariables.getStDate();
        String currentTime = GlobalVariables.getStTime();
        String currentUser = GlobalVariables.getUserId();
        String updateQuery;
        try {
            if(cityId == null || cityId.equals("") || cityId.equals("N/A"))
            {
                if(districtId == null || districtId.equals("") || districtId.equals("N/A"))
                {
                    updateQuery = "UPDATE `city_info` SET `city_id`= NULL,`district_table_id`= NULL,`city_name`='"+cityName+"',`city_status`='"+cityStatus+"',`update_user_id`='"+currentUser+"',`update_date`='"+currentDate+"',`update_time`='"+currentTime+"' WHERE `city_table_id` = '"+cityTableId+"'";
                }
                else
                {
                    updateQuery = "UPDATE `city_info` SET `city_id`= NULL,`district_table_id`='"+districtId+"',`city_name`='"+cityName+"',`city_status`='"+cityStatus+"',`update_user_id`='"+currentUser+"',`update_date`='"+currentDate+"',`update_time`='"+currentTime+"' WHERE `city_table_id` = '"+cityTableId+"'";
                }
            }
            else
            {
                if(districtId == null || districtId.equals("") || districtId.equals("N/A"))
                {
                    updateQuery = "UPDATE `city_info` SET `city_id`='"+cityId+"',`district_table_id`= NULL,`city_name`='"+cityName+"',`city_status`='"+cityStatus+"',`update_user_id`='"+currentUser+"',`update_date`='"+currentDate+"',`update_time`='"+currentTime+"' WHERE `city_table_id` = '"+cityTableId+"'";
                }
                else
                {
                    updateQuery = "UPDATE `city_info` SET `city_id`='"+cityId+"',`district_table_id`='"+districtId+"',`city_name`='"+cityName+"',`city_status`='"+cityStatus+"',`update_user_id`='"+currentUser+"',`update_date`='"+currentDate+"',`update_time`='"+currentTime+"' WHERE `city_table_id` = '"+cityTableId+"'";
                }
            }

            stmt.executeUpdate(updateQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<ArrayList<String>> getAddedCity()
    {
        ArrayList<ArrayList<String>> cityData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        for(int i=0; i<cityIdArr.size(); i++)
        {
            temp = new ArrayList<String>();
            temp.add(cityIdArr.get(i));
            temp.add(cityNameArr.get(i));
            temp.add(districtNameArr.get(i));
            cityData.add(temp);
        }
        return cityData;
    }

    String checkCityId = new String();
    public String confirmNewId(String cityId)
    {
        ResultSet rs3 = null;
        try {
            rs3 = objStmt.executeQuery("SELECT `city_id` FROM `city_info` WHERE `city_id` = '"+cityId+"' AND `city_status` != 'Deleted'");
            while (rs3.next())
            {
                checkCityId = rs3.getString(1) ;
            }
//            objCon.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return checkCityId;
    }

    public void getDistrictsInfo(Statement stmt, Connection con)
    {
        ArrayList<String> tempIds;
        ArrayList<String> tempNames;
        tempIds = new ArrayList<String>();
        tempNames = new ArrayList<String>();
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT `district_table_id`, `district_name` FROM `district_info` WHERE `district_status` != 'Deleted' ORDER BY `district_table_id`");
            while (rs.next())
            {
                tempIds.add(rs.getString(1));
                tempNames.add(rs.getString(2)+" ("+rs.getString(1)+")");
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        setSavedDistrictIds(tempIds);
        setSavedDistrictNames(tempNames);
    }

    public ArrayList<ArrayList<String>> getCitiesWithIds(Statement stmt, Connection con)
    {
        ArrayList<ArrayList<String>> cityData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT `city_table_id`, `city_id`, `city_name`, `district_table_id` FROM `city_info` WHERE `city_status` != 'Deleted'");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Table id
                temp.add(rs.getString(2)); // City id
                if(rs.getString(2) != null && !rs.getString(2).equals(""))
                {
                    temp.add(rs.getString(3)+" ("+rs.getString(2)+")"); // City name
                }
                else
                {
                    temp.add(rs.getString(3)); // City name
                }
                temp.add(rs.getString(4)); // District Id
                cityData.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cityData;
    }
}
