package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import controller.UpdateComment;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ViewSalesReturnInfo {
    private String srNo;
    private String returnDate;
    private String returnDay;
    private String returnId;
    private String dealerName;
    private String dealerContact;
    private String returnQty;
    private String returnPrice;
    private String returnUser;
    private String comment;

    private HBox operationsPane;
    private JFXButton btnComment;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    SimpleDateFormat fmt = new SimpleDateFormat("dd/MMM/yyyy");
    public static String orderId;

    public static StackPane stackPane;
    public static JFXDialog dialog;

    public ViewSalesReturnInfo() {
    }

    public ViewSalesReturnInfo(String srNo, String returnDate, String returnDay, String returnId, String dealerName, String dealerContact, String returnQty, String returnPrice, String returnUser, String comment) {
        this.srNo = srNo;
        this.returnDate = returnDate;
        this.returnDay = returnDay;
        this.returnId = returnId;
        this.dealerName = dealerName;
        this.dealerContact = dealerContact;
        this.returnQty = returnQty;
        this.returnPrice = returnPrice;
        this.returnUser = returnUser;
        this.comment = comment;

        this.btnComment = new JFXButton();
        this.btnEdit = new JFXButton();
        this.btnDelete = new JFXButton();
        this.btnComment.setGraphic(GlobalVariables.createCommentIcon());
        this.btnEdit.setGraphic(GlobalVariables.createEditIcon());
        this.btnDelete.setGraphic(GlobalVariables.createDeleteIcon());
        this.btnComment.getStyleClass().add("btn_icon");
        this.btnEdit.getStyleClass().add("btn_icon");
        this.btnDelete.getStyleClass().add("btn_icon");
        this.operationsPane = new HBox(btnComment, btnEdit, btnDelete);
        this.btnComment.setOnAction((action)->commentClicked());
        this.btnEdit.setOnAction((action)->editClicked());
    }

    private void commentClicked()
    {
        Parent parent = null;
        try {
            UpdateComment.commentType = "Sales Return";
            UpdateComment.orderId = this.returnId;
            UpdateComment.comment = this.comment;
            parent = FXMLLoader.load(getClass().getResource("../view/update_comment.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setStyle("-fx-padding: -20 -10 -20 -10;");
        dialogLayout.setBody(parent);
        dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        stackPane.setVisible(true);
        dialog.show();
        dialog.setOverlayClose(false);
    }

    private void editClicked()
    {
        orderId = this.returnId;
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("../view/view_sale_return_detail.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    public String getReturnDay() {
        return returnDay;
    }

    public void setReturnDay(String returnDay) {
        this.returnDay = returnDay;
    }

    public String getReturnId() {
        return returnId;
    }

    public void setReturnId(String returnId) {
        this.returnId = returnId;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getDealerContact() {
        return dealerContact;
    }

    public void setDealerContact(String dealerContact) {
        this.dealerContact = dealerContact;
    }

    public String getReturnQty() {
        return returnQty;
    }

    public void setReturnQty(String returnQty) {
        this.returnQty = returnQty;
    }

    public String getReturnPrice() {
        return returnPrice;
    }

    public void setReturnPrice(String returnPrice) {
        this.returnPrice = returnPrice;
    }

    public String getReturnUser() {
        return returnUser;
    }

    public void setReturnUser(String returnUser) {
        this.returnUser = returnUser;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnComment() {
        return btnComment;
    }

    public void setBtnComment(JFXButton btnComment) {
        this.btnComment = btnComment;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public ArrayList<ArrayList<String>> getReturnInfo(Statement stmt, Connection con, String fromDate, String toDate)
    {
        ArrayList<ArrayList<String>> returnData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        int day;
        String stDay = "";
        try {
            rs = stmt.executeQuery("SELECT `order_return`.`entry_date`, `order_return`.`return_id`, `dealer_info`.`dealer_name`, `dealer_info`.`dealer_phone`, SUM(`order_return_detail`.`prod_quant`), `order_return`.`return_total_price`, `user_info`.`user_name`, `order_return`.`comments` FROM `order_return` LEFT OUTER JOIN `dealer_info` ON `dealer_info`.`dealer_table_id` = `order_return`.`dealer_id` LEFT OUTER JOIN `order_return_detail` ON `order_return_detail`.`return_id` = `order_return`.`return_id` LEFT OUTER JOIN `user_info` ON `user_info`.`user_table_id` = `order_return`.`enter_user_id` WHERE str_to_date(`order_return`.`entry_date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') AND str_to_date(`order_return`.`entry_date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y') AND `order_return`.`status` != 'Deleted' GROUP BY `order_return`.`return_id` ORDER BY str_to_date(`order_return`.`entry_date`, '%d/%b/%Y') DESC");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Return Date
                Date objDate = fmt.parse(rs.getString(1));
                day = objDate.getDay();
                if(day == 0)
                    stDay = "Sunday";
                else if(day == 1)
                    stDay = "Monday";
                else if(day == 2)
                    stDay = "Tuesday";
                else if(day == 3)
                    stDay = "Wednesday";
                else if(day == 4)
                    stDay = "Thursday";
                else if(day == 5)
                    stDay = "Friday";
                else if(day == 6)
                    stDay = "Saturday";
                else
                    stDay = "N/A";
                temp.add(stDay); // Day
                temp.add(rs.getString(2)); // Return Id
                temp.add(rs.getString(3)); // Dealer Name
                temp.add(rs.getString(4)); // Dealer Contact
                temp.add(rs.getString(5)); // Return Qty
                temp.add(rs.getString(6)); // Return Total Price
                temp.add(rs.getString(7)); // User Name
                temp.add(rs.getString(8)); // Comment
                returnData.add(temp);
            }
        } catch (SQLException | ParseException e) {
            e.printStackTrace();
        }
        return returnData;
    }

    public ArrayList<ArrayList<String>> getReturnInfoSearch(Statement stmt, Connection con, String fromDate, String toDate, String day, String returnReason)
    {
        ArrayList<ArrayList<String>> returnData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        int intDay;
        String stDay = "";
        String query = "SELECT `order_return`.`entry_date`, `order_return`.`return_id`, `dealer_info`.`dealer_name`, `dealer_info`.`dealer_phone`, SUM(`order_return_detail`.`prod_quant`), `order_return`.`return_total_price`, `user_info`.`user_name`, `order_return`.`comments` FROM `order_return` LEFT OUTER JOIN `dealer_info` ON `dealer_info`.`dealer_table_id` = `order_return`.`dealer_id` LEFT OUTER JOIN `order_return_detail` ON `order_return_detail`.`return_id` = `order_return`.`return_id` LEFT OUTER JOIN `user_info` ON `user_info`.`user_table_id` = `order_return`.`enter_user_id` WHERE ";
        if(!fromDate.equals(""))
        {
            query += "str_to_date(`order_return`.`entry_date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') AND ";
        }
        if(!toDate.equals(""))
        {
            query += "str_to_date(`order_return`.`entry_date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y') AND ";
        }
        if(!returnReason.equals("All"))
        {
            query += "`order_return_detail`.`return_reason` = '"+returnReason+"' AND ";
        }
        query += "`order_return`.`status` != 'Deleted' GROUP BY `order_return`.`return_id` ORDER BY str_to_date(`order_return`.`entry_date`, '%d/%b/%Y') DESC";
        try {
            rs = stmt.executeQuery(query);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Return Date
                Date objDate = fmt.parse(rs.getString(1));
                intDay = objDate.getDay();
                if(intDay == 0)
                    stDay = "Sunday";
                else if(intDay == 1)
                    stDay = "Monday";
                else if(intDay == 2)
                    stDay = "Tuesday";
                else if(intDay == 3)
                    stDay = "Wednesday";
                else if(intDay == 4)
                    stDay = "Thursday";
                else if(intDay == 5)
                    stDay = "Friday";
                else if(intDay == 6)
                    stDay = "Saturday";
                else
                    stDay = "N/A";
                temp.add(stDay); // Day
                temp.add(rs.getString(2)); // Return Id
                temp.add(rs.getString(3)); // Dealer Name
                temp.add(rs.getString(4)); // Dealer Contact
                temp.add(rs.getString(5)); // Return Qty
                temp.add(rs.getString(6)); // Return Total Price
                temp.add(rs.getString(7)); // User Name
                temp.add(rs.getString(8)); // Comment
                if(!day.equals("All") && day.equals(stDay))
                {
                    returnData.add(temp);
                }
                else if(day.equals("All"))
                {
                    returnData.add(temp);
                }
            }
        } catch (SQLException | ParseException e) {
            e.printStackTrace();
        }
        return returnData;
    }
}
