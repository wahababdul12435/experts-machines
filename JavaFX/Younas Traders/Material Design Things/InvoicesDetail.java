package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTreeTableColumn;
import com.jfoenix.controls.JFXTreeTableView;
import com.jfoenix.controls.RecursiveTreeItem;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.control.cell.TextFieldTreeTableCell;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.util.Callback;
import model.*;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class InvoicesDetail implements Initializable {

    @FXML
    private Button save;

    @FXML
    private JFXTreeTableView<InvoicesDetailParamter> table_invoicesdetail;

//    @FXML
//    private TableColumn<InvoicesDetailInfo, String> sr_no;
//
//    @FXML
//    private TableColumn<InvoicesDetailInfo, String> order_id;
//
//    @FXML
//    private TableColumn<InvoicesDetailInfo, String> dealer_id;
//
//    @FXML
//    private TableColumn<InvoicesDetailInfo, String> deale_name;
//
//    @FXML
//    private TableColumn<InvoicesDetailInfo, String> items_name;
//
//    @FXML
//    private TableColumn<InvoicesDetailInfo, String> ordered_quantity;
//
//    @FXML
//    private TableColumn<InvoicesDetailInfo, String> ordered_unit;
//
//    @FXML
//    private TableColumn<InvoicesDetailInfo, String> submission_quantity;
//
//   // @FXML
//    //private TableColumn<InvoicesDetailInfo, String> submission_unit;
//
//    @FXML
//    private TableColumn<InvoicesDetailInfo, String> final_price;
//
//    @FXML
//    private TableColumn<InvoicesDetailInfo, String> date;
//
//    @FXML
//    private TableColumn<InvoicesDetailInfo, String> operations;

    @FXML
    private HBox menu_bar;

    public ObservableList<InvoicesDetailParamter> invoicesDetailList;
    InvoiceInfo objInvoiceInfo;
    public ArrayList<InvoiceInfo> invoiceData;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        objInvoiceInfo = new InvoiceInfo();
        invoiceData = new ArrayList<>();
//        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
//        order_id.setCellValueFactory(new PropertyValueFactory<>("orderId"));
//        dealer_id.setCellValueFactory(new PropertyValueFactory<>("dealerId"));
//        deale_name.setCellValueFactory(new PropertyValueFactory<>("dealerName"));
//        items_name.setCellValueFactory(new PropertyValueFactory<>("orderedItems"));
//        ordered_quantity.setCellValueFactory(new PropertyValueFactory<>("orderedQty"));
//        ordered_unit.setCellValueFactory(new PropertyValueFactory<>("orderedUnit"));
//        submission_quantity.setCellValueFactory(new PropertyValueFactory<>("submissionQty"));
//        //submission_unit.setCellValueFactory(new PropertyValueFactory<>("submissionUnit"));
//        final_price.setCellValueFactory(new PropertyValueFactory<>("finalPrice"));
//        date.setCellValueFactory(new PropertyValueFactory<>("date"));
//        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
//        table_invoicesdetail.setItems(parseUserList());

        JFXTreeTableColumn<InvoicesDetail.InvoicesDetailParamter, String> srNo = new JFXTreeTableColumn<>("Sr#");
        srNo.setPrefWidth(150);
        srNo.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<InvoicesDetail.InvoicesDetailParamter, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<InvoicesDetail.InvoicesDetailParamter, String> param) {
                return param.getValue().getValue().srNo;
            }
        });

        JFXTreeTableColumn<InvoicesDetail.InvoicesDetailParamter, String> orderId = new JFXTreeTableColumn<>("Order Id");
        orderId.setPrefWidth(150);
        orderId.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<InvoicesDetail.InvoicesDetailParamter, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<InvoicesDetail.InvoicesDetailParamter, String> param) {
                return param.getValue().getValue().orderId;
            }
        });

        JFXTreeTableColumn<InvoicesDetail.InvoicesDetailParamter, String> dealerId = new JFXTreeTableColumn<>("Dealer Id");
        dealerId.setPrefWidth(150);
        dealerId.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<InvoicesDetail.InvoicesDetailParamter, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<InvoicesDetail.InvoicesDetailParamter, String> param) {
                return param.getValue().getValue().dealerId;
            }
        });

        JFXTreeTableColumn<InvoicesDetail.InvoicesDetailParamter, String> dealerName = new JFXTreeTableColumn<>("Dealer Name");
        dealerName.setPrefWidth(150);
        dealerName.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<InvoicesDetail.InvoicesDetailParamter, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<InvoicesDetail.InvoicesDetailParamter, String> param) {
                return param.getValue().getValue().dealerName;
            }
        });

        JFXTreeTableColumn<InvoicesDetail.InvoicesDetailParamter, String> orderedItem = new JFXTreeTableColumn<>("Ordered Item");
        orderedItem.setPrefWidth(150);
        orderedItem.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<InvoicesDetail.InvoicesDetailParamter, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<InvoicesDetail.InvoicesDetailParamter, String> param) {
                return param.getValue().getValue().orderedItem;
            }
        });

        JFXTreeTableColumn<InvoicesDetail.InvoicesDetailParamter, String> orderedQty = new JFXTreeTableColumn<>("Ordered Qty");
        orderedQty.setPrefWidth(150);
        orderedQty.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<InvoicesDetail.InvoicesDetailParamter, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<InvoicesDetail.InvoicesDetailParamter, String> param) {
                return param.getValue().getValue().orderedQty;
            }
        });

        JFXTreeTableColumn<InvoicesDetail.InvoicesDetailParamter, String> orderedUnit = new JFXTreeTableColumn<>("Ordered Unit");
        orderedUnit.setPrefWidth(150);
        orderedUnit.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<InvoicesDetail.InvoicesDetailParamter, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<InvoicesDetail.InvoicesDetailParamter, String> param) {
                return param.getValue().getValue().orderedUnit;
            }
        });

        JFXTreeTableColumn<InvoicesDetail.InvoicesDetailParamter, String> submissionQty = new JFXTreeTableColumn<>("Submission Qty");
        submissionQty.setPrefWidth(150);
        submissionQty.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<InvoicesDetail.InvoicesDetailParamter, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<InvoicesDetail.InvoicesDetailParamter, String> param) {
                return param.getValue().getValue().submissionQty;
            }
        });

        JFXTreeTableColumn<InvoicesDetail.InvoicesDetailParamter, String> finalPrice = new JFXTreeTableColumn<>("Final Price");
        finalPrice.setPrefWidth(150);
        finalPrice.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<InvoicesDetail.InvoicesDetailParamter, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<InvoicesDetail.InvoicesDetailParamter, String> param) {
                return param.getValue().getValue().finalPrice;
            }
        });

        JFXTreeTableColumn<InvoicesDetail.InvoicesDetailParamter, String> date = new JFXTreeTableColumn<>("Date");
        date.setPrefWidth(150);
        date.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<InvoicesDetail.InvoicesDetailParamter, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<InvoicesDetail.InvoicesDetailParamter, String> param) {
                return param.getValue().getValue().date;
            }
        });

        JFXTreeTableColumn<InvoicesDetailParamter, String> settingsColumn = new JFXTreeTableColumn<>("Others");
        settingsColumn.setPrefWidth(150);
        Callback<TreeTableColumn<InvoicesDetailParamter, String>, TreeTableCell<InvoicesDetailParamter, String>> cellFactory
                = //
                new Callback<TreeTableColumn<InvoicesDetailParamter, String>, TreeTableCell<InvoicesDetailParamter, String>>() {
                    @Override
                    public TreeTableCell call(final TreeTableColumn<InvoicesDetailParamter, String> param) {
                        final TreeTableCell<InvoicesDetailParamter, String> cell = new TreeTableCell<InvoicesDetailParamter, String>() {

                            final JFXButton btn = new JFXButton("Just Do it");

                            @Override
                            public void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                if (empty) {
//                                    setGraphic(null);
                                    setText(null);
                                } else {
                                    btn.setButtonType(JFXButton.ButtonType.RAISED);
                                    btn.setOnAction(event -> {
                                        //Button Action here
                                    });
                                    setGraphic(btn);
                                    setText(null);
                                }
                            }
                        };
                        return cell;
                    }
                };

        settingsColumn.setCellFactory(cellFactory);

//        JFXTreeTableColumn<InvoicesDetail.InvoicesDetailParamter, String> btnValue = new JFXTreeTableColumn<>("Operation");
//        btnValue.setPrefWidth(150);
//        btnValue.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<InvoicesDetail.InvoicesDetailParamter, String>, ObservableValue<String>>() {
//            @Override
//            public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<InvoicesDetail.InvoicesDetailParamter, String> param) {
//                return param.getValue().getValue().btnValue;
//            }
//        });

//        JFXTreeTableColumn<InvoicesDetail.InvoicesDetailParamter, String> operationsPane = new JFXTreeTableColumn<>("Operation");
//        operationsPane.setPrefWidth(150);
//        operationsPane.setCellFactory(new Callback<TreeTableColumn<InvoicesDetail.InvoicesDetailParamter, String>, TreeTableCell<InvoicesDetailParamter, String>>() {
//            @Override
//            public TreeTableCell<InvoicesDetailParamter, String> call(TreeTableColumn<InvoicesDetailParamter, String> param) {
//                return new TextFieldTreeTableCell<>();
//            }
//        });
//
//        operationsPane.setCellFactory(TextFieldTreeTableCell.forTreeTableColumn());

        parseUserList();

        final TreeItem<InvoicesDetail.InvoicesDetailParamter> root = new RecursiveTreeItem<InvoicesDetailParamter>(invoicesDetailList, RecursiveTreeObject::getChildren);
        table_invoicesdetail.getColumns().setAll(srNo, orderId, dealerId, dealerName, orderedItem, orderedQty, orderedUnit, submissionQty, finalPrice, date, settingsColumn);
        table_invoicesdetail.setRoot(root);
        table_invoicesdetail.setShowRoot(false);
    }

    private ObservableList<InvoicesDetailParamter> parseUserList(){
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        InvoicesDetailInfo objInvoicesDetailInfo = new InvoicesDetailInfo();
        invoicesDetailList = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> invoicesData = objInvoicesDetailInfo.getInvoicesDetail(objStmt, objCon, null, null, null, null);
        for (int i = 0; i < invoicesData.size(); i++)
        {
            invoicesDetailList.add(new InvoicesDetailParamter(String.valueOf(i+1), invoicesData.get(i).get(0), invoicesData.get(i).get(1), invoicesData.get(i).get(2), invoicesData.get(i).get(3), invoicesData.get(i).get(4), invoicesData.get(i).get(5), invoicesData.get(i).get(6), invoicesData.get(i).get(8), invoicesData.get(i).get(9), "Cancel"));
        }
        return invoicesDetailList;
    }

    @FXML
    public void generateInvoice() throws JRException {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        ArrayList<ArrayList<String[]>> getInvoiceDetail = objInvoiceInfo.getInvoiceInfo(objStmt, objCon);
        InvoicedItemsInfo objItemsInfos;
        List<InvoicedItemsInfo> itemsList;
        for (int i = 0; i < getInvoiceDetail.size(); i++)
        {
            String[] itemsNo = new String[getInvoiceDetail.get(i).get(5).length];
            itemsNo = getInvoiceDetail.get(i).get(5);
            String[] itemsName = new String[getInvoiceDetail.get(i).get(6).length];
            itemsName = getInvoiceDetail.get(i).get(6);
            String[] quantity = new String[getInvoiceDetail.get(i).get(7).length];
            quantity = getInvoiceDetail.get(i).get(7);
            String[] unit = new String[getInvoiceDetail.get(i).get(8).length];
            unit = getInvoiceDetail.get(i).get(8);
            itemsList = new ArrayList<>();
            for(int j=0; j<itemsNo.length; j++)
            {
                objItemsInfos = new InvoicedItemsInfo();
                objItemsInfos.setItemNo(itemsNo[j]);
                objItemsInfos.setItemName(itemsName[j]);
                objItemsInfos.setQuantity(quantity[j]);
                objItemsInfos.setUnit(unit[j]);
                itemsList.add(objItemsInfos);
            }
            invoiceData.add(new InvoiceInfo(String.join(",",getInvoiceDetail.get(i).get(0)), String.join(",",getInvoiceDetail.get(i).get(1)), String.join(",",getInvoiceDetail.get(i).get(2)), String.join(",",getInvoiceDetail.get(i).get(3)), String.join(",",getInvoiceDetail.get(i).get(4)), itemsList, String.join(",",getInvoiceDetail.get(i).get(9)), String.join(",",getInvoiceDetail.get(i).get(10)), String.join(",",getInvoiceDetail.get(i).get(11)), String.join(",",getInvoiceDetail.get(i).get(12))));
        }
        InputStream objIO = ViewPendingOrders.class.getResourceAsStream("/reports/Invoice.jrxml");
        JasperReport objReport = JasperCompileManager.compileReport(objIO);
        JasperPrint objPrint = JasperFillManager.fillReport(objReport, null, new JRBeanCollectionDataSource(invoiceData));
        JasperViewer.viewReport(objPrint, false);
    }

    public void viewHome() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/home.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    public void viewPendings() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_pending_orders.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    public void SaleInvoices() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/sale_invoice.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    public void addCompany() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/enter_company.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewCompanies() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_company.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    void addArea() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/enter_area.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewAreas() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_area.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewSubAreas() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_subarea.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    void addDealer() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/enter_dealer.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewDealers() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_dealers.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    void addSalesman() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/enter_salesman.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewSalesman() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_salesman.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    void addSupplier() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/enter_supplier.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewSuppliers() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_suppliers.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    void addProduct() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/enter_new_product.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewProducts() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/view_products.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewSystemAccounts() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/system_accounts.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewDrawingSummary() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../view/drawing_summary.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    class InvoicesDetailParamter extends RecursiveTreeObject<InvoicesDetail.InvoicesDetailParamter> {

        StringProperty srNo;
        StringProperty orderId;
        StringProperty dealerId;
        StringProperty dealerName;
        StringProperty orderedItem;
        StringProperty orderedQty;
        StringProperty orderedUnit;
        StringProperty submissionQty;
        StringProperty finalPrice;
        StringProperty date;
        StringProperty btnValue;
        Button btnCancel;
        HBox operationsPane;

        public InvoicesDetailParamter(String srNo, String orderId, String dealerId, String dealerName, String orderedItem, String orderedQty, String orderedUnit, String submissionQty, String finalPrice, String date, String btnValue) {
            this.srNo = new SimpleStringProperty(srNo);
            this.orderId = new SimpleStringProperty(orderId);
            this.dealerId = new SimpleStringProperty(dealerId);
            this.dealerName = new SimpleStringProperty(dealerName);
            this.orderedItem = new SimpleStringProperty(orderedItem);
            this.orderedQty = new SimpleStringProperty(orderedQty);
            this.orderedUnit = new SimpleStringProperty(orderedUnit);
            this.submissionQty = new SimpleStringProperty(submissionQty);
            this.finalPrice = new SimpleStringProperty(finalPrice);
            this.date = new SimpleStringProperty(date);
            this.btnValue = new SimpleStringProperty(btnValue);

            this.btnCancel = new Button(btnValue);
            this.btnCancel.setOnAction(event -> markCancel());
            this.operationsPane = new HBox(this.btnCancel);
        }

        public void markCancel()
        {
            MysqlCon objMysqlCon = new MysqlCon();
            Statement objStmt = objMysqlCon.stmt;
            Connection objCon = objMysqlCon.con;
            try {
                objStmt.executeUpdate("UPDATE `order_info` SET `status`= 'Cancelled' WHERE `order_id` = '"+this.orderId+"'");
                objCon.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

            Parent root = null;
            try {
                root = FXMLLoader.load(getClass().getResource("../view/invoices_detail.fxml"));
            } catch (IOException e) {
                e.printStackTrace();
            }
            Stage objstage = (Stage) operationsPane.getScene().getWindow();
            Scene objscene = new Scene(root);
            objstage.setScene(objscene);
        }
    }
}