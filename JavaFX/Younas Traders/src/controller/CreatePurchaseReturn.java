package controller;

import com.jfoenix.controls.*;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.util.StringConverter;
import model.*;
import org.controlsfx.control.textfield.TextFields;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

public class CreatePurchaseReturn implements Initializable {

    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private JFXComboBox<String> txt_company_name;

    @FXML
    private JFXTextField txt_product_name;

    @FXML
    private JFXComboBox<String> txt_product_batch;

    @FXML
    private JFXTextField txt_product_quantity;

    @FXML
    private JFXTextField txt_return_price;

    @FXML
    private JFXComboBox<String> txt_return_reason;

    @FXML
    private JFXButton btn_cancel_edit;

    @FXML
    private JFXButton btn_update_edit;

    @FXML
    private JFXButton btn_add;

    @FXML
    private HBox summary_hbox1;

    @FXML
    private Label lbl_items_returned;

    @FXML
    private Label lbl_return_price;

    @FXML
    private TableView<CreatePurchaseReturnInfo> table_purchasereturndetaillog;

    @FXML
    private TableColumn<CreatePurchaseReturnInfo, String> sr_no;

    @FXML
    private TableColumn<CreatePurchaseReturnInfo, String> product_name;

    @FXML
    private TableColumn<CreatePurchaseReturnInfo, String> batch_no;

    @FXML
    private TableColumn<CreatePurchaseReturnInfo, String> return_quantity;

    @FXML
    private TableColumn<CreatePurchaseReturnInfo, String> return_price;

    @FXML
    private TableColumn<CreatePurchaseReturnInfo, String> return_reason;

    @FXML
    private TableColumn<CreatePurchaseReturnInfo, String> operations;

    @FXML
    private JFXButton cancelBtn;

    @FXML
    private JFXButton saveBtn;

    @FXML
    private JFXButton btn_print_return;

    @FXML
    private StackPane stackPane;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    @FXML
    private BorderPane brdrpane_navfinances;

    @FXML
    private BorderPane menu_bar;

    private static ObservableList<CreatePurchaseReturnInfo> returnDetails;
    private static CreatePurchaseReturnInfo objCreatePurchaseReturnInfo;
    private ProductInfo objProductInfo;
    private CompanyInfo objCompanyInfo;
    static ArrayList<ArrayList<String>> returnData;
    ArrayList<String> invoicePrintData;
    public static ArrayList<ArrayList<String>> completeCompaniesData;
    public static ArrayList<ArrayList<String>> completeProductData;
    public static ArrayList<ArrayList<String>> productsCompaniesDetail;
    public static ArrayList<ArrayList<String>> completeBatchData;
    public static ArrayList<ArrayList<String>> bonusDetail;
    public static ArrayList<ArrayList<String>> discountDetail;
    static ArrayList<String> selectedBatchIds;
    static ArrayList<String> selectedBatchNos;
    static ArrayList<String> selectedBatchQty;
    private static String selectedBatch;
    //    private static String selectedBatchQty;
    public static String selectedProductId;
    public static String selectedProductPrice;
    ArrayList<String> productNames;
    ArrayList<String> companyNames;
    ArrayList<String> productQty;
    String selectedCompanyId;

    public static float itemsReturned = 0;
    public static float returnPrice = 0;

    MysqlCon objMysqlCon1;
    MysqlCon objMysqlCon2;
    MysqlCon objMysqlCon3;
    static Statement objStmt1;
    Statement objStmt2;
    Statement objStmt3;
    Connection objCon;
    public ArrayList<InvoiceInfo> invoiceData;

    public static Button btnView;
    public static int srNo;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnPurchase.setStyle("-fx-background-color: #47ab1e");
        objMysqlCon1 = new MysqlCon();
        objMysqlCon2 = new MysqlCon();
        objMysqlCon3 = new MysqlCon();
        objStmt1 = objMysqlCon1.stmt;
        objStmt2 = objMysqlCon2.stmt;
        objStmt3 = objMysqlCon3.stmt;
        objCon = objMysqlCon1.con;
        objProductInfo = new ProductInfo();
        objCompanyInfo = new CompanyInfo();
        returnData = new ArrayList<>();

        CreatePurchaseReturnInfo.productIdArr = new ArrayList<>();
        CreatePurchaseReturnInfo.productNameArr = new ArrayList<>();
        CreatePurchaseReturnInfo.batchNoArr = new ArrayList<>();
        CreatePurchaseReturnInfo.returnQuantityArr = new ArrayList<>();
        CreatePurchaseReturnInfo.returnPriceArr = new ArrayList<>();
        CreatePurchaseReturnInfo.returnReasonArr = new ArrayList<>();

        objCreatePurchaseReturnInfo = new CreatePurchaseReturnInfo();

        invoiceData = new ArrayList<>();

        CreatePurchaseReturnInfo.txtProductName = txt_product_name;
        CreatePurchaseReturnInfo.txtBatchNo = txt_product_batch;
        CreatePurchaseReturnInfo.txtReturnQuantity = txt_product_quantity;
        CreatePurchaseReturnInfo.txtReturnPrice = txt_return_price;
        CreatePurchaseReturnInfo.txtReturnReason = txt_return_reason;
        CreatePurchaseReturnInfo.btnAdd = btn_add;
        CreatePurchaseReturnInfo.btnCancel = btn_cancel_edit;
        CreatePurchaseReturnInfo.btnUpdate = btn_update_edit;
        CreatePurchaseReturnInfo.lbl_items_returned = lbl_items_returned;
        CreatePurchaseReturnInfo.lbl_return_price = lbl_return_price;

        completeCompaniesData = objCompanyInfo.getCompanyNames();
        productsCompaniesDetail = objProductInfo.getCompanyProductsData(objStmt1, objCon);
        completeProductData = objProductInfo.getProductsDetail(objStmt1, objCon);
        companyNames = GlobalVariables.getArrayColumn(completeCompaniesData, 1);
        productNames = GlobalVariables.getArrayColumn(completeProductData, 1);
        productQty = GlobalVariables.getArrayColumn(completeProductData, 2);
        TextFields.bindAutoCompletion(txt_product_name, productNames);
        completeBatchData = objProductInfo.getAllBatchesDetail(objStmt1, objCon);
        txt_company_name.getItems().addAll(companyNames);

        txt_product_name.focusedProperty().addListener(new ChangeListener<Boolean>()
        {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue)
            {
                if (newPropertyValue)
                {
//                    System.out.println("Textfield on focus");
                }
                else
                {
                    String productName = txt_product_name.getText();
                    StringBuffer PN = new StringBuffer(productName);
                    int chkBit = 0;
                    for(int q=1;q<PN.length();q++)
                    {
                        char mmn =  PN.charAt(PN.length()-1);
                        if(mmn!='(')
                        {
                            if(chkBit==0)
                            {
                                PN.deleteCharAt(PN.length()-1);
                            }
                        }
                        else
                        {
                            chkBit=1;
                        }

                    }
                    if(PN.charAt(PN.length()-1)=='(')
                    {
                        PN.deleteCharAt(PN.length()-1);
                    }
                    if(PN.charAt(PN.length()-1)==' ')
                    {
                        PN.deleteCharAt(PN.length()-1);
                    }
                    String onlyProdName = PN.toString();
                    selectedProductId = getProductId(completeProductData, onlyProdName);
                    String quant = txt_product_quantity.getText();
                    if(quant == null || quant.equals(""))
                    {
                        quant = "0";
                    }
                    txt_product_batch.getItems().clear();
                    txt_product_batch.getItems().addAll(getProductsBatch(completeBatchData, selectedProductId));
                    txt_return_price.setText(String.valueOf(Float.parseFloat(GlobalVariables.getDataFrom2D(completeProductData, selectedProductId, 0, 3)) * Integer.parseInt(quant)));
//                    txt_product_batch.setValue(selectedBatch);
                }
            }
        });

        txt_product_quantity.textProperty().addListener((observable, oldValue, newValue) -> {
            txt_return_price.setText(computeReturnPrice());
        });

        itemsReturned = 0;
        returnPrice = 0;

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
//        product_id.setCellValueFactory(new PropertyValueFactory<>("productId"));
        product_name.setCellValueFactory(new PropertyValueFactory<>("productName"));
        batch_no.setCellValueFactory(new PropertyValueFactory<>("batchNo"));
        return_quantity.setCellValueFactory(new PropertyValueFactory<>("returnQuantity"));
        return_price.setCellValueFactory(new PropertyValueFactory<>("returnPrice"));
        return_reason.setCellValueFactory(new PropertyValueFactory<>("returnReason"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_purchasereturndetaillog.setItems(parseUserList());
        CreatePurchaseReturnInfo.table_purchasereturndetaillog = table_purchasereturndetaillog;
    }

    public static ObservableList<CreatePurchaseReturnInfo> parseUserList() {
        returnDetails = FXCollections.observableArrayList();
        returnData = objCreatePurchaseReturnInfo.loadReturnTable();
        itemsReturned = 0;
        returnPrice = 0;
        for (int i = 0; i < returnData.size(); i++)
        {
            itemsReturned += (returnData.get(i).get(2) != null && !returnData.get(i).get(2).equals("-")) ? Integer.parseInt(returnData.get(i).get(2)) : 0;
            returnPrice += (returnData.get(i).get(3) != null && !returnData.get(i).get(3).equals("-")) ? Float.parseFloat(returnData.get(i).get(3)) : 0;
            returnDetails.add(new CreatePurchaseReturnInfo(String.valueOf(i+1), returnData.get(i).get(0), returnData.get(i).get(1), String.format("%,.0f", Float.parseFloat(returnData.get(i).get(2))), String.format("%,.2f", Float.parseFloat(returnData.get(i).get(3))), returnData.get(i).get(4)));
        }
        CreatePurchaseReturnInfo.lbl_items_returned.setText("Items Returned\n"+String.format("%,.0f", itemsReturned));
        CreatePurchaseReturnInfo.lbl_return_price.setText("Return Price\n"+"Rs."+String.format("%,.2f", returnPrice)+"/-");
        return returnDetails;
    }

    @FXML
    void companyChange(ActionEvent event) {
        selectedCompanyId = GlobalVariables.getIdFrom2D(completeCompaniesData, txt_company_name.getValue(), 1);
        completeProductData = objProductInfo.getCompanyProductsDetail(objStmt1, objCon, selectedCompanyId);
        productNames = GlobalVariables.getArrayColumn(completeProductData, 1);
        productQty = GlobalVariables.getArrayColumn(completeProductData, 2);
        TextFields.bindAutoCompletion(txt_product_name, productNames);
    }

    @FXML
    void batchChange(ActionEvent event) {
        getBatchPrice(completeBatchData, txt_product_batch.getValue(), selectedProductId);
        txt_return_price.setText(computeReturnPrice());
    }

    @FXML
    void btnAdd(ActionEvent event) {
        String productName = txt_product_name.getText();
        String batchNo = txt_product_batch.getValue();
        String quantity = txt_product_quantity.getText();
        String stReturnPrice = txt_return_price.getText();
        String returnReason = txt_return_reason.getValue();
        CreatePurchaseReturnInfo.productIdArr.add(selectedProductId);
        CreatePurchaseReturnInfo.productNameArr.add(productName);
        CreatePurchaseReturnInfo.batchNoArr.add(batchNo);
        CreatePurchaseReturnInfo.returnQuantityArr.add(quantity);
        CreatePurchaseReturnInfo.returnPriceArr.add(stReturnPrice);
        CreatePurchaseReturnInfo.returnReasonArr.add(returnReason);
        table_purchasereturndetaillog.setItems(parseUserList());
        CreatePurchaseReturnInfo.table_purchasereturndetaillog = table_purchasereturndetaillog;

        txt_product_name.clear();
        txt_product_batch.getItems().clear();
        txt_product_batch.setValue(null);
        txt_product_quantity.clear();
        txt_return_price.clear();
        txt_return_reason.setValue(null);
    }

    @FXML
    void btnCancelEdit(ActionEvent event) {
        txt_product_name.clear();
        txt_product_batch.getItems().clear();
        txt_product_batch.setValue(null);
        txt_product_quantity.clear();
        txt_return_price.clear();
        txt_return_reason.setValue(null);

        btn_add.setVisible(true);
        btn_cancel_edit.setVisible(false);
        btn_update_edit.setVisible(false);
    }

    @FXML
    void btnUpdateEdit(ActionEvent event) {
        String productName = txt_product_name.getText();
        String batchNo = txt_product_batch.getValue();
        String quantity = txt_product_quantity.getText();
        String stReturnPrice = txt_return_price.getText();
        String returnReason = txt_return_reason.getValue();
        CreatePurchaseReturnInfo.productIdArr.set(srNo, selectedProductId);
        CreatePurchaseReturnInfo.productNameArr.set(srNo, productName);
        CreatePurchaseReturnInfo.batchNoArr.set(srNo, batchNo);
        CreatePurchaseReturnInfo.returnQuantityArr.set(srNo, quantity);
        CreatePurchaseReturnInfo.returnPriceArr.set(srNo, stReturnPrice);
        CreatePurchaseReturnInfo.returnReasonArr.set(srNo, returnReason);
        table_purchasereturndetaillog.setItems(parseUserList());
        CreatePurchaseReturnInfo.table_purchasereturndetaillog = table_purchasereturndetaillog;

        txt_product_name.clear();
        txt_product_batch.getItems().clear();
        txt_product_batch.setValue(null);
        txt_product_quantity.clear();
        txt_return_price.clear();
        txt_return_reason.setValue(null);

        btn_add.setVisible(true);
        btn_cancel_edit.setVisible(false);
        btn_update_edit.setVisible(false);
    }

    @FXML
    void cancelClicked(ActionEvent event) {

    }

    @FXML
    void follow(MouseEvent event) {

    }

    @FXML
    void printReturn(ActionEvent event) {

    }

    @FXML
    void productChange(KeyEvent event) {

    }

    @FXML
    void saveClicked(ActionEvent event) {
        objCreatePurchaseReturnInfo.insertBasic(objStmt1, selectedCompanyId, String.valueOf(returnPrice));
        objCreatePurchaseReturnInfo.insertReturn(objStmt1);

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/view_purchase_return.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    public static String getBatchPrice(ArrayList<ArrayList<String>> batchData, String batchNo, String productId)
    {
        for(ArrayList<String> row: batchData) {
            if(row.get(1).equals(batchNo) && row.get(2).equals(productId))
            {
                selectedProductPrice = row.get(3);
//                addedRetailPrice = row.get(2);
                return row.get(0);
            }
        }
        return null;
    }

    public static String getProductId(ArrayList<ArrayList<String>> productsData, String productName)
    {
        for(ArrayList<String> row: productsData) {
            if(row.get(1).equals(productName))
            {
                selectedProductPrice = row.get(3);
//                addedRetailPrice = row.get(2);
                return row.get(0);
            }
        }
        return null;
    }

    public static ArrayList<String> getProductsBatch(ArrayList<ArrayList<String>> batchData, String productId)
    {
        ArrayList<String> columnList = new ArrayList<>();
//        ArrayList<String> stExpiryDate = new ArrayList<>();
//        ArrayList<Date> expiryDate = new ArrayList<>();
//        ArrayList<String> stEntryDate = new ArrayList<>();
//        ArrayList<Date> entryDate = new ArrayList<>();
//        selectedBatchIds = new ArrayList<>();
//        selectedBatchNos = new ArrayList<>();
//        selectedBatchQty = new ArrayList<>();
//        Date entrySelectedDate;
//        Date expirySelectedDate;
//        int selectedIndex = 0;
//        boolean expirySelection = false;
        // Batch Id          0
        // Batch No          1
        // Product Id        2
        // Batch Quantity    3
        // Batch Expiry      4
        // Entry Date        5
        if(batchData!=null)
        {
            for(ArrayList<String> row: batchData) {
                if(row.get(2).equals(productId))
                {
//                    selectedBatchIds.add(row.get(0));
//                    selectedBatchNos.add(row.get(1));
//                    selectedBatchQty.add(row.get(3));
                    columnList.add(row.get(1));
//                    stExpiryDate.add(row.get(4));
//                    stEntryDate.add(row.get(5));
//                    try {
//                        expiryDate.add(fmt.parse(row.get(4)));
//                        entryDate.add(fmt.parse(row.get(5)));
//                    } catch (ParseException e) {
//                        e.printStackTrace();
//                    }

//                    if(batchPolicy.equals("Entry LIFO"))
//                    {
//                        // Entry select max date
//                        entrySelectedDate = Collections.max(entryDate);
//                        expirySelection = false;
//                        selectedIndex = entryDate.indexOf(entrySelectedDate);
//                    }
//                    else if(batchPolicy.equals("Entry FIFO"))
//                    {
//                        // Entry select min date
//                        entrySelectedDate = Collections.min(entryDate);
//                        expirySelection = false;
//                        selectedIndex = entryDate.indexOf(entrySelectedDate);
//                    }
//                    else if(batchPolicy.equals("Expiry LIFO"))
//                    {
//                        // Expiry select max date
//                        expirySelectedDate = Collections.max(expiryDate);
//                        expirySelection = true;
//                        selectedIndex = entryDate.indexOf(expirySelectedDate);
//                    }
//                    else if(batchPolicy.equals("Expiry FIFO"))
//                    {
//                        // Expiry select min date
//                        expirySelectedDate = Collections.min(expiryDate);
//                        expirySelection = true;
//                        selectedIndex = entryDate.indexOf(expirySelectedDate);
//                    }
//                    selectedBatch = columnList.get(selectedIndex);
                }
            }
        }
        return columnList;
    }

    private String computeReturnPrice() {
        String quant = txt_product_quantity.getText();
        if(quant == null || quant.equals(""))
        {
            quant = "0";
        }

        return String.valueOf(Float.parseFloat(selectedProductPrice) * Integer.parseInt(quant));
    }

}
