package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.GlobalVariables;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class NavReports implements Initializable {

    @FXML
    private AnchorPane rootpane;

    @FXML
    private VBox vbox_style;

    @FXML
    private Button btn_report_dashboard;

    @FXML
    private MenuButton btn_dealer_report;

    @FXML
    private MenuButton btn_company_report;

    @FXML
    private MenuButton btn_product_report;

    @FXML
    private MenuButton btn_user_report;

    @FXML
    private MenuButton btn_sales_report;

    @FXML
    private MenuButton btn_purchase_report;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        DashboardReport.btnView = btn_report_dashboard;
        CompanyReport.btnView = btn_company_report;
        DealerReport.btnView = btn_dealer_report;
        DealerGeneralReport.btnView = btn_dealer_report;
        DealerDayReport.btnView = btn_dealer_report;
        DealerFinanceReport.btnView = btn_dealer_report;
        DealerFinanceDetailReport.btnView = btn_dealer_report;
        DealerReportDetail.btnView = btn_dealer_report;
        CompanyGeneralReport.btnView = btn_company_report;
        CompanyDayReport.btnView = btn_company_report;
        CompanyFinanceReport.btnView = btn_company_report;
        CompanyFinanceDetailReport.btnView = btn_company_report;
        CompanyReportDetail.btnView = btn_company_report;
        ProductReport.btnView = btn_product_report;
        ProductReportDetail.btnView = btn_product_report;
        UserReport.btnView = btn_user_report;
        UserReportDetail.btnView = btn_user_report;
        SaleReport.btnView = btn_sales_report;
        PurchaseReport.btnView = btn_purchase_report;
    }

    @FXML
    void companyReport(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/company_general_report.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void companyDayReport(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/company_day_report.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void companyFinanceReport(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/company_finance_report.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void dealerReport(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/dealer_general_report.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
    @FXML
    void areaWiseReport(ActionEvent event) throws IOException {

        Parent root = FXMLLoader.load(getClass().getResource("/view/AreawisePopUp.fxml"));
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.setTitle("AreaWise Sale Parameters");
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.showAndWait();
        //stage.show();

        //stage.setAlwaysOnTop(true);

        //stage.showAndWait();

    }

    @FXML
    void dealerDayReport(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/dealer_day_report.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void dealerFinanceReport(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/dealer_finance_report.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void productReport(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/product_report.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void purchaseReport(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/purchase_report.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void reportsDashboard(ActionEvent event) {

    }

    @FXML
    void salesReport(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/sale_report.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void userReport(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/user_report.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }


}