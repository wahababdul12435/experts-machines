package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextFormatter;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import model.GlobalVariables;
import model.SetupLicenseActivationInfo;
import model.SetupOwnerDealersInfo;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;
import java.io.*;
import java.net.URL;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;

public class SetupLicenseActivation implements Initializable{

    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private ProgressIndicator progress;

    @FXML
    private JFXTextField txt_1;

    @FXML
    private JFXTextField txt_2;

    @FXML
    private JFXTextField txt_3;

    @FXML
    private JFXTextField txt_4;

    @FXML
    private JFXTextField txt_5;

    @FXML
    private JFXButton btn_verify_license;

    @FXML
    private BorderPane menu_bar;

    private SetupLicenseActivationInfo objSetupLicenseActivationInfo;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        txt_1.setTextFormatter(new TextFormatter<>((change) -> {
            change.setText(change.getText().toUpperCase());
            return change;
        }));
        txt_2.setTextFormatter(new TextFormatter<>((change) -> {
            change.setText(change.getText().toUpperCase());
            return change;
        }));
        txt_3.setTextFormatter(new TextFormatter<>((change) -> {
            change.setText(change.getText().toUpperCase());
            return change;
        }));
        txt_4.setTextFormatter(new TextFormatter<>((change) -> {
            change.setText(change.getText().toUpperCase());
            return change;
        }));
        txt_5.setTextFormatter(new TextFormatter<>((change) -> {
            change.setText(change.getText().toUpperCase());
            return change;
        }));
    }

    @FXML
    void charactersCount1(KeyEvent event) {
        if(event.getCode() != KeyCode.BACK_SPACE && event.getCode() != KeyCode.DELETE)
        {
            int count = txt_1.getText().length();
            if(count >= 5)
            {
                txt_2.requestFocus();
            }
        }
    }

    @FXML
    void charactersCount2(KeyEvent event) {
        if(event.getCode() != KeyCode.BACK_SPACE && event.getCode() != KeyCode.DELETE)
        {
            int count = txt_2.getText().length();
            if(count >= 5)
            {
                txt_3.requestFocus();
            }
        }
    }

    @FXML
    void charactersCount3(KeyEvent event) {
        if(event.getCode() != KeyCode.BACK_SPACE && event.getCode() != KeyCode.DELETE)
        {
            int count = txt_3.getText().length();
            if(count >= 5)
            {
                txt_4.requestFocus();
            }
        }
    }

    @FXML
    void charactersCount4(KeyEvent event) {
        if(event.getCode() != KeyCode.BACK_SPACE && event.getCode() != KeyCode.DELETE)
        {
            int count = txt_4.getText().length();
            if(count >= 5)
            {
                txt_5.requestFocus();
            }
        }
    }

    @FXML
    void charactersCount5(KeyEvent event) {
        if(event.getCode() != KeyCode.BACK_SPACE && event.getCode() != KeyCode.DELETE)
        {
            int count = txt_5.getText().length();
            if(count >= 5)
            {
                btn_verify_license.requestFocus();
            }
        }
    }

    @FXML
    void verifyLicense(ActionEvent event) throws IOException {
        progress.setVisible(true);

//        Process process = Runtime.getRuntime().exec(directoryCommand);
//        process = Runtime.getRuntime().exec("attrib +h +s +r the-file-name.txt");
//        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
//        String line = "";
//        while ((line = reader.readLine()) != null) {
//            System.out.println(line);
//        }


        Thread thread = new Thread() {
            public void run(){
                String response;
                String txt1 = txt_1.getText();
                String txt2 = txt_2.getText();
                String txt3 = txt_3.getText();
                String txt4 = txt_4.getText();
                String txt5 = txt_5.getText();
                String licenseNumber = txt1+"-"+txt2+"-"+txt3+"-"+txt4+"-"+txt5;
                objSetupLicenseActivationInfo = new SetupLicenseActivationInfo();
                response = objSetupLicenseActivationInfo.sendData(licenseNumber);

                Platform.runLater(new Runnable(){
                    @Override
                    public void run() {
                        if(!response.equals("0") && !response.equals("-1") )
                        {
                            String stStartDate = GlobalVariables.getStDate();
                            String stEndDate = "";
                            Date startDate = null;
                            try {
                                startDate = GlobalVariables.fmt.parse(stStartDate);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            Calendar cal1 = Calendar.getInstance();
                            cal1.setTime(startDate);
                            cal1.add(Calendar.DAY_OF_MONTH, 365);
                            stEndDate = GlobalVariables.fmt.format(cal1.getTime());
                            GlobalVariables.softwareLicId = response;
                            GlobalVariables.softwareLicNumber = licenseNumber;
                            GlobalVariables.softwareLicRegDate = stStartDate;
                            GlobalVariables.softwareLicExpDate = stEndDate;
                            GlobalVariables.workingDistrict = "";
                            String title = "Success";
                            String message = "License Activated";
                            GlobalVariables.showNotification(1, title, message);

//                             ------------------------------------ Setting Core File -------------------------
                            String directoryName = "secret-credentials";
                            File directory = new File(directoryName);
                            if (!directory.exists()){
                                directory.mkdir();
                            }
                            PrintWriter writer = null;
                            try {
                                writer = new PrintWriter("secret-credentials/data.txt", "UTF-8");
                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                            writer.println(GlobalVariables.softwareLicId);
                            writer.println(licenseNumber);
                            writer.println(GlobalVariables.getStDate());
                            writer.println(stEndDate);
                            writer.println("Owner Business");
                            writer.println("Nil");
                            writer.close();
                            String currentDirectory = System.getProperty("user.dir");
                            String command = "cd "+currentDirectory+" & attrib +h +r +s secret-credentials";

                            ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c", command);
                            builder.redirectErrorStream(true);
                            try {
                                Process p = builder.start();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
//                             ------------------------------------ Setting Core File -------------------------


                            SetupHeader.labelName = "Owner &\nBusiness Setup";
                            Parent root = null;
                            try {
                                root = FXMLLoader.load(getClass().getResource("/view/setup_owner_business.fxml"));
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            GlobalVariables.baseScene.setRoot(root);
                            GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
                        }
                        else if(response.equals("0"))
                        {
                            String title = "Error";
                            String message = "License Not Activated, Please try again later";
                            GlobalVariables.showNotification(0, title, message);
                        }
                        else
                        {
                            String title = "Error";
                            String message = "Incorrect License Number";
                            GlobalVariables.showNotification(-1, title, message);
                        }
                    }
                    // do your GUI stuff here
                });
            }
        };
        thread.start();
        
    }
}
