package controller;

import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.util.Duration;
import model.DistrictInfo;
import model.GlobalVariables;
import model.MysqlCon;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class EnterDistrict implements Initializable {

    @FXML
    private BorderPane menu_bar1;

    @FXML
    private BorderPane nav_setup;

    @FXML
    private JFXTextField txt_district_id;

    @FXML
    private JFXTextField txt_district_name;

    @FXML
    private Button cancelBtn;

    @FXML
    private Button saveBtn;

    @FXML
    private Button btn_add_district;

    @FXML    private Button btn_edit_cancel;
    @FXML    private Button btn_edit_update;

    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private TableView<DistrictInfo> table_add_district;

    @FXML
    private TableColumn<DistrictInfo, String> sr_no;

    @FXML
    private TableColumn<DistrictInfo, String> district_id;

    @FXML
    private TableColumn<DistrictInfo, String> district_name;

    @FXML
    private TableColumn<DistrictInfo, String> operations;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    public static ObservableList<DistrictInfo> districtDetails;
    Alert alert = new Alert(Alert.AlertType.NONE);
    DistrictInfo objDistrictInfo;
    public static int srNo;
    public static Button btnView;

    public void initialize(URL url, ResourceBundle resourceBundle) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnSetup.setStyle("-fx-background-color: #47ab1e");
        objDistrictInfo = new DistrictInfo();
        btnpane.setOnMouseEntered(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(22);
                btnpane.setMinWidth(20.0);
                btn11.setMinWidth(20.0);

            }
        });
        btnpane.setOnMouseExited(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(14);
                btnpane.setMinWidth(10.0);
                btn11.setMinWidth(10.0);
            }
        });

        inner_anchor.setLeftAnchor(inner_anchor, 280d);
        drawer.setSidePane(nav_setup);
        drawer.open();

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        district_id.setCellValueFactory(new PropertyValueFactory<>("districtId"));
        district_name.setCellValueFactory(new PropertyValueFactory<>("districtName"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        DistrictInfo.table_add_district = table_add_district;
        table_add_district.setItems(parseUserList());
        DistrictInfo.txtDistrictId = txt_district_id;
        DistrictInfo.txtDistrictName = txt_district_name;
        DistrictInfo.btnAdd = btn_add_district;
        DistrictInfo.btnCancel = btn_edit_cancel;
        DistrictInfo.btnUpdate = btn_edit_update;
    }

    public static ObservableList<DistrictInfo> parseUserList(){
        DistrictInfo objDistrictInfo = new DistrictInfo();
        districtDetails = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> districtData = objDistrictInfo.getAddedDistrict();
        for (int i = 0; i < districtData.size(); i++)
        {
            districtDetails.add(new DistrictInfo(String.valueOf(i+1), ((districtData.get(i).get(0) == null || districtData.get(i).get(0).equals("")) ? "N/A" : districtData.get(i).get(0)), districtData.get(i).get(1)));
        }
        return districtDetails;
    }

    @FXML
    void addDistrict(ActionEvent event) {
        String districtId = txt_district_id.getText();
        String districtName = txt_district_name.getText();
        String districtStatus = "Active";

        String comp = objDistrictInfo.confirmNewId(districtId);

        if(!comp.equals(districtId) || comp.equals("")) {
            MysqlCon objMysqlCon = new MysqlCon();
            Statement objStmt = objMysqlCon.stmt;
            Connection objCon = objMysqlCon.con;
            DistrictInfo.districtIdArr.add(districtId);
            DistrictInfo.districtNameArr.add(districtName);
            table_add_district.setItems(parseUserList());
            refreshControls();
        }
        else
        {
            alert.setAlertType(Alert.AlertType.ERROR);
            alert.setHeaderText("Error!");
            alert.setContentText("District ID Already Saved.");
            alert.show();
        }
    }

    @FXML
    void editCancel(ActionEvent event) {
        refreshControls();
        btn_add_district.setVisible(true);
        btn_edit_cancel.setVisible(false);
        btn_edit_update.setVisible(false);
    }

    @FXML
    void editUpdate(ActionEvent event) {
        String districtId = txt_district_id.getText();
        String districtName = txt_district_name.getText();
        String districtStatus = "Active";

        String comp = objDistrictInfo.confirmNewId(districtId);

        if(!comp.equals(districtId) || comp.equals("")) {
            MysqlCon objMysqlCon = new MysqlCon();
            Statement objStmt = objMysqlCon.stmt;
            Connection objCon = objMysqlCon.con;
            DistrictInfo.districtIdArr.set(srNo, districtId);
            DistrictInfo.districtNameArr.set(srNo, districtName);
            table_add_district.setItems(parseUserList());
            refreshControls();
        }
        else
        {
            alert.setAlertType(Alert.AlertType.ERROR);
            alert.setHeaderText("Error!");
            alert.setContentText("District ID Already Saved.");
            alert.show();
        }

        btn_add_district.setVisible(true);
        btn_edit_cancel.setVisible(false);
        btn_edit_update.setVisible(false);
    }

    @FXML
    void cancelClicked(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_district.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void follow(MouseEvent event) {
        if (drawer.isOpened()) {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(-250);
            transition.setByY(0);
            transition.setCycleCount(1);
            transition.play();
            drawer.close();
            FA_Icon.setGlyphName("ANGLE_DOUBLE_RIGHT");
            FA_Icon.setGlyphSize(14);
            //inner_anchor.setRightAnchor(inner_anchor,1d);
            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(-90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(1.1000);
            scale.setToY(1);

            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();
        }
        else {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(250);
            transition.setByY(0);
            transition.setCycleCount(1);
            FA_Icon.setGlyphName("ANGLE_DOUBLE_LEFT");
            FA_Icon.setGlyphSize(14);
            drawer.open();

            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(0.99947777);
            scale.setToY(1);

            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();
        }
    }

    @FXML
    void saveClicked(ActionEvent event) throws IOException {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        try {
            objDistrictInfo.insertDistrict(objStmt, objCon);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Parent root = FXMLLoader.load(getClass().getResource("/view/view_district.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    public void refreshControls()
    {
        this.txt_district_id.setText("");
        this.txt_district_name.setText("");
    }

}
