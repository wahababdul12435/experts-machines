package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import model.*;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class CashCollection implements Initializable {
    @FXML    private Label label_update;
    @FXML    private JFXTextField txt_dealer_id;
    @FXML    private JFXTextField txt_dealer_name;
    @FXML    private JFXTextField txt_InvoiceNum;
    @FXML    private DatePicker datePick_invoiceDate;
    @FXML    private JFXTextField txt_newPayment;
    @FXML    private JFXTextField txt_prevPayment;
    @FXML    private JFXTextField txt_totalPayment;
    @FXML    private JFXTextField txt_cashReceived;
    @FXML    private JFXButton dialog_company_close;
    @FXML    private JFXButton dialog_company_update;

    public static String dealerId ="";
    public static String invoicesNumber ="";


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        //ViewCompanyInfo.lblUpdate = label_update;
        txt_dealer_id.setText(dealerId);
        txt_InvoiceNum.setText(invoicesNumber);
        CashCollectionInfo OBJcashcollection = new CashCollectionInfo();

        int intValue_dealerID = Integer.parseInt(dealerId);
        int intValue_invoiceNumb = Integer.parseInt(invoicesNumber);


        ArrayList<String> orderdetailData = new ArrayList<>();
        MysqlCon objctsMysqlConnc = new MysqlCon();
        Statement objectStatmnt = objctsMysqlConnc.stmt;
        Connection objectConnnection = objctsMysqlConnc.con;
        orderdetailData = OBJcashcollection.get_orderDetails(objectStatmnt,objectConnnection,intValue_dealerID,intValue_invoiceNumb);

        String[] splitOrderData = orderdetailData.get(0).split("--");

        txt_dealer_name.setText(splitOrderData[0]);
        datePick_invoiceDate.setValue(GlobalVariables.LOCAL_DATE(splitOrderData[1]));
        txt_newPayment.setText(splitOrderData[2]);
        txt_prevPayment.setText(splitOrderData[3]);

        int totalPayment = Integer.parseInt(splitOrderData[2]) + Integer.parseInt(splitOrderData[3]);
        txt_totalPayment.setText(String.valueOf(totalPayment));

    }

    public void closeDialog()
    {
        CollectionSummaryInfo.dialog.close();
        CollectionSummaryInfo.stackPane.setVisible(false);
    }
    public void collectCash()
    {
        int dealers_id = Integer.parseInt(txt_dealer_id.getText());
        int orders_id = Integer.parseInt(txt_InvoiceNum.getText());
        String cashCollection_date = txt_dealer_id.getText();
        float totalPayment = Float.parseFloat(txt_totalPayment.getText());
        float cash_received = Float.parseFloat(txt_cashReceived.getText());

        MysqlCon myobject = new MysqlCon();
        Statement statmentOBJ = myobject.stmt;
        Connection CnectionOBJ = myobject.con;
        CashCollectionInfo OBJcahcollection = new CashCollectionInfo();
        OBJcahcollection.insert_CashCollected(statmentOBJ,CnectionOBJ,dealers_id ,orders_id, cashCollection_date,totalPayment,cash_received);

    }

}
