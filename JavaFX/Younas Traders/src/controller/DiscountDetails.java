package controller;

import com.jfoenix.controls.JFXTextField;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import model.*;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class DiscountDetails implements Initializable {

    @FXML    private JFXTextField txt_approvalID;
    @FXML    private JFXTextField txt_startDate;
    @FXML    private JFXTextField txt_endDate;
    @FXML    private JFXTextField txt_status;

    @FXML    private TableView table_fixedDiscPolicy;
    @FXML   private TableColumn<DiscountDetailInfo, String> serial_no;
    @FXML   private TableColumn<DiscountDetailInfo, String> dealersID;
    @FXML   private TableColumn<DiscountDetailInfo, String> dealersName;
    @FXML   private TableColumn<DiscountDetailInfo, String> companiesID;
    @FXML   private TableColumn<DiscountDetailInfo, String> companiesName;
    @FXML   private TableColumn<DiscountDetailInfo, String> discountValue;

    @FXML    private TableView table_saleDiscountPolicy;
    @FXML   private TableColumn<DiscountDetailInfo, String> sr_no;
    @FXML   private TableColumn<DiscountDetailInfo, String> companyID;
    @FXML   private TableColumn<DiscountDetailInfo, String> companyName;
    @FXML   private TableColumn<DiscountDetailInfo, String> dealerID;
    @FXML   private TableColumn<DiscountDetailInfo, String>dealerName;
    @FXML   private TableColumn<DiscountDetailInfo, String> saleAmount;
    @FXML   private TableColumn<DiscountDetailInfo, String> disc;

    private String currentDiscapprovalId;
    private ObservableList<DiscountDetailInfo> discountDetails;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        if (!ViewDiscountInfo.discountviewApprovalId.equals("")) {
            currentDiscapprovalId = ViewDiscountInfo.discountviewApprovalId;
            ViewDiscountInfo.discountviewApprovalId = "";
        }

        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        ArrayList<ArrayList<String>> discountPolicy_details;
        DiscountDetailInfo objViewDiscount = new DiscountDetailInfo();
        discountPolicy_details = objViewDiscount.getDiscountPolicydetails(objStmt,objCon,currentDiscapprovalId);
        for(int i = 0;i<discountPolicy_details.size();i++)
        {
            if(discountPolicy_details.get(i).get(5).equals("0") || discountPolicy_details.get(i).get(5).equals("null"))
            {
                table_saleDiscountPolicy.setVisible(false);
                table_fixedDiscPolicy.setVisible(true);
                sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
                dealersID.setCellValueFactory(new PropertyValueFactory<>("dealers_id"));
                dealersName.setCellValueFactory(new PropertyValueFactory<>("dealers_name"));
                companiesID.setCellValueFactory(new PropertyValueFactory<>("company_id"));
                companiesName.setCellValueFactory(new PropertyValueFactory<>("company_name"));
                discountValue.setCellValueFactory(new PropertyValueFactory<>("discount"));
                table_fixedDiscPolicy.setItems(parseUserList());
            }
            if(!discountPolicy_details.get(i).get(5).equals("0"))
            {
                table_fixedDiscPolicy.setVisible(false);
                table_saleDiscountPolicy.setVisible(true);
                sr_no.setCellValueFactory(new PropertyValueFactory<>("sr_No"));
                dealerID.setCellValueFactory(new PropertyValueFactory<>("dealers_id"));
                dealerName.setCellValueFactory(new PropertyValueFactory<>("dealers_name"));
                companyID.setCellValueFactory(new PropertyValueFactory<>("company_id"));
                companyName.setCellValueFactory(new PropertyValueFactory<>("company_name"));
                saleAmount.setCellValueFactory(new PropertyValueFactory<>("sale_amount"));
                disc.setCellValueFactory(new PropertyValueFactory<>("discount"));
                table_saleDiscountPolicy.setItems(parseUserList());
            }
        }



    }

    private ObservableList<DiscountDetailInfo> parseUserList(){
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        discountDetails = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> discount_Data;
        DiscountDetailInfo objdiscdetailInfo = new DiscountDetailInfo();

        discount_Data = objdiscdetailInfo.getDiscountPolicydetails(objStmt, objCon, currentDiscapprovalId);

        txt_approvalID.setText(discount_Data.get(0).get(0));
        txt_startDate.setText(discount_Data.get(0).get(7));
        txt_endDate.setText(discount_Data.get(0).get(8));
        txt_status.setText(discount_Data.get(0).get(9));

        txt_status.setDisable(true);
        txt_endDate.setDisable(true);
        txt_startDate.setDisable(true);
        txt_approvalID.setDisable(true);

        for (int i = 0; i < discount_Data.size(); i++)
        {
            if(discount_Data.get(i).get(5).equals("0") || discount_Data.get(i).get(5).equals("null")) {
                discountDetails.add(new DiscountDetailInfo(String.valueOf(i + 1), discount_Data.get(i).get(1), discount_Data.get(i).get(2), discount_Data.get(i).get(3), discount_Data.get(i).get(4), discount_Data.get(i).get(6), discount_Data.get(i).get(6)));
            }
            if(!discount_Data.get(i).get(5).equals("0")) {
                discountDetails.add(new DiscountDetailInfo(String.valueOf(i + 1), discount_Data.get(i).get(1), discount_Data.get(i).get(2), discount_Data.get(i).get(4), discount_Data.get(i).get(5), discount_Data.get(i).get(3), discount_Data.get(i).get(6)));
            }
        }
        return discountDetails;
    }

    @FXML
    void closeDialogue() throws IOException {
        ViewDiscountInfo.stackPane.setVisible(false);
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_discount.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

        @FXML
    void goBack(ActionEvent event)
    {

    }
}
