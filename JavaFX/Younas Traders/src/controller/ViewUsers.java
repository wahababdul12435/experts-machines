package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.*;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.util.Duration;
import model.*;
import model.ViewUsersInfo;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class ViewUsers implements Initializable {
    @FXML
    private TableView<ViewUsersInfo> table_viewusers;

    @FXML
    private TableColumn<ViewUsersInfo, String> sr_no;

    @FXML
    private TableColumn<ViewUsersInfo, String> system_user_id;

    @FXML
    private TableColumn<ViewUsersInfo, String> user_id;

    @FXML
    private TableColumn<ViewUsersInfo, String> user_name;

    @FXML
    private TableColumn<ViewUsersInfo, String> user_type;

    @FXML
    private TableColumn<ViewUsersInfo, String> user_contact;

    @FXML
    private TableColumn<ViewUsersInfo, String> user_address;

//    @FXML
//    private TableColumn<ViewUsersInfo, String> user_cnic;

    @FXML
    private TableColumn<ViewUsersInfo, String> designated_areas;

    @FXML
    private TableColumn<ViewUsersInfo, String> status;

    @FXML
    private TableColumn<ViewUsersInfo, String> operations;

    @FXML    private AnchorPane inner_anchor;
    @FXML    private JFXDrawer drawer;
    @FXML    private BorderPane nav_setup;
    @FXML    private BorderPane menu_bar1;
    @FXML    private Pane btnpane;
    @FXML    private Button btn11;
    @FXML    private FontAwesomeIconView FA_Icon;

    @FXML
    private AnchorPane filter_pane;

    @FXML
    private HBox filter_hbox;

    @FXML
    private JFXTextField txt_user_id;

    @FXML
    private JFXTextField txt_user_name;

    @FXML
    private JFXComboBox<String> txt_user_type;

    @FXML
    private JFXTextField txt_user_contact;

    @FXML
    private JFXTextField txt_user_cnic;

    @FXML
    private JFXComboBox<String> txt_user_status;
    @FXML
    private StackPane stackPane;

    @FXML
    private HBox summary_hbox;

    @FXML
    private Label lbl_total;

    @FXML
    private Label lbl_active;

    @FXML
    private Label lbl_inactive;

    @FXML
    private JFXButton btn_add;
    @FXML
    private JFXButton btn_export;

    public ObservableList<ViewUsersInfo> userDetail;
    PendingOrders objPendingOrders;
    MysqlCon objMysqlCon;
    Statement objStmt;
    Connection objCon;
    private int summaryTotal;
    private int summaryActive;
    private int summaryInActive;

    public static String txtUserId = "";
    public static String txtUserName = "";
    public static String txtUserType = "All";
    public static String txtUserContact = "";
    public static String txtUserCnic = "";
    public static String txtUserStatus = "All";
    public static boolean filter;

    public static Button btnView;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnSetup.setStyle("-fx-background-color: #47ab1e");
        objMysqlCon = new MysqlCon();
        objStmt = objMysqlCon.stmt;
        objCon = objMysqlCon.con;
//        objPendingOrders = new PendingOrders();
//        unSeenOrders = objPendingOrders.getUnSeenOrders(objStmt, objCon);
//        pending_orders.setText("Pending Orders ("+unSeenOrders+")");
        ViewUsersInfo.stackPane = stackPane;

        if(!GlobalVariables.userType.equals("Admin"))
        {
            if(!GlobalVariables.rightCreate)
            {
                btn_add.setVisible(false);
                AnchorPane.setRightAnchor(btn_export, 15.0);
            }
        }

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        system_user_id.setCellValueFactory(new PropertyValueFactory<>("userTableId"));
        user_id.setCellValueFactory(new PropertyValueFactory<>("userId"));
        user_name.setCellValueFactory(new PropertyValueFactory<>("userName"));
        user_type.setCellValueFactory(new PropertyValueFactory<>("userType"));
        user_contact.setCellValueFactory(new PropertyValueFactory<>("userContact"));
        user_address.setCellValueFactory(new PropertyValueFactory<>("userAddress"));
//        user_cnic.setCellValueFactory(new PropertyValueFactory<>("cnic"));
        designated_areas.setCellValueFactory(new PropertyValueFactory<>("designatedAreas"));
        status.setCellValueFactory(new PropertyValueFactory<>("userStatus"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_viewusers.setItems(parseUserList());
        lbl_total.setText("Users\n"+String.format("%,.0f", (float)summaryTotal));
        lbl_active.setText("Active\n"+String.format("%,.0f", (float)summaryActive));
        lbl_inactive.setText("In Active\n"+String.format("%,.0f", (float)summaryInActive));
    }

    private ObservableList<ViewUsersInfo> parseUserList(){
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        ViewUsersInfo objViewUsersInfo = new ViewUsersInfo();
        userDetail = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> userData;
        if(filter)
        {
            userData  = objViewUsersInfo.getUserSearch(objStmt, objCon, txtUserId, txtUserName, txtUserType, txtUserContact, txtUserCnic, txtUserStatus);
            txt_user_id.setText(txtUserId);
            txt_user_name.setText(txtUserName);
            txt_user_type.setValue(txtUserType);
            txt_user_contact.setText(txtUserContact);
            txt_user_cnic.setText(txtUserCnic);
            txt_user_status.setValue(txtUserStatus);
            filter = false;
            filter_pane.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
        }
        else
        {
            userData = objViewUsersInfo.getUserInfo(objStmt, objCon);
        }
        for (int i = 0; i < userData.size(); i++)
        {
            summaryTotal++;
            if(userData.get(i).get(9).equals("Active"))
            {
                summaryActive++;
            }
            else if(userData.get(i).get(9).equals("In Active"))
            {
                summaryInActive++;
            }
//            System.out.println(String.valueOf(i+1)+"   ---   "+userData.get(i).get(0)+"   ---   "+userData.get(i).get(1)+"   ---   "+userData.get(i).get(2)+"   ---   "+userData.get(i).get(3)+"   ---   "+userData.get(i).get(4)+"   ---   "+userData.get(i).get(5)+"   ---   "+userData.get(i).get(6)+"   ---   "+userData.get(i).get(7)+"   ---   "+userData.get(i).get(8));
            userDetail.add(new ViewUsersInfo(String.valueOf(i+1), userData.get(i).get(0), ((userData.get(i).get(1) == null) ? "N/A" : userData.get(i).get(1)), userData.get(i).get(2), userData.get(i).get(3), userData.get(i).get(4), userData.get(i).get(5), userData.get(i).get(6), userData.get(i).get(7), userData.get(i).get(8), userData.get(i).get(9)));
        }
        return userDetail;
    }

    @FXML
    void btnSearch(ActionEvent event) {
        txtUserId = txt_user_id.getText();
        txtUserName = txt_user_name.getText();
        txtUserType = txt_user_type.getValue();
        if(txtUserType == null)
        {
            txtUserType = "All";
        }
        txtUserContact = txt_user_contact.getText();
        txtUserCnic = txt_user_cnic.getText();
        txtUserStatus = txt_user_status.getValue();
        if(txtUserStatus == null)
        {
            txtUserStatus = "All";
        }
        filter = true;

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/view_users.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void btnReset(ActionEvent event) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/view_users.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void exportExcel(ActionEvent event) throws IOException {
        Workbook workbook = new HSSFWorkbook();
        Sheet spreadsheet = workbook.createSheet("data");
        Row row = spreadsheet.createRow(0);

        for (int j = 0; j < table_viewusers.getColumns().size()-1; j++) {
            row.createCell(j).setCellValue(table_viewusers.getColumns().get(j).getText());
        }

        for (int i = 0; i < table_viewusers.getItems().size(); i++) {
            row = spreadsheet.createRow(i + 1);
            for (int j = 0; j < table_viewusers.getColumns().size()-1; j++) {
                if(table_viewusers.getColumns().get(j).getCellData(i) != null) {
                    row.createCell(j).setCellValue(table_viewusers.getColumns().get(j).getCellData(i).toString());
                }
                else {
                    row.createCell(j).setCellValue("");
                }
            }
        }

        FileOutputStream fileOut = new FileOutputStream("data/View_user.xls");
        workbook.write(fileOut);
        fileOut.close();
    }

    public void follow() throws IOException
    {
        if (drawer.isOpened()) {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(-250);
            transition.setByY(0);
            transition.setCycleCount(1);
            transition.play();
            drawer.close();
            FA_Icon.setGlyphName("ANGLE_DOUBLE_RIGHT");
            FA_Icon.setGlyphSize(14);
            //inner_anchor.setRightAnchor(inner_anchor,1d);
            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(-90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(1.1000);
            scale.setToY(1);

            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();

        }
        else {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(250);
            transition.setByY(0);
            transition.setCycleCount(1);
            FA_Icon.setGlyphName("ANGLE_DOUBLE_LEFT");
            FA_Icon.setGlyphSize(14);
            drawer.open();

            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(0.99947777);
            scale.setToY(1);



            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();

        }

    }

    @FXML
    void addUser() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/enter_user.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
}
