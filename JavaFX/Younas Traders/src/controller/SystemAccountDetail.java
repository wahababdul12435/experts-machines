package controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import model.MysqlCon;
import java.sql.ResultSet;
import model.SystemAccountsInfo;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.ResourceBundle;

public class SystemAccountDetail implements Initializable {
    @FXML
    private HBox menu_bar;
    @FXML
    private Button save_account;
    @FXML
    private Button cancel_account;
    @FXML private TextField txt_userid;
    @FXML private TextField txt_username;
    @FXML private TextField txt_userdesig;
    @FXML private TextField txt_userstatus;
    @FXML private DatePicker datepick_regDate;
    @FXML private CheckBox chkbx_orderbooking;
    @FXML private CheckBox chkbx_ordercolction;
    @FXML private CheckBox chkbx_create;
    @FXML private CheckBox chkbx_read;
    @FXML private CheckBox chkbx_update;
    @FXML private CheckBox chkbx_delete;
    @FXML private CheckBox chkbx_manage_settings;



    private String userId;
    String[] getuserdetails = new String[3];
    int chkbx_orderbooking_val=0;
    int chkbx_ordercolction_val=0;
    int chkbx_create_val=0;
    int chkbx_read_val=0;
    int chkbx_update_val=0;
    int chkbx_delete_val=0;
    int chkbx_access_settings_val=0;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.userId = SystemAccountsInfo.getUserIdDetail();
        System.out.println(this.userId);
        txt_userid.setText(userId);
        txt_userid.setEditable(false);
        int intval_userid=Integer.parseInt(userId);
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        SystemAccountsInfo objSystemAccountsInfo = new SystemAccountsInfo();
        getuserdetails = objSystemAccountsInfo.getusersinfo(objStmt, objCon, intval_userid);
        if(getuserdetails[0]==null)
        {

        }
        else
        {
            String[] usersdetails = getuserdetails[0].split("-");
            txt_username.setText(usersdetails[0]);
            txt_userstatus.setText(usersdetails[3]);
            txt_userdesig.setText(usersdetails[1]);
            String[] splitDate_Time = usersdetails[2].split("/");
            String nsss = splitDate_Time[0] +'-' + splitDate_Time[1] +'-'+ splitDate_Time[2];
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
            Date varDate = null;
            try {
                varDate = dateFormat.parse(nsss);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            dateFormat=new SimpleDateFormat("yyyy-MM-dd");
            String mydate = dateFormat.format(varDate);
            LocalDate datePart = LocalDate.parse(mydate);
            datepick_regDate.setValue(datePart);
        }


    }


    String[] getuser_rightsdetails = new String[3];
    public void saveAccount()
    {int intval_userid=Integer.parseInt(userId);
        if(chkbx_delete.isSelected())
        {
            chkbx_delete_val=1;
        }

        if(chkbx_read.isSelected())
        {
            chkbx_read_val=1;
        }

        if(chkbx_update.isSelected())
        {
            chkbx_update_val=1;
        }

        if(chkbx_create.isSelected())
        {
            chkbx_create_val=1;
        }

        if(chkbx_orderbooking.isSelected())
        {
            chkbx_orderbooking_val=1;
        }

        if(chkbx_ordercolction.isSelected())
        {
            chkbx_ordercolction_val=1;
        }
        if(chkbx_manage_settings.isSelected())
        {
            chkbx_access_settings_val=1;
        }
        SystemAccountsInfo objSystemAccountsInfo = new SystemAccountsInfo();
        MysqlCon mysqlDB_con = new MysqlCon();
        Statement DB_Statmnt = mysqlDB_con.stmt;
        Connection DB_conn = mysqlDB_con.con;
        getuser_rightsdetails = objSystemAccountsInfo.getuser_rights_sinfo(DB_Statmnt, DB_conn, intval_userid);
        if(getuser_rightsdetails[0]==null)
        {
            MysqlCon MysqlCon_obj = new MysqlCon();
            Statement Statmt_obj = MysqlCon_obj.stmt;
            Connection Conn_obj = MysqlCon_obj.con;
            objSystemAccountsInfo.saveuserRights(Statmt_obj, Conn_obj, userId,chkbx_update_val, chkbx_delete_val, chkbx_orderbooking_val, chkbx_create_val, chkbx_access_settings_val, chkbx_read_val, chkbx_ordercolction_val);
            MysqlCon objMysqlCon = new MysqlCon();
            Statement objStmt = objMysqlCon.stmt;
            Connection objCon = objMysqlCon.con;

            objSystemAccountsInfo.saveAccountDetail(objStmt, objCon, userId, "Approved");
            try {
                viewSystemAccounts();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else
        {
            MysqlCon MysqlCon_obj = new MysqlCon();
            Statement Statmt_obj = MysqlCon_obj.stmt;
            Connection Conn_obj = MysqlCon_obj.con;
            objSystemAccountsInfo.updateuserRights(Statmt_obj, Conn_obj, userId,chkbx_update_val, chkbx_delete_val, chkbx_orderbooking_val, chkbx_create_val, chkbx_access_settings_val, chkbx_read_val, chkbx_ordercolction_val);
        }





    }

    public void cancelAccount()
    {
        try {
            viewSystemAccounts();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void viewHome() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/home.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    public void viewPendings() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_pending_orders.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    public void SaleInvoices() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/sale_invoice.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    public void addCompany() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/enter_company.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewCompanies() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_company.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    void addArea() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/enter_area.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewAreas() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_area.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewSubAreas() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_subarea.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    void addDealer() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/enter_dealer.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewDealers() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_dealers.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    void addSalesman() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/enter_salesman.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewSalesman() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_salesman.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    void addSupplier() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/enter_supplier.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewSuppliers() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_suppliers.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    void addProduct() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/enter_new_product.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewProducts() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_products.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewSystemAccounts() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/system_accounts.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }
}
