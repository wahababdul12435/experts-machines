package controller;

import com.jfoenix.controls.JFXDrawer;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.web.WebView;
import model.DistrictDealersDetailInfo;

import java.net.URL;
import java.util.ResourceBundle;

public class DistrictDealersDetail implements Initializable {

    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private WebView web_google_map;

    @FXML
    private TableView<DistrictDealersDetailInfo> table_viewdealer;

    @FXML
    private TableColumn<DistrictDealersDetailInfo, String> sr_no;

    @FXML
    private TableColumn<DistrictDealersDetailInfo, String> dealer_name;

    @FXML
    private TableColumn<DistrictDealersDetailInfo, String> dealer_contact;

    @FXML
    private TableColumn<DistrictDealersDetailInfo, String> dealer_address;

    @FXML
    private TableColumn<DistrictDealersDetailInfo, String> dealer_cnic;

    @FXML
    private TableColumn<DistrictDealersDetailInfo, String> dealer_type;

    @FXML
    private TableColumn<DistrictDealersDetailInfo, String> dealer_added;

    @FXML
    private TableColumn<DistrictDealersDetailInfo, String> operations;

    @FXML
    private BorderPane menu_bar;

    private DistrictDealersDetailInfo objDistrictDealersDetailInfo;
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        objDistrictDealersDetailInfo = new DistrictDealersDetailInfo();

    }
}
