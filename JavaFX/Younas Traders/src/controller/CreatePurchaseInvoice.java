package controller;

import com.jfoenix.controls.*;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.util.StringConverter;
import model.*;
import org.controlsfx.control.textfield.TextFields;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

public class CreatePurchaseInvoice implements Initializable {

    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private JFXTextField txt_invoice_no;

    @FXML
    private JFXComboBox<String> txt_company_name;

    @FXML
    private JFXComboBox<String> txt_supplier_name;

    @FXML
    private JFXDatePicker txt_purchase_date;

    @FXML
    private JFXTextField txt_product_name;

    @FXML
    private JFXTextField txt_product_batch;

    @FXML
    private JFXDatePicker txt_batch_expiry;

    @FXML
    private JFXTextField txt_product_quantity;

    @FXML
    private JFXTextField txt_product_bonus;

    @FXML
    private JFXTextField txt_retail_price;

    @FXML
    private JFXTextField txt_trade_price;

    @FXML
    private JFXTextField txt_product_price;

    @FXML
    private JFXTextField txt_product_discount;

    @FXML
    private JFXTextField txt_product_net_price;

    @FXML
    private JFXCheckBox change_price;

    @FXML
    private JFXButton btn_cancel_edit;

    @FXML
    private JFXButton btn_update_edit;

    @FXML
    private JFXButton btn_add;

    @FXML
    private HBox summary_hbox1;

    @FXML
    private Label lbl_product_quantity;

    @FXML
    private Label lbl_bonus_quantity;

    @FXML
    private Label lbl_net_price;

    @FXML
    private Label lbl_discount_given;

    @FXML
    private Label lbl_near_expiry;

    @FXML
    private TableView<CreatePurchaseInvoiceInfo> table_purchasedetaillog;

    @FXML
    private TableColumn<CreatePurchaseInvoiceInfo, String> sr_no;

    @FXML
    private TableColumn<CreatePurchaseInvoiceInfo, String> product_id;

    @FXML
    private TableColumn<CreatePurchaseInvoiceInfo, String> product_name;

    @FXML
    private TableColumn<CreatePurchaseInvoiceInfo, String> batch_no;

    @FXML
    private TableColumn<CreatePurchaseInvoiceInfo, String> batch_expiry;

    @FXML
    private TableColumn<CreatePurchaseInvoiceInfo, String> product_quantity;

    @FXML
    private TableColumn<CreatePurchaseInvoiceInfo, String> bonus_quantity;

    @FXML
    private TableColumn<CreatePurchaseInvoiceInfo, String> retail_price;

    @FXML
    private TableColumn<CreatePurchaseInvoiceInfo, String> trade_price;

    @FXML
    private TableColumn<CreatePurchaseInvoiceInfo, String> product_price;

    @FXML
    private TableColumn<CreatePurchaseInvoiceInfo, String> product_discount;

    @FXML
    private TableColumn<CreatePurchaseInvoiceInfo, String> product_net_price;

    @FXML
    private TableColumn<CreatePurchaseInvoiceInfo, String> operations;

    @FXML
    private JFXButton cancelBtn;

    @FXML
    private JFXButton saveBtn;

    @FXML
    private StackPane stackPane;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    @FXML
    private BorderPane brdrpane_navfinances;

    @FXML
    private BorderPane menu_bar;

    private static ObservableList<CreatePurchaseInvoiceInfo> purchaseDetails;
    private static CreatePurchaseInvoiceInfo objCreatePurchaseInvoiceInfo;
    private ProductInfo objProductInfo;
    static ArrayList<ArrayList<String>> purchaseData;
    public static ArrayList<ArrayList<String>> completeProductData;
    private static String selectedBatch;
    //    private static String selectedBatchQty;
    private static String selectedProductPrice;
    ArrayList<String> productNames;
    ArrayList<String> productQty;
    static SimpleDateFormat fmt = new SimpleDateFormat("dd/MMM/yyyy");

    public static float productsQty = 0;
    public static float bonusQty = 0;
    public static float netPrice = 0;
    public static float discountGiven = 0;
    public static float nearExpiry = 0;

    MysqlCon objMysqlCon1;
    MysqlCon objMysqlCon2;
    MysqlCon objMysqlCon3;
    static Statement objStmt1;
    Statement objStmt2;
    Statement objStmt3;
    Connection objCon;
    CompanyInfo objCompanyInfo;
    SupplierInfo objSupplierInfo;
    public ArrayList<InvoiceInfo> invoiceData;
    ArrayList<String> companyNames;
    ArrayList<ArrayList<String>> companyData;
    ArrayList<ArrayList<String>> supplierData;
    ArrayList<String> supplierNames;
    String selectedCompanyId;

    public static Button btnView;
    public static int srNo;
    ArrayList<String> prodRates = new ArrayList<>();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnPurchase.setStyle("-fx-background-color: #47ab1e");
        txt_purchase_date.setValue(LocalDate.now());
        txt_batch_expiry.setValue(LocalDate.now());

        objMysqlCon1 = new MysqlCon();
        objMysqlCon2 = new MysqlCon();
        objMysqlCon3 = new MysqlCon();
        objStmt1 = objMysqlCon1.stmt;
        objStmt2 = objMysqlCon2.stmt;
        objStmt3 = objMysqlCon3.stmt;
        objCon = objMysqlCon1.con;
        objProductInfo = new ProductInfo();
        purchaseData = new ArrayList<>();

        CreatePurchaseInvoiceInfo.productIdArr = new ArrayList<>();
        CreatePurchaseInvoiceInfo.productNameArr = new ArrayList<>();
        CreatePurchaseInvoiceInfo.batchNoArr = new ArrayList<>();
        CreatePurchaseInvoiceInfo.batchExpiryArr = new ArrayList<>();
        CreatePurchaseInvoiceInfo.productQtyArr = new ArrayList<>();
        CreatePurchaseInvoiceInfo.bonusQtyArr = new ArrayList<>();
        CreatePurchaseInvoiceInfo.productPriceArr = new ArrayList<>();
        CreatePurchaseInvoiceInfo.productDiscountArr = new ArrayList<>();
        CreatePurchaseInvoiceInfo.productNetPriceArr = new ArrayList<>();

        invoiceData = new ArrayList<>();

        objCompanyInfo = new CompanyInfo();
        objSupplierInfo = new SupplierInfo();
        objCreatePurchaseInvoiceInfo = new CreatePurchaseInvoiceInfo();
        companyData = objCompanyInfo.getCompanyNames();
        supplierData = objSupplierInfo.getSuppliersNames();
        companyNames = GlobalVariables.getArrayColumn(companyData, 1);
        supplierNames = GlobalVariables.getArrayColumn(supplierData, 1);
        txt_company_name.getItems().addAll(companyNames);
        txt_supplier_name.getItems().addAll(supplierNames);
        completeProductData = objProductInfo.getProductsDetail(GlobalVariables.objStmt, GlobalVariables.objCon);
        productNames = GlobalVariables.getArrayColumn(completeProductData, 1);
        productQty = GlobalVariables.getArrayColumn(completeProductData, 2);
        TextFields.bindAutoCompletion(txt_product_name, productNames);

        txt_purchase_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        txt_batch_expiry.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        txt_product_name.focusedProperty().addListener(new ChangeListener<Boolean>()
        {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue)
            {
                if(!txt_product_name.getText().equals(""))
                {
                MysqlCon newobjMysqlConnc = new MysqlCon();
                Statement objectStattmt = newobjMysqlConnc.stmt;
                Connection objectConnnec = newobjMysqlConnc.con;
                CreateSaleInvoiceInfo objctsaleinvoice = new CreateSaleInvoiceInfo();
                prodRates = objctsaleinvoice.getprodRates(objectStattmt,objectConnnec,txt_product_name.getText());
                    txt_retail_price.setText(prodRates.get(0));
                    txt_trade_price.setText(prodRates.get(1));
                    txt_product_price.setText(prodRates.get(2));
                    txt_product_discount.setText(prodRates.get(3));

                }

                   /* for(int i=0; i<completeProductData.size(); i++)
                    {
                        if(txt_product_name.getText().equals(completeProductData.get(i).get(1)))
                        {
                            txt_retail_price.setText(completeProductData.get(i).get(5));
                            txt_trade_price.setText(completeProductData.get(i).get(6));
                            txt_product_price.setText(completeProductData.get(i).get(7));
                            txt_product_discount.setText(completeProductData.get(i).get(8));

                            String mmm = txt_retail_price.getText();
                            String nmmm = txt_trade_price.getText();
                            String jmmm = txt_product_price.getText();
                        }
                    }*/


            }
        });

        CreatePurchaseInvoiceInfo.txtProductName = txt_product_name;
        CreatePurchaseInvoiceInfo.txtBatchNo = txt_product_batch;
        CreatePurchaseInvoiceInfo.txtBatchExpiry = txt_batch_expiry;
        CreatePurchaseInvoiceInfo.txtProductQty = txt_product_quantity;
        CreatePurchaseInvoiceInfo.txtProductBonus = txt_product_bonus;
        CreatePurchaseInvoiceInfo.txtRetailPrice = txt_retail_price;
        CreatePurchaseInvoiceInfo.txtTradePrice = txt_trade_price;
        CreatePurchaseInvoiceInfo.txtProductPrice = txt_product_price;
        CreatePurchaseInvoiceInfo.txtProductDiscount = txt_product_discount;
        CreatePurchaseInvoiceInfo.txtProductNetPrice = txt_product_net_price;
        CreatePurchaseInvoiceInfo.btnAdd = btn_add;
        CreatePurchaseInvoiceInfo.btnCancel = btn_cancel_edit;
        CreatePurchaseInvoiceInfo.btnUpdate = btn_update_edit;
        CreatePurchaseInvoiceInfo.lbl_product_quantity = lbl_product_quantity;
        CreatePurchaseInvoiceInfo.lbl_bonus_quantity = lbl_bonus_quantity;
        CreatePurchaseInvoiceInfo.lbl_net_price = lbl_net_price;
        CreatePurchaseInvoiceInfo.lbl_discount_given = lbl_discount_given;
        CreatePurchaseInvoiceInfo.lbl_near_expiry = lbl_near_expiry;

        txt_product_price.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                calculateNetPrice();
            }
        });

        txt_product_discount.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                calculateNetPrice();
            }
        });

        productsQty = 0;
        bonusQty = 0;
        netPrice = 0;
        discountGiven = 0;
        nearExpiry = 0;

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        product_id.setCellValueFactory(new PropertyValueFactory<>("productId"));
        product_name.setCellValueFactory(new PropertyValueFactory<>("productName"));
        batch_no.setCellValueFactory(new PropertyValueFactory<>("batchNo"));
        batch_expiry.setCellValueFactory(new PropertyValueFactory<>("batchExpiry"));
        product_quantity.setCellValueFactory(new PropertyValueFactory<>("productQty"));
        bonus_quantity.setCellValueFactory(new PropertyValueFactory<>("bonusQty"));
        retail_price.setCellValueFactory(new PropertyValueFactory<>("retailPrice"));
        trade_price.setCellValueFactory(new PropertyValueFactory<>("tradePrice"));
        product_price.setCellValueFactory(new PropertyValueFactory<>("productPrice"));
        product_discount.setCellValueFactory(new PropertyValueFactory<>("productDiscount"));
        product_net_price.setCellValueFactory(new PropertyValueFactory<>("productNetPrice"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_purchasedetaillog.setItems(parseUserList());
        CreatePurchaseInvoiceInfo.table_purchasedetaillog = table_purchasedetaillog;
    }

    public static ObservableList<CreatePurchaseInvoiceInfo> parseUserList() {

        purchaseDetails = FXCollections.observableArrayList();
        purchaseData = objCreatePurchaseInvoiceInfo.loadPurchaseTable();

        productsQty = 0;
        bonusQty = 0;
        netPrice = 0;
        discountGiven = 0;
        nearExpiry = 0;

        for (int i = 0; i < purchaseData.size(); i++)
        {
            productsQty += (purchaseData.get(i).get(4) != null && !purchaseData.get(i).get(4).equals("")) ? Integer.parseInt(purchaseData.get(i).get(4)) : 0;
            bonusQty += (purchaseData.get(i).get(5) != null && !purchaseData.get(i).get(5).equals("")) ? Float.parseFloat(purchaseData.get(i).get(5)) : 0;
            netPrice += (purchaseData.get(i).get(8) != null && !purchaseData.get(i).get(8).equals("")) ? Float.parseFloat(purchaseData.get(i).get(8)) : 0;
            discountGiven += (purchaseData.get(i).get(7) != null && !purchaseData.get(i).get(7).equals("")) ? Float.parseFloat(purchaseData.get(i).get(7)) : 0;
            if(purchaseData.get(i).get(3) != null && !purchaseData.get(i).get(3).equals("") && !purchaseData.get(i).get(3).equals("N/A"))
            {
                String stEndDate = purchaseData.get(i).get(3);
                String stStartDate = GlobalVariables.getStDate();

                Date endDate = null;
                Date startDate = null;
                try {
                    startDate = GlobalVariables.fmt.parse(stStartDate);
                    endDate = GlobalVariables.fmt.parse(stEndDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                long diff = endDate.getTime() - startDate.getTime();
                diff = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);

                if(diff <= 180)
                {
                    nearExpiry++;
                    CreatePurchaseInvoiceInfo.nearExpiryArr.add(true);
                }
                else
                {
                    CreatePurchaseInvoiceInfo.nearExpiryArr.add(false);
                }

            }
            purchaseDetails.add(new CreatePurchaseInvoiceInfo(String.valueOf(i+1), purchaseData.get(i).get(0), purchaseData.get(i).get(1), purchaseData.get(i).get(2), purchaseData.get(i).get(3), String.format("%,.0f", Float.parseFloat(purchaseData.get(i).get(4))), String.format("%,.0f", Float.parseFloat(purchaseData.get(i).get(5))), String.format("%,.2f", Float.parseFloat(purchaseData.get(i).get(6))), String.format("%,.2f", Float.parseFloat(purchaseData.get(i).get(7))), String.format("%,.2f", Float.parseFloat(purchaseData.get(i).get(8))), String.format("%,.2f", Float.parseFloat(purchaseData.get(i).get(9))), String.format("%,.2f", Float.parseFloat(purchaseData.get(i).get(10)))));
        }
        CreatePurchaseInvoiceInfo.lbl_product_quantity.setText("Purchase Qty\n"+String.format("%,.0f", productsQty));
        CreatePurchaseInvoiceInfo.lbl_bonus_quantity.setText("Bonus Qty\n"+String.format("%,.0f", bonusQty));
        CreatePurchaseInvoiceInfo.lbl_net_price.setText("Net Price\n"+"Rs."+String.format("%,.0f", netPrice)+"/-");
        CreatePurchaseInvoiceInfo.lbl_discount_given.setText("Discount Given\n"+"Rs."+String.format("%,.0f", discountGiven)+"/-");
        CreatePurchaseInvoiceInfo.lbl_near_expiry.setText("Near Expiry / Expired\n"+String.format("%,.0f", nearExpiry));
        return purchaseDetails;
    }

    @FXML
    void changePrice(ActionEvent event) {
        if(change_price.isSelected())
        {
            txt_retail_price.setDisable(false);
            txt_trade_price.setDisable(false);
            txt_product_price.setDisable(false);
            txt_product_discount.setDisable(false);
        }
        else
        {
            txt_retail_price.setDisable(true);
            txt_trade_price.setDisable(true);
            txt_product_price.setDisable(true);
            txt_product_discount.setDisable(true);

            if(!txt_product_name.getText().equals(""))
            {
                for(int i=0; i<completeProductData.size(); i++)
                {
                    if(txt_product_name.getText().equals(completeProductData.get(i).get(1)))
                    {
                        txt_retail_price.setText(completeProductData.get(i).get(5));
                        txt_trade_price.setText(completeProductData.get(i).get(6));
                        txt_product_price.setText(completeProductData.get(i).get(7));
                        txt_product_discount.setText(completeProductData.get(i).get(8));
                    }
                }
            }
        }
    }

    @FXML
    void enterpressBonus(KeyEvent event)
    {
        if(event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB) {
            btn_add.requestFocus();
        }
    }

    @FXML
    void btnAdd(ActionEvent event)
    {
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        String productName = txt_product_name.getText();
        StringBuffer PN = new StringBuffer(productName);
        int chkBit = 0;
        for(int q=1;q<PN.length();q++)
        {
            char mmn =  PN.charAt(PN.length()-1);
            if(mmn!='(')
            {
                if(chkBit==0)
                {
                    PN.deleteCharAt(PN.length()-1);
                }
            }
            else
            {
                chkBit=1;
            }

        }
        if(PN.charAt(PN.length()-1)=='(')
        {
            PN.deleteCharAt(PN.length()-1);
        }
        if(PN.charAt(PN.length()-1)==' ')
        {
            PN.deleteCharAt(PN.length()-1);
        }
        String onlyProdName = PN.toString();

        String productId = GlobalVariables.getIdFrom2D(completeProductData, onlyProdName, 1);
        if(productId==""||productId==null)
        {
            String title = "Error";
            String message = "Enter Product of Selected Company.";
            GlobalVariables.showNotification(-1, title, message);
            txt_product_name.setText("");
            txt_product_batch.setText("");
            txt_batch_expiry.setValue(LocalDate.now());
            txt_product_quantity.setText("");
            txt_product_bonus.setText("");
            txt_retail_price.setText("");
            txt_trade_price.setText("");
            txt_product_price.setText("");
            txt_product_discount.setText("");
            txt_product_net_price.setText("");
            btn_add.setVisible(true);
            btn_cancel_edit.setVisible(false);
            btn_update_edit.setVisible(false);

        }
        else {
            String batchNo = txt_product_batch.getText();
            String batchExpiry = "";
            Date selectedDate = null;
            if (txt_batch_expiry.getValue() != null) {
                batchExpiry = txt_batch_expiry.getValue().toString();
                try {
                    selectedDate = sdf.parse(batchExpiry);
                    batchExpiry = sdf2.format(selectedDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            String productQty = txt_product_quantity.getText();
            String stBonusQty = txt_product_bonus.getText();
            String retailPrice = txt_retail_price.getText();
            String tradePrice = txt_trade_price.getText();
            String productPrice = txt_product_price.getText();
            String productDiscount = txt_product_discount.getText();
            String productNetPrice = txt_product_net_price.getText();


            CreatePurchaseInvoiceInfo.productIdArr.add(productId);
            CreatePurchaseInvoiceInfo.productNameArr.add(productName);
            CreatePurchaseInvoiceInfo.batchNoArr.add(batchNo);
            CreatePurchaseInvoiceInfo.batchExpiryArr.add(batchExpiry);
            CreatePurchaseInvoiceInfo.productQtyArr.add(productQty);
            CreatePurchaseInvoiceInfo.bonusQtyArr.add(stBonusQty.equals("") ? "0" : stBonusQty);
            CreatePurchaseInvoiceInfo.retailPriceArr.add(retailPrice);
            CreatePurchaseInvoiceInfo.tradePriceArr.add(tradePrice);
            CreatePurchaseInvoiceInfo.productPriceArr.add(productPrice);
            CreatePurchaseInvoiceInfo.productDiscountArr.add(productDiscount.equals("") ? "0" : productDiscount);
            CreatePurchaseInvoiceInfo.productNetPriceArr.add(productNetPrice);
            table_purchasedetaillog.setItems(parseUserList());
            CreatePurchaseInvoiceInfo.table_purchasedetaillog = table_purchasedetaillog;
            txt_product_name.setText("");
            txt_product_batch.setText("");
            txt_batch_expiry.setValue(LocalDate.now());
            txt_product_quantity.setText("");
            txt_product_bonus.setText("");
            txt_retail_price.setText("");
            txt_trade_price.setText("");
            txt_product_price.setText("");
            txt_product_discount.setText("");
            txt_product_net_price.setText("");
            btn_add.setVisible(true);
            btn_cancel_edit.setVisible(false);
            btn_update_edit.setVisible(false);
        }
    }

    @FXML
    void btnCancelEdit(ActionEvent event) {
        txt_product_name.setText("");
        txt_product_batch.setText("");
        txt_batch_expiry.setValue(LocalDate.now());
        txt_product_quantity.setText("");
        txt_product_bonus.setText("");
        txt_retail_price.setText("");
        txt_trade_price.setText("");
        txt_product_price.setText("");
        txt_product_discount.setText("");
        txt_product_net_price.setText("");
        btn_add.setVisible(true);
        btn_cancel_edit.setVisible(false);
        btn_update_edit.setVisible(false);
    }

    @FXML
    void companyChange(ActionEvent event) {
        selectedCompanyId = GlobalVariables.getIdFrom2D(companyData, txt_company_name.getValue(), 1);
        completeProductData = objProductInfo.getCompanyProductsDetail(objStmt1, objCon, selectedCompanyId);
        productNames = GlobalVariables.getArrayColumn(completeProductData, 1);
        productQty = GlobalVariables.getArrayColumn(completeProductData, 2);
        txt_product_name.setText("");
        //TextFields.bindAutoCompletion(txt_product_name, productNames);
    }

    @FXML
    void btnUpdateEdit(ActionEvent event) {
        String productName = txt_product_name.getText();
        String productId = GlobalVariables.getIdFrom2D(completeProductData, productName, 1);
        String batchNo = txt_product_batch.getText();
        String batchExpiry = "";
        Date selectedDate = null;
        if(txt_batch_expiry.getValue() != null)
        {
            batchExpiry = txt_batch_expiry.getValue().toString();
            try {
                selectedDate = GlobalVariables.sdf.parse(batchExpiry);
                batchExpiry = GlobalVariables.sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        String productQty = txt_product_quantity.getText();
        String stBonusQty = txt_product_bonus.getText();
        String retailPrice = txt_retail_price.getText();
        String tradePrice = txt_trade_price.getText();
        String productPrice = txt_product_price.getText();
        String productDiscount = txt_product_discount.getText();
        String productNetPrice = txt_product_net_price.getText();


        CreatePurchaseInvoiceInfo.productIdArr.set(srNo, productId);
        CreatePurchaseInvoiceInfo.productNameArr.set(srNo, productName);
        CreatePurchaseInvoiceInfo.batchNoArr.set(srNo, batchNo);
        CreatePurchaseInvoiceInfo.batchExpiryArr.set(srNo, batchExpiry);
        CreatePurchaseInvoiceInfo.productQtyArr.set(srNo, productQty);
        CreatePurchaseInvoiceInfo.bonusQtyArr.set(srNo, stBonusQty.equals("") ? "0" : stBonusQty);
        CreatePurchaseInvoiceInfo.retailPriceArr.set(srNo, retailPrice);
        CreatePurchaseInvoiceInfo.tradePriceArr.set(srNo, tradePrice);
        CreatePurchaseInvoiceInfo.productPriceArr.set(srNo, productPrice);
        CreatePurchaseInvoiceInfo.productDiscountArr.set(srNo, productDiscount.equals("") ? "0" : productDiscount);
        CreatePurchaseInvoiceInfo.productNetPriceArr.set(srNo, productNetPrice);
        table_purchasedetaillog.setItems(parseUserList());
        CreatePurchaseInvoiceInfo.table_purchasedetaillog = table_purchasedetaillog;
        lbl_product_quantity.setText("Purchase Qty\n"+String.format("%,.0f", productsQty));
        lbl_bonus_quantity.setText("Bonus Qty\n"+String.format("%,.0f", bonusQty));
        lbl_net_price.setText("Net Price\n"+String.format("%,.0f", netPrice));
        lbl_discount_given.setText("Discount Given\n"+String.format("%,.0f", discountGiven));
        lbl_near_expiry.setText("Near Expiry / Expired\n"+String.format("%,.0f", nearExpiry));

        txt_product_name.setText("");
        txt_product_batch.setText("");
        txt_batch_expiry.setValue(LocalDate.now());
        txt_product_quantity.setText("");
        txt_product_bonus.setText("");
        txt_retail_price.setText("");
        txt_trade_price.setText("");
        txt_product_price.setText("");
        txt_product_discount.setText("");
        txt_product_net_price.setText("");
        btn_add.setVisible(true);
        btn_cancel_edit.setVisible(false);
        btn_update_edit.setVisible(false);
    }

    @FXML
    void cancelClicked(ActionEvent event) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/view_purchase.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void follow(MouseEvent event) {

    }

    private void calculateNetPrice() {
        float productPrice;
        float productDiscount;
        float productNetPrice;
        if(!txt_product_price.getText().equals(""))
        {
            productPrice = Float.parseFloat(txt_product_price.getText());
        }
        else
        {
            productPrice = 0;
        }

        if(!txt_product_discount.getText().equals(""))
        {
            productDiscount = Float.parseFloat(txt_product_discount.getText());
        }
        else
        {
            productDiscount = 0;
        }

        productNetPrice = productPrice - productDiscount;
        productNetPrice = GlobalVariables.roundFloat(productNetPrice, 2);
        txt_product_net_price.setText(String.valueOf(productNetPrice));
    }

    @FXML
    void saveClicked(ActionEvent event) {
        float totalProductPrice = 0;
        float totalDiscount = 0;
        float totalNetPrice = 0;
        String invoiceNum = txt_invoice_no.getText();
        String supplierId = GlobalVariables.getIdFrom2D(supplierData, txt_supplier_name.getValue(), 1);
        String purchaseDate = "";
        Date selectedDate = null;
        if(txt_purchase_date.getValue() != null)
        {
            purchaseDate = txt_purchase_date.getValue().toString();
            try {
                selectedDate = GlobalVariables.sdf.parse(purchaseDate);
                purchaseDate = GlobalVariables.sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        for(int i=0; i<CreatePurchaseInvoiceInfo.productPriceArr.size(); i++)
        {
            totalProductPrice += Float.parseFloat(CreatePurchaseInvoiceInfo.productPriceArr.get(i));
            totalDiscount += Float.parseFloat(CreatePurchaseInvoiceInfo.productDiscountArr.get(i));
            totalNetPrice += Float.parseFloat(CreatePurchaseInvoiceInfo.productNetPriceArr.get(i));
        }

        objCreatePurchaseInvoiceInfo.insertBasic(objStmt1, selectedCompanyId, invoiceNum, supplierId, purchaseDate, String.valueOf(totalProductPrice), String.valueOf(totalDiscount), String.valueOf(totalNetPrice));
        objCreatePurchaseInvoiceInfo.insertInvoice(objStmt1, objCon);

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/view_purchase.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void showBooked(MouseEvent event) {

    }

    @FXML
    void showBookingPrice(MouseEvent event) {

    }

    @FXML
    void showDelivered(MouseEvent event) {

    }

    @FXML
    void showDiscountGiven(MouseEvent event) {

    }

    @FXML
    void showReturned(MouseEvent event) {

    }
}
