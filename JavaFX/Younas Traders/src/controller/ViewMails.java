package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.util.StringConverter;
import model.GlobalVariables;
import model.MysqlCon;
import model.ViewMailsInfo;
import model.ViewMailsInfo;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

public class ViewMails implements Initializable {

    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private TableView<ViewMailsInfo> table_viewmails;

    @FXML
    private TableColumn<ViewMailsInfo, String> sr_no;

    @FXML
    private TableColumn<ViewMailsInfo, String> mail_id;

    @FXML
    private TableColumn<ViewMailsInfo, String> from_email;

    @FXML
    private TableColumn<ViewMailsInfo, String> to_email;

    @FXML
    private TableColumn<ViewMailsInfo, String> attachment;

    @FXML
    private TableColumn<ViewMailsInfo, String> user_name;

    @FXML
    private TableColumn<ViewMailsInfo, String> date;

    @FXML
    private TableColumn<ViewMailsInfo, String> time;

    @FXML
    private TableColumn<ViewMailsInfo, String> status;

    @FXML
    private TableColumn<ViewMailsInfo, String> operations;

    @FXML
    private JFXButton btn_export;

    @FXML
    private JFXButton btn_send_new;

    @FXML
    private AnchorPane filter_pane;

    @FXML
    private HBox filter_hbox;

    @FXML
    private JFXTextField txt_mail_id;

    @FXML
    private JFXTextField txt_mail_subject;

    @FXML
    private JFXComboBox<String> txt_from_email;

    @FXML
    private JFXComboBox<String> txt_to_email;

    @FXML
    private JFXDatePicker txt_from_date;

    @FXML
    private JFXDatePicker txt_to_date;

    @FXML
    private JFXComboBox<String> txt_status;

    @FXML
    private HBox summary_hbox;

    @FXML
    private Label lbl_total;

    @FXML
    private Label lbl_sent;

    @FXML
    private Label lbl_un_sent;

    @FXML
    private StackPane stackPane;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private BorderPane menu_bar1;

    @FXML
    private BorderPane nav_live;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    public ObservableList<ViewMailsInfo> mailDetails;
    private ViewMailsInfo objViewMailsInfo;
    private ArrayList<String> fromMailsData = new ArrayList<>();
    private ArrayList<String> toMailsData = new ArrayList<>();
    private int summaryTotal;
    private int summarySent;
    private int summaryUnSent;

    public static String txtMailId = "";
    public static String txtMailSubject = "";
    public static String txtFromEmail = "All";
    public static String txtToEmail = "All";
    public static String txtFromDate = "";
    public static String txtToDate = "";
    public static String txtStatus = "All";

    public static boolean filter;
    public static Button btnView;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnLive.setStyle("-fx-background-color: #47ab1e");
        objViewMailsInfo = new ViewMailsInfo();
        fromMailsData = objViewMailsInfo.getFromMailsData(GlobalVariables.objStmt, GlobalVariables.objCon);
        txt_from_email.getItems().addAll(fromMailsData);
        toMailsData = objViewMailsInfo.getToMailsData(GlobalVariables.objStmt, GlobalVariables.objCon);
        txt_to_email.getItems().addAll(toMailsData);


        txt_from_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        txt_to_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        mail_id.setCellValueFactory(new PropertyValueFactory<>("mailId"));
        from_email.setCellValueFactory(new PropertyValueFactory<>("fromEmail"));
        to_email.setCellValueFactory(new PropertyValueFactory<>("toEmail"));
        attachment.setCellValueFactory(new PropertyValueFactory<>("attachment"));
        user_name.setCellValueFactory(new PropertyValueFactory<>("userName"));
        date.setCellValueFactory(new PropertyValueFactory<>("date"));
        time.setCellValueFactory(new PropertyValueFactory<>("time"));
        status.setCellValueFactory(new PropertyValueFactory<>("status"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_viewmails.setItems(parseUserList());
        lbl_total.setText("Total\n"+summaryTotal);
        lbl_sent.setText("Sent\n"+summarySent);
        lbl_un_sent.setText("Un Sent\n"+summaryUnSent);
    }

    private ObservableList<ViewMailsInfo> parseUserList(){
        ViewMailsInfo objViewMailsInfo = new ViewMailsInfo();
        mailDetails = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> mailsData;
        if(filter)
        {
            mailsData  = objViewMailsInfo.getMailsDetailSearch(GlobalVariables.objStmt, GlobalVariables.objCon, txtMailId, txtMailSubject, txtFromEmail, txtToEmail, txtFromDate, txtToDate, txtStatus);
            txt_mail_id.setText(txtMailId);
            txt_mail_subject.setText(txtMailSubject);
            txt_from_email.setValue(txtFromEmail);
            txt_to_email.setValue(txtToEmail);
            if (txtFromDate != null && !txtFromDate.equals("")) {
                txt_from_date.setValue(GlobalVariables.LOCAL_DATE(txtFromDate));
            }
            if (txtToDate != null && !txtToDate.equals("")) {
                txt_to_date.setValue(GlobalVariables.LOCAL_DATE(txtToDate));
            }
            else
            {
                txtToDate = GlobalVariables.getStDate();
            }
            txt_status.setValue(txtStatus);
            filter = false;
            filter_pane.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
        }
        else
        {
            mailsData = objViewMailsInfo.getMailsDetail(GlobalVariables.objStmt, GlobalVariables.objCon);
        }

        for (int i = 0; i < mailsData.size(); i++)
        {
            summaryTotal++;
            if(mailsData.get(i).get(7).equals("Sent"))
            {
                summarySent++;
            }
            else if(mailsData.get(i).get(7).equals("Unsent"))
            {
                summaryUnSent++;
            }
            mailDetails.add(new ViewMailsInfo(String.valueOf(i+1), mailsData.get(i).get(0), ((mailsData.get(i).get(1) == null) ? "N/A" : mailsData.get(i).get(1)), mailsData.get(i).get(2), ((mailsData.get(i).get(3) == null) ? "N/A" : mailsData.get(i).get(3)), ((mailsData.get(i).get(4) == null) ? "N/A" : mailsData.get(i).get(4)), ((mailsData.get(i).get(5) == null) ? "N/A" : mailsData.get(i).get(5)), mailsData.get(i).get(6), mailsData.get(i).get(7)));
        }
        return mailDetails;
    }

    @FXML
    void sendNewMail(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/send_mail.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void btnReset(ActionEvent event) {

    }

    @FXML
    void btnSearch(ActionEvent event) {
        txtMailId = txt_mail_id.getText();
        txtMailSubject = txt_mail_subject.getText();
        txtFromEmail = txt_from_email.getValue();
        if(txtFromEmail == null)
        {
            txtFromEmail = "All";
        }
        txtToEmail = txt_to_email.getValue();
        if(txtToEmail == null)
        {
            txtToEmail = "All";
        }
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date selectedDate = null;
        if(txt_from_date.getValue() != null)
        {
            txtFromDate = txt_from_date.getValue().toString();
            try {
                selectedDate = sdf.parse(txtFromDate);
                txtFromDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        else
        {
            txtFromDate = "";
        }

        if(txt_to_date.getValue() != null)
        {
            txtToDate = txt_to_date.getValue().toString();
            try {
                selectedDate = sdf.parse(txtToDate);
                txtToDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        else
        {
            txtToDate = "";
        }
        txtStatus = txt_status.getValue();
        if(txtStatus == null)
        {
            txtStatus = "All";
        }
        filter = true;

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/view_mails.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void exportExcel(ActionEvent event) {

    }

    @FXML
    void follow(MouseEvent event) {

    }


}
