package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.FetchServerData;
import model.GlobalVariables;
import model.ViewDealerFinanceInfo;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;


public class NavFinances implements Initializable {

    @FXML
    private AnchorPane rootpane;

    @FXML
    private VBox vbox_style;

    @FXML
    private Button btn_finance_dashboard;

    @FXML
    private Button btn_dealer_finance;

    @FXML
    private Button btn_company_finance;

    FetchServerData objFetchServerData;
    ArrayList<ArrayList<String>> newDealerFinance;
    ViewDealerFinanceInfo objViewDealerFinanceInfo;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        DashboardFinance.btnView = btn_finance_dashboard;
        ViewDealerFinance.btnView = btn_dealer_finance;
        AddDealerFinance.btnView = btn_dealer_finance;
        ViewCompanyFinance.btnView = btn_company_finance;
        AddCompanyFinance.btnView = btn_company_finance;
        if(GlobalVariables.findNewDealerFinance > 0)
        {
            btn_dealer_finance.setText("Dealer Finance ("+GlobalVariables.findNewDealerFinance+")");
        }
        else
        {
            btn_dealer_finance.setText("Dealer Finance");
        }
    }

    @FXML
    void financeDashboard(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/dashboard_finance.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void dealerFinance(ActionEvent event) throws IOException {
        ViewDealerFinanceInfo.newIds = new ArrayList<>();
        if(GlobalVariables.findNewDealerFinance > 0)
        {
            newDealerFinance = importNewDealerFinance();
            objViewDealerFinanceInfo = new ViewDealerFinanceInfo();
            if(newDealerFinance != null)
            {
                objViewDealerFinanceInfo.insertNewDealerFinance(newDealerFinance);
            }
        }

        Parent root = FXMLLoader.load(getClass().getResource("/view/view_dealer_finance.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void companyFinance(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_company_finance.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    public ArrayList<ArrayList<String>> importNewDealerFinance()
    {
        objFetchServerData = new FetchServerData("new_dealer_finance");
        return objFetchServerData.getNewDealerFinance();
    }
}
