package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.MenuButton;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import model.GlobalVariables;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;


public class NavLive implements Initializable {

    @FXML
    private AnchorPane rootpane;

    @FXML
    private VBox vbox_style;

    @FXML
    private Button btn_live_dashboard;

    @FXML
    private Button btn_stock;

    @FXML
    private Button btn_expired_items;

    @FXML
    private Button btn_google_map;

    @FXML
    private Button btn_mail;

    @FXML
    private MenuButton btn_discount;

    @FXML
    private MenuButton btn_bonus;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        DashboardLive.btnView = btn_live_dashboard;
        ViewStock.btnView = btn_stock;
        UsersLocation.btnView = btn_google_map;
        ViewMails.btnView = btn_mail;
        SendMail.btnView = btn_mail;
        ViewCompaniesDiscount.btnView = btn_discount;
        ViewProductsDiscount.btnView = btn_discount;
        ViewDealersDiscount.btnView = btn_discount;
        ViewCompaniesBonus.btnView = btn_bonus;
        ViewProductsBonus.btnView = btn_bonus;
        ViewDealersBonus.btnView = btn_bonus;

        if(!GlobalVariables.userType.equals("Admin"))
        {
            btn_google_map.setVisible(false);
            btn_google_map.setManaged(false);
            vbox_style.setPrefHeight(150);
        }
    }

    @FXML
    void liveDashboard(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/dashboard_live.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void viewStock(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_stock.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void mail(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_mails.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void viewExpiredItems(ActionEvent event) throws IOException {

    }

    @FXML
    void googleMap(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/users_location.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void companiesBonus(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_companies_bonus.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void companiesDiscount(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_companies_discount.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void dealersBonus(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_dealers_bonus.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void dealersDiscount(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_dealers_discount.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void productsBonus(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_products_bonus.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void productsDiscount(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_products_discount.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
}
