package controller;

import com.jfoenix.controls.JFXDrawer;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.util.Callback;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.util.StringConverter;
import model.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;


import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.UnaryOperator;

public class EnterPurchaseReturn  implements Initializable{

    @FXML
    private TextField txt_purchretrnNo;
    @FXML
    private Label lbl_discAmount;
    @FXML
    private Label lbl_totalAmount;
    @FXML
    private Label lbl_retailPrice;
    @FXML
    private Label lbl_purchPrice;
    @FXML
    private Label lbl_tradePrice;
    @FXML
    private TextField txt_prodID;
    @FXML
    private TextField txt_supplierName;
    @FXML
    private TextField txt_compID;
    @FXML
    private TextField txt_prodName;
    @FXML
    private Button btn_add;
    @FXML
    private TextField txt_bonus;
    @FXML
    private  TextField txt_purchInvNo;
    @FXML
    private TextField txt_discount;
    @FXML
    private TextField txt_quant;
    @FXML
    private MenuBar menu_bar;
    @FXML
    private DatePicker datepick_invoicedate;
    @FXML
    private DatePicker datepick_curntDate;
    @FXML
    private DatePicker datepick_expiry;
    @FXML
    private TableView tableView;
    @FXML
    private TextField txt_Batchno;
    @FXML
    private ComboBox combo_packSize;
    @FXML    private ComboBox combo_return_reason;
    @FXML    private  Label lbl_stockQaunt;
    @FXML    private  Label lbl_stockBONUSQaunt;
    @FXML    private TextField txt_totaldiscAmount;
    @FXML    private TextField txt_grossAmount;
    @FXML    private TextField txt_supplierID;
    @FXML    private TextField txt_totalAmount;
    @FXML    private AnchorPane inner_anchor;
    @FXML    private Button btn11;
    @FXML    private BorderPane nav_purch;
    @FXML    private Pane btnpane;
    @FXML    private JFXDrawer drawer;
    @FXML    private FontAwesomeIconView FA_Icon;
    @FXML    private Button btn_savePurchinvoice;
    public static Button btnView;

    Alert a = new Alert(Alert.AlertType.NONE);

    String[] getlast_returnNum = new String[5];
    public void initialize(URL url, ResourceBundle resourceBundle) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnPurchase.setStyle("-fx-background-color: #47ab1e");
        datepick_curntDate.setValue(LocalDate.now());
        txt_purchretrnNo.requestFocus();

        datepick_invoicedate.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        datepick_expiry.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        datepick_curntDate.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        btnpane.setOnMouseEntered(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(22);
                btnpane.setMinWidth(20.0);
                btn11.setMinWidth(20.0);

            }
        });
        btnpane.setOnMouseExited(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(14);
                btnpane.setMinWidth(10.0);
                btn11.setMinWidth(10.0);
            }
        });

        inner_anchor.setLeftAnchor(inner_anchor, 260d);
        drawer.setSidePane(nav_purch);
        drawer.open();

        MysqlCon newobjMysqlCon = new MysqlCon();
        Statement objectStmt = newobjMysqlCon.stmt;
        Connection objectCon = newobjMysqlCon.con;
        PurchaseReturn object_purchreturn = new PurchaseReturn();

        getlast_returnNum = object_purchreturn.get_lastreturnnumber(objectStmt,objectCon);
        if(getlast_returnNum[0] == null)
        {
            txt_purchretrnNo.setText("1");
        }
        else
        {
            int cnvrtsalinvoint = Integer.parseInt(getlast_returnNum[0]);
            cnvrtsalinvoint = cnvrtsalinvoint + 1;
            txt_purchretrnNo.setText(String.valueOf(cnvrtsalinvoint));
        }
        ObservableList<String> prodPacking = FXCollections.observableArrayList();
        prodPacking.add( "Box");
        prodPacking.add("Packets");
        combo_packSize.setItems(prodPacking);
        combo_packSize.setValue("Packets");

        ObservableList<String> rtrnReasons = FXCollections.observableArrayList();
        prodPacking.add( "Breakage");
        prodPacking.add("Expiry");
        combo_return_reason.setItems(rtrnReasons);
        combo_return_reason.setValue("Expiry");

        TableColumn productcompIDCOL = new TableColumn("Company ID");
        productcompIDCOL.setMinWidth(200);
        productcompIDCOL.setVisible(false);
        TableColumn productIDCOL = new TableColumn("Product ID");
        productIDCOL.setMinWidth(200);
        productIDCOL.setVisible(false);
        TableColumn productnameCOL = new TableColumn("Product Name");
        productnameCOL.setMinWidth(200);
        TableColumn productbatchCOL = new TableColumn("Batch No.");
        productbatchCOL.setMinWidth(150);
        TableColumn productquantCOL = new TableColumn("Quantity");
        productquantCOL.setMinWidth(120);
        TableColumn productDiscountCOL = new TableColumn("Discount");
        productDiscountCOL.setMinWidth(100);
        TableColumn productBonusCOL = new TableColumn("Bonus");
        productBonusCOL.setMinWidth(150);
        TableColumn productexpiryCOL = new TableColumn("Expiry");
        productexpiryCOL.setMinWidth(120);
        TableColumn productTotalCOL = new TableColumn("Total");
        productTotalCOL.setMinWidth(100);
        TableColumn productpacksizeCOL = new TableColumn("Pack Size");
        productpacksizeCOL.setMinWidth(10);
        productpacksizeCOL.setVisible(false);
        TableColumn productdiscAmountCOL = new TableColumn("Discount Amount");
        productdiscAmountCOL.setMinWidth(10);
        productdiscAmountCOL.setVisible(false);
        TableColumn productprodstockCOL = new TableColumn("Quantity in Stock");
        productprodstockCOL.setMinWidth(10);
        productprodstockCOL.setVisible(false);
        TableColumn productprodbonusstockCOL = new TableColumn("Bonus Quantity in Stock");
        productprodbonusstockCOL.setMinWidth(10);
        productprodbonusstockCOL.setVisible(false);
        TableColumn productEditCOL = new TableColumn("Actions");
        productEditCOL.setMinWidth(200);
        TableColumn firstEDITCol = new TableColumn("Edit");
        TableColumn secondDELETECol = new TableColumn("Delete");

        productEditCOL.getColumns().addAll(firstEDITCol, secondDELETECol);

        tableView.getColumns().addAll(productcompIDCOL,productIDCOL, productnameCOL,productbatchCOL, productquantCOL,productDiscountCOL,productBonusCOL,productexpiryCOL,productTotalCOL,productpacksizeCOL,productdiscAmountCOL,productprodstockCOL,productprodbonusstockCOL,productEditCOL);

        productcompIDCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("productCompID"));
        productIDCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("productID"));
        productnameCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("productName"));
        productbatchCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("productBatch"));
        productquantCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("productQuant"));
        productDiscountCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("productDisc"));
        productBonusCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("productBonus"));
        productexpiryCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("productExpiry"));
        productTotalCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("totalValue"));
        productpacksizeCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("packSize"));
        productdiscAmountCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("discAmount"));
        productprodstockCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("stockQuant"));
        productprodbonusstockCOL.setCellValueFactory(new PropertyValueFactory<Product,String>("bonusstockQuant"));
        //START of Creating Delete Button
        secondDELETECol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Product,Boolean>, ObservableValue>() {
            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<Product, Boolean> param) {
                return new SimpleBooleanProperty(param.getValue()!=null);
            }

        });
        secondDELETECol.setCellFactory(new Callback<TableColumn<Product, Boolean>, TableCell<Product, Boolean>>() {
            @Override
            public TableCell<Product, Boolean> call(TableColumn<Product, Boolean> param) {
                return new ButtonCell(tableView);
            }
        });
        //END of Creating Delete Button
        //START of Creating EDIT Button
        firstEDITCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Product,Boolean>, ObservableValue>() {
            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<Product, Boolean> param) {
                return new SimpleBooleanProperty(param.getValue()!=null);
            }

        });
        firstEDITCol.setCellFactory(new Callback<TableColumn<Product, Boolean>, TableCell<Product, Boolean>>() {
            @Override
            public TableCell<Product, Boolean> call(TableColumn<Product, Boolean> param) {
                return new Editbtncell(tableView);
            }
        });
        //END of Creating EDIT Button

        tableView.setItems(data);
    }

    public void follow() throws IOException
    {
        if (drawer.isOpened()) {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(-300);
            transition.setByY(0);
            transition.setCycleCount(1);
            transition.play();
            drawer.close();
            FA_Icon.setGlyphName("ANGLE_DOUBLE_RIGHT");
            FA_Icon.setGlyphSize(14);
            //inner_anchor.setRightAnchor(inner_anchor,1d);


            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(-150);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(1.31017);
            scale.setToY(1);



            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();
            //
            //inner_anchor.setLeftAnchor(inner_anchor, 5d);
        } else {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(300);
            transition.setByY(0);
            transition.setCycleCount(1);
            FA_Icon.setGlyphName("ANGLE_DOUBLE_LEFT");
            FA_Icon.setGlyphSize(14);
            drawer.open();

            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(150);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(0.99947777);
            scale.setToY(1);



            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();

        }


    }
    int EDIT_CHK_Value = 0;
    ObservableList<Product> data = FXCollections.observableArrayList();
    int rowvalueAfterEdit = 0;
    // START of Button Code for Editing a Row
    private class Editbtncell extends TableCell<Product,Boolean>{
        final Button btncell = new Button("Edit");

        public  Editbtncell(final TableView view1){
            btncell.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    rowvalueAfterEdit = getTableRow().getIndex();
                    Product newobj = (Product) tableView.getItems().get(rowvalueAfterEdit);
                    txt_compID.setText(newobj.getProductCompID());
                    txt_prodName.setText(newobj.getProductName());
                    txt_prodID.setText(newobj.getProductId());
                    txt_quant.setText(newobj.getProductQuant());
                    txt_bonus.setText(newobj.getProductBonus());
                    txt_discount.setText(newobj.getProductDisc());
                    lbl_totalAmount.setText(newobj.getTotalValue());
                    combo_packSize.setValue(newobj.getPackSize());
                    txt_compID.setEditable(false);
                    txt_prodID.setEditable(false);
                    txt_prodName.setEditable(false);
                    EDIT_CHK_Value = 1;
                }
            });
        }
        @Override
        protected  void updateItem(Boolean n, boolean boln){
            super.updateItem(n,boln);
            if(boln){
                setGraphic(null);
            }
            else
            {
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setGraphic(btncell);
            }
        }
    }
    // END of Button Code for Editing a Row
    //START of Button Code for Deleting a Row
    private class ButtonCell extends TableCell<Product, Boolean> {

        final Button CellButton = new Button("Delete");
        public ButtonCell(final TableView view) {
            CellButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    int selectedIndex = getTableRow().getIndex();
                    data.remove(selectedIndex);
                    ///////////////////////////START OF CALCULATIONS TO GET TOTAL amount and total Discount amount OF ALL PRODUCT ADDED IN TBALEVIEW
                    Float sumval =0.0f;
                    Float finalsumofDiscount2 = 0.0f;
                    Float nettotalamount = 0.0f;
                    for(int i = 0; i<tableView.getItems().size();i++)
                    {
                        Product newwobj = (Product) tableView.getItems().get(i);
                        float newval = Float.parseFloat(newwobj.getTotalValue());
                        float finalsumofDiscount = Float.parseFloat(newwobj.getDiscAmount());
                        finalsumofDiscount2 = finalsumofDiscount2 + finalsumofDiscount;
                        sumval = sumval + newval;
                        nettotalamount = sumval + finalsumofDiscount2;
                    }
                    txt_totalAmount.setText(String.valueOf(sumval));
                    txt_totaldiscAmount.setText(String.valueOf(finalsumofDiscount2));
                    txt_grossAmount.setText(String.valueOf(nettotalamount));
                    txt_compID.requestFocus();
                    /////////////////////////////////END OF CALCULATIONS TO GET TOTAL amount and total Discount amount OF ALL PRODUCT ADDED IN TBALEVIEW
                }
            });
        }
        @Override
        protected void updateItem(Boolean t, boolean bln) {
            super.updateItem(t, bln);
            if (bln) {
                setGraphic(null);
            } else {
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setGraphic(CellButton);
            }

        }
    }
    //END of Button Code for Deleting a Row

    //////////////////////////////////////////////////////////////////////////////////START of Add Button Click Code
    public void  addbtn_click() throws IOException{
        if(lbl_totalAmount.getText() == "0.00")
        {
            if(txt_discount.getText()=="0")
            {
                if (txt_quant.getText() != "")
                {
                    float getretail_floatval = Float.parseFloat(lbl_tradePrice.getText());
                    int intcnvrt_quant = Integer.parseInt(txt_quant.getText());

                    float totalquantity_price = intcnvrt_quant * getretail_floatval;
                    lbl_totalAmount.setText(String.valueOf(totalquantity_price));
                    btn_add.requestFocus();
                }
            }
            else
            {
                if (!txt_quant.getText().isEmpty() && txt_discount.getText()!="0")
                {
                    float intgetretail_price = Integer.parseInt(lbl_tradePrice.getText());
                    int intcnvrt_quant = Integer.parseInt(txt_quant.getText());
                    float intdiscount_value = Integer.parseInt(txt_discount.getText());
                    float totalquantity_price = intcnvrt_quant * intgetretail_price;
                    float disc_firstVal = (totalquantity_price * intdiscount_value) / 100;
                    lbl_discAmount.setText(String.valueOf(disc_firstVal));
                    float final_discVal = totalquantity_price - disc_firstVal;
                    lbl_totalAmount.setText(String.valueOf(final_discVal));
                    btn_add.requestFocus();
                }
                else
                {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setHeaderText("Input Field Empty");
                    a.setContentText("Please Enter Quantity");
                    a.show();
                    txt_quant.requestFocus();
                }
            }
        }
        if(EDIT_CHK_Value == 1)
        {
            Product newobj = (Product) tableView.getItems().get(rowvalueAfterEdit);

            newobj.setProductBatch(txt_Batchno.getText());
            newobj.setProductBonus(txt_bonus.getText());
            newobj.setProductDisc(txt_discount.getText());
            newobj.setProductQuant(txt_quant.getText());
            newobj.setProductExpiry(datepick_expiry.getValue().toString());
            newobj.setTotalValue(lbl_totalAmount.getText());
            newobj.setPackSize(combo_packSize.getValue().toString());
            newobj.setDiscAmount(lbl_discAmount.getText());
            newobj.setStockQuant(lbl_stockQaunt.getText());
            newobj.setBonusstockQuant(lbl_stockBONUSQaunt.getText());
            tableView.refresh();
            txt_prodID.setEditable(true);
            txt_prodName.setEditable(true);
            txt_prodID.setText("");
            txt_prodName.setText("");
            txt_quant.setText("");
            txt_bonus.setText("");
            txt_Batchno.setText("");
            txt_discount.setText("");
            txt_compID.setText("");
            combo_packSize.setValue("Packs");
            lbl_totalAmount.setText("0.00");
            lbl_tradePrice.setText("0.00");
            lbl_purchPrice.setText("0.00");
            lbl_retailPrice.setText("0.00");
            lbl_discAmount.setText("0.00");
            datepick_expiry.setValue(LocalDate.now());
            lbl_stockQaunt.setText("-");
            lbl_stockBONUSQaunt.setText("-");
            ///////////////////////////START OF CALCULATIONS TO GET TOTAL amount and total Discount amount OF ALL PRODUCT ADDED IN TBALEVIEW
            Float sumval =0.0f;
            Float finalsumofDiscount2 = 0.0f;
            Float nettotalamount = 0.0f;
            for(int i = 0; i<tableView.getItems().size();i++)
            {
                Product newwobj = (Product) tableView.getItems().get(i);
                float newval = Float.parseFloat(newwobj.getTotalValue());
                float finalsumofDiscount = Float.parseFloat(newwobj.getDiscAmount());
                finalsumofDiscount2 = finalsumofDiscount2 + finalsumofDiscount;
                sumval = sumval + newval;
                nettotalamount = sumval + finalsumofDiscount2;
            }
            txt_totalAmount.setText(String.valueOf(sumval));
            txt_totaldiscAmount.setText(String.valueOf(finalsumofDiscount2));
            txt_grossAmount.setText(String.valueOf(nettotalamount));
            txt_compID.requestFocus();
            /////////////////////////////////END OF CALCULATIONS TO GET TOTAL amount and total Discount amount OF ALL PRODUCT ADDED IN TBALEVIEW
            //txt_quant1.setText(String.valueOf(sumval));
            txt_prodID.requestFocus();
            EDIT_CHK_Value = 0;
        }
        else {
            if (txt_prodName.getText().isEmpty()) {
                txt_prodName.setText("ERROR");
            } else if (txt_prodID.getText().isEmpty() || txt_quant.getText().isEmpty()) {
                txt_prodID.setText("ERROR");
            } else if (txt_quant.getText().isEmpty()) {
                txt_quant.setText("ERROR");
            }
            /*if(txt_bonus.getText().isEmpty()){
                txt_bonus.setText("ERROR");
            }*/
            else {
                Product objnew = new Product();
                objnew.setProductId(txt_prodID.getText());
                objnew.setProductCompID(txt_compID.getText());
                objnew.setProductName(txt_prodName.getText());
                objnew.setProductBatch(txt_Batchno.getText());
                objnew.setProductQuant(txt_quant.getText());
                objnew.setProductBonus(txt_bonus.getText());
                objnew.setProductDisc(txt_discount.getText());
                objnew.setProductExpiry(datepick_expiry.getValue().toString());
                objnew.setTotalValue(lbl_totalAmount.getText());
                objnew.setPackSize(combo_packSize.getValue().toString());
                objnew.setDiscAmount(lbl_discAmount.getText());
                objnew.setStockQuant(lbl_stockQaunt.getText());
                objnew.setBonusstockQuant(lbl_stockBONUSQaunt.getText());

                tableView.getItems().add(objnew);
                txt_prodID.setText("");
                txt_prodName.setText("");
                txt_quant.setText("");
                txt_bonus.setText("0");
                txt_discount.setText("0");
                txt_compID.setText("");
                txt_Batchno.setText("");
                combo_packSize.setValue("Packs");
                lbl_totalAmount.setText("0.00");
                lbl_tradePrice.setText("0.00");
                lbl_purchPrice.setText("0.00");
                lbl_retailPrice.setText("0.00");
                lbl_discAmount.setText("0.00");
                datepick_expiry.setValue(LocalDate.now());
                lbl_stockQaunt.setText("-");
                lbl_stockBONUSQaunt.setText("-");

                ///////////////////////////START OF CALCULATIONS TO GET TOTAL amount and total Discount amount OF ALL PRODUCT ADDED IN TBALEVIEW
                Float sumval =0.0f;
                Float finalsumofDiscount2 = 0.0f;
                Float nettotalamount = 0.0f;
                for(int i = 0; i<tableView.getItems().size();i++)
                {
                    Product newwobj = (Product) tableView.getItems().get(i);
                    float newval = Float.parseFloat(newwobj.getTotalValue());
                    float finalsumofDiscount = Float.parseFloat(newwobj.getDiscAmount());
                    finalsumofDiscount2 = finalsumofDiscount2 + finalsumofDiscount;
                    sumval = sumval + newval;
                    nettotalamount = sumval + finalsumofDiscount2;
                }
                txt_totalAmount.setText(String.valueOf(sumval));
                txt_totaldiscAmount.setText(String.valueOf(finalsumofDiscount2));
                txt_grossAmount.setText(String.valueOf(nettotalamount));
                txt_compID.requestFocus();
                /////////////////////////////////END OF CALCULATIONS TO GET TOTAL amount and total Discount amount OF ALL PRODUCT ADDED IN TBALEVIEW
                //txt_quant1.setText(String.valueOf(sumval));
                txt_prodID.requestFocus();

            }
        }
    }
    /////////////////////////////////////////////////////////////////////////////////////END of Add Button Click Code
    //////////////////////////////////////////////////////////////////////////////////START of Add Button ENTER Press Code
    public void  addbtn_enterpress(KeyEvent event) throws IOException {
        if (event.getCode() == KeyCode.ENTER ) {
            if (lbl_totalAmount.getText() == "0.00") {
                if (txt_discount.getText() == "0") {
                    if (txt_quant.getText() != "") {
                        float getretail_floatval = Float.parseFloat(lbl_tradePrice.getText());
                        int intcnvrt_quant = Integer.parseInt(txt_quant.getText());

                        float totalquantity_price = intcnvrt_quant * getretail_floatval;
                        lbl_totalAmount.setText(String.valueOf(totalquantity_price));
                        btn_add.requestFocus();
                    }
                } else {
                    if (!txt_quant.getText().isEmpty() && txt_discount.getText() != "0") {
                        float intgetretail_price = Integer.parseInt(lbl_tradePrice.getText());
                        int intcnvrt_quant = Integer.parseInt(txt_quant.getText());
                        float intdiscount_value = Integer.parseInt(txt_discount.getText());
                        float totalquantity_price = intcnvrt_quant * intgetretail_price;
                        float disc_firstVal = (totalquantity_price * intdiscount_value) / 100;
                        lbl_discAmount.setText(String.valueOf(disc_firstVal));
                        float final_discVal = totalquantity_price - disc_firstVal;
                        lbl_totalAmount.setText(String.valueOf(final_discVal));
                        btn_add.requestFocus();
                    } else {
                        a.setAlertType(Alert.AlertType.ERROR);
                        a.setHeaderText("Input Field Empty");
                        a.setContentText("Please Enter Quantity");
                        a.show();
                        txt_quant.requestFocus();
                    }
                }
            }
            if (EDIT_CHK_Value == 1) {
                Product newobj = (Product) tableView.getItems().get(rowvalueAfterEdit);

                newobj.setProductBatch(txt_Batchno.getText());
                newobj.setProductBonus(txt_bonus.getText());
                newobj.setProductDisc(txt_discount.getText());
                newobj.setProductQuant(txt_quant.getText());
                newobj.setProductExpiry(datepick_expiry.getValue().toString());
                newobj.setTotalValue(lbl_totalAmount.getText());
                newobj.setPackSize(combo_packSize.getValue().toString());
                newobj.setDiscAmount(lbl_discAmount.getText());
                newobj.setStockQuant(lbl_stockQaunt.getText());
                newobj.setBonusstockQuant(lbl_stockBONUSQaunt.getText());
                tableView.refresh();
                txt_prodID.setEditable(true);
                txt_prodName.setEditable(true);
                txt_prodID.setText("");
                txt_prodName.setText("");
                txt_quant.setText("");
                txt_bonus.setText("");
                txt_Batchno.setText("");
                txt_discount.setText("");
                txt_compID.setText("");
                combo_packSize.setValue("Packs");
                lbl_totalAmount.setText("0.00");
                lbl_tradePrice.setText("0.00");
                lbl_purchPrice.setText("0.00");
                lbl_retailPrice.setText("0.00");
                lbl_discAmount.setText("0.00");
                datepick_expiry.setValue(LocalDate.now());
                lbl_stockQaunt.setText("-");
                lbl_stockBONUSQaunt.setText("-");
                ///////////////////////////START OF CALCULATIONS TO GET TOTAL amount and total Discount amount OF ALL PRODUCT ADDED IN TBALEVIEW
                Float sumval = 0.0f;
                Float finalsumofDiscount2 = 0.0f;
                Float nettotalamount = 0.0f;
                for (int i = 0; i < tableView.getItems().size(); i++) {
                    Product newwobj = (Product) tableView.getItems().get(i);
                    float newval = Float.parseFloat(newwobj.getTotalValue());
                    float finalsumofDiscount = Float.parseFloat(newwobj.getDiscAmount());
                    finalsumofDiscount2 = finalsumofDiscount2 + finalsumofDiscount;
                    sumval = sumval + newval;
                    nettotalamount = sumval + finalsumofDiscount2;
                }
                txt_totalAmount.setText(String.valueOf(sumval));
                txt_totaldiscAmount.setText(String.valueOf(finalsumofDiscount2));
                txt_grossAmount.setText(String.valueOf(nettotalamount));
                txt_compID.requestFocus();
                /////////////////////////////////END OF CALCULATIONS TO GET TOTAL amount and total Discount amount OF ALL PRODUCT ADDED IN TBALEVIEW
                //txt_quant1.setText(String.valueOf(sumval));
                txt_prodID.requestFocus();
                EDIT_CHK_Value = 0;
            } else {
                if (txt_prodName.getText().isEmpty()) {
                    txt_prodName.setText("ERROR");
                } else if (txt_prodID.getText().isEmpty() || txt_quant.getText().isEmpty()) {
                    txt_prodID.setText("ERROR");
                } else if (txt_quant.getText().isEmpty()) {
                    txt_quant.setText("ERROR");
                }
            /*if(txt_bonus.getText().isEmpty()){
                txt_bonus.setText("ERROR");
            }*/
                else {
                    Product objnew = new Product();
                    objnew.setProductId(txt_prodID.getText());
                    objnew.setProductCompID(txt_compID.getText());
                    objnew.setProductName(txt_prodName.getText());
                    objnew.setProductBatch(txt_Batchno.getText());
                    objnew.setProductQuant(txt_quant.getText());
                    objnew.setProductBonus(txt_bonus.getText());
                    objnew.setProductDisc(txt_discount.getText());
                    objnew.setProductExpiry(datepick_expiry.getValue().toString());
                    objnew.setTotalValue(lbl_totalAmount.getText());
                    objnew.setPackSize(combo_packSize.getValue().toString());
                    objnew.setDiscAmount(lbl_discAmount.getText());
                    objnew.setStockQuant(lbl_stockQaunt.getText());
                    objnew.setBonusstockQuant(lbl_stockBONUSQaunt.getText());

                    tableView.getItems().add(objnew);
                    txt_prodID.setText("");
                    txt_prodName.setText("");
                    txt_quant.setText("");
                    txt_bonus.setText("0");
                    txt_discount.setText("0");
                    txt_compID.setText("");
                    txt_Batchno.setText("");
                    combo_packSize.setValue("Packs");
                    lbl_totalAmount.setText("0.00");
                    lbl_tradePrice.setText("0.00");
                    lbl_purchPrice.setText("0.00");
                    lbl_retailPrice.setText("0.00");
                    lbl_discAmount.setText("0.00");
                    datepick_expiry.setValue(LocalDate.now());
                    lbl_stockQaunt.setText("-");
                    lbl_stockBONUSQaunt.setText("-");

                    ///////////////////////////START OF CALCULATIONS TO GET TOTAL amount and total Discount amount OF ALL PRODUCT ADDED IN TBALEVIEW
                    Float sumval = 0.0f;
                    Float finalsumofDiscount2 = 0.0f;
                    Float nettotalamount = 0.0f;
                    for (int i = 0; i < tableView.getItems().size(); i++) {
                        Product newwobj = (Product) tableView.getItems().get(i);
                        float newval = Float.parseFloat(newwobj.getTotalValue());
                        float finalsumofDiscount = Float.parseFloat(newwobj.getDiscAmount());
                        finalsumofDiscount2 = finalsumofDiscount2 + finalsumofDiscount;
                        sumval = sumval + newval;
                        nettotalamount = sumval + finalsumofDiscount2;
                    }
                    txt_totalAmount.setText(String.valueOf(sumval));
                    txt_totaldiscAmount.setText(String.valueOf(finalsumofDiscount2));
                    txt_grossAmount.setText(String.valueOf(nettotalamount));
                    txt_compID.requestFocus();
                    /////////////////////////////////END OF CALCULATIONS TO GET TOTAL amount and total Discount amount OF ALL PRODUCT ADDED IN TBALEVIEW
                    //txt_quant1.setText(String.valueOf(sumval));
                    txt_prodID.requestFocus();

                }
            }
        }
    }
    /////////////////////////////////////////////////////////////////////////////////////END of Add Button ENTER Press Code
    public void btnsave_clicks() throws IOException
    {
        int counttableROWS = tableView.getItems().size();
        String[] getprodid_tableview = new String[counttableROWS];
        String[] getprodName_tableview = new String[counttableROWS];
        String[] getbatchNum_tableview = new String[counttableROWS];
        String[] getprodCompid_tableview = new String[counttableROWS];
        String[] getprodQuant_tableview = new String[counttableROWS];
        String[] getprodBonus_tableview = new String[counttableROWS];
        String[] getprodDisc_tableview = new String[counttableROWS];
        String[] getpacksize_tableview = new String[counttableROWS];
        String[] getquant_inStock_tableview = new String[counttableROWS];
        String[] getbonusquant_inStock_tableview = new String[counttableROWS];
        String[] getprodTotal_table = new String[counttableROWS];
        String[] getdiscAmount_tableview = new String[counttableROWS];
        PurchaseReturn objectPurchReturn  = new PurchaseReturn();

        for(int i =0;i<counttableROWS;i++) //////////////////////////////////////START OF LOOP TO READ ALL ROWS OF TABLVIEW
        {
            Product ProductObject = (Product) tableView.getItems().get(i);
            getprodid_tableview[i] = ProductObject.getProductId();
            getprodCompid_tableview[i] = ProductObject.getProductCompID();
            getprodQuant_tableview[i] = ProductObject.getProductQuant();
            getprodBonus_tableview[i] = ProductObject.getProductBonus();
            getprodDisc_tableview[i] = ProductObject.getProductDisc();
            getpacksize_tableview[i] = ProductObject.getPackSize();
            getquant_inStock_tableview[i] = ProductObject.getStockQuant();
            getbonusquant_inStock_tableview[i] = ProductObject.getBonusstockQuant();
            getprodTotal_table[i] = ProductObject.getTotalValue();
            getdiscAmount_tableview[i] = ProductObject.getDiscAmount();
            getbatchNum_tableview[i] = ProductObject.getProductBatch();

            int quantityReturn = Integer.parseInt(getprodQuant_tableview[i]);
            int bonusReturn = Integer.parseInt(getprodBonus_tableview[i]);
            int productId = Integer.parseInt(getprodid_tableview[i]);
            int stockprodQuantity = Integer.parseInt(getquant_inStock_tableview[i]);
            int stockbonusQuantity = Integer.parseInt(getbonusquant_inStock_tableview[i]);

            int remainingProdQuantity = stockprodQuantity - quantityReturn;
            int remainingBonusQuantity  = stockbonusQuantity - bonusReturn;

            int purchasedQuantity = 0;

            if(!txt_purchInvNo.getText().equals("") || !txt_purchInvNo.getText().isEmpty())
            {
                MysqlCon sqlConnction = new MysqlCon();
                Statement stamentsObject = sqlConnction.stmt;
                Connection ConnctnObject  = sqlConnction.con;
                getPurchInvoDetails = objectPurchReturn.getpprchaseDetails(stamentsObject,ConnctnObject,productId,txt_purchInvNo.getText());
                if(getPurchInvoDetails[0] == null)
                { }
                else
                {
                    String prch_invodetail[] = getPurchInvoDetails[0].split("--");
                    purchasedQuantity = Integer.parseInt(prch_invodetail[2]);
                }
                if(purchasedQuantity == quantityReturn)
                {
                    MysqlCon objMsqlCon1 = new MysqlCon();
                    Statement objctStmt1 = objMsqlCon1.stmt;
                    Connection objctCon1 = objMsqlCon1.con;
                    objectPurchReturn.updatereturnBit(objctStmt1, objctCon1, productId, quantityReturn ,bonusReturn,txt_purchInvNo.getText());
                }
            }
            String[] batchQuantities = new String[1];
            int batchStockQuant = 0;
            int batchStockBonusQuant = 0;
            int remaining_batchStockQuant = 0;
            int remaining_batchStockBonusQuant = 0;
            MysqlCon mysqlOBJ = new MysqlCon();
            Statement mystamtOBJ = mysqlOBJ.stmt;
            Connection mysqlConn = mysqlOBJ.con;
            PurchaseReturn prchrtrn = new PurchaseReturn();
            batchQuantities = prchrtrn.getprodbatch_quantity(mystamtOBJ, mysqlConn, productId, getbatchNum_tableview[i]);
            if (batchQuantities[0] == null) {}
            else
                {
                    String prch_invodetail[] = batchQuantities[0].split("--");
                    batchStockQuant = Integer.parseInt(prch_invodetail[0]);
                    batchStockBonusQuant = Integer.parseInt(prch_invodetail[1]);
                    remaining_batchStockQuant = batchStockQuant  - quantityReturn;
                    remaining_batchStockBonusQuant = batchStockBonusQuant - bonusReturn;
                }


            MysqlCon objMsqlCon1 = new MysqlCon();
            Statement objctStmt1 = objMsqlCon1.stmt;
            Connection objctCon1 = objMsqlCon1.con;
            objectPurchReturn.UPDATE_batchStock(objctStmt1, objctCon1, productId, remaining_batchStockQuant ,remaining_batchStockBonusQuant,getbatchNum_tableview[i]);

            MysqlCon objctMsqlCon1 = new MysqlCon();
            Statement objectStmt1 = objctMsqlCon1.stmt;
            Connection objectCon1 = objctMsqlCon1.con;
            objectPurchReturn.subtract_quantFROMStock(objectStmt1, objectCon1, productId, remainingProdQuantity ,remainingBonusQuantity);

            float discountValue = Float.parseFloat(getprodDisc_tableview[i]);
            float totalValue = Float.parseFloat(getprodTotal_table[i]);

            MysqlCon objMysqlConct = new MysqlCon();
            Statement objStmt = objMysqlConct.stmt;
            Connection objContn = objMysqlConct.con;
            objectPurchReturn.insertPurchReturnInfodetails(objStmt, objContn,Integer.parseInt(txt_purchretrnNo.getText()),productId,getbatchNum_tableview[i],quantityReturn,bonusReturn,discountValue,totalValue,getpacksize_tableview[i],combo_return_reason.getValue().toString());
        }
        String InvoNumber ="";
        if(!txt_purchInvNo.getText().isEmpty())
        {
            InvoNumber = "null";
        }
        else
        {
            InvoNumber = txt_purchInvNo.getText();
        }
        String purchase_date = GlobalVariables.getStDate();
        String purchase_time  = GlobalVariables.getStTime();
        int purchase_by = Integer.parseInt(GlobalVariables.getUserId());
        String purhcasestatus = "Active";
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        objectPurchReturn.insertpurchaseInvoiceInfo(objStmt, objCon,Integer.parseInt(txt_purchretrnNo.getText()),InvoNumber, txt_compID.getText(),Integer.parseInt(txt_supplierID.getText()),Float.parseFloat(txt_totalAmount.getText()),Float.parseFloat(txt_grossAmount.getText()),purchase_date,purchase_time,purchase_date,purchase_time,purchase_by,purhcasestatus);

        Parent root = FXMLLoader.load(getClass().getResource("/view/purch_returns.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);

    }

    public  void  btnsave_enterpress(KeyEvent event) throws  IOException     {
        if(event.getCode() == KeyCode.ENTER)
        {
            btnsave_clicks();
        }
    }
    /////////////////////////////////////////////////////////////////////////// START of ENter Press on Supplier Name
    String Supdetails_onSUPPNameEnter[] = new String[1];
    public void enterpress_SupplierName(KeyEvent event) throws IOException {
        if(event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB) {
            if(!txt_supplierName.getText().equals(""))
            {
                String Supp_name = txt_supplierName.getText();
                MysqlCon objMysqlCon = new MysqlCon();
                Statement objStmt = objMysqlCon.stmt;
                Connection objCon = objMysqlCon.con;
                PurchaseInvoice prchInvoObj = new PurchaseInvoice();
                Supdetails_onSUPPNameEnter = prchInvoObj.getSuppDetails_SupName(objStmt, objCon, Supp_name);
                if (Supdetails_onSUPPNameEnter[0] == null) {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setHeaderText("Error!");
                    a.setContentText("Wrong Supplier Name.");
                    a.show();
                    txt_supplierName.clear();
                    txt_supplierName.requestFocus();
                }
                else {
                    txt_supplierID.setText(Supdetails_onSUPPNameEnter[0]);
                    /*
                    String[] myArray = proddetails_onSUPPNameEnter[0].split("--");
                    txt_compID.setText(myArray[1]);
                    lbl_retailPrice.setText(myArray[2]);
                    lbl_purchPrice.setText(myArray[3]);
                    lbl_tradePrice.setText(myArray[4]);*/
                }

                txt_purchInvNo.requestFocus();
            }

            else
            {
                a.setAlertType(Alert.AlertType.ERROR);
                a.setHeaderText("Error!");
                a.setContentText("Product Name not entered.");
                a.show();
                txt_prodName.requestFocus();
            }
        }
    }
    /////////////////////////////////////////////////////////////////////////// END of Enter Press on Supplier Name
    /////////////////////////////////////////////////////////////////////////// START of Enter Press on Supplier ID
    String[] suppdetails_onIDEnter = new String[1];
    public void keypress_SuppID(KeyEvent event) throws IOException{
        if(event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB) {
            String txtbx_suppID = txt_supplierID.getText();

            MysqlCon objMysqlCon = new MysqlCon();
            Statement objStmt = objMysqlCon.stmt;
            Connection objCon = objMysqlCon.con;
            PurchaseInvoice objpurchinvoice = new PurchaseInvoice();
            suppdetails_onIDEnter = objpurchinvoice.get_SuppDetails(objStmt, objCon, txtbx_suppID);
            if (suppdetails_onIDEnter[0] == null) {
            } else {
                String[] myArray = suppdetails_onIDEnter[0].split("--");
                txt_supplierName.setText(myArray[0]);
                txt_purchInvNo.requestFocus();
            }
        }
    }
    /////////////////////////////////////////////////////////////////////////// END of Enter Press on Supplier ID

/////////////////////////////////////////////////////////////////////////// START of Enter Press on Purhcase Invoice Number
    String PurchInvoice_detailArray[] = new String[1];
    String read_supplierID;
    String supplierdetailArray[] = new String[1];
    public void getPurch_InvoiceDetails(KeyEvent event) throws IOException, ParseException {
        if(event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB) {
            if(txt_purchInvNo.getText().isEmpty())
            {
                a.setAlertType(AlertType.ERROR);
                a.setHeaderText("Input Field Empty");
                a.setContentText("Please Enter Invoice Number.");
                a.show();
                txt_purchInvNo.requestFocus();
            }
            else
            {
            PurchaseReturn objectpurchretrn = new PurchaseReturn();
            MysqlCon newobjMysqlCon1 = new MysqlCon();
            Statement newobjStmt1 = newobjMysqlCon1.stmt;
            Connection newobjCon1 = newobjMysqlCon1.con;
            PurchInvoice_detailArray = objectpurchretrn.getinvodetails(newobjStmt1, newobjCon1, txt_purchInvNo.getText());
            if (PurchInvoice_detailArray[0] == null) {
                a.setAlertType(Alert.AlertType.ERROR);
                a.setHeaderText("Error!");
                a.setContentText("Entered Purchase Invoice Number not Found");
                a.show();
                txt_purchInvNo.requestFocus();
            } else {
                String[] Invoice_detail = PurchInvoice_detailArray[0].split("--");
                txt_purchretrnNo.setText(Invoice_detail[0]);
                read_supplierID = Invoice_detail[1];
                txt_supplierID.setText(Invoice_detail[1]);

                  String[] splitdate = Invoice_detail[2].split("/");
                        String nsss = splitdate[0] +'-' + splitdate[1] +'-'+ splitdate[2];
                        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
                        Date varDate=dateFormat.parse(nsss);
                        dateFormat=new SimpleDateFormat("dd-MM-yyyy");
                        String mydate = dateFormat.format(varDate);

                        String[] sdate = mydate.split("-");
                        int yyear = Integer.parseInt(sdate[2]);
                        int montth = Integer.parseInt(sdate[1]);
                        int ddate = Integer.parseInt(sdate[0]);

                datepick_invoicedate.setValue(LocalDate.of(yyear,montth,ddate));

                txt_prodID.requestFocus();
            }
            MysqlCon mysqlconection = new MysqlCon();
            Statement statmntObject = mysqlconection.stmt;
            Connection connctionObject = mysqlconection.con;
            supplierdetailArray = objectpurchretrn.getdealersname(statmntObject, connctionObject, read_supplierID);
            if (supplierdetailArray[0] == null) {
            } else {
                String[] Sup_detail = supplierdetailArray[0].split("--");
                txt_supplierName.setText(Sup_detail[0]);
            }
        }
        }
    }
    /////////////////////////////////////////////////////////////////////////// END of Enter Press on Purhcase Invoice Number
    /////////////////////////////////////////////////////////////////////////// START of Enter Press on Product ID
    String proddetails_onIDEnter[] = new String[1];
    String prodquantinstock_onIDEnter[] = new String[1];
    String getPurchInvoDetails[] = new String[5];
    public void enterpress_prodID(KeyEvent event) throws IOException, ParseException {
        if(event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB)
        {
            PurchaseReturn objpurchretrn = new PurchaseReturn();

            String product_ids = txt_prodID.getText();
            int intprod_id = Integer.parseInt(product_ids);
            MysqlCon objMysqlCon1 = new MysqlCon();
            Statement objStmt1 = objMysqlCon1.stmt;
            Connection objCon1 = objMysqlCon1.con;

            proddetails_onIDEnter = objpurchretrn.get_prodDetailswithIDs(objStmt1, objCon1, intprod_id);
            if(proddetails_onIDEnter[0]==null)
            {
                a.setAlertType(Alert.AlertType.ERROR);
                a.setHeaderText("ERROR!");
                a.setContentText("Wrong Product Code Enter.");
                a.show();
                txt_prodID.clear();
                txt_prodID.requestFocus();
            }
            else
                {
                    String[] myArray = proddetails_onIDEnter[0].split("--");
                    txt_prodName.setText(myArray[0]);
                    lbl_retailPrice.setText(myArray[1]);
                    lbl_purchPrice.setText(myArray[2]);
                    lbl_tradePrice.setText(myArray[3]);
                    txt_compID.setText(myArray[4]);
                    txt_Batchno.requestFocus();

                    if(!txt_purchInvNo.getText().equals("") || !txt_purchInvNo.getText().isEmpty())
                    {
                        MysqlCon sqlConnction = new MysqlCon();
                        Statement stamentsObject = sqlConnction.stmt;
                        Connection ConnctnObject  = sqlConnction.con;
                        getPurchInvoDetails = objpurchretrn.getpprchaseDetails(stamentsObject,ConnctnObject,intprod_id,txt_purchInvNo.getText());
                        if(getPurchInvoDetails[0] == null)
                        {

                        }
                        else
                        {
                            String prch_invodetail[] = getPurchInvoDetails[0].split("--");
                            txt_discount.setText(prch_invodetail[0]);
                            txt_bonus.setText(prch_invodetail[1]);
                            txt_quant.setText(prch_invodetail[2]);
                            txt_Batchno.setText(prch_invodetail[3]);
                            String[] splitdate = prch_invodetail[4].split("/");
                            String nsss = splitdate[0] +'-' + splitdate[1] +'-'+ splitdate[2];
                            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
                            Date varDate=dateFormat.parse(nsss);
                            dateFormat=new SimpleDateFormat("dd-MM-yyyy");
                            String mydate = dateFormat.format(varDate);

                            String[] sdate = mydate.split("-");
                            int yyear = Integer.parseInt(sdate[2]);
                            int montth = Integer.parseInt(sdate[1]);
                            int ddate = Integer.parseInt(sdate[0]);

                            datepick_expiry.setValue(LocalDate.of(yyear,montth,ddate));

                        }
                    }

                    MysqlCon objMysqlCon = new MysqlCon();
                    Statement objStmt = objMysqlCon.stmt;
                    Connection objCon = objMysqlCon.con;
                    prodquantinstock_onIDEnter = objpurchretrn.getprodstockquant_withIDs(objStmt, objCon,   intprod_id);
                    if(prodquantinstock_onIDEnter[0]==null){}
                    else
                    {
                        String[] prodStock_quantarr = prodquantinstock_onIDEnter[0].split("--");
                        lbl_stockQaunt.setText(prodStock_quantarr[0]);
                        lbl_stockBONUSQaunt.setText(prodStock_quantarr[1]);
                    }

                }


        }
    }
    /////////////////////////////////////////////////////////////////////////// END of Enter Press on Product ID
/////////////////////////////////////////////////////////////////////////// START of ENter Press on Product Name
    String proddetails_onprodNameEnter[] = new String[1];
    public void enterpress_prodName(KeyEvent event) throws IOException {
        if(event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB) {
            if(!txt_prodName.getText().equals("") || !txt_prodName.getText().isEmpty())
            {
                String product_name = txt_prodName.getText();
                MysqlCon objMysqlCon = new MysqlCon();
                Statement objStmt = objMysqlCon.stmt;
                Connection objCon = objMysqlCon.con;
                SaleInvoice objsaleinvoice = new SaleInvoice();
                proddetails_onprodNameEnter = objsaleinvoice.get_prodDetailswithname(objStmt, objCon, product_name);
                if (proddetails_onprodNameEnter[0] == null) {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setHeaderText("Error!");
                    a.setContentText("Wrong Product Name.");
                    a.show();
                    txt_prodName.clear();
                    txt_prodName.requestFocus();
                }
                else {
                    String[] myArray = proddetails_onprodNameEnter[0].split("--");
                    txt_compID.setText(myArray[1]);
                    txt_prodID.setText(myArray[0]);
                    lbl_retailPrice.setText(myArray[2]);
                    lbl_purchPrice.setText(myArray[3]);
                    lbl_tradePrice.setText(myArray[4]);

                    int intprod_id = Integer.parseInt(txt_prodID.getText());

                    MysqlCon objectMysqlCon = new MysqlCon();
                    Statement objectStmt = objectMysqlCon.stmt;
                    Connection objectCon = objectMysqlCon.con;
                    PurchaseInvoice objectsaleinvoice = new PurchaseInvoice();

                    prodquantinstock_onIDEnter = objectsaleinvoice.getprodstockquant_withprodID(objectStmt, objectCon,   intprod_id);
                    if(prodquantinstock_onIDEnter[0]==null){
                        lbl_stockQaunt.setText("0");
                        lbl_stockBONUSQaunt.setText("0");
                    }
                    else
                    {
                        String[] prodStock_quantarr = prodquantinstock_onIDEnter[0].split("--");
                        lbl_stockQaunt.setText(prodStock_quantarr[0]);
                        lbl_stockBONUSQaunt.setText(prodStock_quantarr[1]);
                    }

                }

                txt_prodName.requestFocus();
            }

            else
            {
                a.setAlertType(Alert.AlertType.ERROR);
                a.setHeaderText("Error!");
                a.setContentText("Product Name not entered.");
                a.show();
                txt_prodName.requestFocus();
            }
        }
    }
    /////////////////////////////////////////////////////////////////////////// END of Enter Press on Product Name

    ArrayList<String> newbcthlist = new ArrayList<>();
    public void enterpress_batchNo(KeyEvent event) throws IOException
    {
        if (event.getCode() == KeyCode.ENTER  || event.getCode() == KeyCode.TAB)
        {
            int intvalue_prodID = Integer.parseInt(txt_prodID.getText());
            int notequalbit = 1;
            PurchaseReturn objpurchretrn = new PurchaseReturn();
            MysqlCon objecmySQL = new MysqlCon();
            Statement statOBJ = objecmySQL.stmt;
            Connection CnnctOBJ = objecmySQL.con;
            newbcthlist = objpurchretrn.getprod_Allbatch(statOBJ,CnnctOBJ,intvalue_prodID);

                for(int i =0; i< newbcthlist.size();i++)
                {
                    if(newbcthlist.get(i).equals(txt_Batchno.getText()))
                    {
                        notequalbit = 0;
                    }

                }
                if(notequalbit == 0)
                {
                    txt_bonus.requestFocus();
                    notequalbit =1;
                }
                else
                {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setHeaderText("ERROR!");
                    a.setContentText("Cannot Find Batch in Stock.");
                    a.show();
                    txt_Batchno.requestFocus();
                }




        }
    }
    public void enterpress_bonus(KeyEvent event) throws IOException
    {
        if (event.getCode() == KeyCode.ENTER  || event.getCode() == KeyCode.TAB)
        {
            txt_discount.requestFocus();
        }
    }
    public void enterpress_discount(KeyEvent event) throws IOException
    {
        if (event.getCode() == KeyCode.ENTER  || event.getCode() == KeyCode.TAB)
        {
            txt_quant.requestFocus();
        }
    }

    String[] getbatchno_quantity = new String[5];
    int intbatchqaunt_val=0;
    public void enterpress_quantity(KeyEvent event) throws IOException
    {
        if(event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB) {
            int intval_prodID = Integer.parseInt(txt_prodID.getText());
            String getquant_firstbatch = "";
            MysqlCon mysqlOBJ = new MysqlCon();
            Statement mystamtOBJ = mysqlOBJ.stmt;
            Connection mysqlConn = mysqlOBJ.con;
            PurchaseReturn prchrtrn = new PurchaseReturn();
            getbatchno_quantity = prchrtrn.getprodbatch_quantity(mystamtOBJ, mysqlConn, intval_prodID, txt_Batchno.getText());
            if (getbatchno_quantity[0] == null) {
            } else {
                String[] batch_quantitiesarray = getbatchno_quantity[0].split("--");
                getquant_firstbatch = batch_quantitiesarray[0];
            }
            intbatchqaunt_val = Integer.parseInt(getquant_firstbatch);

            if (txt_discount.getText().isEmpty()) {
                txt_discount.setText("0");
            }
            int intvalue_discounttxtbx = Integer.parseInt(txt_discount.getText());
            if (intvalue_discounttxtbx == 0) {

                if (!txt_quant.getText().isEmpty() && !txt_quant.getText().equals("0")) {
                    float getretail_floatval = Float.parseFloat(lbl_tradePrice.getText());
                    int intcnvrt_quant = Integer.parseInt(txt_quant.getText());
                    //////////////////////////////////////////////////////////// START OF CONDITIONS FOR FIFO, LIFO ETC (PRODUCT QUANTITY)
                    if (intcnvrt_quant > intbatchqaunt_val) {

                            a.setAlertType(Alert.AlertType.ERROR);
                            a.setHeaderText("ERROR!");
                            a.setContentText("Entered Quantity is more than Quantity of this Batch in Stock");
                            a.show();
                            txt_quant.requestFocus();
                        }
                    else {
                            float totalquantity_price = intcnvrt_quant * getretail_floatval;
                            lbl_totalAmount.setText(String.valueOf(totalquantity_price));
                            datepick_expiry.requestFocus();
                        }
                }
                else {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setHeaderText("Input Field Empty");
                    a.setContentText("Please Enter Quantity");
                    a.show();
                    txt_quant.requestFocus();
                }
            }
            else {
                if (!txt_quant.getText().isEmpty() && !txt_quant.getText().equals("0"))
                {

                    float intgetretail_price = Integer.parseInt(lbl_tradePrice.getText());
                    int intcnvrt_quant = Integer.parseInt(txt_quant.getText());
                    float intdiscount_value = Integer.parseInt(txt_discount.getText());

                    if (intcnvrt_quant > intbatchqaunt_val)
                    {

                            a.setAlertType(Alert.AlertType.ERROR);
                            a.setHeaderText("ERROR!");
                            a.setContentText("Entered Quantity is more than Quantity of this Batch in Stock");
                            a.show();
                            txt_quant.requestFocus();

                        }
                    else {
                            float totalquantity_price = intcnvrt_quant * intgetretail_price;
                            float disc_firstVal = (totalquantity_price * intdiscount_value) / 100;
                            lbl_discAmount.setText(String.valueOf(disc_firstVal));
                            float final_discVal = totalquantity_price - disc_firstVal;
                            lbl_totalAmount.setText(String.valueOf(final_discVal));
                            datepick_expiry.requestFocus();
                        }


                }
                else {
                    a.setAlertType(Alert.AlertType.ERROR);
                    a.setHeaderText("Input Field Empty");
                    a.setContentText("Please Enter Quantity");
                    a.show();
                    txt_quant.requestFocus();
                }
            }
        }
    }
    /////////////////////////////////////////////////////////////////////////// START of ENter Press on Expirydate
    public void keypress_onExpirydate(KeyEvent event) throws IOException
    {
        if(event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB) {
            btn_add.requestFocus();
        }
    }
    /////////////////////////////////////////////////////////////////////////// END of ENter Press on Expirydate


}
