package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import model.*;
import model.UpdateSupplierInfo;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class UpdateSupplier implements Initializable {

    @FXML
    private Label label_update_supplier;

    @FXML
    private JFXTextField txt_supplier_address;

    @FXML
    private JFXButton dialog_supplier_close;

    @FXML
    private JFXButton dialog_supplier_update;

    @FXML
    private JFXTextField txt_supplier_city;

    @FXML
    private Label label_update1;

    @FXML
    private JFXTextField txt_supplier_id;

    @FXML
    private JFXTextField txt_contact_person;

    @FXML
    private JFXTextField txt_supplier_name;

    @FXML
    private JFXTextField txt_supplier_number;

    @FXML
    private JFXTextField txt_supplier_email;

    @FXML
    private Label label_update11;

    @FXML
    private JFXComboBox<String> txt_supplier_status;

    @FXML
    private JFXComboBox<String> txt_company_name;

    @FXML
    private TableView<UpdateSupplierInfo> table_suppliercompanies;

    @FXML
    private TableColumn<UpdateSupplierInfo, String> sr_no;

    @FXML
    private TableColumn<UpdateSupplierInfo, String> company_name;

    @FXML
    private TableColumn<UpdateSupplierInfo, String> operations;

    public static String supplierTableId = "";
    public static String supplierId = "";
    public static String supplierName = "";
    public static String supplierContact = "";
    public static String supplierAddress = "";
    public static String supplierEmail = "";
    public static String supplierCity = "";
    public static String contactPerson = "";
    public static String supplierStatus = "";
    public static ArrayList<String> supplierCompanyIds = new ArrayList<>();
    public static ArrayList<String> supplierCompanyNames = new ArrayList<>();

    private ArrayList<ArrayList<String>> companiesData = new ArrayList<>();

    public static ObservableList<UpdateSupplierInfo> supplierCompaniesDetail;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ViewSupplierInfo.lblUpdate = label_update_supplier;
        UpdateSupplierInfo objUpdateSupplierInfo = new UpdateSupplierInfo();
        companiesData = objUpdateSupplierInfo.getCompaniesData();

        txt_supplier_id.setText(supplierId);
        txt_supplier_name.setText(supplierName);
        txt_supplier_address.setText(supplierAddress);
        txt_supplier_city.setText(supplierCity);
        txt_supplier_email.setText(supplierEmail);
        txt_contact_person.setText(contactPerson);
        txt_supplier_number.setText(supplierContact);
        txt_supplier_status.setValue(supplierStatus);
        dialog_supplier_close.setOnAction((action)->closeDialog());
        dialog_supplier_update.setOnAction((action)->updateSupplier());

        ArrayList<String> chosenCompanyNames = getArrayColumn(companiesData, 1);
        txt_company_name.getItems().addAll(chosenCompanyNames);

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        company_name.setCellValueFactory(new PropertyValueFactory<>("companyName"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_suppliercompanies.setItems(parseUserList());
        UpdateSupplierInfo.table_suppliercompanies = table_suppliercompanies;
    }

    public static ObservableList<UpdateSupplierInfo> parseUserList() {
        supplierCompaniesDetail = FXCollections.observableArrayList();
        for (int i = 0; i < supplierCompanyIds.size(); i++)
        {
            supplierCompaniesDetail.add(new UpdateSupplierInfo(String.valueOf(i+1), supplierCompanyNames.get(i)));
        }
        return supplierCompaniesDetail;
    }

    public void closeDialog()
    {
        ViewSupplierInfo.dialog.close();
        ViewSupplierInfo.stackPane.setVisible(false);
    }

    public void updateSupplier() {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        SupplierInfo objSupplierInfo = new SupplierInfo();
        supplierId = txt_supplier_id.getText();
        supplierName = txt_supplier_name.getText();
        supplierAddress = txt_supplier_address.getText();
        supplierCity = txt_supplier_city.getText();
        supplierEmail = txt_supplier_email.getText();
        contactPerson = txt_contact_person.getText();
        supplierContact = txt_supplier_number.getText();
        supplierStatus = txt_supplier_status.getValue();
        objSupplierInfo.updateSupplier(objStmt, objCon, supplierTableId, supplierId, supplierName, supplierAddress, supplierCity, supplierEmail, contactPerson, supplierContact, supplierCompanyIds, supplierStatus);
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/view_suppliers.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void addCompany(ActionEvent event) {
        String addCompanyName = txt_company_name.getValue();
        supplierCompanyNames.add(addCompanyName);
        supplierCompanyIds.add(getCompanyId(companiesData, addCompanyName));
        table_suppliercompanies.setItems(parseUserList());
    }

    public String getCompanyId(ArrayList<ArrayList<String>> companiesData, String companyName)
    {
        for(ArrayList<String> row: companiesData) {
            if(row.get(1).equals(companyName))
            {
                return row.get(0);
            }
        }
        return null;
    }

    public ArrayList<String> getArrayColumn(ArrayList<ArrayList<String>> companiesData, int columnIndex)
    {
        ArrayList<String> columnList = new ArrayList<>();
        for(ArrayList<String> row: companiesData) {
            columnList.add(row.get(columnIndex));
        }
        return columnList;
    }
}
