package controller;

import com.jfoenix.controls.JFXButton;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import model.GlobalVariables;
import model.SetupOwnerCompanyInfo;
import model.SetupOwnerDealersInfo;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class SetupOwnerCompany implements Initializable {

    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private TableView<SetupOwnerCompanyInfo> table_viewcompanies;

    @FXML
    private TableColumn<SetupOwnerCompanyInfo, String> sr_no;

    @FXML
    private TableColumn<SetupOwnerCompanyInfo, String> company_name;

    @FXML
    private TableColumn<SetupOwnerCompanyInfo, String> company_contact;

    @FXML
    private TableColumn<SetupOwnerCompanyInfo, String> company_address;

    @FXML
    private TableColumn<SetupOwnerCompanyInfo, String> company_email;

//    @FXML
//    private TableColumn<SetupOwnerCompanyInfo, String> company_added;

    @FXML
    private TableColumn<SetupOwnerCompanyInfo, String> operations;

    @FXML
    private JFXButton btn_skip;

    @FXML
    private JFXButton btn_add;

    @FXML
    private ProgressIndicator progress;

    @FXML
    private Label lbl_total_companies;

    @FXML
    private Label lbl_companies_selected;

    @FXML
    private JFXButton btn_unselect;

    @FXML
    private JFXButton btn_select;

    @FXML
    private BorderPane menu_bar;

    private static SetupOwnerCompanyInfo objSetupOwnerCompanyInfo;
    public static ArrayList<ArrayList<String>> companiesData;
    public static ObservableList<SetupOwnerCompanyInfo> companiesDetail;
    public static int totalCompanies = 0;
    public static int selectedCompanies = 0;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        if(SetupOwnerCompanyInfo.businessCategory.equals(""))
        {
            // Skip This and Move to Next
        }
        else
        {
            SetupOwnerCompanyInfo.lblCompaniesSelected = lbl_companies_selected;
            SetupOwnerCompanyInfo.table_viewcompanies = table_viewcompanies;
            objSetupOwnerCompanyInfo = new SetupOwnerCompanyInfo();
            sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
            company_name.setCellValueFactory(new PropertyValueFactory<>("companyName"));
            company_contact.setCellValueFactory(new PropertyValueFactory<>("companyContact"));
            company_address.setCellValueFactory(new PropertyValueFactory<>("companyAddress"));
            company_email.setCellValueFactory(new PropertyValueFactory<>("companyEmail"));
//            company_added.setCellValueFactory(new PropertyValueFactory<>("companyAdded"));
            operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
            lbl_total_companies.setText(String.valueOf(totalCompanies));

//            SetupOwnerCompanyInfo.table_viewcompanies.setRowFactory(tv -> new TableRow<SetupOwnerCompanyInfo>() {
//                @Override
//                protected void updateItem(SetupOwnerCompanyInfo item, boolean empty) {
//                    super.updateItem(item, empty);
//                    item = new SetupOwnerCompanyInfo();
//                    for(int i=0; i<SetupOwnerCompanyInfo.arrCheckBox.size(); i++)
//                    {
//                        if(SetupOwnerCompanyInfo.arrCheckBox.get(i).isSelected())
//                        {
//                            setStyle("-fx-background-color: #ffd7d1;");
//                        }
//                    }
//
//                }
//            });
        }
    }

    public static ObservableList<SetupOwnerCompanyInfo> parseUserList() {
        
        companiesDetail = FXCollections.observableArrayList();
        companiesData = SetupOwnerCompanyInfo.companiesData;

        if(companiesData.size() <= 0)
        {
            SetupHeader.labelName = "Dealers in\nDistrict "+ SetupOwnerDealersInfo.districtName;
            Parent root = null;
            try {
                root = FXMLLoader.load(SetupOwnerCompany.class.getResource("/view/setup_owner_dealers.fxml"));
            } catch (IOException e) {
                e.printStackTrace();
            }
            GlobalVariables.baseScene.setRoot(root);
            GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
        }

        int num = 1;
        for (int i = 0; i < companiesData.size(); i++)
        {
            totalCompanies++;
            SetupOwnerCompanyInfo.companyNameArr.add(companiesData.get(i).get(1));
            SetupOwnerCompanyInfo.companyContactArr.add(companiesData.get(i).get(2));
            SetupOwnerCompanyInfo.companyAddressArr.add(companiesData.get(i).get(3));
            SetupOwnerCompanyInfo.companyEmailArr.add(companiesData.get(i).get(4));
            companiesDetail.add(new SetupOwnerCompanyInfo(String.valueOf(i+1), companiesData.get(i).get(1), companiesData.get(i).get(2), companiesData.get(i).get(3), companiesData.get(i).get(4)));
        }
        return companiesDetail;
    }

    @FXML
    void addClicked(ActionEvent event) {
        progress.setVisible(true);

        Thread thread = new Thread(){
            public void run(){
                boolean response;
                response = objSetupOwnerCompanyInfo.insertCompany(GlobalVariables.objStmt);

                Platform.runLater(new Runnable(){
                    @Override
                    public void run() {
                        if(response)
                        {
//                            SetupOwnerCompanyInfo.businessCategory = businessCategory;
                            SetupHeader.labelName = "Products\nSelection";
                            String title = "Success";
                            String message = "Companies Record Added";
                            GlobalVariables.showNotification(1, title, message);

                            //                             ------------------------------------ Setting Core File -------------------------
                            String directoryName = "secret-credentials";
                            File directory = new File(directoryName);
                            if (!directory.exists()){
                                directory.mkdir();
                            }
                            PrintWriter writer = null;
                            try {
                                writer = new PrintWriter("secret-credentials/data.txt", "UTF-8");
                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                            writer.println(GlobalVariables.softwareLicId);
                            writer.println(GlobalVariables.softwareLicNumber);
                            writer.println(GlobalVariables.softwareLicRegDate);
                            writer.println(GlobalVariables.softwareLicExpDate);
                            writer.println("Products");
                            writer.println(GlobalVariables.workingDistrict);
                            writer.close();
                            String currentDirectory = System.getProperty("user.dir");
                            String command = "cd "+currentDirectory+" & attrib +h +r +s secret-credentials";

                            ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c", command);
                            builder.redirectErrorStream(true);
                            try {
                                Process p = builder.start();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
//                             ------------------------------------ Setting Core File -------------------------

                            Parent root = null;
                            try {
                                root = FXMLLoader.load(getClass().getResource("/view/setup_owner_products.fxml"));
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            GlobalVariables.baseScene.setRoot(root);
                            GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
                        }
                        else
                        {
                            String title = "Error";
                            String message = "Record Not Added";
                            GlobalVariables.showNotification(-1, title, message);
                        }
                    }
                    // do your GUI stuff here
                });
            }
        };
        thread.start();
    }

    @FXML
    void selectAllClicked(ActionEvent event) {
        selectedCompanies = 0;
        for(int i=0; i<SetupOwnerCompanyInfo.arrCheckBox.size(); i++)
        {
            selectedCompanies++;
            SetupOwnerCompanyInfo.arrCheckBox.get(i).setSelected(true);
        }
        lbl_companies_selected.setText(String.valueOf(selectedCompanies));

//        SetupOwnerCompanyInfo.table_viewcompanies.setRowFactory(tv -> new TableRow<SetupOwnerCompanyInfo>() {
//            @Override
//            protected void updateItem(SetupOwnerCompanyInfo item, boolean empty) {
//                super.updateItem(item, empty);
//                item = new SetupOwnerCompanyInfo();
//                for(int i=0; i<SetupOwnerCompanyInfo.arrCheckBox.size(); i++)
//                {
//                    if(SetupOwnerCompanyInfo.arrCheckBox.get(i).isSelected())
//                    {
//                        setStyle("-fx-background-color: #ffd7d1;");
//                    }
//                }
//
//            }
//        });
    }

    @FXML
    void skipClicked(ActionEvent event) {
        //                             ------------------------------------ Setting Core File -------------------------
        String directoryName = "secret-credentials";
        File directory = new File(directoryName);
        if (!directory.exists()){
            directory.mkdir();
        }
        PrintWriter writer = null;
        try {
            writer = new PrintWriter("secret-credentials/data.txt", "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        writer.println(GlobalVariables.softwareLicId);
        writer.println(GlobalVariables.softwareLicNumber);
        writer.println(GlobalVariables.softwareLicRegDate);
        writer.println(GlobalVariables.softwareLicExpDate);
        writer.println("Dealers");
        writer.println(GlobalVariables.workingDistrict);
        writer.close();
        String currentDirectory = System.getProperty("user.dir");
        String command = "cd "+currentDirectory+" & attrib +h +r +s secret-credentials";

        ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c", command);
        builder.redirectErrorStream(true);
        try {
            Process p = builder.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
//                             ------------------------------------ Setting Core File -------------------------


        SetupHeader.labelName = "Dealers in\nDistrict "+ SetupOwnerDealersInfo.districtName;
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/setup_owner_dealers.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void unSelectAllClicked(ActionEvent event) {
        for(int i=0; i<SetupOwnerCompanyInfo.arrCheckBox.size(); i++)
        {
            SetupOwnerCompanyInfo.arrCheckBox.get(i).setSelected(false);
        }
        selectedCompanies = 0;
        lbl_companies_selected.setText(String.valueOf(selectedCompanies));

//        SetupOwnerCompanyInfo.table_viewcompanies.setRowFactory(tv -> new TableRow<SetupOwnerCompanyInfo>() {
//            @Override
//            protected void updateItem(SetupOwnerCompanyInfo item, boolean empty) {
//                super.updateItem(item, empty);
//                item = new SetupOwnerCompanyInfo();
//                for(int i=0; i<SetupOwnerCompanyInfo.arrCheckBox.size(); i++)
//                {
//                    if(SetupOwnerCompanyInfo.arrCheckBox.get(i).isSelected())
//                    {
//                        setStyle("-fx-background-color: #ffd7d1;");
//                    }
//                }
//
//            }
//        });
    }
}
