package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import model.*;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class UpdateArea implements Initializable {

    @FXML
    private Label label_update;

    @FXML
    private JFXTextField txt_area_id;

    @FXML
    private JFXTextField txt_area_name;

    @FXML
    private JFXComboBox<String> txt_city_name;

    @FXML
    private JFXComboBox<String> txt_area_status;

    @FXML
    private JFXButton dialog_area_close;

    @FXML
    private JFXButton dialog_area_update;

    public static String areaTableId = "";
    public static String areaId = "";
    public static String areaName = "";
    public static String cityId = "";
    public static String cityName = "";
    public static String areaStatus = "";

    private String selectedCityId;
    private ArrayList<ArrayList<String>> citiesData = new ArrayList<>();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ViewAreaInfo.lblUpdate = label_update;
        UpdateAreaInfo objUpdateAreaInfo = new UpdateAreaInfo();
        citiesData = objUpdateAreaInfo.getCitiesData();
        ArrayList<String> chosenCityNames = getArrayColumn(citiesData, 1);
        txt_city_name.getItems().addAll(chosenCityNames);
        txt_area_id.setText(areaId);
        txt_area_name.setText(areaName);
        txt_city_name.setValue(cityName);
        txt_area_status.setValue(areaStatus);
        selectedCityId = cityId;
        dialog_area_close.setOnAction((action)->closeDialog());
        dialog_area_update.setOnAction((action)->updateCity());
    }

    public void closeDialog()
    {
        ViewAreaInfo.dialog.close();
        ViewAreaInfo.stackPane.setVisible(false);
    }

    public void updateCity() {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        AreaInfo objAreaInfo = new AreaInfo();
        areaId = txt_area_id.getText();
        areaName = txt_area_name.getText();
        cityName = txt_city_name.getValue();
        areaStatus = txt_area_status.getValue();
        objAreaInfo.updateArea(objStmt, objCon, areaTableId, areaId, areaName, selectedCityId, areaStatus);

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/view_area.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void cityChange(ActionEvent event) {
        int index = txt_city_name.getSelectionModel().getSelectedIndex();
        selectedCityId = citiesData.get(index).get(0);
    }

    public ArrayList<String> getArrayColumn(ArrayList<ArrayList<String>> companiesData, int columnIndex)
    {
        ArrayList<String> columnList = new ArrayList<>();
        for(ArrayList<String> row: companiesData) {
            columnList.add(row.get(columnIndex));
        }
        return columnList;
    }
}
