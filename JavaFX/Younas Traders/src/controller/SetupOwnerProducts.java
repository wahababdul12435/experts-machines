package controller;

import com.jfoenix.controls.JFXButton;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import model.CompanyInfo;
import model.GlobalVariables;
import model.SetupOwnerDealersInfo;
import model.SetupOwnerProductsInfo;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class SetupOwnerProducts implements Initializable {

    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private TableView<SetupOwnerProductsInfo> table_viewproducts;

    @FXML
    private TableColumn<SetupOwnerProductsInfo, String> sr_no;

    @FXML
    private TableColumn<SetupOwnerProductsInfo, String> company_name;

    @FXML
    private TableColumn<SetupOwnerProductsInfo, String> product_name;

    @FXML
    private TableColumn<SetupOwnerProductsInfo, String> product_type;

    @FXML
    private TableColumn<SetupOwnerProductsInfo, String> pack_size;

    @FXML
    private TableColumn<SetupOwnerProductsInfo, String> carton_size;

    @FXML
    private TableColumn<SetupOwnerProductsInfo, String> purchase_price;

    @FXML
    private TableColumn<SetupOwnerProductsInfo, String> trade_price;

    @FXML
    private TableColumn<SetupOwnerProductsInfo, String> retail_price;

//    @FXML
//    private TableColumn<SetupOwnerProductsInfo, String> company_added;

    @FXML
    private TableColumn<SetupOwnerProductsInfo, String> operations;

    @FXML
    private JFXButton btn_skip;

    @FXML
    private JFXButton btn_add;

    @FXML
    private ProgressIndicator progress;

    @FXML
    private Label lbl_total_products;

    @FXML
    private Label lbl_products_selected;

    @FXML
    private JFXButton btn_unselect;

    @FXML
    private JFXButton btn_select;

    @FXML
    private BorderPane menu_bar;

    private static CompanyInfo objCompanyInfo;
    private static SetupOwnerProductsInfo objSetupOwnerProductsInfo;
    public static ArrayList<ArrayList<String>> companiesData;
    public static ArrayList<ArrayList<String>> productsData;
    public static ObservableList<SetupOwnerProductsInfo> productsDetail;
    public static int totalProducts = 0;
    public static int selectedProducts = 0;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        if(SetupOwnerProductsInfo.companyIds.equals(""))
        {
            // Skip This and Move to Next
        }
        else
        {
            SetupOwnerProductsInfo.lblProductsSelected = lbl_products_selected;
            SetupOwnerProductsInfo.table_viewproducts = table_viewproducts;
            objCompanyInfo = new CompanyInfo();
            companiesData = objCompanyInfo.getCompanyNames();
            objSetupOwnerProductsInfo = new SetupOwnerProductsInfo();
            sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
            company_name.setCellValueFactory(new PropertyValueFactory<>("companyName"));
            product_name.setCellValueFactory(new PropertyValueFactory<>("productName"));
            product_type.setCellValueFactory(new PropertyValueFactory<>("productType"));
            pack_size.setCellValueFactory(new PropertyValueFactory<>("packSize"));
            carton_size.setCellValueFactory(new PropertyValueFactory<>("cartonSize"));
            purchase_price.setCellValueFactory(new PropertyValueFactory<>("purchasePrice"));
            trade_price.setCellValueFactory(new PropertyValueFactory<>("tradePrice"));
            retail_price.setCellValueFactory(new PropertyValueFactory<>("retailPrice"));
            operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
            lbl_total_products.setText(String.valueOf(totalProducts));
        }
    }

    public static ObservableList<SetupOwnerProductsInfo> parseUserList() {
        
        productsDetail = FXCollections.observableArrayList();
        productsData = SetupOwnerProductsInfo.productsData;
        String companyId = "";

        if(productsData.size() <= 0)
        {
            SetupHeader.labelName = "Dealers in\nDistrict "+ SetupOwnerDealersInfo.districtName;
            Parent root = null;
            try {
                root = FXMLLoader.load(SetupOwnerProducts.class.getResource("/view/setup_owner_dealers.fxml"));
            } catch (IOException e) {
                e.printStackTrace();
            }
            GlobalVariables.baseScene.setRoot(root);
            GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
        }

        int num = 1;
        for (int i = 0; i < productsData.size(); i++)
        {
            totalProducts++;
            companyId = GlobalVariables.getIdFrom2D(companiesData, productsData.get(i).get(0), 1);
            SetupOwnerProductsInfo.companyIdArr.add(companyId);
            SetupOwnerProductsInfo.companyNameArr.add(productsData.get(i).get(0));
            SetupOwnerProductsInfo.productNameArr.add(productsData.get(i).get(1));
            SetupOwnerProductsInfo.productTypeArr.add(productsData.get(i).get(2));
            SetupOwnerProductsInfo.packSizeArr.add(productsData.get(i).get(3));
            SetupOwnerProductsInfo.cartonSizeArr.add(productsData.get(i).get(4));
            SetupOwnerProductsInfo.purchasePriceArr.add(productsData.get(i).get(5));
            SetupOwnerProductsInfo.tradePriceArr.add(productsData.get(i).get(6));
            SetupOwnerProductsInfo.retailPriceArr.add(productsData.get(i).get(7));
            productsDetail.add(new SetupOwnerProductsInfo(String.valueOf(i+1), productsData.get(i).get(0), productsData.get(i).get(1), productsData.get(i).get(2), productsData.get(i).get(3), productsData.get(i).get(4), productsData.get(i).get(5), productsData.get(i).get(6), productsData.get(i).get(7)));
        }
        return productsDetail;
    }

    @FXML
    void addClicked(ActionEvent event) {
        progress.setVisible(true);

        Thread thread = new Thread(){
            public void run(){
                boolean response;
                response = objSetupOwnerProductsInfo.insertProduct(GlobalVariables.objStmt);

                Platform.runLater(new Runnable(){
                    @Override
                    public void run() {
                        if(response)
                        {
//                            SetupOwnerProductsInfo.businessCategory = businessCategory;
                            SetupHeader.labelName = "Dealers in\nDistrict "+ SetupOwnerDealersInfo.districtName;
                            String title = "Success";
                            String message = "Products Record Added";
                            GlobalVariables.showNotification(1, title, message);

                            //                             ------------------------------------ Setting Core File -------------------------
                            String directoryName = "secret-credentials";
                            File directory = new File(directoryName);
                            if (!directory.exists()){
                                directory.mkdir();
                            }
                            PrintWriter writer = null;
                            try {
                                writer = new PrintWriter("secret-credentials/data.txt", "UTF-8");
                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                            writer.println(GlobalVariables.softwareLicId);
                            writer.println(GlobalVariables.softwareLicNumber);
                            writer.println(GlobalVariables.softwareLicRegDate);
                            writer.println(GlobalVariables.softwareLicExpDate);
                            writer.println("Dealers");
                            writer.println(GlobalVariables.workingDistrict);
                            writer.close();
                            String currentDirectory = System.getProperty("user.dir");
                            String command = "cd "+currentDirectory+" & attrib +h +r +s secret-credentials";

                            ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c", command);
                            builder.redirectErrorStream(true);
                            try {
                                Process p = builder.start();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
//                             ------------------------------------ Setting Core File -------------------------

                            Parent root = null;
                            try {
                                root = FXMLLoader.load(getClass().getResource("/view/setup_owner_dealers.fxml"));
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            GlobalVariables.baseScene.setRoot(root);
                            GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
                        }
                        else
                        {
                            String title = "Error";
                            String message = "Record Not Added";
                            GlobalVariables.showNotification(-1, title, message);
                        }
                    }
                    // do your GUI stuff here
                });
            }
        };
        thread.start();
    }

    @FXML
    void selectAllClicked(ActionEvent event) {
        selectedProducts = 0;
        for(int i=0; i<SetupOwnerProductsInfo.arrCheckBox.size(); i++)
        {
            selectedProducts++;
            SetupOwnerProductsInfo.arrCheckBox.get(i).setSelected(true);
        }
        lbl_products_selected.setText(String.valueOf(selectedProducts));

//        SetupOwnerProductsInfo.table_viewproducts.setRowFactory(tv -> new TableRow<SetupOwnerProductsInfo>() {
//            @Override
//            protected void updateItem(SetupOwnerProductsInfo item, boolean empty) {
//                super.updateItem(item, empty);
//                item = new SetupOwnerProductsInfo();
//                for(int i=0; i<SetupOwnerProductsInfo.arrCheckBox.size(); i++)
//                {
//                    if(SetupOwnerProductsInfo.arrCheckBox.get(i).isSelected())
//                    {
//                        setStyle("-fx-background-color: #ffd7d1;");
//                    }
//                }
//
//            }
//        });
    }

    @FXML
    void skipClicked(ActionEvent event) {
        //                             ------------------------------------ Setting Core File -------------------------
        String directoryName = "secret-credentials";
        File directory = new File(directoryName);
        if (!directory.exists()){
            directory.mkdir();
        }
        PrintWriter writer = null;
        try {
            writer = new PrintWriter("secret-credentials/data.txt", "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        writer.println(GlobalVariables.softwareLicId);
        writer.println(GlobalVariables.softwareLicNumber);
        writer.println(GlobalVariables.softwareLicRegDate);
        writer.println(GlobalVariables.softwareLicExpDate);
        writer.println("Dealers");
        writer.println(GlobalVariables.workingDistrict);
        writer.close();
        String currentDirectory = System.getProperty("user.dir");
        String command = "cd "+currentDirectory+" & attrib +h +r +s secret-credentials";

        ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c", command);
        builder.redirectErrorStream(true);
        try {
            Process p = builder.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
//                             ------------------------------------ Setting Core File -------------------------


        SetupHeader.labelName = "Dealers in\nDistrict "+ SetupOwnerDealersInfo.districtName;
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/setup_owner_dealers.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void unSelectAllClicked(ActionEvent event) {
        for(int i=0; i<SetupOwnerProductsInfo.arrCheckBox.size(); i++)
        {
            SetupOwnerProductsInfo.arrCheckBox.get(i).setSelected(false);
        }
        selectedProducts = 0;
        lbl_products_selected.setText(String.valueOf(selectedProducts));

//        SetupOwnerProductsInfo.table_viewproducts.setRowFactory(tv -> new TableRow<SetupOwnerProductsInfo>() {
//            @Override
//            protected void updateItem(SetupOwnerProductsInfo item, boolean empty) {
//                super.updateItem(item, empty);
//                item = new SetupOwnerProductsInfo();
//                for(int i=0; i<SetupOwnerProductsInfo.arrCheckBox.size(); i++)
//                {
//                    if(SetupOwnerProductsInfo.arrCheckBox.get(i).isSelected())
//                    {
//                        setStyle("-fx-background-color: #ffd7d1;");
//                    }
//                }
//
//            }
//        });
    }
}
