package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.InputMethodEvent;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import model.*;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

public class DeleteWindow implements Initializable {

    @FXML
    private Label label_update;

    @FXML
    private JFXPasswordField txt_password;

    @FXML
    private JFXButton btn_cancel;

    @FXML
    private JFXButton btn_delete;

    @FXML
    private Label lbl_incorrect;

    public static boolean deletion = false;
    public static String sceneWindow = "";
    public static String deletionId = "";

    MysqlCon objMysqlCon;
    Statement objStmt;
    Connection objCon;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        objMysqlCon = new MysqlCon();
        objStmt = objMysqlCon.stmt;
        objCon = objMysqlCon.con;
    }

    @FXML
    void cancelClicked(ActionEvent event) {
        if(sceneWindow.equals("ViewCompany"))
        {
            deletion = false;
            sceneWindow = "";
            ViewCompanyInfo.dialog.close();
            ViewCompanyInfo.stackPane.setVisible(false);
        }
        else if(sceneWindow.equals("ViewBonus"))
        {
            deletion = false;
            sceneWindow = "";
            ViewBonusInfo.dialog.close();
            ViewBonusInfo.stackPane.setVisible(false);
        }
        else if(sceneWindow.equals("ViewDiscount"))
        {
            deletion = false;
            sceneWindow = "";
            ViewDiscountInfo.dialog.close();
            ViewDiscountInfo.stackPane.setVisible(false);
        }
        else if(sceneWindow.equals("ViewGroup"))
        {
            deletion = false;
            sceneWindow = "";
            ViewGroupsInfo.dialog.close();
            ViewGroupsInfo.stackPane.setVisible(false);
        }
        else if(sceneWindow.equals("ViewSupplier"))
        {
            deletion = false;
            sceneWindow = "";
            ViewSupplierInfo.dialog.close();
            ViewSupplierInfo.stackPane.setVisible(false);
        }
        else if(sceneWindow.equals("ViewProducts"))
        {
            deletion = false;
            sceneWindow = "";
            ViewProductsInfo.dialog.close();
            ViewProductsInfo.stackPane.setVisible(false);
        }
        else if(sceneWindow.equals("ViewUser"))
        {
            deletion = false;
            sceneWindow = "";
            ViewUsersInfo.dialog.close();
            ViewUsersInfo.stackPane.setVisible(false);
        }
        else if(sceneWindow.equals("ViewDealer"))
        {
            deletion = false;
            sceneWindow = "";
            ViewDealerInfo.dialog.close();
            ViewDealerInfo.stackPane.setVisible(false);
        }
        else if(sceneWindow.equals("ViewDistrict"))
        {
            deletion = false;
            sceneWindow = "";
            ViewDistrictInfo.dialog.close();
            ViewDistrictInfo.stackPane.setVisible(false);
        }
        else if(sceneWindow.equals("ViewCity"))
        {
            deletion = false;
            sceneWindow = "";
            ViewCityInfo.dialog.close();
            ViewCityInfo.stackPane.setVisible(false);
        }
        else if(sceneWindow.equals("ViewArea"))
        {
            deletion = false;
            sceneWindow = "";
            ViewAreaInfo.dialog.close();
            ViewAreaInfo.stackPane.setVisible(false);
        }
        else if(sceneWindow.equals("ViewPurchase"))
        {
            deletion = false;
            sceneWindow = "";
            ViewPurchaseInfo.dialog.close();
            ViewPurchaseInfo.stackPane.setVisible(false);
        }
        else if(sceneWindow.equals("ViewPurchaseReturn"))
        {
            deletion = false;
            sceneWindow = "";
            ViewPurchaseReturnInfo.dialog.close();
            ViewPurchaseReturnInfo.stackPane.setVisible(false);
        }
        else if(sceneWindow.equals("ViewSales"))
        {
            deletion = false;
            sceneWindow = "";
            ViewSalesInfo.dialog.close();
            ViewSalesInfo.stackPane.setVisible(false);
        }
        else if(sceneWindow.equals("ViewSalesReturn"))
        {
            deletion = false;
            sceneWindow = "";
            ViewSalesReturnInfo.dialog.close();
            ViewSalesReturnInfo.stackPane.setVisible(false);
        }
        else if(sceneWindow.equals("DealerFinanceDetailReport"))
        {
            deletion = false;
            sceneWindow = "";
            DealerFinanceDetailReportInfo.dialog.close();
            DealerFinanceDetailReportInfo.stackPane.setVisible(false);
        }
    }

    @FXML
    void deleteClicked(ActionEvent event) throws IOException {
        String password = txt_password.getText();
        if(password.equals(GlobalVariables.currentUserPassword))
        {
            deletion = true;
            if(sceneWindow.equals("ViewCompany"))
            {
                ViewCompanyInfo.dialog.close();
                ViewCompanyInfo.stackPane.setVisible(false);
                try {
                    String updateQuery = "UPDATE `company_info` SET `company_status`='Deleted', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE 'Done' END WHERE `company_table_id` = '"+deletionId+"'";
                    objStmt.executeUpdate(updateQuery);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                sceneWindow = "";
                deletion = false;
                Parent root = FXMLLoader.load(getClass().getResource("/view/view_company.fxml"));
                GlobalVariables.baseScene.setRoot(root);
                GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
            }
            else if(sceneWindow.equals("ViewDiscount"))
            {
                ViewGroupsInfo.dialog.close();
                ViewGroupsInfo.stackPane.setVisible(false);
                sceneWindow = "";
                deletion = false;
                try {
                    String updateQuery = "UPDATE `discount_policy` SET `policy_status`='Deleted', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE 'Done' END WHERE `approval_id` = '"+deletionId+"'";
                    objStmt.executeUpdate(updateQuery);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                Parent root = FXMLLoader.load(getClass().getResource("/view/view_discount.fxml"));
                GlobalVariables.baseScene.setRoot(root);
                GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
            }
            else if(sceneWindow.equals("ViewBonus"))
            {
                ViewGroupsInfo.dialog.close();
                ViewGroupsInfo.stackPane.setVisible(false);
                sceneWindow = "";
                deletion = false;
                try {
                    String updateQuery = "UPDATE `bonus_policy` SET `policy_status`='Deleted', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE 'Done' END WHERE `approval_id` = '"+deletionId+"'";
                    objStmt.executeUpdate(updateQuery);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                Parent root = FXMLLoader.load(getClass().getResource("/view/view_groups.fxml"));
                GlobalVariables.baseScene.setRoot(root);
                GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
            }
            else if(sceneWindow.equals("ViewGroup"))
            {
                ViewGroupsInfo.dialog.close();
                ViewGroupsInfo.stackPane.setVisible(false);
                sceneWindow = "";
                deletion = false;
                try {
                    String updateQuery = "UPDATE `groups_info` SET `group_status`='Deleted', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE 'Done' END WHERE `group_table_id` = '"+deletionId+"'";
                    objStmt.executeUpdate(updateQuery);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                Parent root = FXMLLoader.load(getClass().getResource("/view/view_groups.fxml"));
                GlobalVariables.baseScene.setRoot(root);
                GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
            }
            else if(sceneWindow.equals("ViewSupplier"))
            {
                ViewSupplierInfo.dialog.close();
                ViewSupplierInfo.stackPane.setVisible(false);
                sceneWindow = "";
                deletion = false;
                try {
                    String updateQuery = "UPDATE `supplier_info` SET `supplier_status`='Deleted', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE 'Done' END WHERE `supplier_table_id` = '"+deletionId+"'";
                    objStmt.executeUpdate(updateQuery);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                Parent root = FXMLLoader.load(getClass().getResource("/view/view_suppliers.fxml"));
                GlobalVariables.baseScene.setRoot(root);
                GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
            }
            else if(sceneWindow.equals("ViewProducts"))
            {
                ViewProductsInfo.dialog.close();
                ViewProductsInfo.stackPane.setVisible(false);
                sceneWindow = "";
                deletion = false;
                try {
                    String updateQuery = "UPDATE `product_info` SET `product_status`='Deleted', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE 'Done' END WHERE `product_table_id` = '"+deletionId+"'";
                    objStmt.executeUpdate(updateQuery);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                Parent root = FXMLLoader.load(getClass().getResource("/view/view_products.fxml"));
                GlobalVariables.baseScene.setRoot(root);
                GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
            }
            else if(sceneWindow.equals("ViewUser"))
            {
                ViewUsersInfo.dialog.close();
                ViewUsersInfo.stackPane.setVisible(false);
                sceneWindow = "";
                deletion = false;
                try {
                    String updateQuery = "UPDATE `user_info` SET `user_status`='Deleted', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE 'Done' END WHERE `user_table_id` = '"+deletionId+"'";
                    objStmt.executeUpdate(updateQuery);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                Parent root = FXMLLoader.load(getClass().getResource("/view/view_users.fxml"));
                GlobalVariables.baseScene.setRoot(root);
                GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
            }
            else if(sceneWindow.equals("ViewDealer"))
            {
                ViewDealerInfo.dialog.close();
                ViewDealerInfo.stackPane.setVisible(false);
                sceneWindow = "";
                deletion = false;
                try {
                    String updateQuery = "UPDATE `dealer_info` SET `dealer_status`='Deleted', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE 'Done' END WHERE `dealer_table_id` = '"+deletionId+"'";
                    objStmt.executeUpdate(updateQuery);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                Parent root = FXMLLoader.load(getClass().getResource("/view/view_dealers.fxml"));
                GlobalVariables.baseScene.setRoot(root);
                GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
            }
            else if(sceneWindow.equals("ViewDistrict"))
            {
                ViewDistrictInfo.dialog.close();
                ViewDistrictInfo.stackPane.setVisible(false);
                sceneWindow = "";
                deletion = false;
                try {
                    String updateQuery = "UPDATE `district_info` SET `district_status`='Deleted', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE 'Done' END WHERE `district_table_id` = '"+deletionId+"'";
                    objStmt.executeUpdate(updateQuery);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                Parent root = FXMLLoader.load(getClass().getResource("/view/view_district.fxml"));
                GlobalVariables.baseScene.setRoot(root);
                GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
            }
            else if(sceneWindow.equals("ViewCity"))
            {
                ViewCityInfo.dialog.close();
                ViewCityInfo.stackPane.setVisible(false);
                sceneWindow = "";
                deletion = false;
                try {
                    String updateQuery = "UPDATE `city_info` SET `city_status`='Deleted', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE 'Done' END WHERE `city_table_id` = '"+deletionId+"'";
                    objStmt.executeUpdate(updateQuery);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                Parent root = FXMLLoader.load(getClass().getResource("/view/view_city.fxml"));
                GlobalVariables.baseScene.setRoot(root);
                GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
            }
            else if(sceneWindow.equals("ViewArea"))
            {
                ViewAreaInfo.dialog.close();
                ViewAreaInfo.stackPane.setVisible(false);
                sceneWindow = "";
                deletion = false;
                try {
                    String updateQuery = "UPDATE `area_info` SET `area_status`='Deleted', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE 'Done' END WHERE `area_table_id` = '"+deletionId+"'";
                    objStmt.executeUpdate(updateQuery);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                Parent root = FXMLLoader.load(getClass().getResource("/view/view_area.fxml"));
                GlobalVariables.baseScene.setRoot(root);
                GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
            }
            else if(sceneWindow.equals("ViewPurchase"))
            {
                ViewPurchaseInfo.dialog.close();
                ViewPurchaseInfo.stackPane.setVisible(false);
                sceneWindow = "";
                deletion = false;
                try {
                    String updateQuery = "UPDATE `purchase_info` SET `status` = 'Deleted', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE 'Done' END  WHERE `purchase_id` = '"+deletionId+"'";
                    objStmt.executeUpdate(updateQuery);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                Parent root = FXMLLoader.load(getClass().getResource("/view/view_purchase.fxml"));
                GlobalVariables.baseScene.setRoot(root);
                GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
            }
            else if(sceneWindow.equals("ViewPurchaseReturn"))
            {
                ViewPurchaseReturnInfo.dialog.close();
                ViewPurchaseReturnInfo.stackPane.setVisible(false);
                sceneWindow = "";
                deletion = false;
                try {
                    String updateQuery = "UPDATE `purchase_return` SET `status` = 'Deleted', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE 'Done' END WHERE `returnTable_id` = '"+deletionId+"'";
                    objStmt.executeUpdate(updateQuery);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                Parent root = FXMLLoader.load(getClass().getResource("/view/view_purchase_return.fxml"));
                GlobalVariables.baseScene.setRoot(root);
                GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
            }
            else if(sceneWindow.equals("ViewSales"))
            {
                ViewSalesInfo.dialog.close();
                ViewSalesInfo.stackPane.setVisible(false);
                sceneWindow = "";
                deletion = false;
                try {
                    String updateQuery = "UPDATE `order_info` SET `status` = 'Deleted', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE 'Done' END  WHERE `order_id` = '"+deletionId+"'";
                    objStmt.executeUpdate(updateQuery);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                Parent root = FXMLLoader.load(getClass().getResource("/view/view_sales.fxml"));
                GlobalVariables.baseScene.setRoot(root);
                GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
            }
            else if(sceneWindow.equals("ViewSalesReturn"))
            {
                ViewSalesReturnInfo.dialog.close();
                ViewSalesReturnInfo.stackPane.setVisible(false);
                sceneWindow = "";
                deletion = false;
                try {
                    String updateQuery = "UPDATE `order_return` SET `status` = 'Deleted', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE 'Done' END WHERE `return_id` = '"+deletionId+"'";
                    objStmt.executeUpdate(updateQuery);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                Parent root = FXMLLoader.load(getClass().getResource("/view/view_sales_return.fxml"));
                GlobalVariables.baseScene.setRoot(root);
                GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
            }
            else if(sceneWindow.equals("DealerFinanceDetailReport"))
            {
                DealerFinanceDetailReportInfo.dialog.close();
                DealerFinanceDetailReportInfo.stackPane.setVisible(false);
                sceneWindow = "";
                deletion = false;
                try {
                    String updateQuery = "UPDATE `dealer_payments` SET `status` = 'Deleted', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE 'Done' END WHERE `payment_id` = '"+deletionId+"'";
                    objStmt.executeUpdate(updateQuery);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                Parent root = FXMLLoader.load(getClass().getResource("/view/dealer_finance_detail_report.fxml"));
                GlobalVariables.baseScene.setRoot(root);
                GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
            }
        }
        else
        {
            deletion = false;
            lbl_incorrect.setVisible(true);
        }
    }

    @FXML
    void checkEntry(KeyEvent event) {
        if(txt_password.getText().equals(""))
        {
            lbl_incorrect.setVisible(false);
        }
    }
}
