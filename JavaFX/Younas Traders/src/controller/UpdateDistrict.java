package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import model.*;
import model.ViewDistrictInfo;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ResourceBundle;

public class UpdateDistrict implements Initializable {

    @FXML
    private Label label_update;

    @FXML
    private JFXTextField txt_district_id;

    @FXML
    private JFXTextField txt_district_name;

    @FXML
    private JFXComboBox<String> txt_district_status;

    @FXML
    private JFXButton dialog_district_close;

    @FXML
    private JFXButton dialog_district_update;

    public static String districtTableId = "";
    public static String districtId = "";
    public static String districtName = "";
    public static String districtStatus = "";

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ViewDistrictInfo.lblUpdate = label_update;
        txt_district_id.setText(districtId);
        txt_district_name.setText(districtName);
        txt_district_status.setValue(districtStatus);
        dialog_district_close.setOnAction((action)->closeDialog());
        dialog_district_update.setOnAction((action)->updateDistrict());
    }

    public void closeDialog()
    {
        ViewDistrictInfo.dialog.close();
        ViewDistrictInfo.stackPane.setVisible(false);
    }

    public void updateDistrict() {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        DistrictInfo objDistrictInfo = new DistrictInfo();
        districtId = txt_district_id.getText();
        districtName = txt_district_name.getText();
        districtStatus = txt_district_status.getValue();
        objDistrictInfo.updateDistrict(objStmt, objCon, districtTableId, districtId, districtName, districtStatus);

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/view_district.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

}
