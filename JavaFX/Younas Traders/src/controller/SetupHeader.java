package controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;

import java.net.URL;
import java.util.ResourceBundle;

public class SetupHeader implements Initializable {

    @FXML
    private AnchorPane rootpane;

    @FXML
    private Label lbl_title;

    public static String labelName = "Setup";

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        lbl_title.setText(labelName);
    }
}