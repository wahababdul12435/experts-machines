package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.util.Duration;
import model.*;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class ViewSupplier implements  Initializable{
    @FXML
    private TableView<ViewSupplierInfo> table_viewsuppliers;

    @FXML
    private TableColumn<ViewSupplierInfo, String> sr_no;

    @FXML
    private TableColumn<ViewSupplierInfo, String> supplier_id;

    @FXML
    private TableColumn<ViewSupplierInfo, String> supplier_name;

//    @FXML
//    private TableColumn<ViewSupplierInfo, String> supplier_email;

    @FXML
    private TableColumn<ViewSupplierInfo, String> supplier_contact;

    @FXML
    private TableColumn<ViewSupplierInfo, String> contact_person;

    @FXML
    private TableColumn<ViewSupplierInfo, String> supplier_address;

    @FXML
    private TableColumn<ViewSupplierInfo, String> supplier_city;

    @FXML
    private TableColumn<ViewSupplierInfo, String> supplier_companies;

    @FXML
    private TableColumn<ViewSupplierInfo, String> supplier_status;

    @FXML
    private TableColumn<ViewSupplierInfo, String> operations;
    @FXML    private AnchorPane inner_anchor;
    @FXML    private JFXDrawer drawer;
    @FXML    private BorderPane nav_setup;
    @FXML    private BorderPane menu_bar1;
    @FXML    private Pane btnpane;
    @FXML    private Button btn11;
    @FXML    private FontAwesomeIconView FA_Icon;

    @FXML
    private AnchorPane filter_pane;

    @FXML
    private HBox filter_hbox;

    @FXML
    private JFXTextField txt_supplier_id;

    @FXML
    private JFXTextField txt_supplier_name;

    @FXML
    private JFXTextField txt_supplier_email;

    @FXML
    private JFXTextField txt_supplier_contact;

    @FXML
    private JFXComboBox<String> txt_supplier_status;
    @FXML
    private StackPane stackPane;

    @FXML
    private HBox summary_hbox;

    @FXML
    private Label lbl_total;

    @FXML
    private Label lbl_active;

    @FXML
    private Label lbl_inactive;

    @FXML
    private JFXButton btn_add;
    @FXML
    private JFXButton btn_export;

    public ObservableList<ViewSupplierInfo> supplierDetail;
    PendingOrders objPendingOrders;
    MysqlCon objMysqlCon;
    Statement objStmt;
    Connection objCon;
    int unSeenOrders;
    private int summaryTotal;
    private int summaryActive;
    private int summaryInActive;

    public static String txtSupplierId = "";
    public static String txtSupplierName = "";
    public static String txtSupplierContact = "";
    public static String txtSupplierEmail = "";
    public static String txtSupplierStatus = "";
    public static boolean filter;

    public static Button btnView;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnSetup.setStyle("-fx-background-color: #47ab1e");
        unSeenOrders = 0;
        objMysqlCon = new MysqlCon();
        objStmt = objMysqlCon.stmt;
        objCon = objMysqlCon.con;
//        objPendingOrders = new PendingOrders();
//        unSeenOrders = objPendingOrders.getUnSeenOrders(objStmt, objCon);
        ViewSupplierInfo.stackPane = stackPane;

        if(!GlobalVariables.userType.equals("Admin"))
        {
            if(!GlobalVariables.rightCreate)
            {
                btn_add.setVisible(false);
                AnchorPane.setRightAnchor(btn_export, 15.0);
            }
        }

        btnpane.setOnMouseEntered(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(22);
                btnpane.setMinWidth(20.0);
                btn11.setMinWidth(20.0);

            }
        });
        btnpane.setOnMouseExited(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(14);
                btnpane.setMinWidth(10.0);
                btn11.setMinWidth(10.0);
            }
        });
        inner_anchor.setLeftAnchor(inner_anchor, 270d);
        drawer.setSidePane(nav_setup);
        drawer.open();

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        supplier_id.setCellValueFactory(new PropertyValueFactory<>("supplierId"));
        supplier_name.setCellValueFactory(new PropertyValueFactory<>("supplierName"));
//        supplier_email.setCellValueFactory(new PropertyValueFactory<>("supplierEmail"));
        supplier_contact.setCellValueFactory(new PropertyValueFactory<>("supplierContact"));
        contact_person.setCellValueFactory(new PropertyValueFactory<>("contactPerson"));
        supplier_address.setCellValueFactory(new PropertyValueFactory<>("supplierAddress"));
        supplier_city.setCellValueFactory(new PropertyValueFactory<>("supplierCity"));
        supplier_companies.setCellValueFactory(new PropertyValueFactory<>("supplierCompanies"));
        supplier_status.setCellValueFactory(new PropertyValueFactory<>("supplierStatus"));

        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_viewsuppliers.setItems(parseUserList());
        lbl_total.setText("Suppliers\n"+String.format("%,.0f", (float)summaryTotal));
        lbl_active.setText("Active\n"+String.format("%,.0f", (float)summaryActive));
        lbl_inactive.setText("In Active\n"+String.format("%,.0f", (float)summaryInActive));
    }

    public void follow() throws IOException
    {
    /*PathTransition transition;
    transition = new PathTransition();
    Polyline mpoly = new Polyline();
    mpoly.getPoints().addAll(new Double[]{
            0.0,0.0,
            0.0,0.0,
            -200.0,0.0});
    transition.setNode(btn11);
    transition.setDuration(Duration.seconds(2));
    transition.setPath(mpoly);
    transition.play();*/
        if (drawer.isOpened()) {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(-250);
            transition.setByY(0);
            transition.setCycleCount(1);
            transition.play();
            drawer.close();
            FA_Icon.setGlyphName("ANGLE_DOUBLE_RIGHT");
            FA_Icon.setGlyphSize(14);
            //inner_anchor.setRightAnchor(inner_anchor,1d);

            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(-90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(1.1000);
            scale.setToY(1);

            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();
            //
            //inner_anchor.setLeftAnchor(inner_anchor, 5d);
        } else {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(250);
            transition.setByY(0);
            transition.setCycleCount(1);
            FA_Icon.setGlyphName("ANGLE_DOUBLE_LEFT");
            FA_Icon.setGlyphSize(14);
            drawer.open();

            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(0.99947777);
            scale.setToY(1);

            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();
        }
    }

    private ObservableList<ViewSupplierInfo> parseUserList(){
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        ViewSupplierInfo objViewSupplierInfo = new ViewSupplierInfo();
        supplierDetail = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> supplierData;
        if(filter)
        {
            supplierData  = objViewSupplierInfo.getSupplierSearch(objStmt, objCon, txtSupplierId, txtSupplierName, txtSupplierContact, txtSupplierEmail, txtSupplierStatus);
            txt_supplier_id.setText(txtSupplierId);
            txt_supplier_name.setText(txtSupplierName);
            txt_supplier_contact.setText(txtSupplierContact);
            txt_supplier_email.setText(txtSupplierEmail);
            txt_supplier_status.setValue(txtSupplierStatus);
            filter = false;
            filter_pane.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
        }
        else
        {
            supplierData = objViewSupplierInfo.getSupplierInfo(objStmt, objCon);
        }
        for (int i = 0; i < supplierData.size(); i++)
        {
            summaryTotal++;
            if(supplierData.get(i).get(10).equals("Active"))
            {
                summaryActive++;
            }
            else if(supplierData.get(i).get(10).equals("In Active"))
            {
                summaryInActive++;
            }
            supplierDetail.add(new ViewSupplierInfo(String.valueOf(i+1), supplierData.get(i).get(0), ((supplierData.get(i).get(1) == null) ? "N/A" : supplierData.get(i).get(1)), supplierData.get(i).get(2), supplierData.get(i).get(3), supplierData.get(i).get(4), supplierData.get(i).get(5), supplierData.get(i).get(6), supplierData.get(i).get(7), supplierData.get(i).get(8), supplierData.get(i).get(9), supplierData.get(i).get(10)));
        }
        return supplierDetail;
    }

    @FXML
    void btnSearch(ActionEvent event) {
        txtSupplierId = txt_supplier_id.getText();
        txtSupplierName = txt_supplier_name.getText();
        txtSupplierContact = txt_supplier_contact.getText();
        txtSupplierEmail = txt_supplier_email.getText();
        txtSupplierStatus = txt_supplier_status.getValue();
        if(txtSupplierStatus == null)
        {
            txtSupplierStatus = "All";
        }
        filter = true;

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/view_suppliers.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void btnReset(ActionEvent event) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/view_suppliers.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void exportExcel(ActionEvent event) throws IOException {
        Workbook workbook = new HSSFWorkbook();
        Sheet spreadsheet = workbook.createSheet("data");
        Row row = spreadsheet.createRow(0);

        for (int j = 0; j < table_viewsuppliers.getColumns().size()-1; j++) {
            row.createCell(j).setCellValue(table_viewsuppliers.getColumns().get(j).getText());
        }

        for (int i = 0; i < table_viewsuppliers.getItems().size(); i++) {
            row = spreadsheet.createRow(i + 1);
            for (int j = 0; j < table_viewsuppliers.getColumns().size()-1; j++) {
                if(table_viewsuppliers.getColumns().get(j).getCellData(i) != null) {
                    row.createCell(j).setCellValue(table_viewsuppliers.getColumns().get(j).getCellData(i).toString());
                }
                else {
                    row.createCell(j).setCellValue("");
                }
            }
        }

        FileOutputStream fileOut = new FileOutputStream("data/View_Suppliers.xls");
        workbook.write(fileOut);
        fileOut.close();
    }

    public void addSupplier() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/enter_supplier.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
}
