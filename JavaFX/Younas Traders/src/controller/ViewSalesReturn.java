package controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXDrawer;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.util.StringConverter;
import model.*;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

public class ViewSalesReturn implements Initializable {

    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private AnchorPane filter_pane;

    @FXML
    private JFXDatePicker txt_from_date;

    @FXML
    private JFXDatePicker txt_to_date;

    @FXML
    private JFXComboBox<String> txt_day;

    @FXML
    private JFXComboBox<String> txt_return_reason;

    @FXML
    private HBox summary_hbox1;

    @FXML
    private Label lbl_items_returned;

    @FXML
    private Label lbl_return_price;

    @FXML
    private TableView<ViewSalesReturnInfo> table_viewsalesreturnlog;

    @FXML
    private TableColumn<ViewSalesReturnInfo, String> sr_no;

    @FXML
    private TableColumn<ViewSalesReturnInfo, String> return_date;

    @FXML
    private TableColumn<ViewSalesReturnInfo, String> return_day;

    @FXML
    private TableColumn<ViewSalesReturnInfo, String> return_id;

    @FXML
    private TableColumn<ViewSalesReturnInfo, String> dealer_name;

    @FXML
    private TableColumn<ViewSalesReturnInfo, String> dealer_contact;

    @FXML
    private TableColumn<ViewSalesReturnInfo, String> returned_qty;

    @FXML
    private TableColumn<ViewSalesReturnInfo, String> return_price;

    @FXML
    private TableColumn<ViewSalesReturnInfo, String> return_user;

    @FXML
    private TableColumn<ViewSalesReturnInfo, String> operations;

    @FXML
    private StackPane stackPane;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    @FXML
    private BorderPane brdrpane_navfinances;

    @FXML
    private BorderPane menu_bar;

    private ObservableList<ViewSalesReturnInfo> saleReturnDetails;
    private ViewSalesReturnInfo objViewSalesReturnInfo;
    ArrayList<ArrayList<String>> saleReturnData;
    public ArrayList<SaleReturnInfo> invoiceData;
    public static ArrayList<ArrayList<String>> saleReturnDetailData;
    private static ViewSaleReturnDetailInfo objViewSaleReturnDetailInfo;
    private SaleInvoicePrintInfo objSaleInvoicePrintInfo;
    ArrayList<String> invoicePrintData;

    float returnedItems = 0;
    float returnPrice = 0;

    public static String txtFromDate = "";
    public static String txtToDate = "";
    public static String txtDay = "All";
    public static String txtReturnReason = "All";
    public static boolean filter;

    MysqlCon objMysqlCon1;
    MysqlCon objMysqlCon2;
    MysqlCon objMysqlCon3;
    Statement objStmt1;
    Statement objStmt2;
    Statement objStmt3;
    Connection objCon;

    public static Button btnView;
    SimpleDateFormat fmt = new SimpleDateFormat("dd/MMM/yyyy");

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnSale.setStyle("-fx-background-color: #47ab1e");
        objMysqlCon1 = new MysqlCon();
        objMysqlCon2 = new MysqlCon();
        objMysqlCon3 = new MysqlCon();
        objStmt1 = objMysqlCon1.stmt;
        objStmt2 = objMysqlCon2.stmt;
        objStmt3 = objMysqlCon3.stmt;
        objCon = objMysqlCon1.con;
        objViewSalesReturnInfo = new ViewSalesReturnInfo();
        objSaleInvoicePrintInfo = new SaleInvoicePrintInfo();
        invoicePrintData = objSaleInvoicePrintInfo.getSaleInvoicePrintInfo(objStmt1, objCon);
        ViewSalesReturnInfo.stackPane = stackPane;

        saleReturnData = new ArrayList<>();
        invoiceData = new ArrayList<>();

        txt_from_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        txt_to_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        return_date.setCellValueFactory(new PropertyValueFactory<>("returnDate"));
        return_day.setCellValueFactory(new PropertyValueFactory<>("returnDay"));
        return_id.setCellValueFactory(new PropertyValueFactory<>("returnId"));
        dealer_name.setCellValueFactory(new PropertyValueFactory<>("dealerName"));
        dealer_contact.setCellValueFactory(new PropertyValueFactory<>("dealerContact"));
        returned_qty.setCellValueFactory(new PropertyValueFactory<>("returnQty"));
        return_price.setCellValueFactory(new PropertyValueFactory<>("returnPrice"));
        return_user.setCellValueFactory(new PropertyValueFactory<>("returnUser"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_viewsalesreturnlog.setItems(parseUserList());
        lbl_items_returned.setText("Items Returned\n"+String.format("%,.0f", returnedItems));
        lbl_return_price.setText("Return Price\n"+"Rs."+String.format("%,.0f", returnPrice)+"/-");
    }

    private ObservableList<ViewSalesReturnInfo> parseUserList() {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        saleReturnDetails = FXCollections.observableArrayList();

        if (filter) {
            saleReturnData = objViewSalesReturnInfo.getReturnInfoSearch(objStmt1, objCon, txtFromDate, txtToDate, txtDay, txtReturnReason);
            if (txtFromDate != null && !txtFromDate.equals("")) {
                txt_from_date.setValue(GlobalVariables.LOCAL_DATE(txtFromDate));
            }
            if (txtToDate != null && !txtToDate.equals("")) {
                txt_to_date.setValue(GlobalVariables.LOCAL_DATE(txtToDate));
            }
            txt_day.setValue(txtDay);
            txt_return_reason.setValue(txtReturnReason);
            filter = false;
            filter_pane.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
            txtFromDate = "";
            txtToDate = "";
            txtDay = "All";
            txtReturnReason = "All";
        } else {
            saleReturnData = objViewSalesReturnInfo.getReturnInfo(objStmt1, objCon, GlobalVariables.getStDate(), GlobalVariables.getStDate());
        }

        saleReturnDetailData = new ArrayList<>();

        for (int i = 0; i < saleReturnData.size(); i++)
        {
            objViewSaleReturnDetailInfo = new ViewSaleReturnDetailInfo(saleReturnData.get(i).get(2));
            objViewSaleReturnDetailInfo.getSaleReturnDetailInfo(objStmt1, objCon);
            returnedItems += (saleReturnData.get(i).get(5) != null && !saleReturnData.get(i).get(5).equals("-")) ? Float.parseFloat(saleReturnData.get(i).get(5)) : 0;
            returnPrice += (saleReturnData.get(i).get(6) != null && !saleReturnData.get(i).get(6).equals("-")) ? Float.parseFloat(saleReturnData.get(i).get(6)) : 0;
            saleReturnDetails.add(new ViewSalesReturnInfo(String.valueOf(i+1), ((saleReturnData.get(i).get(0) == null || saleReturnData.get(i).get(0).equals("")) ? "N/A" : saleReturnData.get(i).get(0)), saleReturnData.get(i).get(1), saleReturnData.get(i).get(2), saleReturnData.get(i).get(3), saleReturnData.get(i).get(4), String.format("%,.0f", Float.parseFloat(saleReturnData.get(i).get(5))), String.format("%,.2f", Float.parseFloat(saleReturnData.get(i).get(6))), saleReturnData.get(i).get(7), saleReturnData.get(i).get(8)));
        }
        return saleReturnDetails;
    }

    @FXML
    void btnPrintAllReturns(ActionEvent event) throws JRException {
        invoiceData = new ArrayList<>();
        int j = 0;
        for (int i = 0; i < saleReturnData.size(); i++)
        {
            String returnId = saleReturnData.get(i).get(2);
            String dealerId = saleReturnData.get(i).get(9);
            String dealerName = saleReturnData.get(i).get(3);
            String dealerLic = saleReturnData.get(i).get(10);
            String dealerAddress = saleReturnData.get(i).get(11);
            String dealerContact = saleReturnData.get(i).get(4);
            String dealerNTN = saleReturnData.get(i).get(12);
            String dealerCnic = saleReturnData.get(i).get(13);
            String dealerSalesTaxNo = " - ";
            String invoiceDate = saleReturnData.get(i).get(0);
            String deliveredUser = saleReturnData.get(i).get(7);
            String totalQty = "0";
            String finalPrice = saleReturnData.get(i).get(6);
            SaleReturnItemsInfo objItemsInfos;
            List<SaleReturnItemsInfo> itemsList = null;
            itemsList = new ArrayList<>();
            int qty = 0;
            int num = 1;
            while(i<saleReturnData.size() && j<saleReturnDetailData.size() && saleReturnDetailData.get(j).get(6).equals(saleReturnData.get(i).get(2)))
            {
                objItemsInfos = new SaleReturnItemsInfo(String.valueOf(num), saleReturnDetailData.get(j).get(0), saleReturnDetailData.get(j).get(1), saleReturnDetailData.get(j).get(2), String.format("%,.2f", Float.parseFloat(saleReturnDetailData.get(j).get(7))), String.format("%,.0f", Float.parseFloat(saleReturnDetailData.get(j).get(3))), saleReturnDetailData.get(j).get(5), String.format("%,.2f", Float.parseFloat(saleReturnDetailData.get(j).get(4))));
                qty += Integer.parseInt(saleReturnDetailData.get(j).get(3));
                totalQty = String.format("%,.0f", (float)qty);
                objItemsInfos.setTotalQty(totalQty);
                objItemsInfos.setFinalPrice(String.format("%,.2f", Float.parseFloat(finalPrice)));
                objItemsInfos.setTextAmount(NumberToWords.convert(Long.parseLong(String.format("%.0f", Float.parseFloat(finalPrice)))));
                itemsList.add(objItemsInfos);
                j++;
                num++;
            }
            invoiceData.add(new SaleReturnInfo(returnId, dealerId, dealerName, dealerLic, dealerAddress, dealerContact, dealerNTN, dealerCnic, dealerSalesTaxNo, invoiceDate, deliveredUser, itemsList, totalQty, finalPrice));
        }
        InputStream objIO = ViewSalesReturn.class.getResourceAsStream("/reports/SaleReturn.jrxml");
        JasperReport objReport = JasperCompileManager.compileReport(objIO);
        JasperPrint objPrint = JasperFillManager.fillReport(objReport, null, new JRBeanCollectionDataSource(invoiceData));
        JasperViewer.viewReport(objPrint, false);
    }

    @FXML
    void btnReset(ActionEvent event) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/view_sales_return.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void btnSearch(ActionEvent event) {
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date selectedDate = null;
        if(txt_from_date.getValue() != null)
        {
            txtFromDate = txt_from_date.getValue().toString();
            try {
                selectedDate = sdf.parse(txtFromDate);
                txtFromDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if(txt_to_date.getValue() != null)
        {
            txtToDate = txt_to_date.getValue().toString();
            try {
                selectedDate = sdf.parse(txtToDate);
                txtToDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        txtDay = txt_day.getValue();
        if(txtDay == null)
        {
            txtDay = "All";
        }
        txtReturnReason = txt_return_reason.getValue();
        if(txtReturnReason == null)
        {
            txtReturnReason = "All";
        }
        filter = true;

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/view_sales_return.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void follow(MouseEvent event) {

    }

    @FXML
    void showItemsReturned(MouseEvent event) {

    }

    @FXML
    void showReturnPrice(MouseEvent event) {

    }
}
