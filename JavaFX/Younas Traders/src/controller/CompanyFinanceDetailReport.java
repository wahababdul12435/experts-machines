package controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.util.StringConverter;
import model.*;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

public class CompanyFinanceDetailReport implements Initializable {

    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private Label lbl_company_id;

    @FXML
    private Label lbl_company_name;

    @FXML
    private Label lbl_company_contact;

    @FXML
    private Label lbl_company_address;

    @FXML
    private Label lbl_company_person;

    @FXML
    private Label lbl_company_email;

    @FXML
    private Label lbl_company_created;

    @FXML
    private Label lbl_company_updated;

    @FXML
    private Label lbl_company_status;

    @FXML
    private ImageView img_user;

    @FXML
    private BarChart<Integer, String> cash_sent_chart;

    @FXML
    private CategoryAxis timeAxis;

    @FXML
    private NumberAxis sentAxis;

    @FXML
    private AnchorPane filter_pane;

    @FXML
    private JFXDatePicker txt_from_date;

    @FXML
    private JFXDatePicker txt_to_date;

    @FXML
    private JFXComboBox<String> txt_day;

    @FXML
    private JFXTextField txt_from_amount;

    @FXML
    private JFXTextField txt_to_amount;

    @FXML
    private JFXComboBox<String> txt_type;

    @FXML
    private TableView<CompanyFinanceDetailReportInfo> table_companycashlog;

    @FXML
    private TableColumn<CompanyFinanceDetailReportInfo, String> sr_no;

    @FXML
    private TableColumn<CompanyFinanceDetailReportInfo, String> date;

    @FXML
    private TableColumn<CompanyFinanceDetailReportInfo, String> day;

    @FXML
    private TableColumn<CompanyFinanceDetailReportInfo, String> cash_sent;

    @FXML
    private TableColumn<CompanyFinanceDetailReportInfo, String> cash_return;

    @FXML
    private TableColumn<CompanyFinanceDetailReportInfo, String> cash_waive_off;

    @FXML
    private TableColumn<CompanyFinanceDetailReportInfo, String> operations;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    @FXML
    private BorderPane brdrpane_navfinances;

    @FXML
    private StackPane stackPane;

    @FXML
    private BorderPane menu_bar;

    private CompanyInfo objCompanyInfo;
    private CompanyFinanceDetailReportInfo objCompanyFinanceDetailReportInfo;
    private String currentCompanyId;
    private String selectedImagePath;
    private ArrayList<String> companyInfo;
    private ObservableList<CompanyFinanceDetailReportInfo> companyDetails;
    ArrayList<ArrayList<String>> stDates = new ArrayList<>();
    ArrayList<Date> dates = new ArrayList<Date>();

    public static String txtFromDate = "";
    public static String txtToDate = "";
    public static String txtDay = "All";
    public static String txtFromAmount = "";
    public static String txtToAmount = "";
    public static String txtType = "All";
    public static boolean filter;

    public static MenuButton btnView;
    SimpleDateFormat fmt = new SimpleDateFormat("dd/MMM/yyyy");
    int span;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnFinance.setStyle("-fx-background-color: #47ab1e");
        CompanyFinanceDetailReportInfo.stackPane = stackPane;
        objCompanyInfo = new CompanyInfo();
        objCompanyFinanceDetailReportInfo = new CompanyFinanceDetailReportInfo();
        if(!CompanyFinanceReportInfo.companyFinanceId.equals(""))
        {
            currentCompanyId = CompanyFinanceReportInfo.companyFinanceId;
            CompanyFinanceReportInfo.companyFinanceId = "";
        }
        companyInfo = objCompanyInfo.getCompanyDetail(currentCompanyId);
        lbl_company_id.setText(((companyInfo.get(0) == null) ? "N/A" : companyInfo.get(0)));
        lbl_company_name.setText(companyInfo.get(1));
        lbl_company_contact.setText(companyInfo.get(2));
        lbl_company_address.setText(companyInfo.get(3));
        lbl_company_person.setText((companyInfo.get(4) == null) ? "N/A" : companyInfo.get(4));
        lbl_company_email.setText(companyInfo.get(5));
        lbl_company_created.setText(companyInfo.get(6));
        lbl_company_updated.setText((companyInfo.get(7) == null) ? "N/A" : companyInfo.get(7));
        lbl_company_status.setText(companyInfo.get(8));
        if(companyInfo.get(9) != null && !companyInfo.get(9).equals(""))
        {
            selectedImagePath = companyInfo.get(9);
            File file = new File(selectedImagePath);
            boolean exists = file.exists();
            if(exists)
            {
                Image image = new Image(file.toURI().toString());
                img_user.setImage(image);
            }
        }

        txt_from_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        txt_to_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        date.setCellValueFactory(new PropertyValueFactory<>("date"));
        day.setCellValueFactory(new PropertyValueFactory<>("day"));
        cash_sent.setCellValueFactory(new PropertyValueFactory<>("cashSent"));
        cash_return.setCellValueFactory(new PropertyValueFactory<>("cashReturn"));
        cash_waive_off.setCellValueFactory(new PropertyValueFactory<>("cashWaiveOff"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_companycashlog.setItems(parseUserList());

        timeAxis.setLabel("Time Span");
        sentAxis.setLabel("Cash Sent");

//        for(int i=0; i<stDates.size(); i++)
//        {
//            for(int j=0; j<2; j++)
//            {
//                System.out.print(stDates.get(i).get(j)+"  --  ");
//            }
//            System.out.println();
//        }

        XYChart.Series dataSeries1 = new XYChart.Series();
        XYChart.Series dataSeries2 = new XYChart.Series();
        XYChart.Series dataSeries3 = new XYChart.Series();
        for(int i=0; i<stDates.size()-1; i++)
        {
            if(stDates.get(i).get(0).equals(stDates.get(stDates.size()-1).get(0)))
            {
                dataSeries1.getData().add(new XYChart.Data((Integer.parseInt(stDates.get(i).get(1))), stDates.get(i+1).get(0)));
                dataSeries2.getData().add(new XYChart.Data((Integer.parseInt(stDates.get(i).get(2))), stDates.get(i+1).get(0)));
                dataSeries3.getData().add(new XYChart.Data((Integer.parseInt(stDates.get(i).get(3))), stDates.get(i+1).get(0)));
                break;
            }
            String stLowerBoundDate = stDates.get(i).get(0);
            Date lowerBoundDate = null;
            try {
                lowerBoundDate = fmt.parse(stLowerBoundDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Calendar calTemp = Calendar.getInstance();
            calTemp.setTime(lowerBoundDate);
            calTemp.add(Calendar.DAY_OF_MONTH, -1);
            try {
                lowerBoundDate = fmt.parse(fmt.format(calTemp.getTime()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            fmt.format(calTemp.getTime());
            stLowerBoundDate = fmt.format(calTemp.getTime());
            if(span > 1)
            {
                dataSeries1.getData().add(new XYChart.Data((Integer.parseInt(stDates.get(i).get(1))), stDates.get(i+1).get(0)+"\n|\n"+stLowerBoundDate));
                dataSeries2.getData().add(new XYChart.Data((Integer.parseInt(stDates.get(i).get(2))), stDates.get(i+1).get(0)+"\n|\n"+stLowerBoundDate));
                dataSeries3.getData().add(new XYChart.Data((Integer.parseInt(stDates.get(i).get(3))), stDates.get(i+1).get(0)+"\n|\n"+stLowerBoundDate));
            }
            else
            {
                dataSeries1.getData().add(new XYChart.Data((Integer.parseInt(stDates.get(i).get(1))), stDates.get(i).get(0)));
                dataSeries2.getData().add(new XYChart.Data((Integer.parseInt(stDates.get(i).get(2))), stDates.get(i).get(0)));
                dataSeries3.getData().add(new XYChart.Data((Integer.parseInt(stDates.get(i).get(3))), stDates.get(i).get(0)));
            }

        }
        dataSeries1.setName("Sent");
        dataSeries2.setName("Received");
        dataSeries3.setName("Waive Off");
        cash_sent_chart.getData().add(dataSeries1);
        cash_sent_chart.getData().add(dataSeries2);
        cash_sent_chart.getData().add(dataSeries3);
    }

    private ObservableList<CompanyFinanceDetailReportInfo> parseUserList(){
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        companyDetails = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> companyFinanceData;

        if(filter)
        {
            companyFinanceData = objCompanyFinanceDetailReportInfo.getCompanyFinanceDetailSearch(objStmt, objCon, currentCompanyId, txtFromDate, txtToDate, txtDay, txtFromAmount, txtToAmount, txtType);
            if(txtFromDate != null && !txtFromDate.equals(""))
            {
                txt_from_date.setValue(LOCAL_DATE(txtFromDate));
            }
            if(txtToDate != null && !txtToDate.equals(""))
            {
                txt_to_date.setValue(LOCAL_DATE(txtToDate));
            }
            txt_day.setValue(txtDay);
            txt_from_amount.setText(txtFromAmount);
            txt_to_amount.setText(txtToAmount);
            txt_type.setValue(txtType);
            filter = false;
            filter_pane.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
        }
        else
        {
            companyFinanceData = objCompanyFinanceDetailReportInfo.getCompanyFinanceDetail(objStmt, objCon, currentCompanyId);
        }


        Date tempDate = null;
        if(companyFinanceData.size() > 0)
        {
            String stEndDate = companyFinanceData.get(0).get(3);
            String stStartDate = companyFinanceData.get(companyFinanceData.size()-1).get(3);

            Date endDate = null;
            Date startDate = null;
            try {
                startDate = fmt.parse(stStartDate);
                endDate = fmt.parse(stEndDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            long diff = endDate.getTime() - startDate.getTime();
            diff = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
            span = (int) (diff/4);
            span = span * -1;
            Calendar cal = Calendar.getInstance();
            cal.setTime(endDate);
            ArrayList<String> temp = new ArrayList<>();
            temp.add(fmt.format(cal.getTime()));
            temp.add("0");
            temp.add("0");
            temp.add("0");
            stDates.add(temp);
            tempDate = endDate;
            dates.add(tempDate);
            for(int i=1; i<=5; i++)
            {
                temp = new ArrayList<>();
                cal.add(Calendar.DAY_OF_MONTH, span);
                try {
                    tempDate = fmt.parse(fmt.format(cal.getTime()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                temp.add(fmt.format(cal.getTime()));
                temp.add("0");
                temp.add("0");
                temp.add("0");
                stDates.add(temp);
                dates.add(tempDate);
            }
        }


        for (int i = 0; i < companyFinanceData.size(); i++)
        {
            try {
                tempDate = fmt.parse(companyFinanceData.get(i).get(3));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if(companyFinanceData.get(i).get(6).equals("Sent"))
            {
                if((tempDate.before(dates.get(0)) && tempDate.after(dates.get(1))) || tempDate.equals(dates.get(0)))
                {
//                    System.out.println("1st: Small Date: "+dates.get(0)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(1));
                    String value = String.valueOf(Integer.parseInt(stDates.get(0).get(1)) + Integer.parseInt(companyFinanceData.get(i).get(5)));
                    stDates.get(0).set(1, value);
                }
                else if((tempDate.before(dates.get(1)) && tempDate.after(dates.get(2))) || tempDate.equals(dates.get(1)))
                {
//                    System.out.println("2nd: Small Date: "+dates.get(1)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(2));
                    String value = String.valueOf(Integer.parseInt(stDates.get(1).get(1)) + Integer.parseInt(companyFinanceData.get(i).get(5)));
                    stDates.get(1).set(1, value);
                }
                else if((tempDate.before(dates.get(2)) && tempDate.after(dates.get(3))) || tempDate.equals(dates.get(2)))
                {
//                    System.out.println("3rd: Small Date: "+dates.get(2)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(3));
                    String value = String.valueOf(Integer.parseInt(stDates.get(2).get(1)) + Integer.parseInt(companyFinanceData.get(i).get(5)));
                    stDates.get(2).set(1, value);
                }
                else if((tempDate.before(dates.get(3)) && tempDate.after(dates.get(4))) || tempDate.equals(dates.get(3)))
                {
//                    System.out.println("4th: Small Date: "+dates.get(3)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(4));
                    String value = String.valueOf(Integer.parseInt(stDates.get(3).get(1)) + Integer.parseInt(companyFinanceData.get(i).get(5)));
                    stDates.get(3).set(1, value);
                }
                else if((tempDate.before(dates.get(4)) && tempDate.after(dates.get(5))) || tempDate.equals(dates.get(4)))
                {
//                    System.out.println("4th: Small Date: "+dates.get(3)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(4));
                    String value = String.valueOf(Integer.parseInt(stDates.get(4).get(1)) + Integer.parseInt(companyFinanceData.get(i).get(5)));
                    stDates.get(4).set(1, value);
                }
                companyDetails.add(new CompanyFinanceDetailReportInfo(String.valueOf(i+1), companyFinanceData.get(i).get(0), companyFinanceData.get(i).get(1), companyFinanceData.get(i).get(2), companyFinanceData.get(i).get(3), companyFinanceData.get(i).get(4), companyFinanceData.get(i).get(5), "-", "-", companyFinanceData.get(i).get(7)));
            }
            else if(companyFinanceData.get(i).get(6).equals("Received"))
            {
                if((tempDate.before(dates.get(0)) && tempDate.after(dates.get(1))) || tempDate.equals(dates.get(0)))
                {
//                    System.out.println("1st: Small Date: "+dates.get(0)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(1));
                    String value = String.valueOf(Integer.parseInt(stDates.get(0).get(2)) + Integer.parseInt(companyFinanceData.get(i).get(5)));
                    stDates.get(0).set(2, value);
                }
                else if((tempDate.before(dates.get(1)) && tempDate.after(dates.get(2))) || tempDate.equals(dates.get(1)))
                {
//                    System.out.println("2nd: Small Date: "+dates.get(1)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(2));
                    String value = String.valueOf(Integer.parseInt(stDates.get(1).get(2)) + Integer.parseInt(companyFinanceData.get(i).get(5)));
                    stDates.get(1).set(2, value);
                }
                else if((tempDate.before(dates.get(2)) && tempDate.after(dates.get(3))) || tempDate.equals(dates.get(2)))
                {
//                    System.out.println("3rd: Small Date: "+dates.get(2)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(3));
                    String value = String.valueOf(Integer.parseInt(stDates.get(2).get(2)) + Integer.parseInt(companyFinanceData.get(i).get(5)));
                    stDates.get(2).set(2, value);
                }
                else if((tempDate.before(dates.get(3)) && tempDate.after(dates.get(4))) || tempDate.equals(dates.get(3)))
                {
//                    System.out.println("4th: Small Date: "+dates.get(3)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(4));
                    String value = String.valueOf(Integer.parseInt(stDates.get(3).get(2)) + Integer.parseInt(companyFinanceData.get(i).get(5)));
                    stDates.get(3).set(2, value);
                }
                else if((tempDate.before(dates.get(4)) && tempDate.after(dates.get(5))) || tempDate.equals(dates.get(4)))
                {
//                    System.out.println("4th: Small Date: "+dates.get(3)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(4));
                    String value = String.valueOf(Integer.parseInt(stDates.get(4).get(2)) + Integer.parseInt(companyFinanceData.get(i).get(5)));
                    stDates.get(4).set(2, value);
                }
                companyDetails.add(new CompanyFinanceDetailReportInfo(String.valueOf(i+1), companyFinanceData.get(i).get(0), companyFinanceData.get(i).get(1), companyFinanceData.get(i).get(2), companyFinanceData.get(i).get(3), companyFinanceData.get(i).get(4), "-", companyFinanceData.get(i).get(5), "-", companyFinanceData.get(i).get(7)));
            }
            else
            {
                if((tempDate.before(dates.get(0)) && tempDate.after(dates.get(1))) || tempDate.equals(dates.get(0)))
                {
//                    System.out.println("1st: Small Date: "+dates.get(0)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(1));
                    String value = String.valueOf(Integer.parseInt(stDates.get(0).get(3)) + Integer.parseInt(companyFinanceData.get(i).get(5)));
                    stDates.get(0).set(3, value);
                }
                else if((tempDate.before(dates.get(1)) && tempDate.after(dates.get(2))) || tempDate.equals(dates.get(1)))
                {
//                    System.out.println("2nd: Small Date: "+dates.get(1)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(2));
                    String value = String.valueOf(Integer.parseInt(stDates.get(1).get(3)) + Integer.parseInt(companyFinanceData.get(i).get(5)));
                    stDates.get(1).set(3, value);
                }
                else if((tempDate.before(dates.get(2)) && tempDate.after(dates.get(3))) || tempDate.equals(dates.get(2)))
                {
//                    System.out.println("3rd: Small Date: "+dates.get(2)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(3));
                    String value = String.valueOf(Integer.parseInt(stDates.get(2).get(3)) + Integer.parseInt(companyFinanceData.get(i).get(5)));
                    stDates.get(2).set(3, value);
                }
                else if((tempDate.before(dates.get(3)) && tempDate.after(dates.get(4))) || tempDate.equals(dates.get(3)))
                {
//                    System.out.println("4th: Small Date: "+dates.get(3)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(4));
                    String value = String.valueOf(Integer.parseInt(stDates.get(3).get(3)) + Integer.parseInt(companyFinanceData.get(i).get(5)));
                    stDates.get(3).set(3, value);
                }
                else if((tempDate.before(dates.get(4)) && tempDate.after(dates.get(5))) || tempDate.equals(dates.get(4)))
                {
//                    System.out.println("4th: Small Date: "+dates.get(3)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(4));
                    String value = String.valueOf(Integer.parseInt(stDates.get(4).get(3)) + Integer.parseInt(companyFinanceData.get(i).get(5)));
                    stDates.get(4).set(3, value);
                }
                companyDetails.add(new CompanyFinanceDetailReportInfo(String.valueOf(i+1), companyFinanceData.get(i).get(0), companyFinanceData.get(i).get(1), companyFinanceData.get(i).get(2), companyFinanceData.get(i).get(3), companyFinanceData.get(i).get(4), "-", "-", companyFinanceData.get(i).get(5), companyFinanceData.get(i).get(7)));
            }

        }
//        System.out.println("   -----------------------------   ");
//        System.out.println("   -----------------------------   ");
//        System.out.println("   -----------------------------   ");
        return companyDetails;
    }

    @FXML
    void follow(MouseEvent event) {

    }

    @FXML
    void btnReset(ActionEvent event) {
        CompanyFinanceReportInfo.companyFinanceId = currentCompanyId;
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/company_finance_detail.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void btnSearch(ActionEvent event) {
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date selectedDate = null;
        if(txt_from_date.getValue() != null)
        {
            txtFromDate = txt_from_date.getValue().toString();
            try {
                selectedDate = sdf.parse(txtFromDate);
                txtFromDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if(txt_to_date.getValue() != null)
        {
            txtToDate = txt_to_date.getValue().toString();
            try {
                selectedDate = sdf.parse(txtToDate);
                txtToDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        txtDay = txt_day.getValue();
        if(txtDay == null)
        {
            txtDay = "All";
        }
        txtFromAmount = txt_from_amount.getText();
        txtToAmount = txt_to_amount.getText();
        txtType = txt_type.getValue();
        if(txtType == null)
        {
            txtType = "All";
        }
        filter = true;

        CompanyFinanceReportInfo.companyFinanceId = currentCompanyId;
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/company_finance_detail.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    public static final LocalDate LOCAL_DATE (String dateString) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MMM/yyyy");
        LocalDate localDate = LocalDate.parse(dateString, formatter);
        return localDate;
    }
}
