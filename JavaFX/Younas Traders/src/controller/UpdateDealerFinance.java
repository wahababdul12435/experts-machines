package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import model.*;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.ResourceBundle;

public class UpdateDealerFinance implements Initializable {

    @FXML
    private Label label_update;

    @FXML
    private JFXTextField txt_dealer_name;

    @FXML
    private JFXDatePicker txt_date;

    @FXML
    private JFXTextField txt_amount;

    @FXML
    private JFXComboBox<String> txt_type;

    @FXML
    private JFXButton dialog_dealer_finance_close;

    @FXML
    private JFXButton dialog_dealer_finance_update;

    public static String paymentId = "";
    public static String dealerId = "";
    public static String dealerName = "";
    public static String date = "";
    public static String preCashAmount = "";
    public static String cashAmount = "";
    public static String preCashType = "";
    public static String cashType = "";
    public static String returnPage = "";

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        txt_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }

            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        txt_dealer_name.setText(dealerName);
        txt_date.setValue(LOCAL_DATE(date));
        txt_amount.setText(cashAmount);
        txt_type.setValue(cashType);
        dialog_dealer_finance_close.setOnAction((action)->closeDialog());
        dialog_dealer_finance_update.setOnAction((action)->updateDealerFinance());
    }

    public void closeDialog()
    {
        if(returnPage.equals("View Dealer Finance"))
        {
            ViewDealerFinanceInfo.dialog.close();
            ViewDealerFinanceInfo.stackPane.setVisible(false);
        }
        else
        {
            DealerFinanceDetailReportInfo.dialog.close();
            DealerFinanceDetailReportInfo.stackPane.setVisible(false);
        }
    }

    public void updateDealerFinance() {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        DealerFinanceDetailReportInfo objDealerFinanceDetailReportInfo = new DealerFinanceDetailReportInfo();
        date = txt_date.getValue().toString();
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date selectedDate = null;
        try {
            selectedDate = sdf.parse(date);
            date = sdf2.format(selectedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        int day;
        String stDay = "";
        day = selectedDate.getDay();
        if(day == 0)
            stDay = "Sunday";
        else if(day == 1)
            stDay = "Monday";
        else if(day == 2)
            stDay = "Tuesday";
        else if(day == 3)
            stDay = "Wednesday";
        else if(day == 4)
            stDay = "Thursday";
        else if(day == 5)
            stDay = "Friday";
        else if(day == 6)
            stDay = "Saturday";
        cashAmount = txt_amount.getText();
        cashType = txt_type.getValue();
        objDealerFinanceDetailReportInfo.updateDealerFinance(objStmt, objCon, paymentId, dealerId, preCashAmount, preCashType, date, stDay, cashAmount, cashType);

        Parent root = null;

        if(returnPage.equals("View Dealer Finance"))
        {
            try {
                root = FXMLLoader.load(getClass().getResource("/view/view_dealer_finance.fxml"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else
        {
            try {
                root = FXMLLoader.load(getClass().getResource("/view/dealer_finance_detail_report.fxml"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    public static final LocalDate LOCAL_DATE (String dateString) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MMM/yyyy");
        LocalDate localDate = LocalDate.parse(dateString, formatter);
        return localDate;
    }
}