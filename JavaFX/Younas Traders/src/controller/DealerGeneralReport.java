package controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.util.StringConverter;
import model.*;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;

import java.io.*;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

public class DealerGeneralReport implements Initializable {
    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private TableView<DealerGeneralReportInfo> table_dealergeneralreport;

    @FXML
    private TableColumn<DealerGeneralReportInfo, String> sr_no;

    @FXML
    private TableColumn<DealerGeneralReportInfo, String> dealer_id;

    @FXML
    private TableColumn<DealerGeneralReportInfo, String> dealer_name;

    @FXML
    private TableColumn<DealerGeneralReportInfo, String> area_name;

    @FXML
    private TableColumn<DealerGeneralReportInfo, String> dealer_type;

    @FXML
    private TableColumn<DealerGeneralReportInfo, String> booked;

    @FXML
    private TableColumn<DealerGeneralReportInfo, String> delivered;

    @FXML
    private TableColumn<DealerGeneralReportInfo, String> returned;

    @FXML
    private TableColumn<DealerGeneralReportInfo, String> booking_price;

    @FXML
    private TableColumn<DealerGeneralReportInfo, String> discount_given;

    @FXML
    private TableColumn<DealerGeneralReportInfo, String> items_returned;

    @FXML
    private TableColumn<DealerGeneralReportInfo, String> return_price;

    @FXML
    private TableColumn<DealerGeneralReportInfo, String> cash_collection;

    @FXML
    private TableColumn<DealerGeneralReportInfo, String> cash_return;

    @FXML
    private TableColumn<DealerGeneralReportInfo, String> cash_waive_off;

    @FXML
    private TableColumn<DealerGeneralReportInfo, String> cash_pending;

    @FXML
    private TableColumn<DealerGeneralReportInfo, String> dealer_status;

    @FXML
    private TableColumn<DealerGeneralReportInfo, String> operations;

    @FXML
    private AnchorPane filter_pane;

    @FXML
    private HBox filter_hbox;

    @FXML
    private JFXTextField txt_dealer_id;

    @FXML
    private JFXTextField txt_dealer_name;

    @FXML
    private JFXTextField txt_dealer_contact;

    @FXML
    private JFXComboBox<String> txt_district_name;

    @FXML
    private JFXComboBox<String> txt_city_name;

    @FXML
    private JFXComboBox<String> txt_area_name;

    @FXML
    private JFXComboBox<String> txt_dealer_type;

    @FXML
    private JFXDatePicker txt_from_date;

    @FXML
    private JFXDatePicker txt_to_date;

    @FXML
    private HBox summary_hbox1;

    @FXML
    private Label lbl_booked;

    @FXML
    private Label lbl_delivered;

    @FXML
    private Label lbl_returned;

    @FXML
    private Label lbl_booking_price;

    @FXML
    private Label lbl_discount_given;

    @FXML
    private Label lbl_items_returned;

    @FXML
    private HBox summary_hbox2;

    @FXML
    private Label lbl_return_price;

    @FXML
    private Label lbl_cash_collection;

    @FXML
    private Label lbl_cash_return;

    @FXML
    private Label lbl_cash_waiveoff;

    @FXML
    private Label lbl_cash_pending;
    @FXML
    private JFXDrawer drawer;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    @FXML
    private BorderPane brdrpane_navfinances;

    @FXML
    private BorderPane menu_bar;

    public ArrayList<DealerGeneralReportInfo> summaryData;
    MysqlCon objMysqlCon1;
    MysqlCon objMysqlCon2;
    MysqlCon objMysqlCon3;
    Statement objStmt1;
    Statement objStmt2;
    Statement objStmt3;
    Connection objCon;
    DistrictInfo objDistrictInfo;
    CityInfo objCityInfo;
    AreaInfo objAreaInfo;
    private ArrayList<ArrayList<String>> districtsData;
    private ArrayList<ArrayList<String>> citiesData;
    private ArrayList<ArrayList<String>> areasData;
    ArrayList<String> districtNameList;
    ArrayList<String> cityNameList;
    ArrayList<String> chosenCityIds = new ArrayList<>();
    ArrayList<String> areaNameList;
    ArrayList<String> chosenAreaIds = new ArrayList<>();
    FilteredList<String> filteredItems;

    float totalBooked = 0;
    float totalDelivered = 0;
    float totalReturned = 0;
    float bookingPrice = 0;
    float discountGiven = 0;
    float itemsReturned = 0;
    float returnPrice = 0;
    float cashCollection = 0;
    float cashReturn = 0;
    float cashWaiveOff = 0;
    float cashPending = 0;

    public static String txtDealerId = "";
    public static String txtDealerName = "";
    public static String txtDealerContact = "";
    public static String txtDistrictName = "All";
    public static String txtDistrictId = "All";
    public static String txtCityName = "All";
    public static String txtCityId = "All";
    public static String txtAreaName = "All";
    public static String txtAreaId = "All";
    public static String txtDealerType = "";
    public static String txtFromDate = "";
    public static String txtToDate = "";
    public static boolean filter;
    public static String summary = "";

    public static MenuButton btnView;
    public ObservableList<DealerGeneralReportInfo> dealersReportDetail;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnReport.setStyle("-fx-background-color: #47ab1e");
        objDistrictInfo = new DistrictInfo();
        objCityInfo = new CityInfo();
        objAreaInfo = new AreaInfo();
        objMysqlCon1 = new MysqlCon();
        objMysqlCon2 = new MysqlCon();
        objMysqlCon3 = new MysqlCon();
        objStmt1 = objMysqlCon1.stmt;
        objStmt2 = objMysqlCon2.stmt;
        objStmt3 = objMysqlCon3.stmt;
        objCon = objMysqlCon1.con;
        districtsData = objDistrictInfo.getDistrictsWithIds(objStmt1, objCon);
        citiesData = objCityInfo.getCitiesWithIds(objStmt1, objCon);
        areasData = objAreaInfo.getAreasWithIds(objStmt1, objCon);
        districtNameList = GlobalVariables.getArrayColumn(districtsData, 2);
        districtNameList.add(0, "All");
        filteredItems = GlobalVariables.setSearchComboBox(districtNameList, txt_district_name);
        txt_district_name.setItems(filteredItems);
        cityNameList = GlobalVariables.getArrayColumn(citiesData, 2);
        cityNameList.add(0, "All");
        filteredItems = GlobalVariables.setSearchComboBox(cityNameList, txt_city_name);
        txt_city_name.setItems(filteredItems);
        areaNameList = GlobalVariables.getArrayColumn(areasData, 2);
        areaNameList.add(0, "All");
        filteredItems = GlobalVariables.setSearchComboBox(areaNameList, txt_area_name);
        txt_area_name.setItems(filteredItems);

        inner_anchor.setLeftAnchor(inner_anchor, 280d);
        drawer.setSidePane(brdrpane_navfinances);
        drawer.open();

        txt_from_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        txt_to_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        summaryData = new ArrayList<>();
        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        dealer_id.setCellValueFactory(new PropertyValueFactory<>("dealerId"));
        dealer_name.setCellValueFactory(new PropertyValueFactory<>("dealerName"));
        area_name.setCellValueFactory(new PropertyValueFactory<>("areaName"));
        dealer_type.setCellValueFactory(new PropertyValueFactory<>("dealerType"));
        booked.setCellValueFactory(new PropertyValueFactory<>("booked"));
        delivered.setCellValueFactory(new PropertyValueFactory<>("delivered"));
        returned.setCellValueFactory(new PropertyValueFactory<>("returned"));
        booking_price.setCellValueFactory(new PropertyValueFactory<>("bookingPrice"));
        discount_given.setCellValueFactory(new PropertyValueFactory<>("discountGiven"));
        items_returned.setCellValueFactory(new PropertyValueFactory<>("itemsReturned"));
        return_price.setCellValueFactory(new PropertyValueFactory<>("returnPrice"));
        cash_collection.setCellValueFactory(new PropertyValueFactory<>("cashCollection"));
        cash_return.setCellValueFactory(new PropertyValueFactory<>("cashReturn"));
        cash_waive_off.setCellValueFactory(new PropertyValueFactory<>("cashWaiveOff"));
        cash_pending.setCellValueFactory(new PropertyValueFactory<>("cashPending"));
        dealer_status.setCellValueFactory(new PropertyValueFactory<>("dealerStatus"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_dealergeneralreport.setItems(parseUserList());
        lbl_booked.setText("Booked\n"+String.format("%,.0f", totalBooked));
        lbl_delivered.setText("Delivered\n"+String.format("%,.0f", totalDelivered));
        lbl_returned.setText("Returned\n"+String.format("%,.0f", totalReturned));
        lbl_booking_price.setText("Booking Price\n"+"Rs."+String.format("%,.0f", bookingPrice)+"/-");
        lbl_discount_given.setText("Discount Given\n"+"Rs."+String.format("%,.0f", discountGiven)+"/-");
        lbl_items_returned.setText("Items Returned\n"+String.format("%,.0f", itemsReturned));
        lbl_return_price.setText("Return Price\n"+"Rs."+String.format("%,.0f", returnPrice)+"/-");
        lbl_cash_collection.setText("Cash Collection\n"+"Rs."+String.format("%,.0f", cashCollection)+"/-");
        lbl_cash_return.setText("Cash Return\n"+"Rs."+String.format("%,.0f", cashReturn)+"/-");
        lbl_cash_waiveoff.setText("Cash Waived Off\n"+"Rs."+String.format("%,.0f", cashWaiveOff)+"/-");
        lbl_cash_pending.setText("Cash Pending\n"+"Rs."+String.format("%,.0f", cashPending)+"/-");
        summary = "";
    }

    private ObservableList<DealerGeneralReportInfo> parseUserList() {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        DealerGeneralReportInfo objDealerGeneralReportInfo = new DealerGeneralReportInfo();
        dealersReportDetail = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> dealerGeneralReportData = null;
        if(filter)
        {
            dealerGeneralReportData  = objDealerGeneralReportInfo.getDealersReportSearch(objStmt1, objStmt2, objStmt3, objCon, txtDealerId, txtDealerName, txtDealerContact, txtDealerType, txtDistrictId, txtCityId, txtAreaId, txtFromDate, txtToDate);
            txt_dealer_id.setText(txtDealerId);
            txt_dealer_name.setText(txtDealerName);
            txt_dealer_contact.setText(txtDealerContact);
            txt_district_name.setValue(txtDistrictName);
            int index = txt_district_name.getSelectionModel().getSelectedIndex();
            if(index >=1)
            {
                txtDistrictId = districtsData.get(index-1).get(0);
                ArrayList<ArrayList<String>> chosenCityDetail = GlobalVariables.getParentsChildren(citiesData, txtDistrictId);
                chosenCityIds = GlobalVariables.getArrayColumn(chosenCityDetail, 0);
                chosenCityIds.add(0, "All");
                ArrayList<String> chosenCityNames = GlobalVariables.getArrayColumn(chosenCityDetail, 1);
                if(chosenCityNames.size() > 0)
                {
                    chosenCityNames.add(0, "All");
                    filteredItems = GlobalVariables.setSearchComboBox(chosenCityNames, txt_city_name);
                    txt_city_name.setItems(filteredItems);
                    txt_city_name.setValue("All");
                }
                else
                {
                    chosenCityNames.add(0, "-");
                    filteredItems = GlobalVariables.setSearchComboBox(chosenCityNames, txt_city_name);
                    txt_city_name.setItems(filteredItems);
                }
                txtCityId = "All";
            }
            else
            {
                chosenCityIds = new ArrayList<>();
                txtDistrictId = "All";
                txtCityId = "All";
                filteredItems = GlobalVariables.setSearchComboBox(cityNameList, txt_city_name);
                txt_city_name.setItems(filteredItems);
            }
            txt_city_name.setValue(txtCityName);
            index = txt_city_name.getSelectionModel().getSelectedIndex();
            if(index >=1)
            {
                if(chosenCityIds.size() > 0)
                {
                    txtCityId = chosenCityIds.get(index);
                }
                else
                {
                    txtCityId = citiesData.get(index-1).get(0);
                }
                ArrayList<ArrayList<String>> chosenAreasDetail = GlobalVariables.getParentsChildren(areasData, txtCityId);
                chosenAreaIds = GlobalVariables.getArrayColumn(chosenAreasDetail, 0);
                chosenAreaIds.add(0, "All");
                ArrayList<String> chosenAreaNames = GlobalVariables.getArrayColumn(chosenAreasDetail, 1);
                if(chosenAreaNames.size() > 0)
                {
                    chosenAreaNames.add(0, "All");
                    filteredItems = GlobalVariables.setSearchComboBox(chosenAreaNames, txt_area_name);
                    txt_area_name.setItems(filteredItems);
                    txt_area_name.setValue("All");
                }
                else
                {
                    chosenAreaNames.add(0, "-");
                    filteredItems = GlobalVariables.setSearchComboBox(chosenAreaNames, txt_area_name);
                    txt_area_name.setItems(filteredItems);
                }
                txtAreaId = "All";
            }
            else
            {
                chosenAreaIds = new ArrayList<>();
                txtCityId = "All";
                txtAreaId = "All";
                filteredItems = GlobalVariables.setSearchComboBox(areaNameList, txt_area_name);
                txt_area_name.setItems(filteredItems);
            }
            txt_area_name.setValue(txtAreaName);
            index = txt_area_name.getSelectionModel().getSelectedIndex();
            if(index >= 1)
            {
                if(chosenAreaIds.size() > 0)
                {
                    txtAreaId = chosenAreaIds.get(index);
                }
                else
                {
                    txtAreaId = areasData.get(index-1).get(0);
                }

            }
            else
            {
                txtAreaId = "All";
            }
            txt_dealer_type.setValue(txtDealerType);
            if (txtFromDate != null && !txtFromDate.equals("")) {
                txt_from_date.setValue(GlobalVariables.LOCAL_DATE(txtFromDate));
            }
            if (txtToDate != null && !txtToDate.equals("")) {
                txt_to_date.setValue(GlobalVariables.LOCAL_DATE(txtToDate));
                cash_pending.setText("Cash Pending\non\n"+txtToDate);
            }
            else
            {
                txtToDate = GlobalVariables.getStDate();
                cash_pending.setText("Cash Pending\non\n"+txtToDate);
            }
            filter = false;
            filter_pane.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
        }
        else
        {
            txtToDate = GlobalVariables.getStDate();
            cash_pending.setText("Cash Pending\non\n"+txtToDate);
            dealerGeneralReportData = objDealerGeneralReportInfo.getDealersReportInfo(objStmt1, objStmt2, objStmt3, objCon, GlobalVariables.getStDate(), GlobalVariables.getStDate());
        }
        for (int i = 0; i < dealerGeneralReportData.size(); i++)
        {
            totalBooked += (dealerGeneralReportData.get(i).get(5) != null) ? Float.parseFloat(dealerGeneralReportData.get(i).get(5)) : 0;
            totalDelivered += (dealerGeneralReportData.get(i).get(6) != null) ? Float.parseFloat(dealerGeneralReportData.get(i).get(6)) : 0;
            totalReturned += (dealerGeneralReportData.get(i).get(7) != null) ? Float.parseFloat(dealerGeneralReportData.get(i).get(7)) : 0;
            bookingPrice += (dealerGeneralReportData.get(i).get(8) != null) ? Float.parseFloat(dealerGeneralReportData.get(i).get(8)) : 0;
            discountGiven += (dealerGeneralReportData.get(i).get(9) != null) ? Float.parseFloat(dealerGeneralReportData.get(i).get(9)) : 0;
            itemsReturned += (dealerGeneralReportData.get(i).get(10) != null) ? Float.parseFloat(dealerGeneralReportData.get(i).get(10)) : 0;
            returnPrice += (dealerGeneralReportData.get(i).get(11) != null) ? Float.parseFloat(dealerGeneralReportData.get(i).get(11)) : 0;
            cashCollection += (dealerGeneralReportData.get(i).get(12) != null) ? Float.parseFloat(dealerGeneralReportData.get(i).get(12)) : 0;
            cashReturn += (dealerGeneralReportData.get(i).get(13) != null) ? Float.parseFloat(dealerGeneralReportData.get(i).get(13)) : 0;
            cashWaiveOff += (dealerGeneralReportData.get(i).get(14) != null) ? Float.parseFloat(dealerGeneralReportData.get(i).get(14)) : 0;
            cashPending += (dealerGeneralReportData.get(i).get(15) != null) ? Float.parseFloat(dealerGeneralReportData.get(i).get(15)) : 0;
            dealersReportDetail.add(new DealerGeneralReportInfo(String.valueOf(i+1), dealerGeneralReportData.get(i).get(0), ((dealerGeneralReportData.get(i).get(1) == null) ? "N/A" : dealerGeneralReportData.get(i).get(1)), dealerGeneralReportData.get(i).get(2), ((dealerGeneralReportData.get(i).get(3) == null) ? "N/A" : dealerGeneralReportData.get(i).get(3)), ((dealerGeneralReportData.get(i).get(4) == null) ? "0" : dealerGeneralReportData.get(i).get(4)), ((dealerGeneralReportData.get(i).get(5) == null) ? "0" : String.format("%,.0f", Float.parseFloat(dealerGeneralReportData.get(i).get(5)))), ((dealerGeneralReportData.get(i).get(6) == null) ? "0" : String.format("%,.0f", Float.parseFloat(dealerGeneralReportData.get(i).get(6)))), ((dealerGeneralReportData.get(i).get(7) == null) ? "0" : String.format("%,.0f", Float.parseFloat(dealerGeneralReportData.get(i).get(7)))), ((dealerGeneralReportData.get(i).get(8) == null) ? "0" : String.format("%,.2f", Float.parseFloat(dealerGeneralReportData.get(i).get(8)))), ((dealerGeneralReportData.get(i).get(9) == null) ? "0" : String.format("%,.2f", Float.parseFloat(dealerGeneralReportData.get(i).get(9)))), ((dealerGeneralReportData.get(i).get(10) == null) ? "0" : String.format("%,.0f", Float.parseFloat(dealerGeneralReportData.get(i).get(10)))), ((dealerGeneralReportData.get(i).get(11) == null) ? "0" : String.format("%,.2f", Float.parseFloat(dealerGeneralReportData.get(i).get(11)))), ((dealerGeneralReportData.get(i).get(12) == null) ? "0" : String.format("%,.2f", Float.parseFloat(dealerGeneralReportData.get(i).get(12)))), ((dealerGeneralReportData.get(i).get(13) == null) ? "0" : String.format("%,.2f", Float.parseFloat(dealerGeneralReportData.get(i).get(13)))), ((dealerGeneralReportData.get(i).get(14) == null) ? "0" : String.format("%,.2f", Float.parseFloat(dealerGeneralReportData.get(i).get(14)))), ((dealerGeneralReportData.get(i).get(15) == null) ? "0" : String.format("%,.2f", Float.parseFloat(dealerGeneralReportData.get(i).get(15)))), ((dealerGeneralReportData.get(i).get(16) == null) ? "0" : dealerGeneralReportData.get(i).get(16))));
            summaryData.add(new DealerGeneralReportInfo(String.valueOf(i+1), dealerGeneralReportData.get(i).get(0), ((dealerGeneralReportData.get(i).get(1) == null) ? "N/A" : dealerGeneralReportData.get(i).get(1)), dealerGeneralReportData.get(i).get(2), ((dealerGeneralReportData.get(i).get(3) == null) ? "N/A" : dealerGeneralReportData.get(i).get(3)), ((dealerGeneralReportData.get(i).get(4) == null) ? "0" : dealerGeneralReportData.get(i).get(4)), ((dealerGeneralReportData.get(i).get(5) == null) ? "0" : String.format("%,.0f", Float.parseFloat(dealerGeneralReportData.get(i).get(5)))), ((dealerGeneralReportData.get(i).get(6) == null) ? "0" : String.format("%,.0f", Float.parseFloat(dealerGeneralReportData.get(i).get(6)))), ((dealerGeneralReportData.get(i).get(7) == null) ? "0" : String.format("%,.0f", Float.parseFloat(dealerGeneralReportData.get(i).get(7)))), ((dealerGeneralReportData.get(i).get(8) == null) ? "0" : String.format("%,.2f", Float.parseFloat(dealerGeneralReportData.get(i).get(8)))), ((dealerGeneralReportData.get(i).get(9) == null) ? "0" : String.format("%,.2f", Float.parseFloat(dealerGeneralReportData.get(i).get(9)))), ((dealerGeneralReportData.get(i).get(10) == null) ? "0" : String.format("%,.0f", Float.parseFloat(dealerGeneralReportData.get(i).get(10)))), ((dealerGeneralReportData.get(i).get(11) == null) ? "0" : String.format("%,.2f", Float.parseFloat(dealerGeneralReportData.get(i).get(11)))), ((dealerGeneralReportData.get(i).get(12) == null) ? "0" : String.format("%,.2f", Float.parseFloat(dealerGeneralReportData.get(i).get(12)))), ((dealerGeneralReportData.get(i).get(13) == null) ? "0" : String.format("%,.2f", Float.parseFloat(dealerGeneralReportData.get(i).get(13)))), ((dealerGeneralReportData.get(i).get(14) == null) ? "0" : String.format("%,.2f", Float.parseFloat(dealerGeneralReportData.get(i).get(14)))), ((dealerGeneralReportData.get(i).get(15) == null) ? "0" : String.format("%,.2f", Float.parseFloat(dealerGeneralReportData.get(i).get(15)))), ((dealerGeneralReportData.get(i).get(16) == null) ? "0" : dealerGeneralReportData.get(i).get(16))));
        }
        return dealersReportDetail;
    }

    public void generateSummary(ArrayList<DealerGeneralReportInfo> summaryList) throws JRException {
//        InputStream input = null;
//        try {
//            input = new FileInputStream(new File("C:/wamp64/www/GitKraken/distribution/experts-machines/JavaFX/Younas Traders/src/reports/DealerGeneralReport.jrxml"));
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
        InputStream objIO = DealerGeneralReport.class.getResourceAsStream("/reports/DealerGeneralReport.jrxml");
        JasperReport objReport = JasperCompileManager.compileReport(objIO);
        JasperPrint objPrint = JasperFillManager.fillReport(objReport, null, new JRBeanCollectionDataSource(summaryList));
        JasperViewer.viewReport(objPrint, false);
    }

    @FXML
    void printData(ActionEvent event) {
        try {
            generateSummary(summaryData);
        } catch (JRException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void follow(MouseEvent event) {

    }

    @FXML
    void btnReset(ActionEvent event) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/dealer_general_report.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void btnSearch(ActionEvent event) {
        txtDealerId = txt_dealer_id.getText();
        txtDealerName = txt_dealer_name.getText();
        txtDealerContact = txt_dealer_contact.getText();
        txtDistrictName = txt_district_name.getValue();
        txtCityName = txt_city_name.getValue();
        txtAreaName = txt_area_name.getValue();
//        System.out.println("In Search: Area Id: "+txtAreaId);
        txtDealerType = txt_dealer_type.getValue();
        if(txtDealerType == null)
        {
            txtDealerType = "All";
        }
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date selectedDate = null;
        if(txt_from_date.getValue() != null)
        {
            txtFromDate = txt_from_date.getValue().toString();
            try {
                selectedDate = sdf.parse(txtFromDate);
                txtFromDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        else
        {
            txtFromDate = "";
        }

        if(txt_to_date.getValue() != null)
        {
            txtToDate = txt_to_date.getValue().toString();
            try {
                selectedDate = sdf.parse(txtToDate);
                txtToDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        else
        {
            txtToDate = "";
        }
        filter = true;

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/dealer_general_report.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void districtChange(ActionEvent event) {
        int index = txt_district_name.getSelectionModel().getSelectedIndex();
        if(index >=1)
        {
            txtDistrictId = districtsData.get(index-1).get(0);
            ArrayList<ArrayList<String>> chosenCityDetail = GlobalVariables.getParentsChildren(citiesData, txtDistrictId);
            chosenCityIds = GlobalVariables.getArrayColumn(chosenCityDetail, 0);
            chosenCityIds.add(0, "All");
            ArrayList<String> chosenCityNames = GlobalVariables.getArrayColumn(chosenCityDetail, 1);
            if(chosenCityNames.size() > 0)
            {
                chosenCityNames.add(0, "All");
                filteredItems = GlobalVariables.setSearchComboBox(chosenCityNames, txt_city_name);
                txt_city_name.setItems(filteredItems);
//                txt_city_name.setValue("All");
            }
            else
            {
                chosenCityNames.add(0, "-");
                filteredItems = GlobalVariables.setSearchComboBox(chosenCityNames, txt_city_name);
                txt_city_name.setItems(filteredItems);
                txtCityId = "All";
            }
        }
        else
        {
            chosenCityIds = new ArrayList<>();
            txtDistrictId = "All";
            txtCityId = "All";
            filteredItems = GlobalVariables.setSearchComboBox(cityNameList, txt_city_name);
            txt_city_name.setItems(filteredItems);
        }
    }

    @FXML
    void cityChange(ActionEvent event) {
        int index = txt_city_name.getSelectionModel().getSelectedIndex();
//        System.out.println(index+" "+txt_city_name.getValue());
        if(index >=1)
        {
            if(chosenCityIds.size() > 0)
            {
                txtCityId = chosenCityIds.get(index);
            }
            else
            {
                txtCityId = citiesData.get(index-1).get(0);
            }
            ArrayList<ArrayList<String>> chosenAreasDetail = GlobalVariables.getParentsChildren(areasData, txtCityId);
            chosenAreaIds = GlobalVariables.getArrayColumn(chosenAreasDetail, 0);
            chosenAreaIds.add(0,"All");
            ArrayList<String> chosenAreaNames = GlobalVariables.getArrayColumn(chosenAreasDetail, 1);
//            System.out.println("Chosen Area Names: "+chosenAreaNames);
            if(chosenAreaNames.size() > 0)
            {
                chosenAreaNames.add(0, "All");
//                txt_area_name.getItems().removeAll();
                filteredItems = GlobalVariables.setSearchComboBox(chosenAreaNames, txt_area_name);
                txt_area_name.setItems(filteredItems);
//                txt_area_name.setValue("All");
            }
            else
            {
                chosenAreaNames.add(0, "-");
                filteredItems = GlobalVariables.setSearchComboBox(chosenAreaNames, txt_area_name);
                txt_area_name.setItems(filteredItems);
                txtAreaId = "All";
            }
        }
        else
        {
            chosenAreaIds = new ArrayList<>();
            txtCityId = "All";
            txtAreaId = "All";
            filteredItems = GlobalVariables.setSearchComboBox(areaNameList, txt_area_name);
            txt_area_name.setItems(filteredItems);
        }
    }

    @FXML
    void areaChange(ActionEvent event) {
        int index = txt_area_name.getSelectionModel().getSelectedIndex();
        if(index >= 1)
        {
//            System.out.println("Index: "+index);
            if(chosenAreaIds.size() > 0)
            {
                txtAreaId = chosenAreaIds.get(index);
//                System.out.println("Choosen Area Ids: "+chosenAreaIds);
            }
            else
            {
                txtAreaId = areasData.get(index-1).get(0);
            }

        }
        else
        {
            txtAreaId = "All";
        }
//        System.out.println("Area Id & Name: "+txtAreaId+" - "+txt_area_name.getValue());
    }

//    @FXML
//    void addCash(ActionEvent event) throws IOException {
//        Parent root = FXMLLoader.load(getClass().getResource("/view/add_dealer_finance.fxml"));
//        GlobalVariables.baseScene.setRoot(root);
//        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
//    }

    @FXML
    void showCollection(MouseEvent event) {
        summary = "Collection";

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/dealer_finance.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void showDiscount(MouseEvent event) {
        summary = "Discount";

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/dealer_finance.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void showPending(MouseEvent event) {
        summary = "Pending";

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/dealer_finance.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void showReturn(MouseEvent event) {
        summary = "Return";

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/dealer_finance.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void showBooked(MouseEvent event) {

    }

    @FXML
    void showBookingPrice(MouseEvent event) {

    }

    @FXML
    void showDelivered(MouseEvent event) {

    }

    @FXML
    void showDiscountGiven(MouseEvent event) {

    }

    @FXML
    void showItemsReturned(MouseEvent event) {

    }

    @FXML
    void showReturnPrice(MouseEvent event) {

    }

    @FXML
    void showReturned(MouseEvent event) {

    }

    @FXML
    void showWaiveOff(MouseEvent event) {

    }

}
