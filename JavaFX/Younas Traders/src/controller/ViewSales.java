package controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXDrawer;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.util.StringConverter;
import model.*;
import model.ViewSalesInfo;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.view.JasperViewer;

import java.io.*;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static net.sf.jasperreports.engine.fill.JRFillSubreport.loadReport;

public class ViewSales implements Initializable {

    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private AnchorPane filter_pane;

    @FXML
    private JFXDatePicker txt_from_date;

    @FXML
    private JFXDatePicker txt_to_date;

    @FXML
    private JFXComboBox<String> txt_day;

    @FXML
    private JFXComboBox<String> txt_invoice_status;

    @FXML
    private HBox summary_hbox1;

    @FXML
    private Label lbl_invoices;

    @FXML
    private Label lbl_pending_invoices;

    @FXML
    private Label lbl_ordered_items;

    @FXML
    private Label lbl_submitted_items;

    @FXML
    private Label lbl_missed_items;

    @FXML
    private HBox summary_hbox2;

    @FXML
    private Label lbl_returned_items;

    @FXML
    private Label lbl_missed_price;

    @FXML
    private Label lbl_return_price;

    @FXML
    private Label lbl_invoice_price;

    @FXML
    private Label lbl_discount_given;

    @FXML
    private TableView<ViewSalesInfo> table_viewsaleslog;

    @FXML
    private TableColumn<ViewSalesInfo, String> sr_no;

    @FXML
    private TableColumn<ViewSalesInfo, String> booking_date;

    @FXML
    private TableColumn<ViewSalesInfo, String> booking_day;

    @FXML
    private TableColumn<ViewSalesInfo, String> invoice_no;

    @FXML
    private TableColumn<ViewSalesInfo, String> dealer_name;

    @FXML
    private TableColumn<ViewSalesInfo, String> dealer_contact;

    @FXML
    private TableColumn<ViewSalesInfo, String> ordered_items;

    @FXML
    private TableColumn<ViewSalesInfo, String> submitted_items;

    @FXML
    private TableColumn<ViewSalesInfo, String> missed_items;

    @FXML
    private TableColumn<ViewSalesInfo, String> returned_items;

    @FXML
    private TableColumn<ViewSalesInfo, String> missed_price;

    @FXML
    private TableColumn<ViewSalesInfo, String> return_price;

    @FXML
    private TableColumn<ViewSalesInfo, String> invoice_price;

    @FXML
    private TableColumn<ViewSalesInfo, String> discount_given;

    @FXML
    private TableColumn<ViewSalesInfo, String> booking_user;

    @FXML
    private TableColumn<ViewSalesInfo, String> invoice_status;

    @FXML
    private TableColumn<ViewSalesInfo, String> operations;

    @FXML
    private StackPane stackPane;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    @FXML
    private BorderPane brdrpane_navfinances;

    @FXML
    private BorderPane menu_bar;

    private ObservableList<ViewSalesInfo> saleDetails;
    private ViewSalesInfo objViewSalesInfo;
    ArrayList<ArrayList<String>> saleData;
    public static ArrayList<ArrayList<String>> saleDetailData;
    public ArrayList<InvoiceInfo> invoiceData;
    private static ViewSaleDetailInfo objViewSaleDetailInfo;
    private SaleInvoicePrintInfo objSaleInvoicePrintInfo;
    ArrayList<String> invoicePrintData;

    float totalInvoices = 0;
    float orderedItems = 0;
    float submittedItems = 0;
    float missedItems = 0;
    float returnedItems = 0;
    float missedPrice = 0;
    float returnPrice = 0;
    float invoicePrice = 0;
    float discountGiven = 0;
    float pendingInvoices = 0;

    public static String txtFromDate = "";
    public static String txtToDate = "";
    public static String txtDay = "All";
    public static String txtInvoiceStatus = "All";
    public static boolean filter;

    MysqlCon objMysqlCon1;
    MysqlCon objMysqlCon2;
    MysqlCon objMysqlCon3;
    Statement objStmt1;
    Statement objStmt2;
    Statement objStmt3;
    Connection objCon;

    public static Button btnView;
    SimpleDateFormat fmt = new SimpleDateFormat("dd/MMM/yyyy");

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnSale.setStyle("-fx-background-color: #47ab1e");
        objMysqlCon1 = new MysqlCon();
        objMysqlCon2 = new MysqlCon();
        objMysqlCon3 = new MysqlCon();
        objStmt1 = objMysqlCon1.stmt;
        objStmt2 = objMysqlCon2.stmt;
        objStmt3 = objMysqlCon3.stmt;
        objCon = objMysqlCon1.con;
        objViewSalesInfo = new ViewSalesInfo();
        objSaleInvoicePrintInfo = new SaleInvoicePrintInfo();
        invoicePrintData = objSaleInvoicePrintInfo.getSaleInvoicePrintInfo(objStmt1, objCon);
        ViewSalesInfo.stackPane = stackPane;

        saleData = new ArrayList<>();
        invoiceData = new ArrayList<>();

        txt_from_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        txt_to_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        table_viewsaleslog.setRowFactory(tv -> new TableRow<ViewSalesInfo>() {
            @Override
            protected void updateItem(ViewSalesInfo item, boolean empty) {
                super.updateItem(item, empty);
                if (item == null || item.getInvoiceNo() == null)
                    setStyle("");
                else if (ViewSalesInfo.newIds.size() > 0 && ViewSalesInfo.newIds.contains(item.getInvoiceNo()))
                    setStyle("-fx-background-color: #47ab1e;");
                else
                    setStyle("");
            }
        });

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        booking_date.setCellValueFactory(new PropertyValueFactory<>("bookingDate"));
        booking_day.setCellValueFactory(new PropertyValueFactory<>("bookingDay"));
        invoice_no.setCellValueFactory(new PropertyValueFactory<>("invoiceNo"));
        dealer_name.setCellValueFactory(new PropertyValueFactory<>("dealerName"));
        dealer_contact.setCellValueFactory(new PropertyValueFactory<>("dealerContact"));
        ordered_items.setCellValueFactory(new PropertyValueFactory<>("orderedItems"));
        submitted_items.setCellValueFactory(new PropertyValueFactory<>("submittedItems"));
        missed_items.setCellValueFactory(new PropertyValueFactory<>("missedItems"));
        returned_items.setCellValueFactory(new PropertyValueFactory<>("returnedItems"));
        missed_price.setCellValueFactory(new PropertyValueFactory<>("missedPrice"));
        return_price.setCellValueFactory(new PropertyValueFactory<>("returnPrice"));
        invoice_price.setCellValueFactory(new PropertyValueFactory<>("invoicePrice"));
        discount_given.setCellValueFactory(new PropertyValueFactory<>("discountGiven"));
        booking_user.setCellValueFactory(new PropertyValueFactory<>("bookingUser"));
        invoice_status.setCellValueFactory(new PropertyValueFactory<>("invoiceStatus"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_viewsaleslog.setItems(parseUserList());
        lbl_invoices.setText("Total Invoices\n"+String.format("%,.0f", totalInvoices));
        lbl_pending_invoices.setText("Pending Invoices\n"+String.format("%,.0f", pendingInvoices));
        lbl_ordered_items.setText("Ordered Items\n"+String.format("%,.0f", orderedItems));
        lbl_submitted_items.setText("Submitted Items\n"+String.format("%,.0f", submittedItems));
        lbl_missed_items.setText("Missed Items\n"+String.format("%,.0f", missedItems));
        lbl_returned_items.setText("Returned Items\n"+String.format("%,.0f", returnedItems));
        lbl_missed_price.setText("Missed Price\n"+"Rs."+String.format("%,.0f", missedPrice)+"/-");
        lbl_return_price.setText("Return Price\n"+"Rs."+String.format("%,.0f", returnPrice)+"/-");
        lbl_invoice_price.setText("Invoice Price\n"+"Rs."+String.format("%,.0f", invoicePrice)+"/-");
        lbl_discount_given.setText("Discount Given\n"+"Rs."+String.format("%,.0f", discountGiven)+"/-");
    }

    private ObservableList<ViewSalesInfo> parseUserList() {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        saleDetails = FXCollections.observableArrayList();

        if (filter) {
            saleData = objViewSalesInfo.getSalesInfoSearch(objStmt1, objCon, txtFromDate, txtToDate, txtDay, txtInvoiceStatus);
            if (txtFromDate != null && !txtFromDate.equals("")) {
                txt_from_date.setValue(GlobalVariables.LOCAL_DATE(txtFromDate));
            }
            if (txtToDate != null && !txtToDate.equals("")) {
                txt_to_date.setValue(GlobalVariables.LOCAL_DATE(txtToDate));
            }
            txt_day.setValue(txtDay);
            txt_invoice_status.setValue(txtInvoiceStatus);
            filter = false;
            filter_pane.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
            txtFromDate = "";
            txtToDate = "";
            txtDay = "All";
            txtInvoiceStatus = "All";
        } else {
            saleData = objViewSalesInfo.getSalesInfo(objStmt1, objCon, GlobalVariables.getStDate(), GlobalVariables.getStDate());
        }

        saleDetailData = new ArrayList<>();
        for (int i = 0; i < saleData.size(); i++)
        {
            objViewSaleDetailInfo = new ViewSaleDetailInfo(saleData.get(i).get(2));
            objViewSaleDetailInfo.getSaleDetailInfo(objStmt1, objCon);
            totalInvoices += (saleData.get(i).get(2) != null && !saleData.get(i).get(2).equals("-")) ? 1 : 0;
            if(saleData.get(i).get(14) != null && saleData.get(i).get(14).equals("Pending"))
            {
                pendingInvoices++;
            }
            orderedItems += (saleData.get(i).get(5) != null && !saleData.get(i).get(5).equals("-")) ? Float.parseFloat(saleData.get(i).get(5)) : 0;
            submittedItems += (saleData.get(i).get(6) != null && !saleData.get(i).get(6).equals("-")) ? Float.parseFloat(saleData.get(i).get(6)) : 0;
            missedItems += (saleData.get(i).get(7) != null && !saleData.get(i).get(7).equals("-")) ? Float.parseFloat(saleData.get(i).get(7)) : 0;
            returnedItems += (saleData.get(i).get(8) != null && !saleData.get(i).get(8).equals("-")) ? Float.parseFloat(saleData.get(i).get(8)) : 0;
            missedPrice += (saleData.get(i).get(9) != null && !saleData.get(i).get(9).equals("-")) ? Float.parseFloat(saleData.get(i).get(9)) : 0;
            returnPrice += (saleData.get(i).get(10) != null && !saleData.get(i).get(10).equals("-")) ? Float.parseFloat(saleData.get(i).get(10)) : 0;
            invoicePrice += (saleData.get(i).get(11) != null && !saleData.get(i).get(11).equals("-")) ? Float.parseFloat(saleData.get(i).get(11)) : 0;
            discountGiven += (saleData.get(i).get(12) != null && !saleData.get(i).get(12).equals("-")) ? Float.parseFloat(saleData.get(i).get(12)) : 0;
            saleDetails.add(new ViewSalesInfo(String.valueOf(i+1), ((saleData.get(i).get(0) == null || saleData.get(i).get(0).equals("")) ? "N/A" : saleData.get(i).get(0)), saleData.get(i).get(1), saleData.get(i).get(2), saleData.get(i).get(3), saleData.get(i).get(4), String.format("%,.0f", Float.parseFloat(saleData.get(i).get(5))), String.format("%,.0f", Float.parseFloat(saleData.get(i).get(6))), String.format("%,.0f", Float.parseFloat(saleData.get(i).get(7))), String.format("%,.0f", Float.parseFloat(saleData.get(i).get(8))), String.format("%,.2f", Float.parseFloat(saleData.get(i).get(9))), String.format("%,.2f", Float.parseFloat(saleData.get(i).get(10))), String.format("%,.2f", Float.parseFloat(saleData.get(i).get(11))), String.format("%,.2f", Float.parseFloat(saleData.get(i).get(12))), saleData.get(i).get(13), saleData.get(i).get(14), saleData.get(i).get(15)));
        }
        return saleDetails;
    }

    @FXML
    void btnReset(ActionEvent event) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/view_sales.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void btnSearch(ActionEvent event) {
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date selectedDate = null;
        if(txt_from_date.getValue() != null)
        {
            txtFromDate = txt_from_date.getValue().toString();
            try {
                selectedDate = sdf.parse(txtFromDate);
                txtFromDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if(txt_to_date.getValue() != null)
        {
            txtToDate = txt_to_date.getValue().toString();
            try {
                selectedDate = sdf.parse(txtToDate);
                txtToDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        txtDay = txt_day.getValue();
        if(txtDay == null)
        {
            txtDay = "All";
        }
        txtInvoiceStatus = txt_invoice_status.getValue();
        if(txtInvoiceStatus == null)
        {
            txtInvoiceStatus = "All";
        }
        filter = true;

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/view_sales.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void btnPrintAllInvoices(ActionEvent event) throws JRException {
        invoiceData = new ArrayList<>();
        int j = 0;
        float advtaxAmount;
        float totalNetValue;
        for (int i = 0; i < saleData.size(); i++)
        {
            String orderId = saleData.get(i).get(2);
            String dealerId = saleData.get(i).get(16);
            String dealerName = saleData.get(i).get(3);
            String dealerLic = saleData.get(i).get(17);
            String dealerAddress = saleData.get(i).get(18);
            String dealerContact = saleData.get(i).get(4);
            String dealerNTN = saleData.get(i).get(19);
            String dealerCnic = saleData.get(i).get(20);
            String dealerSalesTaxNo = " - ";
            String invoiceDate = saleData.get(i).get(0);
            String deliveredUser = saleData.get(i).get(12);
            String totalQty = "0";
            String orgPrice = saleData.get(i).get(21) == null ? "0" : String.format("%,.2f", Float.parseFloat(saleData.get(i).get(21)));
            String salestax = "1.00";
            if(dealerNTN != null && !dealerNTN.equals(""))
            {
                salestax = "0.50";
            }
            else
            {
                salestax = "1.00";
            }
            String totalBonus = "0";
            String totalDiscount = saleData.get(i).get(12);
            String finalPrice = saleData.get(i).get(11) == null ? "0" : String.format("%,.2f", Float.parseFloat(saleData.get(i).get(11)));
            InvoicedItemsInfo objItemsInfos;
            List<InvoicedItemsInfo> itemsList = null;
            itemsList = new ArrayList<>();
            int qty = 0;
            int bonus = 0;
            int num = 1;
            while(i<saleData.size() && j<saleDetailData.size() && saleDetailData.get(j).get(11).equals(saleData.get(i).get(2)))
            {
                if(Integer.parseInt(saleDetailData.get(j).get(6)) > 0)
                {
                    objItemsInfos = new InvoicedItemsInfo(String.valueOf(num), saleDetailData.get(j).get(0), saleDetailData.get(j).get(1), saleDetailData.get(j).get(13), saleDetailData.get(j).get(3), !saleDetailData.get(j).get(12).equals("N/A") ? String.format("%,.2f", Float.parseFloat(saleDetailData.get(j).get(12))) : "N/A", String.format("%,.0f", Float.parseFloat(saleDetailData.get(j).get(6))), String.format("%,.0f", Float.parseFloat(saleDetailData.get(j).get(7))), String.format("%,.2f", Float.parseFloat(saleDetailData.get(j).get(8))), salestax, String.format("%,.2f", Float.parseFloat(saleDetailData.get(j).get(10))), String.format("%,.2f", Float.parseFloat(saleDetailData.get(j).get(9))));
                    totalQty = String.valueOf(qty);
                    totalBonus = String.valueOf(bonus);
                    objItemsInfos.setTotalQty(String.format("%,.0f", Float.parseFloat(totalQty)));
                    objItemsInfos.setTotalBonus(String.format("%,.0f", Float.parseFloat(totalBonus)));
                    objItemsInfos.setOrgPrice(orgPrice);
                    objItemsInfos.setTotalSalesTax(salestax);
                    objItemsInfos.setTotalDiscount(totalDiscount);
                    objItemsInfos.setFinalPrice(finalPrice);
                    objItemsInfos.setAdvTax(salestax);
                    advtaxAmount = Float.parseFloat(saleData.get(i).get(11)) / 100 * Float.parseFloat(salestax);
                    totalNetValue = Float.parseFloat(saleData.get(i).get(11)) + advtaxAmount;
                    objItemsInfos.setAdvTaxAmount(String.format("%,.2f", advtaxAmount));
                    objItemsInfos.setTotalNetValue(String.format("%,.2f", totalNetValue));
                    objItemsInfos.setTextAmount(NumberToWords.convert(((long)totalNetValue)));
                    itemsList.add(objItemsInfos);
                    num++;
                }
                j++;
            }
            invoiceData.add(new InvoiceInfo(orderId, dealerId, dealerName, dealerLic, dealerAddress, dealerContact, dealerNTN, dealerCnic, dealerSalesTaxNo, invoiceDate, deliveredUser, itemsList, totalQty, orgPrice, salestax, totalBonus, totalDiscount, finalPrice, invoicePrintData.get(11)));
        }
        InputStream objIO = ViewSales.class.getResourceAsStream("/reports/Invoice.jrxml");
        JasperReport objReport = JasperCompileManager.compileReport(objIO);
        JasperPrint objPrint = JasperFillManager.fillReport(objReport, null, new JRBeanCollectionDataSource(invoiceData));
        JasperViewer.viewReport(objPrint, false);

    }

    @FXML
    void follow(MouseEvent event) {

    }

    @FXML
    void showInvoicePrice(MouseEvent event) {

    }

    @FXML
    void showWPendingInvoices(MouseEvent event) {

    }

    @FXML
    void showInvoices(MouseEvent event) {

    }

    @FXML
    void showMissedItems(MouseEvent event) {

    }

    @FXML
    void showMissedPrice(MouseEvent event) {

    }

    @FXML
    void showOrderedItems(MouseEvent event) {

    }

    @FXML
    void showReturnedItems(MouseEvent event) {

    }

    @FXML
    void showSubmittedItems(MouseEvent event) {

    }

    @FXML
    void showWDiscountGiven(MouseEvent event) {

    }
}
