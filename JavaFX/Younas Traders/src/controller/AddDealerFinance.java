package controller;

import com.jfoenix.controls.*;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.util.StringConverter;
import model.AddDealerFinanceInfo;
import model.GlobalVariables;
import model.MysqlCon;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

public class AddDealerFinance implements Initializable {

    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private JFXComboBox<String> txt_select_dealer;

    @FXML
    private ImageView img_user;

    @FXML
    private JFXTextField txt_name;

    @FXML
    private JFXTextField txt_contact;

    @FXML
    private JFXTextField txt_address;

    @FXML
    private JFXTextField txt_cnic;

    @FXML
    private JFXTextField txt_pending_amoount;

    @FXML
    private JFXTextField txt_amount;

    @FXML
    private JFXComboBox<String> txt_type;

    @FXML
    private JFXDatePicker txt_date;

    @FXML
    private Button txt_today_date;

    @FXML
    private Button btn_edit_update;

    @FXML
    private Button btn_edit_cancel;

    @FXML
    private Button btn_add_cash;

    @FXML
    private Button cancelBtn;

    @FXML
    private Button saveBtn;

    @FXML
    private TableView<AddDealerFinanceInfo> table_adddealercash;

    @FXML
    private TableColumn<AddDealerFinanceInfo, String> sr_no;

    @FXML
    private TableColumn<AddDealerFinanceInfo, String> dealer_name;

    @FXML
    private TableColumn<AddDealerFinanceInfo, String> dealer_contact;

    @FXML
    private TableColumn<AddDealerFinanceInfo, String> added_date;

    @FXML
    private TableColumn<AddDealerFinanceInfo, String> added_day;

    @FXML
    private TableColumn<AddDealerFinanceInfo, String> amount;

    @FXML
    private TableColumn<AddDealerFinanceInfo, String> type;

    @FXML
    private TableColumn<AddDealerFinanceInfo, String> pending;

    @FXML
    private TableColumn<AddDealerFinanceInfo, String> operations;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    @FXML
    private BorderPane brdrpane_navfinances;

    @FXML
    private BorderPane menu_bar;

    private AddDealerFinanceInfo objAddDealerFinanceInfo;
    private ArrayList<ArrayList<String>> dealersData;
    MysqlCon objMysqlCon;
    Statement objStmt;
    Connection objCon;;
    private String dealerImage;
    private String selectedDealerId;
    public static ObservableList<AddDealerFinanceInfo> dealerDetails;
    private boolean addCash;
    public static int srNo;
    public static Button btnView;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnFinance.setStyle("-fx-background-color: #47ab1e");
        objMysqlCon = new MysqlCon();
        objStmt = objMysqlCon.stmt;
        objCon = objMysqlCon.con;
        objAddDealerFinanceInfo = new AddDealerFinanceInfo();
        dealersData = objAddDealerFinanceInfo.getDealersInfo(objStmt, objCon);
        ArrayList<String> dealersNameList = getArrayColumn(dealersData, 1);
        addCash = false;
        FilteredList<String> filteredItems = setSearchComboBox(dealersNameList, txt_select_dealer);
        txt_select_dealer.setItems(filteredItems);

        txt_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }

            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        dealer_name.setCellValueFactory(new PropertyValueFactory<>("dealerName"));
        dealer_contact.setCellValueFactory(new PropertyValueFactory<>("dealerContact"));
        added_date.setCellValueFactory(new PropertyValueFactory<>("addedDate"));
        added_day.setCellValueFactory(new PropertyValueFactory<>("addedDay"));
        amount.setCellValueFactory(new PropertyValueFactory<>("amount"));
        type.setCellValueFactory(new PropertyValueFactory<>("type"));
        pending.setCellValueFactory(new PropertyValueFactory<>("pending"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_adddealercash.setItems(parseUserList());

        AddDealerFinanceInfo.table_adddealercash = table_adddealercash;
        AddDealerFinanceInfo.txtSelectDealer = txt_select_dealer;
        AddDealerFinanceInfo.txtDealerImage = img_user;
        AddDealerFinanceInfo.txtDealerName = txt_name;
        AddDealerFinanceInfo.txtDealerContact = txt_contact;
        AddDealerFinanceInfo.txtDealerAddress = txt_address;
        AddDealerFinanceInfo.txtDealerCnic = txt_cnic;
        AddDealerFinanceInfo.txtDealerPendingAmount = txt_pending_amoount;
        AddDealerFinanceInfo.txtAmount = txt_amount;
        AddDealerFinanceInfo.txtType = txt_type;
        AddDealerFinanceInfo.txtAddedDate = txt_date;
        AddDealerFinanceInfo.btnAdd = btn_add_cash;
        AddDealerFinanceInfo.btnCancel = btn_edit_cancel;
        AddDealerFinanceInfo.btnUpdate = btn_edit_update;
    }

    public static ObservableList<AddDealerFinanceInfo> parseUserList() {
        AddDealerFinanceInfo objAddDealerFinanceInfo = new AddDealerFinanceInfo();
        dealerDetails = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> dealerData = objAddDealerFinanceInfo.getAddedCash();
        for (int i = 0; i < dealerData.size(); i++)
        {
            dealerDetails.add(new AddDealerFinanceInfo(String.valueOf(i+1), ((dealerData.get(i).get(0) == null || dealerData.get(i).get(0).equals("")) ? "N/A" : dealerData.get(i).get(0)), dealerData.get(i).get(1), dealerData.get(i).get(2), dealerData.get(i).get(3), dealerData.get(i).get(4), dealerData.get(i).get(5), dealerData.get(i).get(6), dealerData.get(i).get(7)));
        }
        return dealerDetails;
    }

    @FXML
    void cancelClicked(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/dealer_finance.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void editCancel(ActionEvent event) {
        refreshControls();
        btn_add_cash.setVisible(true);
        btn_edit_cancel.setVisible(false);
        btn_edit_update.setVisible(false);
    }

    @FXML
    void editUpdate(ActionEvent event) {
        addCash = true;
        AddDealerFinanceInfo.selectedDealerArr.add(txt_select_dealer.getValue());
        AddDealerFinanceInfo.dealerIdArr.set(srNo, selectedDealerId);
        AddDealerFinanceInfo.dealerNameArr.set(srNo, txt_name.getText());
        AddDealerFinanceInfo.dealerContactArr.set(srNo, txt_contact.getText());
        AddDealerFinanceInfo.dealerAddressArr.set(srNo, txt_address.getText());
        AddDealerFinanceInfo.dealerCnicArr.set(srNo, txt_cnic.getText());
        AddDealerFinanceInfo.dealerImageArr.set(srNo, dealerImage);
        AddDealerFinanceInfo.dealerPendingAmountArr.set(srNo, txt_pending_amoount.getText());
        AddDealerFinanceInfo.amountArr.set(srNo, txt_amount.getText());
        AddDealerFinanceInfo.typeArr.set(srNo, txt_type.getValue());
        String addedDate = txt_date.getValue().toString();
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date selectedDate = null;
        try {
            selectedDate = sdf.parse(addedDate);
            addedDate = sdf2.format(selectedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        int day;
        String stDay = "";
        day = selectedDate.getDay();
        if(day == 0)
            stDay = "Sunday";
        else if(day == 1)
            stDay = "Monday";
        else if(day == 2)
            stDay = "Tuesday";
        else if(day == 3)
            stDay = "Wednesday";
        else if(day == 4)
            stDay = "Thursday";
        else if(day == 5)
            stDay = "Friday";
        else if(day == 6)
            stDay = "Saturday";
        AddDealerFinanceInfo.addedDateArr.set(srNo, addedDate);
        AddDealerFinanceInfo.addedDayArr.set(srNo, stDay);
        float pending;
        if(txt_type.getValue().equals("Return"))
        {
            pending = Float.parseFloat(txt_pending_amoount.getText()) + Float.parseFloat(txt_amount.getText());
        }
        else
        {
            pending = Float.parseFloat(txt_pending_amoount.getText()) - Float.parseFloat(txt_amount.getText());
        }

        AddDealerFinanceInfo.pendingArr.set(srNo, String.valueOf(pending));
        table_adddealercash.setItems(parseUserList());
        refreshControls();
        btn_add_cash.setVisible(true);
        btn_edit_cancel.setVisible(false);
        btn_edit_update.setVisible(false);
        addCash = false;
    }

    @FXML
    void follow(MouseEvent event) {

    }

    @FXML
    void saveClicked(ActionEvent event) throws IOException {
        try {
            objAddDealerFinanceInfo.insertDealerCash(objStmt, objCon);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_dealer_finance.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void addCash(ActionEvent event) {
        addCash = true;
        AddDealerFinanceInfo.selectedDealerArr.add(txt_select_dealer.getValue());
        AddDealerFinanceInfo.dealerIdArr.add(selectedDealerId);
        AddDealerFinanceInfo.dealerNameArr.add(txt_name.getText());
        AddDealerFinanceInfo.dealerContactArr.add(txt_contact.getText());
        AddDealerFinanceInfo.dealerAddressArr.add(txt_address.getText());
        AddDealerFinanceInfo.dealerCnicArr.add(txt_cnic.getText());
        AddDealerFinanceInfo.dealerImageArr.add(dealerImage);
        AddDealerFinanceInfo.dealerPendingAmountArr.add(txt_pending_amoount.getText());
        AddDealerFinanceInfo.amountArr.add(txt_amount.getText());
        AddDealerFinanceInfo.typeArr.add(txt_type.getValue());
        String addedDate = txt_date.getValue().toString();
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date selectedDate = null;
        try {
            selectedDate = sdf.parse(addedDate);
            addedDate = sdf2.format(selectedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        AddDealerFinanceInfo.addedDateArr.add(addedDate);
        float pending;
        int day;
        String stDay = "";
        day = selectedDate.getDay();
        if(day == 0)
            stDay = "Sunday";
        else if(day == 1)
            stDay = "Monday";
        else if(day == 2)
            stDay = "Tuesday";
        else if(day == 3)
            stDay = "Wednesday";
        else if(day == 4)
            stDay = "Thursday";
        else if(day == 5)
            stDay = "Friday";
        else if(day == 6)
            stDay = "Saturday";
        AddDealerFinanceInfo.addedDayArr.add(stDay);
        if(txt_type.getValue().equals("Return"))
        {
            pending = Float.parseFloat(txt_pending_amoount.getText()) + Float.parseFloat(txt_amount.getText());
        }
        else
        {
            pending = Float.parseFloat(txt_pending_amoount.getText()) - Float.parseFloat(txt_amount.getText());
        }

        AddDealerFinanceInfo.pendingArr.add(String.valueOf(pending));
        table_adddealercash.setItems(parseUserList());
        refreshControls();
        addCash = false;
    }

    @FXML
    void setTodayDate(ActionEvent event) {
        String todayDate = GlobalVariables.getStDate();
        txt_date.setValue(LOCAL_DATE(todayDate));
    }


    @FXML
    void showList(ActionEvent event) {
        if(!addCash)
        {
            txt_select_dealer.show();
        }
        int index = txt_select_dealer.getSelectionModel().getSelectedIndex();
        if(index >= 0)
        {
            selectedDealerId = dealersData.get(index).get(0);
            txt_name.setText(dealersData.get(index).get(2));
            txt_contact.setText(dealersData.get(index).get(3));
            txt_address.setText(dealersData.get(index).get(4));
            txt_cnic.setText(dealersData.get(index).get(5));
            if(dealersData.get(index).get(7) != null && !dealersData.get(index).get(7).equals(""))
            {
                txt_pending_amoount.setText(dealersData.get(index).get(7));
            }
            else
            {
                txt_pending_amoount.setText("0");
            }

            if(!dealersData.get(index).get(6).equals(""))
            {
                File file = new File(dealersData.get(index).get(6));
                boolean exists = file.exists();
                if(exists)
                {
                    dealerImage = dealersData.get(index).get(6);
                    Image image = new Image(file.toURI().toString());
                    img_user.setImage(image);
                }
                else
                {
                    dealerImage = "src/view/images/users/empty_user.jpg";
                    file = new File("src/view/images/users/empty_user.jpg");
                    Image image = new Image(file.toURI().toString());
                    img_user.setImage(image);
                }
            }
            else
            {
                File file = new File("src/view/images/users/empty_user.jpg");
                boolean exists = file.exists();
                if(exists)
                {
                    Image image = new Image(file.toURI().toString());
                    img_user.setImage(image);
                }
                dealerImage = "src/view/images/users/empty_user.jpg";
            }
        }
    }

    public static final LocalDate LOCAL_DATE (String dateString) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MMM/yyyy");
        LocalDate localDate = LocalDate.parse(dateString, formatter);
        return localDate;
    }

    public ArrayList<String> getArrayColumn(ArrayList<ArrayList<String>> dealersData, int columnIndex)
    {
        ArrayList<String> columnList = new ArrayList<>();
        for(ArrayList<String> row: dealersData) {
            columnList.add(row.get(columnIndex));
        }
        return columnList;
    }

    private FilteredList<String> setSearchComboBox(ArrayList<String> list, JFXComboBox<String> simpleComboBox)
    {
        // Create a list with some dummy values.
        ObservableList<String> items = FXCollections.observableArrayList(list);

        // Create a FilteredList wrapping the ObservableList.
        FilteredList<String> filteredItems = new FilteredList<String>(items, p -> true);

        // Add a listener to the textProperty of the combobox editor. The
        // listener will simply filter the list every time the input is changed
        // as long as the user hasn't selected an item in the list.
        simpleComboBox.getEditor().textProperty().addListener((obs, oldValue, newValue) -> {
            final TextField editor = simpleComboBox.getEditor();
            final String selected = simpleComboBox.getSelectionModel().getSelectedItem();

            // This needs run on the GUI thread to avoid the error described
            // here: https://bugs.openjdk.java.net/browse/JDK-8081700.
            Platform.runLater(() -> {
                // If the no item in the list is selected or the selected item
                // isn't equal to the current input, we refilter the list.
                if (selected == null || !selected.equals(editor.getText())) {
                    filteredItems.setPredicate(item -> {
                        // We return true for any items that starts with the
                        // same letters as the input. We use toUpperCase to
                        // avoid case sensitivity.
                        if (item.toUpperCase().startsWith(newValue.toUpperCase())) {
                            return true;
                        } else {
                            return false;
                        }
                    });
                }
            });
        });

        return filteredItems;
    }

    private void refreshControls()
    {
        addCash = true;
        txt_select_dealer.getSelectionModel().clearSelection();
        txt_name.clear();
        txt_contact.clear();
        txt_address.clear();
        txt_cnic.clear();
        txt_pending_amoount.clear();
        txt_amount.clear();
        txt_type.getSelectionModel().clearSelection();
        txt_date.setValue(null);
    }
}
