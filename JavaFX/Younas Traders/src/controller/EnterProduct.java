package controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.util.Duration;
import model.*;
import model.ProductInfo;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class EnterProduct implements Initializable {

    @FXML    private AnchorPane inner_anchor;
    @FXML    private JFXTextField txt_product_id;
    @FXML    private JFXComboBox<String> txt_company_name;
    @FXML    private JFXComboBox<String> txt_company_group;
    @FXML    private JFXTextField txt_product_name;
    @FXML    private JFXComboBox<String> txt_product_type;
    @FXML    private JFXTextField txt_retail_price;
    @FXML    private JFXTextField txt_trade_price;
    @FXML    private JFXTextField txt_purchase_price;
    @FXML    private JFXTextField txt_purchase_discount;
    @FXML    private Button cancelBtn;
    @FXML    private Button saveBtn;
    @FXML    private JFXTextField txt_product_packing;
    @FXML    private JFXTextField txt_carton_packing;
    @FXML    private JFXTextField txt_sales_tax;
    @FXML    private JFXTextField txt_hold_stock;
    @FXML    private JFXTextField txt_max_stock_level;
    @FXML    private JFXTextField txt_min_stock_level;
    @FXML    private TableView<ProductInfo> table_add_product;
    @FXML    private TableColumn<ProductInfo, String> sr_no;
    @FXML    private TableColumn<ProductInfo, String> product_id;
    @FXML    private TableColumn<ProductInfo, String> company_name;
    @FXML    private TableColumn<ProductInfo, String> group_name;
    @FXML    private TableColumn<ProductInfo, String> product_name;
    @FXML    private TableColumn<ProductInfo, String> product_type;
    @FXML    private TableColumn<ProductInfo, String> purchase_price;
    @FXML    private TableColumn<ProductInfo, String> retail_price;
    @FXML    private TableColumn<ProductInfo, String> operations;
    @FXML    private Button add_product;
    @FXML    private Button btn_edit_cancel;
    @FXML    private Button btn_edit_update;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private BorderPane menu_bar1;

    @FXML
    private BorderPane nav_setup;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    private ArrayList<ArrayList<String>> companiesData = new ArrayList<>();
    private ArrayList<ArrayList<String>> groupsData = new ArrayList<>();
    private ArrayList<String> chosenSubcompanyIds = new ArrayList<>();

    public static ObservableList<ProductInfo> productDetails;
    Alert alert = new Alert(Alert.AlertType.NONE);
    ProductInfo objProductInfo;
    public static int srNo;
    public static Button btnView;

    public void initialize(URL url, ResourceBundle resourceBundle) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnSetup.setStyle("-fx-background-color: #47ab1e");
        objProductInfo = new ProductInfo();
        companiesData = objProductInfo.getCompaniesData();
        groupsData = objProductInfo.getGroupsData();

        btnpane.setOnMouseEntered(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(22);
                btnpane.setMinWidth(20.0);
                btn11.setMinWidth(20.0);

            }
        });
        btnpane.setOnMouseExited(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(14);
                btnpane.setMinWidth(10.0);
                btn11.setMinWidth(10.0);
            }
        });

        inner_anchor.setLeftAnchor(inner_anchor, 280d);
        drawer.setSidePane(nav_setup);
        drawer.open();

        ArrayList<String> chosenCompanyName = getArrayColumn(companiesData, 1);
        txt_company_name.getItems().addAll(chosenCompanyName);

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        product_id.setCellValueFactory(new PropertyValueFactory<>("productId"));
        company_name.setCellValueFactory(new PropertyValueFactory<>("companyName"));
        group_name.setCellValueFactory(new PropertyValueFactory<>("groupName"));
        product_name.setCellValueFactory(new PropertyValueFactory<>("productName"));
        product_type.setCellValueFactory(new PropertyValueFactory<>("productType"));
        purchase_price.setCellValueFactory(new PropertyValueFactory<>("purchasePrice"));
        retail_price.setCellValueFactory(new PropertyValueFactory<>("retailPrice"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        ProductInfo.table_add_product = table_add_product;
        table_add_product.setItems(parseUserList());
        ProductInfo.txtProductId = txt_product_id;
        ProductInfo.txtCompanyName = txt_company_name;
        ProductInfo.txtGroupName = txt_company_group;
        ProductInfo.txtProductName = txt_product_name;
        ProductInfo.txtProductType = txt_product_type;
        ProductInfo.txtRetailPrice = txt_retail_price;
        ProductInfo.txtTradePrice = txt_trade_price;
        ProductInfo.txtPurchasePrice = txt_purchase_price;
        ProductInfo.txtPurchaseDiscount = txt_purchase_discount;
        ProductInfo.txtProductPacking = txt_product_packing;
        ProductInfo.txtCartonPacking = txt_product_packing;
        ProductInfo.txtSalesTax = txt_carton_packing;
        ProductInfo.txtHoldStock = txt_hold_stock;
        ProductInfo.txtMaxStock = txt_max_stock_level;
        ProductInfo.txtMinStock = txt_min_stock_level;
        ProductInfo.btnAdd = add_product;
        ProductInfo.btnCancel = btn_edit_cancel;
        ProductInfo.btnUpdate = btn_edit_update;
    }

    public static ObservableList<ProductInfo> parseUserList(){
        ProductInfo objProductInfo = new ProductInfo();
        productDetails = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> productData = objProductInfo.getAddedProducts();
        for (int i = 0; i < productData.size(); i++)
        {
            productDetails.add(new ProductInfo(String.valueOf(i+1), ((productData.get(i).get(0) == null || productData.get(i).get(0).equals("")) ? "N/A" : productData.get(i).get(0)), productData.get(i).get(1), ((productData.get(i).get(2) == null || productData.get(i).get(2).equals("")) ? "N/A" : productData.get(i).get(2)), productData.get(i).get(3), productData.get(i).get(4), productData.get(i).get(5), productData.get(i).get(6)));
        }
        return productDetails;
    }

    @FXML
    void addProduct(ActionEvent event) {
        String productId = txt_product_id.getText();
        String companyId = getCompanyId(companiesData, txt_company_name.getValue());
        String companyName = txt_company_name.getValue();
        String groupId = findIdInGroup(groupsData, txt_company_group.getValue(), getCompanyId(companiesData, txt_company_name.getValue()));
        String groupName = txt_company_group.getValue();
        String productName = txt_product_name.getText();
        String productType = txt_product_type.getValue();
        String productPacking = txt_product_packing.getText();
        String cartonPacking = txt_carton_packing.getText();
        String retailPrice = txt_retail_price.getText();
        String tradePrice = txt_trade_price.getText();
        String purchaseDiscount = txt_purchase_discount.getText();
        String purchasePrice = txt_purchase_price.getText();
        String salesTax = txt_sales_tax.getText();
        String holdStock = txt_hold_stock.getText();
        String maxStockLevel = txt_max_stock_level.getText();
        String minStockLevel = txt_min_stock_level.getText();
        String productStatus = "Active";

        String comp = objProductInfo.confirmNewId(productId);

        if(!comp.equals(productId) || comp.equals("")) {
            MysqlCon objMysqlCon = new MysqlCon();
            Statement objStmt = objMysqlCon.stmt;
            Connection objCon = objMysqlCon.con;
            ProductInfo.productIdArr.add(productId);
            ProductInfo.companyIdArr.add(companyId);
            ProductInfo.companyNameArr.add(companyName);
            ProductInfo.groupIdArr.add(groupId);
            ProductInfo.groupNameArr.add(groupName);
            ProductInfo.productNameArr.add(productName);
            ProductInfo.productTypeArr.add(productType);
            ProductInfo.retailPriceArr.add(retailPrice);
            ProductInfo.tradePriceArr.add(tradePrice);
            ProductInfo.purchasePriceArr.add(purchasePrice);
            if(purchaseDiscount.equals(""))
            {
                purchaseDiscount = "0.0";
            }
            ProductInfo.purchaseDiscountArr.add(purchaseDiscount);
            if(productPacking.equals(""))
            {
                productPacking = "0";
            }
            ProductInfo.productPackingArr.add(productPacking);
            ProductInfo.cartonPackingArr.add(cartonPacking);
            if(salesTax.equals(""))
            {
                salesTax = "0.0";
            }
            ProductInfo.salesTaxArr.add(salesTax);
            ProductInfo.holdStockArr.add(holdStock);
            ProductInfo.maxStockArr.add(maxStockLevel);
            ProductInfo.minStockArr.add(minStockLevel);
            table_add_product.setItems(parseUserList());
            refreshControls();
        }
        else
        {
            alert.setAlertType(Alert.AlertType.ERROR);
            alert.setHeaderText("Error!");
            alert.setContentText("Product ID Already Saved.");
            alert.show();
        }
    }

    @FXML
    void editCancel(ActionEvent event) {
        refreshControls();
        add_product.setVisible(true);
        btn_edit_cancel.setVisible(false);
        btn_edit_update.setVisible(false);
    }

    @FXML
    void editUpdate(ActionEvent event) {
        String productId = txt_product_id.getText();
        String companyId = getCompanyId(companiesData, txt_company_name.getValue());
        String companyName = txt_company_name.getValue();
        String groupId = findIdInGroup(groupsData, txt_company_group.getValue(), getCompanyId(companiesData, txt_company_name.getValue()));
        String groupName = txt_company_group.getValue();
        String productName = txt_product_name.getText();
        String productType = txt_product_type.getValue();
        String productPacking = txt_product_packing.getText();
        String retailPrice = txt_retail_price.getText();
        String tradePrice = txt_trade_price.getText();
        String purchaseDiscount = txt_purchase_discount.getText();
        String purchasePrice = txt_purchase_price.getText();
        String salesTax = txt_sales_tax.getText();
        String holdStock = txt_hold_stock.getText();
        String maxStockLevel = txt_max_stock_level.getText();
        String minStockLevel = txt_min_stock_level.getText();
        String productStatus = "Active";

        String comp = objProductInfo.confirmNewId(productId);

        if(!comp.equals(productId) || comp.equals("")) {
            MysqlCon objMysqlCon = new MysqlCon();
            Statement objStmt = objMysqlCon.stmt;
            Connection objCon = objMysqlCon.con;
            ProductInfo.productIdArr.set(srNo, productId);
            ProductInfo.companyIdArr.set(srNo, companyId);
            ProductInfo.companyNameArr.set(srNo, companyName);
            ProductInfo.groupIdArr.set(srNo, groupId);
            ProductInfo.groupNameArr.set(srNo, groupName);
            ProductInfo.productNameArr.set(srNo, productName);
            ProductInfo.productTypeArr.set(srNo, productType);
            ProductInfo.retailPriceArr.set(srNo, retailPrice);
            ProductInfo.tradePriceArr.set(srNo, tradePrice);
            ProductInfo.purchasePriceArr.set(srNo, purchasePrice);
            ProductInfo.purchaseDiscountArr.set(srNo, purchaseDiscount);
            ProductInfo.productPackingArr.set(srNo, productPacking);
            ProductInfo.salesTaxArr.set(srNo, salesTax);
            ProductInfo.holdStockArr.set(srNo, holdStock);
            ProductInfo.maxStockArr.set(srNo, maxStockLevel);
            ProductInfo.minStockArr.set(srNo, minStockLevel);
            table_add_product.setItems(parseUserList());
            refreshControls();
        }
        else
        {
            alert.setAlertType(Alert.AlertType.ERROR);
            alert.setHeaderText("Error!");
            alert.setContentText("Product ID Already Saved.");
            alert.show();
        }

        add_product.setVisible(true);
        btn_edit_cancel.setVisible(false);
        btn_edit_update.setVisible(false);
    }

    @FXML
    void cancelClicked(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_products.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void follow(MouseEvent event) {
        if (drawer.isOpened()) {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(-250);
            transition.setByY(0);
            transition.setCycleCount(1);
            transition.play();
            drawer.close();
            FA_Icon.setGlyphName("ANGLE_DOUBLE_RIGHT");
            FA_Icon.setGlyphSize(14);
            //inner_anchor.setRightAnchor(inner_anchor,1d);
            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(-90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(1.1000);
            scale.setToY(1);

            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();

        }
        else {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(250);
            transition.setByY(0);
            transition.setCycleCount(1);
            FA_Icon.setGlyphName("ANGLE_DOUBLE_LEFT");
            FA_Icon.setGlyphSize(14);
            drawer.open();

            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(0.99947777);
            scale.setToY(1);



            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();
        }
    }

    @FXML
    void saveClicked(ActionEvent event) throws IOException {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        try {
            objProductInfo.insertProduct(objStmt, objCon);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Parent root = FXMLLoader.load(getClass().getResource("/view/view_products.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void companyChange(ActionEvent event) {
        txt_company_group.getItems().clear();
        txt_company_group.getItems().addAll(getCompanyGroups(groupsData, getCompanyId(companiesData, txt_company_name.getValue())));
    }


    public ArrayList<String> getArrayColumn(ArrayList<ArrayList<String>> array, int columnIndex)
    {
        ArrayList<String> columnList = new ArrayList<>();
        for(ArrayList<String> row: array) {
            columnList.add(row.get(columnIndex));
        }
        return columnList;
    }

    public ArrayList<String> getCompanyGroups(ArrayList<ArrayList<String>> groupsData, String companyId)
    {
        ArrayList<String> columnList = new ArrayList<>();
        chosenSubcompanyIds = new ArrayList<>();
        for(ArrayList<String> row: groupsData) {
            if(row.get(2).equals(companyId))
            {
                chosenSubcompanyIds.add(row.get(0));
                columnList.add(row.get(1));
            }
        }
        return columnList;
    }

    public String getCompanyId(ArrayList<ArrayList<String>> companiesData, String companyName)
    {
        for(ArrayList<String> row: companiesData) {
            if(row.get(1).equals(companyName))
            {
                return row.get(0);
            }
        }
        return null;
    }

    public String findIdInGroup(ArrayList<ArrayList<String>> groupsData, String groupName, String companyId)
    {
        for (int i = 0 ; i < groupsData.size(); i++)
            if (groupsData.get(i).get(1).equals(groupName) && groupsData.get(i).get(2).equals(companyId))
            {
                return groupsData.get(i).get(0);
            }
        return null;
    }

    public void refreshControls()
    {
        this.txt_product_id.setText("");
        this.txt_company_name.getSelectionModel().clearSelection();
        this.txt_company_group.getSelectionModel().clearSelection();
        this.txt_product_name.setText("");
        this.txt_product_type.getSelectionModel().clearSelection();
        this.txt_product_packing.setText("");
        this.txt_retail_price.setText("");
        this.txt_trade_price.setText("");
        this.txt_purchase_price.setText("");
        this.txt_purchase_discount.setText("");
        this.txt_sales_tax.setText("");
        this.txt_hold_stock.setText("");
        this.txt_max_stock_level.setText("");
        this.txt_min_stock_level.setText("");
    }

}
