package controller;

import com.jfoenix.controls.JFXTextField;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import model.*;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

public class Bonusdetails implements Initializable {

    @FXML    private JFXTextField txt_approvalID;
    @FXML    private JFXTextField txt_startDate;
    @FXML    private JFXTextField txt_endDate;
    @FXML    private JFXTextField txt_status;

    @FXML    private TableView table_TypeBonusPolicy;
    @FXML   private TableColumn<BonusDetailInfo, String> serial_no;
    @FXML   private TableColumn<BonusDetailInfo, String> product_id;
    @FXML   private TableColumn<BonusDetailInfo, String> product_name;
    @FXML   private TableColumn<BonusDetailInfo, String> dealer_type;
    @FXML   private TableColumn<BonusDetailInfo, String> quantity;
    @FXML   private TableColumn<BonusDetailInfo, String> bonus;


    @FXML    private TableView table_dealerIDBonusPolicy;
    @FXML   private TableColumn<BonusDetailInfo, String> sr_no;
    @FXML   private TableColumn<BonusDetailInfo, String> productID;
    @FXML   private TableColumn<BonusDetailInfo, String> productName;
    @FXML   private TableColumn<BonusDetailInfo, String> dealerID;
    @FXML   private TableColumn<BonusDetailInfo, String>dealerName;
    @FXML   private TableColumn<BonusDetailInfo, String> quantities;
    @FXML   private TableColumn<BonusDetailInfo, String> bonuses;

    private String currentBonusapprovalId;
    private ObservableList<BonusDetailInfo> bonusesDetails;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle)
    {
        if(!ViewBonusInfo.viewApprovalId.equals(""))
        {
            currentBonusapprovalId = ViewBonusInfo.viewApprovalId;
            ViewBonusInfo.viewApprovalId = "";
        }

        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        ArrayList<ArrayList<String>> bonusPolicy_details;
        BonusDetailInfo objViewBonus = new BonusDetailInfo();
        bonusPolicy_details = objViewBonus.getBonusPolicydetails(objStmt,objCon,currentBonusapprovalId);
        for(int i = 0;i<bonusPolicy_details.size();i++)
        {
                table_TypeBonusPolicy.setVisible(false);
                table_dealerIDBonusPolicy.setVisible(true);
                sr_no.setCellValueFactory(new PropertyValueFactory<>("sr_No"));
                productID.setCellValueFactory(new PropertyValueFactory<>("products_id"));
                productName.setCellValueFactory(new PropertyValueFactory<>("products_name"));
                dealerID.setCellValueFactory(new PropertyValueFactory<>("dealers_id"));
                dealerName.setCellValueFactory(new PropertyValueFactory<>("dealers_name"));
                quantities.setCellValueFactory(new PropertyValueFactory<>("quantities"));
                bonuses.setCellValueFactory(new PropertyValueFactory<>("bonuses"));
            try {
                table_dealerIDBonusPolicy.setItems(parseUserList());
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }
    }
    String policy_Status = "";
    private ObservableList<BonusDetailInfo> parseUserList() throws ParseException {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        bonusesDetails = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> bonus_Data;
        BonusDetailInfo objbonusdetailInfo = new BonusDetailInfo();
            String getcurrDate = GlobalVariables.getStDate();
        bonus_Data = objbonusdetailInfo.getBonusPolicydetails(objStmt, objCon, currentBonusapprovalId);

        boolean activ_or_not = activ_notactiv(bonus_Data.get(0).get(7),bonus_Data.get(0).get(8),getcurrDate);
        if(activ_or_not == true)
        {
            policy_Status= "Active";
        }
        else
        {
            policy_Status = "In Active";
        }

        txt_approvalID.setText(bonus_Data.get(0).get(0));
        txt_startDate.setText(bonus_Data.get(0).get(7));
        txt_endDate.setText(bonus_Data.get(0).get(8));
        txt_status.setText(policy_Status);

        if(policy_Status.equals(bonus_Data.get(0).get(9)))
        {

        }
        else
        {
            MysqlCon mysqlOBJects = new MysqlCon();
            Statement mystatmntOBJect = mysqlOBJects.stmt;
            Connection mysqlConnection = mysqlOBJects.con;
            SaleInvoice objectssaleinvoice = new SaleInvoice();
            int intvalu_approvID = Integer.parseInt(bonus_Data.get(0).get(0));
            objectssaleinvoice.UPDATE_BonuspolicyStatus(mystatmntOBJect,mysqlConnection,intvalu_approvID,bonus_Data.get(0).get(1),policy_Status);
        }

        txt_status.setDisable(true);
        txt_endDate.setDisable(true);
        txt_startDate.setDisable(true);
        txt_approvalID.setDisable(true);

        for (int i = 0; i < bonus_Data.size(); i++)
        {

                bonusesDetails.add(new BonusDetailInfo(String.valueOf(i + 1), bonus_Data.get(i).get(1), bonus_Data.get(i).get(2), bonus_Data.get(i).get(3), bonus_Data.get(i).get(4), bonus_Data.get(i).get(5), bonus_Data.get(i).get(5), bonus_Data.get(i).get(6)));

        }
        return bonusesDetails;
    }
    public boolean activ_notactiv(String st_date,String endDate,String curr_date) throws ParseException {
        DateFormat format = new SimpleDateFormat("dd/MMM/yyyy", Locale.ENGLISH);
        Date Sdate = format.parse(st_date);
        Date Edate = format.parse(endDate);
        Date Currdate = format.parse(curr_date);


        return Sdate.compareTo(Currdate) * Currdate.compareTo(Edate) >= 0;
    }
    @FXML
    void closeDialogue() throws IOException {
        ViewBonusInfo.stackPane.setVisible(false);
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_bonus.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
}
