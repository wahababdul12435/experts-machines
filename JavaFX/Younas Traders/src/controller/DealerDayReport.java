package controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXDrawer;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.util.StringConverter;
import model.*;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

public class DealerDayReport implements Initializable {

    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private AnchorPane filter_pane;

    @FXML
    private JFXDatePicker txt_from_date;

    @FXML
    private JFXDatePicker txt_to_date;

    @FXML
    private JFXComboBox<String> txt_day;

    @FXML
    private HBox summary_hbox1;

    @FXML
    private Label lbl_booked;

    @FXML
    private Label lbl_delivered;

    @FXML
    private Label lbl_returned;

    @FXML
    private Label lbl_booking_price;

    @FXML
    private Label lbl_discount_given;

    @FXML
    private Label lbl_items_returned;

    @FXML
    private HBox summary_hbox2;

    @FXML
    private Label lbl_return_price;

    @FXML
    private Label lbl_cash_collection;

    @FXML
    private Label lbl_cash_return;

    @FXML
    private Label lbl_cash_waiveoff;

    @FXML
    private Label lbl_cash_pending;

    @FXML
    private TableView<DealerDayReportInfo> table_dealerreportlog;

    @FXML
    private TableColumn<DealerDayReportInfo, String> sr_no;

    @FXML
    private TableColumn<DealerDayReportInfo, String> date;

    @FXML
    private TableColumn<DealerDayReportInfo, String> day;

    @FXML
    private TableColumn<DealerDayReportInfo, String> booking_invoice;

    @FXML
    private TableColumn<DealerDayReportInfo, String> delivered_invoice;

    @FXML
    private TableColumn<DealerDayReportInfo, String> returned_invoice;

    @FXML
    private TableColumn<DealerDayReportInfo, String> booking_price;

    @FXML
    private TableColumn<DealerDayReportInfo, String> discount_given;

    @FXML
    private TableColumn<DealerDayReportInfo, String> quantity_returned;

    @FXML
    private TableColumn<DealerDayReportInfo, String> return_price;

    @FXML
    private TableColumn<DealerDayReportInfo, String> cash_collection;

    @FXML
    private TableColumn<DealerDayReportInfo, String> cash_return;

    @FXML
    private TableColumn<DealerDayReportInfo, String> cash_waive_off;

    @FXML
    private TableColumn<DealerDayReportInfo, String> cash_pending;

//    @FXML
//    private TableColumn<DealerDayReportInfo, String> operations;

    @FXML
    private StackPane stackPane;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    @FXML
    private BorderPane brdrpane_navfinances;

    @FXML
    private BorderPane menu_bar;

    private ObservableList<DealerDayReportInfo> dealerDetails;
    private DealerDayReportInfo objDealerDayReportInfo;
    ArrayList<ArrayList<String>> dealerReportData;
    public ArrayList<DealerDayReportInfo> summaryData;

    float totalBooked = 0;
    float totalDelivered = 0;
    float totalReturned = 0;
    float bookingPrice = 0;
    float discountGiven = 0;
    float itemsReturned = 0;
    float returnPrice = 0;
    float cashCollection = 0;
    float cashReturn = 0;
    float cashWaiveOff = 0;
    float cashPending = 0;

    public static String txtFromDate = "";
    public static String txtToDate = "";
    public static String txtDay = "All";
    public static boolean filter;

    MysqlCon objMysqlCon1;
    MysqlCon objMysqlCon2;
    MysqlCon objMysqlCon3;
    Statement objStmt1;
    Statement objStmt2;
    Statement objStmt3;
    Connection objCon;

    public static MenuButton btnView;
    SimpleDateFormat fmt = new SimpleDateFormat("dd/MMM/yyyy");

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnReport.setStyle("-fx-background-color: #47ab1e");
        objMysqlCon1 = new MysqlCon();
        objMysqlCon2 = new MysqlCon();
        objMysqlCon3 = new MysqlCon();
        objStmt1 = objMysqlCon1.stmt;
        objStmt2 = objMysqlCon2.stmt;
        objStmt3 = objMysqlCon3.stmt;
        objCon = objMysqlCon1.con;
        objDealerDayReportInfo = new DealerDayReportInfo();
        dealerReportData = new ArrayList<>();

        txt_from_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        txt_to_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        summaryData = new ArrayList<>();
        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        date.setCellValueFactory(new PropertyValueFactory<>("date"));
        day.setCellValueFactory(new PropertyValueFactory<>("day"));
        booking_invoice.setCellValueFactory(new PropertyValueFactory<>("bookingInvoice"));
        delivered_invoice.setCellValueFactory(new PropertyValueFactory<>("deliveredInvoice"));
        returned_invoice.setCellValueFactory(new PropertyValueFactory<>("returnedInvoice"));
        booking_price.setCellValueFactory(new PropertyValueFactory<>("bookingPrice"));
        discount_given.setCellValueFactory(new PropertyValueFactory<>("discountGiven"));
        quantity_returned.setCellValueFactory(new PropertyValueFactory<>("quantityReturned"));
        return_price.setCellValueFactory(new PropertyValueFactory<>("returnPrice"));
        cash_collection.setCellValueFactory(new PropertyValueFactory<>("cashCollection"));
        cash_return.setCellValueFactory(new PropertyValueFactory<>("cashReturn"));
        cash_waive_off.setCellValueFactory(new PropertyValueFactory<>("cashWaiveOff"));
        cash_pending.setCellValueFactory(new PropertyValueFactory<>("cashPending"));
//        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_dealerreportlog.setItems(parseUserList());
        lbl_booked.setText("Booked\n"+String.format("%,.0f", totalBooked));
        lbl_delivered.setText("Delivered\n"+String.format("%,.0f", totalDelivered));
        lbl_returned.setText("Returned\n"+String.format("%,.0f", totalReturned));
        lbl_booking_price.setText("Booking Price\n"+"Rs."+String.format("%,.0f", bookingPrice)+"/-");
        lbl_discount_given.setText("Discount Given\n"+"Rs."+String.format("%,.0f", discountGiven)+"/-");
        lbl_items_returned.setText("Items Returned\n"+String.format("%,.0f", itemsReturned));
        lbl_return_price.setText("Return Price\n"+"Rs."+String.format("%,.0f", returnPrice)+"/-");
        lbl_cash_collection.setText("Cash Collection\n"+"Rs."+String.format("%,.0f", cashCollection)+"/-");
        lbl_cash_return.setText("Cash Return\n"+"Rs."+String.format("%,.0f", cashReturn)+"/-");
        lbl_cash_waiveoff.setText("Cash Waived Off\n"+"Rs."+String.format("%,.0f", cashWaiveOff)+"/-");
        lbl_cash_pending.setText("Cash Pending\n"+"Rs."+String.format("%,.0f", cashPending)+"/-");
    }

    private ObservableList<DealerDayReportInfo> parseUserList() {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        dealerDetails = FXCollections.observableArrayList();

        if (filter) {
            try {
                dealerReportData = objDealerDayReportInfo.getDealerReportDetailSearch(objStmt1, objStmt2, objStmt3, objCon, txtFromDate, txtToDate, txtDay);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (txtFromDate != null && !txtFromDate.equals("")) {
                txt_from_date.setValue(GlobalVariables.LOCAL_DATE(txtFromDate));
            }
            if (txtToDate != null && !txtToDate.equals("")) {
                txt_to_date.setValue(GlobalVariables.LOCAL_DATE(txtToDate));
            }
            txt_day.setValue(txtDay);
            filter = false;
            filter_pane.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
            txtFromDate = "";
            txtToDate = "";
            txtDay = "All";
        } else {
            try {
                dealerReportData = objDealerDayReportInfo.getDealerReportDetail(objStmt1, objStmt2, objStmt3, objCon);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }


        for (int i = 0; i < dealerReportData.size(); i++)
        {
            totalBooked += (dealerReportData.get(i).get(2) != null && !dealerReportData.get(i).get(2).equals("-")) ? Float.parseFloat(dealerReportData.get(i).get(2)) : 0;
            totalDelivered += (dealerReportData.get(i).get(3) != null && !dealerReportData.get(i).get(3).equals("-")) ? Float.parseFloat(dealerReportData.get(i).get(3)) : 0;
            totalReturned += (dealerReportData.get(i).get(4) != null && !dealerReportData.get(i).get(4).equals("-")) ? Float.parseFloat(dealerReportData.get(i).get(4)) : 0;
            bookingPrice += (dealerReportData.get(i).get(5) != null && !dealerReportData.get(i).get(5).equals("-")) ? Float.parseFloat(dealerReportData.get(i).get(5)) : 0;
            discountGiven += (dealerReportData.get(i).get(6) != null && !dealerReportData.get(i).get(6).equals("-")) ? Float.parseFloat(dealerReportData.get(i).get(6)) : 0;
            itemsReturned += (dealerReportData.get(i).get(7) != null && !dealerReportData.get(i).get(7).equals("-")) ? Float.parseFloat(dealerReportData.get(i).get(7)) : 0;
            returnPrice += (dealerReportData.get(i).get(8) != null && !dealerReportData.get(i).get(8).equals("-")) ? Float.parseFloat(dealerReportData.get(i).get(8)) : 0;
            cashCollection += (dealerReportData.get(i).get(9) != null && !dealerReportData.get(i).get(9).equals("-")) ? Float.parseFloat(dealerReportData.get(i).get(9)) : 0;
            cashReturn += (dealerReportData.get(i).get(10) != null && !dealerReportData.get(i).get(10).equals("-")) ? Float.parseFloat(dealerReportData.get(i).get(10)) : 0;
            cashWaiveOff += (dealerReportData.get(i).get(11) != null && !dealerReportData.get(i).get(11).equals("-")) ? Float.parseFloat(dealerReportData.get(i).get(11)) : 0;
            cashPending += (dealerReportData.get(i).get(12) != null && !dealerReportData.get(i).get(12).equals("-")) ? Float.parseFloat(dealerReportData.get(i).get(12)) : 0;
            dealerDetails.add(new DealerDayReportInfo(String.valueOf(i+1), ((dealerReportData.get(i).get(0) == null || dealerReportData.get(i).get(0).equals("")) ? "N/A" : dealerReportData.get(i).get(0)), dealerReportData.get(i).get(1), dealerReportData.get(i).get(2), dealerReportData.get(i).get(3), dealerReportData.get(i).get(4), dealerReportData.get(i).get(5), dealerReportData.get(i).get(6), dealerReportData.get(i).get(7), dealerReportData.get(i).get(8), dealerReportData.get(i).get(9), dealerReportData.get(i).get(10), dealerReportData.get(i).get(11), dealerReportData.get(i).get(12)));
            summaryData.add(new DealerDayReportInfo(String.valueOf(i+1), ((dealerReportData.get(i).get(0) == null || dealerReportData.get(i).get(0).equals("")) ? "N/A" : dealerReportData.get(i).get(0)), dealerReportData.get(i).get(1), dealerReportData.get(i).get(2), dealerReportData.get(i).get(3), dealerReportData.get(i).get(4), dealerReportData.get(i).get(5), dealerReportData.get(i).get(6), dealerReportData.get(i).get(7), dealerReportData.get(i).get(8), dealerReportData.get(i).get(9), dealerReportData.get(i).get(10), dealerReportData.get(i).get(11), dealerReportData.get(i).get(12)));
        }
        return dealerDetails;
    }

    public void generateSummary(ArrayList<DealerDayReportInfo> summaryList) throws JRException {
        InputStream objIO = DealerDayReport.class.getResourceAsStream("/reports/DealerDayReport.jrxml");
        JasperReport objReport = JasperCompileManager.compileReport(objIO);
        JasperPrint objPrint = JasperFillManager.fillReport(objReport, null, new JRBeanCollectionDataSource(summaryList));
        JasperViewer.viewReport(objPrint, false);
    }

    @FXML
    void printData(ActionEvent event) {
        try {
            generateSummary(summaryData);
        } catch (JRException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void btnReset(ActionEvent event) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/dealer_day_report.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void btnSearch(ActionEvent event) {
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date selectedDate = null;
        if(txt_from_date.getValue() != null)
        {
            txtFromDate = txt_from_date.getValue().toString();
            try {
                selectedDate = sdf.parse(txtFromDate);
                txtFromDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if(txt_to_date.getValue() != null)
        {
            txtToDate = txt_to_date.getValue().toString();
            try {
                selectedDate = sdf.parse(txtToDate);
                txtToDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        txtDay = txt_day.getValue();
        if(txtDay == null)
        {
            txtDay = "All";
        }
        filter = true;

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/dealer_day_report.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void follow(MouseEvent event) {

    }

    @FXML
    void showBooked(MouseEvent event) {

    }

    @FXML
    void showBookingPrice(MouseEvent event) {

    }

    @FXML
    void showCollection(MouseEvent event) {

    }

    @FXML
    void showDelivered(MouseEvent event) {

    }

    @FXML
    void showDiscountGiven(MouseEvent event) {

    }

    @FXML
    void showItemsReturned(MouseEvent event) {

    }

    @FXML
    void showPending(MouseEvent event) {

    }

    @FXML
    void showReturn(MouseEvent event) {

    }

    @FXML
    void showReturnPrice(MouseEvent event) {

    }

    @FXML
    void showReturned(MouseEvent event) {

    }

    @FXML
    void showWaiveOff(MouseEvent event) {

    }
}
