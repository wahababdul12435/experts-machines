package controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.util.StringConverter;
import model.*;
import org.apache.poi.ss.usermodel.DateUtil;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class DealerFinanceDetailReport implements Initializable {

    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private Label lbl_dealer_id;

    @FXML
    private Label lbl_dealer_name;

    @FXML
    private Label lbl_dealer_contact;

    @FXML
    private Label lbl_dealer_area;

    @FXML
    private Label lbl_dealer_address;

    @FXML
    private Label lbl_dealer_type;

    @FXML
    private Label lbl_dealer_cnic;

    @FXML
    private Label lbl_dealer_licnum;

    @FXML
    private Label lbl_dealer_licexp;

    @FXML
    private Label lbl_dealer_created;

    @FXML
    private Label lbl_dealer_updated;

    @FXML
    private Label lbl_dealer_status;

    @FXML
    private ImageView img_user;

    @FXML
    private BarChart<Integer, String> cash_collection_chart;

    @FXML
    private CategoryAxis timeAxis;

    @FXML
    private NumberAxis collectionAxis;

    @FXML
    private AnchorPane filter_pane;

    @FXML
    private JFXDatePicker txt_from_date;

    @FXML
    private JFXDatePicker txt_to_date;

    @FXML
    private JFXComboBox<String> txt_day;

    @FXML
    private JFXTextField txt_from_amount;

    @FXML
    private JFXTextField txt_to_amount;

    @FXML
    private JFXComboBox<String> txt_type;

    @FXML
    private TableView<DealerFinanceDetailReportInfo> table_dealercashlog;

    @FXML
    private TableColumn<DealerFinanceDetailReportInfo, String> sr_no;

    @FXML
    private TableColumn<DealerFinanceDetailReportInfo, String> date;

    @FXML
    private TableColumn<DealerFinanceDetailReportInfo, String> day;

    @FXML
    private TableColumn<DealerFinanceDetailReportInfo, String> cash_collection;

    @FXML
    private TableColumn<DealerFinanceDetailReportInfo, String> cash_return;

    @FXML
    private TableColumn<DealerFinanceDetailReportInfo, String> cash_waive_off;

    @FXML
    private TableColumn<DealerFinanceDetailReportInfo, String> operations;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    @FXML
    private BorderPane brdrpane_navfinances;

    @FXML
    private StackPane stackPane;

    @FXML
    private BorderPane menu_bar;

    private DealerInfo objDealerInfo;
    private DealerFinanceDetailReportInfo objDealerFinanceDetailReportInfo;
    private String currentDealerId;
    private String selectedImagePath;
    private ArrayList<String> dealerInfo;
    private ObservableList<DealerFinanceDetailReportInfo> dealerDetails;
    ArrayList<ArrayList<String>> stDates = new ArrayList<>();
    ArrayList<Date> dates = new ArrayList<Date>();

    public static String txtFromDate = "";
    public static String txtToDate = "";
    public static String txtDay = "All";
    public static String txtFromAmount = "";
    public static String txtToAmount = "";
    public static String txtType = "All";
    public static boolean filter;

    public static MenuButton btnView;
    SimpleDateFormat fmt = new SimpleDateFormat("dd/MMM/yyyy");
    int span;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnReport.setStyle("-fx-background-color: #47ab1e");
        DealerFinanceDetailReportInfo.stackPane = stackPane;
        objDealerInfo = new DealerInfo();
        objDealerFinanceDetailReportInfo = new DealerFinanceDetailReportInfo();
        if(!DealerFinanceReportInfo.dealerFinanceId.equals(""))
        {
            currentDealerId = DealerFinanceReportInfo.dealerFinanceId;
            DealerFinanceReportInfo.dealerFinanceId = "";
        }
        dealerInfo = objDealerInfo.getDealerDetail(currentDealerId);
        lbl_dealer_id.setText(((dealerInfo.get(0) == null) ? "N/A" : dealerInfo.get(0)));
        lbl_dealer_name.setText(dealerInfo.get(1));
        lbl_dealer_contact.setText(dealerInfo.get(2));
        lbl_dealer_area.setText((dealerInfo.get(3) == null) ? "N/A" : dealerInfo.get(3));
        lbl_dealer_address.setText(dealerInfo.get(4));
        lbl_dealer_type.setText((dealerInfo.get(5) == null) ? "N/A" : dealerInfo.get(5));
        lbl_dealer_cnic.setText(dealerInfo.get(6));
        lbl_dealer_licnum.setText((dealerInfo.get(7) == null) ? "N/A" : dealerInfo.get(7));
        lbl_dealer_licexp.setText((dealerInfo.get(8) == null) ? "N/A" : dealerInfo.get(8));
        lbl_dealer_created.setText(dealerInfo.get(9));
        lbl_dealer_updated.setText((dealerInfo.get(10) == null) ? "N/A" : dealerInfo.get(10));
        lbl_dealer_status.setText(dealerInfo.get(11));
        if(dealerInfo.get(12) != null && !dealerInfo.get(12).equals(""))
        {
            selectedImagePath = dealerInfo.get(12);
            File file = new File(selectedImagePath);
            boolean exists = file.exists();
            if(exists)
            {
                Image image = new Image(file.toURI().toString());
                img_user.setImage(image);
            }
        }

        txt_from_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        txt_to_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        date.setCellValueFactory(new PropertyValueFactory<>("date"));
        day.setCellValueFactory(new PropertyValueFactory<>("day"));
        cash_collection.setCellValueFactory(new PropertyValueFactory<>("cashCollection"));
        cash_return.setCellValueFactory(new PropertyValueFactory<>("cashReturn"));
        cash_waive_off.setCellValueFactory(new PropertyValueFactory<>("cashWaiveOff"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_dealercashlog.setItems(parseUserList());

        timeAxis.setLabel("Time Span");
        collectionAxis.setLabel("Cash Collection");

//        for(int i=0; i<stDates.size(); i++)
//        {
//            for(int j=0; j<2; j++)
//            {
//                System.out.print(stDates.get(i).get(j)+"  --  ");
//            }
//            System.out.println();
//        }

        XYChart.Series dataSeries1 = new XYChart.Series();
        XYChart.Series dataSeries2 = new XYChart.Series();
        XYChart.Series dataSeries3 = new XYChart.Series();
        for(int i=0; i<stDates.size()-1; i++)
        {
            if(stDates.get(i).get(0).equals(stDates.get(stDates.size()-1).get(0)))
            {
                dataSeries1.getData().add(new XYChart.Data((Integer.parseInt(stDates.get(i).get(1))), stDates.get(i+1).get(0)));
                dataSeries2.getData().add(new XYChart.Data((Integer.parseInt(stDates.get(i).get(2))), stDates.get(i+1).get(0)));
                dataSeries3.getData().add(new XYChart.Data((Integer.parseInt(stDates.get(i).get(3))), stDates.get(i+1).get(0)));
                break;
            }
            String stLowerBoundDate = stDates.get(i).get(0);
            Date lowerBoundDate = null;
            try {
                lowerBoundDate = fmt.parse(stLowerBoundDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Calendar calTemp = Calendar.getInstance();
            calTemp.setTime(lowerBoundDate);
            calTemp.add(Calendar.DAY_OF_MONTH, -1);
            try {
                lowerBoundDate = fmt.parse(fmt.format(calTemp.getTime()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            fmt.format(calTemp.getTime());
            stLowerBoundDate = fmt.format(calTemp.getTime());
            if(span > 1)
            {
                dataSeries1.getData().add(new XYChart.Data((Integer.parseInt(stDates.get(i).get(1))), stDates.get(i+1).get(0)+"\n|\n"+stLowerBoundDate));
                dataSeries2.getData().add(new XYChart.Data((Integer.parseInt(stDates.get(i).get(2))), stDates.get(i+1).get(0)+"\n|\n"+stLowerBoundDate));
                dataSeries3.getData().add(new XYChart.Data((Integer.parseInt(stDates.get(i).get(3))), stDates.get(i+1).get(0)+"\n|\n"+stLowerBoundDate));
            }
            else
            {
                dataSeries1.getData().add(new XYChart.Data((Integer.parseInt(stDates.get(i).get(1))), stDates.get(i).get(0)));
                dataSeries2.getData().add(new XYChart.Data((Integer.parseInt(stDates.get(i).get(2))), stDates.get(i).get(0)));
                dataSeries3.getData().add(new XYChart.Data((Integer.parseInt(stDates.get(i).get(3))), stDates.get(i).get(0)));
            }
        }
        dataSeries1.setName("Collection");
        dataSeries2.setName("Return");
        dataSeries3.setName("Waive Off");
        cash_collection_chart.getData().add(dataSeries1);
        cash_collection_chart.getData().add(dataSeries2);
        cash_collection_chart.getData().add(dataSeries3);
    }

    private ObservableList<DealerFinanceDetailReportInfo> parseUserList(){
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        dealerDetails = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> dealerFinanceData;

        if(filter)
        {
            dealerFinanceData = objDealerFinanceDetailReportInfo.getDealerFinanceDetailSearch(objStmt, objCon, currentDealerId, txtFromDate, txtToDate, txtDay, txtFromAmount, txtToAmount, txtType);
            if(txtFromDate != null && !txtFromDate.equals(""))
            {
                txt_from_date.setValue(LOCAL_DATE(txtFromDate));
            }
            if(txtToDate != null && !txtToDate.equals(""))
            {
                txt_to_date.setValue(LOCAL_DATE(txtToDate));
            }
            txt_day.setValue(txtDay);
            txt_from_amount.setText(txtFromAmount);
            txt_to_amount.setText(txtToAmount);
            txt_type.setValue(txtType);
            filter = false;
            filter_pane.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
        }
        else
        {
            dealerFinanceData = objDealerFinanceDetailReportInfo.getDealerFinanceDetail(objStmt, objCon, currentDealerId);
        }

        Date tempDate = null;
        if(dealerFinanceData.size() > 0)
        {
            String stEndDate = dealerFinanceData.get(0).get(3);
            String stStartDate = dealerFinanceData.get(dealerFinanceData.size()-1).get(3);

            Date endDate = null;
            Date startDate = null;
            try {
                startDate = fmt.parse(stStartDate);
                endDate = fmt.parse(stEndDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            long diff = endDate.getTime() - startDate.getTime();
            diff = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
            span = (int) (diff/4);
            span = span * -1;
            Calendar cal = Calendar.getInstance();
            cal.setTime(endDate);
            ArrayList<String> temp = new ArrayList<>();
            temp.add(fmt.format(cal.getTime()));
            temp.add("0");
            temp.add("0");
            temp.add("0");
            stDates.add(temp);
            tempDate = endDate;
            dates.add(tempDate);
            for(int i=1; i<=5; i++)
            {
                temp = new ArrayList<>();
                cal.add(Calendar.DAY_OF_MONTH, span);
                try {
                    tempDate = fmt.parse(fmt.format(cal.getTime()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                temp.add(fmt.format(cal.getTime()));
                temp.add("0");
                temp.add("0");
                temp.add("0");
                stDates.add(temp);
                dates.add(tempDate);
            }

        }

        for (int i = 0; i < dealerFinanceData.size(); i++)
        {
            try {
                tempDate = fmt.parse(dealerFinanceData.get(i).get(3));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if(dealerFinanceData.get(i).get(6).equals("Collection"))
            {
                if((tempDate.before(dates.get(0)) && tempDate.after(dates.get(1))) || tempDate.equals(dates.get(0)))
                {
//                    System.out.println("1st: Small Date: "+dates.get(0)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(1));
                    String value = String.valueOf(Integer.parseInt(stDates.get(0).get(1)) + Integer.parseInt(dealerFinanceData.get(i).get(5)));
                    stDates.get(0).set(1, value);
                }
                else if((tempDate.before(dates.get(1)) && tempDate.after(dates.get(2))) || tempDate.equals(dates.get(1)))
                {
//                    System.out.println("2nd: Small Date: "+dates.get(1)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(2));
                    String value = String.valueOf(Integer.parseInt(stDates.get(1).get(1)) + Integer.parseInt(dealerFinanceData.get(i).get(5)));
                    stDates.get(1).set(1, value);
                }
                else if((tempDate.before(dates.get(2)) && tempDate.after(dates.get(3))) || tempDate.equals(dates.get(2)))
                {
//                    System.out.println("3rd: Small Date: "+dates.get(2)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(3));
                    String value = String.valueOf(Integer.parseInt(stDates.get(2).get(1)) + Integer.parseInt(dealerFinanceData.get(i).get(5)));
                    stDates.get(2).set(1, value);
                }
                else if((tempDate.before(dates.get(3)) && tempDate.after(dates.get(4))) || tempDate.equals(dates.get(3)))
                {
//                    System.out.println("4th: Small Date: "+dates.get(3)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(4));
                    String value = String.valueOf(Integer.parseInt(stDates.get(3).get(1)) + Integer.parseInt(dealerFinanceData.get(i).get(5)));
                    stDates.get(3).set(1, value);
                }
                else if((tempDate.before(dates.get(4)) && tempDate.after(dates.get(5))) || tempDate.equals(dates.get(4)))
                {
//                    System.out.println("4th: Small Date: "+dates.get(3)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(4));
                    String value = String.valueOf(Integer.parseInt(stDates.get(4).get(1)) + Integer.parseInt(dealerFinanceData.get(i).get(5)));
                    stDates.get(4).set(1, value);
                }
                dealerDetails.add(new DealerFinanceDetailReportInfo(String.valueOf(i+1), dealerFinanceData.get(i).get(0), dealerFinanceData.get(i).get(1), dealerFinanceData.get(i).get(2), dealerFinanceData.get(i).get(3), dealerFinanceData.get(i).get(4), dealerFinanceData.get(i).get(5), "-", "-", dealerFinanceData.get(i).get(7)));
            }
            else if(dealerFinanceData.get(i).get(6).equals("Return"))
            {
                if((tempDate.before(dates.get(0)) && tempDate.after(dates.get(1))) || tempDate.equals(dates.get(0)))
                {
//                    System.out.println("1st: Small Date: "+dates.get(0)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(1));
                    String value = String.valueOf(Integer.parseInt(stDates.get(0).get(2)) + Integer.parseInt(dealerFinanceData.get(i).get(5)));
                    stDates.get(0).set(2, value);
                }
                else if((tempDate.before(dates.get(1)) && tempDate.after(dates.get(2))) || tempDate.equals(dates.get(1)))
                {
//                    System.out.println("2nd: Small Date: "+dates.get(1)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(2));
                    String value = String.valueOf(Integer.parseInt(stDates.get(1).get(2)) + Integer.parseInt(dealerFinanceData.get(i).get(5)));
                    stDates.get(1).set(2, value);
                }
                else if((tempDate.before(dates.get(2)) && tempDate.after(dates.get(3))) || tempDate.equals(dates.get(2)))
                {
//                    System.out.println("3rd: Small Date: "+dates.get(2)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(3));
                    String value = String.valueOf(Integer.parseInt(stDates.get(2).get(2)) + Integer.parseInt(dealerFinanceData.get(i).get(5)));
                    stDates.get(2).set(2, value);
                }
                else if((tempDate.before(dates.get(3)) && tempDate.after(dates.get(4))) || tempDate.equals(dates.get(3)))
                {
//                    System.out.println("4th: Small Date: "+dates.get(3)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(4));
                    String value = String.valueOf(Integer.parseInt(stDates.get(3).get(2)) + Integer.parseInt(dealerFinanceData.get(i).get(5)));
                    stDates.get(3).set(2, value);
                }
                else if((tempDate.before(dates.get(4)) && tempDate.after(dates.get(5))) || tempDate.equals(dates.get(4)))
                {
//                    System.out.println("4th: Small Date: "+dates.get(3)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(4));
                    String value = String.valueOf(Integer.parseInt(stDates.get(4).get(2)) + Integer.parseInt(dealerFinanceData.get(i).get(5)));
                    stDates.get(4).set(2, value);
                }
                dealerDetails.add(new DealerFinanceDetailReportInfo(String.valueOf(i+1), dealerFinanceData.get(i).get(0), dealerFinanceData.get(i).get(1), dealerFinanceData.get(i).get(2), dealerFinanceData.get(i).get(3), dealerFinanceData.get(i).get(4), "-", dealerFinanceData.get(i).get(5), "-", dealerFinanceData.get(i).get(7)));
            }
            else
            {
                if((tempDate.before(dates.get(0)) && tempDate.after(dates.get(1))) || tempDate.equals(dates.get(0)))
                {
//                    System.out.println("1st: Small Date: "+dates.get(0)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(1));
                    String value = String.valueOf(Integer.parseInt(stDates.get(0).get(3)) + Integer.parseInt(dealerFinanceData.get(i).get(5)));
                    stDates.get(0).set(3, value);
                }
                else if((tempDate.before(dates.get(1)) && tempDate.after(dates.get(2))) || tempDate.equals(dates.get(1)))
                {
//                    System.out.println("2nd: Small Date: "+dates.get(1)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(2));
                    String value = String.valueOf(Integer.parseInt(stDates.get(1).get(3)) + Integer.parseInt(dealerFinanceData.get(i).get(5)));
                    stDates.get(1).set(3, value);
                }
                else if((tempDate.before(dates.get(2)) && tempDate.after(dates.get(3))) || tempDate.equals(dates.get(2)))
                {
//                    System.out.println("3rd: Small Date: "+dates.get(2)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(3));
                    String value = String.valueOf(Integer.parseInt(stDates.get(2).get(3)) + Integer.parseInt(dealerFinanceData.get(i).get(5)));
                    stDates.get(2).set(3, value);
                }
                else if((tempDate.before(dates.get(3)) && tempDate.after(dates.get(4))) || tempDate.equals(dates.get(3)))
                {
//                    System.out.println("4th: Small Date: "+dates.get(3)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(4));
                    String value = String.valueOf(Integer.parseInt(stDates.get(3).get(3)) + Integer.parseInt(dealerFinanceData.get(i).get(5)));
                    stDates.get(3).set(3, value);
                }
                else if((tempDate.before(dates.get(4)) && tempDate.after(dates.get(5))) || tempDate.equals(dates.get(4)))
                {
//                    System.out.println("4th: Small Date: "+dates.get(3)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(4));
                    String value = String.valueOf(Integer.parseInt(stDates.get(4).get(3)) + Integer.parseInt(dealerFinanceData.get(i).get(5)));
                    stDates.get(4).set(3, value);
                }
                dealerDetails.add(new DealerFinanceDetailReportInfo(String.valueOf(i+1), dealerFinanceData.get(i).get(0), dealerFinanceData.get(i).get(1), dealerFinanceData.get(i).get(2), dealerFinanceData.get(i).get(3), dealerFinanceData.get(i).get(4), "-", "-", dealerFinanceData.get(i).get(5), dealerFinanceData.get(i).get(7)));
            }
        }
//        System.out.println("   -----------------------------   ");
//        System.out.println("   -----------------------------   ");
//        System.out.println("   -----------------------------   ");
        return dealerDetails;
    }

    @FXML
    void follow(MouseEvent event) {

    }

    @FXML
    void btnReset(ActionEvent event) {
        DealerFinanceReportInfo.dealerFinanceId = currentDealerId;
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/dealer_finance_detail.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void btnSearch(ActionEvent event) {
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date selectedDate = null;
        if(txt_from_date.getValue() != null)
        {
            txtFromDate = txt_from_date.getValue().toString();
            try {
                selectedDate = sdf.parse(txtFromDate);
                txtFromDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if(txt_to_date.getValue() != null)
        {
            txtToDate = txt_to_date.getValue().toString();
            try {
                selectedDate = sdf.parse(txtToDate);
                txtToDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        txtDay = txt_day.getValue();
        if(txtDay == null)
        {
            txtDay = "All";
        }
        txtFromAmount = txt_from_amount.getText();
        txtToAmount = txt_to_amount.getText();
        txtType = txt_type.getValue();
        if(txtType == null)
        {
            txtType = "All";
        }
        filter = true;

        DealerFinanceReportInfo.dealerFinanceId = currentDealerId;
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/dealer_finance_detail.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    public static final LocalDate LOCAL_DATE (String dateString) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MMM/yyyy");
        LocalDate localDate = LocalDate.parse(dateString, formatter);
        return localDate;
    }
}
