package controller;

import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXScrollPane;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import javafx.util.Duration;
import model.*;
import tray.animations.AnimationType;
import tray.notification.NotificationType;
import tray.notification.TrayNotification;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

public class DashboardLive implements Initializable {

    @FXML
    private BorderPane menu_bar;
    @FXML
    private JFXDrawer drawer;
    @FXML
    private VBox Vbox_btns;
    @FXML
    private AnchorPane inner_anchor;
    @FXML
    private Button btn11;
    @FXML
    private Pane btnpane;
    @FXML
    private FontAwesomeIconView FA_Icon;
    @FXML
    private BorderPane brdrpane_navclaims;
    @FXML
    private HBox hbox_active_users;
    @FXML
    private BarChart<String, Integer> items_expiry_chart;

    @FXML
    private CategoryAxis expiryTimeAxis;

    @FXML
    private NumberAxis itemsCountAxis;

    public static Button btnView;

    ArrayList<ArrayList<String>> stDataExpiry = new ArrayList<>();
    ArrayList<ArrayList<String>> stDataUsers = new ArrayList<>();
    ArrayList<Date> dates = new ArrayList<Date>();
    SimpleDateFormat fmt = new SimpleDateFormat("dd/MMM/yyyy");
    int span;
    private String stStartDate;
    private String stEndDate;
    Date tempDate;
    private DashboardLiveInfo objDashboardLiveInfo;
    MysqlCon objMysqlCon;
    Statement objStmt;
    Connection objCon;
    int intervals = 6;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnLive.setStyle("-fx-background-color: #47ab1e");
        objDashboardLiveInfo = new DashboardLiveInfo();
        objMysqlCon = new MysqlCon();
        objStmt = objMysqlCon.stmt;
        objCon = objMysqlCon.con;
        GlobalVariables.startDataSync();

        btnpane.setOnMouseEntered(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(22);
                btnpane.setMinWidth(20.0);
                btn11.setMinWidth(20.0);

            }
        });
        btnpane.setOnMouseExited(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(14);
                btnpane.setMinWidth(10.0);
                btn11.setMinWidth(10.0);
            }
        });

        inner_anchor.setLeftAnchor(inner_anchor, 260d);
        drawer.setSidePane(brdrpane_navclaims);
        drawer.open();

        expiryTimeAxis.setLabel("Time Span");
        itemsCountAxis.setLabel("Quantity");

        setDateRange();
        setBarChartDates();
        setNearByExpiryBarChart();
        XYChart.Series expiryDataSeries1 = new XYChart.Series();
        expiryDataSeries1.setName("Tablet");
        XYChart.Series expiryDataSeries2 = new XYChart.Series();
        expiryDataSeries2.setName("Capsule");
        XYChart.Series expiryDataSeries3 = new XYChart.Series();
        expiryDataSeries3.setName("Injection");
        XYChart.Series expiryDataSeries4 = new XYChart.Series();
        expiryDataSeries4.setName("Syrup");
        for(int i=stDataExpiry.size()-2; i>=0; i--)
        {
            if(stDataExpiry.get(i).get(0).equals(stDataExpiry.get(stDataExpiry.size()-1).get(0)))
            {
                expiryDataSeries1.getData().add(new XYChart.Data(stDataExpiry.get(i+1).get(0), (Integer.parseInt(stDataExpiry.get(i).get(1)))));
                expiryDataSeries2.getData().add(new XYChart.Data(stDataExpiry.get(i+1).get(0), (Integer.parseInt(stDataExpiry.get(i).get(2)))));
                expiryDataSeries3.getData().add(new XYChart.Data(stDataExpiry.get(i+1).get(0), (Integer.parseInt(stDataExpiry.get(i).get(3)))));
                expiryDataSeries4.getData().add(new XYChart.Data(stDataExpiry.get(i+1).get(0), (Integer.parseInt(stDataExpiry.get(i).get(4)))));
                break;
            }
            String stLowerBoundDate = stDataExpiry.get(i).get(0);
            Date lowerBoundDate = null;
            try {
                lowerBoundDate = fmt.parse(stLowerBoundDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Calendar calTemp = Calendar.getInstance();
            calTemp.setTime(lowerBoundDate);
            calTemp.add(Calendar.DAY_OF_MONTH, -1);
            try {
                lowerBoundDate = fmt.parse(fmt.format(calTemp.getTime()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            fmt.format(calTemp.getTime());
            stLowerBoundDate = fmt.format(calTemp.getTime());
            if(span < -1)
            {
                expiryDataSeries1.getData().add(new XYChart.Data(stDataExpiry.get(i+1).get(0)+"\n|\n"+stLowerBoundDate, (Integer.parseInt(stDataExpiry.get(i).get(1)))));
                expiryDataSeries2.getData().add(new XYChart.Data(stDataExpiry.get(i+1).get(0)+"\n|\n"+stLowerBoundDate, (Integer.parseInt(stDataExpiry.get(i).get(2)))));
                expiryDataSeries3.getData().add(new XYChart.Data(stDataExpiry.get(i+1).get(0)+"\n|\n"+stLowerBoundDate, (Integer.parseInt(stDataExpiry.get(i).get(3)))));
                expiryDataSeries4.getData().add(new XYChart.Data(stDataExpiry.get(i+1).get(0)+"\n|\n"+stLowerBoundDate, (Integer.parseInt(stDataExpiry.get(i).get(4)))));
            }
            else
            {
                expiryDataSeries1.getData().add(new XYChart.Data(stDataExpiry.get(i).get(0), (Integer.parseInt(stDataExpiry.get(i).get(1)))));
                expiryDataSeries2.getData().add(new XYChart.Data(stDataExpiry.get(i).get(0), (Integer.parseInt(stDataExpiry.get(i).get(2)))));
                expiryDataSeries3.getData().add(new XYChart.Data(stDataExpiry.get(i).get(0), (Integer.parseInt(stDataExpiry.get(i).get(3)))));
                expiryDataSeries4.getData().add(new XYChart.Data(stDataExpiry.get(i).get(0), (Integer.parseInt(stDataExpiry.get(i).get(4)))));
            }

        }
        items_expiry_chart.getData().add(expiryDataSeries1);
        items_expiry_chart.getData().add(expiryDataSeries2);
        items_expiry_chart.getData().add(expiryDataSeries3);
        items_expiry_chart.getData().add(expiryDataSeries4);

        stDataUsers = objDashboardLiveInfo.getTodayActiveUsersDetail(objStmt, objCon);
        if(stDataUsers.size() == 0)
        {
            Label lblEmpty = new Label("No Data Found");
            hbox_active_users.getChildren().add(lblEmpty);
        }
        for(int j=0; j<stDataUsers.size(); j++)
        {
            hbox_active_users.getChildren().add(createUserPane(stDataUsers.get(j).get(0), stDataUsers.get(j).get(1), stDataUsers.get(j).get(2), stDataUsers.get(j).get(3), stDataUsers.get(j).get(4), stDataUsers.get(j).get(5), stDataUsers.get(j).get(6)));
        }

//        hbox_active_users.getChildren().add(createUserPane("src/view/images/users/empty_user.jpg", "Saffi Ullah", "Admin", "5", "500", false));
    }

    public void follow() throws IOException
    {
    /*PathTransition transition;
    transition = new PathTransition();
    Polyline mpoly = new Polyline();
    mpoly.getPoints().addAll(new Double[]{
            0.0,0.0,
            0.0,0.0,
            -200.0,0.0});
    transition.setNode(btn11);
    transition.setDuration(Duration.seconds(2));
    transition.setPath(mpoly);
    transition.play();*/
        if (drawer.isOpened()) {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(-250);
            transition.setByY(0);
            transition.setCycleCount(1);
            transition.play();
            drawer.close();
            FA_Icon.setGlyphName("ANGLE_DOUBLE_RIGHT");
            FA_Icon.setGlyphSize(14);
            //inner_anchor.setRightAnchor(inner_anchor,1d);


            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(-90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(1.31017);
            scale.setToY(1);



            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();
            //
            //inner_anchor.setLeftAnchor(inner_anchor, 5d);
        } else {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(250);
            transition.setByY(0);
            transition.setCycleCount(1);
            FA_Icon.setGlyphName("ANGLE_DOUBLE_LEFT");
            FA_Icon.setGlyphSize(14);
            drawer.open();

            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(0.99947777);
            scale.setToY(1);



            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();

        }


    }

    private void setDateRange()
    {
        stStartDate = GlobalVariables.getStDate();
        Date startDate = null;
        try {
            startDate = fmt.parse(stStartDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(startDate);
        cal1.add(Calendar.DAY_OF_MONTH, 180);
        stEndDate = fmt.format(cal1.getTime());
    }

    private void setBarChartDates()
    {
        Date endDate = null;
        Date startDate = null;
        try {
            startDate = fmt.parse(stStartDate);
            endDate = fmt.parse(stEndDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long diff = endDate.getTime() - startDate.getTime();
        diff = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
        span = (int) (diff/intervals);
        span = span * -1;
        Calendar cal = Calendar.getInstance();
        cal.setTime(endDate);
        ArrayList<String> temp = new ArrayList<>();
        temp.add(fmt.format(cal.getTime()));
        temp.add("0");
        temp.add("0");
        temp.add("0");
        temp.add("0");
        stDataExpiry.add(temp);
        tempDate = endDate;
        dates.add(tempDate);
        for(int i=1; i<=intervals; i++)
        {
            temp = new ArrayList<>();
            cal.add(Calendar.DAY_OF_MONTH, span);
            try {
                tempDate = fmt.parse(fmt.format(cal.getTime()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            temp.add(fmt.format(cal.getTime()));
            temp.add("0");
            temp.add("0");
            temp.add("0");
            temp.add("0");
            stDataExpiry.add(temp);
            dates.add(tempDate);
        }
    }

    private void setNearByExpiryBarChart()
    {
        ArrayList<ArrayList<String>> nearbyExpiryData;
        nearbyExpiryData = objDashboardLiveInfo.getNearbyExpiryDetail(objStmt, objCon, stStartDate, stEndDate);

//        System.out.println(nearbyExpiryData);
        for (int i = 0; i < nearbyExpiryData.size(); i++)
        {
            try {
                tempDate = fmt.parse(nearbyExpiryData.get(i).get(2));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if((tempDate.before(dates.get(0)) && tempDate.after(dates.get(1))) || tempDate.equals(dates.get(0)))
            {
//                    System.out.println("1st: Small Date: "+dates.get(0)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(1));
                if(nearbyExpiryData.get(i).get(0).equals("Tablet"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataExpiry.get(0).get(1)) + Integer.parseInt(nearbyExpiryData.get(i).get(1)));
                    stDataExpiry.get(0).set(1, value);
                }
                else if(nearbyExpiryData.get(i).get(0).equals("Capsule"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataExpiry.get(0).get(2)) + Integer.parseInt(nearbyExpiryData.get(i).get(1)));
                    stDataExpiry.get(0).set(2, value);
                }
                else if(nearbyExpiryData.get(i).get(0).equals("Injection"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataExpiry.get(0).get(2)) + Integer.parseInt(nearbyExpiryData.get(i).get(1)));
                    stDataExpiry.get(0).set(3, value);
                }
                else
                {
                    String value = String.valueOf(Integer.parseInt(stDataExpiry.get(0).get(3)) + Integer.parseInt(nearbyExpiryData.get(i).get(1)));
                    stDataExpiry.get(0).set(4, value);
                }

            }
            else if((tempDate.before(dates.get(1)) && tempDate.after(dates.get(2))) || tempDate.equals(dates.get(1)))
            {
//                    System.out.println("2nd: Small Date: "+dates.get(1)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(2));
                if(nearbyExpiryData.get(i).get(0).equals("Tablet"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataExpiry.get(1).get(1)) + Integer.parseInt(nearbyExpiryData.get(i).get(1)));
                    stDataExpiry.get(1).set(1, value);
                }
                else if(nearbyExpiryData.get(i).get(0).equals("Capsule"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataExpiry.get(1).get(2)) + Integer.parseInt(nearbyExpiryData.get(i).get(1)));
                    stDataExpiry.get(1).set(2, value);
                }
                else if(nearbyExpiryData.get(i).get(0).equals("Injection"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataExpiry.get(1).get(2)) + Integer.parseInt(nearbyExpiryData.get(i).get(1)));
                    stDataExpiry.get(1).set(3, value);
                }
                else
                {
                    String value = String.valueOf(Integer.parseInt(stDataExpiry.get(1).get(3)) + Integer.parseInt(nearbyExpiryData.get(i).get(1)));
                    stDataExpiry.get(1).set(4, value);
                }
            }
            else if((tempDate.before(dates.get(2)) && tempDate.after(dates.get(3))) || tempDate.equals(dates.get(2)))
            {
//                    System.out.println("3rd: Small Date: "+dates.get(2)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(3));
                if(nearbyExpiryData.get(i).get(0).equals("Tablet"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataExpiry.get(2).get(1)) + Integer.parseInt(nearbyExpiryData.get(i).get(1)));
                    stDataExpiry.get(2).set(1, value);
                }
                else if(nearbyExpiryData.get(i).get(0).equals("Capsule"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataExpiry.get(2).get(2)) + Integer.parseInt(nearbyExpiryData.get(i).get(1)));
                    stDataExpiry.get(0).set(2, value);
                }
                else if(nearbyExpiryData.get(i).get(0).equals("Injection"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataExpiry.get(2).get(2)) + Integer.parseInt(nearbyExpiryData.get(i).get(1)));
                    stDataExpiry.get(2).set(3, value);
                }
                else
                {
                    String value = String.valueOf(Integer.parseInt(stDataExpiry.get(2).get(3)) + Integer.parseInt(nearbyExpiryData.get(i).get(1)));
                    stDataExpiry.get(2).set(4, value);
                }
            }
            else if((tempDate.before(dates.get(3)) && tempDate.after(dates.get(4))) || tempDate.equals(dates.get(3)))
            {
//                    System.out.println("4th: Small Date: "+dates.get(3)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(4));
                if(nearbyExpiryData.get(i).get(0).equals("Tablet"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataExpiry.get(3).get(1)) + Integer.parseInt(nearbyExpiryData.get(i).get(1)));
                    stDataExpiry.get(3).set(1, value);
                }
                else if(nearbyExpiryData.get(i).get(0).equals("Capsule"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataExpiry.get(3).get(2)) + Integer.parseInt(nearbyExpiryData.get(i).get(1)));
                    stDataExpiry.get(3).set(2, value);
                }
                else if(nearbyExpiryData.get(i).get(0).equals("Injection"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataExpiry.get(3).get(2)) + Integer.parseInt(nearbyExpiryData.get(i).get(1)));
                    stDataExpiry.get(3).set(3, value);
                }
                else
                {
                    String value = String.valueOf(Integer.parseInt(stDataExpiry.get(3).get(3)) + Integer.parseInt(nearbyExpiryData.get(i).get(1)));
                    stDataExpiry.get(3).set(4, value);
                }
            }
            else if((tempDate.before(dates.get(4)) && tempDate.after(dates.get(5))) || tempDate.equals(dates.get(4)))
            {
//                    System.out.println("4th: Small Date: "+dates.get(3)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(4));
                if(nearbyExpiryData.get(i).get(0).equals("Tablet"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataExpiry.get(4).get(1)) + Integer.parseInt(nearbyExpiryData.get(i).get(1)));
                    stDataExpiry.get(4).set(1, value);
                }
                else if(nearbyExpiryData.get(i).get(0).equals("Capsule"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataExpiry.get(4).get(2)) + Integer.parseInt(nearbyExpiryData.get(i).get(1)));
                    stDataExpiry.get(4).set(2, value);
                }
                else if(nearbyExpiryData.get(i).get(0).equals("Injection"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataExpiry.get(4).get(2)) + Integer.parseInt(nearbyExpiryData.get(i).get(1)));
                    stDataExpiry.get(4).set(3, value);
                }
                else
                {
                    String value = String.valueOf(Integer.parseInt(stDataExpiry.get(4).get(3)) + Integer.parseInt(nearbyExpiryData.get(i).get(1)));
                    stDataExpiry.get(4).set(4, value);
                }
            }
            else if((tempDate.before(dates.get(5)) && tempDate.after(dates.get(6))) || tempDate.equals(dates.get(5)))
            {
//                    System.out.println("4th: Small Date: "+dates.get(3)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(4));
                if(nearbyExpiryData.get(i).get(0).equals("Tablet"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataExpiry.get(5).get(1)) + Integer.parseInt(nearbyExpiryData.get(i).get(1)));
                    stDataExpiry.get(5).set(1, value);
                }
                else if(nearbyExpiryData.get(i).get(0).equals("Capsule"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataExpiry.get(5).get(2)) + Integer.parseInt(nearbyExpiryData.get(i).get(1)));
                    stDataExpiry.get(5).set(2, value);
                }
                else if(nearbyExpiryData.get(i).get(0).equals("Injection"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataExpiry.get(5).get(2)) + Integer.parseInt(nearbyExpiryData.get(i).get(1)));
                    stDataExpiry.get(5).set(3, value);
                }
                else
                {
                    String value = String.valueOf(Integer.parseInt(stDataExpiry.get(5).get(3)) + Integer.parseInt(nearbyExpiryData.get(i).get(1)));
                    stDataExpiry.get(5).set(4, value);
                }
            }
            else if((tempDate.before(dates.get(6)) && tempDate.after(dates.get(7))) || tempDate.equals(dates.get(6)))
            {
//                    System.out.println("4th: Small Date: "+dates.get(3)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(4));
                if(nearbyExpiryData.get(i).get(0).equals("Tablet"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataExpiry.get(6).get(1)) + Integer.parseInt(nearbyExpiryData.get(i).get(1)));
                    stDataExpiry.get(6).set(1, value);
                }
                else if(nearbyExpiryData.get(i).get(0).equals("Capsule"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataExpiry.get(6).get(2)) + Integer.parseInt(nearbyExpiryData.get(i).get(1)));
                    stDataExpiry.get(6).set(2, value);
                }
                else if(nearbyExpiryData.get(i).get(0).equals("Injection"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataExpiry.get(6).get(2)) + Integer.parseInt(nearbyExpiryData.get(i).get(1)));
                    stDataExpiry.get(6).set(3, value);
                }
                else
                {
                    String value = String.valueOf(Integer.parseInt(stDataExpiry.get(6).get(3)) + Integer.parseInt(nearbyExpiryData.get(i).get(1)));
                    stDataExpiry.get(6).set(4, value);
                }
            }
        }
//        System.out.println("   -------------------   ");
    }

    private AnchorPane createUserPane(String userId, String userName, String userType, String imageUrl, String bookings, String bookingPrice, String onlineStatus)
    {
        String css = "view/css/theme-green.css";
        AnchorPane objAnchorPane = new AnchorPane();
        objAnchorPane.setPrefWidth(200);
        objAnchorPane.setPrefHeight(280);
        objAnchorPane.getStylesheets().add(css);
        if(onlineStatus.equals("on"))
        {
            objAnchorPane.getStyleClass().add("users_pane_online");
        }
        else
        {
            objAnchorPane.getStyleClass().add("users_pane");
        }

        HBox objHbox;
        ImageView objImageView = new ImageView();
        File file = new File(imageUrl);
        boolean exists = file.exists();
        if(exists)
        {
            Image image = new Image(file.toURI().toString());
            objImageView.setImage(image);
        }
        else
        {
            file = new File("src/view/images/users/empty_user.jpg");
            Image image = new Image(file.toURI().toString());
            objImageView.setImage(image);
        }
        objImageView.setFitWidth(80);
        objImageView.setFitHeight(80);
        objHbox = new HBox(objImageView);
        objHbox.setPrefWidth(200);
        objHbox.setAlignment(Pos.CENTER);
        VBox objVbox = new VBox();
        objVbox.setPrefWidth(200);
        Label lblName = new Label(userName);
        Label lblType = new Label(userType);
        Label lblBooking = new Label("Bookings: "+bookings);
        Label lblBookingPrice = new Label("Booking Price: Rs."+String.format("%,.0f", Float.parseFloat(bookingPrice))+"/-");
        Button objView = new Button("View");
        objVbox = new VBox(objHbox, lblName, lblType, lblBooking, lblBookingPrice, objView);
        objVbox.setSpacing(10);
        objVbox.setStyle("-fx-padding: 20 0 0 0");
        objVbox.setAlignment(Pos.CENTER);
        objAnchorPane.getChildren().addAll(objVbox);
        return objAnchorPane;
    }
}
