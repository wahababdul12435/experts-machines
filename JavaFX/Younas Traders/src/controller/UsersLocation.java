package controller;

import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXDrawer;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.event.ActionEvent;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.util.StringConverter;
import model.GlobalVariables;
import model.UsersLocationInfo;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

public class UsersLocation implements Initializable {

    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private WebView web_google_map;

    @FXML
    private ScrollPane scroll_users;

    @FXML
    private HBox hbox_active_users;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    @FXML
    private BorderPane brdrpane_navclaims;

    @FXML
    private BorderPane menu_bar;

    @FXML
    private JFXDatePicker txt_date;

    @FXML
    private Label lbl_date;

    @FXML
    private ImageView img_user;

    @FXML
    private Label lbl_user_name;

    @FXML
    private Label lbl_order_booked;

    @FXML
    private Label lbl_booking_price;

    @FXML
    private Label lbl_orders_delivered;

    @FXML
    private Label lbl_delivered_price;

    @FXML
    private Label lbl_returns;

    @FXML
    private Label lbl_return_price;

    @FXML
    private Label lbl_cash_collected;

    @FXML
    private Button btn_close_summary;

    private UsersLocationInfo objUsersLocationInfo;
    ArrayList<ArrayList<String>> stDataUsers = new ArrayList<>();
    public static String dateSearchLocation = "";
    ArrayList<AnchorPane> listUserPanes;
    private String selectedImagePath;

    private int totalBookings = 0;
    private float totalBookingPrice = 0;
    private int totalDelivered = 0;
    private float totalDeliveredPrice = 0;
    private int totalReturn = 0;
    private float totalReturnPrice = 0;
    private float totalCashCollected = 0;
    public static Button btnView;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnLive.setStyle("-fx-background-color: #47ab1e");
        txt_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override
            public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }

            @Override
            public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });
        listUserPanes = new ArrayList<>();

        if (dateSearchLocation.equals(""))
        {
            dateSearchLocation = GlobalVariables.getStDate();
        }
        lbl_date.setText(dateSearchLocation);

        WebEngine objWebEngine = web_google_map.getEngine();
        objUsersLocationInfo = new UsersLocationInfo();
        objWebEngine.load("https://tuberichy.com/ProfitFlow/GPSTracking.php?softwareid="+ GlobalVariables.softwareLicId+"&date="+dateSearchLocation);

        String date = lbl_date.getText();
        stDataUsers = objUsersLocationInfo.getTodayActiveUsersDetail(GlobalVariables.objStmt, GlobalVariables.objCon, date);
        if(stDataUsers.size() == 0)
        {
            Label lblEmpty = new Label("No Data Found");
            hbox_active_users.getChildren().add(lblEmpty);
        }

        for(int j=0; j<stDataUsers.size(); j++)
        {
            totalBookings += Integer.parseInt(stDataUsers.get(j).get(4) == null? "0" : stDataUsers.get(j).get(4));
            totalDelivered += Integer.parseInt(stDataUsers.get(j).get(5) == null? "0" : stDataUsers.get(j).get(5));
            totalReturn += Integer.parseInt(stDataUsers.get(j).get(6) == null? "0" : stDataUsers.get(j).get(6));
            totalBookingPrice += Float.parseFloat(stDataUsers.get(j).get(7) == null? "0" : stDataUsers.get(j).get(7));
            totalDeliveredPrice += Float.parseFloat(stDataUsers.get(j).get(8) == null? "0" : stDataUsers.get(j).get(8));
            totalReturnPrice += Float.parseFloat(stDataUsers.get(j).get(9) == null? "0" : stDataUsers.get(j).get(9));
            totalCashCollected += Float.parseFloat(stDataUsers.get(j).get(10) == null? "0" : stDataUsers.get(j).get(10));
            listUserPanes.add(createUserPane(stDataUsers.get(j).get(0), stDataUsers.get(j).get(1), stDataUsers.get(j).get(2), stDataUsers.get(j).get(3)));
            hbox_active_users.getChildren().add(listUserPanes.get(j));
            int finalJ = j;
            listUserPanes.get(j).setOnMouseClicked(event -> {
                if(stDataUsers.get(finalJ).get(2) != null && !stDataUsers.get(finalJ).get(2).equals(""))
                {
                    selectedImagePath = stDataUsers.get(finalJ).get(2);
                    File file = new File(selectedImagePath);
                    boolean exists = file.exists();
                    if(exists)
                    {
                        Image image = new Image(file.toURI().toString());
                        img_user.setImage(image);
                    }
                }
                lbl_user_name.setText(stDataUsers.get(finalJ).get(1));
                lbl_order_booked.setText("Orders Booked: "+stDataUsers.get(finalJ).get(4));
                lbl_orders_delivered.setText("Orders Delivered: "+stDataUsers.get(finalJ).get(5));
                lbl_returns.setText("Returns: "+stDataUsers.get(finalJ).get(6));
                lbl_booking_price.setText("Booking Price: "+stDataUsers.get(finalJ).get(7));
                lbl_delivered_price.setText("Delivered Price: "+stDataUsers.get(finalJ).get(8));
                lbl_return_price.setText("Return Price: "+stDataUsers.get(finalJ).get(9));
                lbl_cash_collected.setText("Cash Collected: "+stDataUsers.get(finalJ).get(10));
                btn_close_summary.setVisible(true);
            });
        }

        selectedImagePath = "view/images/users/empty_user.jpg";
        File file = new File(selectedImagePath);
        boolean exists = file.exists();
        if(exists)
        {
            Image image = new Image(file.toURI().toString());
            img_user.setImage(image);
        }
        lbl_user_name.setText("Complete Summary");
        lbl_order_booked.setText("Orders Booked: "+totalBookings);
        lbl_orders_delivered.setText("Orders Delivered: "+totalDelivered);
        lbl_returns.setText("Returns: "+totalReturn);
        lbl_booking_price.setText("Booking Price: "+totalBookingPrice);
        lbl_delivered_price.setText("Delivered Price: "+totalDeliveredPrice);
        lbl_return_price.setText("Return Price: "+totalReturnPrice);
        lbl_cash_collected.setText("Cash Collected: "+totalCashCollected);
    }

    @FXML
    void closeSummary(ActionEvent event) {
        selectedImagePath = "view/images/users/empty_user.jpg";
        File file = new File(selectedImagePath);
        boolean exists = file.exists();
        if(exists)
        {
            Image image = new Image(file.toURI().toString());
            img_user.setImage(image);
        }
        lbl_user_name.setText("Complete Summary");
        lbl_order_booked.setText("Orders Booked: "+totalBookings);
        lbl_orders_delivered.setText("Orders Delivered: "+totalDelivered);
        lbl_returns.setText("Returns: "+totalReturn);
        lbl_booking_price.setText("Booking Price: "+totalBookingPrice);
        lbl_delivered_price.setText("Delivered Price: "+totalDeliveredPrice);
        lbl_return_price.setText("Return Price: "+totalReturnPrice);
        lbl_cash_collected.setText("Cash Collected: "+totalCashCollected);
        btn_close_summary.setVisible(false);
    }

    @FXML
    void follow(MouseEvent event) {

    }

    @FXML
    void searchLocations(ActionEvent event) {
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date selectedDate = null;
        if(txt_date.getValue() != null)
        {
            dateSearchLocation = txt_date.getValue().toString();
            try {
                selectedDate = sdf.parse(dateSearchLocation);
                dateSearchLocation = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/users_location.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    private AnchorPane createUserPane(String userId, String userName, String imageUrl, String onlineStatus)
    {
        String css = "view/css/theme-green.css";
        AnchorPane objAnchorPane = new AnchorPane();
        objAnchorPane.setPrefWidth(200);
        objAnchorPane.setPrefHeight(110);
        objAnchorPane.getStylesheets().add(css);
        if(onlineStatus.equals("on"))
        {
            objAnchorPane.getStyleClass().add("users_pane_online");
        }
        else
        {
            objAnchorPane.getStyleClass().add("users_pane");
        }

        HBox objHbox;
        ImageView objImageView = new ImageView();
        File file = new File(imageUrl);
        boolean exists = file.exists();
        if(exists)
        {
            Image image = new Image(file.toURI().toString());
            objImageView.setImage(image);
        }
        else
        {
            file = new File("src/view/images/users/empty_user.jpg");
            Image image = new Image(file.toURI().toString());
            objImageView.setImage(image);
        }
        objImageView.setFitWidth(70);
        objImageView.setFitHeight(70);
        objHbox = new HBox(objImageView);
        objHbox.setPrefWidth(200);
        objHbox.setAlignment(Pos.CENTER);
        VBox objVbox = new VBox();
        objVbox.setPrefWidth(200);
        objVbox.setPrefHeight(80);
        Label lblName = new Label(userName);
//        Label lblType = new Label(userType);
//        Label lblBooking = new Label("Bookings: "+bookings);
//        Label lblBookingPrice = new Label("Booking Price: "+bookingPrice);
//        Button objView = new Button("View");
        objVbox = new VBox(objHbox, lblName);
        objVbox.setSpacing(10);
        objVbox.setStyle("-fx-padding: 20 0 0 0");
        objVbox.setAlignment(Pos.CENTER);
        objAnchorPane.getChildren().addAll(objVbox);
        return objAnchorPane;
    }
}
