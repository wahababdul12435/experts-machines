package controller;

import com.lynden.gmapsfx.service.elevation.ElevationService;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import model.AreaWisePopUpInfo;
import model.CreatePurchaseReturnInfo;
import org.controlsfx.control.CheckListView;
import java.lang.Object;
import  javafx.scene.Node;
import  javafx.scene.Parent;
import  javafx.scene.layout.Region;
import  javafx.scene.control.Control;


import javax.jws.soap.SOAPBinding;
import javax.swing.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class AreaWisePOPUP implements Initializable {
    @FXML
    private AnchorPane inner_anchor;
    @FXML    private CheckListView checkListView;
    @FXML    private CheckListView checkListViewCompany;
    @FXML    private CheckListView checkListViewGroup;
    @FXML
    private TableView tableView;
    @FXML    private TextField txt_Area_id;
    @FXML    private TextField txt_Company;
    @FXML    private TextField txt_Group;



    public void initialize(URL url, ResourceBundle resourceBundle) {

        ArrayList<String> selectdAreaNames=new ArrayList<>();
        ArrayList<String> selectdCompanyNames=new ArrayList<>();
        ArrayList<String> selectd_Group_Names=new ArrayList<>();

        AreaWisePopUpInfo OBJCTareawisepopinfo = new AreaWisePopUpInfo();
        checkListView.setItems(OBJCTareawisepopinfo.arealist());
        ObservableList<String> mylist = OBJCTareawisepopinfo.areaname();
        checkListViewCompany.setItems(OBJCTareawisepopinfo.cmpnylist());

        checkListViewGroup.setItems(OBJCTareawisepopinfo.grouplist());

        checkListView.getCheckModel().getCheckedItems().addListener(new ListChangeListener<String>() {
        public void onChanged(ListChangeListener.Change<? extends String> c) {
            //System.out.println(checkListView.getCheckModel().getCheckedItems());
            ObservableList<Integer> mynewIndex = FXCollections.observableArrayList();
            mynewIndex.removeAll();
            mynewIndex = checkListView.getCheckModel().getCheckedIndices();
            int getttt = mynewIndex.get(0);

            AreaWisePopUpInfo popUpInfoOBJ = new AreaWisePopUpInfo();
            ObservableList<String> areasNamesList = popUpInfoOBJ.areaname();
            String selectedOnlyName = areasNamesList.get(getttt);

            if(selectedOnlyName.equals("Check All"))
            {
                for (int t = 1;t<checkListView.getItems().size();t++)
                checkListView.getCheckModel().check(t);
            }
            else
            {
                selectdAreaNames.add(selectedOnlyName);
            }


        }
    });

        checkListViewGroup.getCheckModel().getCheckedItems().addListener(new ListChangeListener<String>() {
            public void onChanged(ListChangeListener.Change<? extends String> c) {
                //System.out.println(checkListView.getCheckModel().getCheckedItems());
                String selectdArea = checkListViewGroup.getCheckModel().getCheckedItems().toString();

                ObservableList<Integer> mynewIndex = FXCollections.observableArrayList();
                mynewIndex.removeAll();
                mynewIndex = checkListViewGroup.getCheckModel().getCheckedIndices();
                int getttt = mynewIndex.get(0);

                AreaWisePopUpInfo popUpInfoOBJ = new AreaWisePopUpInfo();
                ObservableList<String> areasNamesList = popUpInfoOBJ.areaname();
                String selectedOnlyName = areasNamesList.get(getttt);

                if(selectedOnlyName.equals("Check All"))
                {
                    for (int t = 1;t<checkListViewGroup.getItems().size();t++)
                        checkListViewGroup.getCheckModel().check(t);
                }
                else
                {
                    selectd_Group_Names.add(selectedOnlyName);
                }


            }
        });


        checkListViewCompany.getCheckModel().getCheckedItems().addListener(new ListChangeListener<String>() {
            public void onChanged(ListChangeListener.Change<? extends String> c) {
                //System.out.println(checkListViewCompany.getCheckModel().getCheckedItems());
                String selectdcompy = checkListViewCompany.getCheckModel().getCheckedItems().toString();

                ObservableList<Integer> mynewIndex = FXCollections.observableArrayList();
                mynewIndex.removeAll();
                        mynewIndex = checkListViewCompany.getCheckModel().getCheckedIndices();
                int getttt = mynewIndex.get(0);

                AreaWisePopUpInfo popUpInfoOBJect = new AreaWisePopUpInfo();
                ObservableList<String> compniesNamesList = popUpInfoOBJect.cmpnyName();
                String selectedOnlyName = compniesNamesList.get(getttt);

                ObservableList<String> multiple_groupsList = FXCollections.observableArrayList();
                String[] arraychar={};
                for (int i = 0; i < selectdcompy.toCharArray().length; i++) {
                    char y = selectdcompy.charAt(i);
                    if(y==',')
                    {
                        arraychar = selectdcompy.split(",");
                    }
                }
                if(arraychar.length==0) {
                    AreaWisePopUpInfo popUpInfoOBJ = new AreaWisePopUpInfo();
                    int compTableID = popUpInfoOBJ.compTableID(selectedOnlyName);

                    checkListViewGroup.setItems(OBJCTareawisepopinfo.selectedComp_grouplist(compTableID));
                }
                else
                {
                        selectdCompanyNames.add(selectedOnlyName);
                        AreaWisePopUpInfo popUpInfoOBJ = new AreaWisePopUpInfo();
                        int compTableID = popUpInfoOBJ.compTableID(selectedOnlyName);
                        multiple_groupsList.addAll((OBJCTareawisepopinfo.selectedComp_grouplist(compTableID)));
                        checkListViewGroup.setItems(multiple_groupsList);
                }
            }
        });
 //inner_anchor.getChildren().add(checkListView);
    }

    public void onAreaChange()
    {
        checkListView.getItems().clear();
        String areaTxt = txt_Area_id.getText();
        AreaWisePopUpInfo OBJCTareawisepopinfo = new AreaWisePopUpInfo();
        checkListView.setItems(OBJCTareawisepopinfo.searcharealist(areaTxt));

    }

    public void searchreport()
    {
        
    }

    public void onCompanyChange()
    {
         checkListViewCompany.getItems().clear();
        String areaTxt = txt_Company.getText();
        AreaWisePopUpInfo OBJCTareawisepopinfo = new AreaWisePopUpInfo();
        checkListViewCompany.setItems(OBJCTareawisepopinfo.searchcomplist(areaTxt));

    }

    public void onGroupChange()
    {
        checkListViewGroup.getItems().clear();
        String areaTxt = txt_Group.getText();
        AreaWisePopUpInfo OBJCTareawisepopinfo = new AreaWisePopUpInfo();
        checkListViewGroup.setItems(OBJCTareawisepopinfo.searchgrouolist(areaTxt));

    }
}
