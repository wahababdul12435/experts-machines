package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.*;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.util.Duration;
import model.*;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class ViewDealers implements Initializable {
    @FXML
    private TableView<ViewDealerInfo> table_viewdealer;

    @FXML
    private TableColumn<ViewDealerInfo, String> sr_no;

    @FXML
    private TableColumn<ViewDealerInfo, String> dealer_id;

    @FXML
    private TableColumn<ViewDealerInfo, String> dealer_name;

    @FXML
    private TableColumn<ViewDealerInfo, String> dealer_contact;

    @FXML
    private TableColumn<ViewDealerInfo, String> dealer_address;

    @FXML
    private TableColumn<ViewDealerInfo, String> dealer_type;

    @FXML
    private TableColumn<ViewDealerInfo, String> dealer_status;

    @FXML
    private TableColumn<ViewDealerInfo, String> operations;

    @FXML    private AnchorPane inner_anchor;
    @FXML    private JFXDrawer drawer;
    @FXML    private BorderPane nav_setup;
    @FXML    private BorderPane menu_bar1;
    @FXML    private Pane btnpane;
    @FXML    private Button btn11;
    @FXML    private FontAwesomeIconView FA_Icon;

    @FXML
    private AnchorPane filter_pane;

    @FXML
    private HBox filter_hbox;

    @FXML
    private JFXTextField txt_dealer_id;

    @FXML
    private JFXTextField txt_dealer_name;

    @FXML
    private JFXTextField txt_dealer_contact;

    @FXML
    private JFXTextField txt_dealer_cnic;

    @FXML
    private JFXComboBox<String> txt_dealer_city;

    @FXML
    private JFXComboBox<String> txt_dealer_area;

    @FXML
    private JFXComboBox<String> txt_dealer_type;

    @FXML
    private JFXComboBox<String> txt_dealer_status;

    @FXML
    private StackPane stackPane;

    @FXML
    private HBox summary_hbox;

    @FXML
    private Label lbl_total;

    @FXML
    private Label lbl_active;

    @FXML
    private Label lbl_inactive;

    @FXML
    private JFXButton btn_add;
    @FXML
    private JFXButton btn_export;

    public ObservableList<ViewDealerInfo> dealerDetails;
    PendingOrders objPendingOrders;
    MysqlCon objMysqlCon;
    Statement objStmt;
    Connection objCon;
    int unSeenOrders;
    private int summaryTotal;
    private int summaryActive;
    private int summaryInActive;

    public static String txtDealerId = "";
    public static String txtDealerName = "";
    public static String txtDealerContact = "";
    public static String txtDealerCnic = "";
    public static String txtDealerLicNum = "";
    public static String txtCityId = "All";
    public static String txtCityName = "";
    public static String txtAreaId = "All";
    public static String txtAreaName = "";
    public static String txtDealerType = "";
    public static String txtDealerStatus = "";
    public static boolean filter;

    private String selectedCityId;
    private String selectedAreaId;

    private ArrayList<ArrayList<String>> citiesData = new ArrayList<>();
    private ArrayList<ArrayList<String>> areasData = new ArrayList<>();
    private ArrayList<String> chosenAreaIds = new ArrayList<>();

    public static Button btnView;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnSetup.setStyle("-fx-background-color: #47ab1e");
        unSeenOrders = 0;
        objMysqlCon = new MysqlCon();
        objStmt = objMysqlCon.stmt;
        objCon = objMysqlCon.con;
        ViewDealerInfo.stackPane = stackPane;

        if(!GlobalVariables.userType.equals("Admin"))
        {
            if(!GlobalVariables.rightCreate)
            {
                btn_add.setVisible(false);
                AnchorPane.setRightAnchor(btn_export, 15.0);
            }
        }

        UpdateDealerInfo objUpdateDealerInfo = new UpdateDealerInfo();
        citiesData = objUpdateDealerInfo.getCitiesData();
        areasData = objUpdateDealerInfo.getCityAreasData();

        ArrayList<String> chosenCityNames = getArrayColumn(citiesData, 1);
        txt_dealer_city.getItems().addAll(chosenCityNames);

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        dealer_id.setCellValueFactory(new PropertyValueFactory<>("dealerId"));
        dealer_name.setCellValueFactory(new PropertyValueFactory<>("dealerName"));
        dealer_contact.setCellValueFactory(new PropertyValueFactory<>("dealerContact"));
        dealer_address.setCellValueFactory(new PropertyValueFactory<>("dealerAddress"));
        dealer_type.setCellValueFactory(new PropertyValueFactory<>("dealerType"));
        dealer_status.setCellValueFactory(new PropertyValueFactory<>("dealerStatus"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_viewdealer.setItems(parseUserList());
        lbl_total.setText("Dealers\n"+String.format("%,.0f", (float)summaryTotal));
        lbl_active.setText("Active\n"+String.format("%,.0f", (float)summaryActive));
        lbl_inactive.setText("In Active\n"+String.format("%,.0f", (float)summaryInActive));
    }

    private ObservableList<ViewDealerInfo> parseUserList(){
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        ViewDealerInfo objViewDealerInfo = new ViewDealerInfo();
        dealerDetails = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> dealersData;
        if(filter)
        {
            dealersData  = objViewDealerInfo.getDealersSearch(objStmt, objCon, txtDealerId, txtDealerName, txtDealerContact, txtDealerCnic, txtDealerLicNum, txtCityId, txtAreaId, txtDealerType, txtDealerStatus);
            txt_dealer_id.setText(txtDealerId);
            txt_dealer_name.setText(txtDealerName);
            txt_dealer_contact.setText(txtDealerContact);
            txt_dealer_cnic.setText(txtDealerCnic);
            txt_dealer_city.setValue(txtCityName);
            int index = txt_dealer_city.getSelectionModel().getSelectedIndex();
            if(index >=1)
            {
                selectedCityId = citiesData.get(index-1).get(0);
                txtCityId = selectedCityId;
                txt_dealer_area.getItems().clear();
                ArrayList<String> chosenGroupNames = getCityAreas(areasData, selectedCityId);
                if(chosenGroupNames.size() > 0)
                {
                    txt_dealer_area.getItems().addAll("All");
                    txt_dealer_area.getItems().addAll(chosenGroupNames);
                    txt_dealer_area.setValue(txtAreaName);
                }
            }
            txt_dealer_type.setValue(txtDealerType);
            txt_dealer_status.setValue(txtDealerStatus);
            filter = false;
            filter_pane.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
        }
        else
        {
            dealersData = objViewDealerInfo.getDealersDetail(objStmt, objCon);
        }
        for (int i = 0; i < dealersData.size(); i++)
        {
            summaryTotal++;
            if(dealersData.get(i).get(16).equals("Active"))
            {
                summaryActive++;
            }
            else if(dealersData.get(i).get(16).equals("In Active"))
            {
                summaryInActive++;
            }
            dealerDetails.add(new ViewDealerInfo(String.valueOf(i+1), dealersData.get(i).get(0), ((dealersData.get(i).get(1) == null) ? "N/A" : dealersData.get(i).get(1)), dealersData.get(i).get(2), ((dealersData.get(i).get(3) == null) ? "N/A" : dealersData.get(i).get(3)), ((dealersData.get(i).get(4) == null) ? "N/A" : dealersData.get(i).get(4)), ((dealersData.get(i).get(5) == null) ? "N/A" : dealersData.get(i).get(5)), dealersData.get(i).get(6), dealersData.get(i).get(7), dealersData.get(i).get(8), dealersData.get(i).get(9), dealersData.get(i).get(10), dealersData.get(i).get(11), dealersData.get(i).get(12), dealersData.get(i).get(13), dealersData.get(i).get(14), dealersData.get(i).get(15), dealersData.get(i).get(16)));
        }
        return dealerDetails;
    }

    @FXML
    void btnSearch(ActionEvent event) {
        txtDealerId = txt_dealer_id.getText();
        txtDealerName = txt_dealer_name.getText();
        txtDealerContact = txt_dealer_contact.getText();
        txtDealerCnic = txt_dealer_cnic.getText();
        txtCityName = txt_dealer_city.getValue();
        txtAreaName = txt_dealer_area.getValue();
        txtDealerType = txt_dealer_type.getValue();
        if(txtDealerType == null)
        {
            txtDealerType = "All";
        }
        txtDealerStatus = txt_dealer_status.getValue();
        if(txtDealerStatus == null)
        {
            txtDealerStatus = "All";
        }
        filter = true;

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/view_dealers.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void btnReset(ActionEvent event) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/view_dealers.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void exportExcel(ActionEvent event) throws IOException {
        Workbook workbook = new HSSFWorkbook();
        Sheet spreadsheet = workbook.createSheet("data");
        Row row = spreadsheet.createRow(0);

        for (int j = 0; j < table_viewdealer.getColumns().size()-1; j++) {
            row.createCell(j).setCellValue(table_viewdealer.getColumns().get(j).getText());
        }

        for (int i = 0; i < table_viewdealer.getItems().size(); i++) {
            row = spreadsheet.createRow(i + 1);
            for (int j = 0; j < table_viewdealer.getColumns().size()-1; j++) {
                if(table_viewdealer.getColumns().get(j).getCellData(i) != null) {
                    row.createCell(j).setCellValue(table_viewdealer.getColumns().get(j).getCellData(i).toString());
                }
                else {
                    row.createCell(j).setCellValue("");
                }
            }
        }

        FileOutputStream fileOut = new FileOutputStream("data/View_Dealers.xls");
        workbook.write(fileOut);
        fileOut.close();
    }

    public void follow() throws IOException
    {
        if (drawer.isOpened()) {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(-250);
            transition.setByY(0);
            transition.setCycleCount(1);
            transition.play();
            drawer.close();
            FA_Icon.setGlyphName("ANGLE_DOUBLE_RIGHT");
            FA_Icon.setGlyphSize(14);
            //inner_anchor.setRightAnchor(inner_anchor,1d);
            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(-90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(1.1000);
            scale.setToY(1);

            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();

        }
        else {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(250);
            transition.setByY(0);
            transition.setCycleCount(1);
            FA_Icon.setGlyphName("ANGLE_DOUBLE_LEFT");
            FA_Icon.setGlyphSize(14);
            drawer.open();

            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(0.99947777);
            scale.setToY(1);

            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();

        }
    }

    @FXML
    void cityChange(ActionEvent event) {
        int index = txt_dealer_city.getSelectionModel().getSelectedIndex();
        if(index >=1)
        {
            selectedCityId = citiesData.get(index-1).get(0);
            txtCityId = selectedCityId;
            txt_dealer_area.getItems().clear();
            ArrayList<String> chosenGroupNames = getCityAreas(areasData, selectedCityId);
            if(chosenGroupNames.size() > 0)
            {
                txt_dealer_area.getItems().addAll("All");
                txt_dealer_area.getItems().addAll(chosenGroupNames);
                txt_dealer_area.setValue("All");
            }
            txtAreaId = "All";
        }
        else
        {
            txtCityId = "All";
            txtAreaId = "All";
            txt_dealer_area.getItems().clear();
            txt_dealer_area.getItems().addAll("All");
            txt_dealer_area.setValue("All");
        }

    }

    @FXML
    void areaChange(ActionEvent event) {
        int index = txt_dealer_area.getSelectionModel().getSelectedIndex();
        if(index >= 1)
        {
            selectedAreaId = chosenAreaIds.get(index-1);
            txtAreaId = selectedAreaId;
        }
        else
        {
            txtAreaId = "All";
        }
    }

    public ArrayList<String> getArrayColumn(ArrayList<ArrayList<String>> citiesData, int columnIndex)
    {
        ArrayList<String> columnList = new ArrayList<>();
        for(ArrayList<String> row: citiesData) {
            columnList.add(row.get(columnIndex));
        }
        return columnList;
    }

    public ArrayList<String> getCityAreas(ArrayList<ArrayList<String>> areasData, String companyId)
    {
        ArrayList<String> columnList = new ArrayList<>();
        chosenAreaIds = new ArrayList<>();
        for(ArrayList<String> row: areasData) {
            if(row.get(2).equals(companyId))
            {
                chosenAreaIds.add(row.get(0));
                columnList.add(row.get(1));
            }
        }
        return columnList;
    }

    @FXML
    void addDealer() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/enter_dealer.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
}
