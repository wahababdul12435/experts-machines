package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import model.FetchServerData;
import model.FetchServerThread;
import model.GlobalVariables;
import model.LoginInfo;

import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class Login implements Initializable {

    @FXML
    private Label label_update;

    @FXML
    private JFXButton btn_login;

    @FXML
    private JFXTextField txt_username;

    @FXML
    private JFXPasswordField txt_password;

    @FXML
    private Label lbl_incorrect;

    private LoginInfo objLoginInfo = new LoginInfo();


    public void initialize(URL url, ResourceBundle resourceBundle) {
        txt_username.requestFocus();
    }

    public void enterpress_username(KeyEvent event) throws IOException, ParseException {
        if (event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB) {
            txt_password.requestFocus();
        }
    }
    public void enterpress_password(KeyEvent event) throws IOException, ParseException {
        if (event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB) {
            btn_login.requestFocus();
        }
    }
    @FXML
    void btnLogin(ActionEvent event) throws IOException {
        String userName = txt_username.getText();
        String password = txt_password.getText();
        ArrayList<String> userDetail = objLoginInfo.checkLogin(userName, password);
        if(userDetail != null && userDetail.size() > 0)
        {
            FetchServerThread objFetchThread = new FetchServerThread();
            objFetchThread.setDaemon(true);
            objFetchThread.start();
            GlobalVariables.userId = userDetail.get(0);
            GlobalVariables.userImage = userDetail.get(1);
            GlobalVariables.currentUserPassword = userDetail.get(2);
            Parent root = FXMLLoader.load(getClass().getResource("/view/dashboard_live.fxml"));
            GlobalVariables.baseScene.setRoot(root);
            GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
        }
        else
        {
            lbl_incorrect.setVisible(true);
        }
    }

}
