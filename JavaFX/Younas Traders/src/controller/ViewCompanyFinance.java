package controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.util.StringConverter;
import model.GlobalVariables;
import model.MysqlCon;
import model.ViewCompanyFinanceInfo;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

public class ViewCompanyFinance implements Initializable {

    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private TableView<ViewCompanyFinanceInfo> table_companyfinancelog;

    @FXML
    private TableColumn<ViewCompanyFinanceInfo, String> sr_no;

    @FXML
    private TableColumn<ViewCompanyFinanceInfo, String> payment_date;

    @FXML
    private TableColumn<ViewCompanyFinanceInfo, String> payment_day;

    @FXML
    private TableColumn<ViewCompanyFinanceInfo, String> company_name;

    @FXML
    private TableColumn<ViewCompanyFinanceInfo, String> company_contact;

    @FXML
    private TableColumn<ViewCompanyFinanceInfo, String> cash_sent;

    @FXML
    private TableColumn<ViewCompanyFinanceInfo, String> cash_received;

    @FXML
    private TableColumn<ViewCompanyFinanceInfo, String> cash_waived_off;

    @FXML
    private TableColumn<ViewCompanyFinanceInfo, String> cash_pending;

    @FXML
    private TableColumn<ViewCompanyFinanceInfo, String> user_name;

    @FXML
    private TableColumn<ViewCompanyFinanceInfo, String> operations;

    @FXML
    private AnchorPane filter_pane;

    @FXML
    private HBox filter_hbox1;

    @FXML
    private JFXTextField txt_company_id;

    @FXML
    private JFXTextField txt_company_name;

    @FXML
    private JFXDatePicker txt_from_date;

    @FXML
    private JFXDatePicker txt_to_date;

    @FXML
    private JFXComboBox<String> txt_day;

    @FXML
    private JFXComboBox<String> txt_cash_type;

    @FXML
    private HBox summary_hbox1;

    @FXML
    private Label lbl_cash_sent;

    @FXML
    private Label lbl_cash_received;

    @FXML
    private Label lbl_cash_waived_off;

    @FXML
    private Label lbl_cash_pending;

    @FXML
    private StackPane stackPane;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    @FXML
    private BorderPane brdrpane_navfinances;

    @FXML
    private BorderPane menu_bar;

    private ObservableList<ViewCompanyFinanceInfo> paymentDetails;
    private ViewCompanyFinanceInfo objViewCompanyFinanceInfo;
    ArrayList<ArrayList<String>> paymentData;

    float totalSent = 0;
    float totalReceived = 0;
    float totalWaivedOff = 0;
    float totalPending = 0;

    public static String txtCompanyId = "";
    public static String txtCompanyName = "";
    public static String txtFromDate = "";
    public static String txtToDate = "";
    public static String txtDay = "All";
    public static String txtCashType = "All";
    public static boolean filter;

    private String summary = "";

    MysqlCon objMysqlCon1;
    Statement objStmt1;
    Connection objCon;

    public static Button btnView;
    SimpleDateFormat fmt = new SimpleDateFormat("dd/MMM/yyyy");

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnFinance.setStyle("-fx-background-color: #47ab1e");
        objMysqlCon1 = new MysqlCon();
        objStmt1 = objMysqlCon1.stmt;
        objCon = objMysqlCon1.con;
        objViewCompanyFinanceInfo = new ViewCompanyFinanceInfo();
        ViewCompanyFinanceInfo.stackPane = stackPane;

        paymentData = new ArrayList<>();

        txt_from_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        txt_to_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        payment_date.setCellValueFactory(new PropertyValueFactory<>("paymentDate"));
        payment_day.setCellValueFactory(new PropertyValueFactory<>("paymentDay"));
        company_name.setCellValueFactory(new PropertyValueFactory<>("companyName"));
        company_contact.setCellValueFactory(new PropertyValueFactory<>("companyContact"));
        cash_sent.setCellValueFactory(new PropertyValueFactory<>("cashSent"));
        cash_received.setCellValueFactory(new PropertyValueFactory<>("cashReceived"));
        cash_waived_off.setCellValueFactory(new PropertyValueFactory<>("cashWaivedOff"));
        cash_pending.setCellValueFactory(new PropertyValueFactory<>("cashPending"));
        user_name.setCellValueFactory(new PropertyValueFactory<>("userName"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_companyfinancelog.setItems(parseUserList());
        lbl_cash_sent.setText("Sent\n"+"Rs."+String.format("%,.0f", totalSent)+"/-");
        lbl_cash_received.setText("Received\n"+"Rs."+String.format("%,.0f", totalReceived)+"/-");
        lbl_cash_waived_off.setText("Waived Off\n"+"Rs."+String.format("%,.0f", totalWaivedOff)+"/-");
        lbl_cash_pending.setText("Pending\n"+"Rs."+String.format("%,.0f", totalPending)+"/-");
    }

    private ObservableList<ViewCompanyFinanceInfo> parseUserList() {

        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        paymentDetails = FXCollections.observableArrayList();

        if (filter) {
            paymentData = objViewCompanyFinanceInfo.getCompanyFinanceInfoSearch(objStmt1, objCon, txtCompanyId, txtCompanyName, txtFromDate, txtToDate, txtDay, txtCashType);
            txt_company_id.setText(txtCompanyId);
            txt_company_name.setText(txtCompanyName);
            if (txtFromDate != null && !txtFromDate.equals("")) {
                txt_from_date.setValue(GlobalVariables.LOCAL_DATE(txtFromDate));
            }
            if (txtToDate != null && !txtToDate.equals("")) {
                txt_to_date.setValue(GlobalVariables.LOCAL_DATE(txtToDate));
            }
            txt_day.setValue(txtDay);
            txt_cash_type.setValue(txtCashType);
            filter = false;
            filter_pane.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
            txtFromDate = "";
            txtToDate = "";
            txtDay = "All";
            txtCashType = "All";
        } else if(summary.equals("")){
            paymentData = objViewCompanyFinanceInfo.getCompanyFinanceInfo(objStmt1, objCon);
        }

        if (txtToDate != null && !txtToDate.equals("")) {
            txt_to_date.setValue(GlobalVariables.LOCAL_DATE(txtToDate));
            cash_pending.setText("Cash Pending\non\n"+txtToDate);
        }
        else
        {
            txtToDate = GlobalVariables.getStDate();
            cash_pending.setText("Cash Pending\non\n"+txtToDate);
        }
        int num = 1;
        for (int i = 0; i < paymentData.size(); i++)
        {
            totalSent += (paymentData.get(i).get(6) != null && !paymentData.get(i).get(6).equals("-")) ? Float.parseFloat(paymentData.get(i).get(6)) : 0;
            totalReceived += (paymentData.get(i).get(7) != null && !paymentData.get(i).get(7).equals("-")) ? Float.parseFloat(paymentData.get(i).get(7)) : 0;
            totalWaivedOff += (paymentData.get(i).get(8) != null && !paymentData.get(i).get(8).equals("-")) ? Float.parseFloat(paymentData.get(i).get(8)) : 0;
            totalPending += (paymentData.get(i).get(9) != null && !paymentData.get(i).get(9).equals("-")) ? Float.parseFloat(paymentData.get(i).get(9)) : 0;
            if(summary.equals("Sent") && !paymentData.get(i).get(6).equals("-"))
            {
                paymentDetails.add(new ViewCompanyFinanceInfo(String.valueOf(num), ((paymentData.get(i).get(0) == null || paymentData.get(i).get(0).equals("")) ? "N/A" : paymentData.get(i).get(0)), paymentData.get(i).get(1), paymentData.get(i).get(2), paymentData.get(i).get(3), paymentData.get(i).get(4), paymentData.get(i).get(5), paymentData.get(i).get(6), paymentData.get(i).get(7), paymentData.get(i).get(8), paymentData.get(i).get(9), paymentData.get(i).get(10), paymentData.get(i).get(11)));
                num++;
            }
            else if(summary.equals("Received") && !paymentData.get(i).get(7).equals("-"))
            {
                paymentDetails.add(new ViewCompanyFinanceInfo(String.valueOf(num), ((paymentData.get(i).get(0) == null || paymentData.get(i).get(0).equals("")) ? "N/A" : paymentData.get(i).get(0)), paymentData.get(i).get(1), paymentData.get(i).get(2), paymentData.get(i).get(3), paymentData.get(i).get(4), paymentData.get(i).get(5), paymentData.get(i).get(6), paymentData.get(i).get(7), paymentData.get(i).get(8), paymentData.get(i).get(9), paymentData.get(i).get(10), paymentData.get(i).get(11)));
                num++;
            }
            else if(summary.equals("Waived Off") && !paymentData.get(i).get(8).equals("-"))
            {
                paymentDetails.add(new ViewCompanyFinanceInfo(String.valueOf(num), ((paymentData.get(i).get(0) == null || paymentData.get(i).get(0).equals("")) ? "N/A" : paymentData.get(i).get(0)), paymentData.get(i).get(1), paymentData.get(i).get(2), paymentData.get(i).get(3), paymentData.get(i).get(4), paymentData.get(i).get(5), paymentData.get(i).get(6), paymentData.get(i).get(7), paymentData.get(i).get(8), paymentData.get(i).get(9), paymentData.get(i).get(10), paymentData.get(i).get(11)));
                num++;
            }
            else if(summary.equals("Pending") && !paymentData.get(i).get(9).equals("0"))
            {
                paymentDetails.add(new ViewCompanyFinanceInfo(String.valueOf(num), ((paymentData.get(i).get(0) == null || paymentData.get(i).get(0).equals("")) ? "N/A" : paymentData.get(i).get(0)), paymentData.get(i).get(1), paymentData.get(i).get(2), paymentData.get(i).get(3), paymentData.get(i).get(4), paymentData.get(i).get(5), paymentData.get(i).get(6), paymentData.get(i).get(7), paymentData.get(i).get(8), paymentData.get(i).get(9), paymentData.get(i).get(10), paymentData.get(i).get(11)));
                num++;
            }
            else if(summary.equals(""))
            {
                paymentDetails.add(new ViewCompanyFinanceInfo(String.valueOf(i+1), ((paymentData.get(i).get(0) == null || paymentData.get(i).get(0).equals("")) ? "N/A" : paymentData.get(i).get(0)), paymentData.get(i).get(1), paymentData.get(i).get(2), paymentData.get(i).get(3), paymentData.get(i).get(4), paymentData.get(i).get(5), paymentData.get(i).get(6), paymentData.get(i).get(7), paymentData.get(i).get(8), paymentData.get(i).get(9), paymentData.get(i).get(10), paymentData.get(i).get(11)));
            }
        }
        return paymentDetails;
    }

    @FXML
    void btnReset(ActionEvent event) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/view_company_finance.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void btnSearch(ActionEvent event) {
        txtCompanyId = txt_company_id.getText();
        txtCompanyName = txt_company_name.getText();
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date selectedDate = null;
        if(txt_from_date.getValue() != null)
        {
            txtFromDate = txt_from_date.getValue().toString();
            try {
                selectedDate = sdf.parse(txtFromDate);
                txtFromDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if(txt_to_date.getValue() != null)
        {
            txtToDate = txt_to_date.getValue().toString();
            try {
                selectedDate = sdf.parse(txtToDate);
                txtToDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        txtDay = txt_day.getValue();
        if(txtDay == null)
        {
            txtDay = "All";
        }
        txtCashType = txt_cash_type.getValue();
        if(txtCashType == null)
        {
            txtCashType = "All";
        }
        filter = true;

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/view_company_finance.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }


    @FXML
    void addCash(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/add_company_finance.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void follow(MouseEvent event) {

    }

    @FXML
    void showSent(MouseEvent event) {
        summary = "Sent";
        table_companyfinancelog.setItems(parseUserList());
    }

    @FXML
    void showReceived(MouseEvent event) {
        summary = "Received";
        table_companyfinancelog.setItems(parseUserList());
    }

    @FXML
    void showWaiveOff(MouseEvent event) {
        summary = "Waived Off";
        table_companyfinancelog.setItems(parseUserList());
    }

    @FXML
    void showPending(MouseEvent event) {
        summary = "Pending";
        table_companyfinancelog.setItems(parseUserList());
    }

}
