package controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXDrawer;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.util.StringConverter;
import model.*;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class PurchaseReport implements Initializable {

    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private AnchorPane filter_pane;

    @FXML
    private JFXDatePicker txt_from_date;

    @FXML
    private JFXDatePicker txt_to_date;

    @FXML
    private JFXComboBox<String> txt_day;

    @FXML
    private HBox summary_hbox1;

    @FXML
    private Label lbl_purchase;

    @FXML
    private Label lbl_purchase_price;

    @FXML
    private Label lbl_return_qty;

    @FXML
    private Label lbl_return_price;

    @FXML
    private HBox summary_hbox2;

    @FXML
    private Label lbl_cash_sent;

    @FXML
    private Label lbl_cash_return;

    @FXML
    private Label lbl_cash_waive_off;

    @FXML
    private TableView<PurchaseReportInfo> table_purchasereportlog;

    @FXML
    private TableColumn<PurchaseReportInfo, String> sr_no;

    @FXML
    private TableColumn<PurchaseReportInfo, String> date;

    @FXML
    private TableColumn<PurchaseReportInfo, String> day;

    @FXML
    private TableColumn<PurchaseReportInfo, String> purchase_invoice;

    @FXML
    private TableColumn<PurchaseReportInfo, String> purchase_price;

    @FXML
    private TableColumn<PurchaseReportInfo, String> quantity_returned;

    @FXML
    private TableColumn<PurchaseReportInfo, String> return_price;

    @FXML
    private TableColumn<PurchaseReportInfo, String> cash_collection;

    @FXML
    private TableColumn<PurchaseReportInfo, String> cash_return;

    @FXML
    private TableColumn<PurchaseReportInfo, String> cash_waive_off;

    @FXML
    private TableColumn<PurchaseReportInfo, String> operations;

    @FXML
    private StackPane stackPane;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    @FXML
    private BorderPane brdrpane_navfinances;

    @FXML
    private BorderPane menu_bar;

    public ArrayList<PurchaseReportInfo> summaryData;
    private String selectedImagePath;
    private ArrayList<String> companyInfo;
    private ObservableList<PurchaseReportInfo> dealerDetails;
    private PurchaseReportInfo objPurchaseReportInfo;
    ArrayList<ArrayList<String>> purchaseReportData;
    private float purchase = 0;
    private float purchasePrice = 0;
    private float returnQty = 0;
    private float returnPrice = 0;
    private float cashSent = 0;
    private float cashReturned = 0;
    private float cashWaivedOff = 0;

    public static String txtFromDate = "";
    public static String txtToDate = "";
    public static String txtDay = "All";
    public static boolean filter;

    MysqlCon objMysqlCon1;
    MysqlCon objMysqlCon2;
    MysqlCon objMysqlCon3;
    Statement objStmt1;
    Statement objStmt2;
    Statement objStmt3;
    Connection objCon;

    public static MenuButton btnView;
    SimpleDateFormat fmt = new SimpleDateFormat("dd/MMM/yyyy");

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        objMysqlCon1 = new MysqlCon();
        objMysqlCon2 = new MysqlCon();
        objMysqlCon3 = new MysqlCon();
        objStmt1 = objMysqlCon1.stmt;
        objStmt2 = objMysqlCon2.stmt;
        objStmt3 = objMysqlCon3.stmt;
        objCon = objMysqlCon1.con;
        TopHeader.btnReport.setStyle("-fx-background-color: #47ab1e");

        objPurchaseReportInfo = new PurchaseReportInfo();
        purchaseReportData = new ArrayList<>();

        txt_from_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        txt_to_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        summaryData = new ArrayList<>();
        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        date.setCellValueFactory(new PropertyValueFactory<>("date"));
        day.setCellValueFactory(new PropertyValueFactory<>("day"));
        purchase_invoice.setCellValueFactory(new PropertyValueFactory<>("purchaseInvoice"));
        purchase_price.setCellValueFactory(new PropertyValueFactory<>("purchasePrice"));
        quantity_returned.setCellValueFactory(new PropertyValueFactory<>("quantityReturned"));
        return_price.setCellValueFactory(new PropertyValueFactory<>("returnPrice"));
        cash_collection.setCellValueFactory(new PropertyValueFactory<>("cashCollection"));
        cash_return.setCellValueFactory(new PropertyValueFactory<>("cashReturn"));
        cash_waive_off.setCellValueFactory(new PropertyValueFactory<>("cashWaiveOff"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_purchasereportlog.setItems(parseUserList());
        lbl_purchase.setText("Purchase Invoice\n"+String.format("%,.0f", purchase));
        lbl_purchase_price.setText("Purchase Price\n"+"Rs."+String.format("%,.0f", purchasePrice)+"/-");
        lbl_return_qty.setText("Items Returned\n"+String.format("%,.0f", returnQty));
        lbl_return_price.setText("Return Price\n"+"Rs."+String.format("%,.0f", returnPrice)+"/-");
        lbl_cash_sent.setText("Cash Sent\n"+"Rs."+String.format("%,.0f", cashSent)+"/-");
        lbl_cash_return.setText("Cash Returned\n"+"Rs."+String.format("%,.0f", cashReturned)+"/-");
        lbl_cash_waive_off.setText("Cash Waived Off\n"+"Rs."+String.format("%,.0f", cashWaivedOff)+"/-");
    }

    private ObservableList<PurchaseReportInfo> parseUserList() {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        dealerDetails = FXCollections.observableArrayList();

        if (filter) {
//            try {
//                purchaseReportData = objPurchaseReportInfo.getDealerReportDetailSearch(objStmt1, objStmt2, objStmt3, objCon, txtFromDate, txtToDate, txtDay);
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
            if (txtFromDate != null && !txtFromDate.equals("")) {
                txt_from_date.setValue(GlobalVariables.LOCAL_DATE(txtFromDate));
            }
            if (txtToDate != null && !txtToDate.equals("")) {
                txt_to_date.setValue(GlobalVariables.LOCAL_DATE(txtToDate));
            }
            txt_day.setValue(txtDay);
            filter = false;
            filter_pane.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
            txtFromDate = "";
            txtToDate = "";
            txtDay = "All";
        } else {
            try {
                purchaseReportData = objPurchaseReportInfo.getPurchaseReportDetail(objStmt1, objStmt2, objStmt3, objCon);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }


        for (int i = 0; i < purchaseReportData.size(); i++)
        {
            if(!purchaseReportData.get(i).get(2).equals("-"))
            {
                purchase += Integer.parseInt(purchaseReportData.get(i).get(2));
            }
            if(!purchaseReportData.get(i).get(3).equals("-"))
            {
                purchasePrice += Integer.parseInt(purchaseReportData.get(i).get(3));
            }
            if(!purchaseReportData.get(i).get(4).equals("-"))
            {
                returnQty += Integer.parseInt(purchaseReportData.get(i).get(4));
            }
            if(!purchaseReportData.get(i).get(5).equals("-"))
            {
                returnPrice += Integer.parseInt(purchaseReportData.get(i).get(5));
            }
            if(!purchaseReportData.get(i).get(6).equals("-"))
            {
                cashSent += Integer.parseInt(purchaseReportData.get(i).get(6));
            }
            if(!purchaseReportData.get(i).get(7).equals("-"))
            {
                cashReturned += Integer.parseInt(purchaseReportData.get(i).get(7));
            }
            if(!purchaseReportData.get(i).get(8).equals("-"))
            {
                cashWaivedOff += Integer.parseInt(purchaseReportData.get(i).get(8));
            }
            dealerDetails.add(new PurchaseReportInfo(String.valueOf(i+1), ((purchaseReportData.get(i).get(0) == null || purchaseReportData.get(i).get(0).equals("")) ? "N/A" : purchaseReportData.get(i).get(0)), purchaseReportData.get(i).get(1), purchaseReportData.get(i).get(2), purchaseReportData.get(i).get(3), purchaseReportData.get(i).get(4), purchaseReportData.get(i).get(5), purchaseReportData.get(i).get(6), purchaseReportData.get(i).get(7), purchaseReportData.get(i).get(8)));
            summaryData.add(new PurchaseReportInfo(String.valueOf(i+1), ((purchaseReportData.get(i).get(0) == null || purchaseReportData.get(i).get(0).equals("")) ? "N/A" : purchaseReportData.get(i).get(0)), purchaseReportData.get(i).get(1), purchaseReportData.get(i).get(2), purchaseReportData.get(i).get(3), purchaseReportData.get(i).get(4), purchaseReportData.get(i).get(5), purchaseReportData.get(i).get(6), purchaseReportData.get(i).get(7), purchaseReportData.get(i).get(8)));
        }
        return dealerDetails;
    }

    public void generateSummary(ArrayList<PurchaseReportInfo> summaryList) throws JRException {
        InputStream objIO = PurchaseReport.class.getResourceAsStream("/reports/PurchaseReport.jrxml");
        JasperReport objReport = JasperCompileManager.compileReport(objIO);
        JasperPrint objPrint = JasperFillManager.fillReport(objReport, null, new JRBeanCollectionDataSource(summaryList));
        JasperViewer.viewReport(objPrint, false);
    }

    @FXML
    void printData(ActionEvent event) {
        try {
            generateSummary(summaryData);
        } catch (JRException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void btnReset(ActionEvent event) {

    }

    @FXML
    void btnSearch(ActionEvent event) {

    }

    @FXML
    void follow(MouseEvent event) {

    }

    @FXML
    void showPurchase(MouseEvent event) {

    }

    @FXML
    void showReturns(MouseEvent event) {

    }

    @FXML
    void showSent(MouseEvent event) {

    }
}
