package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import model.MysqlCon;
import model.SystemAccountsInfo;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class SystemAccounts implements Initializable {
    @FXML
    private HBox menu_bar;
    @FXML
    private TableView<SystemAccountsInfo> system_accounts_table;
    @FXML
    private TableColumn<SystemAccountsInfo, String> sr_no;

    @FXML
    private TableColumn<SystemAccountsInfo, String> user_name;

    @FXML
    private TableColumn<SystemAccountsInfo, String> user_id;

    @FXML
    private TableColumn<SystemAccountsInfo, String> permission_access;

    @FXML
    private TableColumn<SystemAccountsInfo, String> active_status;

    @FXML
    private TableColumn<SystemAccountsInfo, String> operations;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        user_name.setCellValueFactory(new PropertyValueFactory<>("userName"));
        user_id.setCellValueFactory(new PropertyValueFactory<>("userId"));
        permission_access.setCellValueFactory(new PropertyValueFactory<>("permissionAccess"));
        active_status.setCellValueFactory(new PropertyValueFactory<>("activeStatus"));
        operations.setCellValueFactory(new PropertyValueFactory<>("actionPane"));
        system_accounts_table.setItems(parseUserList());
    }

    private ObservableList<SystemAccountsInfo> parseUserList(){
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        SystemAccountsInfo objSystemAccountsInfo = new SystemAccountsInfo();
        ObservableList<SystemAccountsInfo> systemAccounts = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> usersData = objSystemAccountsInfo.getSystemAccounts(objStmt, objCon);
        for (int i = 0; i < usersData.size(); i++)
        {
            if(usersData.get(i).get(7).equals("Approved"))
            {
                systemAccounts.add(new SystemAccountsInfo(String.valueOf(i+1), usersData.get(i).get(1), usersData.get(i).get(2), usersData.get(i).get(4), usersData.get(i).get(6), "Manage"));
            }
            else if(usersData.get(i).get(7).equals("Pending"))
            {
                systemAccounts.add(new SystemAccountsInfo(String.valueOf(i+1), usersData.get(i).get(1), usersData.get(i).get(2), usersData.get(i).get(4), usersData.get(i).get(6), "Approve", "Decline"));
            }

        }

        /*systemAccounts.add(new SystemAccountsInfo("1", "Account 1", "123", "Mobile", "Active", "Approve", "Decline"));
        systemAccounts.add(new SystemAccountsInfo("2", "Account 2", "456", "Mobile", "In Active", "Approve","Decline"));
        systemAccounts.add(new SystemAccountsInfo("3", "Account 3", "789", "Computer", "Active", "Approve", "Decline"));
        systemAccounts.add(new SystemAccountsInfo("3", "Account 3", "789", "Computer", "Active", "Manage"));
        systemAccounts.add(new SystemAccountsInfo("3", "Account 3", "789", "Computer", "Active", "Manage"));
        systemAccounts.add(new SystemAccountsInfo("3", "Account 3", "789", "Computer", "Active", "Manage"));*/
        return systemAccounts;
    }

    public void viewHome() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/home.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    public void viewPendings() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_pending_orders.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    public void SaleInvoices() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/sale_invoice.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    public void addCompany() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/enter_company.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewCompanies() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_company.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    void addArea() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/enter_area.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewAreas() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_area.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewSubAreas() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_subarea.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    void addDealer() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/enter_dealer.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewDealers() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_dealers.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    void addSalesman() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/enter_salesman.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewSalesman() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_salesman.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    void addSupplier() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/enter_supplier.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewSuppliers() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_suppliers.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    void addProduct() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/enter_new_product.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewProducts() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_products.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewSystemAccounts() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/system_accounts.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

}
