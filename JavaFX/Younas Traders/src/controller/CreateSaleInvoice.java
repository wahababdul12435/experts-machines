package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import model.*;
import org.controlsfx.control.textfield.TextFields;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.ResourceBundle;

public class CreateSaleInvoice implements Initializable {

    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private JFXTextField txt_dealer_id;

    @FXML
    private JFXTextField txt_dealer_name;

    @FXML
    private JFXTextField txt_dealer_contact;

    @FXML
    private JFXTextField txt_dealer_address;

    @FXML
    private JFXTextField txt_dealer_lic;

    @FXML
    private JFXTextField txt_dealer_lic_exp;

    @FXML
    private JFXTextField txt_product_name;

    @FXML
    private JFXComboBox<String> txt_product_batch;

    @FXML
    private JFXTextField txt_product_quantity;

    @FXML
    private JFXTextField txt_product_bonus;

    @FXML
    private JFXButton btn_cancel_edit;

    @FXML
    private JFXButton btn_update_edit;

    @FXML
    private JFXButton btn_add;

    @FXML
    private Label lbl_error_product;

    @FXML
    private HBox summary_hbox1;

    @FXML
    private HBox Dealersummary_hbox1;

    @FXML
    private HBox Prodsummary_hbox1;

    @FXML
    private Label lbl_products;

    @FXML
    private Label lbl_items_ordered;

    @FXML
    private Label lbl_items_missed;

    @FXML
    private Label lbl_dealerBalance;

    @FXML
    private Label lbl_dealerLastInvNum;

    @FXML
    private Label lbl_dealerLastinvValue;

    @FXML
    private Label lbl_prodBalance;

    @FXML
    private Label lbl_batchQuant;

    @FXML
    private Label lbl_nextbatch;

    @FXML
    private Label lbl_near_expiry;

    @FXML
    private Label lbl_bonus_items;

    @FXML
    private TableView<CreateSaleInvoiceInfo> table_saledetaillog;

    @FXML
    private TableColumn<CreateSaleInvoiceInfo, String> sr_no;

    @FXML
    private TableColumn<CreateSaleInvoiceInfo, String> product_name;

    @FXML
    private TableColumn<CreateSaleInvoiceInfo, String> batch_no;

    @FXML
    private TableColumn<CreateSaleInvoiceInfo, String> batch_expiry;

    @FXML
    private TableColumn<CreateSaleInvoiceInfo, String> ordered_quantity;

    @FXML
    private TableColumn<CreateSaleInvoiceInfo, String> bonus_quantity;

    @FXML
    private TableColumn<CreateSaleInvoiceInfo, String> product_price;

    @FXML
    private TableColumn<CreateSaleInvoiceInfo, String> discount_given;

    @FXML
    private TableColumn<CreateSaleInvoiceInfo, String> operations;

    @FXML
    private JFXButton cancelBtn;

    @FXML
    private JFXButton saveBtn;

    @FXML
    private StackPane stackPane;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    @FXML
    private BorderPane brdrpane_navfinances;

    @FXML
    private BorderPane menu_bar;

    private static ObservableList<CreateSaleInvoiceInfo> saleDetails;
    ArrayList<String> dealersName;
    private static CreateSaleInvoiceInfo objCreateSaleInvoiceInfo;
    private ProductInfo objProductInfo;
    private Bonus objBonus;
    private Discount objDiscount;
    static ArrayList<ArrayList<String>> saleData;
    static ArrayList<ArrayList<String>> dealerPrevData;
    public static ArrayList<ArrayList<String>> completeDealersData;
    public static ArrayList<ArrayList<String>> completeProductData;
    public static ArrayList<ArrayList<String>> productsCompaniesDetail;
    public static ArrayList<ArrayList<String>> completeBatchData;
    public static ArrayList<ArrayList<String>> bonusDetail;
    public static ArrayList<ArrayList<String>> discountDetail;
    static ArrayList<String> selectedBatchIds;
    static ArrayList<String> selectedBatchNos;
    static ArrayList<String> selectedBatchQty;
    static ArrayList<String> selectedBatchPrices;
    private static String currentInvoiceId;
    private static String batchPolicy;
    private static String selectedBatch;
    //    private static String selectedBatchQty;
    private static String selectedProductPrice;
    ArrayList<String> productNames;
    ArrayList<String> productQty;

    public static float totalProducts = 0;
    public static float orderedItems = 0;
    public static float missedItems = 0;
    public static float nearExpiry = 0;
    public static float bonusItems = 0;

    public static float dealerBalance = 0;
    public static float dealerLastInvNum = 0;
    public static float dealerLastinvValue = 0;

    public static float productQaunt = 0;
    public static float batchQuant = 0;
    public static String nextBatch = "";

    public static int srNo;
    public static Button btnView;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnSale.setStyle("-fx-background-color: #47ab1e");
        completeDealersData = new ArrayList<>();
        objCreateSaleInvoiceInfo = new CreateSaleInvoiceInfo();
        table_saledetaillog.refresh();


        completeDealersData = objCreateSaleInvoiceInfo.getDealersCompleteData(GlobalVariables.objStmt);
        dealersName = GlobalVariables.getArrayColumn(completeDealersData, 2);
        TextFields.bindAutoCompletion(txt_dealer_name, dealersName);
        objBonus = new Bonus();
        objDiscount = new Discount();
        objProductInfo = new ProductInfo();
        saleData = new ArrayList<>();
        dealerPrevData = new ArrayList<>();
        bonusDetail = objBonus.getCurrentBonusInfo(GlobalVariables.objStmt, GlobalVariables.objCon);
        discountDetail = objDiscount.getCurrentDiscountInfo(GlobalVariables.objStmt, GlobalVariables.objCon);
        batchPolicy = objCreateSaleInvoiceInfo.getBatchPolicy(GlobalVariables.objStmt, GlobalVariables.objCon);

        CreateSaleInvoiceInfo.txtProductName = txt_product_name;
        CreateSaleInvoiceInfo.txtBatchNo = txt_product_batch;
        CreateSaleInvoiceInfo.txtQuantity = txt_product_quantity;
        CreateSaleInvoiceInfo.txtBonusPack = txt_product_bonus;
        CreateSaleInvoiceInfo.btnAdd = btn_add;
        CreateSaleInvoiceInfo.btnCancel = btn_cancel_edit;
        CreateSaleInvoiceInfo.btnUpdate = btn_update_edit;
        CreateSaleInvoiceInfo.lblProducts = lbl_products;
        CreateSaleInvoiceInfo.lblItemsOrdered = lbl_items_ordered;
        CreateSaleInvoiceInfo.lblItemsMissed = lbl_items_missed;
        CreateSaleInvoiceInfo.lblNearExpiry = lbl_near_expiry;
        CreateSaleInvoiceInfo.lblBonusItems = lbl_bonus_items;
        CreateSaleInvoiceInfo.lbl_dealerBalance = lbl_dealerBalance;
        CreateSaleInvoiceInfo.lbl_dealerLastInvNum = lbl_dealerLastInvNum;
        CreateSaleInvoiceInfo.lbl_dealerLastinvValue = lbl_dealerLastinvValue;

        CreateSaleInvoiceInfo.lbl_prodBalance = lbl_prodBalance;
        CreateSaleInvoiceInfo.lbl_batchQuant = lbl_batchQuant;
        CreateSaleInvoiceInfo.lbl_nextbatch = lbl_nextbatch;

        productsCompaniesDetail = objProductInfo.getCompanyProductsData(GlobalVariables.objStmt, GlobalVariables.objCon);
        completeProductData = objProductInfo.getProductsDetail(GlobalVariables.objStmt, GlobalVariables.objCon);
        productNames = GlobalVariables.getArrayColumn(completeProductData, 1);
        productQty = GlobalVariables.getArrayColumn(completeProductData, 2);
        TextFields.bindAutoCompletion(txt_product_name, productNames);
        completeBatchData = objProductInfo.getBatchesDetail(GlobalVariables.objStmt, GlobalVariables.objCon);

        txt_product_name.focusedProperty().addListener(new ChangeListener<Boolean>()
        {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue)
            {
                if (newPropertyValue)
                {
//                    System.out.println("Textfield on focus");
                }
                else
                {
                    String prodId = getProductId(completeProductData, txt_product_name.getText());
                    if(prodId == null)
                    {
                        btn_add.setDisable(true);
                        lbl_error_product.setVisible(true);
                    }
                    else if(productAlreadyAdded(CreateSaleInvoiceInfo.productIdArr, prodId))
                    {
                        btn_add.setDisable(true);
                        lbl_error_product.setVisible(true);
                    }
                    else
                    {
                        txt_product_batch.getItems().clear();
                        txt_product_batch.getItems().addAll(getProductsBatch(completeBatchData, prodId));
                        txt_product_batch.setValue(selectedBatch);
                        lbl_error_product.setVisible(false);
                        btn_add.setDisable(false);


                    }

                }
            }
        });

        txt_dealer_name.textProperty().addListener((observable, oldValue, newValue) -> {
//            System.out.println("textfield changed from " + oldValue + " to " + newValue);
            if(!txt_dealer_name.getText().equals(""))
            {
                int index = dealersName.indexOf(txt_dealer_name.getText());
                if(index >= 0)
                {
                    txt_dealer_id.setText(completeDealersData.get(index).get(1));
                    txt_dealer_contact.setText(completeDealersData.get(index).get(3));
                    txt_dealer_address.setText(completeDealersData.get(index).get(4));
                    txt_dealer_lic.setText(completeDealersData.get(index).get(5));
                    txt_dealer_lic_exp.setText(completeDealersData.get(index).get(6));
                }
            }
        });
        totalProducts = 0;
        orderedItems = 0;
        missedItems = 0;
        nearExpiry = 0;
        bonusItems = 0;

        dealerBalance = 0;
        dealerLastInvNum = 0;
        dealerLastinvValue = 0;


        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        product_name.setCellValueFactory(new PropertyValueFactory<>("productName"));
        batch_no.setCellValueFactory(new PropertyValueFactory<>("batchNo"));
        batch_expiry.setCellValueFactory(new PropertyValueFactory<>("batchExpiry"));
        ordered_quantity.setCellValueFactory(new PropertyValueFactory<>("orderedQuantity"));
        bonus_quantity.setCellValueFactory(new PropertyValueFactory<>("bonusQuantity"));
        product_price.setCellValueFactory(new PropertyValueFactory<>("productPrice"));
        discount_given.setCellValueFactory(new PropertyValueFactory<>("discountGiven"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_saledetaillog.setItems(parseUserList());
        CreateSaleInvoiceInfo.table_saledetaillog = table_saledetaillog;
        for ( int i = 0; i<table_saledetaillog.getItems().size(); i++) {
            table_saledetaillog.getItems().clear();
        }
    }

    public static ObservableList<CreateSaleInvoiceInfo> parseUserList() {
        saleDetails = FXCollections.observableArrayList();
        saleDetails.removeAll();
        saleData.clear();
        CreateSaleInvoiceInfo.lblProducts.setText("Products\n"+String.format("%,.0f", 0.0));
        CreateSaleInvoiceInfo.lblItemsOrdered.setText("Items Ordered\n"+String.format("%,.0f", 0.0));
        CreateSaleInvoiceInfo.lblItemsMissed.setText("Items Missed\n"+String.format("%,.0f", 0.0));
        CreateSaleInvoiceInfo.lblNearExpiry.setText("Near Expiry\n"+String.format("%,.0f", 0.0));
        CreateSaleInvoiceInfo.lblBonusItems.setText("Bonus Items\n"+String.format("%,.0f", 0.0));
        saleData = objCreateSaleInvoiceInfo.loadSaleTable();


        for (int i = 0; i < saleData.size(); i++)
        {
//            totalProducts += (saleData.get(i).get(0) != null && !saleData.get(i).get(0).equals("-")) ? 1 : 0;
//            orderedItems += (saleData.get(i).get(5) != null && !saleData.get(i).get(5).equals("-")) ? Float.parseFloat(saleData.get(i).get(5)) : 0;
//            missedItems += (saleData.get(i).get(7) != null && !saleData.get(i).get(7).equals("-")) ? Float.parseFloat(saleData.get(i).get(7)) : 0;
//            nearExpiry += (saleData.get(i).get(6) != null && !saleData.get(i).get(6).equals("-")) ? Float.parseFloat(saleData.get(i).get(6)) : 0;
//            bonusItems += (saleData.get(i).get(4) != null && !saleData.get(i).get(4).equals("")) ? Float.parseFloat(saleData.get(i).get(4)) : 0;
            saleDetails.add(new CreateSaleInvoiceInfo(String.valueOf(i+1), saleData.get(i).get(0), saleData.get(i).get(1), saleData.get(i).get(2), saleData.get(i).get(3), saleData.get(i).get(4), String.format("%,.2f", Float.parseFloat(saleData.get(i).get(5))), String.format("%,.2f", Float.parseFloat(saleData.get(i).get(6)))));
        }
        CreateSaleInvoiceInfo.lblProducts.setText("Products\n"+String.format("%,.0f", totalProducts));
        CreateSaleInvoiceInfo.lblItemsOrdered.setText("Items Ordered\n"+String.format("%,.0f", orderedItems));
        CreateSaleInvoiceInfo.lblItemsMissed.setText("Items Missed\n"+String.format("%,.0f", missedItems));
        CreateSaleInvoiceInfo.lblNearExpiry.setText("Near Expiry\n"+String.format("%,.0f", nearExpiry));
        CreateSaleInvoiceInfo.lblBonusItems.setText("Bonus Items\n"+String.format("%,.0f", bonusItems));
        return saleDetails;
    }

    @FXML
    void batchChange(ActionEvent event) {

    }

    @FXML
    void btnAdd(ActionEvent event) {
        String dealerName = txt_dealer_name.getText();
        String dealerId = GlobalVariables.getIdFrom2D(completeDealersData, dealerName, 2);
        String productName = txt_product_name.getText();
        String productId = GlobalVariables.getIdFrom2D(completeProductData, productName, 1);
        String companyId = GlobalVariables.getDataFrom2D(completeProductData, productName, 1, 4);
        String batchNo = txt_product_batch.getValue();
        String quantity = txt_product_quantity.getText();
        String bonus = txt_product_bonus.getText();
        selectedProductPrice = String.valueOf(Float.parseFloat(selectedProductPrice) * Float.parseFloat(quantity));
        bonus = getBonus(dealerId, companyId, productId, quantity);
        String discount = getDiscount(dealerId, companyId, productId, selectedProductPrice);
        String finalProductPrice = String.valueOf(Float.parseFloat(selectedProductPrice) - Float.parseFloat(discount));
        String qtyOfBatch = getBatchQty(selectedBatchNos, batchNo);
        String qtyOfProduct = getProductQty(completeProductData, productName);
        if(qtyOfBatch == null)
        {
            qtyOfBatch = "0";
        }
        if(qtyOfProduct == null)
        {
            qtyOfProduct = "0";
        }
        CreateSaleInvoiceInfo.productIdArr.add(getProductId(completeProductData, productName));
        CreateSaleInvoiceInfo.productNameArr.add(productName);
        CreateSaleInvoiceInfo.orderedQuantityArr.add(quantity);
        if(qtyOfBatch != null && !qtyOfBatch.equals("0"))
        {
            if(Integer.parseInt(quantity)<=Integer.parseInt(qtyOfBatch))
            {
                CreateSaleInvoiceInfo.submissionQuantityArr.add(quantity);
                qtyOfBatch = String.valueOf(Integer.parseInt(qtyOfBatch) - Integer.parseInt(quantity));
                setBatchQty(selectedBatchNos, batchNo, qtyOfBatch);
                qtyOfProduct = String.valueOf(Integer.parseInt(qtyOfProduct) - Integer.parseInt(quantity));
                setProductQty(completeProductData, productName, qtyOfProduct);
            }
            else
            {
                CreateSaleInvoiceInfo.submissionQuantityArr.add(qtyOfBatch);
                setBatchQty(selectedBatchNos, batchNo, "0");
                qtyOfProduct = String.valueOf(Integer.parseInt(qtyOfProduct) - Integer.parseInt(qtyOfBatch));
                setProductQty(completeProductData, productName, qtyOfProduct);
            }
            CreateSaleInvoiceInfo.batchIdArr.add(getBatchId(completeBatchData, batchNo));
            CreateSaleInvoiceInfo.batchNoArr.add(batchNo);
            CreateSaleInvoiceInfo.batchExpiryArr.add(getBatchExpiry(completeBatchData, batchNo));
            CreateSaleInvoiceInfo.nearExpiryArr.add(false);
        }
        else
        {
            if(Integer.parseInt(quantity)<=Integer.parseInt(qtyOfProduct))
            {
                CreateSaleInvoiceInfo.submissionQuantityArr.add(quantity);
                qtyOfProduct = String.valueOf(Integer.parseInt(qtyOfProduct) - Integer.parseInt(quantity));
                setProductQty(completeProductData, productName, qtyOfProduct);
            }
            else
            {
                CreateSaleInvoiceInfo.submissionQuantityArr.add(qtyOfProduct);
                setProductQty(completeProductData, productName, "0");
            }
            CreateSaleInvoiceInfo.batchIdArr.add("0");
            CreateSaleInvoiceInfo.batchNoArr.add("N/A");
            CreateSaleInvoiceInfo.batchExpiryArr.add("N/A");
            CreateSaleInvoiceInfo.nearExpiryArr.add(false);
        }

        CreateSaleInvoiceInfo.bonusQuantityArr.add(bonus);
        CreateSaleInvoiceInfo.productPriceArr.add(selectedProductPrice);
        CreateSaleInvoiceInfo.finalPriceArr.add(finalProductPrice);
        CreateSaleInvoiceInfo.discountGivenArr.add(discount);
        table_saledetaillog.setItems(parseUserList());
        CreateSaleInvoiceInfo.table_saledetaillog = table_saledetaillog;

        batchQuant=0;
        productQaunt=0;
        nextBatch="0";
        CreateSaleInvoiceInfo.lbl_prodBalance.setText("Product Balance\n"+String.format("%,.0f", productQaunt));
        CreateSaleInvoiceInfo.lbl_batchQuant.setText("Batch Quantity\n"+String.format("%,.0f", batchQuant));
        CreateSaleInvoiceInfo.lbl_nextbatch.setText("Next Batch\n"+String.format("%s", nextBatch));

        txt_product_name.clear();
        txt_product_batch.getItems().clear();
        txt_product_batch.setValue(null);
        txt_product_quantity.clear();
        txt_product_bonus.clear();
    }

    @FXML
    void fillDealersDetail(KeyEvent event) {
        if(!txt_dealer_name.getText().equals(""))
        {
            int index = dealersName.indexOf(txt_dealer_name.getText());
            if(index >= 0)
            {
                txt_dealer_id.setText(completeDealersData.get(index).get(1));
                txt_dealer_contact.setText(completeDealersData.get(index).get(3));
                txt_dealer_address.setText(completeDealersData.get(index).get(4));
                txt_dealer_lic.setText(completeDealersData.get(index).get(5));
                txt_dealer_lic_exp.setText(completeDealersData.get(index).get(6));

                int integ_dealerID = Integer.parseInt(completeDealersData.get(index).get(1));

                dealerPrevData = objCreateSaleInvoiceInfo.loaddealerPrevdata(integ_dealerID,GlobalVariables.objStmt);

                CreateSaleInvoiceInfo.lbl_dealerBalance.setText("Balance\n"+String.format("%,.0f", dealerBalance));
                CreateSaleInvoiceInfo.lbl_dealerLastInvNum.setText("Last invoice Number\n"+String.format("%,.0f", dealerLastInvNum));
                CreateSaleInvoiceInfo.lbl_dealerLastinvValue.setText("Last invoice Value\n"+String.format("%,.0f", dealerLastinvValue));
            }
        }

    }

    @FXML
    void btnCancelEdit(ActionEvent event) {
        txt_product_name.clear();
        txt_product_batch.getItems().clear();
        txt_product_batch.setValue(null);
        txt_product_quantity.clear();
        txt_product_bonus.clear();
        btn_add.setVisible(true);
        btn_cancel_edit.setVisible(false);
        btn_update_edit.setVisible(false);
    }

    @FXML
    void btnUpdateEdit(ActionEvent event) {
        String dealerName = txt_dealer_name.getText();
        String dealerId = GlobalVariables.getIdFrom2D(completeDealersData, dealerName, 2);
        String productName = txt_product_name.getText();
        String productId = GlobalVariables.getIdFrom2D(completeProductData, productName, 1);
        String companyId = GlobalVariables.getDataFrom2D(completeProductData, productName, 1, 4);
        String batchNo = txt_product_batch.getValue();
        String quantity = txt_product_quantity.getText();
        String bonus = txt_product_bonus.getText();
        selectedProductPrice = String.valueOf(Float.parseFloat(selectedProductPrice) * Float.parseFloat(quantity));
        bonus = getBonus(dealerId, companyId, productId, quantity);
        String discount = getDiscount(dealerId, companyId, productId, selectedProductPrice);
        String finalProductPrice = String.valueOf(Float.parseFloat(selectedProductPrice) - Float.parseFloat(discount));
        String qtyOfBatch = getBatchQty(selectedBatchNos, batchNo);
        String qtyOfProduct = getProductQty(completeProductData, productName);
        if(qtyOfBatch == null)
        {
            qtyOfBatch = "0";
        }
        if(qtyOfProduct == null)
        {
            qtyOfProduct = "0";
        }
        CreateSaleInvoiceInfo.productIdArr.set(srNo, getProductId(completeProductData, productName));
        CreateSaleInvoiceInfo.productNameArr.set(srNo, productName);
        CreateSaleInvoiceInfo.orderedQuantityArr.set(srNo, quantity);
        if(qtyOfBatch != null && !qtyOfBatch.equals("0"))
        {
            if(Integer.parseInt(quantity)<=Integer.parseInt(qtyOfBatch))
            {
                CreateSaleInvoiceInfo.submissionQuantityArr.set(srNo, quantity);
                qtyOfBatch = String.valueOf(Integer.parseInt(qtyOfBatch) - Integer.parseInt(quantity));
                setBatchQty(selectedBatchNos, batchNo, qtyOfBatch);
                qtyOfProduct = String.valueOf(Integer.parseInt(qtyOfProduct) - Integer.parseInt(quantity));
                setProductQty(completeProductData, productName, qtyOfProduct);
            }
            else
            {
                CreateSaleInvoiceInfo.submissionQuantityArr.set(srNo, qtyOfBatch);
                setBatchQty(selectedBatchNos, batchNo, "0");
                qtyOfProduct = String.valueOf(Integer.parseInt(qtyOfProduct) - Integer.parseInt(qtyOfBatch));
                setProductQty(completeProductData, productName, qtyOfProduct);
            }
            CreateSaleInvoiceInfo.batchIdArr.set(srNo, getBatchId(completeBatchData, batchNo));
            CreateSaleInvoiceInfo.batchNoArr.set(srNo, batchNo);
            CreateSaleInvoiceInfo.batchExpiryArr.set(srNo, getBatchExpiry(completeBatchData, batchNo));
            CreateSaleInvoiceInfo.nearExpiryArr.set(srNo, false);
        }
        else
        {
            if(Integer.parseInt(quantity)<=Integer.parseInt(qtyOfProduct))
            {
                CreateSaleInvoiceInfo.submissionQuantityArr.set(srNo, quantity);
                qtyOfProduct = String.valueOf(Integer.parseInt(qtyOfProduct) - Integer.parseInt(quantity));
                setProductQty(completeProductData, productName, qtyOfProduct);
            }
            else
            {
                CreateSaleInvoiceInfo.submissionQuantityArr.set(srNo, qtyOfProduct);
                setProductQty(completeProductData, productName, "0");
            }
            CreateSaleInvoiceInfo.batchIdArr.set(srNo, "0");
            CreateSaleInvoiceInfo.batchNoArr.set(srNo, "N/A");
            CreateSaleInvoiceInfo.batchExpiryArr.set(srNo, "N/A");
            CreateSaleInvoiceInfo.nearExpiryArr.set(srNo, false);
        }

        CreateSaleInvoiceInfo.bonusQuantityArr.set(srNo, bonus);
        CreateSaleInvoiceInfo.productPriceArr.set(srNo, selectedProductPrice);
        CreateSaleInvoiceInfo.finalPriceArr.set(srNo, finalProductPrice);
        CreateSaleInvoiceInfo.discountGivenArr.set(srNo, discount);
        table_saledetaillog.setItems(parseUserList());
        CreateSaleInvoiceInfo.table_saledetaillog = table_saledetaillog;

        txt_product_name.clear();
        txt_product_batch.getItems().clear();
        txt_product_batch.setValue(null);
        txt_product_quantity.clear();
        txt_product_bonus.clear();
        btn_add.setVisible(true);
        btn_cancel_edit.setVisible(false);
        btn_update_edit.setVisible(false);
    }

    @FXML
    void cancelClicked(ActionEvent event) {

    }

    @FXML
    void follow(MouseEvent event) {

    }

    @FXML
    void productChange(KeyEvent event) {

    }

    @FXML
    void saveClicked(ActionEvent event) {
        String dealerId = GlobalVariables.getIdFrom2D(completeDealersData, txt_dealer_name.getText(), 2);
        float orderPrice = 0;
        float discount = 0;
//        float waivedOffPrice = 0;
        float finalPrice = 0;
        String orderStatus = "Pending";
        for(int i=0; i<CreateSaleInvoiceInfo.productIdArr.size(); i++)
        {
            orderPrice += Float.parseFloat(CreateSaleInvoiceInfo.productPriceArr.get(i));
            discount += Float.parseFloat(CreateSaleInvoiceInfo.discountGivenArr.get(i));
            finalPrice += Float.parseFloat(CreateSaleInvoiceInfo.finalPriceArr.get(i));
        }

        objCreateSaleInvoiceInfo.insertBasic(GlobalVariables.objStmt, dealerId, String.valueOf(orderPrice), String.valueOf(discount), String.valueOf(finalPrice), orderStatus);
        objCreateSaleInvoiceInfo.insertInvoice(GlobalVariables.objStmt);
        table_saledetaillog.refresh();
        for ( int i = 0; i<table_saledetaillog.getItems().size(); i++) {
            table_saledetaillog.getItems().clear();
        }

        saleDetails.removeAll();
        saleData.clear();


        CreateSaleInvoiceInfo.productIdArr.clear();
        CreateSaleInvoiceInfo.productNameArr.clear();
        CreateSaleInvoiceInfo.batchNoArr.clear();
        CreateSaleInvoiceInfo.batchExpiryArr.clear();
        CreateSaleInvoiceInfo.orderedQuantityArr.clear();
        CreateSaleInvoiceInfo.submissionQuantityArr.clear();
        CreateSaleInvoiceInfo.bonusQuantityArr.clear();
        CreateSaleInvoiceInfo.finalPriceArr.clear();
        CreateSaleInvoiceInfo.discountGivenArr.clear();
        CreateSaleInvoiceInfo.nearExpiryArr.clear();
        CreateSaleInvoiceInfo.batchIdArr.clear();
        CreateSaleInvoiceInfo.productPriceArr.clear();


        totalProducts=0;
        orderedItems=0;
        missedItems=0;
        bonusItems=0;

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/view_sales.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void showBooked(MouseEvent event) {

    }

    @FXML
    void showBookingPrice(MouseEvent event) {

    }

    @FXML
    void showDelivered(MouseEvent event) {

    }

    @FXML
    void showDiscountGiven(MouseEvent event) {

    }

    @FXML
    void showReturned(MouseEvent event) {

    }
    CreateSaleInvoiceInfo objcreateinvoice;
    public static String getProductId(ArrayList<ArrayList<String>> productsData, String productName)
    {
        for(ArrayList<String> row: productsData) {
            if(row.get(1).equals(productName))
            {
                selectedProductPrice = row.get(3);
//                addedRetailPrice = row.get(2);
                return row.get(0);
            }
        }
        return null;
    }

    public static String getProductQty(ArrayList<ArrayList<String>> productsData, String productName)
    {
        for(ArrayList<String> row: productsData) {
            if(row.get(1).equals(productName))
            {
                return row.get(2);
            }
        }
        return null;
    }

    public static void setProductQty(ArrayList<ArrayList<String>> productsData, String productName, String quantity)
    {
        int i=0;
        for(ArrayList<String> row: productsData) {
            if(row.get(1).equals(productName))
            {
                row.set(2, quantity);
                productsData.set(i, row);
            }
            i++;
        }
    }

    public String getBatchQty(ArrayList<String> selectedBatch, String batchNo)
    {
        for(int i=0; i<selectedBatch.size(); i++) {
            if(selectedBatch.get(i).equals(batchNo))
            {
                return selectedBatchQty.get(i);
            }
        }
        return null;
    }

    public void setBatchQty(ArrayList<String> selectedBatch, String batchNo, String quantity)
    {
        for(int i=0; i<selectedBatch.size(); i++) {
            if(selectedBatch.get(i).equals(batchNo))
            {
                selectedBatchQty.set(i, quantity);
            }
        }
    }

    public String getBatchId(ArrayList<ArrayList<String>> batchData, String batchNo)
    {
        for(ArrayList<String> row: batchData) {
            if(row.get(1).equals(batchNo))
            {
//                addedRetailPrice = row.get(2);
                return row.get(0);
            }
        }
        return null;
    }

    public String getBatchExpiry(ArrayList<ArrayList<String>> batchData, String batchNo)
    {
        for(ArrayList<String> row: batchData) {
            if(row.get(1).equals(batchNo))
            {
//                addedRetailPrice = row.get(2);
                return row.get(4);
            }
        }
        return null;
    }

    public static ArrayList<String> getProductsBatch(ArrayList<ArrayList<String>> batchData, String productId)
    {
        ArrayList<ArrayList<String>> prods_details = new ArrayList<ArrayList<String>>();
        ArrayList<String> columnList = new ArrayList<>();
        ArrayList<String> prodtotalQuant = new ArrayList<>();
        ArrayList<String> stExpiryDate = new ArrayList<>();
        ArrayList<Date> expiryDate = new ArrayList<>();
        ArrayList<String> stEntryDate = new ArrayList<>();
        ArrayList<Date> entryDate = new ArrayList<>();
        selectedBatchIds = new ArrayList<>();
        selectedBatchNos = new ArrayList<>();
        selectedBatchQty = new ArrayList<>();
        selectedBatchPrices = new ArrayList<>();
        Date entrySelectedDate;
        Date expirySelectedDate;
        int selectedIndex = 0;
        boolean expirySelection = false;
        // Batch Id          0
        // Batch No          1
        // Product Id        2
        // Batch Quantity    3
        // Batch Expiry      4
        // Entry Date        5
        // Trade Price        6
        if(batchData!=null)
        {
            for(ArrayList<String> row: batchData) {
                if(row.get(2).equals(productId))
                {

                    MysqlCon objMysqlCon = new MysqlCon();
                    Statement objStmt = objMysqlCon.stmt;
                    Connection objCon = objMysqlCon.con;
                    prods_details = objCreateSaleInvoiceInfo.gettableID(objStmt, objCon,productId);

                    for(int ii =0 ; ii<prods_details.size();ii++)
                    {
                    selectedBatchIds.add(prods_details.get(ii).get(0));
                    selectedBatchNos.add(prods_details.get(ii).get(1));
                    selectedBatchQty.add(prods_details.get(ii).get(2));
                    selectedBatchPrices.add(prods_details.get(ii).get(3));
                    columnList.add(prods_details.get(ii).get(1));
                    stExpiryDate.add(prods_details.get(ii).get(4));
                    stEntryDate.add(prods_details.get(ii).get(5));
                    try {
                        expiryDate.add(GlobalVariables.fmt.parse(prods_details.get(ii).get(5)));
                        entryDate.add(GlobalVariables.fmt.parse(prods_details.get(ii).get(4)));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    }
                    MysqlCon objMysqlCon1 = new MysqlCon();
                    Statement objStmt1 = objMysqlCon1.stmt;
                    Connection objCon1 = objMysqlCon1.con;
                    prodtotalQuant = objCreateSaleInvoiceInfo.getprodQuantID(objStmt1, objCon1,productId);

                    if(batchPolicy.equals("Entry LIFO"))
                    {
                        // Entry select max date
                        entrySelectedDate = Collections.max(entryDate);
                        expirySelection = false;
                        selectedIndex = entryDate.indexOf(entrySelectedDate);
                    }
                    else if(batchPolicy.equals("Entry FIFO"))
                    {
                        // Entry select min date
                        entrySelectedDate = Collections.min(entryDate);
                        expirySelection = false;
                        selectedIndex = entryDate.indexOf(entrySelectedDate);
                    }
                    else if(batchPolicy.equals("Expiry LIFO"))
                    {
                        // Expiry select max date
                        expirySelectedDate = Collections.max(expiryDate);
                        expirySelection = true;
                        selectedIndex = expiryDate.indexOf(expirySelectedDate);
                    }
                    else if(batchPolicy.equals("Expiry FIFO"))
                    {
                        // Expiry select min date
                        expirySelectedDate = Collections.min(expiryDate);
                        expirySelection = true;
                        selectedIndex = expiryDate.indexOf(expirySelectedDate);
                    }
                    int nxtindex = 0;
                    float quntity = Float.parseFloat(prodtotalQuant.get(0));
                    float bonusquntity = Float.parseFloat(prodtotalQuant.get(1));
                    float total_quantity = quntity+bonusquntity;
                    productQaunt = total_quantity;
                    if(columnList.size()==1)
                    {
                        nextBatch= "0";
                    }
                    if(columnList.size()==2)
                    {
                        nxtindex= selectedIndex-1;
                        nextBatch = columnList.get(nxtindex);
                    }
                    if(columnList.size()>2)
                    {
                        nxtindex= selectedIndex+1;
                        nextBatch = columnList.get(nxtindex);
                    }

                    String vvvv = selectedBatchQty.get(selectedIndex);
                    batchQuant = Float.parseFloat(vvvv);
                    CreateSaleInvoiceInfo.lbl_prodBalance.setText("Product Balance\n"+String.format("%,.0f", productQaunt));
                    CreateSaleInvoiceInfo.lbl_batchQuant.setText("Batch Quantity\n"+String.format("%,.0f", batchQuant));
                    CreateSaleInvoiceInfo.lbl_nextbatch.setText("Next Batch\n"+String.format("%s", nextBatch));


                    selectedBatch = columnList.get(selectedIndex);


                }
            }
        }
        return columnList;
    }

    private String getBonus(String dealerId, String companyId, String productId, String quantity)
    {
        String bonus = "0";
        if(quantity.equals(""))
        {
            quantity = "0";
        }

        for(int i=0; i<bonusDetail.size(); i++)
        {
            if(dealerId.equals(bonusDetail.get(i).get(0)) && (bonusDetail.get(i).get(1).equals("0") || bonusDetail.get(i).get(1).equals("null")) && (bonusDetail.get(i).get(2).equals("0") || bonusDetail.get(i).get(2).equals("null")))
            {
                if(bonusDetail.get(i).get(3).equals("0"))
                {
                    return bonusDetail.get(i).get(4);
                }
                else
                {
                    int num = Integer.valueOf(quantity)/Integer.valueOf(bonusDetail.get(i).get(3));
                    num = num*Integer.valueOf(bonusDetail.get(i).get(4));
                    return String.valueOf(num);
                }
            }
            else if(dealerId.equals(bonusDetail.get(i).get(0)) && (bonusDetail.get(i).get(1).equals("0") || bonusDetail.get(i).get(1).equals("null")) && productId.equals(bonusDetail.get(i).get(2)))
            {
                if(bonusDetail.get(i).get(3).equals("0"))
                {
                    return bonusDetail.get(i).get(4);
                }
                else
                {
                    int num = Integer.valueOf(quantity)/Integer.valueOf(bonusDetail.get(i).get(3));
                    num = num*Integer.valueOf(bonusDetail.get(i).get(4));
                    return String.valueOf(num);
                }
            }
            else if((bonusDetail.get(i).get(0).equals("0") || bonusDetail.get(i).get(0).equals("null")) && (bonusDetail.get(i).get(1).equals("0") || bonusDetail.get(i).get(1).equals("null")) && productId.equals(bonusDetail.get(i).get(2)))
            {
                if(bonusDetail.get(i).get(3).equals("0"))
                {
                    return bonusDetail.get(i).get(4);
                }
                else
                {
                    int num = Integer.valueOf(quantity)/Integer.valueOf(bonusDetail.get(i).get(3));
                    num = num*Integer.valueOf(bonusDetail.get(i).get(4));
                    return String.valueOf(num);
                }
            }
            else if((bonusDetail.get(i).get(0).equals("0") || bonusDetail.get(i).get(0).equals("null")) && !(bonusDetail.get(i).get(1).equals("0") || bonusDetail.get(i).get(1).equals("null")) && (bonusDetail.get(i).get(2).equals("0") || bonusDetail.get(i).get(2).equals("null")))
            {
                for(int j=0; j<productsCompaniesDetail.size(); j++)
                {
                    if(productsCompaniesDetail.get(j).get(1).equals(companyId) && companyId.equals(bonusDetail.get(i).get(1)))
                    {
                        if(bonusDetail.get(i).get(3).equals("0"))
                        {
                            return bonusDetail.get(i).get(4);
                        }
                        else
                        {
                            int num = Integer.valueOf(quantity)/Integer.valueOf(bonusDetail.get(i).get(3));
                            num = num*Integer.valueOf(bonusDetail.get(i).get(4));
                            return String.valueOf(num);
                        }
                    }
                }
            }
            else if(dealerId.equals(bonusDetail.get(i).get(0)) && !(bonusDetail.get(i).get(1).equals("0") || bonusDetail.get(i).get(1).equals("null")) && (bonusDetail.get(i).get(2).equals("0") || bonusDetail.get(i).get(2).equals("null")))
            {
                for(int j=0; j<productsCompaniesDetail.size(); j++)
                {
                    if(productsCompaniesDetail.get(j).get(1).equals(companyId) && companyId.equals(bonusDetail.get(i).get(1)))
                    {
                        if(bonusDetail.get(i).get(3).equals("0"))
                        {
                            return bonusDetail.get(i).get(4);
                        }
                        else
                        {
                            int num = Integer.valueOf(quantity)/Integer.valueOf(bonusDetail.get(i).get(3));
                            num = num*Integer.valueOf(bonusDetail.get(i).get(4));
                            return String.valueOf(num);
                        }
                    }
                }
            }
            else if((bonusDetail.get(i).get(0).equals("0") || bonusDetail.get(i).get(0).equals("null")) && !(bonusDetail.get(i).get(1).equals("0") || bonusDetail.get(i).get(1).equals("null")) && productId.equals(bonusDetail.get(i).get(2)))
            {
                for(int j=0; j<productsCompaniesDetail.size(); j++)
                {
                    if(productsCompaniesDetail.get(j).get(1).equals(companyId) && companyId.equals(bonusDetail.get(i).get(1)))
                    {
                        if(bonusDetail.get(i).get(3).equals("0"))
                        {
                            return bonusDetail.get(i).get(4);
                        }
                        else
                        {
                            int num = Integer.valueOf(quantity)/Integer.valueOf(bonusDetail.get(i).get(3));
                            num = num*Integer.valueOf(bonusDetail.get(i).get(4));
                            return String.valueOf(num);
                        }
                    }
                }
            }
            else if((bonusDetail.get(i).get(0).equals("0") || bonusDetail.get(i).get(0).equals("null")) && (bonusDetail.get(i).get(1).equals("0") || bonusDetail.get(i).get(1).equals("null")) && (bonusDetail.get(i).get(2).equals("0") || bonusDetail.get(i).get(2).equals("null")))
            {
                if(bonusDetail.get(i).get(3).equals("0"))
                {
                    return bonusDetail.get(i).get(4);
                }
                else
                {
                    int num = Integer.valueOf(quantity)/Integer.valueOf(bonusDetail.get(i).get(3));
                    num = num*Integer.valueOf(bonusDetail.get(i).get(4));
                    return String.valueOf(num);
                }
            }
        }
        return bonus;
    }

    private String getDiscount(String dealerId, String companyId, String productId, String saleAmount)
    {
        String discount = "0";
        if(saleAmount.equals(""))
        {
            saleAmount = "0";
        }

        for(int i=0; i<discountDetail.size(); i++)
        {
            if(dealerId.equals(discountDetail.get(i).get(0)) && (discountDetail.get(i).get(1).equals("0") || discountDetail.get(i).get(1).equals("null")) && (discountDetail.get(i).get(2).equals("0") || discountDetail.get(i).get(2).equals("null")))
            {
                if(Float.parseFloat(discountDetail.get(i).get(3)) == 0)
                {
                    float num = Float.parseFloat(saleAmount)/100;
                    num = num*Float.parseFloat(discountDetail.get(i).get(4));
                    return String.valueOf(num);
                }
                else
                {
                    float num = Float.parseFloat(saleAmount)/Float.parseFloat(discountDetail.get(i).get(3));
                    num = num*Float.parseFloat(discountDetail.get(i).get(4));
                    return String.valueOf(num);
                }
            }
            else if(dealerId.equals(discountDetail.get(i).get(0)) && (discountDetail.get(i).get(1).equals("0") || discountDetail.get(i).get(1).equals("null")) && productId.equals(discountDetail.get(i).get(2)))
            {
                if(Float.parseFloat(discountDetail.get(i).get(3)) == 0)
                {
                    float num = Float.parseFloat(saleAmount)/100;
                    num = num*Float.parseFloat(discountDetail.get(i).get(4));
                    return String.valueOf(num);
                }
                else
                {
                    float num = Float.parseFloat(saleAmount)/Float.parseFloat(discountDetail.get(i).get(3));
                    num = num*Float.parseFloat(discountDetail.get(i).get(4));
                    return String.valueOf(num);
                }
            }
            else if((discountDetail.get(i).get(0).equals("0") || discountDetail.get(i).get(0).equals("null")) && (discountDetail.get(i).get(1).equals("0") || discountDetail.get(i).get(1).equals("null")) && productId.equals(discountDetail.get(i).get(2)))
            {
                if(Float.parseFloat(discountDetail.get(i).get(3)) == 0)
                {
                    float num = Float.parseFloat(saleAmount)/100;
                    num = num*Float.parseFloat(discountDetail.get(i).get(4));
                    return String.valueOf(num);
                }
                else
                {
                    float num = Float.parseFloat(saleAmount)/Float.parseFloat(discountDetail.get(i).get(3));
                    num = num*Float.parseFloat(discountDetail.get(i).get(4));
                    return String.valueOf(num);
                }
            }
            else if((discountDetail.get(i).get(0).equals("0") || discountDetail.get(i).get(0).equals("null")) && !(discountDetail.get(i).get(1).equals("0") || discountDetail.get(i).get(1).equals("null")) && (discountDetail.get(i).get(2).equals("0") || discountDetail.get(i).get(2).equals("null")))
            {
                for(int j=0; j<productsCompaniesDetail.size(); j++)
                {
                    if(productsCompaniesDetail.get(j).get(1).equals(companyId) && companyId.equals(discountDetail.get(i).get(1)))
                    {
                        if(Float.parseFloat(discountDetail.get(i).get(3)) == 0)
                        {
                            float num = Float.parseFloat(saleAmount)/100;
                            num = num*Float.parseFloat(discountDetail.get(i).get(4));
                            return String.valueOf(num);
                        }
                        else
                        {
                            float num = Float.parseFloat(saleAmount)/Float.parseFloat(discountDetail.get(i).get(3));
                            num = num*Float.parseFloat(discountDetail.get(i).get(4));
                            return String.valueOf(num);
                        }
                    }
                }
            }
            else if(dealerId.equals(discountDetail.get(i).get(0)) && !(discountDetail.get(i).get(1).equals("0") || discountDetail.get(i).get(1).equals("null")) && (discountDetail.get(i).get(2).equals("0") || discountDetail.get(i).get(2).equals("null")))
            {
                for(int j=0; j<productsCompaniesDetail.size(); j++)
                {
                    if(productsCompaniesDetail.get(j).get(1).equals(companyId) && companyId.equals(discountDetail.get(i).get(1)))
                    {
                        if(Float.parseFloat(discountDetail.get(i).get(3)) == 0)
                        {
                            float num = Float.parseFloat(saleAmount)/100;
                            num = num*Float.parseFloat(discountDetail.get(i).get(4));
                            return String.valueOf(num);
                        }
                        else
                        {
                            float num = Float.parseFloat(saleAmount)/Float.parseFloat(discountDetail.get(i).get(3));
                            num = num*Float.parseFloat(discountDetail.get(i).get(4));
                            return String.valueOf(num);
                        }
                    }
                }
            }
            else if((discountDetail.get(i).get(0).equals("0") || discountDetail.get(i).get(0).equals("null")) && !(discountDetail.get(i).get(1).equals("0") || discountDetail.get(i).get(1).equals("null")) && productId.equals(discountDetail.get(i).get(2)))
            {
                for(int j=0; j<productsCompaniesDetail.size(); j++)
                {
                    if(productsCompaniesDetail.get(j).get(1).equals(companyId) && companyId.equals(discountDetail.get(i).get(1)))
                    {
                        if(Float.parseFloat(discountDetail.get(i).get(3)) == 0)
                        {
                            float num = Float.parseFloat(saleAmount)/100;
                            num = num*Float.parseFloat(discountDetail.get(i).get(4));
                            return String.valueOf(num);
                        }
                        else
                        {
                            float num = Float.parseFloat(saleAmount)/Float.parseFloat(discountDetail.get(i).get(3));
                            num = num*Float.parseFloat(discountDetail.get(i).get(4));
                            return String.valueOf(num);
                        }
                    }
                }
            }
            else if((discountDetail.get(i).get(0).equals("0") || discountDetail.get(i).get(0).equals("null")) && (discountDetail.get(i).get(1).equals("0") || discountDetail.get(i).get(1).equals("null")) && (discountDetail.get(i).get(2).equals("0") || discountDetail.get(i).get(2).equals("null")))
            {
                if(Float.parseFloat(discountDetail.get(i).get(3)) == 0)
                {
                    float num = Float.parseFloat(saleAmount)/100;
                    num = num*Float.parseFloat(discountDetail.get(i).get(4));
                    return String.valueOf(num);
                }
                else
                {
                    float num = Float.parseFloat(saleAmount)/Float.parseFloat(discountDetail.get(i).get(3));
                    num = num*Float.parseFloat(discountDetail.get(i).get(4));
                    return String.valueOf(num);
                }
            }
        }
        return discount;
    }

    private boolean productAlreadyAdded(ArrayList<String> addedProducts, String productId)
    {
        return addedProducts.contains(productId);
    }
}
