package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.validation.RequiredFieldValidator;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.util.Duration;
import model.CompanyInfo;
import model.GlobalVariables;
import model.MysqlCon;
import model.CompanyInfo;
import org.controlsfx.control.textfield.TextFields;

import javax.swing.*;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class EnterCompany implements Initializable {

    private static int x = 1;
    private static int groupsCount = 0;
    @FXML    private JFXTextField txt_city;
    @FXML    private JFXTextField txt_compID;
    @FXML    private JFXTextField txt_compAddress;
    @FXML    private JFXTextField txt_contactPerson;
    @FXML    private JFXTextField txt_compEmail;
    @FXML    private JFXTextField txt_compName;
    @FXML    private JFXTextField txt_contactNum;
    @FXML    private JFXTextField compGroup1;
    @FXML    private JFXTextField compGroup2;
    @FXML    private JFXTextField compGroup3;
    @FXML    private JFXTextField compGroup4;
    @FXML    private JFXTextField compGroup5;
    @FXML    private Button cancelBtn;
    @FXML    private Button saveBtn;
    @FXML    private BorderPane menu_bar1;
    @FXML    private AnchorPane inner_anchor;
    @FXML    private BorderPane nav_setup;
    @FXML    private Pane btnpane;
    @FXML    private Button btn11;
    @FXML    private FontAwesomeIconView FA_Icon;
    @FXML    private JFXDrawer drawer;
    @FXML    private Button buttn1;
    @FXML    private Button buttn2;
    @FXML    private Button buttn3;
    @FXML    private Button buttn5;
    @FXML    private Button buttn4;
    @FXML    private Button btn_edit_cancel;
    @FXML    private Button btn_edit_update;
    @FXML
    private TableView<CompanyInfo> table_addcompany;

    @FXML
    private TableColumn<CompanyInfo, String> sr_no;

    @FXML
    private TableColumn<CompanyInfo, String> company_id;

    @FXML
    private TableColumn<CompanyInfo, String> company_name;

    @FXML
    private TableColumn<CompanyInfo, String> groups_count;

    @FXML
    private TableColumn<CompanyInfo, String> operations;

    @FXML
    private JFXButton add_company;

    public static ObservableList<CompanyInfo> companyDetails;

    Alert alert = new Alert(Alert.AlertType.NONE);

    public static int srNo;
    public static Button btnView;

    @FXML
    void addCompany(ActionEvent event) {

        String companyIDs = txt_compID.getText();
        String companyName = txt_compName.getText();
        String companyAddress = txt_compAddress.getText();
        String companyContact = txt_contactPerson.getText();
        String companyEmail = txt_compEmail.getText();
        String companyCity = txt_city.getText();
        String contactNumber = txt_contactNum.getText();
        String companyStatus = "Active";
        String[] companyGroup = new String[4];

        if(groupsCount > 0)
        {
            companyGroup[0] = compGroup1.getText();
        }
        if(groupsCount > 1)
        {
            companyGroup[1] = compGroup2.getText();
        }
        if(groupsCount > 2)
        {
            companyGroup[2] = compGroup3.getText();
        }
        if(groupsCount > 3)
        {
            companyGroup[3] = compGroup4.getText();
        }
        if(groupsCount > 4)
        {
            companyGroup[4] = compGroup5.getText();
        }

        MysqlCon objMysqlCon1 = new MysqlCon();
        Statement objStmt1 = objMysqlCon1.stmt;
        Connection objCon1 = objMysqlCon1.con;
        CompanyInfo objCompInfo = new CompanyInfo();
        String comp = objCompInfo.confirmNewId(objStmt1, objCon1,companyIDs);

        if(!comp.equals(companyIDs) || comp.equals(""))
        {
            MysqlCon objMysqlCon = new MysqlCon();
            Statement objStmt = objMysqlCon.stmt;
            Connection objCon = objMysqlCon.con;
            CompanyInfo objCompanyInfo = new CompanyInfo();
//            System.out.println("Id: "+companyIDs+" Name: "+companyName+" Address: "+companyAddress+" Contact: "+companyContact+" Email: "+companyEmail+" City: "+companyCity+" Number: "+contactNumber+" Status: "+companyStatus+" Total Groups: "+groupsCount);
            CompanyInfo.idArr.add(companyIDs);
            CompanyInfo.nameArr.add(companyName);
            CompanyInfo.companyAddressArr.add(companyAddress);
            CompanyInfo.companyContactArr.add(companyContact);
            CompanyInfo.companyEmailArr.add(companyEmail);
            CompanyInfo.companyCityArr.add(companyCity);
            CompanyInfo.contactNumbertArr.add(contactNumber);
            CompanyInfo.groupsCountArr.add(String.valueOf(groupsCount));
            ArrayList<String> temp = new ArrayList<>();
            for(int x=0; x<4 && companyGroup[x] != null; x++)
            {
                temp.add(companyGroup[x]);
            }
            CompanyInfo.groupNamesArr.add(temp);
            table_addcompany.setItems(parseUserList());
            refreshControls();
//            objCompanyInfo.insertCompany(objStmt, objCon,companyIDs, company_name, company_address, company_contact, company_email, company_status,company_city,contactNumber,  companyGroup);
        }
        else
        {
            alert.setAlertType(Alert.AlertType.ERROR);
            alert.setHeaderText("Error!");
            alert.setContentText("Company ID Already Saved.");
            alert.show();
        }
    }


    public void initialize(URL url, ResourceBundle resourceBundle) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnSetup.setStyle("-fx-background-color: #47ab1e");
        CompanyInfo objCompanyInfo = new CompanyInfo();
        btnpane.setOnMouseEntered(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(22);
                btnpane.setMinWidth(20.0);
                btn11.setMinWidth(20.0);

            }
        });
        btnpane.setOnMouseExited(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(14);
                btnpane.setMinWidth(10.0);
                btn11.setMinWidth(10.0);
            }
        });

        inner_anchor.setLeftAnchor(inner_anchor, 280d);
        drawer.setSidePane(nav_setup);
        drawer.open();
        ArrayList<String> newCitylist = new ArrayList<>();
        MysqlCon objctsMysqlConnc = new MysqlCon();
        Statement objectStatmnt = objctsMysqlConnc.stmt;
        Connection objectConnnection = objctsMysqlConnc.con;
        newCitylist = objCompanyInfo.get_AllCities(objectStatmnt,objectConnnection);
        String[] City_possiblee= new String[newCitylist.size()];
        for(int i =0; i < newCitylist.size();i++)
        {
            City_possiblee[i] = newCitylist.get(i);
        }
        TextFields.bindAutoCompletion(txt_city,City_possiblee);

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        company_id.setCellValueFactory(new PropertyValueFactory<>("id"));
        company_name.setCellValueFactory(new PropertyValueFactory<>("name"));
        groups_count.setCellValueFactory(new PropertyValueFactory<>("groupsCount"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_addcompany.setItems(parseUserList());
        CompanyInfo.table_addcompany = table_addcompany;
        CompanyInfo.txtCompanyId = txt_compID;
        CompanyInfo.txtCity = txt_city;
        CompanyInfo.txtCompanyAddress = txt_compAddress;
        CompanyInfo.txtContactPerson = txt_contactPerson;
        CompanyInfo.txtCompanyEmail = txt_compEmail;
        CompanyInfo.txtCompanyName = txt_compName;
        CompanyInfo.txtContactNum = txt_contactNum;
        CompanyInfo.txtCompGroup1 = compGroup1;
        CompanyInfo.txtCompGroup2 = compGroup2;
        CompanyInfo.txtCompGroup3 = compGroup3;
        CompanyInfo.txtCompGroup4 = compGroup4;
        CompanyInfo.txtCompGroup5 = compGroup5;
        CompanyInfo.btn1 = buttn1;
        CompanyInfo.btn2 = buttn2;
        CompanyInfo.btn3 = buttn3;
        CompanyInfo.btn4 = buttn4;
        CompanyInfo.btn5 = buttn5;
        CompanyInfo.btnAdd = add_company;
        CompanyInfo.btnCancel = btn_edit_cancel;
        CompanyInfo.btnUpdate = btn_edit_update;
    }

    public static ObservableList<CompanyInfo> parseUserList(){
        CompanyInfo objCompanyInfo = new CompanyInfo();
        companyDetails = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> companyData = objCompanyInfo.getAddedCompanies();
        for (int i = 0; i < companyData.size(); i++)
        {
            companyDetails.add(new CompanyInfo(String.valueOf(i+1), ((companyData.get(i).get(0) == null || companyData.get(i).get(0).equals("")) ? "N/A" : companyData.get(i).get(0)), companyData.get(i).get(1), companyData.get(i).get(2)));
        }
        return companyDetails;
    }

    @FXML
    void checkKey(KeyEvent event) {
        if(event.getCode() == KeyCode.TAB)
        {
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    add_company.requestFocus();
                }
            });
        }
    }

    @FXML
    void addClicked(KeyEvent event) {
        if(event.getCode() == KeyCode.ENTER)
        {
            add_company.fire();
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    txt_compID.requestFocus();
                }
            });
        }
    }

    @FXML
    void editCancel(ActionEvent event) {
        refreshControls();
        add_company.setVisible(true);
        btn_edit_cancel.setVisible(false);
        btn_edit_update.setVisible(false);
    }

    @FXML
    void editUpdate(ActionEvent event) {
        String companyIDs = txt_compID.getText();
        String companyName = txt_compName.getText();
        String companyAddress = txt_compAddress.getText();
        String companyContact = txt_contactPerson.getText();
        String companyEmail = txt_compEmail.getText();
        String companyCity = txt_city.getText();
        String contactNumber = txt_contactNum.getText();
        String companyStatus = "Active";
        String[] companyGroup = new String[4];

        if(groupsCount > 0)
        {
            companyGroup[0] = compGroup1.getText();
        }
        if(groupsCount > 1)
        {
            companyGroup[1] = compGroup2.getText();
        }
        if(groupsCount > 2)
        {
            companyGroup[2] = compGroup3.getText();
        }
        if(groupsCount > 3)
        {
            companyGroup[3] = compGroup4.getText();
        }
        if(groupsCount > 4)
        {
            companyGroup[4] = compGroup5.getText();
        }

        MysqlCon objMysqlCon1 = new MysqlCon();
        Statement objStmt1 = objMysqlCon1.stmt;
        Connection objCon1 = objMysqlCon1.con;
        CompanyInfo objCompInfo = new CompanyInfo();
        String comp = objCompInfo.confirmNewId(objStmt1, objCon1,companyIDs);

        if(!comp.equals(companyIDs) || comp.equals(""))
        {
            CompanyInfo.idArr.set(srNo, companyIDs);
            CompanyInfo.nameArr.set(srNo, companyName);
            CompanyInfo.companyAddressArr.set(srNo, companyAddress);
            CompanyInfo.companyContactArr.set(srNo, companyContact);
            CompanyInfo.companyEmailArr.set(srNo, companyEmail);
            CompanyInfo.companyCityArr.set(srNo, companyCity);
            CompanyInfo.contactNumbertArr.set(srNo, contactNumber);
            CompanyInfo.groupsCountArr.set(srNo, String.valueOf(groupsCount));
            ArrayList<String> temp = new ArrayList<>();
            for(int x=0; x<4 && companyGroup[x] != null; x++)
            {
                temp.add(companyGroup[x]);
            }
            CompanyInfo.groupNamesArr.set(srNo, temp);
            table_addcompany.setItems(parseUserList());
            refreshControls();
        }
        else
        {
            alert.setAlertType(Alert.AlertType.ERROR);
            alert.setHeaderText("Error!");
            alert.setContentText("Company ID Already Saved.");
            alert.show();
        }
        add_company.setVisible(true);
        btn_edit_cancel.setVisible(false);
        btn_edit_update.setVisible(false);
    }

    public void hideGroup1() throws IOException
    {
        compGroup1.clear();
        compGroup1.setVisible(false);
        buttn1.setVisible(false);
        groupsCount--;
    }
    public void hideGroup2() throws IOException
    {
        compGroup2.clear();
        compGroup2.setVisible(false);
        buttn2.setVisible(false);
        groupsCount--;
    }
    public void hideGroup3() throws IOException
    {
        compGroup3.clear();
        compGroup3.setVisible(false);
        buttn3.setVisible(false);
        groupsCount--;
    }
    public void hideGroup4() throws IOException
    {
        compGroup4.clear();
        compGroup4.setVisible(false);
        buttn4.setVisible(false);
        groupsCount--;
    }
    public void hideGroup5() throws IOException
    {
        compGroup5.clear();
        compGroup5.setVisible(false);
        buttn5.setVisible(false);
        groupsCount--;
    }

    public void follow() throws IOException
    {
        if (drawer.isOpened()) {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(-250);
            transition.setByY(0);
            transition.setCycleCount(1);
            transition.play();
            drawer.close();
            FA_Icon.setGlyphName("ANGLE_DOUBLE_RIGHT");
            FA_Icon.setGlyphSize(14);
            //inner_anchor.setRightAnchor(inner_anchor,1d);
            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(-90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(1.1000);
            scale.setToY(1);

            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();

        }
        else {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(250);
            transition.setByY(0);
            transition.setCycleCount(1);
            FA_Icon.setGlyphName("ANGLE_DOUBLE_LEFT");
            FA_Icon.setGlyphSize(14);
            drawer.open();

            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(0.99947777);
            scale.setToY(1);

            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();
        }
    }

    public void cancelClicked() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_company.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }



    public void addGroup()
    {
        if(groupsCount == 0)
        {
            compGroup1.setVisible(true);
            buttn1.setVisible(true);
        }
        else if(groupsCount == 1)
        {
            compGroup2.setVisible(true);
            buttn2.setVisible(true);
        }
        else if(groupsCount == 2)
        {
            compGroup3.setVisible(true);
            buttn3.setVisible(true);
        }
        else if(groupsCount == 3)
        {
            compGroup4.setVisible(true);
            buttn4.setVisible(true);
        }
        else if(groupsCount == 4)
        {
            compGroup5.setVisible(true);
            buttn5.setVisible(true);
        }
        groupsCount++;
    }

    public void saveClicked() throws IOException {
//        String companyIDs = txt_compID.getText();
//        String company_name = txt_compName.getText();
//        String company_address = txt_compAddress.getText();
//        String company_contact = txt_contactPerson.getText();
//        String company_email = txt_compEmail.getText();
//        String company_city = txt_city.getText();
//        String contactNumber = txt_contactNum.getText();
//        String company_status = "Active";
//        String[] companyGroup = new String[4];
//
//        if(groupsCount > 0)
//        {
//            companyGroup[0] = compGroup1.getText();
//        }
//        if(groupsCount > 1)
//        {
//            companyGroup[1] = compGroup2.getText();
//        }
//        if(groupsCount > 2)
//        {
//            companyGroup[2] = compGroup3.getText();
//        }
//        if(groupsCount > 3)
//        {
//            companyGroup[3] = compGroup4.getText();
//        }
//        if(groupsCount > 4)
//        {
//            companyGroup[4] = compGroup5.getText();
//        }
//        int companyIDs = Integer.parseInt(companyIDs);
//
//        MysqlCon objMysqlCon1 = new MysqlCon();
//        Statement objStmt1 = objMysqlCon1.stmt;
//        Connection objCon1 = objMysqlCon1.con;
//        CompanyInfo objCompInfo = new CompanyInfo();
//        String comp = objCompInfo.cnfrmNewCompID(objStmt1, objCon1,companyIDs);
//
//        if(!comp.equals(companyIDs))
//        {
//            MysqlCon objMysqlCon = new MysqlCon();
//            Statement objStmt = objMysqlCon.stmt;
//            Connection objCon = objMysqlCon.con;
//            CompanyInfo objCompanyInfo = new CompanyInfo();
//            objCompanyInfo.insertCompany(objStmt, objCon,companyIDs, company_name, company_address, company_contact, company_email, company_status,company_city,contactNumber,  companyGroup);
//        }
//        else
//        {
//            a.setAlertType(Alert.AlertType.ERROR);
//            a.setHeaderText("Error!");
//            a.setContentText("Company ID Already Saved.");
//            a.show();
//        }

        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        CompanyInfo objCompanyInfo = new CompanyInfo();
        objCompanyInfo.insertCompany(objStmt, objCon);

        Parent root = FXMLLoader.load(getClass().getResource("/view/view_company.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    public void refreshControls()
    {
        this.txt_compID.setText("");
        this.txt_compName.setText("");
        this.txt_compAddress.setText("");
        this.txt_compEmail.setText("");
        this.txt_contactNum.setText("");
        this.txt_contactPerson.setText("");
        this.txt_city.setText("");
        this.compGroup1.setText("");
        this.compGroup2.setText("");
        this.compGroup3.setText("");
        this.compGroup4.setText("");
        this.compGroup5.setText("");
        this.compGroup1.setVisible(false);
        this.compGroup2.setVisible(false);
        this.compGroup3.setVisible(false);
        this.compGroup4.setVisible(false);
        this.compGroup5.setVisible(false);
        this.buttn1.setVisible(false);
        this.buttn2.setVisible(false);
        this.buttn3.setVisible(false);
        this.buttn4.setVisible(false);
        this.buttn5.setVisible(false);
    }

    public JFXTextField getTxt_city() {
        return txt_city;
    }

    public void setTxt_city(JFXTextField txt_city) {
        this.txt_city = txt_city;
    }

    public JFXTextField getTxt_compID() {
        return txt_compID;
    }

    public void setTxt_compID(JFXTextField txt_compID) {
        this.txt_compID = txt_compID;
    }

    public JFXTextField getTxt_compAddress() {
        return txt_compAddress;
    }

    public void setTxt_compAddress(JFXTextField txt_compAddress) {
        this.txt_compAddress = txt_compAddress;
    }

    public JFXTextField getTxt_contactPerson() {
        return txt_contactPerson;
    }

    public void setTxt_contactPerson(JFXTextField txt_contactPerson) {
        this.txt_contactPerson = txt_contactPerson;
    }

    public JFXTextField getTxt_compEmail() {
        return txt_compEmail;
    }

    public void setTxt_compEmail(JFXTextField txt_compEmail) {
        this.txt_compEmail = txt_compEmail;
    }

    public JFXTextField getTxt_compName() {
        return txt_compName;
    }

    public void setTxt_compName(JFXTextField txt_compName) {
        this.txt_compName = txt_compName;
    }

    public JFXTextField getTxt_contactNum() {
        return txt_contactNum;
    }

    public void setTxt_contactNum(JFXTextField txt_contactNum) {
        this.txt_contactNum = txt_contactNum;
    }

    public JFXTextField getcompGroup1() {
        return compGroup1;
    }

    public void setcompGroup1(JFXTextField compGroup1) {
        compGroup1 = compGroup1;
    }

    public JFXTextField getcompGroup2() {
        return compGroup2;
    }

    public void setcompGroup2(JFXTextField compGroup2) {
        compGroup2 = compGroup2;
    }

    public JFXTextField getcompGroup3() {
        return compGroup3;
    }

    public void setcompGroup3(JFXTextField compGroup3) {
        compGroup3 = compGroup3;
    }

    public JFXTextField getcompGroup4() {
        return compGroup4;
    }

    public void setcompGroup4(JFXTextField compGroup4) {
        compGroup4 = compGroup4;
    }

    public JFXTextField getcompGroup5() {
        return compGroup5;
    }

    public void setcompGroup5(JFXTextField compGroup5) {
        compGroup5 = compGroup5;
    }
}
