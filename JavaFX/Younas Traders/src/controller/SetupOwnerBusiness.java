package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import model.*;
import model.SetupOwnerBusinessInfo;
import org.controlsfx.control.textfield.TextFields;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class SetupOwnerBusiness implements Initializable {

    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private JFXTextField txt_owner_name;

    @FXML
    private JFXTextField txt_owner_contact;

    @FXML
    private JFXTextField txt_owner_address;

    @FXML
    private JFXTextField txt_owner_cnic;

    @FXML
    private JFXTextField txt_working_district;

    @FXML
    private JFXTextField txt_business_name;

    @FXML
    private JFXTextField txt_business_contact;

    @FXML
    private JFXTextField txt_business_address;

    @FXML
    private JFXTextField txt_business_ntn;

    @FXML
    private JFXComboBox<String> txt_business_category;

    @FXML
    private JFXTextField txt_company_name;

//    @FXML
//    private JFXButton btn_cancel;

    @FXML
    private JFXButton btn_submit;

    @FXML
    private ProgressIndicator progress;

    @FXML
    private BorderPane menu_bar;

    private SetupOwnerBusinessInfo objSetupOwnerBusinessInfo;
    private ArrayList<String> districtNames;
    public static JFXTextField txtWorkingDistrict;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        SetupHeader.labelName = "Owner &\nBusiness Setup";
        txtWorkingDistrict = txt_working_district;
        objSetupOwnerBusinessInfo = new SetupOwnerBusinessInfo();
        districtNames = objSetupOwnerBusinessInfo.getDistrictNames();
//        TextFields.bindAutoCompletion(txt_working_district, districtNames);
//        companyNames = objSetupOwnerBusinessInfo.getCompaniesName();
//        TextFields.bindAutoCompletion(txt_company_name, companyNames);
    }

//    @FXML
//    void cancelClicked(ActionEvent event) {
//
//    }

    @FXML
    void submitClicked(ActionEvent event) {
        progress.setVisible(true);

        Thread thread = new Thread(){
            public void run(){
                String response;
                String ownerName = txt_owner_name.getText();
                String ownerContact = txt_owner_contact.getText();
                String ownerAddress = txt_owner_address.getText();
                String ownerCNIC = txt_owner_cnic.getText();
                String businessName = txt_business_name.getText();
                String businessContact = txt_business_contact.getText();
                String businessAddress = txt_business_address.getText();
                String businessNTN = txt_business_ntn.getText();
                String businessCategory = txt_business_category.getValue();
                String workingDistrict = txt_working_district.getText();

                response = objSetupOwnerBusinessInfo.sendData(ownerName, ownerContact, ownerAddress, ownerCNIC, businessName, businessContact, businessAddress, businessNTN, businessCategory, workingDistrict);

                Platform.runLater(new Runnable(){
                    @Override
                    public void run() {
                        if(response.equals("True"))
                        {
                            SetupOwnerCompanyInfo.businessCategory = businessCategory;
                            SetupOwnerDealersInfo.businessCategory = businessCategory;
                            SetupOwnerDealersInfo.districtName = workingDistrict;
                            GlobalVariables.workingDistrict = workingDistrict;
                            String title = "Success";
                            String message = "Owner & Business Record Submitted";
                            GlobalVariables.showNotification(1, title, message);

//                             ------------------------------------ Setting Core File -------------------------
                            String directoryName = "secret-credentials";
                            File directory = new File(directoryName);
                            if (!directory.exists()){
                                directory.mkdir();
                            }
                            PrintWriter writer = null;
                            try {
                                writer = new PrintWriter("secret-credentials/data.txt", "UTF-8");
                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                            writer.println(GlobalVariables.softwareLicId);
                            writer.println(GlobalVariables.softwareLicNumber);
                            writer.println(GlobalVariables.softwareLicRegDate);
                            writer.println(GlobalVariables.softwareLicExpDate);
                            writer.println("Companies");
                            writer.println(workingDistrict);
                            writer.close();
                            String currentDirectory = System.getProperty("user.dir");
                            String command = "cd "+currentDirectory+" & attrib +h +r +s secret-credentials";

                            ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c", command);
                            builder.redirectErrorStream(true);
                            try {
                                Process p = builder.start();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
//                             ------------------------------------ Setting Core File -------------------------

                            SetupHeader.labelName = "Companies\nSelection";
                            Parent root = null;
                            try {
                                root = FXMLLoader.load(getClass().getResource("/view/setup_owner_company.fxml"));
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            GlobalVariables.baseScene.setRoot(root);
                            GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
                        }
                        else
                        {
                            String title = "Error";
                            String message = "Record Not Submitted";
                            GlobalVariables.showNotification(-1, title, message);
                        }
                    }
                    // do your GUI stuff here
                });
            }
        };
        thread.start();

    }
}
