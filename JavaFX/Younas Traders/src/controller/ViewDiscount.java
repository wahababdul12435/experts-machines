package controller;

import com.jfoenix.controls.*;
import com.jfoenix.transitions.hamburger.HamburgerBackArrowBasicTransition;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.util.Duration;
import model.*;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class ViewDiscount implements Initializable
{
    @FXML    private TableView<ViewDiscountInfo> table_viewdiscount;
    @FXML    private TableColumn<ViewDiscountInfo, String> sr_no;
    @FXML    private TableColumn<ViewDiscountInfo, String> approval_id;
    @FXML    private TableColumn<ViewDiscountInfo, String> startDate;
    @FXML    private TableColumn<ViewDiscountInfo, String> endDate;
    @FXML    private TableColumn<ViewDiscountInfo, String> policy_status;
    @FXML    private TableColumn<ViewDiscountInfo, String> entry_date;
    @FXML    private TableColumn<ViewDiscountInfo, String> operations;
    @FXML    private StackPane stackPane;
    @FXML    private BorderPane menu_bar1;
    @FXML    private AnchorPane inner_anchor;
    @FXML    private BorderPane nav_setup;
    @FXML    private Pane btnpane;
    @FXML    private Button btn11;
    @FXML    private FontAwesomeIconView FA_Icon;
    @FXML    private JFXDrawer drawer;
    @FXML    private AnchorPane filter_pane;
    @FXML    private HBox filter_hbox;
    @FXML    private JFXTextField txt_approval_id;
    @FXML    private JFXDatePicker datepick_startdate;
    @FXML    private JFXDatePicker datepick_enddate;
    @FXML    private JFXDatePicker datepick_entryDate;
    @FXML    private JFXComboBox<String> policyStatus;
    @FXML    private HBox summary_hbox;
    @FXML    private Label lbl_total;
    @FXML    private Label lbl_active;
    @FXML    private Label lbl_inactive;

    private int summaryTotal;
    private int summaryActive;
    private int summaryInActive;

    private ArrayList<ArrayList<String>> discountData;
    public ObservableList<ViewDiscountInfo> discountDetails;

    public static String StrngApprovalId;
    public static String StrngstartDate;
    public static String StrngendDate;
    public static String StrngpolicyStatus;
    public static String StrngentryDate;
    public static boolean filter;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle)
    {
        ViewDiscountInfo.stackPane = stackPane;
        btnpane.setOnMouseEntered(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(22);
                btnpane.setMinWidth(20.0);
                btn11.setMinWidth(20.0);

            }
        });
        btnpane.setOnMouseExited(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(14);
                btnpane.setMinWidth(10.0);
                btn11.setMinWidth(10.0);
            }
        });

        inner_anchor.setLeftAnchor(inner_anchor, 280d);
        drawer.setSidePane(nav_setup);
        drawer.open();

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        approval_id.setCellValueFactory(new PropertyValueFactory<>("approval_id"));
        startDate.setCellValueFactory(new PropertyValueFactory<>("startDate"));
        endDate.setCellValueFactory(new PropertyValueFactory<>("endDate"));
        policy_status.setCellValueFactory(new PropertyValueFactory<>("policy_status"));
        entry_date.setCellValueFactory(new PropertyValueFactory<>("entry_date"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));

        try {
            table_viewdiscount.setItems(parseUserList());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        lbl_total.setText("Discount Policies\n"+summaryTotal);
        lbl_active.setText("Active\n"+summaryActive);
        lbl_inactive.setText("In Active\n"+summaryInActive);
    }

    private ObservableList<ViewDiscountInfo> parseUserList() throws ParseException {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        ViewDiscountInfo objVIewDiscountInfo = new ViewDiscountInfo();
        discountDetails = FXCollections.observableArrayList();
        if(filter)
        {
            LocalDate staart = null;
            LocalDate ennd = null;
            LocalDate entry = null;
            discountData  = objVIewDiscountInfo.getDiscountSearch(objStmt, objCon, StrngApprovalId, StrngstartDate, StrngendDate, StrngpolicyStatus, StrngentryDate);
            txt_approval_id.setText(StrngApprovalId);
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");


            if(StrngstartDate != null) {
                String[] splitStartDate = StrngstartDate.split("/");
                Date date = new SimpleDateFormat("MMMM").parse(splitStartDate[1]);
                Calendar cal = Calendar.getInstance();
                cal.setTime(date);
                int im = cal.get(Calendar.MONTH);
                im =+1;
                if(im<=9) {
                    splitStartDate[1] = "0" + im;
                }
                String strtdate_value = splitStartDate[2]+"-"+splitStartDate[1]+"-"+splitStartDate[0];
                staart = LocalDate.parse(strtdate_value, formatter);

            }
            if(StrngendDate != null) {
                String[] splitendDate = StrngendDate.split("/");

                Date date = new SimpleDateFormat("MMMM").parse(splitendDate[1]);
                Calendar cal = Calendar.getInstance();
                cal.setTime(date);
                int im = cal.get(Calendar.MONTH);
                im =+1;
                if(im<=9) {
                    splitendDate[1] = "0" + im;
                }

                String enndddate_value = splitendDate[2]+"-"+splitendDate[1]+"-"+splitendDate[0];
                ennd = LocalDate.parse(enndddate_value, formatter);

            }
            if(StrngentryDate != null) {
                String[] splitEntryDate = StrngentryDate.split("/");

                Date date = new SimpleDateFormat("MMMM").parse(splitEntryDate[1]);
                Calendar cal = Calendar.getInstance();
                cal.setTime(date);
                int im = cal.get(Calendar.MONTH);
                im =+1;
                if(im<=9) {
                    splitEntryDate[1] = "0" + im;
                }

                String entry_date_value = splitEntryDate[2]+"-"+splitEntryDate[1]+"-"+splitEntryDate[0];
                entry = LocalDate.parse(entry_date_value, formatter);
            }
            datepick_enddate.setValue(ennd);
            datepick_startdate.setValue(staart);
            datepick_entryDate.setValue(entry);
            policy_status.setText(StrngpolicyStatus);
            filter = false;
            filter_pane.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
        }
        else
        {
            discountData = objVIewDiscountInfo.getDiscountInfo(objStmt, objCon);
        }

        for (int i = 0; i < discountData.size(); i++)
        {
            summaryTotal++;
            if(discountData.get(i).get(3).equals("Active"))
            {
                summaryActive++;
            }
            else if(discountData.get(i).get(3).equals("In Active"))
            {
                summaryInActive++;
            }
            discountDetails.add(new ViewDiscountInfo(String.valueOf(i+1), discountData.get(i).get(0), ((discountData.get(i).get(1) == null) ? "N/A" : discountData.get(i).get(1)), discountData.get(i).get(2), ((discountData.get(i).get(3) == null) ? "N/A" : discountData.get(i).get(3)), ((discountData.get(i).get(4) == null) ? "N/A" : discountData.get(i).get(4))));
        }
        return discountDetails;
    }


    public void follow() throws IOException
    {
        if (drawer.isOpened()) {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(-250);
            transition.setByY(0);
            transition.setCycleCount(1);
            transition.play();
            drawer.close();
            FA_Icon.setGlyphName("ANGLE_DOUBLE_RIGHT");
            FA_Icon.setGlyphSize(14);
            //inner_anchor.setRightAnchor(inner_anchor,1d);
            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(-90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(1.1000);
            scale.setToY(1);

            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();

        }
        else {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(250);
            transition.setByY(0);
            transition.setCycleCount(1);
            FA_Icon.setGlyphName("ANGLE_DOUBLE_LEFT");
            FA_Icon.setGlyphSize(14);
            drawer.open();

            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(0.99947777);
            scale.setToY(1);

            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();
        }
    }

    @FXML
    void btnSearch(ActionEvent event) {
        StrngApprovalId = txt_approval_id.getText();
        if(datepick_entryDate.getValue() != null)
        {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);
            StrngentryDate = dateFormatter.format(datepick_entryDate.getValue());
        }
        if(datepick_startdate.getValue() != null)
        {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);
            StrngstartDate =dateFormatter.format(datepick_startdate.getValue());
        }
        if(datepick_enddate.getValue() != null)
        {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);
            StrngendDate = dateFormatter.format(datepick_enddate.getValue());
        }
        StrngpolicyStatus = policyStatus.getValue();


        if(StrngpolicyStatus == null)
        {
            StrngpolicyStatus = "All";
        }
        filter = true;

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/view_discount.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void btnReset(ActionEvent event) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/view_discount.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    public void addDiscount() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/discount_policy.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
    @FXML
    void exportExcel(ActionEvent event) throws IOException {
        Workbook workbook = new HSSFWorkbook();
        Sheet spreadsheet = workbook.createSheet("data");
        Row row = spreadsheet.createRow(0);

        for (int j = 0; j < table_viewdiscount.getColumns().size()-1; j++) {
            row.createCell(j).setCellValue(table_viewdiscount.getColumns().get(j).getText());
        }

        for (int i = 0; i < table_viewdiscount.getItems().size(); i++) {
            row = spreadsheet.createRow(i + 1);
            for (int j = 0; j < table_viewdiscount.getColumns().size()-1; j++) {
                if(table_viewdiscount.getColumns().get(j).getCellData(i) != null) {
                    row.createCell(j).setCellValue(table_viewdiscount.getColumns().get(j).getCellData(i).toString());
                }
                else {
                    row.createCell(j).setCellValue("");
                }
            }
        }

        FileOutputStream fileOut = new FileOutputStream("data/View_Discount.xls");
        workbook.write(fileOut);
        fileOut.close();
    }
}
