package controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.Separator;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import model.GlobalVariables;

import java.net.URL;
import java.util.ResourceBundle;

public class Footer implements Initializable {

    @FXML
    private AnchorPane rootpane;

    @FXML
    private HBox menu_bar;

    @FXML
    private Label lbl_software_lic_id;

    @FXML
    private Label lbl_connect_status;

    @FXML
    private Label lbl_users_active;

    @FXML
    private Separator sep_user;

    @FXML
    private Label lbl_backup;

    @FXML
    private ProgressIndicator progress_backup;

    @FXML
    private Separator sep_backup;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        GlobalVariables.lblSoftwareLicId = lbl_software_lic_id;
        GlobalVariables.lblConnectionStatus = lbl_connect_status;
        GlobalVariables.sepUsers = sep_user;
        GlobalVariables.lblUsersActive = lbl_users_active;
        GlobalVariables.lblDataBackup = lbl_backup;
        GlobalVariables.progressBackup = progress_backup;
        GlobalVariables.sepBackup = sep_backup;
        lbl_software_lic_id.setText("Software Lic Id ("+GlobalVariables.softwareLicId+")");

        if(GlobalVariables.isConnectedToServer)
        {
            GlobalVariables.lblConnectionStatus.setText("Connected");
            GlobalVariables.sepUsers.setVisible(true);
            GlobalVariables.lblUsersActive.setVisible(true);
            GlobalVariables.lblUsersActive.setManaged(true);
        }
        else
        {
            GlobalVariables.lblConnectionStatus.setText("Not Connected");
            GlobalVariables.sepUsers.setVisible(false);
            GlobalVariables.lblUsersActive.setVisible(false);
            GlobalVariables.lblUsersActive.setManaged(false);
        }
    }
}
