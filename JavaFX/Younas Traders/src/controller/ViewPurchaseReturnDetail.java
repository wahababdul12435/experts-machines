package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTimePicker;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.util.StringConverter;
import model.*;
import org.controlsfx.control.textfield.TextFields;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

public class ViewPurchaseReturnDetail implements Initializable {

    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private JFXTextField txt_return_id;

    @FXML
    private JFXTextField txt_company_name;

    @FXML
    private JFXComboBox<String> txt_return_user;

    @FXML
    private JFXDatePicker txt_return_date;

    @FXML
    private JFXTimePicker txt_return_time;

    @FXML
    private JFXTextField txt_product_name;

    @FXML
    private JFXComboBox<String> txt_product_batch;

    @FXML
    private JFXTextField txt_product_quantity;

    @FXML
    private JFXTextField txt_return_price;

    @FXML
    private JFXComboBox<String> txt_return_reason;

    @FXML
    private JFXButton btn_cancel_edit;

    @FXML
    private JFXButton btn_update_edit;

    @FXML
    private JFXButton btn_add;

    @FXML
    private HBox summary_hbox1;

    @FXML
    private Label lbl_items_returned;

    @FXML
    private Label lbl_return_price;

    @FXML
    private TableView<ViewPurchaseReturnDetailInfo> table_purchasereturndetaillog;

    @FXML
    private TableColumn<ViewPurchaseReturnDetailInfo, String> sr_no;

    @FXML
    private TableColumn<ViewPurchaseReturnDetailInfo, String> product_name;

    @FXML
    private TableColumn<ViewPurchaseReturnDetailInfo, String> batch_no;

    @FXML
    private TableColumn<ViewPurchaseReturnDetailInfo, String> return_quantity;

    @FXML
    private TableColumn<ViewPurchaseReturnDetailInfo, String> return_price;

    @FXML
    private TableColumn<ViewPurchaseReturnDetailInfo, String> return_reason;

    @FXML
    private TableColumn<ViewPurchaseReturnDetailInfo, String> operations;

    @FXML
    private JFXButton cancelBtn;

    @FXML
    private JFXButton saveBtn;

    @FXML
    private JFXButton btn_print_return;

    @FXML
    private StackPane stackPane;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    @FXML
    private BorderPane brdrpane_navfinances;

    @FXML
    private BorderPane menu_bar;

    private static ObservableList<ViewPurchaseReturnDetailInfo> returnDetails;
    private static ViewPurchaseReturnDetailInfo objViewPurchaseReturnDetailInfo;
    private UserInfo objUserInfo;
    private ProductInfo objProductInfo;
    private CompanyInfo objCompanyInfo;
    ArrayList<String> returnBasicData;
    ArrayList<String> userNames;
    static ArrayList<ArrayList<String>> returnData;
    ArrayList<ArrayList<String>> usersData;
    ArrayList<String> invoicePrintData;
    public static ArrayList<ArrayList<String>> completeCompaniesData;
    public static ArrayList<ArrayList<String>> completeProductData;
    public static ArrayList<ArrayList<String>> productsCompaniesDetail;
    public static ArrayList<ArrayList<String>> completeBatchData;
    public static ArrayList<ArrayList<String>> bonusDetail;
    public static ArrayList<ArrayList<String>> discountDetail;
    static ArrayList<String> selectedBatchIds;
    static ArrayList<String> selectedBatchNos;
    static ArrayList<String> selectedBatchQty;
    private static String currentInvoiceId;
    private static String selectedBatch;
    //    private static String selectedBatchQty;
    public static String selectedProductId;
    public static String selectedProductPrice;
    ArrayList<String> productNames;
    ArrayList<String> companyNames;
    ArrayList<String> productQty;
    static SimpleDateFormat fmt = new SimpleDateFormat("dd/MMM/yyyy");

    static float itemsReturned = 0;
    static float returnPrice = 0;

    MysqlCon objMysqlCon1;
    MysqlCon objMysqlCon2;
    MysqlCon objMysqlCon3;
    static Statement objStmt1;
    Statement objStmt2;
    Statement objStmt3;
    Connection objCon;
    public ArrayList<InvoiceInfo> invoiceData;

    public static Button btnView;
    public static int srNo;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnPurchase.setStyle("-fx-background-color: #47ab1e");
        objMysqlCon1 = new MysqlCon();
        objMysqlCon2 = new MysqlCon();
        objMysqlCon3 = new MysqlCon();
        objStmt1 = objMysqlCon1.stmt;
        objStmt2 = objMysqlCon2.stmt;
        objStmt3 = objMysqlCon3.stmt;
        objCon = objMysqlCon1.con;
        objUserInfo = new UserInfo();
        objProductInfo = new ProductInfo();
        objCompanyInfo = new CompanyInfo();
        returnData = new ArrayList<>();

        if(!ViewPurchaseReturnInfo.orderId.equals(""))
        {
            currentInvoiceId = ViewPurchaseReturnInfo.orderId;
            ViewPurchaseReturnInfo.orderId = "";
        }

        ViewPurchaseReturnDetailInfo.productIdArr = new ArrayList<>();
        ViewPurchaseReturnDetailInfo.productNameArr = new ArrayList<>();
        ViewPurchaseReturnDetailInfo.batchNoArr = new ArrayList<>();
        ViewPurchaseReturnDetailInfo.returnQuantityArr = new ArrayList<>();
        ViewPurchaseReturnDetailInfo.returnPriceArr = new ArrayList<>();
        ViewPurchaseReturnDetailInfo.returnReasonArr = new ArrayList<>();

        objViewPurchaseReturnDetailInfo = new ViewPurchaseReturnDetailInfo(currentInvoiceId);
        objViewPurchaseReturnDetailInfo.getPurchaseReturnDetailInfo(objStmt1, objCon);

        invoiceData = new ArrayList<>();
        usersData = objUserInfo.getUserWithIds(objStmt1, objCon);
        userNames = GlobalVariables.getArrayColumn(usersData, 2);
        txt_return_user.getItems().addAll(userNames);

        txt_return_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        ViewPurchaseReturnDetailInfo.txtProductName = txt_product_name;
        ViewPurchaseReturnDetailInfo.txtBatchNo = txt_product_batch;
        ViewPurchaseReturnDetailInfo.txtReturnQuantity = txt_product_quantity;
        ViewPurchaseReturnDetailInfo.txtReturnPrice = txt_return_price;
        ViewPurchaseReturnDetailInfo.txtReturnReason = txt_return_reason;
        ViewPurchaseReturnDetailInfo.btnAdd = btn_add;
        ViewPurchaseReturnDetailInfo.btnCancel = btn_cancel_edit;
        ViewPurchaseReturnDetailInfo.btnUpdate = btn_update_edit;

        returnBasicData = objViewPurchaseReturnDetailInfo.getPurchaseReturnBasicInfo(objStmt1, objCon);
        txt_return_id.setText(returnBasicData.get(0));
        txt_company_name.setText(returnBasicData.get(1));
        txt_return_user.setValue(returnBasicData.get(2));

        completeCompaniesData = objCompanyInfo.getCompanyNames();
        productsCompaniesDetail = objProductInfo.getCompanyProductsData(objStmt1, objCon);
        completeProductData = objProductInfo.getProductsDetail(objStmt1, objCon);
        productNames = GlobalVariables.getArrayColumn(completeProductData, 1);
        productQty = GlobalVariables.getArrayColumn(completeProductData, 2);
        TextFields.bindAutoCompletion(txt_product_name, productNames);
        completeBatchData = objProductInfo.getAllBatchesDetail(objStmt1, objCon);

        if(returnBasicData.get(3) != null)
        {
            txt_return_date.setValue(GlobalVariables.LOCAL_DATE(returnBasicData.get(3)));
        }

        SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
        Date date = null;

        if(returnBasicData.get(4) != null)
        {
            try {
                date = parseFormat.parse(returnBasicData.get(4));
            } catch (ParseException e) {
                e.printStackTrace();
            }
//            System.out.println(parseFormat.format(date) + " = " + displayFormat.format(date));
//            System.out.println(LocalTime.parse(displayFormat.format(date)));
            txt_return_time.setValue(LocalTime.parse(displayFormat.format(date)));
        }

        txt_product_name.focusedProperty().addListener(new ChangeListener<Boolean>()
        {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue)
            {
                if (newPropertyValue)
                {
//                    System.out.println("Textfield on focus");
                }
                else
                {
                    selectedProductId = getProductId(completeProductData, txt_product_name.getText());
                    String quant = txt_product_quantity.getText();
                    if(quant == null || quant.equals(""))
                    {
                        quant = "0";
                    }
                    txt_product_batch.getItems().clear();
                    txt_product_batch.getItems().addAll(getProductsBatch(completeBatchData, selectedProductId));
                    txt_return_price.setText(String.valueOf(Float.parseFloat(GlobalVariables.getDataFrom2D(completeProductData, selectedProductId, 0, 3)) * Integer.parseInt(quant)));
//                    txt_product_batch.setValue(selectedBatch);
                }
            }
        });

        txt_product_quantity.textProperty().addListener((observable, oldValue, newValue) -> {
            txt_return_price.setText(computeReturnPrice());
        });
        companyNames = GlobalVariables.getArrayColumn(completeCompaniesData, 1);
        TextFields.bindAutoCompletion(txt_company_name, companyNames);

        itemsReturned = 0;
        returnPrice = 0;

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
//        product_id.setCellValueFactory(new PropertyValueFactory<>("productId"));
        product_name.setCellValueFactory(new PropertyValueFactory<>("productName"));
        batch_no.setCellValueFactory(new PropertyValueFactory<>("batchNo"));
        return_quantity.setCellValueFactory(new PropertyValueFactory<>("returnQuantity"));
        return_price.setCellValueFactory(new PropertyValueFactory<>("returnPrice"));
        return_reason.setCellValueFactory(new PropertyValueFactory<>("returnReason"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_purchasereturndetaillog.setItems(parseUserList());
        ViewPurchaseReturnDetailInfo.table_purchasereturndetaillog = table_purchasereturndetaillog;
        lbl_items_returned.setText("Items Returned\n"+String.format("%,.0f", itemsReturned));
        lbl_return_price.setText("Return Price\n"+"Rs."+String.format("%,.2f", returnPrice)+"/-");
    }

    public static ObservableList<ViewPurchaseReturnDetailInfo> parseUserList() {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        returnDetails = FXCollections.observableArrayList();

        returnData = objViewPurchaseReturnDetailInfo.loadReturnTable();
        itemsReturned = 0;
        returnPrice = 0;
        for (int i = 0; i < returnData.size(); i++)
        {
            itemsReturned += (returnData.get(i).get(2) != null && !returnData.get(i).get(2).equals("-")) ? Integer.parseInt(returnData.get(i).get(2)) : 0;
            returnPrice += (returnData.get(i).get(3) != null && !returnData.get(i).get(3).equals("-")) ? Float.parseFloat(returnData.get(i).get(3)) : 0;
            returnDetails.add(new ViewPurchaseReturnDetailInfo(String.valueOf(i+1), returnData.get(i).get(0), returnData.get(i).get(1), String.format("%,.0f", Float.parseFloat(returnData.get(i).get(2))), String.format("%,.2f", Float.parseFloat(returnData.get(i).get(3))), returnData.get(i).get(4)));
        }
        return returnDetails;
    }

    @FXML
    void batchChange(ActionEvent event) {
        getBatchPrice(completeBatchData, txt_product_batch.getValue(), selectedProductId);
        txt_return_price.setText(computeReturnPrice());
    }

    @FXML
    void btnAdd(ActionEvent event) {
        String productName = txt_product_name.getText();
        String batchNo = txt_product_batch.getValue();
        String quantity = txt_product_quantity.getText();
        String stReturnPrice = txt_return_price.getText();
        String returnReason = txt_return_reason.getValue();
        ViewPurchaseReturnDetailInfo.productIdArr.add(selectedProductId);
        ViewPurchaseReturnDetailInfo.productNameArr.add(productName);
        ViewPurchaseReturnDetailInfo.batchNoArr.add(batchNo);
        ViewPurchaseReturnDetailInfo.returnQuantityArr.add(quantity);
        ViewPurchaseReturnDetailInfo.returnPriceArr.add(stReturnPrice);
        ViewPurchaseReturnDetailInfo.returnReasonArr.add(returnReason);
        table_purchasereturndetaillog.setItems(parseUserList());
        ViewPurchaseReturnDetailInfo.table_purchasereturndetaillog = table_purchasereturndetaillog;
        lbl_items_returned.setText("Items Returned\n"+String.format("%,.0f", itemsReturned));
        lbl_return_price.setText("Return Price\n"+String.format("%,.2f", returnPrice));

        txt_product_name.clear();
        txt_product_batch.getItems().clear();
        txt_product_batch.setValue(null);
        txt_product_quantity.clear();
        txt_return_price.clear();
        txt_return_reason.setValue(null);
    }

    @FXML
    void btnCancelEdit(ActionEvent event) {
        txt_product_name.clear();
        txt_product_batch.getItems().clear();
        txt_product_batch.setValue(null);
        txt_product_quantity.clear();
        txt_return_price.clear();
        txt_return_reason.setValue(null);

        btn_add.setVisible(true);
        btn_cancel_edit.setVisible(false);
        btn_update_edit.setVisible(false);
    }

    @FXML
    void btnUpdateEdit(ActionEvent event) {
        String productName = txt_product_name.getText();
        String batchNo = txt_product_batch.getValue();
        String quantity = txt_product_quantity.getText();
        String stReturnPrice = txt_return_price.getText();
        String returnReason = txt_return_reason.getValue();
        ViewPurchaseReturnDetailInfo.productIdArr.set(srNo, selectedProductId);
        ViewPurchaseReturnDetailInfo.productNameArr.set(srNo, productName);
        ViewPurchaseReturnDetailInfo.batchNoArr.set(srNo, batchNo);
        ViewPurchaseReturnDetailInfo.returnQuantityArr.set(srNo, quantity);
        ViewPurchaseReturnDetailInfo.returnPriceArr.set(srNo, stReturnPrice);
        ViewPurchaseReturnDetailInfo.returnReasonArr.set(srNo, returnReason);
        table_purchasereturndetaillog.setItems(parseUserList());
        ViewPurchaseReturnDetailInfo.table_purchasereturndetaillog = table_purchasereturndetaillog;
        lbl_items_returned.setText("Items Returned\n"+String.format("%,.0f", itemsReturned));
        lbl_return_price.setText("Return Price\n"+String.format("%,.2f", returnPrice));

        txt_product_name.clear();
        txt_product_batch.getItems().clear();
        txt_product_batch.setValue(null);
        txt_product_quantity.clear();
        txt_return_price.clear();
        txt_return_reason.setValue(null);

        btn_add.setVisible(true);
        btn_cancel_edit.setVisible(false);
        btn_update_edit.setVisible(false);
    }

    @FXML
    void cancelClicked(ActionEvent event) {

    }

    @FXML
    void follow(MouseEvent event) {

    }

    @FXML
    void printReturn(ActionEvent event) {

    }

    @FXML
    void productChange(KeyEvent event) {

    }

    @FXML
    void saveClicked(ActionEvent event) {
        String companyId = GlobalVariables.getIdFrom2D(completeCompaniesData, txt_company_name.getText(), 1);
        String returnUser = "0";
        String returnDate = "";
        String returnTime = "";
        if(txt_return_user.getValue() != null && !txt_return_user.getValue().equals(""))
        {
            returnUser = GlobalVariables.getIdFrom2D(usersData, txt_return_user.getValue(), 2);
        }
        else
        {
            returnUser = "0";
        }

        Date selectedDate = null;
        if(txt_return_date.getValue() != null)
        {
            returnDate = txt_return_date.getValue().toString();
            try {
                selectedDate = GlobalVariables.sdf.parse(returnDate);
                returnDate = GlobalVariables.sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        else
        {
            returnDate = "";
        }
        Date _24HourDt = null;

        if(txt_return_time.getValue() != null)
        {
            returnTime = txt_return_time.getValue().toString();
            try {
                _24HourDt = GlobalVariables._24HourSDF.parse(returnTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            returnTime = GlobalVariables._12HourSDF.format(_24HourDt);
        }
        objViewPurchaseReturnDetailInfo.updateBasic(objStmt1, objCon, companyId, String.valueOf(returnPrice), returnUser, returnDate, returnTime);
        objViewPurchaseReturnDetailInfo.updateReturn(objStmt1, objCon);

        ViewPurchaseReturnInfo.orderId = currentInvoiceId;
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/view_purchase_return.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    public static String getBatchPrice(ArrayList<ArrayList<String>> batchData, String batchNo, String productId)
    {
        for(ArrayList<String> row: batchData) {
            if(row.get(1).equals(batchNo) && row.get(2).equals(productId))
            {
                selectedProductPrice = row.get(3);
//                addedRetailPrice = row.get(2);
                return row.get(0);
            }
        }
        return null;
    }

    public static String getProductId(ArrayList<ArrayList<String>> productsData, String productName)
    {
        for(ArrayList<String> row: productsData) {
            if(row.get(1).equals(productName))
            {
                selectedProductPrice = row.get(3);
//                addedRetailPrice = row.get(2);
                return row.get(0);
            }
        }
        return null;
    }

    public static ArrayList<String> getProductsBatch(ArrayList<ArrayList<String>> batchData, String productId)
    {
        ArrayList<String> columnList = new ArrayList<>();
//        ArrayList<String> stExpiryDate = new ArrayList<>();
//        ArrayList<Date> expiryDate = new ArrayList<>();
//        ArrayList<String> stEntryDate = new ArrayList<>();
//        ArrayList<Date> entryDate = new ArrayList<>();
//        selectedBatchIds = new ArrayList<>();
//        selectedBatchNos = new ArrayList<>();
//        selectedBatchQty = new ArrayList<>();
//        Date entrySelectedDate;
//        Date expirySelectedDate;
//        int selectedIndex = 0;
//        boolean expirySelection = false;
        // Batch Id          0
        // Batch No          1
        // Product Id        2
        // Batch Quantity    3
        // Batch Expiry      4
        // Entry Date        5
        if(batchData!=null)
        {
            for(ArrayList<String> row: batchData) {
                if(row.get(2).equals(productId))
                {
//                    selectedBatchIds.add(row.get(0));
//                    selectedBatchNos.add(row.get(1));
//                    selectedBatchQty.add(row.get(3));
                    columnList.add(row.get(1));
//                    stExpiryDate.add(row.get(4));
//                    stEntryDate.add(row.get(5));
//                    try {
//                        expiryDate.add(fmt.parse(row.get(4)));
//                        entryDate.add(fmt.parse(row.get(5)));
//                    } catch (ParseException e) {
//                        e.printStackTrace();
//                    }

//                    if(batchPolicy.equals("Entry LIFO"))
//                    {
//                        // Entry select max date
//                        entrySelectedDate = Collections.max(entryDate);
//                        expirySelection = false;
//                        selectedIndex = entryDate.indexOf(entrySelectedDate);
//                    }
//                    else if(batchPolicy.equals("Entry FIFO"))
//                    {
//                        // Entry select min date
//                        entrySelectedDate = Collections.min(entryDate);
//                        expirySelection = false;
//                        selectedIndex = entryDate.indexOf(entrySelectedDate);
//                    }
//                    else if(batchPolicy.equals("Expiry LIFO"))
//                    {
//                        // Expiry select max date
//                        expirySelectedDate = Collections.max(expiryDate);
//                        expirySelection = true;
//                        selectedIndex = entryDate.indexOf(expirySelectedDate);
//                    }
//                    else if(batchPolicy.equals("Expiry FIFO"))
//                    {
//                        // Expiry select min date
//                        expirySelectedDate = Collections.min(expiryDate);
//                        expirySelection = true;
//                        selectedIndex = entryDate.indexOf(expirySelectedDate);
//                    }
//                    selectedBatch = columnList.get(selectedIndex);
                }
            }
        }
        return columnList;
    }

    private String computeReturnPrice() {
        String quant = txt_product_quantity.getText();
        if(quant == null || quant.equals(""))
        {
            quant = "0";
        }

        return String.valueOf(Float.parseFloat(selectedProductPrice) * Integer.parseInt(quant));
    }

}
