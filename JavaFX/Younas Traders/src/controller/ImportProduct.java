package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.util.Duration;
import model.*;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

public class ImportProduct implements Initializable {
    @FXML    private TableView<ViewProductsInfo> table_viewproducts;
    @FXML    private TableColumn<ViewProductsInfo, String> sr_no;
    @FXML    private TableColumn<ViewProductsInfo, String> product_id;
    @FXML    private TableColumn<ViewProductsInfo, String> company_name;
    @FXML    private TableColumn<ViewProductsInfo, String> company_group;
    @FXML    private TableColumn<ViewProductsInfo, String> product_name;
    @FXML    private TableColumn<ViewProductsInfo, String> product_type;
    @FXML    private TableColumn<ViewProductsInfo, String> retial_price;
    @FXML    private TableColumn<ViewProductsInfo, String> trade_price;
    @FXML    private TableColumn<ViewProductsInfo, String> purchase_price;
    @FXML    private TableColumn<ViewProductsInfo, String> purchase_discount;
    @FXML    private TableColumn<ViewProductsInfo, String> status;
    @FXML    private TableColumn<ViewProductsInfo, String> operations;

    @FXML    private BorderPane menu_bar1;
    @FXML    private AnchorPane inner_anchor;
    @FXML    private BorderPane nav_setup;
    @FXML    private Pane btnpane;
    @FXML    private Button btn11;
    @FXML    private FontAwesomeIconView FA_Icon;
    @FXML    private JFXDrawer drawer;

    @FXML    private AnchorPane filter_pane;
    @FXML    private HBox filter_hbox;
    @FXML    private JFXTextField txt_product_id;
    @FXML    private JFXTextField txt_product_name;
    @FXML    private JFXComboBox<String> txt_product_type;
    @FXML    private JFXComboBox<String> txt_company_name;
    @FXML    private JFXComboBox<String> txt_group_name;
    @FXML    private JFXComboBox<String> txt_product_status;
    @FXML    private StackPane stackPane;
    @FXML    private HBox summary_hbox;
    @FXML    private Label lbl_total;
    @FXML    private Label lbl_active;
    @FXML    private Label lbl_inactive;
    @FXML
    private JFXButton btn_add;
    @FXML
    private JFXButton btn_export;

    public ObservableList<ViewProductsInfo> productsDetail;
    PendingOrders objPendingOrders;
    MysqlCon objMysqlCon;
    Statement objStmt;
    Connection objCon;
    private int summaryTotal;
    private int summaryActive;
    private int summaryInActive;

    private String selectedCompanyId;
    private String selectedGroupId;

    private ArrayList<ArrayList<String>> companiesData = new ArrayList<>();
    private ArrayList<ArrayList<String>> groupsData = new ArrayList<>();
    private ArrayList<String> chosenGroupIds = new ArrayList<>();

    public static String txtProductId = "";
    public static String txtProductName = "";
    public static String txtProductType = "All";
    public static String txtCompanyId = "All";
    public static String txtCompanyName = "";
    public static String txtGroupId = "All";
    public static String txtGroupName = "";
    public static String txtProductStatus = "";
    public static boolean filter;

    public static Button btnView;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        //btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnSetup.setStyle("-fx-background-color: #47ab1e");
        objMysqlCon = new MysqlCon();
        objStmt = objMysqlCon.stmt;
        objCon = objMysqlCon.con;
        ViewProductsInfo.stackPane = stackPane;

        UpdateProductInfo objUpdateProductInfo = new UpdateProductInfo();
        companiesData = objUpdateProductInfo.getCompaniesData();
        groupsData = objUpdateProductInfo.getCompanyGroupsData();
        ArrayList<String> chosenCompanyNames = getArrayColumn(companiesData, 1);
        txt_company_name.getItems().addAll(chosenCompanyNames);

        if(!GlobalVariables.userType.equals("Admin"))
        {
            if(!GlobalVariables.rightCreate)
            {
                btn_add.setVisible(false);
                AnchorPane.setRightAnchor(btn_export, 15.0);
            }
        }

        btnpane.setOnMouseEntered(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(22);
                btnpane.setMinWidth(20.0);
                btn11.setMinWidth(20.0);

            }
        });
        btnpane.setOnMouseExited(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(14);
                btnpane.setMinWidth(10.0);
                btn11.setMinWidth(10.0);
            }
        });
        inner_anchor.setLeftAnchor(inner_anchor, 270d);
        drawer.setSidePane(nav_setup);
        drawer.open();


        //table_viewproducts.setItems(parseUserList());
        lbl_total.setText("Products\n"+String.format("%,.0f", (float)summaryTotal));
        lbl_active.setText("Active\n"+String.format("%,.0f", (float)summaryActive));
        lbl_inactive.setText("In Active\n"+String.format("%,.0f", (float)summaryInActive));
    }

    public void follow() throws IOException
    {
        if (drawer.isOpened()) {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(-250);
            transition.setByY(0);
            transition.setCycleCount(1);
            transition.play();
            drawer.close();
            FA_Icon.setGlyphName("ANGLE_DOUBLE_RIGHT");
            FA_Icon.setGlyphSize(14);
            //inner_anchor.setRightAnchor(inner_anchor,1d);

            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(-90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(1.1000);
            scale.setToY(1);

            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();
            //
            //inner_anchor.setLeftAnchor(inner_anchor, 5d);
        } else {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(250);
            transition.setByY(0);
            transition.setCycleCount(1);
            FA_Icon.setGlyphName("ANGLE_DOUBLE_LEFT");
            FA_Icon.setGlyphSize(14);
            drawer.open();

            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(0.99947777);
            scale.setToY(1);

            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();
        }
    }



    @FXML
    void btnSearch(ActionEvent event) {
        txtProductId = txt_product_id.getText();
        txtProductName = txt_product_name.getText();
        txtProductType = txt_product_type.getValue();
        if(txtProductType == null)
        {
            txtProductType = "All";
        }
        txtCompanyName = txt_company_name.getValue();
        txtGroupName = txt_group_name.getValue();
        txtProductStatus = txt_product_status.getValue();
        if(txtProductStatus == null)
        {
            txtProductStatus = "All";
        }
        filter = true;

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/view_products.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void btnReset(ActionEvent event) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/view_products.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
    ImportProductInfo objimportProdInfo;

    @FXML
    void exportProductExcel(ActionEvent event) throws IOException {

        FileInputStream fis=new FileInputStream(new File("D:\\prodList15-10-23.xls"));
//creating workbook instance that refers to .xls file
        HSSFWorkbook wb=new HSSFWorkbook(fis);
//creating a Sheet object to retrieve the object
        HSSFSheet sheet=wb.getSheetAt(0);
//evaluating cell type
        ArrayList<ArrayList<String>> table = new ArrayList<ArrayList<String>>();
        FormulaEvaluator formulaEvaluator=wb.getCreationHelper().createFormulaEvaluator();

        int loopsint  = 0;

        for(Row row: sheet)     //iteration over row using for each loop
        {
            ArrayList<String> getdata = new ArrayList<String>();
            loopsint=0;
            for(Cell cell: row)    //iteration over cell using for each loop
            {
                loopsint++;
                switch(formulaEvaluator.evaluateInCell(cell).getCellType())
                {
                    case Cell.CELL_TYPE_NUMERIC:   //field that represents numeric cell type
//getting the value of the cell as a number

                        double getvalue = cell.getNumericCellValue();
                        if(loopsint==11 || loopsint==12 || loopsint==13)
                        {
                            getdata.add(String.valueOf(getvalue));
                        }
                        else {
                            int IntValue = (int) getvalue;
                            getdata.add(String.valueOf(IntValue));
                        }
                        break;
                    case Cell.CELL_TYPE_STRING:    //field that represents string cell type
//getting the value of the cell as a string

                            getdata.add(String.valueOf(cell.getStringCellValue()));

                        break;
                }
            }
            ArrayList<String> cars = new ArrayList<String>();
            cars = getdata;
            table.add(getdata);
            System.out.println();
        }
        objimportProdInfo = new ImportProductInfo();

        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        objimportProdInfo.insertclicked(objStmt, objCon,table);


    }

    @FXML
    void exportBatchExcel(ActionEvent event) throws IOException {
        /*Workbook workbook = new HSSFWorkbook();
        Sheet spreadsheet = workbook.createSheet("data");
        Row row = spreadsheet.createRow(0);

        for (int j = 0; j < table_viewproducts.getColumns().size()-1; j++) {
            row.createCell(j).setCellValue(table_viewproducts.getColumns().get(j).getText());
        }

        for (int i = 0; i < table_viewproducts.getItems().size(); i++) {
            row = spreadsheet.createRow(i + 1);
            for (int j = 0; j < table_viewproducts.getColumns().size()-1; j++) {
                if(table_viewproducts.getColumns().get(j).getCellData(i) != null) {
                    row.createCell(j).setCellValue(table_viewproducts.getColumns().get(j).getCellData(i).toString());
                }
                else {
                    row.createCell(j).setCellValue("");
                }
            }
        }

        FileOutputStream fileOut = new FileOutputStream("data/View_Products.xls");
        workbook.write(fileOut);
        fileOut.close();*/

        FileInputStream fis=new FileInputStream(new File("D:\\batchwise tabros-saffron.xls"));
//creating workbook instance that refers to .xls file
        HSSFWorkbook wb=new HSSFWorkbook(fis);
//creating a Sheet object to retrieve the object
        HSSFSheet sheet=wb.getSheetAt(0);
//evaluating cell type
        int loopint  = 0;
        int loopint1 = 0;
        int prod_code = 0;
        String valll = "";
        ArrayList<ArrayList<String>> table = new ArrayList<ArrayList<String>>();
        FormulaEvaluator formulaEvaluator=wb.getCreationHelper().createFormulaEvaluator();
        objimportProdInfo = new ImportProductInfo();
        for(Row row: sheet)     //iteration over row using for each loop
        {
            ArrayList<String> getdata = new ArrayList<String>();
            loopint=0;
            loopint1 = 0;
            for(Cell cell: row)    //iteration over cell using for each loop
            {
                loopint++;
                switch(formulaEvaluator.evaluateInCell(cell).getCellType())
                {
                    case Cell.CELL_TYPE_NUMERIC:   //field that represents numeric cell type
//getting the value of the cell as a number

                        double getvalue = cell.getNumericCellValue();
                        if(loopint==5) {
                            String hhh = cell.toString();
                            String[] splitstring_value = hhh.split("-");
                            String stringDate= splitstring_value[0]+"/"+splitstring_value[1]+"/"+splitstring_value[2];
                            getdata.add(stringDate);
                            SimpleDateFormat formatter = new SimpleDateFormat("dd/MMM/yyyy");
                            SimpleDateFormat timeformatter = new SimpleDateFormat("hh:mm aa");
                            Date date = new Date();
                            String newdate = formatter.format(date);
                            String newtime = timeformatter.format(date);
                            getdata.add(newdate);
                            getdata.add(newtime);
                            getdata.add(newdate);
                            getdata.add(newtime);
                        }
                        int IntValue = (int) getvalue;
                        if(loopint==2) {
                            int length = String.valueOf(IntValue).length();
                            if (length == 1 || length == 2) {
                                valll = String.format("%03d", IntValue);
                                getdata.add(String.valueOf(valll));
                            }
                            else {
                                getdata.add(String.valueOf(IntValue));
                            }

                        }
                        if(loopint==3) {
                            getdata.add(String.valueOf(getvalue));
                            getdata.add(String.valueOf(getvalue));
                        }
                        if(loopint==4) {
                            getdata.add(String.valueOf(IntValue));
                            getdata.add(String.valueOf(0));
                        }

                        break;
                    case Cell.CELL_TYPE_STRING:    //field that represents string cell type
//getting the value of the cell as a string
                        String getstringvalue = cell.getStringCellValue();
                        char[] ch = getstringvalue.toCharArray();

                        char nnn = ch[0];
                        if(Character.isDigit(nnn)) {
                            if(loopint==1) {
                                String[] splitString = getstringvalue.split("-");
                                int IntegerValue = Integer.parseInt(splitString[0]);
                                String newvalu = splitString[1].trim();
                                //String[] splitinteger1 = splitString[1].split(" ");
                                int IntegerValue1 = Integer.parseInt(newvalu);
                                String str1 = String.valueOf(IntegerValue);
                                String str2 = String.valueOf(IntegerValue1);
                                String combineString = str1+str2;
                                prod_code = Integer.parseInt(combineString);
                            }
                        }
                        if(prod_code != 0)
                        {
                            ArrayList<String> prods_details = new ArrayList<String>();
                            String Value_prodCode = String.valueOf(prod_code);
                            MysqlCon objMysqlCon = new MysqlCon();
                             Statement objStmt = objMysqlCon.stmt;
                            Connection objCon = objMysqlCon.con;
                            prods_details = objimportProdInfo.gettableID(objStmt, objCon,Value_prodCode);
                            getdata.add(prods_details.get(0));
                            getdata.add(prods_details.get(1));

                            prod_code=0;
                        }
                        else {
                            getdata.add(String.valueOf(cell.getStringCellValue()));
                        }

                        break;
                }
            }
            String prods_details ="";
            MysqlCon objMysqlCon = new MysqlCon();
            Statement objStmt = objMysqlCon.stmt;
            Connection objCon = objMysqlCon.con;
            prods_details = objimportProdInfo.getPrevQuantity(objStmt, objCon,getdata.get(0));



            int prevQuantity = Integer.parseInt(prods_details);
            int productsIds = Integer.parseInt(getdata.get(0));
            int productsquants = Integer.parseInt(getdata.get(5));
            int totalQuantities = prevQuantity + productsquants;

            MysqlCon objectMysqlCon = new MysqlCon();
            Statement objectStmt = objectMysqlCon.stmt;
            Connection objectCon = objectMysqlCon.con;
            objimportProdInfo.addtition_quantFROMStock(objectStmt, objectCon,productsIds,totalQuantities);


            ArrayList<String> cars = new ArrayList<String>();
            cars = getdata;
            table.add(getdata);
        }


        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        objimportProdInfo.insertBatchData(objStmt, objCon,table);


    }

    public ArrayList<String> getArrayColumn(ArrayList<ArrayList<String>> companiesData, int columnIndex)
    {
        ArrayList<String> columnList = new ArrayList<>();
        for(ArrayList<String> row: companiesData) {
            columnList.add(row.get(columnIndex));
        }
        return columnList;
    }

    public ArrayList<String> getCompanyGroups(ArrayList<ArrayList<String>> groupsData, String companyId)
    {
        ArrayList<String> columnList = new ArrayList<>();
        chosenGroupIds = new ArrayList<>();
        for(ArrayList<String> row: groupsData) {
            if(row.get(2).equals(companyId))
            {
                chosenGroupIds.add(row.get(0));
                columnList.add(row.get(1));
            }
        }
        return columnList;
    }

    public String getCompanyId(ArrayList<ArrayList<String>> companiesData, String companyName)
    {
        for(ArrayList<String> row: companiesData) {
            if(row.get(1).equals(companyName))
            {
                selectedCompanyId = row.get(0);
                return selectedCompanyId;
            }
        }
        return null;
    }

    @FXML
    void companyChange(ActionEvent event) {
        int index = txt_company_name.getSelectionModel().getSelectedIndex();
        if(index >=1)
        {
            selectedCompanyId = companiesData.get(index-1).get(0);
            txtCompanyId = selectedCompanyId;
            txt_group_name.getItems().clear();
            ArrayList<String> chosenGroupNames = getCompanyGroups(groupsData, selectedCompanyId);
            if(chosenGroupNames.size() > 0)
            {
                txt_group_name.getItems().addAll("All");
                txt_group_name.getItems().addAll(chosenGroupNames);
                txt_group_name.setValue("All");
            }
            txtGroupId = "All";
//            if(chosenGroupNames.size() >= 0)
//            {
//                txt_group_name.setValue(chosenGroupNames.get(0));
//            }
//            selectedGroupId = chosenGroupIds.get(0);
//            txtGroupId = selectedGroupId;
        }
        else
        {
            txtCompanyId = "All";
            txtGroupId = "All";
            txt_group_name.getItems().clear();
            txt_group_name.getItems().addAll("All");
            txt_group_name.setValue("All");
        }
    }

    @FXML
    void groupChange(ActionEvent event) {
        int index = txt_group_name.getSelectionModel().getSelectedIndex();
        if(index >=1)
        {
            selectedGroupId = chosenGroupIds.get(index-1);
            txtGroupId = selectedGroupId;
        }
        else
        {
            txtGroupId = "All";
        }
    }

    @FXML
    void addProduct() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/enter_product.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
}
