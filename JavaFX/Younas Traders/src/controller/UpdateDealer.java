package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import model.*;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

public class UpdateDealer implements Initializable {
    @FXML
    private Label label_update_dealer;

    @FXML
    private JFXTextField txt_dealer_Id;

    @FXML
    private JFXTextField txt_dealer_name;

    @FXML
    private JFXComboBox<String> txt_dealer_status;

    @FXML
    private JFXButton dialog_dealer_close;

    @FXML
    private JFXButton dialog_dealer_update;

    @FXML
    private JFXTextField txt_dealer_contact;

    @FXML
    private JFXTextField txt_dealer_address;

    @FXML
    private JFXTextField txt_dealer_cnic;

    @FXML
    private JFXComboBox<String> txt_dealer_type;

    @FXML
    private JFXComboBox<String> txt_dealer_city;

    @FXML
    private JFXComboBox<String> txt_dealer_area;
    @FXML    private JFXDatePicker expiry_9;
    @FXML    private CheckBox chkbx_lic_9;
    @FXML    private JFXTextField license_9;
    @FXML    private JFXDatePicker expiry_10;
    @FXML    private CheckBox chkbx_lic_10;
    @FXML    private JFXTextField license_10;
    @FXML    private JFXDatePicker expiry_11;
    @FXML    private CheckBox chkbx_lic_11;
    @FXML    private JFXTextField license_11;

    public static String dealerTableId = "";
    public static String dealerId = "";
    public static String dealerName = "";
    public static String dealerContact = "";
    public static String dealerAddress = "";
    public static String dealerCnic = "";
    public static String dealerAreaId = "";
    public static String dealerArea = "";
    public static String dealerCity = "";
    public static String dealerType = "";
    public static String dealerLic9Num = "";
    public static String dealerLic9Exp = "";
    public static String dealerLic10Num = "";
    public static String dealerLic10Exp = "";
    public static String dealerLic11Num = "";
    public static String dealerLic11Exp = "";
    public static String dealerStatus = "";

    private String selectedCityId;
    private String selectedAreaId;

    private ArrayList<ArrayList<String>> citiesData = new ArrayList<>();
    private ArrayList<ArrayList<String>> areasData = new ArrayList<>();
    private ArrayList<String> chosenAreaIds = new ArrayList<>();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ViewDealerInfo.lblUpdate = label_update_dealer;
        UpdateDealerInfo objUpdateDealerInfo = new UpdateDealerInfo();
        citiesData = objUpdateDealerInfo.getCitiesData();
        areasData = objUpdateDealerInfo.getCityAreasData();

        txt_dealer_Id.setText(dealerId);
        txt_dealer_name.setText(dealerName);
        ArrayList<String> chosenCityNames = getArrayColumn(citiesData, 1);
        txt_dealer_city.getItems().addAll(chosenCityNames);
        txt_dealer_city.setValue(dealerCity);
        txt_dealer_area.getItems().addAll(getCityAreas(areasData, getCityId(citiesData, dealerCity)));
        txt_dealer_area.setValue(dealerArea);
        getAreaId(areasData, dealerArea, selectedCityId);
        txt_dealer_contact.setText(dealerContact);
        txt_dealer_address.setText(dealerAddress);
        txt_dealer_cnic.setText(dealerCnic);
        txt_dealer_area.setValue(dealerArea);
        txt_dealer_type.setValue(dealerType);

        if(!dealerLic9Num.isEmpty() ) {
            if( !dealerLic9Num.equals("0")){
                license_9.setText(dealerLic9Num);
                if (!dealerLic9Exp.isEmpty()) {
                    expiry_9.setValue(LOCAL_DATE(dealerLic9Exp));
                }
                chkbx_lic_9.setSelected(true);
                license_10.setVisible(false);
                expiry_10.setVisible(false);
                license_11.setVisible(false);
                expiry_11.setVisible(false);
            }
        }
        if(!dealerLic10Num.isEmpty()) {
            if(!dealerLic10Num.equals("0")){
                license_10.setText(dealerLic10Num);
                if (!dealerLic10Exp.isEmpty()) {
                    expiry_10.setValue(LOCAL_DATE(dealerLic10Exp));
                }
                chkbx_lic_10.setSelected(true);
                license_9.setVisible(false);
                expiry_9.setVisible(false);
                license_11.setVisible(false);
                expiry_11.setVisible(false);
            }
        }
        if(!dealerLic11Num.isEmpty()) {
            if(!dealerLic11Num.equals("0")){
                license_11.setText(dealerLic11Num);
                if (!dealerLic11Exp.isEmpty()) {
                    expiry_11.setValue(LOCAL_DATE(dealerLic11Exp));
                }
                chkbx_lic_11.setSelected(true);
                license_10.setVisible(false);
                expiry_10.setVisible(false);
                license_9.setVisible(false);
                expiry_9.setVisible(false);
            }
        }
        txt_dealer_status.setValue(dealerStatus);
        dialog_dealer_close.setOnAction((action)->closeDialog());
        dialog_dealer_update.setOnAction((action)-> {
            try {
                updateDealer();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
    public static final LocalDate LOCAL_DATE (String dateString) {
        //String[] splitdate = dateString.split("-");
        //String dateValue = splitdate[2]+"/"+splitdate[1]+"/"+splitdate[0];
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(dateString, formatter);
        return localDate;
    }
    public void closeDialog()
    {
        ViewDealerInfo.dialog.close();
        ViewDealerInfo.stackPane.setVisible(false);
    }

    public void updateDealer() throws IOException {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        DealerInfo objDealerInfo = new DealerInfo();
        dealerId = txt_dealer_Id.getText();
        dealerName = txt_dealer_name.getText();
        dealerContact = txt_dealer_contact.getText();
        dealerAddress = txt_dealer_address.getText();
        dealerCnic = txt_dealer_cnic.getText();
        dealerType = txt_dealer_type.getValue();
        if(!license_9.getText().isEmpty())
        {
            dealerLic9Num = license_9.getText();
        }
        else
        {
            dealerLic9Num = "0";
        }
        if (expiry_9.getValue() != null) {
            dealerLic9Exp = expiry_9.getValue().toString();
        }
        else
        {
            dealerLic9Exp = "0";
        }
        if(!license_10.getText().isEmpty())
        {
            dealerLic10Num = license_10.getText();
        }
        else
        {
            dealerLic10Num = "0";
        }
        if (expiry_10.getValue() != null) {
            dealerLic10Exp = expiry_10.getValue().toString();
        }
        else
        {
            dealerLic10Exp = "0";
        }
        if(!license_11.getText().isEmpty())
        {
            dealerLic11Num = license_11.getText();
        }
        else
        {
            dealerLic11Num = "0";
        }
        if (expiry_11.getValue() != null) {
            dealerLic11Exp = expiry_11.getValue().toString();
        }
        else
        {
            dealerLic11Exp = "0";
        }

        dealerStatus = txt_dealer_status.getValue();
        objDealerInfo.updateDealer(objStmt, objCon, dealerTableId, dealerId, selectedAreaId, dealerName, dealerContact, dealerAddress, dealerCnic, dealerType, dealerLic9Num, dealerLic9Exp,dealerLic10Num, dealerLic10Exp, dealerLic11Num, dealerLic11Exp, dealerStatus);

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/view_dealers.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        root = FXMLLoader.load(getClass().getResource("/view/view_dealers.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void cityChange(ActionEvent event) {
        int index = txt_dealer_city.getSelectionModel().getSelectedIndex();
        selectedCityId = citiesData.get(index).get(0);
        txt_dealer_area.getItems().clear();
        ArrayList<String> chosenGroupNames = getCityAreas(areasData, selectedCityId);
        txt_dealer_area.getItems().addAll(chosenGroupNames);
        txt_dealer_area.setValue(chosenGroupNames.get(0));
        selectedAreaId = chosenAreaIds.get(0);
    }

    @FXML
    void areaChange(ActionEvent event) {
        int index = txt_dealer_area.getSelectionModel().getSelectedIndex();
        if(index >= 0)
        {
            selectedAreaId = chosenAreaIds.get(index);
        }
    }

    @FXML
    void chkbox9_click(ActionEvent event) throws IOException
    {
        if(chkbx_lic_9.isSelected())
        {
            license_9.setVisible(true);
            expiry_9.setVisible(true);
        }
        if(!chkbx_lic_9.isSelected())
        {
            license_9.setText("");
            expiry_9.setValue(null);
            license_9.setVisible(false);
            expiry_9.setVisible(false);
        }
    }
    @FXML
    void chkbox10_click(ActionEvent event) throws IOException
    {
        if(chkbx_lic_10.isSelected())
        {
            license_10.setVisible(true);
            expiry_10.setVisible(true);
        }
        if(!chkbx_lic_10.isSelected())
        {
            license_10.setText("");
            expiry_10.setValue(null);
            license_10.setVisible(false);
            expiry_10.setVisible(false);
        }
    }
    @FXML
    void chkbox11_click(ActionEvent event) throws IOException
    {
        if(chkbx_lic_11.isSelected())
        {
            license_11.setVisible(true);
            expiry_11.setVisible(true);
        }
        if(!chkbx_lic_11.isSelected())
        {
            license_11.setText("");
            expiry_11.setValue(null);
            license_11.setVisible(false);
            expiry_11.setVisible(false);
        }
    }

    public ArrayList<String> getArrayColumn(ArrayList<ArrayList<String>> citiesData, int columnIndex)
    {
        ArrayList<String> columnList = new ArrayList<>();
        for(ArrayList<String> row: citiesData) {
            columnList.add(row.get(columnIndex));
        }
        return columnList;
    }

    public ArrayList<String> getCityAreas(ArrayList<ArrayList<String>> areasData, String companyId)
    {
        ArrayList<String> columnList = new ArrayList<>();
        chosenAreaIds = new ArrayList<>();
        for(ArrayList<String> row: areasData) {
            if(row.get(2).equals(companyId))
            {
                chosenAreaIds.add(row.get(0));
                columnList.add(row.get(1));
            }
        }
        return columnList;
    }

    public String getCityId(ArrayList<ArrayList<String>> citiesData, String companyName)
    {
        for(ArrayList<String> row: citiesData) {
            if(row.get(1).equals(companyName))
            {
                selectedCityId = row.get(0);
                return selectedCityId;
            }
        }
        return null;
    }

    public String getAreaId(ArrayList<ArrayList<String>> groupData, String groupName, String companyId)
    {
        for(ArrayList<String> row: groupData) {
            if(row.get(1).equals(groupName) && row.get(2).equals(companyId))
            {
                selectedAreaId = row.get(0);
                return selectedAreaId;
            }
        }
        return null;
    }
}
