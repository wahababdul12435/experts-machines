package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.util.StringConverter;
import model.*;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.ResourceBundle;

public class UpdateCompanyFinance implements Initializable {

    @FXML
    private Label label_update;

    @FXML
    private JFXTextField txt_company_name;

    @FXML
    private JFXDatePicker txt_date;

    @FXML
    private JFXTextField txt_amount;

    @FXML
    private JFXComboBox<String> txt_type;

    @FXML
    private JFXButton dialog_company_finance_close;

    @FXML
    private JFXButton dialog_company_finance_update;

    public static String paymentId = "";
    public static String companyId = "";
    public static String companyName = "";
    public static String date = "";
    public static String preCashAmount = "";
    public static String cashAmount = "";
    public static String preCashType = "";
    public static String cashType = "";
    public static String returnPage = "";

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        txt_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }

            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        txt_company_name.setText(companyName);
        txt_date.setValue(LOCAL_DATE(date));
        txt_amount.setText(cashAmount);
        txt_type.setValue(cashType);
        dialog_company_finance_close.setOnAction((action)->closeDialog());
        dialog_company_finance_update.setOnAction((action)->updateCompanyFinance());
    }

    public void closeDialog()
    {
        if(returnPage.equals("View Company Finance"))
        {
            ViewCompanyFinanceInfo.dialog.close();
            ViewCompanyFinanceInfo.stackPane.setVisible(false);
        }
        else
        {
            CompanyFinanceDetailReportInfo.dialog.close();
            CompanyFinanceDetailReportInfo.stackPane.setVisible(false);
        }

    }

    public void updateCompanyFinance() {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        CompanyFinanceDetailReportInfo objCompanyFinanceDetailReportInfo = new CompanyFinanceDetailReportInfo();
        date = txt_date.getValue().toString();
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date selectedDate = null;
        try {
            selectedDate = sdf.parse(date);
            date = sdf2.format(selectedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        int day;
        String stDay = "";
        day = selectedDate.getDay();
        if(day == 0)
            stDay = "Sunday";
        else if(day == 1)
            stDay = "Monday";
        else if(day == 2)
            stDay = "Tuesday";
        else if(day == 3)
            stDay = "Wednesday";
        else if(day == 4)
            stDay = "Thursday";
        else if(day == 5)
            stDay = "Friday";
        else if(day == 6)
            stDay = "Saturday";
        cashAmount = txt_amount.getText();
        cashType = txt_type.getValue();
        objCompanyFinanceDetailReportInfo.updateCompanyFinance(objStmt, objCon, paymentId, companyId, preCashAmount, preCashType, date, stDay, cashAmount, cashType);

        Parent root = null;
        if(returnPage.equals("View Company Finance"))
        {
            try {
                root = FXMLLoader.load(getClass().getResource("/view/view_company_finance.fxml"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else
        {
            try {
                root = FXMLLoader.load(getClass().getResource("/view/company_finance_detail_report.fxml"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    public static final LocalDate LOCAL_DATE (String dateString) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MMM/yyyy");
        LocalDate localDate = LocalDate.parse(dateString, formatter);
        return localDate;
    }
}