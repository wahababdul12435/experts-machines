package controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import jdk.nashorn.internal.objects.Global;
import model.GlobalVariables;
import model.TopHeaderInfo;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class TopHeader implements Initializable {

    @FXML
    private HBox menu_bar;

    @FXML
    private Button btn_setup;

    @FXML
    private Button btn_purchase;

    @FXML
    private Button btn_sales;

    @FXML
    private Button btn_report;

    @FXML
    private Button btn_live;

    @FXML
    private Button btn_finance;

    @FXML
    private Button btn_settings;

    @FXML
    private ImageView img_user;

    public static Button btnSetup;
    public static Button btnPurchase;
    public static Button btnSale;
    public static Button btnReport;
    public static Button btnLive;
    public static Button btnFinance;
    public static Button btnSettings;
    TopHeaderInfo objTopHeaderInfo;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        objTopHeaderInfo = new TopHeaderInfo();
        btnSetup = btn_setup;
        btnPurchase = btn_purchase;
        btnSale = btn_sales;
        btnReport = btn_report;
        btnLive = btn_live;
        btnFinance = btn_finance;
        btnSettings = btn_settings;
        GlobalVariables.btnTopHeaderSale = btn_sales;
        GlobalVariables.btnTopHeaderFinance = btn_finance;
        GlobalVariables.btnTopHeaderSettings = btn_settings;

        int totalBookingReturns = GlobalVariables.findNewBookings + GlobalVariables.findNewReturns;
        if(totalBookingReturns > 0)
        {
            btn_sales.setText("Sales ("+totalBookingReturns+")");
        }
        else
        {
            btn_sales.setText("Sales");
        }

        if(GlobalVariables.findNewDealerFinance > 0)
        {
            btn_finance.setText("Finance ("+GlobalVariables.findNewDealerFinance+")");
        }
        else
        {
            btn_finance.setText("Finance");
        }

        if(GlobalVariables.findNewUserAccounts > 0)
        {
            btn_settings.setText("Settings ("+GlobalVariables.findNewUserAccounts+")");
        }
        else
        {
            btn_settings.setText("Settings");
        }

        if(GlobalVariables.userImage != null && !GlobalVariables.userImage.equals(""))
        {
            File file = new File(GlobalVariables.userImage);
            boolean exists = file.exists();
            if(exists)
            {
                javafx.scene.image.Image image = new Image(file.toURI().toString());
                img_user.setImage(image);
            }
            else
            {
                file = new File("view/images/users/empty_user.jpg");
                javafx.scene.image.Image image = new Image(file.toURI().toString());
                img_user.setImage(image);
            }
        }

//        Circle cir2 = new Circle(250,250,120);
//        Image im = new Image("https://juicylinksmag.files.wordpress.com/2016/02/juliet-ibrahim.jpg",false);
//        cir2.setFill(new ImagePattern(im));
//        cir2.setEffect(new DropShadow(+25d, 0d, +2d, Color.DARKSEAGREEN));
    }

    @FXML
    public void viewMainHome() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/home.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root, GlobalVariables.SCREEN_WIDTH, GlobalVariables.SCREEN_HEIGHT);
        objstage.setScene(objscene);

    }

    @FXML
    public void viewSetup_dashboard() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/dashboard_setup.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    public void viewpurchase_dashboard() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/dashboard_purchase.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);

    }
    @FXML
    public void viewsales_dashboard() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/dashboard_sales1.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);

    }
    @FXML
    public void viewreports_dashboard() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/dashboard_reports.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);

    }
    @FXML
    public void viewlive_dashboard() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/dashboard_live.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);

    }
    @FXML
    public void viewfinance_dashboard() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/dashboard_finance.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    public void viewsettings_dashboard() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/my_account1.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
}
