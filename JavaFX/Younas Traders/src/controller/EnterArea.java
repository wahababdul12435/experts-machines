package controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.util.Duration;
import model.AreaInfo;
import model.CityInfo;
import model.GlobalVariables;
import model.MysqlCon;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class EnterArea implements Initializable {

    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private BorderPane menu_bar1;

    @FXML
    private BorderPane nav_setup;

    @FXML
    private JFXTextField txt_area_id;

    @FXML
    private JFXTextField txt_area_name;

    @FXML
    private JFXComboBox<String> txt_city_name;

    @FXML
    private Button cancelBtn;

    @FXML
    private Button saveBtn;

    @FXML
    private Button btn_add_area;

    @FXML    private Button btn_edit_cancel;
    @FXML    private Button btn_edit_update;

    @FXML
    private TableView<AreaInfo> table_add_area;

    @FXML
    private TableColumn<AreaInfo, String> sr_no;

    @FXML
    private TableColumn<AreaInfo, String> area_id;

    @FXML
    private TableColumn<AreaInfo, String> area_name;

    @FXML
    private TableColumn<AreaInfo, String> city_name;

    @FXML
    private TableColumn<AreaInfo, String> operations;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    public static ObservableList<AreaInfo> areaDetails;
    Alert alert = new Alert(Alert.AlertType.NONE);
    AreaInfo objAreaInfo;
    public static int srNo;
    public static Button btnView;

    public void initialize(URL url, ResourceBundle resourceBundle) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnSetup.setStyle("-fx-background-color: #47ab1e");
        objAreaInfo = new AreaInfo();
        btnpane.setOnMouseEntered(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(22);
                btnpane.setMinWidth(20.0);
                btn11.setMinWidth(20.0);

            }
        });
        btnpane.setOnMouseExited(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(14);
                btnpane.setMinWidth(10.0);
                btn11.setMinWidth(10.0);
            }
        });

        inner_anchor.setLeftAnchor(inner_anchor, 280d);
        drawer.setSidePane(nav_setup);
        drawer.open();

        ArrayList<String> chosenCityNames = objAreaInfo.getSavedCityNames();
        txt_city_name.getItems().addAll(chosenCityNames);

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        area_id.setCellValueFactory(new PropertyValueFactory<>("areaId"));
        area_name.setCellValueFactory(new PropertyValueFactory<>("areaName"));
        city_name.setCellValueFactory(new PropertyValueFactory<>("cityName"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        AreaInfo.table_add_area = table_add_area;
        table_add_area.setItems(parseUserList());
        AreaInfo.txtAreaId = txt_area_id;
        AreaInfo.txtAreaName = txt_area_name;
        AreaInfo.txtCityName = txt_city_name;
        AreaInfo.btnAdd = btn_add_area;
        AreaInfo.btnCancel = btn_edit_cancel;
        AreaInfo.btnUpdate = btn_edit_update;
    }

    public static ObservableList<AreaInfo> parseUserList() {
        AreaInfo objAreaInfo = new AreaInfo();
        areaDetails = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> areaData = objAreaInfo.getAddedArea();
        for (int i = 0; i < areaData.size(); i++)
        {
            areaDetails.add(new AreaInfo(String.valueOf(i+1), ((areaData.get(i).get(0) == null || areaData.get(i).get(0).equals("")) ? "N/A" : areaData.get(i).get(0)), areaData.get(i).get(1), areaData.get(i).get(2)));
        }
        return areaDetails;
    }

    @FXML
    void addArea(ActionEvent event) {
        String areaId = txt_area_id.getText();
        String areaName = txt_area_name.getText();
        String cityId = "0";
        String cityName = "";
        if(txt_city_name.getValue() != null)
        {
            cityId = String.valueOf(objAreaInfo.getSavedCityIds().get(txt_city_name.getSelectionModel().getSelectedIndex()));
            cityName = txt_city_name.getValue();
        }
        String areaStatus = "Active";

        String comp = objAreaInfo.confirmNewId(areaId);

        if(!comp.equals(areaId) || comp.equals("")) {
            MysqlCon objMysqlCon = new MysqlCon();
            Statement objStmt = objMysqlCon.stmt;
            Connection objCon = objMysqlCon.con;
            AreaInfo.areaIdArr.add(areaId);
            AreaInfo.areaNameArr.add(areaName);
            AreaInfo.cityIdArr.add(cityId);
            AreaInfo.cityNameArr.add(cityName);
            table_add_area.setItems(parseUserList());
            refreshControls();
        }
        else
        {
            alert.setAlertType(Alert.AlertType.ERROR);
            alert.setHeaderText("Error!");
            alert.setContentText("Area ID Already Saved.");
            alert.show();
        }
    }

    @FXML
    void editCancel(ActionEvent event) {
        refreshControls();
        btn_add_area.setVisible(true);
        btn_edit_cancel.setVisible(false);
        btn_edit_update.setVisible(false);
    }

    @FXML
    void editUpdate(ActionEvent event) {
        String areaId = txt_area_id.getText();
        String areaName = txt_area_name.getText();
        String cityId = String.valueOf(objAreaInfo.getSavedCityIds().get(txt_city_name.getSelectionModel().getSelectedIndex()));
        String cityName = txt_city_name.getValue();
        String areaStatus = "Active";

        String comp = objAreaInfo.confirmNewId(areaId);

        if(!comp.equals(areaId) || comp.equals("")) {
            MysqlCon objMysqlCon = new MysqlCon();
            Statement objStmt = objMysqlCon.stmt;
            Connection objCon = objMysqlCon.con;
            AreaInfo.areaIdArr.set(srNo, areaId);
            AreaInfo.areaNameArr.set(srNo, areaName);
            AreaInfo.cityIdArr.set(srNo, cityId);
            AreaInfo.cityNameArr.set(srNo, cityName);
            table_add_area.setItems(parseUserList());
            refreshControls();
        }
        else
        {
            alert.setAlertType(Alert.AlertType.ERROR);
            alert.setHeaderText("Error!");
            alert.setContentText("Area ID Already Saved.");
            alert.show();
        }

        btn_add_area.setVisible(true);
        btn_edit_cancel.setVisible(false);
        btn_edit_update.setVisible(false);
    }

    @FXML
    void cancelClicked(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_area.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void follow(MouseEvent event) {
        if (drawer.isOpened()) {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(-250);
            transition.setByY(0);
            transition.setCycleCount(1);
            transition.play();
            drawer.close();
            FA_Icon.setGlyphName("ANGLE_DOUBLE_RIGHT");
            FA_Icon.setGlyphSize(14);
            //inner_anchor.setRightAnchor(inner_anchor,1d);
            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(-90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(1.1000);
            scale.setToY(1);

            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();
        }
        else {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(250);
            transition.setByY(0);
            transition.setCycleCount(1);
            FA_Icon.setGlyphName("ANGLE_DOUBLE_LEFT");
            FA_Icon.setGlyphSize(14);
            drawer.open();

            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(0.99947777);
            scale.setToY(1);

            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();
        }
    }

    @FXML
    void saveClicked(ActionEvent event) throws IOException {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        try {
            objAreaInfo.insertArea(objStmt, objCon);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Parent root = FXMLLoader.load(getClass().getResource("/view/view_area.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    public void refreshControls()
    {
        this.txt_area_id.setText("");
        this.txt_area_name.setText("");
        this.txt_city_name.getSelectionModel().clearSelection();
    }

}
