package controller;

import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.util.StringConverter;
import model.GlobalVariables;
import model.MysqlCon;
import model.ViewCompaniesDiscountInfo;
import model.ViewDealersDiscountInfo;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

public class ViewDealersDiscount implements Initializable {

    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private TableView<ViewDealersDiscountInfo> table_viewdealersdiscount;

    @FXML
    private TableColumn<ViewDealersDiscountInfo, String> sr_no;

    @FXML
    private TableColumn<ViewDealersDiscountInfo, String> approval_id;

    @FXML
    private TableColumn<ViewDealersDiscountInfo, String> dealer_id;

    @FXML
    private TableColumn<ViewDealersDiscountInfo, String> dealer_name;

    @FXML
    private TableColumn<ViewDealersDiscountInfo, String> sale_amount;

    @FXML
    private TableColumn<ViewDealersDiscountInfo, String> discount;

    @FXML
    private TableColumn<ViewDealersDiscountInfo, String> start_date;

    @FXML
    private TableColumn<ViewDealersDiscountInfo, String> end_date;

    @FXML
    private TableColumn<ViewDealersDiscountInfo, String> entered_user;

    @FXML
    private TableColumn<ViewDealersDiscountInfo, String> operations;

    @FXML
    private AnchorPane filter_pane;

    @FXML
    private HBox filter_hbox;

    @FXML
    private JFXTextField txt_approval_id;

    @FXML
    private JFXTextField txt_dealer_id;

    @FXML
    private JFXTextField txt_dealer_name;

    @FXML
    private JFXDatePicker txt_start_date;

    @FXML
    private JFXDatePicker txt_end_date;

    @FXML
    private HBox summary_hbox;

    @FXML
    private Label lbl_total;

    @FXML
    private Label lbl_sale_amount;

    @FXML
    private Label lbl_discount;

    @FXML
    private StackPane stackPane;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private BorderPane menu_bar1;

    @FXML
    private BorderPane nav_live;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    public ArrayList<ViewDealersDiscountInfo> summaryData;
    public ObservableList<ViewDealersDiscountInfo> discountDetails;
    ArrayList<ArrayList<String>> discountData;
    ViewDealersDiscountInfo objViewDealersDiscountInfo;
    public static String txtApprovalId = "";
    public static String txtDealerId = "";
    public static String txtDealerName = "";
    public static String txtStartDate = "";
    public static String txtEndDate = "";
    public static boolean filter;

    public static MenuButton btnView;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnLive.setStyle("-fx-background-color: #47ab1e");
        summaryData = new ArrayList<>();
        objViewDealersDiscountInfo = new ViewDealersDiscountInfo();

        txt_start_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        txt_end_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        approval_id.setCellValueFactory(new PropertyValueFactory<>("approvalId"));
        dealer_id.setCellValueFactory(new PropertyValueFactory<>("dealerId"));
        dealer_name.setCellValueFactory(new PropertyValueFactory<>("dealerName"));
        sale_amount.setCellValueFactory(new PropertyValueFactory<>("saleAmount"));
        discount.setCellValueFactory(new PropertyValueFactory<>("discount"));
        start_date.setCellValueFactory(new PropertyValueFactory<>("startDate"));
        end_date.setCellValueFactory(new PropertyValueFactory<>("endDate"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_viewdealersdiscount.setItems(parseUserList());
    }

    private ObservableList<ViewDealersDiscountInfo> parseUserList(){
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        ViewDealersDiscountInfo objViewDealersDiscountInfo = new ViewDealersDiscountInfo();
        discountDetails = FXCollections.observableArrayList();

        if(filter)
        {
            discountData = objViewDealersDiscountInfo.getDealersDiscountSearch(GlobalVariables.objStmt, txtApprovalId, txtDealerId, txtDealerName, txtStartDate, txtEndDate);
            txt_approval_id.setText(txtApprovalId);
            txt_dealer_id.setText(txtDealerId);
            txt_dealer_name.setText(txtDealerName);

            if(txtStartDate != null && !txtStartDate.equals(""))
            {
                txt_start_date.setValue(GlobalVariables.LOCAL_DATE(txtStartDate));
            }
            if(txtEndDate != null && !txtEndDate.equals(""))
            {
                txt_end_date.setValue(GlobalVariables.LOCAL_DATE(txtEndDate));
            }

            filter = false;
            filter_pane.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
        }
        else
        {
            discountData = objViewDealersDiscountInfo.getDealersDiscount(GlobalVariables.objStmt);
        }


        for (int i = 0; i < discountData.size(); i++)
        {
            discountDetails.add(new ViewDealersDiscountInfo(String.valueOf(i+1), discountData.get(i).get(0), discountData.get(i).get(1), discountData.get(i).get(2), ((discountData.get(i).get(3) == null) ? "N/A" : String.format("%,.2f", Float.parseFloat(discountData.get(i).get(3)))), ((discountData.get(i).get(4) == null) ? "N/A" : discountData.get(i).get(4)+"%"), ((discountData.get(i).get(5) == null) ? "N/A" : discountData.get(i).get(5)), discountData.get(i).get(6), discountData.get(i).get(7)));
            summaryData.add(new ViewDealersDiscountInfo(String.valueOf(i+1), discountData.get(i).get(0), discountData.get(i).get(1), discountData.get(i).get(2), ((discountData.get(i).get(3) == null) ? "N/A" : String.format("%,.2f", Float.parseFloat(discountData.get(i).get(3)))), ((discountData.get(i).get(4) == null) ? "N/A" : discountData.get(i).get(4)+"%"), ((discountData.get(i).get(5) == null) ? "N/A" : discountData.get(i).get(5)), discountData.get(i).get(6), discountData.get(i).get(7)));
        }
        return discountDetails;
    }

    @FXML
    void addDiscount(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/discount_policy.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void btnReset(ActionEvent event) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/view_dealers_discount.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void btnSearch(ActionEvent event) {
        txtApprovalId = txt_approval_id.getText();
        txtDealerId = txt_dealer_id.getText();
        txtDealerName = txt_dealer_name.getText();
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date selectedDate = null;
        if(txt_start_date.getValue() != null)
        {
            txtStartDate = txt_start_date.getValue().toString();
            try {
                selectedDate = sdf.parse(txtStartDate);
                txtStartDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if(txt_end_date.getValue() != null)
        {
            txtEndDate = txt_end_date.getValue().toString();
            try {
                selectedDate = sdf.parse(txtEndDate);
                txtEndDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        filter = true;
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/view_dealers_discount.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    public void generateSummary(ArrayList<ViewDealersDiscountInfo> summaryList) throws JRException {
        InputStream objIO = DealerFinanceReport.class.getResourceAsStream("/reports/DealerDiscount.jrxml");
        JasperReport objReport = JasperCompileManager.compileReport(objIO);
        JasperPrint objPrint = JasperFillManager.fillReport(objReport, null, new JRBeanCollectionDataSource(summaryList));
        JasperViewer.viewReport(objPrint, false);
    }

    @FXML
    void printData(ActionEvent event) {
        try {
            generateSummary(summaryData);
        } catch (JRException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void exportExcel(ActionEvent event) throws IOException {
        Workbook workbook = new HSSFWorkbook();
        Sheet spreadsheet = workbook.createSheet("data");
        Row row = spreadsheet.createRow(0);

        for (int j = 0; j < table_viewdealersdiscount.getColumns().size()-1; j++) {
            row.createCell(j).setCellValue(table_viewdealersdiscount.getColumns().get(j).getText());
        }

        for (int i = 0; i < table_viewdealersdiscount.getItems().size(); i++) {
            row = spreadsheet.createRow(i + 1);
            for (int j = 0; j < table_viewdealersdiscount.getColumns().size()-1; j++) {
                if(table_viewdealersdiscount.getColumns().get(j).getCellData(i) != null) {
                    row.createCell(j).setCellValue(table_viewdealersdiscount.getColumns().get(j).getCellData(i).toString());
                }
                else {
                    row.createCell(j).setCellValue("");
                }
            }
        }

        FileOutputStream fileOut = new FileOutputStream("data/view_dealers_discount.xls");
        workbook.write(fileOut);
        fileOut.close();
    }

    @FXML
    void follow(MouseEvent event) {

    }
}
