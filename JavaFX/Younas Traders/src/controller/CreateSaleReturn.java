package controller;

import com.jfoenix.controls.*;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.util.StringConverter;
import model.*;
import org.controlsfx.control.textfield.TextFields;

import java.awt.event.KeyEvent;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

public class CreateSaleReturn implements Initializable {

    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private JFXDatePicker txt_return_date;

    @FXML
    private JFXTextField txt_dealer_id;

    @FXML
    private JFXTextField txt_dealer_name;

    @FXML
    private JFXTextField txt_dealer_contact;

    @FXML
    private JFXTextField txt_dealer_address;

    @FXML
    private JFXTextField txt_dealer_lic;

    @FXML
    private JFXTextField txt_dealer_lic_exp;

    @FXML
    private JFXTextField txt_product_name;

    @FXML
    private JFXComboBox<String> txt_product_batch;

    @FXML
    private JFXTextField txt_product_quantity;

    @FXML
    private JFXTextField txt_return_id;
    @FXML
    private JFXTextField txt_invoiceNum;

    @FXML
    private JFXTextField txt_return_price;

    @FXML
    private JFXComboBox<String> txt_return_reason;

    @FXML
    private JFXButton btn_cancel_edit;

    @FXML
    private JFXButton btn_update_edit;

    @FXML
    private JFXButton btn_add;

    @FXML
    private HBox summary_hbox1;

    @FXML
    private Label lbl_items_returned;

    @FXML
    private Label lbl_return_price;

    @FXML
    private TableView<CreateSaleReturnInfo> table_salereturndetaillog;

    @FXML
    private TableColumn<CreateSaleReturnInfo, String> sr_no;

    @FXML
    private TableColumn<CreateSaleReturnInfo, String> product_name;

    @FXML
    private TableColumn<CreateSaleReturnInfo, String> batch_no;

    @FXML
    private TableColumn<CreateSaleReturnInfo, String> return_quantity;

    @FXML
    private TableColumn<CreateSaleReturnInfo, String> return_price;

    @FXML
    private TableColumn<CreateSaleReturnInfo, String> return_reason;

    @FXML
    private TableColumn<CreateSaleReturnInfo, String> operations;

    @FXML
    private JFXButton cancelBtn;

    @FXML
    private JFXButton saveBtn;

    @FXML
    private JFXButton btn_print_return;

    @FXML
    private StackPane stackPane;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    @FXML
    private BorderPane brdrpane_navfinances;

    @FXML
    private BorderPane menu_bar;

    private static ObservableList<CreateSaleReturnInfo> returnDetails;
    private static CreateSaleReturnInfo objCreateSaleReturnInfo;
    private SaleInvoicePrintInfo objSaleInvoicePrintInfo;
    private UserInfo objUserInfo;
    private ProductInfo objProductInfo;
    private DealerInfo objDealerInfo;
    ArrayList<String> returnBasicData;
    ArrayList<String> dealersName;
    static ArrayList<ArrayList<String>> returnData;
    ArrayList<String> invoicePrintData;
    public static ArrayList<ArrayList<String>> completeDealersData;
    public static ArrayList<ArrayList<String>> completeProductData;
    public static ArrayList<ArrayList<String>> productsCompaniesDetail;
    public static ArrayList<ArrayList<String>> completeBatchData;
    public static ArrayList<ArrayList<String>> bonusDetail;
    public static ArrayList<ArrayList<String>> discountDetail;
    static ArrayList<String> selectedBatchIds;
    static ArrayList<String> selectedBatchNos;
    static ArrayList<String> selectedBatchQty;
    private static String currentInvoiceId;
    private static String selectedBatch;
    //    private static String selectedBatchQty;
    public static String selectedProductId;
    public static String selectedProductPrice;
    ArrayList<String> productNames;
    ArrayList<String> dealerNames;
    ArrayList<String> productQty;
    static SimpleDateFormat fmt = new SimpleDateFormat("dd/MMM/yyyy");

    static float itemsReturned = 0;
    static float returnPrice = 0;

    MysqlCon objMysqlCon1;
    MysqlCon objMysqlCon2;
    MysqlCon objMysqlCon3;
    static Statement objStmt1;
    Statement objStmt2;
    Statement objStmt3;
    Connection objCon;
    public ArrayList<InvoiceInfo> invoiceData;

    public static Button btnView;
    public static int srNo;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnSale.setStyle("-fx-background-color: #47ab1e");
        objMysqlCon1 = new MysqlCon();
        objMysqlCon2 = new MysqlCon();
        objMysqlCon3 = new MysqlCon();
        objStmt1 = objMysqlCon1.stmt;
        objStmt2 = objMysqlCon2.stmt;
        objStmt3 = objMysqlCon3.stmt;
        objCon = objMysqlCon1.con;
        objUserInfo = new UserInfo();
        objProductInfo = new ProductInfo();
        objDealerInfo = new DealerInfo();
        returnData = new ArrayList<>();
        txt_return_date.setValue(LocalDate.now());
        completeDealersData = new ArrayList<>();
        objCreateSaleReturnInfo = new CreateSaleReturnInfo();
        completeDealersData = objCreateSaleReturnInfo.getDealersCompleteData(GlobalVariables.objStmt);
        dealersName = GlobalVariables.getArrayColumn(completeDealersData, 2);
        TextFields.bindAutoCompletion(txt_dealer_name, dealersName);

        CreateSaleReturnInfo.productIdArr = new ArrayList<>();
        CreateSaleReturnInfo.productNameArr = new ArrayList<>();
        CreateSaleReturnInfo.batchNoArr = new ArrayList<>();
        CreateSaleReturnInfo.returnQuantityArr = new ArrayList<>();
        CreateSaleReturnInfo.returnPriceArr = new ArrayList<>();
        CreateSaleReturnInfo.returnReasonArr = new ArrayList<>();

        objSaleInvoicePrintInfo = new SaleInvoicePrintInfo();
        invoicePrintData = objSaleInvoicePrintInfo.getSaleInvoicePrintInfo(objStmt1, objCon);

        invoiceData = new ArrayList<>();

        CreateSaleReturnInfo.txtProductName = txt_product_name;
        CreateSaleReturnInfo.txtBatchNo = txt_product_batch;
        CreateSaleReturnInfo.txtReturnQuantity = txt_product_quantity;
        CreateSaleReturnInfo.txtReturnPrice = txt_return_price;
        CreateSaleReturnInfo.txtReturnReason = txt_return_reason;
        CreateSaleReturnInfo.btnAdd = btn_add;
        CreateSaleReturnInfo.btnCancel = btn_cancel_edit;
        CreateSaleReturnInfo.btnUpdate = btn_update_edit;

        completeDealersData = objDealerInfo.getDealersDetail(objStmt1, objCon);
        productsCompaniesDetail = objProductInfo.getCompanyProductsData(objStmt1, objCon);
        completeProductData = objProductInfo.getProductsDetail(objStmt1, objCon);
        productNames = GlobalVariables.getArrayColumn(completeProductData, 1);
        productQty = GlobalVariables.getArrayColumn(completeProductData, 2);
        TextFields.bindAutoCompletion(txt_product_name, productNames);
        completeBatchData = objProductInfo.getAllBatchesDetail(objStmt1, objCon);
        CreateSaleReturnInfo objreturn= new CreateSaleReturnInfo();
        txt_return_id.setText(objreturn.get_return_id(objStmt1));

        txt_product_name.focusedProperty().addListener(new ChangeListener<Boolean>()
        {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue)
            {
                if (newPropertyValue)
                {
//                    System.out.println("Textfield on focus");
                }
                else
                {
                    if(txt_product_name.getText()!="")
                    {
                        selectedProductId = getProductId(completeProductData, txt_product_name.getText());
                        String quant = txt_product_quantity.getText();
                        if(quant == null || quant.equals(""))
                        {
                            quant = "0";
                        }
                        txt_product_batch.getItems().clear();
                        txt_product_batch.getItems().addAll(getProductsBatch(completeBatchData, selectedProductId));
                        txt_return_price.setText(String.valueOf(Float.parseFloat(GlobalVariables.getDataFrom2D(completeProductData, selectedProductId, 0, 3)) * Integer.parseInt(quant)));
//                    txt_product_batch.setValue(selectedBatch);
                    }

                }
            }
        });

        txt_product_quantity.textProperty().addListener((observable, oldValue, newValue) -> {
            txt_return_price.setText(computeReturnPrice());
        });
        dealerNames = GlobalVariables.getArrayColumn(completeDealersData, 1);
        TextFields.bindAutoCompletion(txt_dealer_name, dealerNames);

        itemsReturned = 0;
        returnPrice = 0;

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
//        product_id.setCellValueFactory(new PropertyValueFactory<>("productId"));
        product_name.setCellValueFactory(new PropertyValueFactory<>("productName"));
        batch_no.setCellValueFactory(new PropertyValueFactory<>("batchNo"));
        return_quantity.setCellValueFactory(new PropertyValueFactory<>("returnQuantity"));
        return_price.setCellValueFactory(new PropertyValueFactory<>("returnPrice"));
        return_reason.setCellValueFactory(new PropertyValueFactory<>("returnReason"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_salereturndetaillog.setItems(parseUserList());
        CreateSaleReturnInfo.table_salereturndetaillog = table_salereturndetaillog;
        lbl_items_returned.setText("Items Returned\n"+String.format("%,.0f", itemsReturned));
        lbl_return_price.setText("Return Price\n"+"Rs."+String.format("%,.2f", returnPrice)+"/-");
    }

    public static ObservableList<CreateSaleReturnInfo> parseUserList() {
        returnDetails = FXCollections.observableArrayList();

        returnData = objCreateSaleReturnInfo.loadReturnTable();
        itemsReturned = 0;
        returnPrice = 0;
        for (int i = 0; i < returnData.size(); i++)
        {
            itemsReturned += (returnData.get(i).get(2) != null && !returnData.get(i).get(2).equals("-")) ? Integer.parseInt(returnData.get(i).get(2)) : 0;
            returnPrice += (returnData.get(i).get(3) != null && !returnData.get(i).get(3).equals("-")) ? Float.parseFloat(returnData.get(i).get(3)) : 0;
            returnDetails.add(new CreateSaleReturnInfo(String.valueOf(i+1), returnData.get(i).get(0), returnData.get(i).get(1), returnData.get(i).get(2), String.format("%,.2f", Float.parseFloat(returnData.get(i).get(3))), returnData.get(i).get(4)));
        }
        return returnDetails;
    }

    @FXML
    void batchChange(ActionEvent event) {
        getBatchPrice(completeBatchData, txt_product_batch.getValue(), selectedProductId);
        txt_return_price.setText(computeReturnPrice());
    }

    @FXML
    void fillDealersDetail(javafx.scene.input.KeyEvent event) {
        if(!txt_dealer_name.getText().equals(""))
        {
            int index = dealersName.indexOf(txt_dealer_name.getText());
            if(index >= 0)
            {
                String mnm = completeDealersData.get(index).get(0);
                txt_dealer_id.setText(completeDealersData.get(index).get(0));
                String selectdDealerID = txt_dealer_id.getText();
                DealerInfo objDealerInfo = new DealerInfo();
                ArrayList<String> dealersDetails = new ArrayList();
                dealersDetails = objDealerInfo.getDealerDetail(selectdDealerID);
                txt_dealer_contact.setText(dealersDetails.get(2));
                txt_dealer_address.setText(dealersDetails.get(4));
                txt_dealer_lic.setText(dealersDetails.get(7));
                txt_dealer_lic_exp.setText(dealersDetails.get(8));

            }
        }

    }
    @FXML
    void productChange(javafx.scene.input.KeyEvent event) {

    }

    @FXML
    void btnAdd(ActionEvent event) {
        CreateSaleReturnInfo.returnId = txt_return_id.getText();
        String productName = txt_product_name.getText();
        String batchNo = txt_product_batch.getValue();
        String quantity = txt_product_quantity.getText();
        String stReturnPrice = txt_return_price.getText();
        String returnReason = txt_return_reason.getValue();
        CreateSaleReturnInfo.productIdArr.add(selectedProductId);
        CreateSaleReturnInfo.productNameArr.add(productName);
        CreateSaleReturnInfo.batchNoArr.add(batchNo);
        CreateSaleReturnInfo.returnQuantityArr.add(quantity);
        CreateSaleReturnInfo.returnPriceArr.add(stReturnPrice);
        CreateSaleReturnInfo.returnReasonArr.add(returnReason);
        table_salereturndetaillog.setItems(parseUserList());
        CreateSaleReturnInfo.table_salereturndetaillog = table_salereturndetaillog;
        lbl_items_returned.setText("Items Returned\n"+String.format("%,.0f", itemsReturned));
        lbl_return_price.setText("Return Price\n"+String.format("%,.2f", returnPrice));

        txt_product_name.clear();
        txt_product_batch.getItems().clear();
        txt_product_batch.setValue(null);
        txt_product_quantity.clear();
        txt_return_price.clear();
        txt_return_reason.setValue(null);
    }

    @FXML
    void btnCancelEdit(ActionEvent event) {
        txt_product_name.clear();
        txt_product_batch.getItems().clear();
        txt_product_batch.setValue(null);
        txt_product_quantity.clear();
        txt_return_price.clear();
        txt_return_reason.setValue(null);

        btn_add.setVisible(true);
        btn_cancel_edit.setVisible(false);
        btn_update_edit.setVisible(false);
    }

    @FXML
    void btnUpdateEdit(ActionEvent event) {
        String productName = txt_product_name.getText();
        String batchNo = txt_product_batch.getValue();
        String quantity = txt_product_quantity.getText();
        String stReturnPrice = txt_return_price.getText();
        String returnReason = txt_return_reason.getValue();
        CreateSaleReturnInfo.productIdArr.set(srNo, selectedProductId);
        CreateSaleReturnInfo.productNameArr.set(srNo, productName);
        CreateSaleReturnInfo.batchNoArr.set(srNo, batchNo);
        CreateSaleReturnInfo.returnQuantityArr.set(srNo, quantity);
        CreateSaleReturnInfo.returnPriceArr.set(srNo, stReturnPrice);
        CreateSaleReturnInfo.returnReasonArr.set(srNo, returnReason);
        table_salereturndetaillog.setItems(parseUserList());
        CreateSaleReturnInfo.table_salereturndetaillog = table_salereturndetaillog;
        lbl_items_returned.setText("Items Returned\n"+String.format("%,.0f", itemsReturned));
        lbl_return_price.setText("Return Price\n"+String.format("%,.2f", returnPrice));

        txt_product_name.clear();
        txt_product_batch.getItems().clear();
        txt_product_batch.setValue(null);
        txt_product_quantity.clear();
        txt_return_price.clear();
        txt_return_reason.setValue(null);

        btn_add.setVisible(true);
        btn_cancel_edit.setVisible(false);
        btn_update_edit.setVisible(false);
    }

    @FXML
    void cancelClicked(ActionEvent event) {

    }
    @FXML
    void fillDealersDetail(KeyEvent event) {

    }
    @FXML
    void follow(MouseEvent event) {

    }
    @FXML
    void printReturn(ActionEvent event) {

    }
    @FXML
    void productChange(KeyEvent event) {

    }

    @FXML
    void saveClicked(ActionEvent event) throws SQLException {
        CreateSaleReturnInfo returnInfoObj = new CreateSaleReturnInfo();

        returnInfoObj.insert_Return_Invoice(GlobalVariables.objStmt);

        String nowdate = LocalDate.now().toString();
        String nowtime = LocalTime.now().toString();
        String returnComment = "Return";
        String returnStatus = "Active";
        String syncStatus="Insert";
        returnInfoObj.saveReturnData(GlobalVariables.objStmt , txt_dealer_id.getText(),returnPrice,returnPrice,txt_invoiceNum.getText(),nowdate,nowtime,GlobalVariables.userId,nowdate,nowtime,GlobalVariables.userId,returnComment,returnStatus,syncStatus,itemsReturned);

    }

    public static ArrayList<String> getProductsBatch(ArrayList<ArrayList<String>> batchData, String productId)
    {
        ArrayList<String> columnList = new ArrayList<>();
//        ArrayList<String> stExpiryDate = new ArrayList<>();
//        ArrayList<Date> expiryDate = new ArrayList<>();
//        ArrayList<String> stEntryDate = new ArrayList<>();
//        ArrayList<Date> entryDate = new ArrayList<>();
//        selectedBatchIds = new ArrayList<>();
//        selectedBatchNos = new ArrayList<>();
//        selectedBatchQty = new ArrayList<>();
//        Date entrySelectedDate;
//        Date expirySelectedDate;
//        int selectedIndex = 0;
//        boolean expirySelection = false;
        // Batch Id          0
        // Batch No          1
        // Product Id        2
        // Batch Quantity    3
        // Batch Expiry      4
        // Entry Date        5
        if(batchData!=null)
        {
            for(ArrayList<String> row: batchData) {
                if(row.get(2).equals(productId))
                {
//                    selectedBatchIds.add(row.get(0));
//                    selectedBatchNos.add(row.get(1));
//                    selectedBatchQty.add(row.get(3));
                    columnList.add(row.get(1));
//                    stExpiryDate.add(row.get(4));
//                    stEntryDate.add(row.get(5));
//                    try {
//                        expiryDate.add(fmt.parse(row.get(4)));
//                        entryDate.add(fmt.parse(row.get(5)));
//                    } catch (ParseException e) {
//                        e.printStackTrace();
//                    }

//                    if(batchPolicy.equals("Entry LIFO"))
//                    {
//                        // Entry select max date
//                        entrySelectedDate = Collections.max(entryDate);
//                        expirySelection = false;
//                        selectedIndex = entryDate.indexOf(entrySelectedDate);
//                    }
//                    else if(batchPolicy.equals("Entry FIFO"))
//                    {
//                        // Entry select min date
//                        entrySelectedDate = Collections.min(entryDate);
//                        expirySelection = false;
//                        selectedIndex = entryDate.indexOf(entrySelectedDate);
//                    }
//                    else if(batchPolicy.equals("Expiry LIFO"))
//                    {
//                        // Expiry select max date
//                        expirySelectedDate = Collections.max(expiryDate);
//                        expirySelection = true;
//                        selectedIndex = entryDate.indexOf(expirySelectedDate);
//                    }
//                    else if(batchPolicy.equals("Expiry FIFO"))
//                    {
//                        // Expiry select min date
//                        expirySelectedDate = Collections.min(expiryDate);
//                        expirySelection = true;
//                        selectedIndex = entryDate.indexOf(expirySelectedDate);
//                    }
//                    selectedBatch = columnList.get(selectedIndex);
                }
            }
        }
        return columnList;
    }

    public static String getProductId(ArrayList<ArrayList<String>> productsData, String productName)
    {
        for(ArrayList<String> row: productsData) {
            if(row.get(1).equals(productName))
            {
                selectedProductPrice = row.get(3);
//                addedRetailPrice = row.get(2);
                return row.get(0);
            }
        }
        return null;
    }

    public static String getBatchPrice(ArrayList<ArrayList<String>> batchData, String batchNo, String productId)
    {
        for(ArrayList<String> row: batchData) {
            if(row.get(1).equals(batchNo) && row.get(2).equals(productId))
            {
                selectedProductPrice = row.get(3);
//                addedRetailPrice = row.get(2);
                return row.get(0);
            }
        }
        return null;
    }

    private String computeReturnPrice() {
        String quant = txt_product_quantity.getText();
        if(quant == null || quant.equals(""))
        {
            quant = "0";
        }

        return String.valueOf(Float.parseFloat(selectedProductPrice) * Integer.parseInt(quant));
    }

}
