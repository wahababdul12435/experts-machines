package controller;

import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import model.GlobalVariables;
import model.MyAccountInfo;
import model.MysqlCon;
import model.UserAccountsInfo;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;
import static java.nio.file.StandardCopyOption.*;

public class MyAccount implements Initializable {
    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private JFXTextField txt_user_id;

    @FXML
    private JFXTextField txt_username;

    @FXML
    private JFXPasswordField txt_password;

    @FXML
    private JFXTextField txt_view_password;

    @FXML
    private ImageView img_user;

    @FXML
    private JFXPasswordField txt_curr_password;

    @FXML
    private JFXPasswordField txt_new_password;

    @FXML
    private JFXPasswordField txt_re_new_password;

    @FXML
    private JFXTextField txt_name;

    @FXML
    private JFXTextField txt_contact;

    @FXML
    private JFXTextField txt_address;

    @FXML
    private JFXTextField txt_cnic;

    @FXML
    private JFXTextField txt_reg_date;

    @FXML
    private JFXTextField txt_status;

    @FXML
    private JFXTextField txt_type;

    @FXML
    private JFXCheckBox chk_insert;

    @FXML
    private JFXCheckBox chk_view;

    @FXML
    private JFXCheckBox chk_change;

    @FXML
    private JFXCheckBox chk_delete;

    @FXML
    private JFXCheckBox chk_manage_stock;

    @FXML
    private JFXCheckBox chk_manage_cash;

    @FXML
    private JFXCheckBox chk_manage_policies;

    @FXML
    private JFXCheckBox chk_mobile_app;

    @FXML
    private Button btn_change_username;

    @FXML
    private Button btn_change_password;

    @FXML
    private Button btn_change_info;

    @FXML
    private Button btn_cancel_username;

    @FXML
    private Button btn_update_username;

    @FXML
    private Button btn_cancel_password;

    @FXML
    private Button btn_update_password;

    @FXML
    private Button btn_cancel_info;

    @FXML
    private Button btn_update_info;

    @FXML
    private Button btn_apply_changes;

    @FXML
    private Button btn_cancel_changes;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    @FXML
    private BorderPane brdrpane_navsetting;

    @FXML
    private BorderPane menu_bar;

    @FXML
    private Button btn_view_password;

    @FXML
    private Button btn_change_rights;

    @FXML
    private Button btn_cancel_rights;

    @FXML
    private Button btn_update_rights;

    MysqlCon objMysqlCon;
    Statement objStmt;
    Connection objCon;
    ArrayList<String> userDetail;
    private String currentId;
    String selectedImagePath;

    private String rightInsert = "0";
    private String rightView = "0";
    private String rightChange = "0";
    private String rightDelete = "0";
    private String rightStock = "0";
    private String rightCash = "0";
    private String rightSettings = "0";
    private String rightMobile = "0";


    int changeRequest = 0;

    Alert alert = new Alert(Alert.AlertType.NONE);

    public static Button btnView;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnSettings.setStyle("-fx-background-color: #47ab1e");
        objMysqlCon = new MysqlCon();
        objStmt = objMysqlCon.stmt;
        objCon = objMysqlCon.con;
        MyAccountInfo objMyAccountInfo = new MyAccountInfo();
        if (UserAccountsInfo.getAccountDetail != null) {
            currentId = UserAccountsInfo.getAccountDetail;
            btn_change_username.setVisible(false);
            btn_change_password.setVisible(false);
            UserAccountsInfo.getAccountDetail = null;
        } else {
            btn_change_rights.setVisible(false);
            btn_view_password.setVisible(false);
            currentId = GlobalVariables.getUserId();
        }
        userDetail = objMyAccountInfo.getCompleteUserDetail(objStmt, objCon, currentId);
        txt_user_id.setText(currentId);
        txt_username.setText(userDetail.get(0));
        txt_password.setText(userDetail.get(1));
        txt_view_password.setText(userDetail.get(1));
        txt_name.setText(userDetail.get(2));
        txt_contact.setText(userDetail.get(3));
        txt_address.setText(userDetail.get(4));
        txt_cnic.setText(userDetail.get(5));
        txt_type.setText(userDetail.get(6));
        if(userDetail.get(7) != null && !userDetail.get(7).equals(""))
        {
            selectedImagePath = userDetail.get(7);
            File file = new File(selectedImagePath);
            boolean exists = file.exists();
            if(exists)
            {
                Image image = new Image(file.toURI().toString());
                img_user.setImage(image);
            }
        }

        txt_reg_date.setText(userDetail.get(8));
        txt_status.setText(userDetail.get(9));
        if (userDetail.get(10).equals("1")) {
            chk_insert.setSelected(true);
        }
        if (userDetail.get(11).equals("1")) {
            chk_view.setSelected(true);
        }
        if (userDetail.get(12).equals("1")) {
            chk_change.setSelected(true);
        }
        if (userDetail.get(13).equals("1")) {
            chk_delete.setSelected(true);
        }
        if (userDetail.get(14).equals("1")) {
            chk_manage_stock.setSelected(true);
        }
        if (userDetail.get(15).equals("1")) {
            chk_manage_cash.setSelected(true);
        }
        if (userDetail.get(16).equals("1")) {
            chk_manage_policies.setSelected(true);
        }
        if (userDetail.get(17).equals("1")) {
            chk_mobile_app.setSelected(true);
        }
    }

    @FXML
    void cancelInfo(ActionEvent event) {
        txt_name.setText(userDetail.get(2));
        txt_contact.setText(userDetail.get(3));
        txt_address.setText(userDetail.get(4));
        txt_cnic.setText(userDetail.get(5));

        txt_name.setDisable(true);
        txt_contact.setDisable(true);
        txt_address.setDisable(true);
        txt_cnic.setDisable(true);
        btn_cancel_info.setVisible(false);
        btn_update_info.setVisible(false);
        btn_change_info.setVisible(true);
    }

    @FXML
    void cancelPassword(ActionEvent event) {
        txt_curr_password.setVisible(false);
        txt_new_password.setVisible(false);
        txt_re_new_password.setVisible(false);
        btn_cancel_password.setVisible(false);
        btn_update_password.setVisible(false);
        btn_change_password.setVisible(true);
    }

    @FXML
    void cancelUsername(ActionEvent event) {
        txt_username.setText(userDetail.get(0));
        txt_username.setDisable(true);
        btn_cancel_username.setVisible(false);
        btn_update_username.setVisible(false);
        btn_change_username.setVisible(true);
    }

    @FXML
    void cancelRights(ActionEvent event) {
        if (userDetail.get(10).equals("1")) {
            chk_insert.setSelected(true);
        } else {
            chk_insert.setSelected(false);
        }
        if (userDetail.get(11).equals("1")) {
            chk_view.setSelected(true);
        } else {
            chk_view.setSelected(false);
        }
        if (userDetail.get(12).equals("1")) {
            chk_change.setSelected(true);
        } else {
            chk_change.setSelected(false);
        }
        if (userDetail.get(13).equals("1")) {
            chk_delete.setSelected(true);
        } else {
            chk_delete.setSelected(false);
        }
        if (userDetail.get(14).equals("1")) {
            chk_manage_stock.setSelected(true);
        } else {
            chk_manage_stock.setSelected(false);
        }
        if (userDetail.get(15).equals("1")) {
            chk_manage_cash.setSelected(true);
        } else {
            chk_manage_cash.setSelected(false);
        }
        if (userDetail.get(16).equals("1")) {
            chk_manage_policies.setSelected(true);
        } else {
            chk_manage_policies.setSelected(false);
        }
        if (userDetail.get(17).equals("1")) {
            chk_mobile_app.setSelected(true);
        } else {
            chk_mobile_app.setSelected(false);
        }

        chk_insert.setDisable(true);
        chk_view.setDisable(true);
        chk_change.setDisable(true);
        chk_delete.setDisable(true);
        chk_manage_stock.setDisable(true);
        chk_manage_cash.setDisable(true);
        chk_manage_policies.setDisable(true);
        chk_mobile_app.setDisable(true);

        btn_change_rights.setVisible(true);
        btn_cancel_rights.setVisible(false);
        btn_update_rights.setVisible(false);
    }

    @FXML
    void changeInfo(ActionEvent event) {
        txt_name.setDisable(false);
        txt_contact.setDisable(false);
        txt_address.setDisable(false);
        txt_cnic.setDisable(false);
        btn_cancel_info.setVisible(true);
        btn_update_info.setVisible(true);
        btn_change_info.setVisible(false);
    }

    @FXML
    void changePassword(ActionEvent event) {
        txt_curr_password.setVisible(true);
        txt_new_password.setVisible(true);
        txt_re_new_password.setVisible(true);
        btn_cancel_password.setVisible(true);
        btn_update_password.setVisible(true);
        btn_change_password.setVisible(false);
    }

    @FXML
    void changeUsername(ActionEvent event) {
        txt_username.setDisable(false);
        btn_cancel_username.setVisible(true);
        btn_update_username.setVisible(true);
        btn_change_username.setVisible(false);
    }

    @FXML
    void changeRights(ActionEvent event) {
        chk_insert.setDisable(false);
        chk_view.setDisable(false);
        chk_change.setDisable(false);
        chk_delete.setDisable(false);
        chk_manage_stock.setDisable(false);
        chk_manage_cash.setDisable(false);
        chk_manage_policies.setDisable(false);
        chk_mobile_app.setDisable(false);

        btn_change_rights.setVisible(false);
        btn_cancel_rights.setVisible(true);
        btn_update_rights.setVisible(true);
    }

    @FXML
    void follow(MouseEvent event) {

    }

    @FXML
    void updateRights(ActionEvent event) {
        if (chk_insert.isSelected()) {
            userDetail.set(10, "1");
        }
        if (chk_view.isSelected()) {
            userDetail.set(11, "1");
        }
        if (chk_change.isSelected()) {
            userDetail.set(12, "1");
        }
        if (chk_delete.isSelected()) {
            userDetail.set(13, "1");
        }
        if (chk_manage_stock.isSelected()) {
            userDetail.set(14, "1");
        }
        if (chk_manage_cash.isSelected()) {
            userDetail.set(15, "1");
        }
        if (chk_manage_policies.isSelected()) {
            userDetail.set(16, "1");
        }
        if (chk_mobile_app.isSelected()) {
            userDetail.set(17, "1");
        }

        chk_insert.setDisable(true);
        chk_view.setDisable(true);
        chk_change.setDisable(true);
        chk_delete.setDisable(true);
        chk_manage_stock.setDisable(true);
        chk_manage_cash.setDisable(true);
        chk_manage_policies.setDisable(true);
        chk_mobile_app.setDisable(true);

        btn_change_rights.setVisible(true);
        btn_cancel_rights.setVisible(false);
        btn_update_rights.setVisible(false);
        btn_cancel_changes.setVisible(true);
        btn_apply_changes.setVisible(true);
    }

    @FXML
    void updateInfo(ActionEvent event) {
        if (authenticateInfoChange()) {
            userDetail.set(2, txt_name.getText().toString());
            userDetail.set(3, txt_contact.getText().toString());
            userDetail.set(4, txt_address.getText().toString());
            userDetail.set(5, txt_cnic.getText().toString());
            txt_name.setDisable(true);
            txt_contact.setDisable(true);
            txt_address.setDisable(true);
            txt_cnic.setDisable(true);
            btn_cancel_info.setVisible(false);
            btn_update_info.setVisible(false);
            btn_change_info.setVisible(true);
            btn_cancel_changes.setVisible(true);
            btn_apply_changes.setVisible(true);
        } else {
            alert.setAlertType(Alert.AlertType.ERROR);
            alert.setHeaderText("Error!");
            alert.setContentText("Info Change Error\nPlease fill all fields.");
            alert.show();
        }
    }

    @FXML
    void updatePassword(ActionEvent event) {
        if (authenticatePasswordChange()) {
            txt_password.setText(txt_new_password.getText().toString());
            txt_curr_password.setVisible(false);
            txt_new_password.setVisible(false);
            txt_re_new_password.setVisible(false);
            btn_cancel_password.setVisible(false);
            btn_update_password.setVisible(false);
            btn_change_password.setVisible(true);
            btn_cancel_changes.setVisible(true);
            btn_apply_changes.setVisible(true);
        } else {
            alert.setAlertType(Alert.AlertType.ERROR);
            alert.setHeaderText("Error!");
            alert.setContentText("Password Change Error\nCurrent Password should be correct\nNew Password should not be same as Current Password.");
            alert.show();
        }
    }

    @FXML
    void updateUsername(ActionEvent event) {
        String userName = txt_username.getText().toString();
        if (!userName.equals("")) {
            userDetail.set(0, userName);
            txt_username.setDisable(true);
            btn_cancel_username.setVisible(false);
            btn_update_username.setVisible(false);
            btn_change_username.setVisible(true);
            btn_cancel_changes.setVisible(true);
            btn_apply_changes.setVisible(true);
        } else {
            alert.setAlertType(Alert.AlertType.ERROR);
            alert.setHeaderText("Error!");
            alert.setContentText("Username can't be empty.");
            alert.show();
        }
    }

    @FXML
    void applyChanges(ActionEvent event) throws IOException {
        if (chk_insert.isSelected()) {
            rightInsert = "1";
        }
        if (chk_view.isSelected()) {
            rightView = "1";
        }
        if (chk_change.isSelected()) {
            rightChange = "1";
        }
        if (chk_delete.isSelected()) {
            rightDelete = "1";
        }
        if (chk_manage_stock.isSelected()) {
            rightStock = "1";
        }
        if (chk_manage_cash.isSelected()) {
            rightCash = "1";
        }
        if (chk_manage_policies.isSelected()) {
            rightSettings = "1";
        }
        if (chk_mobile_app.isSelected()) {
            rightMobile = "1";
        }

        MyAccountInfo objMyAccountInfo = new MyAccountInfo();
        objMyAccountInfo.updateAccountInfo(objStmt, objCon, currentId, txt_username.getText().toString(), txt_password.getText().toString(), txt_name.getText().toString(), txt_contact.getText().toString(), txt_address.getText().toString(), txt_cnic.getText().toString(), selectedImagePath, rightInsert, rightView, rightChange, rightDelete, rightStock, rightCash, rightSettings, rightMobile, userDetail.get(18), userDetail.get(19), userDetail.get(20));
        Parent root = FXMLLoader.load(getClass().getResource("/view/user_accounts.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void cancelChanges(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/my_account.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void changePic(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        File selectedFile = fileChooser.showOpenDialog(GlobalVariables.baseStage);
        if(selectedFile != null)
        {
            File file = new File(selectedFile.toString());
            Image image = new Image(file.toURI().toString());
            img_user.setImage(image);
            selectedImagePath = selectedFile.toString();
            selectedImagePath = selectedImagePath.replace("\\", "/");
            btn_cancel_changes.setVisible(true);
            btn_apply_changes.setVisible(true);
//            writeFile(selectedImagePath);
        }
    }

    @FXML
    void viewPassword(MouseEvent event) {
        txt_password.setVisible(false);
        txt_view_password.setVisible(true);
    }

    @FXML
    void hidePassword(MouseEvent event) {
        txt_password.setVisible(true);
        txt_view_password.setVisible(false);
    }

    private boolean authenticatePasswordChange() {
        String pass = userDetail.get(1);
        String currPass = txt_curr_password.getText().toString();
        String newPass = txt_new_password.getText().toString();
        String reNewPass = txt_re_new_password.getText().toString();
        return pass.equals(currPass) && !pass.equals(newPass) && newPass.equals(reNewPass);
    }

    private boolean authenticateInfoChange() {
        String name = txt_name.getText().toString();
        String contact = txt_contact.getText().toString();
        String address = txt_address.getText().toString();
        String cnic = txt_cnic.getText().toString();
        return !name.equals("") && !contact.equals("") && !address.equals("") && !cnic.equals("");
    }

    public void writeFile(String filePath) {
        String directoryName = "../Younas Traders/src/view/images/users/"+currentId+"/";
        // initialize File object
        File file = new File(directoryName);
        // check if the pathname already exists
        // if not create it
        if(!file.exists()){
            // create the folder
            boolean result = file.mkdirs();
            if(result){
                String absPath = file.getAbsolutePath();
                absPath = absPath.replace("\\", "/")+"/";
//                copyImage(filePath, absPath+"\\");
                System.out.println("Abs Path: "+absPath);
                System.out.println("Successfully created "+file.getAbsolutePath());
                try {
                    copyFile(filePath, absPath);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else{
                System.out.println("Failed creating "+file.getAbsolutePath());
            }
        }else{
            System.out.println("Pathname already exists");
        }
    }

    public static void copyFile(String from, String to) throws IOException{
        System.out.println("src: "+from);
        System.out.println("dest: "+to);
        Path src = Paths.get(from);
        Path dest = Paths.get(to);
        Files.copy(src, dest, REPLACE_EXISTING);
    }


}
