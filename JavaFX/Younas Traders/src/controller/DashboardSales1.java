package controller;

import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.transitions.hamburger.HamburgerBackArrowBasicTransition;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.util.StringConverter;
import model.DashboardSales1Info;
import model.GlobalVariables;

import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class DashboardSales1 implements Initializable {

    @FXML
    private BorderPane menu_bar;
    @FXML
    private JFXHamburger hamburger;
    @FXML
    private JFXDrawer drawer;
    @FXML
    private VBox Vbox_btns;
    @FXML
    private AnchorPane inner_anchor;
    @FXML
    private Button btn11;
    @FXML
    private Pane btnpane;
    @FXML
    private FontAwesomeIconView FA_Icon;
    @FXML
    private BorderPane brdrpane_navpurchase;

    @FXML
    private BarChart<String, Integer> purchase_chart;

    @FXML
    private CategoryAxis purchaseTimeAxis;

    @FXML
    private NumberAxis purchaseCashAxis;

    @FXML
    private BarChart<String, Integer> return_chart;

    @FXML
    private CategoryAxis returnTimeAxis;

    @FXML
    private NumberAxis returnCashAxis;

    @FXML
    private JFXDatePicker txt_from_date;

    @FXML
    private JFXDatePicker txt_to_date;

    public static Button btnView;
    public static String txtFromDate;
    public static String txtToDate;
    public static boolean filter;

    ArrayList<ArrayList<String>> processedPurchaseData = new ArrayList<>();
    ArrayList<ArrayList<String>> processedReturnData = new ArrayList<>();
    ArrayList<ArrayList<String>> purchaseData = new ArrayList<>();
    ArrayList<ArrayList<String>> returnData = new ArrayList<>();
    ArrayList<String> purchaseCompanies = new ArrayList<String>();
    ArrayList<String> returnCompanies = new ArrayList<String>();
    ArrayList<Date> dates = new ArrayList<Date>();
    SimpleDateFormat fmt = new SimpleDateFormat("dd/MMM/yyyy");
    int span;
    private String stStartDate;
    private String stEndDate;
    Date tempDate;
    int intervals = 8;
    private DashboardSales1Info objDashboardSales1Info;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnSale.setStyle("-fx-background-color: #47ab1e");
        objDashboardSales1Info = new DashboardSales1Info();

        btnpane.setOnMouseEntered(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(22);
                btnpane.setMinWidth(20.0);
                btn11.setMinWidth(20.0);

            }
        });
        btnpane.setOnMouseExited(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(14);
                btnpane.setMinWidth(10.0);
                btn11.setMinWidth(10.0);
            }
        });

        txt_from_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        txt_to_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        inner_anchor.setLeftAnchor(inner_anchor, 260d);
        drawer.setSidePane(brdrpane_navpurchase);
        drawer.open();

        purchaseTimeAxis.setLabel("Time Span");
        purchaseCashAxis.setLabel("Purchase");
        returnTimeAxis.setLabel("Time Span");
        returnCashAxis.setLabel("Return");


        if(filter)
        {
            if (txtFromDate != null && !txtFromDate.equals("")) {
                txt_from_date.setValue(GlobalVariables.LOCAL_DATE(txtFromDate));
                stStartDate = txtFromDate;
            }
            if (txtToDate != null && !txtToDate.equals("")) {
                txt_to_date.setValue(GlobalVariables.LOCAL_DATE(txtToDate));
                stEndDate = txtToDate;
            }
            filter = false;
        }
        else
        {
            setDateRange();
        }

        purchaseData = objDashboardSales1Info.getCompanyPurchaseDetail(GlobalVariables.objStmt, GlobalVariables.objCon, stStartDate, stEndDate);
        returnData = objDashboardSales1Info.getCompanyReturnDetail(GlobalVariables.objStmt, GlobalVariables.objCon, stStartDate, stEndDate);
        purchaseCompanies = GlobalVariables.getArrayColumn(purchaseData, 3);
        returnCompanies = GlobalVariables.getArrayColumn(returnData, 3);
        purchaseCompanies = GlobalVariables.getUniqueValues(purchaseCompanies);
        returnCompanies = GlobalVariables.getUniqueValues(returnCompanies);

        setBarChartDates();
        setPurchaseBarChart();
        setReturnBarChart();
        ArrayList<XYChart.Series> purchaseDataSeries = new ArrayList<>();
        ArrayList<XYChart.Series> returnDataSeries = new ArrayList<>();
        for(int i=0; i<purchaseCompanies.size(); i++)
        {
            XYChart.Series xySeries = new XYChart.Series();
            xySeries.setName(purchaseCompanies.get(i));
            purchaseDataSeries.add(xySeries);
        }

        for(int i=0; i<returnCompanies.size(); i++)
        {
            XYChart.Series xySeries = new XYChart.Series();
            xySeries.setName(returnCompanies.get(i));
            returnDataSeries.add(xySeries);
        }

        if(purchaseCompanies.size() > 0)
        {
            for(int i=processedPurchaseData.size()-2; i>=1; i--)
            {
                if(processedPurchaseData.get(i).get(0).equals(processedPurchaseData.get(processedPurchaseData.size()-1).get(0)))
                {
                    for(int j=0; j<purchaseCompanies.size(); j++)
                    {
                        purchaseDataSeries.get(j).getData().add(new XYChart.Data(processedPurchaseData.get(i+1).get(0), (Float.parseFloat(processedPurchaseData.get(i).get(j+1)))));
                    }
                    break;
                }
                String stLowerBoundDate = processedPurchaseData.get(i).get(0);
                Date lowerBoundDate = null;
                try {
                    lowerBoundDate = fmt.parse(stLowerBoundDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Calendar calTemp = Calendar.getInstance();
                calTemp.setTime(lowerBoundDate);
                calTemp.add(Calendar.DAY_OF_MONTH, -1);
                try {
                    lowerBoundDate = fmt.parse(fmt.format(calTemp.getTime()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                fmt.format(calTemp.getTime());
                stLowerBoundDate = fmt.format(calTemp.getTime());
                if(span < -1)
                {
                    for(int j=0; j<purchaseCompanies.size(); j++)
                    {
                        purchaseDataSeries.get(j).getData().add(new XYChart.Data(processedPurchaseData.get(i+1).get(0)+"\n|\n"+stLowerBoundDate, (Float.parseFloat(processedPurchaseData.get(i).get(j+1)))));
                    }
                }
                else
                {
                    for(int j=0; j<purchaseCompanies.size(); j++)
                    {
                        purchaseDataSeries.get(j).getData().add(new XYChart.Data(processedPurchaseData.get(i).get(0), (Float.parseFloat(processedPurchaseData.get(i).get(j+1)))));
                    }
                }
            }
            for(int j=0; j<purchaseCompanies.size(); j++)
            {
                purchase_chart.getData().add(purchaseDataSeries.get(j));
            }
        }

        if(returnCompanies.size() > 0)
        {
            for(int i=processedReturnData.size()-2; i>=1; i--)
            {
                if(processedReturnData.get(i).get(0).equals(processedReturnData.get(processedReturnData.size()-1).get(0)))
                {
                    for(int j=0; j<returnCompanies.size(); j++)
                    {
                        returnDataSeries.get(j).getData().add(new XYChart.Data(processedReturnData.get(i+1).get(0), (Float.parseFloat(processedReturnData.get(i).get(j+1)))));
                    }
                    break;
                }
                String stLowerBoundDate = processedReturnData.get(i).get(0);
                Date lowerBoundDate = null;
                try {
                    lowerBoundDate = fmt.parse(stLowerBoundDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Calendar calTemp = Calendar.getInstance();
                calTemp.setTime(lowerBoundDate);
                calTemp.add(Calendar.DAY_OF_MONTH, -1);
                try {
                    lowerBoundDate = fmt.parse(fmt.format(calTemp.getTime()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                fmt.format(calTemp.getTime());
                stLowerBoundDate = fmt.format(calTemp.getTime());
                if(span < -1)
                {
                    for(int j=0; j<returnCompanies.size(); j++)
                    {
                        returnDataSeries.get(j).getData().add(new XYChart.Data(processedReturnData.get(i+1).get(0)+"\n|\n"+stLowerBoundDate, (Float.parseFloat(processedReturnData.get(i).get(j+1)))));
                    }
                }
                else
                {
                    for(int j=0; j<returnCompanies.size(); j++)
                    {
                        returnDataSeries.get(j).getData().add(new XYChart.Data(processedReturnData.get(i).get(0), (Float.parseFloat(processedReturnData.get(i).get(j+1)))));
                    }
                }
            }
            for(int j=0; j<returnCompanies.size(); j++)
            {
                return_chart.getData().add(returnDataSeries.get(j));
            }
        }
    }

    @FXML
    void btnSearch(ActionEvent event) {
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date selectedDate = null;
        if(txt_from_date.getValue() != null)
        {
            txtFromDate = txt_from_date.getValue().toString();
            try {
                selectedDate = sdf.parse(txtFromDate);
                txtFromDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if(txt_to_date.getValue() != null)
        {
            txtToDate = txt_to_date.getValue().toString();
            try {
                selectedDate = sdf.parse(txtToDate);
                txtToDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        filter = true;

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/dashboard_purchase.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    public void follow() throws IOException
    {
    /*PathTransition transition;
    transition = new PathTransition();
    Polyline mpoly = new Polyline();
    mpoly.getPoints().addAll(new Double[]{
            0.0,0.0,
            0.0,0.0,
            -200.0,0.0});
    transition.setNode(btn11);
    transition.setDuration(Duration.seconds(2));
    transition.setPath(mpoly);
    transition.play();*/
        if (drawer.isOpened()) {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(-250);
            transition.setByY(0);
            transition.setCycleCount(1);
            transition.play();
            drawer.close();
            FA_Icon.setGlyphName("ANGLE_DOUBLE_RIGHT");
            FA_Icon.setGlyphSize(14);
            //inner_anchor.setRightAnchor(inner_anchor,1d);


            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(-90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(1.31017);
            scale.setToY(1);

            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();
            //
            //inner_anchor.setLeftAnchor(inner_anchor, 5d);
        } else {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(250);
            transition.setByY(0);
            transition.setCycleCount(1);
            FA_Icon.setGlyphName("ANGLE_DOUBLE_LEFT");
            FA_Icon.setGlyphSize(14);
            drawer.open();

            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(0.99947777);
            scale.setToY(1);

            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();
        }
    }

    private void setDateRange()
    {
        stEndDate = GlobalVariables.getStDate();
        Date endDate = null;
        try {
            endDate = fmt.parse(stEndDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(endDate);
        cal1.add(Calendar.DAY_OF_MONTH, -120);
        stStartDate = fmt.format(cal1.getTime());
        txt_from_date.setValue(GlobalVariables.LOCAL_DATE(stStartDate));
        txt_to_date.setValue(GlobalVariables.LOCAL_DATE(stEndDate));
    }

    private void setBarChartDates()
    {
        Date endDate = null;
        Date startDate = null;
        try {
            startDate = fmt.parse(stStartDate);
            endDate = fmt.parse(stEndDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long diff = endDate.getTime() - startDate.getTime();
        diff = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
        span = (int) (diff/intervals);
        span = span * -1;
        Calendar cal = Calendar.getInstance();
        cal.setTime(endDate);
        ArrayList<String> temp1 = new ArrayList<>();
        ArrayList<String> temp2 = new ArrayList<>();
        temp1.add("Dates");
        temp2.add("Dates");
        for(int i=0; i<purchaseCompanies.size(); i++)
        {
            temp1.add(purchaseCompanies.get(i));
        }
        for(int i=0; i<returnCompanies.size(); i++)
        {
            temp2.add(returnCompanies.get(i));
        }
        processedPurchaseData.add(temp1);
        processedReturnData.add(temp2);
        temp1 = new ArrayList<>();
        temp2 = new ArrayList<>();
        temp1.add(fmt.format(cal.getTime()));
        temp2.add(fmt.format(cal.getTime()));
        for(int i=0; i<purchaseCompanies.size(); i++)
        {
            temp1.add("0");
        }
        for(int i=0; i<returnCompanies.size(); i++)
        {
            temp2.add("0");
        }
        processedPurchaseData.add(temp1);
        processedReturnData.add(temp2);
        tempDate = endDate;
        dates.add(tempDate);
        for(int i=1; i<=intervals; i++)
        {
            temp1 = new ArrayList<>();
            temp2 = new ArrayList<>();
            cal.add(Calendar.DAY_OF_MONTH, span);
            try {
                tempDate = fmt.parse(fmt.format(cal.getTime()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            temp1.add(fmt.format(cal.getTime()));
            temp2.add(fmt.format(cal.getTime()));
            for(int j=0; j<purchaseCompanies.size(); j++)
            {
                temp1.add("0");
            }
            for(int j=0; j<returnCompanies.size(); j++)
            {
                temp2.add("0");
            }
            processedPurchaseData.add(temp1);
            processedReturnData.add(temp2);
            dates.add(tempDate);
        }
    }

    private void setPurchaseBarChart()
    {
        for (int i = 0; i < purchaseData.size(); i++)
        {
            try {
                tempDate = fmt.parse(purchaseData.get(i).get(2));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            int specifiedNum = 0;
            if((tempDate.before(dates.get(0)) && tempDate.after(dates.get(1))) || tempDate.equals(dates.get(0)))
            {
//                    System.out.println("1st: Small Date: "+dates.get(0)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(1));
                specifiedNum = 1;
            }
            else if((tempDate.before(dates.get(1)) && tempDate.after(dates.get(2))) || tempDate.equals(dates.get(1)))
            {
//                    System.out.println("2nd: Small Date: "+dates.get(1)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(2));
                specifiedNum = 2;
            }
            else if((tempDate.before(dates.get(2)) && tempDate.after(dates.get(3))) || tempDate.equals(dates.get(2)))
            {
//                    System.out.println("3rd: Small Date: "+dates.get(2)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(3));
                specifiedNum = 3;
            }
            else if((tempDate.before(dates.get(3)) && tempDate.after(dates.get(4))) || tempDate.equals(dates.get(3)))
            {
//                    System.out.println("4th: Small Date: "+dates.get(3)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(4));
                specifiedNum = 4;
            }
            else if((tempDate.before(dates.get(4)) && tempDate.after(dates.get(5))) || tempDate.equals(dates.get(4)))
            {
//                    System.out.println("4th: Small Date: "+dates.get(3)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(4));
                specifiedNum = 5;
            }
            else if((tempDate.before(dates.get(5)) && tempDate.after(dates.get(6))) || tempDate.equals(dates.get(5)))
            {
//                    System.out.println("4th: Small Date: "+dates.get(3)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(4));
                specifiedNum = 6;
            }
            else if((tempDate.before(dates.get(6)) && tempDate.after(dates.get(7))) || tempDate.equals(dates.get(6)))
            {
//                    System.out.println("4th: Small Date: "+dates.get(3)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(4));
                specifiedNum = 7;
            }
            else if((tempDate.before(dates.get(7)) && tempDate.after(dates.get(8))) || tempDate.equals(dates.get(7)))
            {
//                    System.out.println("4th: Small Date: "+dates.get(3)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(4));
                specifiedNum = 8;
            }
            else if((tempDate.before(dates.get(8)) && tempDate.after(dates.get(9))) || tempDate.equals(dates.get(8)))
            {
//                    System.out.println("4th: Small Date: "+dates.get(3)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(4));
                specifiedNum = 9;
            }
            int x = 1;
            while(!purchaseData.get(i).get(3).equals(processedPurchaseData.get(0).get(x)))
            {
                x++;
            }
            String value = String.valueOf(Float.parseFloat(processedPurchaseData.get(specifiedNum).get(x)) + Float.parseFloat(purchaseData.get(i).get(1)));
            processedPurchaseData.get(specifiedNum).set(x, value);
        }
//        System.out.println("   -------------------   ");
    }

    private void setReturnBarChart()
    {
        for (int i = 0; i < returnData.size(); i++)
        {
            try {
                tempDate = fmt.parse(returnData.get(i).get(2));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            int specifiedNum = 0;
            if((tempDate.before(dates.get(0)) && tempDate.after(dates.get(1))) || tempDate.equals(dates.get(0)))
            {
//                    System.out.println("1st: Small Date: "+dates.get(0)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(1));
                specifiedNum = 1;
            }
            else if((tempDate.before(dates.get(1)) && tempDate.after(dates.get(2))) || tempDate.equals(dates.get(1)))
            {
//                    System.out.println("2nd: Small Date: "+dates.get(1)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(2));
                specifiedNum = 2;
            }
            else if((tempDate.before(dates.get(2)) && tempDate.after(dates.get(3))) || tempDate.equals(dates.get(2)))
            {
//                    System.out.println("3rd: Small Date: "+dates.get(2)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(3));
                specifiedNum = 3;
            }
            else if((tempDate.before(dates.get(3)) && tempDate.after(dates.get(4))) || tempDate.equals(dates.get(3)))
            {
//                    System.out.println("4th: Small Date: "+dates.get(3)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(4));
                specifiedNum = 4;
            }
            else if((tempDate.before(dates.get(4)) && tempDate.after(dates.get(5))) || tempDate.equals(dates.get(4)))
            {
//                    System.out.println("4th: Small Date: "+dates.get(3)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(4));
                specifiedNum = 5;
            }
            else if((tempDate.before(dates.get(5)) && tempDate.after(dates.get(6))) || tempDate.equals(dates.get(5)))
            {
//                    System.out.println("4th: Small Date: "+dates.get(3)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(4));
                specifiedNum = 6;
            }
            else if((tempDate.before(dates.get(6)) && tempDate.after(dates.get(7))) || tempDate.equals(dates.get(6)))
            {
//                    System.out.println("4th: Small Date: "+dates.get(3)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(4));
                specifiedNum = 7;
            }
            else if((tempDate.before(dates.get(7)) && tempDate.after(dates.get(8))) || tempDate.equals(dates.get(7)))
            {
//                    System.out.println("4th: Small Date: "+dates.get(3)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(4));
                specifiedNum = 8;
            }
            else if((tempDate.before(dates.get(8)) && tempDate.after(dates.get(9))) || tempDate.equals(dates.get(8)))
            {
//                    System.out.println("4th: Small Date: "+dates.get(3)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(4));
                specifiedNum = 9;
            }
            int x = 1;
            while(!returnData.get(i).get(3).equals(processedReturnData.get(0).get(x)))
            {
                x++;
            }
            String value = String.valueOf(Float.parseFloat(processedReturnData.get(specifiedNum).get(x)) + Float.parseFloat(returnData.get(i).get(1)));
            processedReturnData.get(specifiedNum).set(x, value);
        }
//        System.out.println("   -------------------   ");
    }


}
