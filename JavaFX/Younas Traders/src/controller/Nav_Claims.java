package controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;


public class Nav_Claims {

    @FXML
    private VBox Vbox_btns;


    public void PurchInvoice() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/purch_invoice.fxml"));
        Stage objstage = (Stage) Vbox_btns.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }
    public void PurchReturn() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/purch_return.fxml"));
        Stage objstage = (Stage) Vbox_btns.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }
    public void ClaimsDashboard() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/dashboard_purchase.fxml"));
        Stage objstage = (Stage) Vbox_btns.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

}
