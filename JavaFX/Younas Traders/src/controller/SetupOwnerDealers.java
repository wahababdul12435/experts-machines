package controller;

import com.jfoenix.controls.JFXButton;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import model.GlobalVariables;
import model.SetupOwnerDealersInfo;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class SetupOwnerDealers implements Initializable {

    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private TableView<SetupOwnerDealersInfo> table_viewdealers;

    @FXML
    private TableColumn<SetupOwnerDealersInfo, String> sr_no;

    @FXML
    private TableColumn<SetupOwnerDealersInfo, String> dealer_name;

    @FXML
    private TableColumn<SetupOwnerDealersInfo, String> dealer_contact;

    @FXML
    private TableColumn<SetupOwnerDealersInfo, String> dealer_address;

    @FXML
    private TableColumn<SetupOwnerDealersInfo, String> dealer_cnic;

    @FXML
    private TableColumn<SetupOwnerDealersInfo, String> dealer_type;

    @FXML
    private TableColumn<SetupOwnerDealersInfo, String> operations;

    @FXML
    private JFXButton btn_skip;

    @FXML
    private JFXButton btn_add;

    @FXML
    private ProgressIndicator progress;

    @FXML
    private Label lbl_total_dealers;

    @FXML
    private Label lbl_dealers_selected;

    @FXML
    private JFXButton btn_unselect;

    @FXML
    private JFXButton btn_select;

    @FXML
    private BorderPane menu_bar;

    private static SetupOwnerDealersInfo objSetupOwnerDealersInfo;
    public static ArrayList<ArrayList<String>> dealersData;
    public static ObservableList<SetupOwnerDealersInfo> dealersDetail;
    public static int totalDealers = 0;
    public static int selectedDealers = 0;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        if(SetupOwnerDealersInfo.districtName.equals(""))
        {
            // Skip This and Move to Next
        }
        else
        {
            SetupOwnerDealersInfo.lblDealersSelected = lbl_dealers_selected;
            SetupOwnerDealersInfo.table_viewdealers = table_viewdealers;
            objSetupOwnerDealersInfo = new SetupOwnerDealersInfo();
            sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
            dealer_name.setCellValueFactory(new PropertyValueFactory<>("dealerName"));
            dealer_contact.setCellValueFactory(new PropertyValueFactory<>("dealerContact"));
            dealer_address.setCellValueFactory(new PropertyValueFactory<>("dealerAddress"));
            dealer_cnic.setCellValueFactory(new PropertyValueFactory<>("dealerCnic"));
            dealer_type.setCellValueFactory(new PropertyValueFactory<>("dealerType"));
            operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
            lbl_total_dealers.setText(String.valueOf(totalDealers));

        }
    }

    public static ObservableList<SetupOwnerDealersInfo> parseUserList() {
        
        dealersDetail = FXCollections.observableArrayList();
        dealersData = SetupOwnerDealersInfo.dealersData;

        if(dealersData.size() <= 0)
        {
            Parent root = null;
            try {
                root = FXMLLoader.load(SetupOwnerDealers.class.getResource("/view/login.fxml"));
            } catch (IOException e) {
                e.printStackTrace();
            }
            GlobalVariables.baseScene.setRoot(root);
            GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
        }

        int num = 1;
        for (int i = 0; i < dealersData.size(); i++)
        {
            totalDealers++;
            SetupOwnerDealersInfo.dealerNameArr.add(dealersData.get(i).get(1));
            SetupOwnerDealersInfo.dealerContactPersonArr.add(dealersData.get(i).get(2));
            SetupOwnerDealersInfo.dealerContactArr.add(dealersData.get(i).get(3));
            SetupOwnerDealersInfo.dealerFaxArr.add(dealersData.get(i).get(4));
            SetupOwnerDealersInfo.dealerAddressArr.add(dealersData.get(i).get(5));
            SetupOwnerDealersInfo.dealerAddress1Arr.add(dealersData.get(i).get(6));
            SetupOwnerDealersInfo.dealerTypeArr.add(dealersData.get(i).get(7));
            SetupOwnerDealersInfo.dealerCnicArr.add(dealersData.get(i).get(8));
            SetupOwnerDealersInfo.dealerNtnArr.add(dealersData.get(i).get(9));
            SetupOwnerDealersInfo.dealerLic9NumArr.add(dealersData.get(i).get(10));
            SetupOwnerDealersInfo.dealerLic9ExpArr.add(dealersData.get(i).get(11));
            SetupOwnerDealersInfo.dealerLic10NumArr.add(dealersData.get(i).get(12));
            SetupOwnerDealersInfo.dealerLic10ExpArr.add(dealersData.get(i).get(13));
            SetupOwnerDealersInfo.dealerLic11NumArr.add(dealersData.get(i).get(14));
            SetupOwnerDealersInfo.dealerLic11ExpArr.add(dealersData.get(i).get(15));
            SetupOwnerDealersInfo.dealerLatitudeArr.add(dealersData.get(i).get(16));
            SetupOwnerDealersInfo.dealerLongitudeArr.add(dealersData.get(i).get(17));
            SetupOwnerDealersInfo.dealerLocationArr.add(dealersData.get(i).get(18));
            dealersDetail.add(new SetupOwnerDealersInfo(String.valueOf(i+1), dealersData.get(i).get(1), dealersData.get(i).get(3), dealersData.get(i).get(5), dealersData.get(i).get(8), dealersData.get(i).get(7)));
        }
        return dealersDetail;
    }

    @FXML
    void addClicked(ActionEvent event) {
        progress.setVisible(true);

        Thread thread = new Thread(){
            public void run(){
                boolean response;
                response = objSetupOwnerDealersInfo.insertDealer(GlobalVariables.objStmt);

                Platform.runLater(new Runnable(){
                    @Override
                    public void run() {
                        if(response)
                        {
//                            SetupOwnerDealersInfo.businessCategory = businessCategory;
                            String title = "Success";
                            String message = "Dealers Record Added";
                            GlobalVariables.showNotification(1, title, message);

                            //                             ------------------------------------ Setting Core File -------------------------
                            String directoryName = "secret-credentials";
                            File directory = new File(directoryName);
                            if (!directory.exists()){
                                directory.mkdir();
                            }
                            PrintWriter writer = null;
                            try {
                                writer = new PrintWriter("secret-credentials/data.txt", "UTF-8");
                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                            writer.println(GlobalVariables.softwareLicId);
                            writer.println(GlobalVariables.softwareLicNumber);
                            writer.println(GlobalVariables.softwareLicRegDate);
                            writer.println(GlobalVariables.softwareLicExpDate);
                            writer.println("Done");
                            writer.println(GlobalVariables.workingDistrict);
                            writer.close();
                            String currentDirectory = System.getProperty("user.dir");
                            String command = "cd "+currentDirectory+" & attrib +h +r +s secret-credentials";

                            ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c", command);
                            builder.redirectErrorStream(true);
                            try {
                                Process p = builder.start();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
//                             ------------------------------------ Setting Core File -------------------------

                            Parent root = null;
                            try {
                                root = FXMLLoader.load(getClass().getResource("/view/login.fxml"));
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            GlobalVariables.baseScene.setRoot(root);
                            GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
                        }
                        else
                        {
                            String title = "Error";
                            String message = "Record Not Added";
                            GlobalVariables.showNotification(-1, title, message);
                        }
                    }
                    // do your GUI stuff here
                });
            }
        };
        thread.start();
    }

    @FXML
    void selectAllClicked(ActionEvent event) {
        selectedDealers = 0;
        for(int i=0; i<SetupOwnerDealersInfo.arrCheckBox.size(); i++)
        {
            selectedDealers++;
            SetupOwnerDealersInfo.arrCheckBox.get(i).setSelected(true);
        }
        lbl_dealers_selected.setText(String.valueOf(selectedDealers));

//        SetupOwnerDealersInfo.table_viewdealers.setRowFactory(tv -> new TableRow<SetupOwnerDealersInfo>() {
//            @Override
//            protected void updateItem(SetupOwnerDealersInfo item, boolean empty) {
//                super.updateItem(item, empty);
//                item = new SetupOwnerDealersInfo();
//                for(int i=0; i<SetupOwnerDealersInfo.arrCheckBox.size(); i++)
//                {
//                    if(SetupOwnerDealersInfo.arrCheckBox.get(i).isSelected())
//                    {
//                        setStyle("-fx-background-color: #ffd7d1;");
//                    }
//                }
//
//            }
//        });
    }

    @FXML
    void skipClicked(ActionEvent event) {
        //                             ------------------------------------ Setting Core File -------------------------
        String directoryName = "secret-credentials";
        File directory = new File(directoryName);
        if (!directory.exists()){
            directory.mkdir();
        }
        PrintWriter writer = null;
        try {
            writer = new PrintWriter("secret-credentials/data.txt", "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        writer.println(GlobalVariables.softwareLicId);
        writer.println(GlobalVariables.softwareLicNumber);
        writer.println(GlobalVariables.softwareLicRegDate);
        writer.println(GlobalVariables.softwareLicExpDate);
        writer.println("Done");
        writer.println(GlobalVariables.workingDistrict);
        writer.close();
        String currentDirectory = System.getProperty("user.dir");
        String command = "cd "+currentDirectory+" & attrib +h +r +s secret-credentials";

        ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c", command);
        builder.redirectErrorStream(true);
        try {
            Process p = builder.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
//                             ------------------------------------ Setting Core File -------------------------


        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/login.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void unSelectAllClicked(ActionEvent event) {
        for(int i=0; i<SetupOwnerDealersInfo.arrCheckBox.size(); i++)
        {
            SetupOwnerDealersInfo.arrCheckBox.get(i).setSelected(false);
        }
        selectedDealers = 0;
        lbl_dealers_selected.setText(String.valueOf(selectedDealers));

//        SetupOwnerDealersInfo.table_viewdealers.setRowFactory(tv -> new TableRow<SetupOwnerDealersInfo>() {
//            @Override
//            protected void updateItem(SetupOwnerDealersInfo item, boolean empty) {
//                super.updateItem(item, empty);
//                item = new SetupOwnerDealersInfo();
//                for(int i=0; i<SetupOwnerDealersInfo.arrCheckBox.size(); i++)
//                {
//                    if(SetupOwnerDealersInfo.arrCheckBox.get(i).isSelected())
//                    {
//                        setStyle("-fx-background-color: #ffd7d1;");
//                    }
//                }
//
//            }
//        });
    }
}
