package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTextField;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.AdjustInvoiceStockInfo;
import model.DrawingSummaryInfo;
import model.MysqlCon;
import model.OrderedProductsDetailInfo;
import sun.java2d.loops.DrawGlyphList;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class OrderedProductsDetail implements Initializable {

    @FXML
    private BorderPane menu_bar1;
    @FXML
    private TableView<OrderedProductsDetailInfo> table_orderedproduct;

    @FXML
    private TableColumn<OrderedProductsDetailInfo, String> sr_no;

    @FXML
    private TableColumn<OrderedProductsDetailInfo, String> order_id;

    @FXML
    private TableColumn<OrderedProductsDetailInfo, String> dealer_id;

    @FXML
    private TableColumn<OrderedProductsDetailInfo, String> deale_name;
    @FXML
    private TableColumn<OrderedProductsDetailInfo, String> batch_num;

    @FXML
    private TableColumn<OrderedProductsDetailInfo, String> ordered_quantity;

    @FXML
    private TableColumn<OrderedProductsDetailInfo, String> ordered_unit;

    @FXML
    private TableColumn<OrderedProductsDetailInfo, String> submission_quantity;

    @FXML
    private TableColumn<OrderedProductsDetailInfo, String> submission_unit;

    @FXML
    private TableColumn<OrderedProductsDetailInfo, String> org_price;

    @FXML
    private TableColumn<OrderedProductsDetailInfo, String> discount;

    @FXML
    private TableColumn<OrderedProductsDetailInfo, String> final_price;

    @FXML
    private TableColumn<OrderedProductsDetailInfo, String> date;

    @FXML
    private TableColumn<OrderedProductsDetailInfo, String> operations;

    @FXML
    private MenuBar menu_bar;
    @FXML
    private JFXTextField txt_prodName;
    @FXML
    private BorderPane nav_sales;
    @FXML
    private JFXTextField txt_prodID;

    public ObservableList<OrderedProductsDetailInfo> orderedProductsList;

    public static String productIdDetail;
    @FXML    private JFXButton btn_goback;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //productIdDetail = AdjustInvoiceStockInfo.productIdDetail;
        productIdDetail ="2";
                txt_prodID.setText(productIdDetail);
        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        order_id.setCellValueFactory(new PropertyValueFactory<>("orderId"));
        dealer_id.setCellValueFactory(new PropertyValueFactory<>("dealerId"));
        deale_name.setCellValueFactory(new PropertyValueFactory<>("dealerName"));
        batch_num.setCellValueFactory(new PropertyValueFactory<>("batchnumber"));
        ordered_quantity.setCellValueFactory(new PropertyValueFactory<>("orderedQty"));
        ordered_unit.setCellValueFactory(new PropertyValueFactory<>("orderedUnit"));
        submission_quantity.setCellValueFactory(new PropertyValueFactory<>("txtSubmissionQty"));
        submission_unit.setCellValueFactory(new PropertyValueFactory<>("comboSubmissionUnit"));
        org_price.setCellValueFactory(new PropertyValueFactory<>("orgPrice"));
        discount.setCellValueFactory(new PropertyValueFactory<>("discount"));
        final_price.setCellValueFactory(new PropertyValueFactory<>("finalPrice"));
        date.setCellValueFactory(new PropertyValueFactory<>("date"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
//        submission_quantity.setCellFactory(TextFieldTableCell.forTableColumn());
//        submission_quantity.setOnEditCommit(event -> System.out.println("Editied"));
        table_orderedproduct.setItems(parseUserList());
//        table_orderedproduct.setEditable(true);
    }

    private ObservableList<OrderedProductsDetailInfo> parseUserList(){
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        ResultSet rs = null;
        ArrayList<String> temp;
        try {
            rs = objStmt.executeQuery("SELECT `product_name` FROM `product_info` WHERE `product_id` = '"+productIdDetail+"'");
            rs.next();
            txt_prodName.setText(rs.getString(1));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        OrderedProductsDetailInfo objOrderedProductDetailInfo = new OrderedProductsDetailInfo();
        orderedProductsList = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> orderedProductData = objOrderedProductDetailInfo.getOrderedProductDetail(objStmt, objCon, productIdDetail);
        for (int i = 0; i < orderedProductData.size(); i++)
        {
            orderedProductsList.add(new OrderedProductsDetailInfo(String.valueOf(i+1), orderedProductData.get(i).get(0), orderedProductData.get(i).get(1), orderedProductData.get(i).get(2), orderedProductData.get(i).get(3), orderedProductData.get(i).get(4), orderedProductData.get(i).get(5), orderedProductData.get(i).get(6), orderedProductData.get(i).get(7), orderedProductData.get(i).get(8), orderedProductData.get(i).get(9),orderedProductData.get(i).get(10) ,orderedProductData.get(i).get(11), "Change"));
        }
        return orderedProductsList;
    }

    /////////////////////////////////////////////////////////////////////////////ANOTHER WAY TO OPEN NEW WINDOW
    @FXML
    public void viewDrawingSummary(ActionEvent event) throws IOException {
    try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/view/drawing_summary.fxml"));
            Scene scene = new Scene(fxmlLoader.load());
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.show();
            ((Node)(event.getSource())).getScene().getWindow().hide();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void viewHome() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/home.fxml"));
        Stage objstage = (Stage) menu_bar1.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    public void viewPendings() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_pending_orders.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    public void SaleInvoices() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/sale_invoice.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    public void addCompany() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/enter_company.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }
    @FXML
    public void viewCompanies() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_company.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }
    @FXML
    public void viewAreas() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_area.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }
    @FXML
    public void viewSubAreas() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_subarea.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }
    @FXML
    public void viewDealers() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_dealers.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }
    @FXML
    public void viewSalesman() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_salesman.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }
    @FXML
    public void viewSuppliers() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_suppliers.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }
    @FXML
    public void viewProducts() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_products.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }
    @FXML
    public void viewSystemAccounts() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/system_accounts.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }
}
