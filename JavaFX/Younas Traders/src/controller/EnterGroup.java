package controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.util.Duration;
import model.*;
import model.GroupInfo;
import model.GroupInfo;
import org.controlsfx.control.textfield.TextFields;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class EnterGroup implements Initializable {

    @FXML
    private BorderPane menu_bar1;

    @FXML
    private BorderPane nav_setup;

    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private JFXTextField txt_group_id;

    @FXML
    private JFXComboBox<String> txt_company_name;

    @FXML
    private JFXTextField txt_group_name;

    @FXML
    private Button cancelBtn;

    @FXML
    private Button saveBtn;

    @FXML
    private TableView<GroupInfo> table_addgroup;

    @FXML
    private TableColumn<GroupInfo, String> sr_no;

    @FXML
    private TableColumn<GroupInfo, String> group_id;

    @FXML
    private TableColumn<GroupInfo, String> company_name;

    @FXML
    private TableColumn<GroupInfo, String> groups_name;

    @FXML
    private TableColumn<GroupInfo, String> operations;

    @FXML
    private Button add_group;

    @FXML    private Button btn_edit_cancel;
    @FXML    private Button btn_edit_update;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    public static ObservableList<GroupInfo> groupDetails;
    Alert alert = new Alert(Alert.AlertType.NONE);
    GroupInfo objGroupInfo;
    public static int srNo;
    public static Button btnView;

    public void initialize(URL url, ResourceBundle resourceBundle) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnSetup.setStyle("-fx-background-color: #47ab1e");
        objGroupInfo = new GroupInfo();
        btnpane.setOnMouseEntered(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(22);
                btnpane.setMinWidth(20.0);
                btn11.setMinWidth(20.0);

            }
        });
        btnpane.setOnMouseExited(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(14);
                btnpane.setMinWidth(10.0);
                btn11.setMinWidth(10.0);
            }
        });

        inner_anchor.setLeftAnchor(inner_anchor, 280d);
        drawer.setSidePane(nav_setup);
        drawer.open();

        ArrayList<String> chosenCompanyNames = objGroupInfo.getSavedCompanyNames();
        txt_company_name.getItems().addAll(chosenCompanyNames);

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        group_id.setCellValueFactory(new PropertyValueFactory<>("groupId"));
        company_name.setCellValueFactory(new PropertyValueFactory<>("companyName"));
        groups_name.setCellValueFactory(new PropertyValueFactory<>("groupName"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        GroupInfo.table_addgroup = table_addgroup;
        table_addgroup.setItems(parseUserList());
        GroupInfo.txtGroupId = txt_group_id;
        GroupInfo.txtCompanyName = txt_company_name;
        GroupInfo.txtGroupName = txt_group_name;
        GroupInfo.btnAdd = add_group;
        GroupInfo.btnCancel = btn_edit_cancel;
        GroupInfo.btnUpdate = btn_edit_update;
    }

    public static ObservableList<GroupInfo> parseUserList(){
        GroupInfo objGroupInfo = new GroupInfo();
        groupDetails = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> groupData = objGroupInfo.getAddedGroups();
        for (int i = 0; i < groupData.size(); i++)
        {
            groupDetails.add(new GroupInfo(String.valueOf(i+1), ((groupData.get(i).get(0) == null || groupData.get(i).get(0).equals("")) ? "N/A" : groupData.get(i).get(0)), groupData.get(i).get(1), groupData.get(i).get(2)));
        }
        return groupDetails;
    }

    @FXML
    void addGroup(ActionEvent event) {
        String groupId = txt_group_id.getText();
        String companyId = String.valueOf(objGroupInfo.getSavedCompanyIds().get(txt_company_name.getSelectionModel().getSelectedIndex()));
        String companyName = txt_company_name.getValue();
        String groupName = txt_group_name.getText();
        String groupStatus = "Active";

        MysqlCon objMysqlCon1 = new MysqlCon();
        Statement objStmt1 = objMysqlCon1.stmt;
        Connection objCon1 = objMysqlCon1.con;
        String comp = objGroupInfo.confirmNewId(objStmt1, objCon1,groupId);

        if(!comp.equals(groupId) || comp.equals("")) {
            MysqlCon objMysqlCon = new MysqlCon();
            Statement objStmt = objMysqlCon.stmt;
            Connection objCon = objMysqlCon.con;
            GroupInfo.groupIdArr.add(groupId);
            GroupInfo.companyIdArr.add(companyId);
            GroupInfo.companyNameArr.add(companyName);
            GroupInfo.groupNameArr.add(groupName);
            table_addgroup.setItems(parseUserList());
            refreshControls();
        }
        else
        {
            alert.setAlertType(Alert.AlertType.ERROR);
            alert.setHeaderText("Error!");
            alert.setContentText("Group ID Already Saved.");
            alert.show();
        }
    }

    @FXML
    void editCancel(ActionEvent event) {
        refreshControls();
        add_group.setVisible(true);
        btn_edit_cancel.setVisible(false);
        btn_edit_update.setVisible(false);
    }

    @FXML
    void editUpdate(ActionEvent event) {
        String groupId = txt_group_id.getText();
        String companyId = String.valueOf(objGroupInfo.getSavedCompanyIds().get(txt_company_name.getSelectionModel().getSelectedIndex()));
        String companyName = txt_company_name.getValue();
        String groupName = txt_group_name.getText();
        String groupStatus = "Active";

        MysqlCon objMysqlCon1 = new MysqlCon();
        Statement objStmt1 = objMysqlCon1.stmt;
        Connection objCon1 = objMysqlCon1.con;
        String comp = objGroupInfo.confirmNewId(objStmt1, objCon1, groupId);

        if(!comp.equals(groupId) || comp.equals("")) {
            MysqlCon objMysqlCon = new MysqlCon();
            Statement objStmt = objMysqlCon.stmt;
            Connection objCon = objMysqlCon.con;
            GroupInfo.groupIdArr.set(srNo, groupId);
            GroupInfo.companyIdArr.set(srNo, companyId);
            GroupInfo.companyNameArr.set(srNo, companyName);
            GroupInfo.groupNameArr.set(srNo, groupName);
            table_addgroup.setItems(parseUserList());
            refreshControls();
        }
        else {
            alert.setAlertType(Alert.AlertType.ERROR);
            alert.setHeaderText("Error!");
            alert.setContentText("Group ID Already Saved.");
            alert.show();
        }

        add_group.setVisible(true);
        btn_edit_cancel.setVisible(false);
        btn_edit_update.setVisible(false);
    }

    @FXML
    void cancelClicked(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_groups.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    public void follow() throws IOException
    {
        if (drawer.isOpened()) {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(-250);
            transition.setByY(0);
            transition.setCycleCount(1);
            transition.play();
            drawer.close();
            FA_Icon.setGlyphName("ANGLE_DOUBLE_RIGHT");
            FA_Icon.setGlyphSize(14);
            //inner_anchor.setRightAnchor(inner_anchor,1d);
            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(-90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(1.1000);
            scale.setToY(1);

            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();

        }
        else {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(250);
            transition.setByY(0);
            transition.setCycleCount(1);
            FA_Icon.setGlyphName("ANGLE_DOUBLE_LEFT");
            FA_Icon.setGlyphSize(14);
            drawer.open();

            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(0.99947777);
            scale.setToY(1);



            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();

        }


    }

    @FXML
    void saveClicked(ActionEvent event) throws IOException {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        try {
            objGroupInfo.insertGroup(objStmt, objCon);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Parent root = FXMLLoader.load(getClass().getResource("/view/view_groups.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    public void refreshControls()
    {
        this.txt_group_id.setText("");
        this.txt_company_name.getSelectionModel().clearSelection();
        this.txt_group_name.setText("");
    }
}