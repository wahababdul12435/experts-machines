package controller;

import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.transitions.hamburger.HamburgerBackArrowBasicTransition;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.*;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.util.StringConverter;
import model.*;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

public class DashboardFinance implements Initializable {

    @FXML
    private BorderPane menu_bar;
    @FXML
    private JFXHamburger hamburger;
    @FXML
    private JFXDrawer drawer;
    @FXML
    private VBox Vbox_btns;
    @FXML
    private AnchorPane inner_anchor;
    @FXML
    private Button btn11;
    @FXML
    private Pane btnpane;
    @FXML
    private FontAwesomeIconView FA_Icon;
    @FXML
    private BorderPane brdrpane_navfinances;

    @FXML
    private BarChart<String, Integer> dealer_finance_chart;

    @FXML
    private CategoryAxis dealerTimeAxis;

    @FXML
    private NumberAxis dealerCashAxis;

    @FXML
    private BarChart<String, Integer> company_finance_chart;

    @FXML
    private CategoryAxis companyTimeAxis;

    @FXML
    private NumberAxis companyCashAxis;

    @FXML
    private JFXDatePicker txt_from_date;

    @FXML
    private JFXDatePicker txt_to_date;

    public static Button btnView;
    public static String txtFromDate;
    public static String txtToDate;
    public static boolean filter;

    ArrayList<ArrayList<String>> stDataDealer = new ArrayList<>();
    ArrayList<ArrayList<String>> stDataCompany = new ArrayList<>();
    ArrayList<Date> dates = new ArrayList<Date>();
    SimpleDateFormat fmt = new SimpleDateFormat("dd/MMM/yyyy");
    int span;
    private String stStartDate;
    private String stEndDate;
    Date tempDate;
    private DashboardFinanceInfo objDashboardFinanceInfo;
    MysqlCon objMysqlCon;
    Statement objStmt;
    Connection objCon;
    int intervals = 8;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnFinance.setStyle("-fx-background-color: #47ab1e");
        objDashboardFinanceInfo = new DashboardFinanceInfo();
        objMysqlCon = new MysqlCon();
        objStmt = objMysqlCon.stmt;
        objCon = objMysqlCon.con;

        btnpane.setOnMouseEntered(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(22);
                btnpane.setMinWidth(20.0);
                btn11.setMinWidth(20.0);

            }
        });
        btnpane.setOnMouseExited(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(14);
                btnpane.setMinWidth(10.0);
                btn11.setMinWidth(10.0);
            }
        });

        txt_from_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        txt_to_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        inner_anchor.setLeftAnchor(inner_anchor, 260d);
        drawer.setSidePane(brdrpane_navfinances);
        drawer.open();

        dealerTimeAxis.setLabel("Time Span");
        dealerCashAxis.setLabel("Dealer Cash");
        companyTimeAxis.setLabel("Time Span");
        companyCashAxis.setLabel("Company Cash");


        if(filter)
        {
            if (txtFromDate != null && !txtFromDate.equals("")) {
                txt_from_date.setValue(GlobalVariables.LOCAL_DATE(txtFromDate));
                stStartDate = txtFromDate;
            }
            if (txtToDate != null && !txtToDate.equals("")) {
                txt_to_date.setValue(GlobalVariables.LOCAL_DATE(txtToDate));
                stEndDate = txtToDate;
            }
            filter = false;
        }
        else
        {
            setDateRange();
        }

        setBarChartDates();
        setDealerBarChart();
        XYChart.Series dealerDataSeries1 = new XYChart.Series();
        dealerDataSeries1.setName("Collection");
        XYChart.Series dealerDataSeries2 = new XYChart.Series();
        dealerDataSeries2.setName("Return");
        XYChart.Series dealerDataSeries3 = new XYChart.Series();
        dealerDataSeries3.setName("Waived Off");
        for(int i=stDataDealer.size()-2; i>=0; i--)
        {
            if(stDataDealer.get(i).get(0).equals(stDataDealer.get(stDataDealer.size()-1).get(0)))
            {
                dealerDataSeries1.getData().add(new XYChart.Data(stDataDealer.get(i+1).get(0), (Integer.parseInt(stDataDealer.get(i).get(1)))));
                dealerDataSeries2.getData().add(new XYChart.Data(stDataDealer.get(i+1).get(0), (Integer.parseInt(stDataDealer.get(i).get(2)))));
                dealerDataSeries3.getData().add(new XYChart.Data(stDataDealer.get(i+1).get(0), (Integer.parseInt(stDataDealer.get(i).get(3)))));
                break;
            }
            String stLowerBoundDate = stDataDealer.get(i).get(0);
            Date lowerBoundDate = null;
            try {
                lowerBoundDate = fmt.parse(stLowerBoundDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Calendar calTemp = Calendar.getInstance();
            calTemp.setTime(lowerBoundDate);
            calTemp.add(Calendar.DAY_OF_MONTH, -1);
            try {
                lowerBoundDate = fmt.parse(fmt.format(calTemp.getTime()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            fmt.format(calTemp.getTime());
            stLowerBoundDate = fmt.format(calTemp.getTime());
            if(span < -1)
            {
                dealerDataSeries1.getData().add(new XYChart.Data(stDataDealer.get(i+1).get(0)+"\n|\n"+stLowerBoundDate, (Integer.parseInt(stDataDealer.get(i).get(1)))));
                dealerDataSeries2.getData().add(new XYChart.Data(stDataDealer.get(i+1).get(0)+"\n|\n"+stLowerBoundDate, (Integer.parseInt(stDataDealer.get(i).get(2)))));
                dealerDataSeries3.getData().add(new XYChart.Data(stDataDealer.get(i+1).get(0)+"\n|\n"+stLowerBoundDate, (Integer.parseInt(stDataDealer.get(i).get(3)))));
            }
            else
            {
                dealerDataSeries1.getData().add(new XYChart.Data(stDataDealer.get(i).get(0), (Integer.parseInt(stDataDealer.get(i).get(1)))));
                dealerDataSeries2.getData().add(new XYChart.Data(stDataDealer.get(i).get(0), (Integer.parseInt(stDataDealer.get(i).get(2)))));
                dealerDataSeries3.getData().add(new XYChart.Data(stDataDealer.get(i).get(0), (Integer.parseInt(stDataDealer.get(i).get(3)))));
            }

        }
        dealer_finance_chart.getData().add(dealerDataSeries1);
        dealer_finance_chart.getData().add(dealerDataSeries2);
        dealer_finance_chart.getData().add(dealerDataSeries3);

        setCompanyBarChart();
        XYChart.Series companyDataSeries1 = new XYChart.Series();
        companyDataSeries1.setName("Sent");
        XYChart.Series companyDataSeries2 = new XYChart.Series();
        companyDataSeries2.setName("Received");
        XYChart.Series companyDataSeries3 = new XYChart.Series();
        companyDataSeries3.setName("Waived Off");
        for(int i=stDataCompany.size()-2; i>=0; i--)
        {
            if(stDataCompany.get(i).get(0).equals(stDataCompany.get(stDataCompany.size()-1).get(0)))
            {
                companyDataSeries1.getData().add(new XYChart.Data(stDataCompany.get(i+1).get(0), (Integer.parseInt(stDataCompany.get(i).get(1)))));
                companyDataSeries2.getData().add(new XYChart.Data(stDataCompany.get(i+1).get(0), (Integer.parseInt(stDataCompany.get(i).get(2)))));
                companyDataSeries3.getData().add(new XYChart.Data(stDataCompany.get(i+1).get(0), (Integer.parseInt(stDataCompany.get(i).get(3)))));
                break;
            }
            String stLowerBoundDate = stDataCompany.get(i).get(0);
            Date lowerBoundDate = null;
            try {
                lowerBoundDate = fmt.parse(stLowerBoundDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Calendar calTemp = Calendar.getInstance();
            calTemp.setTime(lowerBoundDate);
            calTemp.add(Calendar.DAY_OF_MONTH, -1);
            try {
                lowerBoundDate = fmt.parse(fmt.format(calTemp.getTime()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            fmt.format(calTemp.getTime());
            stLowerBoundDate = fmt.format(calTemp.getTime());
            if(span < -1)
            {
                companyDataSeries1.getData().add(new XYChart.Data(stDataCompany.get(i+1).get(0)+"\n|\n"+stLowerBoundDate, (Integer.parseInt(stDataCompany.get(i).get(1)))));
                companyDataSeries2.getData().add(new XYChart.Data(stDataCompany.get(i+1).get(0)+"\n|\n"+stLowerBoundDate, (Integer.parseInt(stDataCompany.get(i).get(2)))));
                companyDataSeries3.getData().add(new XYChart.Data(stDataCompany.get(i+1).get(0)+"\n|\n"+stLowerBoundDate, (Integer.parseInt(stDataCompany.get(i).get(3)))));
            }
            else
            {
                companyDataSeries1.getData().add(new XYChart.Data(stDataCompany.get(i).get(0), (Integer.parseInt(stDataCompany.get(i).get(1)))));
                companyDataSeries2.getData().add(new XYChart.Data(stDataCompany.get(i).get(0), (Integer.parseInt(stDataCompany.get(i).get(2)))));
                companyDataSeries3.getData().add(new XYChart.Data(stDataCompany.get(i).get(0), (Integer.parseInt(stDataCompany.get(i).get(3)))));
            }

        }
        company_finance_chart.getData().add(companyDataSeries1);
        company_finance_chart.getData().add(companyDataSeries2);
        company_finance_chart.getData().add(companyDataSeries3);
    }

    @FXML
    void btnSearch(ActionEvent event) {
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date selectedDate = null;
        if(txt_from_date.getValue() != null)
        {
            txtFromDate = txt_from_date.getValue().toString();
            try {
                selectedDate = sdf.parse(txtFromDate);
                txtFromDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if(txt_to_date.getValue() != null)
        {
            txtToDate = txt_to_date.getValue().toString();
            try {
                selectedDate = sdf.parse(txtToDate);
                txtToDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        filter = true;

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/dashboard_finance.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    public void follow() throws IOException
    {
    /*PathTransition transition;
    transition = new PathTransition();
    Polyline mpoly = new Polyline();
    mpoly.getPoints().addAll(new Double[]{
            0.0,0.0,
            0.0,0.0,
            -200.0,0.0});
    transition.setNode(btn11);
    transition.setDuration(Duration.seconds(2));
    transition.setPath(mpoly);
    transition.play();*/
        if (drawer.isOpened()) {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(-250);
            transition.setByY(0);
            transition.setCycleCount(1);
            transition.play();
            drawer.close();
            FA_Icon.setGlyphName("ANGLE_DOUBLE_RIGHT");
            FA_Icon.setGlyphSize(14);
            //inner_anchor.setRightAnchor(inner_anchor,1d);


            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(-90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(1.31017);
            scale.setToY(1);

            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();
            //
            //inner_anchor.setLeftAnchor(inner_anchor, 5d);
        } else {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(250);
            transition.setByY(0);
            transition.setCycleCount(1);
            FA_Icon.setGlyphName("ANGLE_DOUBLE_LEFT");
            FA_Icon.setGlyphSize(14);
            drawer.open();

            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(0.99947777);
            scale.setToY(1);

            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();
        }

    }

    private void setDateRange()
    {
        stEndDate = GlobalVariables.getStDate();
        Date endDate = null;
        try {
            endDate = fmt.parse(stEndDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(endDate);
        cal1.add(Calendar.DAY_OF_MONTH, -120);
        stStartDate = fmt.format(cal1.getTime());
        txt_from_date.setValue(GlobalVariables.LOCAL_DATE(stStartDate));
        txt_to_date.setValue(GlobalVariables.LOCAL_DATE(stEndDate));
    }

    private void setBarChartDates()
    {
        Date endDate = null;
        Date startDate = null;
        try {
            startDate = fmt.parse(stStartDate);
            endDate = fmt.parse(stEndDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long diff = endDate.getTime() - startDate.getTime();
        diff = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
        span = (int) (diff/intervals);
        span = span * -1;
        Calendar cal = Calendar.getInstance();
        cal.setTime(endDate);
        ArrayList<String> temp1 = new ArrayList<>();
        ArrayList<String> temp2 = new ArrayList<>();
        temp1.add(fmt.format(cal.getTime()));
        temp1.add("0");
        temp1.add("0");
        temp1.add("0");
        temp2.add(fmt.format(cal.getTime()));
        temp2.add("0");
        temp2.add("0");
        temp2.add("0");
        stDataDealer.add(temp1);
        stDataCompany.add(temp2);
        tempDate = endDate;
        dates.add(tempDate);
        for(int i=1; i<=intervals; i++)
        {
            temp1 = new ArrayList<>();
            temp2 = new ArrayList<>();
            cal.add(Calendar.DAY_OF_MONTH, span);
            try {
                tempDate = fmt.parse(fmt.format(cal.getTime()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            temp1.add(fmt.format(cal.getTime()));
            temp1.add("0");
            temp1.add("0");
            temp1.add("0");
            temp2.add(fmt.format(cal.getTime()));
            temp2.add("0");
            temp2.add("0");
            temp2.add("0");
            stDataDealer.add(temp1);
            stDataCompany.add(temp2);
            dates.add(tempDate);
        }
    }

    private void setDealerBarChart()
    {
        ArrayList<ArrayList<String>> dealerFinanceData;
        dealerFinanceData = objDashboardFinanceInfo.getDealerFinanceDetail(objStmt, objCon, stStartDate, stEndDate);

//        System.out.println(dealerFinanceData);
        for (int i = 0; i < dealerFinanceData.size(); i++)
        {
            try {
                tempDate = fmt.parse(dealerFinanceData.get(i).get(1));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if((tempDate.before(dates.get(0)) && tempDate.after(dates.get(1))) || tempDate.equals(dates.get(0)))
            {
//                    System.out.println("1st: Small Date: "+dates.get(0)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(1));
                if(dealerFinanceData.get(i).get(2).equals("Collection"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataDealer.get(0).get(1)) + Integer.parseInt(dealerFinanceData.get(i).get(0)));
                    stDataDealer.get(0).set(1, value);
                }
                else if(dealerFinanceData.get(i).get(2).equals("Return"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataDealer.get(0).get(2)) + Integer.parseInt(dealerFinanceData.get(i).get(0)));
                    stDataDealer.get(0).set(2, value);
                }
                else
                {
                    String value = String.valueOf(Integer.parseInt(stDataDealer.get(0).get(3)) + Integer.parseInt(dealerFinanceData.get(i).get(0)));
                    stDataDealer.get(0).set(3, value);
                }

            }
            else if((tempDate.before(dates.get(1)) && tempDate.after(dates.get(2))) || tempDate.equals(dates.get(1)))
            {
//                    System.out.println("2nd: Small Date: "+dates.get(1)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(2));
                if(dealerFinanceData.get(i).get(2).equals("Collection"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataDealer.get(1).get(1)) + Integer.parseInt(dealerFinanceData.get(i).get(0)));
                    stDataDealer.get(1).set(1, value);
                }
                else if(dealerFinanceData.get(i).get(2).equals("Return"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataDealer.get(1).get(2)) + Integer.parseInt(dealerFinanceData.get(i).get(0)));
                    stDataDealer.get(1).set(2, value);
                }
                else
                {
                    String value = String.valueOf(Integer.parseInt(stDataDealer.get(1).get(3)) + Integer.parseInt(dealerFinanceData.get(i).get(0)));
                    stDataDealer.get(1).set(3, value);
                }
            }
            else if((tempDate.before(dates.get(2)) && tempDate.after(dates.get(3))) || tempDate.equals(dates.get(2)))
            {
//                    System.out.println("3rd: Small Date: "+dates.get(2)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(3));
                if(dealerFinanceData.get(i).get(2).equals("Collection"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataDealer.get(2).get(1)) + Integer.parseInt(dealerFinanceData.get(i).get(0)));
                    stDataDealer.get(2).set(1, value);
                }
                else if(dealerFinanceData.get(i).get(2).equals("Return"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataDealer.get(2).get(2)) + Integer.parseInt(dealerFinanceData.get(i).get(0)));
                    stDataDealer.get(2).set(2, value);
                }
                else
                {
                    String value = String.valueOf(Integer.parseInt(stDataDealer.get(2).get(3)) + Integer.parseInt(dealerFinanceData.get(i).get(0)));
                    stDataDealer.get(2).set(3, value);
                }
            }
            else if((tempDate.before(dates.get(3)) && tempDate.after(dates.get(4))) || tempDate.equals(dates.get(3)))
            {
//                    System.out.println("4th: Small Date: "+dates.get(3)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(4));
                if(dealerFinanceData.get(i).get(2).equals("Collection"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataDealer.get(3).get(1)) + Integer.parseInt(dealerFinanceData.get(i).get(0)));
                    stDataDealer.get(3).set(1, value);
                }
                else if(dealerFinanceData.get(i).get(2).equals("Return"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataDealer.get(3).get(2)) + Integer.parseInt(dealerFinanceData.get(i).get(0)));
                    stDataDealer.get(3).set(2, value);
                }
                else
                {
                    String value = String.valueOf(Integer.parseInt(stDataDealer.get(3).get(3)) + Integer.parseInt(dealerFinanceData.get(i).get(0)));
                    stDataDealer.get(3).set(3, value);
                }
            }
            else if((tempDate.before(dates.get(4)) && tempDate.after(dates.get(5))) || tempDate.equals(dates.get(4)))
            {
//                    System.out.println("4th: Small Date: "+dates.get(3)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(4));
                if(dealerFinanceData.get(i).get(2).equals("Collection"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataDealer.get(4).get(1)) + Integer.parseInt(dealerFinanceData.get(i).get(0)));
                    stDataDealer.get(4).set(1, value);
                }
                else if(dealerFinanceData.get(i).get(2).equals("Return"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataDealer.get(4).get(2)) + Integer.parseInt(dealerFinanceData.get(i).get(0)));
                    stDataDealer.get(4).set(2, value);
                }
                else
                {
                    String value = String.valueOf(Integer.parseInt(stDataDealer.get(4).get(3)) + Integer.parseInt(dealerFinanceData.get(i).get(0)));
                    stDataDealer.get(4).set(3, value);
                }
            }
            else if((tempDate.before(dates.get(5)) && tempDate.after(dates.get(6))) || tempDate.equals(dates.get(5)))
            {
//                    System.out.println("4th: Small Date: "+dates.get(3)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(4));
                if(dealerFinanceData.get(i).get(2).equals("Collection"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataDealer.get(5).get(1)) + Integer.parseInt(dealerFinanceData.get(i).get(0)));
                    stDataDealer.get(5).set(1, value);
                }
                else if(dealerFinanceData.get(i).get(2).equals("Return"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataDealer.get(5).get(2)) + Integer.parseInt(dealerFinanceData.get(i).get(0)));
                    stDataDealer.get(5).set(2, value);
                }
                else
                {
                    String value = String.valueOf(Integer.parseInt(stDataDealer.get(5).get(3)) + Integer.parseInt(dealerFinanceData.get(i).get(0)));
                    stDataDealer.get(5).set(3, value);
                }
            }
            else if((tempDate.before(dates.get(6)) && tempDate.after(dates.get(7))) || tempDate.equals(dates.get(6)))
            {
//                    System.out.println("4th: Small Date: "+dates.get(3)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(4));
                if(dealerFinanceData.get(i).get(2).equals("Collection"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataDealer.get(6).get(1)) + Integer.parseInt(dealerFinanceData.get(i).get(0)));
                    stDataDealer.get(6).set(1, value);
                }
                else if(dealerFinanceData.get(i).get(2).equals("Return"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataDealer.get(6).get(2)) + Integer.parseInt(dealerFinanceData.get(i).get(0)));
                    stDataDealer.get(6).set(2, value);
                }
                else
                {
                    String value = String.valueOf(Integer.parseInt(stDataDealer.get(6).get(3)) + Integer.parseInt(dealerFinanceData.get(i).get(0)));
                    stDataDealer.get(6).set(3, value);
                }
            }
            else if((tempDate.before(dates.get(7)) && tempDate.after(dates.get(8))) || tempDate.equals(dates.get(7)))
            {
//                    System.out.println("4th: Small Date: "+dates.get(3)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(4));
                if(dealerFinanceData.get(i).get(2).equals("Collection"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataDealer.get(7).get(1)) + Integer.parseInt(dealerFinanceData.get(i).get(0)));
                    stDataDealer.get(7).set(1, value);
                }
                else if(dealerFinanceData.get(i).get(2).equals("Return"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataDealer.get(7).get(2)) + Integer.parseInt(dealerFinanceData.get(i).get(0)));
                    stDataDealer.get(7).set(2, value);
                }
                else
                {
                    String value = String.valueOf(Integer.parseInt(stDataDealer.get(7).get(3)) + Integer.parseInt(dealerFinanceData.get(i).get(0)));
                    stDataDealer.get(7).set(3, value);
                }
            }
            else if((tempDate.before(dates.get(8)) && tempDate.after(dates.get(9))) || tempDate.equals(dates.get(8)))
            {
//                    System.out.println("4th: Small Date: "+dates.get(3)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(4));
                if(dealerFinanceData.get(i).get(2).equals("Collection"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataDealer.get(8).get(1)) + Integer.parseInt(dealerFinanceData.get(i).get(0)));
                    stDataDealer.get(8).set(1, value);
                }
                else if(dealerFinanceData.get(i).get(2).equals("Return"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataDealer.get(8).get(2)) + Integer.parseInt(dealerFinanceData.get(i).get(0)));
                    stDataDealer.get(8).set(2, value);
                }
                else
                {
                    String value = String.valueOf(Integer.parseInt(stDataDealer.get(8).get(3)) + Integer.parseInt(dealerFinanceData.get(i).get(0)));
                    stDataDealer.get(8).set(3, value);
                }
            }
        }
//        System.out.println("   -------------------   ");
    }

    private void setCompanyBarChart()
    {
        ArrayList<ArrayList<String>> companyFinanceData;
        companyFinanceData = objDashboardFinanceInfo.getCompanyFinanceDetail(objStmt, objCon, stStartDate, stEndDate);
        for (int i = 0; i < companyFinanceData.size(); i++)
        {
            try {
                tempDate = fmt.parse(companyFinanceData.get(i).get(1));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if((tempDate.before(dates.get(0)) && tempDate.after(dates.get(1))) || tempDate.equals(dates.get(0)))
            {
//                    System.out.println("1st: Small Date: "+dates.get(0)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(1));
                if(companyFinanceData.get(i).get(2).equals("Sent"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataCompany.get(0).get(1)) + Integer.parseInt(companyFinanceData.get(i).get(0)));
                    stDataCompany.get(0).set(1, value);
                }
                else if(companyFinanceData.get(i).get(2).equals("Received"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataCompany.get(0).get(2)) + Integer.parseInt(companyFinanceData.get(i).get(0)));
                    stDataCompany.get(0).set(2, value);
                }
                else
                {
                    String value = String.valueOf(Integer.parseInt(stDataCompany.get(0).get(3)) + Integer.parseInt(companyFinanceData.get(i).get(0)));
                    stDataCompany.get(0).set(3, value);
                }
            }
            else if((tempDate.before(dates.get(1)) && tempDate.after(dates.get(2))) || tempDate.equals(dates.get(1)))
            {
//                    System.out.println("2nd: Small Date: "+dates.get(1)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(2));
                if(companyFinanceData.get(i).get(2).equals("Sent"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataCompany.get(1).get(1)) + Integer.parseInt(companyFinanceData.get(i).get(0)));
                    stDataCompany.get(1).set(1, value);
                }
                else if(companyFinanceData.get(i).get(2).equals("Received"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataCompany.get(1).get(2)) + Integer.parseInt(companyFinanceData.get(i).get(0)));
                    stDataCompany.get(1).set(2, value);
                }
                else
                {
                    String value = String.valueOf(Integer.parseInt(stDataCompany.get(1).get(3)) + Integer.parseInt(companyFinanceData.get(i).get(0)));
                    stDataCompany.get(1).set(3, value);
                }
            }
            else if((tempDate.before(dates.get(2)) && tempDate.after(dates.get(3))) || tempDate.equals(dates.get(2)))
            {
//                    System.out.println("3rd: Small Date: "+dates.get(2)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(3));
                if(companyFinanceData.get(i).get(2).equals("Sent"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataCompany.get(2).get(1)) + Integer.parseInt(companyFinanceData.get(i).get(0)));
                    stDataCompany.get(2).set(1, value);
                }
                else if(companyFinanceData.get(i).get(2).equals("Received"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataCompany.get(2).get(2)) + Integer.parseInt(companyFinanceData.get(i).get(0)));
                    stDataCompany.get(2).set(2, value);
                }
                else
                {
                    String value = String.valueOf(Integer.parseInt(stDataCompany.get(2).get(3)) + Integer.parseInt(companyFinanceData.get(i).get(0)));
                    stDataCompany.get(2).set(3, value);
                }
            }
            else if((tempDate.before(dates.get(3)) && tempDate.after(dates.get(4))) || tempDate.equals(dates.get(3)))
            {
//                    System.out.println("4th: Small Date: "+dates.get(3)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(4));
                if(companyFinanceData.get(i).get(2).equals("Sent"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataCompany.get(3).get(1)) + Integer.parseInt(companyFinanceData.get(i).get(0)));
                    stDataCompany.get(3).set(1, value);
                }
                else if(companyFinanceData.get(i).get(2).equals("Received"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataCompany.get(3).get(2)) + Integer.parseInt(companyFinanceData.get(i).get(0)));
                    stDataCompany.get(3).set(2, value);
                }
                else
                {
                    String value = String.valueOf(Integer.parseInt(stDataCompany.get(3).get(3)) + Integer.parseInt(companyFinanceData.get(i).get(0)));
                    stDataCompany.get(3).set(3, value);
                }
            }
            else if((tempDate.before(dates.get(4)) && tempDate.after(dates.get(5))) || tempDate.equals(dates.get(4)))
            {
//                    System.out.println("4th: Small Date: "+dates.get(3)+"  --  Actual Date: "+tempDate+"  --  Big Date: "+dates.get(4));
                if(companyFinanceData.get(i).get(2).equals("Sent"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataCompany.get(4).get(1)) + Integer.parseInt(companyFinanceData.get(i).get(0)));
                    stDataCompany.get(4).set(1, value);
                }
                else if(companyFinanceData.get(i).get(2).equals("Received"))
                {
                    String value = String.valueOf(Integer.parseInt(stDataCompany.get(4).get(2)) + Integer.parseInt(companyFinanceData.get(i).get(0)));
                    stDataCompany.get(4).set(2, value);
                }
                else
                {
                    String value = String.valueOf(Integer.parseInt(stDataCompany.get(4).get(3)) + Integer.parseInt(companyFinanceData.get(i).get(0)));
                    stDataCompany.get(4).set(3, value);
                }
            }

        }
    }

}
