package controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.util.StringConverter;
import model.AddCompanyFinanceInfo;
import model.AddDealerFinanceInfo;
import model.GlobalVariables;
import model.MysqlCon;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

public class AddCompanyFinance implements Initializable {

    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private JFXComboBox<String> txt_select_company;

    @FXML
    private ImageView img_user;

    @FXML
    private JFXTextField txt_name;

    @FXML
    private JFXTextField txt_contact;

    @FXML
    private JFXTextField txt_address;

    @FXML
    private JFXTextField txt_cnic;

    @FXML
    private JFXTextField txt_pending_amoount;

    @FXML
    private JFXTextField txt_amount;

    @FXML
    private JFXComboBox<String> txt_type;

    @FXML
    private JFXDatePicker txt_date;

    @FXML
    private Button txt_today_date;

    @FXML
    private Button btn_edit_update;

    @FXML
    private Button btn_edit_cancel;

    @FXML
    private Button btn_add_cash;

    @FXML
    private Button cancelBtn;

    @FXML
    private Button saveBtn;

    @FXML
    private TableView<AddCompanyFinanceInfo> table_addcompanycash;

    @FXML
    private TableColumn<AddCompanyFinanceInfo, String> sr_no;

    @FXML
    private TableColumn<AddCompanyFinanceInfo, String> company_name;

    @FXML
    private TableColumn<AddCompanyFinanceInfo, String> company_contact;

    @FXML
    private TableColumn<AddCompanyFinanceInfo, String> added_date;

    @FXML
    private TableColumn<AddDealerFinanceInfo, String> added_day;

    @FXML
    private TableColumn<AddCompanyFinanceInfo, String> amount;

    @FXML
    private TableColumn<AddCompanyFinanceInfo, String> type;

    @FXML
    private TableColumn<AddCompanyFinanceInfo, String> pending;

    @FXML
    private TableColumn<AddCompanyFinanceInfo, String> operations;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    @FXML
    private BorderPane brdrpane_navfinances;

    @FXML
    private BorderPane menu_bar;

    private AddCompanyFinanceInfo objAddCompanyFinanceInfo;
    private ArrayList<ArrayList<String>> companiesData;
    MysqlCon objMysqlCon;
    Statement objStmt;
    Connection objCon;;
    private String companyImage;
    private String selectedCompanyId;
    public static ObservableList<AddCompanyFinanceInfo> companyDetails;
    private boolean addCash;
    public static int srNo;
    public static Button btnView;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnFinance.setStyle("-fx-background-color: #47ab1e");
        objMysqlCon = new MysqlCon();
        objStmt = objMysqlCon.stmt;
        objCon = objMysqlCon.con;
        objAddCompanyFinanceInfo = new AddCompanyFinanceInfo();
        companiesData = objAddCompanyFinanceInfo.getCompaniesInfo(objStmt, objCon);
        ArrayList<String> companiesNameList = getArrayColumn(companiesData, 1);
        addCash = false;
        setComboBox(companiesNameList);

        txt_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }

            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        company_name.setCellValueFactory(new PropertyValueFactory<>("companyName"));
        company_contact.setCellValueFactory(new PropertyValueFactory<>("companyContact"));
        added_date.setCellValueFactory(new PropertyValueFactory<>("addedDate"));
        added_day.setCellValueFactory(new PropertyValueFactory<>("addedDay"));
        amount.setCellValueFactory(new PropertyValueFactory<>("amount"));
        type.setCellValueFactory(new PropertyValueFactory<>("type"));
        pending.setCellValueFactory(new PropertyValueFactory<>("pending"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_addcompanycash.setItems(parseUserList());

        AddCompanyFinanceInfo.table_addcompanycash = table_addcompanycash;
        AddCompanyFinanceInfo.txtSelectCompany = txt_select_company;
        AddCompanyFinanceInfo.txtCompanyImage = img_user;
        AddCompanyFinanceInfo.txtCompanyName = txt_name;
        AddCompanyFinanceInfo.txtCompanyContact = txt_contact;
        AddCompanyFinanceInfo.txtCompanyAddress = txt_address;
        AddCompanyFinanceInfo.txtCompanyCnic = txt_cnic;
        AddCompanyFinanceInfo.txtCompanyPendingAmount = txt_pending_amoount;
        AddCompanyFinanceInfo.txtAmount = txt_amount;
        AddCompanyFinanceInfo.txtType = txt_type;
        AddCompanyFinanceInfo.txtAddedDate = txt_date;
        AddCompanyFinanceInfo.btnAdd = btn_add_cash;
        AddCompanyFinanceInfo.btnCancel = btn_edit_cancel;
        AddCompanyFinanceInfo.btnUpdate = btn_edit_update;
    }

    public static ObservableList<AddCompanyFinanceInfo> parseUserList() {
        AddCompanyFinanceInfo objAddCompanyFinanceInfo = new AddCompanyFinanceInfo();
        companyDetails = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> companyData = objAddCompanyFinanceInfo.getAddedCash();
        for (int i = 0; i < companyData.size(); i++)
        {
            companyDetails.add(new AddCompanyFinanceInfo(String.valueOf(i+1), ((companyData.get(i).get(0) == null || companyData.get(i).get(0).equals("")) ? "N/A" : companyData.get(i).get(0)), companyData.get(i).get(1), companyData.get(i).get(2), companyData.get(i).get(3), companyData.get(i).get(4), companyData.get(i).get(5), companyData.get(i).get(6), companyData.get(i).get(7)));
        }
        return companyDetails;
    }

    @FXML
    void cancelClicked(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/company_finance.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void editCancel(ActionEvent event) {
        refreshControls();
        btn_add_cash.setVisible(true);
        btn_edit_cancel.setVisible(false);
        btn_edit_update.setVisible(false);
    }

    @FXML
    void editUpdate(ActionEvent event) {
        addCash = true;
        AddCompanyFinanceInfo.selectedCompanyArr.add(txt_select_company.getValue());
        AddCompanyFinanceInfo.companyIdArr.set(srNo, selectedCompanyId);
        AddCompanyFinanceInfo.companyNameArr.set(srNo, txt_name.getText());
        AddCompanyFinanceInfo.companyContactArr.set(srNo, txt_contact.getText());
        AddCompanyFinanceInfo.companyAddressArr.set(srNo, txt_address.getText());
        AddCompanyFinanceInfo.companyCnicArr.set(srNo, txt_cnic.getText());
        AddCompanyFinanceInfo.companyImageArr.set(srNo, companyImage);
        AddCompanyFinanceInfo.companyPendingAmountArr.set(srNo, txt_pending_amoount.getText());
        AddCompanyFinanceInfo.amountArr.set(srNo, txt_amount.getText());
        AddCompanyFinanceInfo.typeArr.set(srNo, txt_type.getValue());
        String addedDate = txt_date.getValue().toString();
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date selectedDate = null;
        try {
            selectedDate = sdf.parse(addedDate);
            addedDate = sdf2.format(selectedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        int day;
        String stDay = "";
        day = selectedDate.getDay();
        if(day == 0)
            stDay = "Sunday";
        else if(day == 1)
            stDay = "Monday";
        else if(day == 2)
            stDay = "Tuesday";
        else if(day == 3)
            stDay = "Wednesday";
        else if(day == 4)
            stDay = "Thursday";
        else if(day == 5)
            stDay = "Friday";
        else if(day == 6)
            stDay = "Saturday";
        AddCompanyFinanceInfo.addedDateArr.set(srNo, addedDate);
        AddCompanyFinanceInfo.addedDayArr.set(srNo, stDay);
        float pending;
        if(txt_type.getValue().equals("Received"))
        {
            pending = Float.parseFloat(txt_pending_amoount.getText()) + Float.parseFloat(txt_amount.getText());
        }
        else
        {
            pending = Float.parseFloat(txt_pending_amoount.getText()) - Float.parseFloat(txt_amount.getText());
        }

        AddCompanyFinanceInfo.pendingArr.set(srNo, String.valueOf(pending));
        table_addcompanycash.setItems(parseUserList());
        refreshControls();
        btn_add_cash.setVisible(true);
        btn_edit_cancel.setVisible(false);
        btn_edit_update.setVisible(false);
        addCash = false;
    }

    @FXML
    void follow(MouseEvent event) {

    }

    @FXML
    void saveClicked(ActionEvent event) throws IOException {
        try {
            objAddCompanyFinanceInfo.insertCompanyCash(objStmt, objCon);
        } catch (SQLException e) {
            e.printStackTrace();
            GlobalVariables.showNotification(-1, "Error", e.getMessage());
        }
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_company_finance.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void addCash(ActionEvent event) {
        addCash = true;
        AddCompanyFinanceInfo.selectedCompanyArr.add(txt_select_company.getValue());
        AddCompanyFinanceInfo.companyIdArr.add(selectedCompanyId);
        AddCompanyFinanceInfo.companyNameArr.add(txt_name.getText());
        AddCompanyFinanceInfo.companyContactArr.add(txt_contact.getText());
        AddCompanyFinanceInfo.companyAddressArr.add(txt_address.getText());
        AddCompanyFinanceInfo.companyCnicArr.add(txt_cnic.getText());
        AddCompanyFinanceInfo.companyImageArr.add(companyImage);
        AddCompanyFinanceInfo.companyPendingAmountArr.add(txt_pending_amoount.getText());
        AddCompanyFinanceInfo.amountArr.add(txt_amount.getText());
        AddCompanyFinanceInfo.typeArr.add(txt_type.getValue());
        String addedDate = txt_date.getValue().toString();
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date selectedDate = null;
        try {
            selectedDate = sdf.parse(addedDate);
            addedDate = sdf2.format(selectedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        int day;
        String stDay = "";
        day = selectedDate.getDay();
        if(day == 0)
            stDay = "Sunday";
        else if(day == 1)
            stDay = "Monday";
        else if(day == 2)
            stDay = "Tuesday";
        else if(day == 3)
            stDay = "Wednesday";
        else if(day == 4)
            stDay = "Thursday";
        else if(day == 5)
            stDay = "Friday";
        else if(day == 6)
            stDay = "Saturday";
        AddCompanyFinanceInfo.addedDayArr.add(stDay);
        AddCompanyFinanceInfo.addedDateArr.add(addedDate);
        float pending;
        if(txt_type.getValue().equals("Received"))
        {
            pending = Float.parseFloat(txt_pending_amoount.getText()) + Float.parseFloat(txt_amount.getText());
        }
        else
        {
            pending = Float.parseFloat(txt_pending_amoount.getText()) - Float.parseFloat(txt_amount.getText());
        }

        AddCompanyFinanceInfo.pendingArr.add(String.valueOf(pending));
        table_addcompanycash.setItems(parseUserList());
        refreshControls();
        addCash = false;
    }

    @FXML
    void setTodayDate(ActionEvent event) {
        String todayDate = GlobalVariables.getStDate();
        txt_date.setValue(LOCAL_DATE(todayDate));
    }


    @FXML
    void showList(ActionEvent event) {
        if(!addCash)
        {
            txt_select_company.show();
        }
        int index = txt_select_company.getSelectionModel().getSelectedIndex();
        if(index >= 0)
        {
            selectedCompanyId = companiesData.get(index).get(0);
            txt_name.setText( companiesData.get(index).get(2));
            txt_contact.setText( companiesData.get(index).get(3));
            txt_address.setText( companiesData.get(index).get(4));
            txt_cnic.setText( companiesData.get(index).get(5));
            if( companiesData.get(index).get(7) != null && ! companiesData.get(index).get(7).equals(""))
            {
                txt_pending_amoount.setText( companiesData.get(index).get(7));
            }
            else
            {
                txt_pending_amoount.setText("0");
            }

            if(!companiesData.get(index).get(6).equals(""))
            {
                File file = new File(companiesData.get(index).get(6));
                boolean exists = file.exists();
                if(exists)
                {
                    companyImage = companiesData.get(index).get(6);
                    Image image = new Image(file.toURI().toString());
                    img_user.setImage(image);
                }
                else
                {
                    companyImage = "src/view/images/users/empty_user.jpg";
                    file = new File("src/view/images/users/empty_user.jpg");
                    Image image = new Image(file.toURI().toString());
                    img_user.setImage(image);
                }
            }
            else
            {
                File file = new File("src/view/images/users/empty_user.jpg");
                boolean exists = file.exists();
                if(exists)
                {
                    Image image = new Image(file.toURI().toString());
                    img_user.setImage(image);
                }
                companyImage = "src/view/images/users/empty_user.jpg";
            }
        }
    }

    public static final LocalDate LOCAL_DATE (String dateString) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MMM/yyyy");
        LocalDate localDate = LocalDate.parse(dateString, formatter);
        return localDate;
    }

    public ArrayList<String> getArrayColumn(ArrayList<ArrayList<String>> companiesData, int columnIndex)
    {
        ArrayList<String> columnList = new ArrayList<>();
        for(ArrayList<String> row: companiesData) {
            columnList.add(row.get(columnIndex));
        }
        return columnList;
    }

    private void setComboBox(ArrayList<String> list)
    {
        // Create a list with some dummy values.
        ObservableList<String> items = FXCollections.observableArrayList(list);

        // Create a FilteredList wrapping the ObservableList.
        FilteredList<String> filteredItems = new FilteredList<String>(items, p -> true);

        // Add a listener to the textProperty of the combobox editor. The
        // listener will simply filter the list every time the input is changed
        // as long as the user hasn't selected an item in the list.
        txt_select_company.getEditor().textProperty().addListener((obs, oldValue, newValue) -> {
            final TextField editor = txt_select_company.getEditor();
            final String selected = txt_select_company.getSelectionModel().getSelectedItem();

            // This needs run on the GUI thread to avoid the error described
            // here: https://bugs.openjdk.java.net/browse/JDK-8081700.
            Platform.runLater(() -> {
                // If the no item in the list is selected or the selected item
                // isn't equal to the current input, we refilter the list.
                if (selected == null || !selected.equals(editor.getText())) {
                    filteredItems.setPredicate(item -> {
                        // We return true for any items that starts with the
                        // same letters as the input. We use toUpperCase to
                        // avoid case sensitivity.
                        if (item.toUpperCase().startsWith(newValue.toUpperCase())) {
                            return true;
                        } else {
                            return false;
                        }
                    });
                }
            });
        });

        txt_select_company.setItems(filteredItems);
    }

    private void refreshControls()
    {
        addCash = true;
        txt_select_company.getSelectionModel().clearSelection();
        txt_name.clear();
        txt_contact.clear();
        txt_address.clear();
        txt_cnic.clear();
        txt_pending_amoount.clear();
        txt_amount.clear();
        txt_type.getSelectionModel().clearSelection();
        txt_date.setValue(null);
    }
}
