package controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import model.CompanyFinanceReportInfo;
import model.DealerGeneralReportInfo;
import model.GlobalVariables;
import model.MysqlCon;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class CompanyFinanceReport implements Initializable {
    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private TableView<CompanyFinanceReportInfo> table_companyfinance;

    @FXML
    private TableColumn<CompanyFinanceReportInfo, String> sr_no;

    @FXML
    private TableColumn<CompanyFinanceReportInfo, String> company_id;

    @FXML
    private TableColumn<CompanyFinanceReportInfo, String> company_name;

    @FXML
    private TableColumn<CompanyFinanceReportInfo, String> company_city;

    @FXML
    private TableColumn<CompanyFinanceReportInfo, String> company_contact;

    @FXML
    private TableColumn<CompanyFinanceReportInfo, String> total_purchase;

    @FXML
    private TableColumn<CompanyFinanceReportInfo, String> cash_sent;

    @FXML
    private TableColumn<CompanyFinanceReportInfo, String> cash_return;

    @FXML
    private TableColumn<CompanyFinanceReportInfo, String> cash_discount;

    @FXML
    private TableColumn<CompanyFinanceReportInfo, String> cash_waive_off;

    @FXML
    private TableColumn<CompanyFinanceReportInfo, String> cash_pending;

    @FXML
    private TableColumn<CompanyFinanceReportInfo, String> operations;

    @FXML
    private AnchorPane filter_pane;

    @FXML
    private HBox filter_hbox;

    @FXML
    private JFXTextField txt_company_id;

    @FXML
    private JFXTextField txt_company_name;

    @FXML
    private JFXTextField txt_company_contact;

    @FXML
    private JFXComboBox<String> txt_company_status;

    @FXML
    private HBox summary_hbox1;

    @FXML
    private Label lbl_total_purchase;

    @FXML
    private Label lbl_cash_sent;

    @FXML
    private Label lbl_cash_return;

    @FXML
    private HBox summary_hbox2;

    @FXML
    private Label lbl_cash_discount;

    @FXML
    private Label lbl_cash_waiveoff;

    @FXML
    private Label lbl_cash_pending;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    @FXML
    private BorderPane brdrpane_navfinances;

    @FXML
    private BorderPane menu_bar;

    public ArrayList<CompanyFinanceReportInfo> summaryData;
    MysqlCon objMysqlCon;
    Statement objStmt;
    Connection objCon;
    static float totalPurchase = 0;
    static float cashSent = 0;
    static float cashReturn = 0;
    static float cashDiscount = 0;
    static float cashWaiveOff = 0;
    static float cashPending = 0;

    public static String txtCompanyId = "";
    public static String txtCompanyName = "";
    public static String txtCompanyContact = "";
    public static String txtCompanyStatus = "All";
    public static boolean filter;
    public static String summary = "";

    public static MenuButton btnView;
    public ObservableList<CompanyFinanceReportInfo> companysFinanceDetail;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnFinance.setStyle("-fx-background-color: #47ab1e");
        objMysqlCon = new MysqlCon();
        objStmt = objMysqlCon.stmt;
        objCon = objMysqlCon.con;

        inner_anchor.setLeftAnchor(inner_anchor, 280d);
        drawer.setSidePane(brdrpane_navfinances);
        drawer.open();

        summaryData = new ArrayList<>();
        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        company_id.setCellValueFactory(new PropertyValueFactory<>("companyId"));
        company_name.setCellValueFactory(new PropertyValueFactory<>("companyName"));
        company_city.setCellValueFactory(new PropertyValueFactory<>("companyCity"));
        company_contact.setCellValueFactory(new PropertyValueFactory<>("companyContact"));
        total_purchase.setCellValueFactory(new PropertyValueFactory<>("totalPurchase"));
        cash_sent.setCellValueFactory(new PropertyValueFactory<>("cashSent"));
        cash_return.setCellValueFactory(new PropertyValueFactory<>("cashReturn"));
        cash_discount.setCellValueFactory(new PropertyValueFactory<>("cashDiscount"));
        cash_waive_off.setCellValueFactory(new PropertyValueFactory<>("cashWaiveOff"));
        cash_pending.setCellValueFactory(new PropertyValueFactory<>("cashPending"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_companyfinance.setItems(parseUserList());
        lbl_total_purchase.setText("Purchase\n"+"Rs."+String.format("%,.0f", totalPurchase)+"/-");
        lbl_cash_sent.setText("Sent\n"+"Rs."+String.format("%,.0f", cashSent)+"/-");
        lbl_cash_return.setText("Return\n"+"Rs."+String.format("%,.0f", cashReturn)+"/-");
        lbl_cash_discount.setText("Discount\n"+"Rs."+String.format("%,.0f", cashDiscount)+"/-");
        lbl_cash_waiveoff.setText("Waive Off\n"+"Rs."+String.format("%,.0f", cashWaiveOff)+"/-");
        lbl_cash_pending.setText("Pending\n"+"Rs."+String.format("%,.0f", cashPending)+"/-");
        summary = "";
    }

    private ObservableList<CompanyFinanceReportInfo> parseUserList() {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        CompanyFinanceReportInfo objCompanyFinanceReportInfo = new CompanyFinanceReportInfo();
        companysFinanceDetail = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> companyFinanceData;
        if(filter)
        {
            companyFinanceData  = objCompanyFinanceReportInfo.getCompaniesFinanceSearch(objStmt, objCon, txtCompanyId, txtCompanyName, txtCompanyContact, txtCompanyStatus);
            txt_company_id.setText(txtCompanyId);
            txt_company_name.setText(txtCompanyName);
            txt_company_contact.setText(txtCompanyContact);
            txt_company_status.setValue(txtCompanyStatus);
            filter = false;
            filter_pane.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
            totalPurchase = 0;
            cashSent = 0;
            cashReturn = 0;
            cashDiscount = 0;
            cashWaiveOff = 0;
            cashPending = 0;
        }
        else if(summary.equals("Sale"))
        {
            companyFinanceData = objCompanyFinanceReportInfo.getCompaniesFinanceSummary(objStmt, objCon, summary);
            lbl_total_purchase.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
        }
        else if(summary.equals("Sent"))
        {
            companyFinanceData = objCompanyFinanceReportInfo.getCompaniesFinanceSummary(objStmt, objCon, summary);
            lbl_cash_sent.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
        }
        else if(summary.equals("Return"))
        {
            companyFinanceData = objCompanyFinanceReportInfo.getCompaniesFinanceSummary(objStmt, objCon, summary);
            lbl_cash_return.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
        }
        else if(summary.equals("Discount"))
        {
            companyFinanceData = objCompanyFinanceReportInfo.getCompaniesFinanceSummary(objStmt, objCon, summary);
            lbl_cash_discount.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
        }
        else if(summary.equals("Pending"))
        {
            companyFinanceData = objCompanyFinanceReportInfo.getCompaniesFinanceSummary(objStmt, objCon, summary);
            lbl_cash_pending.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
        }
        else if(summary.equals("Waive Off"))
        {
            companyFinanceData = objCompanyFinanceReportInfo.getCompaniesFinanceSummary(objStmt, objCon, summary);
            lbl_cash_waiveoff.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
        }
        else
        {
            companyFinanceData = objCompanyFinanceReportInfo.getCompanyFinanceInfo(objStmt, objCon);
            totalPurchase = 0;
            cashSent = 0;
            cashReturn = 0;
            cashDiscount = 0;
            cashWaiveOff = 0;
            cashPending = 0;
        }
        for (int i = 0; i < companyFinanceData.size(); i++)
        {
            if(summary.equals(""))
            {
                totalPurchase += (companyFinanceData.get(i).get(5) != null) ? Float.parseFloat(companyFinanceData.get(i).get(5)) : 0;
                cashSent += (companyFinanceData.get(i).get(6) != null) ? Float.parseFloat(companyFinanceData.get(i).get(6)) : 0;
                cashReturn += (companyFinanceData.get(i).get(7) != null) ? Float.parseFloat(companyFinanceData.get(i).get(7)) : 0;
                cashDiscount += (companyFinanceData.get(i).get(8) != null) ? Float.parseFloat(companyFinanceData.get(i).get(8)) : 0;
                cashWaiveOff += (companyFinanceData.get(i).get(9) != null) ? Float.parseFloat(companyFinanceData.get(i).get(9)) : 0;
                cashPending += (companyFinanceData.get(i).get(10) != null) ? Float.parseFloat(companyFinanceData.get(i).get(10)) : 0;
            }
            companysFinanceDetail.add(new CompanyFinanceReportInfo(String.valueOf(i+1), companyFinanceData.get(i).get(0), ((companyFinanceData.get(i).get(1) == null) ? "N/A" : companyFinanceData.get(i).get(1)), companyFinanceData.get(i).get(2), ((companyFinanceData.get(i).get(3) == null) ? "N/A" : companyFinanceData.get(i).get(3)), ((companyFinanceData.get(i).get(4) == null) ? "0" : companyFinanceData.get(i).get(4)), ((companyFinanceData.get(i).get(5) == null) ? "0" : companyFinanceData.get(i).get(5)), ((companyFinanceData.get(i).get(6) == null) ? "0" : companyFinanceData.get(i).get(6)), ((companyFinanceData.get(i).get(7) == null) ? "0" : companyFinanceData.get(i).get(7)), ((companyFinanceData.get(i).get(8) == null) ? "0" : companyFinanceData.get(i).get(8)), ((companyFinanceData.get(i).get(9) == null) ? "0" : companyFinanceData.get(i).get(9)), ((companyFinanceData.get(i).get(10) == null) ? "0" : companyFinanceData.get(i).get(10))));
            summaryData.add(new CompanyFinanceReportInfo(String.valueOf(i+1), companyFinanceData.get(i).get(0), ((companyFinanceData.get(i).get(1) == null) ? "N/A" : companyFinanceData.get(i).get(1)), companyFinanceData.get(i).get(2), ((companyFinanceData.get(i).get(3) == null) ? "N/A" : companyFinanceData.get(i).get(3)), ((companyFinanceData.get(i).get(4) == null) ? "0" : companyFinanceData.get(i).get(4)), ((companyFinanceData.get(i).get(5) == null) ? "0" : companyFinanceData.get(i).get(5)), ((companyFinanceData.get(i).get(6) == null) ? "0" : companyFinanceData.get(i).get(6)), ((companyFinanceData.get(i).get(7) == null) ? "0" : companyFinanceData.get(i).get(7)), ((companyFinanceData.get(i).get(8) == null) ? "0" : companyFinanceData.get(i).get(8)), ((companyFinanceData.get(i).get(9) == null) ? "0" : companyFinanceData.get(i).get(9)), ((companyFinanceData.get(i).get(10) == null) ? "0" : companyFinanceData.get(i).get(10))));
        }
        return companysFinanceDetail;
    }

    public void generateSummary(ArrayList<CompanyFinanceReportInfo> summaryList) throws JRException {
        InputStream objIO = CompanyFinanceReport.class.getResourceAsStream("/reports/CompanyFinanceReport.jrxml");
        JasperReport objReport = JasperCompileManager.compileReport(objIO);
        JasperPrint objPrint = JasperFillManager.fillReport(objReport, null, new JRBeanCollectionDataSource(summaryList));
        JasperViewer.viewReport(objPrint, false);
    }

    @FXML
    void printData(ActionEvent event) {
        try {
            generateSummary(summaryData);
        } catch (JRException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void follow(MouseEvent event) {

    }

    @FXML
    void btnReset(ActionEvent event) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/company_finance_report.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void btnSearch(ActionEvent event) {
        txtCompanyId = txt_company_id.getText();
        txtCompanyName = txt_company_name.getText();
        txtCompanyContact = txt_company_contact.getText();
        txtCompanyStatus = txt_company_status.getValue();
        if(txtCompanyStatus == null)
        {
            txtCompanyStatus = "All";
        }
        filter = true;

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/company_finance_report.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void addCash(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/add_company_finance.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void showSent(MouseEvent event) {
        summary = "Sent";

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/company_finance_report.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void showDiscount(MouseEvent event) {
        summary = "Discount";

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/company_finance_report.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void showPending(MouseEvent event) {
        summary = "Pending";

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/company_finance_report.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void showReturn(MouseEvent event) {
        summary = "Return";

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/company_finance_report.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void showSale(MouseEvent event) {
        summary = "Sale";

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/company_finance_report.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void showWaiveOff(MouseEvent event) {
        summary = "Waive Off";

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/company_finance_report.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
}
