package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.util.StringConverter;
import model.CollectionSummary1Info;
import model.GlobalVariables;
import model.MysqlCon;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;
import org.controlsfx.control.CheckTreeView;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

public class CollectionSummary1 implements Initializable {

    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private TableView<CollectionSummary1Info> table_collectionsummary;

    @FXML
    private TableColumn<CollectionSummary1Info, String> sr_no;

    @FXML
    private TableColumn<CollectionSummary1Info, String> dealer_id;

    @FXML
    private TableColumn<CollectionSummary1Info, String> order_id;

    @FXML
    private TableColumn<CollectionSummary1Info, String> dealer_name;

    @FXML
    private TableColumn<CollectionSummary1Info, String> dealer_address;

    @FXML
    private TableColumn<CollectionSummary1Info, String> dealer_contact;

    @FXML
    private TableColumn<CollectionSummary1Info, String> old_payments;

    @FXML
    private TableColumn<CollectionSummary1Info, String> new_payments;

    @FXML
    private TableColumn<CollectionSummary1Info, String> total_payments;

    @FXML
    private TableColumn<CollectionSummary1Info, String> operations;

    @FXML
    private HBox filter_pane;

    @FXML
    private CheckTreeView<String> tree_view_area;

    @FXML
    private CheckTreeView<String> tree_view_dealers;

    @FXML
    private JFXTextField txt_from_total_payments;

    @FXML
    private JFXTextField txt_to_total_payments;

    @FXML
    private JFXButton btn_reset;

    @FXML
    private JFXButton btn_search;

    @FXML
    private HBox summary_hbox;

    @FXML
    private Label lbl_old_payments;

    @FXML
    private Label lbl_new_payments;

    @FXML
    private Label lbl_total_payments;

    @FXML
    private JFXButton btn_print_summary;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    @FXML
    private BorderPane brdrpane_navpurchase;

    public ObservableList<CollectionSummary1Info> collectionSummary;
    public ArrayList<CollectionSummary1Info> summaryData;
    private float summaryOldPayments;
    private float summaryNewPayments;
    private float summaryTotalPayments;
    private CollectionSummary1Info objCollectionSummary1Info;
    private ArrayList<ArrayList<String>> areasData = new ArrayList<>();
    private ArrayList<ArrayList<String>> dealersData = new ArrayList<>();
    ArrayList<CheckBoxTreeItem> areaTree;
    ArrayList<CheckBoxTreeItem> dealerTreeRetailer;
    ArrayList<CheckBoxTreeItem> dealerTreeDoctor;
    CheckBoxTreeItem<String> areaTreeData;
    CheckBoxTreeItem<String> dealerTreeData;
    CheckBoxTreeItem<String> dealerTreeDataRetailer;
    CheckBoxTreeItem<String> dealerTreeDataDoctor;
    public static ArrayList<String> areaIds = new ArrayList<>();
    public static ArrayList<String> dealerIds = new ArrayList<>();
    public static ArrayList<String> selectedAreaNames = new ArrayList<>();
    public static ArrayList<String> selectedDealerName = new ArrayList<>();
    public static ArrayList<String> selectedDealerNameDoctor = new ArrayList<>();
    public static ArrayList<String> selectedDealerNameRetailer = new ArrayList<>();
    public static String txtFromTotalPayments = "";
    public static String txtToTotalPayments = "";
    public static boolean filter;
    public static Button btnView;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnSale.setStyle("-fx-background-color: #47ab1e");
        objCollectionSummary1Info = new CollectionSummary1Info();
        areasData = objCollectionSummary1Info.getAreasDetail(GlobalVariables.objStmt, GlobalVariables.objCon);
        dealersData = objCollectionSummary1Info.getDealersDetail(GlobalVariables.objStmt, GlobalVariables.objCon);
        ArrayList<String> areaNames = GlobalVariables.getArrayColumn(areasData, 1);
        areaTree = new ArrayList<>();
        dealerTreeRetailer = new ArrayList<>();
        dealerTreeDoctor = new ArrayList<>();
        for(String str : areaNames)
        {
            areaTree.add(new CheckBoxTreeItem<String>(str));
        }
        areaTreeData = new CheckBoxTreeItem<String>("Select Areas");
        areaTreeData.setExpanded(true);
        for(CheckBoxTreeItem val : areaTree)
        {
            areaTreeData.getChildren().add(val);
        }
        dealerTreeData = new CheckBoxTreeItem<String>("Select Dealers");
        dealerTreeDataRetailer = new CheckBoxTreeItem<String>("Retailer");
        dealerTreeDataDoctor = new CheckBoxTreeItem<String>("Doctor");

        for(int i=0; i<dealersData.size(); i++)
        {
            if(dealersData.get(i).get(2).equals("Doctor"))
            {
                dealerTreeDoctor.add(new CheckBoxTreeItem<String>(dealersData.get(i).get(1)));
            }
            else
            {
                dealerTreeRetailer.add(new CheckBoxTreeItem<String>(dealersData.get(i).get(1)));
            }
        }
        for(CheckBoxTreeItem val : dealerTreeDoctor)
        {
            dealerTreeDataDoctor.getChildren().add(val);
        }
        for(CheckBoxTreeItem val : dealerTreeRetailer)
        {
            dealerTreeDataRetailer.getChildren().add(val);
        }
        dealerTreeData.getChildren().add(dealerTreeDataDoctor);
        dealerTreeData.getChildren().add(dealerTreeDataRetailer);
        dealerTreeData.setExpanded(true);

        tree_view_area.setRoot(areaTreeData);
        tree_view_dealers.setRoot(dealerTreeData);

        summaryData = new ArrayList<>();
        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        dealer_id.setCellValueFactory(new PropertyValueFactory<>("dealerId"));
        order_id.setCellValueFactory(new PropertyValueFactory<>("orderId"));
        dealer_name.setCellValueFactory(new PropertyValueFactory<>("dealerName"));
        dealer_address.setCellValueFactory(new PropertyValueFactory<>("dealerAddress"));
        dealer_contact.setCellValueFactory(new PropertyValueFactory<>("dealerContact"));
        old_payments.setCellValueFactory(new PropertyValueFactory<>("oldPayment"));
        new_payments.setCellValueFactory(new PropertyValueFactory<>("newPayment"));
        total_payments.setCellValueFactory(new PropertyValueFactory<>("totalPayment"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_collectionsummary.setItems(parseUserList());
        lbl_old_payments.setText("Old Payments\n"+String.format("%,.0f", summaryOldPayments));
        lbl_new_payments.setText("New Payments\n"+String.format("%,.0f", summaryNewPayments));
        lbl_total_payments.setText("Total Payments\n"+String.format("%,.0f", summaryTotalPayments));
    }

    private ObservableList<CollectionSummary1Info> parseUserList() {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        collectionSummary = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> collectionSummaryData = null;

        if (filter) {
            collectionSummaryData = objCollectionSummary1Info.getCollectionSummarySearch(objStmt, objCon, areaIds, dealerIds, txtFromTotalPayments, txtToTotalPayments);

            for(int i=0; i<selectedAreaNames.size(); i++)
            {
                if(selectedAreaNames.get(i).equals("All"))
                {
                    areaTreeData.setSelected(true);
                    break;
                }
                else
                {
                    areaTree.get(Integer.parseInt(selectedAreaNames.get(i))).setSelected(true);
                }
            }

            for(int i=0; i<selectedDealerName.size(); i++)
            {
                if(selectedDealerName.get(i).equals("All"))
                {
                    dealerTreeData.setSelected(true);
                    break;
                }
                else if(selectedDealerName.get(i).equals("All Doctor"))
                {
                    dealerTreeDataDoctor.setSelected(true);
                }
                else if(selectedDealerName.get(i).equals("All Retailer"))
                {
                    dealerTreeDataRetailer.setSelected(true);
                }
            }
            for(int i=0; i<selectedDealerNameDoctor.size(); i++)
            {
                dealerTreeDoctor.get(Integer.parseInt(selectedDealerNameDoctor.get(i))).setSelected(true);
            }
            for(int i=0; i<selectedDealerNameRetailer.size(); i++)
            {
                dealerTreeRetailer.get(Integer.parseInt(selectedDealerNameRetailer.get(i))).setSelected(true);
            }
            selectedAreaNames = new ArrayList<>();
            selectedDealerName = new ArrayList<>();
            selectedDealerNameDoctor = new ArrayList<>();
            selectedDealerNameRetailer = new ArrayList<>();


            txt_from_total_payments.setText(txtFromTotalPayments);
            txt_to_total_payments.setText(txtToTotalPayments);
            filter = false;
            filter_pane.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
        }
        else
        {
            areaTreeData.setSelected(true);
            dealerTreeData.setSelected(true);
            collectionSummaryData = objCollectionSummary1Info.getCollectionSummary(objStmt, objCon);
        }

        for (int i = 0; i < collectionSummaryData.size(); i++)
        {
            if(!collectionSummaryData.get(i).get(6).equals("N/A"))
            {
                summaryOldPayments += Float.parseFloat(collectionSummaryData.get(i).get(6));
            }
            if(!collectionSummaryData.get(i).get(7).equals("N/A"))
            {
                summaryNewPayments += Float.parseFloat(collectionSummaryData.get(i).get(7));
            }
            if(!collectionSummaryData.get(i).get(8).equals("N/A"))
            {
                summaryTotalPayments += Float.parseFloat(collectionSummaryData.get(i).get(8));
            }
            collectionSummary.add(new CollectionSummary1Info(String.valueOf(i+1), collectionSummaryData.get(i).get(0), collectionSummaryData.get(i).get(1), collectionSummaryData.get(i).get(2), collectionSummaryData.get(i).get(3), collectionSummaryData.get(i).get(4), collectionSummaryData.get(i).get(5), collectionSummaryData.get(i).get(6), collectionSummaryData.get(i).get(7), collectionSummaryData.get(i).get(8)));
            summaryData.add(new CollectionSummary1Info(String.valueOf(i+1), collectionSummaryData.get(i).get(0), collectionSummaryData.get(i).get(1), collectionSummaryData.get(i).get(2), collectionSummaryData.get(i).get(3), collectionSummaryData.get(i).get(4), collectionSummaryData.get(i).get(5), collectionSummaryData.get(i).get(6), collectionSummaryData.get(i).get(7), collectionSummaryData.get(i).get(8)));
        }
        return collectionSummary;
    }

    public void generateCollectionSummary(ArrayList<CollectionSummary1Info> summaryList) throws JRException {
        InputStream objIO = CollectionSummary1.class.getResourceAsStream("/reports/CollectionSummary.jrxml");
        JasperReport objReport = JasperCompileManager.compileReport(objIO);
        JasperPrint objPrint = JasperFillManager.fillReport(objReport, null, new JRBeanCollectionDataSource(summaryList));
        JasperViewer.viewReport(objPrint, false);
    }

    @FXML
    void printCollectionSummary(ActionEvent event) {
        try {
            generateCollectionSummary(summaryData);
        } catch (JRException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void resetSummary(ActionEvent event) {
        filter = false;
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/collection_summary1.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void searchSummary(ActionEvent event) {
        areaIds = new ArrayList<>();
        dealerIds = new ArrayList<>();
        selectedAreaNames = new ArrayList<>();
        selectedDealerNameDoctor = new ArrayList<>();
        selectedDealerNameRetailer = new ArrayList<>();
        if(areaTreeData.isSelected() && !areaTreeData.isIndeterminate())
        {
            areaIds.add("All");
            selectedAreaNames.add("All");
        }
        else
        {
//            for (CheckBoxTreeItem<String> treeItem : areaTree) {
            for (int i=0; i<areaTree.size(); i++) {
                if (areaTree.get(i).isSelected()) {
                    areaIds.add(GlobalVariables.getIdFrom2D(areasData, areaTree.get(i).getValue().toString(), 1));
                    selectedAreaNames.add(String.valueOf(i));
                }
            }
        }

        if(dealerTreeData.isSelected() && !dealerTreeData.isIndeterminate())
        {
            dealerIds.add("All");
            selectedDealerName.add("All");
        }
        else
        {
            if(dealerTreeDataDoctor.isSelected())
            {
                dealerIds.add("All Doctor");
                selectedDealerName.add("All Doctor");
            }
            else
            {
                for (int i=0; i<dealerTreeDoctor.size(); i++) {
                    if (dealerTreeDoctor.get(i).isSelected()) {
                        dealerIds.add(GlobalVariables.getIdFrom2D(dealersData, dealerTreeDoctor.get(i).getValue().toString(), 1));
                        selectedDealerNameDoctor.add(String.valueOf(i));
                    }
                }
            }

            if(dealerTreeDataRetailer.isSelected())
            {
                dealerIds.add("All Retailer");
                selectedDealerName.add("All Retailer");
            }
            else
            {
                for (int i=0; i<dealerTreeRetailer.size(); i++) {
                    if (dealerTreeRetailer.get(i).isSelected()) {
                        dealerIds.add(GlobalVariables.getIdFrom2D(dealersData, dealerTreeRetailer.get(i).getValue().toString(), 1));
                        selectedDealerNameRetailer.add(String.valueOf(i));
                    }
                }
            }
        }

        txtFromTotalPayments = txt_from_total_payments.getText();
        txtToTotalPayments = txt_to_total_payments.getText();
        filter = true;
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/collection_summary1.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }


}
