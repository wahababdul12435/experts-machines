package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.GlobalVariables;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;


public class NavPurchase implements Initializable {

    @FXML
    private VBox vbox_style;

    @FXML
    private Button btn_purchase_dashboard;

    @FXML
    private Button btn_view_purchase;

    @FXML
    private Button btn_purchase_invoice;

    @FXML
    private Button btn_view_return;

    @FXML
    private Button btn_purchase_return;
    @FXML
    private Button btn_purchase_order;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        DashboardPurchase.btnView = btn_purchase_dashboard;
        ViewPurchase.btnView = btn_view_purchase;
        ViewPurchaseDetail.btnView = btn_view_purchase;
        ViewPurchaseReturn.btnView = btn_view_return;
        ViewPurchaseReturnDetail.btnView = btn_view_return;
        CreatePurchaseInvoice.btnView = btn_purchase_invoice;
        CreatePurchaseReturn.btnView = btn_purchase_return;
    }

    public void PurchInvoice() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/create_purchase_invoice.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
    public void PurchReturn() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/create_purchase_return.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
    public void PurchDashboard() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/dashboard_purchase.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
    public void viewPurchase() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_purchase.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
    public void viewReturn(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_purchase_return.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
    public void PurchOrder(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_purchase_order.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
}
