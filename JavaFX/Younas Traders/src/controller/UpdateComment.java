package controller;

import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;
import model.*;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

public class UpdateComment implements Initializable {

    @FXML
    private Label label_update;

    @FXML
    private JFXButton dialog_comment_close;

    @FXML
    private JFXButton dialog_comment_update;

    @FXML
    private TextArea txt_comment;

    public static String commentType;
    public static String orderId;
    public static String comment;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        txt_comment.setText(comment);
        dialog_comment_close.setOnAction((action)->closeDialog());
        dialog_comment_update.setOnAction((action)->updateCompany());
    }

    public void closeDialog()
    {
        if(commentType.equals("Sales"))
        {
            ViewSalesInfo.dialog.close();
            ViewSalesInfo.stackPane.setVisible(false);
        }
        else if(commentType.equals("Sales Return"))
        {
            ViewSalesReturnInfo.dialog.close();
            ViewSalesReturnInfo.stackPane.setVisible(false);
        }
        else if(commentType.equals("Purchase"))
        {
            ViewPurchaseInfo.dialog.close();
            ViewPurchaseInfo.stackPane.setVisible(false);
        }
        else if(commentType.equals("Purchase Return"))
        {
            ViewPurchaseReturnInfo.dialog.close();
            ViewPurchaseReturnInfo.stackPane.setVisible(false);
        }
        else if(commentType.equals("Dealer Finance"))
        {
            DealerFinanceDetailReportInfo.dialog.close();
            DealerFinanceDetailReportInfo.stackPane.setVisible(false);
        }
        else if(commentType.equals("Company Finance"))
        {
            CompanyFinanceDetailReportInfo.dialog.close();
            CompanyFinanceDetailReportInfo.stackPane.setVisible(false);
        }
        else if(commentType.equals("View Dealer Finance"))
        {
            ViewDealerFinanceInfo.dialog.close();
            ViewDealerFinanceInfo.stackPane.setVisible(false);
        }
        else if(commentType.equals("View company Finance"))
        {
            ViewCompanyFinanceInfo.dialog.close();
            ViewCompanyFinanceInfo.stackPane.setVisible(false);
        }
    }

    public void updateCompany() {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        String comment = txt_comment.getText();
        Parent root = null;
        if(commentType.equals("Sales"))
        {
            String currentDate = GlobalVariables.getStDate();
            String currentTime = GlobalVariables.getStTime();
            String currentUser = GlobalVariables.getUserId();
            String updateQuery = "UPDATE `order_info` SET `update_dates` = '"+currentDate+"', `update_times` = '"+currentTime+"',`update_user_id`= '"+currentUser+"', `comments` = '"+comment+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `order_id` = '"+orderId+"'";
            try {
                GlobalVariables.objStmt.executeUpdate(updateQuery);
            } catch (SQLException e) {
                e.printStackTrace();
            }

            try {
                root = FXMLLoader.load(getClass().getResource("/view/view_sales.fxml"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if(commentType.equals("Sales Return"))
        {
            String currentDate = GlobalVariables.getStDate();
            String currentTime = GlobalVariables.getStTime();
            String currentUser = GlobalVariables.getUserId();
            String updateQuery = "UPDATE `order_return` SET `update_date` = '"+currentDate+"', `update_time` = '"+currentTime+"',`update_user_id`= '"+currentUser+"', `comments` = '"+comment+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `return_id` = '"+orderId+"'";
            try {
                GlobalVariables.objStmt.executeUpdate(updateQuery);
            } catch (SQLException e) {
                e.printStackTrace();
            }

            try {
                root = FXMLLoader.load(getClass().getResource("/view/view_sales_return.fxml"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if(commentType.equals("Purchase"))
        {
            String currentDate = GlobalVariables.getStDate();
            String currentTime = GlobalVariables.getStTime();
            String currentUser = GlobalVariables.getUserId();
            String updateQuery = "UPDATE `purchase_info` SET `update_date` = '"+currentDate+"', `update_time` = '"+currentTime+"',`update_user_id`= '"+currentUser+"', `comments` = '"+comment+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `purchase_id` = '"+orderId+"'";
            try {
                GlobalVariables.objStmt.executeUpdate(updateQuery);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            root = null;
            try {
                root = FXMLLoader.load(getClass().getResource("/view/view_purchase.fxml"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if(commentType.equals("Purchase Return"))
        {
            String currentDate = GlobalVariables.getStDate();
            String currentTime = GlobalVariables.getStTime();
            String currentUser = GlobalVariables.getUserId();
            String updateQuery = "UPDATE `purchase_return` SET `update_date` = '"+currentDate+"', `update_time` = '"+currentTime+"',`user_id`= '"+currentUser+"', `comments` = '"+comment+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `returnTable_id` = '"+orderId+"'";
            try {
                GlobalVariables.objStmt.executeUpdate(updateQuery);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            root = null;
            try {
                root = FXMLLoader.load(getClass().getResource("/view/view_purchase_return.fxml"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if(commentType.equals("Dealer Finance"))
        {
            String updateQuery = "UPDATE `dealer_payments` SET `comments` = '"+comment+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `payment_id` = '"+orderId+"'";
            try {
                GlobalVariables.objStmt.executeUpdate(updateQuery);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            root = null;
            try {
                root = FXMLLoader.load(getClass().getResource("/view/dealer_finance_report.fxml"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if(commentType.equals("Company Finance"))
        {
            String updateQuery = "UPDATE `company_payments` SET `comments` = '"+comment+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `payment_id` = '"+orderId+"'";
            try {
                GlobalVariables.objStmt.executeUpdate(updateQuery);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            root = null;
            try {
                root = FXMLLoader.load(getClass().getResource("/view/company_finance_report.fxml"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if(commentType.equals("View Dealer Finance"))
        {
            String updateQuery = "UPDATE `dealer_payments` SET `comments` = '"+comment+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `payment_id` = '"+orderId+"'";
            try {
                GlobalVariables.objStmt.executeUpdate(updateQuery);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            root = null;
            try {
                root = FXMLLoader.load(getClass().getResource("/view/view_dealer_finance.fxml"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if(commentType.equals("View company Finance"))
        {
            String updateQuery = "UPDATE `company_payments` SET `comments` = '"+comment+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `payment_id` = '"+orderId+"'";
            try {
                GlobalVariables.objStmt.executeUpdate(updateQuery);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            root = null;
            try {
                root = FXMLLoader.load(getClass().getResource("/view/view_company_finance.fxml"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
}
