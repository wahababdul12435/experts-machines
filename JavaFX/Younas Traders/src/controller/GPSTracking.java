package controller;
import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.MapComponentInitializedListener;
import com.lynden.gmapsfx.javascript.object.*;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import static com.lynden.gmapsfx.javascript.object.MapTypeIdEnum.HYBRID;
import static com.lynden.gmapsfx.javascript.object.MapTypeIdEnum.ROADMAP;


public class GPSTracking implements Initializable, MapComponentInitializedListener {

    @FXML
    private HBox menu_bar;

    @FXML
    private Button button;

    @FXML
    private GoogleMapView mapView;

    private GoogleMap map;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        mapView.addMapInializedListener(this);
        GoogleMapView objGoogleMapView = new GoogleMapView();
    }

    @Override
    public void mapInitialized() {
        LatLong joeSmithLocation = new LatLong(47.6197, -122.3231);
        LatLong joshAndersonLocation = new LatLong(47.6297, -122.3431);
        LatLong bobUnderwoodLocation = new LatLong(47.6397, -122.3031);
        LatLong tomChoiceLocation = new LatLong(47.6497, -122.3325);
        LatLong fredWilkieLocation = new LatLong(47.6597, -122.3357);


        //Set the initial properties of the map.
        MapOptions mapOptions = new MapOptions();

        mapOptions.center(new LatLong(47.6097, -122.3331))
                .mapType(ROADMAP)
                .overviewMapControl(false)
                .panControl(false)
                .rotateControl(false)
                .scaleControl(false)
                .streetViewControl(false)
                .zoomControl(false)
                .zoom(12);

        map = mapView.createMap(mapOptions);

        //Add markers to the map
        MarkerOptions markerOptions1 = new MarkerOptions();
        markerOptions1.position(joeSmithLocation);

        MarkerOptions markerOptions2 = new MarkerOptions();
        markerOptions2.position(joshAndersonLocation);

        MarkerOptions markerOptions3 = new MarkerOptions();
        markerOptions3.position(bobUnderwoodLocation);

        MarkerOptions markerOptions4 = new MarkerOptions();
        markerOptions4.position(tomChoiceLocation);

        MarkerOptions markerOptions5 = new MarkerOptions();
        markerOptions5.position(fredWilkieLocation);

        Marker joeSmithMarker = new Marker(markerOptions1);
        Marker joshAndersonMarker = new Marker(markerOptions2);
        Marker bobUnderwoodMarker = new Marker(markerOptions3);
        Marker tomChoiceMarker= new Marker(markerOptions4);
        Marker fredWilkieMarker = new Marker(markerOptions5);

        map.addMarker( joeSmithMarker );
        map.addMarker( joshAndersonMarker );
        map.addMarker( bobUnderwoodMarker );
        map.addMarker( tomChoiceMarker );
        map.addMarker( fredWilkieMarker );

        InfoWindowOptions infoWindowOptions = new InfoWindowOptions();
        infoWindowOptions.content("<h2>Fred Wilkie</h2>"
                + "Current Location: Safeway<br>"
                + "ETA: 45 minutes" );

        InfoWindow fredWilkeInfoWindow = new InfoWindow(infoWindowOptions);
        fredWilkeInfoWindow.open(map, fredWilkieMarker);
    }

    public void viewHome() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/home.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    public void viewPendings() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_pending_orders.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    public void SaleInvoices() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/sale_invoice.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    public void addCompany() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/enter_company.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewCompanies() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_company.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    void addArea() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/enter_area.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewAreas() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_area.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewSubAreas() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_subarea.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    void addDealer() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/enter_dealer.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewDealers() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_dealers.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    void addSalesman() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/enter_salesman.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewSalesman() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_salesman.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    void addSupplier() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/enter_supplier.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewSuppliers() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_suppliers.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    void addProduct() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/enter_new_product.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewProducts() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_products.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewSystemAccounts() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/system_accounts.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewDrawingSummary() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/drawing_summary.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewCollectionSummary() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/collection_summary.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    @FXML
    public void viewInvoicesDetail() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/invoices_detail.fxml"));
        Stage objstage = (Stage) menu_bar.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }
}