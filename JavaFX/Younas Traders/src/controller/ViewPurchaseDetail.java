package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTimePicker;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.InputMethodEvent;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.util.StringConverter;
import model.*;
import org.controlsfx.control.textfield.TextFields;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

public class ViewPurchaseDetail implements Initializable {

    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private JFXTextField txt_invoice_no;

    @FXML
    private JFXComboBox<String> txt_company_name;

    @FXML
    private JFXComboBox<String> txt_supplier_name;

    @FXML
    private JFXDatePicker txt_purchase_date;

    @FXML
    private JFXComboBox<String> txt_invoice_status;

    @FXML
    private JFXComboBox<String> txt_entered_user;

    @FXML
    private JFXDatePicker txt_entered_date;

    @FXML
    private JFXTimePicker txt_entered_time;

    @FXML
    private JFXComboBox<String> txt_update_user;

    @FXML
    private JFXDatePicker txt_update_date;

    @FXML
    private JFXTimePicker txt_update_time;

    @FXML
    private JFXComboBox<String> txt_product_name;

    @FXML
    private JFXTextField txt_product_batch;

    @FXML
    private JFXDatePicker txt_batch_expiry;

    @FXML
    private JFXTextField txt_product_quantity;

    @FXML
    private JFXTextField txt_product_bonus;

    @FXML
    private JFXTextField txt_product_price;

    @FXML
    private JFXTextField txt_product_discount;

    @FXML
    private JFXTextField txt_product_net_price;

    @FXML
    private JFXButton btn_cancel_edit;

    @FXML
    private JFXButton btn_update_edit;

    @FXML
    private JFXButton btn_add;

    @FXML
    private HBox summary_hbox1;

    @FXML
    private Label lbl_product_quantity;

    @FXML
    private Label lbl_bonus_quantity;

    @FXML
    private Label lbl_net_price;

    @FXML
    private Label lbl_discount_given;

    @FXML
    private Label lbl_near_expiry;

    @FXML
    private TableView<ViewPurchaseDetailInfo> table_purchasedetaillog;

    @FXML
    private TableColumn<ViewPurchaseDetailInfo, String> sr_no;

    @FXML
    private TableColumn<ViewPurchaseDetailInfo, String> product_name;

    @FXML
    private TableColumn<ViewPurchaseDetailInfo, String> batch_no;

    @FXML
    private TableColumn<ViewPurchaseDetailInfo, String> batch_expiry;

    @FXML
    private TableColumn<ViewPurchaseDetailInfo, String> product_quantity;

    @FXML
    private TableColumn<ViewPurchaseDetailInfo, String> bonus_quantity;

    @FXML
    private TableColumn<ViewPurchaseDetailInfo, String> product_price;

    @FXML
    private TableColumn<ViewPurchaseDetailInfo, String> product_discount;

    @FXML
    private TableColumn<ViewPurchaseDetailInfo, String> product_net_price;

    @FXML
    private TableColumn<ViewPurchaseDetailInfo, String> operations;

    @FXML
    private JFXButton cancelBtn;

    @FXML
    private JFXButton saveBtn;

    @FXML
    private JFXButton btn_print_invoice;

    @FXML
    private StackPane stackPane;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    @FXML
    private BorderPane brdrpane_navfinances;

    @FXML
    private BorderPane menu_bar;

    private static ObservableList<ViewPurchaseDetailInfo> purchaseDetails;
    private static ViewPurchaseDetailInfo objViewPurchaseDetailInfo;
    private UserInfo objUserInfo;
    private ProductInfo objProductInfo;
    ArrayList<String> purchaseBasicData;
    ArrayList<String> userNames;
    static ArrayList<ArrayList<String>> purchaseData;
    ArrayList<ArrayList<String>> usersData;
    ArrayList<String> invoicePrintData;
    public static ArrayList<ArrayList<String>> completeProductData;
    private static String currentInvoiceId;
    private static String selectedBatch;
    //    private static String selectedBatchQty;
    private static String selectedProductPrice;
    ArrayList<String> productNames;
    ArrayList<String> productQty;
    static SimpleDateFormat fmt = new SimpleDateFormat("dd/MMM/yyyy");

    static float productsQty = 0;
    static float bonusQty = 0;
    static float netPrice = 0;
    static float discountGiven = 0;
    static float nearExpiry = 0;

    MysqlCon objMysqlCon1;
    MysqlCon objMysqlCon2;
    MysqlCon objMysqlCon3;
    static Statement objStmt1;
    Statement objStmt2;
    Statement objStmt3;
    Connection objCon;
    CompanyInfo objCompanyInfo;
    SupplierInfo objSupplierInfo;
    public ArrayList<InvoiceInfo> invoiceData;
    ArrayList<String> companyNames;
    ArrayList<ArrayList<String>> supplierData;
    ArrayList<String> supplierNames;

    public static Button btnView;
    public static int srNo;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnPurchase.setStyle("-fx-background-color: #47ab1e");
        objMysqlCon1 = new MysqlCon();
        objMysqlCon2 = new MysqlCon();
        objMysqlCon3 = new MysqlCon();
        objStmt1 = objMysqlCon1.stmt;
        objStmt2 = objMysqlCon2.stmt;
        objStmt3 = objMysqlCon3.stmt;
        objCon = objMysqlCon1.con;
        objUserInfo = new UserInfo();
        objProductInfo = new ProductInfo();
        purchaseData = new ArrayList<>();

        if(!ViewPurchaseInfo.orderId.equals(""))
        {
            currentInvoiceId = ViewPurchaseInfo.orderId;
            ViewPurchaseInfo.orderId = "";
        }

        ViewPurchaseDetailInfo.productIdArr = new ArrayList<>();
        ViewPurchaseDetailInfo.productNameArr = new ArrayList<>();
        ViewPurchaseDetailInfo.batchNoArr = new ArrayList<>();
        ViewPurchaseDetailInfo.batchExpiryArr = new ArrayList<>();
        ViewPurchaseDetailInfo.productQtyArr = new ArrayList<>();
        ViewPurchaseDetailInfo.bonusQtyArr = new ArrayList<>();
        ViewPurchaseDetailInfo.productPriceArr = new ArrayList<>();
        ViewPurchaseDetailInfo.productDiscountArr = new ArrayList<>();
        ViewPurchaseDetailInfo.productNetPriceArr = new ArrayList<>();

        objViewPurchaseDetailInfo = new ViewPurchaseDetailInfo(currentInvoiceId);
        objViewPurchaseDetailInfo.getPurchaseDetailInfo(objStmt1, objCon);

        invoiceData = new ArrayList<>();

        objCompanyInfo = new CompanyInfo();
        objSupplierInfo = new SupplierInfo();
        ArrayList<ArrayList<String>> companyData = objCompanyInfo.getCompanyNames();
        supplierData = objSupplierInfo.getSuppliersNames();
        companyNames = GlobalVariables.getArrayColumn(companyData, 1);
        supplierNames = GlobalVariables.getArrayColumn(supplierData, 1);
        txt_company_name.getItems().addAll(companyNames);
        txt_supplier_name.getItems().addAll(supplierNames);
        usersData = objUserInfo.getUserWithIds(objStmt1, objCon);
        userNames = GlobalVariables.getArrayColumn(usersData, 2);
        txt_entered_user.getItems().addAll(userNames);
        txt_update_user.getItems().addAll(userNames);

        txt_entered_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        txt_update_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        txt_purchase_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        txt_batch_expiry.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        ViewPurchaseDetailInfo.txtProductName = txt_product_name;
        ViewPurchaseDetailInfo.txtBatchNo = txt_product_batch;
        ViewPurchaseDetailInfo.txtBatchExpiry = txt_batch_expiry;
        ViewPurchaseDetailInfo.txtProductQty = txt_product_quantity;
        ViewPurchaseDetailInfo.txtProductBonus = txt_product_bonus;
        ViewPurchaseDetailInfo.txtProductPrice = txt_product_price;
        ViewPurchaseDetailInfo.txtProductDiscount = txt_product_discount;
        ViewPurchaseDetailInfo.txtProductNetPrice = txt_product_net_price;
        ViewPurchaseDetailInfo.btnAdd = btn_add;
        ViewPurchaseDetailInfo.btnCancel = btn_cancel_edit;
        ViewPurchaseDetailInfo.btnUpdate = btn_update_edit;

        purchaseBasicData = objViewPurchaseDetailInfo.getPurchaseBasicInfo(objStmt1, objCon);
        txt_invoice_no.setText(purchaseBasicData.get(0));
        txt_company_name.setValue(purchaseBasicData.get(2));
        txt_supplier_name.setValue(purchaseBasicData.get(4));
        if(purchaseBasicData.get(5) != null && !purchaseBasicData.get(5).equals(""))
        {
            txt_purchase_date.setValue(GlobalVariables.LOCAL_DATE(purchaseBasicData.get(5)));
        }

        txt_invoice_status.setValue(purchaseBasicData.get(6));
        txt_entered_user.setValue(purchaseBasicData.get(7));
        if(purchaseBasicData.get(8) != null && !purchaseBasicData.get(8).equals(""))
        {
            txt_entered_date.setValue(GlobalVariables.LOCAL_DATE(purchaseBasicData.get(8)));
        }

        SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
        Date date = null;

        if(purchaseBasicData.get(9) != null && !purchaseBasicData.get(9).equals(""))
        {
            try {
                date = parseFormat.parse(purchaseBasicData.get(9));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            txt_entered_time.setValue(LocalTime.parse(displayFormat.format(date)));
        }
        txt_update_user.setValue(purchaseBasicData.get(10));
        if(purchaseBasicData.get(11) != null && !purchaseBasicData.get(11).equals(""))
        {
            txt_update_date.setValue(GlobalVariables.LOCAL_DATE(purchaseBasicData.get(11)));
        }

        if(purchaseBasicData.get(12) != null && !purchaseBasicData.get(12).equals(""))
        {
            try {
                date = parseFormat.parse(purchaseBasicData.get(12));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            txt_update_time.setValue(LocalTime.parse(displayFormat.format(date)));
        }

        completeProductData = objProductInfo.getCompanyProductsDetail(objStmt1, objCon, purchaseBasicData.get(1));
        productNames = GlobalVariables.getArrayColumn(completeProductData, 1);
        productQty = GlobalVariables.getArrayColumn(completeProductData, 2);
        txt_product_name.getItems().clear();
        txt_product_name.getItems().addAll(productNames);

        txt_product_price.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                calculateNetPrice();
            }
        });

        txt_product_discount.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                calculateNetPrice();
            }
        });

        productsQty = 0;
        bonusQty = 0;
        netPrice = 0;
        discountGiven = 0;
        nearExpiry = 0;

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
//        product_id.setCellValueFactory(new PropertyValueFactory<>("productId"));
        product_name.setCellValueFactory(new PropertyValueFactory<>("productName"));
        batch_no.setCellValueFactory(new PropertyValueFactory<>("batchNo"));
        batch_expiry.setCellValueFactory(new PropertyValueFactory<>("batchExpiry"));
        product_quantity.setCellValueFactory(new PropertyValueFactory<>("productQty"));
        bonus_quantity.setCellValueFactory(new PropertyValueFactory<>("bonusQty"));
        product_price.setCellValueFactory(new PropertyValueFactory<>("productPrice"));
        product_discount.setCellValueFactory(new PropertyValueFactory<>("productDiscount"));
        product_net_price.setCellValueFactory(new PropertyValueFactory<>("productNetPrice"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_purchasedetaillog.setItems(parseUserList());
        ViewPurchaseDetailInfo.table_purchasedetaillog = table_purchasedetaillog;
        lbl_product_quantity.setText("Purchase Qty\n"+String.format("%,.0f", productsQty));
        lbl_bonus_quantity.setText("Bonus Qty\n"+String.format("%,.0f", bonusQty));
        lbl_net_price.setText("Net Price\n"+"Rs."+String.format("%,.0f", netPrice)+"/-");
        lbl_discount_given.setText("Discount Given\n"+"Rs."+String.format("%,.0f", discountGiven)+"/-");
        lbl_near_expiry.setText("Near Expiry / Expired\n"+String.format("%,.0f", nearExpiry));
    }

    public static ObservableList<ViewPurchaseDetailInfo> parseUserList() {

        purchaseDetails = FXCollections.observableArrayList();
        purchaseData = objViewPurchaseDetailInfo.loadPurchaseTable();

        productsQty = 0;
        bonusQty = 0;
        netPrice = 0;
        discountGiven = 0;
        nearExpiry = 0;

        for (int i = 0; i < purchaseData.size(); i++)
        {
            productsQty += (purchaseData.get(i).get(4) != null && !purchaseData.get(i).get(4).equals("")) ? Integer.parseInt(purchaseData.get(i).get(4)) : 0;
            bonusQty += (purchaseData.get(i).get(5) != null && !purchaseData.get(i).get(5).equals("")) ? Float.parseFloat(purchaseData.get(i).get(5)) : 0;
            netPrice += (purchaseData.get(i).get(8) != null && !purchaseData.get(i).get(8).equals("")) ? Float.parseFloat(purchaseData.get(i).get(8)) : 0;
            discountGiven += (purchaseData.get(i).get(7) != null && !purchaseData.get(i).get(7).equals("")) ? Float.parseFloat(purchaseData.get(i).get(7)) : 0;
            if(purchaseData.get(i).get(3) != null && !purchaseData.get(i).get(3).equals("") && !purchaseData.get(i).get(3).equals("N/A"))
            {
                String stEndDate = purchaseData.get(i).get(3);
                String stStartDate = GlobalVariables.getStDate();

                Date endDate = null;
                Date startDate = null;
                try {
                    startDate = GlobalVariables.fmt.parse(stStartDate);
                    endDate = GlobalVariables.fmt.parse(stEndDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                long diff = endDate.getTime() - startDate.getTime();
                diff = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);

                if(diff <= 180)
                {
                    nearExpiry++;
                }
            }
            purchaseDetails.add(new ViewPurchaseDetailInfo(String.valueOf(i+1), purchaseData.get(i).get(0), purchaseData.get(i).get(1), purchaseData.get(i).get(2), purchaseData.get(i).get(3), String.format("%,.0f", Float.parseFloat(purchaseData.get(i).get(4))), String.format("%,.0f", Float.parseFloat(purchaseData.get(i).get(5))), String.format("%,.2f", Float.parseFloat(purchaseData.get(i).get(6))), String.format("%,.2f", Float.parseFloat(purchaseData.get(i).get(7))), String.format("%,.2f", Float.parseFloat(purchaseData.get(i).get(8)))));
        }
        return purchaseDetails;
    }

    @FXML
    void batchChange(ActionEvent event) {

    }

    @FXML
    void btnAdd(ActionEvent event) {
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        String productName = txt_product_name.getValue();
        String productId = GlobalVariables.getIdFrom2D(completeProductData, productName, 1);
        String batchNo = txt_product_batch.getText();
        String batchExpiry = "";
        Date selectedDate = null;
        if(txt_batch_expiry.getValue() != null)
        {
            batchExpiry = txt_batch_expiry.getValue().toString();
            try {
                selectedDate = sdf.parse(batchExpiry);
                batchExpiry = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        String productQty = txt_product_quantity.getText();
        String stBonusQty = txt_product_bonus.getText();
        String productPrice = txt_product_price.getText();
        String productDiscount = txt_product_discount.getText();
        String productNetPrice = txt_product_net_price.getText();


        ViewPurchaseDetailInfo.productIdArr.add(productId);
        ViewPurchaseDetailInfo.productNameArr.add(productName);
        ViewPurchaseDetailInfo.batchNoArr.add(batchNo);
        ViewPurchaseDetailInfo.batchExpiryArr.add(batchExpiry);
        ViewPurchaseDetailInfo.productQtyArr.add(productQty);
        ViewPurchaseDetailInfo.bonusQtyArr.add(stBonusQty.equals("") ? "0" : stBonusQty);
        ViewPurchaseDetailInfo.productPriceArr.add(productPrice);
        ViewPurchaseDetailInfo.productDiscountArr.add(productDiscount.equals("") ? "0" : productDiscount);
        ViewPurchaseDetailInfo.productNetPriceArr.add(productNetPrice);
        table_purchasedetaillog.setItems(parseUserList());
        ViewPurchaseDetailInfo.table_purchasedetaillog = table_purchasedetaillog;
        lbl_product_quantity.setText("Purchase Qty\n"+String.format("%,.0f", productsQty));
        lbl_bonus_quantity.setText("Bonus Qty\n"+String.format("%,.0f", bonusQty));
        lbl_net_price.setText("Net Price\n"+String.format("%,.0f", netPrice));
        lbl_discount_given.setText("Discount Given\n"+String.format("%,.0f", discountGiven));
        lbl_near_expiry.setText("Near Expiry / Expired\n"+String.format("%,.0f", nearExpiry));
        txt_product_name.setValue("");
        txt_product_batch.setText("");
        txt_batch_expiry.setValue(null);
        txt_product_quantity.setText("");
        txt_product_bonus.setText("");
        txt_product_price.setText("");
        txt_product_discount.setText("");
        txt_product_net_price.setText("");
        btn_add.setVisible(true);
        btn_cancel_edit.setVisible(false);
        btn_update_edit.setVisible(false);
    }

    @FXML
    void btnCancelEdit(ActionEvent event) {
        txt_product_name.setValue("");
        txt_product_batch.setText("");
        txt_batch_expiry.setValue(null);
        txt_product_quantity.setText("");
        txt_product_bonus.setText("");
        txt_product_price.setText("");
        txt_product_discount.setText("");
        txt_product_net_price.setText("");
        btn_add.setVisible(true);
        btn_cancel_edit.setVisible(false);
        btn_update_edit.setVisible(false);
    }

    @FXML
    void btnUpdateEdit(ActionEvent event) {
        String productName = txt_product_name.getValue();
        String productId = GlobalVariables.getIdFrom2D(completeProductData, productName, 1);
        String batchNo = txt_product_batch.getText();
        String batchExpiry = "";
        Date selectedDate = null;
        if(txt_batch_expiry.getValue() != null)
        {
            batchExpiry = txt_batch_expiry.getValue().toString();
            try {
                selectedDate = GlobalVariables.sdf.parse(batchExpiry);
                batchExpiry = GlobalVariables.sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        String productQty = txt_product_quantity.getText();
        String stBonusQty = txt_product_bonus.getText();
        String productPrice = txt_product_price.getText();
        String productDiscount = txt_product_discount.getText();
        String productNetPrice = txt_product_net_price.getText();


        ViewPurchaseDetailInfo.productIdArr.set(srNo, productId);
        ViewPurchaseDetailInfo.productNameArr.set(srNo, productName);
        ViewPurchaseDetailInfo.batchNoArr.set(srNo, batchNo);
        ViewPurchaseDetailInfo.batchExpiryArr.set(srNo, batchExpiry);
        ViewPurchaseDetailInfo.productQtyArr.set(srNo, productQty);
        ViewPurchaseDetailInfo.bonusQtyArr.set(srNo, stBonusQty.equals("") ? "0" : stBonusQty);
        ViewPurchaseDetailInfo.productPriceArr.set(srNo, productPrice);
        ViewPurchaseDetailInfo.productDiscountArr.set(srNo, productDiscount.equals("") ? "0" : productDiscount);
        ViewPurchaseDetailInfo.productNetPriceArr.set(srNo, productNetPrice);
        table_purchasedetaillog.setItems(parseUserList());
        ViewPurchaseDetailInfo.table_purchasedetaillog = table_purchasedetaillog;
        lbl_product_quantity.setText("Purchase Qty\n"+String.format("%,.0f", productsQty));
        lbl_bonus_quantity.setText("Bonus Qty\n"+String.format("%,.0f", bonusQty));
        lbl_net_price.setText("Net Price\n"+String.format("%,.0f", netPrice));
        lbl_discount_given.setText("Discount Given\n"+String.format("%,.0f", discountGiven));
        lbl_near_expiry.setText("Near Expiry / Expired\n"+String.format("%,.0f", nearExpiry));

        txt_product_name.setValue("");
        txt_product_batch.setText("");
        txt_batch_expiry.setValue(null);
        txt_product_quantity.setText("");
        txt_product_bonus.setText("");
        txt_product_price.setText("");
        txt_product_discount.setText("");
        txt_product_net_price.setText("");
        btn_add.setVisible(true);
        btn_cancel_edit.setVisible(false);
        btn_update_edit.setVisible(false);
    }

    @FXML
    void cancelClicked(ActionEvent event) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/view_purchase.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void follow(MouseEvent event) {

    }

    @FXML
    void printInvoice(ActionEvent event) {

    }

    private void calculateNetPrice() {
        float productPrice;
        float productDiscount;
        float productNetPrice;
        if(!txt_product_price.getText().equals(""))
        {
            productPrice = Float.parseFloat(txt_product_price.getText());
        }
        else
        {
            productPrice = 0;
        }

        if(!txt_product_discount.getText().equals(""))
        {
            productDiscount = Float.parseFloat(txt_product_discount.getText());
        }
        else
        {
            productDiscount = 0;
        }

        productNetPrice = productPrice - productDiscount;
        txt_product_net_price.setText(String.valueOf(productNetPrice));
    }

    @FXML
    void saveClicked(ActionEvent event) {
        float totalProductPrice = 0;
        float totalDiscount = 0;
        float totalNetPrice = 0;
        String invoiceNum = txt_invoice_no.getText();
        String supplierId = GlobalVariables.getIdFrom2D(supplierData, txt_supplier_name.getValue(), 1);
        String purchaseDate = "";
        Date selectedDate = null;
        if(txt_purchase_date.getValue() != null)
        {
            purchaseDate = txt_purchase_date.getValue().toString();
            try {
                selectedDate = GlobalVariables.sdf.parse(purchaseDate);
                purchaseDate = GlobalVariables.sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        for(int i=0; i<ViewPurchaseDetailInfo.productPriceArr.size(); i++)
        {
            totalProductPrice += Float.parseFloat(ViewPurchaseDetailInfo.productPriceArr.get(i));
            totalDiscount += Float.parseFloat(ViewPurchaseDetailInfo.productDiscountArr.get(i));
            totalNetPrice += Float.parseFloat(ViewPurchaseDetailInfo.productNetPriceArr.get(i));
        }
        String enterUserId = GlobalVariables.getIdFrom2D(usersData, txt_entered_user.getValue(), 2);
        String enterDate = "";
        selectedDate = null;
        if(txt_entered_date.getValue() != null)
        {
            enterDate = txt_entered_date.getValue().toString();
            try {
                selectedDate = GlobalVariables.sdf.parse(enterDate);
                enterDate = GlobalVariables.sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        String enterTime = txt_entered_time.getValue().toString();
        Date _24HourDt = null;
        try {
            _24HourDt = GlobalVariables._24HourSDF.parse(enterTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        enterTime = GlobalVariables._12HourSDF.format(_24HourDt);
        String invoiceStatus = txt_invoice_status.getValue();

        objViewPurchaseDetailInfo.updateBasic(objStmt1, objCon, invoiceNum, supplierId, purchaseDate, String.valueOf(totalProductPrice), String.valueOf(totalDiscount), String.valueOf(totalNetPrice), enterUserId, enterDate, enterTime, invoiceStatus);
        objViewPurchaseDetailInfo.updateInvoice(objStmt1, objCon);

        ViewSalesInfo.orderId = currentInvoiceId;
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/view_purchase.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void showBooked(MouseEvent event) {

    }

    @FXML
    void showBookingPrice(MouseEvent event) {

    }

    @FXML
    void showDelivered(MouseEvent event) {

    }

    @FXML
    void showDiscountGiven(MouseEvent event) {

    }

    @FXML
    void showReturned(MouseEvent event) {

    }
}
