package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableSet;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import model.DrawingSummary1Info;
import model.GlobalVariables;
import model.MysqlCon;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;
import org.controlsfx.control.CheckTreeView;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

public class DrawingSummary1 implements Initializable {
    @FXML
    private Button print_DrawingSummary;

    @FXML
    private TableView<DrawingSummary1Info> table_drawingsummary;

    @FXML
    private TableColumn<DrawingSummary1Info, String> sr_no;

    @FXML
    private TableColumn<DrawingSummary1Info, String> product_no;

    @FXML
    private TableColumn<DrawingSummary1Info, String> product_name;

    @FXML
    private TableColumn<DrawingSummary1Info, String> product_batch;

    @FXML
    private TableColumn<DrawingSummary1Info, String> ordered_quantity;

    @FXML
    private TableColumn<DrawingSummary1Info, String> bonus_quantity;

    @FXML
    private TableColumn<DrawingSummary1Info, String> trade_price;

    @FXML
    private TableColumn<DrawingSummary1Info, String> total_amount;

    @FXML
    private TableColumn<DrawingSummary1Info, String> in_stock;

    @FXML
    private TableColumn<DrawingSummary1Info, String> expiry_date;

    @FXML
    private TableColumn<DrawingSummary1Info, String> days_left;

    @FXML
    private TableColumn<DrawingSummary1Info, String> deficiency;

    @FXML
    private TableColumn<DrawingSummary1Info, String> operations;

    @FXML
    private HBox summary_hbox;

    @FXML
    private HBox filter_pane;

    @FXML
    private Label lbl_quantity;

    @FXML
    private Label lbl_less_stock;

    @FXML
    private Label lbl_expired_products;

    @FXML
    private CheckTreeView<String> tree_view_area;

    @FXML
    private CheckTreeView<String> tree_view_products;

    @FXML
    private CheckTreeView<String> tree_view_dealers;

    @FXML
    private JFXDatePicker txt_from_date;

    @FXML
    private JFXDatePicker txt_to_date;

    @FXML
    private JFXComboBox<String> txt_invoice_status;

    @FXML
    private JFXTextField txt_from_invoice_no;

    @FXML
    private JFXTextField txt_to_invoice_no;

    @FXML
    private JFXButton btn_print_summary;

    @FXML
    private JFXButton btn_reset;

    @FXML
    private JFXButton btn_search;

    @FXML
    private HBox menu_bar;

    public ObservableList<DrawingSummary1Info> drawingSummary1;
    public ArrayList<DrawingSummary1Info> summaryData;
    private int summaryQuantity;
    private int summaryLessStock;
    private int summaryExpiredProducts;
    private DrawingSummary1Info objDrawingSummary1Info;
    private ArrayList<ArrayList<String>> areasData = new ArrayList<>();
    private ArrayList<ArrayList<String>> productsData = new ArrayList<>();
    private ArrayList<ArrayList<String>> dealersData = new ArrayList<>();
    ArrayList<CheckBoxTreeItem> areaTree;
    ArrayList<CheckBoxTreeItem> productTree;
    ArrayList<CheckBoxTreeItem> dealerTreeRetailer;
    ArrayList<CheckBoxTreeItem> dealerTreeDoctor;
    CheckBoxTreeItem<String> areaTreeData;
    CheckBoxTreeItem<String> productTreeData;
    CheckBoxTreeItem<String> dealerTreeData;
    CheckBoxTreeItem<String> dealerTreeDataRetailer;
    CheckBoxTreeItem<String> dealerTreeDataDoctor;
    public static ArrayList<String> areaIds = new ArrayList<>();
    public static ArrayList<String> productIds = new ArrayList<>();
    public static ArrayList<String> dealerIds = new ArrayList<>();
    public static ArrayList<String> selectedAreaNames = new ArrayList<>();
    public static ArrayList<String> selectedProductNames = new ArrayList<>();
    public static ArrayList<String> selectedDealerName = new ArrayList<>();
    public static ArrayList<String> selectedDealerNameDoctor = new ArrayList<>();
    public static ArrayList<String> selectedDealerNameRetailer = new ArrayList<>();
    public static String txtFromDate = "";
    public static String txtToDate = "";
    public static String txtInvoiceStatus = "All";
    public static String txtFromInvoiceNo = "";
    public static String txtToInvoiceNo = "";
    public static boolean filter;
    public static Button btnView;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnSale.setStyle("-fx-background-color: #47ab1e");
        objDrawingSummary1Info = new DrawingSummary1Info();
        areasData = objDrawingSummary1Info.getAreasDetail(GlobalVariables.objStmt, GlobalVariables.objCon);
        productsData = objDrawingSummary1Info.getProductsDetail(GlobalVariables.objStmt, GlobalVariables.objCon);
        dealersData = objDrawingSummary1Info.getDealersDetail(GlobalVariables.objStmt, GlobalVariables.objCon);
        ArrayList<String> areaNames = GlobalVariables.getArrayColumn(areasData, 1);
        ArrayList<String> productNames = GlobalVariables.getArrayColumn(productsData, 1);
        areaTree = new ArrayList<>();
        productTree = new ArrayList<>();
        dealerTreeRetailer = new ArrayList<>();
        dealerTreeDoctor = new ArrayList<>();
        for(String str : areaNames)
        {
            areaTree.add(new CheckBoxTreeItem<String>(str));
        }
        for(String str : productNames)
        {
            productTree.add(new CheckBoxTreeItem<String>(str));
        }
        areaTreeData = new CheckBoxTreeItem<String>("Select Areas");
        areaTreeData.setExpanded(true);
        for(CheckBoxTreeItem val : areaTree)
        {
            areaTreeData.getChildren().add(val);
        }
        productTreeData = new CheckBoxTreeItem<String>("Select Products");
        productTreeData.setExpanded(true);
        for(CheckBoxTreeItem val : productTree)
        {
            productTreeData.getChildren().add(val);
        }
        dealerTreeData = new CheckBoxTreeItem<String>("Select Dealers");
        dealerTreeDataRetailer = new CheckBoxTreeItem<String>("Retailer");
        dealerTreeDataDoctor = new CheckBoxTreeItem<String>("Doctor");

        for(int i=0; i<dealersData.size(); i++)
        {
            if(dealersData.get(i).get(2).equals("Doctor"))
            {
                dealerTreeDoctor.add(new CheckBoxTreeItem<String>(dealersData.get(i).get(1)));
            }
            else
            {
                dealerTreeRetailer.add(new CheckBoxTreeItem<String>(dealersData.get(i).get(1)));
            }
        }
        for(CheckBoxTreeItem val : dealerTreeDoctor)
        {
            dealerTreeDataDoctor.getChildren().add(val);
        }
        for(CheckBoxTreeItem val : dealerTreeRetailer)
        {
            dealerTreeDataRetailer.getChildren().add(val);
        }
        dealerTreeData.getChildren().add(dealerTreeDataDoctor);
        dealerTreeData.getChildren().add(dealerTreeDataRetailer);
        dealerTreeData.setExpanded(true);

        tree_view_area.setRoot(areaTreeData);
        tree_view_products.setRoot(productTreeData);
        tree_view_dealers.setRoot(dealerTreeData);

        txt_from_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        txt_to_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        summaryData = new ArrayList<>();
        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        product_no.setCellValueFactory(new PropertyValueFactory<>("productId"));
        product_name.setCellValueFactory(new PropertyValueFactory<>("productName"));
        product_batch.setCellValueFactory(new PropertyValueFactory<>("productBatch"));
        ordered_quantity.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        bonus_quantity.setCellValueFactory(new PropertyValueFactory<>("bonusQuantity"));
        trade_price.setCellValueFactory(new PropertyValueFactory<>("tradePrice"));
        total_amount.setCellValueFactory(new PropertyValueFactory<>("totalAmount"));
        in_stock.setCellValueFactory(new PropertyValueFactory<>("itemsInStock"));
        expiry_date.setCellValueFactory(new PropertyValueFactory<>("itemsInStock"));
        days_left.setCellValueFactory(new PropertyValueFactory<>("itemsInStock"));
        deficiency.setCellValueFactory(new PropertyValueFactory<>("deficiency"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_drawingsummary.setItems(parseUserList());
        lbl_quantity.setText("Quantity\n"+summaryQuantity);
        lbl_less_stock.setText("Less In Stock\n"+summaryLessStock);
        lbl_expired_products.setText("Expired Products\n"+summaryExpiredProducts);
    }

    private ObservableList<DrawingSummary1Info> parseUserList(){
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        drawingSummary1 = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> drawingSummary1Data = null;

        if (filter) {
            drawingSummary1Data = objDrawingSummary1Info.getDrawingSummarySearch(objStmt, objCon, areaIds, productIds, dealerIds, txtFromDate, txtToDate, txtInvoiceStatus, txtFromInvoiceNo, txtToInvoiceNo);

            for(int i=0; i<selectedAreaNames.size(); i++)
            {
                if(selectedAreaNames.get(i).equals("All"))
                {
                    areaTreeData.setSelected(true);
                    break;
                }
                else
                {
                    areaTree.get(Integer.parseInt(selectedAreaNames.get(i))).setSelected(true);
                }
            }
            for(int i=0; i<selectedProductNames.size(); i++)
            {
                if(selectedProductNames.get(i).equals("All"))
                {
                    productTreeData.setSelected(true);
                    break;
                }
                else
                {
                    productTree.get(Integer.parseInt(selectedProductNames.get(i))).setSelected(true);
                }
            }
            for(int i=0; i<selectedDealerName.size(); i++)
            {
                if(selectedDealerName.get(i).equals("All"))
                {
                    dealerTreeData.setSelected(true);
                    break;
                }
                else if(selectedDealerName.get(i).equals("All Doctor"))
                {
                    dealerTreeDataDoctor.setSelected(true);
                }
                else if(selectedDealerName.get(i).equals("All Retailer"))
                {
                    dealerTreeDataRetailer.setSelected(true);
                }
            }
            for(int i=0; i<selectedDealerNameDoctor.size(); i++)
            {
                dealerTreeDoctor.get(Integer.parseInt(selectedDealerNameDoctor.get(i))).setSelected(true);
            }
            for(int i=0; i<selectedDealerNameRetailer.size(); i++)
            {
                dealerTreeRetailer.get(Integer.parseInt(selectedDealerNameRetailer.get(i))).setSelected(true);
            }
            selectedAreaNames = new ArrayList<>();
            selectedProductNames = new ArrayList<>();
            selectedDealerName = new ArrayList<>();
            selectedDealerNameDoctor = new ArrayList<>();
            selectedDealerNameRetailer = new ArrayList<>();

            if (txtFromDate != null && !txtFromDate.equals("")) {
                txt_from_date.setValue(GlobalVariables.LOCAL_DATE(txtFromDate));
            }
            if (txtToDate != null && !txtToDate.equals("")) {
                txt_to_date.setValue(GlobalVariables.LOCAL_DATE(txtToDate));
            }
            txt_invoice_status.setValue(txtInvoiceStatus);
            txt_from_invoice_no.setText(txtFromInvoiceNo);
            txt_to_invoice_no.setText(txtToInvoiceNo);
            filter = false;
            filter_pane.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
            txtFromDate = "";
            txtToDate = "";
            txtInvoiceStatus = "All";
        }
        else
        {
            areaTreeData.setSelected(true);
            productTreeData.setSelected(true);
            dealerTreeData.setSelected(true);
            drawingSummary1Data = objDrawingSummary1Info.getDrawingSummary(objStmt, objCon);
        }

        for (int i = 0; i < drawingSummary1Data.size(); i++)
        {
            if(!drawingSummary1Data.get(i).get(4).equals("N/A"))
            {
                summaryQuantity += Integer.parseInt(drawingSummary1Data.get(i).get(4));
            }
            if(!drawingSummary1Data.get(i).get(5).equals("N/A"))
            {
                summaryQuantity += Integer.parseInt(drawingSummary1Data.get(i).get(5));
            }
            if(!drawingSummary1Data.get(i).get(11).equals("N/A"))
            {
                if(Integer.parseInt(drawingSummary1Data.get(i).get(11)) < 0)
                {
                    summaryLessStock += -1 * Integer.parseInt(drawingSummary1Data.get(i).get(11));
                }
            }
            if(!drawingSummary1Data.get(i).get(10).equals("N/A"))
            {
                if(Integer.parseInt(drawingSummary1Data.get(i).get(10)) < 0)
                {
                    summaryExpiredProducts += -1 * Integer.parseInt(drawingSummary1Data.get(i).get(10));
                }
            }
            drawingSummary1.add(new DrawingSummary1Info(String.valueOf(i+1), drawingSummary1Data.get(i).get(0), drawingSummary1Data.get(i).get(1), drawingSummary1Data.get(i).get(2), drawingSummary1Data.get(i).get(3), drawingSummary1Data.get(i).get(4), drawingSummary1Data.get(i).get(5), drawingSummary1Data.get(i).get(6), drawingSummary1Data.get(i).get(7), drawingSummary1Data.get(i).get(8), drawingSummary1Data.get(i).get(9), drawingSummary1Data.get(i).get(10), drawingSummary1Data.get(i).get(11)));
            summaryData.add(new DrawingSummary1Info(String.valueOf(i+1), drawingSummary1Data.get(i).get(0), drawingSummary1Data.get(i).get(1), drawingSummary1Data.get(i).get(2), drawingSummary1Data.get(i).get(3), drawingSummary1Data.get(i).get(4), drawingSummary1Data.get(i).get(5), drawingSummary1Data.get(i).get(6), drawingSummary1Data.get(i).get(7), drawingSummary1Data.get(i).get(8), drawingSummary1Data.get(i).get(9), drawingSummary1Data.get(i).get(10), drawingSummary1Data.get(i).get(11)));
        }
        return drawingSummary1;
    }

    public void generateDrawingSummary1(ArrayList<DrawingSummary1Info> summaryList) throws JRException {
        InputStream objIO = DrawingSummary1.class.getResourceAsStream("/reports/DrawingSummary.jrxml");
        JasperReport objReport = JasperCompileManager.compileReport(objIO);
        JasperPrint objPrint = JasperFillManager.fillReport(objReport, null, new JRBeanCollectionDataSource(summaryList));
        JasperViewer.viewReport(objPrint, false);
    }

    @FXML
    void printDrawingSummary(ActionEvent event) {
        try {
            generateDrawingSummary1(summaryData);
        } catch (JRException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void resetSummary(ActionEvent event) {
        filter = false;
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/drawing_summary1.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void searchSummary(ActionEvent event) {
        areaIds = new ArrayList<>();
        productIds = new ArrayList<>();
        dealerIds = new ArrayList<>();
        selectedAreaNames = new ArrayList<>();
        selectedProductNames = new ArrayList<>();
        selectedDealerNameDoctor = new ArrayList<>();
        selectedDealerNameRetailer = new ArrayList<>();
        if(areaTreeData.isSelected() && !areaTreeData.isIndeterminate())
        {
            areaIds.add("All");
            selectedAreaNames.add("All");
        }
        else
        {
//            for (CheckBoxTreeItem<String> treeItem : areaTree) {
            for (int i=0; i<areaTree.size(); i++) {
                if (areaTree.get(i).isSelected()) {
                    areaIds.add(GlobalVariables.getIdFrom2D(areasData, areaTree.get(i).getValue().toString(), 1));
                    selectedAreaNames.add(String.valueOf(i));
                }
            }
        }

        if(productTreeData.isSelected() && !productTreeData.isIndeterminate())
        {
            productIds.add("All");
            selectedProductNames.add("All");
        }
        else
        {
            for (int i=0; i<productTree.size(); i++) {
                if (productTree.get(i).isSelected()) {
                    productIds.add(GlobalVariables.getIdFrom2D(productsData, productTree.get(i).getValue().toString(), 1));
                    selectedProductNames.add(String.valueOf(i));
                }
            }
        }

        if(dealerTreeData.isSelected() && !dealerTreeData.isIndeterminate())
        {
            dealerIds.add("All");
            selectedDealerName.add("All");
        }
        else
        {
            if(dealerTreeDataDoctor.isSelected())
            {
                dealerIds.add("All Doctor");
                selectedDealerName.add("All Doctor");
            }
            else
            {
                for (int i=0; i<dealerTreeDoctor.size(); i++) {
                    if (dealerTreeDoctor.get(i).isSelected()) {
                        dealerIds.add(GlobalVariables.getIdFrom2D(dealersData, dealerTreeDoctor.get(i).getValue().toString(), 1));
                        selectedDealerNameDoctor.add(String.valueOf(i));
                    }
                }
            }

            if(dealerTreeDataRetailer.isSelected())
            {
                dealerIds.add("All Retailer");
                selectedDealerName.add("All Retailer");
            }
            else
            {
                for (int i=0; i<dealerTreeRetailer.size(); i++) {
                    if (dealerTreeRetailer.get(i).isSelected()) {
                        dealerIds.add(GlobalVariables.getIdFrom2D(dealersData, dealerTreeRetailer.get(i).getValue().toString(), 1));
                        selectedDealerNameRetailer.add(String.valueOf(i));
                    }
                }
            }
        }

        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date selectedDate = null;
        if(txt_from_date.getValue() != null)
        {
            txtFromDate = txt_from_date.getValue().toString();
            try {
                selectedDate = sdf.parse(txtFromDate);
                txtFromDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if(txt_to_date.getValue() != null)
        {
            txtToDate = txt_to_date.getValue().toString();
            try {
                selectedDate = sdf.parse(txtToDate);
                txtToDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        txtInvoiceStatus = txt_invoice_status.getValue();
        if(txtInvoiceStatus == null)
        {
            txtInvoiceStatus = "All";
        }
        txtFromInvoiceNo = txt_from_invoice_no.getText();
        txtToInvoiceNo = txt_to_invoice_no.getText();
        filter = true;
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/drawing_summary1.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);

    }

}
