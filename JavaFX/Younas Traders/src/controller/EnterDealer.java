package controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.util.StringConverter;
import model.DealerInfo;
import model.GlobalVariables;
import model.MysqlCon;

import javax.swing.*;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class EnterDealer implements Initializable {

    @FXML    private BorderPane menu_bar1;
    @FXML    private BorderPane nav_setup;
    @FXML    private JFXTextField txt_dealer_id;
    @FXML    private JFXTextField txt_dealer_name;
    @FXML   private JFXTextField txt_dealer_contact;
    @FXML    private JFXTextField txt_dealer_address;
    @FXML    private Button cancelBtn;
    @FXML    private Button saveBtn;
    @FXML    private JFXComboBox<String> txt_dealer_type;
    @FXML    private JFXTextField txt_creditLimit;
    @FXML    private JFXTextField txt_NTN;
    @FXML    private JFXTextField txt_CNIC;
    @FXML    private JFXComboBox<String> txt_dealer_area;
    @FXML    private JFXDatePicker expiry_9;
    @FXML    private CheckBox chkbx_lic_9;
    @FXML    private JFXTextField license_9;
    @FXML    private JFXDatePicker expiry_10;
    @FXML    private CheckBox chkbx_lic_10;
    @FXML    private JFXTextField license_10;
    @FXML    private JFXDatePicker expiry_11;
    @FXML    private CheckBox chkbx_lic_11;
    @FXML    private JFXTextField license_11;
    @FXML    private Button btn_add_dealer;
    @FXML    private Button btn_edit_cancel;
    @FXML    private Button btn_edit_update;
    @FXML    private AnchorPane inner_anchor;
    @FXML    private TableView<DealerInfo> table_add_dealer;
    @FXML    private TableColumn<DealerInfo, String> sr_no;
    @FXML    private TableColumn<DealerInfo, String> dealer_id;
    @FXML    private TableColumn<DealerInfo, String> dealer_name;
    @FXML    private TableColumn<DealerInfo, String> dealer_contact;
    @FXML    private TableColumn<DealerInfo, String> dealer_address;
    @FXML    private TableColumn<DealerInfo, String> operations;
    @FXML    private JFXDrawer drawer;
    @FXML    private Pane btnpane;
    @FXML    private Button btn11;
    @FXML    private FontAwesomeIconView FA_Icon;

    public static ObservableList<DealerInfo> dealerDetails;
    Alert alert = new Alert(Alert.AlertType.NONE);
    DealerInfo objDealerInfo;
    public static int srNo;
    public static Button btnView;

    public void initialize(URL url, ResourceBundle resourceBundle) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnSetup.setStyle("-fx-background-color: #47ab1e");
        license_9.setVisible(false);
        expiry_9.setVisible(false);
        license_10.setVisible(false);
        expiry_10.setVisible(false);
        license_11.setVisible(false);
        expiry_11.setVisible(false);
        objDealerInfo = new DealerInfo();
        btnpane.setOnMouseEntered(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(22);
                btnpane.setMinWidth(20.0);
                btn11.setMinWidth(20.0);

            }
        });
        btnpane.setOnMouseExited(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(14);
                btnpane.setMinWidth(10.0);
                btn11.setMinWidth(10.0);
            }
        });

        inner_anchor.setLeftAnchor(inner_anchor, 280d);
        drawer.setSidePane(nav_setup);
        drawer.open();

        ArrayList<String> chosenAreaNames = objDealerInfo.getSavedAreaNames();
        txt_dealer_area.getItems().addAll(chosenAreaNames);

        expiry_9.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }

            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        dealer_id.setCellValueFactory(new PropertyValueFactory<>("dealerId"));
        dealer_name.setCellValueFactory(new PropertyValueFactory<>("dealerName"));
        dealer_contact.setCellValueFactory(new PropertyValueFactory<>("dealerContact"));
        dealer_address.setCellValueFactory(new PropertyValueFactory<>("dealerAddress"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        DealerInfo.table_add_dealer = table_add_dealer;
        table_add_dealer.setItems(parseUserList());
        DealerInfo.txtDealerId = txt_dealer_id;
        DealerInfo.txtDealerName = txt_dealer_name;
        DealerInfo.txtDealerContact = txt_dealer_contact;
        DealerInfo.txtDealerAddress = txt_dealer_address;
        DealerInfo.txtDealerType = txt_dealer_type;
        DealerInfo.txtDealerCnic = txt_CNIC;
        DealerInfo.txtDealerLic9Num = license_9;
        DealerInfo.txtDealerLic9Exp = expiry_9;
        DealerInfo.txtDealerLic10Num = license_10;
        DealerInfo.txtDealerLic10Exp = expiry_10;
        DealerInfo.txtDealerLic11Num = license_11;
        DealerInfo.txtDealerLic11Exp = expiry_11;


        DealerInfo.txtDealerArea = txt_dealer_area;
        DealerInfo.btnAdd = btn_add_dealer;
        DealerInfo.btnCancel = btn_edit_cancel;
        DealerInfo.btnUpdate = btn_edit_update;
    }

    public static ObservableList<DealerInfo> parseUserList() {
        DealerInfo objDealerInfo = new DealerInfo();
        dealerDetails = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> dealerData = objDealerInfo.getAddedDealer();
        for (int i = 0; i < dealerData.size(); i++)
        {
            dealerDetails.add(new DealerInfo(String.valueOf(i+1), ((dealerData.get(i).get(0) == null || dealerData.get(i).get(0).equals("")) ? "N/A" : dealerData.get(i).get(0)), dealerData.get(i).get(1), dealerData.get(i).get(2), dealerData.get(i).get(3)));
        }
        return dealerDetails;
    }

    @FXML
    void addDealer(ActionEvent event) {
        String areaId = "";
        String dealerId = txt_dealer_id.getText();
        if(txt_dealer_area.getSelectionModel().getSelectedIndex() >= 0)
        {
            areaId = String.valueOf(objDealerInfo.getSavedAreaIds().get(txt_dealer_area.getSelectionModel().getSelectedIndex()));
        }
        String areaName = txt_dealer_area.getValue();
        String dealerName = txt_dealer_name.getText();
        String dealerContact = txt_dealer_contact.getText();
        String dealerAddress = txt_dealer_address.getText();
        String dealerType = txt_dealer_type.getValue();
        String dealerCnic = txt_CNIC.getText();
        String dealerLic9Num = license_9.getText();
        String dealerLic11Exp = "";
        String dealerLic10Exp = "";
        String dealerLic9Exp = "";
        if(expiry_9.getValue() != null) {
            dealerLic9Exp = expiry_9.getValue().toString();
        }
        String dealerLic10Num = license_10.getText();
        if(expiry_10.getValue() != null)
        {
            dealerLic10Exp = expiry_10.getValue().toString();
        }
        String dealerLic11Num = license_11.getText();
        if(expiry_11.getValue()!= null) {
             dealerLic11Exp = expiry_11.getValue().toString();
        }
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            if(expiry_9.getValue() != null) {
                dealerLic9Exp = sdf2.format(sdf.parse(dealerLic9Exp));
            }
            if(expiry_10.getValue() != null) {
                dealerLic10Exp = sdf2.format(sdf.parse(dealerLic10Exp));
            }
            if(expiry_11.getValue() != null){
                dealerLic11Exp = sdf2.format(sdf.parse(dealerLic11Exp));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String dealerStatus = "Active";

        String comp = objDealerInfo.confirmNewId(dealerId);

        if(!comp.equals(dealerId) || comp.equals("")) {
            MysqlCon objMysqlCon = new MysqlCon();
            Statement objStmt = objMysqlCon.stmt;
            Connection objCon = objMysqlCon.con;
            DealerInfo.dealerIdArr.add(dealerId);
            DealerInfo.dealerNameArr.add(dealerName);
            DealerInfo.dealerContactArr.add(dealerContact);
            DealerInfo.dealerAddressArr.add(dealerAddress);
            DealerInfo.dealerTypeArr.add(dealerType);
            DealerInfo.dealerCnicArr.add(dealerCnic);
            DealerInfo.dealerLic9NumArr.add(dealerLic9Num);
            DealerInfo.dealerLic9ExpArr.add(dealerLic9Exp);
            DealerInfo.dealerLic10NumArr.add(dealerLic10Num);
            DealerInfo.dealerLic10ExpArr.add(dealerLic10Exp);
            DealerInfo.dealerLic11NumArr.add(dealerLic11Num);
            DealerInfo.dealerLic11ExpArr.add(dealerLic11Exp);
            DealerInfo.dealerAreaIdArr.add(areaId);
            DealerInfo.dealerAreaNameArr.add(areaName);
            table_add_dealer.setItems(parseUserList());
            refreshControls();
        }
        else
        {
            alert.setAlertType(Alert.AlertType.ERROR);
            alert.setHeaderText("Error!");
            alert.setContentText("Dealer ID Already Saved.");
            alert.show();
        }
    }

    @FXML
    void editCancel(ActionEvent event) {
        refreshControls();
        btn_add_dealer.setVisible(true);
        btn_edit_cancel.setVisible(false);
        btn_edit_update.setVisible(false);
    }

    @FXML
    void editUpdate(ActionEvent event) {
        String areaId = "";
        String dealerId = txt_dealer_id.getText();
        if(txt_dealer_area.getSelectionModel().getSelectedIndex() >= 0)
        {
            areaId = String.valueOf(objDealerInfo.getSavedAreaIds().get(txt_dealer_area.getSelectionModel().getSelectedIndex()));
        }
        String areaName = txt_dealer_area.getValue();
        String dealerName = txt_dealer_name.getText();
        String dealerContact = txt_dealer_contact.getText();
        String dealerAddress = txt_dealer_address.getText();
        String dealerType = txt_dealer_type.getValue();
        String dealerCnic = txt_CNIC.getText();
        String dealerLic9Num = license_9.getText();
        String dealerLic11Exp = "";
        String dealerLic10Exp = "";
        String dealerLic9Exp = "";
        if(expiry_9.getValue() != null) {
            dealerLic9Exp = expiry_9.getValue().toString();
        }
        String dealerLic10Num = license_10.getText();
        if(expiry_10.getValue() != null)
        {
            dealerLic10Exp = expiry_10.getValue().toString();
        }
        String dealerLic11Num = license_11.getText();
        if(expiry_11.getValue() != null){
            dealerLic11Exp = expiry_11.getValue().toString();
        }
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            if(expiry_9.getValue() != null) {
                dealerLic9Exp = sdf2.format(sdf.parse(dealerLic9Exp));
            }
            if(expiry_10.getValue() != null) {
                dealerLic10Exp = sdf2.format(sdf.parse(dealerLic10Exp));
            }
            if(expiry_11.getValue() != null) {
                dealerLic11Exp = sdf2.format(sdf.parse(dealerLic11Exp));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String dealerStatus = "Active";

        String comp = objDealerInfo.confirmNewId(dealerId);

        if(!comp.equals(dealerId) || comp.equals("")) {
            MysqlCon objMysqlCon = new MysqlCon();
            Statement objStmt = objMysqlCon.stmt;
            Connection objCon = objMysqlCon.con;
            DealerInfo.dealerIdArr.set(srNo, dealerId);
            DealerInfo.dealerNameArr.set(srNo, dealerName);
            DealerInfo.dealerContactArr.set(srNo, dealerContact);
            DealerInfo.dealerAddressArr.set(srNo, dealerAddress);
            DealerInfo.dealerTypeArr.set(srNo, dealerType);
            DealerInfo.dealerCnicArr.set(srNo, dealerCnic);
            DealerInfo.dealerLic9NumArr.set(srNo, dealerLic9Num);
            DealerInfo.dealerLic9ExpArr.set(srNo, dealerLic9Exp);
            DealerInfo.dealerLic10NumArr.set(srNo, dealerLic10Num);
            DealerInfo.dealerLic10ExpArr.set(srNo, dealerLic10Exp);
            DealerInfo.dealerLic11NumArr.set(srNo, dealerLic11Num);
            DealerInfo.dealerLic11ExpArr.set(srNo, dealerLic11Exp);
            DealerInfo.dealerAreaIdArr.set(srNo, areaId);
            DealerInfo.dealerAreaNameArr.set(srNo, areaName);
            table_add_dealer.setItems(parseUserList());
            refreshControls();
        }
        else
        {
            alert.setAlertType(Alert.AlertType.ERROR);
            alert.setHeaderText("Error!");
            alert.setContentText("Dealer ID Already Saved.");
            alert.show();
        }

        btn_add_dealer.setVisible(true);
        btn_edit_cancel.setVisible(false);
        btn_edit_update.setVisible(false);
    }

    @FXML
    void chkbox9_click(ActionEvent event) throws IOException
    {
        if(chkbx_lic_9.isSelected())
        {
            license_9.setVisible(true);
            expiry_9.setVisible(true);
        }
        if(!chkbx_lic_9.isSelected())
        {
            license_9.setText("");
            expiry_9.setValue(null);
            license_9.setVisible(false);
            expiry_9.setVisible(false);
        }
    }
    @FXML
    void chkbox10_click(ActionEvent event) throws IOException
    {
        if(chkbx_lic_10.isSelected())
        {
            license_10.setVisible(true);
            expiry_10.setVisible(true);
        }
        if(!chkbx_lic_10.isSelected())
        {
            license_10.setText("");
            expiry_10.setValue(null);
            license_10.setVisible(false);
            expiry_10.setVisible(false);
        }
    }
    @FXML
    void chkbox11_click(ActionEvent event) throws IOException
    {
        if(chkbx_lic_11.isSelected())
        {
            license_11.setVisible(true);
            expiry_11.setVisible(true);
        }
        if(!chkbx_lic_11.isSelected())
        {
            license_11.setText("");
            expiry_11.setValue(null);
            license_11.setVisible(false);
            expiry_11.setVisible(false);
        }
    }

    @FXML
    void cancelClicked(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_dealers.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void follow(MouseEvent event) {
        if (drawer.isOpened()) {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(-250);
            transition.setByY(0);
            transition.setCycleCount(1);
            transition.play();
            drawer.close();
            FA_Icon.setGlyphName("ANGLE_DOUBLE_RIGHT");
            FA_Icon.setGlyphSize(14);
            //inner_anchor.setRightAnchor(inner_anchor,1d);
            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(-90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(1.1000);
            scale.setToY(1);

            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();

        }
        else {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(250);
            transition.setByY(0);
            transition.setCycleCount(1);
            FA_Icon.setGlyphName("ANGLE_DOUBLE_LEFT");
            FA_Icon.setGlyphSize(14);
            drawer.open();

            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(0.99947777);
            scale.setToY(1);

            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();
        }
    }

    @FXML
    void saveClicked(ActionEvent event) throws IOException {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        try {
            objDealerInfo.insertDealer(objStmt, objCon);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Parent root = FXMLLoader.load(getClass().getResource("/view/view_dealers.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    public void refreshControls()
    {
        this.txt_dealer_id.setText("");
        this.txt_dealer_area.getSelectionModel().clearSelection();
        this.txt_dealer_name.setText("");
        this.txt_dealer_contact.setText("");
        this.txt_dealer_address.setText("");
        this.txt_dealer_type.getSelectionModel().clearSelection();
        this.txt_CNIC.setText("");
        this.license_9.setText("");
        this.expiry_9.setValue(null);
        this.license_10.setText("");
        this.expiry_10.setValue(null);
        this.license_11.setText("");
        this.expiry_11.setValue(null);    }

}
