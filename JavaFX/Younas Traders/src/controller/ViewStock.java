package controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;
import javafx.util.StringConverter;
import model.*;
import model.ViewStockInfo;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

public class ViewStock implements Initializable {

    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private TableView<ViewStockInfo> table_viewstock;

    @FXML
    private TableColumn<ViewStockInfo, String> sr_no;

    @FXML
    private TableColumn<ViewStockInfo, String> product_name;

    @FXML
    private TableColumn<ViewStockInfo, String> batch_no;

    @FXML
    private TableColumn<ViewStockInfo, String> quantity;

    @FXML
    private TableColumn<ViewStockInfo, String> retail_price;

    @FXML
    private TableColumn<ViewStockInfo, String> trade_price;

    @FXML
    private TableColumn<ViewStockInfo, String> discount;

    @FXML
    private TableColumn<ViewStockInfo, String> exp_date;

    @FXML
    private TableColumn<ViewStockInfo, String> days_left;

    @FXML
    private TableColumn<ViewStockInfo, String> company_name;

    @FXML
    private TableColumn<ViewStockInfo, String> group_name;

    @FXML
    private TableColumn<ViewStockInfo, String> product_status;

    @FXML
    private TableColumn<ViewStockInfo, String> operations;

    @FXML
    private AnchorPane filter_pane;

    @FXML
    private HBox filter_hbox;

    @FXML
    private JFXTextField txt_product_id;

    @FXML
    private JFXTextField txt_product_name;

    @FXML
    private JFXTextField txt_batch_num;

    @FXML
    private JFXTextField txt_from_retail;

    @FXML
    private JFXTextField txt_to_retail;

    @FXML
    private JFXDatePicker txt_from_expiry;

    @FXML
    private JFXDatePicker txt_to_expiry;

    @FXML
    private JFXComboBox<String> txt_company_name;

//    @FXML
//    private JFXComboBox<String> txt_group_name;

    @FXML
    private JFXComboBox<String> txt_product_status;

    @FXML
    private JFXComboBox<String> txt_show_batch;

    @FXML
    private HBox summary_hbox;

    @FXML
    private Label lbl_total;

    @FXML
    private Label lbl_quantity;

    @FXML
    private Label lbl_less_stock;

    @FXML
    private Label lbl_expired_products;

    @FXML
    private StackPane stackPane;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private BorderPane menu_bar1;

    @FXML
    private BorderPane nav_setup;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    private ViewStockInfo objViewStockInfo;
    public ObservableList<ViewStockInfo> stockDetails;
    private int summaryTotal;
    private int summaryQuantity;
    private int summaryLessStock;
    private int summaryExpiredProducts;
    private int productCount;
    private int isStockLess;
    private ArrayList<ArrayList<String>> stockData;

    private String selectedCompanyId;
//    private String selectedGroupId;
    private ArrayList<ArrayList<String>> companiesData = new ArrayList<>();
//    private ArrayList<ArrayList<String>> groupsData = new ArrayList<>();
//    private ArrayList<String> chosenGroupIds = new ArrayList<>();

    public static String txtProductId = "";
    public static String txtProductName = "";
    public static String txtBatchNo = "";
    public static String txtFromRetail = "";
    public static String txtToRetail = "";
    public static String txtFromExpiry = "";
    public static String txtToExpiry = "";
    public static String txtCompanyId = "All";
    public static String txtCompanyName = "";
//    public static String txtGroupId = "All";
//    public static String txtGroupName = "";
    public static String txtProductStatus = "All";
    public static boolean filter;
    public static String batchShow = "Show Current Batches";
    public static Button btnView;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnLive.setStyle("-fx-background-color: #47ab1e");
        objViewStockInfo = new ViewStockInfo();

        UpdateProductInfo objUpdateProductInfo = new UpdateProductInfo();
        companiesData = objUpdateProductInfo.getCompaniesData();
//        groupsData = objUpdateProductInfo.getCompanyGroupsData();
        ArrayList<String> chosenCompanyNames = getArrayColumn(companiesData, 1);
        txt_company_name.getItems().addAll(chosenCompanyNames);

        txt_from_expiry.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        txt_to_expiry.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });



        btnpane.setOnMouseEntered(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(22);
                btnpane.setMinWidth(20.0);
                btn11.setMinWidth(20.0);

            }
        });
        btnpane.setOnMouseExited(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(14);
                btnpane.setMinWidth(10.0);
                btn11.setMinWidth(10.0);
            }
        });

        inner_anchor.setLeftAnchor(inner_anchor, 280d);
        drawer.setSidePane(nav_setup);
        drawer.open();

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        product_name.setCellValueFactory(new PropertyValueFactory<>("productName"));
        batch_no.setCellValueFactory(new PropertyValueFactory<>("batchNo"));
        quantity.setCellValueFactory(new PropertyValueFactory<>("lblQty"));
        retail_price.setCellValueFactory(new PropertyValueFactory<>("retailPrice"));
        trade_price.setCellValueFactory(new PropertyValueFactory<>("tradePrice"));
        discount.setCellValueFactory(new PropertyValueFactory<>("discount"));
        exp_date.setCellValueFactory(new PropertyValueFactory<>("lblExpiry"));
        days_left.setCellValueFactory(new PropertyValueFactory<>("daysLeft"));
        company_name.setCellValueFactory(new PropertyValueFactory<>("companyName"));
        group_name.setCellValueFactory(new PropertyValueFactory<>("groupName"));
        product_status.setCellValueFactory(new PropertyValueFactory<>("productStatus"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_viewstock.setItems(parseUserList());
        lbl_total.setText("Products\n"+summaryTotal);
        lbl_quantity.setText("Quantity\n"+summaryQuantity);
        lbl_less_stock.setText("Less In Stock\n"+summaryLessStock);
        lbl_expired_products.setText("Expired Products\n"+summaryExpiredProducts);
        txt_show_batch.setValue(batchShow);
    }

    private ObservableList<ViewStockInfo> parseUserList(){
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        ViewStockInfo objViewStockInfo = new ViewStockInfo();
        stockDetails = FXCollections.observableArrayList();

        if(filter)
        {
            stockData = objViewStockInfo.getStockInfoSearch(GlobalVariables.objStmt, GlobalVariables.objCon, batchShow, txtProductId, txtProductName, txtBatchNo, txtFromRetail, txtToRetail, txtFromExpiry, txtToExpiry, txtCompanyId, txtProductStatus);
            txt_product_id.setText(txtProductId);
            txt_product_name.setText(txtProductName);
            txt_batch_num.setText(txtBatchNo);
            txt_from_retail.setText(txtFromRetail);
            txt_to_retail.setText(txtToRetail);

            if(txtFromExpiry != null && !txtFromExpiry.equals(""))
            {
                txt_from_expiry.setValue(GlobalVariables.LOCAL_DATE(txtFromExpiry));
            }
            if(txtToExpiry != null && !txtToExpiry.equals(""))
            {
                txt_to_expiry.setValue(GlobalVariables.LOCAL_DATE(txtToExpiry));
            }

            txt_company_name.setValue(txtCompanyName);
//            txt_group_name.setValue(txtGroupId);
            txt_product_status.setValue(txtProductStatus);
//            filter = false;
            filter_pane.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
        }
        else
        {
            stockData = objViewStockInfo.getStockInfo(GlobalVariables.objStmt, GlobalVariables.objCon, batchShow);
        }


        for (int i = 0; i < stockData.size(); i++)
        {
            summaryTotal++;
            productCount = 0;
            isStockLess = 0;
            String[] qtys = stockData.get(i).get(3).split("\n");
            for (String qty:
                 qtys) {
                if(!qty.equals("N/A"))
                {
                    summaryQuantity += Integer.parseInt(qty);
                    productCount += Integer.parseInt(qty);
                }
            }
            if(stockData.get(i).get(11) != null && !stockData.get(i).get(11).equals(""))
            {
                if(productCount <= Integer.parseInt(stockData.get(i).get(11)))
                {
                    isStockLess = 1;
                    summaryLessStock++;
                }
            }
            else
            {
                isStockLess = 2;
            }
            if(!stockData.get(i).get(8).equals("N/A"))
            {
                if(Integer.parseInt(stockData.get(i).get(8)) <= 0)
                {
                    summaryExpiredProducts++;
                }
            }

            stockDetails.add(new ViewStockInfo(String.valueOf(i+1), stockData.get(i).get(0), stockData.get(i).get(1), stockData.get(i).get(2), ((stockData.get(i).get(3) == null) ? "N/A" : stockData.get(i).get(3)), ((stockData.get(i).get(4) == null) ? "N/A" : stockData.get(i).get(4)), ((stockData.get(i).get(5) == null) ? "N/A" : stockData.get(i).get(5)), stockData.get(i).get(6), stockData.get(i).get(7), stockData.get(i).get(8), stockData.get(i).get(9), stockData.get(i).get(10) , stockData.get(i).get(12), isStockLess));
        }
        return stockDetails;
    }

    @FXML
    void addCompany(ActionEvent event) {

    }

    @FXML
    void btnReset(ActionEvent event) {
        filter = false;
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/view_stock.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void btnSearch(ActionEvent event) {
        txtProductId = txt_product_id.getText();
        txtProductName = txt_product_name.getText();
        txtBatchNo = txt_batch_num.getText();
        txtFromRetail = txt_from_retail.getText();
        txtToRetail = txt_to_retail.getText();
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date selectedDate = null;
        if(txt_from_expiry.getValue() != null)
        {
            txtFromExpiry = txt_from_expiry.getValue().toString();
            try {
                selectedDate = sdf.parse(txtFromExpiry);
                txtFromExpiry = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if(txt_to_expiry.getValue() != null)
        {
            txtToExpiry = txt_to_expiry.getValue().toString();
            try {
                selectedDate = sdf.parse(txtToExpiry);
                txtToExpiry = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        txtProductStatus = txt_product_status.getValue();
        if(txtProductStatus == null)
        {
            txtProductStatus = "All";
        }
        filter = true;

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/view_stock.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void companyChange(ActionEvent event) {
        int index = txt_company_name.getSelectionModel().getSelectedIndex();
        if(index >=1)
        {
            selectedCompanyId = companiesData.get(index-1).get(0);
            txtCompanyId = selectedCompanyId;
            txtCompanyName = companiesData.get(index-1).get(1);
//            txt_group_name.getItems().clear();
//            ArrayList<String> chosenGroupNames = getCompanyGroups(groupsData, selectedCompanyId);
//            if(chosenGroupNames.size() > 0)
//            {
//                txt_group_name.getItems().addAll("All");
//                txt_group_name.getItems().addAll(chosenGroupNames);
//                txt_group_name.setValue("All");
//            }
//            txtGroupId = "All";
//            txtGroupName = "All";
//            if(chosenGroupNames.size() >= 0)
//            {
//                txt_group_name.setValue(chosenGroupNames.get(0));
//            }
//            selectedGroupId = chosenGroupIds.get(0);
//            txtGroupId = selectedGroupId;
        }
        else
        {
            txtCompanyId = "All";
            txtCompanyName = "All";
//            txtGroupId = "All";
//            txtGroupName = "All";
//            txt_group_name.getItems().clear();
//            txt_group_name.getItems().addAll("All");
//            txt_group_name.setValue("All");
        }
    }

    @FXML
    void setBatchShow(ActionEvent event) {
        batchShow = txt_show_batch.getValue();
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/view_stock.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void exportExcel(ActionEvent event) {

    }

    @FXML
    void follow(MouseEvent event) {
        if (drawer.isOpened()) {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(-250);
            transition.setByY(0);
            transition.setCycleCount(1);
            transition.play();
            drawer.close();
            FA_Icon.setGlyphName("ANGLE_DOUBLE_RIGHT");
            FA_Icon.setGlyphSize(14);
            //inner_anchor.setRightAnchor(inner_anchor,1d);

            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(-90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(1.1000);
            scale.setToY(1);

            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();
            //
            //inner_anchor.setLeftAnchor(inner_anchor, 5d);
        } else {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(250);
            transition.setByY(0);
            transition.setCycleCount(1);
            FA_Icon.setGlyphName("ANGLE_DOUBLE_LEFT");
            FA_Icon.setGlyphSize(14);
            drawer.open();

            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(0.99947777);
            scale.setToY(1);

            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();
        }
    }

//    @FXML
//    void groupChange(ActionEvent event) {
//        int index = txt_group_name.getSelectionModel().getSelectedIndex();
//        if(index >=1)
//        {
//            selectedGroupId = chosenGroupIds.get(index-1);
//            txtGroupId = selectedGroupId;
//        }
//        else
//        {
//            txtGroupId = "All";
//        }
//    }

    public ArrayList<String> getArrayColumn(ArrayList<ArrayList<String>> companiesData, int columnIndex)
    {
        ArrayList<String> columnList = new ArrayList<>();
        for(ArrayList<String> row: companiesData) {
            columnList.add(row.get(columnIndex));
        }
        return columnList;
    }

//    public ArrayList<String> getCompanyGroups(ArrayList<ArrayList<String>> groupsData, String companyId)
//    {
//        ArrayList<String> columnList = new ArrayList<>();
//        chosenGroupIds = new ArrayList<>();
//        for(ArrayList<String> row: groupsData) {
//            if(row.get(2).equals(companyId))
//            {
//                chosenGroupIds.add(row.get(0));
//                columnList.add(row.get(1));
//            }
//        }
//        return columnList;
//    }
}
