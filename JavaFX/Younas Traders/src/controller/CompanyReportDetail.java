package controller;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXDrawer;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.util.StringConverter;
import model.*;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

public class CompanyReportDetail implements Initializable {

    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private Label lbl_company_id;

    @FXML
    private Label lbl_company_name;

    @FXML
    private Label lbl_company_contact;

    @FXML
    private Label lbl_company_person;

    @FXML
    private Label lbl_company_address;

    @FXML
    private Label lbl_company_email;

    @FXML
    private Label lbl_company_created;

    @FXML
    private Label lbl_company_updated;

    @FXML
    private Label lbl_company_status;

    @FXML
    private ImageView img_user;

    @FXML
    private AnchorPane filter_pane;

    @FXML
    private JFXDatePicker txt_from_date;

    @FXML
    private JFXDatePicker txt_to_date;

    @FXML
    private JFXComboBox<String> txt_day;

    @FXML
    private HBox summary_hbox1;

    @FXML
    private Label lbl_invoices_purchased;

    @FXML
    private Label lbl_received_items;

    @FXML
    private Label lbl_total_purchase;

    @FXML
    private Label lbl_discount_given;

    @FXML
    private Label lbl_returned_items;

    @FXML
    private HBox summary_hbox2;

    @FXML
    private Label lbl_return_price;

    @FXML
    private Label lbl_cash_sent;

    @FXML
    private Label lbl_cash_return;

    @FXML
    private Label lbl_cash_waiveoff;

    @FXML
    private Label lbl_cash_pending;

    @FXML
    private TableView<CompanyReportDetailInfo> table_companyreportlog;

    @FXML
    private TableColumn<CompanyReportDetailInfo, String> sr_no;

    @FXML
    private TableColumn<CompanyReportDetailInfo, String> date;

    @FXML
    private TableColumn<CompanyReportDetailInfo, String> day;

    @FXML
    private TableColumn<CompanyReportDetailInfo, String> purchase_invoice;

    @FXML
    private TableColumn<CompanyReportDetailInfo, String> received_items;

    @FXML
    private TableColumn<CompanyReportDetailInfo, String> purchase_price;

    @FXML
    private TableColumn<CompanyReportDetailInfo, String> discount_given;

    @FXML
    private TableColumn<CompanyReportDetailInfo, String> quantity_returned;

    @FXML
    private TableColumn<CompanyReportDetailInfo, String> return_price;

    @FXML
    private TableColumn<CompanyReportDetailInfo, String> cash_collection;

    @FXML
    private TableColumn<CompanyReportDetailInfo, String> cash_return;

    @FXML
    private TableColumn<CompanyReportDetailInfo, String> cash_waive_off;

    @FXML
    private TableColumn<CompanyReportDetailInfo, String> cash_pending;

//    @FXML
//    private TableColumn<CompanyReportDetailInfo, String> operations;

    @FXML
    private StackPane stackPane;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    @FXML
    private BorderPane brdrpane_navfinances;

    @FXML
    private BorderPane menu_bar;

    private CompanyInfo objCompanyInfo;
    private String currentCompanyId;
    private String selectedImagePath;
    private ArrayList<String> companyInfo;
    private ObservableList<CompanyReportDetailInfo> dealerDetails;
    private CompanyReportDetailInfo objCompanyReportDetailInfo;
    ArrayList<ArrayList<String>> companyReportData;
    float purchasedInvoices = 0;
    float receivedItems = 0;
    float totalPurchase = 0;
    float discountGiven = 0;
    float itemsReturned = 0;
    float returnPrice = 0;
    float cashSent = 0;
    float cashReturn = 0;
    float cashWaiveOff = 0;
    float cashPending = 0;

    public static String txtFromDate = "";
    public static String txtToDate = "";
    public static String txtDay = "All";
    public static boolean filter;

    MysqlCon objMysqlCon1;
    MysqlCon objMysqlCon2;
    MysqlCon objMysqlCon3;
    Statement objStmt1;
    Statement objStmt2;
    Statement objStmt3;
    Connection objCon;

    public static MenuButton btnView;
    SimpleDateFormat fmt = new SimpleDateFormat("dd/MMM/yyyy");

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnReport.setStyle("-fx-background-color: #47ab1e");
        objMysqlCon1 = new MysqlCon();
        objMysqlCon2 = new MysqlCon();
        objMysqlCon3 = new MysqlCon();
        objStmt1 = objMysqlCon1.stmt;
        objStmt2 = objMysqlCon2.stmt;
        objStmt3 = objMysqlCon3.stmt;
        objCon = objMysqlCon1.con;

        objCompanyInfo = new CompanyInfo();
        if(!CompanyGeneralReportInfo.companyReportId.equals(""))
        {
            currentCompanyId = CompanyGeneralReportInfo.companyReportId;
            CompanyGeneralReportInfo.companyReportId = "";
        }
        companyInfo = objCompanyInfo.getCompanyDetail(currentCompanyId);
        lbl_company_id.setText(((companyInfo.get(0) == null) ? "N/A" : companyInfo.get(0)));
        lbl_company_name.setText(companyInfo.get(1));
        lbl_company_contact.setText(companyInfo.get(2));
        lbl_company_address.setText(companyInfo.get(3));
        lbl_company_person.setText((companyInfo.get(4) == null) ? "N/A" : companyInfo.get(4));
        lbl_company_email.setText(companyInfo.get(5));
        lbl_company_created.setText(companyInfo.get(6));
        lbl_company_updated.setText((companyInfo.get(7) == null) ? "N/A" : companyInfo.get(7));
        lbl_company_status.setText(companyInfo.get(8));
        if(companyInfo.get(9) != null && !companyInfo.get(9).equals(""))
        {
            selectedImagePath = companyInfo.get(9);
            File file = new File(selectedImagePath);
            boolean exists = file.exists();
            if(exists)
            {
                Image image = new Image(file.toURI().toString());
                img_user.setImage(image);
            }
        }

        objCompanyReportDetailInfo = new CompanyReportDetailInfo();
        companyReportData = new ArrayList<>();

        txt_from_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        txt_to_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        date.setCellValueFactory(new PropertyValueFactory<>("date"));
        day.setCellValueFactory(new PropertyValueFactory<>("day"));
        purchase_invoice.setCellValueFactory(new PropertyValueFactory<>("purchaseInvoice"));
        received_items.setCellValueFactory(new PropertyValueFactory<>("receivedItems"));
        purchase_price.setCellValueFactory(new PropertyValueFactory<>("purchasePrice"));
        discount_given.setCellValueFactory(new PropertyValueFactory<>("discountGiven"));
        quantity_returned.setCellValueFactory(new PropertyValueFactory<>("quantityReturned"));
        return_price.setCellValueFactory(new PropertyValueFactory<>("returnPrice"));
        cash_collection.setCellValueFactory(new PropertyValueFactory<>("cashCollection"));
        cash_return.setCellValueFactory(new PropertyValueFactory<>("cashReturn"));
        cash_waive_off.setCellValueFactory(new PropertyValueFactory<>("cashWaiveOff"));
        cash_pending.setCellValueFactory(new PropertyValueFactory<>("cashPending"));
//        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_companyreportlog.setItems(parseUserList());
        lbl_invoices_purchased.setText("Ordered Items\n"+String.format("%,.0f", purchasedInvoices));
        lbl_received_items.setText("Received Items\n"+String.format("%,.0f", receivedItems));
        lbl_total_purchase.setText("Total Purchase\n"+String.format("%,.0f", totalPurchase));
        lbl_discount_given.setText("Discount Given\n"+String.format("%,.0f", discountGiven));
        lbl_returned_items.setText("Items Returned\n"+String.format("%,.0f", itemsReturned));
        lbl_return_price.setText("Return Price\n"+String.format("%,.0f", returnPrice));
        lbl_cash_sent.setText("Cash Sent\n"+String.format("%,.0f", cashSent));
        lbl_cash_return.setText("Cash Returned\n"+String.format("%,.0f", cashReturn));
        lbl_cash_waiveoff.setText("Cash Waived Off\n"+String.format("%,.0f", cashWaiveOff));
        lbl_cash_pending.setText("Cash Pending\n"+String.format("%,.0f", cashPending));
    }

    private ObservableList<CompanyReportDetailInfo> parseUserList() {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        dealerDetails = FXCollections.observableArrayList();

        if (filter) {
            try {
                companyReportData = objCompanyReportDetailInfo.getCompanyReportDetailSearch(objStmt1, objStmt2, objStmt3, objCon, currentCompanyId, txtFromDate, txtToDate, txtDay);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (txtFromDate != null && !txtFromDate.equals("")) {
                txt_from_date.setValue(GlobalVariables.LOCAL_DATE(txtFromDate));
            }
            if (txtToDate != null && !txtToDate.equals("")) {
                txt_to_date.setValue(GlobalVariables.LOCAL_DATE(txtToDate));
            }
            txt_day.setValue(txtDay);
            filter = false;
            filter_pane.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
            txtFromDate = "";
            txtToDate = "";
            txtDay = "All";
        } else {
            try {
                companyReportData = objCompanyReportDetailInfo.getCompanyReportDetail(objStmt1, objStmt2, objStmt3, objCon, currentCompanyId);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }


        for (int i = 0; i < companyReportData.size(); i++)
        {
            if(!companyReportData.get(i).get(2).equals("-"))
            {
                String[] ordersPurchase = companyReportData.get(i).get(2).split("\n");
                purchasedInvoices += ordersPurchase.length;
            }
            receivedItems += (companyReportData.get(i).get(3) != null  && !companyReportData.get(i).get(3).equals("-")) ? Float.parseFloat(companyReportData.get(i).get(3)) : 0;
            totalPurchase += (companyReportData.get(i).get(4) != null && !companyReportData.get(i).get(4).equals("-")) ? Float.parseFloat(companyReportData.get(i).get(4)) : 0;
            discountGiven += (companyReportData.get(i).get(5) != null && !companyReportData.get(i).get(5).equals("-")) ? Float.parseFloat(companyReportData.get(i).get(5)) : 0;
            itemsReturned += (companyReportData.get(i).get(6) != null && !companyReportData.get(i).get(6).equals("-")) ? Float.parseFloat(companyReportData.get(i).get(6)) : 0;
            returnPrice += (companyReportData.get(i).get(7) != null && !companyReportData.get(i).get(7).equals("-")) ? Float.parseFloat(companyReportData.get(i).get(7)) : 0;
            cashSent += (companyReportData.get(i).get(8) != null && !companyReportData.get(i).get(8).equals("-")) ? Float.parseFloat(companyReportData.get(i).get(8)) : 0;
            cashReturn += (companyReportData.get(i).get(9) != null && !companyReportData.get(i).get(9).equals("-")) ? Float.parseFloat(companyReportData.get(i).get(9)) : 0;
            cashWaiveOff += (companyReportData.get(i).get(10) != null && !companyReportData.get(i).get(10).equals("-")) ? Float.parseFloat(companyReportData.get(i).get(10)) : 0;
            cashPending += (companyReportData.get(i).get(11) != null && !companyReportData.get(i).get(11).equals("-")) ? Float.parseFloat(companyReportData.get(i).get(11)) : 0;
            dealerDetails.add(new CompanyReportDetailInfo(String.valueOf(i+1), ((companyReportData.get(i).get(0) == null || companyReportData.get(i).get(0).equals("")) ? "N/A" : companyReportData.get(i).get(0)), companyReportData.get(i).get(1), companyReportData.get(i).get(2), companyReportData.get(i).get(3), companyReportData.get(i).get(4), companyReportData.get(i).get(5), companyReportData.get(i).get(6), companyReportData.get(i).get(7), companyReportData.get(i).get(8), companyReportData.get(i).get(9), companyReportData.get(i).get(10), companyReportData.get(i).get(11)));
        }
        return dealerDetails;
    }

    @FXML
    void btnReset(ActionEvent event) {
        CompanyGeneralReportInfo.companyReportId = currentCompanyId;
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/company_report_detail.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void btnSearch(ActionEvent event) {
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date selectedDate = null;
        if(txt_from_date.getValue() != null)
        {
            txtFromDate = txt_from_date.getValue().toString();
            try {
                selectedDate = sdf.parse(txtFromDate);
                txtFromDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if(txt_to_date.getValue() != null)
        {
            txtToDate = txt_to_date.getValue().toString();
            try {
                selectedDate = sdf.parse(txtToDate);
                txtToDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        txtDay = txt_day.getValue();
        if(txtDay == null)
        {
            txtDay = "All";
        }
        filter = true;

        CompanyGeneralReportInfo.companyReportId = currentCompanyId;
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/company_report_detail.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void follow(MouseEvent event) {

    }

    @FXML
    void showCashReturn(MouseEvent event) {

    }

    @FXML
    void showCashSent(MouseEvent event) {

    }

    @FXML
    void showInvoicesPurchased(MouseEvent event) {

    }

    @FXML
    void showPending(MouseEvent event) {

    }

    @FXML
    void showPurchase(MouseEvent event) {

    }

    @FXML
    void showReceivedItems(MouseEvent event) {

    }

    @FXML
    void showReturnPrice(MouseEvent event) {

    }

    @FXML
    void showReturnedItems(MouseEvent event) {

    }

    @FXML
    void showWaiveOff(MouseEvent event) {

    }
}
