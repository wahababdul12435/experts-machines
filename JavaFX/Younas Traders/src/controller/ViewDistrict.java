package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.util.Duration;
import model.*;
import model.ViewDistrictInfo;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class ViewDistrict implements Initializable {
    @FXML
    private TableView<ViewDistrictInfo> table_viewdistrict;

    @FXML
    private TableColumn<ViewDistrictInfo, String> sr_no;

    @FXML
    private TableColumn<ViewDistrictInfo, String> district_id;

    @FXML
    private TableColumn<ViewDistrictInfo, String> district_name;

    @FXML
    private TableColumn<ViewDistrictInfo, String> cities;

    @FXML
    private TableColumn<ViewDistrictInfo, String> district_status;

    @FXML
    private TableColumn<ViewDistrictInfo, String> operations;

    @FXML
    private StackPane stackPane;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private BorderPane menu_bar1;

    @FXML
    private BorderPane nav_setup;

    @FXML    private MenuBar menu_bar;

    @FXML    private AnchorPane inner_anchor;
    @FXML    private Pane btnpane;
    @FXML    private Button btn11;
    @FXML    private FontAwesomeIconView FA_Icon;

    @FXML
    private AnchorPane filter_pane;

    @FXML
    private HBox filter_hbox;

    @FXML
    private JFXTextField txt_district_id;

    @FXML
    private JFXTextField txt_district_name;

    @FXML
    private JFXComboBox<String> txt_district_status;

    @FXML
    private HBox summary_hbox;

    @FXML
    private Label lbl_total;

    @FXML
    private Label lbl_active;

    @FXML
    private Label lbl_inactive;

    @FXML
    private JFXButton btn_add;
    @FXML
    private JFXButton btn_export;



    public ObservableList<ViewDistrictInfo> districtsDetail;
    PendingOrders objPendingOrders;
    MysqlCon objMysqlCon;
    Statement objStmt;
    Connection objCon;
    private int summaryTotal;
    private int summaryActive;
    private int summaryInActive;

    public static String txtDistrictId = "";
    public static String txtDistrictName = "";
    public static String txtDistrictStatus = "";
    public static boolean filter;

    public static Button btnView;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnSetup.setStyle("-fx-background-color: #47ab1e");
        objMysqlCon = new MysqlCon();
        objStmt = objMysqlCon.stmt;
        objCon = objMysqlCon.con;
        objPendingOrders = new PendingOrders();
        ViewDistrictInfo.stackPane = stackPane;

        if(!GlobalVariables.userType.equals("Admin"))
        {
            if(!GlobalVariables.rightCreate)
            {
                btn_add.setVisible(false);
                AnchorPane.setRightAnchor(btn_export, 15.0);
            }
        }

        btnpane.setOnMouseEntered(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(22);
                btnpane.setMinWidth(20.0);
                btn11.setMinWidth(20.0);

            }
        });
        btnpane.setOnMouseExited(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                FA_Icon.setGlyphSize(14);
                btnpane.setMinWidth(10.0);
                btn11.setMinWidth(10.0);
            }
        });

        inner_anchor.setLeftAnchor(inner_anchor, 280d);
        drawer.setSidePane(nav_setup);
        drawer.open();

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        district_id.setCellValueFactory(new PropertyValueFactory<>("districtId"));
        district_name.setCellValueFactory(new PropertyValueFactory<>("districtName"));
        cities.setCellValueFactory(new PropertyValueFactory<>("cityNames"));
        district_status.setCellValueFactory(new PropertyValueFactory<>("districtStatus"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_viewdistrict.setItems(parseUserList());
        lbl_total.setText("Districts\n"+String.format("%,.0f", (float)summaryTotal));
        lbl_active.setText("Active\n"+String.format("%,.0f", (float)summaryActive));
        lbl_inactive.setText("In Active\n"+String.format("%,.0f", (float)summaryInActive));
    }

    private ObservableList<ViewDistrictInfo> parseUserList(){
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        ViewDistrictInfo objViewDistrictInfo = new ViewDistrictInfo();
        districtsDetail = FXCollections.observableArrayList();
        ArrayList<ArrayList<String>> districtData;
        if(filter)
        {
            districtData  = objViewDistrictInfo.getDistrictsSearch(objStmt, objCon, txtDistrictId, txtDistrictName, txtDistrictStatus);
            txt_district_id.setText(txtDistrictId);
            txt_district_name.setText(txtDistrictName);
            txt_district_status.setValue(txtDistrictStatus);
            filter = false;
            filter_pane.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
        }
        else
        {
            districtData = objViewDistrictInfo.getDistrictsInfo(objStmt, objCon);
        }
        for (int i = 0; i < districtData.size(); i++)
        {
            summaryTotal++;
            if(districtData.get(i).get(5).equals("Active"))
            {
                summaryActive++;
            }
            else if(districtData.get(i).get(5).equals("In Active"))
            {
                summaryInActive++;
            }
//            System.out.println(String.valueOf(i+1)+"  ---   Tb Id: "+districtData.get(i).get(0)+"   ---   Id: "+districtData.get(i).get(1)+"   ---   Name: "+districtData.get(i).get(2)+"   ---   City Id: "+districtData.get(i).get(3)+"   ---   City Name: "+districtData.get(i).get(4)+"   ---   Status: "+districtData.get(i).get(5));
//            System.out.println("      ----------------------------------------     ");
            districtsDetail.add(new ViewDistrictInfo(String.valueOf(i+1), districtData.get(i).get(0), ((districtData.get(i).get(1) == null) ? "N/A" : districtData.get(i).get(1)), districtData.get(i).get(2), districtData.get(i).get(3),  ((districtData.get(i).get(4) == null) ? "N/A" : districtData.get(i).get(4)),  districtData.get(i).get(5)));
        }
        return districtsDetail;
    }

    @FXML
    void btnSearch(ActionEvent event) {
        txtDistrictId = txt_district_id.getText();
        txtDistrictName = txt_district_name.getText();
        txtDistrictStatus = txt_district_status.getValue();
        if(txtDistrictStatus == null)
        {
            txtDistrictStatus = "All";
        }
        filter = true;

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/view_district.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void btnReset(ActionEvent event) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/view_district.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void exportExcel(ActionEvent event) throws IOException {
        Workbook workbook = new HSSFWorkbook();
        Sheet spreadsheet = workbook.createSheet("data");
        Row row = spreadsheet.createRow(0);

        for (int j = 0; j < table_viewdistrict.getColumns().size()-1; j++) {
            row.createCell(j).setCellValue(table_viewdistrict.getColumns().get(j).getText());
        }

        for (int i = 0; i < table_viewdistrict.getItems().size(); i++) {
            row = spreadsheet.createRow(i + 1);
            for (int j = 0; j < table_viewdistrict.getColumns().size()-1; j++) {
                if(table_viewdistrict.getColumns().get(j).getCellData(i) != null) {
                    row.createCell(j).setCellValue(table_viewdistrict.getColumns().get(j).getCellData(i).toString());
                }
                else {
                    row.createCell(j).setCellValue("");
                }
            }
        }

        FileOutputStream fileOut = new FileOutputStream("data/View_Districts.xls");
        workbook.write(fileOut);
        fileOut.close();
    }

    public void follow() throws IOException
    {
        if (drawer.isOpened()) {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(-250);
            transition.setByY(0);
            transition.setCycleCount(1);
            transition.play();
            drawer.close();
            FA_Icon.setGlyphName("ANGLE_DOUBLE_RIGHT");
            FA_Icon.setGlyphSize(14);
            //inner_anchor.setRightAnchor(inner_anchor,1d);
            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(-90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(1.1000);
            scale.setToY(1);

            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();

        }
        else {
            Duration duration = Duration.seconds(0.4);
            //Create new translate transition
            TranslateTransition transition = new TranslateTransition(duration, btnpane);
            transition.setByX(250);
            transition.setByY(0);
            transition.setCycleCount(1);
            FA_Icon.setGlyphName("ANGLE_DOUBLE_LEFT");
            FA_Icon.setGlyphSize(14);
            drawer.open();

            TranslateTransition anchorePaneTransition = new TranslateTransition(duration, inner_anchor);
            anchorePaneTransition.setByX(90);
            anchorePaneTransition.setByY(0);
            anchorePaneTransition.setCycleCount(1);

            ScaleTransition scale = new ScaleTransition(duration,inner_anchor);
            scale.setToX(0.99947777);
            scale.setToY(1);

            ParallelTransition pltTransition = new ParallelTransition();
            pltTransition.getChildren().addAll(scale,anchorePaneTransition,transition);
            pltTransition.setCycleCount(1);
            pltTransition.play();

        }
    }

    @FXML
    void addDistrict(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/enter_district.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
}
