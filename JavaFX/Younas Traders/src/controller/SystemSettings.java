package controller;

import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXRadioButton;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import model.GlobalVariables;
import model.MysqlCon;
import model.SystemSettingsInfo;

import javax.swing.*;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ResourceBundle;

public class SystemSettings implements Initializable {
    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private AnchorPane batch_pane;

    @FXML
    private HBox batch_hbox;

    @FXML
    private ToggleGroup toggle_batch;

    @FXML
    private JFXRadioButton rad_entry_fifo;

    @FXML
    private JFXRadioButton rad_entry_lifo;

    @FXML
    private JFXRadioButton rad_expiry_fifo;

    @FXML
    private JFXRadioButton rad_expiry_lifo;

    @FXML
    private Button btn_update;

    @FXML
    private Button btn_cancel;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    @FXML
    private BorderPane brdrpane_navsetting;

    @FXML
    private BorderPane menu_bar;

    private SystemSettingsInfo objSystemSettingsInfo;
    private String batchPolicy;
    MysqlCon objMysqlCon;
    Statement objStmt;
    Connection objCon;
    public static Button btnView;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnSettings.setStyle("-fx-background-color: #47ab1e");
        objMysqlCon = new MysqlCon();
        objStmt = objMysqlCon.stmt;
        objCon = objMysqlCon.con;

        objSystemSettingsInfo = new SystemSettingsInfo();
        batchPolicy = objSystemSettingsInfo.getBatchPolicy(objStmt, objCon);
        if (batchPolicy.equals("Entry FIFO"))
        {
            rad_entry_fifo.setSelected(true);
        }
        else if (batchPolicy.equals("Entry LIFO"))
        {
            rad_entry_lifo.setSelected(true);
        }
        else if (batchPolicy.equals("Expiry FIFO"))
        {
            rad_expiry_fifo.setSelected(true);
        }
        else if (batchPolicy.equals("Expiry LIFO"))
        {
            rad_expiry_lifo.setSelected(true);
        }
    }

    @FXML
    void updateClicked(ActionEvent event) throws IOException {
        int index = toggle_batch.getToggles().indexOf(toggle_batch.getSelectedToggle());
        String selectedBatch = "";
        switch (index) {
            case 0:
                selectedBatch = "Entry FIFO";
                break;
            case 1:
                selectedBatch = "Entry LIFO";
                break;
            case 2:
                selectedBatch = "Expiry FIFO";
                break;
            case 3:
                selectedBatch = "Expiry LIFO";
                break;
        }
        objSystemSettingsInfo.updateBatchPolicy(objStmt, objCon, selectedBatch);
        Parent root = FXMLLoader.load(getClass().getResource("/view/system_settings.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void cancelClicked(ActionEvent event) {

    }

    @FXML
    void follow(MouseEvent event) {

    }
}
