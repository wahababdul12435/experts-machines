package controller;

import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.util.StringConverter;
import model.GlobalVariables;
import model.MysqlCon;
import model.ViewCompaniesDiscountInfo;
import model.ViewProductsDiscountInfo;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

public class ViewProductsDiscount implements Initializable {

    @FXML
    private AnchorPane inner_anchor;

    @FXML
    private TableView<ViewProductsDiscountInfo> table_viewproductsdiscount;

    @FXML
    private TableColumn<ViewProductsDiscountInfo, String> sr_no;

    @FXML
    private TableColumn<ViewProductsDiscountInfo, String> approval_id;

    @FXML
    private TableColumn<ViewProductsDiscountInfo, String> product_id;

    @FXML
    private TableColumn<ViewProductsDiscountInfo, String> product_name;

    @FXML
    private TableColumn<ViewProductsDiscountInfo, String> sale_amount;

    @FXML
    private TableColumn<ViewProductsDiscountInfo, String> discount;

    @FXML
    private TableColumn<ViewProductsDiscountInfo, String> start_date;

    @FXML
    private TableColumn<ViewProductsDiscountInfo, String> end_date;

    @FXML
    private TableColumn<ViewProductsDiscountInfo, String> entered_user;

    @FXML
    private TableColumn<ViewProductsDiscountInfo, String> operations;

    @FXML
    private AnchorPane filter_pane;

    @FXML
    private HBox filter_hbox;

    @FXML
    private JFXTextField txt_approval_id;

    @FXML
    private JFXTextField txt_product_id;

    @FXML
    private JFXTextField txt_product_name;

    @FXML
    private JFXDatePicker txt_start_date;

    @FXML
    private JFXDatePicker txt_end_date;

    @FXML
    private HBox summary_hbox;

    @FXML
    private Label lbl_total;

    @FXML
    private Label lbl_sale_amount;

    @FXML
    private Label lbl_discount;

    @FXML
    private StackPane stackPane;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private BorderPane menu_bar1;

    @FXML
    private BorderPane nav_live;

    @FXML
    private Pane btnpane;

    @FXML
    private Button btn11;

    @FXML
    private FontAwesomeIconView FA_Icon;

    public ArrayList<ViewProductsDiscountInfo> summaryData;
    public ObservableList<ViewProductsDiscountInfo> discountDetails;
    ArrayList<ArrayList<String>> discountData;
    ViewProductsDiscountInfo objViewProductsDiscountInfo;
    public static String txtApprovalId = "";
    public static String txtProductId = "";
    public static String txtProductName = "";
    public static String txtStartDate = "";
    public static String txtEndDate = "";
    public static boolean filter;

    public static MenuButton btnView;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnView.setStyle("-fx-background-color: #47ab1e");
        TopHeader.btnLive.setStyle("-fx-background-color: #47ab1e");
        summaryData = new ArrayList<>();
        objViewProductsDiscountInfo = new ViewProductsDiscountInfo();

        txt_start_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        txt_end_date.setConverter(new StringConverter<LocalDate>() {
            String pattern = "dd/MMM/yyyy";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });

        sr_no.setCellValueFactory(new PropertyValueFactory<>("srNo"));
        approval_id.setCellValueFactory(new PropertyValueFactory<>("approvalId"));
        product_id.setCellValueFactory(new PropertyValueFactory<>("productId"));
        product_name.setCellValueFactory(new PropertyValueFactory<>("productName"));
        sale_amount.setCellValueFactory(new PropertyValueFactory<>("saleAmount"));
        discount.setCellValueFactory(new PropertyValueFactory<>("discount"));
        start_date.setCellValueFactory(new PropertyValueFactory<>("startDate"));
        end_date.setCellValueFactory(new PropertyValueFactory<>("endDate"));
        operations.setCellValueFactory(new PropertyValueFactory<>("operationsPane"));
        table_viewproductsdiscount.setItems(parseUserList());
    }

    private ObservableList<ViewProductsDiscountInfo> parseUserList(){
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        ViewProductsDiscountInfo objViewProductsDiscountInfo = new ViewProductsDiscountInfo();
        discountDetails = FXCollections.observableArrayList();

        if(filter)
        {
            discountData = objViewProductsDiscountInfo.getProductsDiscountSearch(GlobalVariables.objStmt, txtApprovalId, txtProductId, txtProductName, txtStartDate, txtEndDate);
            txt_approval_id.setText(txtApprovalId);
            txt_product_id.setText(txtProductId);
            txt_product_name.setText(txtProductName);

            if(txtStartDate != null && !txtStartDate.equals(""))
            {
                txt_start_date.setValue(GlobalVariables.LOCAL_DATE(txtStartDate));
            }
            if(txtEndDate != null && !txtEndDate.equals(""))
            {
                txt_end_date.setValue(GlobalVariables.LOCAL_DATE(txtEndDate));
            }

            filter = false;
            filter_pane.setStyle("-fx-background-color: rgba(71,171,30, 0.1);");
        }
        else
        {
            discountData = objViewProductsDiscountInfo.getProductsDiscount(GlobalVariables.objStmt);
        }


        for (int i = 0; i < discountData.size(); i++)
        {
            discountDetails.add(new ViewProductsDiscountInfo(String.valueOf(i+1), discountData.get(i).get(0), discountData.get(i).get(1), discountData.get(i).get(2), ((discountData.get(i).get(3) == null) ? "N/A" : String.format("%,.2f", Float.parseFloat(discountData.get(i).get(3)))), ((discountData.get(i).get(4) == null) ? "N/A" : discountData.get(i).get(4)+"%"), ((discountData.get(i).get(5) == null) ? "N/A" : discountData.get(i).get(5)), discountData.get(i).get(6), discountData.get(i).get(7)));
            summaryData.add(new ViewProductsDiscountInfo(String.valueOf(i+1), discountData.get(i).get(0), discountData.get(i).get(1), discountData.get(i).get(2), ((discountData.get(i).get(3) == null) ? "N/A" : String.format("%,.2f", Float.parseFloat(discountData.get(i).get(3)))), ((discountData.get(i).get(4) == null) ? "N/A" : discountData.get(i).get(4)+"%"), ((discountData.get(i).get(5) == null) ? "N/A" : discountData.get(i).get(5)), discountData.get(i).get(6), discountData.get(i).get(7)));
        }
        return discountDetails;
    }

    @FXML
    void addDiscount(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/view/discount_policy.fxml"));
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void btnReset(ActionEvent event) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/view_products_discount.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    @FXML
    void btnSearch(ActionEvent event) {
        txtApprovalId = txt_approval_id.getText();
        txtProductId = txt_product_id.getText();
        txtProductName = txt_product_name.getText();
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date selectedDate = null;
        if(txt_start_date.getValue() != null)
        {
            txtStartDate = txt_start_date.getValue().toString();
            try {
                selectedDate = sdf.parse(txtStartDate);
                txtStartDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if(txt_end_date.getValue() != null)
        {
            txtEndDate = txt_end_date.getValue().toString();
            try {
                selectedDate = sdf.parse(txtEndDate);
                txtEndDate = sdf2.format(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        filter = true;
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/view_products_discount.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    public void generateSummary(ArrayList<ViewProductsDiscountInfo> summaryList) throws JRException {
        InputStream objIO = DealerFinanceReport.class.getResourceAsStream("/reports/ProductDiscount.jrxml");
        JasperReport objReport = JasperCompileManager.compileReport(objIO);
        JasperPrint objPrint = JasperFillManager.fillReport(objReport, null, new JRBeanCollectionDataSource(summaryList));
        JasperViewer.viewReport(objPrint, false);
    }

    @FXML
    void printData(ActionEvent event) {
        try {
            generateSummary(summaryData);
        } catch (JRException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void exportExcel(ActionEvent event) throws IOException {
        Workbook workbook = new HSSFWorkbook();
        Sheet spreadsheet = workbook.createSheet("data");
        Row row = spreadsheet.createRow(0);

        for (int j = 0; j < table_viewproductsdiscount.getColumns().size()-1; j++) {
            row.createCell(j).setCellValue(table_viewproductsdiscount.getColumns().get(j).getText());
        }

        for (int i = 0; i < table_viewproductsdiscount.getItems().size(); i++) {
            row = spreadsheet.createRow(i + 1);
            for (int j = 0; j < table_viewproductsdiscount.getColumns().size()-1; j++) {
                if(table_viewproductsdiscount.getColumns().get(j).getCellData(i) != null) {
                    row.createCell(j).setCellValue(table_viewproductsdiscount.getColumns().get(j).getCellData(i).toString());
                }
                else {
                    row.createCell(j).setCellValue("");
                }
            }
        }

        FileOutputStream fileOut = new FileOutputStream("data/View_Products_Discount.xls");
        workbook.write(fileOut);
        fileOut.close();
    }

    @FXML
    void follow(MouseEvent event) {

    }
}
