package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import controller.EnterDealer;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class DealerInfo {
    private String srNo;
    private String dealerId;
    private String dealerName;
    private String dealerContact;
    private String dealerAddress;

    private HBox operationsPane;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    public static ArrayList<String> dealerIdArr = new ArrayList<>();
    public static ArrayList<String> dealerNameArr = new ArrayList<>();
    public static ArrayList<String> dealerContactArr = new ArrayList<>();
    public static ArrayList<String> dealerAddressArr = new ArrayList<>();
    public static ArrayList<String> dealerTypeArr = new ArrayList<>();
    public static ArrayList<String> dealerCnicArr = new ArrayList<>();
    public static ArrayList<String> dealerLic9NumArr = new ArrayList<>();
    public static ArrayList<String> dealerLic9ExpArr = new ArrayList<>();
    public static ArrayList<String> dealerLic10NumArr = new ArrayList<>();
    public static ArrayList<String> dealerLic10ExpArr = new ArrayList<>();
    public static ArrayList<String> dealerLic11NumArr = new ArrayList<>();
    public static ArrayList<String> dealerLic11ExpArr = new ArrayList<>();
    public static ArrayList<String> dealerAreaIdArr = new ArrayList<>();
    public static ArrayList<String> dealerAreaNameArr = new ArrayList<>();

    public static TableView<DealerInfo> table_add_dealer;

    public static JFXTextField txtDealerId;
    public static JFXTextField txtDealerName;
    public static JFXTextField txtDealerContact;
    public static JFXTextField txtDealerAddress;
    public static JFXComboBox<String> txtDealerType;
    public static JFXTextField txtDealerCnic;
    public static JFXTextField txtDealerLic9Num;
    public static JFXDatePicker txtDealerLic9Exp;
    public static JFXTextField txtDealerLic10Num;
    public static JFXDatePicker txtDealerLic10Exp;
    public static JFXTextField txtDealerLic11Num;
    public static JFXDatePicker txtDealerLic11Exp;
    public static JFXComboBox<String> txtDealerArea;

    public static Button btnAdd;
    public static Button btnCancel;
    public static Button btnUpdate;

    private ArrayList<String> savedAreaIds;
    private ArrayList<String> savedAreaNames;

    MysqlCon objMysqlCon = new MysqlCon();
    Statement objStmt = objMysqlCon.stmt;
    Connection objCon = objMysqlCon.con;

    public DealerInfo() {
        srNo = "";
        dealerId = "";
        dealerName = "";
        dealerContact = "";
        dealerAddress = "";

        objMysqlCon = new MysqlCon();
        objStmt = objMysqlCon.stmt;
        objCon = objMysqlCon.con;

        getAreasInfo(objStmt, objCon);
    }

    public DealerInfo(String srNo, String dealerId, String dealerName, String dealerContact, String dealerAddress) {
        this.srNo = srNo;
        this.dealerId = dealerId;
        this.dealerName = dealerName;
        this.dealerContact = dealerContact;
        this.dealerAddress = dealerAddress;

        this.btnEdit = new JFXButton("Edit");
        this.btnDelete = new JFXButton("Delete");
        this.btnEdit.setOnAction((action)->editAddedRecord());
        this.btnDelete.setOnAction((action)->deleteAddedRecord());
        this.operationsPane = new HBox(btnEdit, btnDelete);
    }

    private void deleteAddedRecord()
    {
        dealerIdArr.remove(Integer.parseInt(srNo)-1);
        dealerNameArr.remove(Integer.parseInt(srNo)-1);
        dealerContactArr.remove(Integer.parseInt(srNo)-1);
        dealerAddressArr.remove(Integer.parseInt(srNo)-1);
        dealerTypeArr.remove(Integer.parseInt(srNo)-1);
        dealerCnicArr.remove(Integer.parseInt(srNo)-1);
        dealerLic9NumArr.remove(Integer.parseInt(srNo)-1);
        dealerLic9ExpArr.remove(Integer.parseInt(srNo)-1);
        dealerLic10NumArr.remove(Integer.parseInt(srNo)-1);
        dealerLic10ExpArr.remove(Integer.parseInt(srNo)-1);
        dealerLic11NumArr.remove(Integer.parseInt(srNo)-1);
        dealerLic11ExpArr.remove(Integer.parseInt(srNo)-1);
        dealerAreaIdArr.remove(Integer.parseInt(srNo)-1);
        dealerAreaNameArr.remove(Integer.parseInt(srNo)-1);
        table_add_dealer.setItems(EnterDealer.parseUserList());
    }

    public void editAddedRecord()
    {
        EnterDealer.srNo = Integer.parseInt(srNo)-1;
        txtDealerId.setText(dealerIdArr.get(Integer.parseInt(srNo)-1));
        txtDealerName.setText(dealerNameArr.get(Integer.parseInt(srNo)-1));
        txtDealerContact.setText(dealerContactArr.get(Integer.parseInt(srNo)-1));
        txtDealerAddress.setText(dealerAddressArr.get(Integer.parseInt(srNo)-1));
        txtDealerType.setValue(dealerTypeArr.get(Integer.parseInt(srNo)-1));
        txtDealerCnic.setText(dealerCnicArr.get(Integer.parseInt(srNo)-1));
        txtDealerLic9Num.setText(dealerLic9NumArr.get(Integer.parseInt(srNo)-1));
        txtDealerLic9Exp.setValue(LOCAL_DATE(dealerLic9ExpArr.get(Integer.parseInt(srNo)-1)));
        txtDealerLic10Num.setText(dealerLic9NumArr.get(Integer.parseInt(srNo)-1));
        txtDealerLic10Exp.setValue(LOCAL_DATE(dealerLic9ExpArr.get(Integer.parseInt(srNo)-1)));
        txtDealerLic11Num.setText(dealerLic9NumArr.get(Integer.parseInt(srNo)-1));
        txtDealerLic11Exp.setValue(LOCAL_DATE(dealerLic9ExpArr.get(Integer.parseInt(srNo)-1)));
        txtDealerArea.setValue(dealerAreaNameArr.get(Integer.parseInt(srNo)-1));
        btnAdd.setVisible(false);
        btnCancel.setVisible(true);
        btnUpdate.setVisible(true);
    }

    public static final LocalDate LOCAL_DATE (String dateString) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MMM/yyyy");
        LocalDate localDate = LocalDate.parse(dateString, formatter);
        return localDate;
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getDealerId() {
        return dealerId;
    }

    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getDealerContact() {
        return dealerContact;
    }

    public void setDealerContact(String dealerContact) {
        this.dealerContact = dealerContact;
    }

    public String getDealerAddress() {
        return dealerAddress;
    }

    public void setDealerAddress(String dealerAddress) {
        this.dealerAddress = dealerAddress;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public ArrayList<String> getSavedAreaIds() {
        return savedAreaIds;
    }

    public void setSavedAreaIds(ArrayList<String> savedAreaIds) {
        this.savedAreaIds = savedAreaIds;
    }

    public ArrayList<String> getSavedAreaNames() {
        return savedAreaNames;
    }

    public void setSavedAreaNames(ArrayList<String> savedAreaNames) {
        this.savedAreaNames = savedAreaNames;
    }

    public void insertDealer(Statement stmt, Connection con, String dealerAreaName, String dealerName, String dealerContact, String dealerAddress, String dealerType, String dealerCNIC, String dealerLicNum, String dealerLicExp, String dealerStatus)
    {
        try {
            String getSubAreaID = "SELECT `subarea_id` FROM `subarea_info` WHERE `sub_area_name` = '"+dealerAreaName+"'";
            ResultSet rs =stmt.executeQuery(getSubAreaID);
            rs.next();
            String dealerAreaID = rs.getString(1);

            String insertQuery = "INSERT INTO `dealer_info`(`dealer_area_id`, `dealer_name`, `dealer_contact_person`, `dealer_phone`, `dealer_address`, `dealer_type`, `dealer_cnic`, `dealer_lic9_num`, `dealer_lic9_exp`, `dealer_lic10_num`, `dealer_lic10_exp`, `dealer_lic11_num`, `dealer_lic11_exp`, `dealer_status`) VALUES ('"+dealerAreaID+"','"+dealerName+"','"+dealerName+"','"+dealerContact+"','"+dealerAddress+"','"+dealerType+"','"+dealerCNIC+"','"+dealerLicNum+"','"+dealerLicExp+"','"+dealerStatus+"')";
            stmt.executeUpdate(insertQuery);
            String getDealerId = "SELECT `dealer_id` FROM `dealer_info` WHERE `dealer_name` = '"+dealerName+"' AND `dealer_area_id` = '"+dealerAreaID+"' AND `dealer_phone` = '"+dealerContact+"'";
            rs = stmt.executeQuery(getDealerId);
            rs.next();
            String dealerId = rs.getString(1);
            String insertOverall = "INSERT INTO `dealer_overall_record`(`dealer_id`, `orders_given`, `successful_orders`, `ordered_packets`, `submitted_packets`, `ordered_boxes`, `submitted_boxes`, `order_price`, `discount_price`, `invoiced_price`, `waived_off_price`, `pending_payments`) VALUES ('"+dealerId+"','0','0','0','0','0','0','0','0','0','0','0')";
            stmt.executeUpdate(insertOverall);
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertDealer(Statement stmt, Connection con) throws SQLException {
        String currentDate = GlobalVariables.getStDate();
        String currentTime = GlobalVariables.getStTime();
        String currentUser = GlobalVariables.getUserId();
        boolean withId = false;
        boolean withoutId = false;
        String insertQuery;
        String insertOverall;
        String dealerTableId;
        int insertInfo = 1;

        for(int i=0; i<dealerIdArr.size(); i++)
        {
            if(dealerIdArr.get(i).equals("") || dealerIdArr.get(i) == null)
            {
                insertQuery = "INSERT INTO `dealer_info`(`dealer_area_id`, `dealer_name`, `dealer_contact_person`, `dealer_phone`, `dealer_address`, `dealer_type`, `dealer_cnic`, `dealer_lic9_num`, `dealer_lic9_exp`, `dealer_lic10_num`, `dealer_lic10_Exp`, `dealer_lic11_num`, `dealer_lic11_Exp`, `dealer_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`) VALUES ('"+dealerAreaIdArr.get(i)+"','"+dealerNameArr.get(i)+"','"+dealerNameArr.get(i)+"','"+dealerContactArr.get(i)+"','"+dealerAddressArr.get(i)+"','"+dealerTypeArr.get(i)+"','"+dealerCnicArr.get(i)+"','"+dealerLic9NumArr.get(i)+"','"+dealerLic9ExpArr.get(i)+"','"+dealerLic10NumArr.get(i)+"','"+dealerLic10ExpArr.get(i)+"','"+dealerLic11NumArr.get(i)+"','"+dealerLic11ExpArr.get(i)+"','Active','"+currentUser+"','"+currentDate+"','"+currentTime+"','"+currentUser+"','"+currentDate+"','"+currentTime+"')";
            }
            else
            {
                insertQuery = "INSERT INTO `dealer_info`(`dealer_id`, `dealer_area_id`, `dealer_name`, `dealer_contact_person`, `dealer_phone`, `dealer_address`, `dealer_type`, `dealer_cnic`,`dealer_lic9_num`, `dealer_lic9_exp`, `dealer_lic10_num`, `dealer_lic10_Exp`, `dealer_lic11_num`, `dealer_lic11_Exp`, `dealer_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`) VALUES ('"+dealerIdArr.get(i)+"','"+dealerAreaIdArr.get(i)+"','"+dealerNameArr.get(i)+"','"+dealerNameArr.get(i)+"','"+dealerContactArr.get(i)+"','"+dealerAddressArr.get(i)+"','"+dealerTypeArr.get(i)+"','"+dealerCnicArr.get(i)+"','"+dealerLic9NumArr.get(i)+"','"+dealerLic9ExpArr.get(i)+"','"+dealerLic10NumArr.get(i)+"','"+dealerLic10ExpArr.get(i)+"','"+dealerLic11NumArr.get(i)+"','"+dealerLic11ExpArr.get(i)+"','Active','"+currentUser+"','"+currentDate+"','"+currentTime+"','"+currentUser+"','"+currentDate+"','"+currentTime+"')";
            }
            if(stmt.executeUpdate(insertQuery) == -1)
            {
                insertInfo = -1;
            }
            dealerTableId = getDealerTableId(stmt, con, dealerNameArr.get(i), dealerContactArr.get(i), dealerCnicArr.get(i));
            insertOverall = "INSERT INTO `dealer_overall_record`(`dealer_id`, `orders_given`, `successful_orders`, `ordered_packets`, `submitted_packets`, `ordered_boxes`, `submitted_boxes`, `order_price`, `discount_price`, `invoiced_price`, `cash_collected`, `return_packets`, `return_boxes`, `return_price`, `cash_return`, `waived_off_price`, `pending_payments`) VALUES ('"+dealerIdArr.get(i)+"','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0')";
            if(stmt.executeUpdate(insertOverall) == -1)
            {
                insertInfo = -1;
            }
        }
        if(insertInfo == -1)
        {
            String title = "Error";
            String message = "Unable to insert";
            GlobalVariables.showNotification(-1, title, message);
        }
        else
        {
            String title = "Successfull";
            String message = "Record Inserted";
            GlobalVariables.showNotification(1, title, message);
        }

        refreshAllArrays();
    }

    private void refreshAllArrays()
    {
        dealerIdArr = new ArrayList<>();
        dealerNameArr = new ArrayList<>();
        dealerContactArr = new ArrayList<>();
        dealerAddressArr = new ArrayList<>();
        dealerTypeArr = new ArrayList<>();
        dealerCnicArr = new ArrayList<>();
        dealerLic9NumArr = new ArrayList<>();
        dealerLic9ExpArr = new ArrayList<>();
        dealerLic10ExpArr = new ArrayList<>();
        dealerLic10ExpArr = new ArrayList<>();
        dealerLic11ExpArr = new ArrayList<>();
        dealerLic11ExpArr = new ArrayList<>();
        dealerAreaIdArr = new ArrayList<>();
        dealerAreaNameArr = new ArrayList<>();
    }

    public void updateDealer(Statement stmt, Connection con, String dealerTableId, String dealerId, String areaId, String dealerName, String dealerContact, String dealerAddress, String dealerCnic, String dealerType, String dealerLic9Num, String dealerLic9Exp, String dealerLic10Num, String dealerLic10Exp, String dealerLic11Num, String dealerLic11Exp, String dealerStatus)
    {
        String currentDate = GlobalVariables.getStDate();
        String currentTime = GlobalVariables.getStTime();
        String currentUser = GlobalVariables.getUserId();
        String updateQuery;
        try {
            if(dealerId.equals("") || dealerId.equals("N/A"))
            {
                updateQuery = "UPDATE `dealer_info` SET `dealer_id`= NULL,`dealer_area_id`='"+areaId+"',`dealer_name`='"+dealerName+"',`dealer_phone`='"+dealerContact+"',`dealer_address`='"+dealerAddress+"',`dealer_type`='"+dealerType+"',`dealer_cnic`='"+dealerCnic+"',`dealer_lic9_num`='"+dealerLic9Num+"',`dealer_lic9_exp`='"+dealerLic9Exp+"',`dealer_lic10_num`='"+dealerLic10Num+"',`dealer_lic10_exp`='"+dealerLic10Exp+"',`dealer_lic11_num`='"+dealerLic11Num+"',`dealer_lic11_exp`='"+dealerLic11Exp+"',`dealer_status`='"+dealerStatus+"', `update_user_id`='"+currentUser+"',`update_date`='"+currentDate+"',`update_time`='"+currentTime+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `dealer_table_id` = '"+dealerTableId+"'";
            }
            else
            {
                updateQuery = "UPDATE `dealer_info` SET `dealer_id`='"+dealerId+"',`dealer_area_id`='"+areaId+"',`dealer_name`='"+dealerName+"',`dealer_phone   `='"+dealerContact+"',`dealer_address`='"+dealerAddress+"',`dealer_type`='"+dealerType+"',`dealer_cnic`='"+dealerCnic+"',`dealer_lic9_num`='"+dealerLic9Num+"',`dealer_lic9_exp`='"+dealerLic9Exp+"',`dealer_lic10_num`='"+dealerLic10Num+"',`dealer_lic10_exp`='"+dealerLic10Exp+"',`dealer_lic11_num`='"+dealerLic11Num+"',`dealer_lic11_exp`='"+dealerLic11Exp+"',`dealer_status`='"+dealerStatus+"', `update_user_id`='"+currentUser+"',`update_date`='"+currentDate+"',`update_time`='"+currentTime+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `dealer_table_id` = '"+dealerTableId+"'";
            }

            stmt.executeUpdate(updateQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<ArrayList<String>> getAddedDealer()
    {
        ArrayList<ArrayList<String>> dealerData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        for(int i=0; i<dealerIdArr.size(); i++)
        {
            temp = new ArrayList<String>();
            temp.add(dealerIdArr.get(i));
            temp.add(dealerNameArr.get(i));
            temp.add(dealerContactArr.get(i));
            temp.add(dealerAddressArr.get(i));
            dealerData.add(temp);
        }
        return dealerData;
    }


    public ArrayList<String> getDealerDetail(String dealerId)
    {
        ArrayList<String> dealerInfo = new ArrayList<>();
        ResultSet rs = null;
        try {
            rs = objStmt.executeQuery("SELECT `dealer_info`.`dealer_id`, `dealer_info`.`dealer_name`, `dealer_info`.`dealer_phone`, `area_info`.`area_name`, `dealer_info`.`dealer_address`, `dealer_info`.`dealer_type`, `dealer_info`.`dealer_cnic`, `dealer_info`.`dealer_lic9_num`, `dealer_info`.`dealer_lic9_exp`, `dealer_info`.`creating_date`, `dealer_info`.`update_date`, `dealer_info`.`dealer_status`, `dealer_info`.`dealer_image` FROM `dealer_info` LEFT OUTER JOIN `area_info` ON `dealer_info`.`dealer_area_id` = `area_info`.`area_table_id`  WHERE `dealer_info`.`dealer_table_id` = '"+dealerId+"'");
            while (rs.next())
            {
                dealerInfo.add(rs.getString(1)); // Dealer Id
                dealerInfo.add(rs.getString(2)); // Name
                dealerInfo.add(rs.getString(3)); // Contact
                dealerInfo.add(rs.getString(4)); // Area
                dealerInfo.add(rs.getString(5)); // Address
                dealerInfo.add(rs.getString(6)); // Type
                dealerInfo.add(rs.getString(7)); // CNIC
                dealerInfo.add(rs.getString(8)); // Lic Num
                dealerInfo.add(rs.getString(9)); // Lic Exp
                dealerInfo.add(rs.getString(10)); // Created
                dealerInfo.add(rs.getString(11)); // Updated
                dealerInfo.add(rs.getString(12)); // Status
                dealerInfo.add(rs.getString(13)); // Image
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dealerInfo;
    }

    String checkDealerId = new String();
    public String confirmNewId(String dealerId)
    {
        ResultSet rs3 = null;
        try {
            rs3 = objStmt.executeQuery("SELECT `dealer_id` FROM `dealer_info` WHERE `dealer_id` = '"+dealerId+"' AND `dealer_status` != 'Deleted'");
            while (rs3.next())
            {
                checkDealerId = rs3.getString(1) ;
            }
//            objCon.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return checkDealerId;
    }

    public ArrayList<ArrayList<String>> getDealersDetail(Statement stmt, Connection con)
    {
        ArrayList<ArrayList<String>> dealerData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT `dealer_table_id`, `dealer_name` FROM `dealer_info` WHERE `dealer_status` != 'Deleted'");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Dealer Id
                temp.add(rs.getString(2)); // Dealer Name
                dealerData.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dealerData;
    }

    public void getAreasInfo(Statement stmt, Connection con)
    {
        ArrayList<String> tempIds;
        ArrayList<String> tempNames;
        tempIds = new ArrayList<String>();
        tempNames = new ArrayList<String>();
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT `area_table_id`, `area_name` FROM `area_info` WHERE `area_status` != 'Deleted' ORDER BY `area_table_id`");
            while (rs.next())
            {
                tempIds.add(rs.getString(1));
                tempNames.add(rs.getString(2)+" ("+rs.getString(1)+")");
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        setSavedAreaIds(tempIds);
        setSavedAreaNames(tempNames);
    }

    public String getDealerTableId(Statement stmt3, Connection con3 , String dealerName, String dealerContact, String dealerCnic)
    {
        ResultSet rs3 = null;
        try {
            String getIdStatment = "SELECT `dealer_table_id` FROM `dealer_info` WHERE `dealer_name` = '"+dealerName+"' AND `dealer_phone` = '"+dealerContact+"' AND `dealer_cnic` = '"+dealerCnic+"' AND `dealer_status` != 'Deleted'";
            rs3 = stmt3.executeQuery(getIdStatment);
            while (rs3.next())
            {
                checkDealerId = rs3.getString(1) ;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return checkDealerId;
    }
}
