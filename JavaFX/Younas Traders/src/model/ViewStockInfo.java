package model;

import com.jfoenix.controls.JFXButton;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.text.TextAlignment;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class ViewStockInfo {
    private String srNo;
    private String productId;
    private String productName;
    private String batchNo;
    private String quantity;
    private String retailPrice;
    private String tradePrice;
    private String discount;
    private String expDate;
    private String daysLeft;
    private String companyName;
    private String groupName;
    private String productStatus;
    private Label lblQty;
    private Label lblExpiry;

    private HBox operationsPane;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    public ViewStockInfo() {
    }

    public ViewStockInfo(String srNo, String productId, String productName, String batchNo, String quantity, String retailPrice, String tradePrice, String discount, String expDate, String daysLeft, String companyName, String groupName, String productStatus, int isStockLess) {
        this.srNo = srNo;
        this.productId = productId;
        this.productName = productName;
        this.batchNo = batchNo;
        this.quantity = quantity;
        this.lblQty = new Label(this.quantity);
        if(isStockLess == 1)
        {
            this.lblQty.setStyle("-fx-background-color: yellow;");
        }
        else if(isStockLess == 2)
        {
            this.lblQty.setStyle("-fx-background-color: orange;");
        }
        this.lblQty.setPrefWidth(100);
        this.lblQty.setAlignment(Pos.CENTER);
//        this.lblQty.setScaleY(100);
//        this.lblQty.setText("Helloo");
        this.retailPrice = retailPrice;
        this.tradePrice = tradePrice;
        this.discount = discount;
        this.expDate = expDate;
        this.lblExpiry = new Label(this.expDate);
        if(!daysLeft.equals("N/A") && Integer.parseInt(daysLeft) < 60)
        {
            this.lblExpiry.setStyle("-fx-background-color: yellow;");
        }
        this.lblExpiry.setPrefWidth(150);
        this.lblExpiry.setAlignment(Pos.CENTER);
        this.daysLeft = daysLeft;
        this.companyName = companyName;
        this.groupName = groupName;
        this.productStatus = productStatus;

        this.btnEdit = new JFXButton();
        this.btnDelete = new JFXButton();
        this.btnEdit.setGraphic(GlobalVariables.createEditIcon());
        this.btnDelete.setGraphic(GlobalVariables.createDeleteIcon());
        this.btnEdit.getStyleClass().add("btn_icon");
        this.btnDelete.getStyleClass().add("btn_icon");
        this.operationsPane = new HBox(btnEdit, btnDelete);
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getRetailPrice() {
        return retailPrice;
    }

    public void setRetailPrice(String retailPrice) {
        this.retailPrice = retailPrice;
    }

    public String getTradePrice() {
        return tradePrice;
    }

    public void setTradePrice(String tradePrice) {
        this.tradePrice = tradePrice;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getExpDate() {
        return expDate;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }

    public String getDaysLeft() {
        return daysLeft;
    }

    public void setDaysLeft(String daysLeft) {
        this.daysLeft = daysLeft;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getProductStatus() {
        return productStatus;
    }

    public Label getLblQty() {
        return lblQty;
    }

    public void setLblQty(Label lblQty) {
        this.lblQty = lblQty;
    }

    public Label getLblExpiry() {
        return lblExpiry;
    }

    public void setLblExpiry(Label lblExpiry) {
        this.lblExpiry = lblExpiry;
    }

    public void setProductStatus(String productStatus) {
        this.productStatus = productStatus;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public ArrayList<ArrayList<String>> getStockInfo(Statement stmt, Connection con, String batches)
    {
        ArrayList<ArrayList<String>> salesmanData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        temp = new ArrayList<String>();
        ResultSet rs = null;
        String preProductId = "";
        String batchNos  = "";
        String batchQty  = "";
        String batchExp  = "";
        String query = "";
        boolean iter = false;
        if(batches.equals("Show Current Batches"))
        {
            query = "SELECT `product_info`.`product_table_id`, `product_info`.`product_name`, `batchwise_stock`.`batch_no`, `batchwise_stock`.`quantity`, `product_info`.`retail_price`, `product_info`.`trade_price`, `product_info`.`purchase_discount`, `batchwise_stock`.`batch_expiry`, `company_info`.`company_name`, `groups_info`.`group_name`, `products_stock`.`in_stock`-`products_stock`.`min_stock` as 'stock_threshold', `product_info`.`product_status` FROM `product_info` LEFT OUTER JOIN `batchwise_stock` ON `batchwise_stock`.`product_table_id` = `product_info`.`product_table_id` AND `batchwise_stock`.`quantity` != 0 LEFT OUTER JOIN `company_info` ON `company_info`.`company_table_id` = `product_info`.`company_table_id` LEFT OUTER JOIN `groups_info` ON `groups_info`.`group_table_id` = `product_info`.`group_table_id` LEFT OUTER JOIN `products_stock` ON `products_stock`.`product_table_id` = `product_info`.`product_table_id` WHERE `product_info`.`product_status` != 'Deleted' ORDER BY `product_info`.`product_table_id`";
        }
        else
        {
            query = "SELECT `product_info`.`product_table_id`, `product_info`.`product_name`, `batchwise_stock`.`batch_no`, `batchwise_stock`.`quantity`, `product_info`.`retail_price`, `product_info`.`trade_price`, `product_info`.`purchase_discount`, `batchwise_stock`.`batch_expiry`, `company_info`.`company_name`, `groups_info`.`group_name`, `products_stock`.`in_stock`-`products_stock`.`min_stock` as 'stock_threshold', `product_info`.`product_status` FROM `product_info` LEFT OUTER JOIN `batchwise_stock` ON `batchwise_stock`.`product_table_id` = `product_info`.`product_table_id` LEFT OUTER JOIN `company_info` ON `company_info`.`company_table_id` = `product_info`.`company_table_id` LEFT OUTER JOIN `groups_info` ON `groups_info`.`group_table_id` = `product_info`.`group_table_id` LEFT OUTER JOIN `products_stock` ON `products_stock`.`product_table_id` = `product_info`.`product_table_id` WHERE `product_info`.`product_status` != 'Deleted' ORDER BY `product_info`.`product_table_id`";
        }
        try {
            rs = stmt.executeQuery(query);
            while (rs.next())
            {
                if(preProductId.equals(rs.getString(1)))
                {
                    if(rs.getString(3) != null && !rs.getString(3).equals(""))
                    {
                        batchNos += "\n"+rs.getString(3);
                        batchQty += "\n"+rs.getString(4);
                    }
                    else
                    {
                        batchNos += "\n"+"N/A";
                        batchQty += "\n"+"N/A";
                    }
                    if(rs.getString(8) != null && !rs.getString(8).equals(""))
                    {
                        batchExp += "\n"+rs.getString(8);
                    }
                    else
                    {
                        batchExp += "\n"+"N/A";
                    }

                }
                else
                {
                    if(iter)
                    {
                        temp.set(2, batchNos);
                        temp.set(3, batchQty);
                        temp.set(7, batchExp);
                        salesmanData.add(temp);

                        if(rs.getString(3) != null && !rs.getString(3).equals(""))
                        {
                            batchNos = rs.getString(3);
                            batchQty = rs.getString(4);
                        }
                        else
                        {
                            batchNos = "N/A";
                            batchQty = "N/A";
                        }

                        if(rs.getString(8) != null && !rs.getString(8).equals(""))
                        {
                            batchExp = rs.getString(8);
                        }
                        else
                        {
                            batchExp = "N/A";
                        }
                        temp = new ArrayList<String>();
                        preProductId = rs.getString(1);
                    }
                    else
                    {
                        iter = true;
                        if(rs.getString(3) != null && !rs.getString(3).equals(""))
                        {
                            batchNos = rs.getString(3);
                            batchQty = rs.getString(4);
                        }
                        else
                        {
                            batchNos = "N/A";
                            batchQty = "N/A";
                        }

                        if(rs.getString(8) != null && !rs.getString(8).equals(""))
                        {
                            batchExp = rs.getString(8);
                        }
                        else
                        {
                            batchExp = "N/A";
                        }
                        temp = new ArrayList<>();
                        preProductId = rs.getString(1);
                    }
                    temp.add(rs.getString(1)); // Table Id
                    temp.add(rs.getString(2)); // Name
                    if(rs.getString(3) != null && !rs.getString(3).equals(""))
                    {
                        temp.add(rs.getString(3)); // Batch No
                        temp.add(rs.getString(4)); // Batch Qty
                    }
                    else
                    {
                        temp.add("N/A"); // Batch No
                        temp.add("N/A"); // Batch Qty
                    }

                    temp.add(rs.getString(5)); // Retail Price
                    temp.add(rs.getString(6)); // Trade Price
                    temp.add(rs.getString(7)); // Discount

                    if(rs.getString(8) != null && !rs.getString(8).equals(""))
                    {
                        temp.add(rs.getString(8)); // Batch Expiry
                        String stEndDate = rs.getString(8);
                        String stStartDate = GlobalVariables.getStDate();

                        Date endDate = null;
                        Date startDate = null;
                        try {
                            startDate = GlobalVariables.fmt.parse(stStartDate);
                            endDate = GlobalVariables.fmt.parse(stEndDate);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        long diff = endDate.getTime() - startDate.getTime();
                        diff = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);

                        temp.add(String.valueOf(diff)); // days left
                    }
                    else
                    {
                        temp.add("N/A"); // Batch Expiry
                        temp.add("N/A"); // days left
                    }

                    temp.add(rs.getString(9)); // Company Name
                    temp.add(rs.getString(10)); // Group Name
                    temp.add(rs.getString(11)); // Stock Threshold
                    temp.add(rs.getString(12)); // status
                }
            }
            if(iter)
            {
                temp.set(2, batchNos);
                temp.set(3, batchQty);
                temp.set(7, batchExp);
                salesmanData.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return salesmanData;
    }

    public ArrayList<ArrayList<String>> getStockInfoSearch(Statement stmt, Connection con, String batches, String productId, String productName, String batchNo, String fromRetail, String toRetail, String fromExpiry, String toExpiry, String companyId, String productStatus)
    {
        ArrayList<ArrayList<String>> salesmanData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        temp = new ArrayList<String>();
        ResultSet rs = null;
        String preProductId = "";
        String batchNos  = "";
        String batchQty  = "";
        String batchExp  = "";
        boolean iter = false;
        String searchQuery;
        int multipleSearch = 0;
        if(batches.equals("Show Current Batches"))
        {
            searchQuery = "SELECT `product_info`.`product_table_id`, `product_info`.`product_name`, `batchwise_stock`.`batch_no`, `batchwise_stock`.`quantity`, `product_info`.`retail_price`, `product_info`.`trade_price`, `product_info`.`purchase_discount`, `batchwise_stock`.`batch_expiry`, `company_info`.`company_name`, `groups_info`.`group_name`, `products_stock`.`in_stock`-`products_stock`.`min_stock` as 'stock_threshold', `product_info`.`product_status` FROM `product_info` LEFT OUTER JOIN `batchwise_stock` ON `batchwise_stock`.`product_table_id` = `product_info`.`product_table_id` AND `batchwise_stock`.`quantity` != 0 LEFT OUTER JOIN `company_info` ON `company_info`.`company_table_id` = `product_info`.`company_table_id` LEFT OUTER JOIN `groups_info` ON `groups_info`.`group_table_id` = `product_info`.`group_table_id` LEFT OUTER JOIN `products_stock` ON `products_stock`.`product_table_id` = `product_info`.`product_table_id` WHERE ";
        }
        else
        {
            searchQuery = "SELECT `product_info`.`product_table_id`, `product_info`.`product_name`, `batchwise_stock`.`batch_no`, `batchwise_stock`.`quantity`, `product_info`.`retail_price`, `product_info`.`trade_price`, `product_info`.`purchase_discount`, `batchwise_stock`.`batch_expiry`, `company_info`.`company_name`, `groups_info`.`group_name`, `products_stock`.`in_stock`-`products_stock`.`min_stock` as 'stock_threshold', `product_info`.`product_status` FROM `product_info` LEFT OUTER JOIN `batchwise_stock` ON `batchwise_stock`.`product_table_id` = `product_info`.`product_table_id` LEFT OUTER JOIN `company_info` ON `company_info`.`company_table_id` = `product_info`.`company_table_id` LEFT OUTER JOIN `groups_info` ON `groups_info`.`group_table_id` = `product_info`.`group_table_id` LEFT OUTER JOIN `products_stock` ON `products_stock`.`product_table_id` = `product_info`.`product_table_id` WHERE ";
        }

        if(!productId.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`product_info`.`product_id` = '"+productId+"'";
            multipleSearch++;
        }
        if(!productName.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`product_info`.`product_name` = '"+productName+"'";
            multipleSearch++;
        }
        if(!batchNo.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`batchwise_stock`.`batch_no` = '"+batchNo+"'";
            multipleSearch++;
        }
        if(!fromRetail.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`product_info`.`retail_price` >= '"+fromRetail+"'";
            multipleSearch++;
        }
        if(!toRetail.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`product_info`.`retail_price` <= '"+toRetail+"'";
            multipleSearch++;
        }
        if(!fromExpiry.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "str_to_date(`batchwise_stock`.`batch_expiry`, '%d/%b/%Y') >= str_to_date('"+fromExpiry+"', '%d/%b/%Y')";
            multipleSearch++;
        }
        if(!toExpiry.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "str_to_date(`batchwise_stock`.`batch_expiry`, '%d/%b/%Y') <= str_to_date('"+toExpiry+"', '%d/%b/%Y')";
            multipleSearch++;
        }
        if(!companyId.equals("All"))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`product_info`.`company_table_id` = '"+companyId+"'";
            multipleSearch++;
        }
//        if(!groupId.equals("All"))
//        {
//            if(multipleSearch > 0)
//            {
//                searchQuery += " AND ";
//            }
//            searchQuery += "`product_info`.`group_table_id` = '"+groupId+"'";
//            multipleSearch++;
//        }
        if(!productStatus.equals("All"))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`product_info`.`product_status` = '"+productStatus+"'";
            multipleSearch++;
        }
        if(multipleSearch >= 1)
        {
            searchQuery += " AND ";
        }
        searchQuery += "`product_info`.`product_status` != 'Deleted' ORDER BY `product_info`.`product_table_id`";
        try {
//            System.out.println(searchQuery);
            rs = stmt.executeQuery(searchQuery);
            while (rs.next())
            {
                if(preProductId.equals(rs.getString(1)))
                {
                    if(rs.getString(3) != null && !rs.getString(3).equals(""))
                    {
                        batchNos += "\n"+rs.getString(3);
                        batchQty += "\n"+rs.getString(4);
                    }
                    else
                    {
                        batchNos += "\n"+"N/A";
                        batchQty += "\n"+"N/A";
                    }
                    if(rs.getString(8) != null && !rs.getString(8).equals(""))
                    {
                        batchExp += "\n"+rs.getString(8);
                    }
                    else
                    {
                        batchExp += "\n"+"N/A";
                    }

                }
                else
                {
                    if(iter)
                    {
                        temp.set(2, batchNos);
                        temp.set(3, batchQty);
                        temp.set(7, batchExp);
                        salesmanData.add(temp);

                        if(rs.getString(3) != null && !rs.getString(3).equals(""))
                        {
                            batchNos = rs.getString(3);
                            batchQty = rs.getString(4);
                        }
                        else
                        {
                            batchNos = "N/A";
                            batchQty = "N/A";
                        }

                        if(rs.getString(8) != null && !rs.getString(8).equals(""))
                        {
                            batchExp = rs.getString(8);
                        }
                        else
                        {
                            batchExp = "N/A";
                        }
                        temp = new ArrayList<String>();
                        preProductId = rs.getString(1);
                    }
                    else
                    {
                        iter = true;
                        if(rs.getString(3) != null && !rs.getString(3).equals(""))
                        {
                            batchNos = rs.getString(3);
                            batchQty = rs.getString(4);
                        }
                        else
                        {
                            batchNos = "N/A";
                            batchQty = "N/A";
                        }

                        if(rs.getString(8) != null && !rs.getString(8).equals(""))
                        {
                            batchExp = rs.getString(8);
                        }
                        else
                        {
                            batchExp = "N/A";
                        }
                        temp = new ArrayList<>();
                        preProductId = rs.getString(1);
                    }
                    temp.add(rs.getString(1)); // Table Id
                    temp.add(rs.getString(2)); // Name
                    if(rs.getString(3) != null && !rs.getString(3).equals(""))
                    {
                        temp.add(rs.getString(3)); // Batch No
                        temp.add(rs.getString(4)); // Batch Qty
                    }
                    else
                    {
                        temp.add("N/A"); // Batch No
                        temp.add("N/A"); // Batch Qty
                    }

                    temp.add(rs.getString(5)); // Retail Price
                    temp.add(rs.getString(6)); // Trade Price
                    temp.add(rs.getString(7)); // Discount

                    if(rs.getString(8) != null && !rs.getString(8).equals(""))
                    {
                        temp.add(rs.getString(8)); // Batch Expiry
                        String stEndDate = rs.getString(8);
                        String stStartDate = GlobalVariables.getStDate();

                        Date endDate = null;
                        Date startDate = null;
                        try {
                            startDate = GlobalVariables.fmt.parse(stStartDate);
                            endDate = GlobalVariables.fmt.parse(stEndDate);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        long diff = endDate.getTime() - startDate.getTime();
                        diff = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);

                        temp.add(String.valueOf(diff)); // days left
                    }
                    else
                    {
                        temp.add("N/A"); // Batch Expiry
                        temp.add("N/A"); // days left
                    }

                    temp.add(rs.getString(9)); // Company Name
                    temp.add(rs.getString(10)); // Group Name
                    temp.add(rs.getString(11)); // Stock Threshold
                    temp.add(rs.getString(12)); // status
                }
            }
            if(iter)
            {
                temp.set(2, batchNos);
                temp.set(3, batchQty);
                temp.set(7, batchExp);
                salesmanData.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return salesmanData;
    }

    private Node createEditIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.EDIT);
        graphic.setFontSize(20.0);
        return graphic;
    }

    private Node createDeleteIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.TRASH);
        graphic.setFontSize(20.0);
        return graphic;
    }
}
