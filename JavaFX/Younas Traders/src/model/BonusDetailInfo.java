package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class BonusDetailInfo {

    private String sr_No;
    private String products_id;
    private String products_name;
    private String dealers_id;
    private String dealers_name;
    private String dealers_type;
    private String quantities;
    private String bonuses;

    public BonusDetailInfo()
    {  }

    public BonusDetailInfo(String sr_No, String products_id, String products_name, String dealers_id, String dealers_name, String dealers_type, String quantities, String bonuses) {
        this.sr_No = sr_No;
        this.products_id = products_id;
        this.products_name = products_name;
        this.dealers_id = dealers_id;
        this.dealers_name = dealers_name;
        this.dealers_type = dealers_type;
        this.quantities = quantities;
        this.bonuses = bonuses;
    }

    public String getSr_No() {
        return sr_No;
    }

    public void setSr_No(String sr_No) {
        this.sr_No = sr_No;
    }

    public String getProducts_id() {
        return products_id;
    }

    public void setProducts_id(String products_id) {
        this.products_id = products_id;
    }

    public String getProducts_name() {
        return products_name;
    }

    public void setProducts_name(String products_name) {
        this.products_name = products_name;
    }

    public String getDealers_id() {
        return dealers_id;
    }

    public void setDealers_id(String dealers_id) {
        this.dealers_id = dealers_id;
    }

    public String getDealers_name() {
        return dealers_name;
    }

    public void setDealers_name(String dealers_name) {
        this.dealers_name = dealers_name;
    }

    public String getDealers_type() {
        return dealers_type;
    }

    public void setDealers_type(String dealers_type) {
        this.dealers_type = dealers_type;
    }

    public String getQuantities() {
        return quantities;
    }

    public void setQuantities(String quantities) {
        this.quantities = quantities;
    }

    public String getBonuses() {
        return bonuses;
    }

    public void setBonuses(String bonuses) {
        this.bonuses = bonuses;
    }

    public ArrayList<ArrayList<String>> getBonusPolicydetails(Statement stmt, Connection con, String policy_approvalID)
    {
        ArrayList<ArrayList<String>> bonusPolicyData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        ResultSet rs1 = null;
        try {
            rs = stmt.executeQuery("SELECT `bonus_policy`.approval_id,`product_info`.product_id,`dealer_info`.dealer_id,`dealer_info`.dealer_name,`bonus_policy`.quantity,`bonus_policy`.bonus_quant, `bonus_policy`.start_date,`bonus_policy`.end_date,`bonus_policy`.policy_status FROM `bonus_policy` inner JOIN `product_info` on `product_info`.`product_table_id` = `bonus_policy`.`product_table_id` inner join `dealer_info` on `dealer_info`.`dealer_table_id` = `bonus_policy`.`dealer_table_id` WHERE policy_status != 'Deleted' and approval_id =  '"+policy_approvalID+"' ");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2));
                String productsID = rs.getString(2);

                MysqlCon objMysqlCon = new MysqlCon();
                Statement objStmt = objMysqlCon.stmt;
                Connection objCon = objMysqlCon.con;

                rs1 = objStmt.executeQuery("SELECT product_name from product_info where product_id = '"+productsID+"'");
                while (rs1.next())
                {
                    temp.add(rs1.getString(1));
                }

                temp.add(rs.getString(3));
                temp.add(rs.getString(4));
                temp.add(rs.getString(5));
                temp.add(rs.getString(6));
                temp.add(rs.getString(7));
                temp.add(rs.getString(8));
                temp.add(rs.getString(9));
                bonusPolicyData.add(temp);
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return bonusPolicyData;
    }
}
