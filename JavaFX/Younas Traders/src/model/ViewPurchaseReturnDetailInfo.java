package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import controller.ViewPurchaseReturnDetail;
import controller.ViewSaleReturnDetail;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ViewPurchaseReturnDetailInfo {
    public String returnId;
    private String srNo;
    private String productId;
    private String productName;
    private String batchNo;
    private String returnQuantity;
    private String returnPrice;
    private String returnReason;

    private HBox operationsPane;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    public static ArrayList<String> preProductIdArr = new ArrayList<>();
    public static ArrayList<String> productIdArr = new ArrayList<>();
    public static ArrayList<String> productNameArr = new ArrayList<>();
    public static ArrayList<String> preBatchNoArr = new ArrayList<>();
    public static ArrayList<String> batchNoArr = new ArrayList<>();
    public static ArrayList<String> preReturnQuantityArr = new ArrayList<>();
    public static ArrayList<String> returnQuantityArr = new ArrayList<>();
    public static ArrayList<String> returnPriceArr = new ArrayList<>();
    //    public static ArrayList<String> unitArr = new ArrayList<>();
    public static ArrayList<String> returnReasonArr = new ArrayList<>();

    public static TableView<ViewPurchaseReturnDetailInfo> table_purchasereturndetaillog;

    public static JFXTextField txtProductName;
    public static JFXComboBox<String> txtBatchNo;
    public static JFXTextField txtReturnQuantity;
    public static JFXTextField txtReturnPrice;
    //    public static JFXComboBox<String>  txtUnit;
    public static JFXComboBox<String> txtReturnReason;

    public static Button btnAdd;
    public static Button btnCancel;
    public static Button btnUpdate;

    public ViewPurchaseReturnDetailInfo(String returnId) {
        this.returnId = returnId;
    }

    public ViewPurchaseReturnDetailInfo(String srNo, String productName, String batchNo, String returnQuantity, String returnPrice, String returnReason) {
        this.srNo = srNo;
        this.productName = productName;
        this.batchNo = batchNo;
        this.returnQuantity = returnQuantity;
        this.returnPrice = returnPrice;
        this.returnReason = returnReason;

        this.btnEdit = new JFXButton();
        this.btnDelete = new JFXButton();
        this.btnEdit.setGraphic(GlobalVariables.createEditIcon());
        this.btnDelete.setGraphic(GlobalVariables.createDeleteIcon());
        this.btnEdit.getStyleClass().add("btn_icon");
        this.btnDelete.getStyleClass().add("btn_icon");
        this.operationsPane = new HBox(btnEdit, btnDelete);
        this.btnEdit.setOnAction((action)->editAddedRecord());
        this.btnDelete.setOnAction((action)->deleteAddedRecord());
    }

    private void editAddedRecord()
    {
        ViewSaleReturnDetail.srNo = Integer.parseInt(srNo)-1;
        txtProductName.setText(productNameArr.get(Integer.parseInt(srNo)-1));
        txtBatchNo.getItems().clear();
        String productId = ViewSaleReturnDetail.getProductId(ViewSaleReturnDetail.completeProductData, txtProductName.getText());
        ArrayList<String> batchData = ViewSaleReturnDetail.getProductsBatch(ViewSaleReturnDetail.completeBatchData, productId);
        txtBatchNo.getItems().addAll(batchData);
        txtBatchNo.setValue(batchNoArr.get(Integer.parseInt(srNo)-1));
        txtReturnQuantity.setText(returnQuantityArr.get(Integer.parseInt(srNo)-1));
        txtReturnPrice.setText(returnPriceArr.get(Integer.parseInt(srNo)-1));
//        txtUnit.setValue(unitArr.get(Integer.parseInt(srNo)-1));
        txtReturnReason.setValue(returnReasonArr.get(Integer.parseInt(srNo)-1));
        ViewSaleReturnDetail.selectedProductId = productIdArr.get(Integer.parseInt(srNo)-1);
        ViewSaleReturnDetail.selectedProductPrice = String.valueOf(Float.parseFloat(returnPriceArr.get(Integer.parseInt(srNo)-1))/Integer.parseInt(returnQuantityArr.get(Integer.parseInt(srNo)-1)));
        btnAdd.setVisible(false);
        btnCancel.setVisible(true);
        btnUpdate.setVisible(true);
    }

    private void deleteAddedRecord()
    {
        productIdArr.remove(Integer.parseInt(srNo)-1);
        productNameArr.remove(Integer.parseInt(srNo)-1);
        batchNoArr.remove(Integer.parseInt(srNo)-1);
        returnQuantityArr.remove(Integer.parseInt(srNo)-1);
        returnPriceArr.remove(Integer.parseInt(srNo)-1);
//        unitArr.remove(Integer.parseInt(srNo)-1);
        returnReasonArr.remove(Integer.parseInt(srNo)-1);
        table_purchasereturndetaillog.setItems(ViewPurchaseReturnDetail.parseUserList());
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    public String getReturnQuantity() {
        return returnQuantity;
    }

    public void setReturnQuantity(String returnQuantity) {
        this.returnQuantity = returnQuantity;
    }

    public String getReturnPrice() {
        return returnPrice;
    }

    public void setReturnPrice(String returnPrice) {
        this.returnPrice = returnPrice;
    }

    public String getReturnReason() {
        return returnReason;
    }

    public void setReturnReason(String returnReason) {
        this.returnReason = returnReason;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public ArrayList<String> getPurchaseReturnBasicInfo(Statement stmt, Connection con)
    {
        ArrayList<String> returnBasicData = new ArrayList<String>();
        ArrayList<String> temp;
        ResultSet rs = null;
        String query = "SELECT `purchase_return`.`returnTable_id`, `company_info`.`company_name`, CONCAT(`user_info`.`user_name`, ' (', `user_info`.`user_table_id`, ')'), `purchase_return`.`entry_date`, `purchase_return`.`entry_time` FROM `purchase_return` LEFT OUTER JOIN `company_info` ON `company_info`.`company_table_id` = `purchase_return`.`company_table_id` LEFT OUTER JOIN `user_info` ON `user_info`.`user_table_id` = `purchase_return`.`user_id` WHERE `purchase_return`.`returnTable_id` = '"+returnId+"'";
//        System.out.println(query);
        try {
            rs = stmt.executeQuery(query);
            while (rs.next())
            {
                returnBasicData.add(rs.getString(1)); // Return Id
                returnBasicData.add(rs.getString(2)); // Dealer Name
                returnBasicData.add(rs.getString(3)); // User Name
                returnBasicData.add(rs.getString(4)); // Return Date
                returnBasicData.add(rs.getString(5)); // Return Time
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return returnBasicData;
    }

    public ArrayList<ArrayList<String>> getPurchaseReturnDetailInfo(Statement stmt, Connection con)
    {
        ArrayList<ArrayList<String>> returnData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        String query = "SELECT `purchase_return_detail`.`prod_id`, `product_info`.`product_name`, `purchase_return_detail`.`prod_batch`, `purchase_return_detail`.`prod_quant`, `purchase_return_detail`.`total_amount`, `purchase_return_detail`.`return_reason` FROM `purchase_return_detail` INNER JOIN `purchase_return` ON `purchase_return`.`returnTable_id` = `purchase_return_detail`.`return_id` AND `purchase_return`.`returnTable_id` = '"+returnId+"' LEFT OUTER JOIN `product_info` ON `product_info`.`product_table_id` = `purchase_return_detail`.`prod_id` ORDER BY `purchase_return_detail`.`prod_id`";
        try {
            rs = stmt.executeQuery(query);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Product Id
                preProductIdArr.add(rs.getString(1));
                productIdArr.add(rs.getString(1));
                temp.add(rs.getString(2)); // Product Name
                productNameArr.add(rs.getString(2));
                temp.add(rs.getString(3)); // Batch No
                preBatchNoArr.add(rs.getString(3));
                batchNoArr.add(rs.getString(3));
                temp.add(rs.getString(4) == null || rs.getString(4).equals("") ? "0" : rs.getString(4)); // Return Qty
                preReturnQuantityArr.add(rs.getString(4));
                returnQuantityArr.add(rs.getString(4));
                temp.add(rs.getString(5) == null || rs.getString(5).equals("") ? "0" : rs.getString(5)); // Return Price
                returnPriceArr.add(rs.getString(5));
                temp.add(rs.getString(6)); // Return Reason
                returnReasonArr.add(rs.getString(6));
                returnData.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return returnData;
    }

    public ArrayList<ArrayList<String>> loadReturnTable()
    {
        ArrayList<ArrayList<String>> saleData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        for(int i=0; i<productIdArr.size(); i++)
        {
            temp = new ArrayList<String>();
            temp.add(productNameArr.get(i));
            temp.add(batchNoArr.get(i));
            temp.add(returnQuantityArr.get(i));
            temp.add(returnPriceArr.get(i));
            temp.add(returnReasonArr.get(i));
            saleData.add(temp);
        }
        return saleData;
    }

    public void updateBasic(Statement stmt, Connection con, String companyId, String returnPrice, String enterUserId, String enterDate, String enterTime)
    {
        String currentDate = GlobalVariables.getStDate();
        String currentTime = GlobalVariables.getStTime();
        String currentUser = GlobalVariables.getUserId();
        String updateQuery = "UPDATE `purchase_return` SET `company_table_id` = '"+companyId+"', `return_total_price` = '"+returnPrice+"', `return_gross_price` = '"+returnPrice+"', `entry_date` = '"+(enterDate.equals(null) ? "" : enterDate)+"', `entry_time` = '"+(enterTime.equals(null) ? "" : enterTime)+"', `update_date` = '"+currentDate+"', `update_time` = '"+currentTime+"', `user_id` = '"+(enterUserId.equals(null) ? "" : enterUserId)+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `returnTable_id` = '"+this.returnId+"'";
        try {
            stmt.executeUpdate(updateQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateReturn(Statement stmt, Connection con)
    {
        String currentDate = GlobalVariables.getStDate();
        String currentTime = GlobalVariables.getStTime();
        String currentUser = GlobalVariables.getUserId();
        String updateProductStock = "";
        String updateBatchStock = "";
        String updateQuery = "INSERT INTO `purchase_return_detail`(`return_id`, `prod_id`, `prod_batch`, `prod_quant`, `bonus_quant`, `discount_amount`, `total_amount`, `unit`, `return_reason`) VALUES ";
        String deleteQuery = "DELETE FROM `purchase_return_detail` WHERE `return_id` = '"+returnId+"'";
        String unit = "Packets";
        try {
            stmt.executeUpdate(deleteQuery);
            for(int i=0; i<preProductIdArr.size(); i++)
            {
                updateProductStock = "UPDATE `products_stock` SET `in_stock` = `in_stock` + '"+preReturnQuantityArr.get(i)+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `product_table_id` = '"+preProductIdArr.get(i)+"'; ";
                updateBatchStock = "UPDATE `batchwise_stock` SET `quantity` = `quantity` + '"+preReturnQuantityArr.get(i)+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `product_table_id` = '"+preProductIdArr.get(i)+"' AND `batch_no` = '"+preBatchNoArr.get(i)+"';";
                stmt.executeUpdate(updateProductStock);
                stmt.executeUpdate(updateBatchStock);
            }
            for(int i=0; i<productIdArr.size(); i++)
            {
                if(i == productIdArr.size()-1)
                {
                    updateQuery += "('"+returnId+"','"+productIdArr.get(i)+"','"+batchNoArr.get(i)+"','"+returnQuantityArr.get(i)+"','0','0','"+returnPriceArr.get(i)+"','"+unit+"','"+returnReasonArr.get(i)+"') ";
                }
                else
                {
                    updateQuery += "('"+returnId+"','"+productIdArr.get(i)+"','"+batchNoArr.get(i)+"','"+returnQuantityArr.get(i)+"','0','0','"+returnPriceArr.get(i)+"','"+unit+"','"+returnReasonArr.get(i)+"'), ";
                }
                updateProductStock = "UPDATE `products_stock` SET `in_stock` = `in_stock` - '"+returnQuantityArr.get(i)+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `product_table_id` = '"+productIdArr.get(i)+"'; ";
                updateBatchStock = "UPDATE `batchwise_stock` SET `quantity` = `quantity` - '"+returnQuantityArr.get(i)+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `product_table_id` = '"+productIdArr.get(i)+"' AND `batch_no` = '"+batchNoArr.get(i)+"';";
                stmt.executeUpdate(updateProductStock);
                stmt.executeUpdate(updateBatchStock);
            }
//            updateQuery = "UPDATE `dealer_info` SET `dealer_id`='"+dealerId+"',`dealer_area_id`='"+areaId+"',`dealer_name`='"+dealerName+"',`dealer_phone`='"+dealerContact+"',`dealer_address`='"+dealerAddress+"',`dealer_type`='"+dealerType+"',`dealer_cnic`='"+dealerCnic+"',`dealer_lic9_num`='"+dealerLicNum+"',`dealer_lic9_exp`='"+dealerLicExp+"',`dealer_status`='"+dealerStatus+"', `update_user_id`='"+currentUser+"',`update_date`='"+currentDate+"',`update_time`='"+currentTime+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `dealer_table_id` = '"+dealerTableId+"'";
            stmt.executeUpdate(updateQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
