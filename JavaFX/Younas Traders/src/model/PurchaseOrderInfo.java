package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import controller.DeleteWindow;
import controller.UpdateCompany;
import controller.UpdateDealer;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;

public class PurchaseOrderInfo {
    private String srNo;
    private String productID;
    private String productName;
    private String cartonSize;
    private String soldQauntity;
    private String quantityInstock;
    private String orderQauntity;
    private String purchPrice;
    private String netAmount;

    private HBox operationsPane;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    public static StackPane stackPane;
    public static JFXDialog dialog;
    public static Label lblUpdate;

    public PurchaseOrderInfo()
    {
        this.srNo = "";
        this.productID = "";
        this.productName = "";
        this.cartonSize = "";
        this.soldQauntity = "";
        this.quantityInstock = "";
        this.orderQauntity = "";
        this.purchPrice = "";
        this.netAmount = "";

        this.btnEdit = new JFXButton("Edit");
        this.btnDelete = new JFXButton("Delete");
        this.operationsPane = new HBox(btnEdit, btnDelete);
        lblUpdate = new Label("");
    }

    public PurchaseOrderInfo(String srNo, String productID, String productName,String cartonSize, String soldQauntity , String quantityInstock,String orderQauntity,String purchPrice,String netAmount) {
        this.srNo = srNo;
        this.productID = productID;
        this.productName = productName;
        this.cartonSize = cartonSize;
        this.soldQauntity = soldQauntity;
        this.quantityInstock = quantityInstock;
        this.orderQauntity = orderQauntity;
        this.purchPrice = purchPrice;
        this.netAmount = netAmount;
        lblUpdate = new Label("");

        this.btnEdit = new JFXButton();
        this.btnDelete = new JFXButton();
        this.btnEdit.setGraphic(GlobalVariables.createEditIcon());
        this.btnDelete.setGraphic(GlobalVariables.createDeleteIcon());
        this.btnEdit.getStyleClass().add("btn_icon");
        this.btnDelete.getStyleClass().add("btn_icon");
        this.operationsPane = new HBox(btnEdit, btnDelete);

        this.btnEdit.setOnAction((action)->editClicked());
        this.btnDelete.setOnAction(event -> {
            try {
                deleteDealer(this.productID);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void editClicked()
    {

    }

    public void deleteDealer(String dealerTableId) throws IOException {
        DeleteWindow.sceneWindow = "ViewDealer";
        DeleteWindow.deletionId = dealerTableId;
        Parent parent = null;
        try {
            parent = FXMLLoader.load(getClass().getResource("/view/delete_window.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setStyle("-fx-padding: -40 -20 -40 -20;");
        dialogLayout.setBody(parent);
        dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        stackPane.setVisible(true);
        dialog.show();
        dialog.setOverlayClose(false);
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getProductID() {
        return productID;
    }

    public void setProductID(String dealerTableId) {
        this.productID = dealerTableId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String dealerId) {
        this.productName = dealerId;
    }

    public String getCartonSize() {
        return cartonSize;
    }

    public void setCartonSize(String cartonSize) {
        this.cartonSize = cartonSize;
    }

    public String getSoldQauntity() {
        return soldQauntity;
    }

    public void setSoldQauntity(String soldQauntity) {
        this.soldQauntity = soldQauntity;
    }

    public String getQuantityInstock() {
        return quantityInstock;
    }

    public void setQuantityInstock(String quantityInstock) {
        this.quantityInstock = quantityInstock;
    }

    public String getOrderQauntity() {
        return orderQauntity;
    }

    public void setOrderQauntity(String orderQauntity) {
        this.orderQauntity = orderQauntity;
    }

    public String getPurchPrice() {
        return purchPrice;
    }

    public void setPurchPrice(String purchPrice) {
        this.purchPrice = purchPrice;
    }

    public String getNetAmount() {
        return netAmount;
    }

    public void setNetAmount(String netAmount) {
        this.netAmount = netAmount;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public ArrayList<ArrayList<String>> getOrder_Edit(Statement stmt, Connection con, String ordersID)
    {
        ArrayList<ArrayList<String>> orderData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT  `product_info`.product_id, product_info.product_name, product_info.carton_size,purchase_order_detail.quantity_sold,products_stock.in_stock,purchase_order_detail.quantity_ordered,product_info.purchase_price  FROM `purchase_order_detail`   left JOIN product_info on product_info.product_table_id = `purchase_order_detail`.product_table_id left JOIN products_stock ON products_stock.product_table_id = purchase_order_detail.product_table_id where purchase_order_detail.purch_order_id = '"+ordersID+"'   ");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1));//product id
                temp.add(rs.getString(2));//product name
                temp.add(rs.getString(3));//carton size
                String sold_quantity = rs.getString(4);//sold quantity
                temp.add(sold_quantity);
                String quant_inStock = rs.getString(5);//in stock
                if(quant_inStock == null)
                {
                    quant_inStock = "0";
                }
                temp.add(quant_inStock);
                String quant_ordered = rs.getString(6);//quantity ordered
                if(quant_ordered == null)
                {
                    quant_ordered = "0";
                }
                temp.add(quant_ordered);
                temp.add(rs.getString(7));//purchase price
                float purchaseprice = Float.parseFloat(rs.getString(7));
                int quantity_order = Integer.parseInt(quant_ordered);
                float netamounts = purchaseprice * quantity_order;
                temp.add(String.valueOf(netamounts));
                /*int soldQuant = Integer.parseInt(rs.getString(4));
                int inventoryQuantity = soldQuant * inventoryDays;
                int instock_quant = Integer.parseInt(rs.getString(5));
                int purch_orderQuantity = inventoryQuantity - instock_quant;
                int cartonSize = Integer.parseInt(rs.getString(3));
                if(cartonSize != 0) {
                    if (purch_orderQuantity < cartonSize) {
                        purch_orderQuantity = 0;
                    }
                    else
                    {
                        int divideValue = purch_orderQuantity/cartonSize;
                        purch_orderQuantity = divideValue * cartonSize;
                    }
                }
                String strngorder = String.valueOf(purch_orderQuantity);
                temp.add(strngorder);//quantity ordered
                temp.add(rs.getString(6));//purchase price
                float price = Float.parseFloat(rs.getString(6));
                float netAmounts = price * purch_orderQuantity;
                String strngNetAmount = String.valueOf(netAmounts);
                temp.add(strngNetAmount);//net amount*/
                orderData.add(temp);
            }

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return orderData;
    }

    public ArrayList<ArrayList<String>> getOrderData(Statement stmt, Connection con, String startingDate,String endingDate,int int_compID,int inventoryDays)
    {
        ArrayList<ArrayList<String>> orderData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT `product_info`.`product_id`,product_info.product_name,product_info.carton_size, sum(`order_info_detailed`.`quantity`) as totalSold,products_stock.in_stock,product_info.purchase_price FROM `product_info` LEFT JOIN products_stock on products_stock.product_table_id = product_info.product_table_id LEFT OUTER JOIN `order_info_detailed` ON `order_info_detailed`.`product_table_id` = `product_info`.`product_table_id` LEFT OUTER JOIN `order_info` ON `order_info`.`order_id` = `order_info_detailed`.`order_id` AND str_to_date(`order_info`.`booking_date`, '%d/%b/%Y') >= str_to_date('"+startingDate+"', '%d/%b/%Y') and str_to_date(`order_info`.`booking_date`, '%d/%b/%Y') <= str_to_date('"+endingDate+"', '%d/%b/%Y') where product_info.company_table_id = '"+int_compID+"' GROUP BY `product_info`.`product_id`");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1));//product id
                temp.add(rs.getString(2));//product name
                temp.add(rs.getString(3));//carton size
                String soldQuantity = rs.getString(4);//sold quantity
                if(soldQuantity == null)
                {
                    soldQuantity = "0";
                }
                temp.add(soldQuantity);
                String instockQuant = rs.getString(5);//in stock
                if(instockQuant==null)
                {
                    instockQuant = "0";
                }
                temp.add(instockQuant);

                int soldQuant = Integer.parseInt(soldQuantity);
                int inventoryQuantity = soldQuant * inventoryDays;
                int intstock_quant = Integer.parseInt(instockQuant);
                int purch_orderQuantity = inventoryQuantity - intstock_quant;
                int cartonSize = Integer.parseInt(rs.getString(3));
                if(cartonSize != 0) {
                    if (purch_orderQuantity < cartonSize) {
                        purch_orderQuantity = 0;
                    }
                    else
                    {
                        int divideValue = purch_orderQuantity/cartonSize;
                        purch_orderQuantity = divideValue * cartonSize;
                    }
                    if(instockQuant.equals("0"))
                    {
                        purch_orderQuantity =  cartonSize;
                    }
                }

                String strngorder = String.valueOf(purch_orderQuantity);
                temp.add(strngorder);//quantity ordered
                temp.add(rs.getString(6));//purchase price
                float price = Float.parseFloat(rs.getString(6));
                float netAmounts = price * purch_orderQuantity;
                String strngNetAmount = String.valueOf(netAmounts);
                temp.add(strngNetAmount);//net amount
                orderData.add(temp);
            }
            if(orderData.isEmpty())
            {
                ResultSet result = null;
                try {
                    result = stmt.executeQuery("SELECT  `product_info`.product_id, product_info.product_name, product_info.carton_size, products_stock.in_stock,product_info.purchase_price  FROM   product_info INNER JOIN products_stock ON products_stock.product_table_id = product_info.product_table_id where  product_info.company_table_id = '" + int_compID + "'    ");
                    while (result.next()) {
                        temp = new ArrayList<String>();
                        temp.add(result.getString(1));//product id
                        temp.add(result.getString(2));//product name
                        temp.add(result.getString(3));//carton size
                        temp.add("0");//sold quantity
                        String in_stock = result.getString(4);//in stock
                        if(in_stock == null)
                        {
                            in_stock = "0";
                        }
                        int int_valueInstock = Integer.parseInt(in_stock);
                        float netAmounts = 0.0f;
                        if(int_valueInstock == 0)
                        {
                            temp.add(result.getString(3));
                        }
                        else {
                            temp.add("0");//ordered quantity
                        }
                        int intValue_cartonsize = Integer.parseInt(result.getString(3));

                        temp.add(result.getString(5));//purchase price
                        float price = Float.parseFloat(result.getString(5));

                        if(int_valueInstock == 0)
                        {
                            netAmounts =  intValue_cartonsize * price;
                        }
                        else
                        {
                            netAmounts = price * 0;
                        }
                        String strngNetAmount = String.valueOf(netAmounts);
                        temp.add(strngNetAmount);//net amount
                        orderData.add(temp);
                    }
                }
                catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return orderData;
    }

    public ArrayList<String> getpurchOrder_edit(Statement stmt, Connection con, String orderID)
    {
        ArrayList<String> temp= new ArrayList<String>();
        ResultSet rs = null;
        String searchQuery;
        int multipleSearch = 0;
        searchQuery = "SELECT `purchase_order_info`.`purch_order_amount`, `purchase_order_info`.`numb_of_prod`,`purchase_order_info`.`inventoryDays`, `company_info`.`company_name`, `purchase_order_info`.`order_status` FROM `purchase_order_info` INNER JOIN `company_info` ON `company_info`.`company_table_id` = `purchase_order_info`.`comp_table_id`  WHERE `purchase_order_info`.`purch_order_id` = '"+orderID+"'";

        try {
            rs = stmt.executeQuery(searchQuery);
            while (rs.next())
            {
                temp.add(rs.getString(1)); // order amount
                temp.add(rs.getString(2)); // numb of prod
                temp.add(rs.getString(3)); // inventory days
                temp.add(rs.getString(4)); // company name
                temp.add(rs.getString(5)); // status
            }

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return temp;
    }
    String compName = "";
    public String getcompName(Statement stmt, Connection con)
    {
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT company_name FROM `company_info` WHERE `company` != 'Deleted'");
            while (rs.next())
            {
                compName = rs.getString(1);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return compName;
    }

    String lastPurchOrderID = "";
    public  String getcurrentOrderID(Statement stmt2, Connection con2)
    {
        ResultSet rs = null;
        try {
            rs = stmt2.executeQuery("select purch_order_id from purchase_order_info ");
            int i = 0;
            while (rs.next())
            {
                lastPurchOrderID = rs.getString(1);
            }

            con2.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lastPurchOrderID;
    }

    String selected_companyID = "";
    public  String getTablecompanyID(Statement stmt2, Connection con2,String companyName)
    {
        ResultSet rs = null;
        try {
            rs = stmt2.executeQuery("select company_table_id from company_info where company_name = '"+companyName+"' ");
            int i = 0;
            while (rs.next())
            {
                selected_companyID = rs.getString(1);
            }

            con2.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return selected_companyID;
    }

    public void insertPurchOrderInfo(Statement stmt, Connection con, String order_id, String orderDate,String  orderAmount,String numbOFProd,String inventory_days, String compTableID,String order_status,String creatingUser,String creatingTime)
    {
        try {
            String insertQuery = "INSERT INTO `purchase_order_info` (`purch_order_id`, `purch_order_date`, `purch_order_amount`,`numb_of_prod`,`inventoryDays`, `comp_table_id`, `order_status`,`creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`) VALUES ('"+order_id+"','"+orderDate+"','"+orderAmount+"','"+numbOFProd+"','"+inventory_days+"','"+compTableID+"','"+order_status+"','"+creatingUser+"','"+orderDate+"','"+creatingTime+"','"+creatingUser+"','"+orderDate+"','"+creatingTime+"')";
            stmt.executeUpdate(insertQuery);
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void updatePurchOrderInfo(Statement stmt, Connection con, String order_id, String orderDate,String  orderAmount,String numbOFProd,String inventory_days, String compTableID,String order_status,String creatingUser,String creatingTime)
    {
        try {
            String insertQuery = "Update  `purchase_order_info` set   `purch_order_amount` = '"+orderAmount+"', `numb_of_prod` = '"+numbOFProd+"' ,`inventoryDays` = '"+inventory_days+"' , `comp_table_id` =  '"+compTableID+"',  `order_status` = '"+order_status+"',  `update_user_id` = '"+creatingUser+"',  `update_date` = '"+orderDate+"',  `update_time` = '"+creatingTime+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END where `purch_order_id` = '"+order_id+"' and str_to_date(`purch_order_date`, '%d/%b/%Y') = str_to_date('"+orderDate+"', '%d/%b/%Y')";
            stmt.executeUpdate(insertQuery);
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertPurchOrderDetails(Statement stmt, Connection con, String product_id, String quantitySold,String  quantInStock,String quantityOrdered,String purchOrderID, String netamount)
    {
        try {
            String insertQuery = "INSERT INTO `purchase_order_detail` (`product_table_id`, `quantity_sold`, `quantity_instock`,`quantity_ordered`,`purch_order_id`, `net_amount`) VALUES ('"+product_id+"','"+quantitySold+"','"+quantInStock+"','"+quantityOrdered+"','"+purchOrderID+"','"+netamount+"')";
            stmt.executeUpdate(insertQuery);
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void updatePurchOrderDetails(Statement stmt, Connection con, String product_id, String quantitySold,String  quantInStock,String quantityOrdered,String purchOrderID, String netamount)
    {
        try {
            String deleteQuery = "DELETE FROM purchase_order_detail WHERE purch_order_id ='"+purchOrderID+"')";
            stmt.executeUpdate(deleteQuery);
            String insertQuery = "INSERT INTO `purchase_order_detail` (`product_table_id`, `quantity_sold`, `quantity_instock`,`quantity_ordered`,`purch_order_id`, `net_amount`) VALUES ('"+product_id+"','"+quantitySold+"','"+quantInStock+"','"+quantityOrdered+"','"+purchOrderID+"','"+netamount+"')";
            stmt.executeUpdate(insertQuery);
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
