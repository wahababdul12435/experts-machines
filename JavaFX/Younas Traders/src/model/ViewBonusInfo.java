package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.JFXTextField;
import controller.DeleteWindow;
import controller.UpdateBonus;
import controller.UpdateCompany;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import javax.xml.soap.Text;
import java.awt.*;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;

public class ViewBonusInfo {


    private String srNo;
    private String approval_id;
    private String startDate;
    private String endDate;
    private String policy_status;
    private String entry_date;

    private HBox operationsPane;
    private JFXButton btnView;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    public static StackPane stackPane;
    public static JFXDialog dialog;
    public static Label lblUpdate;

    public static String viewApprovalId;

    GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");

    public ViewBonusInfo()
    {
        this.srNo = "";
        this.approval_id = "";
        this.startDate = "";
        this.entry_date = "";
        this.policy_status = "";
        this.entry_date = "";
        this.btnView = new JFXButton();
        this.btnEdit = new JFXButton();
        this.btnDelete = new JFXButton();
        this.btnView.setGraphic(GlobalVariables.createViewIcon());
        this.btnEdit.setGraphic(GlobalVariables.createEditIcon());
        this.btnDelete.setGraphic(GlobalVariables.createDeleteIcon());
        this.btnView.getStyleClass().add("btn_icon");
        this.btnEdit.getStyleClass().add("btn_icon");
        this.btnDelete.getStyleClass().add("btn_icon");
        this.operationsPane = new HBox(btnView, btnEdit, btnDelete);
        lblUpdate = new Label("");
    }
    public ViewBonusInfo(String srNo, String approval_id, String startDate, String endDate, String policy_status, String entry_date) {
        this.srNo = srNo;
        this.approval_id = approval_id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.policy_status = policy_status;
        this.entry_date = entry_date;

        this.btnView = new JFXButton();
        this.btnEdit = new JFXButton();
        this.btnDelete = new JFXButton();
        this.btnView.setGraphic(GlobalVariables.createViewIcon());
        this.btnEdit.setGraphic(GlobalVariables.createEditIcon());
        this.btnDelete.setGraphic(GlobalVariables.createDeleteIcon());
        this.btnView.getStyleClass().add("btn_icon");
        this.btnEdit.getStyleClass().add("btn_icon");
        this.btnDelete.getStyleClass().add("btn_icon");
        this.operationsPane = new HBox(btnView, btnEdit, btnDelete);
        lblUpdate = new Label("");

        this.btnView.setOnAction((action)->viewClicked());
        this.btnEdit.setOnAction((action)->editClicked());
        this.btnDelete.setOnAction(event -> {
            try {
                deleteBonus(this.approval_id);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getApproval_id() {
        return approval_id;
    }

    public void setApproval_id(String approval_id) {
        this.approval_id = approval_id;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getPolicy_status() {
        return policy_status;
    }

    public void setPolicy_status(String policy_status) {
        this.policy_status = policy_status;
    }

    public String getEntry_date() {
        return entry_date;
    }

    public void setEntry_date(String entry_date) {
        this.entry_date = entry_date;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnView() {
        return btnView;
    }

    public void setBtnView(JFXButton btnView) {
        this.btnView = btnView;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public static StackPane getStackPane() {
        return stackPane;
    }

    public static void setStackPane(StackPane stackPane) {
        ViewBonusInfo.stackPane = stackPane;
    }

    public static JFXDialog getDialog() {
        return dialog;
    }

    public static void setDialog(JFXDialog dialog) {
        ViewBonusInfo.dialog = dialog;
    }

    public static Label getLblUpdate() {
        return lblUpdate;
    }

    public static void setLblUpdate(Label lblUpdate) {
        ViewBonusInfo.lblUpdate = lblUpdate;
    }

    public static String getViewApprovalId() {
        return viewApprovalId;
    }

    public static void setViewApprovalId(String viewApprovalId) {
        ViewBonusInfo.viewApprovalId = viewApprovalId;
    }

    public GlyphFont getFontAwesome() {
        return fontAwesome;
    }

    public void setFontAwesome(GlyphFont fontAwesome) {
        this.fontAwesome = fontAwesome;
    }

    public ArrayList<ArrayList<String>> getBonusInfo(Statement stmt, Connection con)
    {
        ArrayList<ArrayList<String>> bonusPolicyData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT approval_id, start_date,end_date,policy_status,entry_date  FROM `bonus_policy` WHERE policy_status != 'Deleted' ORDER BY bonus_policyID");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2));
                temp.add(rs.getString(3));
                temp.add(rs.getString(4));
                temp.add(rs.getString(5));
                bonusPolicyData.add(temp);
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return bonusPolicyData;
    }


    public ArrayList<ArrayList<String>> getBonusSearch(Statement stmt, Connection con, String approvalId, String startDate, String endDate, String policyStatus, String entryDate)
    {
        ArrayList<ArrayList<String>> bonusData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        String searchQuery;
        int multipleSearch = 0;
        searchQuery = "SELECT  `approval_id`, `start_date`, `end_date`, `policy_status`, `entry_date`  FROM `bonus_policy` WHERE ";
        if(!approvalId.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`approval_id` = '"+approvalId+"'";
            multipleSearch++;
        }
        if(startDate!=null)
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`start_date` = '"+startDate+"'";
            multipleSearch++;
        }
        if(endDate!=null)
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`end_date` = '"+endDate+"'";
            multipleSearch++;
        }
        if(entryDate!=null)
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`entry_date` = '"+entryDate+"'";
            multipleSearch++;
        }



        if(multipleSearch >= 1)
        {
            searchQuery += " AND ";
        }
        searchQuery += "`policy_status` != 'Deleted' ORDER BY `bonus_policyID`";
        try {
            rs = stmt.executeQuery(searchQuery);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2));
                temp.add(rs.getString(3));
                temp.add(rs.getString(4));
                temp.add(rs.getString(5));
                bonusData.add(temp);
            }

//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return bonusData;
    }

    private Node createViewIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.EYE);
        graphic.setFontSize(20.0);
        return graphic;
    }

    private Node createEditIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.EDIT);
        graphic.setFontSize(20.0);
        return graphic;
    }

    private Node createDeleteIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.TRASH);
        graphic.setFontSize(20.0);
        return graphic;
    }

    public void deleteBonus(String BonusTableId) throws IOException
    {
        DeleteWindow.sceneWindow = "ViewBonus";
        DeleteWindow.deletionId = BonusTableId;
        Parent parent = null;
        try {
            parent = FXMLLoader.load(getClass().getResource("/view/delete_window.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setStyle("-fx-padding: -40 -20 -40 -20;");
        dialogLayout.setBody(parent);
        dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        stackPane.setVisible(true);
        dialog.show();
        dialog.setOverlayClose(false);
    }

    String bonuspolicy_details = "";
    public String get_bonuspolicy(Statement stmt, Connection con, int approval_id )
    {
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("select bonus_policy.approval_id,bonus_policy.quantity,bonus_policy.bonus_quant,bonus_policy.start_date,bonus_policy.end_date,bonus_policy.policy_status,dealer_info.dealer_id,dealer_info.dealer_name ,company_info.company_id,company_info.company_name ,product_info.product_id,product_info.product_name from bonus_policy inner join dealer_info on bonus_policy.dealer_table_id = dealer_info.dealer_table_id inner JOIN company_info on  bonus_policy.company_table_id = company_info.company_table_id  inner JOIN product_info on  bonus_policy.product_table_id = product_info.product_table_id  where approval_id ='"+approval_id+"'  and bonus_policy.policy_status = 'Active' or bonus_policy.policy_status = 'In Active'  ");
            int i = 0;
            while (rs.next())
            {

                bonuspolicy_details = rs.getString(1) + "--" + rs.getString(2) + "--" + rs.getString(3) + "--" + rs.getString(4) + "--" + rs.getString(5) + "--" + rs.getString(6) + "--" + rs.getString(7) + "--" + rs.getString(8)+ "--" + rs.getString(9) + "--" + rs.getString(10) + "--" + rs.getString(11)+ "--" + rs.getString(12);
            }

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return bonuspolicy_details;

    }


    public void viewClicked()
    {
        Parent parent = null;
        viewApprovalId = this.approval_id;
        try {
            parent = FXMLLoader.load(getClass().getResource("/view/bonusdetails.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setStyle("-fx-padding: -20 -10 -20 -10;");
        dialogLayout.setBody(parent);
        dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        stackPane.setVisible(true);
        dialog.show();
        dialog.setOverlayClose(false);
    }

    public void updatebonusdetails()
    {

    }
    String companydetails = "";
    public String companyDetailswithIDs(Statement stmt, Connection con, int compan_Ids )
    {
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("select company_name from company_info where company_id ='"+compan_Ids+"'  and company_status = 'Active' ");
            int i = 0;
            while (rs.next() )
            {

                companydetails = rs.getString(1) ;
            }

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return companydetails;

    }
    String deleardetails = "";
    public String get_dealerDetailswithIDs(Statement stmt, Connection con, int dealer_Ids )
    {
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("select dealer_name from dealer_info where dealer_id ='"+dealer_Ids+"'  and dealer_status = 'Active' ");
            int i = 0;
            while (rs.next() )
            {

                deleardetails = rs.getString(1) ;
            }

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return deleardetails;

    }
    String productsDetails = "";
    public String get_productsDetailswithIDs(Statement stmt, Connection con, int product_Ids )
    {
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("select product_name from product_info where product_id ='"+product_Ids+"'  and product_status = 'Active' ");
            int i = 0;
            while (rs.next())
            {

                productsDetails = rs.getString(1) ;
            }

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return productsDetails;

    }
    public void editClicked()
    {
        Parent parent = null;
        try {
            UpdateBonus.bonusapprovalID = this.approval_id;
            parent = FXMLLoader.load(getClass().getResource("/view/update_bonus.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setStyle("-fx-padding: -20 -10 -20 -10;");
        dialogLayout.setHeading(lblUpdate);
        dialogLayout.setBody(parent);
        dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        stackPane.setVisible(true);
        dialog.show();
        dialog.setOverlayClose(false);
    }
}
