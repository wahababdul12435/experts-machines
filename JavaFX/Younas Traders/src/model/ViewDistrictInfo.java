package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import controller.DeleteWindow;
import controller.UpdateCompany;
import controller.UpdateDistrict;
import controller.ViewDistrict;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ViewDistrictInfo {
    private String srNo;
    private String districtTableId;
    private String districtId;
    private String districtName;
    private String cityIds;
    private String cityNames;
    private String districtStatus;

    private HBox operationsPane;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    public static StackPane stackPane;
    public static JFXDialog dialog;
    public static Label lblUpdate;

    public ViewDistrictInfo() {
        srNo = "";
        districtTableId = "";
        districtId = "";
        districtName = "";
        cityIds = "";
        cityNames = "";
        districtStatus = "";
        this.btnEdit = new JFXButton("Edit");
        this.btnDelete = new JFXButton("Delete");
        this.operationsPane = new HBox(btnEdit, btnDelete);
    }

    public ViewDistrictInfo(String srNo, String districtTableId, String districtId, String districtName, String cityIds, String cityNames, String districtStatus) {
        this.srNo = srNo;
        this.districtTableId = districtTableId;
        this.districtId = districtId;
        this.districtName = districtName;
        this.cityIds = cityIds;
        this.cityNames = cityNames;
        this.districtStatus = districtStatus;

        this.btnEdit = new JFXButton();
        this.btnDelete = new JFXButton();
//        this.btnView.setGraphic(GlobalVariables.createViewIcon());

        this.operationsPane = new HBox();

//        this.btnView.getStyleClass().add("btn_icon");
//        this.btnView.setOnAction((action)->viewClicked());
//        this.operationsPane.getChildren().add(this.btnView);

        if(GlobalVariables.rightEdit)
        {
            this.btnEdit.setGraphic(GlobalVariables.createEditIcon());
            this.btnEdit.getStyleClass().add("btn_icon");
            this.btnEdit.setOnAction((action)->editClicked());
            this.operationsPane.getChildren().add(this.btnEdit);
        }
        if(GlobalVariables.rightDelete)
        {
            this.btnDelete.setGraphic(GlobalVariables.createDeleteIcon());
            this.btnDelete.getStyleClass().add("btn_icon");
            this.btnDelete.setOnAction(event -> {
                try {
                    deleteDistrict(this.districtTableId);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            this.operationsPane.getChildren().add(this.btnDelete);
        }

    }

    public void editClicked()
    {
        Parent parent = null;
        try {
            UpdateDistrict.districtTableId = districtTableId;
            UpdateDistrict.districtId = districtId;
            UpdateDistrict.districtName = districtName;
            UpdateDistrict.districtStatus = districtStatus;
            parent = FXMLLoader.load(getClass().getResource("/view/update_district.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setBody(parent);
        dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        stackPane.setVisible(true);
        dialog.show();
        dialog.setOverlayClose(false);
    }

    public void deleteDistrict(String districtTableId) throws IOException {
        DeleteWindow.sceneWindow = "ViewDistrict";
        DeleteWindow.deletionId = districtTableId;
        Parent parent = null;
        try {
            parent = FXMLLoader.load(getClass().getResource("/view/delete_window.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setStyle("-fx-padding: -40 -20 -40 -20;");
        dialogLayout.setBody(parent);
        dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        stackPane.setVisible(true);
        dialog.show();
        dialog.setOverlayClose(false);
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getDistrictTableId() {
        return districtTableId;
    }

    public void setDistrictTableId(String districtTableId) {
        this.districtTableId = districtTableId;
    }

    public String getDistrictId() {
        return districtId;
    }

    public void setDistrictId(String districtId) {
        this.districtId = districtId;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public String getCityIds() {
        return cityIds;
    }

    public void setCityIds(String cityIds) {
        this.cityIds = cityIds;
    }

    public String getCityNames() {
        return cityNames;
    }

    public void setCityNames(String cityNames) {
        this.cityNames = cityNames;
    }

    public String getDistrictStatus() {
        return districtStatus;
    }

    public void setDistrictStatus(String districtStatus) {
        this.districtStatus = districtStatus;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public static StackPane getStackPane() {
        return stackPane;
    }

    public static void setStackPane(StackPane stackPane) {
        ViewDistrictInfo.stackPane = stackPane;
    }

    public static JFXDialog getDialog() {
        return dialog;
    }

    public static void setDialog(JFXDialog dialog) {
        ViewDistrictInfo.dialog = dialog;
    }

    public static Label getLblUpdate() {
        return lblUpdate;
    }

    public static void setLblUpdate(Label lblUpdate) {
        ViewDistrictInfo.lblUpdate = lblUpdate;
    }

    public ArrayList<ArrayList<String>> getDistrictsInfo(Statement stmt, Connection con)
    {
        ArrayList<ArrayList<String>> districtData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        temp = new ArrayList<String>();
        ResultSet rs = null;
        String preDistrictId = "";
        String cityId  = "";
        String cityName  = "";
        boolean iter = false;
        try {
            rs = stmt.executeQuery("SELECT `district_info`.`district_table_id`, `district_info`.`district_id`, `district_info`.`district_name`, `district_info`.`district_status`, `city_info`.`city_table_id`, `city_info`.`city_name` FROM `district_info` LEFT OUTER JOIN `city_info` ON `district_info`.`district_table_id` = `city_info`.`district_table_id` WHERE `district_info`.`district_status` != 'Deleted' ORDER BY `district_info`.`district_table_id`");
            while (rs.next())
            {
                if(preDistrictId.equals(rs.getString(1)))
                {
                    cityId += "\n"+rs.getString(5);
                    cityName += "\n"+rs.getString(6);
                }
                else
                {
                    if(iter)
                    {
                        temp.set(3, cityId);
                        temp.set(4, cityName);
                        districtData.add(temp);
                        cityId = rs.getString(5);
                        cityName = rs.getString(6);
                        temp = new ArrayList<String>();
                        preDistrictId = rs.getString(1);
                    }
                    else
                    {
                        iter = true;
                        cityId = rs.getString(5);
                        cityName = rs.getString(6);
                        temp = new ArrayList<>();
                        preDistrictId = rs.getString(1);
                    }
                    temp.add(rs.getString(1)); // Table Id
                    temp.add(rs.getString(2)); // Id
                    temp.add(rs.getString(3)); // Name
                    temp.add(rs.getString(5)); // City Id
                    temp.add(rs.getString(6)); // City Name
                    temp.add(rs.getString(4)); // Status
                }
            }
            if(iter)
            {
                temp.set(3, cityId);
                temp.set(4, cityName);
                districtData.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return districtData;
    }

    public ArrayList<ArrayList<String>> getDistrictsSearch(Statement stmt, Connection con, String districtId, String districtName, String districtStatus)
    {
        ArrayList<ArrayList<String>> districtData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        temp = new ArrayList<String>();
        ResultSet rs = null;
        String preDistrictId = "";
        String cityId  = "";
        String cityName  = "";
        boolean iter = false;
        String searchQuery;
        int multipleSearch = 0;
        searchQuery = "SELECT `district_info`.`district_table_id`, `district_info`.`district_id`, `district_info`.`district_name`, `district_info`.`district_status`, `city_info`.`city_table_id`, `city_info`.`city_name` FROM `district_info` LEFT OUTER JOIN `city_info` ON `district_info`.`district_table_id` = `city_info`.`district_table_id` WHERE ";
        if(!districtId.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`district_info`.`district_id` = '"+districtId+"'";
            multipleSearch++;
        }
        if(!districtName.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`district_info`.`district_name` = '"+districtName+"'";
            multipleSearch++;
        }
        if(!districtStatus.equals("All"))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`district_info`.`district_status` = '"+districtStatus+"'";
            multipleSearch++;
        }
        if(multipleSearch >= 1)
        {
            searchQuery += " AND ";
        }
        searchQuery += "`district_info`.`district_status` != 'Deleted' ORDER BY `district_info`.`district_table_id`";
        try {
            rs = stmt.executeQuery(searchQuery);
            while (rs.next())
            {
                if(preDistrictId.equals(rs.getString(1)))
                {
                    cityId += "\n"+rs.getString(5);
                    cityName += "\n"+rs.getString(6);
                }
                else
                {
                    if(iter)
                    {
                        temp.set(3, cityId);
                        temp.set(4, cityName);
                        districtData.add(temp);
                        cityId = rs.getString(5);
                        cityName = rs.getString(6);
                        temp = new ArrayList<String>();
                        preDistrictId = rs.getString(1);
                    }
                    else
                    {
                        iter = true;
                        cityId = rs.getString(5);
                        cityName = rs.getString(6);
                        temp = new ArrayList<>();
                        preDistrictId = rs.getString(1);
                    }
                    temp.add(rs.getString(1)); // Table Id
                    temp.add(rs.getString(2)); // Id
                    temp.add(rs.getString(3)); // Name
                    temp.add(rs.getString(5)); // City Id
                    temp.add(rs.getString(6)); // City Name
                    temp.add(rs.getString(4)); // Status
                }
            }
            if(iter)
            {
                temp.set(3, cityId);
                temp.set(4, cityName);
                districtData.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return districtData;
    }

    public ArrayList<String> getDistrictsSummary(Statement stmt, Connection con)
    {
        ArrayList<String> groupData = new ArrayList<String>();
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT COUNT(`district_table_id`) FROM `district_info` WHERE `district_status` != 'Deleted'");
            while (rs.next())
            {
                groupData.add(rs.getString(1));
            }
            rs = stmt.executeQuery("SELECT COUNT(`district_table_id`) FROM `district_info` WHERE `district_status` = 'Active'");
            while (rs.next())
            {
                groupData.add(rs.getString(1));
            }
            rs = stmt.executeQuery("SELECT COUNT(`district_table_id`) FROM `district_info` WHERE `district_status` = 'In Active'");
            while (rs.next())
            {
                groupData.add(rs.getString(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return groupData;
    }

    private Node createErrorIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.EDIT);
        graphic.setFontSize(20.0);
        return graphic;
    }

    private Node createDeleteIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.TRASH);
        graphic.setFontSize(20.0);
        return graphic;
    }
}
