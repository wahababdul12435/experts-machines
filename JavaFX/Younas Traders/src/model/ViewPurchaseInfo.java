package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import controller.DeleteWindow;
import controller.UpdateComment;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ViewPurchaseInfo {
    private String srNo;
    private String purchaseId;
    private String purchaseDate;
    private String purchaseDay;
    private String purchaseCompany;
    private String invoiceNo;
    private String purchasedItems;
    private String bonusItems;
    private String grossAmount;
    private String discount;
    private String netAmount;
    private String supplier;
    private String comment;

    private HBox operationsPane;
    private JFXButton btnComment;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    SimpleDateFormat fmt = new SimpleDateFormat("dd/MMM/yyyy");
    public static String orderId;

    public static StackPane stackPane;
    public static JFXDialog dialog;

    public ViewPurchaseInfo() {
    }

    public ViewPurchaseInfo(String srNo, String purchaseId, String purchaseDate, String purchaseDay, String purchaseCompany, String invoiceNo, String purchasedItems, String bonusItems, String grossAmount, String discount, String netAmount, String supplier, String comment) {
        this.srNo = srNo;
        this.purchaseId = purchaseId;
        this.purchaseDate = purchaseDate;
        this.purchaseDay = purchaseDay;
        this.purchaseCompany = purchaseCompany;
        this.invoiceNo = invoiceNo;
        this.purchasedItems = purchasedItems;
        this.bonusItems = bonusItems;
        this.grossAmount = grossAmount;
        this.discount = discount;
        this.netAmount = netAmount;
        this.supplier = supplier;
        this.comment = comment;

        this.btnComment = new JFXButton();
        this.btnEdit = new JFXButton();
        this.btnDelete = new JFXButton();
        this.btnComment.setGraphic(GlobalVariables.createCommentIcon());
        this.btnEdit.setGraphic(GlobalVariables.createEditIcon());
        this.btnDelete.setGraphic(GlobalVariables.createDeleteIcon());
        this.btnComment.getStyleClass().add("btn_icon");
        this.btnComment.setOnAction((action)->commentClicked());
        this.btnEdit.setGraphic(GlobalVariables.createEditIcon());
        this.btnEdit.getStyleClass().add("btn_icon");
        this.btnEdit.setOnAction((action)->editClicked());
//        this.operationsPane.getChildren().add(this.btnEdit);
        this.btnDelete.setGraphic(GlobalVariables.createDeleteIcon());
        this.btnDelete.getStyleClass().add("btn_icon");
        this.btnDelete.setOnAction(event -> {
            try {
                deletePurchase(this.purchaseId);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        this.operationsPane = new HBox(this.btnComment, this.btnEdit, this.btnDelete);
    }

    private void commentClicked()
    {
        Parent parent = null;
        try {
            UpdateComment.commentType = "Purchase";
            UpdateComment.orderId = this.purchaseId;
            UpdateComment.comment = this.comment;
            parent = FXMLLoader.load(getClass().getResource("/view/update_comment.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setStyle("-fx-padding: -20 -10 -20 -10;");
        dialogLayout.setBody(parent);
        dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        stackPane.setVisible(true);
        dialog.show();
        dialog.setOverlayClose(false);
    }

    private void editClicked()
    {
        orderId = this.purchaseId;
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/view_purchase_detail.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    public void deletePurchase(String purchaseId) throws IOException {
        DeleteWindow.sceneWindow = "ViewPurchase";
        DeleteWindow.deletionId = purchaseId;
        Parent parent = null;
        try {
            parent = FXMLLoader.load(getClass().getResource("/view/delete_window.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setStyle("-fx-padding: -40 -20 -40 -20;");
        dialogLayout.setBody(parent);
        dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        stackPane.setVisible(true);
        dialog.show();
        dialog.setOverlayClose(false);
    }


    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getPurchaseId() {
        return purchaseId;
    }

    public void setPurchaseId(String purchaseId) {
        this.purchaseId = purchaseId;
    }

    public String getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(String purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public String getPurchaseDay() {
        return purchaseDay;
    }

    public void setPurchaseDay(String purchaseDay) {
        this.purchaseDay = purchaseDay;
    }

    public String getPurchaseCompany() {
        return purchaseCompany;
    }

    public void setPurchaseCompany(String purchaseCompany) {
        this.purchaseCompany = purchaseCompany;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getPurchasedItems() {
        return purchasedItems;
    }

    public void setPurchasedItems(String purchasedItems) {
        this.purchasedItems = purchasedItems;
    }

    public String getBonusItems() {
        return bonusItems;
    }

    public void setBonusItems(String bonusItems) {
        this.bonusItems = bonusItems;
    }

    public String getGrossAmount() {
        return grossAmount;
    }

    public void setGrossAmount(String grossAmount) {
        this.grossAmount = grossAmount;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getNetAmount() {
        return netAmount;
    }

    public void setNetAmount(String netAmount) {
        this.netAmount = netAmount;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public ArrayList<ArrayList<String>> getPurchaseInfo(Statement stmt, Connection con, String fromDate, String toDate)
    {
        ArrayList<ArrayList<String>> purchaseData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        int day;
        String stDay = "";
        try {
            rs = stmt.executeQuery("SELECT `purchase_info`.`purchase_id`, `purchase_info`.`purchase_date`, `company_info`.`company_name`, `purchase_info`.`invoice_num`, SUM(`purchase_info_detail`.`recieve_quant`), SUM(`purchase_info_detail`.`bonus_quant`), `purchase_info`.`gross_amount`, `purchase_info`.`disc_amount`, `purchase_info`.`net_amount`, `supplier_info`.`supplier_name`, `purchase_info`.`status`, `purchase_info`.`comments` FROM `purchase_info` LEFT OUTER JOIN `company_info` ON `company_info`.`company_table_id` = `purchase_info`.`comp_id` LEFT OUTER JOIN `purchase_info_detail` ON `purchase_info_detail`.`purchase_id` = `purchase_info`.`purchase_id` LEFT OUTER JOIN `supplier_info` ON `supplier_info`.`supplier_table_id` = `purchase_info`.`supplier_id` WHERE `purchase_info`.`status` != 'Deleted' GROUP BY `purchase_info`.`purchase_id` ORDER BY str_to_date(`purchase_info`.`purchase_date`, '%d/%b/%Y') DESC");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Purchase Id
                temp.add(rs.getString(2)); // Purchase Date
                Date objDate = fmt.parse(rs.getString(2));
                day = objDate.getDay();
                if(day == 0)
                    stDay = "Sunday";
                else if(day == 1)
                    stDay = "Monday";
                else if(day == 2)
                    stDay = "Tuesday";
                else if(day == 3)
                    stDay = "Wednesday";
                else if(day == 4)
                    stDay = "Thursday";
                else if(day == 5)
                    stDay = "Friday";
                else if(day == 6)
                    stDay = "Saturday";
                else
                    stDay = "N/A";
                temp.add(stDay); // Purchase Day
                temp.add(rs.getString(3)); // Company Name
                temp.add(rs.getString(4)); // Invoice No
                temp.add(rs.getString(5) == null || rs.getString(5).equals("") ? "0" : rs.getString(5)); // Receive Quantity
                temp.add(rs.getString(6) == null || rs.getString(6).equals("") ? "0" : rs.getString(6)); // Bonus Quantity
                temp.add(rs.getString(7) == null || rs.getString(7).equals("") ? "0" : rs.getString(7)); // Gross Amount
                temp.add(rs.getString(8) == null || rs.getString(8).equals("") ? "0" : rs.getString(8)); // Discount
                temp.add(rs.getString(9) == null || rs.getString(9).equals("") ? "0" : rs.getString(9)); // Net Amount
                temp.add(rs.getString(10)); // Supplier Name
                temp.add(rs.getString(11)); // Status
                temp.add(rs.getString(12)); // comment
                purchaseData.add(temp);
            }
        } catch (SQLException | ParseException e) {
            e.printStackTrace();
        }
        return purchaseData;
    }

    public ArrayList<ArrayList<String>> getPurchaseInfoSearch(Statement stmt, Connection con, String fromDate, String toDate, String day, String invoiceStatus)
    {
        ArrayList<ArrayList<String>> purchaseData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        int intDay;
        String stDay = "";
        String query = "SELECT `purchase_info`.`purchase_id`, `purchase_info`.`purchase_date`, `company_info`.`company_name`, `purchase_info`.`invoice_num`, SUM(`purchase_info_detail`.`recieve_quant`), SUM(`purchase_info_detail`.`bonus_quant`), `purchase_info`.`gross_amount`, `purchase_info`.`disc_amount`, `purchase_info`.`net_amount`, `supplier_info`.`supplier_name`, `purchase_info`.`status`, `purchase_info`.`comments` FROM `purchase_info` LEFT OUTER JOIN `company_info` ON `company_info`.`company_table_id` = `purchase_info`.`comp_id` LEFT OUTER JOIN `purchase_info_detail` ON `purchase_info_detail`.`purchase_id` = `purchase_info`.`purchase_id` LEFT OUTER JOIN `supplier_info` ON `supplier_info`.`supplier_table_id` = `purchase_info`.`supplier_id` WHERE ";
        if(!fromDate.equals(""))
        {
            query += "str_to_date(`purchase_info`.`purchase_date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') AND ";
        }
        if(!toDate.equals(""))
        {
            query += "str_to_date(`purchase_info`.`purchase_date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y') AND ";
        }
        if(!invoiceStatus.equals("All"))
        {
            query += "`purchase_info`.`status` = '"+invoiceStatus+"' AND ";
        }
        query += "`purchase_info`.`status` != 'Deleted' GROUP BY `purchase_info`.`purchase_id` ORDER BY str_to_date(`purchase_info`.`purchase_date`, '%d/%b/%Y') DESC";
        try {
            rs = stmt.executeQuery(query);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Purchase Id
                temp.add(rs.getString(2)); // Purchase Date
                Date objDate = fmt.parse(rs.getString(2));
                intDay = objDate.getDay();
                if(intDay == 0)
                    stDay = "Sunday";
                else if(intDay == 1)
                    stDay = "Monday";
                else if(intDay == 2)
                    stDay = "Tuesday";
                else if(intDay == 3)
                    stDay = "Wednesday";
                else if(intDay == 4)
                    stDay = "Thursday";
                else if(intDay == 5)
                    stDay = "Friday";
                else if(intDay == 6)
                    stDay = "Saturday";
                else
                    stDay = "N/A";
                temp.add(stDay); // Purchase Day
                temp.add(rs.getString(3)); // Company Name
                temp.add(rs.getString(4)); // Invoice No
                temp.add(rs.getString(5) == null || rs.getString(5).equals("") ? "0" : rs.getString(5)); // Receive Quantity
                temp.add(rs.getString(6) == null || rs.getString(6).equals("") ? "0" : rs.getString(6)); // Bonus Quantity
                temp.add(rs.getString(7) == null || rs.getString(7).equals("") ? "0" : rs.getString(7)); // Gross Amount
                temp.add(rs.getString(8) == null || rs.getString(8).equals("") ? "0" : rs.getString(8)); // Discount
                temp.add(rs.getString(9) == null || rs.getString(9).equals("") ? "0" : rs.getString(9)); // Net Amount
                temp.add(rs.getString(10)); // Supplier Name
                temp.add(rs.getString(11)); // Status
                temp.add(rs.getString(12)); // comment
                if(!day.equals("All") && day.equals(stDay))
                {
                    purchaseData.add(temp);
                }
                else if(day.equals("All"))
                {
                    purchaseData.add(temp);
                }
            }
        } catch (SQLException | ParseException e) {
            e.printStackTrace();
        }
        return purchaseData;
    }

    private Node createEditIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.EDIT);
        graphic.setFontSize(20.0);
        return graphic;
    }

    private Node createDeleteIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.TRASH);
        graphic.setFontSize(20.0);
        return graphic;
    }
}
