package model;

import javafx.application.Application;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

import java.io.*;
import java.net.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.Date;

public class Test {

    public static void main(String[] args) {
        String testURL = "http://localhost/GitKraken/distribution/experts-machines/PHP/YounasTraders/Test3.php";
        String userName = "user 123";
        String password = "pass 123";
        String response = "";
        try {
            URL objurl = new URL(testURL);
            HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
            objhttpurlconnection.setConnectTimeout(5 * 1000);
            objhttpurlconnection.connect();
            if (objhttpurlconnection.getResponseCode() == 200)
            {
                objhttpurlconnection.disconnect();
                objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("user_name", "UTF-8") +"="+URLEncoder.encode(userName, "UTF-8")+"&"+
                        URLEncoder.encode("password", "UTF-8")+"="+URLEncoder.encode(password, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                String orderResponse;
                while ((orderResponse = bufferedReader.readLine()) != null)
                {
                    response += orderResponse;
                }
                objhttpurlconnection.disconnect();
            }
            else
            {
                response = "Unable to Connect To Server";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


//        try {
//            // open a connection to the site
//            URL url = new URL(testURL);
//            URLConnection con = url.openConnection();
//            // activate the output
//            con.setDoOutput(true);
//            PrintStream ps = new PrintStream(con.getOutputStream());
//            // send your parameters to your site
//            ps.print("&user_name="+userName);
//            ps.print("&password="+password);
//
//            // we have to get the input stream in order to actually send the request
//            con.getInputStream();
//
//            // close the print stream
//            ps.close();
//        } catch (MalformedURLException e1) {
//            e1.printStackTrace();
//        } catch (IOException e2) {
//            e2.printStackTrace();
//        }
    }
}