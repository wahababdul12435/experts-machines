package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import controller.DeleteWindow;
import controller.UpdateProduct;
import controller.UpdateUser;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;

public class ViewUsersInfo {
    private String srNo;
    private String userTableId;
    private String userId;
    private String userName;
    private String userType;
    private String userContact;
    private String userAddress;
    private String cnic;
    private String designatedAreas;
    private String designatedAreasId;
    private String userStatus;

    private HBox operationsPane;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    public static StackPane stackPane;
    public static JFXDialog dialog;
    public static Label lblUpdate;

    public ViewUsersInfo()
    {
        this.srNo = "";
        this.userTableId = "";
        this.userId = "";
        this.userName = "";
        this.userType = "";
        this.userContact = "";
        this.userAddress = "";
        this.cnic = "";
        this.designatedAreas = "";
        this.designatedAreasId = "";
        this.userStatus = "";
        this.btnEdit = new JFXButton("Edit");
        this.btnDelete = new JFXButton("Delete");
        this.operationsPane = new HBox(btnEdit, btnDelete);
    }

    public ViewUsersInfo(String srNo, String userTableId, String userId, String userName, String userType, String userContact, String userAddress, String cnic, String designatedAreasId, String designatedAreas, String userStatus) {
        this.srNo = srNo;
        this.userTableId = userTableId;
        this.userId = userId;
        this.userName = userName;
        this.userType = userType;
        this.userContact = userContact;
        this.userAddress = userAddress;
        this.cnic = cnic;
        this.designatedAreasId = designatedAreasId;
        this.designatedAreas = designatedAreas;
        this.userStatus = userStatus;

        this.btnEdit = new JFXButton();
        this.btnDelete = new JFXButton();
//        this.btnView.setGraphic(GlobalVariables.createViewIcon());

        this.operationsPane = new HBox();

//        this.btnView.getStyleClass().add("btn_icon");
//        this.btnView.setOnAction((action)->viewClicked());
//        this.operationsPane.getChildren().add(this.btnView);

        if(GlobalVariables.rightEdit)
        {
            this.btnEdit.setGraphic(GlobalVariables.createEditIcon());
            this.btnEdit.getStyleClass().add("btn_icon");
            this.btnEdit.setOnAction((action)->editClicked());
            this.operationsPane.getChildren().add(this.btnEdit);
        }
        if(GlobalVariables.rightDelete)
        {
            this.btnDelete.setGraphic(GlobalVariables.createDeleteIcon());
            this.btnDelete.getStyleClass().add("btn_icon");
            this.btnDelete.setOnAction(event -> {
                try {
                    deleteUser(this.userTableId);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            this.operationsPane.getChildren().add(this.btnDelete);
        }
    }

    public void editClicked()
    {
        Parent parent = null;
        try {
            UpdateUser.userTableId = userTableId;
            UpdateUser.userId = userId;
            UpdateUser.userName = userName;
            UpdateUser.userType = userType;
            UpdateUser.userContact = userContact;
            UpdateUser.userAddress = userAddress;
            UpdateUser.userCnic = cnic;
            UpdateUser.userStatus = userStatus;

            ArrayList<String> userAreas = new ArrayList<>();
            if(designatedAreas != null)
            {
                String[] arr = designatedAreas.split("\n");
                for (String areaData:arr) {
                    userAreas.add(areaData);
                }
            }

            ArrayList<String> userAreasId = new ArrayList<>();
            if(designatedAreas != null)
            {
                String[] arrId = designatedAreasId.split("\n");
                for (String areaDataId:arrId) {
                    userAreasId.add(areaDataId);
                }
            }

            UpdateUser.userCityNames = userAreas;
            UpdateUser.userCityIds = userAreasId;
            parent = FXMLLoader.load(getClass().getResource("/view/update_user.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setBody(parent);
        dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        stackPane.setVisible(true);
        dialog.show();
        dialog.setOverlayClose(false);
    }

    public void deleteUser(String userTableId) throws IOException {
        DeleteWindow.sceneWindow = "ViewUser";
        DeleteWindow.deletionId = userTableId;
        Parent parent = null;
        try {
            parent = FXMLLoader.load(getClass().getResource("/view/delete_window.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setStyle("-fx-padding: -40 -20 -40 -20;");
        dialogLayout.setBody(parent);
        dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        stackPane.setVisible(true);
        dialog.show();
        dialog.setOverlayClose(false);
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getUserTableId() {
        return userTableId;
    }

    public void setUserTableId(String userTableId) {
        this.userTableId = userTableId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserContact() {
        return userContact;
    }

    public void setUserContact(String userContact) {
        this.userContact = userContact;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public String getCnic() {
        return cnic;
    }

    public void setCnic(String cnic) {
        this.cnic = cnic;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getDesignatedAreasId() {
        return designatedAreasId;
    }

    public void setDesignatedAreasId(String designatedAreasId) {
        this.designatedAreasId = designatedAreasId;
    }

    public String getDesignatedAreas() {
        return designatedAreas;
    }

    public void setDesignatedAreas(String designatedAreas) {
        this.designatedAreas = designatedAreas;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public ArrayList<ArrayList<String>> getUserInfo(Statement stmt, Connection con)
    {
        ArrayList<ArrayList<String>> userData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        temp = new ArrayList<String>();
        ResultSet rs = null;
        String preUserId = "";
        String designatedAreas  = "";
        String designatedAreasId  = "";
        boolean iter = false;
        try {
            rs = stmt.executeQuery("SELECT `user_info`.`user_table_id`, `user_info`.`user_id`, `user_info`.`user_name`, `user_info`.`user_type`, `user_info`.`user_contact`, `user_info`.`user_address`, `user_info`.`user_cnic`, `salesman_designated_cities`.`designated_city_id`, `city_info`.`city_name`, `user_info`.`user_status` FROM `user_info` LEFT OUTER JOIN `salesman_designated_cities` ON `user_info`.`user_table_id` = `salesman_designated_cities`.`salesman_table_id` AND `salesman_designated_cities`.`designated_status` = 'Active' LEFT OUTER JOIN `city_info` ON `city_info`.`city_table_id` = `salesman_designated_cities`.`designated_city_id` WHERE `user_info`.`user_status` != 'Deleted' ORDER BY `user_info`.`user_table_id`");
            while (rs.next())
            {
                if(preUserId.equals(rs.getString(1)))
                {
                    designatedAreasId += "\n"+rs.getString(8);
                    designatedAreas += "\n"+rs.getString(9);
                }
                else
                {
                    if(iter)
                    {
                        temp.set(7, designatedAreasId);
                        temp.set(8, designatedAreas);
                        userData.add(temp);
                        designatedAreasId = rs.getString(8);
                        designatedAreas = rs.getString(9);
                        temp = new ArrayList<String>();
                        preUserId = rs.getString(1);
                    }
                    else
                    {
                        iter = true;
                        designatedAreasId = rs.getString(8);
                        designatedAreas = rs.getString(9);
                        temp = new ArrayList<>();
                        preUserId = rs.getString(1);
                    }
                    temp.add(rs.getString(1)); // Table Id
                    temp.add(rs.getString(2)); // Id
                    temp.add(rs.getString(3)); // Name
                    temp.add(rs.getString(4)); // Type
                    temp.add(rs.getString(5)); // Contact
                    temp.add(rs.getString(6)); // Address
                    temp.add(rs.getString(7)); // cnic
                    temp.add(rs.getString(8)); // area Id
                    temp.add(rs.getString(9)); // area Name
                    temp.add(rs.getString(10)); // status
                }
            }
            if(iter)
            {
                temp.set(7, designatedAreasId);
                temp.set(8, designatedAreas);
                userData.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userData;
    }

    public ArrayList<ArrayList<String>> getUserSearch(Statement stmt, Connection con, String userId, String userName, String userType, String userContact, String userCnic, String userStatus)
    {
        ArrayList<ArrayList<String>> userData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        temp = new ArrayList<String>();
        ResultSet rs = null;
        String preUserId = "";
        String designatedAreas  = "";
        String designatedAreasId  = "";
        boolean iter = false;
        String searchQuery;
        int multipleSearch = 0;
        searchQuery = "SELECT `user_info`.`user_table_id`, `user_info`.`user_id`, `user_info`.`user_name`, `user_info`.`user_type`, `user_info`.`user_contact`, `user_info`.`user_address`, `user_info`.`user_cnic`, `salesman_designated_cities`.`designated_city_id`, `city_info`.`city_name`, `user_info`.`user_status` FROM `user_info` LEFT OUTER JOIN `salesman_designated_cities` ON `user_info`.`user_table_id` = `salesman_designated_cities`.`salesman_table_id` AND `salesman_designated_cities`.`designated_status` = 'Active' LEFT OUTER JOIN `city_info` ON `city_info`.`city_table_id` = `salesman_designated_cities`.`designated_city_id` WHERE ";
        if(!userId.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`user_info`.`user_id` = '"+userId+"'";
            multipleSearch++;
        }
        if(!userName.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`user_info`.`user_name` = '"+userName+"'";
            multipleSearch++;
        }
        if(!userType.equals("All"))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`user_info`.`user_type` = '"+userType+"'";
            multipleSearch++;
        }
        if(!userContact.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`user_info`.`user_contact` = '"+userContact+"'";
            multipleSearch++;
        }
        if(!userCnic.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`user_info`.`user_cnic` = '"+userCnic+"'";
            multipleSearch++;
        }
        if(!userStatus.equals("All"))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`user_info`.`user_status` = '"+userStatus+"'";
            multipleSearch++;
        }
        if(multipleSearch >= 1)
        {
            searchQuery += " AND ";
        }
        searchQuery += "`user_info`.`user_status` != 'Deleted' ORDER BY `user_info`.`user_table_id`";
        try {
            rs = stmt.executeQuery(searchQuery);
            while (rs.next())
            {
                if(preUserId.equals(rs.getString(1)))
                {
                    designatedAreasId += "\n"+rs.getString(8);
                    designatedAreas += "\n"+rs.getString(9);
                }
                else
                {
                    if(iter)
                    {
                        temp.set(7, designatedAreasId);
                        temp.set(8, designatedAreas);
                        userData.add(temp);
                        designatedAreasId = rs.getString(8);
                        designatedAreas = rs.getString(9);
                        temp = new ArrayList<String>();
                        preUserId = rs.getString(1);
                    }
                    else
                    {
                        iter = true;
                        designatedAreasId = rs.getString(8);
                        designatedAreas = rs.getString(9);
                        temp = new ArrayList<>();
                        preUserId = rs.getString(1);
                    }
                    temp.add(rs.getString(1)); // Table Id
                    temp.add(rs.getString(2)); // Id
                    temp.add(rs.getString(3)); // Name
                    temp.add(rs.getString(4)); // Type
                    temp.add(rs.getString(5)); // Contact
                    temp.add(rs.getString(6)); // Address
                    temp.add(rs.getString(7)); // cnic
                    temp.add(rs.getString(8)); // area Id
                    temp.add(rs.getString(9)); // area Name
                    temp.add(rs.getString(10)); // status
                }
            }
            if(iter)
            {
                temp.set(7, designatedAreasId);
                temp.set(8, designatedAreas);
                userData.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userData;
    }

    public ArrayList<String> getUserSummary(Statement stmt, Connection con)
    {
        ArrayList<String> groupData = new ArrayList<String>();
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT COUNT(`salesman_table_id`) FROM `salesman_info` WHERE `salesman_status` != 'Deleted'");
            while (rs.next())
            {
                groupData.add(rs.getString(1));
            }
            rs = stmt.executeQuery("SELECT COUNT(`salesman_table_id`) FROM `salesman_info` WHERE `salesman_status` = 'Active'");
            while (rs.next())
            {
                groupData.add(rs.getString(1));
            }
            rs = stmt.executeQuery("SELECT COUNT(`salesman_table_id`) FROM `salesman_info` WHERE `salesman_status` = 'In Active'");
            while (rs.next())
            {
                groupData.add(rs.getString(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return groupData;
    }

    private Node createErrorIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.EDIT);
        graphic.setFontSize(20.0);
        return graphic;
    }

    private Node createDeleteIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.TRASH);
        graphic.setFontSize(20.0);
        return graphic;
    }
}
