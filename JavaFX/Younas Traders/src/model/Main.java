package model;

import controller.SetupHeader;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Main extends Application {
    ArrayList<String> coreData = new ArrayList<>();

    @Override
    public void start(Stage primaryStage) throws Exception {
//        Parent root = FXMLLoader.load(getClass().getResource("/view/login.fxml"));
//        primaryStage.setTitle("Profit Flow");
//        primaryStage.setX(GlobalVariables.boundsX);
//        primaryStage.setY(GlobalVariables.boundsY);
//        GlobalVariables.baseScene = new Scene(root, GlobalVariables.SCREEN_WIDTH, GlobalVariables.SCREEN_HEIGHT);
//        primaryStage.setScene(GlobalVariables.baseScene);
//        primaryStage.setMaximized(true);
//        primaryStage.getIcons().add(new Image(Main.class.getResourceAsStream("/view/images/icon.png")));
//        primaryStage.show();
//        GlobalVariables.baseStage = primaryStage;

        readCoreFile();
        String directoryName = "secret-credentials";
        File directory = new File(directoryName);
        if (!directory.exists())
        {
            Parent root = FXMLLoader.load(getClass().getResource("/view/setup_license_activation.fxml"));
            primaryStage.setTitle("Profit Flow");
            primaryStage.setX(GlobalVariables.boundsX);
            primaryStage.setY(GlobalVariables.boundsY);
            GlobalVariables.baseScene = new Scene(root, GlobalVariables.SCREEN_WIDTH, GlobalVariables.SCREEN_HEIGHT);
            primaryStage.setScene(GlobalVariables.baseScene);
            primaryStage.setMaximized(true);
            primaryStage.getIcons().add(new Image("/view/images/icon.PNG"));
            primaryStage.show();
            GlobalVariables.baseStage = primaryStage;
        }
        else
        {
            if(isValid() && coreData.get(4).equals("Owner Business"))
            {
                Parent root = FXMLLoader.load(getClass().getResource("/view/setup_owner_business.fxml"));
                primaryStage.setTitle("Profit Flow");
                primaryStage.setX(GlobalVariables.boundsX);
                primaryStage.setY(GlobalVariables.boundsY);
                GlobalVariables.baseScene = new Scene(root, GlobalVariables.SCREEN_WIDTH, GlobalVariables.SCREEN_HEIGHT);
                primaryStage.setScene(GlobalVariables.baseScene);
                primaryStage.setMaximized(true);
                primaryStage.getIcons().add(new Image("/view/images/icon.PNG"));
                primaryStage.show();
                GlobalVariables.baseStage = primaryStage;
            }
            else if(isValid() && coreData.get(4).equals("Companies"))
            {
                SetupHeader.labelName = "Companies\nSelection";
                Parent root = FXMLLoader.load(getClass().getResource("/view/setup_owner_company.fxml"));
                primaryStage.setTitle("Profit Flow");
                primaryStage.setX(GlobalVariables.boundsX);
                primaryStage.setY(GlobalVariables.boundsY);
                GlobalVariables.baseScene = new Scene(root, GlobalVariables.SCREEN_WIDTH, GlobalVariables.SCREEN_HEIGHT);
                primaryStage.setScene(GlobalVariables.baseScene);
                primaryStage.setMaximized(true);
                primaryStage.getIcons().add(new Image("/view/images/icon.PNG"));
                primaryStage.show();
                GlobalVariables.baseStage = primaryStage;
            }
            else if(isValid() && coreData.get(4).equals("Products"))
            {
                SetupHeader.labelName = "Products\nSelection";
                Parent root = FXMLLoader.load(getClass().getResource("/view/setup_owner_products.fxml"));
                primaryStage.setTitle("Profit Flow");
                primaryStage.setX(GlobalVariables.boundsX);
                primaryStage.setY(GlobalVariables.boundsY);
                GlobalVariables.baseScene = new Scene(root, GlobalVariables.SCREEN_WIDTH, GlobalVariables.SCREEN_HEIGHT);
                primaryStage.setScene(GlobalVariables.baseScene);
                primaryStage.setMaximized(true);
                primaryStage.getIcons().add(new Image("/view/images/icon.PNG"));
                primaryStage.show();
                GlobalVariables.baseStage = primaryStage;
            }
            else if(isValid() && coreData.get(4).equals("Dealers"))
            {
                SetupHeader.labelName = "Dealers in\nDistrict "+ SetupOwnerDealersInfo.districtName;
                Parent root = FXMLLoader.load(getClass().getResource("/view/setup_owner_dealers.fxml"));
                primaryStage.setTitle("Profit Flow");
                primaryStage.setX(GlobalVariables.boundsX);
                primaryStage.setY(GlobalVariables.boundsY);
                GlobalVariables.baseScene = new Scene(root, GlobalVariables.SCREEN_WIDTH, GlobalVariables.SCREEN_HEIGHT);
                primaryStage.setScene(GlobalVariables.baseScene);
                primaryStage.setMaximized(true);
                primaryStage.getIcons().add(new Image("/view/images/icon.PNG"));
                primaryStage.show();
                GlobalVariables.baseStage = primaryStage;
            }
            else
            {
                if(isValid())
                {
                    Parent root = FXMLLoader.load(getClass().getResource("/view/login.fxml"));
                    primaryStage.setTitle("Profit Flow");
                    primaryStage.setX(GlobalVariables.boundsX);
                    primaryStage.setY(GlobalVariables.boundsY);
                    GlobalVariables.baseScene = new Scene(root, GlobalVariables.SCREEN_WIDTH, GlobalVariables.SCREEN_HEIGHT);
                    primaryStage.setScene(GlobalVariables.baseScene);
                    primaryStage.setMaximized(true);
                    primaryStage.getIcons().add(new Image("/view/images/icon.PNG"));
                    primaryStage.show();
                    GlobalVariables.baseStage = primaryStage;
                }
                else
                {

                }
            }
        }

    }


    public static void main(String[] args) {
        launch(args);
    }

    public void readCoreFile() {
        String directoryName = "secret-credentials";
        File directory = new File(directoryName);
        if (directory.exists())
        {
            try {
                String filePath = "secret-credentials/data.txt";
                File myObj = new File(filePath);
                Scanner myReader = new Scanner(myObj);
                while (myReader.hasNextLine()) {
                    String data = myReader.nextLine();
                    coreData.add(data);
                }
                myReader.close();
            } catch (FileNotFoundException e) {
                System.out.println("An error occurred.");
                e.printStackTrace();
            }
            GlobalVariables.softwareLicId = coreData.get(0);
            GlobalVariables.softwareLicNumber = coreData.get(1);
            GlobalVariables.softwareLicRegDate = coreData.get(2);
            GlobalVariables.softwareLicExpDate = coreData.get(3);
            GlobalVariables.workingDistrict = coreData.get(5);
        }

    }

    private boolean isValid()
    {
        String stEndDate = coreData.get(3);
        String stStartDate = GlobalVariables.getStDate();

        Date endDate = null;
        Date startDate = null;
        try {
            startDate = GlobalVariables.fmt.parse(stStartDate);
            endDate = GlobalVariables.fmt.parse(stEndDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long diff = endDate.getTime() - startDate.getTime();
        diff = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);

        if(diff > 0)
        {
            return true;
        }
        else if(diff > -15)
        {
            String title = "Warning";
            String message = "License Expired, Please Renew Your License";
            GlobalVariables.showNotification(0, title, message);
            return true;
        }
        else
        {
            String title = "ERROR";
            String message = "LICENSE ENDED";
            GlobalVariables.showNotification(-1, title, message);
            return false;
        }
    }
}
