package model;

import javafx.util.Duration;
import tray.animations.AnimationType;
import tray.notification.NotificationType;
import tray.notification.TrayNotification;

import java.sql.*;

public class MysqlCon {
    public Connection con;
    public Statement stmt;
    public MysqlCon() {
        try {
            Class.forName("com.mysql.jdbc.Driver");

            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/younas_traders", "root", "");
            stmt = con.createStatement();

        } catch (Exception e) {
            String title = "Error";
            String message = "Unable to Connect to Server";

            GlobalVariables.showNotification(-1, title, message);
//            System.out.println(e);
        }
    }
    public static void main(String[] args)
    {

    }
}