package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DashboardSales1Info {

    public ArrayList<ArrayList<String>> getCompanyPurchaseDetail(Statement stmt, Connection con, String fromDate, String toDate)
    {
        ArrayList<ArrayList<String>> dealersFinanceData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            String query = "SELECT `order_info`.`order_id`, `order_info`.`final_price`, `order_info`.`delivered_date`, `user_info`.`user_name` FROM `order_info` LEFT OUTER JOIN `user_info` ON `user_info`.`user_table_id` = `order_info`.`delivered_user_id` WHERE str_to_date(`order_info`.`delivered_date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') AND str_to_date(`order_info`.`delivered_date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y') AND `order_info`.`status` = 'Delivered' ORDER BY str_to_date(`order_info`.`delivered_date`, '%d/%b/%Y') DESC";
            rs = stmt.executeQuery(query);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Purchase Id
                temp.add(rs.getString(2) == null?"N/A":rs.getString(2)); // Net Amount
                temp.add(rs.getString(3) == null?"N/A":rs.getString(3)); // Date
                temp.add(rs.getString(4) == null?"N/A":rs.getString(4)); // Company Name
                dealersFinanceData.add(temp);
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dealersFinanceData;
    }

    public ArrayList<ArrayList<String>> getCompanyReturnDetail(Statement stmt, Connection con, String fromDate, String toDate)
    {
        ArrayList<ArrayList<String>> dealersFinanceData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            String query = "SELECT `order_return`.`return_id`, `order_return`.`return_total_price`, `order_return`.`entry_date`, `user_info`.`user_name` FROM `order_return` LEFT OUTER JOIN `user_info` ON `user_info`.`user_table_id` = `order_return`.`enter_user_id` WHERE str_to_date(`order_return`.`entry_date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') AND str_to_date(`order_return`.`entry_date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y') AND `order_return`.`status` != 'Deleted' ORDER BY str_to_date(`order_return`.`entry_date`, '%d/%b/%Y') DESC";
            rs = stmt.executeQuery(query);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Return Id
                temp.add(rs.getString(2) == null?"N/A":rs.getString(2)); // Amount
                temp.add(rs.getString(3) == null?"N/A":rs.getString(3)); // Date
                temp.add(rs.getString(4) == null?"N/A":rs.getString(4)); // Company Name
                dealersFinanceData.add(temp);
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dealersFinanceData;
    }
}
