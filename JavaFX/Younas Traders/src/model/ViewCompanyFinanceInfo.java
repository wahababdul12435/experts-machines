package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import controller.UpdateComment;
import controller.UpdateCompanyFinance;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class ViewCompanyFinanceInfo {
    private String srNo;
    private String paymentId;
    private String paymentDate;
    private String paymentDay;
    private String companyId;
    private String companyName;
    private String companyContact;
    private String cashSent;
    private String cashReceived;
    private String cashWaivedOff;
    private String cashPending;
    private String userName;
    private String comment;

    private HBox operationsPane;
    private JFXButton btnComment;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    SimpleDateFormat fmt = new SimpleDateFormat("dd/MMM/yyyy");
    public static String orderId;

    public static StackPane stackPane;
    public static JFXDialog dialog;

    public ViewCompanyFinanceInfo() {
    }

    public ViewCompanyFinanceInfo(String srNo, String paymentId, String paymentDate, String paymentDay, String companyId, String companyName, String companyContact, String cashSent, String cashReceived, String cashWaivedOff, String cashPending, String userName, String comment) {
        this.srNo = srNo;
        this.paymentId = paymentId;
        this.paymentDate = paymentDate;
        this.paymentDay = paymentDay;
        this.companyId = companyId;
        this.companyName = companyName;
        this.companyContact = companyContact;
        this.cashSent = cashSent == null || cashSent.equals("-") ? "0" : String.format("%,.2f", Float.parseFloat(cashSent));
        this.cashReceived = cashReceived == null || cashReceived.equals("-") ? "0" : String.format("%,.2f", Float.parseFloat(cashReceived));
        this.cashWaivedOff = cashWaivedOff == null || cashWaivedOff.equals("-") ? "0" : String.format("%,.2f", Float.parseFloat(cashWaivedOff));
        this.cashPending = cashPending == null || cashPending.equals("-") ? "0" : String.format("%,.2f", Float.parseFloat(cashPending));
        this.userName = userName;
        this.comment = comment;

        this.btnComment = new JFXButton();
        this.btnEdit = new JFXButton();
        this.btnDelete = new JFXButton();
        this.btnComment.setGraphic(GlobalVariables.createCommentIcon());
        this.btnEdit.setGraphic(GlobalVariables.createEditIcon());
        this.btnDelete.setGraphic(GlobalVariables.createDeleteIcon());
        this.btnComment.getStyleClass().add("btn_icon");
        this.btnEdit.getStyleClass().add("btn_icon");
        this.btnDelete.getStyleClass().add("btn_icon");
        this.operationsPane = new HBox(btnComment, btnEdit, btnDelete);
        this.btnComment.setOnAction((action)->commentClicked());
        this.btnEdit.setOnAction((action)->editClicked());
    }

    private void commentClicked()
    {
        Parent parent = null;
        try {
            UpdateComment.commentType = "View company Finance";
            UpdateComment.orderId = this.paymentId;
            UpdateComment.comment = this.comment;
            parent = FXMLLoader.load(getClass().getResource("/view/update_comment.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setStyle("-fx-padding: -20 -10 -20 -10;");
        dialogLayout.setBody(parent);
        dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        stackPane.setVisible(true);
        dialog.show();
        dialog.setOverlayClose(false);
    }

    public void editClicked()
    {
        Parent parent = null;
        CompanyFinanceReportInfo.companyFinanceId = companyId;
        try {
            UpdateCompanyFinance.paymentId = paymentId;
            UpdateCompanyFinance.companyId = companyId;
            UpdateCompanyFinance.companyName = companyName;
            UpdateCompanyFinance.date = paymentDate;
            UpdateCompanyFinance.returnPage = "View Company Finance";
            if(!cashSent.equals("-"))
            {
                UpdateCompanyFinance.preCashAmount = cashSent;
                UpdateCompanyFinance.cashAmount = cashSent;
                UpdateCompanyFinance.preCashType = "Sent";
                UpdateCompanyFinance.cashType = "Sent";
            }
            else if(!cashReceived.equals("-"))
            {
                UpdateCompanyFinance.preCashAmount = cashReceived;
                UpdateCompanyFinance.cashAmount = cashReceived;
                UpdateCompanyFinance.preCashType = "Received";
                UpdateCompanyFinance.cashType = "Received";
            }
            else if(!cashWaivedOff.equals("-"))
            {
                UpdateCompanyFinance.preCashAmount = cashWaivedOff;
                UpdateCompanyFinance.cashAmount = cashWaivedOff;
                UpdateCompanyFinance.preCashType = "Waive Off";
                UpdateCompanyFinance.cashType = "Waive Off";
            }

            parent = FXMLLoader.load(getClass().getResource("/view/update_company_finance.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setStyle("-fx-padding: -20 -10 -20 -10;");
        dialogLayout.setBody(parent);
        dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        stackPane.setVisible(true);
        dialog.show();
        dialog.setOverlayClose(false);
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getPaymentDay() {
        return paymentDay;
    }

    public void setPaymentDay(String paymentDay) {
        this.paymentDay = paymentDay;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyContact() {
        return companyContact;
    }

    public void setCompanyContact(String companyContact) {
        this.companyContact = companyContact;
    }

    public String getCashSent() {
        return cashSent;
    }

    public void setCashSent(String cashSent) {
        this.cashSent = cashSent;
    }

    public String getCashReceived() {
        return cashReceived;
    }

    public void setCashReceived(String cashReceived) {
        this.cashReceived = cashReceived;
    }

    public String getCashWaivedOff() {
        return cashWaivedOff;
    }

    public void setCashWaivedOff(String cashWaivedOff) {
        this.cashWaivedOff = cashWaivedOff;
    }

    public String getCashPending() {
        return cashPending;
    }

    public void setCashPending(String cashPending) {
        this.cashPending = cashPending;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnComment() {
        return btnComment;
    }

    public void setBtnComment(JFXButton btnComment) {
        this.btnComment = btnComment;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public ArrayList<ArrayList<String>> getCompanyFinanceInfo(Statement stmt, Connection con)
    {
        ArrayList<ArrayList<String>> companyFinanceData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT `company_payments`.`payment_id`, `company_payments`.`date`, `company_payments`.`day`, `company_payments`.`company_id`, `company_info`.`company_name`, `company_info`.`company_contact`, `company_payments`.`amount`, `company_payments`.`cash_type`, `company_payments`.`pending_payments`, `user_info`.`user_name`, `company_payments`.`comments` FROM `company_payments` LEFT OUTER JOIN `company_info` ON `company_info`.`company_table_id` = `company_payments`.`company_id` LEFT OUTER JOIN `user_info` ON `user_info`.`user_table_id` = `company_payments`.`user_id` WHERE `company_payments`.`status` != 'Deleted' GROUP BY `company_payments`.`payment_id` ORDER BY str_to_date(`company_payments`.`date`, '%d/%b/%Y') DESC LIMIT 5000");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Payment Id
                temp.add(rs.getString(2)); // Payment Date
                temp.add(rs.getString(3)); // Payment Day
                temp.add(rs.getString(4)); // Company Id
                temp.add(rs.getString(5)); // Company Name
                temp.add(rs.getString(6)); // Company Contact
                if(rs.getString(8).equals("Sent"))
                {
                    temp.add(rs.getString(7));
                    temp.add("-");
                    temp.add("-");
                }
                else if(rs.getString(8).equals("Received"))
                {
                    temp.add("-");
                    temp.add(rs.getString(7));
                    temp.add("-");
                }
                else
                {
                    temp.add("-");
                    temp.add("-");
                    temp.add(rs.getString(7));
                }
                temp.add(rs.getString(9)); // Pending Payments
                temp.add(rs.getString(10)); // User Name
                temp.add(rs.getString(11)); // Comment
                companyFinanceData.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return companyFinanceData;
    }

    public ArrayList<ArrayList<String>> getCompanyFinanceInfoSearch(Statement stmt, Connection con, String companyId, String companyName, String fromDate, String toDate, String day, String cashType)
    {
        ArrayList<ArrayList<String>> companyFinanceData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        String searchQuery;
        searchQuery = "SELECT `company_payments`.`payment_id`, `company_payments`.`date`, `company_payments`.`day`, `company_payments`.`company_id`, `company_info`.`company_name`, `company_info`.`company_contact`, `company_payments`.`amount`, `company_payments`.`cash_type`, `company_payments`.`pending_payments`, `user_info`.`user_name`, `company_payments`.`comments` FROM `company_payments` LEFT OUTER JOIN `company_info` ON `company_info`.`company_table_id` = `company_payments`.`company_id` LEFT OUTER JOIN `user_info` ON `user_info`.`user_table_id` = `company_payments`.`user_id` WHERE `company_payments`.`status` != 'Deleted' ";
        if(!companyId.equals(""))
        {
            searchQuery += "AND `company_info`.`company_id` = '"+companyId+"' ";
        }
        if(!companyName.equals(""))
        {

            searchQuery += "AND `company_info`.`company_name` = '"+companyName+"' ";
        }
        if(!fromDate.equals(""))
        {
            searchQuery += "AND str_to_date(`company_payments`.`date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') ";;
        }
        if(!toDate.equals(""))
        {
            searchQuery += "AND str_to_date(`company_payments`.`date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y') ";
        }
        if(!day.equals("All"))
        {
            searchQuery += "AND `company_payments`.`day` = '"+day+"' ";
        }
        if(!cashType.equals("All"))
        {
            searchQuery += "AND `company_payments`.`cash_type` = '"+cashType+"' ";
        }
        searchQuery += "GROUP BY `company_payments`.`payment_id` ORDER BY str_to_date(`company_payments`.`date`, '%d/%b/%Y') DESC";
        try {
            rs = stmt.executeQuery(searchQuery);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Payment Id
                temp.add(rs.getString(2)); // Payment Date
                temp.add(rs.getString(3)); // Payment Day
                temp.add(rs.getString(4)); // Company Id
                temp.add(rs.getString(5)); // Company Name
                temp.add(rs.getString(6)); // Company Contact
                if(rs.getString(8).equals("Sent"))
                {
                    temp.add(rs.getString(7));
                    temp.add("-");
                    temp.add("-");
                }
                else if(rs.getString(8).equals("Received"))
                {
                    temp.add("-");
                    temp.add(rs.getString(7));
                    temp.add("-");
                }
                else
                {
                    temp.add("-");
                    temp.add("-");
                    temp.add(rs.getString(7));
                }
                temp.add(rs.getString(9)); // Pending Payments
                temp.add(rs.getString(10)); // User Name
                temp.add(rs.getString(11)); // Comment
                companyFinanceData.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return companyFinanceData;
    }
}
