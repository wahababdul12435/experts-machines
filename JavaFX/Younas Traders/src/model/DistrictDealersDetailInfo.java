package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import javafx.application.Platform;
import javafx.scene.layout.HBox;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class DistrictDealersDetailInfo {
    private String srNo;
    private String dealerName;
    private String dealerContact;
    private String dealerAddress;
    private String dealerCnic;
    private String dealerType;
    private String dealerAdded;
    private String district;
    private String businessCategory;

    private HBox operationsPane;
    private JFXCheckBox btnAddDealer;
    private JFXButton btnShowOnMap;

    private ArrayList<ArrayList<String>> dealersData;

    String webUrl;

    public DistrictDealersDetailInfo() {
    }

    public DistrictDealersDetailInfo(String district, String businessCategory) {
        this.district = district;
        this.businessCategory = businessCategory;
        this.webUrl = "https://tuberichy.com/ProfitFlow/ServerDataGet/GetData.php?software_lic_id="+GlobalVariables.softwareLicId+"&required_data=district_dealers_data&district="+this.district+"&business_category="+this.businessCategory;
        String fetchedOrder = fetchServerData();
        if(fetchedOrder != null && !fetchedOrder.equals(""))
        {
            loadIntoListView(fetchedOrder);
        }
    }

    public DistrictDealersDetailInfo(String srNo, String dealerName, String dealerContact, String dealerAddress, String dealerCnic, String dealerType, String dealerAdded) {
        this.srNo = srNo;
        this.dealerName = dealerName;
        this.dealerContact = dealerContact;
        this.dealerAddress = dealerAddress;
        this.dealerCnic = dealerCnic;
        this.dealerType = dealerType;
        this.dealerAdded = dealerAdded;
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getDealerContact() {
        return dealerContact;
    }

    public void setDealerContact(String dealerContact) {
        this.dealerContact = dealerContact;
    }

    public String getDealerAddress() {
        return dealerAddress;
    }

    public void setDealerAddress(String dealerAddress) {
        this.dealerAddress = dealerAddress;
    }

    public String getDealerCnic() {
        return dealerCnic;
    }

    public void setDealerCnic(String dealerCnic) {
        this.dealerCnic = dealerCnic;
    }

    public String getDealerType() {
        return dealerType;
    }

    public void setDealerType(String dealerType) {
        this.dealerType = dealerType;
    }

    public String getDealerAdded() {
        return dealerAdded;
    }

    public void setDealerAdded(String dealerAdded) {
        this.dealerAdded = dealerAdded;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXCheckBox getBtnAddDealer() {
        return btnAddDealer;
    }

    public void setBtnAddDealer(JFXCheckBox btnAddDealer) {
        this.btnAddDealer = btnAddDealer;
    }

    public JFXButton getBtnShowOnMap() {
        return btnShowOnMap;
    }

    public void setBtnShowOnMap(JFXButton btnShowOnMap) {
        this.btnShowOnMap = btnShowOnMap;
    }

    private String fetchServerData()
    {
        try
        {
            URL url = new URL(webUrl);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setConnectTimeout(2 * 1000);
            con.connect();
            if (con.getResponseCode() == 200)
            {
                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String json;
                while ((json = bufferedReader.readLine()) != null)
                {
                    sb.append(json + "\n");
                }
                GlobalVariables.isConnectedToServer = true;
                if(GlobalVariables.connectionCheck)
                {
                    GlobalVariables.connectionCheck = false;
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            GlobalVariables.lblConnectionStatus.setText("Connected");
                            GlobalVariables.sepUsers.setVisible(true);
                            GlobalVariables.lblUsersActive.setVisible(true);
                            GlobalVariables.lblUsersActive.setManaged(true);
                            String title = "Connected";
                            String message = "Connected to Server";
                            GlobalVariables.showNotification(1, title, message);
                        }
                        // do your GUI stuff here
                    });
                }
                return sb.toString().trim();
            }
            else
            {
                GlobalVariables.isConnectedToServer = false;
                if(!GlobalVariables.connectionCheck)
                {
                    GlobalVariables.connectionCheck = true;
                    Platform.runLater(new Runnable(){
                        @Override
                        public void run() {
                            GlobalVariables.lblConnectionStatus.setText("Not Connected");
                            GlobalVariables.sepUsers.setVisible(false);
                            GlobalVariables.lblUsersActive.setVisible(false);
                            GlobalVariables.lblUsersActive.setManaged(false);
                            String title = "Not Connected";
                            String message = "Unable to Connect to Server";
                            GlobalVariables.showNotification(-1, title, message);
                        }
                        // do your GUI stuff here
                    });
                }
                return null;
            }
        }
        catch (Exception e)
        {
            GlobalVariables.isConnectedToServer = false;
            if(!GlobalVariables.connectionCheck)
            {
                GlobalVariables.connectionCheck = true;
                Platform.runLater(new Runnable(){
                    @Override
                    public void run() {
                        GlobalVariables.lblConnectionStatus.setText("Not Connected");
                        GlobalVariables.sepUsers.setVisible(false);
                        GlobalVariables.lblUsersActive.setVisible(false);
                        GlobalVariables.lblUsersActive.setManaged(false);
                        String title = "Not Connected";
                        String message = "Unable to Connect to Server";
                        GlobalVariables.showNotification(-1, title, message);
                    }
                    // do your GUI stuff here
                });
            }
            return null;
        }
    }

    private void loadIntoListView(String json) throws JSONException {
        JSONArray jsonArray = new JSONArray(json);
        dealersData = new ArrayList<>();
        ArrayList<String> temp;
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject obj = jsonArray.getJSONObject(i);
            temp = new ArrayList<>();
            temp.add(String.valueOf(obj.getInt("order_loc_id")));
            temp.add(String.valueOf(obj.getInt("order_id")));
            temp.add(String.valueOf(obj.getInt("user_id")));
            temp.add(obj.getString("latitude"));
            temp.add(obj.getString("longitude"));
            temp.add(obj.getString("loc_name"));
            temp.add(obj.getString("date"));
            temp.add(obj.getString("time"));
            temp.add(String.valueOf(obj.getInt("dealer_id")));
            temp.add(String.valueOf(obj.getFloat("price")));
            temp.add(obj.getString("type"));
            dealersData.add(temp);
        }
    }
}
