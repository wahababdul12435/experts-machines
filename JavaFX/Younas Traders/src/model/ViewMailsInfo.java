package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ViewMailsInfo {
    private String srNo;
    private String mailId;
    private String fromEmail;
    private String toEmail;
    private String attachment;
    private String userName;
    private String date;
    private String time;
    private String status;

    private HBox operationsPane;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    public static StackPane stackPane;
    public static JFXDialog dialog;

    public ViewMailsInfo() {
        this.srNo = "";
        this.mailId = "";
        this.fromEmail = "";
        this.toEmail = "";
        this.attachment = "";
        this.userName = "";
        this.date = "";
        this.time = "";
        this.status = "";
    }

    public ViewMailsInfo(String srNo, String mailId, String fromEmail, String toEmail, String attachment, String userName, String date, String time, String status) {
        this.srNo = srNo;
        this.mailId = mailId;
        this.fromEmail = fromEmail;
        this.toEmail = toEmail;
        this.attachment = attachment;
        this.userName = userName;
        this.date = date;
        this.time = time;
        this.status = status;

        this.btnEdit = new JFXButton();
        this.btnDelete = new JFXButton();
        this.btnEdit.setGraphic(GlobalVariables.createEditIcon());
        this.btnDelete.setGraphic(GlobalVariables.createDeleteIcon());
        this.btnEdit.getStyleClass().add("btn_icon");
        this.btnDelete.getStyleClass().add("btn_icon");
        if(!status.equals("Sent"))
        {
            this.operationsPane = new HBox(btnEdit, btnDelete);
//            this.btnEdit.setOnAction((action)->editClicked());
//            this.btnDelete.setOnAction((action)->deleteClicked());
        }

    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getMailId() {
        return mailId;
    }

    public void setMailId(String mailId) {
        this.mailId = mailId;
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }

    public String getToEmail() {
        return toEmail;
    }

    public void setToEmail(String toEmail) {
        this.toEmail = toEmail;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public ArrayList<String> getFromMailsData(Statement stmt, Connection con)
    {
        ArrayList<String> mailsData = new ArrayList<String>();
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT DISTINCT(`from_email`) FROM `mails_record`");
            while (rs.next())
            {
                mailsData.add(rs.getString(1)); // Mail
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return mailsData;
    }

    public ArrayList<String> getToMailsData(Statement stmt, Connection con)
    {
        ArrayList<String> mailsData = new ArrayList<String>();
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT DISTINCT(`to_email`) FROM `mails_record`");
            while (rs.next())
            {
                mailsData.add(rs.getString(1)); // Mail
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return mailsData;
    }

    public ArrayList<ArrayList<String>> getMailsDetail(Statement stmt, Connection con)
    {
        ArrayList<ArrayList<String>> mailsData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT `mail_id`, `from_email`, `to_email`, `attachment`, `user_info`.`user_name`, `date`, `time`, `mails_record`.`status` FROM `mails_record` INNER JOIN `user_info` ON `user_info`.`user_table_id` = `mails_record`.`user_id` ORDER BY `mail_id` DESC LIMIT 100");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Mail id
                temp.add(rs.getString(2)); // From
                temp.add(rs.getString(3)); // To
                temp.add(rs.getString(4)); // Attachment
                temp.add(rs.getString(5)); // User Name
                temp.add(rs.getString(6)); // Date
                temp.add(rs.getString(7)); // Time
                temp.add(rs.getString(8)); // Status
                mailsData.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return mailsData;
    }

    public ArrayList<ArrayList<String>> getMailsDetailSearch(Statement stmt, Connection con, String mailId, String mailSubject, String fromMail, String toEmail, String fromDate, String toDate, String status)
    {
        ArrayList<ArrayList<String>> mailsData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        int multipleSearch = 0;
        String query = "SELECT `mail_id`, `from_email`, `to_email`, `attachment`, `user_info`.`user_name`, `date`, `time`, `mails_record`.`status` FROM `mails_record` INNER JOIN `user_info` ON `user_info`.`user_table_id` = `mails_record`.`user_id`";
        if(!mailId.equals(""))
        {
            query += "WHERE `mail_id` = '"+mailId+"'";
            multipleSearch++;
        }
        if(!mailSubject.equals(""))
        {
            if(multipleSearch > 0)
            {
                query += " AND `title` = '"+mailSubject+"'";
            }
            else
            {
                query += "WHERE `title` = '"+mailSubject+"'";
            }
            multipleSearch++;
        }
        if(!fromMail.equals("All"))
        {
            if(multipleSearch > 0)
            {
                query += " AND `from_email` = '"+fromMail+"'";
            }
            else
            {
                query += "WHERE `from_email` = '"+fromMail+"'";
            }
            multipleSearch++;
        }
        if(!toEmail.equals("All"))
        {
            if(multipleSearch > 0)
            {
                query += " AND `to_email` = '"+toEmail+"'";
            }
            else
            {
                query += "WHERE `to_email` = '"+toEmail+"'";
            }
            multipleSearch++;
        }
        if(!fromDate.equals(""))
        {
            if(multipleSearch > 0)
            {
                query += "AND str_to_date(`mails_record`.`date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') ";
            }
            else
            {
                query += "WHERE str_to_date(`mails_record`.`date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') ";
            }

        }
        if(!toDate.equals(""))
        {
            if(multipleSearch > 0)
            {
                query += "AND str_to_date(`mails_record`.`date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y') ";
            }
            else
            {
                query += "WHERE str_to_date(`mails_record`.`date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y') ";
            }
        }
        if(!status.equals("All"))
        {
            if(multipleSearch > 0)
            {
                query += " AND `mails_record`.`status` = '"+status+"'";
            }
            else
            {
                query += "WHERE `mails_record`.`status` = '"+status+"'";
            }
        }
        query += " ORDER BY `mail_id` DESC";
        try {
            rs = stmt.executeQuery(query);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Mail id
                temp.add(rs.getString(2)); // From
                temp.add(rs.getString(3)); // To
                temp.add(rs.getString(4)); // Attachment
                temp.add(rs.getString(5)); // User Name
                temp.add(rs.getString(6)); // Date
                temp.add(rs.getString(7)); // Time
                temp.add(rs.getString(8)); // Status
                mailsData.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return mailsData;
    }
}
