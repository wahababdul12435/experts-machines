package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import controller.DeleteWindow;
import controller.UpdateProduct;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;

public class ImportProductInfo {
    private String srNo;
    private String productTableId;
    private String productId;
    private String companyName;
    private String companyGroup;
    private String productName;
    private String productType;
    private String retialPrice;
    private String tradePrice;
    private String purchasePrice;
    private String purchaseDiscount;
    private String productStatus;
    private HBox operationsPane;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    public static StackPane stackPane;
    public static JFXDialog dialog;
    public static Label lblUpdate;

    public ImportProductInfo() {
        this.srNo = "";
        this.productTableId = "";
        this.productId = "";
        this.companyName = "";
        this.companyGroup = "";
        this.productName = "";
        this.productType = "";
        this.retialPrice = "";
        this.tradePrice = "";
        this.purchasePrice = "";
        this.purchaseDiscount = "";
        this.productStatus = "";
        this.btnEdit = new JFXButton("Edit");
        this.btnDelete = new JFXButton("Delete");
        this.operationsPane = new HBox(btnEdit, btnDelete);
    }

    public ImportProductInfo(String srNo, String productTableId, String productId, String companyName, String companyGroup, String productName, String productType, String retialPrice, String tradePrice, String purchasePrice, String purchaseDiscount, String productStatus)
    {
        this.srNo = srNo;
        this.productTableId = productTableId;
        this.productId = productId;
        this.companyName = companyName;
        this.companyGroup = companyGroup;
        this.productName = productName;
        this.productType = productType;
        this.retialPrice = retialPrice;
        this.tradePrice = tradePrice;
        this.purchasePrice = purchasePrice;
        this.purchaseDiscount = purchaseDiscount;
        this.productStatus = productStatus;

        this.btnEdit = new JFXButton();
        this.btnDelete = new JFXButton();
//        this.btnView.setGraphic(GlobalVariables.createViewIcon());

        this.operationsPane = new HBox();

//        this.btnView.getStyleClass().add("btn_icon");
//        this.btnView.setOnAction((action)->viewClicked());
//        this.operationsPane.getChildren().add(this.btnView);

        if(GlobalVariables.rightEdit)
        {
            this.btnEdit.setGraphic(GlobalVariables.createEditIcon());
            this.btnEdit.getStyleClass().add("btn_icon");
            this.btnEdit.setOnAction((action)->editClicked());
            this.operationsPane.getChildren().add(this.btnEdit);
        }
        if(GlobalVariables.rightDelete)
        {
            this.btnDelete.setGraphic(GlobalVariables.createDeleteIcon());
            this.btnDelete.getStyleClass().add("btn_icon");
            this.btnDelete.setOnAction(event -> {
                try {
                    deleteProduct(this.productTableId);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            this.operationsPane.getChildren().add(this.btnDelete);
        }
    }

    public void editClicked()
    {
        Parent parent = null;
        try {
            UpdateProduct.productTableId = productTableId;
            UpdateProduct.productId = productId;
            UpdateProduct.productName = productName;
            UpdateProduct.productType = productType;
            UpdateProduct.companyName = companyName;
            UpdateProduct.companyGroup = companyGroup;
            UpdateProduct.retailPrice = retialPrice;
            UpdateProduct.tradePrice = tradePrice;
            UpdateProduct.purchasePrice = purchasePrice;
            UpdateProduct.purchaseDiscount = purchaseDiscount;
            UpdateProduct.productStatus = productStatus;
            parent = FXMLLoader.load(getClass().getResource("/view/update_product.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
//        dialogLayout.setStyle("-fx-padding: -20 -10 -20 -10;");
        dialogLayout.setBody(parent);
        dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        stackPane.setVisible(true);
        dialog.show();
        dialog.setOverlayClose(false);
    }

    public void deleteProduct(String productTableId) throws IOException {
        DeleteWindow.sceneWindow = "ViewProducts";
        DeleteWindow.deletionId = productTableId;
        Parent parent = null;
        try {
            parent = FXMLLoader.load(getClass().getResource("/view/delete_window.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setStyle("-fx-padding: -40 -20 -40 -20;");
        dialogLayout.setBody(parent);
        dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        stackPane.setVisible(true);
        dialog.show();
        dialog.setOverlayClose(false);
    }

    public String getSrNo() {
        return srNo;
    }
    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }
    public String getProductId() {
        return productId;
    }
    public void setProductId(String productId) {
        this.productId = productId;
    }
    public String getCompanyName() {
        return companyName;
    }
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
    public String getCompanyGroup() {
        return companyGroup;
    }
    public void setCompanyGroup(String companyGroup) {
        this.companyGroup = companyGroup;
    }
    public String getProductName() {
        return productName;
    }
    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getRetialPrice() {
        return retialPrice;
    }
    public void setRetialPrice(String retialPrice) {
        this.retialPrice = retialPrice;
    }
    public String getTradePrice() {
        return tradePrice;
    }
    public void setTradePrice(String tradePrice) {
        this.tradePrice = tradePrice;
    }
    public String getPurchasePrice() {
        return purchasePrice;
    }
    public void setPurchasePrice(String purchasePrice) {
        this.purchasePrice = purchasePrice;
    }
    public String getPurchaseDiscount() {
        return purchaseDiscount;
    }
    public void setPurchaseDiscount(String purchaseDiscount) {
        this.purchaseDiscount = purchaseDiscount;
    }
    public String getProductStatus() {
        return productStatus;
    }
    public void setProductStatus(String productStatus) {
        this.productStatus = productStatus;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public ArrayList<ArrayList<String>> getProductsInfo(Statement stmt, Connection con)
    {
        ArrayList<ArrayList<String>> productData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT `product_info`.`product_table_id`, `product_info`.`product_id`, `company_info`.`company_name`, `groups_info`.`group_name`,`product_info`.`product_name`,`product_info`.`product_type`,`product_info`.`retail_price`,`product_info`.`trade_price`,`product_info`.`purchase_price`, `product_info`.`purchase_discount`, `product_info`.`product_status` FROM `product_info` LEFT OUTER JOIN `company_info` ON `product_info`.`company_table_id` = `company_info`.`company_table_id` LEFT OUTER JOIN `groups_info` ON `product_info`.`group_table_id` = `groups_info`.`group_table_id` WHERE `product_info`.`product_status` != 'Deleted' ORDER BY `product_info`.`product_table_id`");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Table Id
                temp.add(rs.getString(2)); // Id
                temp.add(rs.getString(3)); // Company Name
                temp.add(rs.getString(4)); // Group Name
                temp.add(rs.getString(5)); // Product Name
                temp.add(rs.getString(6)); // Type
                temp.add(rs.getString(7) == null || rs.getString(7).equals("") ? "0" : rs.getString(7)); // Retail
                temp.add(rs.getString(8) == null || rs.getString(8).equals("") ? "0" : rs.getString(8)); // Trade
                temp.add(rs.getString(9) == null || rs.getString(9).equals("") ? "0" : rs.getString(9)); // Purchase
                temp.add(rs.getString(10) == null || rs.getString(10).equals("") ? "0" : rs.getString(10)); // Discount
                temp.add(rs.getString(11)); // Status

                productData.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return productData;
    }

    public ArrayList<ArrayList<String>> getProductsSearch(Statement stmt, Connection con, String productId, String productName, String productType, String companyId, String groupId, String productStatus)
    {
        ArrayList<ArrayList<String>> productData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        String searchQuery;
        int multipleSearch = 0;
        searchQuery = "SELECT `product_info`.`product_table_id`, `product_info`.`product_id`, `company_info`.`company_name`, `groups_info`.`group_name`,`product_info`.`product_name`, `product_info`.`product_type`,`product_info`.`retail_price`,`product_info`.`trade_price`,`product_info`.`purchase_price`, `product_info`.`purchase_discount`, `product_info`.`product_status` FROM `product_info` LEFT OUTER JOIN `company_info` ON `product_info`.`company_table_id` = `company_info`.`company_table_id` LEFT OUTER JOIN `groups_info` ON `product_info`.`group_table_id` = `groups_info`.`group_table_id` WHERE ";
        if(!productId.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`product_info`.`product_id` = '"+productId+"'";
            multipleSearch++;
        }
        if(!productName.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`product_info`.`product_name` = '"+productName+"'";
            multipleSearch++;
        }
        if(!productType.equals("All"))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`product_info`.`product_type` = '"+productType+"'";
            multipleSearch++;
        }
        if(!companyId.equals("All"))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`product_info`.`company_table_id` = '"+companyId+"'";
            multipleSearch++;
        }
        if(!groupId.equals("All"))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`product_info`.`group_table_id` = '"+groupId+"'";
            multipleSearch++;
        }
        if(!productStatus.equals("All"))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`product_info`.`product_status` = '"+productStatus+"'";
            multipleSearch++;
        }
        if(multipleSearch >= 1)
        {
            searchQuery += " AND ";
        }
        searchQuery += "`product_info`.`product_status` != 'Deleted' ORDER BY `product_info`.`product_table_id`";
        try {
            rs = stmt.executeQuery(searchQuery);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Table Id
                temp.add(rs.getString(2)); // Id
                temp.add(rs.getString(3)); // Company Name
                temp.add(rs.getString(4)); // Group Name
                temp.add(rs.getString(5)); // Product Name
                temp.add(rs.getString(6)); // Type
                temp.add(rs.getString(7) == null || rs.getString(7).equals("") ? "0" : rs.getString(7)); // Retail
                temp.add(rs.getString(8) == null || rs.getString(8).equals("") ? "0" : rs.getString(8)); // Trade
                temp.add(rs.getString(9) == null || rs.getString(9).equals("") ? "0" : rs.getString(9)); // Purchase
                temp.add(rs.getString(10) == null || rs.getString(10).equals("") ? "0" : rs.getString(10)); // Discount
                temp.add(rs.getString(11)); // Status

                productData.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return productData;
    }

    public ArrayList<String> getProductsSummary(Statement stmt, Connection con)
    {
        ArrayList<String> groupData = new ArrayList<String>();
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT COUNT(`product_table_id`) FROM `product_info` WHERE `product_status` != 'Deleted'");
            while (rs.next())
            {
                groupData.add(rs.getString(1));
            }
            rs = stmt.executeQuery("SELECT COUNT(`product_table_id`) FROM `product_info` WHERE `product_status` = 'Active'");
            while (rs.next())
            {
                groupData.add(rs.getString(1));
            }
            rs = stmt.executeQuery("SELECT COUNT(`product_table_id`) FROM `product_info` WHERE `product_status` = 'In Active'");
            while (rs.next())
            {
                groupData.add(rs.getString(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return groupData;
    }

    private Node createErrorIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.EDIT);
        graphic.setFontSize(20.0);
        return graphic;
    }

    private Node createDeleteIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.TRASH);
        graphic.setFontSize(20.0);
        return graphic;
    }

    public void addtition_quantFROMStock(Statement stmt, Connection con,int prodID , int prodQuant){
        try {

            String insertQuery = "UPDATE `product_info` SET `opening_balance`='"+prodQuant+"' WHERE `product_table_id` = '"+prodID+"'";
            stmt.executeUpdate(insertQuery);
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public ArrayList<String> gettableID(Statement stmt, Connection con,String prodCode)
    {
        ArrayList<String> getdata = new ArrayList<String>();
        String prodTableId="";
        ResultSet rs3 = null;
        try {
            rs3 = stmt.executeQuery("select product_table_id,retail_price,opening_balance from product_info where product_id = '"+prodCode+"' ");
            int i = 0;
            while (rs3.next()) {
                getdata.add( rs3.getString(1));
                getdata.add( rs3.getString(2));
                getdata.add( rs3.getString(3));
            }
            con.close();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return getdata;
    }
    public String getPrevQuantity(Statement stmt, Connection con,String prodCode)
    {
        String getdata = "";
        String prodTableId="";
        ResultSet rs3 = null;
        try {
            rs3 = stmt.executeQuery("select opening_balance from product_info where product_table_id = '"+prodCode+"' ");
            int i = 0;
            while (rs3.next()) {
                getdata = rs3.getString(1);
                if(getdata == null)
                {
                    getdata="0";
                }
            }
            con.close();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return getdata;
    }

    public void insertclicked(Statement stmt, Connection con,ArrayList<ArrayList<String>> table)
    {
        try {
            for(int i =0;i<table.size();i++) {
                String insertQuery = "INSERT INTO `product_info`(`product_id`, `company_table_id`,`group_table_id`,`group_name`,`report_prodID`,`old_prodID`, `product_name`, `type_table_id`,`packSize`,`carton_size`, `retail_price`, `trade_price`, `purchase_price`, `purchase_discount`, `sales_tax`, `hold_stock`, `product_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`, `server_sync`) VALUES ('" + table.get(i).get(0) + "','" + table.get(i).get(1) + "','" + table.get(i).get(2) + "','" + table.get(i).get(3) + "','" + table.get(i).get(4) + "','" + table.get(i).get(5) + "','" + table.get(i).get(6) + "','" + table.get(i).get(7) + "','" + table.get(i).get(8) + "','" + table.get(i).get(9) + "','" + table.get(i).get(10) + "','" + table.get(i).get(11) + "','" + table.get(i).get(12) + "','" + table.get(i).get(13) + "','" + table.get(i).get(14) + "','" + table.get(i).get(15) + "','" + table.get(i).get(16) + "','" + table.get(i).get(17) + "','" + table.get(i).get(18) + "','" + table.get(i).get(19) + "','" + table.get(i).get(20) + "','" + table.get(i).get(21) + "','" + table.get(i).get(22) + "','" + table.get(i).get(23) + "')";
                stmt.executeUpdate(insertQuery);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }
    public void insertBatchData(Statement stmt, Connection con,ArrayList<ArrayList<String>> table)
    {
        try {
            for(int i =0;i<table.size();i++) {
                String insertQuery = "INSERT INTO `batchwise_stock` (`product_table_id`, `batch_no`,`quantity`,`bonus`,`retail_price`,`trade_price`, `purchase_price`, `batch_expiry`,`entry_date`, `entry_time`, `update_date`, `update_time`, `server_sync`) VALUES ('" + table.get(i).get(0) + "','" + table.get(i).get(2) + "','" + table.get(i).get(5) + "','" + table.get(i).get(6) + "','" + table.get(i).get(1) + "','" + table.get(i).get(3) + "','" + table.get(i).get(4) + "','" + table.get(i).get(7) + "','" + table.get(i).get(8) + "','" + table.get(i).get(9) + "','" + table.get(i).get(10) + "','" + table.get(i).get(11) + "','Insert')";
                stmt.executeUpdate(insertQuery);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //insertQuery = "INSERT INTO `product_info`(`product_id`, `company_table_id`, `group_table_id`, `product_name`, `product_type`,`packSize`,`carton_size`, `retail_price`, `trade_price`, `purchase_price`, `purchase_discount`, `sales_tax`, `product_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`, `server_sync`) VALUES ('"+productIdArr.get(i)+"','"+companyIdArr.get(i)+"','"+groupIdArr.get(i)+"','"+productNameArr.get(i)+"','"+productTypeArr.get(i)+"','"+productPackingArr.get(i)+"','"+cartonPackingArr.get(i)+"','"+retailPriceArr.get(i)+"','"+tradePriceArr.get(i)+"','"+purchasePriceArr.get(i)+"','"+purchaseDiscountArr.get(i)+"','"+salesTaxArr.get(i)+"','Active','"+currentUser+"','"+currentDate+"','"+currentTime+"','"+currentUser+"','"+currentDate+"','"+currentTime+"', 'Insert')";

    }
}
