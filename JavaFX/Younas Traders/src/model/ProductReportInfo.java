package model;

import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.HBox;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ProductReportInfo {
    private String srNo;
    private String productTableId;
    private String productId;
    private String productName;
    private String productType;
    private String companyName;
    private String totalSold;
    private String totalSale;
    private String inStock;
    private String status;

    private HBox operationsPane;
    private JFXButton btnView;
    private JFXButton btnEdit;

    public static String productReportId;

    public ProductReportInfo() {
    }

    public ProductReportInfo(String srNo, String productTableId, String productId, String productName, String productType, String companyName, String totalSold, String totalSale, String inStock, String status) {
        this.srNo = srNo;
        this.productTableId = productTableId;
        this.productId = productId;
        this.productName = productName;
        this.productType = productType;
        this.companyName = companyName;
        this.totalSold = totalSold == null || totalSold.equals("") ? "0" : String.format("%,.0f", Float.parseFloat(totalSold));
        this.totalSale = totalSale == null || totalSale.equals("") ? "0" : String.format("%,.2f", Float.parseFloat(totalSale));
        this.inStock = inStock == null || inStock.equals("") ? "0" : String.format("%,.0f", Float.parseFloat(inStock));
        this.status = status;

        this.btnView = new JFXButton();
        this.btnView.setGraphic(GlobalVariables.createViewIcon());
        this.btnView.getStyleClass().add("btn_icon");
        this.btnEdit = new JFXButton();
        this.btnEdit.setGraphic(GlobalVariables.createEditIcon());
        this.btnEdit.getStyleClass().add("btn_icon");
        this.operationsPane = new HBox(btnView, btnEdit);
        this.btnView.setOnAction((event)->viewProductDetail());
    }

    public void viewProductDetail()
    {
        Parent parent = null;
        try {
            productReportId = productTableId;
            parent = FXMLLoader.load(getClass().getResource("/view/product_report_detail.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(parent);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getProductTableId() {
        return productTableId;
    }

    public void setProductTableId(String productTableId) {
        this.productTableId = productTableId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getTotalSold() {
        return totalSold;
    }

    public void setTotalSold(String totalSold) {
        this.totalSold = totalSold;
    }

    public String getTotalSale() {
        return totalSale;
    }

    public void setTotalSale(String totalSale) {
        this.totalSale = totalSale;
    }

    public String getInStock() {
        return inStock;
    }

    public void setInStock(String inStock) {
        this.inStock = inStock;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnView() {
        return btnView;
    }

    public void setBtnView(JFXButton btnView) {
        this.btnView = btnView;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public ArrayList<ArrayList<String>> getProductReportInfo(Statement stmt, Connection con)
    {
        ArrayList<ArrayList<String>> productReportData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT `product_info`.`product_table_id`, `product_info`.`product_id`, `product_info`.`product_name`, `product_types`.`type_name`, `company_info`.`company_name`, SUM(`order_info_detailed`.`submission_quantity`) as 'total_sold', ROUND(SUM(`order_info_detailed`.`final_price`),2) as 'total_sale', `products_stock`.`in_stock`, `product_info`.`product_status` FROM `product_info` LEFT OUTER JOIN `company_info` ON `company_info`.`company_table_id` = `product_info`.`company_table_id` AND `company_info`.`company_status` != 'Deleted' LEFT OUTER JOIN `order_info_detailed` ON `order_info_detailed`.`product_table_id` = `product_info`.`product_table_id` LEFT OUTER JOIN `products_stock` ON `products_stock`.`product_table_id` = `product_info`.`product_table_id`AND `products_stock`.`status` != 'Deleted' INNER JOIN product_types on product_info.type_table_id=product_types.type_table_id WHERE `product_info`.`product_status` != 'Deleted' GROUP BY `product_info`.`product_id`");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Table Id
                temp.add(rs.getString(2)); // Id
                temp.add(rs.getString(3)); // Name
                temp.add(rs.getString(4)); // Type
                temp.add(rs.getString(5)); // Company Name
                temp.add(rs.getString(6)); // Total Sold
                temp.add(rs.getString(7)); // Total Sale
                temp.add(rs.getString(8)); // In Stock
                temp.add(rs.getString(9)); // Product Status
                productReportData.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return productReportData;
    }

    private Node createEditIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.EDIT);
        graphic.setFontSize(20.0);
        return graphic;
    }

    private Node createViewIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.EYE);
        graphic.setFontSize(20.0);
        return graphic;
    }
}
