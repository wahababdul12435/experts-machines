package model;

import com.jfoenix.controls.JFXButton;
import controller.UpdateSupplier;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class UpdateSupplierInfo {
    private String srNo;
    private String companyName;

    private HBox operationsPane;
    private JFXButton btnDelete;

    public static TableView<UpdateSupplierInfo> table_suppliercompanies;
    public ArrayList<ArrayList<String>> companiesData;

    public UpdateSupplierInfo() {
        this.srNo = "";
        this.companyName = "";
        companiesData = new ArrayList<ArrayList<String>>();
        this.btnDelete = new JFXButton("Delete");
        this.operationsPane = new HBox(btnDelete);
    }

    public UpdateSupplierInfo(String srNo, String companyName) {
        this.srNo = srNo;
        this.companyName = companyName;
        companiesData = new ArrayList<ArrayList<String>>();
        this.btnDelete = new JFXButton("Delete");
        this.btnDelete.setOnAction((action)->deleteDesignatedArea());
        this.operationsPane = new HBox(btnDelete);
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public void deleteDesignatedArea()
    {
        UpdateSupplier.supplierCompanyIds.remove(Integer.parseInt(srNo)-1);
        UpdateSupplier.supplierCompanyNames.remove(Integer.parseInt(srNo)-1);
        table_suppliercompanies.setItems(UpdateSupplier.parseUserList());
    }

    public ArrayList<ArrayList<String>> getCompaniesData()
    {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement stmt = objMysqlCon.stmt;
        Connection con = objMysqlCon.con;
        ResultSet rs = null;
        ArrayList<String> temp;
        try {
            rs = stmt.executeQuery("SELECT `company_table_id`, `company_name` FROM `company_info` WHERE `company_status` != 'Deleted'");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2));
                companiesData.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return companiesData;
    }
}
