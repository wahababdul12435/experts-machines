package model;

import com.jfoenix.controls.JFXButton;
import javafx.scene.Node;
import javafx.scene.layout.HBox;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

public class SaleReportInfo {
    private String srNo;
    private String date;
    private String day;
    private String bookings;
    private String delivered;
    private String itemsReturned;
    private String bookingPrice;
    private String returnPrice;
    private String cashCollection;
    private String cashReturn;
    private String cashWaiveOff;

    private HBox operationsPane;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    SimpleDateFormat fmt = new SimpleDateFormat("dd/MMM/yyyy");

    public SaleReportInfo() {
    }

    public SaleReportInfo(String srNo, String date, String day, String bookings, String delivered, String itemsReturned, String bookingPrice, String returnPrice, String cashCollection, String cashReturn, String cashWaiveOff) {
        this.srNo = srNo;
        this.date = date;
        this.day = day;
        this.bookings = bookings == null || bookings.equals("-") ? "0" : String.format("%,.0f", Float.parseFloat(bookings));
        this.delivered = delivered == null || delivered.equals("-") ? "0" : String.format("%,.0f", Float.parseFloat(delivered));
        this.itemsReturned = itemsReturned == null || itemsReturned.equals("-") ? "0" : String.format("%,.0f", Float.parseFloat(itemsReturned));
        this.bookingPrice = bookingPrice == null || bookingPrice.equals("-") ? "0" : String.format("%,.2f", Float.parseFloat(bookingPrice));
        this.returnPrice = returnPrice == null || returnPrice.equals("-") ? "0" : String.format("%,.2f", Float.parseFloat(returnPrice));
        this.cashCollection = cashCollection == null || cashCollection.equals("-") ? "0" : String.format("%,.2f", Float.parseFloat(cashCollection));
        this.cashReturn = cashReturn == null || cashReturn.equals("-") ? "0" : String.format("%,.2f", Float.parseFloat(cashReturn));
        this.cashWaiveOff = cashWaiveOff == null || cashWaiveOff.equals("-") ? "0" : String.format("%,.2f", Float.parseFloat(cashWaiveOff));

        this.btnEdit = new JFXButton();
        this.btnDelete = new JFXButton();
        this.btnEdit.setGraphic(GlobalVariables.createEditIcon());
        this.btnDelete.setGraphic(GlobalVariables.createDeleteIcon());
        this.btnEdit.getStyleClass().add("btn_icon");
        this.btnDelete.getStyleClass().add("btn_icon");
        this.operationsPane = new HBox(btnEdit, btnDelete);
    }


    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getBookings() {
        return bookings;
    }

    public void setBookings(String bookings) {
        this.bookings = bookings;
    }

    public String getDelivered() {
        return delivered;
    }

    public void setDelivered(String delivered) {
        this.delivered = delivered;
    }

    public String getItemsReturned() {
        return itemsReturned;
    }

    public void setItemsReturned(String itemsReturned) {
        this.itemsReturned = itemsReturned;
    }

    public String getBookingPrice() {
        return bookingPrice;
    }

    public void setBookingPrice(String bookingPrice) {
        this.bookingPrice = bookingPrice;
    }

    public String getReturnPrice() {
        return returnPrice;
    }

    public void setReturnPrice(String returnPrice) {
        this.returnPrice = returnPrice;
    }

    public String getCashCollection() {
        return cashCollection;
    }

    public void setCashCollection(String cashCollection) {
        this.cashCollection = cashCollection;
    }

    public String getCashReturn() {
        return cashReturn;
    }

    public void setCashReturn(String cashReturn) {
        this.cashReturn = cashReturn;
    }

    public String getCashWaiveOff() {
        return cashWaiveOff;
    }

    public void setCashWaiveOff(String cashWaiveOff) {
        this.cashWaiveOff = cashWaiveOff;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public ArrayList<ArrayList<String>> getSaleReportDetail(Statement stmt1, Statement stmt2, Statement stmt3, Connection con) throws ParseException {
        ArrayList<ArrayList<String>> userReportData1 = new ArrayList<ArrayList<String>>();
        ArrayList<ArrayList<String>> userReportData2 = new ArrayList<ArrayList<String>>();
        ArrayList<ArrayList<String>> userReportData3 = new ArrayList<ArrayList<String>>();
        ArrayList<ArrayList<String>> userReportData4 = new ArrayList<ArrayList<String>>();
        ArrayList<ArrayList<String>> userReportData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs1 = null;
        ResultSet rs2 = null;
        ResultSet rs3 = null;
        try {
            rs1 = stmt1.executeQuery("SELECT `booking_date`, COUNT(`order_id`), ROUND(SUM(`final_price`),2) FROM `order_info` WHERE `status` != 'Deleted' GROUP BY `booking_date` ORDER BY str_to_date(`booking_date`, '%d/%b/%Y') DESC");
            while (rs1.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs1.getString(1)); // Booking Date
                temp.add(rs1.getString(2)); // Bookings
                temp.add(rs1.getString(3)); // Bookings Price
                userReportData1.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            rs2 = stmt2.executeQuery("SELECT `delivered_date`, COUNT(`order_id`) FROM `order_info` WHERE `status` != 'Deleted' AND `status` != 'Returned' GROUP BY `delivered_date` ORDER BY str_to_date(`delivered_date`, '%d/%b/%Y') DESC");
            while (rs2.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs2.getString(1)); // Delivered Date
                temp.add(rs2.getString(2)); // Delivered
                userReportData2.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            rs3 = stmt3.executeQuery("SELECT `order_return`.`entry_date`, SUM(`order_return_detail`.`prod_quant`) as 'items_returned', ROUND(SUM(`order_return`.`return_total_price`),2) as 'return_price' FROM `order_return_detail` INNER JOIN `order_return` ON `order_return`.`return_id` = `order_return_detail`.`return_id` WHERE `order_return`.`status` != 'Deleted' GROUP BY `order_return`.`entry_date` ORDER BY str_to_date(`order_return`.`entry_date`, '%d/%b/%Y') DESC");
            while (rs3.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs3.getString(1)); // Return Date
                temp.add(rs3.getString(2)); // Product Quantity
                temp.add(rs3.getString(3)); // Return Price
                userReportData3.add(temp);
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            rs3 = stmt3.executeQuery("SELECT `dealer_payments`.`date`, `dealer_payments`.`amount`, `dealer_payments`.`cash_type` FROM `dealer_payments` WHERE `dealer_payments`.`status` != 'Deleted' ORDER BY str_to_date(`date`, '%d/%b/%Y') DESC");
            while (rs3.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs3.getString(1)); // Payment Date
                temp.add(rs3.getString(2)); // Amount
                temp.add(rs3.getString(3)); // Payment Type
                userReportData4.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        String stDateBooking = "";
        String stDateDelivered = "";
        String stDateReturn = "";
        String stDatePayment = "";
        Date dateBooking = null;
        Date dateDelivered = null;
        Date dateReturn = null;
        Date datePayment = null;
        ArrayList<Date> objDatesArray;
        int day;
        String stDay = "";
        boolean newDataAdd = true;

        while (userReportData1.size() > 0 || userReportData2.size() > 0 || userReportData3.size() > 0 || userReportData4.size() > 0)
        {
            temp = new ArrayList<>();
            objDatesArray = new ArrayList<>();
            if(userReportData1.size() > 0)
            {
                stDateBooking = userReportData1.get(0).get(0);
                dateBooking = fmt.parse(stDateBooking);
                objDatesArray.add(dateBooking);
            }
            if(userReportData2.size() > 0)
            {
                stDateDelivered = userReportData2.get(0).get(0);
                if(stDateDelivered != null && !stDateDelivered.equals(""))
                {
                    dateDelivered = fmt.parse(stDateDelivered);
                    objDatesArray.add(dateDelivered);
                }
                else
                {
                    userReportData2.remove(0);
                }
            }
            if(userReportData3.size() > 0)
            {
                stDateReturn = userReportData3.get(0).get(0);
                dateReturn = fmt.parse(stDateReturn);
                objDatesArray.add(dateReturn);
            }
            if(userReportData4.size() > 0)
            {
                stDatePayment = userReportData4.get(0).get(0);
                datePayment = fmt.parse(stDatePayment);
                objDatesArray.add(datePayment);
            }
            Date maxDate = Collections.max(objDatesArray);
            day = maxDate.getDay();
            if(day == 0)
                stDay = "Sunday";
            else if(day == 1)
                stDay = "Monday";
            else if(day == 2)
                stDay = "Tuesday";
            else if(day == 3)
                stDay = "Wednesday";
            else if(day == 4)
                stDay = "Thursday";
            else if(day == 5)
                stDay = "Friday";
            else if(day == 6)
                stDay = "Saturday";
            if(dateBooking != null && maxDate.equals(dateBooking) && userReportData1.size() > 0)
            {
                if(userReportData.size() > 0 && userReportData.get(userReportData.size()-1).get(0).equals(stDateBooking))
                {
                    newDataAdd = false;
                    if(userReportData.get(userReportData.size()-1).get(2).equals("-"))
                    {
                        userReportData.get(userReportData.size()-1).set(2, userReportData1.get(0).get(1));
                        userReportData.get(userReportData.size()-1).set(5, userReportData1.get(0).get(2));
                    }
                    else
                    {
                        String value1 = userReportData.get(userReportData.size()-1).get(2);
                        String value2 = userReportData.get(userReportData.size()-1).get(5);
                        userReportData.get(userReportData.size()-1).set(2, String.valueOf(Integer.parseInt(value1)+Integer.parseInt(userReportData1.get(0).get(1))));
                        userReportData.get(userReportData.size()-1).set(5, String.valueOf(Integer.parseInt(value2)+Integer.parseInt(userReportData1.get(0).get(2))));
                    }
                }
                else
                {
                    newDataAdd = true;
                    temp.add(stDateBooking);
                    temp.add(stDay);
                    temp.add(userReportData1.get(0).get(1));
                    temp.add("-");
                    temp.add("-");
                    temp.add(userReportData1.get(0).get(2));
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                }
                userReportData1.remove(0);
            }
            else if(dateDelivered != null && maxDate.equals(dateDelivered) && userReportData2.size() > 0)
            {
                if(userReportData.size() > 0 && userReportData.get(userReportData.size()-1).get(0).equals(stDateDelivered))
                {
                    newDataAdd = false;
                    if(userReportData.get(userReportData.size()-1).get(3).equals("-"))
                    {
                        userReportData.get(userReportData.size()-1).set(3, userReportData2.get(0).get(1));
                    }
                    else
                    {
                        String value = userReportData.get(userReportData.size()-1).get(3);
                        userReportData.get(userReportData.size()-1).set(3, String.valueOf(Integer.parseInt(value)+Integer.parseInt(userReportData2.get(0).get(1))));
                    }
                }
                else
                {
                    newDataAdd = true;
                    temp.add(stDateDelivered);
                    temp.add(stDay);
                    temp.add("-");
                    temp.add(userReportData2.get(0).get(1));
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                }
                userReportData2.remove(0);
            }
            else if(dateReturn != null && maxDate.equals(dateReturn) && userReportData3.size() > 0)
            {
                if(userReportData.size() > 0 && userReportData.get(userReportData.size()-1).get(0).equals(stDateReturn))
                {
                    newDataAdd = false;
                    if(userReportData.get(userReportData.size()-1).get(4).equals("-"))
                    {
                        userReportData.get(userReportData.size()-1).set(4, userReportData3.get(0).get(1));
                        userReportData.get(userReportData.size()-1).set(6, userReportData3.get(0).get(2));
                    }
                    else
                    {
                        String value1 = userReportData.get(userReportData.size()-1).get(4);
                        String value2 = userReportData.get(userReportData.size()-1).get(6);
                        userReportData.get(userReportData.size()-1).set(4, String.valueOf(Integer.parseInt(value1)+Integer.parseInt(userReportData3.get(0).get(1))));
                        userReportData.get(userReportData.size()-1).set(6, String.valueOf(Integer.parseInt(value2)+Integer.parseInt(userReportData3.get(0).get(2))));
                    }
                }
                else
                {
                    newDataAdd = true;
                    temp.add(stDateReturn);
                    temp.add(stDay);
                    temp.add("-");
                    temp.add("-");
                    temp.add(userReportData3.get(0).get(1));
                    temp.add("-");
                    temp.add(userReportData3.get(0).get(2));
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                }
                userReportData3.remove(0);
            }
            else if (datePayment != null && maxDate.equals(datePayment) && userReportData4.size() > 0)
            {
                if(userReportData.size() > 0 && userReportData.get(userReportData.size()-1).get(0).equals(stDatePayment))
                {
                    newDataAdd = false;
                    if(userReportData4.get(0).get(2).equals("Collection"))
                    {
                        if(userReportData.get(userReportData.size()-1).get(7).equals("-"))
                        {
                            userReportData.get(userReportData.size()-1).set(7, userReportData4.get(0).get(1));
                        }
                        else
                        {
                            String value = userReportData.get(userReportData.size()-1).get(7);
                            float val1 = Float.parseFloat(value);
                            float val2 = Float.parseFloat(userReportData4.get(0).get(1));
                            float totalVal = val1+val2;
                            userReportData.get(userReportData.size()-1).set(7, String.valueOf(totalVal));
                        }
                    }
                    else if (userReportData4.get(0).get(2).equals("Return"))
                    {
                        if(userReportData.get(userReportData.size()-1).get(8).equals("-"))
                        {
                            userReportData.get(userReportData.size()-1).set(8, userReportData4.get(0).get(1));
                        }
                        else
                        {
                            String value = userReportData.get(userReportData.size()-1).get(8);
                            userReportData.get(userReportData.size()-1).set(8, String.valueOf(Float.parseFloat(value)+Float.parseFloat(userReportData4.get(0).get(1))));
                        }
                    }
                    else if (userReportData4.get(0).get(2).equals("Waive Off"))
                    {
                        if(userReportData.get(userReportData.size()-1).get(9).equals("-"))
                        {
                            userReportData.get(userReportData.size()-1).set(9, userReportData4.get(0).get(1));
                        }
                        else
                        {
                            String value = userReportData.get(userReportData.size()-1).get(9);
                            userReportData.get(userReportData.size()-1).set(9, String.valueOf(Integer.parseInt(value)+Integer.parseInt(userReportData4.get(0).get(1))));
                        }
                    }
                }
                else
                {
                    newDataAdd = true;
                    temp.add(stDatePayment);
                    temp.add(stDay);
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    if(userReportData4.get(0).get(2).equals("Collection"))
                    {
                        temp.add(userReportData4.get(0).get(1));
                        temp.add("-");
                        temp.add("-");
                    }
                    else if (userReportData4.get(0).get(2).equals("Return"))
                    {
                        temp.add("-");
                        temp.add(userReportData4.get(0).get(1));
                        temp.add("-");
                    }
                    else if (userReportData4.get(0).get(2).equals("Waive Off"))
                    {
                        temp.add("-");
                        temp.add("-");
                        temp.add(userReportData4.get(0).get(1));
                    }
                }
                userReportData4.remove(0);
            }
            if(newDataAdd)
            {
                userReportData.add(temp);
            }
            else
            {
                newDataAdd = true;
            }
        }

        return userReportData;
    }

    private Node createEditIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.EDIT);
        graphic.setFontSize(20.0);
        return graphic;
    }

    private Node createDeleteIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.TRASH);
        graphic.setFontSize(20.0);
        return graphic;
    }
}
