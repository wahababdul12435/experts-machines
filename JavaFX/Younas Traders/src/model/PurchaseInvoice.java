package model;

import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import controller.RateChange;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.layout.StackPane;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class PurchaseInvoice {

    public static StackPane changeRatePane;
    public static JFXDialog dialog;


    public void rateChange(int prodIDs)
    {
        RateChange.product_ID = prodIDs;
        Parent parent = null;
        try {
            parent = FXMLLoader.load(getClass().getResource("/view/rate_change.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setStyle("-fx-padding: -20 -10 -20 -10;");
        //dialogLayout.setHeading(lblUpdate);
        dialogLayout.setBody(parent);
        dialog = new JFXDialog(changeRatePane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        changeRatePane.setVisible(true);
        dialog.show();
        dialog.setOverlayClose(false);
    }

    int limit = 1;
    String[] suppName_enterpressID = new String[1];
    public String[] get_SuppDetails(Statement stmt3, Connection con3, String suppID_enterpress )
    {
        ResultSet rs3 = null;
        try {
            rs3 = stmt3.executeQuery("select supplier_name from supplier_info where supplier_id = '"+suppID_enterpress+"' ");
            int i = 0;
            while (rs3.next() && i < limit)
            {

                suppName_enterpressID[i] = rs3.getString(1);
                i++;
            }
            con3.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return suppName_enterpressID;
    }

    String[] suppName_enterpressName = new String[1];
    int drpdown = 15;
    public String[] getSuppDetails_SupName(Statement stmt3, Connection con3, String suppName_enterpress )
    {
        ResultSet rs3 = null;
        try {
            rs3 = stmt3.executeQuery("select supplier_id from supplier_info where supplier_name = '"+suppName_enterpress+"' ");
            int i = 0;
            while (rs3.next() && i < limit)
            {

                suppName_enterpressName[i] = rs3.getString(1);
                i++;
            }
            con3.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return suppName_enterpressName;
    }

    ArrayList<String> prodctlist = new ArrayList<>();
    public ArrayList<String> get_Allprodcuts(Statement stmt2, Connection con2)
    {
        ResultSet rs = null;
        try {
            rs = stmt2.executeQuery("select product_name from product_info where product_status='Active'  order by  product_name ASC ");
            int i = 0;
            while (rs.next() && i < drpdown)
            {
                prodctlist.add(i,rs.getString(1));
                i++;
            }

            con2.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return prodctlist;
    }

    ArrayList<String> Supplierlist = new ArrayList<>();
    public ArrayList<String> get_AllSuppliers(Statement stmt2, Connection con2)
    {
        ResultSet rs = null;
        try {
            rs = stmt2.executeQuery("select supplier_name from supplier_info where supplier_status='Active'  order by  supplier_name ASC ");
            int i = 0;
            while (rs.next() && i < drpdown)
            {
                Supplierlist.add(i,rs.getString(1));
                i++;
            }

            con2.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Supplierlist;
    }

    public void insertnewbatch_ifnotavlbl(Statement stmt, Connection con, int productsid , String batchs, int quantity , int bonus,String retailP, String tradeP,String PurchaseP, String batchexpirys, String entrydate,String enteryTime)
    {
        try {
            String insertQuery = "INSERT INTO `batchwise_stock` (`prod_id`,`batch_no` , `quantity`  ,`bonus` , `retail_price` , `trade_price` , `purchase_price` , `batch_expiry` , `entry_date` , `entry_time` ) VALUES   ('"+productsid+"' , '"+batchs+"' , '"+quantity+"' , '"+bonus+"' ,'"+retailP+"' , '"+tradeP+"' , '"+PurchaseP+"' , '"+batchexpirys+"', '"+entrydate+"','"+enteryTime+"')";
            stmt.executeUpdate(insertQuery);
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    ///////////////////////////////////////////////INSERT details of PURCHASE INVOICE in PURCHASE_INFO_DETAIL TABLE
    public void insertdata_RowWise(Statement stmt, Connection con,String invonumber , int purchasedID  , int prodID,float discountVal,int bonusquant , int recievedquant, String batchno, String espiry_dates,float grossamounts,float discamounts,float total_Amounts,String Invodates,int retrnBits ,int retrnquants ,int retrnbonusquant)
    {
        try {
            String insertQuery = "INSERT INTO `purchase_info_detail` ( `invoice_num` , `purchase_id`   , `prod_id` , `discount`, `bonus_quant`,`recieve_quant` , `batch_no`  ,`expiry_date`  ,`gross_amount` , `disc_amount`, `net_amount` , `invoice_date` , `returnBit`  , `returned_quant`  , `returned_bonus_quant` ) VALUES   ('"+invonumber+"','"+purchasedID+"','"+prodID+"','"+discountVal+"','"+bonusquant+"' , '"+recievedquant+"' , '"+batchno+"' , '"+espiry_dates+"', '"+grossamounts+"', '"+discamounts+"' , '"+total_Amounts+"', '"+Invodates+"' , '"+retrnBits+"' , '"+retrnquants+"' , '"+retrnbonusquant+"')";
            stmt.executeUpdate(insertQuery);  //
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    ///////////////////////////////////////////////INSERT VALUES IN PURCHASE_INFO TABLE
    public void insertpurchInvoiceInfo(Statement stmt, Connection con,int purchid,int companiesID , String invonum, int suplierid , String purchdate,float grossAmount,float discountedAmount, float totalamount,String dates,String Times,String update_date,String update_time,int usersID)
    {
        String statusValue = "Active";
        try {
            String insertQuery = "INSERT INTO `purchase_info` ( `purchase_id`,`comp_id`,`invoice_num` , `supplier_id`   ,`purchase_date` ,`gross_amount` ,`disc_amount` , `net_amount` , `entry_date`, `entry_time`, `update_date`, `update_time`, `user_id`, `status` ) VALUES   ('"+purchid+"' ,'"+companiesID+"' ,'"+invonum+"' , '"+suplierid+"' , '"+purchdate+"', '"+grossAmount+"', '"+discountedAmount+"' , '"+totalamount+"', '"+dates+"' ,'"+Times+"' , '"+update_date+"' , '"+update_time+"','"+usersID+"','"+statusValue+"' )";
            stmt.executeUpdate(insertQuery);  //
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    ///////////////////////////////////////////////INSERT VALUES IN PRODUCT STOCK TABLE
    public  void insertprodQUANT_ifnotavlbl(Statement stmt, Connection con,int productsid , int quantity , int bonus, String statuses)
    {
        int totalQty = quantity + bonus;
        int minvalue = 0;
        int maxvalue = 0;
        try {
            String insertQuery = "INSERT INTO `products_stock` ( `product_table_id`,`in_stock`,`min_stock`,`max_stock` , `bonus_quant`  ,`status`  ) VALUES   ('"+productsid+"' , '"+totalQty+"' ,'"+minvalue+"','"+maxvalue+"', '0' , '"+statuses+"' )";
            stmt.executeUpdate(insertQuery);  //
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


    /////////////////////////////////////////////////////////////START OF FUNCTION TO CHANGE QUANTITIES FROM BATCHWISE STOCK TABLE
    public void Addition_BatchStock(Statement stmt, Connection con,int productid , int quantity , int bonus, String proddbatch)
    {
        try {
            String insertQuery = "UPDATE `batchwise_stock` SET `quantity`='"+quantity+"',`bonus`='"+bonus+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `prod_id` = '"+productid+"' and  `batch_no` = '"+proddbatch+"'";
            stmt.executeUpdate(insertQuery);
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    /////////////////////////////////////////////////////////////END OF FUNCTION TO CHANGE QUANTITIES FROM BATCHWISE STOCK TABLE
    /////////////////////////////////////////////////////////////START OF FUNCTION TO ADD QUANTITIES IN PRODUCT STOCK TABLE
    public void addtition_quantFROMStock(Statement stmt, Connection con,int prodID , int prodQuant, int prodbonus){
        try {
            int totalQty = prodQuant + prodbonus;
            String insertQuery = "UPDATE `products_stock` SET `in_stock`='"+totalQty+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `product_id` = '"+prodID+"'";
            stmt.executeUpdate(insertQuery);
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    /////////////////////////////////////////////////////////////END OF FUNCTION TO ADD QUANTITIES IN PRODUCT STOCK TABLE

    String[] AlrdyExst_InvoNum = new String[1];
    public String[] cnfrmInvoNum_already(Statement stmt, Connection con,String purcha_InvoNum )
    {
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("select purchase_id from purchase_info where invoice_num = '"+purcha_InvoNum+"' ");
            int i = 0;
            while (rs.next() && i < limit)
            {

                AlrdyExst_InvoNum[i] = rs.getString(1) ;
            }

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return AlrdyExst_InvoNum;
    }
    /////////////////////////////////////////////////////////////START OF FUNCTION TO GET QUANTITIES BATCH WISE FROM BATCHWISE STOCK TABLE
    String[] quantity_of_batch = new String[1];
    public String[] batch_quant(Statement stmt, Connection con,int productsID ,String productsbatch)
    {
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("select quantity,bonus from batchwise_stock where prod_id = '"+productsID+"' and batch_no = '"+productsbatch+"'");
            int i = 0;
            while (rs.next() && i < limit)
            {

                quantity_of_batch[i] = rs.getString(1) + "--" + rs.getString(2) ;
            }

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return quantity_of_batch;
    }
    /////////////////////////////////////////////////////////////END OF FUNCTION TO GET QUANTITIES BATCH WISE FROM BATCHWISE STOCK TABLE


    String[] stockquantity_enterpressID = new String[1];
    public String[] getprodstockquant_withprodID(Statement stmt, Connection con, int product_Ids)
    {
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("select in_stock,bonus_quant from products_stock where product_id = '"+product_Ids+"' and status = 'Active'");
            int i = 0;
            while (rs.next() && i < limit)
            {

                stockquantity_enterpressID[i] = rs.getString(1) + "--" + rs.getString(2) ;
            }

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return stockquantity_enterpressID;
    }

    String[] purch_ID = new String[1];
    public String[] getlatest_purchID(Statement stmt, Connection con)
    {
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("select purchase_id from purchase_info order by purchase_id DESC LIMIT 1");
            int i = 0;
            while (rs.next() && i < limit)
            {

                purch_ID[i] = rs.getString(1);
            }

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return purch_ID;
    }


    String batchSelectionpolicy;
    public String getbatchSelectionpolicy(Statement stmt2, Connection con2)
    {
        ResultSet rs = null;
        try {
            rs = stmt2.executeQuery("select setting_value from system_settings where setting_id=1");
            while (rs.next())
            {
                batchSelectionpolicy= rs.getString(1) ;
            }

            con2.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return batchSelectionpolicy;
    }
    ArrayList<String> prod_otherbatch = new ArrayList<>();
    public  ArrayList<String> getprod_otherbatchs(Statement stmt2, Connection con2, int product_IDS)
    {
        ResultSet rs = null;
        try {
            rs = stmt2.executeQuery("select batch_no,entry_date,entry_time,batch_expiry from batchwise_stock where product_table_id ='"+product_IDS+"' and quantity>=0");
            int i = 0;
            while (rs.next() && i < limit)
            {

                prod_otherbatch.add( i,rs.getString(1) + "--" + rs.getString(2) + "--" + rs.getString(3) + "--" + rs.getString(4));
                i++;
            }

            con2.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return prod_otherbatch;
    }


    String prodbatchs_entry;
    public String getprodbatch_entry(Statement stmt2, Connection con2, int product_IDS,String bacthentrydate,String bacthentrytime)
    {
        ResultSet rs = null;
        try {
            rs = stmt2.executeQuery("select batch_no,quantity from batchwise_stock where prod_id ='"+product_IDS+"' and entry_date='"+bacthentrydate+"' and entry_time='"+bacthentrytime+"' ");
            int i = 0;
            while (rs.next() && i < limit)
            {

                prodbatchs_entry = rs.getString(1)+"-"+rs.getString(2) ;
            }

            con2.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return prodbatchs_entry;
    }

    String prodbatchs_expiry;
    public String getprodbatch_expiry(Statement stmt2, Connection con2, int product_IDS,String bacthexpirydate,String bacthentrytime)
    {
        ResultSet rs = null;
        try {
            rs = stmt2.executeQuery("select batch_no from batchwise_stock where prod_id ='"+product_IDS+"' and batch_expiry='"+bacthexpirydate+"' and entry_time='"+bacthentrytime+"' ");
            int i = 0;
            while (rs.next() && i < limit)
            {

                prodbatchs_expiry = rs.getString(1) ;
            }

            con2.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return prodbatchs_expiry;
    }

    String[] prod_details = new String[1];
    public String[] get_prodDetailswithIDs(Statement stmt, Connection con,  int prods_Ids)
    {
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("select product_name,retail_price,purchase_price,trade_price,company_table_id from product_info where  product_id = '"+prods_Ids+"'");
            int i = 0;
            while (rs.next() && i < limit)
            {

                prod_details[i] = rs.getString(1) + "--" + rs.getString(2) + "--" + rs.getString(3) + "--" + rs.getString(4) + "--" + rs.getString(5);
            }

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return prod_details;
    }

    String[] proddetails_withbatch = new String[1];
    public String[] get_prodDetailswithID_batch(Statement stmt, Connection con,  int prods_Ids,String batch_numb)
    {
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("select product_info.product_name,batchwise_stock.retail_price,batchwise_stock.purchase_price,batchwise_stock.trade_price,product_info.company_table_id from product_info inner join batchwise_stock on product_info.product_id = `batchwise_stock`.`product_table_id` where product_info.product_id = '"+prods_Ids+"' and batchwise_stock.batch_no = '"+batch_numb+"' ");
            int i = 0;
            while (rs.next() && i < limit)
            {

                proddetails_withbatch[i] = rs.getString(1) + "--" + rs.getString(2) + "--" + rs.getString(3) + "--" + rs.getString(4) + "--" + rs.getString(5);
            }

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return proddetails_withbatch;
    }
}
