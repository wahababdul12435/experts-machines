package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import controller.CreateSaleInvoice;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class CreateSaleInvoiceInfo {
    private String srNo;
    private String orderId;
    private String productId;
    private String productName;
    private String batchNo;
    private Label batchExpiry;
    private Label orderedQuantity;
    private String bonusQuantity;
    private String productPrice;
    private String discountGiven;

    private HBox operationsPane;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    public static ArrayList<String> productIdArr = new ArrayList<>();
    public static ArrayList<String> productNameArr = new ArrayList<>();
    public static ArrayList<String> batchIdArr = new ArrayList<>();
    public static ArrayList<String> batchNoArr = new ArrayList<>();
    public static ArrayList<String> batchExpiryArr = new ArrayList<>();
    public static ArrayList<Boolean> nearExpiryArr = new ArrayList<>();
    public static ArrayList<String> orderedQuantityArr = new ArrayList<>();
    public static ArrayList<String> submissionQuantityArr = new ArrayList<>();
    public static ArrayList<String> bonusQuantityArr = new ArrayList<>();
    public static ArrayList<String> productPriceArr = new ArrayList<>();
    public static ArrayList<String> finalPriceArr = new ArrayList<>();
    public static ArrayList<String> discountGivenArr = new ArrayList<>();

    public static TableView<CreateSaleInvoiceInfo> table_saledetaillog;

    public static JFXTextField txtProductName;
    public static JFXComboBox<String> txtBatchNo;
    public static JFXTextField txtQuantity;
    public static JFXTextField txtBonusPack;

    public static Button btnAdd;
    public static Button btnCancel;
    public static Button btnUpdate;

    public static Label lblProducts;
    public static Label lblItemsOrdered;
    public static Label lblItemsMissed;
    public static Label lblNearExpiry;
    public static Label lblBonusItems;

    public static Label lbl_dealerBalance;
    public static Label lbl_dealerLastInvNum;
    public static Label lbl_dealerLastinvValue;

    public static Label lbl_prodBalance;
    public static Label lbl_batchQuant;
    public static Label lbl_nextbatch;

    public CreateSaleInvoiceInfo() {
    }

    public CreateSaleInvoiceInfo(String srNo, String productName, String batchNo, String batchExpiry, String orderedQuantity, String bonusQuantity, String productPrice, String discountGiven) {
        this.srNo = srNo;
        this.productName = productName;
        this.batchNo = batchNo;

        if(batchExpiry != null && !batchExpiry.equals("") && !batchExpiry.equals("N/A"))
        {
            String stEndDate = batchExpiry;
            String stStartDate = GlobalVariables.getStDate();

            Date endDate = null;
            Date startDate = null;
            try {
                startDate = GlobalVariables.fmt.parse(stStartDate);
                endDate = GlobalVariables.fmt.parse(stEndDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            long diff = endDate.getTime() - startDate.getTime();
            diff = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);

            if(diff >= 180)
            {
                this.batchExpiry = new Label(batchExpiry);
            }
            else if(diff < 0)
            {
                this.batchExpiry = new Label(batchExpiry+"\n(EXPIRED)");
                this.batchExpiry.setStyle("-fx-background-color: orange;");
            }
            else
            {
                this.batchExpiry = new Label(batchExpiry+"\n(Days Left: "+diff+")");
                this.batchExpiry.setStyle("-fx-background-color: yellow;");
            }
        }
        else
        {
            this.batchExpiry = new Label("N/A");
        }

        this.batchExpiry.setPrefWidth(100);
        this.batchExpiry.setAlignment(Pos.CENTER);

        this.orderedQuantity = new Label (orderedQuantity);

        this.bonusQuantity = bonusQuantity;
        this.productPrice = productPrice;
        this.discountGiven = discountGiven;

        this.btnEdit = new JFXButton();
        this.btnDelete = new JFXButton();
        this.btnEdit.setGraphic(GlobalVariables.createEditIcon());
        this.btnDelete.setGraphic(GlobalVariables.createDeleteIcon());
        this.btnEdit.getStyleClass().add("btn_icon");
        this.btnDelete.getStyleClass().add("btn_icon");
        this.operationsPane = new HBox(btnEdit, btnDelete);
        this.btnEdit.setOnAction((action)->editAddedRecord());
        this.btnDelete.setOnAction((action)->deleteAddedRecord());
    }

    private void editAddedRecord()
    {
        CreateSaleInvoice.srNo = Integer.parseInt(srNo)-1;
        txtProductName.setText(productNameArr.get(Integer.parseInt(srNo)-1));
        txtBatchNo.getItems().clear();
        String productId = CreateSaleInvoice.getProductId(CreateSaleInvoice.completeProductData, txtProductName.getText());
        ArrayList<String> batchData = CreateSaleInvoice.getProductsBatch(CreateSaleInvoice.completeBatchData, productId);
        txtBatchNo.getItems().addAll(batchData);
        txtBatchNo.setValue(batchNoArr.get(Integer.parseInt(srNo)-1));
        txtQuantity.setText(orderedQuantityArr.get(Integer.parseInt(srNo)-1));
        txtBonusPack.setText(bonusQuantityArr.get(Integer.parseInt(srNo)-1));
        btnAdd.setVisible(false);
        btnCancel.setVisible(true);
        btnUpdate.setVisible(true);
    }

    private void deleteAddedRecord()
    {
        CreateSaleInvoice.totalProducts--;
        CreateSaleInvoice.orderedItems -= Integer.parseInt(orderedQuantityArr.get(Integer.parseInt(srNo)-1).replace(",", ""));
        if(!orderedQuantityArr.get(Integer.parseInt(srNo)-1).equals(submissionQuantityArr.get(Integer.parseInt(srNo)-1)))
        {
            int value = Integer.parseInt(orderedQuantityArr.get(Integer.parseInt(srNo)-1).replace(",", "")) - Integer.parseInt(submissionQuantityArr.get(Integer.parseInt(srNo)-1).replace(",", ""));
            CreateSaleInvoice.missedItems -= value;
        }
        if(nearExpiryArr.get(Integer.parseInt(srNo)-1))
        {
            CreateSaleInvoice.nearExpiry--;
        }
        CreateSaleInvoice.bonusItems -= Integer.parseInt(bonusQuantityArr.get(Integer.parseInt(srNo)-1).replace(",", ""));
        productIdArr.remove(Integer.parseInt(srNo)-1);
        productNameArr.remove(Integer.parseInt(srNo)-1);
        batchIdArr.remove(Integer.parseInt(srNo)-1);
        batchNoArr.remove(Integer.parseInt(srNo)-1);
        batchExpiryArr.remove(Integer.parseInt(srNo)-1);
        orderedQuantityArr.remove(Integer.parseInt(srNo)-1);
        submissionQuantityArr.remove(Integer.parseInt(srNo)-1);
        bonusQuantityArr.remove(Integer.parseInt(srNo)-1);
        productPriceArr.remove(Integer.parseInt(srNo)-1);
        finalPriceArr.remove(Integer.parseInt(srNo)-1);
        discountGivenArr.remove(Integer.parseInt(srNo)-1);
        table_saledetaillog.setItems(CreateSaleInvoice.parseUserList());
    }

    public ArrayList<ArrayList<String>> gettableID(Statement stmt, Connection con,String prodCode)
    {

        ArrayList<ArrayList<String>> table = new ArrayList<ArrayList<String>>();
        String prodTableId="";
        ResultSet rs3 = null;
        try {
            rs3 = stmt.executeQuery("select batch_stock_id,batch_no,quantity,trade_price,entry_date,batch_expiry from batchwise_stock where product_table_id = '"+prodCode+"' ");
            int i = 0;
            while (rs3.next()) {
                ArrayList<String> getdata = new ArrayList<String>();
                getdata.add( rs3.getString(1));
                getdata.add( rs3.getString(2));
                getdata.add( rs3.getString(3));
                getdata.add( rs3.getString(4));
                getdata.add( rs3.getString(5));
                getdata.add( rs3.getString(6));
                table.add(getdata);
            }
            con.close();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return table;
    }

    public ArrayList<String> getprodRates(Statement stmt, Connection con,String prodName)
    {
        StringBuffer PN = new StringBuffer(prodName);
        int chkBit = 0;
        for(int q=1;q<PN.length();q++)
        {
            char mmn =  PN.charAt(PN.length()-1);
            if(mmn!='(')
            {
                if(chkBit==0)
                {
                    PN.deleteCharAt(PN.length()-1);
                }
            }
            else
            {
                chkBit=1;
            }

        }
        if(PN.charAt(PN.length()-1)=='(')
        {
            PN.deleteCharAt(PN.length()-1);
        }
        if(PN.charAt(PN.length()-1)==' ')
        {
            PN.deleteCharAt(PN.length()-1);
        }
        String onlyProdName = PN.toString();
        ArrayList<String> table = new ArrayList<String>();
        ResultSet rs3 = null;
        try {
            rs3 = stmt.executeQuery("select retail_price,trade_price,purchase_price,purchase_discount from product_info where product_name = '"+onlyProdName+"' ");
            int i = 0;
            while (rs3.next()) {
                table.add( rs3.getString(1));
                table.add( rs3.getString(2));
                table.add( rs3.getString(3));
                table.add( rs3.getString(4));
            }
            con.close();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return table;
    }

    public ArrayList<String> getprodQuantID(Statement stmt, Connection con,String prodCode)
    {
        ArrayList<String> getdata = new ArrayList<String>();
        String prodTableId="";
        ResultSet rs3 = null;
        try {
            rs3 = stmt.executeQuery("select in_stock,bonus_quant from products_stock where product_table_id = '"+prodCode+"' ");
            int i = 0;
            while (rs3.next()) {

                getdata.add( rs3.getString(1));
                getdata.add( rs3.getString(2));

            }
            con.close();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return getdata;
    }


    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    public Label getBatchExpiry() {
        return batchExpiry;
    }

    public void setBatchExpiry(Label batchExpiry) {
        this.batchExpiry = batchExpiry;
    }

    public Label getOrderedQuantity() {
        return orderedQuantity;
    }

    public void setOrderedQuantity(Label orderedQuantity) {
        this.orderedQuantity = orderedQuantity;
    }

    public String getBonusQuantity() {
        return bonusQuantity;
    }

    public void setBonusQuantity(String bonusQuantity) {
        this.bonusQuantity = bonusQuantity;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getDiscountGiven() {
        return discountGiven;
    }

    public void setDiscountGiven(String discountGiven) {
        this.discountGiven = discountGiven;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public ArrayList<ArrayList<String>> loaddealerPrevdata(int dealers_IDs,Statement stmt)
    {
        CreateSaleInvoice.dealerBalance = 0;
        CreateSaleInvoice.dealerLastInvNum = 0;
        CreateSaleInvoice.dealerLastinvValue = 0;
        ArrayList<ArrayList<String>> dealersData = new ArrayList<ArrayList<String>>();

        String tempDealerID ="";
        ResultSet rs1 = null;
        String query1 = "SELECT `dealer_table_id` from dealer_info where `dealer_id` = '"+dealers_IDs+"' ";
        try {
            rs1 = stmt.executeQuery(query1);
            while (rs1.next())
            {

                tempDealerID = (rs1.getString(1)); // dealer table Id
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        String temp ="";
        String temporder_id ="";
        ResultSet rs = null;
        ResultSet r_s = null;
        String query = "SELECT `pending_payments` from dealer_overall_record where `dealer_id` = '"+tempDealerID+"' ";
        String getorderIDquery = "SELECT `order_id` from order_info where `dealer_id` = '"+tempDealerID+"' order by order_id DESC LIMIT 1 ";
        try {
            rs = stmt.executeQuery(query);
            while (rs.next())
            {

                temp = (rs.getString(1)); // peniding payment
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            r_s = stmt.executeQuery(getorderIDquery);
            while (r_s.next())
            {

                temporder_id = (r_s.getString(1)); // previous order id
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if(temp=="")
        {
            temp="0.0";

        }
        if(temporder_id=="")
        {
            temporder_id="0.0";

        }

        CreateSaleInvoice.dealerBalance = Float.parseFloat(temp);
        CreateSaleInvoice.dealerLastInvNum = Float.parseFloat(temporder_id);
        return dealersData;
    }
    public ArrayList<ArrayList<String>> loadSaleTable()
    {
        CreateSaleInvoice.totalProducts = 0;
        CreateSaleInvoice.orderedItems = 0;
        CreateSaleInvoice.missedItems = 0;
        CreateSaleInvoice.nearExpiry = 0;
        CreateSaleInvoice.bonusItems = 0;
        ArrayList<ArrayList<String>> saleData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        int totalSubmittedItems = 0;
        int totalMissedItems = 0;
        float finalPrice = 0;
        for(int i=0; i<productIdArr.size(); i++)
        {
            CreateSaleInvoice.totalProducts++;
            temp = new ArrayList<String>();
            temp.add(productNameArr.get(i));
            temp.add(batchNoArr.get(i));
            temp.add(batchExpiryArr.get(i));
            if(!orderedQuantityArr.get(i).equals(submissionQuantityArr.get(i)))
            {
                temp.add(orderedQuantityArr.get(i)+"\n(Qty in Stock: "+submissionQuantityArr.get(i)+")");
                totalSubmittedItems = Integer.parseInt(submissionQuantityArr.get(i));
                totalMissedItems = Integer.parseInt(orderedQuantityArr.get(i)) - Integer.parseInt(submissionQuantityArr.get(i));
                finalPrice = Float.parseFloat(finalPriceArr.get(i))/Integer.parseInt(orderedQuantityArr.get(i));
                finalPrice = finalPrice * totalSubmittedItems;
                finalPriceArr.set(i, String.valueOf(finalPrice));
            }
            else
            {
                totalSubmittedItems = Integer.parseInt(orderedQuantityArr.get(i));
                temp.add(orderedQuantityArr.get(i));
            }
            CreateSaleInvoice.orderedItems += totalSubmittedItems;
            CreateSaleInvoice.missedItems += totalMissedItems;
            if(batchExpiryArr.get(i) != null && !batchExpiryArr.get(i).equals("") && !batchExpiryArr.get(i).equals("N/A"))
            {
                temp.add(batchExpiryArr.get(i)); // Batch Expiry
                String stEndDate = batchExpiryArr.get(i);
                String stStartDate = GlobalVariables.getStDate();

                Date endDate = null;
                Date startDate = null;
                try {
                    startDate = GlobalVariables.fmt.parse(stStartDate);
                    endDate = GlobalVariables.fmt.parse(stEndDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                long diff = endDate.getTime() - startDate.getTime();
                diff = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);

                if(diff <= 180)
                {
                    CreateSaleInvoice.nearExpiry++;
                    nearExpiryArr.set(i, true);
                }
            }
            CreateSaleInvoice.bonusItems = Float.parseFloat(bonusQuantityArr.get(i));
            temp.add(bonusQuantityArr.get(i));
            temp.add(finalPriceArr.get(i));
            temp.add(discountGivenArr.get(i));
            saleData.add(temp);
        }
        return saleData;
    }

    public ArrayList<ArrayList<String>> getDealersCompleteData(Statement stmt)
    {
        ArrayList<ArrayList<String>> dealersData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        String query = "SELECT `dealer_table_id`, `dealer_id`, `dealer_name`, `dealer_phone`, `dealer_address`, `dealer_lic9_num`, `dealer_lic9_exp`, `dealer_lic10_num`, `dealer_lic10_exp`, `dealer_lic11_num`, `dealer_lic11_exp`  FROM `dealer_info` WHERE `dealer_status` != 'Deleted'";
//        System.out.println(query);
        try {
            rs = stmt.executeQuery(query);
            while (rs.next())
            {
                temp = new ArrayList<>();
                temp.add(rs.getString(1)); // Dealer Table Id
                temp.add(rs.getString(2)); // Dealer Id
                temp.add(rs.getString(3)+" ("+rs.getString(5)+")"); // Dealer Name
                temp.add(rs.getString(4)); // Dealer Contact
                temp.add(rs.getString(5)); // Dealer Address
                temp.add(rs.getString(6)); // Dealer Lic 9 Num
                temp.add(rs.getString(7)); // Dealer Lic 9 Exp
                temp.add(rs.getString(8)); // Dealer Lic 10 Num
                temp.add(rs.getString(9)); // Dealer Lic 10  Exp
                temp.add(rs.getString(10)); // Dealer Lic 11 Num
                temp.add(rs.getString(11)); // Dealer Lic 11 Exp
                dealersData.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dealersData;
    }

    public String getBatchPolicy(Statement stmt, Connection con)
    {
        String batchPolicy = "";
        ArrayList<String> temp;
        ResultSet rs = null;
        String query = "SELECT `setting_value` FROM `system_settings` WHERE `setting_name` = 'batch_policy'";
//        System.out.println(query);
        try {
            rs = stmt.executeQuery(query);
            while (rs.next())
            {
                batchPolicy = rs.getString(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return batchPolicy;
    }

    public void insertBasic(Statement stmt, String dealerId, String orderPrice, String discount, String finalPrice, String orderStatus)
    {
        String currentDate = GlobalVariables.getStDate();
        String currentTime = GlobalVariables.getStTime();
        String currentUser = GlobalVariables.getUserId();
        String updateQuery;
        String QueryString;
        String unit = "";
        String productId = "";
        String quantity = "";
        String bonus = "";
        ResultSet rs = null;
        for(int i=0; i<productIdArr.size(); i++)
        {
            if(i == productIdArr.size()-1)
            {
                productId += productIdArr;
                quantity += submissionQuantityArr;
                bonus += bonusQuantityArr;
                unit += "Packets";
            }
            else
            {
                //productId += productIdArr+"_-_";
                //quantity += submissionQuantityArr+"_-_";
                //bonus += bonusQuantityArr+"_-_";
                //unit += "Packets_-_";
            }
        }
        try {
            updateQuery = "INSERT INTO `order_info`(`dealer_id`, `product_id`, `quantity`, `unit`, `order_price`, `bonus`, `discount`, `waive_off_price`, `final_price`, `order_success`, `booking_latitude`, `booking_longitude`, `booking_area`, `booking_date`, `booking_time`, `booking_user_id`, `status`, `server_sync`) VALUES ('"+dealerId+"', '"+productId+"', '"+quantity+"', '"+unit+"', '"+orderPrice+"', '"+bonus+"', '"+discount+"', '0', '"+finalPrice+"', '1', '', '', '', '"+currentDate+"', '"+currentTime+"', '"+currentUser+"', '"+orderStatus+"', 'Insert')";
            stmt.executeUpdate(updateQuery);
            String getOrderId = "SELECT `order_id` FROM `order_info` WHERE `dealer_id` = '"+dealerId+"' AND `booking_date` = '"+currentDate+"' AND `booking_time` = '"+currentTime+"'";
            rs = stmt.executeQuery(getOrderId);
            rs.next();
            orderId = rs.getString(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ArrayList<String> tempQuant;
        int mm =0;
        String pendingPayments ="";
        float orderAmount = 0;
        float previousAmount = 0;
        float finalAmount = 0;
        try {
            String getpendingPayment = "SELECT `pending_payments` FROM `dealer_overall_record` WHERE `dealer_id` = '"+dealerId+"' ";
            rs = stmt.executeQuery(getpendingPayment);
            if(rs.next())
            {
                pendingPayments = rs.getString(1);
                orderAmount = Float.parseFloat(finalPrice);
                previousAmount = Float.parseFloat(pendingPayments);
                finalAmount = orderAmount + previousAmount;


                QueryString = "update `dealer_overall_record` set `pending_payments` = '"+finalAmount+"' where `dealer_id` = '"+dealerId+"'";
                stmt.executeUpdate(QueryString);
            }
            else
            {
                String[] quantArr = quantity.split(",");
                for(int i=0;i< quantArr.length;i++)
                {
                    mm = mm+  Integer.parseInt(quantArr[i].replaceAll("[^a-zA-Z0-9]+",""));
                }
                String insertQuery = "INSERT INTO `dealer_overall_record` (`dealer_id`, `orders_given`, `successful_orders`, `ordered_packets`, `submitted_packets`, `ordered_boxes`, `submitted_boxes`, `order_price`,`discount_price`, `invoiced_price` , `cash_collected`,`return_packets`,`return_boxes`,`return_price`,`cash_return`, `waived_off_price`, `pending_payments`, `server_sync` ) VALUES ('"+dealerId+"','1','0','"+mm+"','"+mm+"','0','0' ,'"+orderPrice+"','"+discount+"' ,'"+finalPrice+"','0' ,'0','0','0' ,'0','0','"+finalPrice+"','Insert' )";
                stmt.executeUpdate(insertQuery);
            }




        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertInvoice(Statement stmt)
    {
        String currentDate = GlobalVariables.getStDate();
        String currentTime = GlobalVariables.getStTime();
        String currentUser = GlobalVariables.getUserId();
        String updateProductStock = "";
        String updateBatchStock = "";
        String updateQuery = "INSERT INTO `order_info_detailed`(`order_id`, `product_table_id`, `batch_id`, `quantity`, `submission_quantity`, `unit`, `submission_unit`, `order_price`, `bonus_quant`, `discount`, `final_price`, `cmplt_returned_bit`, `returned_quant`, `returned_bonus_quant`) VALUES ";
        String unit = "Packets";
        try {
            for(int i=0; i<productIdArr.size(); i++)
            {
                if(i == productIdArr.size()-1)
                    updateQuery += "('"+orderId+"','"+productIdArr.get(i)+"','"+batchIdArr.get(i)+"','"+orderedQuantityArr.get(i)+"','"+submissionQuantityArr.get(i)+"','"+unit+"','"+unit+"','"+productPriceArr.get(i)+"','"+bonusQuantityArr.get(i)+"','"+discountGivenArr.get(i)+"','"+finalPriceArr.get(i)+"','0','0','0') ";
                else
                    updateQuery += "('"+orderId+"','"+productIdArr.get(i)+"','"+batchIdArr.get(i)+"','"+orderedQuantityArr.get(i)+"','"+submissionQuantityArr.get(i)+"','"+unit+"','"+unit+"','"+productPriceArr.get(i)+"','"+bonusQuantityArr.get(i)+"','"+discountGivenArr.get(i)+"','"+finalPriceArr.get(i)+"','0','0','0'), ";

                updateProductStock = "UPDATE `products_stock` SET `in_stock` = `in_stock` - '"+submissionQuantityArr.get(i)+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `product_table_id` = '"+productIdArr.get(i)+"'; ";
                updateBatchStock = "UPDATE `batchwise_stock` SET `quantity` = `quantity` - '"+submissionQuantityArr.get(i)+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `product_table_id` = '"+productIdArr.get(i)+"' AND `batch_no` = '"+batchNoArr.get(i)+"';";
                stmt.executeUpdate(updateProductStock);
                stmt.executeUpdate(updateBatchStock);
            }
//            updateQuery = "UPDATE `dealer_info` SET `dealer_id`='"+dealerId+"',`dealer_area_id`='"+areaId+"',`dealer_name`='"+dealerName+"',`dealer_phone`='"+dealerContact+"',`dealer_address`='"+dealerAddress+"',`dealer_type`='"+dealerType+"',`dealer_cnic`='"+dealerCnic+"',`dealer_lic9_num`='"+dealerLicNum+"',`dealer_lic9_exp`='"+dealerLicExp+"',`dealer_status`='"+dealerStatus+"', `update_user_id`='"+currentUser+"',`update_date`='"+currentDate+"',`update_time`='"+currentTime+"' WHERE `dealer_table_id` = '"+dealerTableId+"'";

//            System.out.println(updateQuery);
            stmt.executeUpdate(updateQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
