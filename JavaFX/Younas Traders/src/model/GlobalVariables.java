package model;

import com.jfoenix.controls.JFXComboBox;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;
import tray.animations.AnimationType;
import tray.notification.NotificationType;
import tray.notification.TrayNotification;

import java.awt.*;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class GlobalVariables {
    public static String softwareLicId = "1";
    public static String softwareLicNumber = "";
    public static String softwareLicRegDate = "";
    public static String softwareLicExpDate = "";
    public static String workingDistrict = "";

    public static String userId = "0";
    public static String userImage = "";
    public static String stDate;
    public static String stTime;
    public static String email = "";
    public static String password = "";
    public static String host = "smtp.gmail.com";
    public static Alert alert = new Alert(Alert.AlertType.NONE);

    public static Stage baseStage;
    public static Scene baseScene;

    public static String setupNavigation;

    public static Screen screen = Screen.getPrimary();
    public static Rectangle2D bounds = screen.getVisualBounds();
    public static double boundsX = bounds.getMinX();
    public static double boundsY = bounds.getMinY();

    static Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
    public static double SCREEN_WIDTH = screenSize.getWidth();
    public static double SCREEN_HEIGHT = screenSize.getHeight();

    public static SimpleDateFormat fmt = new SimpleDateFormat("dd/MMM/yyyy");

    public static MysqlCon objMysqlCon = new MysqlCon();
    public static Statement objStmt = objMysqlCon.stmt;
    public static Connection objCon = objMysqlCon.con;

    public static SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MMM/yyyy");
    public static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    public static SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
    public static SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");

    public static String currentUserPassword = "";
    public static int findNewBookings = 0;
    public static int findNewReturns = 0;
    public static int findNewDealerFinance = 0;
    public static int findNewUserAccounts = 0;
    public static int usersActive = 0;

    public static Button btnTopHeaderSale;
    public static Button btnNavSaleOrder;
    public static Button btnNavSaleReturn;
    public static Button btnTopHeaderFinance;
    public static Button btnTopHeaderSettings;
    public static Button btnNavUserAccounts;

    public static Label lblSoftwareLicId;
    public static Label lblConnectionStatus;
    public static Separator sepUsers;
    public static Label lblUsersActive;
    public static Label lblDataBackup;
    public static ProgressIndicator progressBackup;
    public static Separator sepBackup;
    public static boolean connectionCheck = true;
    public static boolean connectionErrorShow = true;
    public static boolean isConnectedToServer = false;

    public static String userType = "";
    public static boolean rightCreate = false;
    public static boolean rightRead = false;
    public static boolean rightEdit = false;
    public static boolean rightDelete = false;
    public static boolean rightManageStock = false;
    public static boolean rightManageCash = false;
    public static boolean rightManageSettings = false;
    public static boolean rightMobileApp = false;

    public static DataBackUpThread objDataBackUpThread;

    public static void startDataSync()
    {
        objDataBackUpThread = new DataBackUpThread();
        objDataBackUpThread.setDaemon(true);
        objDataBackUpThread.start();
    }

    public static String getUserId() {
        return userId;
    }

    public static void setUserId(String userId) {
        GlobalVariables.userId = userId;
    }

    public static String getStDate() {
        Date objDate = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd/MMM/yyyy");
        GlobalVariables.stDate = df.format(objDate);
        return stDate;
    }

    public static String getStTime() {
        Date objDate = Calendar.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        GlobalVariables.stTime = sdf.format(objDate);
        return stTime;
    }

    public static LocalDate LOCAL_DATE (String dateString) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MMM/yyyy");
        LocalDate localDate = LocalDate.parse(dateString, formatter);
        return localDate;
    }

    public static void showNotification(int type, String title, String message)
    {
        TrayNotification tray = new TrayNotification();
        tray.setTitle(title);
        tray.setMessage(message);
        if(type == -1)
        {
            tray.setNotificationType(NotificationType.ERROR);
        }
        else if(type == 0)
        {
            tray.setNotificationType(NotificationType.NOTICE);
        }
        else if(type == 1)
        {
            tray.setNotificationType(NotificationType.SUCCESS);
        }
        tray.setAnimationType(AnimationType.POPUP);
        tray.showAndDismiss(Duration.millis(3000));
    }

    public static ArrayList<String> getUniqueValues(ArrayList<String> arr)
    {
        Set<String> set = new HashSet<>(arr);
        arr.clear();
        arr.addAll(set);
        return arr;
    }

    public static ArrayList<String> getArrayColumn(ArrayList<ArrayList<String>> arrayData, int columnIndex)
    {
        ArrayList<String> columnList = new ArrayList<>();
        for(ArrayList<String> row: arrayData) {
            columnList.add(row.get(columnIndex));
        }
        return columnList;
    }

    public static FilteredList<String> setSearchComboBox(ArrayList<String> list, JFXComboBox<String> simpleComboBox)
    {
        // Create a list with some dummy values.
        ObservableList<String> items = FXCollections.observableArrayList(list);

        // Create a FilteredList wrapping the ObservableList.
        FilteredList<String> filteredItems = new FilteredList<String>(items, p -> true);

        // Add a listener to the textProperty of the combobox editor. The
        // listener will simply filter the list every time the input is changed
        // as long as the user hasn't selected an item in the list.
        simpleComboBox.getEditor().textProperty().addListener((obs, oldValue, newValue) -> {
            final TextField editor = simpleComboBox.getEditor();
            final String selected = simpleComboBox.getSelectionModel().getSelectedItem();

            // This needs run on the GUI thread to avoid the error described
            // here: https://bugs.openjdk.java.net/browse/JDK-8081700.
            Platform.runLater(() -> {
                // If the no item in the list is selected or the selected item
                // isn't equal to the current input, we refilter the list.
                if (selected == null || !selected.equals(editor.getText())) {
                    filteredItems.setPredicate(item -> {
                        // We return true for any items that starts with the
                        // same letters as the input. We use toUpperCase to
                        // avoid case sensitivity.
                        if (item.toUpperCase().startsWith(newValue.toUpperCase())) {
                            return true;
                        } else {
                            return false;
                        }
                    });
                }
            });
        });

        return filteredItems;
    }

    public static ArrayList<ArrayList<String>> getParentsChildren(ArrayList<ArrayList<String>> childrenData, String selectionId)
    {
        ArrayList<ArrayList<String>> columnList = new ArrayList<>();
        ArrayList<String> temp;
        for(ArrayList<String> row: childrenData) {
            if(row.get(3) != null && row.get(3).equals(selectionId))
            {
                temp = new ArrayList<>();
                temp.add(row.get(0));
                temp.add(row.get(2));
                columnList.add(temp);
            }
        }
        return columnList;
    }

    public static void showAlert(String type, String message)
    {
        if(type.equals("Error"))
        {
            alert.setAlertType(Alert.AlertType.ERROR);
            alert.setHeaderText("Error!");
        }
        alert.setContentText(message);
        alert.show();
    }

//    1. Alert Show Properly;
//    2. Exception arised when wrong email entered -> Show it in alert;
//    3. Check email sending on all mails with files;

    public static String getIdFrom2D(ArrayList<ArrayList<String>> data, String name, int findIn)
    {
        for(ArrayList<String> row: data) {
            if(row.get(findIn) != null && row.get(findIn).equals(name))
            {
                return row.get(0);
            }
        }
        return null;
    }

    public static String getDataFrom2D(ArrayList<ArrayList<String>> data, String name, int findIn, int returnIndex)
    {
        for(ArrayList<String> row: data) {
            if(row.get(findIn) != null && row.get(findIn).equals(name))
            {
                return row.get(returnIndex);
            }
        }
        return null;
    }

    public static float roundFloat(float d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }

    public static Node createViewIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.EYE);
        graphic.setFontSize(20.0);
        return graphic;
    }

    public static Node createCommentIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.COMMENT);
        graphic.setFontSize(20.0);
        return graphic;
    }

    public static Node createEditIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.EDIT);
        graphic.setFontSize(20.0);
        return graphic;
    }

    public static Node createDeleteIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.TRASH);
        graphic.setFontSize(20.0);
        return graphic;
    }
}
