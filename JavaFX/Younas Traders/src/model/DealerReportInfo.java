package model;

import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.HBox;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DealerReportInfo {
    private String srNo;
    private String dealerTableId;
    private String dealerId;
    private String dealerName;
    private String areaName;
    private String dealerType;
    private String orders;
    private String orderedItems;
    private String submittedItems;
    private String returnedItems;
    private String totalSale;
    private String cashCollection;
    private String cashReturn;
    private String cashDiscount;
    private String cashWaiveOff;
    private String cashPending;

    private HBox operationsPane;
    private JFXButton btnView;
    private JFXButton btnEdit;

    public static String dealerReportId;

    public DealerReportInfo() {
    }

    public DealerReportInfo(String srNo, String dealerTableId, String dealerId, String dealerName, String areaName, String dealerType, String orders, String orderedItems, String submittedItems, String returnedItems, String totalSale, String cashCollection, String cashReturn, String cashDiscount, String cashWaiveOff, String cashPending) {
        this.srNo = srNo;
        this.dealerTableId = dealerTableId;
        this.dealerId = dealerId;
        this.dealerName = dealerName;
        this.areaName = areaName;
        this.dealerType = dealerType;
        this.orders = orders;
        this.orderedItems = orderedItems;
        this.submittedItems = submittedItems;
        this.returnedItems = returnedItems;
        this.totalSale = totalSale;
        this.cashCollection = cashCollection;
        this.cashReturn = cashReturn;
        this.cashDiscount = cashDiscount;
        this.cashWaiveOff = cashWaiveOff;
        this.cashPending = cashPending;

        this.btnView = new JFXButton();
        this.btnView.setGraphic(GlobalVariables.createViewIcon());
        this.btnView.getStyleClass().add("btn_icon");
        this.btnEdit = new JFXButton();
        this.btnEdit.setGraphic(GlobalVariables.createEditIcon());
        this.btnEdit.getStyleClass().add("btn_icon");
        this.operationsPane = new HBox(btnView, btnEdit);

        this.btnView.setOnAction((event)->viewDealerDetail());
        this.btnEdit.setOnAction((event)->editClicked());
    }

    public void viewDealerDetail()
    {
        Parent parent = null;
        try {
            dealerReportId = dealerTableId;
            parent = FXMLLoader.load(getClass().getResource("/view/dealer_report_detail.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(parent);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
    
    public void editClicked()
    {
        Parent parent = null;
        try {
            dealerReportId = dealerTableId;
            parent = FXMLLoader.load(getClass().getResource("/view/add_dealer_finance.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(parent);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getDealerTableId() {
        return dealerTableId;
    }

    public void setDealerTableId(String dealerTableId) {
        this.dealerTableId = dealerTableId;
    }

    public String getDealerId() {
        return dealerId;
    }

    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getDealerType() {
        return dealerType;
    }

    public void setDealerType(String dealerType) {
        this.dealerType = dealerType;
    }

    public String getOrders() {
        return orders;
    }

    public void setOrders(String orders) {
        this.orders = orders;
    }

    public String getOrderedItems() {
        return orderedItems;
    }

    public void setOrderedItems(String orderedItems) {
        this.orderedItems = orderedItems;
    }

    public String getSubmittedItems() {
        return submittedItems;
    }

    public void setSubmittedItems(String submittedItems) {
        this.submittedItems = submittedItems;
    }

    public String getReturnedItems() {
        return returnedItems;
    }

    public void setReturnedItems(String returnedItems) {
        this.returnedItems = returnedItems;
    }

    public String getTotalSale() {
        return totalSale;
    }

    public void setTotalSale(String totalSale) {
        this.totalSale = totalSale;
    }

    public String getCashCollection() {
        return cashCollection;
    }

    public void setCashCollection(String cashCollection) {
        this.cashCollection = cashCollection;
    }

    public String getCashReturn() {
        return cashReturn;
    }

    public void setCashReturn(String cashReturn) {
        this.cashReturn = cashReturn;
    }

    public String getCashDiscount() {
        return cashDiscount;
    }

    public void setCashDiscount(String cashDiscount) {
        this.cashDiscount = cashDiscount;
    }

    public String getCashWaiveOff() {
        return cashWaiveOff;
    }

    public void setCashWaiveOff(String cashWaiveOff) {
        this.cashWaiveOff = cashWaiveOff;
    }

    public String getCashPending() {
        return cashPending;
    }

    public void setCashPending(String cashPending) {
        this.cashPending = cashPending;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public ArrayList<ArrayList<String>> getDealersReportInfo(Statement stmt, Connection con)
    {
        ArrayList<ArrayList<String>> dealerReportData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT `dealer_info`.`dealer_table_id`, `dealer_info`.`dealer_id`, `dealer_info`.`dealer_name`, `area_info`.`area_name`, `dealer_info`.`dealer_type`, `dealer_overall_record`.`orders_given`, `dealer_overall_record`.`ordered_packets`, `dealer_overall_record`.`submitted_packets`, `dealer_overall_record`.`return_packets`, `dealer_overall_record`.`invoiced_price`, `dealer_overall_record`.`cash_collected`, `dealer_overall_record`.`cash_return`, `dealer_overall_record`.`discount_price`, `dealer_overall_record`.`waived_off_price`, `dealer_overall_record`.`pending_payments` FROM `dealer_info` LEFT OUTER JOIN `area_info` ON `dealer_info`.`dealer_area_id` = `area_info`.`area_table_id` LEFT OUTER JOIN `dealer_overall_record` ON `dealer_info`.`dealer_table_id` = `dealer_overall_record`.`dealer_id` WHERE `dealer_info`.`dealer_status` != 'Deleted'");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Table Id
                temp.add(rs.getString(2)); // Id
                temp.add(rs.getString(3)); // Name
                temp.add(rs.getString(4)); // area
                temp.add(rs.getString(5)); // Dealer Type
                temp.add(rs.getString(6)); // Order
                temp.add(rs.getString(7)); // Ordered Items
                temp.add(rs.getString(8)); // Submitted Items
                temp.add(rs.getString(9)); // Returned Items
                temp.add(rs.getString(10)); // Sale
                temp.add(rs.getString(11)); // Collected
                temp.add(rs.getString(12)); // Return
                temp.add(rs.getString(13)); // Discount
                temp.add(rs.getString(14)); // Waive Off
                temp.add(rs.getString(15)); // Pending Payment
                dealerReportData.add(temp);
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dealerReportData;
    }

    public ArrayList<ArrayList<String>> getDealersReportSearch(Statement stmt, Connection con, String dealerId, String dealerName, String areaName, String dealerType)
    {
        ArrayList<ArrayList<String>> dealerReportData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        String searchQuery;
        int multipleSearch = 0;
        searchQuery = "SELECT `dealer_info`.`dealer_table_id`, `dealer_info`.`dealer_id`, `dealer_info`.`dealer_name`, `area_info`.`area_name`, `dealer_info`.`dealer_type`, `dealer_overall_record`.`orders_given`, `dealer_overall_record`.`ordered_packets`, `dealer_overall_record`.`submitted_packets`, `dealer_overall_record`.`return_packets`, `dealer_overall_record`.`invoiced_price`, `dealer_overall_record`.`cash_collected`, `dealer_overall_record`.`cash_return`, `dealer_overall_record`.`discount_price`, `dealer_overall_record`.`waived_off_price`, `dealer_overall_record`.`pending_payments` FROM `dealer_info` LEFT OUTER JOIN `area_info` ON `dealer_info`.`dealer_area_id` = `area_info`.`area_table_id` LEFT OUTER JOIN `dealer_overall_record` ON `dealer_info`.`dealer_table_id` = `dealer_overall_record`.`dealer_id` WHERE ";
        if(!dealerId.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`dealer_info`.`dealer_id` = '"+dealerId+"'";
            multipleSearch++;
        }
        if(!dealerName.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`dealer_info`.`dealer_name` = '"+dealerName+"'";
            multipleSearch++;
        }
        if(!areaName.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`area_info`.`area_name` = '"+areaName+"'";
            multipleSearch++;
        }
        if(!dealerType.equals("All"))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`dealer_info`.`dealer_type` = '"+dealerType+"'";
            multipleSearch++;
        }
        if(multipleSearch >= 1)
        {
            searchQuery += " AND ";
        }
        searchQuery += "`dealer_info`.`dealer_status` != 'Deleted'";
        try {
            rs = stmt.executeQuery(searchQuery);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Table Id
                temp.add(rs.getString(2)); // Id
                temp.add(rs.getString(3)); // Name
                temp.add(rs.getString(4)); // area
                temp.add(rs.getString(5)); // Dealer Type
                temp.add(rs.getString(6)); // Order
                temp.add(rs.getString(7)); // Ordered Items
                temp.add(rs.getString(8)); // Submitted Items
                temp.add(rs.getString(9)); // Returned Items
                temp.add(rs.getString(10)); // Sale
                temp.add(rs.getString(11)); // Collected
                temp.add(rs.getString(12)); // Return
                temp.add(rs.getString(13)); // Discount
                temp.add(rs.getString(14)); // Waive Off
                temp.add(rs.getString(15)); // Pending Payment
                dealerReportData.add(temp);
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dealerReportData;
    }

    public ArrayList<ArrayList<String>> getDealersReportSummary(Statement stmt, Connection con, String summaryBase)
    {
        ArrayList<ArrayList<String>> dealerReportData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        String searchQuery;
        searchQuery = "SELECT `dealer_info`.`dealer_table_id`, `dealer_info`.`dealer_id`, `dealer_info`.`dealer_name`, `area_info`.`area_name`, `dealer_info`.`dealer_type`, `dealer_overall_record`.`orders_given`, `dealer_overall_record`.`ordered_packets`, `dealer_overall_record`.`submitted_packets`, `dealer_overall_record`.`return_packets`, `dealer_overall_record`.`invoiced_price`, `dealer_overall_record`.`cash_collected`, `dealer_overall_record`.`cash_return`, `dealer_overall_record`.`discount_price`, `dealer_overall_record`.`waived_off_price`, `dealer_overall_record`.`pending_payments` FROM `dealer_info` LEFT OUTER JOIN `area_info` ON `dealer_info`.`dealer_area_id` = `area_info`.`area_table_id` LEFT OUTER JOIN `dealer_overall_record` ON `dealer_info`.`dealer_table_id` = `dealer_overall_record`.`dealer_id` WHERE `dealer_info`.`dealer_status` != 'Deleted'";
        if(!summaryBase.equals(""))
        {
            searchQuery += " AND ";
            if(summaryBase.equals("Pending"))
                searchQuery += "`dealer_overall_record`.`pending_payments` != '0'";
            else if(summaryBase.equals("Waive Off"))
                searchQuery += "`dealer_overall_record`.`waived_off_price` != '0'";
            else if(summaryBase.equals("Sale"))
                searchQuery += "`dealer_overall_record`.`invoiced_price` != '0'";
            else if(summaryBase.equals("Collection"))
                searchQuery += "`dealer_overall_record`.`cash_collected` != '0'";
            else if(summaryBase.equals("Return"))
                searchQuery += "`dealer_overall_record`.`cash_return` != '0'";
            else if(summaryBase.equals("Discount"))
                searchQuery += "`dealer_overall_record`.`discount_price` != '0'";
        }
        try {
            rs = stmt.executeQuery(searchQuery);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Table Id
                temp.add(rs.getString(2)); // Id
                temp.add(rs.getString(3)); // Name
                temp.add(rs.getString(4)); // area
                temp.add(rs.getString(5)); // Dealer Type
                temp.add(rs.getString(6)); // Order
                temp.add(rs.getString(7)); // Ordered Items
                temp.add(rs.getString(8)); // Submitted Items
                temp.add(rs.getString(9)); // Returned Items
                temp.add(rs.getString(10)); // Sale
                temp.add(rs.getString(11)); // Collected
                temp.add(rs.getString(12)); // Return
                temp.add(rs.getString(13)); // Discount
                temp.add(rs.getString(14)); // Waive Off
                temp.add(rs.getString(15)); // Pending Payment
                dealerReportData.add(temp);
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dealerReportData;
    }

    private Node createEditIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.EDIT);
        graphic.setFontSize(20.0);
        return graphic;
    }

    private Node createViewIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.EYE);
        graphic.setFontSize(20.0);
        return graphic;
    }
}
