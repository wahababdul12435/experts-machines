package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class SaleInvoicePrintInfo {

    public ArrayList<String> getSaleInvoicePrintInfo(Statement stmt, Connection con)
    {
        ResultSet rs = null;
        ArrayList<String> saleInvoiceInfo = new ArrayList<>();
        String query = "SELECT `header_1`, `header_2`, `header_3`, `header_4`, `header_5`, `owner_name`, `father_name`, `owner_cnic`, `owner_country`, `business_name`, `business_address`, `footer_1`, `footer_2`, `footer_3`, `footer_4` FROM `sale_invoice_print_info`";
        try {
            rs = stmt.executeQuery(query);
            rs.next();
            saleInvoiceInfo.add(rs.getString(1)); // Header 1
            saleInvoiceInfo.add(rs.getString(2)); // Header 2
            saleInvoiceInfo.add(rs.getString(3)); // Header 3
            saleInvoiceInfo.add(rs.getString(4)); // Header 4
            saleInvoiceInfo.add(rs.getString(5)); // Header 5
            saleInvoiceInfo.add(rs.getString(6)); // Owner Name
            saleInvoiceInfo.add(rs.getString(7)); // Father Name
            saleInvoiceInfo.add(rs.getString(8)); // Owner CNIC
            saleInvoiceInfo.add(rs.getString(9)); // Owner Country
            saleInvoiceInfo.add(rs.getString(10)); // Business Name
            saleInvoiceInfo.add(rs.getString(11)); // Business Address
            saleInvoiceInfo.add(rs.getString(12)); // Footer 1
            saleInvoiceInfo.add(rs.getString(13)); // Footer 2
            saleInvoiceInfo.add(rs.getString(14)); // Footer 3
            saleInvoiceInfo.add(rs.getString(15)); // Footer 4
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return saleInvoiceInfo;
    }
}
