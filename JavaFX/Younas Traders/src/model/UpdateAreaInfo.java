package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class UpdateAreaInfo {
    public ArrayList<ArrayList<String>> citiesData;

    public UpdateAreaInfo()
    {
        citiesData = new ArrayList<ArrayList<String>>();
    }

    public ArrayList<ArrayList<String>> getCitiesData()
    {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement stmt = objMysqlCon.stmt;
        Connection con = objMysqlCon.con;
        ResultSet rs = null;
        ArrayList<String> temp;
        try {
            rs = stmt.executeQuery("SELECT `city_table_id`, `city_name` FROM `city_info` WHERE `city_status` != 'Deleted'");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2));
                citiesData.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return citiesData;
    }
}
