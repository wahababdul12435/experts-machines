package model;


import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class SaleInvoice
{
    public static StackPane savePrintPane;
    public static JFXDialog dialog;

    public void editClicked()
    {
        Parent parent = null;
        try {
            parent = FXMLLoader.load(getClass().getResource("/view/save_print.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setStyle("-fx-padding: -20 -10 -20 -10;");
        //dialogLayout.setHeading(lblUpdate);
        dialogLayout.setBody(parent);
        dialog = new JFXDialog(savePrintPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        savePrintPane.setVisible(true);
        dialog.show();
        dialog.setOverlayClose(false);
    }


    public void insertsaleInvoiceInfodetails(Statement stmt, Connection con, int order_id, int prodID,int batchnumbers, int quant,int subquant,String pack_sizee, String subpack_sizee, float final_price,int bon_quantity, float discs,float totalprices, int rtrnd_bit,int retnedquant,int retrnedd_bon_quant)
    {
        try {
            String insertQuery = "INSERT INTO `order_info_detailed` (`order_id`, `product_table_id`, `batch_id`, `quantity`, `submission_quantity`, `unit`, `submission_unit`, `order_price`,`bonus_quant`, `discount`, `final_price`, `cmplt_returned_bit` , `returned_quant` , `returned_bonus_quant` ) VALUES ('"+order_id+"','"+prodID+"','"+batchnumbers+"','"+quant+"','"+subquant+"','"+pack_sizee+"','"+subpack_sizee+"' ,'"+final_price+"','"+bon_quantity+"' ,'"+discs+"','"+totalprices+"','"+rtrnd_bit+"','"+ retnedquant+"','"+retrnedd_bon_quant+"')";
            stmt.executeUpdate(insertQuery);
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void insertsaleInvoiceInfo(Statement stmt, Connection con, String order_id, String dealer_id, String prod_id, String quant,String pack_sizee, float order_price, String bonus, String discount, float final_price, String invo_date,String invo_time , String update_dates,String update_times)
    {
        String orderplace = "Shadiwal";
        String invoice_status = "Pending";
        float waiveOFF = 0.00f;
        String UserIDs = GlobalVariables.getUserId();
        try {
            String insertQuery = "INSERT INTO `order_info` (`order_id`, `dealer_id`, `product_id`, `quantity`, `unit`, `order_price`, `bonus`, `discount`, `waive_off_price`, `final_price`, `order_success`, `booking_latitude`, `booking_longitude`, `booking_area`, `booking_date`, `booking_time`, `booking_user_id` , `update_dates`, `update_times`,`update_user_id`, `status`) VALUES ('"+order_id+"','"+dealer_id+"','"+prod_id+"','"+quant+"','"+pack_sizee+"','"+order_price+"' ,'"+bonus+"' ,'"+discount+"','"+waiveOFF+"','"+final_price+"',1,1.021,2.01,'"+orderplace+"','"+invo_date+"','"+invo_time+"','"+UserIDs+"','"+update_dates+"','"+update_times+"','"+UserIDs+"','"+invoice_status+"')";
            stmt.executeUpdate(insertQuery);
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void subtract_quantFROMStock(Statement stmt, Connection con,int prodID , int prodQuant, int prodbonus){
        try {
            int totalQty = prodQuant + prodbonus;
            String insertQuery = "UPDATE `products_stock` SET `in_stock`='"+totalQty+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `product_table_id` = '"+prodID+"'";
            stmt.executeUpdate(insertQuery);
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void insertdealerOverallRecord(Statement stmt, Connection con, int dealersid, int ordergiven,int successful_orders, int ordered_packets,int submitted_packets,int ordered_boxes, int submitted_boxes , float order_price,float discount_price, float invoiced_price,float cash_collected, float waived_off_price, float pending_payments)
    {
        int retrnpackts = 0;
        int retrnboxs = 0;
        float retrnprice = 0.0f;
        float cashretrn = 0.0f;

        try {
            String insertQuery = "INSERT INTO `dealer_overall_record` (`dealer_id`, `orders_given`, `successful_orders`, `ordered_packets`, `submitted_packets`, `ordered_boxes`, `submitted_boxes`, `order_price`,`discount_price`, `invoiced_price` , `cash_collected`,`return_packets`,`return_boxes`,`return_price`,`cash_return`, `waived_off_price`, `pending_payments` ) VALUES ('"+dealersid+"','"+ordergiven+"','"+successful_orders+"','"+ordered_packets+"','"+submitted_packets+"','"+ordered_boxes+"','"+submitted_boxes+"' ,'"+order_price+"','"+discount_price+"' ,'"+invoiced_price+"','"+cash_collected+"' ,'"+retrnpackts+"','"+retrnboxs+"','"+retrnprice+"' ,'"+cashretrn+"','"+waived_off_price+"','"+pending_payments+"' )";
            stmt.executeUpdate(insertQuery);
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void updatedealerOverallRecord(Statement stmt, Connection con,  int dealersid,int givnOrders, int ordered_packets,int submitted_packets,int ordered_boxes, int submitted_boxes , float order_price,float discount_price, float invoiced_price, float pending_payments)
    {

        try {
            String insertQuery = "UPDATE `dealer_overall_record` SET   `orders_given` = '"+givnOrders+"'  ,  `ordered_packets` = '"+ordered_packets+"'  , `submitted_packets` = '"+submitted_packets+"'  , `ordered_boxes` = '"+ordered_boxes+"'  , `submitted_boxes` = '"+submitted_boxes+"'  , `order_price` = '"+order_price+"'  ,`discount_price` = '"+discount_price+"'  , `invoiced_price` = '"+invoiced_price+"'  , `pending_payments` = '"+pending_payments+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END where `dealer_id` = '"+dealersid+"' ";
            stmt.executeUpdate(insertQuery);
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    String[] prevOverallRecord = new String[1];
    public String[] getprevious_dealeroverallRecord(Statement stmt3, Connection con3,int dealerID )
    {
        ResultSet rs3 = null;
        try {
            rs3 = stmt3.executeQuery("select orders_given,ordered_packets,submitted_packets,ordered_boxes,submitted_boxes,order_price,discount_price,invoiced_price,pending_payments from dealer_overall_record where dealer_id= '"+dealerID+"' ");
            int i = 0;
            while (rs3.next() && i < limit)
            {

                prevOverallRecord[i] = rs3.getString(1) +","+rs3.getString(2) +","+rs3.getString(3)+","+rs3.getString(4) +","+rs3.getString(5)+","+rs3.getString(6)+","+rs3.getString(7)+","+rs3.getString(8) +","+rs3.getString(9);
                i++;
            }
            con3.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return prevOverallRecord;
    }
    ////////////////////////////////////////////////////UPDATE BATCHWISE_STOCK TABLE IF ENTERED QUANTITY IS LESS THAN STOCK QUANTITY
    public void UPDATESimplybatchStock(Statement stmt, Connection con,int prodID , int prodQuant, int prodbonus,  String batchnumber){
        try {
            String insertQuery = "UPDATE `batchwise_stock` SET `quantity`='"+prodQuant+"' , `bonus`='"+prodbonus+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `prod_id` = '"+prodID+"' and `batch_no` = '"+batchnumber+"' ";
            stmt.executeUpdate(insertQuery);
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    ////////////////////////////////////////////////////UPDATE second batch PRODUCT quantity IF ENTERED QUANTITY GREATER THAN BATCH QUANTITY
    public void UPDATE_batchStock(Statement stmt, Connection con,int prodID , int prodQuant, int batchnumbers,  String batchnumber){
        try {
            String insertQuery = "UPDATE `batchwise_stock` SET `quantity`='"+prodQuant+"' , `prod_id` = '"+batchnumbers+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `prod_id` = '"+prodID+"' and `batch_no` = '"+batchnumber+"' ";
            stmt.executeUpdate(insertQuery);
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    ////////////////////////////////////////////////////UPDATE batch PRODUCT quantity TO ZERO FOR FIRST BATCH IF ENTERED QUANTITY GREATER THAN BATCH QUANTITY
    public void BATCHquant_Zero(Statement stmt, Connection con,int prodID , int prodQuant  , String batchnumber){
        try {
            String insertQuery = "UPDATE `batchwise_stock` SET `quantity`='"+prodQuant+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END  WHERE `prod_id` = '"+prodID+"' and `batch_no` = '"+batchnumber+"' ";
            stmt.executeUpdate(insertQuery);
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    ////////////////////////////////////////////////////UPDATE second batch BONUS quantity IF ENTERED QUANTITY GREATER THAN BATCH QUANTITY
    public void UPDATE_batchBONUSStock(Statement stmt, Connection con,int prodID , int prodbonus , String batchnumber){
        try {
            String insertQuery = "UPDATE `batchwise_stock` SET `bonus`='"+prodbonus+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `prod_id` = '"+prodID+"' and `batch_no` = '"+batchnumber+"' ";
            stmt.executeUpdate(insertQuery);
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    ////////////////////////////////////////////////////UPDATE batch BONUS quantity TO ZERO FOR FIRST BATCH IF ENTERED QUANTITY GREATER THAN BATCH QUANTITY
    public void BATCHBONUS_Zero(Statement stmt, Connection con,int prodID , int prodbonus , String batchnumber){
        try {
            String insertQuery = "UPDATE `batchwise_stock` SET `bonus`='"+prodbonus+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `prod_id` = '"+prodID+"' and `batch_no` = '"+batchnumber+"' ";
            stmt.executeUpdate(insertQuery);
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    String[] prevProductOverallRecord = new String[1];
    public String[] getprevious_productoverallRecord(Statement stmt3, Connection con3,int prodID,String batchnumber )
    {
        ResultSet rs3 = null;
        try {
            rs3 = stmt3.executeQuery("select prod_name,quantity_sold,quantity_return from product_overall_record where prod_id= '"+prodID+"' and prod_batch = '"+batchnumber+"' and prod_status = 'Active' ");
            int i = 0;
            while (rs3.next() && i < limit)
            {

                prevProductOverallRecord[i] = rs3.getString(1) +"--"+rs3.getString(2) +"--"+rs3.getString(3);
                i++;
            }
            con3.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return prevProductOverallRecord;
    }

    int BatchNumber_id = 0;
    public int getBatchID(Statement stmt3, Connection con3,int prodID,String batchnumber )
    {
        ResultSet rs3 = null;
        try {
            rs3 = stmt3.executeQuery("select batch_stock_id from batchwise_stock where prod_id= '"+prodID+"' and batch_no = '"+batchnumber+"'  ");
            int i = 0;
            while (rs3.next() && i < limit)
            {

                BatchNumber_id = Integer.parseInt(rs3.getString(1));
                i++;
            }
            con3.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return BatchNumber_id;
    }

    public void insert_productoverallRecord(Statement stmt, Connection con, int prods_ID, String productsNames,String productsBatch, int quantSold,int quantReturn, float Tprice,float Pprice, float Rprice,int expiredquant, int damagedquant)
    {
        String status = "Active";

        try {
            String insertQuery = "INSERT INTO `product_overall_record` (`prod_id`, `prod_name`, `prod_batch`, `quantity_sold`, `quantity_return`, `trade_rate`, `purch_rate`, `retail_rate`,`expired_quantity`, `damaged_quantity` , `prod_status`) VALUES ('"+prods_ID+"','"+productsNames+"','"+productsBatch+"','"+quantSold+"','"+quantReturn+"','"+Tprice+"','"+Pprice+"' ,'"+Rprice+"','"+expiredquant+"' ,'"+damagedquant+"','"+status+"' )";
            stmt.executeUpdate(insertQuery);
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void UPDATE_productoverallRecord(Statement stmt, Connection con,int produc_id , int Quantity , String batch)
    {
        try {
            String insertQuery = "UPDATE `product_overall_record` SET `quantity_sold`='"+Quantity+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `prod_id` = '"+produc_id+"' and `prod_batch` = '"+batch+"' ";
            stmt.executeUpdate(insertQuery);
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void UPDATE_discountpolicyStatus(Statement stmt, Connection con,int aprov_id , int dealer_id , String status){
        try {
            String insertQuery = "UPDATE `discount_policy` SET `policy_status`='"+status+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `approval_id` = '"+aprov_id+"' and `dealer_table_id` = '"+dealer_id+"' ";
            stmt.executeUpdate(insertQuery);
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void UPDATE_BonuspolicyStatus(Statement stmt, Connection con,int aprov_id , String prod_id , String status){
        try {
            String insertQuery = "UPDATE `bonus_policy` SET `policy_status`='"+status+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `approval_id` = '"+aprov_id+"' and `product_table_id` = '"+prod_id+"' ";
            stmt.executeUpdate(insertQuery);
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    String[] discount_policy = new String[1];
    public String[] getdiscount_policy(Statement stmt3, Connection con3,int dealerID )
    {
        ResultSet rs3 = null;
        try {
            rs3 = stmt3.executeQuery("select approval_id,company_table_id,sale_amount,discount_percent,start_date,end_date,policy_status from discount_policy where dealer_table_id= '"+dealerID+"' and policy_status='Active' ");
            int i = 0;
            while (rs3.next() && i < limit)
            {

                discount_policy[i] = rs3.getString(1) +"-"+rs3.getString(2) +"-"+rs3.getString(3)+"-"+rs3.getString(4) +"-"+rs3.getString(5) +"-"+rs3.getString(6)  +"-"+rs3.getString(7);
                i++;
            }
            con3.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return discount_policy;
    }



    String[] lastinvo_numb = new String[1];
    public String[] get_lastinvnumber(Statement stmt3, Connection con3 )
    {
        ResultSet rs3 = null;
        try {
            rs3 = stmt3.executeQuery("select order_id from order_info order by order_id DESC LIMIT 1");
            int i = 0;
            while (rs3.next() && i < limit)
            {

                lastinvo_numb[i] = rs3.getString(1) ;
                i++;
            }
            con3.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return lastinvo_numb;
    }

    ArrayList<String> dealrlist = new ArrayList<>();
    public ArrayList<String> get_AllDealers(Statement stmt2, Connection con2)
    {
        ResultSet rs = null;
        try {
            rs = stmt2.executeQuery("select dealer_name from dealer_info where dealer_status='Active'  order by  dealer_name ASC ");
            int i = 0;
            while (rs.next() && i < limit)
            {
                dealrlist.add(i,rs.getString(1));
                i++;
            }

            con2.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dealrlist;
    }

    ArrayList<String> prodctlist = new ArrayList<>();
    public ArrayList<String> get_Allprodcuts(Statement stmt2, Connection con2)
    {
        ResultSet rs = null;
        try {
            rs = stmt2.executeQuery("select product_name from product_info where product_status='Active'  order by  product_name ASC ");
            int i = 0;
            while (rs.next() && i < limit)
            {
                prodctlist.add(i,rs.getString(1));
                i++;
            }

            con2.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return prodctlist;
    }

    String[] proddetails_enterpressname = new String[1];
    public String[] get_prodDetailswithname(Statement stmt3, Connection con3, String prodName_enterpress )
    {
        ResultSet rs3 = null;
        try {
            rs3 = stmt3.executeQuery("select `product_info`.`product_table_id`, `product_info`.`company_table_id`, `batchwise_stock`.`retail_price`, `batchwise_stock`.`purchase_price`, `batchwise_stock`.`trade_price` from `product_info`  INNER JOIN `batchwise_stock` on `batchwise_stock`.`product_table_id` = `product_info`.`product_table_id` where product_name = '"+prodName_enterpress+"' ");
            int i = 0;
            while (rs3.next() && i < limit)
            {
                proddetails_enterpressname[i] = rs3.getString(1) + "--" + rs3.getString(2) + "--" + rs3.getString(3) + "--" + rs3.getString(4) + "--" + rs3.getString(5);
                i++;
            }
            con3.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return proddetails_enterpressname;
    }
    String[] prod_stockquant = new String[1];
    public String[] gettotalprodstockquant_withIDs(Statement stmt, Connection con , int product_Ids)
    {
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("select in_stock,bonus_quant from products_stock where product_table_id = '"+product_Ids+"' and status = 'Active'");
            int i = 0;
            while (rs.next() && i < limit)
            {
                prod_stockquant[i] = rs.getString(1) + "--" + rs.getString(2) ;
            }

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return prod_stockquant;
    }

    String[] prod_details = new String[1];
    public String[] get_prodDetailswithIDs(Statement stmt1, Connection con1, int prods_Ids,String getbatch)
    {
    ResultSet rs = null;
    try {
        rs = stmt1.executeQuery("select `product_info`.`product_name`, `batchwise_stock`.`retail_price` , `batchwise_stock`.`purchase_price`, `batchwise_stock`.`trade_price`, `product_info`.`company_table_id` from `product_info`  INNER JOIN `batchwise_stock` on `batchwise_stock`.`product_table_id` = `product_info`.`product_table_id` where  `product_info`.product_table_id = '"+prods_Ids+"' and `batchwise_stock`.`batch_no` = '"+getbatch+"' ");
        int i = 0;
        while (rs.next() && i < limit)
        {
            prod_details[i] = rs.getString(1) + "--" + rs.getString(2) + "--" + rs.getString(3) + "--" + rs.getString(4)+"--"+ rs.getString(5);
        }

        con1.close();
    } catch (SQLException e) {
        e.printStackTrace();
    }
    return prod_details;
}


    String[] deleardetails = new String[1];
    String[] delearsdetail = new String[1];
    String[] delearsdetail_onkeypressName = new String[50];
    int limit = 20;
    public String[] get_dealerDetailswithIDs(Statement stmt, Connection con, int dealer_Ids )
    {
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("select dealer_name,dealer_address,dealer_area_id,dealer_lic9_num,dealer_lic10_num,dealer_lic11_num,dealer_type from dealer_info where dealer_id ='"+dealer_Ids+"'  and dealer_status = 'Active' ");
            int i = 0;
            while (rs.next() && i < limit)
            {

                deleardetails[i] = rs.getString(1) + "--" + rs.getString(2) + "--" + rs.getString(3) + "--" + rs.getString(4) + "--" + rs.getString(5)+ "--" + rs.getString(6)+ "--" + rs.getString(7);
            }

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return deleardetails;

    }
    String[] bonuspolicy_details = new String[1];
    public String[] get_bonuspolicy(Statement stmt, Connection con, int productIDs )
    {
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("select bonus_policy.approval_id,bonus_policy.quantity,bonus_policy.bonus_quant,bonus_policy.start_date,bonus_policy.end_date,bonus_policy.policy_status,dealer_info.dealer_id,dealer_info.dealer_name from bonus_policy inner join dealer_info on bonus_policy.dealer_table_id = dealer_info.dealer_table_id   where product_table_id ='"+productIDs+"' and policy_status = 'Active' ");
            int i = 0;
            while (rs.next() && i < limit)
            {

                bonuspolicy_details[i] = rs.getString(1) + "--" + rs.getString(2) + "--" + rs.getString(3) + "--" + rs.getString(4) + "--" + rs.getString(5) + "--" + rs.getString(6) + "--" + rs.getString(7) + "--" + rs.getString(8);
            }

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return bonuspolicy_details;

    }


    String batchSelectionpolicy;
    public String getbatchSelectionpolicy(Statement stmt2, Connection con2)
    {
        ResultSet rs = null;
        try {
            rs = stmt2.executeQuery("select setting_value from system_settings where setting_id=1");
            while (rs.next())
            {
                batchSelectionpolicy= rs.getString(1) ;
            }

            con2.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return batchSelectionpolicy;
    }
    //select batch_no from batchwise_stock where prod_id=3 order by entry_date DESC LIMIT 1
    ArrayList<String> prod_scndLastbatch = new ArrayList<>();
    public  ArrayList<String> getprod_secondlastbatch(Statement stmt2, Connection con2, int product_IDS)
    {
        ResultSet rs = null;
        try {
            rs = stmt2.executeQuery("select batch_no,entry_date,entry_time,batch_expiry from batchwise_stock where product_table_id ='"+product_IDS+"' and quantity>=0");
            int i = 0;
            while (rs.next() && i < limit)
            {

                prod_scndLastbatch.add( i,rs.getString(1) + "--" + rs.getString(2) + "--" + rs.getString(3) + "--" + rs.getString(4));
                i++;
            }

            con2.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return prod_scndLastbatch;
    }
    String productsTableID = "";
    public  String getTableprodID(Statement stmt2, Connection con2, int product_IDS)
    {
        ResultSet rs = null;
        try {
            rs = stmt2.executeQuery("select product_table_id from product_info where product_id ='"+product_IDS+"' and product_status ='Active'");
            int i = 0;
            while (rs.next() && i < limit)
            {
                productsTableID = rs.getString(1);
            }

            con2.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return productsTableID;
    }

    String[] prod_batch = new String[1];
    public String[] getprod_batch1(Statement stmt2, Connection con2, int product_IDS)
    {
        ResultSet rs = null;
        try {
            rs = stmt2.executeQuery("select batch_no,entry_date,entry_time,batch_expiry from batchwise_stock where quantity > 0 and prod_id ='"+product_IDS+"' ");
            int i = 0;
            while (rs.next() && i < limit)
            {

                prod_batch[i] = rs.getString(1) ;
            }

            con2.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return prod_batch;
    }

    String[] batchSelection_policy = new String[1];
    public String[] getbatch_selection_policy(Statement stmt2, Connection con2)
    {
        ResultSet rs = null;
        try {
            rs = stmt2.executeQuery("select setting_value,setting_name from system_settings where setting_id = '1' ");
            int i = 0;
            while (rs.next() && i < limit)
            {

                batchSelection_policy[i] = rs.getString(1) +"-"+ rs.getString(2);
            }

            con2.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return batchSelection_policy;
    }

    String[] prodbatch_quantity = new String[1];
    public String[] getprodbatch_quantity(Statement stmt2, Connection con2, int product_IDS,String batch_product)
    {
        ResultSet rs = null;
        try {
            rs = stmt2.executeQuery("select quantity,bonus from batchwise_stock where product_table_id ='"+product_IDS+"' and batch_no='"+batch_product+"' ");
            int i = 0;
            while (rs.next() && i < limit)
            {

                prodbatch_quantity[i] = rs.getString(1)  + "--" + rs.getString(2);
            }

            con2.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return prodbatch_quantity;
    }
    String prodbatchs_entry;
    public String getprodbatch_entry(Statement stmt2, Connection con2, int product_IDS,String bacthentrydate,String bacthentrytime)
    {
        ResultSet rs = null;
        try {
            rs = stmt2.executeQuery("select batch_no,quantity from batchwise_stock where product_table_id ='"+product_IDS+"' and entry_date='"+bacthentrydate+"' and entry_time='"+bacthentrytime+"' ");
            int i = 0;
            while (rs.next() && i < limit)
            {

                prodbatchs_entry = rs.getString(1)+"-"+rs.getString(2) ;
            }

            con2.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return prodbatchs_entry;
    }

    String prodbatchs_expiry;
    public String getprodbatch_expiry(Statement stmt2, Connection con2, int product_IDS,String bacthexpirydate,String bacthentrytime)
    {
        ResultSet rs = null;
        try {
            rs = stmt2.executeQuery("select batch_no from batchwise_stock where product_table_id ='"+product_IDS+"' and batch_expiry='"+bacthexpirydate+"' and entry_time='"+bacthentrytime+"' ");
            int i = 0;
            while (rs.next() && i < limit)
            {

                prodbatchs_expiry = rs.getString(1) ;
            }

            con2.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return prodbatchs_expiry;
    }
    public String[] dealerDetail_withKeypressname(Statement stmt2, Connection con2)
    {
        ResultSet rs2 = null;
        try {
            rs2 = stmt2.executeQuery("select dealer_name,dealer_area_id from dealer_info where dealer_status='Active'  ");
            int i = 0;
            while (rs2.next() && i < limit)
            {

                delearsdetail_onkeypressName[i] = rs2.getString(1) + "--" + rs2.getString(2);
                i++;
            }
            con2.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return delearsdetail_onkeypressName;

    }

    public String[] get_dealerDetailswithname(Statement stmt, Connection con, String dealersName)
    {
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("select dealer_id,dealer_area_id,dealer_address,dealer_lic_num from dealer_info where dealer_name ='"+dealersName+"' and dealer_status='Active'");
            int i = 0;
            while (rs.next() && i < limit)
            {

                deleardetails[i] = rs.getString(1) + "--" + rs.getString(2) + "--" + rs.getString(3) + "--" + rs.getString(4);
            }

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return  deleardetails;
    }
}
