package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Bonus {
    String approvalids;
    String productIds;
    String productNames;
    String packsizes;
    String bonusfor;
    String dealerID;
    String dealerName;
    String quantities;
    String bonuses;
    String startdates;
    String enddates;

    public Bonus()
    {}

    public Bonus(String approvalids, String productIds, String productNames, String packsizes, String bonusfor, String dealerID, String dealerName, String quantities, String bonuses, String startdates, String enddates) {
        this.approvalids = approvalids;
        this.productIds = productIds;
        this.productNames = productNames;
        this.packsizes = packsizes;
        this.bonusfor = bonusfor;
        this.dealerName = dealerName;
        this.dealerID = dealerID;
        this.quantities = quantities;
        this.bonuses = bonuses;
        this.startdates = startdates;
        this.enddates = enddates;
    }

    public String getApprovalids() {
        return approvalids;
    }

    public void setApprovalids(String approvalids) {
        this.approvalids = approvalids;
    }

    public String getProductIds() {
        return productIds;
    }

    public void setProductIds(String productIds) {
        this.productIds = productIds;
    }

    public String getProductNames() {
        return productNames;
    }

    public void setProductNames(String productNames) {
        this.productNames = productNames;
    }

    public String getPacksizes() {
        return packsizes;
    }

    public String getBonusfor() {
        return bonusfor;
    }

    public void setBonusfor(String bonusfor) {
        this.bonusfor = bonusfor;
    }

    public String getDealerID() {
        return dealerID;
    }

    public void setDealerID(String dealerID) {
        this.dealerID = dealerID;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public void setPacksizes(String packsizes) {
        this.packsizes = packsizes;
    }

    public String getQuantities() {
        return quantities;
    }

    public void setQuantities(String quantities) {
        this.quantities = quantities;
    }

    public String getBonuses() {
        return bonuses;
    }

    public void setBonuses(String bonuses) {
        this.bonuses = bonuses;
    }

    public String getStartdates() {
        return startdates;
    }

    public void setStartdates(String startdates) {
        this.startdates = startdates;
    }

    public String getEnddates() {
        return enddates;
    }

    public void setEnddates(String enddates) {
        this.enddates = enddates;
    }

    public ArrayList<ArrayList<String>> getCurrentBonusInfo(Statement stmt, Connection con)
    {
        ArrayList<ArrayList<String>> bonusData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        String query = "SELECT `dealer_table_id`, `company_table_id`, `product_table_id`, `quantity`, `bonus_quant` FROM `bonus_policy` WHERE str_to_date(`start_date`, '%d/%b/%Y') >= str_to_date('"+GlobalVariables.getStDate()+"', '%d/%b/%Y') AND str_to_date(`end_date`, '%d/%b/%Y') <= str_to_date('"+GlobalVariables.getStDate()+"', '%d/%b/%Y') AND  `policy_status` = 'Active'";
        try {
            rs = stmt.executeQuery(query);
//            System.out.println(query);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Dealer Id
                temp.add(rs.getString(2)); // Company Id
                temp.add(rs.getString(3)); // Product Id
                temp.add(rs.getString(4)); // Qty
                temp.add(rs.getString(5)); // Bonus
                bonusData.add(temp);
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return bonusData;
    }
}
