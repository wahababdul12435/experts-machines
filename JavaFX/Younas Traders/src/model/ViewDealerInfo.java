package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import controller.DeleteWindow;
import controller.UpdateCompany;
import controller.UpdateDealer;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ViewDealerInfo {
    private String srNo;
    private String dealerTableId;
    private String dealerId;
    private String dealerAreaId;
    private String dealerArea;
    private String dealerCity;
    private String dealerName;
    private String dealerContact;
    private String dealerAddress;
    private String dealerCnic;
    private String dealerLic9Num;
    private String dealerLic9Exp;
    private String dealerLic10Num;
    private String dealerLic10Exp;
    private String dealerLic11Num;
    private String dealerLic11Exp;
    private String dealerType;
    private String dealerStatus;

    private HBox operationsPane;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    public static StackPane stackPane;
    public static JFXDialog dialog;
    public static Label lblUpdate;

    public ViewDealerInfo()
    {
        this.srNo = "";
        this.dealerTableId = "";
        this.dealerId = "";
        this.dealerAreaId = "";
        this.dealerArea = "";
        this.dealerCity = "";
        this.dealerName = "";
        this.dealerContact = "";
        this.dealerAddress = "";
        this.dealerCnic = "";
        this.dealerLic9Num = "";
        this.dealerLic9Exp = "";
        this.dealerLic10Num = "";
        this.dealerLic10Exp = "";
        this.dealerLic11Num = "";
        this.dealerLic11Exp = "";
        this.dealerType = "";
        this.dealerStatus = "";

        this.btnEdit = new JFXButton("Edit");
        this.btnDelete = new JFXButton("Delete");
        this.operationsPane = new HBox(btnEdit, btnDelete);
    }

    public ViewDealerInfo(String srNo, String dealerTableId, String dealerId, String dealerAreaId, String dealerArea, String dealerCity, String dealerName, String dealerContact, String dealerAddress, String dealerType, String dealerCnic, String dealerLic9Num, String dealerLic9Exp, String dealerLic10Num, String dealerLic10Exp, String dealerLic11Num, String dealerLic11Exp, String dealerStatus) {
        this.srNo = srNo;
        this.dealerTableId = dealerTableId;
        this.dealerId = dealerId;
        this.dealerAreaId = dealerAreaId;
        this.dealerArea = dealerArea;
        this.dealerCity = dealerCity;
        this.dealerName = dealerName;
        this.dealerContact = dealerContact;
        this.dealerAddress = dealerAddress;
        this.dealerCnic = dealerCnic;
        this.dealerLic9Num = dealerLic9Num;
        this.dealerLic9Exp = dealerLic9Exp;
        this.dealerLic10Num = dealerLic10Num;
        this.dealerLic10Exp = dealerLic10Exp;
        this.dealerLic11Num = dealerLic11Num;
        this.dealerLic11Exp = dealerLic11Exp;
        this.dealerType = dealerType;
        this.dealerStatus = dealerStatus;

        this.btnEdit = new JFXButton();
        this.btnDelete = new JFXButton();
//        this.btnView.setGraphic(GlobalVariables.createViewIcon());

        this.operationsPane = new HBox();

//        this.btnView.getStyleClass().add("btn_icon");
//        this.btnView.setOnAction((action)->viewClicked());
//        this.operationsPane.getChildren().add(this.btnView);

        if(GlobalVariables.rightEdit)
        {
            this.btnEdit.setGraphic(GlobalVariables.createEditIcon());
            this.btnEdit.getStyleClass().add("btn_icon");
            this.btnEdit.setOnAction((action)->editClicked());
            this.operationsPane.getChildren().add(this.btnEdit);
        }
        if(GlobalVariables.rightDelete)
        {
            this.btnDelete.setGraphic(GlobalVariables.createDeleteIcon());
            this.btnDelete.getStyleClass().add("btn_icon");
            this.btnDelete.setOnAction(event -> {
                try {
                    deleteDealer(this.dealerTableId);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            this.operationsPane.getChildren().add(this.btnDelete);
        }
    }

    public void editClicked()
    {
        Parent parent = null;
        try {
            UpdateDealer.dealerTableId = dealerTableId;
            UpdateDealer.dealerId = dealerId;
            UpdateDealer.dealerName = dealerName;
            UpdateDealer.dealerContact = dealerContact;
            UpdateDealer.dealerAddress = dealerAddress;
            UpdateDealer.dealerCnic = dealerCnic;
            UpdateDealer.dealerAreaId = dealerAreaId;
            UpdateDealer.dealerArea = dealerArea;
            UpdateDealer.dealerCity = dealerCity;
            UpdateDealer.dealerType = dealerType;
            UpdateDealer.dealerLic9Num = dealerLic9Num;
            UpdateDealer.dealerLic9Exp = dealerLic9Exp;
            UpdateDealer.dealerLic10Num = dealerLic10Num;
            UpdateDealer.dealerLic10Exp = dealerLic10Exp;
            UpdateDealer.dealerLic11Num = dealerLic11Num;
            UpdateDealer.dealerLic11Exp = dealerLic11Exp;
            UpdateDealer.dealerStatus = dealerStatus;
            parent = FXMLLoader.load(getClass().getResource("/view/update_dealer.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setBody(parent);
        dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        stackPane.setVisible(true);
        dialog.show();
        dialog.setOverlayClose(false);
    }

    public void deleteDealer(String dealerTableId) throws IOException {
        DeleteWindow.sceneWindow = "ViewDealer";
        DeleteWindow.deletionId = dealerTableId;
        Parent parent = null;
        try {
            parent = FXMLLoader.load(getClass().getResource("/view/delete_window.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setStyle("-fx-padding: -40 -20 -40 -20;");
        dialogLayout.setBody(parent);
        dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        stackPane.setVisible(true);
        dialog.show();
        dialog.setOverlayClose(false);
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getDealerTableId() {
        return dealerTableId;
    }

    public void setDealerTableId(String dealerTableId) {
        this.dealerTableId = dealerTableId;
    }

    public String getDealerId() {
        return dealerId;
    }

    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getDealerContact() {
        return dealerContact;
    }

    public void setDealerContact(String dealerContact) {
        this.dealerContact = dealerContact;
    }

    public String getDealerAddress() {
        return dealerAddress;
    }

    public void setDealerAddress(String dealerAddress) {
        this.dealerAddress = dealerAddress;
    }

    public String getDealerAreaId() {
        return dealerAreaId;
    }

    public void setDealerAreaId(String dealerAreaId) {
        this.dealerAreaId = dealerAreaId;
    }

    public String getDealerArea() {
        return dealerArea;
    }

    public void setDealerArea(String dealerArea) {
        this.dealerArea = dealerArea;
    }

    public String getDealerCity() {
        return dealerCity;
    }

    public void setDealerCity(String dealerCity) {
        this.dealerCity = dealerCity;
    }

    public String getDealerCnic() {
        return dealerCnic;
    }

    public void setDealerCnic(String dealerCnic) {
        this.dealerCnic = dealerCnic;
    }

    public String getDealerLic9Num() {
        return dealerLic9Num;
    }
    public void setDealerLic9Num(String dealerLic9Num) {
        this.dealerLic9Num = dealerLic9Num;
    }
    public String getDealerLic9Exp() {
        return dealerLic9Exp;
    }
    public void setDealerLic9Exp(String dealerLic9Exp) {
        this.dealerLic9Exp = dealerLic9Exp;
    }

    public String getDealerLic10Num() {
        return dealerLic10Num;
    }
    public void setDealerLic10Num(String dealerLic10Num) {
        this.dealerLic10Num = dealerLic10Num;
    }
    public String getDealerLic10Exp() {
        return dealerLic10Exp;
    }
    public void setDealerLic10Exp(String dealerLic10Exp) {
        this.dealerLic10Exp = dealerLic10Exp;
    }

    public String getDealerLic11Num() {
        return dealerLic11Num;
    }
    public void setDealerLic11Num(String dealerLic11Num) {
        this.dealerLic11Num = dealerLic11Num;
    }
    public String getDealerLic11Exp() {
        return dealerLic11Exp;
    }
    public void setDealerLic11Exp(String dealerLic11Exp) {
        this.dealerLic11Exp = dealerLic11Exp;
    }

    public String getDealerType() {
        return dealerType;
    }

    public void setDealerType(String dealerType) {
        this.dealerType = dealerType;
    }

    public String getDealerStatus() {
        return dealerStatus;
    }

    public void setDealerStatus(String dealerStatus) {
        this.dealerStatus = dealerStatus;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public ArrayList<ArrayList<String>> getDealersDetail(Statement stmt, Connection con)
    {
        ArrayList<ArrayList<String>> dealersData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT `dealer_info`.`dealer_table_id`, `dealer_info`.`dealer_id`, `dealer_info`.`dealer_area_id`, `area_info`.`area_name`, `city_info`.`city_name`, `dealer_info`.`dealer_name`, `dealer_info`.`dealer_phone`, `dealer_info`.`dealer_address`, `dealer_info`.`dealer_type`, `dealer_info`.`dealer_cnic`,  `dealer_info`.`dealer_lic9_num`,`dealer_info`.`dealer_lic9_exp`,`dealer_info`.`dealer_lic10_num`, `dealer_info`.`dealer_lic10_exp`,`dealer_info`.`dealer_lic11_num`,`dealer_info`.`dealer_lic11_exp`, `dealer_info`.`dealer_status` FROM `dealer_info` LEFT OUTER JOIN `area_info` ON `dealer_info`.`dealer_area_id` = `area_info`.`area_table_id` AND `area_info`.`area_status` = 'Active' LEFT OUTER JOIN `city_info` ON `city_info`.`city_table_id` = `area_info`.`city_table_id` WHERE `dealer_info`.`dealer_status` != 'Deleted' ORDER BY `dealer_info`.`dealer_table_id`");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Table id
                temp.add(rs.getString(2)); // id
                temp.add(rs.getString(3)); // Area id
                temp.add(rs.getString(4)); // Area Name
                temp.add(rs.getString(5)); // City Name
                temp.add(rs.getString(6)); // name
                temp.add(rs.getString(7)); // contact
                temp.add(rs.getString(8)); // address
                temp.add(rs.getString(9)); // type
                temp.add(rs.getString(10)); // CNIC
                temp.add(rs.getString(11)); // Lic 9 Num
                temp.add(rs.getString(12)); // Lic 9 Exp
                temp.add(rs.getString(13)); // Lic 10 Num
                temp.add(rs.getString(14)); // Lic 10 Exp
                temp.add(rs.getString(15)); // Lic 11 Num
                temp.add(rs.getString(16)); // Lic 11 Exp
                temp.add(rs.getString(17)); // dealer status
                dealersData.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dealersData;
    }

    public ArrayList<ArrayList<String>> getDealersSearch(Statement stmt, Connection con, String dealerId, String dealerName, String dealerContact, String dealerCnic, String dealerLicNum, String dealerCityId, String dealerAreaId, String dealerType, String dealerStatus)
    {
        ArrayList<ArrayList<String>> dealersData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        String searchQuery;
        int multipleSearch = 0;
        searchQuery = "SELECT `dealer_info`.`dealer_table_id`, `dealer_info`.`dealer_id`, `dealer_info`.`dealer_area_id`, `area_info`.`area_name`, `city_info`.`city_name`, `dealer_info`.`dealer_name`, `dealer_info`.`dealer_phone`, `dealer_info`.`dealer_address`, `dealer_info`.`dealer_type`, `dealer_info`.`dealer_cnic`, `dealer_info`.`dealer_lic9_num`, `dealer_info`.`dealer_lic9_exp` ,  `dealer_info`.`dealer_lic10_num`, `dealer_info`.`dealer_lic10_exp` , `dealer_info`.`dealer_lic11_num`, `dealer_info`.`dealer_lic11_exp` , `dealer_info`.`dealer_status` FROM `dealer_info` LEFT OUTER JOIN `area_info` ON `dealer_info`.`dealer_area_id` = `area_info`.`area_table_id` AND `area_info`.`area_status` = 'Active' LEFT OUTER JOIN `city_info` ON `city_info`.`city_table_id` = `area_info`.`city_table_id` WHERE ";
        if(!dealerId.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`dealer_info`.`dealer_id` = '"+dealerId+"'";
            multipleSearch++;
        }
        if(!dealerName.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`dealer_info`.`dealer_name` = '"+dealerName+"'";
            multipleSearch++;
        }
        if(!dealerContact.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`dealer_info`.`dealer_phone` = '"+dealerContact+"'";
            multipleSearch++;
        }
        if(!dealerCnic.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`dealer_info`.`dealer_cnic` = '"+dealerCnic+"'";
            multipleSearch++;
        }

        if(!dealerCityId.equals("All"))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`area_info`.`city_table_id` = '"+dealerCityId+"'";
            multipleSearch++;
        }
        if(!dealerAreaId.equals("All"))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`dealer_info`.`dealer_area_id` = '"+dealerAreaId+"'";
            multipleSearch++;
        }
        if(!dealerType.equals("All"))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`dealer_info`.`dealer_type` = '"+dealerType+"'";
            multipleSearch++;
        }
        if(!dealerStatus.equals("All"))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`dealer_info`.`dealer_status` = '"+dealerStatus+"'";
            multipleSearch++;
        }
        if(multipleSearch >= 1)
        {
            searchQuery += " AND ";
        }
        searchQuery += "`dealer_info`.`dealer_status` != 'Deleted' ORDER BY `dealer_info`.`dealer_table_id`";
        try {
            rs = stmt.executeQuery(searchQuery);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Table id
                temp.add(rs.getString(2)); // id
                temp.add(rs.getString(3)); // Area id
                temp.add(rs.getString(4)); // Area Name
                temp.add(rs.getString(5)); // City Name
                temp.add(rs.getString(6)); // name
                temp.add(rs.getString(7)); // contact
                temp.add(rs.getString(8)); // address
                temp.add(rs.getString(9)); // type
                temp.add(rs.getString(10)); // CNIC
                if(rs.getString(11).equals(""))// license 9
                {
                    temp.add("0");
                }
                else
                {
                    temp.add(rs.getString(11));
                }
                if(rs.getString(12).equals(""))// license 9 exp
                {
                    temp.add("0");
                }
                else
                {
                    temp.add(rs.getString(12));
                }
                if(rs.getString(13).equals(""))// license 10
                {
                    temp.add("0");
                }
                else
                {
                    temp.add(rs.getString(13));
                }
                if(rs.getString(14).equals(""))// license 10 exp
                {
                    temp.add("0");
                }
                else
                {
                    temp.add(rs.getString(14));
                }
                if(rs.getString(15).equals(""))// license 11
                {
                    temp.add("0");
                }
                else
                {
                    temp.add(rs.getString(15));
                }
                if(rs.getString(16).equals(""))// license 11 exp
                {
                    temp.add("0");
                }
                else
                {
                    temp.add(rs.getString(16));
                }
                temp.add(rs.getString(17));// status

                dealersData.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dealersData;
    }

    public ArrayList<String> getDealersSummary(Statement stmt, Connection con)
    {
        ArrayList<String> groupData = new ArrayList<String>();
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT COUNT(`dealer_table_id`) FROM `dealer_info` WHERE `dealer_status` != 'Deleted'");
            while (rs.next())
            {
                groupData.add(rs.getString(1));
            }
            rs = stmt.executeQuery("SELECT COUNT(`dealer_table_id`) FROM `dealer_info` WHERE `dealer_status` = 'Active'");
            while (rs.next())
            {
                groupData.add(rs.getString(1));
            }
            rs = stmt.executeQuery("SELECT COUNT(`dealer_table_id`) FROM `dealer_info` WHERE `dealer_status` = 'In Active'");
            while (rs.next())
            {
                groupData.add(rs.getString(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return groupData;
    }

    private Node createEditIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.EDIT);
        graphic.setFontSize(20.0);
        return graphic;
    }

    private Node createDeleteIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.TRASH);
        graphic.setFontSize(20.0);
        return graphic;
    }
}
