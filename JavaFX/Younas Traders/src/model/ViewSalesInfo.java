package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import controller.DeleteWindow;
import controller.UpdateComment;
import controller.UpdateCompany;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

public class ViewSalesInfo {
    private String srNo;
    private String bookingDate;
    private String bookingDay;
    private String invoiceNo;
    private String dealerName;
    private String dealerContact;
    private String orderedItems;
    private String submittedItems;
    private String missedItems;
    private String returnedItems;
    private String missedPrice;
    private String returnPrice;
    private String invoicePrice;
    private String discountGiven;
    private String bookingUser;
    private String comment;
    private String invoiceStatus;

    private HBox operationsPane;
    private JFXButton btnComment;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    SimpleDateFormat fmt = new SimpleDateFormat("dd/MMM/yyyy");
    public static String orderId;

    public static StackPane stackPane;
    public static JFXDialog dialog;

    public static ArrayList<String> newIds = new ArrayList<>();

    public ViewSalesInfo() {
    }

    public ViewSalesInfo(String srNo, String bookingDate, String bookingDay, String invoiceNo, String dealerName, String dealerContact, String orderedItems, String submittedItems, String missedItems, String returnedItems, String missedPrice, String returnPrice, String invoicePrice, String discountGiven, String bookingUser, String invoiceStatus, String comment) {
        this.srNo = srNo;
        this.bookingDate = bookingDate;
        this.bookingDay = bookingDay;
        this.invoiceNo = invoiceNo;
        this.dealerName = dealerName;
        this.dealerContact = dealerContact;
        this.orderedItems = orderedItems;
        this.submittedItems = submittedItems;
        this.missedItems = missedItems;
        this.returnedItems = returnedItems;
        this.missedPrice = missedPrice;
        this.returnPrice = returnPrice;
        this.invoicePrice = invoicePrice;
        this.discountGiven = discountGiven;
        this.bookingUser = bookingUser;
        this.invoiceStatus = invoiceStatus;
        this.comment = comment;

        this.btnComment = new JFXButton();
        this.btnEdit = new JFXButton();
        this.btnDelete = new JFXButton();
        this.btnComment.setGraphic(GlobalVariables.createCommentIcon());
        this.btnEdit.setGraphic(GlobalVariables.createEditIcon());
        this.btnDelete.setGraphic(GlobalVariables.createDeleteIcon());
        this.btnComment.getStyleClass().add("btn_icon");
        this.btnEdit.getStyleClass().add("btn_icon");
        this.btnDelete.getStyleClass().add("btn_icon");
        this.operationsPane = new HBox(btnComment, btnEdit, btnDelete);
        this.btnComment.setOnAction((action)->commentClicked());
        this.btnEdit.setOnAction((action)->editClicked());
        this.btnDelete.setOnAction(event -> {
            try {
                deleteSales(this.invoiceNo);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
}

    private void commentClicked()
    {
        Parent parent = null;
        try {
            UpdateComment.commentType = "Sales";
            UpdateComment.orderId = this.invoiceNo;
            UpdateComment.comment = this.comment;
            parent = FXMLLoader.load(getClass().getResource("/view/update_comment.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setStyle("-fx-padding: -20 -10 -20 -10;");
        dialogLayout.setBody(parent);
        dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        stackPane.setVisible(true);
        dialog.show();
        dialog.setOverlayClose(false);
    }

    private void editClicked()
    {
        orderId = this.invoiceNo;
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/view_sale_detail.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(root);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    public void deleteSales(String saleId) throws IOException {
        DeleteWindow.sceneWindow = "ViewSales";
        DeleteWindow.deletionId = saleId;
        Parent parent = null;
        try {
            parent = FXMLLoader.load(getClass().getResource("/view/delete_window.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setStyle("-fx-padding: -40 -20 -40 -20;");
        dialogLayout.setBody(parent);
        dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        stackPane.setVisible(true);
        dialog.show();
        dialog.setOverlayClose(false);
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    public String getBookingDay() {
        return bookingDay;
    }

    public void setBookingDay(String bookingDay) {
        this.bookingDay = bookingDay;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getDealerContact() {
        return dealerContact;
    }

    public void setDealerContact(String dealerContact) {
        this.dealerContact = dealerContact;
    }

    public String getOrderedItems() {
        return orderedItems;
    }

    public void setOrderedItems(String orderedItems) {
        this.orderedItems = orderedItems;
    }

    public String getSubmittedItems() {
        return submittedItems;
    }

    public void setSubmittedItems(String submittedItems) {
        this.submittedItems = submittedItems;
    }

    public String getMissedItems() {
        return missedItems;
    }

    public void setMissedItems(String missedItems) {
        this.missedItems = missedItems;
    }

    public String getReturnedItems() {
        return returnedItems;
    }

    public void setReturnedItems(String returnedItems) {
        this.returnedItems = returnedItems;
    }

    public String getMissedPrice() {
        return missedPrice;
    }

    public void setMissedPrice(String missedPrice) {
        this.missedPrice = missedPrice;
    }

    public String getReturnPrice() {
        return returnPrice;
    }

    public void setReturnPrice(String returnPrice) {
        this.returnPrice = returnPrice;
    }

    public String getInvoicePrice() {
        return invoicePrice;
    }

    public void setInvoicePrice(String invoicePrice) {
        this.invoicePrice = invoicePrice;
    }

    public String getDiscountGiven() {
        return discountGiven;
    }

    public void setDiscountGiven(String discountGiven) {
        this.discountGiven = discountGiven;
    }

    public String getBookingUser() {
        return bookingUser;
    }

    public void setBookingUser(String bookingUser) {
        this.bookingUser = bookingUser;
    }

    public String getInvoiceStatus() {
        return invoiceStatus;
    }

    public void setInvoiceStatus(String invoiceStatus) {
        this.invoiceStatus = invoiceStatus;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnComment() {
        return btnComment;
    }

    public void setBtnComment(JFXButton btnComment) {
        this.btnComment = btnComment;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public ArrayList<ArrayList<String>> getSalesInfo(Statement stmt, Connection con, String fromDate, String toDate)
    {
        ArrayList<ArrayList<String>> saleData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        int day;
        String stDay = "";
        try {
            rs = stmt.executeQuery("SELECT `order_info`.`booking_date`, `order_info`.`order_id`, `dealer_info`.`dealer_name`, `dealer_info`.`dealer_phone`, SUM(`order_info_detailed`.`quantity`) as 'ordered_items', SUM(`order_info_detailed`.`submission_quantity`) as 'submitted_items', SUM(`order_info_detailed`.`quantity`-`order_info_detailed`.`submission_quantity`) as 'missed_items', SUM(`order_info_detailed`.`returned_quant`) as 'returned_items', (SUM(`order_info_detailed`.`final_price`)/SUM(`order_info_detailed`.`submission_quantity`))*SUM(`order_info_detailed`.`quantity`-`order_info_detailed`.`submission_quantity`) as 'missed_price', (SUM(`order_info_detailed`.`final_price`)/SUM(`order_info_detailed`.`submission_quantity`))*SUM(`order_info_detailed`.`returned_quant`) as 'return_price', SUM(`order_info_detailed`.`final_price`) as 'invoice_price', `order_info`.`discount` as 'discount_given', `user_info`.`user_name`, `order_info`.`status`, `order_info`.`comments`, `order_info`.`dealer_id`, `dealer_info`.`dealer_lic9_num`, `dealer_info`.`dealer_address`, `dealer_info`.`dealer_ntn`, `dealer_info`.`dealer_cnic`, `order_info`.`order_price` FROM `order_info` LEFT OUTER JOIN `dealer_info` ON `dealer_info`.`dealer_table_id` = `order_info`.`dealer_id` LEFT OUTER JOIN `order_info_detailed` ON `order_info_detailed`.`order_id` = `order_info`.`order_id` LEFT OUTER JOIN `user_info` ON `user_info`.`user_table_id` = `order_info`.`booking_user_id` WHERE `order_info`.`status` != 'Deleted' GROUP BY `order_info`.`order_id` ORDER BY str_to_date(`order_info`.`booking_date`, '%d/%b/%Y') DESC LIMIT 1000");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Booking Date
                Date objDate = fmt.parse(rs.getString(1));
                day = objDate.getDay();
                if(day == 0)
                    stDay = "Sunday";
                else if(day == 1)
                    stDay = "Monday";
                else if(day == 2)
                    stDay = "Tuesday";
                else if(day == 3)
                    stDay = "Wednesday";
                else if(day == 4)
                    stDay = "Thursday";
                else if(day == 5)
                    stDay = "Friday";
                else if(day == 6)
                    stDay = "Saturday";
                else
                    stDay = "N/A";
                temp.add(stDay); // Day
                temp.add(rs.getString(2)); // Invoice No
                temp.add(rs.getString(3)); // Dealer Name
                temp.add(rs.getString(4)); // Dealer Contact
                temp.add(rs.getString(5) == null || rs.getString(5).equals("") ? "0" : rs.getString(5)); // Ordered Items
                temp.add(rs.getString(6) == null || rs.getString(6).equals("") ? "0" : rs.getString(6)); // Submitted Items
                temp.add(rs.getString(7) == null || rs.getString(7).equals("") ? "0" : rs.getString(7)); // Missed Items
                temp.add(rs.getString(8) == null || rs.getString(8).equals("") ? "0" : rs.getString(8)); // Returned Items
                temp.add(rs.getString(9) == null || rs.getString(9).equals("") ? "0" : rs.getString(9)); // Missed Price
                temp.add(rs.getString(10) == null || rs.getString(10).equals("") ? "0" : rs.getString(10)); // Return price
                temp.add(rs.getString(11) == null || rs.getString(11).equals("") ? "0" : rs.getString(11)); // Invoice Price
                temp.add(rs.getString(12) == null || rs.getString(12).equals("") ? "0" : rs.getString(12)); // Discount Given
                temp.add(rs.getString(13)); // User Name
                temp.add(rs.getString(14)); // Invoice Status
                temp.add(rs.getString(15)); // Comment
                temp.add(rs.getString(16)); // Dealer Id
                temp.add(rs.getString(17)); // Dealer Lic
                temp.add(rs.getString(18)); // Dealer Address
                temp.add(rs.getString(19)); // Dealer NTN
                temp.add(rs.getString(20)); // Dealer CNIC
                temp.add(rs.getString(21)); // Order Price
                saleData.add(temp);
            }
        } catch (SQLException | ParseException e) {
            e.printStackTrace();
        }
        return saleData;
    }

    public ArrayList<ArrayList<String>> getSalesInfoSearch(Statement stmt, Connection con, String fromDate, String toDate, String day, String invoiceStatus)
    {
        ArrayList<ArrayList<String>> saleData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        int intDay;
        String stDay = "";
        String query = "SELECT `order_info`.`booking_date`, `order_info`.`order_id`, `dealer_info`.`dealer_name`, `dealer_info`.`dealer_phone`, SUM(`order_info_detailed`.`quantity`) as 'ordered_items', SUM(`order_info_detailed`.`submission_quantity`) as 'submitted_items', SUM(`order_info_detailed`.`quantity`-`order_info_detailed`.`submission_quantity`) as 'missed_items', SUM(`order_info_detailed`.`returned_quant`) as 'returned_items', (SUM(`order_info_detailed`.`final_price`)/SUM(`order_info_detailed`.`submission_quantity`))*SUM(`order_info_detailed`.`quantity`-`order_info_detailed`.`submission_quantity`) as 'missed_price', (SUM(`order_info_detailed`.`final_price`)/SUM(`order_info_detailed`.`submission_quantity`))*SUM(`order_info_detailed`.`returned_quant`) as 'return_price', SUM(`order_info_detailed`.`final_price`) as 'invoice_price', `order_info`.`discount` as 'discount_given', `user_info`.`user_name`, `order_info`.`status`, `order_info`.`comments`, `order_info`.`dealer_id`, `dealer_info`.`dealer_lic9_num`, `dealer_info`.`dealer_address`, `dealer_info`.`dealer_ntn`, `dealer_info`.`dealer_cnic`, `order_info`.`order_price` FROM `order_info` LEFT OUTER JOIN `dealer_info` ON `dealer_info`.`dealer_table_id` = `order_info`.`dealer_id` LEFT OUTER JOIN `order_info_detailed` ON `order_info_detailed`.`order_id` = `order_info`.`order_id` LEFT OUTER JOIN `user_info` ON `user_info`.`user_table_id` = `order_info`.`booking_user_id` WHERE ";
        if(!fromDate.equals(""))
        {
            query += "str_to_date(`booking_date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') AND ";
        }
        if(!toDate.equals(""))
        {
            query += "str_to_date(`booking_date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y') AND ";
        }
        if(!invoiceStatus.equals("All"))
        {
            query += "`order_info`.`status` = '"+invoiceStatus+"' AND ";
        }
        query += "`order_info`.`status` != 'Deleted' GROUP BY `order_info`.`order_id` ORDER BY str_to_date(`order_info`.`booking_date`, '%d/%b/%Y') DESC";
        try {
            rs = stmt.executeQuery(query);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Booking Date
                Date objDate = fmt.parse(rs.getString(1));
                intDay = objDate.getDay();
                if(intDay == 0)
                    stDay = "Sunday";
                else if(intDay == 1)
                    stDay = "Monday";
                else if(intDay == 2)
                    stDay = "Tuesday";
                else if(intDay == 3)
                    stDay = "Wednesday";
                else if(intDay == 4)
                    stDay = "Thursday";
                else if(intDay == 5)
                    stDay = "Friday";
                else if(intDay == 6)
                    stDay = "Saturday";
                else
                    stDay = "N/A";
                temp.add(stDay); // Day
                temp.add(rs.getString(2)); // Invoice No
                temp.add(rs.getString(3)); // Dealer Name
                temp.add(rs.getString(4)); // Dealer Contact
                temp.add(rs.getString(5) == null || rs.getString(5).equals("") ? "0" : rs.getString(5)); // Ordered Items
                temp.add(rs.getString(6) == null || rs.getString(6).equals("") ? "0" : rs.getString(6)); // Submitted Items
                temp.add(rs.getString(7) == null || rs.getString(7).equals("") ? "0" : rs.getString(7)); // Missed Items
                temp.add(rs.getString(8) == null || rs.getString(8).equals("") ? "0" : rs.getString(8)); // Returned Items
                temp.add(rs.getString(9) == null || rs.getString(9).equals("") ? "0" : rs.getString(9)); // Missed Price
                temp.add(rs.getString(10) == null || rs.getString(10).equals("") ? "0" : rs.getString(10)); // Return price
                temp.add(rs.getString(11) == null || rs.getString(11).equals("") ? "0" : rs.getString(11)); // Invoice Price
                temp.add(rs.getString(12) == null || rs.getString(12).equals("") ? "0" : rs.getString(12)); // Discount Given
                temp.add(rs.getString(13)); // User Name
                temp.add(rs.getString(14)); // Invoice Status
                temp.add(rs.getString(15)); // Comment
                temp.add(rs.getString(16)); // Dealer Id
                temp.add(rs.getString(17)); // Dealer Lic
                temp.add(rs.getString(18)); // Dealer Address
                temp.add(rs.getString(19)); // Dealer NTN
                temp.add(rs.getString(20)); // Dealer CNIC
                temp.add(rs.getString(21)); // Order Price
                if(!day.equals("All") && day.equals(stDay))
                {
                    saleData.add(temp);
                }
                else if(day.equals("All"))
                {
                    saleData.add(temp);
                }
            }
        } catch (SQLException | ParseException e) {
            e.printStackTrace();
        }
        return saleData;
    }

    public void insertNewServerSales(ArrayList<ArrayList<String>> saleData, ArrayList<ArrayList<String>> saleDetailData)
    {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        String status = "Pending";
        boolean check = false;
        ArrayList<String> dealerIds = new ArrayList<>();
        ArrayList<String> finalPrice = new ArrayList<>();
        ArrayList<String> bookingDates = new ArrayList<>();
        ArrayList<String> bookingTimes = new ArrayList<>();
        ArrayList<String> serverOrderIds = new ArrayList<>();
        ArrayList<String> orderIds = new ArrayList<>();
        int num = 0;
        try {
            String insertQuery = "INSERT INTO `order_info` (`dealer_id`, `product_id`, `quantity`, `unit`, `order_price`, `bonus`, `discount`, `waive_off_price`, `final_price`, `order_success`, `booking_latitude`, `booking_longitude`, `booking_area`, `booking_date`, `booking_time`, `booking_user_id`, `status`) VALUES ";
            for(int i=0; i<saleData.size(); i++)
            {
                serverOrderIds.add(saleData.get(i).get(0));
                if(i == saleData.size()-1)
                {
                    insertQuery += "('"+saleData.get(i).get(1)+"','"+saleData.get(i).get(2)+"','"+saleData.get(i).get(3)+"','"+saleData.get(i).get(4)+"' ,'"+saleData.get(i).get(5)+"' ,'"+saleData.get(i).get(6)+"','"+saleData.get(i).get(7)+"','"+saleData.get(i).get(8)+"', '"+saleData.get(i).get(9)+"','"+saleData.get(i).get(10)+"','"+saleData.get(i).get(11)+"','"+saleData.get(i).get(12)+"','"+saleData.get(i).get(13)+"','"+saleData.get(i).get(14)+"','"+saleData.get(i).get(15)+"','"+saleData.get(i).get(16)+"','"+status+"') ";
                }
                else
                {
                    insertQuery += "('"+saleData.get(i).get(1)+"','"+saleData.get(i).get(2)+"','"+saleData.get(i).get(3)+"','"+saleData.get(i).get(4)+"' ,'"+saleData.get(i).get(5)+"' ,'"+saleData.get(i).get(6)+"','"+saleData.get(i).get(7)+"','"+saleData.get(i).get(8)+"', '"+saleData.get(i).get(9)+"','"+saleData.get(i).get(10)+"','"+saleData.get(i).get(11)+"','"+saleData.get(i).get(12)+"','"+saleData.get(i).get(13)+"','"+saleData.get(i).get(14)+"','"+saleData.get(i).get(15)+"','"+saleData.get(i).get(16)+"','"+status+"'), ";
                }
                dealerIds.add(saleData.get(i).get(1));
                finalPrice.add(saleData.get(i).get(9));
                bookingDates.add(saleData.get(i).get(14));
                bookingTimes.add(saleData.get(i).get(15));
            }
            objStmt.executeUpdate(insertQuery);

            ResultSet rs = null;
            for(int i=0; i<dealerIds.size(); i++)
            {
                String query = "SELECT `order_id` FROM `order_info` WHERE `dealer_id` = '"+dealerIds.get(i)+"' AND `final_price` = '"+finalPrice.get(i)+"' AND `booking_date` = '"+bookingDates.get(i)+"' AND `booking_time` = '"+bookingTimes.get(i)+"' LIMIT 1";
                rs = objStmt.executeQuery(query);
                while (rs.next())
                {
                    orderIds.add(rs.getString(1));
                }
            }
            newIds = orderIds;

            for(int i=0; i<orderIds.size(); i++)
            {
                check = false;
                String query = "INSERT INTO `order_info_detailed`(`order_id`, `product_table_id`, `batch_id`, `quantity`, `submission_quantity`, `unit`, `submission_unit`, `order_price`, `bonus_quant`, `discount`, `final_price`, `cmplt_returned_bit`, `returned_quant`, `returned_bonus_quant`) VALUES ";
                while (saleDetailData.size() > num && serverOrderIds.get(i).equals(saleDetailData.get(num).get(0)))
                {
                    if(check)
                        query += ", ";
                    query += "('"+orderIds.get(i)+"','"+saleDetailData.get(num).get(1)+"','"+saleDetailData.get(num).get(2)+"','"+saleDetailData.get(num).get(3)+"','"+saleDetailData.get(num).get(3)+"','"+saleDetailData.get(num).get(4)+"','"+saleDetailData.get(num).get(4)+"','"+saleDetailData.get(num).get(5)+"','"+saleDetailData.get(num).get(6)+"','"+saleDetailData.get(num).get(7)+"','"+saleDetailData.get(num).get(8)+"','0','0','0')";
                    num++;
                    check = true;
                }
                if(check)
                {
                    objStmt.executeUpdate(query);
                }
            }
            SendServerThread objSendServerThread = new SendServerThread(serverOrderIds, orderIds, "Set new Invoice Status");
            objSendServerThread.setDaemon(true);
            objSendServerThread.start();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
