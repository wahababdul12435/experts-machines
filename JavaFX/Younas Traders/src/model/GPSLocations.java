package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class GPSLocations {

    public void insertMobileGPSLocations(ArrayList<ArrayList<String>> mobileGPSLocationsData)
    {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        ArrayList<String> serverMobLocId = new ArrayList<>();
        int num = 0;
        try {
            String insertQuery = "INSERT INTO `mobile_gps_location`(`user_id`, `latitude`, `longitude`, `loc_name`, `date`, `time`, `server_sync`) VALUES ";
            for(int i=0; i<mobileGPSLocationsData.size(); i++)
            {
                serverMobLocId.add(mobileGPSLocationsData.get(i).get(0));
                if(i == mobileGPSLocationsData.size()-1)
                {
                    insertQuery += "('"+mobileGPSLocationsData.get(i).get(1)+"','"+mobileGPSLocationsData.get(i).get(2)+"','"+mobileGPSLocationsData.get(i).get(3)+"','"+mobileGPSLocationsData.get(i).get(4)+"' ,'"+mobileGPSLocationsData.get(i).get(5)+"' ,'"+mobileGPSLocationsData.get(i).get(6)+"','Done') ";
                }
                else
                {
                    insertQuery += "('"+mobileGPSLocationsData.get(i).get(1)+"','"+mobileGPSLocationsData.get(i).get(2)+"','"+mobileGPSLocationsData.get(i).get(3)+"','"+mobileGPSLocationsData.get(i).get(4)+"' ,'"+mobileGPSLocationsData.get(i).get(5)+"' ,'"+mobileGPSLocationsData.get(i).get(6)+"','Done'), ";
                }
            }
//            System.out.println(insertQuery);
            objStmt.executeUpdate(insertQuery);

            SendServerThread objSendServerThread = new SendServerThread(serverMobLocId, "Set mobile gps Status");
            objSendServerThread.setDaemon(true);
            objSendServerThread.start();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertOrderGPSLocations(ArrayList<ArrayList<String>> orderGPSLocationsData)
    {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        ArrayList<String> serverOrderLocId = new ArrayList<>();
        int num = 0;
        try {
            String insertQuery = "INSERT INTO `order_gps_location`(`order_id`, `user_id`, `latitude`, `longitude`, `loc_name`, `date`, `time`, `dealer_id`, `price`, `type`, `server_sync`) VALUES ";
            for(int i=0; i<orderGPSLocationsData.size(); i++)
            {
                serverOrderLocId.add(orderGPSLocationsData.get(i).get(0));
                if(i == orderGPSLocationsData.size()-1)
                {
                    insertQuery += "('"+orderGPSLocationsData.get(i).get(1)+"','"+orderGPSLocationsData.get(i).get(2)+"','"+orderGPSLocationsData.get(i).get(3)+"','"+orderGPSLocationsData.get(i).get(4)+"' ,'"+orderGPSLocationsData.get(i).get(5)+"' ,'"+orderGPSLocationsData.get(i).get(6)+"' ,'"+orderGPSLocationsData.get(i).get(7)+"' ,'"+orderGPSLocationsData.get(i).get(8)+"' ,'"+orderGPSLocationsData.get(i).get(9)+"','"+orderGPSLocationsData.get(i).get(10)+"','Done') ";
                }
                else
                {
                    insertQuery += "('"+orderGPSLocationsData.get(i).get(1)+"','"+orderGPSLocationsData.get(i).get(2)+"','"+orderGPSLocationsData.get(i).get(3)+"','"+orderGPSLocationsData.get(i).get(4)+"' ,'"+orderGPSLocationsData.get(i).get(5)+"' ,'"+orderGPSLocationsData.get(i).get(6)+"' ,'"+orderGPSLocationsData.get(i).get(7)+"' ,'"+orderGPSLocationsData.get(i).get(8)+"' ,'"+orderGPSLocationsData.get(i).get(9)+"','"+orderGPSLocationsData.get(i).get(10)+"','Done'), ";
                }
            }
            System.out.println(insertQuery);
            objStmt.executeUpdate(insertQuery);

            SendServerThread objSendServerThread = new SendServerThread(serverOrderLocId, "Set order gps Status");
            objSendServerThread.setDaemon(true);
            objSendServerThread.start();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
