package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import controller.UpdatePendingOrder;
import controller.UpdateUser;
import javafx.fxml.FXML;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class UpdatePendingOrderInfo {
    private String srNo;
    private String productName;
    private String batch;
    private String quantity;
    private String unit;
    private String orderItemPrice;
    private String orderItemBonus;
    private String orderItemDiscount;
    private String orderItemTotalPrice;

    private HBox operationsPane;
    private JFXButton btnDelete;

    public static TableView<UpdatePendingOrderInfo> table_ordered_products;
    public static JFXTextField total_order_price;
    public static JFXTextField total_bonus_packs;
    public static JFXTextField total_discount_price;
    public static JFXTextField total_final_price;

    public ArrayList<ArrayList<String>> productsData = new ArrayList<>();
    public ArrayList<ArrayList<String>> batchData = new ArrayList<>();


    public UpdatePendingOrderInfo() {
        this.srNo = "";
        this.productName = "";
        this.batch = "";
        this.quantity = "";
        this.unit = "";
        this.orderItemPrice = "";
        this.orderItemBonus = "";
        this.orderItemDiscount = "";
        this.orderItemTotalPrice = "";

        this.btnDelete = new JFXButton("Delete");
        this.operationsPane = new HBox(btnDelete);
    }

    public UpdatePendingOrderInfo(String srNo, String productName, String batch, String quantity, String unit, String orderItemPrice, String orderItemBonus, String orderItemDiscount, String orderItemTotalPrice) {
        this.srNo = srNo;
        this.productName = productName;
        this.batch = batch;
        this.quantity = quantity;
        this.unit = unit;
        this.orderItemPrice = orderItemPrice;
        this.orderItemBonus = orderItemBonus;
        this.orderItemDiscount = orderItemDiscount;
        this.orderItemTotalPrice = orderItemTotalPrice;

        this.btnDelete = new JFXButton("Delete");
        this.btnDelete.setOnAction((action)->deleteProduct());
        this.operationsPane = new HBox(btnDelete);
    }

    public void deleteProduct()
    {
        UpdatePendingOrder.productsId.remove(Integer.parseInt(srNo)-1);
        UpdatePendingOrder.productsName.remove(Integer.parseInt(srNo)-1);
        UpdatePendingOrder.batch.remove(Integer.parseInt(srNo)-1);
        UpdatePendingOrder.quantity.remove(Integer.parseInt(srNo)-1);
        UpdatePendingOrder.unit.remove(Integer.parseInt(srNo)-1);
        UpdatePendingOrder.productsPrice.remove(Integer.parseInt(srNo)-1);
        UpdatePendingOrder.productsBonus.remove(Integer.parseInt(srNo)-1);
        UpdatePendingOrder.productsDiscount.remove(Integer.parseInt(srNo)-1);
        UpdatePendingOrder.productsFinalPrice.remove(Integer.parseInt(srNo)-1);
        table_ordered_products.setItems(UpdatePendingOrder.parseUserList());
        UpdatePendingOrder.calculatePrices();
        setAllPrices();
    }

    public void setAllPrices()
    {
        DecimalFormat df = new DecimalFormat("0.00");
        total_order_price.setText(df.format(UpdatePendingOrder.totalOrderPrice));
        total_bonus_packs.setText(String.valueOf(UpdatePendingOrder.totalBonus));
        total_discount_price.setText(df.format(UpdatePendingOrder.totalDiscount));
        total_final_price.setText(df.format(UpdatePendingOrder.totalFinalPrice));
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getOrderItemPrice() {
        return orderItemPrice;
    }

    public void setOrderItemPrice(String orderItemPrice) {
        this.orderItemPrice = orderItemPrice;
    }

    public String getOrderItemBonus() {
        return orderItemBonus;
    }

    public void setOrderItemBonus(String orderItemBonus) {
        this.orderItemBonus = orderItemBonus;
    }

    public String getOrderItemDiscount() {
        return orderItemDiscount;
    }

    public void setOrderItemDiscount(String orderItemDiscount) {
        this.orderItemDiscount = orderItemDiscount;
    }

    public String getOrderItemTotalPrice() {
        return orderItemTotalPrice;
    }

    public void setOrderItemTotalPrice(String orderItemTotalPrice) {
        this.orderItemTotalPrice = orderItemTotalPrice;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public ArrayList<ArrayList<String>> getProductsData()
    {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement stmt = objMysqlCon.stmt;
        Connection con = objMysqlCon.con;
        ResultSet rs = null;
        ArrayList<String> temp;
        try {
            rs = stmt.executeQuery("SELECT `product_id`, `product_name`, `retail_price` FROM `product_info`");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2));
                temp.add(rs.getString(3));
                productsData.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return productsData;
    }

    public ArrayList<ArrayList<String>> getBatchData()
    {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement stmt = objMysqlCon.stmt;
        Connection con = objMysqlCon.con;
        ResultSet rs = null;
        ArrayList<String> temp;
        try {
            rs = stmt.executeQuery("SELECT DISTINCT `batch_no`, `batch_stock_id`, `prod_id`  FROM `batchwise_stock` GROUP BY `batch_no`");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(2));
                temp.add(rs.getString(1));
                temp.add(rs.getString(3));
                batchData.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return batchData;
    }
}
