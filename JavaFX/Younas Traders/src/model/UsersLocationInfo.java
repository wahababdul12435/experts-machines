package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class UsersLocationInfo {

    public ArrayList<ArrayList<String>> getTodayActiveUsersDetail(Statement stmt, Connection con, String date)
    {
        ArrayList<ArrayList<String>> usersActiveToday = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            String query = "SELECT `mobile_gps_location`.`user_id`, `user_info`.`user_name`, `user_info`.`user_image`, `user_accounts`.`current_status`, SUM(case when `order_info`.`booking_user_id` = `mobile_gps_location`.`user_id` AND `order_info`.`booking_date` = '"+date+"' AND `order_info`.`status` != 'Deleted' then 1 else 0 end) as 'total_booked', SUM(case when `order_info`.`delivered_user_id` = `mobile_gps_location`.`user_id` AND `order_info`.`delivered_date` = '"+date+"' AND `order_info`.`status` = 'Delivered' then 1 else 0 end) as 'total_delivered', SUM(case when `order_return`.`enter_user_id` = `mobile_gps_location`.`user_id` AND `order_return`.`entry_date` = '"+date+"' AND `order_return`.`status` != 'Deleted' then 1 else 0 end) as 'total_returned', ROUND(COALESCE(SUM(case when `order_info`.`booking_user_id` = `mobile_gps_location`.`user_id` AND `order_info`.`booking_date` = '"+date+"' AND `order_info`.`status` != 'Deleted' then `order_info`.`final_price` else 0 end),0),2) as 'booking_price', ROUND(COALESCE(SUM(case when `order_info`.`delivered_user_id` = `mobile_gps_location`.`user_id` AND `order_info`.`delivered_date` = '"+date+"' AND `order_info`.`status` = 'Delivered' then `order_info`.`final_price` else 0 end),0),2) as 'delivered_price', ROUND(COALESCE(SUM(case when `order_return`.`enter_user_id` = `mobile_gps_location`.`user_id` AND `order_return`.`entry_date` = '"+date+"' AND `order_return`.`status` != 'Deleted' then `order_return`.`return_total_price` else 0 end),0),2) as 'returned_price', ROUND(COALESCE(SUM(case when `dealer_payments`.`user_id` = `mobile_gps_location`.`user_id` AND `dealer_payments`.`date` = '"+date+"' AND `dealer_payments`.`cash_type` = 'Collected' then `dealer_payments`.`amount` else 0 end),0),2) as 'cash_collected' FROM `mobile_gps_location` INNER JOIN `user_info` ON `user_info`.`user_table_id` = `mobile_gps_location`.`user_id` INNER JOIN `user_accounts` ON `user_accounts`.`user_id` = `mobile_gps_location`.`user_id` LEFT OUTER JOIN `order_info` ON `order_info`.`booking_user_id` = `mobile_gps_location`.`user_id` OR `order_info`.`delivered_user_id` = `mobile_gps_location`.`user_id` LEFT OUTER JOIN `order_return` ON `order_return`.`enter_user_id` = `mobile_gps_location`.`user_id` LEFT OUTER JOIN `dealer_payments` ON `dealer_payments`.`user_id` = `mobile_gps_location`.`user_id` WHERE `mobile_gps_location`.`date` = '"+date+"' GROUP BY `mobile_gps_location`.`user_id`";
//            System.out.println(query);
            String query2 = "";
            rs = stmt.executeQuery(query);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // User Id
                temp.add(rs.getString(2)); // User Name
                temp.add(rs.getString(3)); // Image
                temp.add(rs.getString(4)); // Current Status
                temp.add(rs.getString(5)); // Total Booked
                temp.add(rs.getString(6)); // Total Delivered
                temp.add(rs.getString(7)); // Total Returned
                temp.add(rs.getString(8)); // Booking Price
                temp.add(rs.getString(9)); // Delivered Price
                temp.add(rs.getString(10)); // Returned Price
                temp.add(rs.getString(11)); // Cash Collected
                usersActiveToday.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return usersActiveToday;
    }
}
