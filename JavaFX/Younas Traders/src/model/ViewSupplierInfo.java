package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import controller.DeleteWindow;
import controller.UpdateProduct;
import controller.UpdateSupplier;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;

public class ViewSupplierInfo {
    private String srNo;
    private String supplierTableId;
    private String supplierId;
    private String supplierName;
    private String supplierEmail;
    private String supplierContact;
    private String contactPerson;
    private String supplierAddress;
    private String supplierCity;
    private String supplierCompaniesId;
    private String supplierCompanies;
    private String supplierStatus;
    private HBox operationsPane;
    private JFXButton btnView;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    public static String viewSupplierId;
    public static StackPane stackPane;
    public static JFXDialog dialog;
    public static Label lblUpdate;

    public ViewSupplierInfo() {
    }

    public ViewSupplierInfo(String srNo, String supplierTableId, String supplierId, String supplierName, String supplierEmail, String supplierContact, String contactPerson, String supplierAddress, String supplierCity, String supplierCompaniesId, String supplierCompanies, String supplierStatus) {
        this.srNo = srNo;
        this.supplierTableId = supplierTableId;
        this.supplierId = supplierId;
        this.supplierName = supplierName;
        this.supplierEmail = supplierEmail;
        this.supplierContact = supplierContact;
        this.contactPerson = contactPerson;
        this.supplierAddress = supplierAddress;
        this.supplierCity = supplierCity;
        this.supplierCompaniesId = supplierCompaniesId;
        this.supplierCompanies = supplierCompanies;
        this.supplierStatus = supplierStatus;

        this.btnView = new JFXButton();
        this.btnEdit = new JFXButton();
        this.btnDelete = new JFXButton();
        this.btnView.setGraphic(GlobalVariables.createViewIcon());

        this.operationsPane = new HBox();

        this.btnView.getStyleClass().add("btn_icon");
        this.btnView.setOnAction((action)->viewClicked());
        this.operationsPane.getChildren().add(this.btnView);

        if(GlobalVariables.rightEdit)
        {
            this.btnEdit.setGraphic(GlobalVariables.createEditIcon());
            this.btnEdit.getStyleClass().add("btn_icon");
            this.btnEdit.setOnAction((action)->editClicked());
            this.operationsPane.getChildren().add(this.btnEdit);
        }
        if(GlobalVariables.rightDelete)
        {
            this.btnDelete.setGraphic(GlobalVariables.createDeleteIcon());
            this.btnDelete.getStyleClass().add("btn_icon");
            this.btnDelete.setOnAction(event -> {
                try {
                    deleteSupplier(this.supplierTableId);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            this.operationsPane.getChildren().add(this.btnDelete);
        }
    }

    public void viewClicked()
    {
        Parent parent = null;
        viewSupplierId = this.supplierTableId;
        try {
            parent = FXMLLoader.load(getClass().getResource("/view/supplier_detail.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(parent);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    public void editClicked()
    {
        Parent parent = null;
        try {
            UpdateSupplier.supplierTableId = supplierTableId;
            UpdateSupplier.supplierId = supplierId;
            UpdateSupplier.supplierName = supplierName;
            UpdateSupplier.supplierAddress = supplierAddress;
            UpdateSupplier.supplierCity = supplierCity;
            UpdateSupplier.supplierEmail = supplierEmail;
            UpdateSupplier.contactPerson = contactPerson;
            UpdateSupplier.supplierContact = supplierContact;
            UpdateSupplier.supplierStatus = supplierStatus;

            ArrayList<String> companyIds = new ArrayList<>();
            if(supplierCompaniesId != null)
            {
                String[] arr = supplierCompaniesId.split("\n");
                for (String companyData:arr) {
                    companyIds.add(companyData);
                }
            }
//            else
//            {
//                companyIds.add("");
//            }

            ArrayList<String> companyNames = new ArrayList<>();
            if(supplierCompanies != null)
            {
                String[] arrId = supplierCompanies.split("\n");
                for (String companyDataId:arrId) {
                    companyNames.add(companyDataId);
                }
            }
//            else
//            {
//                companyNames.add("");
//            }

            UpdateSupplier.supplierCompanyIds = companyIds;
            UpdateSupplier.supplierCompanyNames = companyNames;

            parent = FXMLLoader.load(getClass().getResource("/view/update_supplier.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setStyle("-fx-padding: -20 -10 -20 -10;");
        dialogLayout.setBody(parent);
        dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        stackPane.setVisible(true);
        dialog.show();
        dialog.setOverlayClose(false);
    }

    public void deleteSupplier(String supplierTableId) throws IOException {
        DeleteWindow.sceneWindow = "ViewSupplier";
        DeleteWindow.deletionId = supplierTableId;
        Parent parent = null;
        try {
            parent = FXMLLoader.load(getClass().getResource("/view/delete_window.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setStyle("-fx-padding: -40 -20 -40 -20;");
        dialogLayout.setBody(parent);
        dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        stackPane.setVisible(true);
        dialog.show();
        dialog.setOverlayClose(false);
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getSupplierTableId() {
        return supplierTableId;
    }

    public void setSupplierTableId(String supplierTableId) {
        this.supplierTableId = supplierTableId;
    }

    public String getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getSupplierAddress() {
        return supplierAddress;
    }

    public void setSupplierAddress(String supplierAddress) {
        this.supplierAddress = supplierAddress;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getSupplierContact() {
        return supplierContact;
    }

    public void setSupplierContact(String supplierContact) {
        this.supplierContact = supplierContact;
    }

    public String getSupplierEmail() {
        return supplierEmail;
    }

    public void setSupplierEmail(String supplierEmail) {
        this.supplierEmail = supplierEmail;
    }

    public String getSupplierCity() {
        return supplierCity;
    }

    public void setSupplierCity(String supplierCity) {
        this.supplierCity = supplierCity;
    }

    public String getSupplierCompanies() {
        return supplierCompanies;
    }

    public void setSupplierCompanies(String supplierCompanies) {
        this.supplierCompanies = supplierCompanies;
    }

    public String getSupplierStatus() {
        return supplierStatus;
    }

    public void setSupplierStatus(String supplierStatus) {
        this.supplierStatus = supplierStatus;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public static StackPane getStackPane() {
        return stackPane;
    }

    public static void setStackPane(StackPane stackPane) {
        ViewSupplierInfo.stackPane = stackPane;
    }

    public static JFXDialog getDialog() {
        return dialog;
    }

    public static void setDialog(JFXDialog dialog) {
        ViewSupplierInfo.dialog = dialog;
    }

    public static Label getLblUpdate() {
        return lblUpdate;
    }

    public static void setLblUpdate(Label lblUpdate) {
        ViewSupplierInfo.lblUpdate = lblUpdate;
    }


    public ArrayList<ArrayList<String>> getSupplierInfo(Statement stmt, Connection con)
    {
        ArrayList<ArrayList<String>> supplierData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        temp = new ArrayList<String>();
        ResultSet rs = null;
        String preSupplierId = "";
        String allocatedCompany  = "";
        String allocatedCompanyId  = "";
        boolean iter = false;
        try {
            rs = stmt.executeQuery("SELECT `supplier_info`.`supplier_table_id`, `supplier_info`.`supplier_id`, `supplier_info`.`supplier_name`, `supplier_info`.`supplier_email`, `supplier_info`.`supplier_contact`, `supplier_info`.`contact_person`, `supplier_info`.`supplier_address`, `supplier_info`.`supplier_city`, `company_info`.`company_table_id`, `company_info`.`company_name`, `supplier_info`.`supplier_status` FROM `supplier_info` LEFT OUTER JOIN `supplier_company` ON `supplier_info`.`supplier_table_id` = `supplier_company`.`supplier_id` LEFT OUTER JOIN `company_info` ON `company_info`.`company_table_id` = `supplier_company`.`company_id` WHERE `supplier_info`.`supplier_status` != 'Deleted' ORDER BY `supplier_info`.`supplier_table_id`");
            while (rs.next())
            {
                if(preSupplierId.equals(rs.getString(1)))
                {
                    allocatedCompanyId += "\n"+rs.getString(9);
                    allocatedCompany += "\n"+rs.getString(10);
                }
                else
                {
                    if(iter)
                    {
                        temp.set(8, allocatedCompanyId);
                        temp.set(9, allocatedCompany);
                        supplierData.add(temp);
                        allocatedCompanyId = rs.getString(9);
                        allocatedCompany = rs.getString(10);
                        temp = new ArrayList<String>();
                        preSupplierId = rs.getString(1);
                    }
                    else
                    {
                        iter = true;
                        allocatedCompanyId = rs.getString(9);
                        allocatedCompany = rs.getString(10);
                        temp = new ArrayList<>();
                        preSupplierId = rs.getString(1);
                    }
                    temp.add(rs.getString(1)); // Table Id
                    temp.add(rs.getString(2)); // Id
                    temp.add(rs.getString(3)); // Name
                    temp.add(rs.getString(4)); // Email
                    temp.add(rs.getString(5)); // Contact
                    temp.add(rs.getString(6)); // Contact Person
                    temp.add(rs.getString(7)); // Address
                    temp.add(rs.getString(8)); // City
                    temp.add(rs.getString(9)); // Company Id
                    temp.add(rs.getString(10)); // Company Name
                    temp.add(rs.getString(11)); // Status
                }
            }
            if(iter)
            {
                temp.set(8, allocatedCompanyId);
                temp.set(9, allocatedCompany);
                supplierData.add(temp);
            }

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return supplierData;
    }

    public ArrayList<ArrayList<String>> getSupplierSearch(Statement stmt, Connection con, String supplierId, String supplierName, String supplierContact, String supplierEmail, String supplierStatus)
    {
        ArrayList<ArrayList<String>> supplierData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        temp = new ArrayList<String>();
        ResultSet rs = null;
        String preSupplierId = "";
        String allocatedCompany  = "";
        String allocatedCompanyId  = "";
        boolean iter = false;
        String searchQuery;
        int multipleSearch = 0;
        searchQuery = "SELECT `supplier_info`.`supplier_table_id`, `supplier_info`.`supplier_id`, `supplier_info`.`supplier_name`, `supplier_info`.`supplier_email`, `supplier_info`.`supplier_contact`, `supplier_info`.`contact_person`, `supplier_info`.`supplier_address`, `supplier_info`.`supplier_city`, `company_info`.`company_table_id`, `company_info`.`company_name`, `supplier_info`.`supplier_status` FROM `supplier_info` LEFT OUTER JOIN `supplier_company` ON `supplier_info`.`supplier_table_id` = `supplier_company`.`supplier_id` LEFT OUTER JOIN `company_info` ON `company_info`.`company_table_id` = `supplier_company`.`company_id` WHERE ";
        if(!supplierId.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`supplier_info`.`supplier_id` = '"+supplierId+"'";
            multipleSearch++;
        }
        if(!supplierName.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`supplier_info`.`supplier_name` = '"+supplierName+"'";
            multipleSearch++;
        }
        if(!supplierContact.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`supplier_info`.`supplier_contact` = '"+supplierContact+"'";
            multipleSearch++;
        }
        if(!supplierEmail.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`supplier_info`.`supplier_email` = '"+supplierEmail+"'";
            multipleSearch++;
        }
        if(!supplierStatus.equals("All"))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`supplier_info`.`supplier_status` = '"+supplierStatus+"'";
            multipleSearch++;
        }
        if(multipleSearch >= 1)
        {
            searchQuery += " AND ";
        }
        searchQuery += "`supplier_info`.`supplier_status` != 'Deleted' ORDER BY `supplier_info`.`supplier_table_id`";
        try {
            rs = stmt.executeQuery(searchQuery);
            while (rs.next())
            {
                if(preSupplierId.equals(rs.getString(1)))
                {
                    allocatedCompanyId += "\n"+rs.getString(9);
                    allocatedCompany += "\n"+rs.getString(10);
                }
                else
                {
                    if(iter)
                    {
                        temp.set(8, allocatedCompanyId);
                        temp.set(9, allocatedCompany);
                        supplierData.add(temp);
                        allocatedCompanyId = rs.getString(9);
                        allocatedCompany = rs.getString(10);
                        temp = new ArrayList<String>();
                        preSupplierId = rs.getString(1);
                    }
                    else
                    {
                        iter = true;
                        allocatedCompanyId = rs.getString(9);
                        allocatedCompany = rs.getString(10);
                        temp = new ArrayList<>();
                        preSupplierId = rs.getString(1);
                    }
                    temp.add(rs.getString(1)); // Table Id
                    temp.add(rs.getString(2)); // Id
                    temp.add(rs.getString(3)); // Name
                    temp.add(rs.getString(4)); // Email
                    temp.add(rs.getString(5)); // Contact
                    temp.add(rs.getString(6)); // Contact Person
                    temp.add(rs.getString(7)); // Address
                    temp.add(rs.getString(8)); // City
                    temp.add(rs.getString(9)); // Company Id
                    temp.add(rs.getString(10)); // Company Name
                    temp.add(rs.getString(11)); // Status
                }
            }
            if(iter)
            {
                temp.set(8, allocatedCompanyId);
                temp.set(9, allocatedCompany);
                supplierData.add(temp);
            }

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return supplierData;
    }

    public ArrayList<String> getSuppliersSummary(Statement stmt, Connection con)
    {
        ArrayList<String> groupData = new ArrayList<String>();
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT COUNT(`supplier_table_id`) FROM `supplier_info` WHERE `supplier_status` != 'Deleted'");
            while (rs.next())
            {
                groupData.add(rs.getString(1));
            }
            rs = stmt.executeQuery("SELECT COUNT(`supplier_table_id`) FROM `supplier_info` WHERE `supplier_status` = 'Active'");
            while (rs.next())
            {
                groupData.add(rs.getString(1));
            }
            rs = stmt.executeQuery("SELECT COUNT(`supplier_table_id`) FROM `supplier_info` WHERE `supplier_status` = 'In Active'");
            while (rs.next())
            {
                groupData.add(rs.getString(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return groupData;
    }
}
