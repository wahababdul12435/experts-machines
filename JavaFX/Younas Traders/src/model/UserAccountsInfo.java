package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import controller.MyAccount;
import controller.UpdateArea;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.HBox;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class UserAccountsInfo {
    private String srNo;
    private String userId;
    private String userName;
    private String userType;
    private String rightInsert;
    private String rightView;
    private String rightChange;
    private String rightDelete;
    private String rightStock;
    private String rightCash;
    private String rightSettings;
    private String mobileApp;
    private String userStatus;

    private HBox operationsPane;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    public static String getAccountDetail;

    public UserAccountsInfo() {
        this.srNo = "";
        this.userId = "";
        this.userName = "";
        this.rightInsert = "";
        this.rightView = "";
        this.rightChange = "";
        this.rightDelete = "";
        this.rightStock = "";
        this.rightCash = "";
        this.rightSettings = "";
        this.mobileApp = "";
        this.userStatus = "";
        getAccountDetail = null;
    }

    public UserAccountsInfo(String srNo, String userId, String userName, String userType, String rightInsert, String rightView, String rightChange, String rightDelete, String rightStock, String rightCash, String rightSettings, String mobileApp, String userStatus) {
        this.srNo = srNo;
        this.userId = userId;
        this.userName = userName;
        this.userType = userType;
        this.rightInsert = rightInsert;
        this.rightView = rightView;
        this.rightChange = rightChange;
        this.rightDelete = rightDelete;
        this.rightStock = rightStock;
        this.rightCash = rightCash;
        this.rightSettings = rightSettings;
        this.mobileApp = mobileApp;
        this.userStatus = userStatus;

        this.btnEdit = new JFXButton();
        this.btnDelete = new JFXButton();
        this.btnEdit.setGraphic(createErrorIcon());
        this.btnDelete.setGraphic(GlobalVariables.createDeleteIcon());
        this.btnEdit.setOnAction((event)->viewAccountDetail());
        this.operationsPane = new HBox(btnEdit, btnDelete);

        getAccountDetail = null;
    }

    public void viewAccountDetail()
    {
        try {
            getAccountDetail = this.userId;
            Parent root = FXMLLoader.load(getClass().getResource("/view/my_account1.fxml"));
            GlobalVariables.baseScene.setRoot(root);
            GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getRightInsert() {
        return rightInsert;
    }

    public void setRightInsert(String rightInsert) {
        this.rightInsert = rightInsert;
    }

    public String getRightView() {
        return rightView;
    }

    public void setRightView(String rightView) {
        this.rightView = rightView;
    }

    public String getRightChange() {
        return rightChange;
    }

    public void setRightChange(String rightChange) {
        this.rightChange = rightChange;
    }

    public String getRightDelete() {
        return rightDelete;
    }

    public void setRightDelete(String rightDelete) {
        this.rightDelete = rightDelete;
    }

    public String getRightStock() {
        return rightStock;
    }

    public void setRightStock(String rightStock) {
        this.rightStock = rightStock;
    }

    public String getRightCash() {
        return rightCash;
    }

    public void setRightCash(String rightCash) {
        this.rightCash = rightCash;
    }

    public String getRightSettings() {
        return rightSettings;
    }

    public void setRightSettings(String rightSettings) {
        this.rightSettings = rightSettings;
    }

    public String getMobileApp() {
        return mobileApp;
    }

    public void setMobileApp(String mobileApp) {
        this.mobileApp = mobileApp;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    private Node createErrorIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.EDIT);
        graphic.setFontSize(20.0);
        return graphic;
    }

    private Node createDeleteIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.TRASH);
        graphic.setFontSize(20.0);
        return graphic;
    }

    public ArrayList<ArrayList<String>> getUsersAccountInfo(Statement stmt, Connection con)
    {
        ArrayList<ArrayList<String>> userData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT `user_accounts`.`user_id`, `user_accounts`.`user_name`, `user_info`.`user_type`, `user_rights`.`create_record`, `user_rights`.`read_record`, `user_rights`.`edit_record`, `user_rights`.`delete_record`, `user_rights`.`manage_stock`, `user_rights`.`manage_cash`, `user_rights`.`manage_settings`, `user_rights`.`mobile_app`, `user_info`.`user_status` FROM `user_accounts` INNER JOIN `user_info` ON `user_info`.`user_table_id` = `user_accounts`.`user_id` LEFT OUTER JOIN `user_rights` ON `user_accounts`.`user_id` = `user_rights`.`user_id` WHERE `user_accounts`.`user_id` != '"+GlobalVariables.userId+"' AND `user_info`.`user_status` != 'Deleted'");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1) == null?"N/A":rs.getString(1)); // User Id
                temp.add(rs.getString(2) == null?"N/A":rs.getString(2)); // User Name
                temp.add(rs.getString(3) == null?"N/A":rs.getString(3)); // User Type
                temp.add(rs.getString(4) == null?"N/A":rs.getString(4)); // Create Record
                temp.add(rs.getString(5) == null?"N/A":rs.getString(5)); // Read Record
                temp.add(rs.getString(6) == null?"N/A":rs.getString(6)); // Edit Record
                temp.add(rs.getString(7) == null?"N/A":rs.getString(7)); // Delete Record
                temp.add(rs.getString(8) == null?"N/A":rs.getString(8)); // Stock
                temp.add(rs.getString(9) == null?"N/A":rs.getString(9)); // Cash
                temp.add(rs.getString(10) == null?"N/A":rs.getString(10)); // Settings
                temp.add(rs.getString(11) == null?"N/A":rs.getString(11)); // Mobile App
                temp.add(rs.getString(12) == null?"N/A":rs.getString(12)); // Status
                userData.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userData;
    }

    public ArrayList<ArrayList<String>> getUsersAccountInfoSearch(Statement stmt, Connection con, String userId, String userName, String userType, String userStatus)
    {
        ArrayList<ArrayList<String>> userData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        String searchQuery;
        int multipleSearch = 0;
        searchQuery = "SELECT `user_accounts`.`user_id`, `user_accounts`.`user_name`, `user_info`.`user_type`, `user_rights`.`create_record`, `user_rights`.`read_record`, `user_rights`.`edit_record`, `user_rights`.`delete_record`, `user_rights`.`manage_stock`, `user_rights`.`manage_cash`, `user_rights`.`manage_settings`, `user_rights`.`mobile_app`, `user_info`.`user_status` FROM `user_accounts` INNER JOIN `user_info` ON `user_info`.`user_table_id` = `user_accounts`.`user_id` LEFT OUTER JOIN `user_rights` ON `user_accounts`.`user_id` = `user_rights`.`user_id` WHERE `user_accounts`.`user_id` != '"+GlobalVariables.userId+"' AND ";
        if(!userId.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`user_accounts`.`user_id` = '"+userId+"'";
            multipleSearch++;
        }
        if(!userName.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`user_accounts`.`user_name` = '"+userName+"'";
            multipleSearch++;
        }
        if(!userType.equals("All"))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`user_info`.`user_type` = '"+userType+"'";
            multipleSearch++;
        }
        if(!userStatus.equals("All"))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`user_info`.`user_status` = '"+userStatus+"'";
            multipleSearch++;
        }
        if(multipleSearch >= 1)
        {
            searchQuery += " AND ";
        }
        searchQuery += "`user_info`.`user_status` != 'Deleted'";
        try {
            rs = stmt.executeQuery(searchQuery);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1) == null?"N/A":rs.getString(1)); // User Id
                temp.add(rs.getString(2) == null?"N/A":rs.getString(2)); // User Name
                temp.add(rs.getString(3) == null?"N/A":rs.getString(3)); // User Type
                temp.add(rs.getString(4) == null?"N/A":rs.getString(4)); // Create Record
                temp.add(rs.getString(5) == null?"N/A":rs.getString(5)); // Read Record
                temp.add(rs.getString(6) == null?"N/A":rs.getString(6)); // Edit Record
                temp.add(rs.getString(7) == null?"N/A":rs.getString(7)); // Delete Record
                temp.add(rs.getString(8) == null?"N/A":rs.getString(8)); // Stock
                temp.add(rs.getString(9) == null?"N/A":rs.getString(9)); // Cash
                temp.add(rs.getString(10) == null?"N/A":rs.getString(10)); // Settings
                temp.add(rs.getString(11) == null?"N/A":rs.getString(11)); // Mobile App
                temp.add(rs.getString(12) == null?"N/A":rs.getString(12)); // Status
                userData.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userData;
    }

    public ArrayList<String> getUserTypes(Statement stmt, Connection con)
    {
        ArrayList<String> userTypesData = new ArrayList<String>();
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT DISTINCT(`user_type`) FROM `user_info` WHERE `user_type` != \"\" AND `user_status` != 'Deleted'");
            while (rs.next())
            {
                userTypesData.add(rs.getString(1)); // User Id
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userTypesData;
    }

    public void insertNewServerUsers(ArrayList<ArrayList<String>> newUsersAccountData)
    {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        String status = "Pending";
        boolean check = false;
        ArrayList<String> userName = new ArrayList<>();
        ArrayList<String> userId = new ArrayList<>();
        ArrayList<String> regDate = new ArrayList<>();
        ArrayList<String> serverUserAccountId = new ArrayList<>();
        ArrayList<String> userAccountIds = new ArrayList<>();
        int num = 0;
        try {
            String insertQuery = "INSERT INTO `user_accounts`(`user_name`, `user_id`, `user_password`, `user_rights`, `reg_date`, `current_status`, `user_status`, `server_sync`) VALUES ";
            String insertRights = "INSERT INTO `user_rights`(`user_id`, `create_record`, `read_record`, `edit_record`, `delete_record`, `manage_stock`, `manage_cash`, `manage_settings`, `mobile_app`, `server_sync`) VALUES ";
            for(int i=0; i<newUsersAccountData.size(); i++)
            {
                serverUserAccountId.add(newUsersAccountData.get(i).get(0));
                if(i == newUsersAccountData.size()-1)
                {
                    insertQuery += "('"+newUsersAccountData.get(i).get(1)+"','"+newUsersAccountData.get(i).get(2)+"','"+newUsersAccountData.get(i).get(3)+"','"+newUsersAccountData.get(i).get(4)+"' ,'"+newUsersAccountData.get(i).get(5)+"' ,'"+newUsersAccountData.get(i).get(6)+"','"+status+"','Done') ";
                    insertRights += "('"+newUsersAccountData.get(i).get(2)+"', '0', '0', '0', '0', '0', '0', '0', '0', '0') ";
                }
                else
                {
                    insertQuery += "('"+newUsersAccountData.get(i).get(1)+"','"+newUsersAccountData.get(i).get(2)+"','"+newUsersAccountData.get(i).get(3)+"','"+newUsersAccountData.get(i).get(4)+"' ,'"+newUsersAccountData.get(i).get(5)+"' ,'"+newUsersAccountData.get(i).get(6)+"','"+status+"','Done'), ";
                    insertRights += "('"+newUsersAccountData.get(i).get(2)+"', '0', '0', '0', '0', '0', '0', '0', '0', '0'), ";
                }
                userName.add(newUsersAccountData.get(i).get(1));
                userId.add(newUsersAccountData.get(i).get(2));
                regDate.add(newUsersAccountData.get(i).get(5));
            }
            objStmt.executeUpdate(insertQuery);
            objStmt.executeUpdate(insertRights);

            ResultSet rs = null;
            for(int i=0; i<userId.size(); i++)
            {
                String query = "SELECT `account_id` FROM `user_accounts` WHERE `user_name` = '"+userName.get(i)+"' AND `user_id` = '"+userId.get(i)+"' AND `reg_date` = '"+regDate.get(i)+"' LIMIT 1";
                rs = objStmt.executeQuery(query);
                while (rs.next())
                {
                    userAccountIds.add(rs.getString(1));
                }
            }
            SendServerThread objSendServerThread = new SendServerThread(serverUserAccountId, userAccountIds, "Set new User Accounts Status");
            objSendServerThread.setDaemon(true);
            objSendServerThread.start();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
