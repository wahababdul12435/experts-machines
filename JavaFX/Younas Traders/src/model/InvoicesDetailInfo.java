package model;

import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;

public class InvoicesDetailInfo {
    private String srNo;
    private String orderId;
    private String dealerId;
    private String dealerName;
    private String finalPrice;
    private String date;
    private JFXButton btnCancel;
    private HBox operationsPane;

    public InvoicesDetailInfo() {
    }

    public InvoicesDetailInfo(String srNo, String orderId, String dealerId, String dealerName, String finalPrice, String date) {
        this.srNo = srNo;
        this.orderId = orderId;
        this.dealerId = dealerId;
        this.dealerName = dealerName;
        this.finalPrice = finalPrice;
        this.date = date;
        this.btnCancel = new JFXButton("Cancel");
        this.btnCancel.setOnAction(event -> markCancel());
        this.operationsPane = new HBox(this.btnCancel);
    }

    public void markCancel()
    {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        try {
            objStmt.executeUpdate("UPDATE `order_info` SET `status`= 'Cancelled', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `order_id` = '"+this.orderId+"'");
            objCon.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/view/invoices_detail.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Stage objstage = (Stage) operationsPane.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getDealerId() {
        return dealerId;
    }

    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(String finalPrice) {
        this.finalPrice = finalPrice;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public JFXButton getBtnCancel() {
        return btnCancel;
    }

    public void setBtnCancel(JFXButton btnCancel) {
        this.btnCancel = btnCancel;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }


    ArrayList<String> getSubareasCodes = new ArrayList<>();
    public ArrayList<ArrayList<String>> getInvoicesDetail(Statement stmt, Connection con,String invo_status,String invoiceFrom,String invoiceTo,ArrayList<String> chkd_subareas,String datefrom,String dateTo) throws ParseException
    {
        String detailsQuery = "SELECT `order_info`.`order_id`, `order_info`.`dealer_id`, `dealer_info`.`dealer_name`, `dealer_info`.`dealer_subarea_id`, `order_info`.`final_price`, `order_info`.`booking_date` FROM `order_info_detailed` INNER JOIN `order_info` ON `order_info_detailed`.`order_id` = `order_info`.`order_id` INNER JOIN `dealer_info` ON  `order_info`.`dealer_id` = `dealer_info`.`dealer_table_id`   WHERE `order_info`.`status` = '"+invo_status+"'";
        if(!invoiceFrom.isEmpty() || !invoiceTo.isEmpty())
        {
            detailsQuery += "and order_info.order_id >='"+invoiceFrom+"' and order_info.order_id <='"+invoiceTo+"'";
        }
        if (!datefrom.isEmpty() || !dateTo.isEmpty())
        {
            detailsQuery += " and str_to_date(`order_info`.`booking_date`, '%d/%b/%Y') >= str_to_date('"+datefrom+"', '%d/%b/%Y') and str_to_date(`order_info`.`booking_date`, '%d/%b/%Y') <= str_to_date('"+dateTo+"', '%d/%b/%Y')";
        }
        if(!chkd_subareas.isEmpty())
        {
            ResultSet rset = null;
            for(int i =0;i<chkd_subareas.size();i++)
            {
                try {
                    rset = stmt.executeQuery("select subarea_id from subarea_info where subarea_status ='Active' and sub_area_name='"+chkd_subareas.get(i)+"' ");

                    while (rset.next() )
                    {
                        getSubareasCodes.add(rset.getString(1));
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }
        ArrayList<ArrayList<String>> invoicesDetail = new ArrayList<ArrayList<String>>();
        ResultSet rs = null;
        ArrayList<String> temp;
        try {
            rs = stmt.executeQuery("SELECT `order_info`.`order_id`, `order_info`.`dealer_id`, `dealer_info`.`dealer_name`, `order_info_detailed`.`product_table_id`, `product_info`.`product_name`, `order_info_detailed`.`quantity`, `order_info_detailed`.`unit`, `order_info_detailed`.`submission_quantity`, `order_info_detailed`.`submission_unit`, `order_info_detailed`.`final_price`, `order_info`.`booking_date` FROM `order_info_detailed` INNER JOIN `order_info` ON `order_info_detailed`.`order_id` = `order_info`.`order_id` LEFT OUTER JOIN `dealer_info` ON `dealer_info`.`dealer_table_id` = `order_info`.`dealer_id` LEFT OUTER JOIN `product_info` ON `product_info`.`product_table_id` = `order_info_detailed`.`product_table_id` WHERE `order_info`.`status` = 'Pending' ORDER BY `order_info`.`order_id`");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                if(chkd_subareas.isEmpty())
                {
                    temp.add(rs.getString(1));
                    temp.add(rs.getString(2));
                    temp.add(rs.getString(3));
                    temp.add(rs.getString(4));
                    temp.add(rs.getString(5));
                    temp.add(rs.getString(6));
                    invoicesDetail.add(temp);
                }

                for(int i=0;i<getSubareasCodes.size();i++)
                {
                    if(( getSubareasCodes.get(i).equals(rs.getString(4)))) {
                        temp.add(rs.getString(1));
                        temp.add(rs.getString(2));
                        temp.add(rs.getString(3));
                        temp.add(rs.getString(4));
                        temp.add(rs.getString(5));
                        temp.add(rs.getString(6));
                        invoicesDetail.add(temp);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return invoicesDetail;
    }
}
