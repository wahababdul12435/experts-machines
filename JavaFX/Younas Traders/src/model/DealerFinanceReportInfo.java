package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import controller.UpdateArea;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.HBox;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DealerFinanceReportInfo {
    private String srNo;
    private String dealerTableId;
    private String dealerId;
    private String dealerName;
    private String areaName;
    private String dealerType;
    private String totalSale;
    private String cashCollection;
    private String cashReturn;
    private String cashDiscount;
    private String cashWaiveOff;
    private String cashPending;

    private HBox operationsPane;
    private JFXButton btnView;
    private JFXButton btnEdit;

    public static String dealerFinanceId = "";

    public DealerFinanceReportInfo() {
    }

    public DealerFinanceReportInfo(String srNo, String dealerTableId, String dealerId, String dealerName, String areaName, String dealerType, String totalSale, String cashCollection, String cashReturn, String cashDiscount, String cashWaiveOff, String cashPending) {
        this.srNo = srNo;
        this.dealerTableId = dealerTableId;
        this.dealerId = dealerId;
        this.dealerName = dealerName;
        this.areaName = areaName;
        this.dealerType = dealerType;
        this.totalSale = totalSale == null || totalSale.equals("") ? "0" : String.format("%,.2f", Float.parseFloat(totalSale));
        this.cashCollection = cashCollection == null || cashCollection.equals("") ? "0" : String.format("%,.2f", Float.parseFloat(cashCollection));
        this.cashReturn = cashReturn == null || cashReturn.equals("") ? "0" : String.format("%,.2f", Float.parseFloat(cashReturn));
        this.cashDiscount = cashDiscount == null || cashDiscount.equals("") ? "0" : String.format("%,.2f", Float.parseFloat(cashDiscount));
        this.cashWaiveOff = cashWaiveOff == null || cashWaiveOff.equals("") ? "0" : String.format("%,.2f", Float.parseFloat(cashWaiveOff));
        this.cashPending = cashPending == null || cashPending.equals("") ? "0" : String.format("%,.2f", Float.parseFloat(cashPending));

        this.btnView = new JFXButton();
        this.btnView.setGraphic(GlobalVariables.createViewIcon());
        this.btnView.getStyleClass().add("btn_icon");
        this.btnEdit = new JFXButton();
        this.btnEdit.setGraphic(GlobalVariables.createEditIcon());
        this.btnEdit.getStyleClass().add("btn_icon");
        this.operationsPane = new HBox(btnView);

        this.btnView.setOnAction((event)->viewDealerDetail());
        this.btnEdit.setOnAction((event)->editClicked());
    }

    public void viewDealerDetail()
    {
        Parent parent = null;
        try {
            dealerFinanceId = dealerTableId;
            parent = FXMLLoader.load(getClass().getResource("/view/dealer_finance_detail_report.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(parent);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }
    
    public void editClicked()
    {
        Parent parent = null;
        try {
            dealerFinanceId = dealerTableId;
            parent = FXMLLoader.load(getClass().getResource("/view/add_dealer_finance.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(parent);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getDealerTableId() {
        return dealerTableId;
    }

    public void setDealerTableId(String dealerTableId) {
        this.dealerTableId = dealerTableId;
    }

    public String getDealerId() {
        return dealerId;
    }

    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getDealerType() {
        return dealerType;
    }

    public void setDealerType(String dealerType) {
        this.dealerType = dealerType;
    }

    public String getTotalSale() {
        return totalSale;
    }

    public void setTotalSale(String totalSale) {
        this.totalSale = totalSale;
    }

    public String getCashCollection() {
        return cashCollection;
    }

    public void setCashCollection(String cashCollection) {
        this.cashCollection = cashCollection;
    }

    public String getCashReturn() {
        return cashReturn;
    }

    public void setCashReturn(String cashReturn) {
        this.cashReturn = cashReturn;
    }

    public String getCashDiscount() {
        return cashDiscount;
    }

    public void setCashDiscount(String cashDiscount) {
        this.cashDiscount = cashDiscount;
    }

    public String getCashWaiveOff() {
        return cashWaiveOff;
    }

    public void setCashWaiveOff(String cashWaiveOff) {
        this.cashWaiveOff = cashWaiveOff;
    }

    public String getCashPending() {
        return cashPending;
    }

    public void setCashPending(String cashPending) {
        this.cashPending = cashPending;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public ArrayList<ArrayList<String>> getDealersFinanceInfo(Statement stmt, Connection con)
    {
        ArrayList<ArrayList<String>> dealerFinanceData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT `dealer_info`.`dealer_table_id`, `dealer_info`.`dealer_id`, `dealer_info`.`dealer_name`, `area_info`.`area_name`, `dealer_info`.`dealer_type`, `dealer_overall_record`.`invoiced_price`, `dealer_overall_record`.`cash_collected`, `dealer_overall_record`.`cash_return`, `dealer_overall_record`.`discount_price`, `dealer_overall_record`.`waived_off_price`, `dealer_overall_record`.`pending_payments` FROM `dealer_info` LEFT OUTER JOIN `area_info` ON `dealer_info`.`dealer_area_id` = `area_info`.`area_table_id` LEFT OUTER JOIN `dealer_overall_record` ON `dealer_info`.`dealer_table_id` = `dealer_overall_record`.`dealer_id` WHERE `dealer_info`.`dealer_status` != 'Deleted'");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Table Id
                temp.add(rs.getString(2)); // Id
                temp.add(rs.getString(3)); // Name
                temp.add(rs.getString(4)); // area
                temp.add(rs.getString(5)); // Dealer Yype
                temp.add(rs.getString(6)); // Sale
                temp.add(rs.getString(7)); // Collected
                temp.add(rs.getString(8)); // Return
                temp.add(rs.getString(9)); // Discount
                temp.add(rs.getString(10)); // Waive Off
                temp.add(rs.getString(11)); // Pending Payment
                dealerFinanceData.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dealerFinanceData;
    }

    public ArrayList<ArrayList<String>> getDealersFinanceSearch(Statement stmt, Connection con, String dealerId, String dealerName, String areaName, String dealerType)
    {
        ArrayList<ArrayList<String>> dealerFinanceData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        String searchQuery;
        int multipleSearch = 0;
        searchQuery = "SELECT `dealer_info`.`dealer_table_id`, `dealer_info`.`dealer_id`, `dealer_info`.`dealer_name`, `area_info`.`area_name`, `dealer_info`.`dealer_type`, `dealer_overall_record`.`invoiced_price`, `dealer_overall_record`.`cash_collected`, `dealer_overall_record`.`cash_return`, `dealer_overall_record`.`discount_price`, `dealer_overall_record`.`waived_off_price`, `dealer_overall_record`.`pending_payments` FROM `dealer_info` LEFT OUTER JOIN `area_info` ON `dealer_info`.`dealer_area_id` = `area_info`.`area_table_id` LEFT OUTER JOIN `dealer_overall_record` ON `dealer_info`.`dealer_table_id` = `dealer_overall_record`.`dealer_id` WHERE ";
        if(!dealerId.equals(""))
        {
            searchQuery += "`dealer_info`.`dealer_id` = '"+dealerId+"'";
            multipleSearch++;
        }
        if(!dealerName.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`dealer_info`.`dealer_name` = '"+dealerName+"'";
            multipleSearch++;
        }
        if(!areaName.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`area_info`.`area_name` LIKE %'"+areaName+"'%";
            multipleSearch++;
        }
        if(!dealerType.equals("All"))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`dealer_info`.`dealer_type` = '"+dealerType+"'";
            multipleSearch++;
        }
        if(multipleSearch >= 1)
        {
            searchQuery += " AND ";
        }
        searchQuery += "`dealer_info`.`dealer_status` != 'Deleted'";
        try {
            rs = stmt.executeQuery(searchQuery);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Table Id
                temp.add(rs.getString(2)); // Id
                temp.add(rs.getString(3)); // Name
                temp.add(rs.getString(4)); // area
                temp.add(rs.getString(5)); // Dealer Yype
                temp.add(rs.getString(6)); // Sale
                temp.add(rs.getString(7)); // Collected
                temp.add(rs.getString(8)); // Return
                temp.add(rs.getString(9)); // Discount
                temp.add(rs.getString(10)); // Waive Off
                temp.add(rs.getString(11)); // Pending Payment
                dealerFinanceData.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dealerFinanceData;
    }

    public ArrayList<ArrayList<String>> getDealersFinanceSummary(Statement stmt, Connection con, String summaryBase)
    {
        ArrayList<ArrayList<String>> dealerFinanceData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        String searchQuery;
        searchQuery = "SELECT `dealer_info`.`dealer_table_id`, `dealer_info`.`dealer_id`, `dealer_info`.`dealer_name`, `area_info`.`area_name`, `dealer_info`.`dealer_type`, `dealer_overall_record`.`invoiced_price`, `dealer_overall_record`.`cash_collected`, `dealer_overall_record`.`cash_return`, `dealer_overall_record`.`discount_price`, `dealer_overall_record`.`waived_off_price`, `dealer_overall_record`.`pending_payments` FROM `dealer_info` LEFT OUTER JOIN `area_info` ON `dealer_info`.`dealer_area_id` = `area_info`.`area_table_id` LEFT OUTER JOIN `dealer_overall_record` ON `dealer_info`.`dealer_table_id` = `dealer_overall_record`.`dealer_id` WHERE `dealer_info`.`dealer_status` != 'Deleted'";
        if(!summaryBase.equals(""))
        {
            searchQuery += " AND ";
            if(summaryBase.equals("Pending"))
                searchQuery += "`dealer_overall_record`.`pending_payments` != '0'";
            else if(summaryBase.equals("Waive Off"))
                searchQuery += "`dealer_overall_record`.`waived_off_price` != '0'";
            else if(summaryBase.equals("Sale"))
                searchQuery += "`dealer_overall_record`.`invoiced_price` != '0'";
            else if(summaryBase.equals("Collection"))
                searchQuery += "`dealer_overall_record`.`cash_collected` != '0'";
            else if(summaryBase.equals("Return"))
                searchQuery += "`dealer_overall_record`.`cash_return` != '0'";
            else if(summaryBase.equals("Discount"))
                searchQuery += "`dealer_overall_record`.`discount_price` != '0'";
        }
        try {
            rs = stmt.executeQuery(searchQuery);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Table Id
                temp.add(rs.getString(2)); // Id
                temp.add(rs.getString(3)); // Name
                temp.add(rs.getString(4)); // area
                temp.add(rs.getString(5)); // Dealer Yype
                temp.add(rs.getString(6)); // Sale
                temp.add(rs.getString(7)); // Collected
                temp.add(rs.getString(8)); // Return
                temp.add(rs.getString(9)); // Discount
                temp.add(rs.getString(10)); // Waive Off
                temp.add(rs.getString(11)); // Pending Payment
                dealerFinanceData.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dealerFinanceData;
    }

    private Node createEditIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.EDIT);
        graphic.setFontSize(20.0);
        return graphic;
    }

    private Node createViewIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.EYE);
        graphic.setFontSize(20.0);
        return graphic;
    }
}
