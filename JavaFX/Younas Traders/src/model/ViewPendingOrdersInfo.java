package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import controller.UpdatePendingOrder;
import controller.UpdateUser;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.*;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class ViewPendingOrdersInfo {
    private String srNo;
    private String orderId;
    private String dealerId;
    private String dealerName;
    private String dealerContact;
    private String dealerAddress;
    private String productId;
    private String orderedItem;
    private String allocatedBatch;
    private String quantity;
    private String unit;
    private String orderPrice;
    private String bonus;
    private String discount;
    private String finalPrice;
    private String totalFinalPrice;
    private String date;
    private String time;
    private String userName;
    private String orderStatus;
    private HBox operationsPane;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    public static StackPane stackPane;
    public static JFXDialog dialog;
    public static Label lblUpdate;

    public ViewPendingOrdersInfo()
    {
        this.srNo = "";
        this.orderId = "";
        this.dealerId = "";
        this.dealerName = "";
        this.dealerContact = "";
        this.dealerAddress = "";
        this.productId = "";
        this.orderedItem = "";
        this.allocatedBatch = "";
        this.quantity = "";
        this.unit = "";
        this.orderPrice = "";
        this.bonus = "";
        this.discount = "";
        this.finalPrice = "";
        this.totalFinalPrice = "";
        this.date = "";
        this.time = "";
        this.userName = "";
        this.orderStatus = "";

        this.btnEdit = new JFXButton("Edit");
        this.btnDelete = new JFXButton("Delete");
        this.operationsPane = new HBox(btnEdit, btnDelete);
    }

    public ViewPendingOrdersInfo(String srNo, String orderId, String dealerId, String dealerName, String dealerContact, String dealerAddress, String productId, String orderedItem, String allocatedBatch, String quantity, String unit, String orderPrice, String bonus, String discount, String finalPrice, String totalFinalPrice, String date, String time, String userName, String orderStatus) {
        this.srNo = srNo;
        this.orderId = orderId;
        this.dealerId = dealerId;
        this.dealerName = dealerName;
        this.dealerContact = dealerContact;
        this.dealerAddress = dealerAddress;
        this.productId = productId;
        this.orderedItem = orderedItem;
        this.allocatedBatch = allocatedBatch;
        this.quantity = quantity;
        this.unit = unit;
        this.orderPrice = orderPrice;
        this.bonus = bonus;
        this.discount = discount;
        this.finalPrice = finalPrice;
        this.totalFinalPrice = totalFinalPrice;
        this.date = date;
        this.time = time;
        this.userName = userName;
        this.orderStatus = orderStatus;

        this.btnEdit = new JFXButton("Edit");
        this.btnDelete = new JFXButton("Delete");
        this.operationsPane = new HBox(btnEdit, btnDelete);

        lblUpdate = new Label("");

        this.btnEdit.setOnAction((action)->editClicked());
        this.btnDelete.setOnAction(event -> {
            try {
                deleteOrder(this.orderId);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void editClicked()
    {
        Parent parent = null;
        try {
            UpdatePendingOrder.orderId = orderId;
            UpdatePendingOrder.dealerId = dealerId;
            UpdatePendingOrder.dealerName = dealerName;
            UpdatePendingOrder.dealerContact = dealerContact;
            UpdatePendingOrder.dealerAddress = dealerAddress;
            UpdatePendingOrder.bookingDate = date;
            UpdatePendingOrder.bookingTime = time;
            UpdatePendingOrder.orderStatus = orderStatus;

            ArrayList<String> orderedIds = new ArrayList<>();
            String[] arr = productId.split("\n");
            for (String itemId : arr) {
                orderedIds.add(itemId);
            }
            UpdatePendingOrder.productsId = orderedIds;

            ArrayList<String> orderedItems = new ArrayList<>();
            arr = orderedItem.split("\n");
            for (String items : arr) {
                orderedItems.add(items);
            }
            UpdatePendingOrder.productsName = orderedItems;

            ArrayList<String> batchNos = new ArrayList<>();
            arr = allocatedBatch.split("\n");
            for (String batch : arr) {
                batchNos.add(batch);
            }
            UpdatePendingOrder.batch = batchNos;

            ArrayList<String> quantities = new ArrayList<>();
            arr = quantity.split("\n");
            for (String quant : arr) {
                quantities.add(quant);
            }
            UpdatePendingOrder.quantity = quantities;

            ArrayList<String> itemsUnit = new ArrayList<>();
            arr = unit.split("\n");
            for (String uni : arr) {
                itemsUnit.add(uni);
            }
            UpdatePendingOrder.unit = itemsUnit;

            ArrayList<String> productsPrice = new ArrayList<>();
            arr = orderPrice.split("\n");
            for (String itemPrice : arr) {
                productsPrice.add(itemPrice);
            }
            UpdatePendingOrder.productsPrice = productsPrice;

            ArrayList<String> productsBonus = new ArrayList<>();
            arr = bonus.split("\n");
            for (String uni : arr) {
                productsBonus.add(uni);
            }
            UpdatePendingOrder.productsBonus = productsBonus;

            ArrayList<String> productsDiscount = new ArrayList<>();
            arr = discount.split("\n");
            for (String disc : arr) {
                productsDiscount.add(disc);
            }
            UpdatePendingOrder.productsDiscount = productsDiscount;

            ArrayList<String> productFinalPrice = new ArrayList<>();
            arr = finalPrice.split("\n");
            for (String finPrice : arr) {
                productFinalPrice.add(finPrice);
            }
            UpdatePendingOrder.productsFinalPrice = productFinalPrice;

            parent = FXMLLoader.load(getClass().getResource("/view/update_pending_order.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setStyle("-fx-padding: -20 -10 -20 -10;");
        dialogLayout.setHeading(lblUpdate);
        dialogLayout.setBody(parent);
        dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        stackPane.setVisible(true);
        dialog.show();
        dialog.setOverlayClose(false);
    }

    public void deleteOrder(String salesmanId) throws IOException {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        try {
            PreparedStatement updateEXP = objCon.prepareStatement("DELETE FROM `salesman_info` WHERE `salesman_id` = '"+salesmanId+"'");
            updateEXP.executeUpdate();
            objCon.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        Parent root = FXMLLoader.load(getClass().getResource("/view/view_pending_orders.fxml"));
        Stage objstage = (Stage) operationsPane.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getDealerContact() {
        return dealerContact;
    }

    public void setDealerContact(String dealerContact) {
        this.dealerContact = dealerContact;
    }

    public String getDealerAddress() {
        return dealerAddress;
    }

    public void setDealerAddress(String dealerAddress) {
        this.dealerAddress = dealerAddress;
    }

    public String getOrderedItem() {
        return orderedItem;
    }

    public void setOrderedItem(String orderedItem) {
        this.orderedItem = orderedItem;
    }

    public String getAllocatedBatch() {
        return allocatedBatch;
    }

    public void setAllocatedBatch(String allocatedBatch) {
        this.allocatedBatch = allocatedBatch;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getDealerId() {
        return dealerId;
    }

    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getOrderPrice() {
        return orderPrice;
    }

    public void setOrderPrice(String orderPrice) {
        this.orderPrice = orderPrice;
    }

    public String getBonus() {
        return bonus;
    }

    public void setBonus(String bonus) {
        this.bonus = bonus;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(String finalPrice) {
        this.finalPrice = finalPrice;
    }

    public String getTotalFinalPrice() {
        return totalFinalPrice;
    }

    public void setTotalFinalPrice(String totalFinalPrice) {
        this.totalFinalPrice = totalFinalPrice;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public ArrayList<ArrayList<String>> getPendingOrdersInfo(Statement stmt, Connection con)
    {
        ArrayList<ArrayList<String>> pendingOrderData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp = new ArrayList<>();
        ResultSet rs = null;
        String preOrderId = "";
        String productId = "";
        String orderedItems  = "";
        String allocatedBatch  = "";
        String quantity  = "";
        String unit  = "";
        String orderPrice  = "";
        String bonus  = "";
        String discount  = "";
        String finalPrice  = "";
        float totalFinalPrice = 0.0f;
        DecimalFormat df = new DecimalFormat("0.00");
        boolean iter = false;
        try {
            rs = stmt.executeQuery("SELECT `order_info`.`order_id`, `order_info`.`dealer_id`, `dealer_info`.`dealer_name`, `dealer_info`.`dealer_phone`, `dealer_info`.`dealer_address`, `order_info_detailed`.`product_table_id`, `product_info`.`product_name`, `order_info_detailed`.`batch_number`, `order_info_detailed`.`quantity`, `order_info_detailed`.`unit`, `order_info_detailed`.`order_price`, `order_info_detailed`.`bonus_quant`, `order_info_detailed`.`discount`, `order_info_detailed`.`final_price`, `order_info`.`booking_date`, `order_info`.`booking_time`, `user_accounts`.`user_name`, `order_info`.`status` FROM `order_info` INNER JOIN `dealer_info` ON `dealer_info`.`dealer_table_id` = `order_info`.`dealer_id` INNER JOIN `order_info_detailed` ON `order_info_detailed`.`order_id` = `order_info`.`order_id` INNER JOIN `product_info` ON `product_info`.`product_table_id` = `order_info_detailed`.`product_table_id` INNER JOIN `user_accounts` ON `user_accounts`.`user_id` = `order_info`.`booking_user_id` ORDER BY `order_info`.`order_id`");
            while (rs.next())
            {
                if(preOrderId.equals(rs.getString(1)))
                {
                    productId += "\n"+rs.getString(6);
                    orderedItems += "\n"+rs.getString(7);
                    allocatedBatch += "\n"+rs.getString(8);
                    quantity += "\n"+rs.getString(9);
                    unit += "\n"+rs.getString(10);
                    orderPrice += "\n"+rs.getString(11);
                    bonus += "\n"+rs.getString(12);
                    discount += "\n"+rs.getString(13);
                    finalPrice += "\n"+rs.getString(14);
                    totalFinalPrice = totalFinalPrice + Float.parseFloat(rs.getString(14));
                }
                else
                {
                    if(iter)
                    {
                        temp.set(5, productId);
                        temp.set(6, orderedItems);
                        temp.set(7, allocatedBatch);
                        temp.set(8, quantity);
                        temp.set(9, unit);
                        temp.set(10, orderPrice);
                        temp.set(11, bonus);
                        temp.set(12, discount);
                        temp.set(13, finalPrice);
                        temp.set(14, df.format(totalFinalPrice));
                        pendingOrderData.add(temp);
                        totalFinalPrice = 0.0f;
                        productId = rs.getString(6);
                        orderedItems = rs.getString(7);
                        allocatedBatch = rs.getString(8);
                        quantity = rs.getString(9);
                        unit = rs.getString(10);
                        orderPrice = rs.getString(11);
                        bonus = rs.getString(12);
                        discount = rs.getString(13);
                        finalPrice = rs.getString(14);
                        totalFinalPrice = totalFinalPrice + Float.parseFloat(rs.getString(14));
                        temp = new ArrayList<>();
                        preOrderId = rs.getString(1);
                        temp.add(rs.getString(1)); // Order Id
                        temp.add(rs.getString(2)); // Dealer Id
                        temp.add(rs.getString(3)); // Dealer Name
                        temp.add(rs.getString(4)); // Dealer Contact
                        temp.add(rs.getString(5)); // Dealer Address
                        temp.add(rs.getString(6)); // Product Id
                        temp.add(rs.getString(7)); // Ordered Item
                        temp.add(rs.getString(8)); // Allocated Batch
                        temp.add(rs.getString(9)); // Quantity
                        temp.add(rs.getString(10)); // Unit
                        temp.add(rs.getString(11)); // Order Price
                        temp.add(rs.getString(12)); // Bonus
                        temp.add(rs.getString(13)); // Discount
                        temp.add(rs.getString(14)); // Final Price
                        temp.add(rs.getString(14)); // Total Final Price
                        temp.add(rs.getString(15)); // Date
                        temp.add(rs.getString(16)); // Time
                        temp.add(rs.getString(17)); // User Name
                        temp.add(rs.getString(18)); // Status
                    }
                    else
                    {
                        iter = true;
                        productId = rs.getString(6);
                        orderedItems = rs.getString(7);
                        allocatedBatch = rs.getString(8);
                        quantity = rs.getString(9);
                        unit = rs.getString(10);
                        orderPrice = rs.getString(11);
                        bonus = rs.getString(12);
                        discount = rs.getString(13);
                        finalPrice = rs.getString(14);
                        totalFinalPrice = totalFinalPrice + Float.parseFloat(rs.getString(14));
                        temp = new ArrayList<>();
                        preOrderId = rs.getString(1);
                        temp.add(rs.getString(1)); // Order Id
                        temp.add(rs.getString(2)); // Dealer Id
                        temp.add(rs.getString(3)); // Dealer Name
                        temp.add(rs.getString(4)); // Dealer Contact
                        temp.add(rs.getString(5)); // Dealer Address
                        temp.add(rs.getString(6)); // Product Id
                        temp.add(rs.getString(7)); // Ordered Item
                        temp.add(rs.getString(8)); // Allocated Batch
                        temp.add(rs.getString(9)); // Quantity
                        temp.add(rs.getString(10)); // Unit
                        temp.add(rs.getString(11)); // Order Price
                        temp.add(rs.getString(12)); // Bonus
                        temp.add(rs.getString(13)); // Discount
                        temp.add(rs.getString(14)); // Final Price
                        temp.add(rs.getString(14)); // Total Final Price
                        temp.add(rs.getString(15)); // Date
                        temp.add(rs.getString(16)); // Time
                        temp.add(rs.getString(17)); // User Name
                        temp.add(rs.getString(18)); // Status
                    }
                }
            }
            if(iter)
            {
                temp.set(5, productId);
                temp.set(6, orderedItems);
                temp.set(7, allocatedBatch);
                temp.set(8, quantity);
                temp.set(9, unit);
                temp.set(10, orderPrice);
                temp.set(11, bonus);
                temp.set(12, discount);
                temp.set(13, finalPrice);
                temp.set(14, df.format(totalFinalPrice));
                pendingOrderData.add(temp);
                totalFinalPrice = 0.0f;
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return pendingOrderData;
    }
}
