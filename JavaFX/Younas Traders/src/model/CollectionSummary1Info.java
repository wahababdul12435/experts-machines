package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import controller.CashCollection;
import controller.CollectionSummary;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class CollectionSummary1Info {
    private String srNo;
    private String dealerTableId;
    private String dealerId;
    private String orderId;
    private String dealerName;
    private String dealerAddress;
    private String dealerContact;
    private String oldPayment;
    private String newPayment;
    private String totalPayment;

    private HBox operationsPane;
    private JFXButton btnCollectCash;

    public static StackPane stackPane;
    public static JFXDialog dialog;
    public static Label lblUpdate;

    public CollectionSummary1Info() {
        this.srNo = "";
        this.dealerTableId = "";
        this.dealerId = "";
        this.orderId = "";
        this.dealerName = "";
        this.dealerAddress = "";
        this.dealerContact = "";
        this.oldPayment = "";
        this.newPayment = "";
        this.totalPayment = "";
    }

    public CollectionSummary1Info(String srNo, String dealerTableId, String dealerId, String orderId, String dealerName, String dealerAddress, String dealerContact, String oldPayment, String newPayment, String totalPayment) {
        this.srNo = srNo;
        this.dealerTableId = dealerTableId;
        this.dealerId = dealerId;
        this.orderId = orderId;
        this.dealerName = dealerName;
        this.dealerAddress = dealerAddress;
        this.dealerContact = dealerContact;
        this.oldPayment = oldPayment;
        this.newPayment = newPayment;
        this.totalPayment = totalPayment;

        this.btnCollectCash = new JFXButton();
        this.btnCollectCash.setOnAction((action)->openCashCollection());
        this.btnCollectCash.setGraphic(GlobalVariables.createViewIcon());
        this.btnCollectCash.getStyleClass().add("btn_icon");
        this.operationsPane = new HBox(btnCollectCash);
    }

    public void openCashCollection()
    {
        CashCollection.dealerId = dealerId;
        CashCollection.invoicesNumber = orderId;

        Parent parent = null;
        try {

            parent = FXMLLoader.load(getClass().getResource("/view/cash_collection.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setStyle("-fx-padding: -20 -10 -20 -10;");
        dialogLayout.setBody(parent);
        dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        stackPane.setVisible(true);
        dialog.show();
        dialog.setOverlayClose(false);

    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getDealerTableId() {
        return dealerTableId;
    }

    public void setDealerTableId(String dealerTableId) {
        this.dealerTableId = dealerTableId;
    }

    public String getDealerId() {
        return dealerId;
    }

    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getDealerAddress() {
        return dealerAddress;
    }

    public void setDealerAddress(String dealerAddress) {
        this.dealerAddress = dealerAddress;
    }

    public String getDealerContact() {
        return dealerContact;
    }

    public void setDealerContact(String dealerContact) {
        this.dealerContact = dealerContact;
    }

    public String getOldPayment() {
        return oldPayment;
    }

    public void setOldPayment(String oldPayment) {
        this.oldPayment = oldPayment;
    }

    public String getNewPayment() {
        return newPayment;
    }

    public void setNewPayment(String newPayment) {
        this.newPayment = newPayment;
    }

    public String getTotalPayment() {
        return totalPayment;
    }

    public void setTotalPayment(String totalPayment) {
        this.totalPayment = totalPayment;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnCollectCash() {
        return btnCollectCash;
    }

    public void setBtnCollectCash(JFXButton btnCollectCash) {
        this.btnCollectCash = btnCollectCash;
    }

    public ArrayList<ArrayList<String>> getCollectionSummary(Statement stmt, Connection con)
    {
        ArrayList<ArrayList<String>> collectionSummaryData = new ArrayList<ArrayList<String>>();
        ArrayList<String> dealersOldPayments = new ArrayList<>();
        ArrayList<String> temp = null;
        ResultSet rs = null;
        String preId = "";
        try {
            rs = stmt.executeQuery("SELECT `dealer_info`.`dealer_table_id`, `dealer_info`.`dealer_id`, `order_info`.`order_id`, `dealer_info`.`dealer_name`, `dealer_info`.`dealer_address`, `dealer_info`.`dealer_phone`, `order_info`.`final_price` from `dealer_info` INNER JOIN `order_info` ON `dealer_info`.`dealer_table_id` = `order_info`.`dealer_id` WHERE `order_info`.`status` = 'Pending' ORDER BY `dealer_info`.`dealer_table_id`");
            while (rs.next())
            {
                if(preId.equals(rs.getString(1)))
                {
                    preId = rs.getString(1);
                    collectionSummaryData.get(collectionSummaryData.size()-1).set(2, collectionSummaryData.get(collectionSummaryData.size()-1).get(2)+"\n"+rs.getString(3));
                    float newPrice = Float.parseFloat(collectionSummaryData.get(collectionSummaryData.size()-1).get(7));
                    newPrice = newPrice + Float.parseFloat(rs.getString(7));
                    collectionSummaryData.get(collectionSummaryData.size()-1).set(7, String.valueOf(GlobalVariables.roundFloat(newPrice, 2)));
                    collectionSummaryData.get(collectionSummaryData.size()-1).set(8, String.valueOf(GlobalVariables.roundFloat(newPrice, 2)));
                }
                else
                {
                    preId = rs.getString(1);
                    temp = new ArrayList<String>();
                    temp.add(rs.getString(1)); // Dealer Table Id
                    temp.add(rs.getString(2)); // Dealer Id
                    temp.add(rs.getString(3)); // Order Id
                    temp.add(rs.getString(4)); // Dealer Name
                    temp.add(rs.getString(5)); // Dealer Address
                    temp.add(rs.getString(6)); // Dealer Phone
                    temp.add("0"); // 6  Old
                    temp.add(rs.getString(7)); // 7 New
                    temp.add(rs.getString(7)); // 8  Total
                    collectionSummaryData.add(temp);
                }
            }
            rs = stmt.executeQuery("SELECT `dealer_payments`.`dealer_id`, `dealer_payments`.`pending_payments` FROM `dealer_payments` WHERE `dealer_payments`.`payment_id` IN (SELECT MAX(`dealer_payments`.`payment_id`) FROM `dealer_payments` INNER JOIN `order_info` ON `order_info`.`dealer_id` = `dealer_payments`.`dealer_id` WHERE `order_info`.`status` = 'Pending' GROUP BY `dealer_payments`.`dealer_id`)");
            while (rs.next())
            {
//                int oldPaymentIndex = collectionSummaryData.get(0).indexOf(rs.getString(1));
                int oldPaymentIndex = findIndex(rs.getString(1), collectionSummaryData);
                if(oldPaymentIndex >= 0)
                {
                    collectionSummaryData.get(oldPaymentIndex).set(6, rs.getString(2));
                    float oldPrice = Float.parseFloat(rs.getString(2));
                    float totalPrice = Float.parseFloat(collectionSummaryData.get(oldPaymentIndex).get(8)) + oldPrice;
                    collectionSummaryData.get(oldPaymentIndex).set(8, String.valueOf(GlobalVariables.roundFloat(totalPrice, 2)));
                }
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return collectionSummaryData;
    }

    public ArrayList<ArrayList<String>> getCollectionSummarySearch(Statement stmt, Connection con, ArrayList<String> areaIds, ArrayList<String> dealerIds, String fromTotalPayment, String toTotalPayment)
    {
        ArrayList<ArrayList<String>> collectionSummaryData = new ArrayList<ArrayList<String>>();
        ArrayList<String> dealersOldPayments = new ArrayList<>();
        ArrayList<String> temp = null;
        ResultSet rs = null;
        String preId = "";
        String query = "SELECT `dealer_info`.`dealer_table_id`, `dealer_info`.`dealer_id`, `order_info`.`order_id`, `dealer_info`.`dealer_name`, `dealer_info`.`dealer_address`, `dealer_info`.`dealer_phone`, `order_info`.`final_price` from `dealer_info` INNER JOIN `order_info` ON `dealer_info`.`dealer_table_id` = `order_info`.`dealer_id` WHERE `order_info`.`status` = 'Pending'";

        if(areaIds.size() > 0)
        {
            if(!areaIds.get(0).equals("All"))
            {
                query += " AND (";
                boolean firstIter = true;
                for(int i=0; i<areaIds.size(); i++)
                {
                    if(firstIter)
                    {
                        firstIter = false;
                    }
                    else
                    {
                        query += " OR ";
                    }
                    query += " `dealer_info`.`dealer_area_id` = '"+areaIds.get(i)+"'";
                }
                query += ")";
            }
        }
        else
        {
            query += " AND `dealer_info`.`dealer_area_id` = '0'";
        }

        if(dealerIds.size() > 0)
        {
            if(!dealerIds.get(0).equals("All"))
            {
                boolean firstIter = true;
                query += " AND (";
                for(int i=0; i<dealerIds.size(); i++)
                {
                    if(firstIter)
                    {
                        firstIter = false;
                    }
                    else
                    {
                        query += " OR";
                    }
                    if(dealerIds.get(i).equals("All Doctor"))
                    {
                        query += " `dealer_info`.`dealer_type` = 'Doctor'";
                    }
                    else if(dealerIds.get(i).equals("All Retailer"))
                    {
                        query += " `dealer_info`.`dealer_type` = 'Retailer'";
                    }
                    else
                    {
                        query += " `order_info`.`dealer_id` = '"+dealerIds.get(i)+"'";
                    }
                }
                query += ")";
            }
        }
        else
        {
            query += " AND `order_info`.`dealer_id` = '0'";
        }
        query += " ORDER BY `dealer_info`.`dealer_table_id`";
        try {
            rs = stmt.executeQuery(query);
            while (rs.next())
            {
                if(preId.equals(rs.getString(1)))
                {
                    preId = rs.getString(1);
                    collectionSummaryData.get(collectionSummaryData.size()-1).set(2, collectionSummaryData.get(collectionSummaryData.size()-1).get(2)+"\n"+rs.getString(3));
                    float newPrice = Float.parseFloat(collectionSummaryData.get(collectionSummaryData.size()-1).get(7));
                    newPrice = newPrice + Float.parseFloat(rs.getString(7));
                    collectionSummaryData.get(collectionSummaryData.size()-1).set(7, String.valueOf(GlobalVariables.roundFloat(newPrice, 2)));
                    collectionSummaryData.get(collectionSummaryData.size()-1).set(8, String.valueOf(GlobalVariables.roundFloat(newPrice, 2)));
                }
                else
                {
                    preId = rs.getString(1);
                    temp = new ArrayList<String>();
                    temp.add(rs.getString(1)); // Dealer Table Id
                    temp.add(rs.getString(2)); // Dealer Id
                    temp.add(rs.getString(3)); // Order Id
                    temp.add(rs.getString(4)); // Dealer Name
                    temp.add(rs.getString(5)); // Dealer Address
                    temp.add(rs.getString(6)); // Dealer Phone
                    temp.add("0"); // 6  Old
                    temp.add(rs.getString(7)); // 7 New
                    temp.add(rs.getString(7)); // 8  Total
                    collectionSummaryData.add(temp);
                }
            }
            rs = stmt.executeQuery("SELECT `dealer_payments`.`dealer_id`, `dealer_payments`.`pending_payments` FROM `dealer_payments` WHERE `dealer_payments`.`payment_id` IN (SELECT MAX(`dealer_payments`.`payment_id`) FROM `dealer_payments` INNER JOIN `order_info` ON `order_info`.`dealer_id` = `dealer_payments`.`dealer_id` WHERE `order_info`.`status` = 'Pending' GROUP BY `dealer_payments`.`dealer_id`)");
            while (rs.next())
            {
//                int oldPaymentIndex = collectionSummaryData.get(0).indexOf(rs.getString(1));
                int oldPaymentIndex = findIndex(rs.getString(1), collectionSummaryData);
                if(oldPaymentIndex >= 0)
                {
                    collectionSummaryData.get(oldPaymentIndex).set(6, rs.getString(2));
                    float oldPrice = Float.parseFloat(rs.getString(2));
                    float totalPrice = Float.parseFloat(collectionSummaryData.get(oldPaymentIndex).get(8)) + oldPrice;
                    collectionSummaryData.get(oldPaymentIndex).set(8, String.valueOf(GlobalVariables.roundFloat(totalPrice, 2)));
                }
            }
            if(!fromTotalPayment.equals("") || !toTotalPayment.equals(""))
            {
                boolean removed = false;
                for(int i=0; i<collectionSummaryData.size(); i++)
                {
                    if(!fromTotalPayment.equals(""))
                    {
                        if(Float.parseFloat(collectionSummaryData.get(i).get(8)) < Float.parseFloat(fromTotalPayment))
                        {
                            collectionSummaryData.remove(i);
                            i--;
                            removed = true;
                        }
                    }
                    if(!removed && !toTotalPayment.equals(""))
                    {
                        if(Float.parseFloat(collectionSummaryData.get(i).get(8)) > Float.parseFloat(toTotalPayment))
                        {
                            collectionSummaryData.remove(i);
                            i--;
                        }
                    }
                    removed = false;
                }
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return collectionSummaryData;
    }

    public ArrayList<ArrayList<String>> getAreasDetail(Statement stmt, Connection con)
    {
        ArrayList<ArrayList<String>> areasData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        String query = "SELECT `area_info`.`area_table_id`, `area_info`.`area_name`, `city_info`.`city_name` FROM `area_info` LEFT OUTER JOIN `city_info` ON `city_info`.`city_table_id` = `area_info`.`city_table_id` WHERE `area_info`.`area_status` != 'Deleted' ORDER BY `area_info`.`area_name`";
        try {
            rs = stmt.executeQuery(query);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Table Id
                temp.add(rs.getString(2)+" - ("+(rs.getString(3) == null ? "N/A" : rs.getString(3))+")"); // (Area + City) Name
                areasData.add(temp);
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return areasData;
    }

    public ArrayList<ArrayList<String>> getDealersDetail(Statement stmt, Connection con)
    {
        ArrayList<ArrayList<String>> dealersData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        String query = "SELECT `dealer_info`.`dealer_table_id`, `dealer_info`.`dealer_name`, `dealer_info`.`dealer_address`, `dealer_info`.`dealer_type` FROM `dealer_info` WHERE `dealer_info`.`dealer_status` != 'Deleted' ORDER BY `dealer_info`.`dealer_name`";
        try {
            rs = stmt.executeQuery(query);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Table Id
                temp.add(rs.getString(2)+" - ("+((rs.getString(3) == null || rs.getString(3).equals(""))? "N/A" : rs.getString(3))+")"); // (Name + Loc)
                temp.add(rs.getString(4)); // Type
                dealersData.add(temp);
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dealersData;
    }

    public int findIndex(String val, ArrayList<ArrayList<String>> arr)
    {
        for (int i = 0 ; i < arr.size(); i++)
            if (arr.get(i).get(0).equals(val))
            {
                return i;
            }
        return -1;
    }
}
