package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import controller.CashCollection;
import controller.UpdateArea;
import controller.UpdateCity;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import java.awt.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;

public class CollectionSummaryInfo {
    private String srNo;
    private String dealerId;
    private String orderId;
    private String dealerName;
    private String dealerAddress;
    private String dealerContact;
    private String oldPayment;
    private String newPayment;
    private String totalPayment;

    private HBox operationsPane;
    private JFXButton btnCollectCash;

    public static StackPane stackPane;
    public static JFXDialog dialog;
    public static Label lblUpdate;


    public CollectionSummaryInfo() {
        srNo = "";
        dealerId = "";
        orderId = "";
        dealerName = "";
        dealerAddress = "";
        dealerContact = "";
        oldPayment = "";
        newPayment = "";
        totalPayment = "";
    }

    public CollectionSummaryInfo(String srNo, String dealerId, String orderId, String dealerName, String dealerAddress, String dealerContact, String oldPayment, String newPayment, String totalPayment) {
        this.srNo = srNo;
        this.dealerId = dealerId;
        this.orderId = orderId;
        this.dealerName = dealerName;
        this.dealerAddress = dealerAddress;
        this.dealerContact = dealerContact;
        this.oldPayment = oldPayment;
        this.newPayment = newPayment;
        this.totalPayment = totalPayment;

        this.btnCollectCash = new JFXButton();
        this.btnCollectCash.setOnAction((action)->openCashCollection());
        this.btnCollectCash.setGraphic(GlobalVariables.createViewIcon());
        this.btnCollectCash.getStyleClass().add("btn_icon");
        this.operationsPane = new HBox(btnCollectCash);
    }

    public void openCashCollection()
    {
        CashCollection.dealerId = dealerId;
        CashCollection.invoicesNumber = orderId;

        Parent parent = null;
        try {

            parent = FXMLLoader.load(getClass().getResource("/view/cash_collection.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setStyle("-fx-padding: -20 -10 -20 -10;");
        dialogLayout.setBody(parent);
        dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        stackPane.setVisible(true);
        dialog.show();
        dialog.setOverlayClose(false);

    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getDealerId() {
        return dealerId;
    }

    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getDealerAddress() {
        return dealerAddress;
    }

    public void setDealerAddress(String dealerAddress) {
        this.dealerAddress = dealerAddress;
    }

    public String getDealerContact() {
        return dealerContact;
    }

    public void setDealerContact(String dealerContact) {
        this.dealerContact = dealerContact;
    }

    public String getOldPayment() {
        return oldPayment;
    }

    public void setOldPayment(String oldPayment) {
        this.oldPayment = oldPayment;
    }

    public String getNewPayment() {
        return newPayment;
    }

    public void setNewPayment(String newPayment) {
        this.newPayment = newPayment;
    }

    public String getTotalPayment() {
        return totalPayment;
    }

    public void setTotalPayment(String totalPayment) {
        this.totalPayment = totalPayment;
    }

    public JFXButton getBtnCollectCash() {
        return btnCollectCash;
    }

    public void setBtnCollectCash(JFXButton btnCollectCash) {
        this.btnCollectCash = btnCollectCash;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public ArrayList<ArrayList<String>> getCollectionSummary(Statement stmt, Connection con)
    {
        ArrayList<ArrayList<String>> collectionSummaryData = new ArrayList<ArrayList<String>>();
        ArrayList<String> dealersOldPayments = new ArrayList<>();
        ArrayList<String> temp = null;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT `dealer_info`.`dealer_id`, `order_info`.`order_id`, `dealer_info`.`dealer_name`, `dealer_info`.`dealer_address`, `dealer_info`.`dealer_phone`, `order_info`.`final_price` from `dealer_info` INNER JOIN `order_info` ON `dealer_info`.`dealer_id` = `order_info`.`dealer_id` WHERE `order_info`.`status` = 'Pending'");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2));
                temp.add(rs.getString(3));
                temp.add(rs.getString(4));
                temp.add(rs.getString(5));
                temp.add("0"); // 5  Old
                temp.add(rs.getString(6)); //6 New
                temp.add(rs.getString(6)); //7  Total
                collectionSummaryData.add(temp);
            }
            rs = stmt.executeQuery("SELECT `dealer_id`, SUM(`final_price`) from `order_info` WHERE `status` != 'Pending'");
            while (rs.next())
            {
//                int oldPaymentIndex = collectionSummaryData.get(0).indexOf(rs.getString(1));
                int oldPaymentIndex = findIndex(rs.getString(1), collectionSummaryData);
                if(oldPaymentIndex >= 0)
                {
                    collectionSummaryData.get(oldPaymentIndex).set(5, rs.getString(2));
                }
            }
            rs = stmt.executeQuery("SELECT `dealer_id`, SUM(`amount`) from `dealer_payments` WHERE `status` = 'Cash'");
            while (rs.next())
            {
                int oldPaymentIndex = findIndex(rs.getString(1), collectionSummaryData);
                if(oldPaymentIndex >= 0)
                {
                    collectionSummaryData.get(oldPaymentIndex).set(5, String.valueOf(Float.parseFloat(collectionSummaryData.get(oldPaymentIndex).get(5)) - Float.parseFloat(rs.getString(2))));
                    collectionSummaryData.get(oldPaymentIndex).set(7, String.valueOf(Float.parseFloat(collectionSummaryData.get(oldPaymentIndex).get(6)) + Float.parseFloat(collectionSummaryData.get(oldPaymentIndex).get(5))));
                }

//                dealersOldPayments.set(oldPaymentIndex, String.valueOf(Float.parseFloat(dealersOldPayments.get(oldPaymentIndex)) - Float.parseFloat(rs.getString(2))));
//                dealersTotalPayments.set(oldPaymentIndex, String.valueOf(Float.parseFloat(dealersTotalPayments.get(oldPaymentIndex))+Float.parseFloat(dealersOldPayments.get(oldPaymentIndex))));
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return collectionSummaryData;
    }
    ArrayList<String> getSubareasCodes = new ArrayList<>();

    public ArrayList<ArrayList<String>> getCollectionSummary_withFilter(Statement stmt, Connection con,String invo_status,String invoiceFrom,String invoiceTo,ArrayList<String> chkd_subareas,String datefrom,String dateTo) throws ParseException
    {
        String collectionQuery = "SELECT `dealer_info`.`dealer_id`, `order_info`.`order_id`, `dealer_info`.`dealer_name`, `dealer_info`.`dealer_address`, `dealer_info`.`dealer_phone`, `order_info`.`final_price` from `dealer_info` INNER JOIN `order_info` ON `dealer_info`.`dealer_id` = `order_info`.`dealer_id` WHERE `order_info`.`status` = '"+invo_status+"'";
        if (!invoiceFrom.isEmpty() || !invoiceTo.isEmpty())
        {
            collectionQuery += "and order_info.order_id >='"+invoiceFrom+"' and order_info.order_id <='"+invoiceTo+"'";
        }
        if (!datefrom.isEmpty() || !dateTo.isEmpty())
        {
            collectionQuery += " and str_to_date(`order_info`.`booking_date`, '%d/%b/%Y') >= str_to_date('"+datefrom+"', '%d/%b/%Y') and str_to_date(`order_info`.`booking_date`, '%d/%b/%Y') <= str_to_date('"+dateTo+"', '%d/%b/%Y')";
        }
        if(!chkd_subareas.isEmpty())
        {
            ResultSet rset = null;
            for(int i =0;i<chkd_subareas.size();i++)
            {
                try {
                    rset = stmt.executeQuery("select subarea_id from subarea_info where subarea_status ='Active' and sub_area_name='"+chkd_subareas.get(i)+"' ");

                    while (rset.next() )
                    {
                        getSubareasCodes.add(rset.getString(1));
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }

        ArrayList<ArrayList<String>> collectionSummaryData = new ArrayList<ArrayList<String>>();
        ArrayList<String> dealersOldPayments = new ArrayList<>();
        ArrayList<String> temp = null;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery(collectionQuery);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2));
                temp.add(rs.getString(3));
                temp.add(rs.getString(4));
                temp.add(rs.getString(5));
                temp.add("0"); // 5  Old
                temp.add(rs.getString(6)); //6 New
                temp.add(rs.getString(6)); //7  Total
                collectionSummaryData.add(temp);
            }
            rs = stmt.executeQuery("SELECT `dealer_id`, SUM(`final_price`) from `order_info` WHERE `status` != 'Pending'");
            while (rs.next())
            {
//                int oldPaymentIndex = collectionSummaryData.get(0).indexOf(rs.getString(1));
                int oldPaymentIndex = findIndex(rs.getString(1), collectionSummaryData);
                if(oldPaymentIndex >= 0)
                {
                    collectionSummaryData.get(oldPaymentIndex).set(5, rs.getString(2));
                }
            }
            rs = stmt.executeQuery("SELECT `dealer_id`, SUM(`amount`) from `dealer_payments` WHERE `status` = 'Cash'");
            while (rs.next())
            {
                int oldPaymentIndex = findIndex(rs.getString(1), collectionSummaryData);
                if(oldPaymentIndex >= 0)
                {
                    collectionSummaryData.get(oldPaymentIndex).set(5, String.valueOf(Float.parseFloat(collectionSummaryData.get(oldPaymentIndex).get(5)) - Float.parseFloat(rs.getString(2))));
                    collectionSummaryData.get(oldPaymentIndex).set(7, String.valueOf(Float.parseFloat(collectionSummaryData.get(oldPaymentIndex).get(6)) + Float.parseFloat(collectionSummaryData.get(oldPaymentIndex).get(5))));
                }

//                dealersOldPayments.set(oldPaymentIndex, String.valueOf(Float.parseFloat(dealersOldPayments.get(oldPaymentIndex)) - Float.parseFloat(rs.getString(2))));
//                dealersTotalPayments.set(oldPaymentIndex, String.valueOf(Float.parseFloat(dealersTotalPayments.get(oldPaymentIndex))+Float.parseFloat(dealersOldPayments.get(oldPaymentIndex))));
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return collectionSummaryData;
    }


    public int findIndex(String val, ArrayList<ArrayList<String>> arr)
    {
        for (int i = 0 ; i < arr.size(); i++)
            if (arr.get(i).get(0).equals(val))
            {
                return i;
            }
        return -1;
    }

    private Node createViewIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.MONEY);
        graphic.setFontSize(20.0);
        return graphic;
    }
}
