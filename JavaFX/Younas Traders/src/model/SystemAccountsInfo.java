package model;

import controller.SystemAccounts;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SystemAccountsInfo {
    private String srNo;
    private String userName;
    private String userId;
    private String permissionAccess;
    private String activeStatus;
    private HBox actionPane;
    private Button btnManage;
    private Button btnApprove;
    private Button btnDecline;
    private static String userIdDetail;

    public SystemAccountsInfo()
    {
        srNo = "";
        userName = "";
        userId = "";
        permissionAccess = "";
        activeStatus = "";
        actionPane = new HBox();
        btnManage = null;
        btnApprove = null;
        btnDecline = null;
    }

    public SystemAccountsInfo(String srNo, String userName, String userId, String permissionAccess, String activeStatus, String btnVal1, String btnVal2)
    {
        this.srNo = srNo;
        this.userName = userName;
        this.userId = userId;
        this.permissionAccess = permissionAccess;
        this.activeStatus = activeStatus;
        btnApprove = new Button(btnVal1);
        btnDecline = new Button(btnVal2);
        this.actionPane = new HBox(btnApprove, btnDecline);
        this.actionPane.setSpacing(10);
        btnApprove.setOnAction(event -> {
            try {
                viewSystemAccountsDetail(this.userId);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        btnDecline.setOnAction(event -> {
            try {
                declineSystemAccount(userId);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public SystemAccountsInfo(String srNo, String userName, String userId, String permissionAccess, String activeStatus, String btnVal)
    {
        this.srNo = srNo;
        this.userName = userName;
        this.userId = userId;
        this.permissionAccess = permissionAccess;
        this.activeStatus = activeStatus;
        this.btnManage = new Button(btnVal);
        btnManage.setOnAction(event -> {
            try {
                viewSystemAccountsDetail(this.userId);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        this.actionPane = new HBox(btnManage);
    }

    public void viewSystemAccountsDetail(String userId) throws IOException {
        userIdDetail = userId;
        Parent root = FXMLLoader.load(getClass().getResource("/view/system_account_detail.fxml"));
        Stage objstage = (Stage) actionPane.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    public void declineSystemAccount(String userId) throws IOException {
        userIdDetail = userId;
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        declineAccount(objStmt, objCon, userId, "Declined");
        Parent root = FXMLLoader.load(getClass().getResource("/view/system_accounts.fxml"));
        Stage objstage = (Stage) actionPane.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPermissionAccess() {
        return permissionAccess;
    }

    public void setPermissionAccess(String permissionAccess) {
        this.permissionAccess = permissionAccess;
    }

    public String getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(String activeStatus) {
        this.activeStatus = activeStatus;
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public HBox getActionPane() {
        return actionPane;
    }

    public void setActionPane(HBox actionPane) {
        this.actionPane = actionPane;
    }

    public Button getBtnApprove() {
        return btnApprove;
    }

    public void setBtnApprove(Button btnApprove) {
        this.btnApprove = btnApprove;
    }

    public Button getBtnDecline() {
        return btnDecline;
    }

    public void setBtnDecline(Button btnDecline) {
        this.btnDecline = btnDecline;
    }

    public Button getBtnManage() {
        return btnManage;
    }

    public void setBtnManage(Button btnManage) {
        this.btnManage = btnManage;
    }

    public static String getUserIdDetail() {
        return userIdDetail;
    }

    public static void setUserIdDetail(String userIdDetail) {
        SystemAccountsInfo.userIdDetail = userIdDetail;
    }

    public ArrayList<ArrayList<String>> getSystemAccounts(Statement stmt, Connection con)
    {
        ArrayList<ArrayList<String>> systemAccounts = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("select * from user_accounts");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2));
                temp.add(rs.getString(3));
                temp.add(rs.getString(4));
                temp.add(rs.getString(5));
                temp.add(rs.getString(6));
                temp.add(rs.getString(7));
                temp.add(rs.getString(8));
                systemAccounts.add(temp);
            }

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return systemAccounts;
    }

    public void saveAccountDetail(Statement stmt, Connection con, String userId, String userStatus)
    {
        ResultSet rs = null;
        try {
            PreparedStatement updateEXP = con.prepareStatement("UPDATE `user_accounts` SET `user_status` = '"+userStatus+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `user_id` = '"+userId+"'");
            int affectedRows = updateEXP.executeUpdate();
            con.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateuserRights(Statement stmt, Connection con, String userId, int edit_record,int delete_record,int order_booking,int create_record,int manage_settings,int read_record,int order_collection)
    {
        try {
            String insertQuery = "UPDATE `user_rights` SET `edit_record` = '"+edit_record+"' , `delete_record` = '"+delete_record+"', `order_booking` = '"+order_booking+"' , `create_record` = '"+create_record+"' , `manage_settings` = '"+manage_settings+"' , `read_record` = '"+read_record+"' , `order_collection` = '"+order_collection+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END where  `user_id` = '"+userId+"' )";
            stmt.executeUpdate(insertQuery);
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void saveuserRights(Statement stmt, Connection con, String userId, int edit_record,int delete_record,int order_booking,int create_record,int manage_settings,int read_record,int order_collection)
    {
        try {
            String insertQuery = "INSERT INTO `user_rights` (`user_id`, `edit_record`, `delete_record`, `order_booking`, `create_record`, `manage_settings`, `read_record`, `order_collection`) VALUES ('"+userId+"','"+edit_record+"','"+delete_record+"','"+order_booking+"','"+create_record+"','"+manage_settings+"','"+read_record+"' ,'"+order_collection+"')";
            stmt.executeUpdate(insertQuery);
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    String[] user_rights_infor = new String[1];
    public String[] getuser_rights_sinfo(Statement stmt3, Connection con3,int userid )
    {
        ResultSet rs3 = null;
        try {
            rs3 = stmt3.executeQuery("select edit_record,delete_record,order_booking,create_record from user_rights where user_id= '"+userid+"' ");
            int i = 0;
            while (rs3.next() && i < 5)
            {

                user_rights_infor[i] = rs3.getString(1) +"-"+rs3.getString(2) +"-"+rs3.getString(3)+"-"+rs3.getString(4);
            }
            con3.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return user_rights_infor;
    }

    String[] userinfor = new String[1];
    public String[] getusersinfo(Statement stmt3, Connection con3,int userid )
    {
        ResultSet rs3 = null;
        try {
            rs3 = stmt3.executeQuery("select user_name,user_power,reg_date,user_status from user_accounts where user_id= '"+userid+"' ");
            int i = 0;
            while (rs3.next() && i < 5)
            {

                userinfor[i] = rs3.getString(1) +"-"+rs3.getString(2) +"-"+rs3.getString(3)+"-"+rs3.getString(4);
            }
            con3.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return userinfor;
    }

    public void declineAccount(Statement stmt, Connection con, String userId, String userStatus)
    {
        ResultSet rs = null;
        try {
            PreparedStatement updateEXP = con.prepareStatement("UPDATE `user_accounts` SET `user_status` = '"+userStatus+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `user_id` = '"+userId+"'");
            int affectedRows = updateEXP.executeUpdate();
            con.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
