package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import controller.AddDealerFinance;
import controller.EnterDealer;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class AddDealerFinanceInfo {
    private String srNo;
    private String dealerId;
    private String dealerName;
    private String dealerContact;
    private String addedDate;
    private String addedDay;
    private String amount;
    private String type;
    private String pending;

    private HBox operationsPane;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    public static ArrayList<String> selectedDealerArr = new ArrayList<>();
    public static ArrayList<String> dealerIdArr = new ArrayList<>();
    public static ArrayList<String> dealerNameArr = new ArrayList<>();
    public static ArrayList<String> dealerContactArr = new ArrayList<>();
    public static ArrayList<String> dealerAddressArr = new ArrayList<>();
    public static ArrayList<String> dealerCnicArr = new ArrayList<>();
    public static ArrayList<String> dealerImageArr = new ArrayList<>();
    public static ArrayList<String> dealerPendingAmountArr = new ArrayList<>();
    public static ArrayList<String> amountArr = new ArrayList<>();
    public static ArrayList<String> typeArr = new ArrayList<>();
    public static ArrayList<String> addedDateArr = new ArrayList<>();
    public static ArrayList<String> addedDayArr = new ArrayList<>();
    public static ArrayList<String> pendingArr = new ArrayList<>();

    public static TableView<AddDealerFinanceInfo> table_adddealercash;

    public static JFXComboBox<String> txtSelectDealer;
    public static JFXTextField txtDealerName;
    public static ImageView txtDealerImage;
    public static JFXTextField txtDealerContact;
    public static JFXTextField txtDealerAddress;
    public static JFXTextField txtDealerCnic;
    public static JFXTextField txtDealerPendingAmount;
    public static JFXTextField txtAmount;
    public static JFXComboBox<String> txtType;
    public static JFXDatePicker txtAddedDate;

    public static Button btnAdd;
    public static Button btnCancel;
    public static Button btnUpdate;

    MysqlCon objMysqlCon = new MysqlCon();
    Statement objStmt = objMysqlCon.stmt;
    Connection objCon = objMysqlCon.con;

    public AddDealerFinanceInfo() {
        srNo = "";
        dealerId = "";
        dealerName = "";
        dealerContact = "";

    }

    public AddDealerFinanceInfo(String srNo, String dealerId, String dealerName, String dealerContact, String addedDate, String addedDay, String amount, String type, String pending) {
        this.srNo = srNo;
        this.dealerId = dealerId;
        this.dealerName = dealerName;
        this.dealerContact = dealerContact;
        this.addedDate = addedDate;
        this.addedDay = addedDay;
        this.amount = amount;
        this.type = type;
        this.pending = pending;

        this.btnEdit = new JFXButton("");
        this.btnDelete = new JFXButton("");
        this.btnEdit.setGraphic(GlobalVariables.createEditIcon());
        this.btnDelete.setGraphic(GlobalVariables.createDeleteIcon());
        this.btnEdit.getStyleClass().add("btn_icon");
        this.btnDelete.getStyleClass().add("btn_icon");
        this.btnEdit.setOnAction((action)->editAddedRecord());
        this.btnDelete.setOnAction((action)->deleteAddedRecord());
        this.operationsPane = new HBox(btnEdit, btnDelete);
    }

    private void deleteAddedRecord()
    {
        selectedDealerArr.remove(Integer.parseInt(srNo)-1);
        dealerIdArr.remove(Integer.parseInt(srNo)-1);
        dealerNameArr.remove(Integer.parseInt(srNo)-1);
        dealerContactArr.remove(Integer.parseInt(srNo)-1);
        dealerAddressArr.remove(Integer.parseInt(srNo)-1);
        dealerCnicArr.remove(Integer.parseInt(srNo)-1);
        dealerImageArr.remove(Integer.parseInt(srNo)-1);
        dealerPendingAmountArr.remove(Integer.parseInt(srNo)-1);
        amountArr.remove(Integer.parseInt(srNo)-1);
        typeArr.remove(Integer.parseInt(srNo)-1);
        addedDateArr.remove(Integer.parseInt(srNo)-1);
        addedDayArr.remove(Integer.parseInt(srNo)-1);
        pendingArr.remove(Integer.parseInt(srNo)-1);
        table_adddealercash.setItems(AddDealerFinance.parseUserList());
    }

    public void editAddedRecord()
    {
        AddDealerFinance.srNo = Integer.parseInt(srNo)-1;
        File file = new File(dealerImageArr.get(Integer.parseInt(srNo)-1));
        boolean exists = file.exists();
        if(exists)
        {
            Image image = new Image(file.toURI().toString());
            txtDealerImage.setImage(image);
        }
        txtDealerName.setText(dealerNameArr.get(Integer.parseInt(srNo)-1));
        txtDealerContact.setText(dealerContactArr.get(Integer.parseInt(srNo)-1));
        txtDealerAddress.setText(dealerAddressArr.get(Integer.parseInt(srNo)-1));
        txtDealerCnic.setText(dealerCnicArr.get(Integer.parseInt(srNo)-1));
        txtDealerPendingAmount.setText(dealerPendingAmountArr.get(Integer.parseInt(srNo)-1));
        txtAmount.setText(amountArr.get(Integer.parseInt(srNo)-1));
        txtType.setValue(typeArr.get(Integer.parseInt(srNo)-1));
        txtAddedDate.setValue(LOCAL_DATE(addedDateArr.get(Integer.parseInt(srNo)-1)));

        btnAdd.setVisible(false);
        btnCancel.setVisible(true);
        btnUpdate.setVisible(true);
    }

    public ArrayList<ArrayList<String>> getAddedCash()
    {
        ArrayList<ArrayList<String>> dealerData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        for(int i=0; i<dealerIdArr.size(); i++)
        {
            temp = new ArrayList<String>();
            temp.add(dealerIdArr.get(i));
            temp.add(dealerNameArr.get(i));
            temp.add(dealerContactArr.get(i));
            temp.add(addedDateArr.get(i));
            temp.add(addedDayArr.get(i));
            temp.add(amountArr.get(i));
            temp.add(typeArr.get(i));
            temp.add(pendingArr.get(i));
            dealerData.add(temp);
        }
        return dealerData;
    }

    public static final LocalDate LOCAL_DATE (String dateString) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MMM/yyyy");
        LocalDate localDate = LocalDate.parse(dateString, formatter);
        return localDate;
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getDealerId() {
        return dealerId;
    }

    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getDealerContact() {
        return dealerContact;
    }

    public void setDealerContact(String dealerContact) {
        this.dealerContact = dealerContact;
    }

    public String getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(String addedDate) {
        this.addedDate = addedDate;
    }

    public String getAddedDay() {
        return addedDay;
    }

    public void setAddedDay(String addedDay) {
        this.addedDay = addedDay;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPending() {
        return pending;
    }

    public void setPending(String pending) {
        this.pending = pending;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public ArrayList<ArrayList<String>> getDealersInfo(Statement stmt, Connection con)
    {
        ArrayList<ArrayList<String>> dealersData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT `dealer_info`.`dealer_table_id`, `dealer_info`.`dealer_name`, `dealer_info`.`dealer_phone`, `dealer_info`.`dealer_address`, `dealer_info`.`dealer_cnic`, `dealer_info`.`dealer_image`, `dealer_overall_record`.`pending_payments` FROM `dealer_info` LEFT OUTER JOIN `dealer_overall_record` ON `dealer_info`.`dealer_table_id` = `dealer_overall_record`.`dealer_id` WHERE `dealer_info`.`dealer_status` != 'Deleted'");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Table id
                temp.add(rs.getString(2)+" ("+rs.getString(4)+" - "+rs.getString(3)+")"); // name
                temp.add(rs.getString(2)); // name
                temp.add(rs.getString(3)); // contact
                temp.add(rs.getString(4)); // address
                temp.add(rs.getString(5)); // Cnic
                if(rs.getString(6) != null)
                {
                    temp.add(rs.getString(6)); // Image
                }
                else
                {
                    temp.add(""); // Image
                }
                temp.add(rs.getString(7)); // Pending Payments
                dealersData.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dealersData;
    }

    public void insertDealerCash(Statement stmt, Connection con) throws SQLException {
        String currentDate = GlobalVariables.getStDate();
        String currentTime = GlobalVariables.getStTime();
        String currentUser = GlobalVariables.getUserId();
        String insertDealerPayment;
        String updateOverAll;
        String orderId = "0";
        String cashStatus = "Cash";
        float pendingPayment = 0;

        for(int i=0; i<dealerIdArr.size(); i++)
        {
            ResultSet rs = null;
            try {
                rs = stmt.executeQuery("SELECT `pending_payments` FROM `dealer_payments` WHERE `dealer_id` = '"+dealerIdArr.get(i)+"' ORDER BY `payment_id` DESC LIMIT 1");
                if(rs.next())
                {
                    pendingPayment = rs.getFloat(1);
                }
                else
                {
                    pendingPayment = 0;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

            if(typeArr.get(i).equals("Collection"))
            {
                pendingPayment = pendingPayment - Float.parseFloat(amountArr.get(i));
                updateOverAll = "UPDATE `dealer_overall_record` SET `cash_collected` = `cash_collected` + '"+amountArr.get(i)+"', `pending_payments` = `pending_payments` - '"+amountArr.get(i)+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `dealer_id` = '"+dealerIdArr.get(i)+"'";
            }
            else if(typeArr.get(i).equals("Return"))
            {
                pendingPayment = pendingPayment + Float.parseFloat(amountArr.get(i));
                updateOverAll = "UPDATE `dealer_overall_record` SET `cash_return` = `cash_return` + '"+amountArr.get(i)+"', `pending_payments` = `pending_payments` + '"+amountArr.get(i)+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `dealer_id` = '"+dealerIdArr.get(i)+"'";
            }
            else
            {
                pendingPayment = pendingPayment - Float.parseFloat(amountArr.get(i));
                updateOverAll = "UPDATE `dealer_overall_record` SET `waived_off_price` = `waived_off_price` + '"+amountArr.get(i)+"', `pending_payments` = `pending_payments` - '"+amountArr.get(i)+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `dealer_id` = '"+dealerIdArr.get(i)+"'";
            }
            insertDealerPayment = "INSERT INTO `dealer_payments`(`dealer_id`, `order_id`, `amount`, `cash_type`, `pending_payments`, `date`, `day`, `user_id`, `status`) VALUES ('"+dealerIdArr.get(i)+"','"+orderId+"','"+amountArr.get(i)+"','"+typeArr.get(i)+"','"+pendingPayment+"','"+addedDateArr.get(i)+"','"+addedDayArr.get(i)+"','"+currentUser+"','"+cashStatus+"')";
            int insertInfo = -1;
            if(stmt.executeUpdate(insertDealerPayment) != -1)
            {
                insertInfo = 1;
            }
            if(stmt.executeUpdate(updateOverAll) != -1)
            {
                insertInfo = 1;
            }
            if(insertInfo == -1)
            {
                String title = "Error";
                String message = "Unable to insert";
                GlobalVariables.showNotification(-1, title, message);
            }
            else
            {
                String title = "Successfull";
                String message = "Record Inserted";
                GlobalVariables.showNotification(1, title, message);
            }
        }
        refreshAllArrays();
    }

    private void refreshAllArrays()
    {
        selectedDealerArr = new ArrayList<>();
        dealerIdArr = new ArrayList<>();
        dealerNameArr = new ArrayList<>();
        dealerContactArr = new ArrayList<>();
        dealerAddressArr = new ArrayList<>();
        dealerCnicArr = new ArrayList<>();
        dealerImageArr = new ArrayList<>();
        dealerPendingAmountArr = new ArrayList<>();
        amountArr = new ArrayList<>();
        typeArr = new ArrayList<>();
        addedDateArr = new ArrayList<>();
        addedDayArr = new ArrayList<>();
        pendingArr = new ArrayList<>();
    }

    private Node createEditIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.EDIT);
        graphic.setFontSize(20.0);
        return graphic;
    }

    private Node createDeleteIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.TRASH);
        graphic.setFontSize(20.0);
        return graphic;
    }
}
