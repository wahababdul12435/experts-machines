package model;

import javafx.application.Platform;

import java.util.ArrayList;

public class SendServerThread extends Thread {
    SendServerData objSendServerData = new SendServerData();
    ArrayList<String> serverIds;
    ArrayList<String> softwareIds;
    String operation;

    public SendServerThread(ArrayList<String> serverIds, String operation)
    {
        this.serverIds = serverIds;
        this.operation = operation;
    }

    public SendServerThread(ArrayList<String> serverIds, ArrayList<String> softwareIds, String operation)
    {
        this.serverIds = serverIds;
        this.softwareIds = softwareIds;
        this.operation = operation;
    }

    public void run()
    {
        boolean check = false;
        if(this.operation.equals("Set mobile gps Status") || this.operation.equals("Set order gps Status"))
        {
            String serverId = "";
            for(int i=0; i<serverIds.size(); i++)
            {
                if(check)
                {
                    serverId = serverId + ",";
                }
                serverId = serverId + serverIds.get(i);
                check = true;
            }
            objSendServerData.setNewDataStatus(serverId, this.operation);
        }
        else
        {
            String serverId = "";
            String softwareId = "";
            for(int i=0; i<serverIds.size(); i++)
            {
                if(check)
                {
                    serverId = serverId + ",";
                    softwareId = softwareId + ",";
                }
                serverId = serverId + serverIds.get(i);
                softwareId = softwareId + softwareIds.get(i);
                check = true;
            }
            objSendServerData.setNewDataStatus(serverId, softwareId, this.operation);
        }
    }
}
