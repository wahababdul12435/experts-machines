package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import controller.CreateSaleReturn;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class CreateSaleReturnInfo {

    private String srNo;
    private String productId;
    private String productName;
    private String batchNo;
    private String returnQuantity;
    private String returnPrice;
    private String returnReason;

    private HBox operationsPane;
    private JFXButton btnEdit;
    private JFXButton btnDelete;
    public static String returnId;
    public static ArrayList<String> preProductIdArr = new ArrayList<>();
    public static ArrayList<String> productIdArr = new ArrayList<>();
    public static ArrayList<String> productNameArr = new ArrayList<>();
    public static ArrayList<String> preBatchNoArr = new ArrayList<>();
    public static ArrayList<String> batchNoArr = new ArrayList<>();
    public static ArrayList<String> preReturnQuantityArr = new ArrayList<>();
    public static ArrayList<String> returnQuantityArr = new ArrayList<>();
    public static ArrayList<String> returnPriceArr = new ArrayList<>();
    //    public static ArrayList<String> unitArr = new ArrayList<>();
    public static ArrayList<String> returnReasonArr = new ArrayList<>();

    public static TableView<CreateSaleReturnInfo> table_salereturndetaillog;

    public static JFXTextField txtProductName;
    public static JFXComboBox<String> txtBatchNo;
    public static JFXTextField txtReturnQuantity;
    public static JFXTextField txtReturnPrice;
    //    public static JFXComboBox<String>  txtUnit;
    public static JFXComboBox<String> txtReturnReason;

    public static Button btnAdd;
    public static Button btnCancel;
    public static Button btnUpdate;

    public CreateSaleReturnInfo() {
    }

    public CreateSaleReturnInfo(String srNo, String productName, String batchNo, String returnQuantity, String returnPrice, String returnReason) {
        this.srNo = srNo;
        this.productName = productName;
        this.batchNo = batchNo;
        this.returnQuantity = returnQuantity;
        this.returnPrice = returnPrice;
        this.returnReason = returnReason;

        this.btnEdit = new JFXButton();
        this.btnDelete = new JFXButton();
        this.btnEdit.setGraphic(GlobalVariables.createEditIcon());
        this.btnDelete.setGraphic(GlobalVariables.createDeleteIcon());
        this.btnEdit.getStyleClass().add("btn_icon");
        this.btnDelete.getStyleClass().add("btn_icon");
        this.operationsPane = new HBox(btnEdit, btnDelete);
        this.btnEdit.setOnAction((action)->editAddedRecord());
        this.btnDelete.setOnAction((action)->deleteAddedRecord());
    }

    private void editAddedRecord()
    {
        CreateSaleReturn.srNo = Integer.parseInt(srNo)-1;
        txtProductName.setText(productNameArr.get(Integer.parseInt(srNo)-1));
        txtBatchNo.getItems().clear();
        String productId = CreateSaleReturn.getProductId(CreateSaleReturn.completeProductData, txtProductName.getText());
        ArrayList<String> batchData = CreateSaleReturn.getProductsBatch(CreateSaleReturn.completeBatchData, productId);
        txtBatchNo.getItems().addAll(batchData);
        txtBatchNo.setValue(batchNoArr.get(Integer.parseInt(srNo)-1));
        txtReturnQuantity.setText(returnQuantityArr.get(Integer.parseInt(srNo)-1));
        txtReturnPrice.setText(returnPriceArr.get(Integer.parseInt(srNo)-1));
//        txtUnit.setValue(unitArr.get(Integer.parseInt(srNo)-1));
        txtReturnReason.setValue(returnReasonArr.get(Integer.parseInt(srNo)-1));
        CreateSaleReturn.selectedProductId = productIdArr.get(Integer.parseInt(srNo)-1);
        CreateSaleReturn.selectedProductPrice = String.valueOf(Float.parseFloat(returnPriceArr.get(Integer.parseInt(srNo)-1))/Integer.parseInt(returnQuantityArr.get(Integer.parseInt(srNo)-1)));
        btnAdd.setVisible(false);
        btnCancel.setVisible(true);
        btnUpdate.setVisible(true);
    }

    private void deleteAddedRecord()
    {
        productIdArr.remove(Integer.parseInt(srNo)-1);
        productNameArr.remove(Integer.parseInt(srNo)-1);
        batchNoArr.remove(Integer.parseInt(srNo)-1);
        returnQuantityArr.remove(Integer.parseInt(srNo)-1);
        returnPriceArr.remove(Integer.parseInt(srNo)-1);
//        unitArr.remove(Integer.parseInt(srNo)-1);
        returnReasonArr.remove(Integer.parseInt(srNo)-1);
        table_salereturndetaillog.setItems(CreateSaleReturn.parseUserList());
    }

    public String getReturnId() {
        return returnId;
    }

    public void setReturnId(String returnId) {
        this.returnId = returnId;
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    public String getReturnQuantity() {
        return returnQuantity;
    }

    public void setReturnQuantity(String returnQuantity) {
        this.returnQuantity = returnQuantity;
    }

    public String getReturnPrice() {
        return returnPrice;
    }

    public void setReturnPrice(String returnPrice) {
        this.returnPrice = returnPrice;
    }

    public String getReturnReason() {
        return returnReason;
    }

    public void setReturnReason(String returnReason) {
        this.returnReason = returnReason;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public ArrayList<ArrayList<String>> getDealersCompleteData(Statement stmt)
    {
        ArrayList<ArrayList<String>> dealersData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        String query = "SELECT `dealer_table_id`, `dealer_id`, `dealer_name`, `dealer_phone`, `dealer_address`, `dealer_lic9_num`, `dealer_lic9_exp`  FROM `dealer_info` WHERE `dealer_status` != 'Deleted'";
//        System.out.println(query);
        try {
            rs = stmt.executeQuery(query);
            while (rs.next())
            {
                temp = new ArrayList<>();
                temp.add(rs.getString(1)); // Dealer Table Id
                temp.add(rs.getString(2)); // Dealer Id
                temp.add(rs.getString(3)+" ("+rs.getString(5)+")"); // Dealer Name
                temp.add(rs.getString(4)); // Dealer Contact
                temp.add(rs.getString(5)); // Dealer Address
                temp.add(rs.getString(6)); // Dealer Lic Num
                temp.add(rs.getString(7)); // Dealer Lic Exp
                dealersData.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dealersData;
    }

    public ArrayList<ArrayList<String>> loadReturnTable()
    {
        ArrayList<ArrayList<String>> saleData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        for(int i=0; i<productIdArr.size(); i++)
        {
            temp = new ArrayList<String>();
            temp.add(productNameArr.get(i));
            temp.add(batchNoArr.get(i));
            temp.add(returnQuantityArr.get(i));
            temp.add(returnPriceArr.get(i));
            temp.add(returnReasonArr.get(i));
            saleData.add(temp);
        }
        return saleData;
    }

    public void saveReturnData(Statement stmt,String dealersID,float returnTotalPrice,float returnGrossPrice,String saleInvoNum,String returnsDate,String returnsTime,String returnUser,String updateReturnsDate,String updateReturnsTime,String updateReturnUser,String returnsCommnt,String returnsStatus,String returnServerSync,float itemsQuantity) throws SQLException {

        if(saleInvoNum.equals(""))
        {
            saleInvoNum="0";
        }
        String insert_return_query= "INSERT INTO `order_return` ( `dealer_id`, `return_total_price`, `return_gross_price`, `order_id`, `entry_date`, `entry_time`, `enter_user_id`, `update_date`, `update_time`, `update_user_id`, `comments`, `status`, `server_sync`) VALUES ";
        insert_return_query += "('"+dealersID+"','"+returnTotalPrice+"','"+returnGrossPrice+"','"+saleInvoNum+"','"+returnsDate+"','"+returnsTime+"','"+returnUser+"','"+updateReturnsDate+"','"+updateReturnsTime+"','"+updateReturnUser+"','"+returnsCommnt+"','"+returnsStatus+"','"+returnServerSync+"') ";
        stmt.executeUpdate(insert_return_query);
        ResultSet rset = null;
        String getvalueoverallID="";
        String overallrecordquery = "SELECT dealer_overall_id from dealer_overall_record where dealer_id='"+dealersID+"'";
        rset = stmt.executeQuery(overallrecordquery);
        while (rset.next()) {
            getvalueoverallID = rset.getString(1);
        }
        if(getvalueoverallID.equals(""))
        {
            int ordergiven=0;
            int successful_orders=0;
            int ordered_packets=0;
            int submitted_packets=0;
            int ordered_boxes=0;
            int submitted_boxes=0;
            float order_price=0.0f;
            float discount_price=0.0f;
            float invoiced_price=0.0f;
            float cash_collected=0.0f;
            float retrnpackts=0;
            int retrnboxs=0;
            float retrnprice=0.0f;
            float cashretrn=0.0f;
            float waived_off_price=0.0f;
            float pending_payments= 0.0f;

            retrnprice = retrnprice-returnTotalPrice;
            retrnpackts = retrnpackts+itemsQuantity;
            cashretrn = cashretrn-returnTotalPrice;
            pending_payments = pending_payments-returnTotalPrice;
            try {
                String insertQuery = "INSERT INTO `dealer_overall_record` (`dealer_id`, `orders_given`, `successful_orders`, `ordered_packets`, `submitted_packets`, `ordered_boxes`, `submitted_boxes`, `order_price`,`discount_price`, `invoiced_price` , `cash_collected`,`return_packets`,`return_boxes`,`return_price`,`cash_return`, `waived_off_price`, `pending_payments` , `server_sync`) VALUES ('"+dealersID+"','"+ordergiven+"','"+successful_orders+"','"+ordered_packets+"','"+submitted_packets+"','"+ordered_boxes+"','"+submitted_boxes+"' ,'"+order_price+"','"+discount_price+"' ,'"+invoiced_price+"','"+cash_collected+"' ,'"+retrnpackts+"','"+retrnboxs+"','"+retrnprice+"' ,'"+cashretrn+"','"+waived_off_price+"','"+pending_payments+"','Insert' )";
                stmt.executeUpdate(insertQuery);
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
        else {

            ArrayList<String> temp = new ArrayList<>();
            ResultSet rs = null;
            String query = "SELECT `return_packets`, `cash_return`, `return_price`, `pending_payments`  FROM `dealer_overall_record` WHERE `dealer_id` != '" + dealersID + "'";
            try {
                rs = stmt.executeQuery(query);
                while (rs.next()) {
                    temp.add(rs.getString(1));
                    temp.add(rs.getString(2));
                    temp.add(rs.getString(3));
                    temp.add(rs.getString(4));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

            float prev_returnPacket = Float.parseFloat(temp.get(0));
            float prev_cashReturn = Float.parseFloat(temp.get(1));
            float prev_ReturnPrice = Float.parseFloat(temp.get(2));
            float prev_pendingPayments = Float.parseFloat(temp.get(3));

            float totalCashReturn = prev_cashReturn + returnTotalPrice;
            float totalReturnPrice = prev_ReturnPrice + returnTotalPrice;
            float totalPendingPayment = prev_pendingPayments - returnTotalPrice;
            float total_retrndPacks = prev_returnPacket + itemsQuantity;

            String updateDealerOverallRecord = "UPDATE `dealer_overall_record` SET `return_packets`='" + total_retrndPacks + "', `return_price`='" + totalReturnPrice + "',`cash_return`='" + totalCashReturn + "',`pending_payments`='" + totalPendingPayment + "' ,`server_sync`='Update' where dealer_id='" + dealersID + "'";
            stmt.executeUpdate(updateDealerOverallRecord);
        }

    }

    public void insert_Return_Invoice(Statement stmt)
    {
        String updateProductStock = "";
        String updateBatchStock = "";
        String unit = "Packets";
        String insert_return_query= "";

        try {
            for(int i=0; i<productIdArr.size(); i++)
            {
                if(i == productIdArr.size()-1) {
                    insert_return_query = "INSERT INTO `order_return_detail`( `return_id`, `prod_id`, `prod_batch`, `prod_quant`, `bonus_quant`, `discount_amount`, `total_amount`, `unit`, `return_reason`, `server_sync`) VALUES ";
                    insert_return_query += "('" + returnId + "','" + productIdArr.get(i) + "','" + batchNoArr.get(i) + "','" + returnQuantityArr.get(i) + "','0','0','" + returnPriceArr.get(i) + "','" + unit + "','" + returnReasonArr.get(i) + "','INSERT') ";
                }
                else {
                    insert_return_query = "INSERT INTO `order_return_detail`( `return_id`, `prod_id`, `prod_batch`, `prod_quant`, `bonus_quant`, `discount_amount`, `total_amount`, `unit`, `return_reason`, `server_sync`) VALUES ";
                    insert_return_query += "('" + returnId + "','" + productIdArr.get(i) + "','" + batchNoArr.get(i) + "','" + returnQuantityArr.get(i) + "','0','0','" + returnPriceArr.get(i) + "','" + unit + "','" + returnReasonArr.get(i) + "','INSERT') ";
                }
                updateProductStock = "UPDATE `products_stock` SET `in_stock` = `in_stock` + '"+returnQuantityArr.get(i)+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `product_table_id` = '"+productIdArr.get(i)+"'; ";
                updateBatchStock = "UPDATE `batchwise_stock` SET `quantity` = `quantity` + '"+returnQuantityArr.get(i)+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `product_table_id` = '"+productIdArr.get(i)+"' AND `batch_no` = '"+batchNoArr.get(i)+"';";
                stmt.executeUpdate(updateProductStock);
                stmt.executeUpdate(updateBatchStock);
                stmt.executeUpdate(insert_return_query);
                insert_return_query="";
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    public String get_return_id(Statement stmt)
    {
        ResultSet rs = null;
        String getIDValue="";
        String IDValue="";
        int aditionValue = 0;
        String query = "SELECT `return_id`  FROM `order_return` WHERE `status` != 'Deleted' ORDER BY return_id DESC LIMIT 1";
//        System.out.println(query);
        try {
            rs = stmt.executeQuery(query);
            while (rs.next())
            {
                IDValue = rs.getString(1);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if(IDValue!="")
        {
            int rtrnID = Integer.parseInt(IDValue);
            aditionValue = rtrnID+1;
        }
        getIDValue = String.valueOf(aditionValue);
        return getIDValue;
    }

}
