package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import controller.SetupOwnerBusiness;
import controller.SetupOwnerCompany;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;
import org.controlsfx.control.textfield.TextFields;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class SetupOwnerCompanyInfo {
    private String srNo;
    private String companyName;
    private String companyContact;
    private String companyAddress;
    private String companyEmail;
    private String companyAdded;

    private JFXCheckBox operationsPane;
    public static ArrayList<String> companyNameArr = new ArrayList<>();
    public static ArrayList<String> companyContactArr = new ArrayList<>();
    public static ArrayList<String> companyAddressArr = new ArrayList<>();
    public static ArrayList<String> companyEmailArr = new ArrayList<>();
    public static ArrayList<JFXCheckBox> arrCheckBox = new ArrayList<>();

    public static Label lblCompaniesSelected;

    String webUrl;
    public static String businessCategory = "Pharmaceutical";

    public static ArrayList<ArrayList<String>> companiesData = new ArrayList<>();
    public static TableView<SetupOwnerCompanyInfo> table_viewcompanies;

    public SetupOwnerCompanyInfo() {
        this.webUrl = "https://tuberichy.com/ProfitFlow/ServerDataGet/GetData.php?software_lic_id="+GlobalVariables.softwareLicId+"&required_data=companies_detail&business_category="+businessCategory;
        String fetchedOrder = fetchServerData();
        if(fetchedOrder != null && !fetchedOrder.equals(""))
        {
            loadIntoListView(fetchedOrder);
            table_viewcompanies.setItems(SetupOwnerCompany.parseUserList());
        }
    }

    public SetupOwnerCompanyInfo(String srNo, String companyName, String companyContact, String companyAddress, String companyEmail) {
        this.srNo = srNo;
        this.companyName = companyName;
        this.companyContact = companyContact;
        this.companyAddress = companyAddress;
        this.companyEmail = companyEmail;
//        this.companyAdded = companyAdded;

        this.operationsPane = new JFXCheckBox();
        this.operationsPane.setOnAction((action)->checkClicked());
        arrCheckBox.add(this.operationsPane);
        this.operationsPane.setAlignment(Pos.CENTER);
    }

    private void checkClicked()
    {
        if(this.operationsPane.isSelected())
        {
            SetupOwnerCompany.selectedCompanies++;
            lblCompaniesSelected.setText(String.valueOf(SetupOwnerCompany.selectedCompanies));
            arrCheckBox.get(Integer.parseInt(srNo)-1).setSelected(true);
        }
        else
        {
            SetupOwnerCompany.selectedCompanies--;
            lblCompaniesSelected.setText(String.valueOf(SetupOwnerCompany.selectedCompanies));
            arrCheckBox.get(Integer.parseInt(srNo)-1).setSelected(false);
        }

//        SetupOwnerCompanyInfo.table_viewcompanies.setRowFactory(tv -> new TableRow<SetupOwnerCompanyInfo>() {
//            @Override
//            protected void updateItem(SetupOwnerCompanyInfo item, boolean empty) {
//                super.updateItem(item, empty);
//
//                for(int i=0; i<SetupOwnerCompanyInfo.arrCheckBox.size(); i++)
//                {
//                    if(SetupOwnerCompanyInfo.arrCheckBox.get(i).isSelected())
//                    {
//                        setStyle("-fx-background-color: #ffd7d1;");
//                    }
//                }
//            }
//        });
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyContact() {
        return companyContact;
    }

    public void setCompanyContact(String companyContact) {
        this.companyContact = companyContact;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getCompanyEmail() {
        return companyEmail;
    }

    public void setCompanyEmail(String companyEmail) {
        this.companyEmail = companyEmail;
    }

    public String getCompanyAdded() {
        return companyAdded;
    }

    public void setCompanyAdded(String companyAdded) {
        this.companyAdded = companyAdded;
    }

    public JFXCheckBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(JFXCheckBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public ArrayList<ArrayList<String>> getCompaniesData() {
        return companiesData;
    }

    private String fetchServerData()
    {
        try
        {
            URL url = new URL(webUrl);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setConnectTimeout(2 * 1000);
            con.connect();
            if (con.getResponseCode() == 200)
            {
                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String json;
                while ((json = bufferedReader.readLine()) != null)
                {
                    sb.append(json + "\n");
                }
                return sb.toString().trim();
            }
            else
            {
                return null;
            }
        }
        catch (Exception e)
        {
            return null;
        }
    }

    private void loadIntoListView(String json) throws JSONException {
        JSONArray jsonArray = new JSONArray(json);
        companiesData = new ArrayList<>();
        ArrayList<String> temp;
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject obj = jsonArray.getJSONObject(i);
            temp = new ArrayList<>();
            temp.add(String.valueOf(obj.getInt("system_company_id")));
            temp.add(obj.getString("company_name"));
            temp.add(obj.getString("company_contact"));
            temp.add(obj.getString("company_address"));
            temp.add(obj.getString("company_email"));
            companiesData.add(temp);
        }
    }

    public boolean insertCompany(Statement stmt)
    {
        String companyIds = "";
        boolean firstIter = true;
        String currentDate = GlobalVariables.getStDate();
        String currentTime = GlobalVariables.getStTime();
        String currentUser = GlobalVariables.getUserId();
        String companyTableId;
        String insertOverall;
        String insertCompanyQuery = "";
        try {
            for(int i=0; i<arrCheckBox.size(); i++)
            {
                if(arrCheckBox.get(i).isSelected())
                {
                    if(firstIter)
                    {
                        companyIds = companiesData.get(i).get(0);
                        firstIter = false;
                    }
                    else
                    {
                        companyIds += ","+companiesData.get(i).get(0);
                    }

                    insertCompanyQuery = "INSERT INTO `company_info`(`company_name`, `company_address`, `company_city`, `company_contact`, `contact_Person`, `company_email`, `company_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`) VALUES ('"+companyNameArr.get(i)+"','"+companyAddressArr.get(i)+"','','"+companyContactArr.get(i)+"','','"+companyEmailArr.get(i)+"','Active' ,'"+currentUser+"','"+currentDate+"','"+currentTime+"','0','','')";
                    stmt.executeUpdate(insertCompanyQuery);
                    companyTableId = getCompanyTableId(stmt, companyNameArr.get(i), companyContactArr.get(i));
                    insertOverall = "INSERT INTO `company_overall_record`(`company_id`, `orders_given`, `successful_orders`, `ordered_packets`, `received_packets`, `ordered_boxes`, `received_boxes`, `order_price`, `discount_price`, `invoiced_price`, `cash_sent`, `return_packets`, `return_boxes`, `return_price`, `cash_return`, `waived_off_price`, `pending_payments`) VALUES ('"+companyTableId+"','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0')";
                    stmt.executeUpdate(insertOverall);
                }
            }
            SetupOwnerProductsInfo.companyIds = companyIds;
            return true;
        } catch (SQLException e) {
            String title = "Error";
//            String message = "Unable to insert";
            GlobalVariables.showNotification(-1, title, e.getMessage());
//            e.printStackTrace();
            return false;
        }
    }

    public String getCompanyTableId(Statement stmt3, String companyName, String contactNumber)
    {
        String compID = "0";
        ResultSet rs3 = null;
        try {
            rs3 = stmt3.executeQuery("select company_table_id from company_info where company_name = '"+companyName+"' AND company_contact = '"+contactNumber+"' and company_status != 'Deleted'");
            while (rs3.next())
            {
                compID = rs3.getString(1) ;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return compID;
    }
}
