package model;

public class InvoicedItemsInfo {
    private String srNo;
    private String itemNo;
    private String itemName;
    private String packing;
    private String batchNo;
    private String itemRate;
    private String quantity;
    private String bonusQty;
    private String grossAmount;
    private String salesTax;
    private String discount;
    private String totalAmount;
    private String totalQty;
    private String totalBonus;
    private String orgPrice;
    private String totalSalesTax;
    private String totalDiscount;
    private String finalPrice;
    private String textAmount;
    private String advTax;
    private String advTaxAmount;
    private String totalNetValue;
    private String footer1;

    private String unit;

    public InvoicedItemsInfo() {
    }

    public InvoicedItemsInfo(String srNo, String itemNo, String itemName, String packing, String batchNo, String itemRate, String quantity, String bonusQty, String grossAmount, String salesTax, String discount, String totalAmount) {
        this.srNo = srNo;
        this.itemNo = itemNo;
        this.itemName = itemName;
        this.packing = packing;
        this.batchNo = batchNo;
        this.itemRate = itemRate;
        this.quantity = quantity;
        this.bonusQty = bonusQty;
        this.grossAmount = grossAmount;
        this.salesTax = salesTax;
        this.discount = discount;
        this.totalAmount = totalAmount;
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getItemNo() {
        return itemNo;
    }

    public void setItemNo(String itemNo) {
        this.itemNo = itemNo;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getPacking() {
        return packing;
    }

    public void setPacking(String packing) {
        this.packing = packing;
    }

    public String getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    public String getItemRate() {
        return itemRate;
    }

    public void setItemRate(String itemRate) {
        this.itemRate = itemRate;
    }

    public String getBonusQty() {
        return bonusQty;
    }

    public void setBonusQty(String bonusQty) {
        this.bonusQty = bonusQty;
    }

    public String getGrossAmount() {
        return grossAmount;
    }

    public void setGrossAmount(String grossAmount) {
        this.grossAmount = grossAmount;
    }

    public String getSalesTax() {
        return salesTax;
    }

    public void setSalesTax(String salesTax) {
        this.salesTax = salesTax;
    }

    public String getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(String totalQty) {
        this.totalQty = totalQty;
    }

    public String getTotalBonus() {
        return totalBonus;
    }

    public void setTotalBonus(String totalBonus) {
        this.totalBonus = totalBonus;
    }

    public String getOrgPrice() {
        return orgPrice;
    }

    public void setOrgPrice(String orgPrice) {
        this.orgPrice = orgPrice;
    }

    public String getTotalSalesTax() {
        return totalSalesTax;
    }

    public void setTotalSalesTax(String totalSalesTax) {
        this.totalSalesTax = totalSalesTax;
    }

    public String getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(String totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public String getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(String finalPrice) {
        this.finalPrice = finalPrice;
    }

    public String getTextAmount() {
        return textAmount;
    }

    public void setTextAmount(String textAmount) {
        this.textAmount = textAmount;
    }

    public String getAdvTax() {
        return advTax;
    }

    public void setAdvTax(String advTax) {
        this.advTax = advTax;
    }

    public String getAdvTaxAmount() {
        return advTaxAmount;
    }

    public void setAdvTaxAmount(String advTaxAmount) {
        this.advTaxAmount = advTaxAmount;
    }

    public String getTotalNetValue() {
        return totalNetValue;
    }

    public void setTotalNetValue(String totalNetValue) {
        this.totalNetValue = totalNetValue;
    }

    public String getFooter1() {
        return footer1;
    }

    public void setFooter1(String footer1) {
        this.footer1 = footer1;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
