package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import controller.*;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

public class ViewPurchaseOrderInfo {
    private String srNo;
    private String order_id;
    private String company_id;
    private String order_date;
    private String order_amount;
    private String orderStatus;

    private HBox operationsPane;
    private JFXButton btnView;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    SimpleDateFormat fmt = new SimpleDateFormat("dd/MMM/yyyy");
    public static String orderId;

    public static StackPane stackPane;
    public static JFXDialog dialog;

    public ViewPurchaseOrderInfo() {
    }

    public ViewPurchaseOrderInfo(String srNo, String order_id, String company_id,String order_date, String order_amount,  String orderStatus) {
        this.srNo = srNo;
        this.order_id = order_id;
        this.company_id = company_id;
        this.order_date = order_date;
        this.order_amount = order_amount;
        this.orderStatus = orderStatus;

        this.btnView = new JFXButton();
        this.btnEdit = new JFXButton();
        this.btnDelete = new JFXButton();
        this.btnView.setGraphic(GlobalVariables.createViewIcon());
        this.btnEdit.setGraphic(GlobalVariables.createEditIcon());
        this.btnDelete.setGraphic(GlobalVariables.createDeleteIcon());
        this.btnView.getStyleClass().add("btn_icon");
        this.btnEdit.getStyleClass().add("btn_icon");
        this.btnDelete.getStyleClass().add("btn_icon");
        this.operationsPane = new HBox(btnView, btnEdit, btnDelete);
        this.btnView.setOnAction((action)->viewClicked());
        this.btnEdit.setOnAction((action)->editClicked());
    }

    private void editClicked()
    {
        Parent parent = null;
        PurchaseOrder.EditValue = "Edit";
        PurchaseOrder.orderId = this.order_id;
        try {
            parent = FXMLLoader.load(getClass().getResource("/view/purchase_order.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(parent);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    private void viewClicked()
    {
        ViewPurchOrderDetail.purchOrderID = this.order_id;
        Parent parent = null;
        try {

            parent = FXMLLoader.load(getClass().getResource("/view/view_purch_order_detail.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setStyle("-fx-padding: -20 -10 -20 -10;");
        dialogLayout.setBody(parent);
        dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        stackPane.setVisible(true);
        dialog.show();
        dialog.setOverlayClose(false);
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getOrder_date() {
        return order_date;
    }

    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }

    public String getOrder_amount() {
        return order_amount;
    }

    public void setOrder_amount(String order_amount) {
        this.order_amount = order_amount;
    }

    public String getCompany_id() {
        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public ArrayList<ArrayList<String>> getSalesInfo(Statement stmt, Connection con, String fromDate, String toDate)
    {
        ArrayList<ArrayList<String>> saleData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        int day;
        String stDay = "";
        try {
            rs = stmt.executeQuery("SELECT `order_info`.`booking_date`, `order_info`.`order_id`, `dealer_info`.`dealer_name`, `dealer_info`.`dealer_phone`, SUM(`order_info_detailed`.`quantity`) as 'ordered_items', SUM(`order_info_detailed`.`submission_quantity`) as 'submitted_items', SUM(`order_info_detailed`.`quantity`-`order_info_detailed`.`submission_quantity`) as 'missed_items', SUM(`order_info_detailed`.`returned_quant`) as 'returned_items', (SUM(`order_info_detailed`.`final_price`)/SUM(`order_info_detailed`.`submission_quantity`))*SUM(`order_info_detailed`.`quantity`-`order_info_detailed`.`submission_quantity`) as 'missed_price', (SUM(`order_info_detailed`.`final_price`)/SUM(`order_info_detailed`.`submission_quantity`))*SUM(`order_info_detailed`.`returned_quant`) as 'return_price', SUM(`order_info_detailed`.`final_price`) as 'invoice_price', `order_info`.`discount` as 'discount_given', `user_info`.`user_name`, `order_info`.`status`, `order_info`.`comments` FROM `order_info` LEFT OUTER JOIN `dealer_info` ON `dealer_info`.`dealer_table_id` = `order_info`.`dealer_id` LEFT OUTER JOIN `order_info_detailed` ON `order_info_detailed`.`order_id` = `order_info`.`order_id` LEFT OUTER JOIN `user_info` ON `user_info`.`user_table_id` = `order_info`.`booking_user_id` WHERE str_to_date(`booking_date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') AND str_to_date(`booking_date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y') AND `order_info`.`status` != 'Deleted' GROUP BY `order_info`.`order_id` ORDER BY str_to_date(`order_info`.`booking_date`, '%d/%b/%Y') DESC");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Booking Date
                Date objDate = fmt.parse(rs.getString(1));
                day = objDate.getDay();
                if(day == 0)
                    stDay = "Sunday";
                else if(day == 1)
                    stDay = "Monday";
                else if(day == 2)
                    stDay = "Tuesday";
                else if(day == 3)
                    stDay = "Wednesday";
                else if(day == 4)
                    stDay = "Thursday";
                else if(day == 5)
                    stDay = "Friday";
                else if(day == 6)
                    stDay = "Saturday";
                else
                    stDay = "N/A";
                temp.add(stDay); // Day
                temp.add(rs.getString(2)); // Invoice No
                temp.add(rs.getString(3)); // Dealer Name
                temp.add(rs.getString(4)); // Dealer Contact
                temp.add(rs.getString(5)); // Ordered Items
                temp.add(rs.getString(6)); // Submitted Items
                temp.add(rs.getString(7)); // Missed Items
                temp.add(rs.getString(8)); // Returned Items
                temp.add(rs.getString(9)); // Missed Price
                temp.add(rs.getString(10)); // Return price
                temp.add(rs.getString(11)); // Invoice Price
                temp.add(rs.getString(12)); // Discount Given
                temp.add(rs.getString(13)); // User Name
                temp.add(rs.getString(14)); // Invoice Status
                temp.add(rs.getString(15)); // Comment
                saleData.add(temp);
            }
        } catch (SQLException | ParseException e) {
            e.printStackTrace();
        }
        return saleData;
    }


    public ArrayList<ArrayList<String>> getPurchaseOrderSearch(Statement stmt, Connection con, String fromDates, String toDates, String companyName, String orderStatus)
    {
        ArrayList<ArrayList<String>> purchOrderData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        String searchQuery;
        int multipleSearch = 0;
        searchQuery = "SELECT `purchase_order_info`.`purch_order_id`, `purchase_order_info`.`purchase_order_date`, `purchase_order_info`.`purchase_order_amount`, `company_info`.`company_id`,`purchase_order_info`.`order_status` inner JOIN `company_info` ON `company_info`.`company_table_id` = `purchase_order_info`.`company_table_id` WHERE ";
        if(!fromDates.equals(""))
        {
            //str_to_date(`delivered_date`, '%d/%b/%Y')
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "str_to_date(`purchase_order_info`.`purchase_order_date`,'%d/%b/%Y') = str_to_date('"+fromDates+"','%d/%b/%Y')";
            multipleSearch++;
        }
        if(!toDates.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "str_to_date(`purchase_order_info`.`purchase_order_date`,'%d/%b/%Y') = str_to_date('"+toDates+"','%d/%b/%Y')";
            multipleSearch++;
        }
        if(!companyName.equals("All"))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`company_info`.`company_name` = '"+companyName+"'";
            multipleSearch++;
        }
        if(!orderStatus.equals("All"))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`purchase_order_info`.`order_status` = '"+orderStatus+"'";
            multipleSearch++;
        }

        if(multipleSearch >= 1)
        {
            searchQuery += " AND ";
        }
        searchQuery += "`product_info`.`product_status` != 'Deleted' ORDER BY `product_info`.`product_table_id`";
        try {
            rs = stmt.executeQuery(searchQuery);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2));
                temp.add(rs.getString(3));
                temp.add(rs.getString(4));
                temp.add(rs.getString(5));
                temp.add(rs.getString(6));
                temp.add(rs.getString(7));
                temp.add(rs.getString(8));
                temp.add(rs.getString(9));
                temp.add(rs.getString(10));
                temp.add(rs.getString(11));

                purchOrderData.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return purchOrderData;
    }
    public ArrayList<ArrayList<String>> getPurchaseOrder(Statement stmt, Connection con)
    {
        ArrayList<ArrayList<String>> purchOrderData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        String searchQuery;
        int multipleSearch = 0;
        searchQuery = "SELECT `purchase_order_info`.`purch_order_id`, `company_info`.`company_name`, `purchase_order_info`.`purch_order_date`, `purchase_order_info`.`purch_order_amount`,   `purchase_order_info`.`order_status` FROM `purchase_order_info` INNER JOIN company_info on `company_info`.`company_table_id` = `purchase_order_info`.`comp_table_id`";
        try {
            rs = stmt.executeQuery(searchQuery);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2));
                temp.add(rs.getString(3));
                temp.add(rs.getString(4));
                temp.add(rs.getString(5));

                purchOrderData.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return purchOrderData;
    }

    public void updateComment(Statement stmt, Connection con, String orderId, String comment)
    {
        String currentDate = GlobalVariables.getStDate();
        String currentTime = GlobalVariables.getStTime();
        String currentUser = GlobalVariables.getUserId();
        String updateQuery = "UPDATE `order_info` SET `update_dates` = '"+currentDate+"', `update_times` = '"+currentTime+"',`update_user_id`= '"+currentUser+"', `comments` = '"+comment+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `order_id` = '"+orderId+"'";
        try {
            stmt.executeUpdate(updateQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


}
