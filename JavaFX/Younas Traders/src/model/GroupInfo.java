package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import controller.EnterGroup;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class GroupInfo {
    private String srNo;
    private String groupId;
    private String companyName;
    private String groupName;

    private HBox operationsPane;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    public static Button btnAdd;
    public static Button btnCancel;
    public static Button btnUpdate;

    public static ArrayList<String> groupIdArr = new ArrayList<>();
    public static ArrayList<String> companyIdArr = new ArrayList<>();
    public static ArrayList<String> companyNameArr = new ArrayList<>();
    public static ArrayList<String> groupNameArr = new ArrayList<>();

    public static TableView<GroupInfo> table_addgroup;

    public static JFXTextField txtGroupId;
    public static JFXComboBox<String> txtCompanyName;
    public static JFXTextField txtGroupName;

    private ArrayList<String> savedCompanyIds;
    private ArrayList<String> savedCompanyNames;

    MysqlCon objMysqlCon1 = new MysqlCon();
    Statement objStmt1 = objMysqlCon1.stmt;
    Connection objCon1 = objMysqlCon1.con;

    public GroupInfo()
    {
        this.srNo = "";
        this.groupId = "";
        this.companyName = "";
        this.groupName = "";

        getCompaniesInfo(objStmt1, objCon1);
    }

    public GroupInfo(String srNo, String groupId, String companyName, String groupName) {
        this.srNo = srNo;
        this.groupId = groupId;
        this.companyName = companyName;
        this.groupName = groupName;

        this.btnEdit = new JFXButton("Edit");
        this.btnDelete = new JFXButton("Delete");
        this.btnEdit.setOnAction((action)->editAddedRecord());
        this.btnDelete.setOnAction((action)->deleteAddedRecord());
        this.operationsPane = new HBox(btnEdit, btnDelete);
    }

    private void deleteAddedRecord()
    {
        groupIdArr.remove(Integer.parseInt(srNo)-1);
        companyIdArr.remove(Integer.parseInt(srNo)-1);
        companyNameArr.remove(Integer.parseInt(srNo)-1);
        groupNameArr.remove(Integer.parseInt(srNo)-1);
        table_addgroup.setItems(EnterGroup.parseUserList());
    }

    public void editAddedRecord()
    {
        EnterGroup.srNo = Integer.parseInt(srNo)-1;
        txtGroupId.setText(groupIdArr.get(Integer.parseInt(srNo)-1));
        txtCompanyName.setValue(companyNameArr.get(Integer.parseInt(srNo)-1));
        txtGroupName.setText(groupNameArr.get(Integer.parseInt(srNo)-1));

        btnAdd.setVisible(false);
        btnCancel.setVisible(true);
        btnUpdate.setVisible(true);
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public static ArrayList<String> getGroupIdArr() {
        return groupIdArr;
    }

    public static void setGroupIdArr(ArrayList<String> groupIdArr) {
        GroupInfo.groupIdArr = groupIdArr;
    }

    public ArrayList<String> getSavedCompanyIds() {
        return savedCompanyIds;
    }

    public void setSavedCompanyIds(ArrayList<String> savedCompanyIds) {
        this.savedCompanyIds = savedCompanyIds;
    }

    public ArrayList<String> getSavedCompanyNames() {
        return savedCompanyNames;
    }

    public void setSavedCompanyNames(ArrayList<String> savedCompanyNames) {
        this.savedCompanyNames = savedCompanyNames;
    }

    public void insertGroup(Statement stmt, Connection con) throws SQLException {
        String currentDate = GlobalVariables.getStDate();
        String currentTime = GlobalVariables.getStTime();
        String currentUser = GlobalVariables.getUserId();
        boolean withId = false;
        boolean withoutId = false;
        String insertQueryWithId = "INSERT INTO `groups_info`(`group_id`, `company_id`, `group_name`, `group_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`) VALUES ";
        String insertQueryWithoutId = "INSERT INTO `groups_info`(`company_id`, `group_name`, `group_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`) VALUES ";

        for(int i=0; i<groupIdArr.size(); i++)
        {
            if(groupIdArr.get(i).equals("") || groupIdArr.get(i) == null)
            {
                if(withoutId)
                {
                    insertQueryWithoutId += ", ";
                }
                withoutId = true;
                insertQueryWithoutId += "('"+companyIdArr.get(i)+"','"+groupNameArr.get(i)+"','Active','"+currentUser+"','"+currentDate+"','"+currentTime+"','"+currentUser+"','"+currentDate+"','"+currentTime+"')";
            }
            else
            {
                if(withId)
                {
                    insertQueryWithId += ", ";
                }
                withId = true;
                insertQueryWithId += "('"+groupIdArr.get(i)+"','"+companyIdArr.get(i)+"','"+groupNameArr.get(i)+"','Active','"+currentUser+"','"+currentDate+"','"+currentTime+"','"+currentUser+"','"+currentDate+"','"+currentTime+"')";
            }
        }
        int insertInfo = 0;
        if(withId)
        {
            if(stmt.executeUpdate(insertQueryWithId) == -1)
            {
                insertInfo = -1;
            }
        }
        if(withoutId)
        {
            if(stmt.executeUpdate(insertQueryWithoutId) == -1)
            {
                insertInfo = -1;
            }
        }

        if(insertInfo == -1)
        {
            String title = "Error";
            String message = "Unable to insert";
            GlobalVariables.showNotification(-1, title, message);
        }
        else
        {
            String title = "Successfull";
            String message = "Record Inserted";
            GlobalVariables.showNotification(1, title, message);
        }
        refreshAllArrays();
    }

    private void refreshAllArrays()
    {
        groupIdArr = new ArrayList<>();
        companyIdArr = new ArrayList<>();
        companyNameArr = new ArrayList<>();
        groupNameArr = new ArrayList<>();
    }

    public void updateGroup(Statement stmt, Connection con, String groupTableId, String groupId, String groupName, String companyId, String groupStatus)
    {
        String currentDate = GlobalVariables.getStDate();
        String currentTime = GlobalVariables.getStTime();
        String currentUser = GlobalVariables.getUserId();
        String updateQuery;
        try {
            if(groupId.equals("") || groupId.equals("N/A"))
            {
                updateQuery = "UPDATE `groups_info` SET `group_id`= NULL,`company_id`='"+companyId+"',`group_name`='"+groupName+"',`group_status`='"+groupStatus+"',`update_user_id`='"+currentUser+"',`update_date`='"+currentDate+"',`update_time`='"+currentTime+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `group_table_id` = '"+groupTableId+"'";
            }
            else
            {
                updateQuery = "UPDATE `groups_info` SET `group_id`='"+groupId+"',`company_id`='"+companyId+"',`group_name`='"+groupName+"',`group_status`='"+groupStatus+"',`update_user_id`='"+currentUser+"',`update_date`='"+currentDate+"',`update_time`='"+currentTime+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `group_table_id` = '"+groupTableId+"'";
            }
            stmt.executeUpdate(updateQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<ArrayList<String>> getAddedGroups()
    {
        ArrayList<ArrayList<String>> groupData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        for(int i=0; i<groupIdArr.size(); i++)
        {
            temp = new ArrayList<String>();
            temp.add(groupIdArr.get(i));
            temp.add(companyNameArr.get(i));
            temp.add(groupNameArr.get(i));
            groupData.add(temp);
        }
        return groupData;
    }

    public ArrayList<String> getGroupDetail(String groupId)
    {
        ArrayList<String> dealerInfo = new ArrayList<>();
        ResultSet rs = null;
        try {
            rs = GlobalVariables.objStmt.executeQuery("SELECT `groups_info`.`group_id`, `groups_info`.`group_name`, `company_info`.`company_name`, `groups_info`.`creating_date`, `groups_info`.`update_date`, `groups_info`.`group_status` FROM `groups_info` INNER JOIN `company_info` ON `company_info`.`company_table_id` = `groups_info`.`company_id` WHERE `groups_info`.`group_table_id` = '"+groupId+"'");
            while (rs.next())
            {
                dealerInfo.add(rs.getString(1)); // Group Id
                dealerInfo.add(rs.getString(2)); // Name
                dealerInfo.add(rs.getString(3)); // Company Name
                dealerInfo.add(rs.getString(4)); // Created
                dealerInfo.add(rs.getString(5)); // Updated
                dealerInfo.add(rs.getString(6)); // Status
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dealerInfo;
    }

    public void getCompaniesInfo(Statement stmt, Connection con)
    {
//        ArrayList<ArrayList<String>> groupData = new ArrayList<ArrayList<String>>();
        ArrayList<String> tempIds;
        ArrayList<String> tempNames;
        tempIds = new ArrayList<String>();
        tempNames = new ArrayList<String>();
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT `company_table_id`, `company_name` FROM `company_info` WHERE `company_status` != 'Deleted' ORDER BY `company_table_id`");
            while (rs.next())
            {
                tempIds.add(rs.getString(1));
                tempNames.add(rs.getString(2)+" ("+rs.getString(1)+")");
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        setSavedCompanyIds(tempIds);
        setSavedCompanyNames(tempNames);
    }

    String checkGroupID = new String();
    public String confirmNewId(Statement stmt3, Connection con3 ,String groupId)
    {
        ResultSet rs3 = null;
        try {
            rs3 = stmt3.executeQuery("SELECT `group_id` FROM `groups_info` WHERE `group_id` = '"+groupId+"' AND `group_status` != 'Deleted'");
            while (rs3.next())
            {
                checkGroupID = rs3.getString(1) ;
            }
//            con3.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return checkGroupID;
    }
}
