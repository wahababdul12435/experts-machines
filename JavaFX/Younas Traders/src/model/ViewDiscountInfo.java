package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.JFXTextField;
import controller.DeleteWindow;
import controller.UpdateBonus;
import controller.UpdateCompany;
import controller.UpdateDiscount;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import javax.xml.soap.Text;
import java.awt.*;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;

public class ViewDiscountInfo {

    private String srNo;
    private String approval_id;
    private String startDate;
    private String endDate;
    private String policy_status;
    private String entry_date;

    private HBox operationsPane;
    private JFXButton btnView;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    public static StackPane stackPane;
    public static JFXDialog dialog;
    public static Label lblUpdate;

    public static String discountviewApprovalId;

    GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
    public ViewDiscountInfo()
    {
        this.srNo = "";
        this.approval_id = "";
        this.startDate = "";
        this.entry_date = "";
        this.policy_status = "";
        this.entry_date = "";
        this.btnView = new JFXButton();
        this.btnEdit = new JFXButton();
        this.btnDelete = new JFXButton();
        this.btnView.setGraphic(GlobalVariables.createViewIcon());
        this.btnEdit.setGraphic(GlobalVariables.createEditIcon());
        this.btnDelete.setGraphic(GlobalVariables.createDeleteIcon());
        this.btnView.getStyleClass().add("btn_icon");
        this.btnEdit.getStyleClass().add("btn_icon");
        this.btnDelete.getStyleClass().add("btn_icon");
        this.operationsPane = new HBox(btnView, btnEdit, btnDelete);
        lblUpdate = new Label("");
    }

    public ViewDiscountInfo(String srNo, String approval_id, String startDate, String endDate, String policy_status, String entry_date) {
        this.srNo = srNo;
        this.approval_id = approval_id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.policy_status = policy_status;
        this.entry_date = entry_date;

        this.btnView = new JFXButton();
        this.btnEdit = new JFXButton();
        this.btnDelete = new JFXButton();
        this.btnView.setGraphic(GlobalVariables.createViewIcon());
        this.btnEdit.setGraphic(GlobalVariables.createEditIcon());
        this.btnDelete.setGraphic(GlobalVariables.createDeleteIcon());
        this.btnView.getStyleClass().add("btn_icon");
        this.btnEdit.getStyleClass().add("btn_icon");
        this.btnDelete.getStyleClass().add("btn_icon");
        this.operationsPane = new HBox(btnView, btnEdit, btnDelete);
        lblUpdate = new Label("");

        this.btnView.setOnAction((action)->viewClicked());
        this.btnEdit.setOnAction((action)->editClicked());
        this.btnDelete.setOnAction(event -> {
            try {
                deleteDiscount(this.approval_id);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public String getSrNo() {
        return srNo;
    }
    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }
    public String getApproval_id() {
        return approval_id;
    }
    public void setApproval_id(String approval_id) {
        this.approval_id = approval_id;
    }
    public String getStartDate() {
        return startDate;
    }
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }
    public String getEndDate() {
        return endDate;
    }
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
    public String getPolicy_status() {
        return policy_status;
    }
    public void setPolicy_status(String policy_status) {
        this.policy_status = policy_status;
    }
    public String getEntry_date() {
        return entry_date;
    }
    public void setEntry_date(String entry_date) {
        this.entry_date = entry_date;
    }
    public HBox getOperationsPane() {
        return operationsPane;
    }
    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }
    public JFXButton getBtnView() {
        return btnView;
    }
    public void setBtnView(JFXButton btnView) {
        this.btnView = btnView;
    }
    public JFXButton getBtnEdit() {
        return btnEdit;
    }
    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }
    public JFXButton getBtnDelete() {
        return btnDelete;
    }
    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }
    public static StackPane getStackPane() {
        return stackPane;
    }
    public static void setStackPane(StackPane stackPane) {
        ViewDiscountInfo.stackPane = stackPane;
    }
    public static JFXDialog getDialog() {
        return dialog;
    }
    public static void setDialog(JFXDialog dialog) {
        ViewDiscountInfo.dialog = dialog;
    }
    public static Label getLblUpdate() {
        return lblUpdate;
    }
    public static void setLblUpdate(Label lblUpdate) {
        ViewDiscountInfo.lblUpdate = lblUpdate;
    }
    public static String getViewApprovalId() {
        return discountviewApprovalId;
    }
    public static void setViewApprovalId(String viewApprovalId) {
        ViewDiscountInfo.discountviewApprovalId = viewApprovalId;
    }
    public GlyphFont getFontAwesome() {
        return fontAwesome;
    }
    public void setFontAwesome(GlyphFont fontAwesome) {
        this.fontAwesome = fontAwesome;
    }

    public ArrayList<ArrayList<String>> getDiscountInfo(Statement stmt, Connection con)
    {
        ArrayList<ArrayList<String>> discountPolicyData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT approval_id, start_date,end_date,policy_status,entry_date  FROM `discount_policy` WHERE policy_status != 'Deleted' ORDER BY disc_policyID");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2));
                temp.add(rs.getString(3));
                temp.add(rs.getString(4));
                temp.add(rs.getString(5));
                discountPolicyData.add(temp);
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return discountPolicyData;
    }

    public ArrayList<ArrayList<String>> getDiscountSearch(Statement stmt, Connection con, String approvalId, String startDate, String endDate, String policyStatus, String entryDate)
    {
        ArrayList<ArrayList<String>> discountData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        String searchQuery;
        int multipleSearch = 0;
        searchQuery = "SELECT  `approval_id`, `start_date`, `end_date`, `policy_status`, `entry_date`  FROM `bonus_policy` WHERE ";
        if(!approvalId.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`approval_id` = '"+approvalId+"'";
            multipleSearch++;
        }
        if(startDate!=null)
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`start_date` = '"+startDate+"'";
            multipleSearch++;
        }
        if(endDate!=null)
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`end_date` = '"+endDate+"'";
            multipleSearch++;
        }
        if(entryDate!=null)
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`entry_date` = '"+entryDate+"'";
            multipleSearch++;
        }



        if(multipleSearch >= 1)
        {
            searchQuery += " AND ";
        }
        searchQuery += "`policy_status` != 'Deleted' ORDER BY `bonus_policyID`";
        try {
            rs = stmt.executeQuery(searchQuery);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2));
                temp.add(rs.getString(3));
                temp.add(rs.getString(4));
                temp.add(rs.getString(5));
                discountData.add(temp);
            }

//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return discountData;
    }

    public void deleteDiscount(String DiscountTableId) throws IOException
    {
        DeleteWindow.sceneWindow = "ViewDiscount";
        DeleteWindow.deletionId = DiscountTableId;
        Parent parent = null;
        try {
            parent = FXMLLoader.load(getClass().getResource("/view/delete_window.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setStyle("-fx-padding: -40 -20 -40 -20;");
        dialogLayout.setBody(parent);
        dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        stackPane.setVisible(true);
        dialog.show();
        dialog.setOverlayClose(false);
    }


    public void viewClicked()
    {
        Parent parent = null;
        discountviewApprovalId = this.approval_id;
        try {
            parent = FXMLLoader.load(getClass().getResource("/view/discountdetails.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setStyle("-fx-padding: -20 -10 -20 -10;");
        dialogLayout.setBody(parent);
        dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        stackPane.setVisible(true);
        dialog.show();
        dialog.setOverlayClose(false);
    }

    public void updateDiscountpolicy(Statement stmt, Connection con,String apporvalsID,int dealerID,int companyID,int productID,String saleAmount,String discountPercent,String start_date, String End_date ,String policyStatus )
    {
        String getuserID = GlobalVariables.getUserId();
        String getcurrDate = GlobalVariables.getStDate();
        String getcurrTime = GlobalVariables.getStTime();



        try {
            String insertQuery = "UPDATE `discount_policy` SET `dealer_table_id`='"+dealerID+"',`company_table_id`='"+companyID+"',`product_table_id`='"+productID+"',`sale_amount`='"+saleAmount+"',`discount_percent`='"+discountPercent+"',`start_date`='"+start_date+"',`end_date`='"+End_date+"',`policy_status`='"+policyStatus+"' ,`updated_by`='"+getuserID+"',`update_date`='"+getcurrDate+"',`update_time`='"+getcurrTime+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `approval_id` = '"+apporvalsID+"'";
            stmt.executeUpdate(insertQuery);
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    String discpolicy_details = "";
    public String get_discountpolicy(Statement stmt, Connection con, int approval_id )
    {
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("select discount_policy.approval_id,discount_policy.sale_amount,discount_policy.discount_percent,discount_policy.start_date,discount_policy.end_date,discount_policy.policy_status,dealer_info.dealer_id,dealer_info.dealer_name ,company_info.company_id,company_info.company_name ,product_info.product_id,product_info.product_name from discount_policy inner join dealer_info on discount_policy.dealer_table_id = dealer_info.dealer_table_id inner JOIN company_info on  discount_policy.company_table_id = company_info.company_table_id LEFT JOIN product_info on product_info.product_table_id = discount_policy.product_table_id  where discount_policy.approval_id ='"+approval_id+"' and  discount_policy.policy_status != 'Deleted'   ");
            int i = 0;
            while (rs.next())
            {
                discpolicy_details = rs.getString(1) + "--" + rs.getString(2) + "--" + rs.getString(3) + "--" + rs.getString(4) + "--" + rs.getString(5) + "--" + rs.getString(6) + "--" + rs.getString(7) + "--" + rs.getString(8)+ "--" + rs.getString(9) + "--" + rs.getString(10)+ "--" + rs.getString(11) + "--" + rs.getString(12);
            }

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return discpolicy_details;

    }

    public void editClicked()
    {
        Parent parent = null;
        try {
            UpdateDiscount.discountapprovalID = this.approval_id;
            parent = FXMLLoader.load(getClass().getResource("/view/update_discount.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setStyle("-fx-padding: -20 -10 -20 -10;");
        dialogLayout.setHeading(lblUpdate);
        dialogLayout.setBody(parent);
        dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        stackPane.setVisible(true);
        dialog.show();
        dialog.setOverlayClose(false);
    }

    private Node createViewIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.EYE);
        graphic.setFontSize(20.0);
        return graphic;
    }

    private Node createEditIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.EDIT);
        graphic.setFontSize(20.0);
        return graphic;
    }

    private Node createDeleteIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.TRASH);
        graphic.setFontSize(20.0);
        return graphic;
    }

}
