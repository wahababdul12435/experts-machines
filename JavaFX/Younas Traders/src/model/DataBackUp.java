package model;

import javafx.fxml.Initializable;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class DataBackUp implements Initializable {
    String webURL = "https://tuberichy.com/ProfitFlow/ServerDataGet/SendData.php";
    String response = "";
    MysqlCon objMysqlCon;
    Statement objStmt;
    Connection objCon;
    String query = "";
    boolean firstIter = true;

    private ArrayList<String> tablesName = new ArrayList<String>();
    ArrayList<String> areaData;
    ArrayList<String> batchwiseStockData;
    ArrayList<String> bonusPolicyData;
    ArrayList<String> cityInfoData;
    ArrayList<String> companyAlternateContactsData;
    ArrayList<String> companyInfoData;
    ArrayList<String> companyOverallRecordData;
    ArrayList<String> companyPaymentsData;
    ArrayList<String> dealerDaySummaryData;
    ArrayList<String> dealerGPSLocationData;
    ArrayList<String> dealerInfoData;
    ArrayList<String> dealerOverallRecordData;
    ArrayList<String> dealerPaymentsData;
    ArrayList<String> dealerTypesData;
    ArrayList<String> discountPolicyData;
    ArrayList<String> districtInfoData;
    ArrayList<String> groupsInfoData;
    ArrayList<String> mailsRecordData;
    ArrayList<String> mobileGPSLocationData;
    ArrayList<String> orderGPSLocationData;
    ArrayList<String> orderInfoData;
    ArrayList<String> orderInfoDetailedData;
    ArrayList<String> orderReturnData;
    ArrayList<String> orderReturnDetailData;
    ArrayList<String> orderStatusInfoData;
    ArrayList<String> productsStockData;
    ArrayList<String> productInfoData;
    ArrayList<String> productOverallRecordData;
    ArrayList<String> purchaseInfoData;
    ArrayList<String> purchaseInfoDetailData;
    ArrayList<String> purchaseOrderDetailData;
    ArrayList<String> purchaseOrderInfoData;
    ArrayList<String> purchaseReturnData;
    ArrayList<String> purchaseReturnDetailData;
    ArrayList<String> purchaseReturnInfoDetailData;
    ArrayList<String> purchReturnInfoData;
    ArrayList<String> salesmanDesignatedCitiesData;
    ArrayList<String> saleInvoicePrintInfoData;
    ArrayList<String> supplierCompanyData;
    ArrayList<String> supplierInfoData;
    ArrayList<String> syncTrackingData;
    ArrayList<String> systemSettingsData;
    ArrayList<String> userAccountsData;
    ArrayList<String> userInfoData;
    ArrayList<String> userRightsData;

    boolean foundBackup = false;


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        tablesName.add("area_info");
        tablesName.add("batchwise_stock");
        tablesName.add("bonus_policy");
        tablesName.add("cash_collected");

    }

    public void compute() throws SQLException {
        objMysqlCon = new MysqlCon();
        objStmt = objMysqlCon.stmt;
        objCon = objMysqlCon.con;

        ArrayList<ArrayList<String>> areaInfo = new ArrayList<>();
        ArrayList<ArrayList<String>> batchwiseStock = new ArrayList<>();
        ArrayList<ArrayList<String>> bonusPolicy = new ArrayList<>();
        ArrayList<ArrayList<String>> cityInfo = new ArrayList<>();
        ArrayList<ArrayList<String>> companyAlternateContacts = new ArrayList<>();
        ArrayList<ArrayList<String>> companyInfo = new ArrayList<>();
        ArrayList<ArrayList<String>> companyOverallRecord = new ArrayList<>();
        ArrayList<ArrayList<String>> companyPayments = new ArrayList<>();
        ArrayList<ArrayList<String>> dealerDaySummary = new ArrayList<>();
        ArrayList<ArrayList<String>> dealerGPSLocation= new ArrayList<>();
        ArrayList<ArrayList<String>> dealerInfo= new ArrayList<>();
        ArrayList<ArrayList<String>> dealerOverallRecord= new ArrayList<>();
        ArrayList<ArrayList<String>> dealerPayments= new ArrayList<>();
        ArrayList<ArrayList<String>> dealerTypes= new ArrayList<>();
        ArrayList<ArrayList<String>> discountPolicy= new ArrayList<>();
        ArrayList<ArrayList<String>> districtInfo= new ArrayList<>();
        ArrayList<ArrayList<String>> groupsInfo= new ArrayList<>();
        ArrayList<ArrayList<String>> mailsRecord= new ArrayList<>();
        ArrayList<ArrayList<String>> mobileGPSLocation= new ArrayList<>();
        ArrayList<ArrayList<String>> orderGPSLocation= new ArrayList<>();
        ArrayList<ArrayList<String>> orderInfo= new ArrayList<>();
        ArrayList<ArrayList<String>> orderInfoDetailed= new ArrayList<>();
        ArrayList<ArrayList<String>> orderReturn= new ArrayList<>();
        ArrayList<ArrayList<String>> orderReturnDetail= new ArrayList<>();
        ArrayList<ArrayList<String>> orderStatusInfo= new ArrayList<>();
        ArrayList<ArrayList<String>> productsStock= new ArrayList<>();
        ArrayList<ArrayList<String>> productInfo= new ArrayList<>();
        ArrayList<ArrayList<String>> productOverallRecord= new ArrayList<>();
        ArrayList<ArrayList<String>> purchaseInfo= new ArrayList<>();
        ArrayList<ArrayList<String>> purchaseInfoDetail= new ArrayList<>();
        ArrayList<ArrayList<String>> purchaseOrderDetail= new ArrayList<>();
        ArrayList<ArrayList<String>> purchaseOrderInfo= new ArrayList<>();
        ArrayList<ArrayList<String>> purchaseReturn= new ArrayList<>();
        ArrayList<ArrayList<String>> purchaseReturnDetail= new ArrayList<>();
        ArrayList<ArrayList<String>> purchaseReturnInfoDetail= new ArrayList<>();
        ArrayList<ArrayList<String>> purchReturnInfo= new ArrayList<>();
        ArrayList<ArrayList<String>> salesmanDesignatedCities= new ArrayList<>();
        ArrayList<ArrayList<String>> saleInvoicePrintInfo = new ArrayList<>();
        ArrayList<ArrayList<String>> supplierCompany = new ArrayList<>();
        ArrayList<ArrayList<String>> supplierInfo = new ArrayList<>();
        ArrayList<ArrayList<String>> syncTracking = new ArrayList<>();
        ArrayList<ArrayList<String>> systemSettings = new ArrayList<>();
        ArrayList<ArrayList<String>> userAccounts = new ArrayList<>();
        ArrayList<ArrayList<String>> userInfo = new ArrayList<>();
        ArrayList<ArrayList<String>> userRights = new ArrayList<>();
        areaInfo = getAreaData();
        batchwiseStock = getBatchwiseStockData();
        bonusPolicy = getBonusPolicyData();
        cityInfo = getCityInfoData();
        companyAlternateContacts = getCompanyAlternateContactsData();
        companyInfo = getCompanyInfoData();
        companyOverallRecord = getCompanyOverallRecordData();
        companyPayments = getCompanyPaymentsData();
        dealerDaySummary = getDealerDaySummaryData();
        dealerGPSLocation = getDealerGPSLocationData();
        dealerInfo = getDealerInfoData();
        dealerOverallRecord = getDealerOverallRecordData();
        dealerPayments = getDealerPaymentsData();
        dealerTypes = getDealerTypesData();
        discountPolicy = getDiscountPolicyData();
        districtInfo = getDistrictInfoData();
        groupsInfo = getGroupsInfoData();
        mailsRecord = getMailsRecordData();
        mobileGPSLocation = getMobileGPSLocationData();
        orderGPSLocation = getOrderGPSLocationData();
        orderInfo = getOrderInfoData();
        orderInfoDetailed = getOrderInfoDetailedData();
        orderReturn = getOrderReturnData();
        orderReturnDetail = getOrderReturnDetailData();
        orderStatusInfo = getOrderStatusInfoData();
        productsStock = getProductStockData();
        productInfo = getProductInfoData();
        productOverallRecord = getProductOverallRecordData();
        purchaseInfo = getPurchaseInfoData();
        purchaseInfoDetail = getPurchaseInfoDetailData();
        purchaseOrderDetail = getPurchaseOrderDetailData();
        purchaseOrderInfo = getPurchaseOrderInfoData();
        purchaseReturn = getPurchaseReturnData();
        purchaseReturnDetail = getPurchaseReturnDetailData();
        purchaseReturnInfoDetail = getPurchaseReturnInfoDetailData();
        purchReturnInfo = getPurchReturnInfoData();
        salesmanDesignatedCities = getSalesmanDesignatedCitiesData();
        saleInvoicePrintInfo = getSaleInvoicePrintInfoData();
        supplierCompany = getSupplierCompanyData();
        supplierInfo = getSupplierInfoData();
        syncTracking = getSyncTrackingData();
        systemSettings = getSystemSettingsData();
        userAccounts = getUserAccountsData();
        userInfo = getUserInfoData();
        userRights = getUserRightsData();

        if(foundBackup)
        {
            GlobalVariables.sepBackup.setVisible(true);
            GlobalVariables.lblDataBackup.setVisible(true);
            GlobalVariables.progressBackup.setVisible(true);
        }

        areaData = processArrayToString(areaInfo);
        batchwiseStockData = processArrayToString(batchwiseStock);
        bonusPolicyData = processArrayToString(bonusPolicy);
        cityInfoData = processArrayToString(cityInfo);
        companyAlternateContactsData = processArrayToString(companyAlternateContacts);
        companyInfoData = processArrayToString(companyInfo);
        companyOverallRecordData = processArrayToString(companyOverallRecord);
        companyPaymentsData = processArrayToString(companyPayments);
        dealerDaySummaryData = processArrayToString(dealerDaySummary);
        dealerGPSLocationData = processArrayToString(dealerGPSLocation);
        dealerInfoData = processArrayToString(dealerInfo);
        dealerOverallRecordData = processArrayToString(dealerOverallRecord);
        dealerPaymentsData = processArrayToString(dealerPayments);
        dealerTypesData = processArrayToString(dealerTypes);
        discountPolicyData = processArrayToString(discountPolicy);
        districtInfoData = processArrayToString(districtInfo);
        groupsInfoData = processArrayToString(groupsInfo);
        mailsRecordData = processArrayToString(mailsRecord);
        mobileGPSLocationData = processArrayToString(mobileGPSLocation);
        orderGPSLocationData = processArrayToString(orderGPSLocation);
        orderInfoData = processArrayToString(orderInfo);
        orderInfoDetailedData = processArrayToString(orderInfoDetailed);
        orderReturnData = processArrayToString(orderReturn);
        orderReturnDetailData = processArrayToString(orderReturnDetail);
        orderStatusInfoData = processArrayToString(orderStatusInfo);
        productsStockData = processArrayToString(productsStock);
        productInfoData = processArrayToString(productInfo);
        productOverallRecordData = processArrayToString(productOverallRecord);
        purchaseInfoData = processArrayToString(purchaseInfo);
        purchaseInfoDetailData = processArrayToString(purchaseInfoDetail);
        purchaseOrderDetailData = processArrayToString(purchaseOrderDetail);
        purchaseOrderInfoData = processArrayToString(purchaseOrderInfo);
        purchaseReturnData = processArrayToString(purchaseReturn);
        purchaseReturnDetailData = processArrayToString(purchaseReturnDetail);
        purchaseReturnInfoDetailData = processArrayToString(purchaseReturnInfoDetail);
        purchReturnInfoData = processArrayToString(purchReturnInfo);
        salesmanDesignatedCitiesData = processArrayToString(salesmanDesignatedCities);
        saleInvoicePrintInfoData = processArrayToString(saleInvoicePrintInfo);
        supplierCompanyData = processArrayToString(supplierCompany);
        supplierInfoData = processArrayToString(supplierInfo);
        syncTrackingData = processArrayToString(syncTracking);
        systemSettingsData = processArrayToString(systemSettings);
        userAccountsData = processArrayToString(userAccounts);
        userInfoData = processArrayToString(userInfo);
        userRightsData = processArrayToString(userRights);

        Statement s = objCon.createStatement();

        if(areaInfo.size() > 0)
        {
            response = sendAreaData();
            if(response.equals("Unable to Connect To Server"))
            {

            }
            else
            {
                firstIter = true;
                String[] responseArr = response.split(":");
                if(responseArr.length == 4)
                {
                    if(responseArr[1].equals("true"))
                    {
                        // Insert Queries Success
                        for(int i=0; i<areaInfo.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(areaInfo.get(i).get(areaInfo.get(i).size()-1).equals("Insert") || areaInfo.get(i).get(areaInfo.get(i).size()-1).equals("0") || areaInfo.get(i).get(areaInfo.get(i).size()-1).equals(""))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `area_info` SET `server_sync`= 'Done' WHERE `area_table_id` = '"+areaInfo.get(i).get(0)+"';";
                                s.addBatch(updateQuery);
                                query += updateQuery;
                            }
                        }
                    } // Set Both These Differently So that make sure that Insert and Update Query executed on PHP Server without any error.
                    if(responseArr[3].equals("true"))
                    {
                        // Update Queries Success
                        for(int i=0; i<areaInfo.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(areaInfo.get(i).get(areaInfo.get(i).size()-1).equals("Update"))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `area_info` SET `server_sync`= 'Done' WHERE `area_table_id` = '"+areaInfo.get(i).get(0)+"';";
                                s.addBatch(updateQuery);
                                query += updateQuery;
                            }
                        }
                    }
                    

                }
            }
        }

        if(batchwiseStock.size() > 0)
        {
            response = sendBatchwiseStockData();
            if(response.equals("Unable to Connect To Server"))
            {

            }
            else
            {
                firstIter = true;
                String[] responseArr = response.split(":");
                if(responseArr.length == 4)
                {
                    if(responseArr[1].equals("true"))
                    {
                        // Insert Queries Success
                        for(int i=0; i<batchwiseStock.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(batchwiseStock.get(i).get(batchwiseStock.get(i).size()-1).equals("Insert") || batchwiseStock.get(i).get(batchwiseStock.get(i).size()-1).equals("0") || batchwiseStock.get(i).get(batchwiseStock.get(i).size()-1).equals(""))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `batchwise_stock` SET `server_sync`= 'Done' WHERE `batch_stock_id` = '"+batchwiseStock.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    } // Set Both These Differently So that make sure that Insert and Update Query executed on PHP Server without any error.
                    if(responseArr[3].equals("true"))
                    {
                        // Update Queries Success
                        for(int i=0; i<batchwiseStock.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(batchwiseStock.get(i).get(batchwiseStock.get(i).size()-1).equals("Update"))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `batchwise_stock` SET `server_sync`= 'Done' WHERE `batch_stock_id` = '"+batchwiseStock.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    }
                    

                }
            }
        }

        if(bonusPolicy.size() > 0)
        {
            response = sendBonusPolicyData();
            if(response.equals("Unable to Connect To Server"))
            {

            }
            else
            {
                firstIter = true;
                String[] responseArr = response.split(":");
                if(responseArr.length == 4)
                {
                    
                    if(responseArr[1].equals("true"))
                    {
                        // Insert Queries Success
                        for(int i=0; i<bonusPolicy.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(bonusPolicy.get(i).get(bonusPolicy.get(i).size()-1).equals("Insert") || bonusPolicy.get(i).get(bonusPolicy.get(i).size()-1).equals("0") || bonusPolicy.get(i).get(bonusPolicy.get(i).size()-1).equals(""))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `bonus_policy` SET `server_sync` = 'Done' WHERE `bonus_policyID` = '"+bonusPolicy.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    } // Set Both These Differently So that make sure that Insert and Update Query executed on PHP Server without any error.
                    if(responseArr[3].equals("true"))
                    {
                        // Update Queries Success
                        for(int i=0; i<bonusPolicy.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(bonusPolicy.get(i).get(bonusPolicy.get(i).size()-1).equals("Update"))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `bonus_policy` SET `server_sync` = 'Done' WHERE `bonus_policyID` = '"+bonusPolicy.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    }
                    

                }
            }
        }

        if(cityInfo.size() > 0)
        {
            response = sendCityInfoData();
            if(response.equals("Unable to Connect To Server"))
            {

            }
            else
            {
                firstIter = true;
                String[] responseArr = response.split(":");
                if(responseArr.length == 4)
                { 
                    if(responseArr[1].equals("true"))
                    {
                        // Insert Queries Success
                        for(int i=0; i<cityInfo.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(cityInfo.get(i).get(cityInfo.get(i).size()-1).equals("Insert") || cityInfo.get(i).get(cityInfo.get(i).size()-1).equals("0") || cityInfo.get(i).get(cityInfo.get(i).size()-1).equals(""))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `city_info` SET `server_sync` = 'Done' WHERE `city_table_id` = '"+cityInfo.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    } // Set Both These Differently So that make sure that Insert and Update Query executed on PHP Server without any error.
                    if(responseArr[3].equals("true"))
                    {
                        // Update Queries Success
                        for(int i=0; i<cityInfo.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(cityInfo.get(i).get(cityInfo.get(i).size()-1).equals("Update"))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `city_info` SET `server_sync` = 'Done' WHERE `city_table_id` = '"+cityInfo.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    }
                    

                }
            }
        }

        if(companyAlternateContacts.size() > 0)
        {
            response = sendCompanyAlternateContactsData();
            if(response.equals("Unable to Connect To Server"))
            {

            }
            else
            {
                firstIter = true;
                String[] responseArr = response.split(":");
                if(responseArr.length == 4)
                { 
                    if(responseArr[1].equals("true"))
                    {
                        // Insert Queries Success
                        for(int i=0; i<companyAlternateContacts.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(companyAlternateContacts.get(i).get(companyAlternateContacts.get(i).size()-1).equals("Insert") || companyAlternateContacts.get(i).get(companyAlternateContacts.get(i).size()-1).equals("0") || companyAlternateContacts.get(i).get(companyAlternateContacts.get(i).size()-1).equals(""))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `company_alternate_contacts` SET `server_sync`= 'Done' WHERE `contact_id` = '"+companyAlternateContacts.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    } // Set Both These Differently So that make sure that Insert and Update Query executed on PHP Server without any error.
                    if(responseArr[3].equals("true"))
                    {
                        // Update Queries Success
                        for(int i=0; i<companyAlternateContacts.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(companyAlternateContacts.get(i).get(companyAlternateContacts.get(i).size()-1).equals("Update"))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `company_alternate_contacts` SET `server_sync`= 'Done' WHERE `contact_id` = '"+companyAlternateContacts.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    }
                    

                }
            }
        }

        if(companyInfo.size() > 0)
        {
            String companyInfoResponse = sendCompanyInfoData();
            if(companyInfoResponse.equals("Unable to Connect To Server"))
            {

            }
            else
            {
                firstIter = true;
                String[] companyInfoResponseArr = companyInfoResponse.split(":");
                if(companyInfoResponseArr.length == 4)
                { 
                    if(companyInfoResponseArr[1].equals("true"))
                    {
                        // Insert Queries Success
                        for(int i=0; i<companyInfo.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(companyInfo.get(i).get(companyInfo.get(i).size()-1).equals("Insert") || companyInfo.get(i).get(companyInfo.get(i).size()-1).equals("0") || companyInfo.get(i).get(companyInfo.get(i).size()-1).equals(""))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `company_info` SET `server_sync` = 'Done' WHERE `company_table_id` = '"+companyInfo.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    } // Set Both These Differently So that make sure that Insert and Update Query executed on PHP Server without any error.
                    if(companyInfoResponseArr[3].equals("true"))
                    {
                        // Update Queries Success
                        for(int i=0; i<companyInfo.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(companyInfo.get(i).get(companyInfo.get(i).size()-1).equals("Update"))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `company_info` SET `server_sync` = 'Done' WHERE `company_table_id` = '"+companyInfo.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    }
//                    if(!query.equals(""))
//                    {
//                        
//                        s.executeBatch();
//                    }
                }
            }
        }

        if(companyOverallRecord.size() > 0)
        {
            String companyOverallRecordResponse = sendCompanyOverallRecordData();
            if(companyOverallRecordResponse.equals("Unable to Connect To Server"))
            {

            }
            else
            {
                firstIter = true;
                String[] companyOverallRecordResponseArr = companyOverallRecordResponse.split(":");
                if(companyOverallRecordResponseArr.length == 4)
                {
                    
                    if(companyOverallRecordResponseArr[1].equals("true"))
                    {
                        // Insert Queries Success
                        for(int i=0; i<companyOverallRecord.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(companyOverallRecord.get(i).get(companyOverallRecord.get(i).size()-1).equals("Insert") || companyOverallRecord.get(i).get(companyOverallRecord.get(i).size()-1).equals("0") || companyOverallRecord.get(i).get(companyOverallRecord.get(i).size()-1).equals(""))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `company_overall_record` SET `server_sync`= 'Done' WHERE `company_overall_id` = '"+companyOverallRecord.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    } // Set Both These Differently So that make sure that Insert and Update Query executed on PHP Server without any error.
                    if(companyOverallRecordResponseArr[3].equals("true"))
                    {
                        // Update Queries Success
                        for(int i=0; i<companyOverallRecord.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(companyOverallRecord.get(i).get(companyOverallRecord.get(i).size()-1).equals("Update"))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `company_overall_record` SET `server_sync`= 'Done' WHERE `company_overall_id` = '"+companyOverallRecord.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    }
                    
//                    if(!query.equals(""))
//                    {
//                       s.executeBatch();
//                    }
                }
            }
        }

        if(companyPayments.size() > 0)
        {
            response = sendCompanyPaymentsData();
            if(response.equals("Unable to Connect To Server"))
            {

            }
            else
            {
                firstIter = true;
                String[] responseArr = response.split(":");
                if(responseArr.length == 4)
                { 
                    if(responseArr[1].equals("true"))
                    {
                        // Insert Queries Success
                        for(int i=0; i<companyPayments.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(companyPayments.get(i).get(companyPayments.get(i).size()-1).equals("Insert") || companyPayments.get(i).get(companyPayments.get(i).size()-1).equals("0") || companyPayments.get(i).get(companyPayments.get(i).size()-1).equals(""))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `company_payments` SET `server_sync`= 'Done' WHERE `payment_id` = '"+companyPayments.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    } // Set Both These Differently So that make sure that Insert and Update Query executed on PHP Server without any error.
                    if(responseArr[3].equals("true"))
                    {
                        // Update Queries Success
                        for(int i=0; i<companyPayments.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(companyPayments.get(i).get(companyPayments.get(i).size()-1).equals("Update"))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `company_payments` SET `server_sync`= 'Done' WHERE `payment_id` = '"+companyPayments.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    }
                    

                }
            }
        }

        if(dealerDaySummary.size() > 0)
        {
            response = sendDealerDaySummaryData();
            if(response.equals("Unable to Connect To Server"))
            {

            }
            else
            {
                firstIter = true;
                String[] responseArr = response.split(":");
                if(responseArr.length == 4)
                {
                    
                    if(responseArr[1].equals("true"))
                    {
                        // Insert Queries Success
                        for(int i=0; i<dealerDaySummary.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(dealerDaySummary.get(i).get(dealerDaySummary.get(i).size()-1).equals("Insert") || dealerDaySummary.get(i).get(dealerDaySummary.get(i).size()-1).equals("0") || dealerDaySummary.get(i).get(dealerDaySummary.get(i).size()-1).equals(""))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `dealer_day_summary` SET `server_sync`= 'Done' WHERE `dealer_summary_id` = '"+dealerDaySummary.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    } // Set Both These Differently So that make sure that Insert and Update Query executed on PHP Server without any error.
                    if(responseArr[3].equals("true"))
                    {
                        // Update Queries Success
                        for(int i=0; i<dealerDaySummary.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(dealerDaySummary.get(i).get(dealerDaySummary.get(i).size()-1).equals("Update"))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `dealer_day_summary` SET `server_sync`= 'Done' WHERE `dealer_summary_id` = '"+dealerDaySummary.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    }
                    

                }
            }
        }

        if(dealerGPSLocation.size() > 0)
        {
            response = sendDealerGPSLocationData();
            if(response.equals("Unable to Connect To Server"))
            {

            }
            else
            {
                firstIter = true;
                String[] responseArr = response.split(":");
                if(responseArr.length == 4)
                { 
                    if(responseArr[1].equals("true"))
                    {
                        // Insert Queries Success
                        for(int i=0; i<dealerGPSLocation.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(dealerGPSLocation.get(i).get(dealerGPSLocation.get(i).size()-1).equals("Insert") || dealerGPSLocation.get(i).get(dealerGPSLocation.get(i).size()-1).equals("0") || dealerGPSLocation.get(i).get(dealerGPSLocation.get(i).size()-1).equals(""))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `dealer_gps_location` SET `server_sync`= 'Done' WHERE `dealer_loc_id` = '"+dealerGPSLocation.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    } // Set Both These Differently So that make sure that Insert and Update Query executed on PHP Server without any error.
                    if(responseArr[3].equals("true"))
                    {
                        // Update Queries Success
                        for(int i=0; i<dealerGPSLocation.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(dealerGPSLocation.get(i).get(dealerGPSLocation.get(i).size()-1).equals("Update"))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `dealer_gps_location` SET `server_sync`= 'Done' WHERE `dealer_loc_id` = '"+dealerGPSLocation.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    }
                    

                }
            }
        }

        if(dealerInfo.size() > 0)
        {
            response = sendDealerInfoData();
            if(response.equals("Unable to Connect To Server"))
            {

            }
            else
            {
                firstIter = true;
                String[] responseArr = response.split(":");
                if(responseArr.length == 4)
                { 
                    if(responseArr[1].equals("true"))
                    {
                        // Insert Queries Success
                        for(int i=0; i<dealerInfo.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(dealerInfo.get(i).get(dealerInfo.get(i).size()-1).equals("Insert") || dealerInfo.get(i).get(dealerInfo.get(i).size()-1).equals("0") || dealerInfo.get(i).get(dealerInfo.get(i).size()-1).equals(""))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `dealer_info` SET `server_sync`= 'Done' WHERE `dealer_table_id` = '"+dealerInfo.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    } // Set Both These Differently So that make sure that Insert and Update Query executed on PHP Server without any error.
                    if(responseArr[3].equals("true"))
                    {
                        // Update Queries Success
                        for(int i=0; i<dealerInfo.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(dealerInfo.get(i).get(dealerInfo.get(i).size()-1).equals("Update"))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `dealer_info` SET `server_sync`= 'Done' WHERE `dealer_table_id` = '"+dealerInfo.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    }
                    

                }
            }
        }

        if(dealerOverallRecord.size() > 0)
        {
            response = sendDealerOverallRecordData();
            if(response.equals("Unable to Connect To Server"))
            {

            }
            else
            {
                firstIter = true;
                String[] responseArr = response.split(":");
                if(responseArr.length == 4)
                { 
                    if(responseArr[1].equals("true"))
                    {
                        // Insert Queries Success
                        for(int i=0; i<dealerOverallRecord.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(dealerOverallRecord.get(i).get(dealerOverallRecord.get(i).size()-1).equals("Insert") || dealerOverallRecord.get(i).get(dealerOverallRecord.get(i).size()-1).equals("0") || dealerOverallRecord.get(i).get(dealerOverallRecord.get(i).size()-1).equals(""))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `dealer_overall_record` SET `server_sync`= 'Done' WHERE `dealer_overall_id` = '"+dealerOverallRecord.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    } // Set Both These Differently So that make sure that Insert and Update Query executed on PHP Server without any error.
                    if(responseArr[3].equals("true"))
                    {
                        // Update Queries Success
                        for(int i=0; i<dealerOverallRecord.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(dealerOverallRecord.get(i).get(dealerOverallRecord.get(i).size()-1).equals("Update"))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `dealer_overall_record` SET `server_sync`= 'Done' WHERE `dealer_overall_id` = '"+dealerOverallRecord.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    }
                    

                }
            }
        }

        if(dealerPayments.size() > 0)
        {
            response = sendDealerPaymentsData();
            if(response.equals("Unable to Connect To Server"))
            {

            }
            else
            {
                firstIter = true;
                String[] responseArr = response.split(":");
                if(responseArr.length == 4)
                { 
                    if(responseArr[1].equals("true"))
                    {
                        // Insert Queries Success
                        for(int i=0; i<dealerPayments.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(dealerPayments.get(i).get(dealerPayments.get(i).size()-1).equals("Insert") || dealerPayments.get(i).get(dealerPayments.get(i).size()-1).equals("0") || dealerPayments.get(i).get(dealerPayments.get(i).size()-1).equals(""))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `dealer_payments` SET `server_sync`= 'Done' WHERE `payment_id` = '"+dealerPayments.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    } // Set Both These Differently So that make sure that Insert and Update Query executed on PHP Server without any error.
                    if(responseArr[3].equals("true"))
                    {
                        // Update Queries Success
                        for(int i=0; i<dealerPayments.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(dealerPayments.get(i).get(dealerPayments.get(i).size()-1).equals("Update"))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `dealer_payments` SET `server_sync`= 'Done' WHERE `payment_id` = '"+dealerPayments.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    }
                    

                }
            }
        }

        if(dealerTypes.size() > 0)
        {
            response = sendDealerTypesData();
            if(response.equals("Unable to Connect To Server"))
            {

            }
            else
            {
                firstIter = true;
                String[] responseArr = response.split(":");
                if(responseArr.length == 4)
                { 
                    if(responseArr[1].equals("true"))
                    {
                        // Insert Queries Success
                        for(int i=0; i<dealerTypes.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(dealerTypes.get(i).get(dealerTypes.get(i).size()-1).equals("Insert") || dealerTypes.get(i).get(dealerTypes.get(i).size()-1).equals("0") || dealerTypes.get(i).get(dealerTypes.get(i).size()-1).equals(""))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `dealer_types` SET `server_sync`= 'Done' WHERE `dealer_type_ID` = '"+dealerTypes.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    } // Set Both These Differently So that make sure that Insert and Update Query executed on PHP Server without any error.
                    if(responseArr[3].equals("true"))
                    {
                        // Update Queries Success
                        for(int i=0; i<dealerTypes.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(dealerTypes.get(i).get(dealerTypes.get(i).size()-1).equals("Update"))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `dealer_types` SET `server_sync`= 'Done' WHERE `dealer_type_ID` = '"+dealerTypes.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    }
                    

                }
            }
        }

        if(discountPolicy.size() > 0)
        {
            response = sendDiscountPolicyData();
            if(response.equals("Unable to Connect To Server"))
            {

            }
            else
            {
                firstIter = true;
                String[] responseArr = response.split(":");
                if(responseArr.length == 4)
                { 
                    if(responseArr[1].equals("true"))
                    {
                        // Insert Queries Success
                        for(int i=0; i<discountPolicy.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(discountPolicy.get(i).get(discountPolicy.get(i).size()-1).equals("Insert") || discountPolicy.get(i).get(discountPolicy.get(i).size()-1).equals("0") || discountPolicy.get(i).get(discountPolicy.get(i).size()-1).equals(""))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `discount_policy` SET `server_sync`= 'Done' WHERE `disc_policyID` = '"+discountPolicy.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    } // Set Both These Differently So that make sure that Insert and Update Query executed on PHP Server without any error.
                    if(responseArr[3].equals("true"))
                    {
                        // Update Queries Success
                        for(int i=0; i<discountPolicy.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(discountPolicy.get(i).get(discountPolicy.get(i).size()-1).equals("Update"))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `discount_policy` SET `server_sync`= 'Done' WHERE `disc_policyID` = '"+discountPolicy.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    }
                    

                }
            }
        }

        if(districtInfo.size() > 0)
        {
            response = sendDistrictInfoData();
            if(response.equals("Unable to Connect To Server"))
            {

            }
            else
            {
                firstIter = true;
                String[] responseArr = response.split(":");
                if(responseArr.length == 4)
                { 
                    if(responseArr[1].equals("true"))
                    {
                        // Insert Queries Success
                        for(int i=0; i<districtInfo.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(districtInfo.get(i).get(districtInfo.get(i).size()-1).equals("Insert") || districtInfo.get(i).get(districtInfo.get(i).size()-1).equals("0") || districtInfo.get(i).get(districtInfo.get(i).size()-1).equals(""))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `district_info` SET `server_sync`= 'Done' WHERE `district_table_id` = '"+districtInfo.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    } // Set Both These Differently So that make sure that Insert and Update Query executed on PHP Server without any error.
                    if(responseArr[3].equals("true"))
                    {
                        // Update Queries Success
                        for(int i=0; i<districtInfo.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(districtInfo.get(i).get(districtInfo.get(i).size()-1).equals("Update"))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `district_info` SET `server_sync`= 'Done' WHERE `district_table_id` = '"+districtInfo.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    }
                    

                }
            }
        }

        if(groupsInfo.size() > 0)
        {
            response = sendGroupsInfoData();
            if(response.equals("Unable to Connect To Server"))
            {

            }
            else
            {
                firstIter = true;
                String[] responseArr = response.split(":");
                if(responseArr.length == 4)
                { 
                    if(responseArr[1].equals("true"))
                    {
                        // Insert Queries Success
                        for(int i=0; i<groupsInfo.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(groupsInfo.get(i).get(groupsInfo.get(i).size()-1).equals("Insert") || groupsInfo.get(i).get(groupsInfo.get(i).size()-1).equals("0") || groupsInfo.get(i).get(groupsInfo.get(i).size()-1).equals(""))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `groups_info` SET `server_sync`= 'Done' WHERE `group_table_id` = '"+groupsInfo.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    } // Set Both These Differently So that make sure that Insert and Update Query executed on PHP Server without any error.
                    if(responseArr[3].equals("true"))
                    {
                        // Update Queries Success
                        for(int i=0; i<groupsInfo.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(groupsInfo.get(i).get(groupsInfo.get(i).size()-1).equals("Update"))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `groups_info` SET `server_sync`= 'Done' WHERE `group_table_id` = '"+groupsInfo.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    }
                    

                }
            }
        }

        if(mailsRecord.size() > 0)
        {
            response = sendMailsRecordData();
            if(response.equals("Unable to Connect To Server"))
            {

            }
            else
            {
                firstIter = true;
                String[] responseArr = response.split(":");
                if(responseArr.length == 4)
                {
                    if(responseArr[1].equals("true"))
                    {
                        // Insert Queries Success
                        for(int i=0; i<mailsRecord.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(mailsRecord.get(i).get(mailsRecord.get(i).size()-1).equals("Insert") || mailsRecord.get(i).get(mailsRecord.get(i).size()-1).equals("0") || mailsRecord.get(i).get(mailsRecord.get(i).size()-1).equals(""))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `mails_record` SET `server_sync`= 'Done' WHERE `mail_id` = '"+mailsRecord.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    } // Set Both These Differently So that make sure that Insert and Update Query executed on PHP Server without any error.
                    if(responseArr[3].equals("true"))
                    {
                        // Update Queries Success
                        for(int i=0; i<mailsRecord.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(mailsRecord.get(i).get(mailsRecord.get(i).size()-1).equals("Update"))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `mails_record` SET `server_sync`= 'Done' WHERE `mail_id` = '"+mailsRecord.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    }


                }
            }
        }

        if(mobileGPSLocation.size() > 0)
        {
            response = sendMobileGPSLocationData();
            if(response.equals("Unable to Connect To Server"))
            {

            }
            else
            {
                firstIter = true;
                String[] responseArr = response.split(":");
                if(responseArr.length == 4)
                { 
                    if(responseArr[1].equals("true"))
                    {
                        // Insert Queries Success
                        for(int i=0; i<mobileGPSLocation.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(mobileGPSLocation.get(i).get(mobileGPSLocation.get(i).size()-1).equals("Insert") || mobileGPSLocation.get(i).get(mobileGPSLocation.get(i).size()-1).equals("0") || mobileGPSLocation.get(i).get(mobileGPSLocation.get(i).size()-1).equals(""))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `mobile_gps_location` SET `server_sync`= 'Done' WHERE `mob_loc_id` = '"+mobileGPSLocation.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    } // Set Both These Differently So that make sure that Insert and Update Query executed on PHP Server without any error.
                    if(responseArr[3].equals("true"))
                    {
                        // Update Queries Success
                        for(int i=0; i<mobileGPSLocation.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(mobileGPSLocation.get(i).get(mobileGPSLocation.get(i).size()-1).equals("Update"))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `mobile_gps_location` SET `server_sync`= 'Done' WHERE `mob_loc_id` = '"+mobileGPSLocation.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    }
                    

                }
            }
        }

        if(orderGPSLocation.size() > 0)
        {
            response = sendOrderGPSLocationData();
            if(response.equals("Unable to Connect To Server"))
            {

            }
            else
            {
                firstIter = true;
                String[] responseArr = response.split(":");
                if(responseArr.length == 4)
                { 
                    if(responseArr[1].equals("true"))
                    {
                        // Insert Queries Success
                        for(int i=0; i<orderGPSLocation.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(orderGPSLocation.get(i).get(orderGPSLocation.get(i).size()-1).equals("Insert") || orderGPSLocation.get(i).get(orderGPSLocation.get(i).size()-1).equals("0") || orderGPSLocation.get(i).get(orderGPSLocation.get(i).size()-1).equals(""))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `order_gps_location` SET `server_sync`= 'Done' WHERE `order_loc_id` = '"+orderGPSLocation.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    } // Set Both These Differently So that make sure that Insert and Update Query executed on PHP Server without any error.
                    if(responseArr[3].equals("true"))
                    {
                        // Update Queries Success
                        for(int i=0; i<orderGPSLocation.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(orderGPSLocation.get(i).get(orderGPSLocation.get(i).size()-1).equals("Update"))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `order_gps_location` SET `server_sync`= 'Done' WHERE `order_loc_id` = '"+orderGPSLocation.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    }
                    

                }
            }
        }

        if(orderInfo.size() > 0)
        {
            response = sendOrderInfoData();
            if(response.equals("Unable to Connect To Server"))
            {

            }
            else
            {
                firstIter = true;
                String[] responseArr = response.split(":");
                if(responseArr.length == 4)
                { 
                    if(responseArr[1].equals("true"))
                    {
                        // Insert Queries Success
                        for(int i=0; i<orderInfo.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(orderInfo.get(i).get(orderInfo.get(i).size()-1).equals("Insert") || orderInfo.get(i).get(orderInfo.get(i).size()-1).equals("0") || orderInfo.get(i).get(orderInfo.get(i).size()-1).equals(""))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `order_info` SET `server_sync`= 'Done' WHERE `order_id` = '"+orderInfo.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    } // Set Both These Differently So that make sure that Insert and Update Query executed on PHP Server without any error.
                    if(responseArr[3].equals("true"))
                    {
                        // Update Queries Success
                        for(int i=0; i<orderInfo.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(orderInfo.get(i).get(orderInfo.get(i).size()-1).equals("Update"))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `order_info` SET `server_sync`= 'Done' WHERE `order_id` = '"+orderInfo.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    }
                    

                }
            }
        }

        if(orderInfoDetailed.size() > 0)
        {
            response = sendOrderInfoDetailedData();
            if(response.equals("Unable to Connect To Server"))
            {

            }
            else
            {
                firstIter = true;
                String[] responseArr = response.split(":");
                if(responseArr.length == 4)
                { 
                    if(responseArr[1].equals("true"))
                    {
                        // Insert Queries Success
                        for(int i=0; i<orderInfoDetailed.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(orderInfoDetailed.get(i).get(orderInfoDetailed.get(i).size()-1).equals("Insert") || orderInfoDetailed.get(i).get(orderInfoDetailed.get(i).size()-1).equals("0") || orderInfoDetailed.get(i).get(orderInfoDetailed.get(i).size()-1).equals(""))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `order_info_detailed` SET `server_sync`= 'Done' WHERE `detail_id` = '"+orderInfoDetailed.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    } // Set Both These Differently So that make sure that Insert and Update Query executed on PHP Server without any error.
                    if(responseArr[3].equals("true"))
                    {
                        // Update Queries Success
                        for(int i=0; i<orderInfoDetailed.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(orderInfoDetailed.get(i).get(orderInfoDetailed.get(i).size()-1).equals("Update"))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `order_info_detailed` SET `server_sync`= 'Done' WHERE `detail_id` = '"+orderInfoDetailed.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    }
                    

                }
            }
        }

        if(orderReturn.size() > 0)
        {
            response = sendOrderReturnData();
            if(response.equals("Unable to Connect To Server"))
            {

            }
            else
            {
                firstIter = true;
                String[] responseArr = response.split(":");
                if(responseArr.length == 4)
                { 
                    if(responseArr[1].equals("true"))
                    {
                        // Insert Queries Success
                        for(int i=0; i<orderReturn.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(orderReturn.get(i).get(orderReturn.get(i).size()-1).equals("Insert") || orderReturn.get(i).get(orderReturn.get(i).size()-1).equals("0") || orderReturn.get(i).get(orderReturn.get(i).size()-1).equals(""))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `order_return` SET `server_sync`= 'Done' WHERE `return_id` = '"+orderReturn.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    } // Set Both These Differently So that make sure that Insert and Update Query executed on PHP Server without any error.
                    if(responseArr[3].equals("true"))
                    {
                        // Update Queries Success
                        for(int i=0; i<orderReturn.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(orderReturn.get(i).get(orderReturn.get(i).size()-1).equals("Update"))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `order_return` SET `server_sync`= 'Done' WHERE `return_id` = '"+orderReturn.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    }
                    

                }
            }
        }

        if(orderReturnDetail.size() > 0)
        {
            response = sendOrderReturnDetailData();
            if(response.equals("Unable to Connect To Server"))
            {

            }
            else
            {
                firstIter = true;
                String[] responseArr = response.split(":");
                if(responseArr.length == 4)
                { 
                    if(responseArr[1].equals("true"))
                    {
                        // Insert Queries Success
                        for(int i=0; i<orderReturnDetail.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(orderReturnDetail.get(i).get(orderReturnDetail.get(i).size()-1).equals("Insert") || orderReturnDetail.get(i).get(orderReturnDetail.get(i).size()-1).equals("0") || orderReturnDetail.get(i).get(orderReturnDetail.get(i).size()-1).equals(""))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `order_return_detail` SET `server_sync`= 'Done' WHERE `return_detail_id` = '"+orderReturnDetail.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    } // Set Both These Differently So that make sure that Insert and Update Query executed on PHP Server without any error.
                    if(responseArr[3].equals("true"))
                    {
                        // Update Queries Success
                        for(int i=0; i<orderReturnDetail.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(orderReturnDetail.get(i).get(orderReturnDetail.get(i).size()-1).equals("Update"))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `order_return_detail` SET `server_sync`= 'Done' WHERE `return_detail_id` = '"+orderReturnDetail.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    }
                    

                }
            }
        }

        if(orderStatusInfo.size() > 0)
        {
            response = sendOrderStatusInfoData();
            if(response.equals("Unable to Connect To Server"))
            {

            }
            else
            {
                firstIter = true;
                String[] responseArr = response.split(":");
                if(responseArr.length == 4)
                { 
                    if(responseArr[1].equals("true"))
                    {
                        // Insert Queries Success
                        for(int i=0; i<orderStatusInfo.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(orderStatusInfo.get(i).get(orderStatusInfo.get(i).size()-1).equals("Insert") || orderStatusInfo.get(i).get(orderStatusInfo.get(i).size()-1).equals("0") || orderStatusInfo.get(i).get(orderStatusInfo.get(i).size()-1).equals(""))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `order_status_info` SET `server_sync`= 'Done' WHERE `order_status_id` = '"+orderStatusInfo.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    } // Set Both These Differently So that make sure that Insert and Update Query executed on PHP Server without any error.
                    if(responseArr[3].equals("true"))
                    {
                        // Update Queries Success
                        for(int i=0; i<orderStatusInfo.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(orderStatusInfo.get(i).get(orderStatusInfo.get(i).size()-1).equals("Update"))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `order_status_info` SET `server_sync`= 'Done' WHERE `order_status_id` = '"+orderStatusInfo.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    }
                    

                }
            }
        }

        if(productsStock.size() > 0)
        {
            response = sendProductsStockData();
            if(response.equals("Unable to Connect To Server"))
            {

            }
            else
            {
                firstIter = true;
                String[] responseArr = response.split(":");
                if(responseArr.length == 4)
                { 
                    if(responseArr[1].equals("true"))
                    {
                        // Insert Queries Success
                        for(int i=0; i<productsStock.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(productsStock.get(i).get(productsStock.get(i).size()-1).equals("Insert") || productsStock.get(i).get(productsStock.get(i).size()-1).equals("0") || productsStock.get(i).get(productsStock.get(i).size()-1).equals(""))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `products_stock` SET `server_sync`= 'Done' WHERE `ProdStockID` = '"+productsStock.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    } // Set Both These Differently So that make sure that Insert and Update Query executed on PHP Server without any error.
                    if(responseArr[3].equals("true"))
                    {
                        // Update Queries Success
                        for(int i=0; i<productsStock.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(productsStock.get(i).get(productsStock.get(i).size()-1).equals("Update"))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `products_stock` SET `server_sync`= 'Done' WHERE `ProdStockID` = '"+productsStock.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    }
                    

                }
            }
        }

        if(productInfo.size() > 0)
        {
            response = sendProductInfoData();
            if(response.equals("Unable to Connect To Server"))
            {

            }
            else
            {
                firstIter = true;
                String[] responseArr = response.split(":");
                if(responseArr.length == 4)
                { 
                    if(responseArr[1].equals("true"))
                    {
                        // Insert Queries Success
                        for(int i=0; i<productInfo.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(productInfo.get(i).get(productInfo.get(i).size()-1).equals("Insert") || productInfo.get(i).get(productInfo.get(i).size()-1).equals("0") || productInfo.get(i).get(productInfo.get(i).size()-1).equals(""))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `product_info` SET `server_sync`= 'Done' WHERE `product_table_id` = '"+productInfo.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    } // Set Both These Differently So that make sure that Insert and Update Query executed on PHP Server without any error.
                    if(responseArr[3].equals("true"))
                    {
                        // Update Queries Success
                        for(int i=0; i<productInfo.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(productInfo.get(i).get(productInfo.get(i).size()-1).equals("Update"))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `product_info` SET `server_sync`= 'Done' WHERE `product_table_id` = '"+productInfo.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    }
                    

                }
            }
        }

        if(productOverallRecord.size() > 0)
        {
            response = sendProductOverallRecordData();
            if(response.equals("Unable to Connect To Server"))
            {

            }
            else
            {
                firstIter = true;
                String[] responseArr = response.split(":");
                if(responseArr.length == 4)
                { 
                    if(responseArr[1].equals("true"))
                    {
                        // Insert Queries Success
                        for(int i=0; i<productOverallRecord.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(productOverallRecord.get(i).get(productOverallRecord.get(i).size()-1).equals("Insert") || productOverallRecord.get(i).get(productOverallRecord.get(i).size()-1).equals("0") || productOverallRecord.get(i).get(productOverallRecord.get(i).size()-1).equals(""))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `product_overall_record` SET `server_sync`= 'Done' WHERE `overall_record_id` = '"+productOverallRecord.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    } // Set Both These Differently So that make sure that Insert and Update Query executed on PHP Server without any error.
                    if(responseArr[3].equals("true"))
                    {
                        // Update Queries Success
                        for(int i=0; i<productOverallRecord.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(productOverallRecord.get(i).get(productOverallRecord.get(i).size()-1).equals("Update"))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `product_overall_record` SET `server_sync`= 'Done' WHERE `overall_record_id` = '"+productOverallRecord.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    }
                    

                }
            }
        }

        if(purchaseInfo.size() > 0)
        {
            response = sendPurchaseInfoData();
            if(response.equals("Unable to Connect To Server"))
            {

            }
            else
            {
                firstIter = true;
                String[] responseArr = response.split(":");
                if(responseArr.length == 4)
                { 
                    if(responseArr[1].equals("true"))
                    {
                        // Insert Queries Success
                        for(int i=0; i<purchaseInfo.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(purchaseInfo.get(i).get(purchaseInfo.get(i).size()-1).equals("Insert") || purchaseInfo.get(i).get(purchaseInfo.get(i).size()-1).equals("0") || purchaseInfo.get(i).get(purchaseInfo.get(i).size()-1).equals(""))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `purchase_info` SET `server_sync`= 'Done' WHERE `purchase_id` = '"+purchaseInfo.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    } // Set Both These Differently So that make sure that Insert and Update Query executed on PHP Server without any error.
                    if(responseArr[3].equals("true"))
                    {
                        // Update Queries Success
                        for(int i=0; i<purchaseInfo.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(purchaseInfo.get(i).get(purchaseInfo.get(i).size()-1).equals("Update"))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `purchase_info` SET `server_sync`= 'Done' WHERE `purchase_id` = '"+purchaseInfo.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    }
                    

                }
            }
        }

        if(purchaseInfoDetail.size() > 0)
        {
            response = sendPurchaseInfoDetailData();
            if(response.equals("Unable to Connect To Server"))
            {

            }
            else
            {
                firstIter = true;
                String[] responseArr = response.split(":");
                if(responseArr.length == 4)
                { 
                    if(responseArr[1].equals("true"))
                    {
                        // Insert Queries Success
                        for(int i=0; i<purchaseInfoDetail.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(purchaseInfoDetail.get(i).get(purchaseInfoDetail.get(i).size()-1).equals("Insert") || purchaseInfoDetail.get(i).get(purchaseInfoDetail.get(i).size()-1).equals("0") || purchaseInfoDetail.get(i).get(purchaseInfoDetail.get(i).size()-1).equals(""))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `purchase_info_detail` SET `server_sync`= 'Done' WHERE `purchase_info_detail_id` = '"+purchaseInfoDetail.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    } // Set Both These Differently So that make sure that Insert and Update Query executed on PHP Server without any error.
                    if(responseArr[3].equals("true"))
                    {
                        // Update Queries Success
                        for(int i=0; i<purchaseInfoDetail.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(purchaseInfoDetail.get(i).get(purchaseInfoDetail.get(i).size()-1).equals("Update"))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `purchase_info_detail` SET `server_sync`= 'Done' WHERE `purchase_info_detail_id` = '"+purchaseInfoDetail.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    }
                    

                }
            }
        }

        if(purchaseOrderDetail.size() > 0)
        {
            response = sendPurchaseOrderDetailData();
            if(response.equals("Unable to Connect To Server"))
            {

            }
            else
            {
                firstIter = true;
                String[] responseArr = response.split(":");
                if(responseArr.length == 4)
                { 
                    if(responseArr[1].equals("true"))
                    {
                        // Insert Queries Success
                        for(int i=0; i<purchaseOrderDetail.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(purchaseOrderDetail.get(i).get(purchaseOrderDetail.get(i).size()-1).equals("Insert") || purchaseOrderDetail.get(i).get(purchaseOrderDetail.get(i).size()-1).equals("0") || purchaseOrderDetail.get(i).get(purchaseOrderDetail.get(i).size()-1).equals(""))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `purchase_order_detail` SET `server_sync`= 'Done' WHERE `order_detail_tableID` = '"+purchaseOrderDetail.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    } // Set Both These Differently So that make sure that Insert and Update Query executed on PHP Server without any error.
                    if(responseArr[3].equals("true"))
                    {
                        // Update Queries Success
                        for(int i=0; i<purchaseOrderDetail.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(purchaseOrderDetail.get(i).get(purchaseOrderDetail.get(i).size()-1).equals("Update"))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `purchase_order_detail` SET `server_sync`= 'Done' WHERE `order_detail_tableID` = '"+purchaseOrderDetail.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    }
                    

                }
            }
        }

        if(purchaseOrderInfo.size() > 0)
        {
            response = sendPurchaseOrderInfoData();
            if(response.equals("Unable to Connect To Server"))
            {

            }
            else
            {
                firstIter = true;
                String[] responseArr = response.split(":");
                if(responseArr.length == 4)
                { 
                    if(responseArr[1].equals("true"))
                    {
                        // Insert Queries Success
                        for(int i=0; i<purchaseOrderInfo.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(purchaseOrderInfo.get(i).get(purchaseOrderInfo.get(i).size()-1).equals("Insert") || purchaseOrderInfo.get(i).get(purchaseOrderInfo.get(i).size()-1).equals("0") || purchaseOrderInfo.get(i).get(purchaseOrderInfo.get(i).size()-1).equals(""))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `purchase_order_info` SET `server_sync`= 'Done' WHERE `purch_order_table_id` = '"+purchaseOrderInfo.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    } // Set Both These Differently So that make sure that Insert and Update Query executed on PHP Server without any error.
                    if(responseArr[3].equals("true"))
                    {
                        // Update Queries Success
                        for(int i=0; i<purchaseOrderInfo.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(purchaseOrderInfo.get(i).get(purchaseOrderInfo.get(i).size()-1).equals("Update"))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `purchase_order_info` SET `server_sync`= 'Done' WHERE `purch_order_table_id` = '"+purchaseOrderInfo.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    }
                    

                }
            }
        }

        if(purchaseReturn.size() > 0)
        {
            response = sendPurchaseReturnData();
            if(response.equals("Unable to Connect To Server"))
            {

            }
            else
            {
                firstIter = true;
                String[] responseArr = response.split(":");
                if(responseArr.length == 4)
                { 
                    if(responseArr[1].equals("true"))
                    {
                        // Insert Queries Success
                        for(int i=0; i<purchaseReturn.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(purchaseReturn.get(i).get(purchaseReturn.get(i).size()-1).equals("Insert") || purchaseReturn.get(i).get(purchaseReturn.get(i).size()-1).equals("0") || purchaseReturn.get(i).get(purchaseReturn.get(i).size()-1).equals(""))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `purchase_return` SET `server_sync`= 'Done' WHERE `returnTable_id` = '"+purchaseReturn.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    } // Set Both These Differently So that make sure that Insert and Update Query executed on PHP Server without any error.
                    if(responseArr[3].equals("true"))
                    {
                        // Update Queries Success
                        for(int i=0; i<purchaseReturn.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(purchaseReturn.get(i).get(purchaseReturn.get(i).size()-1).equals("Update"))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `purchase_return` SET `server_sync`= 'Done' WHERE `returnTable_id` = '"+purchaseReturn.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    }
                    

                }
            }
        }

        if(purchaseReturnDetail.size() > 0)
        {
            response = sendPurchaseReturnDetailData();
            if(response.equals("Unable to Connect To Server"))
            {

            }
            else
            {
                firstIter = true;
                String[] responseArr = response.split(":");
                if(responseArr.length == 4)
                { 
                    if(responseArr[1].equals("true"))
                    {
                        // Insert Queries Success
                        for(int i=0; i<purchaseReturnDetail.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(purchaseReturnDetail.get(i).get(purchaseReturnDetail.get(i).size()-1).equals("Insert") || purchaseReturnDetail.get(i).get(purchaseReturnDetail.get(i).size()-1).equals("0") || purchaseReturnDetail.get(i).get(purchaseReturnDetail.get(i).size()-1).equals(""))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `purchase_return_detail` SET `server_sync`= 'Done' WHERE `return_detail_id` = '"+purchaseReturnDetail.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    } // Set Both These Differently So that make sure that Insert and Update Query executed on PHP Server without any error.
                    if(responseArr[3].equals("true"))
                    {
                        // Update Queries Success
                        for(int i=0; i<purchaseReturnDetail.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(purchaseReturnDetail.get(i).get(purchaseReturnDetail.get(i).size()-1).equals("Update"))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `purchase_return_detail` SET `server_sync`= 'Done' WHERE `return_detail_id` = '"+purchaseReturnDetail.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    }
                    

                }
            }
        }

        if(purchaseReturnInfoDetail.size() > 0)
        {
            response = sendPurchaseReturnInfoDetailData();
            if(response.equals("Unable to Connect To Server"))
            {

            }
            else
            {
                firstIter = true;
                String[] responseArr = response.split(":");
                if(responseArr.length == 4)
                { 
                    if(responseArr[1].equals("true"))
                    {
                        // Insert Queries Success
                        for(int i=0; i<purchaseReturnInfoDetail.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(purchaseReturnInfoDetail.get(i).get(purchaseReturnInfoDetail.get(i).size()-1).equals("Insert") || purchaseReturnInfoDetail.get(i).get(purchaseReturnInfoDetail.get(i).size()-1).equals("0") || purchaseReturnInfoDetail.get(i).get(purchaseReturnInfoDetail.get(i).size()-1).equals(""))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `purchase_return_info_detail` SET `server_sync`= 'Done' WHERE `return_id` = '"+purchaseReturnInfoDetail.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    } // Set Both These Differently So that make sure that Insert and Update Query executed on PHP Server without any error.
                    if(responseArr[3].equals("true"))
                    {
                        // Update Queries Success
                        for(int i=0; i<purchaseReturnInfoDetail.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(purchaseReturnInfoDetail.get(i).get(purchaseReturnInfoDetail.get(i).size()-1).equals("Update"))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `purchase_return_info_detail` SET `server_sync`= 'Done' WHERE `return_id` = '"+purchaseReturnInfoDetail.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    }
                    

                }
            }
        }

        if(purchReturnInfo.size() > 0)
        {
            response = sendPurchReturnInfoData();
            if(response.equals("Unable to Connect To Server"))
            {

            }
            else
            {
                firstIter = true;
                String[] responseArr = response.split(":");
                if(responseArr.length == 4)
                { 
                    if(responseArr[1].equals("true"))
                    {
                        // Insert Queries Success
                        for(int i=0; i<purchReturnInfo.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(purchReturnInfo.get(i).get(purchReturnInfo.get(i).size()-1).equals("Insert") || purchReturnInfo.get(i).get(purchReturnInfo.get(i).size()-1).equals("0") || purchReturnInfo.get(i).get(purchReturnInfo.get(i).size()-1).equals(""))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `purch_return_info` SET `server_sync`= 'Done' WHERE `purch_return_no` = '"+purchReturnInfo.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    } // Set Both These Differently So that make sure that Insert and Update Query executed on PHP Server without any error.
                    if(responseArr[3].equals("true"))
                    {
                        // Update Queries Success
                        for(int i=0; i<purchReturnInfo.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(purchReturnInfo.get(i).get(purchReturnInfo.get(i).size()-1).equals("Update"))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `purch_return_info` SET `server_sync`= 'Done' WHERE `purch_return_no` = '"+purchReturnInfo.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    }
                    

                }
            }
        }

        if(salesmanDesignatedCities.size() > 0)
        {
            response = sendSalesmanDesignatedCitiesData();
            if(response.equals("Unable to Connect To Server"))
            {

            }
            else
            {
                firstIter = true;
                String[] responseArr = response.split(":");
                if(responseArr.length == 4)
                { 
                    if(responseArr[1].equals("true"))
                    {
                        // Insert Queries Success
                        for(int i=0; i<salesmanDesignatedCities.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(salesmanDesignatedCities.get(i).get(salesmanDesignatedCities.get(i).size()-1).equals("Insert") || salesmanDesignatedCities.get(i).get(salesmanDesignatedCities.get(i).size()-1).equals("0") || salesmanDesignatedCities.get(i).get(salesmanDesignatedCities.get(i).size()-1).equals(""))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `salesman_designated_cities` SET `server_sync`= 'Done' WHERE `salesman_designated_id` = '"+salesmanDesignatedCities.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    } // Set Both These Differently So that make sure that Insert and Update Query executed on PHP Server without any error.
                    if(responseArr[3].equals("true"))
                    {
                        // Update Queries Success
                        for(int i=0; i<salesmanDesignatedCities.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(salesmanDesignatedCities.get(i).get(salesmanDesignatedCities.get(i).size()-1).equals("Update"))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `salesman_designated_cities` SET `server_sync`= 'Done' WHERE `salesman_designated_id` = '"+salesmanDesignatedCities.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    }
                    

                }
            }
        }

        if(saleInvoicePrintInfo.size() > 0)
        {
            response = sendSaleInvoicePrintInfoData();
            if(response.equals("Unable to Connect To Server"))
            {

            }
            else
            {
                firstIter = true;
                String[] responseArr = response.split(":");
                if(responseArr.length == 4)
                { 
                    if(responseArr[1].equals("true"))
                    {
                        // Insert Queries Success
                        for(int i=0; i<saleInvoicePrintInfo.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(saleInvoicePrintInfo.get(i).get(saleInvoicePrintInfo.get(i).size()-1).equals("Insert") || saleInvoicePrintInfo.get(i).get(saleInvoicePrintInfo.get(i).size()-1).equals("0") || saleInvoicePrintInfo.get(i).get(saleInvoicePrintInfo.get(i).size()-1).equals(""))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `sale_invoice_print_info` SET `server_sync`= 'Done' WHERE `sale_print_id` = '"+saleInvoicePrintInfo.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    } // Set Both These Differently So that make sure that Insert and Update Query executed on PHP Server without any error.
                    if(responseArr[3].equals("true"))
                    {
                        // Update Queries Success
                        for(int i=0; i<saleInvoicePrintInfo.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(saleInvoicePrintInfo.get(i).get(saleInvoicePrintInfo.get(i).size()-1).equals("Update"))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `sale_invoice_print_info` SET `server_sync`= 'Done' WHERE `sale_print_id` = '"+saleInvoicePrintInfo.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    }
                    

                }
            }
        }

        if(supplierCompany.size() > 0)
        {
            response = sendSupplierCompanyData();
            if(response.equals("Unable to Connect To Server"))
            {

            }
            else
            {
                firstIter = true;
                String[] responseArr = response.split(":");
                if(responseArr.length == 4)
                { 
                    if(responseArr[1].equals("true"))
                    {
                        // Insert Queries Success
                        for(int i=0; i<supplierCompany.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(supplierCompany.get(i).get(supplierCompany.get(i).size()-1).equals("Insert") || supplierCompany.get(i).get(supplierCompany.get(i).size()-1).equals("0") || supplierCompany.get(i).get(supplierCompany.get(i).size()-1).equals(""))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `supplier_company` SET `server_sync`= 'Done' WHERE `supplier_company_id` = '"+supplierCompany.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    } // Set Both These Differently So that make sure that Insert and Update Query executed on PHP Server without any error.
                    if(responseArr[3].equals("true"))
                    {
                        // Update Queries Success
                        for(int i=0; i<supplierCompany.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(supplierCompany.get(i).get(supplierCompany.get(i).size()-1).equals("Update"))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `supplier_company` SET `server_sync`= 'Done' WHERE `supplier_company_id` = '"+supplierCompany.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    }
                    

                }
            }
        }

        if(supplierInfo.size() > 0)
        {
            response = sendSupplierInfoData();
            if(response.equals("Unable to Connect To Server"))
            {

            }
            else
            {
                firstIter = true;
                String[] responseArr = response.split(":");
                if(responseArr.length == 4)
                { 
                    if(responseArr[1].equals("true"))
                    {
                        // Insert Queries Success
                        for(int i=0; i<supplierInfo.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(supplierInfo.get(i).get(supplierInfo.get(i).size()-1).equals("Insert") || supplierInfo.get(i).get(supplierInfo.get(i).size()-1).equals("0") || supplierInfo.get(i).get(supplierInfo.get(i).size()-1).equals(""))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `supplier_info` SET `server_sync`= 'Done' WHERE `supplier_table_id` = '"+supplierInfo.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    } // Set Both These Differently So that make sure that Insert and Update Query executed on PHP Server without any error.
                    if(responseArr[3].equals("true"))
                    {
                        // Update Queries Success
                        for(int i=0; i<supplierInfo.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(supplierInfo.get(i).get(supplierInfo.get(i).size()-1).equals("Update"))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `supplier_info` SET `server_sync`= 'Done' WHERE `supplier_table_id` = '"+supplierInfo.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    }
                    

                }
            }
        }

        if(syncTracking.size() > 0)
        {
            response = sendSyncTrackingData();
            if(response.equals("Unable to Connect To Server"))
            {

            }
            else
            {
                firstIter = true;
                String[] responseArr = response.split(":");
                if(responseArr.length == 4)
                { 
                    if(responseArr[1].equals("true"))
                    {
                        // Insert Queries Success
                        for(int i=0; i<syncTracking.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(syncTracking.get(i).get(syncTracking.get(i).size()-1).equals("Insert") || syncTracking.get(i).get(syncTracking.get(i).size()-1).equals("0") || syncTracking.get(i).get(syncTracking.get(i).size()-1).equals(""))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `sync_tracking` SET `server_sync`= 'Done' WHERE `sync_id` = '"+syncTracking.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    } // Set Both These Differently So that make sure that Insert and Update Query executed on PHP Server without any error.
                    if(responseArr[3].equals("true"))
                    {
                        // Update Queries Success
                        for(int i=0; i<syncTracking.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(syncTracking.get(i).get(syncTracking.get(i).size()-1).equals("Update"))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `sync_tracking` SET `server_sync`= 'Done' WHERE `sync_id` = '"+syncTracking.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    }
                    

                }
            }
        }

        if(systemSettings.size() > 0)
        {
            response = sendSystemSettingsData();
            if(response.equals("Unable to Connect To Server"))
            {

            }
            else
            {
                firstIter = true;
                String[] responseArr = response.split(":");
                if(responseArr.length == 4)
                { 
                    if(responseArr[1].equals("true"))
                    {
                        // Insert Queries Success
                        for(int i=0; i<systemSettings.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(systemSettings.get(i).get(systemSettings.get(i).size()-1).equals("Insert") || systemSettings.get(i).get(systemSettings.get(i).size()-1).equals("0") || systemSettings.get(i).get(systemSettings.get(i).size()-1).equals(""))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `system_settings` SET `server_sync`= 'Done' WHERE `setting_id` = '"+systemSettings.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    } // Set Both These Differently So that make sure that Insert and Update Query executed on PHP Server without any error.
                    if(responseArr[3].equals("true"))
                    {
                        // Update Queries Success
                        for(int i=0; i<systemSettings.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(systemSettings.get(i).get(systemSettings.get(i).size()-1).equals("Update"))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `system_settings` SET `server_sync`= 'Done' WHERE `setting_id` = '"+systemSettings.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    }
                    

                }
            }
        }

        if(userAccounts.size() > 0)
        {
            response = sendUserAccountsData();
            if(response.equals("Unable to Connect To Server"))
            {

            }
            else
            {
                firstIter = true;
                String[] responseArr = response.split(":");
                if(responseArr.length == 4)
                { 
                    if(responseArr[1].equals("true"))
                    {
                        // Insert Queries Success
                        for(int i=0; i<userAccounts.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(userAccounts.get(i).get(userAccounts.get(i).size()-1).equals("Insert") || userAccounts.get(i).get(userAccounts.get(i).size()-1).equals("0") || userAccounts.get(i).get(userAccounts.get(i).size()-1).equals(""))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `user_accounts` SET `server_sync`= 'Done' WHERE `account_id` = '"+userAccounts.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    } // Set Both These Differently So that make sure that Insert and Update Query executed on PHP Server without any error.
                    if(responseArr[3].equals("true"))
                    {
                        // Update Queries Success
                        for(int i=0; i<userAccounts.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(userAccounts.get(i).get(userAccounts.get(i).size()-1).equals("Update"))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `user_accounts` SET `server_sync`= 'Done' WHERE `account_id` = '"+userAccounts.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    }
                    

                }
            }
        }

        if(userInfo.size() > 0)
        { // I am Here, Data Inserted to Server but not updated here
            response = sendUserInfoData();
            if(response.equals("Unable to Connect To Server"))
            {

            }
            else
            {
                firstIter = true;
                String[] responseArr = response.split(":");
                if(responseArr.length == 4)
                { 
                    if(responseArr[1].equals("true"))
                    {
                        // Insert Queries Success
                        for(int i=0; i<userInfo.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(userInfo.get(i).get(userInfo.get(i).size()-1).equals("Insert") || userInfo.get(i).get(userInfo.get(i).size()-1).equals("0") || userInfo.get(i).get(userInfo.get(i).size()-1).equals(""))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `user_info` SET `server_sync`= 'Done' WHERE `user_table_id` = '"+userInfo.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    } // Set Both These Differently So that make sure that Insert and Update Query executed on PHP Server without any error.
                    if(responseArr[3].equals("true"))
                    {
                        // Update Queries Success
                        for(int i=0; i<userInfo.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(userInfo.get(i).get(userInfo.get(i).size()-1).equals("Update"))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `user_info` SET `server_sync`= 'Done' WHERE `user_table_id` = '"+userInfo.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    }
                    

                }
            }
        }

        if(userRights.size() > 0)
        {
            response = sendUserRightsData();
            if(response.equals("Unable to Connect To Server"))
            {

            }
            else
            {
                firstIter = true;
                String[] responseArr = response.split(":");
                if(responseArr.length == 4)
                { 
                    if(responseArr[1].equals("true"))
                    {
                        // Insert Queries Success
                        for(int i=0; i<userRights.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(userRights.get(i).get(userRights.get(i).size()-1).equals("Insert") || userRights.get(i).get(userRights.get(i).size()-1).equals("0") || userRights.get(i).get(userRights.get(i).size()-1).equals(""))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `user_rights` SET `server_sync`= 'Done' WHERE `rights_id` = '"+userRights.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    } // Set Both These Differently So that make sure that Insert and Update Query executed on PHP Server without any error.
                    if(responseArr[3].equals("true"))
                    {
                        // Update Queries Success
                        for(int i=0; i<userRights.size(); i++)
                        {
                            if(!firstIter)
                            {
                                query += "; ";
                            }
                            if(userRights.get(i).get(userRights.get(i).size()-1).equals("Update"))
                            {
                                firstIter = false;
                                String updateQuery = "UPDATE `user_rights` SET `server_sync`= 'Done' WHERE `rights_id` = '"+userRights.get(i).get(0)+"';";
                                query += updateQuery;
                                s.addBatch(updateQuery);
                            }
                        }
                    }

                }
            }
        }
        s.executeBatch();
        GlobalVariables.sepBackup.setVisible(false);
        GlobalVariables.lblDataBackup.setVisible(false);
        GlobalVariables.progressBackup.setVisible(false);
    }

    private ArrayList<String> processArrayToString(ArrayList<ArrayList<String>> arr)
    {
        firstIter = true;
        ArrayList<String> temp = new ArrayList<>();
        for(int i=0; i<arr.size(); i++) {
            for (int j = 0; j < arr.get(i).size(); j++) {
                if(firstIter)
                {
                    temp.add(arr.get(i).get(j));
                }
                else
                {
                    temp.set(j, temp.get(j)+"/-_/"+arr.get(i).get(j));
                }
            }
            firstIter = false;
        }
        return temp;
    }

//    ------------------------------ Get Local Mysql Data Start ------------------------------
//    ------------------------------ Get Local Mysql Data Start ------------------------------
//    ------------------------------ Get Local Mysql Data Start ------------------------------

    public ArrayList<ArrayList<String>> getAreaData()
    {
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        ArrayList<String> temp = new ArrayList<>();
        ResultSet rs = null;
        try {
            String query = "SELECT `area_table_id`, `area_id`, `area_name`, `area_abbrev`, `city_table_id`, `area_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`, `server_sync` FROM `area_info` WHERE `server_sync` != 'Done'";
            rs = objStmt.executeQuery(query);
            while (rs.next())
            {
                foundBackup = true;
                temp = new ArrayList<>();
                temp.add(rs.getString(1)); // Area Table Id
                temp.add(rs.getString(2) == null?"0":rs.getString(2)); // Area Id
                temp.add(rs.getString(3) == null?"":rs.getString(3)); // Area Name
                temp.add(rs.getString(4) == null?"":rs.getString(4)); // Area Abbrev
                temp.add(rs.getString(5) == null?"0":rs.getString(5)); // City Table Id
                temp.add(rs.getString(6) == null?"":rs.getString(6)); // Area Status
                temp.add(rs.getString(7) == null?"0":rs.getString(7)); // Creating User Id
                temp.add(rs.getString(8) == null?"":rs.getString(8)); // Creating Date
                temp.add(rs.getString(9) == null?"":rs.getString(9)); // Creating Time
                temp.add(rs.getString(10) == null?"0":rs.getString(10)); // Update User Id
                temp.add(rs.getString(11) == null?"":rs.getString(11)); // Update Date
                temp.add(rs.getString(12) == null?"":rs.getString(12)); // Update Time
                temp.add(rs.getString(13) == null?"0":rs.getString(13)); // Server Sync
                data.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    public ArrayList<ArrayList<String>> getBatchwiseStockData()
    {
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        ArrayList<String> temp = new ArrayList<>();
        ResultSet rs = null;
        try {
            String query = "SELECT `batch_stock_id`, `product_table_id`, `batch_no`, `quantity`, `bonus`, `retail_price`, `trade_price`, `purchase_price`, `batch_expiry`, `entry_date`, `entry_time`, `server_sync` FROM `batchwise_stock` WHERE `server_sync` != 'Done'";
            rs = objStmt.executeQuery(query);
            while (rs.next())
            {
                foundBackup = true;
                temp = new ArrayList<>();
                temp.add(rs.getString(1)); // Batchwise Stock Table Id
                temp.add(rs.getString(2) == null?"0":rs.getString(2));
                temp.add(rs.getString(3) == null?"":rs.getString(3));
                temp.add(rs.getString(4) == null?"0":rs.getString(4));
                temp.add(rs.getString(5) == null?"0":rs.getString(5));
                temp.add(rs.getString(6) == null?"0":rs.getString(6));
                temp.add(rs.getString(7) == null?"0":rs.getString(7));
                temp.add(rs.getString(8) == null?"0":rs.getString(8));
                temp.add(rs.getString(9) == null?"":rs.getString(9));
                temp.add(rs.getString(10) == null?"0":rs.getString(10));
                temp.add(rs.getString(11) == null?"":rs.getString(11));
                temp.add(rs.getString(12) == null?"0":rs.getString(12));
                data.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    public ArrayList<ArrayList<String>> getBonusPolicyData()
    {
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        ArrayList<String> temp = new ArrayList<>();
        ResultSet rs = null;
        try {
            String query = "SELECT `bonus_policyID`, `approval_id`, `dealer_table_id`, `company_table_id`, `product_table_id`, `quantity`, `bonus_quant`, `start_date`, `end_date`, `policy_status`, `entered_by`, `entry_date`, `entry_time`, `updated_by`, `update_date`, `update_time`, `server_sync` FROM `bonus_policy` WHERE `server_sync` != 'Done'";
            rs = objStmt.executeQuery(query);
            while (rs.next())
            {
                foundBackup = true;
                temp = new ArrayList<>();
                temp.add(rs.getString(1)); // Bonus Policy Table Id
                temp.add(rs.getString(2) == null?"":rs.getString(2));
                temp.add(rs.getString(3) == null?"0":rs.getString(3));
                temp.add(rs.getString(4) == null?"0":rs.getString(4));
                temp.add(rs.getString(5) == null?"0":rs.getString(5));
                temp.add(rs.getString(6) == null?"0":rs.getString(6));
                temp.add(rs.getString(7) == null?"0":rs.getString(7));
                temp.add(rs.getString(8) == null?"":rs.getString(8));
                temp.add(rs.getString(9) == null?"":rs.getString(9));
                temp.add(rs.getString(10) == null?"":rs.getString(10));
                temp.add(rs.getString(11) == null?"0":rs.getString(11));
                temp.add(rs.getString(12) == null?"":rs.getString(12));
                temp.add(rs.getString(13) == null?"":rs.getString(13));
                temp.add(rs.getString(14) == null?"0":rs.getString(14));
                temp.add(rs.getString(15) == null?"":rs.getString(15));
                temp.add(rs.getString(16) == null?"":rs.getString(16));
                temp.add(rs.getString(17) == null?"0":rs.getString(17));
                data.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    public ArrayList<ArrayList<String>> getCityInfoData()
    {
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        ArrayList<String> temp = new ArrayList<>();
        ResultSet rs = null;
        try {
            String query = "SELECT `city_table_id`, `city_id`, `district_table_id`, `city_name`, `city_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`, `server_sync` FROM `city_info` WHERE `server_sync` != 'Done'";
            rs = objStmt.executeQuery(query);
            while (rs.next())
            {
                foundBackup = true;
                temp = new ArrayList<>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2) == null?"0":rs.getString(2));
                temp.add(rs.getString(3) == null?"0":rs.getString(3));
                temp.add(rs.getString(4) == null?"":rs.getString(4));
                temp.add(rs.getString(5) == null?"":rs.getString(5));
                temp.add(rs.getString(6) == null?"0":rs.getString(6));
                temp.add(rs.getString(7) == null?"":rs.getString(7));
                temp.add(rs.getString(8) == null?"":rs.getString(8));
                temp.add(rs.getString(9) == null?"0":rs.getString(9));
                temp.add(rs.getString(10) == null?"":rs.getString(10));
                temp.add(rs.getString(11) == null?"":rs.getString(11));
                temp.add(rs.getString(12) == null?"":rs.getString(12));
                data.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    public ArrayList<ArrayList<String>> getCompanyAlternateContactsData()
    {
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        ArrayList<String> temp = new ArrayList<>();
        ResultSet rs = null;
        try {
            String query = "SELECT `contact_id`, `company_id`, `contact_name`, `contact_number`, `server_sync` FROM `company_alternate_contacts` WHERE `server_sync` != 'Done'";
            rs = objStmt.executeQuery(query);
            while (rs.next())
            {
                foundBackup = true;
                temp = new ArrayList<>();
                temp.add(rs.getString(1)); // Contact Id
                temp.add(rs.getString(2) == null?"0":rs.getString(2));
                temp.add(rs.getString(3) == null?"0":rs.getString(3));
                temp.add(rs.getString(4) == null?"":rs.getString(4));
                temp.add(rs.getString(5) == null?"0":rs.getString(5));
                data.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    public ArrayList<ArrayList<String>> getCompanyInfoData()
    {
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        ArrayList<String> temp = new ArrayList<>();
        ResultSet rs = null;
        try {
            String query = "SELECT `company_table_id`, `company_id`, `company_name`, `company_address`, `company_city`, `company_contact`, `contact_Person`, `company_email`, `company_image`, `company_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`, `server_sync` FROM `company_info` WHERE `server_sync` != 'Done'";
            rs = objStmt.executeQuery(query);
            while (rs.next())
            {
                foundBackup = true;
                temp = new ArrayList<>();
                temp.add(rs.getString(1)); // Company Table Id
                temp.add(rs.getString(2) == null?"0":rs.getString(2));
                temp.add(rs.getString(3) == null?"":rs.getString(3));
                temp.add(rs.getString(4) == null?"":rs.getString(4));
                temp.add(rs.getString(5) == null?"":rs.getString(5));
                temp.add(rs.getString(6) == null?"":rs.getString(6));
                temp.add(rs.getString(7) == null?"":rs.getString(7));
                temp.add(rs.getString(8) == null?"":rs.getString(8));
                temp.add(rs.getString(9) == null?"":rs.getString(9));
                temp.add(rs.getString(10) == null?"":rs.getString(10));
                temp.add(rs.getString(11) == null?"0":rs.getString(11));
                temp.add(rs.getString(12) == null?"":rs.getString(12));
                temp.add(rs.getString(13) == null?"":rs.getString(13));
                temp.add(rs.getString(14) == null?"0":rs.getString(14));
                temp.add(rs.getString(15) == null?"":rs.getString(15));
                temp.add(rs.getString(16) == null?"":rs.getString(16));
                temp.add(rs.getString(17) == null?"0":rs.getString(17));
                data.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    public ArrayList<ArrayList<String>> getCompanyOverallRecordData()
    {
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        ArrayList<String> temp = new ArrayList<>();
        ResultSet rs = null;
        try {
            String query = "SELECT `company_overall_id`, `company_id`, `orders_given`, `successful_orders`, `ordered_packets`, `received_packets`, `ordered_boxes`, `received_boxes`, `order_price`, `discount_price`, `invoiced_price`, `cash_sent`, `return_packets`, `return_boxes`, `return_price`, `cash_return`, `waived_off_price`, `pending_payments`, `server_sync` FROM `company_overall_record` WHERE `server_sync` != 'Done'";
            rs = objStmt.executeQuery(query);
            while (rs.next())
            {
                foundBackup = true;
                temp = new ArrayList<>();
                temp.add(rs.getString(1)); // Company Overall Id
                temp.add(rs.getString(2) == null?"0":rs.getString(2));
                temp.add(rs.getString(3) == null?"0":rs.getString(3));
                temp.add(rs.getString(4) == null?"0":rs.getString(4));
                temp.add(rs.getString(5) == null?"0":rs.getString(5));
                temp.add(rs.getString(6) == null?"0":rs.getString(6));
                temp.add(rs.getString(7) == null?"0":rs.getString(7));
                temp.add(rs.getString(8) == null?"0":rs.getString(8));
                temp.add(rs.getString(9) == null?"0":rs.getString(9));
                temp.add(rs.getString(10) == null?"0":rs.getString(10));
                temp.add(rs.getString(11) == null?"0":rs.getString(11));
                temp.add(rs.getString(12) == null?"0":rs.getString(12));
                temp.add(rs.getString(13) == null?"0":rs.getString(13));
                temp.add(rs.getString(14) == null?"0":rs.getString(14));
                temp.add(rs.getString(15) == null?"0":rs.getString(15));
                temp.add(rs.getString(16) == null?"0":rs.getString(16));
                temp.add(rs.getString(17) == null?"0":rs.getString(17));
                temp.add(rs.getString(18) == null?"0":rs.getString(18));
                temp.add(rs.getString(19) == null?"0":rs.getString(19));
                data.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    public ArrayList<ArrayList<String>> getCompanyPaymentsData()
    {
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        ArrayList<String> temp = new ArrayList<>();
        ResultSet rs = null;
        try {
            String query = "SELECT `payment_id`, `company_id`, `invoice_id`, `amount`, `cash_type`, `pending_payments`, `date`, `day`, `user_id`, `comments`, `status`, `server_sync` FROM `company_payments` WHERE `server_sync` != 'Done'";
            rs = objStmt.executeQuery(query);
            while (rs.next())
            {
                foundBackup = true;
                temp = new ArrayList<>();
                temp.add(rs.getString(1)); // Company Payments Id
                temp.add(rs.getString(2) == null?"0":rs.getString(2));
                temp.add(rs.getString(3) == null?"0":rs.getString(3));
                temp.add(rs.getString(4) == null?"0":rs.getString(4));
                temp.add(rs.getString(5) == null?"":rs.getString(5));
                temp.add(rs.getString(6) == null?"0":rs.getString(6));
                temp.add(rs.getString(7) == null?"":rs.getString(7));
                temp.add(rs.getString(8) == null?"":rs.getString(8));
                temp.add(rs.getString(9) == null?"0":rs.getString(9));
                temp.add(rs.getString(10) == null?"":rs.getString(10));
                temp.add(rs.getString(11) == null?"":rs.getString(11));
                temp.add(rs.getString(12) == null?"0":rs.getString(12));
                data.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    public ArrayList<ArrayList<String>> getDealerDaySummaryData()
    {
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        ArrayList<String> temp = new ArrayList<>();
        ResultSet rs = null;
        try {
            String query = "SELECT `dealer_summary_id`, `dealer_id`, `date`, `day`, `booking_order`, `delivered_order`, `returned_order`, `return_quantity`, `return_price`, `cash_collection`, `cash_return`, `cash_waiveoff`, `server_sync` FROM `dealer_day_summary` WHERE `server_sync` != 'Done'";
            rs = objStmt.executeQuery(query);
            while (rs.next())
            {
                foundBackup = true;
                temp = new ArrayList<>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2) == null?"0":rs.getString(2));
                temp.add(rs.getString(3) == null?"":rs.getString(3));
                temp.add(rs.getString(4) == null?"":rs.getString(4));
                temp.add(rs.getString(5) == null?"0":rs.getString(5));
                temp.add(rs.getString(6) == null?"0":rs.getString(6));
                temp.add(rs.getString(7) == null?"0":rs.getString(7));
                temp.add(rs.getString(8) == null?"0":rs.getString(8));
                temp.add(rs.getString(9) == null?"0":rs.getString(9));
                temp.add(rs.getString(10) == null?"0":rs.getString(10));
                temp.add(rs.getString(11) == null?"0":rs.getString(11));
                temp.add(rs.getString(12) == null?"0":rs.getString(12));
                temp.add(rs.getString(13) == null?"0":rs.getString(13));
                data.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    public ArrayList<ArrayList<String>> getDealerGPSLocationData()
    {
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        ArrayList<String> temp = new ArrayList<>();
        ResultSet rs = null;
        try {
            String query = "SELECT `dealer_loc_id`, `dealer_id`, `latitude`, `longitude`, `loc_name`, `entry_date`, `entry_time`, `update_date`, `update_time`, `user_id`, `server_sync` FROM `dealer_gps_location` WHERE `server_sync` != 'Done'";
            rs = objStmt.executeQuery(query);
            while (rs.next())
            {
                foundBackup = true;
                temp = new ArrayList<>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2) == null?"0":rs.getString(2));
                temp.add(rs.getString(3) == null?"":rs.getString(3));
                temp.add(rs.getString(4) == null?"":rs.getString(4));
                temp.add(rs.getString(5) == null?"":rs.getString(5));
                temp.add(rs.getString(6) == null?"":rs.getString(6));
                temp.add(rs.getString(7) == null?"":rs.getString(7));
                temp.add(rs.getString(8) == null?"":rs.getString(8));
                temp.add(rs.getString(9) == null?"":rs.getString(9));
                temp.add(rs.getString(10) == null?"0":rs.getString(10));
                temp.add(rs.getString(11) == null?"0":rs.getString(11));
                data.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    public ArrayList<ArrayList<String>> getDealerInfoData()
    {
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        ArrayList<String> temp = new ArrayList<>();
        ResultSet rs = null;
        try {
            String query = "SELECT `dealer_table_id`, `dealer_id`, `dealer_area_id`, `dealer_name`, `dealer_contact_person`, `dealer_phone`, `dealer_fax`, `dealer_address`, `dealer_address1`, `dealer_type`, `dealer_cnic`, `dealer_ntn`, `dealer_image`, `dealer_lic9_num`, `dealer_lic9_exp`, `dealer_lic10_num`, `dealer_lic10_Exp`, `dealer_lic11_num`, `dealer_lic11_Exp`, `dealer_credit_limit`, `dealer_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`, `server_sync` FROM `dealer_info` WHERE `server_sync` != 'Done'";
            rs = objStmt.executeQuery(query);
            while (rs.next())
            {
                foundBackup = true;
                temp = new ArrayList<>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2) == null?"0":rs.getString(2));
                temp.add(rs.getString(3) == null?"":rs.getString(3));
                temp.add(rs.getString(4) == null?"":rs.getString(4));
                temp.add(rs.getString(5) == null?"":rs.getString(5));
                temp.add(rs.getString(6) == null?"":rs.getString(6));
                temp.add(rs.getString(7) == null?"":rs.getString(7));
                temp.add(rs.getString(8) == null?"":rs.getString(8));
                temp.add(rs.getString(9) == null?"":rs.getString(9));
                temp.add(rs.getString(10) == null?"":rs.getString(10));
                temp.add(rs.getString(11) == null?"":rs.getString(11));
                temp.add(rs.getString(12) == null?"":rs.getString(12));
                temp.add(rs.getString(13) == null?"":rs.getString(13));
                temp.add(rs.getString(14) == null?"":rs.getString(14));
                temp.add(rs.getString(15) == null?"":rs.getString(15));
                temp.add(rs.getString(16) == null?"":rs.getString(16));
                temp.add(rs.getString(17) == null?"":rs.getString(17));
                temp.add(rs.getString(18) == null?"":rs.getString(18));
                temp.add(rs.getString(19) == null?"":rs.getString(19));
                temp.add(rs.getString(20) == null?"0":rs.getString(20));
                temp.add(rs.getString(21) == null?"":rs.getString(21));
                temp.add(rs.getString(22) == null?"0":rs.getString(22));
                temp.add(rs.getString(23) == null?"":rs.getString(23));
                temp.add(rs.getString(24) == null?"":rs.getString(24));
                temp.add(rs.getString(25) == null?"0":rs.getString(25));
                temp.add(rs.getString(26) == null?"":rs.getString(26));
                temp.add(rs.getString(27) == null?"":rs.getString(27));
                temp.add(rs.getString(28) == null?"0":rs.getString(28));
                data.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    public ArrayList<ArrayList<String>> getDealerOverallRecordData()
    {
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        ArrayList<String> temp = new ArrayList<>();
        ResultSet rs = null;
        try {
            String query = "SELECT `dealer_overall_id`, `dealer_id`, `orders_given`, `successful_orders`, `ordered_packets`, `submitted_packets`, `ordered_boxes`, `submitted_boxes`, `order_price`, `discount_price`, `invoiced_price`, `cash_collected`, `return_packets`, `return_boxes`, `return_price`, `cash_return`, `waived_off_price`, `pending_payments`, `server_sync` FROM `dealer_overall_record` WHERE `server_sync` != 'Done'";
            rs = objStmt.executeQuery(query);
            while (rs.next())
            {
                foundBackup = true;
                temp = new ArrayList<>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2) == null?"0":rs.getString(2));
                temp.add(rs.getString(3) == null?"0":rs.getString(3));
                temp.add(rs.getString(4) == null?"0":rs.getString(4));
                temp.add(rs.getString(5) == null?"0":rs.getString(5));
                temp.add(rs.getString(6) == null?"0":rs.getString(6));
                temp.add(rs.getString(7) == null?"0":rs.getString(7));
                temp.add(rs.getString(8) == null?"0":rs.getString(8));
                temp.add(rs.getString(9) == null?"0":rs.getString(9));
                temp.add(rs.getString(10) == null?"0":rs.getString(10));
                temp.add(rs.getString(11) == null?"0":rs.getString(11));
                temp.add(rs.getString(12) == null?"0":rs.getString(12));
                temp.add(rs.getString(13) == null?"0":rs.getString(13));
                temp.add(rs.getString(14) == null?"0":rs.getString(14));
                temp.add(rs.getString(15) == null?"0":rs.getString(15));
                temp.add(rs.getString(16) == null?"0":rs.getString(16));
                temp.add(rs.getString(17) == null?"0":rs.getString(17));
                temp.add(rs.getString(18) == null?"0":rs.getString(18));
                temp.add(rs.getString(19) == null?"0":rs.getString(19));
                data.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    public ArrayList<ArrayList<String>> getDealerPaymentsData()
    {
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        ArrayList<String> temp = new ArrayList<>();
        ResultSet rs = null;
        try {
            String query = "SELECT `payment_id`, `dealer_id`, `order_id`, `amount`, `cash_type`, `pending_payments`, `date`, `day`, `user_id`, `comments`, `status`, `server_id`, `server_sync` FROM `dealer_payments` WHERE `server_sync` != 'Done'";
            rs = objStmt.executeQuery(query);
            while (rs.next())
            {
                foundBackup = true;
                temp = new ArrayList<>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2) == null?"0":rs.getString(2));
                temp.add(rs.getString(3) == null?"0":rs.getString(3));
                temp.add(rs.getString(4) == null?"0":rs.getString(4));
                temp.add(rs.getString(5) == null?"":rs.getString(5));
                temp.add(rs.getString(6) == null?"0":rs.getString(6));
                temp.add(rs.getString(7) == null?"":rs.getString(7));
                temp.add(rs.getString(8) == null?"":rs.getString(8));
                temp.add(rs.getString(9) == null?"0":rs.getString(9));
                temp.add(rs.getString(10) == null?"":rs.getString(10));
                temp.add(rs.getString(11) == null?"":rs.getString(11));
                temp.add(rs.getString(12) == null?"0":rs.getString(12));
                temp.add(rs.getString(13) == null?"0":rs.getString(13));
                data.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    public ArrayList<ArrayList<String>> getDealerTypesData()
    {
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        ArrayList<String> temp = new ArrayList<>();
        ResultSet rs = null;
        try {
            String query = "SELECT `dealer_type_ID`, `typeID`, `dealerType_name`, `typeStatus`, `typeStatus`, `server_sync` FROM `dealer_types` WHERE `server_sync` != 'Done'";
            rs = objStmt.executeQuery(query);
            while (rs.next())
            {
                foundBackup = true;
                temp = new ArrayList<>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2) == null?"0":rs.getString(2));
                temp.add(rs.getString(3) == null?"":rs.getString(3));
                temp.add(rs.getString(4) == null?"":rs.getString(4));
                temp.add(rs.getString(5) == null?"0":rs.getString(5));
                temp.add(rs.getString(6) == null?"0":rs.getString(6));
                data.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    public ArrayList<ArrayList<String>> getDiscountPolicyData()
    {
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        ArrayList<String> temp = new ArrayList<>();
        ResultSet rs = null;
        try {
            String query = "SELECT `disc_policyID`, `approval_id`, `dealer_table_id`, `company_table_id`, `product_table_id`, `sale_amount`, `discount_percent`, `start_date`, `end_date`, `policy_status`, `entered_by`, `entry_date`, `entry_time`, `updated_by`, `update_date`, `update_time`, `server_sync` FROM `discount_policy` WHERE `server_sync` != 'Done'";
            rs = objStmt.executeQuery(query);
            while (rs.next())
            {
                foundBackup = true;
                temp = new ArrayList<>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2) == null?"0":rs.getString(2));
                temp.add(rs.getString(3) == null?"0":rs.getString(3));
                temp.add(rs.getString(4) == null?"0":rs.getString(4));
                temp.add(rs.getString(5) == null?"0":rs.getString(5));
                temp.add(rs.getString(6) == null?"0":rs.getString(6));
                temp.add(rs.getString(7) == null?"0":rs.getString(7));
                temp.add(rs.getString(8) == null?"":rs.getString(8));
                temp.add(rs.getString(9) == null?"":rs.getString(9));
                temp.add(rs.getString(10) == null?"":rs.getString(10));
                temp.add(rs.getString(11) == null?"0":rs.getString(11));
                temp.add(rs.getString(12) == null?"":rs.getString(12));
                temp.add(rs.getString(13) == null?"":rs.getString(13));
                temp.add(rs.getString(14) == null?"0":rs.getString(14));
                temp.add(rs.getString(15) == null?"":rs.getString(15));
                temp.add(rs.getString(16) == null?"":rs.getString(16));
                temp.add(rs.getString(17) == null?"0":rs.getString(17));
                data.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    public ArrayList<ArrayList<String>> getDistrictInfoData()
    {
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        ArrayList<String> temp = new ArrayList<>();
        ResultSet rs = null;
        try {
            String query = "SELECT `district_table_id`, `district_id`, `district_name`, `district_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`, `server_sync` FROM `district_info` WHERE `server_sync` != 'Done'";
            rs = objStmt.executeQuery(query);
            while (rs.next())
            {
                foundBackup = true;
                temp = new ArrayList<>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2) == null?"0":rs.getString(2));
                temp.add(rs.getString(3) == null?"":rs.getString(3));
                temp.add(rs.getString(4) == null?"":rs.getString(4));
                temp.add(rs.getString(5) == null?"0":rs.getString(5));
                temp.add(rs.getString(6) == null?"":rs.getString(6));
                temp.add(rs.getString(7) == null?"":rs.getString(7));
                temp.add(rs.getString(8) == null?"0":rs.getString(8));
                temp.add(rs.getString(9) == null?"":rs.getString(9));
                temp.add(rs.getString(10) == null?"":rs.getString(10));
                temp.add(rs.getString(11) == null?"0":rs.getString(11));
                data.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    public ArrayList<ArrayList<String>> getGroupsInfoData()
    {
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        ArrayList<String> temp = new ArrayList<>();
        ResultSet rs = null;
        try {
            String query = "SELECT `group_table_id`, `group_id`, `company_id`, `group_name`, `group_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`, `server_sync` FROM `groups_info` WHERE `server_sync` != 'Done'";
            rs = objStmt.executeQuery(query);
            while (rs.next())
            {
                foundBackup = true;
                temp = new ArrayList<>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2) == null?"0":rs.getString(2));
                temp.add(rs.getString(3) == null?"0":rs.getString(3));
                temp.add(rs.getString(4) == null?"":rs.getString(4));
                temp.add(rs.getString(5) == null?"":rs.getString(5));
                temp.add(rs.getString(6) == null?"0":rs.getString(6));
                temp.add(rs.getString(7) == null?"":rs.getString(7));
                temp.add(rs.getString(8) == null?"":rs.getString(8));
                temp.add(rs.getString(9) == null?"0":rs.getString(9));
                temp.add(rs.getString(10) == null?"":rs.getString(10));
                temp.add(rs.getString(11) == null?"":rs.getString(11));
                temp.add(rs.getString(12) == null?"0":rs.getString(12));
                data.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    public ArrayList<ArrayList<String>> getMailsRecordData()
    {
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        ArrayList<String> temp = new ArrayList<>();
        ResultSet rs = null;
        try {
            String query = "SELECT `mail_id`, `from_email`, `to_email`, `title`, `message`, `attachment`, `user_id`, `date`, `time`, `status`, `server_sync` FROM `mails_record` WHERE `server_sync` != 'Done'";
            rs = objStmt.executeQuery(query);
            while (rs.next())
            {
                foundBackup = true;
                temp = new ArrayList<>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2) == null?"":rs.getString(2));
                temp.add(rs.getString(3) == null?"":rs.getString(3));
                temp.add(rs.getString(4) == null?"":rs.getString(4));
                temp.add(rs.getString(5) == null?"":rs.getString(5));
                temp.add(rs.getString(6) == null?"":rs.getString(6));
                temp.add(rs.getString(7) == null?"":rs.getString(7));
                temp.add(rs.getString(8) == null?"":rs.getString(8));
                temp.add(rs.getString(9) == null?"":rs.getString(9));
                temp.add(rs.getString(10) == null?"":rs.getString(10));
                temp.add(rs.getString(11) == null?"":rs.getString(11));
                data.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    public ArrayList<ArrayList<String>> getMobileGPSLocationData()
    {
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        ArrayList<String> temp = new ArrayList<>();
        ResultSet rs = null;
        try {
            String query = "SELECT `mob_loc_id`, `user_id`, `latitude`, `longitude`, `loc_name`, `date`, `time`, `server_sync` FROM `mobile_gps_location` WHERE `server_sync` != 'Done'";
            rs = objStmt.executeQuery(query);
            while (rs.next())
            {
                foundBackup = true;
                temp = new ArrayList<>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2) == null?"0":rs.getString(2));
                temp.add(rs.getString(3) == null?"":rs.getString(3));
                temp.add(rs.getString(4) == null?"":rs.getString(4));
                temp.add(rs.getString(5) == null?"":rs.getString(5));
                temp.add(rs.getString(6) == null?"":rs.getString(6));
                temp.add(rs.getString(7) == null?"":rs.getString(7));
                temp.add(rs.getString(8) == null?"0":rs.getString(8));
                data.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    public ArrayList<ArrayList<String>> getOrderGPSLocationData()
    {
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        ArrayList<String> temp = new ArrayList<>();
        ResultSet rs = null;
        try {
            String query = "SELECT `order_loc_id`, `order_id`, `user_id`, `latitude`, `longitude`, `loc_name`, `date`, `time`, `dealer_id`, `price`, `type`, `server_sync` FROM `order_gps_location` WHERE `server_sync` != 'Done'";
            rs = objStmt.executeQuery(query);
            while (rs.next())
            {
                foundBackup = true;
                temp = new ArrayList<>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2) == null?"0":rs.getString(2));
                temp.add(rs.getString(3) == null?"0":rs.getString(3));
                temp.add(rs.getString(4) == null?"":rs.getString(4));
                temp.add(rs.getString(5) == null?"":rs.getString(5));
                temp.add(rs.getString(6) == null?"":rs.getString(6));
                temp.add(rs.getString(7) == null?"":rs.getString(7));
                temp.add(rs.getString(8) == null?"":rs.getString(8));
                temp.add(rs.getString(9) == null?"0":rs.getString(9));
                temp.add(rs.getString(10) == null?"0":rs.getString(10));
                temp.add(rs.getString(11) == null?"":rs.getString(11));
                temp.add(rs.getString(12) == null?"0":rs.getString(12));
                data.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    public ArrayList<ArrayList<String>> getOrderInfoData()
    {
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        ArrayList<String> temp = new ArrayList<>();
        ResultSet rs = null;
        try {
            String query = "SELECT `order_id`, `dealer_id`, `product_id`, `quantity`, `unit`, `order_price`, `bonus`, `discount`, `waive_off_price`, `final_price`, `order_success`, `booking_latitude`, `booking_longitude`, `booking_area`, `booking_date`, `booking_time`, `booking_user_id`, `delivered_latitude`, `delivered_longitude`, `delivered_area`, `delivered_date`, `delivered_time`, `delivered_user_id`, `update_dates`, `update_times`, `update_user_id`, `comments`, `status`, `server_sync` FROM `order_info` WHERE `server_sync` != 'Done'";
            rs = objStmt.executeQuery(query);
            while (rs.next())
            {
                foundBackup = true;
                temp = new ArrayList<>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2) == null?"0":rs.getString(2));
                temp.add(rs.getString(3) == null?"":rs.getString(3));
                temp.add(rs.getString(4) == null?"":rs.getString(4));
                temp.add(rs.getString(5) == null?"":rs.getString(5));
                temp.add(rs.getString(6) == null?"0":rs.getString(6));
                temp.add(rs.getString(7) == null?"":rs.getString(7));
                temp.add(rs.getString(8) == null?"0":rs.getString(8));
                temp.add(rs.getString(9) == null?"0":rs.getString(9));
                temp.add(rs.getString(10) == null?"":rs.getString(10));
                temp.add(rs.getString(11) == null?"0":rs.getString(11));
                temp.add(rs.getString(12) == null?"":rs.getString(12));
                temp.add(rs.getString(13) == null?"":rs.getString(13));
                temp.add(rs.getString(14) == null?"":rs.getString(14));
                temp.add(rs.getString(15) == null?"":rs.getString(15));
                temp.add(rs.getString(16) == null?"":rs.getString(16));
                temp.add(rs.getString(17) == null?"0":rs.getString(17));
                temp.add(rs.getString(18) == null?"":rs.getString(18));
                temp.add(rs.getString(19) == null?"":rs.getString(19));
                temp.add(rs.getString(20) == null?"":rs.getString(20));
                temp.add(rs.getString(21) == null?"":rs.getString(21));
                temp.add(rs.getString(22) == null?"":rs.getString(22));
                temp.add(rs.getString(23) == null?"0":rs.getString(23));
                temp.add(rs.getString(24) == null?"":rs.getString(24));
                temp.add(rs.getString(25) == null?"":rs.getString(25));
                temp.add(rs.getString(26) == null?"0":rs.getString(26));
                temp.add(rs.getString(27) == null?"":rs.getString(27));
                temp.add(rs.getString(28) == null?"":rs.getString(28));
                temp.add(rs.getString(29) == null?"0":rs.getString(29));
                data.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    public ArrayList<ArrayList<String>> getOrderInfoDetailedData()
    {
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        ArrayList<String> temp = new ArrayList<>();
        ResultSet rs = null;
        try {
            String query = "SELECT `detail_id`, `order_id`, `product_table_id`, `batch_id`, `quantity`, `submission_quantity`, `unit`, `submission_unit`, `order_price`, `bonus_quant`, `discount`, `final_price`, `cmplt_returned_bit`, `returned_quant`, `returned_bonus_quant`, `server_sync` FROM `order_info_detailed` WHERE `server_sync` != 'Done'";
            rs = objStmt.executeQuery(query);
            while (rs.next())
            {
                foundBackup = true;
                temp = new ArrayList<>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2) == null?"0":rs.getString(2));
                temp.add(rs.getString(3) == null?"0":rs.getString(3));
                temp.add(rs.getString(4) == null?"0":rs.getString(4));
                temp.add(rs.getString(5) == null?"0":rs.getString(5));
                temp.add(rs.getString(6) == null?"0":rs.getString(6));
                temp.add(rs.getString(7) == null?"":rs.getString(7));
                temp.add(rs.getString(8) == null?"":rs.getString(8));
                temp.add(rs.getString(9) == null?"0":rs.getString(9));
                temp.add(rs.getString(10) == null?"0":rs.getString(10));
                temp.add(rs.getString(11) == null?"0":rs.getString(11));
                temp.add(rs.getString(12) == null?"0":rs.getString(12));
                temp.add(rs.getString(13) == null?"0":rs.getString(13));
                temp.add(rs.getString(14) == null?"0":rs.getString(14));
                temp.add(rs.getString(15) == null?"0":rs.getString(15));
                temp.add(rs.getString(16) == null?"0":rs.getString(16));
                data.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    public ArrayList<ArrayList<String>> getOrderReturnData()
    {
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        ArrayList<String> temp = new ArrayList<>();
        ResultSet rs = null;
        try {
            String query = "SELECT `return_id`, `dealer_id`, `return_total_price`, `return_gross_price`, `order_id`, `entry_date`, `entry_time`, `enter_user_id`, `update_date`, `update_time`, `update_user_id`, `comments`, `status`, `server_sync` FROM `order_return` WHERE `server_sync` != 'Done'";
            rs = objStmt.executeQuery(query);
            while (rs.next())
            {
                foundBackup = true;
                temp = new ArrayList<>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2) == null?"0":rs.getString(2));
                temp.add(rs.getString(3) == null?"":rs.getString(3));
                temp.add(rs.getString(4) == null?"":rs.getString(4));
                temp.add(rs.getString(5) == null?"0":rs.getString(5));
                temp.add(rs.getString(6) == null?"":rs.getString(6));
                temp.add(rs.getString(7) == null?"":rs.getString(7));
                temp.add(rs.getString(8) == null?"0":rs.getString(8));
                temp.add(rs.getString(9) == null?"":rs.getString(9));
                temp.add(rs.getString(10) == null?"":rs.getString(10));
                temp.add(rs.getString(11) == null?"0":rs.getString(11));
                temp.add(rs.getString(12) == null?"":rs.getString(12));
                temp.add(rs.getString(13) == null?"":rs.getString(13));
                temp.add(rs.getString(14) == null?"0":rs.getString(14));
                data.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    public ArrayList<ArrayList<String>> getOrderReturnDetailData()
    {
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        ArrayList<String> temp = new ArrayList<>();
        ResultSet rs = null;
        try {
            String query = "SELECT `return_detail_id`, `return_id`, `prod_id`, `prod_batch`, `prod_quant`, `bonus_quant`, `discount_amount`, `total_amount`, `unit`, `return_reason`, `server_sync` FROM `order_return_detail` WHERE `server_sync` != 'Done'";
            rs = objStmt.executeQuery(query);
            while (rs.next())
            {
                foundBackup = true;
                temp = new ArrayList<>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2) == null?"0":rs.getString(2));
                temp.add(rs.getString(3) == null?"0":rs.getString(3));
                temp.add(rs.getString(4) == null?"":rs.getString(4));
                temp.add(rs.getString(5) == null?"0":rs.getString(5));
                temp.add(rs.getString(6) == null?"0":rs.getString(6));
                temp.add(rs.getString(7) == null?"0":rs.getString(7));
                temp.add(rs.getString(8) == null?"0":rs.getString(8));
                temp.add(rs.getString(9) == null?"":rs.getString(9));
                temp.add(rs.getString(10) == null?"":rs.getString(10));
                temp.add(rs.getString(11) == null?"0":rs.getString(11));
                data.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    public ArrayList<ArrayList<String>> getOrderStatusInfoData()
    {
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        ArrayList<String> temp = new ArrayList<>();
        ResultSet rs = null;
        try {
            String query = "SELECT `order_status_id`, `order_id`, `booking_user_id`, `booking_date`, `booking_latitude`, `booking_longitude`, `booking_area`, `delivered_user_id`, `delivered_date`, `delivered_latitude`, `delivered_longitude`, `delivered_area`, `server_sync` FROM `order_status_info` WHERE `server_sync` != 'Done'";
            rs = objStmt.executeQuery(query);
            while (rs.next())
            {
                foundBackup = true;
                temp = new ArrayList<>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2) == null?"0":rs.getString(2));
                temp.add(rs.getString(3) == null?"0":rs.getString(3));
                temp.add(rs.getString(4) == null?"":rs.getString(4));
                temp.add(rs.getString(5) == null?"":rs.getString(5));
                temp.add(rs.getString(6) == null?"":rs.getString(6));
                temp.add(rs.getString(7) == null?"":rs.getString(7));
                temp.add(rs.getString(8) == null?"0":rs.getString(8));
                temp.add(rs.getString(9) == null?"":rs.getString(9));
                temp.add(rs.getString(10) == null?"":rs.getString(10));
                temp.add(rs.getString(11) == null?"":rs.getString(11));
                temp.add(rs.getString(12) == null?"":rs.getString(12));
                temp.add(rs.getString(13) == null?"0":rs.getString(13));
                data.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    public ArrayList<ArrayList<String>> getProductStockData()
    {
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        ArrayList<String> temp = new ArrayList<>();
        ResultSet rs = null;
        try {
            String query = "SELECT `ProdStockID`, `product_table_id`, `in_stock`, `min_stock`, `max_stock`, `bonus_quant`, `status`, `server_sync` FROM `products_stock` WHERE `server_sync` != 'Done'";
            rs = objStmt.executeQuery(query);
            while (rs.next())
            {
                foundBackup = true;
                temp = new ArrayList<>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2) == null?"0":rs.getString(2));
                temp.add(rs.getString(3) == null?"0":rs.getString(3));
                temp.add(rs.getString(4) == null?"0":rs.getString(4));
                temp.add(rs.getString(5) == null?"0":rs.getString(5));
                temp.add(rs.getString(6) == null?"0":rs.getString(6));
                temp.add(rs.getString(7) == null?"":rs.getString(7));
                temp.add(rs.getString(8) == null?"0":rs.getString(8));
                data.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    public ArrayList<ArrayList<String>> getProductInfoData()
    {
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        ArrayList<String> temp = new ArrayList<>();
        ResultSet rs = null;
        try {
            String query = "SELECT `product_table_id`, `product_id`, `company_table_id`, `group_table_id`, `report_prodID`, `old_prodID`, `product_name`, `type_table_id`, `packSize`, `carton_size`, `retail_price`, `trade_price`, `purchase_price`, `purchase_discount`, `sales_tax`, `hold_stock`, `product_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`, `server_sync` FROM `product_info` WHERE `server_sync` != 'Done'";
            rs = objStmt.executeQuery(query);
            while (rs.next())
            {
                foundBackup = true;
                temp = new ArrayList<>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2) == null?"0":rs.getString(2));
                temp.add(rs.getString(3) == null?"0":rs.getString(3));
                temp.add(rs.getString(4) == null?"0":rs.getString(4));
                temp.add(rs.getString(5) == null?"":rs.getString(5));
                temp.add(rs.getString(6) == null?"":rs.getString(6));
                temp.add(rs.getString(7) == null?"":rs.getString(7));
                temp.add(rs.getString(8) == null?"":rs.getString(8));
                temp.add(rs.getString(9) == null?"":rs.getString(9));
                temp.add(rs.getString(10) == null?"":rs.getString(10));
                temp.add(rs.getString(11) == null?"0":rs.getString(11));
                temp.add(rs.getString(12) == null?"0":rs.getString(12));
                temp.add(rs.getString(13) == null?"0":rs.getString(13));
                temp.add(rs.getString(14) == null?"0":rs.getString(14));
                temp.add(rs.getString(15) == null?"0":rs.getString(15));
                temp.add(rs.getString(16) == null?"0":rs.getString(16));
                temp.add(rs.getString(17) == null?"":rs.getString(17));
                temp.add(rs.getString(18) == null?"0":rs.getString(18));
                temp.add(rs.getString(19) == null?"":rs.getString(19));
                temp.add(rs.getString(20) == null?"":rs.getString(20));
                temp.add(rs.getString(21) == null?"0":rs.getString(21));
                temp.add(rs.getString(22) == null?"":rs.getString(22));
                temp.add(rs.getString(23) == null?"":rs.getString(23));
                temp.add(rs.getString(24) == null?"0":rs.getString(24));
                data.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    public ArrayList<ArrayList<String>> getProductOverallRecordData()
    {
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        ArrayList<String> temp = new ArrayList<>();
        ResultSet rs = null;
        try {
            String query = "SELECT `overall_record_id`, `product_table_id`, `prod_name`, `prod_batch`, `quantity_sold`, `quantity_return`, `trade_rate`, `purch_rate`, `retail_rate`, `expired_quantity`, `damaged_quantity`, `prod_status`, `server_sync` FROM `product_overall_record` WHERE `server_sync` != 'Done'";
            rs = objStmt.executeQuery(query);
            while (rs.next())
            {
                foundBackup = true;
                temp = new ArrayList<>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2) == null?"0":rs.getString(2));
                temp.add(rs.getString(3) == null?"":rs.getString(3));
                temp.add(rs.getString(4) == null?"":rs.getString(4));
                temp.add(rs.getString(5) == null?"0":rs.getString(5));
                temp.add(rs.getString(6) == null?"0":rs.getString(6));
                temp.add(rs.getString(7) == null?"0":rs.getString(7));
                temp.add(rs.getString(8) == null?"0":rs.getString(8));
                temp.add(rs.getString(9) == null?"0":rs.getString(9));
                temp.add(rs.getString(10) == null?"0":rs.getString(10));
                temp.add(rs.getString(11) == null?"0":rs.getString(11));
                temp.add(rs.getString(12) == null?"":rs.getString(12));
                temp.add(rs.getString(13) == null?"0":rs.getString(13));
                data.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    public ArrayList<ArrayList<String>> getPurchaseInfoData()
    {
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        ArrayList<String> temp = new ArrayList<>();
        ResultSet rs = null;
        try {
            String query = "SELECT `purchase_id`, `comp_id`, `invoice_num`, `supplier_id`, `purchase_date`, `gross_amount`, `disc_amount`, `net_amount`, `enter_user_id`, `entry_date`, `entry_time`, `update_user_id`, `update_date`, `update_time`, `comments`, `status`, `server_sync` FROM `purchase_info` WHERE `server_sync` != 'Done'";
            rs = objStmt.executeQuery(query);
            while (rs.next())
            {
                foundBackup = true;
                temp = new ArrayList<>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2) == null?"0":rs.getString(2));
                temp.add(rs.getString(3) == null?"":rs.getString(3));
                temp.add(rs.getString(4) == null?"0":rs.getString(4));
                temp.add(rs.getString(5) == null?"":rs.getString(5));
                temp.add(rs.getString(6) == null?"0":rs.getString(6));
                temp.add(rs.getString(7) == null?"0":rs.getString(7));
                temp.add(rs.getString(8) == null?"0":rs.getString(8));
                temp.add(rs.getString(9) == null?"0":rs.getString(9));
                temp.add(rs.getString(10) == null?"":rs.getString(10));
                temp.add(rs.getString(11) == null?"":rs.getString(11));
                temp.add(rs.getString(12) == null?"0":rs.getString(12));
                temp.add(rs.getString(13) == null?"":rs.getString(13));
                temp.add(rs.getString(14) == null?"":rs.getString(14));
                temp.add(rs.getString(15) == null?"":rs.getString(15));
                temp.add(rs.getString(16) == null?"":rs.getString(16));
                temp.add(rs.getString(17) == null?"0":rs.getString(17));
                data.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    public ArrayList<ArrayList<String>> getPurchaseInfoDetailData()
    {
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        ArrayList<String> temp = new ArrayList<>();
        ResultSet rs = null;
        try {
            String query = "SELECT `purchase_info_detail_id`, `invoice_num`, `purchase_id`, `prod_id`, `discount`, `bonus_quant`, `recieve_quant`, `batch_no`, `expiry_date`, `gross_amount`, `disc_amount`, `net_amount`, `invoice_date`, `returnBit`, `returned_quant`, `returned_bonus_quant`, `server_sync` FROM `purchase_info_detail` WHERE `server_sync` != 'Done'";
            rs = objStmt.executeQuery(query);
            while (rs.next())
            {
                foundBackup = true;
                temp = new ArrayList<>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2) == null?"":rs.getString(2));
                temp.add(rs.getString(3) == null?"0":rs.getString(3));
                temp.add(rs.getString(4) == null?"0":rs.getString(4));
                temp.add(rs.getString(5) == null?"0":rs.getString(5));
                temp.add(rs.getString(6) == null?"0":rs.getString(6));
                temp.add(rs.getString(7) == null?"0":rs.getString(7));
                temp.add(rs.getString(8) == null?"":rs.getString(8));
                temp.add(rs.getString(9) == null?"":rs.getString(9));
                temp.add(rs.getString(10) == null?"0":rs.getString(10));
                temp.add(rs.getString(11) == null?"0":rs.getString(11));
                temp.add(rs.getString(12) == null?"0":rs.getString(12));
                temp.add(rs.getString(13) == null?"":rs.getString(13));
                temp.add(rs.getString(14) == null?"0":rs.getString(14));
                temp.add(rs.getString(15) == null?"0":rs.getString(15));
                temp.add(rs.getString(16) == null?"0":rs.getString(16));
                temp.add(rs.getString(17) == null?"0":rs.getString(17));
                data.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    public ArrayList<ArrayList<String>> getPurchaseOrderDetailData()
    {
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        ArrayList<String> temp = new ArrayList<>();
        ResultSet rs = null;
        try {
            String query = "SELECT `order_detail_tableID`, `product_table_id`, `quantity_sold`, `quantity_instock`, `quantity_ordered`, `purch_order_id`, `net_amount`, `server_sync` FROM `purchase_order_detail` WHERE `server_sync` != 'Done'";
            rs = objStmt.executeQuery(query);
            while (rs.next())
            {
                foundBackup = true;
                temp = new ArrayList<>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2) == null?"0":rs.getString(2));
                temp.add(rs.getString(3) == null?"0":rs.getString(3));
                temp.add(rs.getString(4) == null?"0":rs.getString(4));
                temp.add(rs.getString(5) == null?"0":rs.getString(5));
                temp.add(rs.getString(6) == null?"0":rs.getString(6));
                temp.add(rs.getString(7) == null?"0":rs.getString(7));
                temp.add(rs.getString(8) == null?"0":rs.getString(8));
                data.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    public ArrayList<ArrayList<String>> getPurchaseOrderInfoData()
    {
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        ArrayList<String> temp = new ArrayList<>();
        ResultSet rs = null;
        try {
            String query = "SELECT `purch_order_table_id`, `purch_order_id`, `purch_order_date`, `purch_order_amount`, `numb_of_prod`, `inventoryDays`, `comp_table_id`, `order_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`, `server_sync` FROM `purchase_order_info` WHERE `server_sync` != 'Done'";
            rs = objStmt.executeQuery(query);
            while (rs.next())
            {
                foundBackup = true;
                temp = new ArrayList<>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2) == null?"0":rs.getString(2));
                temp.add(rs.getString(3) == null?"":rs.getString(3));
                temp.add(rs.getString(4) == null?"0":rs.getString(4));
                temp.add(rs.getString(5) == null?"0":rs.getString(5));
                temp.add(rs.getString(6) == null?"0":rs.getString(6));
                temp.add(rs.getString(7) == null?"0":rs.getString(7));
                temp.add(rs.getString(8) == null?"":rs.getString(8));
                temp.add(rs.getString(9) == null?"0":rs.getString(9));
                temp.add(rs.getString(10) == null?"":rs.getString(10));
                temp.add(rs.getString(11) == null?"":rs.getString(11));
                temp.add(rs.getString(12) == null?"0":rs.getString(12));
                temp.add(rs.getString(13) == null?"":rs.getString(13));
                temp.add(rs.getString(14) == null?"":rs.getString(14));
                temp.add(rs.getString(15) == null?"0":rs.getString(15));
                data.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    public ArrayList<ArrayList<String>> getPurchaseReturnData()
    {
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        ArrayList<String> temp = new ArrayList<>();
        ResultSet rs = null;
        try {
            String query = "SELECT `returnTable_id`, `return_id`, `purchInvoNumb`, `supplier_id`, `return_total_price`, `return_gross_price`, `entry_date`, `entry_time`, `update_date`, `update_time`, `user_id`, `comments`, `status`, `server_sync` FROM `purchase_return` WHERE `server_sync` != 'Done'";
            rs = objStmt.executeQuery(query);
            while (rs.next())
            {
                foundBackup = true;
                temp = new ArrayList<>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2) == null?"0":rs.getString(2));
                temp.add(rs.getString(3) == null?"":rs.getString(3));
                temp.add(rs.getString(4) == null?"0":rs.getString(4));
                temp.add(rs.getString(5) == null?"0":rs.getString(5));
                temp.add(rs.getString(6) == null?"0":rs.getString(6));
                temp.add(rs.getString(7) == null?"":rs.getString(7));
                temp.add(rs.getString(8) == null?"":rs.getString(8));
                temp.add(rs.getString(9) == null?"":rs.getString(9));
                temp.add(rs.getString(10) == null?"":rs.getString(10));
                temp.add(rs.getString(11) == null?"0":rs.getString(11));
                temp.add(rs.getString(12) == null?"":rs.getString(12));
                temp.add(rs.getString(13) == null?"":rs.getString(13));
                temp.add(rs.getString(14) == null?"0":rs.getString(14));
                data.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    public ArrayList<ArrayList<String>> getPurchaseReturnDetailData()
    {
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        ArrayList<String> temp = new ArrayList<>();
        ResultSet rs = null;
        try {
            String query = "SELECT `return_detail_id`, `return_id`, `prod_id`, `prod_batch`, `prod_quant`, `bonus_quant`, `discount_amount`, `total_amount`, `unit`, `return_reason`, `server_sync` FROM `purchase_return_detail` WHERE `server_sync` != 'Done'";
            rs = objStmt.executeQuery(query);
            while (rs.next())
            {
                foundBackup = true;
                temp = new ArrayList<>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2) == null?"0":rs.getString(2));
                temp.add(rs.getString(3) == null?"0":rs.getString(3));
                temp.add(rs.getString(4) == null?"":rs.getString(4));
                temp.add(rs.getString(5) == null?"0":rs.getString(5));
                temp.add(rs.getString(6) == null?"0":rs.getString(6));
                temp.add(rs.getString(7) == null?"0":rs.getString(7));
                temp.add(rs.getString(8) == null?"0":rs.getString(8));
                temp.add(rs.getString(9) == null?"":rs.getString(9));
                temp.add(rs.getString(10) == null?"":rs.getString(10));
                temp.add(rs.getString(11) == null?"0":rs.getString(11));
                data.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    public ArrayList<ArrayList<String>> getPurchaseReturnInfoDetailData()
    {
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        ArrayList<String> temp = new ArrayList<>();
        ResultSet rs = null;
        try {
            String query = "SELECT `return_id`, `supplier_id`, `purch_invo_num`, `return_date`, `prod_id`, `return_quant`, `discount_percent`, `bonus_quantity`, `total_amount`, `batch_no`, `gross_amount`, `server_sync` FROM `purchase_return_info_detail` WHERE `server_sync` != 'Done'";
            rs = objStmt.executeQuery(query);
            while (rs.next())
            {
                foundBackup = true;
                temp = new ArrayList<>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2) == null?"0":rs.getString(2));
                temp.add(rs.getString(3) == null?"":rs.getString(3));
                temp.add(rs.getString(4) == null?"":rs.getString(4));
                temp.add(rs.getString(5) == null?"0":rs.getString(5));
                temp.add(rs.getString(6) == null?"0":rs.getString(6));
                temp.add(rs.getString(7) == null?"0":rs.getString(7));
                temp.add(rs.getString(8) == null?"0":rs.getString(8));
                temp.add(rs.getString(9) == null?"0":rs.getString(9));
                temp.add(rs.getString(10) == null?"":rs.getString(10));
                temp.add(rs.getString(11) == null?"0":rs.getString(11));
                temp.add(rs.getString(12) == null?"0":rs.getString(12));
                data.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    public ArrayList<ArrayList<String>> getPurchReturnInfoData()
    {
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        ArrayList<String> temp = new ArrayList<>();
        ResultSet rs = null;
        try {
            String query = "SELECT `purch_return_no`, `purch_invo_num`, `return_date`, `supplier_id`, `total_amount`, `discount_amount`, `server_sync` FROM `purch_return_info` WHERE `server_sync` != 'Done'";
            rs = objStmt.executeQuery(query);
            while (rs.next())
            {
                foundBackup = true;
                temp = new ArrayList<>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2) == null?"":rs.getString(2));
                temp.add(rs.getString(3) == null?"":rs.getString(3));
                temp.add(rs.getString(4) == null?"0":rs.getString(4));
                temp.add(rs.getString(5) == null?"0":rs.getString(5));
                temp.add(rs.getString(6) == null?"0":rs.getString(6));
                temp.add(rs.getString(7) == null?"0":rs.getString(7));
                data.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    public ArrayList<ArrayList<String>> getSalesmanDesignatedCitiesData()
    {
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        ArrayList<String> temp = new ArrayList<>();
        ResultSet rs = null;
        try {
            String query = "SELECT `salesman_designated_id`, `salesman_table_id`, `designated_city_id`, `start_date`, `end_date`, `designated_status`, `server_sync` FROM `salesman_designated_cities` WHERE `server_sync` != 'Done'";
            rs = objStmt.executeQuery(query);
            while (rs.next())
            {
                foundBackup = true;
                temp = new ArrayList<>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2) == null?"0":rs.getString(2));
                temp.add(rs.getString(3) == null?"0":rs.getString(3));
                temp.add(rs.getString(4) == null?"":rs.getString(4));
                temp.add(rs.getString(5) == null?"":rs.getString(5));
                temp.add(rs.getString(6) == null?"":rs.getString(6));
                temp.add(rs.getString(7) == null?"0":rs.getString(7));
                data.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    public ArrayList<ArrayList<String>> getSaleInvoicePrintInfoData()
    {
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        ArrayList<String> temp = new ArrayList<>();
        ResultSet rs = null;
        try {
            String query = "SELECT `sale_print_id`, `header_1`, `header_2`, `header_3`, `header_4`, `header_5`, `owner_ids`, `owner_name`, `father_name`, `owner_cnic`, `owner_country`, `business_name`, `business_address`, `business_city`, `footer_1`, `footer_2`, `footer_3`, `footer_4`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`, `status`, `server_sync` FROM `sale_invoice_print_info` WHERE `server_sync` != 'Done'";
            rs = objStmt.executeQuery(query);
            while (rs.next())
            {
                foundBackup = true;
                temp = new ArrayList<>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2) == null?"":rs.getString(2));
                temp.add(rs.getString(3) == null?"":rs.getString(3));
                temp.add(rs.getString(4) == null?"":rs.getString(4));
                temp.add(rs.getString(5) == null?"":rs.getString(5));
                temp.add(rs.getString(6) == null?"":rs.getString(6));
                temp.add(rs.getString(7) == null?"0":rs.getString(7));
                temp.add(rs.getString(8) == null?"":rs.getString(8));
                temp.add(rs.getString(9) == null?"":rs.getString(9));
                temp.add(rs.getString(10) == null?"":rs.getString(10));
                temp.add(rs.getString(11) == null?"":rs.getString(11));
                temp.add(rs.getString(12) == null?"":rs.getString(12));
                temp.add(rs.getString(13) == null?"":rs.getString(13));
                temp.add(rs.getString(14) == null?"":rs.getString(14));
                temp.add(rs.getString(15) == null?"":rs.getString(15));
                temp.add(rs.getString(16) == null?"":rs.getString(16));
                temp.add(rs.getString(17) == null?"":rs.getString(17));
                temp.add(rs.getString(18) == null?"":rs.getString(18));
                temp.add(rs.getString(19) == null?"0":rs.getString(19));
                temp.add(rs.getString(20) == null?"":rs.getString(20));
                temp.add(rs.getString(21) == null?"":rs.getString(21));
                temp.add(rs.getString(22) == null?"0":rs.getString(22));
                temp.add(rs.getString(23) == null?"":rs.getString(23));
                temp.add(rs.getString(24) == null?"":rs.getString(24));
                temp.add(rs.getString(25) == null?"":rs.getString(25));
                temp.add(rs.getString(26) == null?"0":rs.getString(26));
                data.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    public ArrayList<ArrayList<String>> getSupplierCompanyData()
    {
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        ArrayList<String> temp = new ArrayList<>();
        ResultSet rs = null;
        try {
            String query = "SELECT `supplier_company_id`, `supplier_id`, `company_id`, `updated_date`, `user_id`, `status`, `server_sync` FROM `supplier_company` WHERE `server_sync` != 'Done'";
            rs = objStmt.executeQuery(query);
            while (rs.next())
            {
                foundBackup = true;
                temp = new ArrayList<>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2) == null?"0":rs.getString(2));
                temp.add(rs.getString(3) == null?"0":rs.getString(3));
                temp.add(rs.getString(4) == null?"":rs.getString(4));
                temp.add(rs.getString(5) == null?"0":rs.getString(5));
                temp.add(rs.getString(6) == null?"":rs.getString(6));
                temp.add(rs.getString(7) == null?"0":rs.getString(7));
                data.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    public ArrayList<ArrayList<String>> getSupplierInfoData()
    {
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        ArrayList<String> temp = new ArrayList<>();
        ResultSet rs = null;
        try {
            String query = "SELECT `supplier_table_id`, `supplier_id`, `supplier_name`, `supplier_email`, `supplier_contact`, `contact_person`, `supplier_address`, `supplier_city`, `supplier_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`, `server_sync` FROM `supplier_info` WHERE `server_sync` != 'Done'";
            rs = objStmt.executeQuery(query);
            while (rs.next())
            {
                foundBackup = true;
                temp = new ArrayList<>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2) == null?"0":rs.getString(2));
                temp.add(rs.getString(3) == null?"":rs.getString(3));
                temp.add(rs.getString(4) == null?"":rs.getString(4));
                temp.add(rs.getString(5) == null?"":rs.getString(5));
                temp.add(rs.getString(6) == null?"":rs.getString(6));
                temp.add(rs.getString(7) == null?"":rs.getString(7));
                temp.add(rs.getString(8) == null?"":rs.getString(8));
                temp.add(rs.getString(9) == null?"":rs.getString(9));
                temp.add(rs.getString(10) == null?"0":rs.getString(10));
                temp.add(rs.getString(11) == null?"":rs.getString(11));
                temp.add(rs.getString(12) == null?"":rs.getString(12));
                temp.add(rs.getString(13) == null?"0":rs.getString(13));
                temp.add(rs.getString(14) == null?"":rs.getString(14));
                temp.add(rs.getString(15) == null?"":rs.getString(15));
                temp.add(rs.getString(16) == null?"0":rs.getString(16));
                data.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    public ArrayList<ArrayList<String>> getSyncTrackingData()
    {
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        ArrayList<String> temp = new ArrayList<>();
        ResultSet rs = null;
        try {
            String query = "SELECT `sync_id`, `user_id`, `dealer_info`, `area_info`, `product_info`, `discount_info`, `bonus_info`, `server_sync` FROM `sync_tracking` WHERE `server_sync` != 'Done'";
            rs = objStmt.executeQuery(query);
            while (rs.next())
            {
                foundBackup = true;
                temp = new ArrayList<>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2) == null?"0":rs.getString(2));
                temp.add(rs.getString(3) == null?"0":rs.getString(3));
                temp.add(rs.getString(4) == null?"0":rs.getString(4));
                temp.add(rs.getString(5) == null?"0":rs.getString(5));
                temp.add(rs.getString(6) == null?"0":rs.getString(6));
                temp.add(rs.getString(7) == null?"0":rs.getString(7));
                temp.add(rs.getString(8) == null?"0":rs.getString(8));
                data.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    public ArrayList<ArrayList<String>> getSystemSettingsData()
    {
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        ArrayList<String> temp = new ArrayList<>();
        ResultSet rs = null;
        try {
            String query = "SELECT `setting_id`, `setting_name`, `setting_value`, `server_sync` FROM `system_settings` WHERE `server_sync` != 'Done'";
            rs = objStmt.executeQuery(query);
            while (rs.next())
            {
                foundBackup = true;
                temp = new ArrayList<>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2) == null?"":rs.getString(2));
                temp.add(rs.getString(3) == null?"":rs.getString(3));
                temp.add(rs.getString(4) == null?"0":rs.getString(4));
                data.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    public ArrayList<ArrayList<String>> getUserAccountsData()
    {
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        ArrayList<String> temp = new ArrayList<>();
        ResultSet rs = null;
        try {
            String query = "SELECT `account_id`, `user_name`, `user_id`, `user_password`, `user_rights`, `reg_date`, `current_status`, `user_status`, `server_sync` FROM `user_accounts` WHERE `server_sync` != 'Done'";
            rs = objStmt.executeQuery(query);
            while (rs.next())
            {
                foundBackup = true;
                temp = new ArrayList<>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2) == null?"":rs.getString(2));
                temp.add(rs.getString(3) == null?"0":rs.getString(3));
                temp.add(rs.getString(4) == null?"":rs.getString(4));
                temp.add(rs.getString(5) == null?"0":rs.getString(5));
                temp.add(rs.getString(6) == null?"":rs.getString(6));
                temp.add(rs.getString(7) == null?"":rs.getString(7));
                temp.add(rs.getString(8) == null?"":rs.getString(8));
                temp.add(rs.getString(9) == null?"0":rs.getString(9));
                data.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    public ArrayList<ArrayList<String>> getUserInfoData()
    {
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        ArrayList<String> temp = new ArrayList<>();
        ResultSet rs = null;
        try {
            String query = "SELECT `user_table_id`, `user_id`, `user_name`, `user_contact`, `user_address`, `user_cnic`, `user_type`, `user_image`, `user_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`, `server_sync` FROM `user_info` WHERE `server_sync` != 'Done'";
            rs = objStmt.executeQuery(query);
            while (rs.next())
            {
                foundBackup = true;
                temp = new ArrayList<>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2) == null?"0":rs.getString(2));
                temp.add(rs.getString(3) == null?"":rs.getString(3));
                temp.add(rs.getString(4) == null?"":rs.getString(4));
                temp.add(rs.getString(5) == null?"":rs.getString(5));
                temp.add(rs.getString(6) == null?"":rs.getString(6));
                temp.add(rs.getString(7) == null?"":rs.getString(7));
                temp.add(rs.getString(8) == null?"":rs.getString(8));
                temp.add(rs.getString(9) == null?"":rs.getString(9));
                temp.add(rs.getString(10) == null?"0":rs.getString(10));
                temp.add(rs.getString(11) == null?"":rs.getString(11));
                temp.add(rs.getString(12) == null?"":rs.getString(12));
                temp.add(rs.getString(13) == null?"0":rs.getString(13));
                temp.add(rs.getString(14) == null?"":rs.getString(14));
                temp.add(rs.getString(15) == null?"":rs.getString(15));
                temp.add(rs.getString(16) == null?"0":rs.getString(16));
                data.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    public ArrayList<ArrayList<String>> getUserRightsData()
    {
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        ArrayList<String> temp = new ArrayList<>();
        ResultSet rs = null;
        try {
            String query = "SELECT `rights_id`, `user_id`, `create_record`, `read_record`, `edit_record`, `delete_record`, `manage_stock`, `manage_cash`, `manage_settings`, `mobile_app`, `server_sync` FROM `user_rights` WHERE `server_sync` != 'Done'";
            rs = objStmt.executeQuery(query);
            while (rs.next())
            {
                foundBackup = true;
                temp = new ArrayList<>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2) == null?"0":rs.getString(2));
                temp.add(rs.getString(3) == null?"0":rs.getString(3));
                temp.add(rs.getString(4) == null?"0":rs.getString(4));
                temp.add(rs.getString(5) == null?"0":rs.getString(5));
                temp.add(rs.getString(6) == null?"0":rs.getString(6));
                temp.add(rs.getString(7) == null?"0":rs.getString(7));
                temp.add(rs.getString(8) == null?"0":rs.getString(8));
                temp.add(rs.getString(9) == null?"0":rs.getString(9));
                temp.add(rs.getString(10) == null?"0":rs.getString(10));
                temp.add(rs.getString(11) == null?"0":rs.getString(11));
                data.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }



//    ------------------------------ Get Local Mysql Data End ------------------------------
//    ------------------------------ Get Local Mysql Data End ------------------------------
//    ------------------------------ Get Local Mysql Data End ------------------------------

//    ------------------------------ Send Data to Server Start ------------------------------
//    ------------------------------ Send Data to Server Start ------------------------------
//    ------------------------------ Send Data to Server Start ------------------------------

    public String sendAreaData()
    {
        String operation = "sync_area_info";
        try {
            URL objurl = new URL(webURL);
            HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
            objhttpurlconnection.setConnectTimeout(5 * 1000);
            objhttpurlconnection.connect();
            if (objhttpurlconnection.getResponseCode() == 200)
            {
                objhttpurlconnection.disconnect();
                objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("operation", "UTF-8") +"="+URLEncoder.encode(operation, "UTF-8")+"&"+
                        URLEncoder.encode("area_table_id", "UTF-8") +"="+URLEncoder.encode(areaData.get(0), "UTF-8")+"&"+
                        URLEncoder.encode("area_id", "UTF-8") +"="+URLEncoder.encode(areaData.get(1), "UTF-8")+"&"+
                        URLEncoder.encode("area_name", "UTF-8") +"="+URLEncoder.encode(areaData.get(2), "UTF-8")+"&"+
                        URLEncoder.encode("area_abbrev", "UTF-8") +"="+URLEncoder.encode(areaData.get(3), "UTF-8")+"&"+
                        URLEncoder.encode("city_table_id", "UTF-8") +"="+URLEncoder.encode(areaData.get(4), "UTF-8")+"&"+
                        URLEncoder.encode("area_status", "UTF-8") +"="+URLEncoder.encode(areaData.get(5), "UTF-8")+"&"+
                        URLEncoder.encode("creating_user_id", "UTF-8") +"="+URLEncoder.encode(areaData.get(6), "UTF-8")+"&"+
                        URLEncoder.encode("creating_date", "UTF-8") +"="+URLEncoder.encode(areaData.get(7), "UTF-8")+"&"+
                        URLEncoder.encode("creating_time", "UTF-8") +"="+URLEncoder.encode(areaData.get(8), "UTF-8")+"&"+
                        URLEncoder.encode("update_user_id", "UTF-8") +"="+URLEncoder.encode(areaData.get(9), "UTF-8")+"&"+
                        URLEncoder.encode("update_date", "UTF-8") +"="+URLEncoder.encode(areaData.get(10), "UTF-8")+"&"+
                        URLEncoder.encode("update_time", "UTF-8") +"="+URLEncoder.encode(areaData.get(11), "UTF-8")+"&"+
                        URLEncoder.encode("server_sync", "UTF-8") +"="+URLEncoder.encode(areaData.get(12), "UTF-8")+"&"+
                        URLEncoder.encode("software_id", "UTF-8") +"="+URLEncoder.encode(GlobalVariables.softwareLicId, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                String orderResponse;
                response = "";
                while ((orderResponse = bufferedReader.readLine()) != null)
                {
                    response += orderResponse;
                }
                objhttpurlconnection.disconnect();
            }
            else
            {
                response = "Unable to Connect To Server";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String sendBatchwiseStockData()
    {
        String operation = "sync_batchwise_stock";
        try {
            URL objurl = new URL(webURL);
            HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
            objhttpurlconnection.setConnectTimeout(5 * 1000);
            objhttpurlconnection.connect();
            if (objhttpurlconnection.getResponseCode() == 200)
            {
                objhttpurlconnection.disconnect();
                objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("operation", "UTF-8") +"="+URLEncoder.encode(operation, "UTF-8")+"&"+
                        URLEncoder.encode("batch_stock_id", "UTF-8") +"="+URLEncoder.encode(batchwiseStockData.get(0), "UTF-8")+"&"+
                        URLEncoder.encode("product_table_id", "UTF-8") +"="+URLEncoder.encode(batchwiseStockData.get(1), "UTF-8")+"&"+
                        URLEncoder.encode("batch_no", "UTF-8") +"="+URLEncoder.encode(batchwiseStockData.get(2), "UTF-8")+"&"+
                        URLEncoder.encode("quantity", "UTF-8") +"="+URLEncoder.encode(batchwiseStockData.get(3), "UTF-8")+"&"+
                        URLEncoder.encode("bonus", "UTF-8") +"="+URLEncoder.encode(batchwiseStockData.get(4), "UTF-8")+"&"+
                        URLEncoder.encode("retail_price", "UTF-8") +"="+URLEncoder.encode(batchwiseStockData.get(5), "UTF-8")+"&"+
                        URLEncoder.encode("trade_price", "UTF-8") +"="+URLEncoder.encode(batchwiseStockData.get(6), "UTF-8")+"&"+
                        URLEncoder.encode("purchase_price", "UTF-8") +"="+URLEncoder.encode(batchwiseStockData.get(7), "UTF-8")+"&"+
                        URLEncoder.encode("batch_expiry", "UTF-8") +"="+URLEncoder.encode(batchwiseStockData.get(8), "UTF-8")+"&"+
                        URLEncoder.encode("entry_date", "UTF-8") +"="+URLEncoder.encode(batchwiseStockData.get(9), "UTF-8")+"&"+
                        URLEncoder.encode("entry_time", "UTF-8") +"="+URLEncoder.encode(batchwiseStockData.get(10), "UTF-8")+"&"+
                        URLEncoder.encode("server_sync", "UTF-8") +"="+URLEncoder.encode(batchwiseStockData.get(12), "UTF-8")+"&"+
                        URLEncoder.encode("software_id", "UTF-8") +"="+URLEncoder.encode(GlobalVariables.softwareLicId, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                String orderResponse;
                response = "";
                while ((orderResponse = bufferedReader.readLine()) != null)
                {
                    response += orderResponse;
                }
                objhttpurlconnection.disconnect();
            }
            else
            {
                response = "Unable to Connect To Server";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String sendBonusPolicyData()
    {
        String operation = "sync_bonus_policy";
        try {
            URL objurl = new URL(webURL);
            HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
            objhttpurlconnection.setConnectTimeout(5 * 1000);
            objhttpurlconnection.connect();
            if (objhttpurlconnection.getResponseCode() == 200)
            {
                objhttpurlconnection.disconnect();
                objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("operation", "UTF-8") +"="+URLEncoder.encode(operation, "UTF-8")+"&"+
                        URLEncoder.encode("bonus_policyID", "UTF-8") +"="+URLEncoder.encode(bonusPolicyData.get(0), "UTF-8")+"&"+
                        URLEncoder.encode("approval_id", "UTF-8") +"="+URLEncoder.encode(bonusPolicyData.get(1), "UTF-8")+"&"+
                        URLEncoder.encode("dealer_table_id", "UTF-8") +"="+URLEncoder.encode(bonusPolicyData.get(2), "UTF-8")+"&"+
                        URLEncoder.encode("company_table_id", "UTF-8") +"="+URLEncoder.encode(bonusPolicyData.get(3), "UTF-8")+"&"+
                        URLEncoder.encode("product_table_id", "UTF-8") +"="+URLEncoder.encode(bonusPolicyData.get(4), "UTF-8")+"&"+
                        URLEncoder.encode("quantity", "UTF-8") +"="+URLEncoder.encode(bonusPolicyData.get(5), "UTF-8")+"&"+
                        URLEncoder.encode("bonus_quant", "UTF-8") +"="+URLEncoder.encode(bonusPolicyData.get(6), "UTF-8")+"&"+
                        URLEncoder.encode("start_date", "UTF-8") +"="+URLEncoder.encode(bonusPolicyData.get(7), "UTF-8")+"&"+
                        URLEncoder.encode("end_date", "UTF-8") +"="+URLEncoder.encode(bonusPolicyData.get(8), "UTF-8")+"&"+
                        URLEncoder.encode("policy_status", "UTF-8") +"="+URLEncoder.encode(bonusPolicyData.get(9), "UTF-8")+"&"+
                        URLEncoder.encode("entered_by", "UTF-8") +"="+URLEncoder.encode(bonusPolicyData.get(10), "UTF-8")+"&"+
                        URLEncoder.encode("entry_date", "UTF-8") +"="+URLEncoder.encode(bonusPolicyData.get(11), "UTF-8")+"&"+
                        URLEncoder.encode("entry_time", "UTF-8") +"="+URLEncoder.encode(bonusPolicyData.get(12), "UTF-8")+"&"+
                        URLEncoder.encode("updated_by", "UTF-8") +"="+URLEncoder.encode(bonusPolicyData.get(13), "UTF-8")+"&"+
                        URLEncoder.encode("update_date", "UTF-8") +"="+URLEncoder.encode(bonusPolicyData.get(14), "UTF-8")+"&"+
                        URLEncoder.encode("update_time", "UTF-8") +"="+URLEncoder.encode(bonusPolicyData.get(15), "UTF-8")+"&"+
                        URLEncoder.encode("server_sync", "UTF-8") +"="+URLEncoder.encode(bonusPolicyData.get(16), "UTF-8")+"&"+
                        URLEncoder.encode("software_id", "UTF-8") +"="+URLEncoder.encode(GlobalVariables.softwareLicId, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                String orderResponse;
                response = "";
                while ((orderResponse = bufferedReader.readLine()) != null)
                {
                    response += orderResponse;
                }
                objhttpurlconnection.disconnect();
            }
            else
            {
                response = "Unable to Connect To Server";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String sendCityInfoData()
    {
        String operation = "sync_city_info";
        try {
            URL objurl = new URL(webURL);
            HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
            objhttpurlconnection.setConnectTimeout(5 * 1000);
            objhttpurlconnection.connect();
            if (objhttpurlconnection.getResponseCode() == 200)
            {
                objhttpurlconnection.disconnect();
                objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("operation", "UTF-8") +"="+URLEncoder.encode(operation, "UTF-8")+"&"+
                        URLEncoder.encode("city_table_id", "UTF-8") +"="+URLEncoder.encode(cityInfoData.get(0), "UTF-8")+"&"+
                        URLEncoder.encode("city_id", "UTF-8") +"="+URLEncoder.encode(cityInfoData.get(1), "UTF-8")+"&"+
                        URLEncoder.encode("district_table_id", "UTF-8") +"="+URLEncoder.encode(cityInfoData.get(2), "UTF-8")+"&"+
                        URLEncoder.encode("city_name", "UTF-8") +"="+URLEncoder.encode(cityInfoData.get(3), "UTF-8")+"&"+
                        URLEncoder.encode("city_status", "UTF-8") +"="+URLEncoder.encode(cityInfoData.get(4), "UTF-8")+"&"+
                        URLEncoder.encode("creating_user_id", "UTF-8") +"="+URLEncoder.encode(cityInfoData.get(5), "UTF-8")+"&"+
                        URLEncoder.encode("creating_date", "UTF-8") +"="+URLEncoder.encode(cityInfoData.get(6), "UTF-8")+"&"+
                        URLEncoder.encode("creating_time", "UTF-8") +"="+URLEncoder.encode(cityInfoData.get(7), "UTF-8")+"&"+
                        URLEncoder.encode("update_user_id", "UTF-8") +"="+URLEncoder.encode(cityInfoData.get(8), "UTF-8")+"&"+
                        URLEncoder.encode("update_date", "UTF-8") +"="+URLEncoder.encode(cityInfoData.get(9), "UTF-8")+"&"+
                        URLEncoder.encode("update_time", "UTF-8") +"="+URLEncoder.encode(cityInfoData.get(10), "UTF-8")+"&"+
                        URLEncoder.encode("server_sync", "UTF-8") +"="+URLEncoder.encode(cityInfoData.get(11), "UTF-8")+"&"+
                        URLEncoder.encode("software_id", "UTF-8") +"="+URLEncoder.encode(GlobalVariables.softwareLicId, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                String orderResponse;
                response = "";
                while ((orderResponse = bufferedReader.readLine()) != null)
                {
                    response += orderResponse;
                }
                objhttpurlconnection.disconnect();
            }
            else
            {
                response = "Unable to Connect To Server";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String sendCompanyAlternateContactsData()
    {
        String operation = "sync_company_alternate_contacts";
        try {
            URL objurl = new URL(webURL);
            HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
            objhttpurlconnection.setConnectTimeout(5 * 1000);
            objhttpurlconnection.connect();
            if (objhttpurlconnection.getResponseCode() == 200)
            {
                objhttpurlconnection.disconnect();
                objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("operation", "UTF-8") +"="+URLEncoder.encode(operation, "UTF-8")+"&"+
                        URLEncoder.encode("contact_id", "UTF-8") +"="+URLEncoder.encode(companyAlternateContactsData.get(0), "UTF-8")+"&"+
                        URLEncoder.encode("company_id", "UTF-8") +"="+URLEncoder.encode(companyAlternateContactsData.get(1), "UTF-8")+"&"+
                        URLEncoder.encode("contact_name", "UTF-8") +"="+URLEncoder.encode(companyAlternateContactsData.get(2), "UTF-8")+"&"+
                        URLEncoder.encode("contact_number", "UTF-8") +"="+URLEncoder.encode(companyAlternateContactsData.get(3), "UTF-8")+"&"+
                        URLEncoder.encode("server_sync", "UTF-8") +"="+URLEncoder.encode(companyAlternateContactsData.get(4), "UTF-8")+"&"+
                        URLEncoder.encode("software_id", "UTF-8") +"="+URLEncoder.encode(GlobalVariables.softwareLicId, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                String orderResponse;
                response = "";
                while ((orderResponse = bufferedReader.readLine()) != null)
                {
                    response += orderResponse;
                }
                objhttpurlconnection.disconnect();
            }
            else
            {
                response = "Unable to Connect To Server";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String sendCompanyInfoData()
    {
        String operation = "sync_company_info";
        try {
            URL objurl = new URL(webURL);
            HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
            objhttpurlconnection.setConnectTimeout(5 * 1000);
            objhttpurlconnection.connect();
            if (objhttpurlconnection.getResponseCode() == 200)
            {
                objhttpurlconnection.disconnect();
                objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("operation", "UTF-8") +"="+URLEncoder.encode(operation, "UTF-8")+"&"+
                        URLEncoder.encode("company_table_id", "UTF-8") +"="+URLEncoder.encode(companyInfoData.get(0), "UTF-8")+"&"+
                        URLEncoder.encode("company_id", "UTF-8") +"="+URLEncoder.encode(companyInfoData.get(1), "UTF-8")+"&"+
                        URLEncoder.encode("company_name", "UTF-8") +"="+URLEncoder.encode(companyInfoData.get(2), "UTF-8")+"&"+
                        URLEncoder.encode("company_address", "UTF-8") +"="+URLEncoder.encode(companyInfoData.get(3), "UTF-8")+"&"+
                        URLEncoder.encode("company_city", "UTF-8") +"="+URLEncoder.encode(companyInfoData.get(4), "UTF-8")+"&"+
                        URLEncoder.encode("company_contact", "UTF-8") +"="+URLEncoder.encode(companyInfoData.get(5), "UTF-8")+"&"+
                        URLEncoder.encode("contact_Person", "UTF-8") +"="+URLEncoder.encode(companyInfoData.get(6), "UTF-8")+"&"+
                        URLEncoder.encode("company_email", "UTF-8") +"="+URLEncoder.encode(companyInfoData.get(7), "UTF-8")+"&"+
                        URLEncoder.encode("company_image", "UTF-8") +"="+URLEncoder.encode(companyInfoData.get(8), "UTF-8")+"&"+
                        URLEncoder.encode("company_status", "UTF-8") +"="+URLEncoder.encode(companyInfoData.get(9), "UTF-8")+"&"+
                        URLEncoder.encode("creating_user_id", "UTF-8") +"="+URLEncoder.encode(companyInfoData.get(10), "UTF-8")+"&"+
                        URLEncoder.encode("creating_date", "UTF-8") +"="+URLEncoder.encode(companyInfoData.get(11), "UTF-8")+"&"+
                        URLEncoder.encode("creating_time", "UTF-8") +"="+URLEncoder.encode(companyInfoData.get(12), "UTF-8")+"&"+
                        URLEncoder.encode("update_user_id", "UTF-8") +"="+URLEncoder.encode(companyInfoData.get(13), "UTF-8")+"&"+
                        URLEncoder.encode("update_date", "UTF-8") +"="+URLEncoder.encode(companyInfoData.get(14), "UTF-8")+"&"+
                        URLEncoder.encode("update_time", "UTF-8") +"="+URLEncoder.encode(companyInfoData.get(15), "UTF-8")+"&"+
                        URLEncoder.encode("server_sync", "UTF-8") +"="+URLEncoder.encode(companyInfoData.get(16), "UTF-8")+"&"+
                        URLEncoder.encode("software_id", "UTF-8") +"="+URLEncoder.encode(GlobalVariables.softwareLicId, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                String orderResponse;
                response = "";
                while ((orderResponse = bufferedReader.readLine()) != null)
                {
                    response += orderResponse;
                }
                objhttpurlconnection.disconnect();
            }
            else
            {
                response = "Unable to Connect To Server";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String sendCompanyOverallRecordData()
    {
        String operation = "sync_company_overall_record";
        try {
            URL objurl = new URL(webURL);
            HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
            objhttpurlconnection.setConnectTimeout(5 * 1000);
            objhttpurlconnection.connect();
            if (objhttpurlconnection.getResponseCode() == 200)
            {
                objhttpurlconnection.disconnect();
                objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("operation", "UTF-8") +"="+URLEncoder.encode(operation, "UTF-8")+"&"+
                        URLEncoder.encode("company_overall_id", "UTF-8") +"="+URLEncoder.encode(companyOverallRecordData.get(0), "UTF-8")+"&"+
                        URLEncoder.encode("company_id", "UTF-8") +"="+URLEncoder.encode(companyOverallRecordData.get(1), "UTF-8")+"&"+
                        URLEncoder.encode("orders_given", "UTF-8") +"="+URLEncoder.encode(companyOverallRecordData.get(2), "UTF-8")+"&"+
                        URLEncoder.encode("successful_orders", "UTF-8") +"="+URLEncoder.encode(companyOverallRecordData.get(3), "UTF-8")+"&"+
                        URLEncoder.encode("ordered_packets", "UTF-8") +"="+URLEncoder.encode(companyOverallRecordData.get(4), "UTF-8")+"&"+
                        URLEncoder.encode("received_packets", "UTF-8") +"="+URLEncoder.encode(companyOverallRecordData.get(5), "UTF-8")+"&"+
                        URLEncoder.encode("ordered_boxes", "UTF-8") +"="+URLEncoder.encode(companyOverallRecordData.get(6), "UTF-8")+"&"+
                        URLEncoder.encode("received_boxes", "UTF-8") +"="+URLEncoder.encode(companyOverallRecordData.get(7), "UTF-8")+"&"+
                        URLEncoder.encode("order_price", "UTF-8") +"="+URLEncoder.encode(companyOverallRecordData.get(8), "UTF-8")+"&"+
                        URLEncoder.encode("discount_price", "UTF-8") +"="+URLEncoder.encode(companyOverallRecordData.get(9), "UTF-8")+"&"+
                        URLEncoder.encode("invoiced_price", "UTF-8") +"="+URLEncoder.encode(companyOverallRecordData.get(10), "UTF-8")+"&"+
                        URLEncoder.encode("cash_sent", "UTF-8") +"="+URLEncoder.encode(companyOverallRecordData.get(11), "UTF-8")+"&"+
                        URLEncoder.encode("return_packets", "UTF-8") +"="+URLEncoder.encode(companyOverallRecordData.get(12), "UTF-8")+"&"+
                        URLEncoder.encode("return_boxes", "UTF-8") +"="+URLEncoder.encode(companyOverallRecordData.get(13), "UTF-8")+"&"+
                        URLEncoder.encode("return_price", "UTF-8") +"="+URLEncoder.encode(companyOverallRecordData.get(14), "UTF-8")+"&"+
                        URLEncoder.encode("cash_return", "UTF-8") +"="+URLEncoder.encode(companyOverallRecordData.get(15), "UTF-8")+"&"+
                        URLEncoder.encode("waived_off_price", "UTF-8") +"="+URLEncoder.encode(companyOverallRecordData.get(16), "UTF-8")+"&"+
                        URLEncoder.encode("pending_payments", "UTF-8") +"="+URLEncoder.encode(companyOverallRecordData.get(17), "UTF-8")+"&"+
                        URLEncoder.encode("server_sync", "UTF-8") +"="+URLEncoder.encode(companyOverallRecordData.get(18), "UTF-8")+"&"+
                        URLEncoder.encode("software_id", "UTF-8") +"="+URLEncoder.encode(GlobalVariables.softwareLicId, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                String orderResponse;
                response = "";
                while ((orderResponse = bufferedReader.readLine()) != null)
                {
                    response += orderResponse;
                }
                objhttpurlconnection.disconnect();
            }
            else
            {
                response = "Unable to Connect To Server";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String sendCompanyPaymentsData()
    {
        String operation = "sync_company_payments";
        try {
            URL objurl = new URL(webURL);
            HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
            objhttpurlconnection.setConnectTimeout(5 * 1000);
            objhttpurlconnection.connect();
            if (objhttpurlconnection.getResponseCode() == 200)
            {
                objhttpurlconnection.disconnect();
                objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("operation", "UTF-8") +"="+URLEncoder.encode(operation, "UTF-8")+"&"+
                        URLEncoder.encode("payment_id", "UTF-8") +"="+URLEncoder.encode(companyPaymentsData.get(0), "UTF-8")+"&"+
                        URLEncoder.encode("company_id", "UTF-8") +"="+URLEncoder.encode(companyPaymentsData.get(1), "UTF-8")+"&"+
                        URLEncoder.encode("invoice_id", "UTF-8") +"="+URLEncoder.encode(companyPaymentsData.get(2), "UTF-8")+"&"+
                        URLEncoder.encode("amount", "UTF-8") +"="+URLEncoder.encode(companyPaymentsData.get(3), "UTF-8")+"&"+
                        URLEncoder.encode("cash_type", "UTF-8") +"="+URLEncoder.encode(companyPaymentsData.get(4), "UTF-8")+"&"+
                        URLEncoder.encode("pending_payments", "UTF-8") +"="+URLEncoder.encode(companyPaymentsData.get(5), "UTF-8")+"&"+
                        URLEncoder.encode("date", "UTF-8") +"="+URLEncoder.encode(companyPaymentsData.get(6), "UTF-8")+"&"+
                        URLEncoder.encode("day", "UTF-8") +"="+URLEncoder.encode(companyPaymentsData.get(7), "UTF-8")+"&"+
                        URLEncoder.encode("user_id", "UTF-8") +"="+URLEncoder.encode(companyPaymentsData.get(8), "UTF-8")+"&"+
                        URLEncoder.encode("comments", "UTF-8") +"="+URLEncoder.encode(companyPaymentsData.get(9), "UTF-8")+"&"+
                        URLEncoder.encode("status", "UTF-8") +"="+URLEncoder.encode(companyPaymentsData.get(10), "UTF-8")+"&"+
                        URLEncoder.encode("server_sync", "UTF-8") +"="+URLEncoder.encode(companyPaymentsData.get(11), "UTF-8")+"&"+
                        URLEncoder.encode("software_id", "UTF-8") +"="+URLEncoder.encode(GlobalVariables.softwareLicId, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                String orderResponse;
                response = "";
                while ((orderResponse = bufferedReader.readLine()) != null)
                {
                    response += orderResponse;
                }
                objhttpurlconnection.disconnect();
            }
            else
            {
                response = "Unable to Connect To Server";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String sendDealerDaySummaryData()
    {
        String operation = "sync_dealer_day_summary";
        try {
            URL objurl = new URL(webURL);
            HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
            objhttpurlconnection.setConnectTimeout(5 * 1000);
            objhttpurlconnection.connect();
            if (objhttpurlconnection.getResponseCode() == 200)
            {
                objhttpurlconnection.disconnect();
                objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("operation", "UTF-8") +"="+URLEncoder.encode(operation, "UTF-8")+"&"+
                        URLEncoder.encode("dealer_summary_id", "UTF-8") +"="+URLEncoder.encode(dealerDaySummaryData.get(0), "UTF-8")+"&"+
                        URLEncoder.encode("dealer_id", "UTF-8") +"="+URLEncoder.encode(dealerDaySummaryData.get(1), "UTF-8")+"&"+
                        URLEncoder.encode("date", "UTF-8") +"="+URLEncoder.encode(dealerDaySummaryData.get(2), "UTF-8")+"&"+
                        URLEncoder.encode("day", "UTF-8") +"="+URLEncoder.encode(dealerDaySummaryData.get(3), "UTF-8")+"&"+
                        URLEncoder.encode("booking_order", "UTF-8") +"="+URLEncoder.encode(dealerDaySummaryData.get(4), "UTF-8")+"&"+
                        URLEncoder.encode("delivered_order", "UTF-8") +"="+URLEncoder.encode(dealerDaySummaryData.get(5), "UTF-8")+"&"+
                        URLEncoder.encode("returned_order", "UTF-8") +"="+URLEncoder.encode(dealerDaySummaryData.get(6), "UTF-8")+"&"+
                        URLEncoder.encode("return_quantity", "UTF-8") +"="+URLEncoder.encode(dealerDaySummaryData.get(7), "UTF-8")+"&"+
                        URLEncoder.encode("return_price", "UTF-8") +"="+URLEncoder.encode(dealerDaySummaryData.get(8), "UTF-8")+"&"+
                        URLEncoder.encode("cash_collection", "UTF-8") +"="+URLEncoder.encode(dealerDaySummaryData.get(9), "UTF-8")+"&"+
                        URLEncoder.encode("cash_return", "UTF-8") +"="+URLEncoder.encode(dealerDaySummaryData.get(10), "UTF-8")+"&"+
                        URLEncoder.encode("cash_waiveoff", "UTF-8") +"="+URLEncoder.encode(dealerDaySummaryData.get(11), "UTF-8")+"&"+
                        URLEncoder.encode("server_sync", "UTF-8") +"="+URLEncoder.encode(dealerDaySummaryData.get(12), "UTF-8")+"&"+
                        URLEncoder.encode("software_id", "UTF-8") +"="+URLEncoder.encode(GlobalVariables.softwareLicId, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                String orderResponse;
                response = "";
                while ((orderResponse = bufferedReader.readLine()) != null)
                {
                    response += orderResponse;
                }
                objhttpurlconnection.disconnect();
            }
            else
            {
                response = "Unable to Connect To Server";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String sendDealerGPSLocationData()
    {
        String operation = "sync_dealer_gps_location";
        try {
            URL objurl = new URL(webURL);
            HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
            objhttpurlconnection.setConnectTimeout(5 * 1000);
            objhttpurlconnection.connect();
            if (objhttpurlconnection.getResponseCode() == 200)
            {
                objhttpurlconnection.disconnect();
                objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("operation", "UTF-8") +"="+URLEncoder.encode(operation, "UTF-8")+"&"+
                        URLEncoder.encode("dealer_loc_id", "UTF-8") +"="+URLEncoder.encode(dealerGPSLocationData.get(0), "UTF-8")+"&"+
                        URLEncoder.encode("dealer_id", "UTF-8") +"="+URLEncoder.encode(dealerGPSLocationData.get(1), "UTF-8")+"&"+
                        URLEncoder.encode("latitude", "UTF-8") +"="+URLEncoder.encode(dealerGPSLocationData.get(2), "UTF-8")+"&"+
                        URLEncoder.encode("longitude", "UTF-8") +"="+URLEncoder.encode(dealerGPSLocationData.get(3), "UTF-8")+"&"+
                        URLEncoder.encode("loc_name", "UTF-8") +"="+URLEncoder.encode(dealerGPSLocationData.get(4), "UTF-8")+"&"+
                        URLEncoder.encode("entry_date", "UTF-8") +"="+URLEncoder.encode(dealerGPSLocationData.get(5), "UTF-8")+"&"+
                        URLEncoder.encode("entry_time", "UTF-8") +"="+URLEncoder.encode(dealerGPSLocationData.get(6), "UTF-8")+"&"+
                        URLEncoder.encode("update_date", "UTF-8") +"="+URLEncoder.encode(dealerGPSLocationData.get(7), "UTF-8")+"&"+
                        URLEncoder.encode("update_time", "UTF-8") +"="+URLEncoder.encode(dealerGPSLocationData.get(8), "UTF-8")+"&"+
                        URLEncoder.encode("user_id", "UTF-8") +"="+URLEncoder.encode(dealerGPSLocationData.get(9), "UTF-8")+"&"+
                        URLEncoder.encode("server_sync", "UTF-8") +"="+URLEncoder.encode(dealerGPSLocationData.get(10), "UTF-8")+"&"+
                        URLEncoder.encode("software_id", "UTF-8") +"="+URLEncoder.encode(GlobalVariables.softwareLicId, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                String orderResponse;
                response = "";
                while ((orderResponse = bufferedReader.readLine()) != null)
                {
                    response += orderResponse;
                }
                objhttpurlconnection.disconnect();
            }
            else
            {
                response = "Unable to Connect To Server";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String sendDealerInfoData()
    {
        String operation = "sync_dealer_info";
        try {
            URL objurl = new URL(webURL);
            HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
            objhttpurlconnection.setConnectTimeout(5 * 1000);
            objhttpurlconnection.connect();
            if (objhttpurlconnection.getResponseCode() == 200)
            {
                objhttpurlconnection.disconnect();
                objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("operation", "UTF-8") +"="+URLEncoder.encode(operation, "UTF-8")+"&"+
                        URLEncoder.encode("dealer_table_id", "UTF-8") +"="+URLEncoder.encode(dealerInfoData.get(0), "UTF-8")+"&"+
                        URLEncoder.encode("dealer_id", "UTF-8") +"="+URLEncoder.encode(dealerInfoData.get(1), "UTF-8")+"&"+
                        URLEncoder.encode("dealer_area_id", "UTF-8") +"="+URLEncoder.encode(dealerInfoData.get(2), "UTF-8")+"&"+
                        URLEncoder.encode("dealer_name", "UTF-8") +"="+URLEncoder.encode(dealerInfoData.get(3), "UTF-8")+"&"+
                        URLEncoder.encode("dealer_contact_person", "UTF-8") +"="+URLEncoder.encode(dealerInfoData.get(4), "UTF-8")+"&"+
                        URLEncoder.encode("dealer_phone", "UTF-8") +"="+URLEncoder.encode(dealerInfoData.get(5), "UTF-8")+"&"+
                        URLEncoder.encode("dealer_fax", "UTF-8") +"="+URLEncoder.encode(dealerInfoData.get(6), "UTF-8")+"&"+
                        URLEncoder.encode("dealer_address", "UTF-8") +"="+URLEncoder.encode(dealerInfoData.get(7), "UTF-8")+"&"+
                        URLEncoder.encode("dealer_address1", "UTF-8") +"="+URLEncoder.encode(dealerInfoData.get(8), "UTF-8")+"&"+
                        URLEncoder.encode("dealer_type", "UTF-8") +"="+URLEncoder.encode(dealerInfoData.get(9), "UTF-8")+"&"+
                        URLEncoder.encode("dealer_cnic", "UTF-8") +"="+URLEncoder.encode(dealerInfoData.get(10), "UTF-8")+"&"+
                        URLEncoder.encode("dealer_ntn", "UTF-8") +"="+URLEncoder.encode(dealerInfoData.get(11), "UTF-8")+"&"+
                        URLEncoder.encode("dealer_image", "UTF-8") +"="+URLEncoder.encode(dealerInfoData.get(12), "UTF-8")+"&"+
                        URLEncoder.encode("dealer_lic9_num", "UTF-8") +"="+URLEncoder.encode(dealerInfoData.get(13), "UTF-8")+"&"+
                        URLEncoder.encode("dealer_lic9_exp", "UTF-8") +"="+URLEncoder.encode(dealerInfoData.get(14), "UTF-8")+"&"+
                        URLEncoder.encode("dealer_lic10_num", "UTF-8") +"="+URLEncoder.encode(dealerInfoData.get(15), "UTF-8")+"&"+
                        URLEncoder.encode("dealer_lic10_Exp", "UTF-8") +"="+URLEncoder.encode(dealerInfoData.get(16), "UTF-8")+"&"+
                        URLEncoder.encode("dealer_lic11_num", "UTF-8") +"="+URLEncoder.encode(dealerInfoData.get(17), "UTF-8")+"&"+
                        URLEncoder.encode("dealer_lic11_Exp", "UTF-8") +"="+URLEncoder.encode(dealerInfoData.get(18), "UTF-8")+"&"+
                        URLEncoder.encode("dealer_credit_limit", "UTF-8") +"="+URLEncoder.encode(dealerInfoData.get(19), "UTF-8")+"&"+
                        URLEncoder.encode("dealer_status", "UTF-8") +"="+URLEncoder.encode(dealerInfoData.get(20), "UTF-8")+"&"+
                        URLEncoder.encode("creating_user_id", "UTF-8") +"="+URLEncoder.encode(dealerInfoData.get(21), "UTF-8")+"&"+
                        URLEncoder.encode("creating_date", "UTF-8") +"="+URLEncoder.encode(dealerInfoData.get(22), "UTF-8")+"&"+
                        URLEncoder.encode("creating_time", "UTF-8") +"="+URLEncoder.encode(dealerInfoData.get(23), "UTF-8")+"&"+
                        URLEncoder.encode("update_user_id", "UTF-8") +"="+URLEncoder.encode(dealerInfoData.get(24), "UTF-8")+"&"+
                        URLEncoder.encode("update_date", "UTF-8") +"="+URLEncoder.encode(dealerInfoData.get(25), "UTF-8")+"&"+
                        URLEncoder.encode("update_time", "UTF-8") +"="+URLEncoder.encode(dealerInfoData.get(26), "UTF-8")+"&"+
                        URLEncoder.encode("server_sync", "UTF-8") +"="+URLEncoder.encode(dealerInfoData.get(27), "UTF-8")+"&"+
                        URLEncoder.encode("software_id", "UTF-8") +"="+URLEncoder.encode(GlobalVariables.softwareLicId, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                String orderResponse;
                response = "";
                while ((orderResponse = bufferedReader.readLine()) != null)
                {
                    response += orderResponse;
                }
                objhttpurlconnection.disconnect();
            }
            else
            {
                response = "Unable to Connect To Server";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String sendDealerOverallRecordData()
    {
        String operation = "sync_dealer_overall_record";
        try {
            URL objurl = new URL(webURL);
            HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
            objhttpurlconnection.setConnectTimeout(5 * 1000);
            objhttpurlconnection.connect();
            if (objhttpurlconnection.getResponseCode() == 200)
            {
                objhttpurlconnection.disconnect();
                objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("operation", "UTF-8") +"="+URLEncoder.encode(operation, "UTF-8")+"&"+
                        URLEncoder.encode("dealer_overall_id", "UTF-8") +"="+URLEncoder.encode(dealerOverallRecordData.get(0), "UTF-8")+"&"+
                        URLEncoder.encode("dealer_id", "UTF-8") +"="+URLEncoder.encode(dealerOverallRecordData.get(1), "UTF-8")+"&"+
                        URLEncoder.encode("orders_given", "UTF-8") +"="+URLEncoder.encode(dealerOverallRecordData.get(2), "UTF-8")+"&"+
                        URLEncoder.encode("successful_orders", "UTF-8") +"="+URLEncoder.encode(dealerOverallRecordData.get(3), "UTF-8")+"&"+
                        URLEncoder.encode("ordered_packets", "UTF-8") +"="+URLEncoder.encode(dealerOverallRecordData.get(4), "UTF-8")+"&"+
                        URLEncoder.encode("submitted_packets", "UTF-8") +"="+URLEncoder.encode(dealerOverallRecordData.get(5), "UTF-8")+"&"+
                        URLEncoder.encode("ordered_boxes", "UTF-8") +"="+URLEncoder.encode(dealerOverallRecordData.get(6), "UTF-8")+"&"+
                        URLEncoder.encode("submitted_boxes", "UTF-8") +"="+URLEncoder.encode(dealerOverallRecordData.get(7), "UTF-8")+"&"+
                        URLEncoder.encode("order_price", "UTF-8") +"="+URLEncoder.encode(dealerOverallRecordData.get(8), "UTF-8")+"&"+
                        URLEncoder.encode("discount_price", "UTF-8") +"="+URLEncoder.encode(dealerOverallRecordData.get(9), "UTF-8")+"&"+
                        URLEncoder.encode("invoiced_price", "UTF-8") +"="+URLEncoder.encode(dealerOverallRecordData.get(10), "UTF-8")+"&"+
                        URLEncoder.encode("cash_collected", "UTF-8") +"="+URLEncoder.encode(dealerOverallRecordData.get(11), "UTF-8")+"&"+
                        URLEncoder.encode("return_packets", "UTF-8") +"="+URLEncoder.encode(dealerOverallRecordData.get(12), "UTF-8")+"&"+
                        URLEncoder.encode("return_boxes", "UTF-8") +"="+URLEncoder.encode(dealerOverallRecordData.get(13), "UTF-8")+"&"+
                        URLEncoder.encode("return_price", "UTF-8") +"="+URLEncoder.encode(dealerOverallRecordData.get(14), "UTF-8")+"&"+
                        URLEncoder.encode("cash_return", "UTF-8") +"="+URLEncoder.encode(dealerOverallRecordData.get(15), "UTF-8")+"&"+
                        URLEncoder.encode("waived_off_price", "UTF-8") +"="+URLEncoder.encode(dealerOverallRecordData.get(16), "UTF-8")+"&"+
                        URLEncoder.encode("pending_payments", "UTF-8") +"="+URLEncoder.encode(dealerOverallRecordData.get(17), "UTF-8")+"&"+
                        URLEncoder.encode("server_sync", "UTF-8") +"="+URLEncoder.encode(dealerOverallRecordData.get(18), "UTF-8")+"&"+
                        URLEncoder.encode("software_id", "UTF-8") +"="+URLEncoder.encode(GlobalVariables.softwareLicId, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                String orderResponse;
                response = "";
                while ((orderResponse = bufferedReader.readLine()) != null)
                {
                    response += orderResponse;
                }
                objhttpurlconnection.disconnect();
            }
            else
            {
                response = "Unable to Connect To Server";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String sendDealerPaymentsData()
    {
        String operation = "sync_dealer_payments";
        try {
            URL objurl = new URL(webURL);
            HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
            objhttpurlconnection.setConnectTimeout(5 * 1000);
            objhttpurlconnection.connect();
            if (objhttpurlconnection.getResponseCode() == 200)
            {
                objhttpurlconnection.disconnect();
                objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("operation", "UTF-8") +"="+URLEncoder.encode(operation, "UTF-8")+"&"+
                        URLEncoder.encode("payment_id", "UTF-8") +"="+URLEncoder.encode(dealerPaymentsData.get(0), "UTF-8")+"&"+
                        URLEncoder.encode("dealer_id", "UTF-8") +"="+URLEncoder.encode(dealerPaymentsData.get(1), "UTF-8")+"&"+
                        URLEncoder.encode("order_id", "UTF-8") +"="+URLEncoder.encode(dealerPaymentsData.get(2), "UTF-8")+"&"+
                        URLEncoder.encode("amount", "UTF-8") +"="+URLEncoder.encode(dealerPaymentsData.get(3), "UTF-8")+"&"+
                        URLEncoder.encode("cash_type", "UTF-8") +"="+URLEncoder.encode(dealerPaymentsData.get(4), "UTF-8")+"&"+
                        URLEncoder.encode("pending_payments", "UTF-8") +"="+URLEncoder.encode(dealerPaymentsData.get(5), "UTF-8")+"&"+
                        URLEncoder.encode("date", "UTF-8") +"="+URLEncoder.encode(dealerPaymentsData.get(6), "UTF-8")+"&"+
                        URLEncoder.encode("day", "UTF-8") +"="+URLEncoder.encode(dealerPaymentsData.get(7), "UTF-8")+"&"+
                        URLEncoder.encode("user_id", "UTF-8") +"="+URLEncoder.encode(dealerPaymentsData.get(8), "UTF-8")+"&"+
                        URLEncoder.encode("comments", "UTF-8") +"="+URLEncoder.encode(dealerPaymentsData.get(9), "UTF-8")+"&"+
                        URLEncoder.encode("status", "UTF-8") +"="+URLEncoder.encode(dealerPaymentsData.get(10), "UTF-8")+"&"+
                        URLEncoder.encode("server_id", "UTF-8") +"="+URLEncoder.encode(dealerPaymentsData.get(11), "UTF-8")+"&"+
                        URLEncoder.encode("server_sync", "UTF-8") +"="+URLEncoder.encode(dealerPaymentsData.get(12), "UTF-8")+"&"+
                        URLEncoder.encode("software_id", "UTF-8") +"="+URLEncoder.encode(GlobalVariables.softwareLicId, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                String orderResponse;
                response = "";
                while ((orderResponse = bufferedReader.readLine()) != null)
                {
                    response += orderResponse;
                }
                objhttpurlconnection.disconnect();
            }
            else
            {
                response = "Unable to Connect To Server";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String sendDealerTypesData()
    {
        String operation = "sync_dealer_types";
        try {
            URL objurl = new URL(webURL);
            HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
            objhttpurlconnection.setConnectTimeout(5 * 1000);
            objhttpurlconnection.connect();
            if (objhttpurlconnection.getResponseCode() == 200)
            {
                objhttpurlconnection.disconnect();
                objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("operation", "UTF-8") +"="+URLEncoder.encode(operation, "UTF-8")+"&"+
                        URLEncoder.encode("dealer_type_ID", "UTF-8") +"="+URLEncoder.encode(dealerTypesData.get(0), "UTF-8")+"&"+
                        URLEncoder.encode("typeID", "UTF-8") +"="+URLEncoder.encode(dealerTypesData.get(1), "UTF-8")+"&"+
                        URLEncoder.encode("dealerType_name", "UTF-8") +"="+URLEncoder.encode(dealerTypesData.get(2), "UTF-8")+"&"+
                        URLEncoder.encode("typeStatus", "UTF-8") +"="+URLEncoder.encode(dealerTypesData.get(3), "UTF-8")+"&"+
                        URLEncoder.encode("DeleteStatus", "UTF-8") +"="+URLEncoder.encode(dealerTypesData.get(4), "UTF-8")+"&"+
                        URLEncoder.encode("server_sync", "UTF-8") +"="+URLEncoder.encode(dealerTypesData.get(5), "UTF-8")+"&"+
                        URLEncoder.encode("software_id", "UTF-8") +"="+URLEncoder.encode(GlobalVariables.softwareLicId, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                String orderResponse;
                response = "";
                while ((orderResponse = bufferedReader.readLine()) != null)
                {
                    response += orderResponse;
                }
                objhttpurlconnection.disconnect();
            }
            else
            {
                response = "Unable to Connect To Server";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String sendDiscountPolicyData()
    {
        String operation = "sync_discount_policy";
        try {
            URL objurl = new URL(webURL);
            HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
            objhttpurlconnection.setConnectTimeout(5 * 1000);
            objhttpurlconnection.connect();
            if (objhttpurlconnection.getResponseCode() == 200)
            {
                objhttpurlconnection.disconnect();
                objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("operation", "UTF-8") +"="+URLEncoder.encode(operation, "UTF-8")+"&"+
                        URLEncoder.encode("disc_policyID", "UTF-8") +"="+URLEncoder.encode(discountPolicyData.get(0), "UTF-8")+"&"+
                        URLEncoder.encode("approval_id", "UTF-8") +"="+URLEncoder.encode(discountPolicyData.get(1), "UTF-8")+"&"+
                        URLEncoder.encode("dealer_table_id", "UTF-8") +"="+URLEncoder.encode(discountPolicyData.get(2), "UTF-8")+"&"+
                        URLEncoder.encode("company_table_id", "UTF-8") +"="+URLEncoder.encode(discountPolicyData.get(3), "UTF-8")+"&"+
                        URLEncoder.encode("product_table_id", "UTF-8") +"="+URLEncoder.encode(discountPolicyData.get(4), "UTF-8")+"&"+
                        URLEncoder.encode("sale_amount", "UTF-8") +"="+URLEncoder.encode(discountPolicyData.get(5), "UTF-8")+"&"+
                        URLEncoder.encode("discount_percent", "UTF-8") +"="+URLEncoder.encode(discountPolicyData.get(6), "UTF-8")+"&"+
                        URLEncoder.encode("start_date", "UTF-8") +"="+URLEncoder.encode(discountPolicyData.get(7), "UTF-8")+"&"+
                        URLEncoder.encode("end_date", "UTF-8") +"="+URLEncoder.encode(discountPolicyData.get(8), "UTF-8")+"&"+
                        URLEncoder.encode("policy_status", "UTF-8") +"="+URLEncoder.encode(discountPolicyData.get(9), "UTF-8")+"&"+
                        URLEncoder.encode("entered_by", "UTF-8") +"="+URLEncoder.encode(discountPolicyData.get(10), "UTF-8")+"&"+
                        URLEncoder.encode("entry_date", "UTF-8") +"="+URLEncoder.encode(discountPolicyData.get(11), "UTF-8")+"&"+
                        URLEncoder.encode("entry_time", "UTF-8") +"="+URLEncoder.encode(discountPolicyData.get(12), "UTF-8")+"&"+
                        URLEncoder.encode("updated_by", "UTF-8") +"="+URLEncoder.encode(discountPolicyData.get(13), "UTF-8")+"&"+
                        URLEncoder.encode("update_date", "UTF-8") +"="+URLEncoder.encode(discountPolicyData.get(14), "UTF-8")+"&"+
                        URLEncoder.encode("update_time", "UTF-8") +"="+URLEncoder.encode(discountPolicyData.get(15), "UTF-8")+"&"+
                        URLEncoder.encode("server_sync", "UTF-8") +"="+URLEncoder.encode(discountPolicyData.get(16), "UTF-8")+"&"+
                        URLEncoder.encode("software_id", "UTF-8") +"="+URLEncoder.encode(GlobalVariables.softwareLicId, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                String orderResponse;
                response = "";
                while ((orderResponse = bufferedReader.readLine()) != null)
                {
                    response += orderResponse;
                }
                objhttpurlconnection.disconnect();
            }
            else
            {
                response = "Unable to Connect To Server";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String sendDistrictInfoData()
    {
        String operation = "sync_district_info";
        try {
            URL objurl = new URL(webURL);
            HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
            objhttpurlconnection.setConnectTimeout(5 * 1000);
            objhttpurlconnection.connect();
            if (objhttpurlconnection.getResponseCode() == 200)
            {
                objhttpurlconnection.disconnect();
                objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("operation", "UTF-8") +"="+URLEncoder.encode(operation, "UTF-8")+"&"+
                        URLEncoder.encode("district_table_id", "UTF-8") +"="+URLEncoder.encode(districtInfoData.get(0), "UTF-8")+"&"+
                        URLEncoder.encode("district_id", "UTF-8") +"="+URLEncoder.encode(districtInfoData.get(1), "UTF-8")+"&"+
                        URLEncoder.encode("district_name", "UTF-8") +"="+URLEncoder.encode(districtInfoData.get(2), "UTF-8")+"&"+
                        URLEncoder.encode("district_status", "UTF-8") +"="+URLEncoder.encode(districtInfoData.get(3), "UTF-8")+"&"+
                        URLEncoder.encode("creating_user_id", "UTF-8") +"="+URLEncoder.encode(districtInfoData.get(4), "UTF-8")+"&"+
                        URLEncoder.encode("creating_date", "UTF-8") +"="+URLEncoder.encode(districtInfoData.get(5), "UTF-8")+"&"+
                        URLEncoder.encode("creating_time", "UTF-8") +"="+URLEncoder.encode(districtInfoData.get(6), "UTF-8")+"&"+
                        URLEncoder.encode("update_user_id", "UTF-8") +"="+URLEncoder.encode(districtInfoData.get(7), "UTF-8")+"&"+
                        URLEncoder.encode("update_date", "UTF-8") +"="+URLEncoder.encode(districtInfoData.get(8), "UTF-8")+"&"+
                        URLEncoder.encode("update_time", "UTF-8") +"="+URLEncoder.encode(districtInfoData.get(9), "UTF-8")+"&"+
                        URLEncoder.encode("server_sync", "UTF-8") +"="+URLEncoder.encode(districtInfoData.get(10), "UTF-8")+"&"+
                        URLEncoder.encode("software_id", "UTF-8") +"="+URLEncoder.encode(GlobalVariables.softwareLicId, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                String orderResponse;
                response = "";
                while ((orderResponse = bufferedReader.readLine()) != null)
                {
                    response += orderResponse;
                }
                objhttpurlconnection.disconnect();
            }
            else
            {
                response = "Unable to Connect To Server";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String sendGroupsInfoData()
    {
        String operation = "sync_groups_info";
        try {
            URL objurl = new URL(webURL);
            HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
            objhttpurlconnection.setConnectTimeout(5 * 1000);
            objhttpurlconnection.connect();
            if (objhttpurlconnection.getResponseCode() == 200)
            {
                objhttpurlconnection.disconnect();
                objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("operation", "UTF-8") +"="+URLEncoder.encode(operation, "UTF-8")+"&"+
                        URLEncoder.encode("group_table_id", "UTF-8") +"="+URLEncoder.encode(groupsInfoData.get(0), "UTF-8")+"&"+
                        URLEncoder.encode("group_id", "UTF-8") +"="+URLEncoder.encode(groupsInfoData.get(1), "UTF-8")+"&"+
                        URLEncoder.encode("company_id", "UTF-8") +"="+URLEncoder.encode(groupsInfoData.get(2), "UTF-8")+"&"+
                        URLEncoder.encode("group_name", "UTF-8") +"="+URLEncoder.encode(groupsInfoData.get(3), "UTF-8")+"&"+
                        URLEncoder.encode("group_status", "UTF-8") +"="+URLEncoder.encode(groupsInfoData.get(4), "UTF-8")+"&"+
                        URLEncoder.encode("creating_user_id", "UTF-8") +"="+URLEncoder.encode(groupsInfoData.get(5), "UTF-8")+"&"+
                        URLEncoder.encode("creating_date", "UTF-8") +"="+URLEncoder.encode(groupsInfoData.get(6), "UTF-8")+"&"+
                        URLEncoder.encode("creating_time", "UTF-8") +"="+URLEncoder.encode(groupsInfoData.get(7), "UTF-8")+"&"+
                        URLEncoder.encode("update_user_id", "UTF-8") +"="+URLEncoder.encode(groupsInfoData.get(8), "UTF-8")+"&"+
                        URLEncoder.encode("update_date", "UTF-8") +"="+URLEncoder.encode(groupsInfoData.get(9), "UTF-8")+"&"+
                        URLEncoder.encode("update_time", "UTF-8") +"="+URLEncoder.encode(groupsInfoData.get(10), "UTF-8")+"&"+
                        URLEncoder.encode("server_sync", "UTF-8") +"="+URLEncoder.encode(groupsInfoData.get(11), "UTF-8")+"&"+
                        URLEncoder.encode("software_id", "UTF-8") +"="+URLEncoder.encode(GlobalVariables.softwareLicId, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                String orderResponse;
                response = "";
                while ((orderResponse = bufferedReader.readLine()) != null)
                {
                    response += orderResponse;
                }
                objhttpurlconnection.disconnect();
            }
            else
            {
                response = "Unable to Connect To Server";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String sendMailsRecordData()
    {
        String operation = "sync_mails_record";
        try {
            URL objurl = new URL(webURL);
            HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
            objhttpurlconnection.setConnectTimeout(5 * 1000);
            objhttpurlconnection.connect();
            if (objhttpurlconnection.getResponseCode() == 200)
            {
                objhttpurlconnection.disconnect();
                objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("operation", "UTF-8") +"="+URLEncoder.encode(operation, "UTF-8")+"&"+
                        URLEncoder.encode("mail_id", "UTF-8") +"="+URLEncoder.encode(mailsRecordData.get(0), "UTF-8")+"&"+
                        URLEncoder.encode("from_email", "UTF-8") +"="+URLEncoder.encode(mailsRecordData.get(1), "UTF-8")+"&"+
                        URLEncoder.encode("to_email", "UTF-8") +"="+URLEncoder.encode(mailsRecordData.get(2), "UTF-8")+"&"+
                        URLEncoder.encode("title", "UTF-8") +"="+URLEncoder.encode(mailsRecordData.get(3), "UTF-8")+"&"+
                        URLEncoder.encode("message", "UTF-8") +"="+URLEncoder.encode(mailsRecordData.get(4), "UTF-8")+"&"+
                        URLEncoder.encode("attachment", "UTF-8") +"="+URLEncoder.encode(mailsRecordData.get(5), "UTF-8")+"&"+
                        URLEncoder.encode("user_id", "UTF-8") +"="+URLEncoder.encode(mailsRecordData.get(6), "UTF-8")+"&"+
                        URLEncoder.encode("date", "UTF-8") +"="+URLEncoder.encode(mailsRecordData.get(7), "UTF-8")+"&"+
                        URLEncoder.encode("time", "UTF-8") +"="+URLEncoder.encode(mailsRecordData.get(8), "UTF-8")+"&"+
                        URLEncoder.encode("status", "UTF-8") +"="+URLEncoder.encode(mailsRecordData.get(9), "UTF-8")+"&"+
                        URLEncoder.encode("server_sync", "UTF-8") +"="+URLEncoder.encode(mailsRecordData.get(10), "UTF-8")+"&"+
                        URLEncoder.encode("software_id", "UTF-8") +"="+URLEncoder.encode(GlobalVariables.softwareLicId, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                String orderResponse;
                response = "";
                while ((orderResponse = bufferedReader.readLine()) != null)
                {
                    response += orderResponse;
                }
                objhttpurlconnection.disconnect();
            }
            else
            {
                response = "Unable to Connect To Server";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String sendMobileGPSLocationData()
    {
        String operation = "sync_mobile_gps_location";
        try {
            URL objurl = new URL(webURL);
            HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
            objhttpurlconnection.setConnectTimeout(5 * 1000);
            objhttpurlconnection.connect();
            if (objhttpurlconnection.getResponseCode() == 200)
            {
                objhttpurlconnection.disconnect();
                objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("operation", "UTF-8") +"="+URLEncoder.encode(operation, "UTF-8")+"&"+
                        URLEncoder.encode("mob_loc_id", "UTF-8") +"="+URLEncoder.encode(mobileGPSLocationData.get(0), "UTF-8")+"&"+
                        URLEncoder.encode("user_id", "UTF-8") +"="+URLEncoder.encode(mobileGPSLocationData.get(1), "UTF-8")+"&"+
                        URLEncoder.encode("latitude", "UTF-8") +"="+URLEncoder.encode(mobileGPSLocationData.get(2), "UTF-8")+"&"+
                        URLEncoder.encode("longitude", "UTF-8") +"="+URLEncoder.encode(mobileGPSLocationData.get(3), "UTF-8")+"&"+
                        URLEncoder.encode("loc_name", "UTF-8") +"="+URLEncoder.encode(mobileGPSLocationData.get(4), "UTF-8")+"&"+
                        URLEncoder.encode("date", "UTF-8") +"="+URLEncoder.encode(mobileGPSLocationData.get(5), "UTF-8")+"&"+
                        URLEncoder.encode("time", "UTF-8") +"="+URLEncoder.encode(mobileGPSLocationData.get(6), "UTF-8")+"&"+
                        URLEncoder.encode("server_sync", "UTF-8") +"="+URLEncoder.encode(mobileGPSLocationData.get(7), "UTF-8")+"&"+
                        URLEncoder.encode("software_id", "UTF-8") +"="+URLEncoder.encode(GlobalVariables.softwareLicId, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                String orderResponse;
                response = "";
                while ((orderResponse = bufferedReader.readLine()) != null)
                {
                    response += orderResponse;
                }
                objhttpurlconnection.disconnect();
            }
            else
            {
                response = "Unable to Connect To Server";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String sendOrderGPSLocationData()
    {
        String operation = "sync_order_gps_location";
        try {
            URL objurl = new URL(webURL);
            HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
            objhttpurlconnection.setConnectTimeout(5 * 1000);
            objhttpurlconnection.connect();
            if (objhttpurlconnection.getResponseCode() == 200)
            {
                objhttpurlconnection.disconnect();
                objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("operation", "UTF-8") +"="+URLEncoder.encode(operation, "UTF-8")+"&"+
                        URLEncoder.encode("order_loc_id", "UTF-8") +"="+URLEncoder.encode(orderGPSLocationData.get(0), "UTF-8")+"&"+
                        URLEncoder.encode("order_id", "UTF-8") +"="+URLEncoder.encode(orderGPSLocationData.get(1), "UTF-8")+"&"+
                        URLEncoder.encode("user_id", "UTF-8") +"="+URLEncoder.encode(orderGPSLocationData.get(2), "UTF-8")+"&"+
                        URLEncoder.encode("latitude", "UTF-8") +"="+URLEncoder.encode(orderGPSLocationData.get(3), "UTF-8")+"&"+
                        URLEncoder.encode("longitude", "UTF-8") +"="+URLEncoder.encode(orderGPSLocationData.get(4), "UTF-8")+"&"+
                        URLEncoder.encode("loc_name", "UTF-8") +"="+URLEncoder.encode(orderGPSLocationData.get(5), "UTF-8")+"&"+
                        URLEncoder.encode("date", "UTF-8") +"="+URLEncoder.encode(orderGPSLocationData.get(6), "UTF-8")+"&"+
                        URLEncoder.encode("time", "UTF-8") +"="+URLEncoder.encode(orderGPSLocationData.get(7), "UTF-8")+"&"+
                        URLEncoder.encode("dealer_id", "UTF-8") +"="+URLEncoder.encode(orderGPSLocationData.get(8), "UTF-8")+"&"+
                        URLEncoder.encode("price", "UTF-8") +"="+URLEncoder.encode(orderGPSLocationData.get(9), "UTF-8")+"&"+
                        URLEncoder.encode("type", "UTF-8") +"="+URLEncoder.encode(orderGPSLocationData.get(10), "UTF-8")+"&"+
                        URLEncoder.encode("server_sync", "UTF-8") +"="+URLEncoder.encode(orderGPSLocationData.get(11), "UTF-8")+"&"+
                        URLEncoder.encode("software_id", "UTF-8") +"="+URLEncoder.encode(GlobalVariables.softwareLicId, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                String orderResponse;
                response = "";
                while ((orderResponse = bufferedReader.readLine()) != null)
                {
                    response += orderResponse;
                }
                objhttpurlconnection.disconnect();
            }
            else
            {
                response = "Unable to Connect To Server";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String sendOrderInfoData()
    {
        String operation = "sync_order_info";
        try {
            URL objurl = new URL(webURL);
            HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
            objhttpurlconnection.setConnectTimeout(5 * 1000);
            objhttpurlconnection.connect();
            if (objhttpurlconnection.getResponseCode() == 200)
            {
                objhttpurlconnection.disconnect();
                objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("operation", "UTF-8") +"="+URLEncoder.encode(operation, "UTF-8")+"&"+
                        URLEncoder.encode("order_id", "UTF-8") +"="+URLEncoder.encode(orderInfoData.get(0), "UTF-8")+"&"+
                        URLEncoder.encode("dealer_id", "UTF-8") +"="+URLEncoder.encode(orderInfoData.get(1), "UTF-8")+"&"+
                        URLEncoder.encode("product_id", "UTF-8") +"="+URLEncoder.encode(orderInfoData.get(2), "UTF-8")+"&"+
                        URLEncoder.encode("quantity", "UTF-8") +"="+URLEncoder.encode(orderInfoData.get(3), "UTF-8")+"&"+
                        URLEncoder.encode("unit", "UTF-8") +"="+URLEncoder.encode(orderInfoData.get(4), "UTF-8")+"&"+
                        URLEncoder.encode("order_price", "UTF-8") +"="+URLEncoder.encode(orderInfoData.get(5), "UTF-8")+"&"+
                        URLEncoder.encode("bonus", "UTF-8") +"="+URLEncoder.encode(orderInfoData.get(6), "UTF-8")+"&"+
                        URLEncoder.encode("discount", "UTF-8") +"="+URLEncoder.encode(orderInfoData.get(7), "UTF-8")+"&"+
                        URLEncoder.encode("waive_off_price", "UTF-8") +"="+URLEncoder.encode(orderInfoData.get(8), "UTF-8")+"&"+
                        URLEncoder.encode("final_price", "UTF-8") +"="+URLEncoder.encode(orderInfoData.get(9), "UTF-8")+"&"+
                        URLEncoder.encode("order_success", "UTF-8") +"="+URLEncoder.encode(orderInfoData.get(10), "UTF-8")+"&"+
                        URLEncoder.encode("booking_latitude", "UTF-8") +"="+URLEncoder.encode(orderInfoData.get(11), "UTF-8")+"&"+
                        URLEncoder.encode("booking_longitude", "UTF-8") +"="+URLEncoder.encode(orderInfoData.get(12), "UTF-8")+"&"+
                        URLEncoder.encode("booking_area", "UTF-8") +"="+URLEncoder.encode(orderInfoData.get(13), "UTF-8")+"&"+
                        URLEncoder.encode("booking_date", "UTF-8") +"="+URLEncoder.encode(orderInfoData.get(14), "UTF-8")+"&"+
                        URLEncoder.encode("booking_time", "UTF-8") +"="+URLEncoder.encode(orderInfoData.get(15), "UTF-8")+"&"+
                        URLEncoder.encode("booking_user_id", "UTF-8") +"="+URLEncoder.encode(orderInfoData.get(16), "UTF-8")+"&"+
                        URLEncoder.encode("delivered_latitude", "UTF-8") +"="+URLEncoder.encode(orderInfoData.get(17), "UTF-8")+"&"+
                        URLEncoder.encode("delivered_longitude", "UTF-8") +"="+URLEncoder.encode(orderInfoData.get(18), "UTF-8")+"&"+
                        URLEncoder.encode("delivered_area", "UTF-8") +"="+URLEncoder.encode(orderInfoData.get(19), "UTF-8")+"&"+
                        URLEncoder.encode("delivered_date", "UTF-8") +"="+URLEncoder.encode(orderInfoData.get(20), "UTF-8")+"&"+
                        URLEncoder.encode("delivered_time", "UTF-8") +"="+URLEncoder.encode(orderInfoData.get(21), "UTF-8")+"&"+
                        URLEncoder.encode("delivered_user_id", "UTF-8") +"="+URLEncoder.encode(orderInfoData.get(22), "UTF-8")+"&"+
                        URLEncoder.encode("update_dates", "UTF-8") +"="+URLEncoder.encode(orderInfoData.get(23), "UTF-8")+"&"+
                        URLEncoder.encode("update_times", "UTF-8") +"="+URLEncoder.encode(orderInfoData.get(24), "UTF-8")+"&"+
                        URLEncoder.encode("update_user_id", "UTF-8") +"="+URLEncoder.encode(orderInfoData.get(25), "UTF-8")+"&"+
                        URLEncoder.encode("comments", "UTF-8") +"="+URLEncoder.encode(orderInfoData.get(26), "UTF-8")+"&"+
                        URLEncoder.encode("status", "UTF-8") +"="+URLEncoder.encode(orderInfoData.get(27), "UTF-8")+"&"+
                        URLEncoder.encode("server_sync", "UTF-8") +"="+URLEncoder.encode(orderInfoData.get(28), "UTF-8")+"&"+
                        URLEncoder.encode("software_id", "UTF-8") +"="+URLEncoder.encode(GlobalVariables.softwareLicId, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                String orderResponse;
                response = "";
                while ((orderResponse = bufferedReader.readLine()) != null)
                {
                    response += orderResponse;
                }
                objhttpurlconnection.disconnect();
            }
            else
            {
                response = "Unable to Connect To Server";
            }
        } catch (MalformedURLException e) {
//            e.printStackTrace();
            GlobalVariables.showNotification(-1, "Error", e.getMessage());
        } catch (IOException e) {
//            e.printStackTrace();
            GlobalVariables.showNotification(-1, "Error", e.getMessage());
        }
        return response;
    }

    public String sendOrderInfoDetailedData()
    {
        String operation = "sync_order_info_detailed";
        try {
            URL objurl = new URL(webURL);
            HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
            objhttpurlconnection.setConnectTimeout(5 * 1000);
            objhttpurlconnection.connect();
            if (objhttpurlconnection.getResponseCode() == 200)
            {
                objhttpurlconnection.disconnect();
                objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("operation", "UTF-8") +"="+URLEncoder.encode(operation, "UTF-8")+"&"+
                            URLEncoder.encode("detail_id", "UTF-8") +"="+URLEncoder.encode(orderInfoDetailedData.get(0), "UTF-8")+"&"+
                            URLEncoder.encode("order_id", "UTF-8") +"="+URLEncoder.encode(orderInfoDetailedData.get(1), "UTF-8")+"&"+
                            URLEncoder.encode("product_table_id", "UTF-8") +"="+URLEncoder.encode(orderInfoDetailedData.get(2), "UTF-8")+"&"+
                            URLEncoder.encode("batch_id", "UTF-8") +"="+URLEncoder.encode(orderInfoDetailedData.get(3), "UTF-8")+"&"+
                            URLEncoder.encode("quantity", "UTF-8") +"="+URLEncoder.encode(orderInfoDetailedData.get(4), "UTF-8")+"&"+
                            URLEncoder.encode("submission_quantity", "UTF-8") +"="+URLEncoder.encode(orderInfoDetailedData.get(5), "UTF-8")+"&"+
                            URLEncoder.encode("unit", "UTF-8") +"="+URLEncoder.encode(orderInfoDetailedData.get(6), "UTF-8")+"&"+
                            URLEncoder.encode("submission_unit", "UTF-8") +"="+URLEncoder.encode(orderInfoDetailedData.get(7), "UTF-8")+"&"+
                            URLEncoder.encode("order_price", "UTF-8") +"="+URLEncoder.encode(orderInfoDetailedData.get(8), "UTF-8")+"&"+
                            URLEncoder.encode("bonus_quant", "UTF-8") +"="+URLEncoder.encode(orderInfoDetailedData.get(9), "UTF-8")+"&"+
                            URLEncoder.encode("discount", "UTF-8") +"="+URLEncoder.encode(orderInfoDetailedData.get(10), "UTF-8")+"&"+
                            URLEncoder.encode("final_price", "UTF-8") +"="+URLEncoder.encode(orderInfoDetailedData.get(11), "UTF-8")+"&"+
                            URLEncoder.encode("cmplt_returned_bit", "UTF-8") +"="+URLEncoder.encode(orderInfoDetailedData.get(12), "UTF-8")+"&"+
                            URLEncoder.encode("returned_quant", "UTF-8") +"="+URLEncoder.encode(orderInfoDetailedData.get(13), "UTF-8")+"&"+
                            URLEncoder.encode("returned_bonus_quant", "UTF-8") +"="+URLEncoder.encode(orderInfoDetailedData.get(14), "UTF-8")+"&"+
                            URLEncoder.encode("server_sync", "UTF-8") +"="+URLEncoder.encode(orderInfoDetailedData.get(15), "UTF-8")+"&"+
                        URLEncoder.encode("software_id", "UTF-8") +"="+URLEncoder.encode(GlobalVariables.softwareLicId, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                String orderResponse;
                response = "";
                while ((orderResponse = bufferedReader.readLine()) != null)
                {
                    response += orderResponse;
                }
                objhttpurlconnection.disconnect();
            }
            else
            {
                response = "Unable to Connect To Server";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String sendOrderReturnData()
    {
        String operation = "sync_order_return";
        try {
            URL objurl = new URL(webURL);
            HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
            objhttpurlconnection.setConnectTimeout(5 * 1000);
            objhttpurlconnection.connect();
            if (objhttpurlconnection.getResponseCode() == 200)
            {
                objhttpurlconnection.disconnect();
                objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("operation", "UTF-8") +"="+URLEncoder.encode(operation, "UTF-8")+"&"+
                        URLEncoder.encode("return_id", "UTF-8") +"="+URLEncoder.encode(orderReturnData.get(0), "UTF-8")+"&"+
                        URLEncoder.encode("dealer_id", "UTF-8") +"="+URLEncoder.encode(orderReturnData.get(1), "UTF-8")+"&"+
                        URLEncoder.encode("return_total_price", "UTF-8") +"="+URLEncoder.encode(orderReturnData.get(2), "UTF-8")+"&"+
                        URLEncoder.encode("return_gross_price", "UTF-8") +"="+URLEncoder.encode(orderReturnData.get(3), "UTF-8")+"&"+
                        URLEncoder.encode("order_id", "UTF-8") +"="+URLEncoder.encode(orderReturnData.get(4), "UTF-8")+"&"+
                        URLEncoder.encode("entry_date", "UTF-8") +"="+URLEncoder.encode(orderReturnData.get(5), "UTF-8")+"&"+
                        URLEncoder.encode("entry_time", "UTF-8") +"="+URLEncoder.encode(orderReturnData.get(6), "UTF-8")+"&"+
                        URLEncoder.encode("enter_user_id", "UTF-8") +"="+URLEncoder.encode(orderReturnData.get(7), "UTF-8")+"&"+
                        URLEncoder.encode("update_date", "UTF-8") +"="+URLEncoder.encode(orderReturnData.get(8), "UTF-8")+"&"+
                        URLEncoder.encode("update_time", "UTF-8") +"="+URLEncoder.encode(orderReturnData.get(9), "UTF-8")+"&"+
                        URLEncoder.encode("update_user_id", "UTF-8") +"="+URLEncoder.encode(orderReturnData.get(10), "UTF-8")+"&"+
                        URLEncoder.encode("comments", "UTF-8") +"="+URLEncoder.encode(orderReturnData.get(11), "UTF-8")+"&"+
                        URLEncoder.encode("status", "UTF-8") +"="+URLEncoder.encode(orderReturnData.get(12), "UTF-8")+"&"+
                        URLEncoder.encode("server_sync", "UTF-8") +"="+URLEncoder.encode(orderReturnData.get(13), "UTF-8")+"&"+
                        URLEncoder.encode("software_id", "UTF-8") +"="+URLEncoder.encode(GlobalVariables.softwareLicId, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                String orderResponse;
                response = "";
                while ((orderResponse = bufferedReader.readLine()) != null)
                {
                    response += orderResponse;
                }
                objhttpurlconnection.disconnect();
            }
            else
            {
                response = "Unable to Connect To Server";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String sendOrderReturnDetailData()
    {
        String operation = "sync_order_return_detail";
        try {
            URL objurl = new URL(webURL);
            HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
            objhttpurlconnection.setConnectTimeout(5 * 1000);
            objhttpurlconnection.connect();
            if (objhttpurlconnection.getResponseCode() == 200)
            {
                objhttpurlconnection.disconnect();
                objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("operation", "UTF-8") +"="+URLEncoder.encode(operation, "UTF-8")+"&"+
                        URLEncoder.encode("return_detail_id", "UTF-8") +"="+URLEncoder.encode(orderReturnDetailData.get(0), "UTF-8")+"&"+
                        URLEncoder.encode("return_id", "UTF-8") +"="+URLEncoder.encode(orderReturnDetailData.get(1), "UTF-8")+"&"+
                        URLEncoder.encode("prod_id", "UTF-8") +"="+URLEncoder.encode(orderReturnDetailData.get(2), "UTF-8")+"&"+
                        URLEncoder.encode("prod_batch", "UTF-8") +"="+URLEncoder.encode(orderReturnDetailData.get(3), "UTF-8")+"&"+
                        URLEncoder.encode("prod_quant", "UTF-8") +"="+URLEncoder.encode(orderReturnDetailData.get(4), "UTF-8")+"&"+
                        URLEncoder.encode("bonus_quant", "UTF-8") +"="+URLEncoder.encode(orderReturnDetailData.get(5), "UTF-8")+"&"+
                        URLEncoder.encode("discount_amount", "UTF-8") +"="+URLEncoder.encode(orderReturnDetailData.get(6), "UTF-8")+"&"+
                        URLEncoder.encode("total_amount", "UTF-8") +"="+URLEncoder.encode(orderReturnDetailData.get(7), "UTF-8")+"&"+
                        URLEncoder.encode("unit", "UTF-8") +"="+URLEncoder.encode(orderReturnDetailData.get(8), "UTF-8")+"&"+
                        URLEncoder.encode("return_reason", "UTF-8") +"="+URLEncoder.encode(orderReturnDetailData.get(9), "UTF-8")+"&"+
                        URLEncoder.encode("server_sync", "UTF-8") +"="+URLEncoder.encode(orderReturnDetailData.get(10), "UTF-8")+"&"+
                        URLEncoder.encode("software_id", "UTF-8") +"="+URLEncoder.encode(GlobalVariables.softwareLicId, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                String orderResponse;
                response = "";
                while ((orderResponse = bufferedReader.readLine()) != null)
                {
                    response += orderResponse;
                }
                objhttpurlconnection.disconnect();
            }
            else
            {
                response = "Unable to Connect To Server";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String sendOrderStatusInfoData()
    {
        String operation = "sync_order_status_info";
        try {
            URL objurl = new URL(webURL);
            HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
            objhttpurlconnection.setConnectTimeout(5 * 1000);
            objhttpurlconnection.connect();
            if (objhttpurlconnection.getResponseCode() == 200)
            {
                objhttpurlconnection.disconnect();
                objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("operation", "UTF-8") +"="+URLEncoder.encode(operation, "UTF-8")+"&"+
                        URLEncoder.encode("order_status_id", "UTF-8") +"="+URLEncoder.encode(orderStatusInfoData.get(0), "UTF-8")+"&"+
                        URLEncoder.encode("order_id", "UTF-8") +"="+URLEncoder.encode(orderStatusInfoData.get(1), "UTF-8")+"&"+
                        URLEncoder.encode("booking_user_id", "UTF-8") +"="+URLEncoder.encode(orderStatusInfoData.get(2), "UTF-8")+"&"+
                        URLEncoder.encode("booking_date", "UTF-8") +"="+URLEncoder.encode(orderStatusInfoData.get(3), "UTF-8")+"&"+
                        URLEncoder.encode("booking_latitude", "UTF-8") +"="+URLEncoder.encode(orderStatusInfoData.get(4), "UTF-8")+"&"+
                        URLEncoder.encode("booking_longitude", "UTF-8") +"="+URLEncoder.encode(orderStatusInfoData.get(5), "UTF-8")+"&"+
                        URLEncoder.encode("booking_area", "UTF-8") +"="+URLEncoder.encode(orderStatusInfoData.get(6), "UTF-8")+"&"+
                        URLEncoder.encode("delivered_user_id", "UTF-8") +"="+URLEncoder.encode(orderStatusInfoData.get(7), "UTF-8")+"&"+
                        URLEncoder.encode("delivered_date", "UTF-8") +"="+URLEncoder.encode(orderStatusInfoData.get(8), "UTF-8")+"&"+
                        URLEncoder.encode("delivered_latitude", "UTF-8") +"="+URLEncoder.encode(orderStatusInfoData.get(9), "UTF-8")+"&"+
                        URLEncoder.encode("delivered_longitude", "UTF-8") +"="+URLEncoder.encode(orderStatusInfoData.get(10), "UTF-8")+"&"+
                        URLEncoder.encode("delivered_area", "UTF-8") +"="+URLEncoder.encode(orderStatusInfoData.get(11), "UTF-8")+"&"+
                        URLEncoder.encode("server_sync", "UTF-8") +"="+URLEncoder.encode(orderStatusInfoData.get(12), "UTF-8")+"&"+
                        URLEncoder.encode("software_id", "UTF-8") +"="+URLEncoder.encode(GlobalVariables.softwareLicId, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                String orderResponse;
                response = "";
                while ((orderResponse = bufferedReader.readLine()) != null)
                {
                    response += orderResponse;
                }
                objhttpurlconnection.disconnect();
            }
            else
            {
                response = "Unable to Connect To Server";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String sendProductsStockData()
    {
        String operation = "sync_products_stock";
        try {
            URL objurl = new URL(webURL);
            HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
            objhttpurlconnection.setConnectTimeout(5 * 1000);
            objhttpurlconnection.connect();
            if (objhttpurlconnection.getResponseCode() == 200)
            {
                objhttpurlconnection.disconnect();
                objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("operation", "UTF-8") +"="+URLEncoder.encode(operation, "UTF-8")+"&"+
                        URLEncoder.encode("ProdStockID", "UTF-8") +"="+URLEncoder.encode(productsStockData.get(0), "UTF-8")+"&"+
                        URLEncoder.encode("product_table_id", "UTF-8") +"="+URLEncoder.encode(productsStockData.get(1), "UTF-8")+"&"+
                        URLEncoder.encode("in_stock", "UTF-8") +"="+URLEncoder.encode(productsStockData.get(2), "UTF-8")+"&"+
                        URLEncoder.encode("min_stock", "UTF-8") +"="+URLEncoder.encode(productsStockData.get(3), "UTF-8")+"&"+
                        URLEncoder.encode("max_stock", "UTF-8") +"="+URLEncoder.encode(productsStockData.get(4), "UTF-8")+"&"+
                        URLEncoder.encode("bonus_quant", "UTF-8") +"="+URLEncoder.encode(productsStockData.get(5), "UTF-8")+"&"+
                        URLEncoder.encode("status", "UTF-8") +"="+URLEncoder.encode(productsStockData.get(6), "UTF-8")+"&"+
                        URLEncoder.encode("server_sync", "UTF-8") +"="+URLEncoder.encode(productsStockData.get(7), "UTF-8")+"&"+
                        URLEncoder.encode("software_id", "UTF-8") +"="+URLEncoder.encode(GlobalVariables.softwareLicId, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                String orderResponse;
                response = "";
                while ((orderResponse = bufferedReader.readLine()) != null)
                {
                    response += orderResponse;
                }
                objhttpurlconnection.disconnect();
            }
            else
            {
                response = "Unable to Connect To Server";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String sendProductInfoData()
    {
        String operation = "sync_product_info";
        try {
            URL objurl = new URL(webURL);
            HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
            objhttpurlconnection.setConnectTimeout(5 * 1000);
            objhttpurlconnection.connect();
            if (objhttpurlconnection.getResponseCode() == 200)
            {
                objhttpurlconnection.disconnect();
                objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("operation", "UTF-8") +"="+URLEncoder.encode(operation, "UTF-8")+"&"+
                        URLEncoder.encode("product_table_id", "UTF-8") +"="+URLEncoder.encode(productInfoData.get(0), "UTF-8")+"&"+
                        URLEncoder.encode("product_id", "UTF-8") +"="+URLEncoder.encode(productInfoData.get(1), "UTF-8")+"&"+
                        URLEncoder.encode("company_table_id", "UTF-8") +"="+URLEncoder.encode(productInfoData.get(2), "UTF-8")+"&"+
                        URLEncoder.encode("group_table_id", "UTF-8") +"="+URLEncoder.encode(productInfoData.get(3), "UTF-8")+"&"+
                        URLEncoder.encode("report_prodID", "UTF-8") +"="+URLEncoder.encode(productInfoData.get(4), "UTF-8")+"&"+
                        URLEncoder.encode("old_prodID", "UTF-8") +"="+URLEncoder.encode(productInfoData.get(5), "UTF-8")+"&"+
                        URLEncoder.encode("product_name", "UTF-8") +"="+URLEncoder.encode(productInfoData.get(6), "UTF-8")+"&"+
                        URLEncoder.encode("product_type", "UTF-8") +"="+URLEncoder.encode(productInfoData.get(7), "UTF-8")+"&"+
                        URLEncoder.encode("packSize", "UTF-8") +"="+URLEncoder.encode(productInfoData.get(8), "UTF-8")+"&"+
                        URLEncoder.encode("carton_size", "UTF-8") +"="+URLEncoder.encode(productInfoData.get(9), "UTF-8")+"&"+
                        URLEncoder.encode("retail_price", "UTF-8") +"="+URLEncoder.encode(productInfoData.get(10), "UTF-8")+"&"+
                        URLEncoder.encode("trade_price", "UTF-8") +"="+URLEncoder.encode(productInfoData.get(11), "UTF-8")+"&"+
                        URLEncoder.encode("purchase_price", "UTF-8") +"="+URLEncoder.encode(productInfoData.get(12), "UTF-8")+"&"+
                        URLEncoder.encode("purchase_discount", "UTF-8") +"="+URLEncoder.encode(productInfoData.get(13), "UTF-8")+"&"+
                        URLEncoder.encode("sales_tax", "UTF-8") +"="+URLEncoder.encode(productInfoData.get(14), "UTF-8")+"&"+
                        URLEncoder.encode("hold_stock", "UTF-8") +"="+URLEncoder.encode(productInfoData.get(15), "UTF-8")+"&"+
                        URLEncoder.encode("product_status", "UTF-8") +"="+URLEncoder.encode(productInfoData.get(16), "UTF-8")+"&"+
                        URLEncoder.encode("creating_user_id", "UTF-8") +"="+URLEncoder.encode(productInfoData.get(17), "UTF-8")+"&"+
                        URLEncoder.encode("creating_date", "UTF-8") +"="+URLEncoder.encode(productInfoData.get(18), "UTF-8")+"&"+
                        URLEncoder.encode("creating_time", "UTF-8") +"="+URLEncoder.encode(productInfoData.get(19), "UTF-8")+"&"+
                        URLEncoder.encode("update_user_id", "UTF-8") +"="+URLEncoder.encode(productInfoData.get(20), "UTF-8")+"&"+
                        URLEncoder.encode("update_date", "UTF-8") +"="+URLEncoder.encode(productInfoData.get(21), "UTF-8")+"&"+
                        URLEncoder.encode("update_time", "UTF-8") +"="+URLEncoder.encode(productInfoData.get(22), "UTF-8")+"&"+
                        URLEncoder.encode("server_sync", "UTF-8") +"="+URLEncoder.encode(productInfoData.get(23), "UTF-8")+"&"+
                        URLEncoder.encode("software_id", "UTF-8") +"="+URLEncoder.encode(GlobalVariables.softwareLicId, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                String orderResponse;
                response = "";
                while ((orderResponse = bufferedReader.readLine()) != null)
                {
                    response += orderResponse;
                }
                objhttpurlconnection.disconnect();
            }
            else
            {
                response = "Unable to Connect To Server";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String sendProductOverallRecordData()
    {
        String operation = "sync_product_overall_record";
        try {
            URL objurl = new URL(webURL);
            HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
            objhttpurlconnection.setConnectTimeout(5 * 1000);
            objhttpurlconnection.connect();
            if (objhttpurlconnection.getResponseCode() == 200)
            {
                objhttpurlconnection.disconnect();
                objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("operation", "UTF-8") +"="+URLEncoder.encode(operation, "UTF-8")+"&"+
                        URLEncoder.encode("overall_record_id", "UTF-8") +"="+URLEncoder.encode(productOverallRecordData.get(0), "UTF-8")+"&"+
                        URLEncoder.encode("product_table_id", "UTF-8") +"="+URLEncoder.encode(productOverallRecordData.get(1), "UTF-8")+"&"+
                        URLEncoder.encode("prod_name", "UTF-8") +"="+URLEncoder.encode(productOverallRecordData.get(2), "UTF-8")+"&"+
                        URLEncoder.encode("prod_batch", "UTF-8") +"="+URLEncoder.encode(productOverallRecordData.get(3), "UTF-8")+"&"+
                        URLEncoder.encode("quantity_sold", "UTF-8") +"="+URLEncoder.encode(productOverallRecordData.get(4), "UTF-8")+"&"+
                        URLEncoder.encode("quantity_return", "UTF-8") +"="+URLEncoder.encode(productOverallRecordData.get(5), "UTF-8")+"&"+
                        URLEncoder.encode("trade_rate", "UTF-8") +"="+URLEncoder.encode(productOverallRecordData.get(6), "UTF-8")+"&"+
                        URLEncoder.encode("purch_rate", "UTF-8") +"="+URLEncoder.encode(productOverallRecordData.get(7), "UTF-8")+"&"+
                        URLEncoder.encode("retail_rate", "UTF-8") +"="+URLEncoder.encode(productOverallRecordData.get(8), "UTF-8")+"&"+
                        URLEncoder.encode("expired_quantity", "UTF-8") +"="+URLEncoder.encode(productOverallRecordData.get(9), "UTF-8")+"&"+
                        URLEncoder.encode("damaged_quantity", "UTF-8") +"="+URLEncoder.encode(productOverallRecordData.get(10), "UTF-8")+"&"+
                        URLEncoder.encode("prod_status", "UTF-8") +"="+URLEncoder.encode(productOverallRecordData.get(11), "UTF-8")+"&"+
                        URLEncoder.encode("server_sync", "UTF-8") +"="+URLEncoder.encode(productOverallRecordData.get(12), "UTF-8")+"&"+
                        URLEncoder.encode("software_id", "UTF-8") +"="+URLEncoder.encode(GlobalVariables.softwareLicId, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                String orderResponse;
                response = "";
                while ((orderResponse = bufferedReader.readLine()) != null)
                {
                    response += orderResponse;
                }
                objhttpurlconnection.disconnect();
            }
            else
            {
                response = "Unable to Connect To Server";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String sendPurchaseInfoData()
    {
        String operation = "sync_purchase_info";
        try {
            URL objurl = new URL(webURL);
            HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
            objhttpurlconnection.setConnectTimeout(5 * 1000);
            objhttpurlconnection.connect();
            if (objhttpurlconnection.getResponseCode() == 200)
            {
                objhttpurlconnection.disconnect();
                objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("operation", "UTF-8") +"="+URLEncoder.encode(operation, "UTF-8")+"&"+
                        URLEncoder.encode("purchase_id", "UTF-8") +"="+URLEncoder.encode(purchaseInfoData.get(0), "UTF-8")+"&"+
                        URLEncoder.encode("comp_id", "UTF-8") +"="+URLEncoder.encode(purchaseInfoData.get(1), "UTF-8")+"&"+
                        URLEncoder.encode("invoice_num", "UTF-8") +"="+URLEncoder.encode(purchaseInfoData.get(2), "UTF-8")+"&"+
                        URLEncoder.encode("supplier_id", "UTF-8") +"="+URLEncoder.encode(purchaseInfoData.get(3), "UTF-8")+"&"+
                        URLEncoder.encode("purchase_date", "UTF-8") +"="+URLEncoder.encode(purchaseInfoData.get(4), "UTF-8")+"&"+
                        URLEncoder.encode("gross_amount", "UTF-8") +"="+URLEncoder.encode(purchaseInfoData.get(5), "UTF-8")+"&"+
                        URLEncoder.encode("disc_amount", "UTF-8") +"="+URLEncoder.encode(purchaseInfoData.get(6), "UTF-8")+"&"+
                        URLEncoder.encode("net_amount", "UTF-8") +"="+URLEncoder.encode(purchaseInfoData.get(7), "UTF-8")+"&"+
                        URLEncoder.encode("enter_user_id", "UTF-8") +"="+URLEncoder.encode(purchaseInfoData.get(8), "UTF-8")+"&"+
                        URLEncoder.encode("entry_date", "UTF-8") +"="+URLEncoder.encode(purchaseInfoData.get(9), "UTF-8")+"&"+
                        URLEncoder.encode("entry_time", "UTF-8") +"="+URLEncoder.encode(purchaseInfoData.get(10), "UTF-8")+"&"+
                        URLEncoder.encode("update_user_id", "UTF-8") +"="+URLEncoder.encode(purchaseInfoData.get(11), "UTF-8")+"&"+
                        URLEncoder.encode("update_date", "UTF-8") +"="+URLEncoder.encode(purchaseInfoData.get(12), "UTF-8")+"&"+
                        URLEncoder.encode("update_time", "UTF-8") +"="+URLEncoder.encode(purchaseInfoData.get(13), "UTF-8")+"&"+
                        URLEncoder.encode("comments", "UTF-8") +"="+URLEncoder.encode(purchaseInfoData.get(14), "UTF-8")+"&"+
                        URLEncoder.encode("status", "UTF-8") +"="+URLEncoder.encode(purchaseInfoData.get(15), "UTF-8")+"&"+
                        URLEncoder.encode("server_sync", "UTF-8") +"="+URLEncoder.encode(purchaseInfoData.get(16), "UTF-8")+"&"+
                        URLEncoder.encode("software_id", "UTF-8") +"="+URLEncoder.encode(GlobalVariables.softwareLicId, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                String orderResponse;
                response = "";
                while ((orderResponse = bufferedReader.readLine()) != null)
                {
                    response += orderResponse;
                }
                objhttpurlconnection.disconnect();
            }
            else
            {
                response = "Unable to Connect To Server";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String sendPurchaseInfoDetailData()
    {
        String operation = "sync_purchase_info_detail";
        try {
            URL objurl = new URL(webURL);
            HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
            objhttpurlconnection.setConnectTimeout(5 * 1000);
            objhttpurlconnection.connect();
            if (objhttpurlconnection.getResponseCode() == 200)
            {
                objhttpurlconnection.disconnect();
                objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("operation", "UTF-8") +"="+URLEncoder.encode(operation, "UTF-8")+"&"+
                        URLEncoder.encode("purchase_info_detail_id", "UTF-8") +"="+URLEncoder.encode(purchaseInfoDetailData.get(0), "UTF-8")+"&"+
                        URLEncoder.encode("invoice_num", "UTF-8") +"="+URLEncoder.encode(purchaseInfoDetailData.get(1), "UTF-8")+"&"+
                        URLEncoder.encode("purchase_id", "UTF-8") +"="+URLEncoder.encode(purchaseInfoDetailData.get(2), "UTF-8")+"&"+
                        URLEncoder.encode("prod_id", "UTF-8") +"="+URLEncoder.encode(purchaseInfoDetailData.get(3), "UTF-8")+"&"+
                        URLEncoder.encode("discount", "UTF-8") +"="+URLEncoder.encode(purchaseInfoDetailData.get(4), "UTF-8")+"&"+
                        URLEncoder.encode("bonus_quant", "UTF-8") +"="+URLEncoder.encode(purchaseInfoDetailData.get(5), "UTF-8")+"&"+
                        URLEncoder.encode("recieve_quant", "UTF-8") +"="+URLEncoder.encode(purchaseInfoDetailData.get(6), "UTF-8")+"&"+
                        URLEncoder.encode("batch_no", "UTF-8") +"="+URLEncoder.encode(purchaseInfoDetailData.get(7), "UTF-8")+"&"+
                        URLEncoder.encode("expiry_date", "UTF-8") +"="+URLEncoder.encode(purchaseInfoDetailData.get(8), "UTF-8")+"&"+
                        URLEncoder.encode("gross_amount", "UTF-8") +"="+URLEncoder.encode(purchaseInfoDetailData.get(9), "UTF-8")+"&"+
                        URLEncoder.encode("disc_amount", "UTF-8") +"="+URLEncoder.encode(purchaseInfoDetailData.get(10), "UTF-8")+"&"+
                        URLEncoder.encode("net_amount", "UTF-8") +"="+URLEncoder.encode(purchaseInfoDetailData.get(11), "UTF-8")+"&"+
                        URLEncoder.encode("invoice_date", "UTF-8") +"="+URLEncoder.encode(purchaseInfoDetailData.get(12), "UTF-8")+"&"+
                        URLEncoder.encode("returnBit", "UTF-8") +"="+URLEncoder.encode(purchaseInfoDetailData.get(13), "UTF-8")+"&"+
                        URLEncoder.encode("returned_quant", "UTF-8") +"="+URLEncoder.encode(purchaseInfoDetailData.get(14), "UTF-8")+"&"+
                        URLEncoder.encode("returned_bonus_quant", "UTF-8") +"="+URLEncoder.encode(purchaseInfoDetailData.get(15), "UTF-8")+"&"+
                        URLEncoder.encode("server_sync", "UTF-8") +"="+URLEncoder.encode(purchaseInfoDetailData.get(16), "UTF-8")+"&"+
                        URLEncoder.encode("software_id", "UTF-8") +"="+URLEncoder.encode(GlobalVariables.softwareLicId, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                String orderResponse;
                response = "";
                while ((orderResponse = bufferedReader.readLine()) != null)
                {
                    response += orderResponse;
                }
                objhttpurlconnection.disconnect();
            }
            else
            {
                response = "Unable to Connect To Server";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String sendPurchaseOrderDetailData()
    {
        String operation = "sync_purchase_order_detail";
        try {
            URL objurl = new URL(webURL);
            HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
            objhttpurlconnection.setConnectTimeout(5 * 1000);
            objhttpurlconnection.connect();
            if (objhttpurlconnection.getResponseCode() == 200)
            {
                objhttpurlconnection.disconnect();
                objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("operation", "UTF-8") +"="+URLEncoder.encode(operation, "UTF-8")+"&"+
                        URLEncoder.encode("order_detail_tableID", "UTF-8") +"="+URLEncoder.encode(purchaseOrderDetailData.get(0), "UTF-8")+"&"+
                        URLEncoder.encode("product_table_id", "UTF-8") +"="+URLEncoder.encode(purchaseOrderDetailData.get(1), "UTF-8")+"&"+
                        URLEncoder.encode("quantity_sold", "UTF-8") +"="+URLEncoder.encode(purchaseOrderDetailData.get(2), "UTF-8")+"&"+
                        URLEncoder.encode("quantity_instock", "UTF-8") +"="+URLEncoder.encode(purchaseOrderDetailData.get(3), "UTF-8")+"&"+
                        URLEncoder.encode("quantity_ordered", "UTF-8") +"="+URLEncoder.encode(purchaseOrderDetailData.get(4), "UTF-8")+"&"+
                        URLEncoder.encode("purch_order_id", "UTF-8") +"="+URLEncoder.encode(purchaseOrderDetailData.get(5), "UTF-8")+"&"+
                        URLEncoder.encode("net_amount", "UTF-8") +"="+URLEncoder.encode(purchaseOrderDetailData.get(6), "UTF-8")+"&"+
                        URLEncoder.encode("server_sync", "UTF-8") +"="+URLEncoder.encode(purchaseOrderDetailData.get(7), "UTF-8")+"&"+
                        URLEncoder.encode("software_id", "UTF-8") +"="+URLEncoder.encode(GlobalVariables.softwareLicId, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                String orderResponse;
                response = "";
                while ((orderResponse = bufferedReader.readLine()) != null)
                {
                    response += orderResponse;
                }
                objhttpurlconnection.disconnect();
            }
            else
            {
                response = "Unable to Connect To Server";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String sendPurchaseOrderInfoData()
    {
        String operation = "sync_purchase_order_info";
        try {
            URL objurl = new URL(webURL);
            HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
            objhttpurlconnection.setConnectTimeout(5 * 1000);
            objhttpurlconnection.connect();
            if (objhttpurlconnection.getResponseCode() == 200)
            {
                objhttpurlconnection.disconnect();
                objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("operation", "UTF-8") +"="+URLEncoder.encode(operation, "UTF-8")+"&"+
                        URLEncoder.encode("purch_order_table_id", "UTF-8") +"="+URLEncoder.encode(purchaseOrderInfoData.get(0), "UTF-8")+"&"+
                        URLEncoder.encode("purch_order_id", "UTF-8") +"="+URLEncoder.encode(purchaseOrderInfoData.get(1), "UTF-8")+"&"+
                        URLEncoder.encode("purch_order_date", "UTF-8") +"="+URLEncoder.encode(purchaseOrderInfoData.get(2), "UTF-8")+"&"+
                        URLEncoder.encode("purch_order_amount", "UTF-8") +"="+URLEncoder.encode(purchaseOrderInfoData.get(3), "UTF-8")+"&"+
                        URLEncoder.encode("numb_of_prod", "UTF-8") +"="+URLEncoder.encode(purchaseOrderInfoData.get(4), "UTF-8")+"&"+
                        URLEncoder.encode("inventoryDays", "UTF-8") +"="+URLEncoder.encode(purchaseOrderInfoData.get(5), "UTF-8")+"&"+
                        URLEncoder.encode("comp_table_id", "UTF-8") +"="+URLEncoder.encode(purchaseOrderInfoData.get(6), "UTF-8")+"&"+
                        URLEncoder.encode("order_status", "UTF-8") +"="+URLEncoder.encode(purchaseOrderInfoData.get(7), "UTF-8")+"&"+
                        URLEncoder.encode("creating_user_id", "UTF-8") +"="+URLEncoder.encode(purchaseOrderInfoData.get(8), "UTF-8")+"&"+
                        URLEncoder.encode("creating_date", "UTF-8") +"="+URLEncoder.encode(purchaseOrderInfoData.get(9), "UTF-8")+"&"+
                        URLEncoder.encode("creating_time", "UTF-8") +"="+URLEncoder.encode(purchaseOrderInfoData.get(10), "UTF-8")+"&"+
                        URLEncoder.encode("update_user_id", "UTF-8") +"="+URLEncoder.encode(purchaseOrderInfoData.get(11), "UTF-8")+"&"+
                        URLEncoder.encode("update_date", "UTF-8") +"="+URLEncoder.encode(purchaseOrderInfoData.get(12), "UTF-8")+"&"+
                        URLEncoder.encode("update_time", "UTF-8") +"="+URLEncoder.encode(purchaseOrderInfoData.get(13), "UTF-8")+"&"+
                        URLEncoder.encode("server_sync", "UTF-8") +"="+URLEncoder.encode(purchaseOrderInfoData.get(14), "UTF-8")+"&"+
                        URLEncoder.encode("software_id", "UTF-8") +"="+URLEncoder.encode(GlobalVariables.softwareLicId, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                String orderResponse;
                response = "";
                while ((orderResponse = bufferedReader.readLine()) != null)
                {
                    response += orderResponse;
                }
                objhttpurlconnection.disconnect();
            }
            else
            {
                response = "Unable to Connect To Server";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String sendPurchaseReturnData()
    {
        String operation = "sync_purchase_return";
        try {
            URL objurl = new URL(webURL);
            HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
            objhttpurlconnection.setConnectTimeout(5 * 1000);
            objhttpurlconnection.connect();
            if (objhttpurlconnection.getResponseCode() == 200)
            {
                objhttpurlconnection.disconnect();
                objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("operation", "UTF-8") +"="+URLEncoder.encode(operation, "UTF-8")+"&"+
                        URLEncoder.encode("returnTable_id", "UTF-8") +"="+URLEncoder.encode(purchaseReturnData.get(0), "UTF-8")+"&"+
                        URLEncoder.encode("return_id", "UTF-8") +"="+URLEncoder.encode(purchaseReturnData.get(1), "UTF-8")+"&"+
                        URLEncoder.encode("purchInvoNumb", "UTF-8") +"="+URLEncoder.encode(purchaseReturnData.get(2), "UTF-8")+"&"+
                        URLEncoder.encode("supplier_id", "UTF-8") +"="+URLEncoder.encode(purchaseReturnData.get(3), "UTF-8")+"&"+
                        URLEncoder.encode("return_total_price", "UTF-8") +"="+URLEncoder.encode(purchaseReturnData.get(4), "UTF-8")+"&"+
                        URLEncoder.encode("return_gross_price", "UTF-8") +"="+URLEncoder.encode(purchaseReturnData.get(5), "UTF-8")+"&"+
                        URLEncoder.encode("entry_date", "UTF-8") +"="+URLEncoder.encode(purchaseReturnData.get(6), "UTF-8")+"&"+
                        URLEncoder.encode("entry_time", "UTF-8") +"="+URLEncoder.encode(purchaseReturnData.get(7), "UTF-8")+"&"+
                        URLEncoder.encode("update_date", "UTF-8") +"="+URLEncoder.encode(purchaseReturnData.get(8), "UTF-8")+"&"+
                        URLEncoder.encode("update_time", "UTF-8") +"="+URLEncoder.encode(purchaseReturnData.get(9), "UTF-8")+"&"+
                        URLEncoder.encode("user_id", "UTF-8") +"="+URLEncoder.encode(purchaseReturnData.get(10), "UTF-8")+"&"+
                        URLEncoder.encode("comments", "UTF-8") +"="+URLEncoder.encode(purchaseReturnData.get(11), "UTF-8")+"&"+
                        URLEncoder.encode("status", "UTF-8") +"="+URLEncoder.encode(purchaseReturnData.get(12), "UTF-8")+"&"+
                        URLEncoder.encode("server_sync", "UTF-8") +"="+URLEncoder.encode(purchaseReturnData.get(13), "UTF-8")+"&"+
                        URLEncoder.encode("software_id", "UTF-8") +"="+URLEncoder.encode(GlobalVariables.softwareLicId, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                String orderResponse;
                response = "";
                while ((orderResponse = bufferedReader.readLine()) != null)
                {
                    response += orderResponse;
                }
                objhttpurlconnection.disconnect();
            }
            else
            {
                response = "Unable to Connect To Server";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String sendPurchaseReturnDetailData()
    {
        String operation = "sync_purchase_return_detail";
        try {
            URL objurl = new URL(webURL);
            HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
            objhttpurlconnection.setConnectTimeout(5 * 1000);
            objhttpurlconnection.connect();
            if (objhttpurlconnection.getResponseCode() == 200)
            {
                objhttpurlconnection.disconnect();
                objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("operation", "UTF-8") +"="+URLEncoder.encode(operation, "UTF-8")+"&"+
                        URLEncoder.encode("return_detail_id", "UTF-8") +"="+URLEncoder.encode(purchaseReturnDetailData.get(0), "UTF-8")+"&"+
                        URLEncoder.encode("return_id", "UTF-8") +"="+URLEncoder.encode(purchaseReturnDetailData.get(1), "UTF-8")+"&"+
                        URLEncoder.encode("prod_id", "UTF-8") +"="+URLEncoder.encode(purchaseReturnDetailData.get(2), "UTF-8")+"&"+
                        URLEncoder.encode("prod_batch", "UTF-8") +"="+URLEncoder.encode(purchaseReturnDetailData.get(3), "UTF-8")+"&"+
                        URLEncoder.encode("prod_quant", "UTF-8") +"="+URLEncoder.encode(purchaseReturnDetailData.get(4), "UTF-8")+"&"+
                        URLEncoder.encode("bonus_quant", "UTF-8") +"="+URLEncoder.encode(purchaseReturnDetailData.get(5), "UTF-8")+"&"+
                        URLEncoder.encode("discount_amount", "UTF-8") +"="+URLEncoder.encode(purchaseReturnDetailData.get(6), "UTF-8")+"&"+
                        URLEncoder.encode("total_amount", "UTF-8") +"="+URLEncoder.encode(purchaseReturnDetailData.get(7), "UTF-8")+"&"+
                        URLEncoder.encode("unit", "UTF-8") +"="+URLEncoder.encode(purchaseReturnDetailData.get(8), "UTF-8")+"&"+
                        URLEncoder.encode("return_reason", "UTF-8") +"="+URLEncoder.encode(purchaseReturnDetailData.get(9), "UTF-8")+"&"+
                        URLEncoder.encode("server_sync", "UTF-8") +"="+URLEncoder.encode(purchaseReturnDetailData.get(10), "UTF-8")+"&"+
                        URLEncoder.encode("software_id", "UTF-8") +"="+URLEncoder.encode(GlobalVariables.softwareLicId, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                String orderResponse;
                response = "";
                while ((orderResponse = bufferedReader.readLine()) != null)
                {
                    response += orderResponse;
                }
                objhttpurlconnection.disconnect();
            }
            else
            {
                response = "Unable to Connect To Server";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String sendPurchaseReturnInfoDetailData()
    {
        String operation = "sync_purchase_return_info_detail";
        try {
            URL objurl = new URL(webURL);
            HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
            objhttpurlconnection.setConnectTimeout(5 * 1000);
            objhttpurlconnection.connect();
            if (objhttpurlconnection.getResponseCode() == 200)
            {
                objhttpurlconnection.disconnect();
                objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("operation", "UTF-8") +"="+URLEncoder.encode(operation, "UTF-8")+"&"+
                        URLEncoder.encode("return_id", "UTF-8") +"="+URLEncoder.encode(purchaseReturnInfoDetailData.get(0), "UTF-8")+"&"+
                        URLEncoder.encode("supplier_id", "UTF-8") +"="+URLEncoder.encode(purchaseReturnInfoDetailData.get(1), "UTF-8")+"&"+
                        URLEncoder.encode("purch_invo_num", "UTF-8") +"="+URLEncoder.encode(purchaseReturnInfoDetailData.get(2), "UTF-8")+"&"+
                        URLEncoder.encode("return_date", "UTF-8") +"="+URLEncoder.encode(purchaseReturnInfoDetailData.get(3), "UTF-8")+"&"+
                        URLEncoder.encode("prod_id", "UTF-8") +"="+URLEncoder.encode(purchaseReturnInfoDetailData.get(4), "UTF-8")+"&"+
                        URLEncoder.encode("return_quant", "UTF-8") +"="+URLEncoder.encode(purchaseReturnInfoDetailData.get(5), "UTF-8")+"&"+
                        URLEncoder.encode("discount_percent", "UTF-8") +"="+URLEncoder.encode(purchaseReturnInfoDetailData.get(6), "UTF-8")+"&"+
                        URLEncoder.encode("bonus_quantity", "UTF-8") +"="+URLEncoder.encode(purchaseReturnInfoDetailData.get(7), "UTF-8")+"&"+
                        URLEncoder.encode("total_amount", "UTF-8") +"="+URLEncoder.encode(purchaseReturnInfoDetailData.get(8), "UTF-8")+"&"+
                        URLEncoder.encode("batch_no", "UTF-8") +"="+URLEncoder.encode(purchaseReturnInfoDetailData.get(9), "UTF-8")+"&"+
                        URLEncoder.encode("gross_amount", "UTF-8") +"="+URLEncoder.encode(purchaseReturnInfoDetailData.get(10), "UTF-8")+"&"+
                        URLEncoder.encode("server_sync", "UTF-8") +"="+URLEncoder.encode(purchaseReturnInfoDetailData.get(11), "UTF-8")+"&"+
                        URLEncoder.encode("software_id", "UTF-8") +"="+URLEncoder.encode(GlobalVariables.softwareLicId, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                String orderResponse;
                response = "";
                while ((orderResponse = bufferedReader.readLine()) != null)
                {
                    response += orderResponse;
                }
                objhttpurlconnection.disconnect();
            }
            else
            {
                response = "Unable to Connect To Server";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String sendPurchReturnInfoData()
    {
        String operation = "sync_purch_return_info";
        try {
            URL objurl = new URL(webURL);
            HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
            objhttpurlconnection.setConnectTimeout(5 * 1000);
            objhttpurlconnection.connect();
            if (objhttpurlconnection.getResponseCode() == 200)
            {
                objhttpurlconnection.disconnect();
                objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("operation", "UTF-8") +"="+URLEncoder.encode(operation, "UTF-8")+"&"+
                        URLEncoder.encode("purch_return_no", "UTF-8") +"="+URLEncoder.encode(purchReturnInfoData.get(0), "UTF-8")+"&"+
                        URLEncoder.encode("purch_invo_num", "UTF-8") +"="+URLEncoder.encode(purchReturnInfoData.get(1), "UTF-8")+"&"+
                        URLEncoder.encode("return_date", "UTF-8") +"="+URLEncoder.encode(purchReturnInfoData.get(2), "UTF-8")+"&"+
                        URLEncoder.encode("supplier_id", "UTF-8") +"="+URLEncoder.encode(purchReturnInfoData.get(3), "UTF-8")+"&"+
                        URLEncoder.encode("total_amount", "UTF-8") +"="+URLEncoder.encode(purchReturnInfoData.get(4), "UTF-8")+"&"+
                        URLEncoder.encode("discount_amount", "UTF-8") +"="+URLEncoder.encode(purchReturnInfoData.get(5), "UTF-8")+"&"+
                        URLEncoder.encode("server_sync", "UTF-8") +"="+URLEncoder.encode(purchReturnInfoData.get(6), "UTF-8")+"&"+
                        URLEncoder.encode("software_id", "UTF-8") +"="+URLEncoder.encode(GlobalVariables.softwareLicId, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                String orderResponse;
                response = "";
                while ((orderResponse = bufferedReader.readLine()) != null)
                {
                    response += orderResponse;
                }
                objhttpurlconnection.disconnect();
            }
            else
            {
                response = "Unable to Connect To Server";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String sendSalesmanDesignatedCitiesData()
    {
        String operation = "sync_salesman_designated_cities";
        try {
            URL objurl = new URL(webURL);
            HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
            objhttpurlconnection.setConnectTimeout(5 * 1000);
            objhttpurlconnection.connect();
            if (objhttpurlconnection.getResponseCode() == 200)
            {
                objhttpurlconnection.disconnect();
                objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("operation", "UTF-8") +"="+URLEncoder.encode(operation, "UTF-8")+"&"+
                        URLEncoder.encode("salesman_designated_id", "UTF-8") +"="+URLEncoder.encode(salesmanDesignatedCitiesData.get(0), "UTF-8")+"&"+
                        URLEncoder.encode("salesman_table_id", "UTF-8") +"="+URLEncoder.encode(salesmanDesignatedCitiesData.get(1), "UTF-8")+"&"+
                        URLEncoder.encode("designated_city_id", "UTF-8") +"="+URLEncoder.encode(salesmanDesignatedCitiesData.get(2), "UTF-8")+"&"+
                        URLEncoder.encode("start_date", "UTF-8") +"="+URLEncoder.encode(salesmanDesignatedCitiesData.get(3), "UTF-8")+"&"+
                        URLEncoder.encode("end_date", "UTF-8") +"="+URLEncoder.encode(salesmanDesignatedCitiesData.get(4), "UTF-8")+"&"+
                        URLEncoder.encode("designated_status", "UTF-8") +"="+URLEncoder.encode(salesmanDesignatedCitiesData.get(5), "UTF-8")+"&"+
                        URLEncoder.encode("server_sync", "UTF-8") +"="+URLEncoder.encode(salesmanDesignatedCitiesData.get(6), "UTF-8")+"&"+
                        URLEncoder.encode("software_id", "UTF-8") +"="+URLEncoder.encode(GlobalVariables.softwareLicId, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                String orderResponse;
                response = "";
                while ((orderResponse = bufferedReader.readLine()) != null)
                {
                    response += orderResponse;
                }
                objhttpurlconnection.disconnect();
            }
            else
            {
                response = "Unable to Connect To Server";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String sendSaleInvoicePrintInfoData()
    {
        String operation = "sync_sale_invoice_print_info";
        try {
            URL objurl = new URL(webURL);
            HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
            objhttpurlconnection.setConnectTimeout(5 * 1000);
            objhttpurlconnection.connect();
            if (objhttpurlconnection.getResponseCode() == 200)
            {
                objhttpurlconnection.disconnect();
                objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("operation", "UTF-8") +"="+URLEncoder.encode(operation, "UTF-8")+"&"+
                        URLEncoder.encode("sale_print_id", "UTF-8") +"="+URLEncoder.encode(saleInvoicePrintInfoData.get(0), "UTF-8")+"&"+
                        URLEncoder.encode("header_1", "UTF-8") +"="+URLEncoder.encode(saleInvoicePrintInfoData.get(1), "UTF-8")+"&"+
                        URLEncoder.encode("header_2", "UTF-8") +"="+URLEncoder.encode(saleInvoicePrintInfoData.get(2), "UTF-8")+"&"+
                        URLEncoder.encode("header_3", "UTF-8") +"="+URLEncoder.encode(saleInvoicePrintInfoData.get(3), "UTF-8")+"&"+
                        URLEncoder.encode("header_4", "UTF-8") +"="+URLEncoder.encode(saleInvoicePrintInfoData.get(4), "UTF-8")+"&"+
                        URLEncoder.encode("header_5", "UTF-8") +"="+URLEncoder.encode(saleInvoicePrintInfoData.get(5), "UTF-8")+"&"+
                        URLEncoder.encode("owner_ids", "UTF-8") +"="+URLEncoder.encode(saleInvoicePrintInfoData.get(6), "UTF-8")+"&"+
                        URLEncoder.encode("owner_name", "UTF-8") +"="+URLEncoder.encode(saleInvoicePrintInfoData.get(7), "UTF-8")+"&"+
                        URLEncoder.encode("father_name", "UTF-8") +"="+URLEncoder.encode(saleInvoicePrintInfoData.get(8), "UTF-8")+"&"+
                        URLEncoder.encode("owner_cnic", "UTF-8") +"="+URLEncoder.encode(saleInvoicePrintInfoData.get(9), "UTF-8")+"&"+
                        URLEncoder.encode("owner_country", "UTF-8") +"="+URLEncoder.encode(saleInvoicePrintInfoData.get(10), "UTF-8")+"&"+
                        URLEncoder.encode("business_name", "UTF-8") +"="+URLEncoder.encode(saleInvoicePrintInfoData.get(11), "UTF-8")+"&"+
                        URLEncoder.encode("business_address", "UTF-8") +"="+URLEncoder.encode(saleInvoicePrintInfoData.get(12), "UTF-8")+"&"+
                        URLEncoder.encode("business_city", "UTF-8") +"="+URLEncoder.encode(saleInvoicePrintInfoData.get(13), "UTF-8")+"&"+
                        URLEncoder.encode("footer_1", "UTF-8") +"="+URLEncoder.encode(saleInvoicePrintInfoData.get(14), "UTF-8")+"&"+
                        URLEncoder.encode("footer_2", "UTF-8") +"="+URLEncoder.encode(saleInvoicePrintInfoData.get(15), "UTF-8")+"&"+
                        URLEncoder.encode("footer_3", "UTF-8") +"="+URLEncoder.encode(saleInvoicePrintInfoData.get(16), "UTF-8")+"&"+
                        URLEncoder.encode("footer_4", "UTF-8") +"="+URLEncoder.encode(saleInvoicePrintInfoData.get(17), "UTF-8")+"&"+
                        URLEncoder.encode("creating_user_id", "UTF-8") +"="+URLEncoder.encode(saleInvoicePrintInfoData.get(18), "UTF-8")+"&"+
                        URLEncoder.encode("creating_date", "UTF-8") +"="+URLEncoder.encode(saleInvoicePrintInfoData.get(19), "UTF-8")+"&"+
                        URLEncoder.encode("creating_time", "UTF-8") +"="+URLEncoder.encode(saleInvoicePrintInfoData.get(20), "UTF-8")+"&"+
                        URLEncoder.encode("update_user_id", "UTF-8") +"="+URLEncoder.encode(saleInvoicePrintInfoData.get(21), "UTF-8")+"&"+
                        URLEncoder.encode("update_date", "UTF-8") +"="+URLEncoder.encode(saleInvoicePrintInfoData.get(22), "UTF-8")+"&"+
                        URLEncoder.encode("update_time", "UTF-8") +"="+URLEncoder.encode(saleInvoicePrintInfoData.get(23), "UTF-8")+"&"+
                        URLEncoder.encode("status", "UTF-8") +"="+URLEncoder.encode(saleInvoicePrintInfoData.get(24), "UTF-8")+"&"+
                        URLEncoder.encode("server_sync", "UTF-8") +"="+URLEncoder.encode(saleInvoicePrintInfoData.get(25), "UTF-8")+"&"+
                        URLEncoder.encode("software_id", "UTF-8") +"="+URLEncoder.encode(GlobalVariables.softwareLicId, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                String orderResponse;
                response = "";
                while ((orderResponse = bufferedReader.readLine()) != null)
                {
                    response += orderResponse;
                }
                objhttpurlconnection.disconnect();
            }
            else
            {
                response = "Unable to Connect To Server";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String sendSupplierCompanyData()
    {
        String operation = "sync_supplier_company";
        try {
            URL objurl = new URL(webURL);
            HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
            objhttpurlconnection.setConnectTimeout(5 * 1000);
            objhttpurlconnection.connect();
            if (objhttpurlconnection.getResponseCode() == 200)
            {
                objhttpurlconnection.disconnect();
                objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("operation", "UTF-8") +"="+URLEncoder.encode(operation, "UTF-8")+"&"+
                        URLEncoder.encode("supplier_company_id", "UTF-8") +"="+URLEncoder.encode(supplierCompanyData.get(0), "UTF-8")+"&"+
                        URLEncoder.encode("supplier_id", "UTF-8") +"="+URLEncoder.encode(supplierCompanyData.get(1), "UTF-8")+"&"+
                        URLEncoder.encode("company_id", "UTF-8") +"="+URLEncoder.encode(supplierCompanyData.get(2), "UTF-8")+"&"+
                        URLEncoder.encode("updated_date", "UTF-8") +"="+URLEncoder.encode(supplierCompanyData.get(3), "UTF-8")+"&"+
                        URLEncoder.encode("user_id", "UTF-8") +"="+URLEncoder.encode(supplierCompanyData.get(4), "UTF-8")+"&"+
                        URLEncoder.encode("status", "UTF-8") +"="+URLEncoder.encode(supplierCompanyData.get(5), "UTF-8")+"&"+
                        URLEncoder.encode("server_sync", "UTF-8") +"="+URLEncoder.encode(supplierCompanyData.get(6), "UTF-8")+"&"+
                        URLEncoder.encode("software_id", "UTF-8") +"="+URLEncoder.encode(GlobalVariables.softwareLicId, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                String orderResponse;
                response = "";
                while ((orderResponse = bufferedReader.readLine()) != null)
                {
                    response += orderResponse;
                }
                objhttpurlconnection.disconnect();
            }
            else
            {
                response = "Unable to Connect To Server";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String sendSupplierInfoData()
    {
        String operation = "sync_supplier_info";
        try {
            URL objurl = new URL(webURL);
            HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
            objhttpurlconnection.setConnectTimeout(5 * 1000);
            objhttpurlconnection.connect();
            if (objhttpurlconnection.getResponseCode() == 200)
            {
                objhttpurlconnection.disconnect();
                objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("operation", "UTF-8") +"="+URLEncoder.encode(operation, "UTF-8")+"&"+
                        URLEncoder.encode("supplier_table_id", "UTF-8") +"="+URLEncoder.encode(supplierInfoData.get(0), "UTF-8")+"&"+
                        URLEncoder.encode("supplier_id", "UTF-8") +"="+URLEncoder.encode(supplierInfoData.get(1), "UTF-8")+"&"+
                        URLEncoder.encode("supplier_name", "UTF-8") +"="+URLEncoder.encode(supplierInfoData.get(2), "UTF-8")+"&"+
                        URLEncoder.encode("supplier_email", "UTF-8") +"="+URLEncoder.encode(supplierInfoData.get(3), "UTF-8")+"&"+
                        URLEncoder.encode("supplier_contact", "UTF-8") +"="+URLEncoder.encode(supplierInfoData.get(4), "UTF-8")+"&"+
                        URLEncoder.encode("contact_person", "UTF-8") +"="+URLEncoder.encode(supplierInfoData.get(5), "UTF-8")+"&"+
                        URLEncoder.encode("supplier_address", "UTF-8") +"="+URLEncoder.encode(supplierInfoData.get(6), "UTF-8")+"&"+
                        URLEncoder.encode("supplier_city", "UTF-8") +"="+URLEncoder.encode(supplierInfoData.get(7), "UTF-8")+"&"+
                        URLEncoder.encode("supplier_status", "UTF-8") +"="+URLEncoder.encode(supplierInfoData.get(8), "UTF-8")+"&"+
                        URLEncoder.encode("creating_user_id", "UTF-8") +"="+URLEncoder.encode(supplierInfoData.get(9), "UTF-8")+"&"+
                        URLEncoder.encode("creating_date", "UTF-8") +"="+URLEncoder.encode(supplierInfoData.get(10), "UTF-8")+"&"+
                        URLEncoder.encode("creating_time", "UTF-8") +"="+URLEncoder.encode(supplierInfoData.get(11), "UTF-8")+"&"+
                        URLEncoder.encode("update_user_id", "UTF-8") +"="+URLEncoder.encode(supplierInfoData.get(12), "UTF-8")+"&"+
                        URLEncoder.encode("update_date", "UTF-8") +"="+URLEncoder.encode(supplierInfoData.get(13), "UTF-8")+"&"+
                        URLEncoder.encode("update_time", "UTF-8") +"="+URLEncoder.encode(supplierInfoData.get(14), "UTF-8")+"&"+
                        URLEncoder.encode("server_sync", "UTF-8") +"="+URLEncoder.encode(supplierInfoData.get(15), "UTF-8")+"&"+
                        URLEncoder.encode("software_id", "UTF-8") +"="+URLEncoder.encode(GlobalVariables.softwareLicId, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                String orderResponse;
                response = "";
                while ((orderResponse = bufferedReader.readLine()) != null)
                {
                    response += orderResponse;
                }
                objhttpurlconnection.disconnect();
            }
            else
            {
                response = "Unable to Connect To Server";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String sendSyncTrackingData()
    {
        String operation = "sync_sync_tracking";
        try {
            URL objurl = new URL(webURL);
            HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
            objhttpurlconnection.setConnectTimeout(5 * 1000);
            objhttpurlconnection.connect();
            if (objhttpurlconnection.getResponseCode() == 200)
            {
                objhttpurlconnection.disconnect();
                objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("operation", "UTF-8") +"="+URLEncoder.encode(operation, "UTF-8")+"&"+
                        URLEncoder.encode("sync_id", "UTF-8") +"="+URLEncoder.encode(syncTrackingData.get(0), "UTF-8")+"&"+
                        URLEncoder.encode("user_id", "UTF-8") +"="+URLEncoder.encode(syncTrackingData.get(1), "UTF-8")+"&"+
                        URLEncoder.encode("dealer_info", "UTF-8") +"="+URLEncoder.encode(syncTrackingData.get(2), "UTF-8")+"&"+
                        URLEncoder.encode("area_info", "UTF-8") +"="+URLEncoder.encode(syncTrackingData.get(3), "UTF-8")+"&"+
                        URLEncoder.encode("product_info", "UTF-8") +"="+URLEncoder.encode(syncTrackingData.get(4), "UTF-8")+"&"+
                        URLEncoder.encode("discount_info", "UTF-8") +"="+URLEncoder.encode(syncTrackingData.get(5), "UTF-8")+"&"+
                        URLEncoder.encode("bonus_info", "UTF-8") +"="+URLEncoder.encode(syncTrackingData.get(6), "UTF-8")+"&"+
                        URLEncoder.encode("server_sync", "UTF-8") +"="+URLEncoder.encode(syncTrackingData.get(7), "UTF-8")+"&"+
                        URLEncoder.encode("software_id", "UTF-8") +"="+URLEncoder.encode(GlobalVariables.softwareLicId, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                String orderResponse;
                response = "";
                while ((orderResponse = bufferedReader.readLine()) != null)
                {
                    response += orderResponse;
                }
                objhttpurlconnection.disconnect();
            }
            else
            {
                response = "Unable to Connect To Server";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String sendSystemSettingsData()
    {
        String operation = "sync_system_settings";
        try {
            URL objurl = new URL(webURL);
            HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
            objhttpurlconnection.setConnectTimeout(5 * 1000);
            objhttpurlconnection.connect();
            if (objhttpurlconnection.getResponseCode() == 200)
            {
                objhttpurlconnection.disconnect();
                objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("operation", "UTF-8") +"="+URLEncoder.encode(operation, "UTF-8")+"&"+
                        URLEncoder.encode("setting_id", "UTF-8") +"="+URLEncoder.encode(systemSettingsData.get(0), "UTF-8")+"&"+
                        URLEncoder.encode("setting_name", "UTF-8") +"="+URLEncoder.encode(systemSettingsData.get(1), "UTF-8")+"&"+
                        URLEncoder.encode("setting_value", "UTF-8") +"="+URLEncoder.encode(systemSettingsData.get(2), "UTF-8")+"&"+
                        URLEncoder.encode("server_sync", "UTF-8") +"="+URLEncoder.encode(systemSettingsData.get(3), "UTF-8")+"&"+
                        URLEncoder.encode("software_id", "UTF-8") +"="+URLEncoder.encode(GlobalVariables.softwareLicId, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                String orderResponse;
                response = "";
                while ((orderResponse = bufferedReader.readLine()) != null)
                {
                    response += orderResponse;
                }
                objhttpurlconnection.disconnect();
            }
            else
            {
                response = "Unable to Connect To Server";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String sendUserAccountsData()
    {
        String operation = "sync_user_accounts";
        try {
            URL objurl = new URL(webURL);
            HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
            objhttpurlconnection.setConnectTimeout(5 * 1000);
            objhttpurlconnection.connect();
            if (objhttpurlconnection.getResponseCode() == 200)
            {
                objhttpurlconnection.disconnect();
                objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("operation", "UTF-8") +"="+URLEncoder.encode(operation, "UTF-8")+"&"+
                        URLEncoder.encode("account_id", "UTF-8") +"="+URLEncoder.encode(userAccountsData.get(0), "UTF-8")+"&"+
                        URLEncoder.encode("user_name", "UTF-8") +"="+URLEncoder.encode(userAccountsData.get(1), "UTF-8")+"&"+
                        URLEncoder.encode("user_id", "UTF-8") +"="+URLEncoder.encode(userAccountsData.get(2), "UTF-8")+"&"+
                        URLEncoder.encode("user_password", "UTF-8") +"="+URLEncoder.encode(userAccountsData.get(3), "UTF-8")+"&"+
                        URLEncoder.encode("user_rights", "UTF-8") +"="+URLEncoder.encode(userAccountsData.get(4), "UTF-8")+"&"+
                        URLEncoder.encode("reg_date", "UTF-8") +"="+URLEncoder.encode(userAccountsData.get(5), "UTF-8")+"&"+
                        URLEncoder.encode("current_status", "UTF-8") +"="+URLEncoder.encode(userAccountsData.get(6), "UTF-8")+"&"+
                        URLEncoder.encode("user_status", "UTF-8") +"="+URLEncoder.encode(userAccountsData.get(7), "UTF-8")+"&"+
                        URLEncoder.encode("server_sync", "UTF-8") +"="+URLEncoder.encode(userAccountsData.get(8), "UTF-8")+"&"+
                        URLEncoder.encode("software_id", "UTF-8") +"="+URLEncoder.encode(GlobalVariables.softwareLicId, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                String orderResponse;
                response = "";
                while ((orderResponse = bufferedReader.readLine()) != null)
                {
                    response += orderResponse;
                }
                objhttpurlconnection.disconnect();
            }
            else
            {
                response = "Unable to Connect To Server";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String sendUserInfoData()
    {
        String operation = "sync_user_info";
        try {
            URL objurl = new URL(webURL);
            HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
            objhttpurlconnection.setConnectTimeout(5 * 1000);
            objhttpurlconnection.connect();
            if (objhttpurlconnection.getResponseCode() == 200)
            {
                objhttpurlconnection.disconnect();
                objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("operation", "UTF-8") +"="+URLEncoder.encode(operation, "UTF-8")+"&"+
                        URLEncoder.encode("user_table_id", "UTF-8") +"="+URLEncoder.encode(userInfoData.get(0), "UTF-8")+"&"+
                        URLEncoder.encode("user_id", "UTF-8") +"="+URLEncoder.encode(userInfoData.get(1), "UTF-8")+"&"+
                        URLEncoder.encode("user_name", "UTF-8") +"="+URLEncoder.encode(userInfoData.get(2), "UTF-8")+"&"+
                        URLEncoder.encode("user_contact", "UTF-8") +"="+URLEncoder.encode(userInfoData.get(3), "UTF-8")+"&"+
                        URLEncoder.encode("user_address", "UTF-8") +"="+URLEncoder.encode(userInfoData.get(4), "UTF-8")+"&"+
                        URLEncoder.encode("user_cnic", "UTF-8") +"="+URLEncoder.encode(userInfoData.get(5), "UTF-8")+"&"+
                        URLEncoder.encode("user_type", "UTF-8") +"="+URLEncoder.encode(userInfoData.get(6), "UTF-8")+"&"+
                        URLEncoder.encode("user_image", "UTF-8") +"="+URLEncoder.encode(userInfoData.get(7), "UTF-8")+"&"+
                        URLEncoder.encode("user_status", "UTF-8") +"="+URLEncoder.encode(userInfoData.get(8), "UTF-8")+"&"+
                        URLEncoder.encode("creating_user_id", "UTF-8") +"="+URLEncoder.encode(userInfoData.get(9), "UTF-8")+"&"+
                        URLEncoder.encode("creating_date", "UTF-8") +"="+URLEncoder.encode(userInfoData.get(10), "UTF-8")+"&"+
                        URLEncoder.encode("creating_time", "UTF-8") +"="+URLEncoder.encode(userInfoData.get(11), "UTF-8")+"&"+
                        URLEncoder.encode("update_user_id", "UTF-8") +"="+URLEncoder.encode(userInfoData.get(12), "UTF-8")+"&"+
                        URLEncoder.encode("update_date", "UTF-8") +"="+URLEncoder.encode(userInfoData.get(13), "UTF-8")+"&"+
                        URLEncoder.encode("update_time", "UTF-8") +"="+URLEncoder.encode(userInfoData.get(14), "UTF-8")+"&"+
                        URLEncoder.encode("server_sync", "UTF-8") +"="+URLEncoder.encode(userInfoData.get(15), "UTF-8")+"&"+
                        URLEncoder.encode("software_id", "UTF-8") +"="+URLEncoder.encode(GlobalVariables.softwareLicId, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                String orderResponse;
                response = "";
                while ((orderResponse = bufferedReader.readLine()) != null)
                {
                    response += orderResponse;
                }
                objhttpurlconnection.disconnect();
            }
            else
            {
                response = "Unable to Connect To Server";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String sendUserRightsData()
    {
        String operation = "sync_user_rights";
        try {
            URL objurl = new URL(webURL);
            HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
            objhttpurlconnection.setConnectTimeout(5 * 1000);
            objhttpurlconnection.connect();
            if (objhttpurlconnection.getResponseCode() == 200)
            {
                objhttpurlconnection.disconnect();
                objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("operation", "UTF-8") +"="+URLEncoder.encode(operation, "UTF-8")+"&"+
                        URLEncoder.encode("rights_id", "UTF-8") +"="+URLEncoder.encode(userRightsData.get(0), "UTF-8")+"&"+
                        URLEncoder.encode("user_id", "UTF-8") +"="+URLEncoder.encode(userRightsData.get(1), "UTF-8")+"&"+
                        URLEncoder.encode("create_record", "UTF-8") +"="+URLEncoder.encode(userRightsData.get(2), "UTF-8")+"&"+
                        URLEncoder.encode("read_record", "UTF-8") +"="+URLEncoder.encode(userRightsData.get(3), "UTF-8")+"&"+
                        URLEncoder.encode("edit_record", "UTF-8") +"="+URLEncoder.encode(userRightsData.get(4), "UTF-8")+"&"+
                        URLEncoder.encode("delete_record", "UTF-8") +"="+URLEncoder.encode(userRightsData.get(5), "UTF-8")+"&"+
                        URLEncoder.encode("manage_stock", "UTF-8") +"="+URLEncoder.encode(userRightsData.get(6), "UTF-8")+"&"+
                        URLEncoder.encode("manage_cash", "UTF-8") +"="+URLEncoder.encode(userRightsData.get(7), "UTF-8")+"&"+
                        URLEncoder.encode("manage_settings", "UTF-8") +"="+URLEncoder.encode(userRightsData.get(8), "UTF-8")+"&"+
                        URLEncoder.encode("mobile_app", "UTF-8") +"="+URLEncoder.encode(userRightsData.get(9), "UTF-8")+"&"+
                        URLEncoder.encode("server_sync", "UTF-8") +"="+URLEncoder.encode(userRightsData.get(10), "UTF-8")+"&"+
                        URLEncoder.encode("software_id", "UTF-8") +"="+URLEncoder.encode(GlobalVariables.softwareLicId, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                String orderResponse;
                response = "";
                while ((orderResponse = bufferedReader.readLine()) != null)
                {
                    response += orderResponse;
                }
                objhttpurlconnection.disconnect();
            }
            else
            {
                response = "Unable to Connect To Server";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }


//    ------------------------------ Send Data to Server End ------------------------------
//    ------------------------------ Send Data to Server End ------------------------------
//    ------------------------------ Send Data to Server End ------------------------------

}
