package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class UpdateCityInfo {
    public ArrayList<ArrayList<String>> districtsData;

    public UpdateCityInfo()
    {
        districtsData = new ArrayList<ArrayList<String>>();
    }

    public ArrayList<ArrayList<String>> getDistrictsData()
    {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement stmt = objMysqlCon.stmt;
        Connection con = objMysqlCon.con;
        ResultSet rs = null;
        ArrayList<String> temp;
        try {
            rs = stmt.executeQuery("SELECT `district_table_id`, `district_name` FROM `district_info` WHERE `district_status` != 'Deleted'");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2));
                districtsData.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return districtsData;
    }
}
