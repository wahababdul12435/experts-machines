package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import controller.UpdateComment;
import controller.UpdateDealerFinance;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ViewDealerFinanceInfo {
    private String srNo;
    private String paymentId;
    private String paymentDate;
    private String paymentDay;
    private String dealerId;
    private String dealerName;
    private String dealerContact;
    private String areaName;
    private String dealerType;
    private String cashCollection;
    private String cashReturn;
    private String cashWaivedOff;
    private String cashPending;
    private String userName;
    private String comment;

    private HBox operationsPane;
    private JFXButton btnComment;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    SimpleDateFormat fmt = new SimpleDateFormat("dd/MMM/yyyy");
    public static String orderId;

    public static StackPane stackPane;
    public static JFXDialog dialog;

    public static ArrayList<String> newIds = new ArrayList<>();

    public ViewDealerFinanceInfo() {
    }

    public ViewDealerFinanceInfo(String srNo, String paymentId, String paymentDate, String paymentDay, String dealerId, String dealerName, String dealerContact, String areaName, String dealerType, String cashCollection, String cashReturn, String cashWaivedOff, String cashPending, String userName, String comment) {
        this.srNo = srNo;
        this.paymentId = paymentId;
        this.paymentDate = paymentDate;
        this.paymentDay = paymentDay;
        this.dealerId = dealerId;
        this.dealerName = dealerName;
        this.dealerContact = dealerContact;
        this.areaName = areaName;
        this.dealerType = dealerType;
        this.cashCollection = cashCollection == null || cashCollection.equals("-") ? "0" : String.format("%,.2f", Float.parseFloat(cashCollection));
        this.cashReturn = cashReturn == null || cashReturn.equals("-") ? "0" : String.format("%,.2f", Float.parseFloat(cashReturn));
        this.cashWaivedOff = cashWaivedOff == null || cashWaivedOff.equals("-") ? "0" : String.format("%,.2f", Float.parseFloat(cashWaivedOff));
        this.cashPending = cashPending == null || cashPending.equals("-") ? "0" : String.format("%,.2f", Float.parseFloat(cashPending));
        this.userName = userName;
        this.comment = comment;

        this.btnComment = new JFXButton();
        this.btnEdit = new JFXButton();
        this.btnDelete = new JFXButton();
        this.btnComment.setGraphic(GlobalVariables.createCommentIcon());
        this.btnEdit.setGraphic(GlobalVariables.createEditIcon());
        this.btnDelete.setGraphic(GlobalVariables.createDeleteIcon());
        this.btnComment.getStyleClass().add("btn_icon");
        this.btnEdit.getStyleClass().add("btn_icon");
        this.btnDelete.getStyleClass().add("btn_icon");
        this.operationsPane = new HBox(btnComment, btnEdit, btnDelete);
        this.btnComment.setOnAction((action)->commentClicked());
        this.btnEdit.setOnAction((action)->editClicked());
    }

    private void commentClicked()
    {
        Parent parent = null;
        try {
            UpdateComment.commentType = "View Dealer Finance";
            UpdateComment.orderId = this.paymentId;
            UpdateComment.comment = this.comment;
            parent = FXMLLoader.load(getClass().getResource("/view/update_comment.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setStyle("-fx-padding: -20 -10 -20 -10;");
        dialogLayout.setBody(parent);
        dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        stackPane.setVisible(true);
        dialog.show();
        dialog.setOverlayClose(false);
    }

    private void editClicked()
    {
        Parent parent = null;
        DealerFinanceReportInfo.dealerFinanceId = dealerId;
        try {
            UpdateDealerFinance.paymentId = paymentId;
            UpdateDealerFinance.dealerId = dealerId;
            UpdateDealerFinance.dealerName = dealerName;
            UpdateDealerFinance.date = paymentDate;
            UpdateDealerFinance.returnPage = "View Dealer Finance";
            if(!cashCollection.equals("-"))
            {
                UpdateDealerFinance.preCashAmount = cashCollection;
                UpdateDealerFinance.cashAmount = cashCollection;
                UpdateDealerFinance.preCashType = "Collection";
                UpdateDealerFinance.cashType = "Collection";
            }
            else if(!cashReturn.equals("-"))
            {
                UpdateDealerFinance.preCashAmount = cashReturn;
                UpdateDealerFinance.cashAmount = cashReturn;
                UpdateDealerFinance.preCashType = "Return";
                UpdateDealerFinance.cashType = "Return";
            }
            else if(!cashWaivedOff.equals("-"))
            {
                UpdateDealerFinance.preCashAmount = cashWaivedOff;
                UpdateDealerFinance.cashAmount = cashWaivedOff;
                UpdateDealerFinance.preCashType = "Waive Off";
                UpdateDealerFinance.cashType = "Waive Off";
            }

            parent = FXMLLoader.load(getClass().getResource("/view/update_dealer_finance.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setStyle("-fx-padding: -20 -10 -20 -10;");
        dialogLayout.setBody(parent);
        dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        stackPane.setVisible(true);
        dialog.show();
        dialog.setOverlayClose(false);
    }


    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getPaymentDay() {
        return paymentDay;
    }

    public void setPaymentDay(String paymentDay) {
        this.paymentDay = paymentDay;
    }

    public String getDealerId() {
        return dealerId;
    }

    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getDealerContact() {
        return dealerContact;
    }

    public void setDealerContact(String dealerContact) {
        this.dealerContact = dealerContact;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getDealerType() {
        return dealerType;
    }

    public void setDealerType(String dealerType) {
        this.dealerType = dealerType;
    }

    public String getCashCollection() {
        return cashCollection;
    }

    public void setCashCollection(String cashCollection) {
        this.cashCollection = cashCollection;
    }

    public String getCashReturn() {
        return cashReturn;
    }

    public void setCashReturn(String cashReturn) {
        this.cashReturn = cashReturn;
    }

    public String getCashWaivedOff() {
        return cashWaivedOff;
    }

    public void setCashWaivedOff(String cashWaivedOff) {
        this.cashWaivedOff = cashWaivedOff;
    }

    public String getCashPending() {
        return cashPending;
    }

    public void setCashPending(String cashPending) {
        this.cashPending = cashPending;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnComment() {
        return btnComment;
    }

    public void setBtnComment(JFXButton btnComment) {
        this.btnComment = btnComment;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public ArrayList<ArrayList<String>> getDealerFinanceInfo(Statement stmt, Connection con)
    {
        ArrayList<ArrayList<String>> dealerFinanceData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT `dealer_payments`.`payment_id`, `dealer_payments`.`date`, `dealer_payments`.`day`, `dealer_payments`.`dealer_id`, `dealer_info`.`dealer_name`, `dealer_info`.`dealer_phone`, `area_info`.`area_name`, `dealer_info`.`dealer_type`, `dealer_payments`.`amount`, `dealer_payments`.`cash_type`, `dealer_payments`.`pending_payments`, `user_info`.`user_name`, `dealer_payments`.`comments` FROM `dealer_payments` LEFT OUTER JOIN `dealer_info` ON `dealer_info`.`dealer_table_id` = `dealer_payments`.`dealer_id` LEFT OUTER JOIN `area_info` ON `area_info`.`area_table_id` = `dealer_info`.`dealer_area_id` LEFT OUTER JOIN `user_info` ON `user_info`.`user_table_id` = `dealer_payments`.`user_id` WHERE `dealer_payments`.`status` != 'Deleted' GROUP BY `dealer_payments`.`payment_id` ORDER BY str_to_date(`dealer_payments`.`date`, '%d/%b/%Y') DESC LIMIT 5000");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Payment Id
                temp.add(rs.getString(2)); // Payment Date
                temp.add(rs.getString(3)); // Payment Day
                temp.add(rs.getString(4)); // Dealer Id
                temp.add(rs.getString(5)); // Dealer Name
                temp.add(rs.getString(6)); // Dealer Contact
                temp.add(rs.getString(7)); // Area Name
                temp.add(rs.getString(8)); // Dealer Type
                if(rs.getString(10).equals("Collection"))
                {
                    temp.add(rs.getString(9));
                    temp.add("-");
                    temp.add("-");
                }
                else if(rs.getString(10).equals("Return"))
                {
                    temp.add("-");
                    temp.add(rs.getString(9));
                    temp.add("-");
                }
                else
                {
                    temp.add("-");
                    temp.add("-");
                    temp.add(rs.getString(9));
                }
                temp.add(rs.getString(11)); // Pending Payments
                temp.add(rs.getString(12)); // User Name
                temp.add(rs.getString(13)); // Comment
                dealerFinanceData.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dealerFinanceData;
    }

    public ArrayList<ArrayList<String>> getDealerFinanceInfoSearch(Statement stmt, Connection con, String dealerId, String dealerName, String areaName, String dealerType, String fromDate, String toDate, String day, String cashType)
    {
        ArrayList<ArrayList<String>> dealerFinanceData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        String searchQuery;
        int multipleSearch = 0;
        searchQuery = "SELECT `dealer_payments`.`payment_id`, `dealer_payments`.`date`, `dealer_payments`.`day`, `dealer_payments`.`dealer_id`, `dealer_info`.`dealer_name`, `dealer_info`.`dealer_phone`, `area_info`.`area_name`, `dealer_info`.`dealer_type`, `dealer_payments`.`amount`, `dealer_payments`.`cash_type`, `dealer_payments`.`pending_payments`, `user_info`.`user_name`, `dealer_payments`.`comments` FROM `dealer_payments` LEFT OUTER JOIN `dealer_info` ON `dealer_info`.`dealer_table_id` = `dealer_payments`.`dealer_id` LEFT OUTER JOIN `area_info` ON `area_info`.`area_table_id` = `dealer_info`.`dealer_area_id` LEFT OUTER JOIN `user_info` ON `user_info`.`user_table_id` = `dealer_payments`.`user_id` WHERE `dealer_payments`.`status` != 'Deleted' ";
        if(!dealerId.equals(""))
        {
            searchQuery += "AND `dealer_info`.`dealer_id` = '"+dealerId+"' ";
        }
        if(!dealerName.equals(""))
        {
            searchQuery += "AND `dealer_info`.`dealer_name` = '"+dealerName+"' ";
        }
        if(!areaName.equals(""))
        {
            searchQuery += "AND `area_info`.`area_name` LIKE %'"+areaName+"'% ";
        }
        if(!dealerType.equals("All"))
        {
            searchQuery += "AND `dealer_info`.`dealer_type` = '"+dealerType+"' ";
        }
        if(!fromDate.equals(""))
        {
            searchQuery += "AND str_to_date(`dealer_payments`.`date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') ";;
        }
        if(!toDate.equals(""))
        {
            searchQuery += "AND str_to_date(`dealer_payments`.`date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y') ";
        }
        if(!day.equals("All"))
        {
            searchQuery += "AND `dealer_payments`.`day` = '"+day+"' ";
        }
        if(!cashType.equals("All"))
        {
            searchQuery += "AND `dealer_payments`.`cash_type` = '"+cashType+"' ";
        }
        searchQuery += "GROUP BY `dealer_payments`.`payment_id` ORDER BY str_to_date(`dealer_payments`.`date`, '%d/%b/%Y') DESC";
        try {
            rs = stmt.executeQuery(searchQuery);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Payment Id
                temp.add(rs.getString(2)); // Payment Date
                temp.add(rs.getString(3)); // Payment Day
                temp.add(rs.getString(4)); // Dealer Id
                temp.add(rs.getString(5)); // Dealer Name
                temp.add(rs.getString(6)); // Dealer Contact
                temp.add(rs.getString(7)); // Area Name
                temp.add(rs.getString(8)); // Dealer Type
                if(rs.getString(10).equals("Collection"))
                {
                    temp.add(rs.getString(9));
                    temp.add("-");
                    temp.add("-");
                }
                else if(rs.getString(10).equals("Return"))
                {
                    temp.add("-");
                    temp.add(rs.getString(9));
                    temp.add("-");
                }
                else
                {
                    temp.add("-");
                    temp.add("-");
                    temp.add(rs.getString(9));
                }
                temp.add(rs.getString(11)); // Pending Payments
                temp.add(rs.getString(12)); // User Name
                temp.add(rs.getString(13)); // Comment
                dealerFinanceData.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dealerFinanceData;
    }

    public void insertNewDealerFinance(ArrayList<ArrayList<String>> financeData)
    {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement objStmt = objMysqlCon.stmt;
        Connection objCon = objMysqlCon.con;
        String status = "Pending";
        ResultSet rs = null;
        boolean check = false;
        ArrayList<String> serverIds = new ArrayList<>();
        ArrayList<String> softwareIds = new ArrayList<>();
        ArrayList<String> dealerPendingPayments = new ArrayList<>();
        float pendingPayment = 0;
        int num = 0;
        try {
            String insertQuery = "INSERT INTO `dealer_payments`(`dealer_id`, `order_id`, `amount`, `cash_type`, `pending_payments`, `date`, `day`, `user_id`, `status`, `server_id`, `server_sync`) VALUES ";

            for(int i=0; i<financeData.size(); i++)
            {
                serverIds.add(financeData.get(i).get(0));
                String getDealerPendingFinance = "SELECT `pending_payments` FROM `dealer_payments` WHERE `dealer_id` = '"+financeData.get(i).get(1)+"' ORDER BY `payment_id` DESC LIMIT 1";
                rs = objStmt.executeQuery(getDealerPendingFinance);
                while (rs.next())
                {
                    pendingPayment = rs.getFloat(1);
                }
                if(financeData.get(i).get(4).equals("Collection"))
                {
                    pendingPayment = pendingPayment - Float.parseFloat(financeData.get(i).get(3));
                }
                else if (financeData.get(i).get(4).equals("Return"))
                {
                    pendingPayment = pendingPayment + Float.parseFloat(financeData.get(i).get(3));
                }
                else
                {
                    pendingPayment = pendingPayment - Float.parseFloat(financeData.get(i).get(3));
                }

                if(i == financeData.size()-1)
                {
                    insertQuery += "('"+financeData.get(i).get(1)+"','"+financeData.get(i).get(2)+"','"+financeData.get(i).get(3)+"','"+financeData.get(i).get(4)+"' ,'"+pendingPayment+"' ,'"+financeData.get(i).get(5)+"','"+financeData.get(i).get(6)+"','"+financeData.get(i).get(7)+"', '"+financeData.get(i).get(8)+"', '"+financeData.get(i).get(0)+"', 'Done') ";
                }
                else
                {
                    insertQuery += "('"+financeData.get(i).get(1)+"','"+financeData.get(i).get(2)+"','"+financeData.get(i).get(3)+"','"+financeData.get(i).get(4)+"' ,'"+pendingPayment+"' ,'"+financeData.get(i).get(5)+"','"+financeData.get(i).get(6)+"','"+financeData.get(i).get(7)+"', '"+financeData.get(i).get(8)+"', '"+financeData.get(i).get(0)+"', 'Done'), ";
                }
            }
            objStmt.executeUpdate(insertQuery);

            for(int i=0; i<serverIds.size(); i++)
            {
                String query = "SELECT `payment_id` FROM `dealer_payments` WHERE `server_id` = '"+serverIds.get(i)+"'";
                rs = objStmt.executeQuery(query);
                while (rs.next())
                {
                    softwareIds.add(rs.getString(1));
                }
            }
            newIds = softwareIds;

            SendServerThread objSendServerThread = new SendServerThread(serverIds, softwareIds, "Set new Dealer Finance");
            objSendServerThread.setDaemon(true);
            objSendServerThread.start();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
