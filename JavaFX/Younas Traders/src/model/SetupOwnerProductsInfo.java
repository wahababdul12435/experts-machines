package model;

import com.jfoenix.controls.JFXCheckBox;
import controller.SetupOwnerProducts;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class SetupOwnerProductsInfo {
    private String srNo;
    private String companyName;
    private String productName;
    private String productType;
    private String packSize;
    private String cartonSize;
    private String purchasePrice;
    private String tradePrice;
    private String retailPrice;
    private JFXCheckBox operationsPane;

    public static ArrayList<String> companyIdArr = new ArrayList<>();
    public static ArrayList<String> companyNameArr = new ArrayList<>();
    public static ArrayList<String> productNameArr = new ArrayList<>();
    public static ArrayList<String> productTypeArr = new ArrayList<>();
    public static ArrayList<String> packSizeArr = new ArrayList<>();
    public static ArrayList<String> cartonSizeArr = new ArrayList<>();
    public static ArrayList<String> purchasePriceArr = new ArrayList<>();
    public static ArrayList<String> tradePriceArr = new ArrayList<>();
    public static ArrayList<String> retailPriceArr = new ArrayList<>();
    public static ArrayList<JFXCheckBox> arrCheckBox = new ArrayList<>();

    public static Label lblProductsSelected;

    String webUrl;
    public static String companyIds = "";

    public static ArrayList<ArrayList<String>> productsData = new ArrayList<>();
    public static TableView<SetupOwnerProductsInfo> table_viewproducts;

    public SetupOwnerProductsInfo() {
        this.webUrl = "https://tuberichy.com/ProfitFlow/ServerDataGet/GetData.php?software_lic_id="+GlobalVariables.softwareLicId+"&required_data=products_detail&company_ids="+companyIds;
        String fetchedOrder = fetchServerData();
        if(fetchedOrder != null && !fetchedOrder.equals(""))
        {
            loadIntoListView(fetchedOrder);
            table_viewproducts.setItems(SetupOwnerProducts.parseUserList());
        }
    }

    public SetupOwnerProductsInfo(String srNo, String companyName, String productName, String productType, String packSize, String cartonSize, String purchasePrice, String tradePrice, String retailPrice) {
        this.srNo = srNo;
        this.companyName = companyName;
        this.productName = productName;
        this.productType = productType;
        this.packSize = packSize;
        this.cartonSize = cartonSize;
        this.purchasePrice = purchasePrice;
        this.tradePrice = tradePrice;
        this.retailPrice = retailPrice;

        this.operationsPane = new JFXCheckBox();
        this.operationsPane.setOnAction((action)->checkClicked());
        arrCheckBox.add(this.operationsPane);
        this.operationsPane.setAlignment(Pos.CENTER);
    }

    private void checkClicked()
    {
        if(this.operationsPane.isSelected())
        {
            SetupOwnerProducts.selectedProducts++;
            lblProductsSelected.setText(String.valueOf(SetupOwnerProducts.selectedProducts));
            arrCheckBox.get(Integer.parseInt(srNo)-1).setSelected(true);
        }
        else
        {
            SetupOwnerProducts.selectedProducts--;
            lblProductsSelected.setText(String.valueOf(SetupOwnerProducts.selectedProducts));
            arrCheckBox.get(Integer.parseInt(srNo)-1).setSelected(false);
        }
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getPackSize() {
        return packSize;
    }

    public void setPackSize(String packSize) {
        this.packSize = packSize;
    }

    public String getCartonSize() {
        return cartonSize;
    }

    public void setCartonSize(String cartonSize) {
        this.cartonSize = cartonSize;
    }

    public String getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(String purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public String getTradePrice() {
        return tradePrice;
    }

    public void setTradePrice(String tradePrice) {
        this.tradePrice = tradePrice;
    }

    public String getRetailPrice() {
        return retailPrice;
    }

    public void setRetailPrice(String retailPrice) {
        this.retailPrice = retailPrice;
    }

    public JFXCheckBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(JFXCheckBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public ArrayList<ArrayList<String>> getProductsData() {
        return productsData;
    }

    private String fetchServerData()
    {
        try
        {
            URL url = new URL(webUrl);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setConnectTimeout(2 * 1000);
            con.connect();
            if (con.getResponseCode() == 200)
            {
                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String json;
                while ((json = bufferedReader.readLine()) != null)
                {
                    sb.append(json + "\n");
                }
                return sb.toString().trim();
            }
            else
            {
                return null;
            }
        }
        catch (Exception e)
        {
            return null;
        }
    }

    private void loadIntoListView(String json) throws JSONException {
        JSONArray jsonArray = new JSONArray(json);
        productsData = new ArrayList<>();
        ArrayList<String> temp;
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject obj = jsonArray.getJSONObject(i);
            temp = new ArrayList<>();
            temp.add(obj.getString("company_name"));
            temp.add(obj.getString("product_name"));
            temp.add(obj.getString("product_type"));
            temp.add(obj.getString("pack_size"));
            temp.add(obj.getString("carton_size"));
            temp.add(String.valueOf(obj.getFloat("purchase_price")));
            temp.add(String.valueOf(obj.getFloat("trade_price")));
            temp.add(String.valueOf(obj.getFloat("retail_price")));
            productsData.add(temp);
        }
    }

    public boolean insertProduct(Statement stmt) {
        String currentDate = GlobalVariables.getStDate();
        String currentTime = GlobalVariables.getStTime();
        String currentUser = GlobalVariables.getUserId();
        boolean firstIter = true;
        String insertQuery = "";
        String insertProductStock;
        String productTableId;
        int insertInfo = 1;

        for(int i=0; i<arrCheckBox.size(); i++)
        {
            if(arrCheckBox.get(i).isSelected())
            {
                insertQuery = "INSERT INTO `product_info`(`company_table_id`, `group_table_id`, `product_name`, `product_type`,`packSize`,`carton_size`, `retail_price`, `trade_price`, `purchase_price`, `purchase_discount`, `sales_tax`, `product_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`, `server_sync`) VALUES ('"+companyIdArr.get(i)+"','0','"+productNameArr.get(i)+"','"+productTypeArr.get(i)+"','"+packSizeArr.get(i)+"','"+cartonSizeArr.get(i)+"','"+retailPriceArr.get(i)+"','"+tradePriceArr.get(i)+"','"+purchasePriceArr.get(i)+"', '0', '0', 'Active','"+currentUser+"','"+currentDate+"','"+currentTime+"','0','','', 'Insert')";
            }
            try {
                if(stmt.executeUpdate(insertQuery) == -1)
                {
                    insertInfo = -1;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            productTableId = getProductTableId(stmt, productNameArr.get(i), companyIdArr.get(i), currentDate);
            insertProductStock = "INSERT INTO `products_stock`(`product_table_id`, `in_stock`, `min_stock`, `max_stock`, `bonus_quant`, `status`, `server_sync`) VALUES ('"+productTableId+"','0','0','0','0','Active', 'Insert')";
                try {
                    if(stmt.executeUpdate(insertProductStock) == -1)
                {
                    insertInfo = -1;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if(insertInfo == -1)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public String getProductTableId(Statement stmt3 , String productName, String companyId, String creatingDate)
    {
        String checkProductId = "0";
        ResultSet rs3 = null;
        try {
            String getIdStatment = "SELECT `product_table_id` FROM `product_info` WHERE `product_name` = '"+productName+"' AND `company_table_id` = '"+companyId+"' AND `creating_date` = '"+creatingDate+"' AND `product_status` != 'Deleted'";
            rs3 = stmt3.executeQuery(getIdStatment);
            while (rs3.next())
            {
                checkProductId = rs3.getString(1) ;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return checkProductId;
    }
}
