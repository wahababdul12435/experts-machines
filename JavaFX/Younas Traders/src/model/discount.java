package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Discount {
    String approvalids;
    String dealerIds;
    String dealerNames;
    String companyId;
    String companyName;
    String saleamount;
    String discount_percent;
    String startdates;
    String enddates;

    public Discount()
    {}

    public Discount(String approvalids, String dealerIds, String dealerNames,String companyId,String companyName, String saleamount, String discount_percent, String startdates, String enddates) {
        this.approvalids = approvalids;
        this.dealerIds = dealerIds;
        this.dealerNames = dealerNames;
        this.companyId = companyId;
        this.companyName = companyName;
        this.saleamount = saleamount;
        this.discount_percent = discount_percent;
        this.startdates = startdates;
        this.enddates = enddates;
    }

    public String getApprovalids() {
        return approvalids;
    }

    public void setApprovalids(String approvalids) {
        this.approvalids = approvalids;
    }

    public String getDealerIds() {
        return dealerIds;
    }

    public void setDealerIds(String dealerIds) {
        this.dealerIds = dealerIds;
    }

    public String getDealerNames() {
        return dealerNames;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public void setDealerNames(String dealerNames) {
        this.dealerNames = dealerNames;
    }

    public String getSaleamount() {
        return saleamount;
    }

    public void setSaleamount(String saleamount) {
        this.saleamount = saleamount;
    }

    public String getDiscount_percent() {
        return discount_percent;
    }

    public void setDiscount_percent(String discount_percent) {
        this.discount_percent = discount_percent;
    }

    public String getStartdates() {
        return startdates;
    }

    public void setStartdates(String startdates) {
        this.startdates = startdates;
    }

    public String getEnddates() {
        return enddates;
    }

    public void setEnddates(String enddates) {
        this.enddates = enddates;
    }

    ArrayList<String> companylist = new ArrayList<>();
    int limit=1000;
    public ArrayList<String> get_AllCompanies(Statement stmt2, Connection con2)
    {
        ResultSet rs = null;
        try {
            rs = stmt2.executeQuery("select company_name from company_info where company_status='Active'  order by  company_name ASC ");
            int i = 0;
            while (rs.next() && i < limit)
            {
                companylist.add(rs.getString(1));
                i++;
            }

            con2.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return companylist;
    }

    public ArrayList<ArrayList<String>> getCurrentDiscountInfo(Statement stmt, Connection con)
    {
        ArrayList<ArrayList<String>> bonusData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        String query = "SELECT `dealer_table_id`, `company_table_id`, `product_table_id`, `sale_amount`, `discount_percent` FROM `discount_policy` WHERE str_to_date(`start_date`, '%d/%b/%Y') >= str_to_date('"+GlobalVariables.getStDate()+"', '%d/%b/%Y') AND str_to_date(`end_date`, '%d/%b/%Y') <= str_to_date('"+GlobalVariables.getStDate()+"', '%d/%b/%Y') AND `policy_status` = 'Active'";
        try {
            rs = stmt.executeQuery(query);
//            System.out.println(query);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Dealer Id
                temp.add(rs.getString(2)); // Company Id
                temp.add(rs.getString(3)); // Product Id
                temp.add(rs.getString(4)); // Sale Amount
                temp.add(rs.getString(5)); // Discount
                bonusData.add(temp);
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return bonusData;
    }
}
