package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import controller.CreatePurchaseReturn;
import controller.CreatePurchaseReturn;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class CreatePurchaseReturnInfo {
    public String returnId;
    private String srNo;
    private String productId;
    private String productName;
    private String batchNo;
    private String returnQuantity;
    private String returnPrice;
    private String returnReason;

    private HBox operationsPane;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    public static ArrayList<String> productIdArr = new ArrayList<>();
    public static ArrayList<String> productNameArr = new ArrayList<>();
    public static ArrayList<String> batchNoArr = new ArrayList<>();
    public static ArrayList<String> returnQuantityArr = new ArrayList<>();
    public static ArrayList<String> returnPriceArr = new ArrayList<>();
    //    public static ArrayList<String> unitArr = new ArrayList<>();
    public static ArrayList<String> returnReasonArr = new ArrayList<>();

    public static TableView<CreatePurchaseReturnInfo> table_purchasereturndetaillog;

    public static JFXTextField txtProductName;
    public static JFXComboBox<String> txtBatchNo;
    public static JFXTextField txtReturnQuantity;
    public static JFXTextField txtReturnPrice;
    //    public static JFXComboBox<String>  txtUnit;
    public static JFXComboBox<String> txtReturnReason;
    public static Label lbl_items_returned;
    public static Label lbl_return_price;

    public static Button btnAdd;
    public static Button btnCancel;
    public static Button btnUpdate;

    public CreatePurchaseReturnInfo() {
    }

    public CreatePurchaseReturnInfo(String srNo, String productName, String batchNo, String returnQuantity, String returnPrice, String returnReason) {
        this.srNo = srNo;
        this.productName = productName;
        this.batchNo = batchNo;
        this.returnQuantity = returnQuantity;
        this.returnPrice = returnPrice;
        this.returnReason = returnReason;

        this.btnEdit = new JFXButton();
        this.btnDelete = new JFXButton();
        this.btnEdit.setGraphic(GlobalVariables.createEditIcon());
        this.btnDelete.setGraphic(GlobalVariables.createDeleteIcon());
        this.btnEdit.getStyleClass().add("btn_icon");
        this.btnDelete.getStyleClass().add("btn_icon");
        this.operationsPane = new HBox(btnEdit, btnDelete);
        this.btnEdit.setOnAction((action)->editAddedRecord());
        this.btnDelete.setOnAction((action)->deleteAddedRecord());
    }

    private void editAddedRecord()
    {
        CreatePurchaseReturn.srNo = Integer.parseInt(srNo)-1;
        txtProductName.setText(productNameArr.get(Integer.parseInt(srNo)-1));
        txtBatchNo.getItems().clear();
        String productId = CreatePurchaseReturn.getProductId(CreatePurchaseReturn.completeProductData, txtProductName.getText());
        ArrayList<String> batchData = CreatePurchaseReturn.getProductsBatch(CreatePurchaseReturn.completeBatchData, productId);
        txtBatchNo.getItems().addAll(batchData);
        txtBatchNo.setValue(batchNoArr.get(Integer.parseInt(srNo)-1));
        txtReturnQuantity.setText(returnQuantityArr.get(Integer.parseInt(srNo)-1));
        txtReturnPrice.setText(returnPriceArr.get(Integer.parseInt(srNo)-1));
//        txtUnit.setValue(unitArr.get(Integer.parseInt(srNo)-1));
        txtReturnReason.setValue(returnReasonArr.get(Integer.parseInt(srNo)-1));
        CreatePurchaseReturn.selectedProductId = productIdArr.get(Integer.parseInt(srNo)-1);
        CreatePurchaseReturn.selectedProductPrice = String.valueOf(Float.parseFloat(returnPriceArr.get(Integer.parseInt(srNo)-1))/Integer.parseInt(returnQuantityArr.get(Integer.parseInt(srNo)-1)));
        btnAdd.setVisible(false);
        btnCancel.setVisible(true);
        btnUpdate.setVisible(true);
    }

    private void deleteAddedRecord()
    {
        CreatePurchaseReturn.itemsReturned -= Integer.parseInt(returnQuantityArr.get(Integer.parseInt(srNo)-1).replace(",",""));
        CreatePurchaseReturn.returnPrice -= Float.parseFloat(returnPriceArr.get(Integer.parseInt(srNo)-1).replace(",",""));
        productIdArr.remove(Integer.parseInt(srNo)-1);
        productNameArr.remove(Integer.parseInt(srNo)-1);
        batchNoArr.remove(Integer.parseInt(srNo)-1);
        returnQuantityArr.remove(Integer.parseInt(srNo)-1);
        returnPriceArr.remove(Integer.parseInt(srNo)-1);
//        unitArr.remove(Integer.parseInt(srNo)-1);
        returnReasonArr.remove(Integer.parseInt(srNo)-1);
        table_purchasereturndetaillog.setItems(CreatePurchaseReturn.parseUserList());
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    public String getReturnQuantity() {
        return returnQuantity;
    }

    public void setReturnQuantity(String returnQuantity) {
        this.returnQuantity = returnQuantity;
    }

    public String getReturnPrice() {
        return returnPrice;
    }

    public void setReturnPrice(String returnPrice) {
        this.returnPrice = returnPrice;
    }

    public String getReturnReason() {
        return returnReason;
    }

    public void setReturnReason(String returnReason) {
        this.returnReason = returnReason;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public ArrayList<ArrayList<String>> loadReturnTable()
    {
        ArrayList<ArrayList<String>> saleData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        for(int i=0; i<productIdArr.size(); i++)
        {
            temp = new ArrayList<String>();
            temp.add(productNameArr.get(i));
            temp.add(batchNoArr.get(i));
            temp.add(returnQuantityArr.get(i));
            temp.add(returnPriceArr.get(i));
            temp.add(returnReasonArr.get(i));
            saleData.add(temp);
        }
        return saleData;
    }

    public void insertBasic(Statement stmt, String companyId, String returnPrice)
    {
        String currentDate = GlobalVariables.getStDate();
        String currentTime = GlobalVariables.getStTime();
        String currentUser = GlobalVariables.getUserId();
        String status = "Active";
        String serverSync = "Insert";
        ResultSet rs = null;
        String updateQuery = "INSERT INTO `purchase_return`(`company_table_id`, `return_total_price`, `return_gross_price`, `entry_date`, `entry_time`, `user_id`, `status`, `server_sync`) VALUES ('"+companyId+"', '"+returnPrice+"', '"+returnPrice+"', '"+currentDate+"', '"+currentTime+"', '"+currentUser+"', '"+status+"', '"+serverSync+"')";
        try {
            stmt.executeUpdate(updateQuery);
            String getReturnId = "SELECT `returnTable_id` FROM `purchase_return` WHERE `company_table_id` = '"+companyId+"' AND `entry_date` = '"+currentDate+"' AND `entry_time` = '"+currentTime+"'";
            rs = stmt.executeQuery(getReturnId);
            rs.next();
            returnId = rs.getString(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertReturn(Statement stmt)
    {
        String currentDate = GlobalVariables.getStDate();
        String currentTime = GlobalVariables.getStTime();
        String currentUser = GlobalVariables.getUserId();
        String updateProductStock = "";
        String updateBatchStock = "";
        String updateQuery = "INSERT INTO `purchase_return_detail`(`return_id`, `prod_id`, `prod_batch`, `prod_quant`, `bonus_quant`, `discount_amount`, `total_amount`, `unit`, `return_reason`) VALUES ";
        String unit = "Packets";
        try {
            for(int i=0; i<productIdArr.size(); i++)
            {
                if(i == productIdArr.size()-1)
                {
                    updateQuery += "('"+returnId+"','"+productIdArr.get(i)+"','"+batchNoArr.get(i)+"','"+returnQuantityArr.get(i)+"','0','0','"+returnPriceArr.get(i)+"','"+unit+"','"+returnReasonArr.get(i)+"') ";
                }
                else
                {
                    updateQuery += "('"+returnId+"','"+productIdArr.get(i)+"','"+batchNoArr.get(i)+"','"+returnQuantityArr.get(i)+"','0','0','"+returnPriceArr.get(i)+"','"+unit+"','"+returnReasonArr.get(i)+"'), ";
                }
                updateProductStock = "UPDATE `products_stock` SET `in_stock` = `in_stock` - '"+returnQuantityArr.get(i)+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `product_table_id` = '"+productIdArr.get(i)+"'; ";
                updateBatchStock = "UPDATE `batchwise_stock` SET `quantity` = `quantity` - '"+returnQuantityArr.get(i)+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `product_table_id` = '"+productIdArr.get(i)+"' AND `batch_no` = '"+batchNoArr.get(i)+"';";
                stmt.executeUpdate(updateProductStock);
                stmt.executeUpdate(updateBatchStock);
            }
//            updateQuery = "UPDATE `dealer_info` SET `dealer_id`='"+dealerId+"',`dealer_area_id`='"+areaId+"',`dealer_name`='"+dealerName+"',`dealer_phone`='"+dealerContact+"',`dealer_address`='"+dealerAddress+"',`dealer_type`='"+dealerType+"',`dealer_cnic`='"+dealerCnic+"',`dealer_lic9_num`='"+dealerLicNum+"',`dealer_lic9_exp`='"+dealerLicExp+"',`dealer_status`='"+dealerStatus+"', `update_user_id`='"+currentUser+"',`update_date`='"+currentDate+"',`update_time`='"+currentTime+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `dealer_table_id` = '"+dealerTableId+"'";
            stmt.executeUpdate(updateQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
