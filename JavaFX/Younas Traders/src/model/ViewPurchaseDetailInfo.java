package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import controller.ViewPurchaseDetail;
import controller.ViewSaleDetail;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ViewPurchaseDetailInfo {
    public String purchaseId;
    public String invoiceNum;
    private String srNo;
    private String productId;
    private String productName;
    private String batchNo;
    private String batchExpiry;
    private String productQty;
    private String bonusQty;
    private String productPrice;
    private String productDiscount;
    private String productNetPrice;

    private HBox operationsPane;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    public static ArrayList<String> productIdArr = new ArrayList<>();
    public static ArrayList<String> productNameArr = new ArrayList<>();
    public static ArrayList<String> batchNoArr = new ArrayList<>();
    public static ArrayList<String> batchExpiryArr = new ArrayList<>();
    public static ArrayList<String> productQtyArr = new ArrayList<>();
    public static ArrayList<String> bonusQtyArr = new ArrayList<>();
    public static ArrayList<String> productPriceArr = new ArrayList<>();
    public static ArrayList<String> productDiscountArr = new ArrayList<>();
    public static ArrayList<String> productNetPriceArr = new ArrayList<>();

    public static TableView<ViewPurchaseDetailInfo> table_purchasedetaillog;

    public static JFXComboBox<String> txtProductName;
    public static JFXTextField txtBatchNo;
    public static JFXDatePicker txtBatchExpiry;
    public static JFXTextField txtProductQty;
    public static JFXTextField txtProductBonus;
    public static JFXTextField txtProductPrice;
    public static JFXTextField txtProductDiscount;
    public static JFXTextField txtProductNetPrice;

    public static Button btnAdd;
    public static Button btnCancel;
    public static Button btnUpdate;

    public ViewPurchaseDetailInfo(String purchaseId) {
        this.purchaseId = purchaseId;
    }

    public ViewPurchaseDetailInfo(String srNo, String productId, String productName, String batchNo, String batchExpiry, String productQty, String bonusQty, String productPrice, String productDiscount, String productNetPrice) {
        this.srNo = srNo;
        this.productId = productId;
        this.productName = productName;
        this.batchNo = batchNo;
        this.batchExpiry = batchExpiry;
        this.productQty = productQty;
        this.bonusQty = bonusQty;
        this.productPrice = productPrice;
        this.productDiscount = productDiscount;
        this.productNetPrice = productNetPrice;

        this.btnEdit = new JFXButton();
        this.btnDelete = new JFXButton();
        this.btnEdit.setGraphic(GlobalVariables.createEditIcon());
        this.btnDelete.setGraphic(GlobalVariables.createDeleteIcon());
        this.btnEdit.getStyleClass().add("btn_icon");
        this.btnDelete.getStyleClass().add("btn_icon");
        this.operationsPane = new HBox(btnEdit, btnDelete);
        this.btnEdit.setOnAction((action)->editAddedRecord());
        this.btnDelete.setOnAction((action)->deleteAddedRecord());
    }

    private void editAddedRecord()
    {
        ViewPurchaseDetail.srNo = Integer.parseInt(srNo)-1;
        txtProductName.setValue(productNameArr.get(Integer.parseInt(srNo)-1));
        txtBatchNo.setText(batchNoArr.get(Integer.parseInt(srNo)-1));
        txtBatchExpiry.setValue(GlobalVariables.LOCAL_DATE(batchExpiryArr.get(Integer.parseInt(srNo)-1)));
        txtProductQty.setText(productQtyArr.get(Integer.parseInt(srNo)-1));
        txtProductBonus.setText(bonusQtyArr.get(Integer.parseInt(srNo)-1));
        txtProductPrice.setText(productPriceArr.get(Integer.parseInt(srNo)-1));
        txtProductDiscount.setText(productDiscountArr.get(Integer.parseInt(srNo)-1));
        txtProductNetPrice.setText(productNetPriceArr.get(Integer.parseInt(srNo)-1));
        btnAdd.setVisible(false);
        btnCancel.setVisible(true);
        btnUpdate.setVisible(true);
    }

    private void deleteAddedRecord()
    {
        productIdArr.remove(Integer.parseInt(srNo)-1);
        productNameArr.remove(Integer.parseInt(srNo)-1);
        batchNoArr.remove(Integer.parseInt(srNo)-1);
        batchExpiryArr.remove(Integer.parseInt(srNo)-1);
        productQtyArr.remove(Integer.parseInt(srNo)-1);
        bonusQtyArr.remove(Integer.parseInt(srNo)-1);
        productPriceArr.remove(Integer.parseInt(srNo)-1);
        productDiscountArr.remove(Integer.parseInt(srNo)-1);
        productNetPriceArr.remove(Integer.parseInt(srNo)-1);
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    public String getBatchExpiry() {
        return batchExpiry;
    }

    public void setBatchExpiry(String batchExpiry) {
        this.batchExpiry = batchExpiry;
    }

    public String getProductQty() {
        return productQty;
    }

    public void setProductQty(String productQty) {
        this.productQty = productQty;
    }

    public String getBonusQty() {
        return bonusQty;
    }

    public void setBonusQty(String bonusQty) {
        this.bonusQty = bonusQty;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductDiscount() {
        return productDiscount;
    }

    public void setProductDiscount(String productDiscount) {
        this.productDiscount = productDiscount;
    }

    public String getProductNetPrice() {
        return productNetPrice;
    }

    public void setProductNetPrice(String productNetPrice) {
        this.productNetPrice = productNetPrice;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public ArrayList<String> getPurchaseBasicInfo(Statement stmt, Connection con)
    {
        ArrayList<String> purchaseBasicData = new ArrayList<String>();
        ArrayList<String> temp;
        ResultSet rs = null;
        String query = "SELECT `purchase_info`.`invoice_num`, `purchase_info`.`comp_id`, `company_info`.`company_name`, `company_info`.`company_contact`, `supplier_info`.`supplier_name`, `purchase_info`.`purchase_date`, `purchase_info`.`status`, (SELECT CONCAT(`user_info`.`user_name`, ' (', `user_info`.`user_table_id`, ')') FROM `user_info` WHERE `purchase_info`.`enter_user_id` = `user_info`.`user_table_id`) as 'enter_by', `purchase_info`.`entry_date`, `purchase_info`.`entry_time`, (SELECT CONCAT(`user_info`.`user_name`, ' (', `user_info`.`user_table_id`, ')') FROM `user_info` WHERE `purchase_info`.`update_user_id` = `user_info`.`user_table_id`) as 'update_by', `purchase_info`.`update_date`, `purchase_info`.`update_time` FROM `purchase_info` LEFT OUTER JOIN `company_info` ON `company_info`.`company_table_id` = `purchase_info`.`comp_id` LEFT OUTER JOIN `supplier_info` ON `supplier_info`.`supplier_table_id` = `purchase_info`.`supplier_id` WHERE `purchase_info`.`purchase_id` = '"+purchaseId+"'";
        try {
            rs = stmt.executeQuery(query);
            while (rs.next())
            {
                invoiceNum = rs.getString(1);
                purchaseBasicData.add(rs.getString(1)); // Invoice No
                purchaseBasicData.add(rs.getString(2)); // Company Id
                purchaseBasicData.add(rs.getString(3)); // Company Name
                purchaseBasicData.add(rs.getString(4)); // Company Contact
                purchaseBasicData.add(rs.getString(5)); // Supplier Name
                purchaseBasicData.add(rs.getString(6)); // Purchase Date
                purchaseBasicData.add(rs.getString(7)); // Status
                purchaseBasicData.add(rs.getString(8)); // Enter By
                purchaseBasicData.add(rs.getString(9)); // Enter Date
                purchaseBasicData.add(rs.getString(10)); // Enter Time
                purchaseBasicData.add(rs.getString(11)); // Update By
                purchaseBasicData.add(rs.getString(12)); // Update Date
                purchaseBasicData.add(rs.getString(13)); // Update Time
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return purchaseBasicData;
    }

    public void getPurchaseDetailInfo(Statement stmt, Connection con)
    {
//        ArrayList<ArrayList<String>> purchaseData = new ArrayList<ArrayList<String>>();
//        ArrayList<String> temp;
        ResultSet rs = null;
        String query = "SELECT `purchase_info_detail`.`prod_id`, `product_info`.`product_name`, `purchase_info_detail`.`batch_no`, `purchase_info_detail`.`expiry_date`, `purchase_info_detail`.`recieve_quant`, `purchase_info_detail`.`bonus_quant`, `purchase_info_detail`.`gross_amount`, `purchase_info_detail`.`disc_amount`, `purchase_info_detail`.`net_amount`, `purchase_info_detail`.`invoice_num` FROM `purchase_info_detail` LEFT OUTER JOIN `product_info` ON `product_info`.`product_table_id` = `purchase_info_detail`.`prod_id` WHERE `purchase_info_detail`.`purchase_id` = '"+purchaseId+"'";
        try {
            rs = stmt.executeQuery(query);
            while (rs.next())
            {
//                temp = new ArrayList<String>();
//                temp.add(rs.getString(1)); // Product Id
                productIdArr.add(rs.getString(1));
//                temp.add(rs.getString(2)); // Product Name
                productNameArr.add(rs.getString(2));
//                temp.add(rs.getString(3)); // Batch No
                batchNoArr.add(rs.getString(3));
//                temp.add(rs.getString(4)); // Batch Expiry
                batchExpiryArr.add(rs.getString(4));
//                temp.add(rs.getString(5)); // Product Qty
                productQtyArr.add(rs.getString(5) == null || rs.getString(5).equals("") ? "0" : rs.getString(5));
//                temp.add(rs.getString(6)); // Bonus Qty
                bonusQtyArr.add(rs.getString(6) == null || rs.getString(6).equals("") ? "0" : rs.getString(6));
//                unitArr.add("Packet");
//                temp.add(rs.getString(7)); // Gross Amount
                productPriceArr.add(rs.getString(7) == null || rs.getString(7).equals("") ? "0" : rs.getString(7));
//                temp.add(rs.getString(8)); // Discount Amount
                productDiscountArr.add(rs.getString(8) == null || rs.getString(8).equals("") ? "0" : rs.getString(8));
//                temp.add(rs.getString(9)); // Net Amount
                productNetPriceArr.add(rs.getString(9) == null || rs.getString(9).equals("") ? "0" : rs.getString(9));
//                purchaseData.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<ArrayList<String>> loadPurchaseTable()
    {
        ArrayList<ArrayList<String>> purchaseData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        for(int i=0; i<productIdArr.size(); i++)
        {
            temp = new ArrayList<String>();
            temp.add(productIdArr.get(i));
            temp.add(productNameArr.get(i));
            temp.add(batchNoArr.get(i));
            temp.add(batchExpiryArr.get(i));
            temp.add(productQtyArr.get(i));
            temp.add(bonusQtyArr.get(i));
            temp.add(productPriceArr.get(i));
            temp.add(productDiscountArr.get(i));
            temp.add(productNetPriceArr.get(i));
            purchaseData.add(temp);
        }
        return purchaseData;
    }

    public void updateInvoice(Statement stmt, Connection con)
    {
        String updateQuery = "INSERT INTO `purchase_info_detail`(`invoice_num`, `purchase_id`, `prod_id`, `bonus_quant`, `recieve_quant`, `batch_no`, `expiry_date`, `gross_amount`, `disc_amount`, `net_amount`, `returnBit`, `returned_quant`, `returned_bonus_quant`) VALUES ";
        String deleteQuery = "DELETE FROM `purchase_info_detail` WHERE `purchase_id` = '"+purchaseId+"'";
        String unit = "Packets";
        try {
            stmt.executeUpdate(deleteQuery);
            for(int i=0; i<productIdArr.size(); i++)
            {
                if(i == productIdArr.size()-1)
                    updateQuery += "('"+invoiceNum+"','"+purchaseId+"','"+productIdArr.get(i)+"','"+bonusQtyArr.get(i)+"','"+productQtyArr.get(i)+"','"+batchNoArr.get(i)+"','"+batchExpiryArr.get(i)+"','"+productPriceArr.get(i)+"','"+productDiscountArr.get(i)+"','"+productNetPriceArr.get(i)+"','0','0','0') ";
                else
                    updateQuery += "('"+invoiceNum+"','"+purchaseId+"','"+productIdArr.get(i)+"','"+bonusQtyArr.get(i)+"','"+productQtyArr.get(i)+"','"+batchNoArr.get(i)+"','"+batchExpiryArr.get(i)+"','"+productPriceArr.get(i)+"','"+productDiscountArr.get(i)+"','"+productNetPriceArr.get(i)+"','0','0','0') , ";
            }
//            updateQuery = "UPDATE `dealer_info` SET `dealer_id`='"+dealerId+"',`dealer_area_id`='"+areaId+"',`dealer_name`='"+dealerName+"',`dealer_phone`='"+dealerContact+"',`dealer_address`='"+dealerAddress+"',`dealer_type`='"+dealerType+"',`dealer_cnic`='"+dealerCnic+"',`dealer_lic9_num`='"+dealerLicNum+"',`dealer_lic9_exp`='"+dealerLicExp+"',`dealer_status`='"+dealerStatus+"', `update_user_id`='"+currentUser+"',`update_date`='"+currentDate+"',`update_time`='"+currentTime+"' WHERE `dealer_table_id` = '"+dealerTableId+"'";

//            System.out.println(updateQuery);
            stmt.executeUpdate(updateQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateBasic(Statement stmt, Connection con, String invoiceNum, String supplierId, String purchaseDate, String grossAmount, String discountAmount, String netAmount, String enterUserId, String enterDate, String enterTime, String status)
    {
        String currentDate = GlobalVariables.getStDate();
        String currentTime = GlobalVariables.getStTime();
        String currentUser = GlobalVariables.getUserId();
        String updateQuery = "UPDATE `purchase_info` SET `invoice_num`= '"+invoiceNum+"', `supplier_id`= '"+supplierId+"', `purchase_date`= '"+purchaseDate+"', `gross_amount`= '"+grossAmount+"', `disc_amount`= '"+discountAmount+"', `net_amount`= '"+netAmount+"', `enter_user_id`= '"+(enterUserId.equals(null) ? "" : enterUserId)+"', `entry_date`= '"+(enterDate.equals(null) ? "" : enterDate)+"', `entry_time`= '"+(enterTime.equals(null) ? "" : enterTime)+"', `update_user_id`= '"+currentUser+"',`update_date`= '"+currentDate+"',`update_time`= '"+currentTime+"', `status`= '"+status+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `purchase_id` = '"+purchaseId+"'";
        try {
            stmt.executeUpdate(updateQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
