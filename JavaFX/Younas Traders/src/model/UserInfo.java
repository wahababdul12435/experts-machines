package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import controller.EnterUser;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class UserInfo {
    private String srNo;
    private String userId;
    private String userName;
    private String userType;
    private String userContact;
    private String userAddress;
    private String userCnic;
    private String userDesignatedCity;

    private HBox operationsPane;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    public static ArrayList<String> userIdArr = new ArrayList<>();
    public static ArrayList<String> userNameArr = new ArrayList<>();
    public static ArrayList<String> userTypeArr = new ArrayList<>();
    public static ArrayList<String> userContactArr = new ArrayList<>();
    public static ArrayList<String> userAddressArr = new ArrayList<>();
    public static ArrayList<String> userCnicArr = new ArrayList<>();
    public static ArrayList<String> userCityIdcArr = new ArrayList<>();
    public static ArrayList<String> userCityNameArr = new ArrayList<>();

    public static TableView<UserInfo> table_add_user;

    public static JFXTextField txtUserId;
    public static JFXTextField txtUserName;
    public static JFXComboBox<String> txtUserType;
    public static JFXTextField txtUserContact;
    public static JFXTextField txtUserAddress;
    public static JFXTextField txtUserCnic;
    public static JFXComboBox<String> txtUserCity;
    public static Button btnAdd;
    public static Button btnCancel;
    public static Button btnUpdate;


    private ArrayList<String> savedCityIds;
    private ArrayList<String> savedCityNames;

    MysqlCon objMysqlCon = new MysqlCon();
    Statement objStmt = objMysqlCon.stmt;
    Connection objCon = objMysqlCon.con;

    public UserInfo() {
        this.srNo = "";
        this.userId = "";
        this.userName = "";
        this.userType = "";
        this.userContact = "";
        this.userAddress = "";
        this.userCnic = "";
        this.userDesignatedCity = "";

        getCitiesInfo(objStmt, objCon);
    }

    public UserInfo(String srNo, String userId, String userName, String userType, String userContact, String userAddress, String userCnic, String userDesignatedCity) {
        this.srNo = srNo;
        this.userId = userId;
        this.userName = userName;
        this.userType = userType;
        this.userContact = userContact;
        this.userAddress = userAddress;
        this.userCnic = userCnic;
        this.userDesignatedCity = userDesignatedCity;

        this.btnEdit = new JFXButton("Edit");
        this.btnDelete = new JFXButton("Delete");
        this.btnEdit.setOnAction((action)->editAddedRecord());
        this.btnDelete.setOnAction((action)->deleteAddedRecord());
        this.operationsPane = new HBox(btnEdit, btnDelete);
    }

    private void deleteAddedRecord()
    {
        userIdArr.remove(Integer.parseInt(srNo)-1);
        userNameArr.remove(Integer.parseInt(srNo)-1);
        userTypeArr.remove(Integer.parseInt(srNo)-1);
        userContactArr.remove(Integer.parseInt(srNo)-1);
        userAddressArr.remove(Integer.parseInt(srNo)-1);
        userCnicArr.remove(Integer.parseInt(srNo)-1);
        userCityIdcArr.remove(Integer.parseInt(srNo)-1);
        userCityNameArr.remove(Integer.parseInt(srNo)-1);
        table_add_user.setItems(EnterUser.parseUserList());
    }

    public void editAddedRecord()
    {
        EnterUser.srNo = Integer.parseInt(srNo)-1;
        txtUserId.setText(userIdArr.get(Integer.parseInt(srNo)-1));
        txtUserName.setText(userNameArr.get(Integer.parseInt(srNo)-1));
        txtUserType.setValue(userTypeArr.get(Integer.parseInt(srNo)-1));
        txtUserContact.setText(userContactArr.get(Integer.parseInt(srNo)-1));
        txtUserAddress.setText(userAddressArr.get(Integer.parseInt(srNo)-1));
        txtUserCnic.setText(userCnicArr.get(Integer.parseInt(srNo)-1));
        txtUserCity.setValue(userCityNameArr.get(Integer.parseInt(srNo)-1));
        btnAdd.setVisible(false);
        btnCancel.setVisible(true);
        btnUpdate.setVisible(true);
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getUserContact() {
        return userContact;
    }

    public void setUserContact(String userContact) {
        this.userContact = userContact;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public String getUserCnic() {
        return userCnic;
    }

    public void setUserCnic(String userCnic) {
        this.userCnic = userCnic;
    }

    public String getUserDesignatedCity() {
        return userDesignatedCity;
    }

    public void setUserDesignatedCity(String userDesignatedCity) {
        this.userDesignatedCity = userDesignatedCity;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public ArrayList<String> getSavedCityIds() {
        return savedCityIds;
    }

    public void setSavedCityIds(ArrayList<String> savedCityIds) {
        this.savedCityIds = savedCityIds;
    }

    public ArrayList<String> getSavedCityNames() {
        return savedCityNames;
    }

    public void setSavedCityNames(ArrayList<String> savedCityNames) {
        this.savedCityNames = savedCityNames;
    }

    public void insertUser(Statement stmt, Connection con, String userName, String userContact, String userAddress, String userCNIC, String userStatus, String designatedAreas[], String designatedStatus)
    {
        try {
            String getUserID = "SELECT `AUTO_INCREMENT` FROM  INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'younas_traders' AND TABLE_NAME = 'salesman_info'";
            ResultSet rs =stmt.executeQuery(getUserID);
            rs.next();
            int userID = rs.getInt(1);

            String designatedAreasID[] = new String[4];
            for(int x=0; x<designatedAreas.length; x++)
            {
                String getSubAreaID = "SELECT `subarea_id` FROM `subarea_info` WHERE `sub_area_name` = '"+designatedAreas[x]+"'";
                rs =stmt.executeQuery(getSubAreaID);
                rs.next();
                designatedAreasID[x] = rs.getString(1);
            }

            String insertQuery = "INSERT INTO `salesman_info`(`salesman_name`, `salesman_contact`, `salesman_address`, `salesman_cnic`, `salesman_status`) VALUES ('"+userName+"','"+userContact+"','"+userAddress+"','"+userCNIC+"','"+userStatus+"')";
            stmt.executeUpdate(insertQuery);

            for(int x=0; x<designatedAreasID.length; x++)
            {
                String insertAreas = "INSERT INTO `salesman_designated_cities`(`salesman_id`, `designated_city_id`, `designated_status`) VALUES ('"+userID+"','"+designatedAreasID[x]+"','"+designatedStatus+"')";
                stmt.executeUpdate(insertAreas);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertUser(Statement stmt, Connection con) throws SQLException {
        String currentDate = GlobalVariables.getStDate();
        String currentTime = GlobalVariables.getStTime();
        String currentUser = GlobalVariables.getUserId();
        boolean withId = false;
        boolean withoutId = false;
        String insertQuery;
        String insertUserCity;
        String userTableId;
        int insertInfo = 1;

        for(int i=0; i<userIdArr.size(); i++)
        {
            if(userIdArr.get(i).equals("") || userIdArr.get(i) == null)
            {
                insertQuery = "INSERT INTO `user_info`(`user_name`, `user_contact`, `user_address`, `user_cnic`, `user_type`, `user_image`, `user_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`) VALUES ('"+userNameArr.get(i)+"','"+userContactArr.get(i)+"','"+userAddressArr.get(i)+"','"+userCnicArr.get(i)+"','"+userTypeArr.get(i)+"','','Active','"+currentUser+"','"+currentDate+"','"+currentTime+"','"+currentUser+"','"+currentDate+"','"+currentTime+"')";
            }
            else
            {
                insertQuery = "INSERT INTO `user_info`(`user_id`, `user_name`, `user_contact`, `user_address`, `user_cnic`, `user_type`, `user_image`, `user_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`) VALUES ('"+userIdArr.get(i)+"','"+userNameArr.get(i)+"','"+userContactArr.get(i)+"','"+userAddressArr.get(i)+"','"+userCnicArr.get(i)+"','"+userTypeArr.get(i)+"','','Active','"+currentUser+"','"+currentDate+"','"+currentTime+"','"+currentUser+"','"+currentDate+"','"+currentTime+"')";
            }
            if(stmt.executeUpdate(insertQuery) == -1)
            {
                insertInfo = -1;
            }
            userTableId = getUserTableId(stmt, con, userNameArr.get(i), userContactArr.get(i), userCnicArr.get(i));
            insertUserCity = "INSERT INTO `salesman_designated_cities`(`salesman_table_id`, `designated_city_id`, `start_date`, `end_date`, `designated_status`) VALUES ('"+userTableId+"','"+userCityIdcArr.get(i)+"','"+currentDate+"','','Active')";
            if(stmt.executeUpdate(insertUserCity) == -1)
            {
                insertInfo = -1;
            }
        }
        if(insertInfo == -1)
        {
            String title = "Error";
            String message = "Unable to insert";
            GlobalVariables.showNotification(-1, title, message);
        }
        else
        {
            String title = "Successfull";
            String message = "Record Inserted";
            GlobalVariables.showNotification(1, title, message);
        }
        refreshAllArrays();
    }

    private void refreshAllArrays()
    {
        userIdArr = new ArrayList<>();
        userNameArr = new ArrayList<>();
        userTypeArr = new ArrayList<>();
        userContactArr = new ArrayList<>();
        userAddressArr = new ArrayList<>();
        userCnicArr = new ArrayList<>();
        userCityIdcArr = new ArrayList<>();
        userCityNameArr = new ArrayList<>();
    }

    public void updateUser(Statement stmt, Connection con, String userTableId, String userId, String userName, String userContact, String userAddress, String userCnic, String userType, String userStatus, ArrayList<String> designatedCityIds)
    {
        String currentDate = GlobalVariables.getStDate();
        String currentTime = GlobalVariables.getStTime();
        String currentUser = GlobalVariables.getUserId();
        String updateQuery;
        try {
            if(userId.equals("") || userId.equals("N/A"))
            {
                updateQuery = "UPDATE `user_info` SET `user_id` = NULL, `user_name`='"+userName+"',`user_contact`='"+userContact+"',`user_address`='"+userAddress+"',`user_cnic`='"+userCnic+"',`user_type`='"+userType+"',`user_status`='"+userStatus+"',`update_user_id`='"+currentUser+"',`update_date`='"+currentDate+"',`update_time`='"+currentTime+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `user_table_id` = '"+userTableId+"'";
            }
            else
            {
                updateQuery = "UPDATE `user_info` SET `user_id` = '"+userId+"', `user_name`='"+userName+"',`user_contact`='"+userContact+"',`user_address`='"+userAddress+"',`user_cnic`='"+userCnic+"',`user_type`='"+userType+"',`user_status`='"+userStatus+"',`update_user_id`='"+currentUser+"',`update_date`='"+currentDate+"',`update_time`='"+currentTime+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `user_table_id` = '"+userTableId+"'";
            }

            stmt.executeUpdate(updateQuery);

            ArrayList<String> previousDesignatedId = new ArrayList<>();
            ResultSet rs = null;
            try {
                rs = stmt.executeQuery("SELECT `designated_city_id` FROM `salesman_designated_cities` WHERE `salesman_table_id` = '"+userTableId+"' AND `designated_status` = 'Active'");
                while (rs.next())
                {
                    previousDesignatedId.add(rs.getString(1));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

            for (String designatedIds: designatedCityIds) {
                if(previousDesignatedId.contains(designatedIds))
                {
                    previousDesignatedId.remove(designatedIds);
                }
                else
                {
                    String insertAreas = "INSERT INTO `salesman_designated_cities`(`salesman_table_id`, `designated_city_id`, `start_date`, `end_date`, `designated_status`) VALUES ('"+userTableId+"','"+designatedIds+"','"+GlobalVariables.getStDate()+"', 'null', 'Active')";
                    stmt.executeUpdate(insertAreas);
                }
            }
            for(String removedIds: previousDesignatedId)
            {
                String updateDesignated = "UPDATE `salesman_designated_cities` SET `designated_status`= 'Removed', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `salesman_table_id` = '"+userTableId+"' AND `designated_city_id` = '"+removedIds+"' AND `designated_status` = 'Active'";
                stmt.executeUpdate(updateDesignated);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<ArrayList<String>> getAddedUser()
    {
        ArrayList<ArrayList<String>> userData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        for(int i=0; i<userIdArr.size(); i++)
        {
            temp = new ArrayList<String>();
            temp.add(userIdArr.get(i));
            temp.add(userNameArr.get(i));
            temp.add(userTypeArr.get(i));
            temp.add(userContactArr.get(i));
            temp.add(userAddressArr.get(i));
            temp.add(userCnicArr.get(i));
            temp.add(userCityNameArr.get(i));
            userData.add(temp);
        }
        return userData;
    }

    public ArrayList<String> getUserDetail(String userId)
    {
        ArrayList<String> userInfo = new ArrayList<>();
        ResultSet rs = null;
        String query = "SELECT `user_id`, `user_name`, `user_contact`, `user_address`, `user_cnic`, `user_type`, `creating_date`, `update_date`, `user_status`, `user_image` FROM `user_info` WHERE `user_table_id` = '"+userId+"'";
        try {
            rs = objStmt.executeQuery(query);
            while (rs.next())
            {
                userInfo.add(rs.getString(1)); // User Id
                userInfo.add(rs.getString(2)); // Name
                userInfo.add(rs.getString(3)); // Contact
                userInfo.add(rs.getString(4)); // Address
                userInfo.add(rs.getString(5)); // Cnic
                userInfo.add(rs.getString(6)); // Type
                userInfo.add(rs.getString(7)); // Created
                userInfo.add(rs.getString(8)); // Updated
                userInfo.add(rs.getString(9)); // Status
                userInfo.add(rs.getString(10)); // Image
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userInfo;
    }

    String checkUserId = new String();
    public String confirmNewId(String userId)
    {
        ResultSet rs3 = null;
        try {
            rs3 = objStmt.executeQuery("SELECT `user_id` FROM `user_info` WHERE `user_id` = '"+userId+"' AND `user_status` != 'Deleted'");
            while (rs3.next())
            {
                checkUserId = rs3.getString(1) ;
            }
//            objCon.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return checkUserId;
    }

    public ArrayList<ArrayList<String>> getUserWithIds(Statement stmt, Connection con)
    {
        ArrayList<ArrayList<String>> userData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT `user_table_id`, `user_id`, `user_name` FROM `user_info` WHERE `user_status` != 'Deleted'");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Table id
                temp.add(rs.getString(2)); // id
                temp.add(rs.getString(3)+" ("+rs.getString(1)+")"); // name
//                if(rs.getString(2) != null && !rs.getString(2).equals(""))
//                {
//                    temp.add(rs.getString(3)+" ("+rs.getString(2)+")"); // name
//                }
//                else
//                {
//                    temp.add(rs.getString(3)); // name
//                }
                userData.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userData;
    }

    public void getCitiesInfo(Statement stmt, Connection con)
    {
        ArrayList<String> tempIds;
        ArrayList<String> tempNames;
        tempIds = new ArrayList<String>();
        tempNames = new ArrayList<String>();
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT `city_table_id`, `city_name` FROM `city_info` WHERE `city_status` != 'Deleted' ORDER BY `city_table_id`");
            while (rs.next())
            {
                tempIds.add(rs.getString(1));
                tempNames.add(rs.getString(2)+" ("+rs.getString(1)+")");
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        setSavedCityIds(tempIds);
        setSavedCityNames(tempNames);
    }

    public String getUserTableId(Statement stmt3, Connection con3 , String userName, String userContact, String userCnic)
    {
        ResultSet rs3 = null;
        try {
            String getIdStatment = "SELECT `user_table_id` FROM `user_info` WHERE `user_name` = '"+userName+"' AND `user_contact` = '"+userContact+"' AND `user_cnic` = '"+userCnic+"' AND `user_status` != 'Deleted'";
            rs3 = stmt3.executeQuery(getIdStatment);
            while (rs3.next())
            {
                checkUserId = rs3.getString(1) ;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return checkUserId;
    }
}
