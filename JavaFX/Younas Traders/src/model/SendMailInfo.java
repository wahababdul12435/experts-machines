package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import controller.EnterCompany;
import controller.SendMail;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

public class SendMailInfo {
    private String srNo;
    private String to;
    private String title;
    private String message;
    private String attachment;

    public static ArrayList<String> toArr = new ArrayList<>();
    public static ArrayList<String> titleArr = new ArrayList<>();
    public static ArrayList<String> messageArr = new ArrayList<>();
    public static ArrayList<String> filePathArr = new ArrayList<>();
    public static ArrayList<String> attachmentArr = new ArrayList<>();

    public static TableView<SendMailInfo> table_new_mails;

    public static JFXTextField txtTo;
    public static JFXTextField txtTitle;
    public static JFXTextArea txtMessage;
    public static Label txtAttachment;

    public static JFXButton btnAdd;
    public static JFXButton btnCancel;
    public static JFXButton btnUpdate;

    private HBox operationsPane;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    public SendMailInfo() {
    }

    public SendMailInfo(String srNo, String to, String title, String message, String attachment) {
        this.srNo = srNo;
        this.to = to;
        this.title = title;
        this.message = message;
        this.attachment = attachment;

        this.btnEdit = new JFXButton();
        this.btnDelete = new JFXButton();
        this.btnEdit.setGraphic(GlobalVariables.createEditIcon());
        this.btnDelete.setGraphic(GlobalVariables.createDeleteIcon());
        this.btnEdit.getStyleClass().add("btn_icon");
        this.btnDelete.getStyleClass().add("btn_icon");
        this.operationsPane = new HBox(btnEdit, btnDelete);

        this.btnEdit.setOnAction((action)->editAddedRecord());
        this.btnDelete.setOnAction((action)->deleteAddedRecord());
    }

    public void editAddedRecord()
    {
        SendMail.srNo = Integer.parseInt(srNo)-1;
        txtTo.setText(toArr.get(Integer.parseInt(srNo)-1));
        txtTitle.setText(titleArr.get(Integer.parseInt(srNo)-1));
        txtMessage.setText(messageArr.get(Integer.parseInt(srNo)-1));
        txtAttachment.setText(filePathArr.get(Integer.parseInt(srNo)-1));

        btnAdd.setVisible(false);
        btnCancel.setVisible(true);
        btnUpdate.setVisible(true);
    }

    private void deleteAddedRecord()
    {
        toArr.remove(Integer.parseInt(srNo)-1);
        titleArr.remove(Integer.parseInt(srNo)-1);
        messageArr.remove(Integer.parseInt(srNo)-1);
        filePathArr.remove(Integer.parseInt(srNo)-1);
        attachmentArr.remove(Integer.parseInt(srNo)-1);

        table_new_mails.setItems(SendMail.parseUserList());
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public ArrayList<ArrayList<String>> getAddedMails()
    {
        ArrayList<ArrayList<String>> mailsData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        for(int i=0; i<toArr.size(); i++)
        {
            temp = new ArrayList<String>();
            temp.add(toArr.get(i));
            temp.add(titleArr.get(i));
            temp.add(messageArr.get(i));
            temp.add(attachmentArr.get(i));
            mailsData.add(temp);
        }
        return mailsData;
    }

    public static boolean sendMail(String to, String title, String message, String attachment, String fileName)
    {
        //setup mail server
        Properties props = System.getProperties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", GlobalVariables.host);
        props.put("mail.smtp.port", "587");

        Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator(){
            @Override
            protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(GlobalVariables.email, GlobalVariables.password);
            }
        });

        try {
            //create mail
            MimeMessage m = new MimeMessage(session);
            m.setFrom(new InternetAddress(GlobalVariables.email));
            m.addRecipient(MimeMessage.RecipientType.TO, new InternetAddress(to));
            m.setSubject(title); // (emailSubjectField.getText());
            m.setText(message); // (emailMessageField.getText());

            MimeBodyPart messageBodyPart = new MimeBodyPart();

            Multipart multipart = new MimeMultipart();

//            String file = "Data/View_Company.xls";
//            String fileName = "View_Company.xls";
            DataSource source = new FileDataSource(attachment);
            messageBodyPart.setDataHandler(new DataHandler(source));
            messageBodyPart.setFileName(fileName);
            multipart.addBodyPart(messageBodyPart);

            m.setContent(multipart);

//            System.out.println("Sending");
            //send mail
            Transport.send(m);
//            System.out.println("Message sent!");

        }   catch (MessagingException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static void insertMail(Statement stmt, Connection con, String from, String to, String title, String message, String attachment, String status)
    {
        String serverSync = "Pending";
        String insertQuery = "INSERT INTO `mails_record`(`from_email`, `to_email`, `title`, `message`, `attachment`, `user_id`, `date`, `time`, `status`, `server_sync`) VALUES ('"+from+"', '"+to+"', '"+title+"', '"+message+"', '"+attachment+"', '"+GlobalVariables.userId+"', '"+GlobalVariables.getStDate()+"', '"+GlobalVariables.getStTime()+"', '"+status+"', '"+serverSync+"')";
        try {
            GlobalVariables.objStmt.executeUpdate(insertQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
