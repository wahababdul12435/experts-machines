package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class LoginInfo {

    MysqlCon objMysqlCon;
    Statement objStmt;
    Connection objCon;

    public LoginInfo()
    {
        objMysqlCon = new MysqlCon();
        objStmt = objMysqlCon.stmt;
        objCon = objMysqlCon.con;
    }

    public ArrayList<String> checkLogin(String userName, String password)
    {
        ArrayList<String> userDetail = new ArrayList<>();
        ResultSet rs = null;
        String query = "SELECT `user_accounts`.`user_id`, `user_info`.`user_image`, `user_info`.`user_type`, `user_rights`.`create_record`, `user_rights`.`read_record`, `user_rights`.`edit_record`, `user_rights`.`delete_record`, `user_rights`.`manage_stock`, `user_rights`.`manage_cash`, `user_rights`.`manage_settings`, `user_rights`.`mobile_app` FROM `user_accounts` INNER JOIN `user_info` ON `user_info`.`user_table_id` = `user_accounts`.`user_id` INNER JOIN `user_rights` ON `user_rights`.`user_id` = `user_accounts`.`user_id` WHERE `user_accounts`.`user_name` = '"+userName +"' AND `user_accounts`.`user_password` = '"+password+"' AND `user_accounts`.`user_status` = 'Approved'";
        try {
            rs = objStmt.executeQuery(query);
            if(rs.next())
            {
                userDetail.add(rs.getString(1)); // User Id
                userDetail.add(rs.getString(2)); // User Image
                userDetail.add(password); // User Password
                GlobalVariables.userType = rs.getString(3); // User Type
                GlobalVariables.rightCreate = rs.getBoolean(4); // Create
                GlobalVariables.rightRead = rs.getBoolean(5); // Read
                GlobalVariables.rightEdit = rs.getBoolean(6); // Edit
                GlobalVariables.rightDelete = rs.getBoolean(7); // Delete
                GlobalVariables.rightManageStock = rs.getBoolean(8); // Stock
                GlobalVariables.rightManageCash = rs.getBoolean(9); // Cash
                GlobalVariables.rightManageSettings = rs.getBoolean(10); // Settings
                GlobalVariables.rightMobileApp = rs.getBoolean(11); // Mobile
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userDetail;
    }
}
