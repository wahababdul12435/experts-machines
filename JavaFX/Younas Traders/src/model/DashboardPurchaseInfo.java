package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DashboardPurchaseInfo {

    public ArrayList<ArrayList<String>> getCompanyPurchaseDetail(Statement stmt, Connection con, String fromDate, String toDate)
    {
        ArrayList<ArrayList<String>> dealersFinanceData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            String query = "SELECT `purchase_info`.`purchase_id`, `purchase_info`.`net_amount`, `purchase_info`.`entry_date`, `company_info`.`company_name` FROM `purchase_info` LEFT OUTER JOIN `company_info` ON `company_info`.`company_table_id` = `purchase_info`.`comp_id` WHERE str_to_date(`purchase_info`.`entry_date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') AND str_to_date(`purchase_info`.`entry_date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y') AND `purchase_info`.`status` != 'Deleted' ORDER BY str_to_date(`purchase_info`.`entry_date`, '%d/%b/%Y') DESC";
            rs = stmt.executeQuery(query);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Purchase Id
                temp.add(rs.getString(2) == null?"N/A":rs.getString(2)); // Net Amount
                temp.add(rs.getString(3) == null?"N/A":rs.getString(3)); // Date
                temp.add(rs.getString(4) == null?"N/A":rs.getString(4)); // Company Name
                dealersFinanceData.add(temp);
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dealersFinanceData;
    }

    public ArrayList<ArrayList<String>> getCompanyReturnDetail(Statement stmt, Connection con, String fromDate, String toDate)
    {
        ArrayList<ArrayList<String>> dealersFinanceData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            String query = "SELECT `purchase_return`.`returnTable_id`, `purchase_return`.`return_total_price`, `purchase_return`.`entry_date`, `company_info`.`company_name` FROM `purchase_return` LEFT OUTER JOIN `company_info` ON `company_info`.`company_table_id` = `purchase_return`.`company_table_id` WHERE str_to_date(`purchase_return`.`entry_date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') AND str_to_date(`purchase_return`.`entry_date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y') AND `purchase_return`.`status` != 'Deleted' ORDER BY str_to_date(`purchase_return`.`entry_date`, '%d/%b/%Y') DESC";
            rs = stmt.executeQuery(query);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Return Id
                temp.add(rs.getString(2) == null?"N/A":rs.getString(2)); // Amount
                temp.add(rs.getString(3) == null?"N/A":rs.getString(3)); // Date
                temp.add(rs.getString(4) == null?"N/A":rs.getString(4)); // Company Name
                dealersFinanceData.add(temp);
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dealersFinanceData;
    }
}
