package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import controller.UpdateDealerFinance;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

public class CompanyReportDetailInfo {
    private String srNo;
    private String date;
    private String day;
    private String purchaseInvoice;
    private String receivedItems;
    private String purchasePrice;
    private String discountGiven;
    private String quantityReturned;
    private String returnPrice;
    private String cashCollection;
    private String cashReturn;
    private String cashWaiveOff;
    private String cashPending;

    private HBox operationsPane;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    SimpleDateFormat fmt = new SimpleDateFormat("dd/MMM/yyyy");

    public static StackPane stackPane;
    public static JFXDialog dialog;

    public CompanyReportDetailInfo() {
        this.srNo = "";
//        this.paymentId = "";
//        this.dealerId = "";
//        this.dealerName = "";
        this.date = "";
        this.day = "";
        this.purchaseInvoice = "";
        this.quantityReturned = "";
        this.returnPrice = "";
        this.cashCollection = "";
        this.cashReturn = "";
        this.cashWaiveOff = "";
    }

    public CompanyReportDetailInfo(String srNo, String date, String day, String purchaseInvoice, String receivedItems, String purchasePrice, String discountGiven, String quantityReturned, String returnPrice, String cashCollection, String cashReturn, String cashWaiveOff, String cashPending) {
        this.srNo = srNo;
        this.date = date;
        this.day = day;
        this.purchaseInvoice = purchaseInvoice;
        this.receivedItems = (receivedItems != null && !receivedItems.equals("-")) ? String.format("%,.0f", Float.parseFloat(receivedItems)) : "-";
        this.purchasePrice = (purchasePrice != null && !purchasePrice.equals("-")) ? String.format("%,.0f", Float.parseFloat(purchasePrice)) : "-";
        this.discountGiven = (discountGiven != null && !discountGiven.equals("-")) ? String.format("%,.0f", Float.parseFloat(discountGiven)) : "-";
        this.quantityReturned = (quantityReturned != null && !quantityReturned.equals("-")) ? String.format("%,.0f", Float.parseFloat(quantityReturned)) : "-";
        this.returnPrice = (returnPrice != null && !returnPrice.equals("-")) ? String.format("%,.0f", Float.parseFloat(returnPrice)) : "-";
        this.cashCollection = (cashCollection != null && !cashCollection.equals("-")) ? String.format("%,.0f", Float.parseFloat(cashCollection)) : "-";
        this.cashReturn = (cashReturn != null && !cashReturn.equals("-")) ? String.format("%,.0f", Float.parseFloat(cashReturn)) : "-";
        this.cashWaiveOff = (cashWaiveOff != null && !cashWaiveOff.equals("-")) ? String.format("%,.0f", Float.parseFloat(cashWaiveOff)) : "-";
        this.cashPending = (cashPending != null && !cashPending.equals("-")) ? String.format("%,.0f", Float.parseFloat(cashPending)) : "-";

//        this.btnEdit = new JFXButton();
//        this.btnDelete = new JFXButton();
//        this.btnEdit.setGraphic(GlobalVariables.createEditIcon());
//        this.btnDelete.setGraphic(GlobalVariables.createDeleteIcon());
//        this.btnEdit.getStyleClass().add("btn_icon");
//        this.btnDelete.getStyleClass().add("btn_icon");
//        this.operationsPane = new HBox(btnEdit, btnDelete);
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getPurchaseInvoice() {
        return purchaseInvoice;
    }

    public void setPurchaseInvoice(String purchaseInvoice) {
        this.purchaseInvoice = purchaseInvoice;
    }

    public String getReceivedItems() {
        return receivedItems;
    }

    public void setReceivedItems(String receivedItems) {
        this.receivedItems = receivedItems;
    }

    public String getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(String purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public String getDiscountGiven() {
        return discountGiven;
    }

    public void setDiscountGiven(String discountGiven) {
        this.discountGiven = discountGiven;
    }

    public String getCashPending() {
        return cashPending;
    }

    public void setCashPending(String cashPending) {
        this.cashPending = cashPending;
    }

    public String getQuantityReturned() {
        return quantityReturned;
    }

    public void setQuantityReturned(String quantityReturned) {
        this.quantityReturned = quantityReturned;
    }

    public String getReturnPrice() {
        return returnPrice;
    }

    public void setReturnPrice(String returnPrice) {
        this.returnPrice = returnPrice;
    }

    public String getCashCollection() {
        return cashCollection;
    }

    public void setCashCollection(String cashCollection) {
        this.cashCollection = cashCollection;
    }

    public String getCashReturn() {
        return cashReturn;
    }

    public void setCashReturn(String cashReturn) {
        this.cashReturn = cashReturn;
    }

    public String getCashWaiveOff() {
        return cashWaiveOff;
    }

    public void setCashWaiveOff(String cashWaiveOff) {
        this.cashWaiveOff = cashWaiveOff;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public static StackPane getStackPane() {
        return stackPane;
    }

    public static void setStackPane(StackPane stackPane) {
        CompanyReportDetailInfo.stackPane = stackPane;
    }

    public static JFXDialog getDialog() {
        return dialog;
    }

    public static void setDialog(JFXDialog dialog) {
        CompanyReportDetailInfo.dialog = dialog;
    }

    public ArrayList<ArrayList<String>> getCompanyReportDetail(Statement stmt1, Statement stmt2, Statement stmt3, Connection con, String companyId) throws ParseException {
        ArrayList<ArrayList<String>> companyReportData1 = new ArrayList<ArrayList<String>>();
        ArrayList<ArrayList<String>> companyReportData2 = new ArrayList<ArrayList<String>>();
        ArrayList<ArrayList<String>> companyReportData3 = new ArrayList<ArrayList<String>>();
        ArrayList<ArrayList<String>> companyReportData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs1 = null;
        ResultSet rs2 = null;
        ResultSet rs3 = null;
        try {
            rs1 = stmt1.executeQuery("SELECT `purchase_info`.`purchase_date`, `purchase_info`.`invoice_num`, SUM(`purchase_info_detail`.`recieve_quant`), `purchase_info`.`net_amount`, `purchase_info`.`disc_amount` FROM `purchase_info` LEFT OUTER JOIN `purchase_info_detail` ON `purchase_info_detail`.`purchase_id` = `purchase_info`.`purchase_id` WHERE `purchase_info`.`status` != 'Deleted' AND `purchase_info`.`comp_id` = '"+companyId+"' GROUP BY `purchase_info`.`purchase_date` ORDER BY str_to_date(`purchase_info`.`purchase_date`, '%d/%b/%Y') DESC");
            while (rs1.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs1.getString(1)); // Purchase Date
                temp.add(rs1.getString(2)); // Invoice Num
                temp.add(rs1.getString(3)); // Receive Qty
                temp.add(rs1.getString(4)); // Purchase Price
                temp.add(rs1.getString(5)); // Discount Given
                companyReportData1.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            rs2 = stmt2.executeQuery("SELECT `purchase_return`.`entry_date`, SUM(`purchase_return_detail`.`prod_quant`) as 'return_quantity', SUM(`purchase_return_detail`.`total_amount`) as 'return_amount' FROM `purchase_return` INNER JOIN `purchase_return_detail` ON `purchase_return_detail`.`return_id` = `purchase_return`.`return_id` WHERE `purchase_return`.`status` != 'Deleted' AND `purchase_return`.`company_table_id` = '"+companyId+"' GROUP BY `purchase_return`.`entry_date` ORDER BY str_to_date(`purchase_return`.`entry_date`, '%d/%b/%Y') DESC");
            while (rs2.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs2.getString(1)); // Return Date
                temp.add(rs2.getString(2)); // Return Quantity
                temp.add(rs2.getString(3)); // Return Price
                companyReportData2.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            rs3 = stmt3.executeQuery("SELECT `company_payments`.`payment_id`, `company_payments`.`date`, `company_payments`.`amount`, `company_payments`.`cash_type`, `company_payments`.`pending_payments` FROM `company_payments` WHERE `company_payments`.`company_id` = '"+companyId+"' AND `company_payments`.`status` != 'Deleted' ORDER BY str_to_date(`date`, '%d/%b/%Y') DESC");
            while (rs3.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs3.getString(1)); // Payment Id
                temp.add(rs3.getString(2)); // Payment Date
                temp.add(rs3.getString(3)); // Amount
                temp.add(rs3.getString(4)); // Payment Type
                temp.add(rs3.getString(5)); // Pending Payments
                companyReportData3.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        String stDatePurchase = "";
        String stDateReturn = "";
        String stDatePayment = "";
        Date datePurchase = null;
        Date dateReturn = null;
        Date datePayment = null;
        ArrayList<Date> objDatesArray;
        int day;
        String stDay = "";
        boolean newDataAdd = true;

        while (companyReportData1.size() > 0 || companyReportData2.size() > 0 || companyReportData3.size() > 0)
        {
            temp = new ArrayList<>();
            objDatesArray = new ArrayList<>();
            if(companyReportData1.size() > 0)
            {
                stDatePurchase = companyReportData1.get(0).get(0);
                datePurchase = fmt.parse(stDatePurchase);
                objDatesArray.add(datePurchase);
            }
            if(companyReportData2.size() > 0)
            {
                stDateReturn = companyReportData2.get(0).get(0);
                dateReturn = fmt.parse(stDateReturn);
                objDatesArray.add(dateReturn);
            }
            if(companyReportData3.size() > 0)
            {
                stDatePayment = companyReportData3.get(0).get(1);
                datePayment = fmt.parse(stDatePayment);
                objDatesArray.add(datePayment);
            }
            Date maxDate = Collections.max(objDatesArray);
            day = maxDate.getDay();
            if(day == 0)
                stDay = "Sunday";
            else if(day == 1)
                stDay = "Monday";
            else if(day == 2)
                stDay = "Tuesday";
            else if(day == 3)
                stDay = "Wednesday";
            else if(day == 4)
                stDay = "Thursday";
            else if(day == 5)
                stDay = "Friday";
            else if(day == 6)
                stDay = "Saturday";
            if(datePurchase != null && maxDate.equals(datePurchase) && companyReportData1.size() > 0)
            {
                if(companyReportData.size() > 0 && companyReportData.get(companyReportData.size()-1).get(0).equals(stDatePurchase))
                {
                    newDataAdd = false;
                    if(companyReportData.get(companyReportData.size()-1).get(2).equals("-"))
                    {
                        companyReportData.get(companyReportData.size()-1).set(2, companyReportData1.get(0).get(1));
                        companyReportData.get(companyReportData.size()-1).set(3, companyReportData1.get(0).get(2));
                        companyReportData.get(companyReportData.size()-1).set(4, companyReportData1.get(0).get(3));
                        companyReportData.get(companyReportData.size()-1).set(5, companyReportData1.get(0).get(4));
                    }
                    else
                    {
                        String value = companyReportData.get(companyReportData.size()-1).get(2);
                        String value1 = companyReportData.get(companyReportData.size()-1).get(3);
                        String value2 = companyReportData.get(companyReportData.size()-1).get(4);
                        String value3 = companyReportData.get(companyReportData.size()-1).get(5);
                        companyReportData.get(companyReportData.size()-1).set(2, value+"\n"+companyReportData1.get(0).get(1));
                        companyReportData.get(companyReportData.size()-1).set(3, String.valueOf(Integer.parseInt(value1)+Integer.parseInt(companyReportData1.get(0).get(2))));
                        companyReportData.get(companyReportData.size()-1).set(4, String.valueOf(Integer.parseInt(value2)+Integer.parseInt(companyReportData1.get(0).get(3))));
                        companyReportData.get(companyReportData.size()-1).set(5, String.valueOf(Integer.parseInt(value3)+Integer.parseInt(companyReportData1.get(0).get(4))));
                    }
                }
                else
                {
                    newDataAdd = true;
                    temp.add(stDatePurchase);
                    temp.add(stDay);
                    temp.add(companyReportData1.get(0).get(1));
                    temp.add(companyReportData1.get(0).get(2));
                    temp.add(companyReportData1.get(0).get(3));
                    temp.add(companyReportData1.get(0).get(4));
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                }
                companyReportData1.remove(0);
            }
            else if(dateReturn != null && maxDate.equals(dateReturn) && companyReportData2.size() > 0)
            {
                if(companyReportData.size() > 0 && companyReportData.get(companyReportData.size()-1).get(0).equals(stDateReturn))
                {
                    newDataAdd = false;
                    if(companyReportData.get(companyReportData.size()-1).get(6).equals("-"))
                    {
                        companyReportData.get(companyReportData.size()-1).set(6, companyReportData2.get(0).get(1));
                        companyReportData.get(companyReportData.size()-1).set(7, companyReportData2.get(0).get(2));
                    }
                    else
                    {
                        String value1 = companyReportData.get(companyReportData.size()-1).get(6);
                        String value2 = companyReportData.get(companyReportData.size()-1).get(7);
                        companyReportData.get(companyReportData.size()-1).set(6, String.valueOf(Integer.parseInt(value1)+Integer.parseInt(companyReportData2.get(0).get(1))));
                        companyReportData.get(companyReportData.size()-1).set(7, String.valueOf(Integer.parseInt(value2)+Integer.parseInt(companyReportData2.get(0).get(2))));
                    }
                }
                else
                {
                    newDataAdd = true;
                    temp.add(stDateReturn);
                    temp.add(stDay);
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add(companyReportData2.get(0).get(1));
                    temp.add(companyReportData2.get(0).get(2));
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                }
                companyReportData2.remove(0);
            }
            else if (datePayment != null && maxDate.equals(datePayment) && companyReportData3.size() > 0)
            {
                if(companyReportData.size() > 0 && companyReportData.get(companyReportData.size()-1).get(0).equals(stDatePayment))
                {
                    newDataAdd = false;
                    if(companyReportData3.get(0).get(3).equals("Sent"))
                    {
                        if(companyReportData.get(companyReportData.size()-1).get(8).equals("-"))
                        {
                            companyReportData.get(companyReportData.size()-1).set(8, companyReportData3.get(0).get(2));
                        }
                        else
                        {
                            String value = companyReportData.get(companyReportData.size()-1).get(8);
                            companyReportData.get(companyReportData.size()-1).set(8, String.valueOf(Integer.parseInt(value)+Integer.parseInt(companyReportData3.get(0).get(2))));
                        }
                    }
                    else if (companyReportData3.get(0).get(3).equals("Return"))
                    {
                        if(companyReportData.get(companyReportData.size()-1).get(9).equals("-"))
                        {
                            companyReportData.get(companyReportData.size()-1).set(9, companyReportData3.get(0).get(2));
                        }
                        else
                        {
                            String value = companyReportData.get(companyReportData.size()-1).get(9);
                            companyReportData.get(companyReportData.size()-1).set(9, String.valueOf(Integer.parseInt(value)+Integer.parseInt(companyReportData3.get(0).get(2))));
                        }
                    }
                    else if (companyReportData3.get(0).get(3).equals("Waive Off"))
                    {
                        if(companyReportData.get(companyReportData.size()-1).get(10).equals("-"))
                        {
                            companyReportData.get(companyReportData.size()-1).set(10, companyReportData3.get(0).get(2));
                        }
                        else
                        {
                            String value = companyReportData.get(companyReportData.size()-1).get(10);
                            companyReportData.get(companyReportData.size()-1).set(10, String.valueOf(Integer.parseInt(value)+Integer.parseInt(companyReportData3.get(0).get(2))));
                        }
                        String value = companyReportData.get(companyReportData.size()-1).get(11);
                        companyReportData.get(companyReportData.size()-1).set(11, String.valueOf(Integer.parseInt(value)+Integer.parseInt(companyReportData3.get(0).get(4))));
                    }
                }
                else
                {
                    newDataAdd = true;
                    temp.add(stDatePayment);
                    temp.add(stDay);
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    temp.add("-");
                    if(companyReportData3.get(0).get(3).equals("Sent"))
                    {
                        temp.add(companyReportData3.get(0).get(2));
                        temp.add("-");
                        temp.add("-");
                    }
                    else if (companyReportData3.get(0).get(3).equals("Received"))
                    {
                        temp.add("-");
                        temp.add(companyReportData3.get(0).get(2));
                        temp.add("-");
                    }
                    else
                    {
                        temp.add("-");
                        temp.add("-");
                        temp.add(companyReportData3.get(0).get(2));
                    }
                    temp.add(companyReportData3.get(0).get(4));
                }
                companyReportData3.remove(0);
            }
            if(newDataAdd)
            {
                companyReportData.add(temp);
            }
            else
            {
                newDataAdd = true;
            }
        }

        return companyReportData;
    }

    public ArrayList<ArrayList<String>> getCompanyReportDetailSearch(Statement stmt1, Statement stmt2, Statement stmt3, Connection con, String companyId, String fromDate, String toDate, String day) throws ParseException {
        ArrayList<ArrayList<String>> companyReportData1 = new ArrayList<ArrayList<String>>();
        ArrayList<ArrayList<String>> companyReportData2 = new ArrayList<ArrayList<String>>();
        ArrayList<ArrayList<String>> companyReportData3 = new ArrayList<ArrayList<String>>();
        ArrayList<ArrayList<String>> companyReportData4 = new ArrayList<ArrayList<String>>();
        ArrayList<ArrayList<String>> companyReportData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs1 = null;
        ResultSet rs2 = null;
        ResultSet rs3 = null;
        String searchQuery1;
        String searchQuery2;
        String searchQuery3;
        int multipleSearch = 0;
        searchQuery1 = "SELECT `purchase_info`.`purchase_date`, `purchase_info`.`invoice_num`, SUM(`purchase_info_detail`.`recieve_quant`), `purchase_info`.`net_amount`, `purchase_info`.`disc_amount` FROM `purchase_info` LEFT OUTER JOIN `purchase_info_detail` ON `purchase_info_detail`.`purchase_id` = `purchase_info`.`purchase_id` WHERE ";
        searchQuery2 = "SELECT `purchase_return`.`entry_date`, SUM(`purchase_return_detail`.`prod_quant`) as 'return_quantity', SUM(`purchase_return_detail`.`total_amount`) as 'return_amount' FROM `purchase_return` INNER JOIN `purchase_return_detail` ON `purchase_return_detail`.`return_id` = `purchase_return`.`return_id` WHERE ";
        searchQuery3 = "SELECT `company_payments`.`payment_id`, `company_payments`.`date`, `company_payments`.`amount`, `company_payments`.`cash_type`, `company_payments`.`pending_payments` FROM `company_payments` WHERE ";
        if(!fromDate.equals(""))
        {
            searchQuery1 += "str_to_date(`purchase_info`.`purchase_date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') AND ";
            searchQuery2 += "str_to_date(`purchase_return`.`entry_date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') AND ";
            searchQuery3 += "str_to_date(`company_payments`.`date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') AND ";
        }
        if(!toDate.equals(""))
        {
            searchQuery1 += "str_to_date(`purchase_info`.`purchase_date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y') AND ";
            searchQuery2 += "str_to_date(`purchase_return`.`entry_date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y') AND ";
            searchQuery3 += "str_to_date(`company_payments`.`date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y') AND ";
        }
        searchQuery1 += "`purchase_info`.`status` != 'Deleted' AND `purchase_info`.`comp_id` = '"+companyId+"' GROUP BY `purchase_info`.`purchase_date` ORDER BY str_to_date(`purchase_info`.`purchase_date`, '%d/%b/%Y') DESC";
        searchQuery2 += "`purchase_return`.`status` != 'Deleted' AND `purchase_return`.`company_table_id` = '"+companyId+"' GROUP BY `purchase_return`.`entry_date` ORDER BY str_to_date(`purchase_return`.`entry_date`, '%d/%b/%Y') DESC";
        searchQuery3 += "`company_payments`.`company_id` = '"+companyId+"' AND `company_payments`.`status` != 'Deleted' ORDER BY str_to_date(`date`, '%d/%b/%Y') DESC";
        try {
            rs1 = stmt1.executeQuery(searchQuery1);
            while (rs1.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs1.getString(1)); // Purchase Date
                temp.add(rs1.getString(2)); // Invoice Num
                temp.add(rs1.getString(3)); // Receive Qty
                temp.add(rs1.getString(4)); // Purchase Price
                temp.add(rs1.getString(5)); // Discount Given
                companyReportData1.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            rs2 = stmt2.executeQuery(searchQuery2);
            while (rs2.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs2.getString(1)); // Return Date
                temp.add(rs2.getString(2)); // Return Quantity
                temp.add(rs2.getString(3)); // Return Price
                companyReportData2.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            rs3 = stmt3.executeQuery(searchQuery3);
            while (rs3.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs3.getString(1)); // Payment Id
                temp.add(rs3.getString(2)); // Payment Date
                temp.add(rs3.getString(3)); // Amount
                temp.add(rs3.getString(4)); // Payment Type
                temp.add(rs3.getString(5)); // Pending Payments
                companyReportData3.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        String stDatePurchase = "";
        String stDateReturn = "";
        String stDatePayment = "";
        Date datePurchase = null;
        Date dateReturn = null;
        Date datePayment = null;
        ArrayList<Date> objDatesArray;
        int intDay;
        String stDay = "";
        boolean newDataAdd = true;

        while (companyReportData1.size() > 0 || companyReportData2.size() > 0 || companyReportData3.size() > 0)
        {
            temp = new ArrayList<>();
            objDatesArray = new ArrayList<>();
            if(companyReportData1.size() > 0)
            {
                stDatePurchase = companyReportData1.get(0).get(0);
                datePurchase = fmt.parse(stDatePurchase);
                objDatesArray.add(datePurchase);
            }
            if(companyReportData2.size() > 0)
            {
                stDateReturn = companyReportData2.get(0).get(0);
                dateReturn = fmt.parse(stDateReturn);
                objDatesArray.add(dateReturn);
            }
            if(companyReportData3.size() > 0)
            {
                stDatePayment = companyReportData3.get(0).get(1);
                datePayment = fmt.parse(stDatePayment);
                objDatesArray.add(datePayment);
            }
            Date maxDate = Collections.max(objDatesArray);
            intDay = maxDate.getDay();
            if(intDay == 0)
                stDay = "Sunday";
            else if(intDay == 1)
                stDay = "Monday";
            else if(intDay == 2)
                stDay = "Tuesday";
            else if(intDay == 3)
                stDay = "Wednesday";
            else if(intDay == 4)
                stDay = "Thursday";
            else if(intDay == 5)
                stDay = "Friday";
            else if(intDay == 6)
                stDay = "Saturday";
            if(datePurchase != null && maxDate.equals(datePurchase) && companyReportData1.size() > 0)
            {
                if(companyReportData.size() > 0 && companyReportData.get(companyReportData.size()-1).get(0).equals(stDatePurchase))
                {
                    newDataAdd = false;
                    if(companyReportData.get(companyReportData.size()-1).get(2).equals("-"))
                    {
                        companyReportData.get(companyReportData.size()-1).set(2, companyReportData1.get(0).get(1));
                        companyReportData.get(companyReportData.size()-1).set(3, companyReportData1.get(0).get(2));
                        companyReportData.get(companyReportData.size()-1).set(4, companyReportData1.get(0).get(3));
                        companyReportData.get(companyReportData.size()-1).set(5, companyReportData1.get(0).get(4));
                    }
                    else
                    {
                        String value = companyReportData.get(companyReportData.size()-1).get(2);
                        String value1 = companyReportData.get(companyReportData.size()-1).get(3);
                        String value2 = companyReportData.get(companyReportData.size()-1).get(4);
                        String value3 = companyReportData.get(companyReportData.size()-1).get(5);
                        companyReportData.get(companyReportData.size()-1).set(2, value+"\n"+companyReportData1.get(0).get(1));
                        companyReportData.get(companyReportData.size()-1).set(3, String.valueOf(Integer.parseInt(value1)+Integer.parseInt(companyReportData1.get(0).get(2))));
                        companyReportData.get(companyReportData.size()-1).set(4, String.valueOf(Integer.parseInt(value2)+Integer.parseInt(companyReportData1.get(0).get(3))));
                        companyReportData.get(companyReportData.size()-1).set(5, String.valueOf(Integer.parseInt(value3)+Integer.parseInt(companyReportData1.get(0).get(4))));
                    }
                }
                else
                {
                    if(!day.equals("All"))
                    {
                        if (day.equals(stDay))
                        {
                            newDataAdd = true;
                            temp.add(stDatePurchase);
                            temp.add(stDay);
                            temp.add(companyReportData1.get(0).get(1));
                            temp.add(companyReportData1.get(0).get(2));
                            temp.add(companyReportData1.get(0).get(3));
                            temp.add(companyReportData1.get(0).get(4));
                            temp.add("-");
                            temp.add("-");
                            temp.add("-");
                            temp.add("-");
                            temp.add("-");
                            temp.add("-");
                        }
                    }
                    else
                    {
                        newDataAdd = true;
                        temp.add(stDatePurchase);
                        temp.add(stDay);
                        temp.add(companyReportData1.get(0).get(1));
                        temp.add(companyReportData1.get(0).get(2));
                        temp.add(companyReportData1.get(0).get(3));
                        temp.add(companyReportData1.get(0).get(4));
                        temp.add("-");
                        temp.add("-");
                        temp.add("-");
                        temp.add("-");
                        temp.add("-");
                        temp.add("-");
                    }

                }
                companyReportData1.remove(0);
            }
            else if(dateReturn != null && maxDate.equals(dateReturn) && companyReportData2.size() > 0)
            {
                if(companyReportData.size() > 0 && companyReportData.get(companyReportData.size()-1).get(0).equals(stDateReturn))
                {
                    newDataAdd = false;
                    if(companyReportData.get(companyReportData.size()-1).get(6).equals("-"))
                    {
                        companyReportData.get(companyReportData.size()-1).set(6, companyReportData2.get(0).get(1));
                        companyReportData.get(companyReportData.size()-1).set(7, companyReportData2.get(0).get(2));
                    }
                    else
                    {
                        String value1 = companyReportData.get(companyReportData.size()-1).get(6);
                        String value2 = companyReportData.get(companyReportData.size()-1).get(7);
                        companyReportData.get(companyReportData.size()-1).set(6, String.valueOf(Integer.parseInt(value1)+Integer.parseInt(companyReportData2.get(0).get(1))));
                        companyReportData.get(companyReportData.size()-1).set(7, String.valueOf(Integer.parseInt(value2)+Integer.parseInt(companyReportData2.get(0).get(2))));
                    }
                }
                else
                {
                    if(!day.equals("All"))
                    {
                        if (day.equals(stDay))
                        {
                            newDataAdd = true;
                            temp.add(stDateReturn);
                            temp.add(stDay);
                            temp.add("-");
                            temp.add("-");
                            temp.add("-");
                            temp.add("-");
                            temp.add(companyReportData2.get(0).get(1));
                            temp.add(companyReportData2.get(0).get(2));
                            temp.add("-");
                            temp.add("-");
                            temp.add("-");
                            temp.add("-");
                        }
                    }
                    else
                    {
                        newDataAdd = true;
                        temp.add(stDateReturn);
                        temp.add(stDay);
                        temp.add("-");
                        temp.add("-");
                        temp.add("-");
                        temp.add("-");
                        temp.add(companyReportData2.get(0).get(1));
                        temp.add(companyReportData2.get(0).get(2));
                        temp.add("-");
                        temp.add("-");
                        temp.add("-");
                        temp.add("-");
                    }

                }
                companyReportData2.remove(0);
            }
            else if (datePayment != null && maxDate.equals(datePayment) && companyReportData3.size() > 0)
            {
                if(companyReportData.size() > 0 && companyReportData.get(companyReportData.size()-1).get(0).equals(stDatePayment))
                {
                    newDataAdd = false;
                    if(companyReportData3.get(0).get(3).equals("Sent"))
                    {
                        if(companyReportData.get(companyReportData.size()-1).get(8).equals("-"))
                        {
                            companyReportData.get(companyReportData.size()-1).set(8, companyReportData3.get(0).get(2));
                        }
                        else
                        {
                            String value = companyReportData.get(companyReportData.size()-1).get(8);
                            companyReportData.get(companyReportData.size()-1).set(8, String.valueOf(Integer.parseInt(value)+Integer.parseInt(companyReportData3.get(0).get(2))));
                        }
                    }
                    else if (companyReportData3.get(0).get(3).equals("Return"))
                    {
                        if(companyReportData.get(companyReportData.size()-1).get(9).equals("-"))
                        {
                            companyReportData.get(companyReportData.size()-1).set(9, companyReportData3.get(0).get(2));
                        }
                        else
                        {
                            String value = companyReportData.get(companyReportData.size()-1).get(9);
                            companyReportData.get(companyReportData.size()-1).set(9, String.valueOf(Integer.parseInt(value)+Integer.parseInt(companyReportData3.get(0).get(2))));
                        }
                    }
                    else if (companyReportData3.get(0).get(3).equals("Waive Off"))
                    {
                        if(companyReportData.get(companyReportData.size()-1).get(10).equals("-"))
                        {
                            companyReportData.get(companyReportData.size()-1).set(10, companyReportData3.get(0).get(2));
                        }
                        else
                        {
                            String value = companyReportData.get(companyReportData.size()-1).get(10);
                            companyReportData.get(companyReportData.size()-1).set(10, String.valueOf(Integer.parseInt(value)+Integer.parseInt(companyReportData3.get(0).get(2))));
                        }
                        String value = companyReportData.get(companyReportData.size()-1).get(11);
                        companyReportData.get(companyReportData.size()-1).set(11, String.valueOf(Integer.parseInt(value)+Integer.parseInt(companyReportData3.get(0).get(4))));
                    }
                }
                else
                {
                    if(!day.equals("All"))
                    {
                        if (day.equals(stDay))
                        {
                            newDataAdd = true;
                            temp.add(stDatePayment);
                            temp.add(stDay);
                            temp.add("-");
                            temp.add("-");
                            temp.add("-");
                            temp.add("-");
                            temp.add("-");
                            temp.add("-");
                            if(companyReportData3.get(0).get(3).equals("Sent"))
                            {
                                temp.add(companyReportData3.get(0).get(2));
                                temp.add("-");
                                temp.add("-");
                            }
                            else if (companyReportData3.get(0).get(3).equals("Received"))
                            {
                                temp.add("-");
                                temp.add(companyReportData3.get(0).get(2));
                                temp.add("-");
                            }
                            else
                            {
                                temp.add("-");
                                temp.add("-");
                                temp.add(companyReportData3.get(0).get(2));
                            }
                            temp.add(companyReportData3.get(0).get(4));
                        }
                    }
                    else
                    {
                        newDataAdd = true;
                        temp.add(stDatePayment);
                        temp.add(stDay);
                        temp.add("-");
                        temp.add("-");
                        temp.add("-");
                        temp.add("-");
                        temp.add("-");
                        temp.add("-");
                        if(companyReportData3.get(0).get(3).equals("Sent"))
                        {
                            temp.add(companyReportData3.get(0).get(2));
                            temp.add("-");
                            temp.add("-");
                        }
                        else if (companyReportData3.get(0).get(3).equals("Received"))
                        {
                            temp.add("-");
                            temp.add(companyReportData3.get(0).get(2));
                            temp.add("-");
                        }
                        else
                        {
                            temp.add("-");
                            temp.add("-");
                            temp.add(companyReportData3.get(0).get(2));
                        }
                        temp.add(companyReportData3.get(0).get(4));
                    }
                }
                companyReportData3.remove(0);
            }
            if(newDataAdd && stDay.equals(day))
            {
                companyReportData.add(temp);
            }
            else if(newDataAdd && day.equals("All"))
            {
                companyReportData.add(temp);
            }
            else
            {
                newDataAdd = true;
            }
        }

        return companyReportData;
    }

}
