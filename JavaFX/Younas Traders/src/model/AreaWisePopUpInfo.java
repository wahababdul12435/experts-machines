package model;


import controller.AreaWisePOPUP;
import javafx.collections.FXCollections;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;
import model.AreaWisePopUpInfo;
import org.controlsfx.control.CheckListView;
import java.lang.Object;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import  javafx.scene.Node;
import  javafx.scene.Parent;
import  javafx.scene.layout.Region;
import  javafx.scene.control.Control;


public class AreaWisePopUpInfo {
    MysqlCon objMysqlCon;
    Statement objStmt;
    Connection objCon;

    public AreaWisePopUpInfo()
    {
        objMysqlCon = new MysqlCon();
        objStmt = objMysqlCon.stmt;
        objCon = objMysqlCon.con;
    }

    public ObservableList arealist()
    {
        final ObservableList<String> areasList = FXCollections.observableArrayList();
        ResultSet rs = null;
        try {
            rs = objStmt.executeQuery("SELECT `area_table_id`, `area_name` FROM `area_info` WHERE `area_status` = 'Active'");
            while (rs.next()) {

                areasList.add(rs.getString(2) +"  ("+ rs.getString(1) +")" );
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        areasList.add(0,"Check All");
        return areasList;
    }
    public ObservableList areaname()
    {
        final ObservableList<String> areasNameList = FXCollections.observableArrayList();
        ResultSet rs = null;
        try {
            rs = objStmt.executeQuery("SELECT  `area_name` FROM `area_info` WHERE `area_status` = 'Active'");
            while (rs.next()) {

                areasNameList.add( rs.getString(1) );
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        areasNameList.add(0,"Check All");
        return areasNameList;
    }
    public ObservableList cmpnylist()
    {
        final ObservableList<String> companiesList = FXCollections.observableArrayList();
        ResultSet rs = null;
        try {
            String queriescomp = "SELECT `company_table_id`, `company_name` FROM `company_info` WHERE `company_status` = 'Active'";
            rs = objStmt.executeQuery(queriescomp);
            while (rs.next()) {

                companiesList.add(rs.getString(2) +"  ("+ rs.getString(1) +")" );
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        companiesList.add(0,"Check All");
        return companiesList;
    }
    public ObservableList cmpnyName()
    {
        final ObservableList<String> companiesList = FXCollections.observableArrayList();
        ResultSet rs = null;
        try {
            String queriescomp = "SELECT `company_name` FROM `company_info` WHERE `company_status` = 'Active'";
            rs = objStmt.executeQuery(queriescomp);
            while (rs.next()) {

                companiesList.add(rs.getString(1));
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        companiesList.add(0,"Check All");
        return companiesList;
    }
    public ObservableList grouplist()
    {
        final ObservableList<String> groupsList = FXCollections.observableArrayList();
        ResultSet rs = null;
        try {
            rs = objStmt.executeQuery("SELECT `group_table_id`, `group_name` FROM `groups_info` WHERE `group_status` = 'Active'");
            while (rs.next()) {

                groupsList.add(rs.getString(2) +"  ("+ rs.getString(1) +")" );
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        groupsList.add(0,"Check All");
        return groupsList;
    }
    public ObservableList selectedComp_grouplist(int company_TableID)
    {
        final ObservableList<String> groupsList = FXCollections.observableArrayList();
        ResultSet rs = null;
        try {
            rs = objStmt.executeQuery("SELECT `group_table_id`, `group_name` FROM `groups_info` WHERE `company_id`='"+company_TableID+"' and `group_status` = 'Active'");
            while (rs.next()) {

                groupsList.add(rs.getString(2) +"  ("+ rs.getString(1) +")" );
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }

        return groupsList;
    }

    public int compTableID(String compsName)
    {
        int groupsList = 0;
        ResultSet rs = null;
        try {
            rs = objStmt.executeQuery("SELECT `company_table_id` FROM `company_info` WHERE `company_name` = '"+compsName+"'");
            while (rs.next()) {

                groupsList = Integer.parseInt(rs.getString(1));
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }

        return groupsList;
    }

    public ObservableList<String> onlyareasList=FXCollections.observableArrayList();

    public ObservableList searcharealist(String areaValue)
    {
        final ObservableList<String> areasList = FXCollections.observableArrayList();

        ResultSet rs = null;
        String getAreaListItem="";
        for (char c : areaValue.toCharArray()) {
            if (Character.isDigit(c)) {
                try {
                    String qury2 = "SELECT `area_table_id`, `area_name` FROM `area_info` WHERE `area_id`='"+areaValue+"%' and  `area_status` = 'Active'";
                    rs = objStmt.executeQuery(qury2);
                    while (rs.next()) {
                        if(areasList.contains(getAreaListItem))
                        { }
                        else
                        {
                            areasList.add(rs.getString(2) +"  ("+ rs.getString(1) +")" );
                            getAreaListItem=areasList.get(0);
                        }
                    }
                }
                catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        for (char c : areaValue.toCharArray()) {
            if (!Character.isDigit(c)) {
                try {
                    String qury1 = "SELECT `area_table_id`, `area_name` FROM `area_info` WHERE `area_name` like '"+areaValue+"%' and  `area_status` = 'Active'";
                    rs = objStmt.executeQuery(qury1);
                    while (rs.next()) {
                        if(areasList.contains(getAreaListItem))
                        {

                        }
                        else
                        {
                            areasList.add(rs.getString(2) +"  ("+ rs.getString(1) +")" );
                            getAreaListItem=areasList.get(0);
                        }
                    }
                }
                catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return areasList;
    }
    public ObservableList searchcomplist(String areaValue)
    {

        final ObservableList<String> compsList = FXCollections.observableArrayList();
        String getlistItem = "";
        ResultSet rs = null;
        for (char c : areaValue.toCharArray()) {
            if (Character.isDigit(c)) {
                try {
                    String qury2 = "SELECT `company_table_id`, `company_name` FROM `company_info` WHERE `company_id`='"+areaValue+"%' and  `company_status` = 'Active'";
                    rs = objStmt.executeQuery(qury2);
                    while (rs.next()) {

                        if(compsList.contains(getlistItem))
                        {

                        }
                        else
                        {
                            compsList.add(rs.getString(2) +"  ("+ rs.getString(1) +")" );
                            getlistItem=compsList.get(0);
                        }
                    }
                }
                catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        for (char c : areaValue.toCharArray()) {
            if (!Character.isDigit(c)) {
                try {
                    String qury1 = "SELECT `company_table_id`, `company_name` FROM `company_info` WHERE `company_name` like '"+areaValue+"%' and  `company_status` = 'Active'";
                    rs = objStmt.executeQuery(qury1);
                    while (rs.next()) {

                        if(compsList.contains(getlistItem))
                        {

                        }
                        else
                        {
                            compsList.add(rs.getString(2) +"  ("+ rs.getString(1) +")" );
                            getlistItem=compsList.get(0);
                        }
                    }
                }
                catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }



        return compsList;
    }
    public ObservableList searchgrouolist(String areaValue)
    {
        final ObservableList<String> groupsList = FXCollections.observableArrayList();
        ResultSet rs = null;
        String groupListItem = "";
        for (char c : areaValue.toCharArray()) {
            if (Character.isDigit(c)) {
                try {
                    String qury2 = "SELECT `group_table_id`, `group_name` FROM `groups_info` WHERE `group_id`='"+areaValue+"%' and  `group_status` = 'Active'";
                    rs = objStmt.executeQuery(qury2);
                    while (rs.next()) {

                        if(groupsList.contains(groupListItem))
                        {

                        }
                        else
                        {
                            groupsList.add(rs.getString(2) +"  ("+ rs.getString(1) +")" );
                            groupListItem=groupsList.get(0);
                        }
                    }
                }
                catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        for (char c : areaValue.toCharArray()) {
            if (!Character.isDigit(c)) {
                try {
                    String qury1 = "SELECT `group_table_id`, `group_name` FROM `groups_info` WHERE `group_name` like '"+areaValue+"%' and  `group_status` = 'Active'";
                    rs = objStmt.executeQuery(qury1);
                    while (rs.next()) {

                        if(groupsList.contains(groupListItem))
                        {

                        }
                        else
                        {
                            groupsList.add(rs.getString(2) +"  ("+ rs.getString(1) +")" );
                            groupListItem=groupsList.get(0);
                        }
                    }
                }
                catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }



        return groupsList;
    }
}
