package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class CreateUserInfo {

    public ArrayList<ArrayList<String>> getStaffData()
    {
        ArrayList<ArrayList<String>> staffData = new ArrayList<>();
        MysqlCon objMysqlCon = new MysqlCon();
        Statement stmt = objMysqlCon.stmt;
        Connection con = objMysqlCon.con;
        ResultSet rs = null;
        ArrayList<String> temp;
        try {
            rs = stmt.executeQuery("SELECT `user_info`.`user_table_id`, `user_info`.`user_name` FROM `user_info` LEFT JOIN `user_accounts` ON `user_info`.`user_table_id` = `user_accounts`.`user_id` WHERE `user_info`.`user_status` != 'Deleted' AND `user_accounts`.`user_id` is NULL");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2)+" ("+rs.getString(1)+")");
                staffData.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return staffData;
    }

    public void insertUser(Statement stmt, Connection con, String userId, String userName, String password, String rightInsert, String rightView, String rightChange, String rightDelete, String rightStock, String rightCash, String rightSettings, String mobileApp) throws SQLException {
        String currentDate = GlobalVariables.getStDate();
        String currentTime = GlobalVariables.getStTime();
        String currentUser = GlobalVariables.getUserId();
        String currentStatus = "off";
        String userStatus = "Approved";
        String insertSql;
        ResultSet rs = null;
        insertSql = "INSERT INTO `user_rights`(`user_id`, `create_record`, `read_record`, `edit_record`, `delete_record`, `manage_stock`, `manage_cash`, `manage_settings`, `mobile_app`) VALUES ('"+userId+"','"+rightInsert+"','"+rightView+"','"+rightChange+"','"+rightDelete+"','"+rightStock+"','"+rightCash+"','"+rightSettings+"','"+mobileApp+"')";
        stmt.executeUpdate(insertSql);
        String rightId = "SELECT `rights_id` FROM `user_rights` WHERE `user_id` = '"+userId+"'";
        rs = stmt.executeQuery(rightId);
        rs.next();
        rightId = rs.getString(1);
        insertSql = "INSERT INTO `user_accounts`(`user_name`, `user_id`, `user_password`, `user_rights`, `reg_date`, `current_status`, `user_status`) VALUES ('"+userName+"','"+userId+"','"+password+"','"+rightId+"','"+currentDate+"','"+currentStatus+"','"+userStatus+"')";
        if(stmt.executeUpdate(insertSql) == -1)
        {
            String title = "Error";
            String message = "Unable to insert";
            GlobalVariables.showNotification(-1, title, message);
        }
        else
        {
            String title = "Successfull";
            String message = "Record Inserted";
            GlobalVariables.showNotification(1, title, message);
        }

    }
}
