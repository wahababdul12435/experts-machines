package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import controller.CreatePurchaseInvoice;
import controller.ViewPurchaseDetail;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;

import javax.swing.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class CreatePurchaseInvoiceInfo {
    public String purchaseId;
    public String invoiceNum;
    private String srNo;
    private String productId;
    private String productName;
    private String batchNo;
    private String batchExpiry;
    private String productQty;
    private String bonusQty;
    private String retailPrice;
    private String tradePrice;
    private String productPrice;
    private String productDiscount;
    private String productNetPrice;

    private HBox operationsPane;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    public static ArrayList<String> productIdArr = new ArrayList<>();
    public static ArrayList<String> productNameArr = new ArrayList<>();
    public static ArrayList<String> batchNoArr = new ArrayList<>();
    public static ArrayList<String> batchExpiryArr = new ArrayList<>();
    public static ArrayList<Boolean> nearExpiryArr = new ArrayList<>();
    public static ArrayList<String> productQtyArr = new ArrayList<>();
    public static ArrayList<String> bonusQtyArr = new ArrayList<>();
    public static ArrayList<String> retailPriceArr = new ArrayList<>();
    public static ArrayList<String> tradePriceArr = new ArrayList<>();
    public static ArrayList<String> productPriceArr = new ArrayList<>();
    public static ArrayList<String> productDiscountArr = new ArrayList<>();
    public static ArrayList<String> productNetPriceArr = new ArrayList<>();

    public static TableView<CreatePurchaseInvoiceInfo> table_purchasedetaillog;

    public static JFXTextField txtProductName;
    public static JFXTextField txtBatchNo;
    public static JFXDatePicker txtBatchExpiry;
    public static JFXTextField txtProductQty;
    public static JFXTextField txtProductBonus;
    public static JFXTextField txtRetailPrice;
    public static JFXTextField txtTradePrice;
    public static JFXTextField txtProductPrice;
    public static JFXTextField txtProductDiscount;
    public static JFXTextField txtProductNetPrice;
    public static Label lbl_product_quantity;
    public static Label lbl_bonus_quantity;
    public static Label lbl_net_price;
    public static Label lbl_discount_given;
    public static Label lbl_near_expiry;

    public static Button btnAdd;
    public static Button btnCancel;
    public static Button btnUpdate;

    public CreatePurchaseInvoiceInfo() {

    }

    public CreatePurchaseInvoiceInfo(String srNo, String productId, String productName, String batchNo, String batchExpiry, String productQty, String bonusQty, String retailPrice, String tradePrice, String productPrice, String productDiscount, String productNetPrice) {
        this.srNo = srNo;
        this.productId = productId;
        this.productName = productName;
        this.batchNo = batchNo;
        this.batchExpiry = batchExpiry;
        this.productQty = productQty;
        this.bonusQty = bonusQty;
        this.retailPrice = retailPrice;
        this.tradePrice = tradePrice;
        this.productPrice = productPrice;
        this.productDiscount = productDiscount;
        this.productNetPrice = productNetPrice;

        this.btnEdit = new JFXButton();
        this.btnDelete = new JFXButton();
        this.btnEdit.setGraphic(GlobalVariables.createEditIcon());
        this.btnDelete.setGraphic(GlobalVariables.createDeleteIcon());
        this.btnEdit.getStyleClass().add("btn_icon");
        this.btnDelete.getStyleClass().add("btn_icon");
        this.operationsPane = new HBox(btnEdit, btnDelete);
        this.btnEdit.setOnAction((action)->editAddedRecord());
        this.btnDelete.setOnAction((action)->deleteAddedRecord());
    }

    private void editAddedRecord()
    {
        ViewPurchaseDetail.srNo = Integer.parseInt(srNo)-1;
        txtProductName.setText(productNameArr.get(Integer.parseInt(srNo)-1));
        txtBatchNo.setText(batchNoArr.get(Integer.parseInt(srNo)-1));
        txtBatchExpiry.setValue(GlobalVariables.LOCAL_DATE(batchExpiryArr.get(Integer.parseInt(srNo)-1)));
        txtProductQty.setText(productQtyArr.get(Integer.parseInt(srNo)-1));
        txtProductBonus.setText(bonusQtyArr.get(Integer.parseInt(srNo)-1));
        txtRetailPrice.setText(retailPriceArr.get(Integer.parseInt(srNo)-1));
        txtTradePrice.setText(tradePriceArr.get(Integer.parseInt(srNo)-1));
        txtProductPrice.setText(productPriceArr.get(Integer.parseInt(srNo)-1));
        txtProductDiscount.setText(productDiscountArr.get(Integer.parseInt(srNo)-1));
        txtProductNetPrice.setText(productNetPriceArr.get(Integer.parseInt(srNo)-1));
        btnAdd.setVisible(false);
        btnCancel.setVisible(true);
        btnUpdate.setVisible(true);
    }

    private void deleteAddedRecord()
    {
        CreatePurchaseInvoice.productsQty--;
        CreatePurchaseInvoice.bonusQty -= Integer.parseInt(bonusQtyArr.get(Integer.parseInt(srNo)-1).replace(",", ""));
        CreatePurchaseInvoice.netPrice -= Float.parseFloat(productNetPriceArr.get(Integer.parseInt(srNo)-1).replace(",", ""));
        CreatePurchaseInvoice.discountGiven -= Float.parseFloat(productDiscountArr.get(Integer.parseInt(srNo)-1).replace(",", ""));
        if(nearExpiryArr.get(Integer.parseInt(srNo)-1))
        {
            CreatePurchaseInvoice.nearExpiry--;
        }
        productIdArr.remove(Integer.parseInt(srNo)-1);
        productNameArr.remove(Integer.parseInt(srNo)-1);
        batchNoArr.remove(Integer.parseInt(srNo)-1);
        batchExpiryArr.remove(Integer.parseInt(srNo)-1);
        nearExpiryArr.remove(Integer.parseInt(srNo)-1);
        productQtyArr.remove(Integer.parseInt(srNo)-1);
        bonusQtyArr.remove(Integer.parseInt(srNo)-1);
        retailPriceArr.remove(Integer.parseInt(srNo)-1);
        tradePriceArr.remove(Integer.parseInt(srNo)-1);
        productPriceArr.remove(Integer.parseInt(srNo)-1);
        productDiscountArr.remove(Integer.parseInt(srNo)-1);
        productNetPriceArr.remove(Integer.parseInt(srNo)-1);
        table_purchasedetaillog.setItems(CreatePurchaseInvoice.parseUserList());
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    public String getBatchExpiry() {
        return batchExpiry;
    }

    public void setBatchExpiry(String batchExpiry) {
        this.batchExpiry = batchExpiry;
    }

    public String getProductQty() {
        return productQty;
    }

    public void setProductQty(String productQty) {
        this.productQty = productQty;
    }

    public String getBonusQty() {
        return bonusQty;
    }

    public void setBonusQty(String bonusQty) {
        this.bonusQty = bonusQty;
    }

    public String getRetailPrice() {
        return retailPrice;
    }

    public void setRetailPrice(String retailPrice) {
        this.retailPrice = retailPrice;
    }

    public String getTradePrice() {
        return tradePrice;
    }

    public void setTradePrice(String tradePrice) {
        this.tradePrice = tradePrice;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductDiscount() {
        return productDiscount;
    }

    public void setProductDiscount(String productDiscount) {
        this.productDiscount = productDiscount;
    }

    public String getProductNetPrice() {
        return productNetPrice;
    }

    public void setProductNetPrice(String productNetPrice) {
        this.productNetPrice = productNetPrice;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public ArrayList<ArrayList<String>> loadPurchaseTable()
    {
        ArrayList<ArrayList<String>> purchaseData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        for(int i=0; i<productIdArr.size(); i++)
        {
            temp = new ArrayList<String>();
            temp.add(productIdArr.get(i));
            temp.add(productNameArr.get(i));
            temp.add(batchNoArr.get(i));
            temp.add(batchExpiryArr.get(i));
            temp.add(productQtyArr.get(i));
            temp.add(bonusQtyArr.get(i));
            temp.add(retailPriceArr.get(i));
            temp.add(tradePriceArr.get(i));
            temp.add(productPriceArr.get(i));
            temp.add(productDiscountArr.get(i));
            temp.add(productNetPriceArr.get(i));
            purchaseData.add(temp);
        }
        return purchaseData;
    }

    public void insertbactches(Statement stmt1, Connection con1,int y) throws SQLException {
        String insertBatch = "INSERT INTO `batchwise_stock`(`product_table_id`, `batch_no`, `quantity`, `bonus`, `retail_price`, `trade_price`, `purchase_price`, `batch_expiry`, `entry_date`, `entry_time`, `update_date`, `update_time`, `server_sync`) VALUES ('"+productIdArr.get(y)+"', '"+batchNoArr.get(y)+"', '"+productQtyArr.get(y)+"', '"+bonusQtyArr.get(y)+"', '"+retailPriceArr.get(y)+"', '"+tradePriceArr.get(y)+"', '"+productPriceArr.get(y)+"', '"+batchExpiryArr.get(y)+"', '"+GlobalVariables.getStDate()+"', '"+GlobalVariables.getStTime()+"', '', '', 'Insert')";
        stmt1.executeUpdate(insertBatch);
    }

    public void updatebactches(Statement stmt2, Connection con2,int z,ArrayList<String> batchList) throws SQLException {
        String updateBatch = "UPDATE `batchwise_stock` SET `quantity` = `quantity` + '"+productQtyArr.get(z)+"', `bonus` = `bonus` + '"+bonusQtyArr.get(z)+"', `retail_price`= '"+retailPriceArr.get(z)+"',`trade_price`= '"+tradePriceArr.get(z)+"',`purchase_price`= '"+productPriceArr.get(z)+"',`batch_expiry`= '"+batchExpiryArr.get(z)+"',`update_date`= '"+GlobalVariables.getStDate()+"',`update_time`= '"+GlobalVariables.getStTime()+"',`server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `batch_stock_id` = '"+batchList.get(z)+"'; ";
        stmt2.executeUpdate(updateBatch);
    }
    public void insertInvoice(Statement stmt, Connection con)
    {
        ResultSet rs = null;
        ResultSet rs3 = null;
        String updateBatch = "";
        String updateQuery = "INSERT INTO `purchase_info_detail`(`invoice_num`, `purchase_id`, `prod_id`, `bonus_quant`, `recieve_quant`, `batch_no`, `expiry_date`, `gross_amount`, `disc_amount`, `net_amount`, `returnBit`, `returned_quant`, `returned_bonus_quant`) VALUES ";
        String unit = "Packets";
        try {
            for(int i=0; i<productIdArr.size(); i++)
            {
                if(i == productIdArr.size()-1)
                    updateQuery += "('"+invoiceNum+"','"+purchaseId+"','"+productIdArr.get(i)+"','"+bonusQtyArr.get(i)+"','"+productQtyArr.get(i)+"','"+batchNoArr.get(i)+"','"+batchExpiryArr.get(i)+"','"+productPriceArr.get(i)+"','"+productDiscountArr.get(i)+"','"+productNetPriceArr.get(i)+"','0','0','0') ";
                else
                    updateQuery += "('"+invoiceNum+"','"+purchaseId+"','"+productIdArr.get(i)+"','"+bonusQtyArr.get(i)+"','"+productQtyArr.get(i)+"','"+batchNoArr.get(i)+"','"+batchExpiryArr.get(i)+"','"+productPriceArr.get(i)+"','"+productDiscountArr.get(i)+"','"+productNetPriceArr.get(i)+"','0','0','0') , ";
            }
//            updateQuery = "UPDATE `dealer_info` SET `dealer_id`='"+dealerId+"',`dealer_area_id`='"+areaId+"',`dealer_name`='"+dealerName+"',`dealer_phone`='"+dealerContact+"',`dealer_address`='"+dealerAddress+"',`dealer_type`='"+dealerType+"',`dealer_cnic`='"+dealerCnic+"',`dealer_lic9_num`='"+dealerLicNum+"',`dealer_lic9_exp`='"+dealerLicExp+"',`dealer_status`='"+dealerStatus+"', `update_user_id`='"+currentUser+"',`update_date`='"+currentDate+"',`update_time`='"+currentTime+"' WHERE `dealer_table_id` = '"+dealerTableId+"'";
//            System.out.println(updateQuery);
            stmt.executeUpdate(updateQuery);

            ArrayList<String> batchStockIdArr = new ArrayList<>();
            for(int i=0; i<productIdArr.size(); i++)
            {
                String getPurchaseId = "SELECT `batch_stock_id` FROM `batchwise_stock` WHERE `product_table_id` = '"+productIdArr.get(i)+"' AND `batch_no` = '"+batchNoArr.get(i)+"'";
                rs = stmt.executeQuery(getPurchaseId);


                if(rs.next())
                {
                    batchStockIdArr.add(rs.getString(1));
                }
                else
                {
                    batchStockIdArr.add("0");
                }
               /* rs.next();
                if(rs.next() != false)
                {
                    batchStockIdArr.add(rs.getString(1));
                }
                else
                {
                    batchStockIdArr.add("0");
                }*/
            }

            for(int i=0; i<productIdArr.size(); i++)
            {
                if(!batchStockIdArr.get(i).equals("0"))
                {
                    updatebactches(stmt,con,i,batchStockIdArr);
                }
                else
                {
                    insertbactches(stmt,con,i);
                }
                String stckProdId = "";
                try {
                    rs3 = stmt.executeQuery("select product_table_id from products_stock where product_table_id = '"+productIdArr.get(i)+"' ");

                    while (rs3.next()) {
                        stckProdId = rs3.getString(1);
                    }

                }
                catch (SQLException e) {
                    e.printStackTrace();
                }
                int totalStock = Integer.parseInt(productQtyArr.get(i)) + Integer.parseInt(bonusQtyArr.get(i));
                if(stckProdId=="")
                {
                    updateBatch = "INSERT INTO `products_stock`( `product_table_id`, `in_stock`, `min_stock`, `max_stock`, `bonus_quant`, `status`, `server_sync`) VALUES ('"+productIdArr.get(i)+"','"+productQtyArr.get(i)+"','0','0','"+bonusQtyArr.get(i)+"','Active','Update')";
                    stmt.executeUpdate(updateBatch);
                }
                else {
                    updateBatch = "UPDATE `products_stock` SET `bonus_quant` = `bonus_quant` + '" + bonusQtyArr.get(i) + "', `in_stock` = `in_stock` + '" + productQtyArr.get(i) + "', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `product_table_id` = '" + productIdArr.get(i) + "'";
                    stmt.executeUpdate(updateBatch);
                }
                con.close();
            }


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertBasic(Statement stmt, String companyId, String invoiceNum, String supplierId, String purchaseDate, String grossAmount, String discountAmount, String netAmount)
    {
        String currentDate = GlobalVariables.getStDate();
        String currentTime = GlobalVariables.getStTime();
        String currentUser = GlobalVariables.getUserId();
        String status = "Active";
        String serverSync = "Insert";
        ResultSet rs = null;
        String updateQuery = "INSERT INTO `purchase_info`(`comp_id`, `invoice_num`, `supplier_id`, `purchase_date`, `gross_amount`, `disc_amount`, `net_amount`, `enter_user_id`, `entry_date`, `entry_time`, `status`, `server_sync`) VALUES ('"+companyId+"', '"+invoiceNum+"', '"+supplierId+"', '"+purchaseDate+"', '"+grossAmount+"', '"+discountAmount+"', '"+netAmount+"', '"+currentUser+"', '"+currentDate+"', '"+currentTime+"', '"+status+"', '"+serverSync+"')";
        try {
            stmt.executeUpdate(updateQuery);
            String getPurchaseId = "SELECT `purchase_id` FROM `purchase_info` WHERE `comp_id` = '"+companyId+"' AND `entry_date` = '"+currentDate+"' AND `entry_time` = '"+currentTime+"'";
            rs = stmt.executeQuery(getPurchaseId);
            rs.next();
            purchaseId = rs.getString(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
