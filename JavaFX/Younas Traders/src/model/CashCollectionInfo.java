package model;

import com.jfoenix.controls.JFXDialog;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class CashCollectionInfo {

    public ArrayList<ArrayList<String>> citiesData = new ArrayList<>();
    public ArrayList<ArrayList<String>> cityAreasData = new ArrayList<>();
    public static StackPane stackPane;
    public static JFXDialog dialog;
    public static Label lblUpdate;

    public ArrayList<ArrayList<String>> getCitiesData()
    {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement stmt = objMysqlCon.stmt;
        Connection con = objMysqlCon.con;
        ResultSet rs = null;
        ArrayList<String> temp;
        try {
            rs = stmt.executeQuery("SELECT `city_table_id`, `city_name` FROM `city_info`");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2));
                citiesData.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return citiesData;
    }
    public ArrayList<String> orderData = new ArrayList<>();
    String[] prevProductOverallRecord = new String[1];
    public ArrayList<String> get_orderDetails(Statement stmt3, Connection con3,int dealerIDs,int invoNumbers )
    {
        ResultSet rs3 = null;
        try {
            rs3 = stmt3.executeQuery("select dealer_info.dealer_name, order_info.booking_date, order_info.final_price,dealer_overall_record.pending_payments from dealer_info INNER JOIN order_info on dealer_info.dealer_id = order_info.dealer_id INNER JOIN dealer_overall_record on dealer_overall_record.dealer_id = dealer_info.dealer_id   where order_info.dealer_id= '"+dealerIDs+"' and order_info.order_id = '"+invoNumbers+"' and order_info.status = 'Pending' ");

            int i = 0;
            while (rs3.next())
            {

                orderData.add(rs3.getString(1) +"--"+rs3.getString(2) +"--"+rs3.getString(3)  +"--"+rs3.getString(4));
                i++;
            }
            con3.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return orderData;
    }

    public void insert_CashCollected(Statement stmt, Connection con, int dealerid, int orderid,String collectiondate, float totalpaymnt,float cashreceived)
    {

        try {
            String insertQuery = "INSERT INTO `cash_collected` (`dealer_id`, `order_id`, `cash_collection_date`, `total_payment`, `cash_received` ) VALUES ('"+dealerid+"','"+orderid+"','"+collectiondate+"','"+totalpaymnt+"','"+cashreceived+"' )";
            if(stmt.executeUpdate(insertQuery) == -1)
            {
                String title = "Error";
                String message = "Unable to insert";
                GlobalVariables.showNotification(-1, title, message);
            }
            else
            {
                String title = "Successfull";
                String message = "Record Inserted";
                GlobalVariables.showNotification(1, title, message);
            }
//            con.close();
        } catch (SQLException e) {
//            e.printStackTrace();
            String title = "Error";
            String message = e.getMessage();
            GlobalVariables.showNotification(-1, title, message);
        }
    }

    public ArrayList<ArrayList<String>> getCityAreasData()
    {
        MysqlCon objMysqlCon = new MysqlCon();
        Statement stmt = objMysqlCon.stmt;
        Connection con = objMysqlCon.con;
        ResultSet rs = null;
        ArrayList<String> temp;
        try {
            rs = stmt.executeQuery("SELECT `area_table_id`, `area_name`, `city_table_id` FROM `area_info`");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2));
                temp.add(rs.getString(3));
                cityAreasData.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cityAreasData;
    }
}
