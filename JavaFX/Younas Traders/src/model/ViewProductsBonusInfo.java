package model;

import com.jfoenix.controls.JFXButton;
import javafx.scene.layout.HBox;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ViewProductsBonusInfo {
    private String srNo;
    private String approvalId;
    private String productId;
    private String productName;
    private String saleAmount;
    private String bonus;
    private String startDate;
    private String endDate;
    private String userName;

    private HBox operationsPane;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    public ViewProductsBonusInfo() {
    }

    public ViewProductsBonusInfo(String srNo, String approvalId, String productId, String productName, String saleAmount, String bonus, String startDate, String endDate, String userName) {
        this.srNo = srNo;
        this.approvalId = approvalId;
        this.productId = productId;
        this.productName = productName;
        this.saleAmount = saleAmount;
        this.bonus = bonus;
        this.startDate = startDate;
        this.endDate = endDate;
        this.userName = userName;

        this.btnEdit = new JFXButton();
        this.btnDelete = new JFXButton();
        this.btnEdit.setGraphic(GlobalVariables.createEditIcon());
        this.btnDelete.setGraphic(GlobalVariables.createDeleteIcon());
        this.btnEdit.getStyleClass().add("btn_icon");
        this.btnDelete.getStyleClass().add("btn_icon");
        this.operationsPane = new HBox(btnEdit, btnDelete);
//        this.btnEdit.setOnAction((action)->editClicked());
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getApprovalId() {
        return approvalId;
    }

    public void setApprovalId(String approvalId) {
        this.approvalId = approvalId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getSaleAmount() {
        return saleAmount;
    }

    public void setSaleAmount(String saleAmount) {
        this.saleAmount = saleAmount;
    }

    public String getBonus() {
        return bonus;
    }

    public void setBonus(String bonus) {
        this.bonus = bonus;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public ArrayList<ArrayList<String>> getProductsBonus(Statement stmt)
    {
        String currentDate = GlobalVariables.getStDate();
        ArrayList<ArrayList<String>> bonusData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        String query = "SELECT `bonus_policy`.`approval_id`, `product_info`.`product_id`, `product_info`.`product_name`, `bonus_policy`.`quantity`, `bonus_policy`.`bonus_quant`, `bonus_policy`.`start_date`, `bonus_policy`.`end_date`, `user_info`.`user_name` FROM `bonus_policy` LEFT OUTER JOIN `product_info` ON `product_info`.`product_table_id` = `bonus_policy`.`product_table_id` LEFT OUTER JOIN `user_info` ON `user_info`.`user_table_id` = `bonus_policy`.`entered_by` WHERE str_to_date(`bonus_policy`.`start_date`, '%d/%b/%Y') <= str_to_date('"+currentDate+"', '%d/%b/%Y') AND str_to_date(`bonus_policy`.`end_date`, '%d/%b/%Y') >= str_to_date('"+currentDate+"', '%d/%b/%Y') AND `bonus_policy`.`dealer_table_id` = '0' AND `bonus_policy`.`company_table_id` = '0' AND `bonus_policy`.`policy_status` != 'Deleted' ORDER BY str_to_date(`bonus_policy`.`end_date`, '%d/%b/%Y') DESC";
        try {
            rs = stmt.executeQuery(query);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2));
                temp.add(rs.getString(3));
                temp.add(rs.getString(4));
                temp.add(rs.getString(5));
                temp.add(rs.getString(6));
                temp.add(rs.getString(7));
                temp.add(rs.getString(8));
                bonusData.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return bonusData;
    }

    public ArrayList<ArrayList<String>> getProductsBonusSearch(Statement stmt, String approvalId, String productId, String productName, String startDate, String endDate)
    {
        String currentDate = GlobalVariables.getStDate();
        ArrayList<ArrayList<String>> bonusData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        boolean multipleSearch = false;
        String searchQuery = "SELECT `bonus_policy`.`approval_id`, `product_info`.`product_id`, `product_info`.`product_name`, `bonus_policy`.`quantity`, `bonus_policy`.`bonus_quant`, `bonus_policy`.`start_date`, `bonus_policy`.`end_date`, `user_info`.`user_name` FROM `bonus_policy` LEFT OUTER JOIN `product_info` ON `product_info`.`product_table_id` = `bonus_policy`.`product_table_id` LEFT OUTER JOIN `user_info` ON `user_info`.`user_table_id` = `bonus_policy`.`entered_by` WHERE";
        if(!approvalId.equals(""))
        {
            searchQuery += " `bonus_policy`.`approval_id` = '"+approvalId+"'";
            multipleSearch = true;
        }
        if(!productId.equals(""))
        {
            if(multipleSearch)
            {
                searchQuery += " AND";
            }
            searchQuery += " `product_info`.`product_id` = '"+productId+"'";
            multipleSearch = true;
        }
        if(!productName.equals(""))
        {
            if(multipleSearch)
            {
                searchQuery += " AND";
            }
            searchQuery += " `product_info`.`product_name` = '"+productName+"'";
            multipleSearch = true;
        }
        if(!startDate.equals(""))
        {
            if(multipleSearch)
            {
                searchQuery += " AND";
            }
            searchQuery += " str_to_date(`bonus_policy`.`start_date`, '%d/%b/%Y') >= str_to_date('"+startDate+"'', '%d/%b/%Y') ";
            multipleSearch = true;
        }
        if(!endDate.equals(""))
        {
            if(multipleSearch)
            {
                searchQuery += " AND";
            }
            searchQuery += " str_to_date(`bonus_policy`.`end_date`, '%d/%b/%Y') <= str_to_date('"+endDate+"'', '%d/%b/%Y') ";
            multipleSearch = true;
        }
        if(multipleSearch)
        {
            searchQuery += " AND";
        }
        searchQuery += " `bonus_policy`.`dealer_table_id` = '0' AND `bonus_policy`.`company_table_id` = '0' AND `bonus_policy`.`policy_status` != 'Deleted' ORDER BY str_to_date(`bonus_policy`.`end_date`, '%d/%b/%Y') DESC";
        try {
            rs = stmt.executeQuery(searchQuery);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2));
                temp.add(rs.getString(3));
                temp.add(rs.getString(4));
                temp.add(rs.getString(5));
                temp.add(rs.getString(6));
                temp.add(rs.getString(7));
                temp.add(rs.getString(8));
                bonusData.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return bonusData;
    }
}
