package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import controller.EnterArea;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class AreaInfo {
    private String srNo;
    private String areaId;
    private String areaName;
    private String cityName;

    private HBox operationsPane;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    public static ArrayList<String> areaIdArr = new ArrayList<>();
    public static ArrayList<String> areaNameArr = new ArrayList<>();
    public static ArrayList<String> cityIdArr = new ArrayList<>();
    public static ArrayList<String> cityNameArr = new ArrayList<>();

    public static TableView<AreaInfo> table_add_area;

    public static JFXTextField txtAreaId;
    public static JFXTextField txtAreaName;
    public static JFXComboBox<String> txtCityName;

    public static Button btnAdd;
    public static Button btnCancel;
    public static Button btnUpdate;

    private ArrayList<String> savedCityIds;
    private ArrayList<String> savedCityNames;

    MysqlCon objMysqlCon = new MysqlCon();
    Statement objStmt = objMysqlCon.stmt;
    Connection objCon = objMysqlCon.con;

    public AreaInfo() {
        this.srNo = "";
        this.areaId = "";
        this.areaName = "";
        this.cityName = "";

        getCitiesInfo(objStmt, objCon);
    }

    public AreaInfo(String srNo, String areaId, String areaName, String cityName) {
        this.srNo = srNo;
        this.areaId = areaId;
        this.areaName = areaName;
        this.cityName = cityName;

        this.btnEdit = new JFXButton("Edit");
        this.btnDelete = new JFXButton("Delete");
        this.btnEdit.setOnAction((action)->editAddedRecord());
        this.btnDelete.setOnAction((action)->deleteAddedRecord());
        this.operationsPane = new HBox(btnEdit, btnDelete);
    }

    private void deleteAddedRecord()
    {
        areaIdArr.remove(Integer.parseInt(srNo)-1);
        areaNameArr.remove(Integer.parseInt(srNo)-1);
        cityIdArr.remove(Integer.parseInt(srNo)-1);
        cityNameArr.remove(Integer.parseInt(srNo)-1);
        table_add_area.setItems(EnterArea.parseUserList());
    }

    public void editAddedRecord()
    {
        EnterArea.srNo = Integer.parseInt(srNo)-1;
        txtAreaId.setText(areaIdArr.get(Integer.parseInt(srNo)-1));
        txtAreaName.setText(areaNameArr.get(Integer.parseInt(srNo)-1));
        txtCityName.setValue(cityNameArr.get(Integer.parseInt(srNo)-1));
        btnAdd.setVisible(false);
        btnCancel.setVisible(true);
        btnUpdate.setVisible(true);
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public ArrayList<String> getSavedCityIds() {
        return savedCityIds;
    }

    public void setSavedCityIds(ArrayList<String> savedCityIds) {
        this.savedCityIds = savedCityIds;
    }

    public ArrayList<String> getSavedCityNames() {
        return savedCityNames;
    }

    public void setSavedCityNames(ArrayList<String> savedCityNames) {
        this.savedCityNames = savedCityNames;
    }

    public void insertArea(Statement stmt, Connection con, String areaName, String areaStatus, String[] subAreas, String subAreaStatus)
    {
        try {
            String getAreaID = "SELECT `AUTO_INCREMENT` FROM  INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'younas_traders' AND TABLE_NAME = 'area_info'";
            ResultSet rs =stmt.executeQuery(getAreaID);
            rs.next();
            int areaID = rs.getInt(1);

            String insertQuery = "INSERT INTO `area_info`(`area_name`, `area_status`) VALUES ('"+areaName+"','"+areaStatus+"')";
            stmt.executeUpdate(insertQuery);

            for(int x=0; x<subAreas.length; x++)
            {
                String insertSubAreas = "INSERT INTO `subarea_info`(`area_id`, `sub_area_name`, `subarea_status`) VALUES ('"+areaID+"','"+subAreas[x]+"','"+subAreaStatus+"')";
                stmt.executeUpdate(insertSubAreas);
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertArea(Statement stmt, Connection con) throws SQLException {
        String currentDate = GlobalVariables.getStDate();
        String currentTime = GlobalVariables.getStTime();
        String currentUser = GlobalVariables.getUserId();
        boolean withId = false;
        boolean withoutId = false;
        String insertQueryWithId = "INSERT INTO `area_info`(`area_id`, `area_name`, `area_abbrev` , `city_table_id`, `area_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`) VALUES ";
        String insertQueryWithoutId = "INSERT INTO `area_info`(`area_name`, `area_abbrev`, `city_table_id`, `area_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`) VALUES ";

        for(int i=0; i<areaIdArr.size(); i++)
        {
            if(areaIdArr.get(i).equals("") || areaIdArr.get(i) == null)
            {
                if(withoutId)
                {
                    insertQueryWithoutId += ", ";
                }
                withoutId = true;
                insertQueryWithoutId += "('"+areaNameArr.get(i)+"','"+areaNameArr.get(i)+"','"+cityIdArr.get(i)+"','Active','"+currentUser+"','"+currentDate+"','"+currentTime+"','"+currentUser+"','"+currentDate+"','"+currentTime+"')";
            }
            else
            {
                if(withId)
                {
                    insertQueryWithId += ", ";
                }
                withId = true;
                insertQueryWithId += "('"+areaIdArr.get(i)+"','"+areaNameArr.get(i)+"','"+areaNameArr.get(i)+"','"+cityIdArr.get(i)+"','Active','"+currentUser+"','"+currentDate+"','"+currentTime+"','"+currentUser+"','"+currentDate+"','"+currentTime+"')";
            }
        }
        int insertInfo = 1;
        if(withId)
        {
            if(stmt.executeUpdate(insertQueryWithId) == -1)
            {
                insertInfo = -1;
            }
        }
        if(withoutId)
        {
            if(stmt.executeUpdate(insertQueryWithoutId) == -1)
            {
                insertInfo = -1;
            }
        }

        if(insertInfo == -1)
        {
            String title = "Error";
            String message = "Unable to insert";
            GlobalVariables.showNotification(-1, title, message);
        }
        else
        {
            String title = "Successfull";
            String message = "Record Inserted";
            GlobalVariables.showNotification(1, title, message);
        }
        refreshAllArrays();
    }

    private void refreshAllArrays()
    {
        areaIdArr = new ArrayList<>();
        areaNameArr = new ArrayList<>();
        cityIdArr = new ArrayList<>();
        cityNameArr = new ArrayList<>();
    }

    public void updateArea(Statement stmt, Connection con, String areaTableId, String areaId, String areaName, String cityId, String areaStatus)
    {
        String currentDate = GlobalVariables.getStDate();
        String currentTime = GlobalVariables.getStTime();
        String currentUser = GlobalVariables.getUserId();
        String updateQuery;
        try {
            if(areaId.equals("") || areaId.equals("N/A"))
            {
                updateQuery = "UPDATE `area_info` SET `area_id`= NULL,`city_table_id`='"+cityId+"',`area_name`='"+areaName+"',`area_status`='"+areaStatus+"',`update_user_id`='"+currentUser+"',`update_date`='"+currentDate+"',`update_time`='"+currentTime+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `area_table_id` = '"+areaTableId+"'";
            }
            else
            {
                updateQuery = "UPDATE `area_info` SET `area_id`='"+areaId+"',`city_table_id`='"+cityId+"',`area_name`='"+areaName+"',`area_status`='"+areaStatus+"',`update_user_id`='"+currentUser+"',`update_date`='"+currentDate+"',`update_time`='"+currentTime+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `area_table_id` = '"+areaTableId+"'";
            }

            stmt.executeUpdate(updateQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<ArrayList<String>> getAddedArea()
    {
        ArrayList<ArrayList<String>> areaData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        for(int i=0; i<areaIdArr.size(); i++)
        {
            temp = new ArrayList<String>();
            temp.add(areaIdArr.get(i));
            temp.add(areaNameArr.get(i));
            temp.add(cityNameArr.get(i));
            areaData.add(temp);
        }
        return areaData;
    }

    String checkAreaId = new String();
    public String confirmNewId(String areaId)
    {
        ResultSet rs3 = null;
        try {
            rs3 = objStmt.executeQuery("SELECT `area_id` FROM `area_info` WHERE `area_id` = '"+areaId+"' AND `area_status` != 'Deleted'");
            while (rs3.next())
            {
                checkAreaId = rs3.getString(1) ;
            }
//            objCon.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return checkAreaId;
    }

    public void getCitiesInfo(Statement stmt, Connection con)
    {
        ArrayList<String> tempIds;
        ArrayList<String> tempNames;
        tempIds = new ArrayList<String>();
        tempNames = new ArrayList<String>();
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT `city_table_id`, `city_name` FROM `city_info` WHERE `city_status` != 'Deleted' ORDER BY `city_table_id`");
            while (rs.next())
            {
                tempIds.add(rs.getString(1));
                tempNames.add(rs.getString(2)+" ("+rs.getString(1)+")");
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        setSavedCityIds(tempIds);
        setSavedCityNames(tempNames);
    }

    public ArrayList<ArrayList<String>> getAreasWithIds(Statement stmt, Connection con)
    {
        ArrayList<ArrayList<String>> areaData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT `area_table_id`, `area_id`, `area_name`, `city_table_id` FROM `area_info` WHERE `area_status` != 'Deleted'");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Table id
                temp.add(rs.getString(2)); // Area id
                if(rs.getString(2) != null && !rs.getString(2).equals(""))
                {
                    temp.add(rs.getString(3)+" ("+rs.getString(2)+")"); // Area name
                }
                else
                {
                    temp.add(rs.getString(3)); // Area name
                }
                temp.add(rs.getString(4)); // City Id
                areaData.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return areaData;
    }
}
