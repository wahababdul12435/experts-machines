package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DashboardLiveInfo {

    public ArrayList<ArrayList<String>> getNearbyExpiryDetail(Statement stmt, Connection con, String fromDate, String toDate)
    {
        ArrayList<ArrayList<String>> itemsExpiryData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            String query = "SELECT `product_info`.`type_table_id`, `batchwise_stock`.`quantity`, `batchwise_stock`.`batch_expiry` FROM `product_info` INNER JOIN `batchwise_stock` ON `batchwise_stock`.`product_table_id` = `product_info`.`product_table_id` WHERE `product_info`.`product_status` != 'Deleted' AND str_to_date(`batchwise_stock`.`batch_expiry`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') AND str_to_date(`batchwise_stock`.`batch_expiry`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y') ORDER BY str_to_date(`batchwise_stock`.`batch_expiry`, '%d/%b/%Y') DESC";
//            System.out.println(query);
            rs = stmt.executeQuery(query);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Product Type
                temp.add(rs.getString(2)); // Quantity
                temp.add(rs.getString(3)); // Expiry Date
                itemsExpiryData.add(temp);
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return itemsExpiryData;
    }

    public ArrayList<ArrayList<String>> getTodayActiveUsersDetail(Statement stmt, Connection con)
    {
        ArrayList<ArrayList<String>> itemsExpiryData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            String query = "SELECT `user_info`.`user_table_id`, `user_info`.`user_name`, `user_info`.`user_type`, `user_info`.`user_image`, COUNT(`order_info`.`order_id`) as 'bookings', SUM(`order_info`.`final_price`) as 'booking_price', `user_accounts`.`current_status` FROM `user_info` INNER JOIN `order_info` ON `order_info`.`booking_user_id` = `user_info`.`user_table_id` AND `order_info`.`booking_date` = '"+GlobalVariables.getStDate()+"' AND `order_info`.`status` != 'Deleted' INNER JOIN `user_accounts` ON `user_accounts`.`user_id` = `user_info`.`user_table_id` WHERE `user_info`.`user_status` != 'Deleted' GROUP BY `user_info`.`user_table_id`";
            rs = stmt.executeQuery(query);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // User Table Id
                temp.add(rs.getString(2)); // Name
                temp.add(rs.getString(3)); // Type
                temp.add(rs.getString(4)); // Image
                temp.add(rs.getString(5)); // Booking
                temp.add(rs.getString(6)); // Booking Price
                temp.add(rs.getString(7)); // Online status
                itemsExpiryData.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return itemsExpiryData;
    }
}
