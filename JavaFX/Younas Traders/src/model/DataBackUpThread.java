package model;

import java.sql.SQLException;

public class DataBackUpThread extends Thread {
    int backupMins = 1;
    public void run()
    {
        try {
            Thread.sleep(15000);
            if(GlobalVariables.isConnectedToServer)
            {
                DataBackUp objDataBackUp = new DataBackUp();
                try {
                    objDataBackUp.compute();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            while(true)
            {
                Thread.sleep(backupMins*60*1000);
                if(GlobalVariables.isConnectedToServer)
                {
                    DataBackUp objDataBackUp = new DataBackUp();
                    try {
                        objDataBackUp.compute();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
