package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DashboardFinanceInfo {

    public ArrayList<ArrayList<String>> getDealerFinanceDetail(Statement stmt, Connection con, String fromDate, String toDate)
    {
        ArrayList<ArrayList<String>> dealersFinanceData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            String query = "SELECT `amount`, `date`, `cash_type` FROM `dealer_payments` WHERE str_to_date(`date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') AND str_to_date(`date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y') AND `status` != 'Deleted' ORDER BY str_to_date(`date`, '%d/%b/%Y') DESC";
            rs = stmt.executeQuery(query);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Collection
                temp.add(rs.getString(2)); // Date
                temp.add(rs.getString(3)); // Cash Type
                dealersFinanceData.add(temp);
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dealersFinanceData;
    }

    public ArrayList<ArrayList<String>> getDealerCollectionDetail(Statement stmt, Connection con, String fromDate, String toDate)
    {
        ArrayList<ArrayList<String>> dealersCollectionData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT COALESCE(SUM(`amount`),0), `dealer_info`.`dealer_name` FROM `dealer_payments` INNER JOIN `dealer_info` ON `dealer_info`.`dealer_table_id` = `dealer_payments`.`dealer_id` AND `dealer_info`.`dealer_status` != 'Deleted' WHERE `dealer_payments`.`cash_type` = 'Collection' AND `dealer_payments`.`status` != 'Deleted' AND str_to_date(`date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') AND str_to_date(`date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y') GROUP BY `dealer_info`.`dealer_table_id` LIMIT 5");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Collection
                temp.add(rs.getString(2)); // Dealer Name
                dealersCollectionData.add(temp);
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dealersCollectionData;
    }

    public ArrayList<ArrayList<String>> getCompanyFinanceDetail(Statement stmt, Connection con, String fromDate, String toDate)
    {
        ArrayList<ArrayList<String>> companyFinanceData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT `amount`, `date`, `cash_type` FROM `company_payments` WHERE str_to_date(`date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') AND str_to_date(`date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y') AND `status` != 'Deleted' ORDER BY str_to_date(`date`, '%d/%b/%Y') DESC");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Amount
                temp.add(rs.getString(2)); // Date
                temp.add(rs.getString(3)); // Cash Type
                companyFinanceData.add(temp);
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return companyFinanceData;
    }

    public ArrayList<ArrayList<String>> getCompanySentDetail(Statement stmt, Connection con, String fromDate, String toDate)
    {
        ArrayList<ArrayList<String>> companySentData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT COALESCE(SUM(`amount`),0), `company_info`.`company_name` FROM `company_payments` INNER JOIN `company_info` ON `company_info`.`company_table_id` = `company_payments`.`company_id` AND `company_info`.`company_status` != 'Deleted' WHERE `company_payments`.`cash_type` = 'Sent' AND `company_payments`.`status` != 'Deleted' AND str_to_date(`date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') AND str_to_date(`date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y') GROUP BY `company_info`.`company_table_id` LIMIT 5");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Sent
                temp.add(rs.getString(2)); // Company Name
                companySentData.add(temp);
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return companySentData;
    }
}
