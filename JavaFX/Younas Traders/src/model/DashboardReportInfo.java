package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DashboardReportInfo {

    public ArrayList<ArrayList<String>> getTopDealerDetail(Statement stmt, Connection con, String fromDate, String toDate)
    {
        ArrayList<ArrayList<String>> topDealers = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            String query = "SELECT `order_info`.`dealer_id`, `dealer_info`.`dealer_name`, COUNT(`order_info`.`order_id`) as 'bookings', ROUND(SUM(`order_info`.`final_price`),2) as 'booking_price', `dealer_info`.`dealer_image` FROM `order_info` INNER JOIN `dealer_info` ON `dealer_info`.`dealer_table_id` = `order_info`.`dealer_id` WHERE str_to_date(`order_info`.`booking_date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') AND str_to_date(`order_info`.`booking_date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y') AND `order_info`.`status` != 'Deleted' AND `order_info`.`status` != 'Returned' GROUP BY `order_info`.`dealer_id` ORDER BY booking_price DESC LIMIT 20";
//            System.out.println(query);
            rs = stmt.executeQuery(query);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Dealer Id
                temp.add(rs.getString(2)); // Dealer Name
                temp.add(rs.getString(3)); // Bookings
                temp.add(rs.getString(4)); // Bookings Price
                temp.add(rs.getString(5)); // Image
                topDealers.add(temp);
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return topDealers;
    }

    public ArrayList<ArrayList<String>> getTopUsersDetail(Statement stmt, Connection con, String fromDate, String toDate)
    {
        ArrayList<ArrayList<String>> topUsers = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            String query = "SELECT `order_info`.`booking_user_id`, `user_info`.`user_name`, COUNT(`order_info`.`order_id`) as 'bookings', ROUND(SUM(`order_info`.`final_price`),2) as 'booking_price', `user_info`.`user_image` FROM `order_info` INNER JOIN `user_info` ON `user_info`.`user_table_id` = `order_info`.`booking_user_id` WHERE str_to_date(`order_info`.`booking_date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') AND str_to_date(`order_info`.`booking_date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y') AND `order_info`.`status` != 'Deleted' AND `order_info`.`status` != 'Returned' GROUP BY `order_info`.`booking_user_id` ORDER BY booking_price DESC LIMIT 20";
//            System.out.println(query);
            rs = stmt.executeQuery(query);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // User Id
                temp.add(rs.getString(2)); // User Name
                temp.add(rs.getString(3)); // Bookings
                temp.add(rs.getString(4)); // Bookings Price
                temp.add(rs.getString(5)); // Image
                topUsers.add(temp);
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return topUsers;
    }

    public ArrayList<ArrayList<String>> getCashInOutDetail(Statement stmt, Connection con, String fromDate, String toDate)
    {
        ArrayList<ArrayList<String>> cashInOut = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            String query = "SELECT (SELECT COALESCE(SUM(`dealer_payments`.`amount`),0) FROM `dealer_payments` WHERE `cash_type` = 'Collection' AND str_to_date(`date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') AND str_to_date(`date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y')) + (SELECT COALESCE(SUM(`company_payments`.`amount`),0) FROM `company_payments` WHERE `cash_type` = 'Received' AND str_to_date(`date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') AND str_to_date(`date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y')) as 'cash_in', (SELECT COALESCE(SUM(`dealer_payments`.`amount`),0) FROM `dealer_payments` WHERE `cash_type` = 'Return' AND str_to_date(`date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') AND str_to_date(`date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y')) + (SELECT COALESCE(SUM(`company_payments`.`amount`),0) FROM `company_payments` WHERE `cash_type` = 'Sent' AND str_to_date(`date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') AND str_to_date(`date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y')) as 'cash_out' FROM `dealer_payments` LIMIT 1";
//            System.out.println(query);
            rs = stmt.executeQuery(query);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                if(rs.getString(1) != null && !rs.getString(1).equals(""))
                {
                    temp.add("Cash In (Rs."+String.format("%,.0f", Float.parseFloat(rs.getString(1)))+")"); // Cash In
                    temp.add(rs.getString(1)); // Cash In
                }
                else
                {
                    temp.add("Cash In (Rs. 0)"); // Cash In
                    temp.add("0"); // Cash In
                }

                cashInOut.add(temp);
                temp = new ArrayList<String>();
                if(rs.getString(2) != null && !rs.getString(2).equals(""))
                {
                    temp.add("Cash Out (Rs."+String.format("%,.0f", Float.parseFloat(rs.getString(2)))+")"); // Cash Out
                    temp.add(rs.getString(2)); // Cash Out
                }
                else
                {
                    temp.add("Cash Out (Rs. 0)"); // Cash Out
                    temp.add("0"); // Cash Out
                }

                cashInOut.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cashInOut;
    }

    public ArrayList<ArrayList<String>> getCityBookingDetail(Statement stmt, Connection con, String fromDate, String toDate)
    {
        ArrayList<ArrayList<String>> cityBooking = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            String query = "SELECT `city_info`.`city_name`, ROUND(SUM(`order_info`.`final_price`),0) as 'city_booking' FROM `order_info` INNER JOIN `dealer_info` ON `dealer_info`.`dealer_table_id` = `order_info`.`dealer_id` INNER JOIN `area_info` ON `area_info`.`area_table_id` = `dealer_info`.`dealer_area_id` INNER JOIN `city_info` ON `city_info`.`city_table_id` = `area_info`.`city_table_id` WHERE str_to_date(`order_info`.`booking_date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') AND str_to_date(`order_info`.`booking_date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y') AND `order_info`.`status` != 'Deleted' AND `order_info`.`status` != 'Returned' GROUP BY `city_info`.`city_table_id` LIMIT 6";
//            System.out.println(query);
            rs = stmt.executeQuery(query);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // City Name
                temp.add(rs.getString(2)); // City Booking
                cityBooking.add(temp);
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cityBooking;
    }
}
