package model;

import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.HBox;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class DealerGeneralReportInfo {
    private String srNo;
    private String dealerTableId;
    private String dealerId;
    private String dealerName;
    private String areaName;
    private String dealerType;
    private String booked;
    private String delivered;
    private String returned;
    private String bookingPrice;
    private String discountGiven;
    private String itemsReturned;
    private String returnPrice;
    private String cashCollection;
    private String cashReturn;
    private String cashWaiveOff;
    private String cashPending;
    private String dealerStatus;

    private HBox operationsPane;
    private JFXButton btnView;
    private JFXButton btnEdit;

    public static String dealerReportId;

    public DealerGeneralReportInfo() {
    }

    public DealerGeneralReportInfo(String srNo, String dealerTableId, String dealerId, String dealerName, String areaName, String dealerType, String booked, String delivered, String returned, String bookingPrice, String discountGiven, String itemsReturned, String returnPrice, String cashCollection, String cashReturn, String cashWaiveOff, String cashPending, String dealerStatus) {
        this.srNo = srNo;
        this.dealerTableId = dealerTableId;
        this.dealerId = dealerId;
        this.dealerName = dealerName;
        this.areaName = areaName;
        this.dealerType = dealerType;
        this.booked = String.format("%,.0f", Float.parseFloat(booked));
        this.delivered = String.format("%,.0f", Float.parseFloat(delivered));
        this.returned = String.format("%,.0f", Float.parseFloat(returned));
        this.bookingPrice = String.format("%,.0f", Float.parseFloat(bookingPrice));
        this.discountGiven = String.format("%,.0f", Float.parseFloat(discountGiven));
        this.itemsReturned = String.format("%,.0f", Float.parseFloat(itemsReturned));
        this.returnPrice = String.format("%,.0f", Float.parseFloat(returnPrice));
        this.cashCollection = String.format("%,.0f", Float.parseFloat(cashCollection));
        this.cashReturn = String.format("%,.0f", Float.parseFloat(cashReturn));
        this.cashWaiveOff = String.format("%,.0f", Float.parseFloat(cashWaiveOff));
        this.cashPending = String.format("%,.0f", Float.parseFloat(cashPending));
        this.dealerStatus = dealerStatus;

        this.btnView = new JFXButton();
        this.btnView.setGraphic(GlobalVariables.createViewIcon());
        this.btnView.getStyleClass().add("btn_icon");
        this.btnEdit = new JFXButton();
        this.btnEdit.setGraphic(GlobalVariables.createEditIcon());
        this.btnEdit.getStyleClass().add("btn_icon");
        this.operationsPane = new HBox(btnView);

        this.btnView.setOnAction((event)->viewDealerDetail());
    }

    public void viewDealerDetail()
    {
        Parent parent = null;
        try {
            dealerReportId = dealerTableId;
            parent = FXMLLoader.load(getClass().getResource("/view/dealer_report_detail.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        GlobalVariables.baseScene.setRoot(parent);
        GlobalVariables.baseStage.setScene(GlobalVariables.baseScene);
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getDealerTableId() {
        return dealerTableId;
    }

    public void setDealerTableId(String dealerTableId) {
        this.dealerTableId = dealerTableId;
    }

    public String getDealerId() {
        return dealerId;
    }

    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getDealerType() {
        return dealerType;
    }

    public void setDealerType(String dealerType) {
        this.dealerType = dealerType;
    }

    public String getBooked() {
        return booked;
    }

    public void setBooked(String booked) {
        this.booked = booked;
    }

    public String getDelivered() {
        return delivered;
    }

    public void setDelivered(String delivered) {
        this.delivered = delivered;
    }

    public String getReturned() {
        return returned;
    }

    public void setReturned(String returned) {
        this.returned = returned;
    }

    public String getBookingPrice() {
        return bookingPrice;
    }

    public void setBookingPrice(String bookingPrice) {
        this.bookingPrice = bookingPrice;
    }

    public String getDiscountGiven() {
        return discountGiven;
    }

    public void setDiscountGiven(String discountGiven) {
        this.discountGiven = discountGiven;
    }

    public String getItemsReturned() {
        return itemsReturned;
    }

    public void setItemsReturned(String itemsReturned) {
        this.itemsReturned = itemsReturned;
    }

    public String getReturnPrice() {
        return returnPrice;
    }

    public void setReturnPrice(String returnPrice) {
        this.returnPrice = returnPrice;
    }

    public String getCashCollection() {
        return cashCollection;
    }

    public void setCashCollection(String cashCollection) {
        this.cashCollection = cashCollection;
    }

    public String getCashReturn() {
        return cashReturn;
    }

    public void setCashReturn(String cashReturn) {
        this.cashReturn = cashReturn;
    }

    public String getCashWaiveOff() {
        return cashWaiveOff;
    }

    public void setCashWaiveOff(String cashWaiveOff) {
        this.cashWaiveOff = cashWaiveOff;
    }

    public String getCashPending() {
        return cashPending;
    }

    public void setCashPending(String cashPending) {
        this.cashPending = cashPending;
    }

    public String getDealerStatus() {
        return dealerStatus;
    }

    public void setDealerStatus(String dealerStatus) {
        this.dealerStatus = dealerStatus;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public ArrayList<ArrayList<String>> getDealersReportInfo(Statement stmt1, Statement stmt2, Statement stmt3, Connection con, String fromDate, String toDate)
    {
        ArrayList<ArrayList<String>> dealerReportData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs1 = null;
        ResultSet rs2 = null;
        ResultSet rs3 = null;
        try {
            rs1 = stmt1.executeQuery("SELECT `dealer_info`.`dealer_table_id`, `dealer_info`.`dealer_id`, `dealer_info`.`dealer_name`, `area_info`.`area_name`, `dealer_info`.`dealer_type`, SUM(case when `order_info`.`dealer_id` = `dealer_info`.`dealer_table_id` AND `order_info`.`status` != 'Deleted' then 1 else 0 end) as 'total_booked', SUM(case when `order_info`.`dealer_id` = `dealer_info`.`dealer_table_id` AND `order_info`.`status` = 'Delivered' then 1 else 0 end) as 'total_received', SUM(case when `order_info`.`dealer_id` = `dealer_info`.`dealer_table_id` AND `order_info`.`status` = 'Returned' then 1 else 0 end) as 'total_returned', ROUND(SUM(case when `order_info`.`dealer_id` = `dealer_info`.`dealer_table_id` AND `order_info`.`status` != 'Deleted' then `order_info`.`final_price` else 0 end),2) as 'booking_price', ROUND(SUM(case when `order_info`.`dealer_id` = `dealer_info`.`dealer_table_id` AND `order_info`.`status` != 'Deleted' then `order_info`.`discount`+`order_info`.`waive_off_price` else 0 end),2) as 'discount_given' FROM `dealer_info` LEFT OUTER JOIN `area_info` ON `area_info`.`area_table_id` = `dealer_info`.`dealer_area_id` LEFT OUTER JOIN `order_info` ON `order_info`.`dealer_id` = `dealer_info`.`dealer_table_id` AND str_to_date(`order_info`.`booking_date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') AND str_to_date(`order_info`.`booking_date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y') WHERE `dealer_info`.`dealer_status` != 'Deleted' GROUP BY `dealer_info`.`dealer_table_id`");
            rs2 = stmt2.executeQuery("SELECT `dealer_info`.`dealer_table_id`, SUM(`order_return_detail`.`prod_quant`) as 'items_returned', ROUND((SELECT COALESCE(SUM(`order_return`.`return_total_price`),0) FROM `order_return` WHERE `order_return`.`dealer_id` = `dealer_info`.`dealer_table_id` AND `order_return`.`status` != 'Deleted' AND str_to_date(`order_return`.`entry_date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') AND str_to_date(`order_return`.`entry_date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y')), 2) as 'return_price' FROM `dealer_info` LEFT OUTER JOIN `order_return` ON `order_return`.`dealer_id` = `dealer_info`.`dealer_table_id` AND `order_return`.`status` != 'Deleted' LEFT OUTER JOIN `order_return_detail` ON `order_return_detail`.`return_id` = `order_return`.`return_id` AND `order_return`.`status` != 'Deleted' AND str_to_date(`order_return`.`entry_date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') AND str_to_date(`order_return`.`entry_date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y') WHERE `dealer_info`.`dealer_status` != 'Deleted' GROUP BY `dealer_info`.`dealer_table_id`");
            rs3 = stmt3.executeQuery("SELECT `dealer_info`.`dealer_table_id`, ROUND((SELECT COALESCE(SUM(`amount`),0) FROM `dealer_payments` WHERE `cash_type` = 'Collection' AND `dealer_payments`.`status` != 'Deleted' AND `dealer_payments`.`dealer_id` = `dealer_info`.`dealer_table_id` AND str_to_date(`dealer_payments`.`date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') AND str_to_date(`dealer_payments`.`date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y')), 2) as 'cash_collection', ROUND((SELECT COALESCE(SUM(`amount`),0) FROM `dealer_payments` WHERE `cash_type` = 'Return' AND `dealer_payments`.`status` != 'Deleted' AND `dealer_payments`.`dealer_id` = `dealer_info`.`dealer_table_id` AND str_to_date(`dealer_payments`.`date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') AND str_to_date(`dealer_payments`.`date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y')), 2) as 'cash_return', ROUND((SELECT COALESCE(SUM(`amount`),0) FROM `dealer_payments` WHERE `cash_type` = 'Waived Off' AND `dealer_payments`.`status` != 'Deleted' AND `dealer_payments`.`dealer_id` = `dealer_info`.`dealer_table_id` AND str_to_date(`dealer_payments`.`date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') AND str_to_date(`dealer_payments`.`date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y')), 2) as 'cash_waivedoff', (SELECT `pending_payments` FROM `dealer_payments` WHERE `dealer_payments`.`status` != 'Deleted' AND `dealer_payments`.`dealer_id` = `dealer_info`.`dealer_table_id` AND str_to_date(`dealer_payments`.`date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') AND str_to_date(`dealer_payments`.`date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y') ORDER BY `payment_id` DESC LIMIT 1) as 'pending_payments', `dealer_info`.`dealer_status` FROM `dealer_info` WHERE `dealer_info`.`dealer_status` != 'Deleted' GROUP BY `dealer_info`.`dealer_table_id`");
            while (rs1.next() && rs2.next() && rs3.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs1.getString(1)); // Table Id
                temp.add(rs1.getString(2)); // Id
                temp.add(rs1.getString(3)); // Name
                temp.add(rs1.getString(4)); // Area
                temp.add(rs1.getString(5)); // Type
                temp.add(rs1.getString(6) == null || rs1.getString(6).equals("") ? "0" : rs1.getString(6)); // Booked
                temp.add(rs1.getString(7) == null || rs1.getString(7).equals("") ? "0" : rs1.getString(7)); // Delivered
                temp.add(rs1.getString(8) == null || rs1.getString(8).equals("") ? "0" : rs1.getString(8)); // Returned
                temp.add(rs1.getString(9) == null || rs1.getString(9).equals("") ? "0" : rs1.getString(9)); // Booking Price
                temp.add(rs1.getString(10) == null || rs1.getString(10).equals("") ? "0" : rs1.getString(10)); // Discount Given
                temp.add(rs2.getString(2) == null || rs2.getString(2).equals("") ? "0" : rs2.getString(2)); // Items Returned
                temp.add(rs2.getString(3) == null || rs2.getString(3).equals("") ? "0" : rs2.getString(3)); // Return Price
                temp.add(rs3.getString(2) == null || rs3.getString(2).equals("") ? "0" : rs3.getString(2)); // Cash Collection
                temp.add(rs3.getString(3) == null || rs3.getString(3).equals("") ? "0" : rs3.getString(3)); // Cash Returned
                temp.add(rs3.getString(4) == null || rs3.getString(4).equals("") ? "0" : rs3.getString(4)); // Cash Waived Off
                temp.add(rs3.getString(5) == null || rs3.getString(5).equals("") ? "0" : rs3.getString(5)); // Pending Payments
                temp.add(rs3.getString(6)); // Dealer Status
                dealerReportData.add(temp);
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dealerReportData;
    }

    public ArrayList<ArrayList<String>> getDealersReportSearch(Statement stmt1, Statement stmt2, Statement stmt3, Connection con, String dealerId, String dealerName, String dealerContact, String dealerType, String districtId, String cityId, String areaId, String fromDate, String toDate)
    {
        ArrayList<ArrayList<String>> dealerReportData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs1 = null;
        ResultSet rs2 = null;
        ResultSet rs3 = null;
        String searchQuery1;
        String searchQuery2;
        String searchQuery3;
        int multipleSearch = 0;
        searchQuery1 = "SELECT `dealer_info`.`dealer_table_id`, `dealer_info`.`dealer_id`, `dealer_info`.`dealer_name`, `area_info`.`area_name`, `dealer_info`.`dealer_type`, SUM(case when `order_info`.`dealer_id` = `dealer_info`.`dealer_table_id` AND `order_info`.`status` != 'Deleted' then 1 else 0 end) as 'total_booked', SUM(case when `order_info`.`dealer_id` = `dealer_info`.`dealer_table_id` AND `order_info`.`status` = 'Delivered' then 1 else 0 end) as 'total_received', SUM(case when `order_info`.`dealer_id` = `dealer_info`.`dealer_table_id` AND `order_info`.`status` = 'Returned' then 1 else 0 end) as 'total_returned', ROUND(SUM(case when `order_info`.`dealer_id` = `dealer_info`.`dealer_table_id` AND `order_info`.`status` != 'Deleted' then `order_info`.`final_price` else 0 end),2) as 'booking_price', ROUND(SUM(case when `order_info`.`dealer_id` = `dealer_info`.`dealer_table_id` AND `order_info`.`status` != 'Deleted' then `order_info`.`discount`+`order_info`.`waive_off_price` else 0 end),2) as 'discount_given' FROM `dealer_info` LEFT OUTER JOIN `area_info` ON `area_info`.`area_table_id` = `dealer_info`.`dealer_area_id` LEFT OUTER JOIN `order_info` ON `order_info`.`dealer_id` = `dealer_info`.`dealer_table_id` ";
        searchQuery2 = "SELECT `dealer_info`.`dealer_table_id`, SUM(`order_return_detail`.`prod_quant`) as 'items_returned', ROUND((SELECT COALESCE(SUM(`order_return`.`return_total_price`),0) FROM `order_return` WHERE `order_return`.`dealer_id` = `dealer_info`.`dealer_table_id` AND `order_return`.`status` != 'Deleted' ";
        searchQuery3 = "SELECT `dealer_info`.`dealer_table_id`, ROUND((SELECT COALESCE(SUM(`amount`),0) FROM `dealer_payments` WHERE `cash_type` = 'Collection' AND `dealer_payments`.`status` != 'Deleted' AND `dealer_payments`.`dealer_id` = `dealer_info`.`dealer_table_id` ";
        if(!fromDate.equals(""))
        {
            searchQuery1 += "AND str_to_date(`booking_date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') ";
            searchQuery2 += "AND str_to_date(`order_return`.`entry_date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') ";
            searchQuery3 += "AND str_to_date(`dealer_payments`.`date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y')";
        }
        if(!toDate.equals(""))
        {
            searchQuery1 += "AND str_to_date(`booking_date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y') ";
            searchQuery2 += " AND str_to_date(`order_return`.`entry_date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y') ";
            searchQuery3 += "AND str_to_date(`dealer_payments`.`date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y') ";
        }
        if(!cityId.equals("All"))
        {
            searchQuery1 += "INNER JOIN `city_info` ON `city_info`.`city_table_id` = `area_info`.`city_table_id` AND `city_info`.`city_table_id` = '"+cityId+"' ";
        }
        else if(!districtId.equals("All"))
        {
            searchQuery1 += "LEFT OUTER JOIN `city_info` ON `city_info`.`city_table_id` = `area_info`.`city_table_id` ";
        }
        if(!districtId.equals("All"))
        {
            searchQuery1 += "INNER JOIN `district_info` ON `district_info`.`district_table_id` = `city_info`.`district_table_id` AND `district_info`.`district_table_id` = '"+districtId+"' ";
        }
        searchQuery2 += "), 2) as 'return_price' FROM `dealer_info` LEFT OUTER JOIN `order_return` ON `order_return`.`dealer_id` = `dealer_info`.`dealer_table_id` LEFT OUTER JOIN `order_return_detail` ON `order_return_detail`.`return_id` = `order_return`.`return_id` ";
        searchQuery3 += "), 2) as 'cash_collection', ROUND((SELECT COALESCE(SUM(`amount`),0) FROM `dealer_payments` WHERE `cash_type` = 'Return' AND `dealer_payments`.`status` != 'Deleted' AND `dealer_payments`.`dealer_id` = `dealer_info`.`dealer_table_id` ";
        if(!fromDate.equals(""))
        {
            searchQuery2 += "AND str_to_date(`order_return`.`entry_date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') ";
            searchQuery3 += "AND str_to_date(`dealer_payments`.`date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y')";
        }
        if(!toDate.equals(""))
        {
            searchQuery2 += "AND str_to_date(`order_return`.`entry_date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y') ";
            searchQuery3 += "AND str_to_date(`dealer_payments`.`date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y')";
        }
        searchQuery3 += "), 2) as 'cash_return', ROUND((SELECT COALESCE(SUM(`amount`),0) FROM `dealer_payments` WHERE `cash_type` = 'Waived Off' AND `dealer_payments`.`status` != 'Deleted' AND `dealer_payments`.`dealer_id` = `dealer_info`.`dealer_table_id` ";
        if(!fromDate.equals(""))
        {
            searchQuery3 += "AND str_to_date(`dealer_payments`.`date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') ";
        }
        if(!toDate.equals(""))
        {
            searchQuery3 += "AND str_to_date(`dealer_payments`.`date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y')";
        }
        searchQuery3 += "), 2) as 'cash_waivedoff', (SELECT `pending_payments` FROM `dealer_payments` WHERE `dealer_payments`.`status` != 'Deleted' AND `dealer_payments`.`dealer_id` = `dealer_info`.`dealer_table_id` ";
        if(!fromDate.equals(""))
        {
            searchQuery3 += "AND str_to_date(`dealer_payments`.`date`, '%d/%b/%Y') >= str_to_date('"+fromDate+"', '%d/%b/%Y') ";
        }
        if(!toDate.equals(""))
        {
            searchQuery3 += "AND str_to_date(`dealer_payments`.`date`, '%d/%b/%Y') <= str_to_date('"+toDate+"', '%d/%b/%Y') ";
        }
        searchQuery3 += "ORDER BY `payment_id` DESC LIMIT 1) as 'pending_payments', `dealer_info`.`dealer_status` FROM `dealer_info` ";

        if(!cityId.equals("All"))
        {
            searchQuery2 += "LEFT OUTER JOIN `area_info` ON `area_info`.`area_table_id` = `dealer_info`.`dealer_area_id` INNER JOIN `city_info` ON `city_info`.`city_table_id` = `area_info`.`city_table_id` AND `city_info`.`city_table_id` = '"+cityId+"' ";
            searchQuery3 += "LEFT OUTER JOIN `area_info` ON `area_info`.`area_table_id` = `dealer_info`.`dealer_area_id` INNER JOIN `city_info` ON `city_info`.`city_table_id` = `area_info`.`city_table_id` AND `city_info`.`city_table_id` = '"+cityId+"' ";
        }
        else if(!districtId.equals("All"))
        {
            searchQuery2 += "LEFT OUTER JOIN `area_info` ON `area_info`.`area_table_id` = `dealer_info`.`dealer_area_id` LEFT OUTER JOIN `city_info` ON `city_info`.`city_table_id` = `area_info`.`city_table_id` ";
            searchQuery3 += "LEFT OUTER JOIN `area_info` ON `area_info`.`area_table_id` = `dealer_info`.`dealer_area_id` LEFT OUTER JOIN `city_info` ON `city_info`.`city_table_id` = `area_info`.`city_table_id` ";
        }
        else
        {
            searchQuery2 += "LEFT OUTER JOIN `area_info` ON `area_info`.`area_table_id` = `dealer_info`.`dealer_area_id` ";
            searchQuery3 += "LEFT OUTER JOIN `area_info` ON `area_info`.`area_table_id` = `dealer_info`.`dealer_area_id` ";
        }
        if(!districtId.equals("All"))
        {
            searchQuery2 += "INNER JOIN `district_info` ON `district_info`.`district_table_id` = `city_info`.`district_table_id` AND `district_info`.`district_table_id` = '"+districtId+"' ";
            searchQuery3 += "INNER JOIN `district_info` ON `district_info`.`district_table_id` = `city_info`.`district_table_id` AND `district_info`.`district_table_id` = '"+districtId+"' ";
        }
        searchQuery1 += " WHERE ";
        searchQuery2 += " WHERE ";
        searchQuery3 += " WHERE ";

        if(!dealerId.equals(""))
        {
            searchQuery1 += "`dealer_info`.`dealer_id` = '"+dealerId+"' AND ";
            searchQuery2 += "`dealer_info`.`dealer_id` = '"+dealerId+"' AND ";
            searchQuery3 += "`dealer_info`.`dealer_id` = '"+dealerId+"' AND ";
        }
        if(!dealerName.equals(""))
        {
            searchQuery1 += "`dealer_info`.`dealer_name` LIKE %'"+dealerName+"'% AND ";
            searchQuery2 += "`dealer_info`.`dealer_name` LIKE %'"+dealerName+"'% AND ";
            searchQuery3 += "`dealer_info`.`dealer_name` LIKE %'"+dealerName+"'% AND ";
        }
        if(!dealerContact.equals(""))
        {
            searchQuery1 += "`dealer_info`.`dealer_phone` = '"+dealerContact+"' AND ";
            searchQuery2 += "`dealer_info`.`dealer_phone` = '"+dealerContact+"' AND ";
            searchQuery3 += "`dealer_info`.`dealer_phone` = '"+dealerContact+"' AND ";
        }
        if(!dealerType.equals("All"))
        {
            searchQuery1 += "`dealer_info`.`dealer_type` = '"+dealerType+"' AND ";
            searchQuery2 += "`dealer_info`.`dealer_type` = '"+dealerType+"' AND ";
            searchQuery3 += "`dealer_info`.`dealer_type` = '"+dealerType+"' AND ";
        }
        if(!areaId.equals("All"))
        {
            searchQuery1 += "`dealer_info`.`dealer_area_id` = '"+areaId+"' AND ";
            searchQuery2 += "`dealer_info`.`dealer_area_id` = '"+areaId+"' AND ";
            searchQuery3 += "`dealer_info`.`dealer_area_id` = '"+areaId+"' AND ";
        }
//        if(!areaName.equals(""))
//        {
//            searchQuery1 += "`dealer_info`.`dealer_id` = "+areaName+" AND "; // District Name Dropdown, City Name Drop Down, Area Name Drop Down
//        }
        searchQuery1 += "`dealer_info`.`dealer_status` != 'Deleted' GROUP BY `dealer_info`.`dealer_table_id`";
        searchQuery2 += "`dealer_info`.`dealer_status` != 'Deleted' GROUP BY `dealer_info`.`dealer_table_id`";
        searchQuery3 += "`dealer_info`.`dealer_status` != 'Deleted' GROUP BY `dealer_info`.`dealer_table_id`";
//        System.out.println(searchQuery1);
//        System.out.println(searchQuery2);
//        System.out.println(searchQuery3);
        try {
            rs1 = stmt1.executeQuery(searchQuery1);
            rs2 = stmt2.executeQuery(searchQuery2);
            rs3 = stmt3.executeQuery(searchQuery3);
            while (rs1.next() && rs2.next() && rs3.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs1.getString(1)); // Table Id
                temp.add(rs1.getString(2)); // Id
                temp.add(rs1.getString(3)); // Name
                temp.add(rs1.getString(4)); // Area
                temp.add(rs1.getString(5)); // Type
                temp.add(rs1.getString(6) == null || rs1.getString(6).equals("") ? "0" : rs1.getString(6)); // Booked
                temp.add(rs1.getString(7) == null || rs1.getString(7).equals("") ? "0" : rs1.getString(7)); // Delivered
                temp.add(rs1.getString(8) == null || rs1.getString(8).equals("") ? "0" : rs1.getString(8)); // Returned
                temp.add(rs1.getString(9) == null || rs1.getString(9).equals("") ? "0" : rs1.getString(9)); // Booking Price
                temp.add(rs1.getString(10) == null || rs1.getString(10).equals("") ? "0" : rs1.getString(10)); // Discount Given
                temp.add(rs2.getString(2) == null || rs2.getString(2).equals("") ? "0" : rs2.getString(2)); // Items Returned
                temp.add(rs2.getString(3) == null || rs2.getString(3).equals("") ? "0" : rs2.getString(3)); // Return Price
                temp.add(rs3.getString(2) == null || rs3.getString(2).equals("") ? "0" : rs3.getString(2)); // Cash Collection
                temp.add(rs3.getString(3) == null || rs3.getString(3).equals("") ? "0" : rs3.getString(3)); // Cash Returned
                temp.add(rs3.getString(4) == null || rs3.getString(4).equals("") ? "0" : rs3.getString(4)); // Cash Waived Off
                temp.add(rs3.getString(5) == null || rs3.getString(5).equals("") ? "0" : rs3.getString(5)); // Pending Payments
                temp.add(rs3.getString(6)); // Dealer Status
                dealerReportData.add(temp);
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dealerReportData;
    }
}
