package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class SystemSettingsInfo {

    public SystemSettingsInfo()
    {

    }

    public String getBatchPolicy(Statement stmt, Connection con)
    {
        String batchPolicy = "";
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT `setting_value` FROM `system_settings` WHERE `setting_name` = 'batch_policy'");
            rs.next();
            batchPolicy = rs.getString(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return batchPolicy;
    }

    public void updateBatchPolicy(Statement stmt, Connection con, String batchPolicy)
    {
        String updateQuery;
        try {
            updateQuery = "UPDATE `system_settings` SET `setting_value`= '"+batchPolicy+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `setting_name` = 'batch_policy'";
            stmt.executeUpdate(updateQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
