package model;

import javafx.application.Platform;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class FetchServerThread extends Thread {
    FetchServerData objFetchServerData;
    String newBookingsNo;
    String newDealerFinanceNo;
    String newReturnsNo;
    String newUserAccountsNo;
    ArrayList<ArrayList<String>> newBookings;
    ArrayList<ArrayList<String>> mobileGPSLocations;
    ArrayList<ArrayList<String>> orderGPSLocations;
    GPSLocations objGPSLocations = new GPSLocations();
    int intervalSeconds = 60;
    public void run()
    {
        while (true)
        {
            try
            {
                Thread.sleep(1000);
                newBookingsNo = getNewOrdersNo();
                newDealerFinanceNo = getDealerFinanceNo();
                newReturnsNo = getNewReturnsNo();
                newUserAccountsNo = getNewUserAccountsNo();

                int totalBookingReturn = 0;
                if(newBookingsNo != null && Integer.parseInt(newBookingsNo) > 0)
                {
                    GlobalVariables.findNewBookings = Integer.parseInt(newBookingsNo);
                    totalBookingReturn = GlobalVariables.findNewBookings + GlobalVariables.findNewReturns;
                }
                else
                {
                    GlobalVariables.findNewBookings = 0;
                }

                if(newReturnsNo != null && Integer.parseInt(newReturnsNo) > 0)
                {
                    GlobalVariables.findNewReturns = Integer.parseInt(newReturnsNo);
                    totalBookingReturn = GlobalVariables.findNewBookings + GlobalVariables.findNewReturns;
                }
                else
                {
                    GlobalVariables.findNewReturns = 0;
                }

                int finalTotalBookingReturn = totalBookingReturn;
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        // do your GUI stuff here
                        if(finalTotalBookingReturn > 0)
                        {
                            GlobalVariables.btnTopHeaderSale.setText("Sales ("+ finalTotalBookingReturn +")");
                        }
                        else
                        {
                            GlobalVariables.btnTopHeaderSale.setText("Sales");
                        }

                        if(GlobalVariables.btnNavSaleOrder != null)
                        {
                            if(GlobalVariables.findNewBookings > 0)
                            {
                                GlobalVariables.btnNavSaleOrder.setText("View Sales ("+GlobalVariables.findNewBookings+")");
                            }
                            else
                            {
                                GlobalVariables.btnNavSaleOrder.setText("View Sales");
                            }
                        }

                        if(GlobalVariables.btnNavSaleReturn != null)
                        {
                            if(GlobalVariables.findNewReturns > 0)
                            {
                                GlobalVariables.btnNavSaleReturn.setText("View Returns ("+GlobalVariables.findNewReturns+")");
                            }
                            else
                            {
                                GlobalVariables.btnNavSaleReturn.setText("View Returns");
                            }
                        }

                    }
                });

                if(newDealerFinanceNo != null && Integer.parseInt(newDealerFinanceNo) > 0)
                {
                    GlobalVariables.findNewDealerFinance = Integer.parseInt(newDealerFinanceNo);
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            // do your GUI stuff here
                            GlobalVariables.btnTopHeaderFinance.setText("Finance ("+newDealerFinanceNo+")");
                        }
                    });
                }
                else
                {
                    GlobalVariables.findNewDealerFinance = 0;
                    Platform.runLater(new Runnable(){
                        @Override
                        public void run() {
                            // do your GUI stuff here
                            GlobalVariables.btnTopHeaderFinance.setText("Finance");
                        }
                    });
                }

                if(newUserAccountsNo != null && Integer.parseInt(newUserAccountsNo) > 0)
                {
                    GlobalVariables.findNewUserAccounts = Integer.parseInt(newUserAccountsNo);
                    Platform.runLater(new Runnable(){
                        @Override
                        public void run() {
                            // do your GUI stuff here
                            GlobalVariables.btnTopHeaderSettings.setText("Settings ("+newUserAccountsNo+")");
                            if(GlobalVariables.btnNavUserAccounts != null)
                            {
                                GlobalVariables.btnNavUserAccounts.setText("Users Account ("+newUserAccountsNo+")");
                            }
                        }
                    });
                }
                else
                {
                    GlobalVariables.findNewUserAccounts = 0;
                    Platform.runLater(new Runnable(){
                        @Override
                        public void run() {
                            // do your GUI stuff here
                            GlobalVariables.btnTopHeaderSettings.setText("Settings");
                            if(GlobalVariables.btnNavUserAccounts != null)
                            {
                                GlobalVariables.btnNavUserAccounts.setText("Users Account");
                            }
                        }
                    });
                }

                intervalSeconds--;
                if(intervalSeconds <= 0)
                {
                    objFetchServerData = new FetchServerData("mobile_gps_locations");
                    mobileGPSLocations = objFetchServerData.getMobileGPSLocations();
                    if(mobileGPSLocations != null && mobileGPSLocations.size() > 0)
                    {
                        objGPSLocations.insertMobileGPSLocations(mobileGPSLocations);
                    }
                    objFetchServerData = new FetchServerData("order_gps_locations");
                    orderGPSLocations = objFetchServerData.getOrderGPSLocations();
                    if(orderGPSLocations != null && orderGPSLocations.size() > 0)
                    {
                        objGPSLocations.insertOrderGPSLocations(orderGPSLocations);
                    }
                    intervalSeconds = 60;
                }
            }
            catch(InterruptedException e){System.out.println(e);}
        }
    }

    public String getNewOrdersNo()
    {
        objFetchServerData = new FetchServerData("new_bookings_no");
        return objFetchServerData.getNewBookingsNo();
    }

    public String getNewReturnsNo()
    {
        objFetchServerData = new FetchServerData("new_returns_no");
        return objFetchServerData.getNewReturnsNo();
    }

    public String getDealerFinanceNo()
    {
        objFetchServerData = new FetchServerData("new_dealer_finance_no");
        return objFetchServerData.getNewDealerFinanceNo();
    }

    public String getNewUserAccountsNo()
    {
        objFetchServerData = new FetchServerData("new_user_accounts_no");
        return objFetchServerData.getNewUserAccountsNo();
    }
}
