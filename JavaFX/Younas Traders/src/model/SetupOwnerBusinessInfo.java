package model;

import com.jfoenix.controls.JFXButton;
import controller.SetupOwnerBusiness;
import controller.UpdateSupplier;
import javafx.application.Platform;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;
import org.controlsfx.control.textfield.TextFields;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class SetupOwnerBusinessInfo {

    String webUrl;
    String webURLSend = "https://tuberichy.com/ProfitFlow/ServerDataGet/SendData.php";
    String response = "";

    private ArrayList<String> districtNames;

    public SetupOwnerBusinessInfo() {
        this.districtNames = new ArrayList<>();
        this.webUrl = "https://tuberichy.com/ProfitFlow/ServerDataGet/GetData.php?software_lic_id="+GlobalVariables.softwareLicId+"&required_data=district_names";
        String fetchedOrder = fetchServerData();
        if(fetchedOrder != null && !fetchedOrder.equals(""))
        {
            loadIntoListView(fetchedOrder);
            TextFields.bindAutoCompletion(SetupOwnerBusiness.txtWorkingDistrict, this.districtNames);
        }
    }

    public ArrayList<String> getDistrictNames() {
        return districtNames;
    }

    private String fetchServerData()
    {
        try
        {
            URL url = new URL(webUrl);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setConnectTimeout(2 * 1000);
            con.connect();
            if (con.getResponseCode() == 200)
            {
                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String json;
                while ((json = bufferedReader.readLine()) != null)
                {
                    sb.append(json + "\n");
                }
                return sb.toString().trim();
            }
            else
            {
                return null;
            }
        }
        catch (Exception e)
        {
            return null;
        }
    }

    private void loadIntoListView(String json) throws JSONException {
        JSONArray jsonArray = new JSONArray(json);
        this.districtNames = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject obj = jsonArray.getJSONObject(i);
            this.districtNames.add(obj.getString("district_name"));
        }
    }

    public String sendData(String ownerName, String ownerContact, String ownerAddress, String ownerCNIC, String businessName, String businessContact, String businessAddress, String businessNTN, String businessCategory, String workingDistrict)
    {
        String operation = "Send Owner Business";
        try {
            URL objurl = new URL(webURLSend);
            HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
            objhttpurlconnection.setConnectTimeout(5 * 1000);
            objhttpurlconnection.connect();
            if (objhttpurlconnection.getResponseCode() == 200)
            {
                objhttpurlconnection.disconnect();
                objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("operation", "UTF-8") +"="+URLEncoder.encode(operation, "UTF-8")+"&"+
                        URLEncoder.encode("owner_name", "UTF-8") +"="+URLEncoder.encode(ownerName, "UTF-8")+"&"+
                        URLEncoder.encode("owner_contact", "UTF-8") +"="+URLEncoder.encode(ownerContact, "UTF-8")+"&"+
                        URLEncoder.encode("owner_address", "UTF-8") +"="+URLEncoder.encode(ownerAddress, "UTF-8")+"&"+
                        URLEncoder.encode("owner_CNIC", "UTF-8") +"="+URLEncoder.encode(ownerCNIC, "UTF-8")+"&"+
                        URLEncoder.encode("business_name", "UTF-8") +"="+URLEncoder.encode(businessName, "UTF-8")+"&"+
                        URLEncoder.encode("business_contact", "UTF-8") +"="+URLEncoder.encode(businessContact, "UTF-8")+"&"+
                        URLEncoder.encode("business_address", "UTF-8") +"="+URLEncoder.encode(businessAddress, "UTF-8")+"&"+
                        URLEncoder.encode("business_NTN", "UTF-8") +"="+URLEncoder.encode(businessNTN, "UTF-8")+"&"+
                        URLEncoder.encode("business_category", "UTF-8") +"="+URLEncoder.encode(businessCategory, "UTF-8")+"&"+
                        URLEncoder.encode("working_district", "UTF-8") +"="+URLEncoder.encode(workingDistrict, "UTF-8")+"&"+
                URLEncoder.encode("software_id", "UTF-8") +"="+URLEncoder.encode(GlobalVariables.softwareLicId, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                String orderResponse;
                while ((orderResponse = bufferedReader.readLine()) != null)
                {
                    response += orderResponse;
                }
                objhttpurlconnection.disconnect();
            }
            else
            {
                response = "Unable to Connect To Server";
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }
}
