package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class SaleReturnInfo {
    private String returnId;
    private String dealerId;
    private String dealerName;
    private String dealerLic;
    private String dealerAddress;
    private String dealerContact;
    private String dealerNTN;
    private String dealerCnic;
    private String dealerSalesTaxNo;
    private String returnDate;
    private String bookingUser;
    private List<SaleReturnItemsInfo> listOfItems;
    private String totalQty;
    private String orgPrice;
    private String salestax;
    private String totalBonus;
    private String discount;
    private String finalPrice;
    private String footer1;

    public SaleReturnInfo() {
    }

    public SaleReturnInfo(String returnId, String dealerId, String dealerName, String dealerLic, String dealerAddress, String dealerContact, String dealerNTN, String dealerCnic, String dealerSalesTaxNo, String returnDate, String bookingUser, List<SaleReturnItemsInfo> listOfItems, String totalQty, String finalPrice) {
        this.returnId = returnId;
        this.dealerId = dealerId;
        this.dealerName = dealerName;
        this.dealerLic = dealerLic;
        this.dealerAddress = dealerAddress;
        this.dealerContact = dealerContact;
        this.dealerNTN = dealerNTN;
        this.dealerCnic = dealerCnic;
        this.dealerSalesTaxNo = dealerSalesTaxNo;
        this.returnDate = returnDate;
        this.bookingUser = bookingUser;
        this.listOfItems = listOfItems;
        this.totalQty = totalQty;
        this.finalPrice = finalPrice;
    }

    public String getReturnId() {
        return returnId;
    }

    public void setReturnId(String returnId) {
        this.returnId = returnId;
    }

    public String getDealerId() {
        return dealerId;
    }

    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getDealerAddress() {
        return dealerAddress;
    }

    public void setDealerAddress(String dealerAddress) {
        this.dealerAddress = dealerAddress;
    }

    public String getDealerContact() {
        return dealerContact;
    }

    public void setDealerContact(String dealerContact) {
        this.dealerContact = dealerContact;
    }

    public String getDealerLic() {
        return dealerLic;
    }

    public void setDealerLic(String dealerLic) {
        this.dealerLic = dealerLic;
    }

    public String getDealerNTN() {
        return dealerNTN;
    }

    public void setDealerNTN(String dealerNTN) {
        this.dealerNTN = dealerNTN;
    }

    public String getDealerCnic() {
        return dealerCnic;
    }

    public void setDealerCnic(String dealerCnic) {
        this.dealerCnic = dealerCnic;
    }

    public String getDealerSalesTaxNo() {
        return dealerSalesTaxNo;
    }

    public void setDealerSalesTaxNo(String dealerSalesTaxNo) {
        this.dealerSalesTaxNo = dealerSalesTaxNo;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    public String getBookingUser() {
        return bookingUser;
    }

    public void setBookingUser(String bookingUser) {
        this.bookingUser = bookingUser;
    }

    public String getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(String totalQty) {
        this.totalQty = totalQty;
    }

    public String getSalestax() {
        return salestax;
    }

    public void setSalestax(String salestax) {
        this.salestax = salestax;
    }

    public String getTotalBonus() {
        return totalBonus;
    }

    public void setTotalBonus(String totalBonus) {
        this.totalBonus = totalBonus;
    }

    public List<SaleReturnItemsInfo> getListOfItems() {
        return listOfItems;
    }

    public void setListOfItems(List<SaleReturnItemsInfo> listOfItems) {
        this.listOfItems = listOfItems;
    }

    public String gettotalBonus() {
        return totalBonus;
    }

    public void settotalBonus(String totalBonus) {
        this.totalBonus = totalBonus;
    }

    public String getOrgPrice() {
        return orgPrice;
    }

    public void setOrgPrice(String orgPrice) {
        this.orgPrice = orgPrice;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getFinalPrice() {
        return finalPrice;
    }

    public String getFooter1() {
        return footer1;
    }

    public void setFooter1(String footer1) {
        this.footer1 = footer1;
    }

    public void setFinalPrice(String finalPrice) {
        this.finalPrice = finalPrice;
    }

    public ArrayList<ArrayList<String[]>> getInvoiceInfo(Statement stmt, Connection con)
    {
        ArrayList<String> productTemp = null;
        ArrayList<ArrayList<String>> productsInfo = new ArrayList<>();

        ArrayList<ArrayList<String[]>> invoicesData = new ArrayList<>();
        ArrayList<String[]> temp = null;
        String[] orderProductIds;
        String[] orderProductNames;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT `product_id`, `product_name` FROM `product_info`");
            while (rs.next())
            {
                productTemp = new ArrayList<>();
                productTemp.add(rs.getString(1)); // product Id
                productTemp.add(rs.getString(2)); // product Name
                productsInfo.add(productTemp);
            }
//            System.out.println(productsInfo);

            rs = stmt.executeQuery("SELECT `order_info`.`order_id`, `dealer_info`.`dealer_id`, `dealer_info`.`dealer_name`, `dealer_info`.`dealer_address`, `dealer_info`.`dealer_phone`, `order_info`.`product_id`, `order_info`.`quantity`, `order_info`.`unit`, `order_info`.`order_price`, `order_info`.`bonus`, `order_info`.`discount`, `order_info`.`final_price`, `order_info`.`booking_date`, `user_info`.`user_name` from `order_info` LEFT OUTER JOIN `dealer_info` ON `dealer_info`.`dealer_table_id` = `order_info`.`dealer_id` LEFT OUTER JOIN `user_info` ON `order_info`.`booking_user_id` = `user_info`.`user_table_id` WHERE `order_info`.`status` = 'Pending'");
            while (rs.next())
            {
                temp = new ArrayList<>();
                temp.add(rs.getString(1).split("_-_")); // order Id
                if(rs.getString(2) != null)
                {
                    temp.add(rs.getString(2).split("_-_")); // dealer Id
                }
                else
                {
                    temp.add("N/A".split("_-_")); // dealer Id
                }

                temp.add(rs.getString(3).split("_-_")); // dealer name
                temp.add(rs.getString(4).split("_-_")); // dealer address
                temp.add(rs.getString(5).split("_-_")); // dealer contact
                orderProductIds = new String[rs.getString(6).split("_-_").length];
                orderProductNames = new String[rs.getString(6).split("_-_").length];
                orderProductIds = rs.getString(6).split("_-_");
                temp.add(orderProductIds); // product Id
                for(int i=0; i<orderProductIds.length; i++)
                {
                    for(int j=0; j<productsInfo.size(); j++)
                    {
                        if(orderProductIds[i].equals(productsInfo.get(j).get(0)))
                        {
                            orderProductNames[i] = productsInfo.get(j).get(1);
                        }
                    }
                }
                temp.add(orderProductNames);
                temp.add(rs.getString(6).split("_-_")); // product Name
                temp.add(rs.getString(7).split("_-_")); // quantity
                temp.add(rs.getString(8).split("_-_")); // unit
                temp.add(rs.getString(9).split("_-_")); // order price
                temp.add(rs.getString(10).split("_-_")); // totalBonus
                temp.add(rs.getString(11).split("_-_")); // discount
                temp.add(rs.getString(12).split("_-_")); // final price
                temp.add(rs.getString(13).split("_-_")); // date
                if(rs.getString(14) != null)
                {
                    temp.add(rs.getString(14).split("_-_")); // salesman Name
                }
                else
                {
                    temp.add("N/A".split("_-_")); // salesman Name
                }

                invoicesData.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
//        System.out.println("Rows: "+invoicesData.size()+" Cols: "+invoicesData.get(0).size());
//        for(int i=0; i<invoicesData.size(); i++)
//        {
//            for(int j=0; j<invoicesData.get(0).size(); j++)
//            {
//                System.out.print(String.join(",", invoicesData.get(i).get(j))+" ----- ");
//            }
//            System.out.println("");
//        }
        return invoicesData;
    }

    public void updateInvoice(Statement stmt, Connection con, String orderId, String dealerId, String dashedProductId, ArrayList<String> productIdArr, ArrayList<String> batchArr, String dashedQuantity, ArrayList<String> quantityArr, String dashedUnit, ArrayList<String> unitArr, String orderPrice, ArrayList<String> orderPriceArr, String dashedtotalBonus, ArrayList<String> totalBonusArr, String dashedDiscount, ArrayList<String> discountArr, String finalPrice, ArrayList<String> finalPriceArr, String updateDate, String updateTime, String orderStatus)
    {
        try {
            String updateQuery = "UPDATE `order_info` SET `dealer_id`='"+dealerId+"',`product_id`='"+dashedProductId+"',`quantity`='"+dashedQuantity+"',`unit`='"+dashedUnit+"',`order_price`='"+orderPrice+"',`totalBonus`='"+dashedtotalBonus+"',`discount`='"+dashedDiscount+"',`final_price`='"+finalPrice+"',`update_dates`='"+updateDate+"',`update_times`='"+updateTime+"',`status`='"+orderStatus+"', `server_sync` = CASE WHEN `server_sync` = 'Done' OR `server_sync` = 'Update' THEN 'Update' ELSE `server_sync` END WHERE `order_id` = '"+orderId+"'";
            stmt.executeUpdate(updateQuery);

            String deleteData = "DELETE FROM `order_info_detailed` WHERE `order_id` = '"+orderId+"'";
            stmt.execute(deleteData);

            for(int i=0; i<productIdArr.size(); i++)
            {
                String insertOrderData = "INSERT INTO `order_info_detailed`(`order_id`, `product_id`, `batch_number`, `quantity`, `submission_quantity`, `unit`, `submission_unit`, `order_price`, `totalBonus_quant`, `discount`, `final_price`, `cmplt_returned_bit`, `returned_quant`, `returned_totalBonus_quant`) VALUES ('"+orderId+"','"+productIdArr.get(i)+"','"+batchArr.get(i)+"','"+quantityArr.get(i)+"','"+quantityArr.get(i)+"','"+unitArr.get(i)+"','"+unitArr.get(i)+"','"+orderPriceArr.get(i)+"','"+totalBonusArr.get(i)+"','"+discountArr.get(i)+"','"+finalPriceArr.get(i)+"','0','0','0')";
                stmt.executeUpdate(insertOrderData);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
