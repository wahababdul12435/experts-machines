package model;

import com.jfoenix.controls.JFXCheckBox;
import controller.SetupOwnerDealers;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class SetupOwnerDealersInfo {
    private String srNo;
    private String dealerName;
    private String dealerContact;
    private String dealerAddress;
    private String dealerCnic;
    private String dealerType;
    private JFXCheckBox operationsPane;

    public static ArrayList<String> dealerNameArr = new ArrayList<>();
    public static ArrayList<String> dealerContactPersonArr = new ArrayList<>();
    public static ArrayList<String> dealerContactArr = new ArrayList<>();
    public static ArrayList<String> dealerFaxArr = new ArrayList<>();
    public static ArrayList<String> dealerAddressArr = new ArrayList<>();
    public static ArrayList<String> dealerAddress1Arr = new ArrayList<>();
    public static ArrayList<String> dealerTypeArr = new ArrayList<>();
    public static ArrayList<String> dealerCnicArr = new ArrayList<>();
    public static ArrayList<String> dealerNtnArr = new ArrayList<>();
    public static ArrayList<String> dealerLic9NumArr = new ArrayList<>();
    public static ArrayList<String> dealerLic9ExpArr = new ArrayList<>();
    public static ArrayList<String> dealerLic10NumArr = new ArrayList<>();
    public static ArrayList<String> dealerLic10ExpArr = new ArrayList<>();
    public static ArrayList<String> dealerLic11NumArr = new ArrayList<>();
    public static ArrayList<String> dealerLic11ExpArr = new ArrayList<>();
    public static ArrayList<String> dealerLatitudeArr = new ArrayList<>();
    public static ArrayList<String> dealerLongitudeArr = new ArrayList<>();
    public static ArrayList<String> dealerLocationArr = new ArrayList<>();
    public static ArrayList<JFXCheckBox> arrCheckBox = new ArrayList<>();

    public static Label lblDealersSelected;

    String webUrl;
    public static String districtName = "Gujrat";
    public static String businessCategory = "Pharmaceutical";

    public static ArrayList<ArrayList<String>> dealersData = new ArrayList<>();
    public static TableView<SetupOwnerDealersInfo> table_viewdealers;

    public SetupOwnerDealersInfo() {
        this.webUrl = "https://tuberichy.com/ProfitFlow/ServerDataGet/GetData.php?software_lic_id="+GlobalVariables.softwareLicId+"&required_data=dealers_detail&district_name="+districtName+"&business_category="+businessCategory;
        String fetchedOrder = fetchServerData();
        if(fetchedOrder != null && !fetchedOrder.equals(""))
        {
            loadIntoListView(fetchedOrder);
            table_viewdealers.setItems(SetupOwnerDealers.parseUserList());
        }
    }

    public SetupOwnerDealersInfo(String srNo, String dealerName, String dealerContact, String dealerAddress, String dealerCnic, String dealerType) {
        this.srNo = srNo;
        this.dealerName = dealerName;
        this.dealerContact = dealerContact;
        this.dealerAddress = dealerAddress;
        this.dealerCnic = dealerCnic;
        this.dealerType = dealerType;

        this.operationsPane = new JFXCheckBox();
        this.operationsPane.setOnAction((action)->checkClicked());
        arrCheckBox.add(this.operationsPane);
        this.operationsPane.setAlignment(Pos.CENTER);
    }

    private void checkClicked()
    {
        if(this.operationsPane.isSelected())
        {
            SetupOwnerDealers.selectedDealers++;
            lblDealersSelected.setText(String.valueOf(SetupOwnerDealers.selectedDealers));
            arrCheckBox.get(Integer.parseInt(srNo)-1).setSelected(true);
        }
        else
        {
            SetupOwnerDealers.selectedDealers--;
            lblDealersSelected.setText(String.valueOf(SetupOwnerDealers.selectedDealers));
            arrCheckBox.get(Integer.parseInt(srNo)-1).setSelected(false);
        }
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getDealerContact() {
        return dealerContact;
    }

    public void setDealerContact(String dealerContact) {
        this.dealerContact = dealerContact;
    }

    public String getDealerAddress() {
        return dealerAddress;
    }

    public void setDealerAddress(String dealerAddress) {
        this.dealerAddress = dealerAddress;
    }

    public String getDealerCnic() {
        return dealerCnic;
    }

    public void setDealerCnic(String dealerCnic) {
        this.dealerCnic = dealerCnic;
    }

    public String getDealerType() {
        return dealerType;
    }

    public void setDealerType(String dealerType) {
        this.dealerType = dealerType;
    }

    public JFXCheckBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(JFXCheckBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    private String fetchServerData()
    {
        try
        {
            URL url = new URL(webUrl);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setConnectTimeout(2 * 1000);
            con.connect();
            if (con.getResponseCode() == 200)
            {
                StringBuilder sb = new StringBuilder();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String json;
                while ((json = bufferedReader.readLine()) != null)
                {
                    sb.append(json + "\n");
                }
                return sb.toString().trim();
            }
            else
            {
                return null;
            }
        }
        catch (Exception e)
        {
            return null;
        }
    }

    private void loadIntoListView(String json) throws JSONException {
        JSONArray jsonArray = new JSONArray(json);
        dealersData = new ArrayList<>();
        ArrayList<String> temp;
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject obj = jsonArray.getJSONObject(i);
            temp = new ArrayList<>();
            temp.add(String.valueOf(obj.getInt("system_dealer_id")));
            temp.add(obj.getString("dealer_name"));
            temp.add(obj.getString("dealer_contact_person"));
            temp.add(obj.getString("dealer_phone"));
            temp.add(obj.getString("dealer_fax"));
            temp.add(obj.getString("dealer_address"));
            temp.add(obj.getString("dealer_address1"));
            temp.add(obj.getString("dealer_type"));
            temp.add(obj.getString("dealer_cnic"));
            temp.add(obj.getString("dealer_ntn"));
            temp.add(obj.getString("dealer_lic9_num"));
            temp.add(obj.getString("dealer_lic9_exp"));
            temp.add(obj.getString("dealer_lic10_num"));
            temp.add(obj.getString("dealer_lic10_exp"));
            temp.add(obj.getString("dealer_lic11_num"));
            temp.add(obj.getString("dealer_lic11_exp"));
            temp.add(obj.isNull("latitude") ? "" : obj.getString("latitude"));
            temp.add(obj.isNull("longitude") ? "" : obj.getString("longitude"));
            temp.add(obj.isNull("location_name") ? "" : obj.getString("location_name"));
            dealersData.add(temp);
        }
    }

    public boolean insertDealer(Statement stmt) {
        String currentDate = GlobalVariables.getStDate();
        String currentTime = GlobalVariables.getStTime();
        String currentUser = GlobalVariables.getUserId();
        String insertQuery;
        String insertOverall;
        String dealerTableId;
        int insertInfo = 1;

        try {
            for(int i=0; i<dealerNameArr.size(); i++)
            {
                insertQuery = "INSERT INTO `dealer_info`(`dealer_area_id`, `dealer_name`, `dealer_contact_person`, `dealer_phone`, `dealer_address`, `dealer_type`, `dealer_cnic`, `dealer_lic9_num`, `dealer_lic9_exp`, `dealer_lic10_num`, `dealer_lic10_Exp`, `dealer_lic11_num`, `dealer_lic11_Exp`, `dealer_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`) VALUES ('0','"+dealerNameArr.get(i)+"','"+dealerNameArr.get(i)+"','"+dealerContactArr.get(i)+"','"+dealerAddressArr.get(i)+"','"+dealerTypeArr.get(i)+"','"+dealerCnicArr.get(i)+"','"+dealerLic9NumArr.get(i)+"','"+dealerLic9ExpArr.get(i)+"','"+dealerLic10NumArr.get(i)+"','"+dealerLic10ExpArr.get(i)+"','"+dealerLic11NumArr.get(i)+"','"+dealerLic11ExpArr.get(i)+"','Active','"+currentUser+"','"+currentDate+"','"+currentTime+"','"+currentUser+"','"+currentDate+"','"+currentTime+"')";
                try {
                    if(stmt.executeUpdate(insertQuery) == -1)
                    {
                        insertInfo = -1;
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                dealerTableId = getDealerTableId(stmt, dealerNameArr.get(i), dealerContactArr.get(i), dealerCnicArr.get(i));
                insertQuery = "INSERT INTO `dealer_gps_location`(`dealer_id`, `latitude`, `longitude`, `loc_name`, `entry_date`, `entry_time`, `update_date`, `update_time`, `user_id`) VALUES ('"+dealerTableId+"', '"+dealerLatitudeArr.get(i)+"', '"+dealerLongitudeArr.get(i)+"', '"+dealerLocationArr.get(i)+"', '"+currentDate+"', '"+currentTime+"', '', '', '"+currentUser+"')";
                if(stmt.executeUpdate(insertQuery) == -1)
                {
                    insertInfo = -1;
                }
                insertOverall = "INSERT INTO `dealer_overall_record`(`dealer_id`, `orders_given`, `successful_orders`, `ordered_packets`, `submitted_packets`, `ordered_boxes`, `submitted_boxes`, `order_price`, `discount_price`, `invoiced_price`, `cash_collected`, `return_packets`, `return_boxes`, `return_price`, `cash_return`, `waived_off_price`, `pending_payments`) VALUES ('"+dealerTableId+"','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0')";
                if(stmt.executeUpdate(insertOverall) == -1)
                {
                    insertInfo = -1;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if(insertInfo == -1)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public String getDealerTableId(Statement stmt3, String dealerName, String dealerContact, String dealerCnic)
    {
        String checkDealerId = "0";
        ResultSet rs3 = null;
        try {
            String getIdStatment = "SELECT `dealer_table_id` FROM `dealer_info` WHERE `dealer_name` = '"+dealerName+"' AND `dealer_phone` = '"+dealerContact+"' AND `dealer_cnic` = '"+dealerCnic+"' AND `dealer_status` != 'Deleted'";
            rs3 = stmt3.executeQuery(getIdStatment);
            while (rs3.next())
            {
                checkDealerId = rs3.getString(1) ;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return checkDealerId;
    }
}
