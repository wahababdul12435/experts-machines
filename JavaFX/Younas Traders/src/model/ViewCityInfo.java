package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import controller.DeleteWindow;
import controller.UpdateCity;
import controller.UpdateDistrict;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ViewCityInfo {
    private String srNo;
    private String cityTableId;
    private String cityId;
    private String districtId;
    private String districtName;
    private String cityName;
    private String areaIds;
    private String areaNames;
    private String cityStatus;

    private HBox operationsPane;
    private JFXButton btnEdit;
    private JFXButton btnDelete;

    public static StackPane stackPane;
    public static JFXDialog dialog;
    public static Label lblUpdate;

    public ViewCityInfo()
    {
        this.srNo = "";
        this.cityTableId = "";
        this.cityId = "";
        this.districtId = "";
        this.districtName = "";
        this.cityName = "";
        this.areaIds = "";
        this.areaNames = "";
        this.cityStatus = "";
        this.btnEdit = new JFXButton("Edit");
        this.btnDelete = new JFXButton("Delete");
        this.operationsPane = new HBox(btnEdit, btnDelete);
    }

    public ViewCityInfo(String srNo, String cityTableId, String cityId, String districtId, String districtName, String cityName, String areaIds, String areaNames, String cityStatus) {
        this.srNo = srNo;
        this.cityTableId = cityTableId;
        this.cityId = cityId;
        this.districtId = districtId;
        this.districtName = districtName;
        this.cityName = cityName;
        this.areaIds = areaIds;
        this.areaNames = areaNames;
        this.cityStatus = cityStatus;

        this.btnEdit = new JFXButton();
        this.btnDelete = new JFXButton();
//        this.btnView.setGraphic(GlobalVariables.createViewIcon());

        this.operationsPane = new HBox();

//        this.btnView.getStyleClass().add("btn_icon");
//        this.btnView.setOnAction((action)->viewClicked());
//        this.operationsPane.getChildren().add(this.btnView);

        if(GlobalVariables.rightEdit)
        {
            this.btnEdit.setGraphic(GlobalVariables.createEditIcon());
            this.btnEdit.getStyleClass().add("btn_icon");
            this.btnEdit.setOnAction((action)->editClicked());
            this.operationsPane.getChildren().add(this.btnEdit);
        }
        if(GlobalVariables.rightDelete)
        {
            this.btnDelete.setGraphic(GlobalVariables.createDeleteIcon());
            this.btnDelete.getStyleClass().add("btn_icon");
            this.btnDelete.setOnAction(event -> {
                try {
                    deleteCity(this.cityTableId);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            this.operationsPane.getChildren().add(this.btnDelete);
        }
    }

    public void editClicked()
    {
        Parent parent = null;
        try {
            UpdateCity.cityTableId = cityTableId;
            UpdateCity.cityId = cityId;
            UpdateCity.cityName = cityName;
            UpdateCity.districtId = districtId;
            UpdateCity.districtName = districtName;
            UpdateCity.cityStatus = cityStatus;
            parent = FXMLLoader.load(getClass().getResource("/view/update_city.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setBody(parent);
        dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        stackPane.setVisible(true);
        dialog.show();
        dialog.setOverlayClose(false);
    }

    public void deleteCity(String cityTableId) throws IOException {
        DeleteWindow.sceneWindow = "ViewCity";
        DeleteWindow.deletionId = cityTableId;
        Parent parent = null;
        try {
            parent = FXMLLoader.load(getClass().getResource("/view/delete_window.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setStyle("-fx-padding: -40 -20 -40 -20;");
        dialogLayout.setBody(parent);
        dialog = new JFXDialog(stackPane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        stackPane.setVisible(true);
        dialog.show();
        dialog.setOverlayClose(false);
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getCityTableId() {
        return cityTableId;
    }

    public void setCityTableId(String cityTableId) {
        this.cityTableId = cityTableId;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getDistrictId() {
        return districtId;
    }

    public void setDistrictId(String districtId) {
        this.districtId = districtId;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getAreaIds() {
        return areaIds;
    }

    public void setAreaIds(String areaIds) {
        this.areaIds = areaIds;
    }

    public String getAreaNames() {
        return areaNames;
    }

    public void setAreaNames(String areaNames) {
        this.areaNames = areaNames;
    }

    public String getCityStatus() {
        return cityStatus;
    }

    public void setCityStatus(String cityStatus) {
        this.cityStatus = cityStatus;
    }

    public String getcityStatus() {
        return cityStatus;
    }

    public void setcityStatus(String cityStatus) {
        this.cityStatus = cityStatus;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnEdit() {
        return btnEdit;
    }

    public void setBtnEdit(JFXButton btnEdit) {
        this.btnEdit = btnEdit;
    }

    public JFXButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(JFXButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public ArrayList<ArrayList<String>> getCitiesInfo(Statement stmt, Connection con)
    {
        ArrayList<ArrayList<String>> districtData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        temp = new ArrayList<String>();
        ResultSet rs = null;
        String preCityId = "";
        String areaId  = "";
        String areaName  = "";
        boolean iter = false;
        try {
            rs = stmt.executeQuery("SELECT `city_info`.`city_table_id`, `city_info`.`city_id`, `district_info`.`district_table_id`, `district_info`.`district_name`, `city_info`.`city_name`, `area_info`.`area_table_id`, `area_info`.`area_name`, `city_info`.`city_status` FROM `city_info` LEFT OUTER JOIN `district_info` ON `city_info`.`district_table_id` = `district_info`.`district_table_id` LEFT OUTER JOIN `area_info` ON `city_info`.`city_table_id` = `area_info`.`city_table_id` WHERE `city_info`.`city_status` != 'Deleted' ORDER BY `city_info`.`city_table_id`");
            while (rs.next())
            {
                if(preCityId.equals(rs.getString(1)))
                {
                    areaId += "\n"+rs.getString(6);
                    areaName += "\n"+rs.getString(7);
                }
                else
                {
                    if(iter)
                    {
                        temp.set(5, areaId);
                        temp.set(6, areaName);
                        districtData.add(temp);
                        areaId = rs.getString(6);
                        areaName = rs.getString(7);
                        temp = new ArrayList<String>();
                        preCityId = rs.getString(1);
                    }
                    else
                    {
                        iter = true;
                        areaId = rs.getString(6);
                        areaName = rs.getString(7);
                        temp = new ArrayList<>();
                        preCityId = rs.getString(1);
                    }
                    temp.add(rs.getString(1)); // Table Id
                    temp.add(rs.getString(2)); // City Id
                    temp.add(rs.getString(3)); // District Id
                    temp.add(rs.getString(4)); // District Name
                    temp.add(rs.getString(5)); // City Name
                    temp.add(rs.getString(6)); // Area Id
                    temp.add(rs.getString(7)); // Area Name
                    temp.add(rs.getString(8)); // Status
                }
            }
            if(iter)
            {
                temp.set(5, areaId);
                temp.set(6, areaName);
                districtData.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return districtData;
    }

    public ArrayList<ArrayList<String>> getCitiesSearch(Statement stmt, Connection con, String cityId, String cityName, String districtId, String cityStatus)
    {
        ArrayList<ArrayList<String>> districtData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        temp = new ArrayList<String>();
        ResultSet rs = null;
        String preCityId = "";
        String areaId  = "";
        String areaName  = "";
        boolean iter = false;
        String searchQuery;
        int multipleSearch = 0;
        searchQuery = "SELECT `city_info`.`city_table_id`, `city_info`.`city_id`, `district_info`.`district_table_id`, `district_info`.`district_name`, `city_info`.`city_name`, `area_info`.`area_table_id`, `area_info`.`area_name`, `city_info`.`city_status` FROM `city_info` LEFT OUTER JOIN `district_info` ON `city_info`.`district_table_id` = `district_info`.`district_table_id` LEFT OUTER JOIN `area_info` ON `city_info`.`city_table_id` = `area_info`.`city_table_id` WHERE ";
        if(!cityId.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`city_info`.`city_id` = '"+cityId+"'";
            multipleSearch++;
        }
        if(!cityName.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`city_info`.`city_name` = '"+cityName+"'";
            multipleSearch++;
        }
        if(!districtId.equals(""))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`city_info`.`district_table_id` = '"+districtId+"'";
            multipleSearch++;
        }
        if(!cityStatus.equals("All"))
        {
            if(multipleSearch > 0)
            {
                searchQuery += " AND ";
            }
            searchQuery += "`city_info`.`city_status` = '"+cityStatus+"'";
            multipleSearch++;
        }
        if(multipleSearch >= 1)
        {
            searchQuery += " AND ";
        }
        searchQuery += "`city_info`.`city_status` != 'Deleted' ORDER BY `city_info`.`city_table_id`";
        try {
            rs = stmt.executeQuery(searchQuery);
            while (rs.next())
            {
                if(preCityId.equals(rs.getString(1)))
                {
                    areaId += "\n"+rs.getString(6);
                    areaName += "\n"+rs.getString(7);
                }
                else
                {
                    if(iter)
                    {
                        temp.set(5, areaId);
                        temp.set(6, areaName);
                        districtData.add(temp);
                        areaId = rs.getString(6);
                        areaName = rs.getString(7);
                        temp = new ArrayList<String>();
                        preCityId = rs.getString(1);
                    }
                    else
                    {
                        iter = true;
                        areaId = rs.getString(6);
                        areaName = rs.getString(7);
                        temp = new ArrayList<>();
                        preCityId = rs.getString(1);
                    }
                    temp.add(rs.getString(1)); // Table Id
                    temp.add(rs.getString(2)); // City Id
                    temp.add(rs.getString(3)); // District Id
                    temp.add(rs.getString(4)); // District Name
                    temp.add(rs.getString(5)); // City Name
                    temp.add(rs.getString(6)); // Area Id
                    temp.add(rs.getString(7)); // Area Name
                    temp.add(rs.getString(8)); // Status
                }
            }
            if(iter)
            {
                temp.set(5, areaId);
                temp.set(6, areaName);
                districtData.add(temp);
            }
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return districtData;
    }

    public ArrayList<String> getCitiesSummary(Statement stmt, Connection con)
    {
        ArrayList<String> groupData = new ArrayList<String>();
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT COUNT(`city_table_id`) FROM `city_info` WHERE `city_status` != 'Deleted'");
            while (rs.next())
            {
                groupData.add(rs.getString(1));
            }
            rs = stmt.executeQuery("SELECT COUNT(`city_table_id`) FROM `city_info` WHERE `city_status` = 'Active'");
            while (rs.next())
            {
                groupData.add(rs.getString(1));
            }
            rs = stmt.executeQuery("SELECT COUNT(`city_table_id`) FROM `city_info` WHERE `city_status` = 'In Active'");
            while (rs.next())
            {
                groupData.add(rs.getString(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return groupData;
    }

    private Node createErrorIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.EDIT);
        graphic.setFontSize(20.0);
        return graphic;
    }

    private Node createDeleteIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.TRASH);
        graphic.setFontSize(20.0);
        return graphic;
    }
}
