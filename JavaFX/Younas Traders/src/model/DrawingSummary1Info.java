package model;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class DrawingSummary1Info {
    private String srNo;
    private String productTableId;
    private String productId;
    private String productName;
    private String productBatch;
    private String quantity;
    private String bonusQuantity;
    private String tradePrice;
    private String totalAmount;
    private String itemsInStock;
    private String expiryDate;
    private String daysLeft;
    private String deficiency;
    private HBox operationsPane;
    private Button btnView;
    public static String productIdDetail;

    public DrawingSummary1Info() {
        this.srNo = "0";
        this.productTableId = "0";
        this.productId = "0";
        this.productName = "";
        this.productBatch = "";
        this.quantity = "0";
        this.bonusQuantity = "0";
        this.tradePrice = "0";
        this.totalAmount = "0";
        this.itemsInStock = "0";
        this.expiryDate = "";
        this.daysLeft = "0";
        this.deficiency = "0";
        this.operationsPane = new HBox();
    }

    public DrawingSummary1Info(String srNo, String productTableId, String productId, String productName, String productBatch, String quantity, String bonusQuantity, String tradePrice, String totalAmount, String itemsInStock, String expiryDate, String daysLeft, String deficiency) {
        this.srNo = srNo;
        this.productTableId = productTableId;
        this.productId = productId;
        this.productName = productName;
        this.productBatch = productBatch;
        this.quantity = quantity;
        this.bonusQuantity = bonusQuantity;
        this.tradePrice = tradePrice;
        this.totalAmount = totalAmount;
        this.itemsInStock = itemsInStock;
        this.expiryDate = expiryDate;
        this.daysLeft = daysLeft;
        this.deficiency = deficiency;
        btnView = new Button("Details");
        btnView.setOnAction(event -> {
            try {
                goToOrderedProductDetail(productId);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        this.operationsPane = new HBox(btnView);
    }

    public void goToOrderedProductDetail(String productIdDet) throws IOException {
        productIdDetail = productIdDet;
        Parent root = FXMLLoader.load(getClass().getResource("/view/ordered_products_detail.fxml"));
        Stage objstage = (Stage) operationsPane.getScene().getWindow();
        Scene objscene = new Scene(root);
        objstage.setScene(objscene);
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getProductTableId() {
        return productTableId;
    }

    public void setProductTableId(String productTableId) {
        this.productTableId = productTableId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductBatch() {
        return productBatch;
    }

    public void setProductBatch(String productBatch) {
        this.productBatch = productBatch;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getBonusQuantity() {
        return bonusQuantity;
    }

    public void setBonusQuantity(String bonusQuantity) {
        this.bonusQuantity = bonusQuantity;
    }

    public String getTradePrice() {
        return tradePrice;
    }

    public void setTradePrice(String tradePrice) {
        this.tradePrice = tradePrice;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getItemsInStock() {
        return itemsInStock;
    }

    public void setItemsInStock(String itemsInStock) {
        this.itemsInStock = itemsInStock;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getDaysLeft() {
        return daysLeft;
    }

    public void setDaysLeft(String daysLeft) {
        this.daysLeft = daysLeft;
    }

    public String getDeficiency() {
        return deficiency;
    }

    public void setDeficiency(String deficiency) {
        this.deficiency = deficiency;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public Button getBtnView() {
        return btnView;
    }

    public void setBtnView(Button btnView) {
        this.btnView = btnView;
    }

//    public ArrayList<ArrayList<String>> getDrawingSummary(Statement stmt, Connection con)
//    {
//        ArrayList<ArrayList<String>> drawingSummaryRawData = new ArrayList<ArrayList<String>>();
//        ArrayList<String> temp;
//        String[] productIds;
//        String[] quantity;
//        ArrayList<ArrayList<String>> inStockData = null;
//        ResultSet rs = null;
//        try {
//            rs = stmt.executeQuery("SELECT `product_id`, `quantity` FROM `order_info` WHERE `status` = 'Pending'");
//            while (rs.next())
//            {
//                productIds = rs.getString(1).split("_-_");
//                if(productIds.length > 1)
//                {
//                    quantity = rs.getString(2).split("_-_");
//                    for(int i=0; i<productIds.length; i++)
//                    {
//                        temp = new ArrayList<String>();
//                        temp.add(productIds[i]);
//                        temp.add(quantity[i]);
//                        drawingSummaryRawData.add(temp);
//                    }
//                }
//                else
//                {
//                    temp = new ArrayList<String>();
//                    temp.add(rs.getString(1));
//                    temp.add(rs.getString(2));
//                    drawingSummaryRawData.add(temp);
//                }
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        ArrayList<String> uniqueProductIds = new ArrayList<String>();
//        ArrayList<String> uniqueProductNames = new ArrayList<String>();
//        ArrayList<String> uniqueProductIdsQuantities = new ArrayList<String>();
//        for(ArrayList<String> row: drawingSummaryRawData) {
//            if(!uniqueProductIds.contains(row.get(0)))
//            {
//                uniqueProductIds.add(row.get(0));
//                uniqueProductNames.add("");
//            }
//        }
//        int currQuant;
//        for(String row: uniqueProductIds) {
//            currQuant = 0;
//            for(int i=0; i<drawingSummaryRawData.size(); i++)
//            {
//                if(drawingSummaryRawData.get(i).get(0).equals(row))
//                {
//                    currQuant += Integer.parseInt(drawingSummaryRawData.get(i).get(1));
//                }
//            }
//            uniqueProductIdsQuantities.add(String.valueOf(currQuant));
//        }
//
//        ArrayList<ArrayList<String>> drawingSummaryData = new ArrayList<ArrayList<String>>();
//        ResultSet rs1 = null;
//        ResultSet rs2 = null;
//        int productIndex;
//        try {
//            rs1 = stmt.executeQuery("SELECT `product_table_id`, `product_name` FROM `product_info`");
//            while (rs1.next())
//            {
//                productIndex = uniqueProductIds.indexOf(rs1.getString(1));
//                if(productIndex>=0)
//                {
//                    uniqueProductNames.set(productIndex, rs1.getString(2));
//                }
//            }
//            rs2 = stmt.executeQuery("SELECT `product_table_id`, `in_stock` FROM `products_stock`");
//            inStockData = new ArrayList<>();
//            while (rs2.next())
//            {
//                temp = new ArrayList<String>();
//                temp.add(rs2.getString(1));
//                temp.add(rs2.getString(2));
//                inStockData.add(temp);
//            }
//            con.close();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//
//        for (int i=0; i<uniqueProductIds.size(); i++)
//        {
//            temp = new ArrayList<String>();
//            temp.add(uniqueProductIds.get(i));
//            temp.add(uniqueProductNames.get(i));
//            temp.add(uniqueProductIdsQuantities.get(i));
//            int indexOfStock = findIndex(uniqueProductIds.get(i), inStockData);
//            int dif = 0;
//            if(indexOfStock >= 0)
//            {
//                temp.add(inStockData.get(indexOfStock).get(1));
//                dif = Integer.parseInt(inStockData.get(indexOfStock).get(1)) - Integer.parseInt(uniqueProductIdsQuantities.get(i));
//            }
//            else
//            {
//                temp.add("0");
//            }
//            temp.add(String.valueOf(dif));
//            drawingSummaryData.add(temp);
//        }
//
//        return drawingSummaryData;
//    }

    public ArrayList<ArrayList<String>> getDrawingSummary(Statement stmt, Connection con)
    {
        ArrayList<ArrayList<String>> drawingSummaryData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        String query = "SELECT `order_info_detailed`.`product_table_id`, `product_info`.`product_id`, `product_info`.`product_name`, `batchwise_stock`.`batch_no`, SUM(`order_info_detailed`.`submission_quantity`) as 'quantity', SUM(`order_info_detailed`.`bonus_quant`) as 'bonus_quantity', ROUND(SUM(`product_info`.`trade_price` * `order_info_detailed`.`submission_quantity`), 2) as 'trade_price', ROUND(SUM(`order_info_detailed`.`final_price`), 2) as 'final_price', `products_stock`.`in_stock` as 'in_stock', `batchwise_stock`.`batch_expiry`, `products_stock`.`in_stock` - (SUM(`order_info_detailed`.`submission_quantity`) + SUM(`order_info_detailed`.`bonus_quant`)) as 'difference' FROM `order_info` INNER JOIN `order_info_detailed` ON `order_info_detailed`.`order_id` = `order_info`.`order_id` LEFT OUTER JOIN `product_info` ON `product_info`.`product_table_id` = `order_info_detailed`.`product_table_id` LEFT OUTER JOIN `products_stock` ON `products_stock`.`product_table_id` = `order_info_detailed`.`product_table_id` LEFT OUTER JOIN `batchwise_stock` ON `batchwise_stock`.`batch_stock_id` = `order_info_detailed`.`batch_id` WHERE `order_info`.`status` = 'Pending' GROUP BY `order_info_detailed`.`product_table_id`";
        try {
            rs = stmt.executeQuery(query);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Table Id
                temp.add(rs.getString(2) == null || rs.getString(2).equals("") ? "N/A" : rs.getString(2)); // Id
                temp.add(rs.getString(3) == null || rs.getString(3).equals("") ? "N/A" : rs.getString(3)); // Name
                temp.add(rs.getString(4) == null || rs.getString(4).equals("") ? "N/A" : rs.getString(4)); // Batch No
                temp.add(rs.getString(5) == null || rs.getString(5).equals("") ? "N/A" : rs.getString(5)); // Quantity
                temp.add(rs.getString(6) == null || rs.getString(6).equals("") ? "N/A" : rs.getString(6)); // Bonus Quantity
                temp.add(rs.getString(7) == null || rs.getString(7).equals("") ? "N/A" : rs.getString(7)); // Trade Price
                temp.add(rs.getString(8) == null || rs.getString(8).equals("") ? "N/A" : rs.getString(8)); // Final Price
                temp.add(rs.getString(9) == null || rs.getString(9).equals("") ? "N/A" : rs.getString(9)); // In Stock

                if(rs.getString(10) != null && !rs.getString(10).equals(""))
                {
                    temp.add(rs.getString(10)); // Batch Expiry
                    String stEndDate = rs.getString(10);
                    String stStartDate = GlobalVariables.getStDate();

                    Date endDate = null;
                    Date startDate = null;
                    try {
                        startDate = GlobalVariables.fmt.parse(stStartDate);
                        endDate = GlobalVariables.fmt.parse(stEndDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    long diff = endDate.getTime() - startDate.getTime();
                    diff = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);

                    temp.add(String.valueOf(diff)); // days left
                }
                else
                {
                    temp.add("N/A"); // Batch Expiry
                    temp.add("N/A"); // days left
                }

                temp.add(rs.getString(11) == null || rs.getString(11).equals("") ? "N/A" : rs.getString(11)); // Difference
                drawingSummaryData.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return drawingSummaryData;
    }

    public ArrayList<ArrayList<String>> getDrawingSummarySearch(Statement stmt, Connection con, ArrayList<String> areaIds, ArrayList<String> productIds, ArrayList<String> dealerIds, String fromBookingDate, String toBookingDate, String invoiceStatus, String fromInvoiceNo, String toInvoiceNo)
    {
        ArrayList<ArrayList<String>> drawingSummaryData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        String query = "SELECT `order_info_detailed`.`product_table_id`, `product_info`.`product_id`, `product_info`.`product_name`, `batchwise_stock`.`batch_no`, SUM(`order_info_detailed`.`submission_quantity`) as 'quantity', SUM(`order_info_detailed`.`bonus_quant`) as 'bonus_quantity', ROUND(SUM(`product_info`.`trade_price` * `order_info_detailed`.`submission_quantity`), 2) as 'trade_price', ROUND(SUM(`order_info_detailed`.`final_price`), 2) as 'final_price', `products_stock`.`in_stock` as 'in_stock', `batchwise_stock`.`batch_expiry`, `products_stock`.`in_stock` - (SUM(`order_info_detailed`.`submission_quantity`) + SUM(`order_info_detailed`.`bonus_quant`)) as 'difference' FROM `order_info` INNER JOIN `order_info_detailed` ON `order_info_detailed`.`order_id` = `order_info`.`order_id` LEFT OUTER JOIN `product_info` ON `product_info`.`product_table_id` = `order_info_detailed`.`product_table_id` LEFT OUTER JOIN `products_stock` ON `products_stock`.`product_table_id` = `order_info_detailed`.`product_table_id` LEFT OUTER JOIN `batchwise_stock` ON `batchwise_stock`.`batch_stock_id` = `order_info_detailed`.`batch_id` LEFT OUTER JOIN `dealer_info` ON `dealer_info`.`dealer_table_id` = `order_info`.`dealer_id` WHERE `order_info`.`status` = 'Pending'";
        if(areaIds.size() > 0)
        {
            if(!areaIds.get(0).equals("All"))
            {
                query += " AND (";
                boolean firstIter = true;
                for(int i=0; i<areaIds.size(); i++)
                {
                    if(firstIter)
                    {
                        firstIter = false;
                    }
                    else
                    {
                        query += " OR ";
                    }
                    query += " `dealer_info`.`dealer_area_id` = '"+areaIds.get(i)+"'";
                }
                query += ")";
            }
        }
        else
        {
            query += " AND `dealer_info`.`dealer_area_id` = '0'";
        }
        if(productIds.size() > 0)
        {
            if(!productIds.get(0).equals("All"))
            {
                query += " AND (";
                boolean firstIter = true;
                for(int i=0; i<productIds.size(); i++)
                {
                    if(firstIter)
                    {
                        firstIter = false;
                    }
                    else
                    {
                        query += " OR ";
                    }
                    query += " `order_info_detailed`.`product_table_id` = '"+productIds.get(i)+"'";
                }
                query += ")";
            }
        }
        else
        {
            query += " AND `order_info_detailed`.`product_table_id` = '0'";
        }
        if(dealerIds.size() > 0)
        {
            if(!dealerIds.get(0).equals("All"))
            {
                boolean firstIter = true;
                query += " AND (";
                for(int i=0; i<dealerIds.size(); i++)
                {
                    if(firstIter)
                    {
                        firstIter = false;
                    }
                    else
                    {
                        query += " OR";
                    }
                    if(dealerIds.get(i).equals("All Doctor"))
                    {
                        query += " `dealer_info`.`dealer_type` = 'Doctor'";
                    }
                    else if(dealerIds.get(i).equals("All Retailer"))
                    {
                        query += " `dealer_info`.`dealer_type` = 'Retailer'";
                    }
                    else
                    {
                        query += " `order_info`.`dealer_id` = '"+dealerIds.get(i)+"'";
                    }
                }
                query += ")";
            }
        }
        else
        {
            query += " AND `order_info`.`dealer_id` = '0'";
        }


        if(!fromBookingDate.equals(""))
        {
            query += " AND str_to_date(`order_info`.`booking_date`, '%d/%b/%Y') >= str_to_date('"+fromBookingDate+"', '%d/%b/%Y')";
        }
        if(!toBookingDate.equals(""))
        {
            query += " AND str_to_date(`order_info`.`booking_date`, '%d/%b/%Y') <= str_to_date('"+toBookingDate+"', '%d/%b/%Y')";
        }
        if(!invoiceStatus.equals("All"))
        {
            query += " AND `order_info`.`status` = '"+invoiceStatus+"'";
        }
        if(!fromInvoiceNo.equals(""))
        {
            query += " AND `order_info`.`order_id` >= '"+invoiceStatus+"'";
        }
        if(!toInvoiceNo.equals(""))
        {
            query += " AND `order_info`.`order_id` <= '"+invoiceStatus+"'";
        }
        query += " GROUP BY `order_info_detailed`.`product_table_id`";
//        System.out.println(query);
        try {
            rs = stmt.executeQuery(query);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Table Id
                temp.add(rs.getString(2) == null || rs.getString(2).equals("") ? "N/A" : rs.getString(2)); // Id
                temp.add(rs.getString(3) == null || rs.getString(3).equals("") ? "N/A" : rs.getString(3)); // Name
                temp.add(rs.getString(4) == null || rs.getString(4).equals("") ? "N/A" : rs.getString(4)); // Batch No
                temp.add(rs.getString(5) == null || rs.getString(5).equals("") ? "N/A" : rs.getString(5)); // Quantity
                temp.add(rs.getString(6) == null || rs.getString(6).equals("") ? "N/A" : rs.getString(6)); // Bonus Quantity
                temp.add(rs.getString(7) == null || rs.getString(7).equals("") ? "N/A" : rs.getString(7)); // Trade Price
                temp.add(rs.getString(8) == null || rs.getString(8).equals("") ? "N/A" : rs.getString(8)); // Final Price
                temp.add(rs.getString(9) == null || rs.getString(9).equals("") ? "N/A" : rs.getString(9)); // In Stock

                if(rs.getString(10) != null && !rs.getString(10).equals(""))
                {
                    temp.add(rs.getString(10)); // Batch Expiry
                    String stEndDate = rs.getString(10);
                    String stStartDate = GlobalVariables.getStDate();

                    Date endDate = null;
                    Date startDate = null;
                    try {
                        startDate = GlobalVariables.fmt.parse(stStartDate);
                        endDate = GlobalVariables.fmt.parse(stEndDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    long diff = endDate.getTime() - startDate.getTime();
                    diff = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);

                    temp.add(String.valueOf(diff)); // days left
                }
                else
                {
                    temp.add("N/A"); // Batch Expiry
                    temp.add("N/A"); // days left
                }

                temp.add(rs.getString(11) == null || rs.getString(11).equals("") ? "N/A" : rs.getString(11)); // Difference
                drawingSummaryData.add(temp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return drawingSummaryData;
    }

    public ArrayList<ArrayList<String>> getAreasDetail(Statement stmt, Connection con)
    {
        ArrayList<ArrayList<String>> areasData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        String query = "SELECT `area_info`.`area_table_id`, `area_info`.`area_name`, `city_info`.`city_name` FROM `area_info` LEFT OUTER JOIN `city_info` ON `city_info`.`city_table_id` = `area_info`.`city_table_id` WHERE `area_info`.`area_status` != 'Deleted' ORDER BY `area_info`.`area_name`";
        try {
            rs = stmt.executeQuery(query);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Table Id
                temp.add(rs.getString(2)+" - ("+(rs.getString(3) == null ? "N/A" : rs.getString(3))+")"); // (Area + City) Name
                areasData.add(temp);
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return areasData;
    }

    public ArrayList<ArrayList<String>> getProductsDetail(Statement stmt, Connection con)
    {
        ArrayList<ArrayList<String>> productsData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        String query = "SELECT `product_info`.`product_table_id`, `product_info`.`product_name` FROM `product_info` WHERE `product_info`.`product_status` != 'Deleted' ORDER BY `product_info`.`product_name`";
        try {
            rs = stmt.executeQuery(query);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Table Id
                temp.add(rs.getString(2)); // Name
                productsData.add(temp);
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return productsData;
    }

    public ArrayList<ArrayList<String>> getDealersDetail(Statement stmt, Connection con)
    {
        ArrayList<ArrayList<String>> dealersData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        String query = "SELECT `dealer_info`.`dealer_table_id`, `dealer_info`.`dealer_name`, `dealer_info`.`dealer_address`, `dealer_info`.`dealer_type` FROM `dealer_info` WHERE `dealer_info`.`dealer_status` != 'Deleted' ORDER BY `dealer_info`.`dealer_name`";
        try {
            rs = stmt.executeQuery(query);
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1)); // Table Id
                temp.add(rs.getString(2)+" - ("+((rs.getString(3) == null || rs.getString(3).equals(""))? "N/A" : rs.getString(3))+")"); // (Name + Loc)
                temp.add(rs.getString(4)); // Type
                dealersData.add(temp);
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dealersData;
    }



    public int findIndex(String val, ArrayList<ArrayList<String>> arr)
    {
        for (int i = 0 ; i < arr.size(); i++)
            if ( arr.get(i).get(0).equals(val))
            {
                return i;
            }
        return -1;
    }
}
