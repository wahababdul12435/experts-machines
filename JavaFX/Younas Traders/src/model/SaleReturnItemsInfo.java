package model;

public class SaleReturnItemsInfo {
    private String srNo;
    private String itemNo;
    private String itemName;
    private String batchNo;
    private String itemRate;
    private String quantity;
    private String returnReason;
    private String totalAmount;
    private String totalQty;
    private String finalPrice;
    private String textAmount;

    public SaleReturnItemsInfo() {
    }

    public SaleReturnItemsInfo(String srNo, String itemNo, String itemName, String batchNo, String itemRate, String quantity, String returnReason, String totalAmount) {
        this.srNo = srNo;
        this.itemNo = itemNo;
        this.itemName = itemName;
        this.batchNo = batchNo;
        this.itemRate = itemRate;
        this.quantity = quantity;
        this.returnReason = returnReason;
        this.totalAmount = totalAmount;
    }

    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getItemNo() {
        return itemNo;
    }

    public void setItemNo(String itemNo) {
        this.itemNo = itemNo;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    public String getItemRate() {
        return itemRate;
    }

    public void setItemRate(String itemRate) {
        this.itemRate = itemRate;
    }

    public String getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(String totalQty) {
        this.totalQty = totalQty;
    }

    public String getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(String finalPrice) {
        this.finalPrice = finalPrice;
    }

    public String getTextAmount() {
        return textAmount;
    }

    public void setTextAmount(String textAmount) {
        this.textAmount = textAmount;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getReturnReason() {
        return returnReason;
    }

    public void setReturnReason(String returnReason) {
        this.returnReason = returnReason;
    }
}
