package model;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import controller.RateChange;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class RateChangeInfo {

    private String srNo;
    private String productId;
    private String productName;
    private String productBatch;
    private String productRetail;
    private String productTrade;
    private String productPurchase;
    private String productQuantities;

    private HBox operationsPane;
    private JFXButton btnView;



    public RateChangeInfo()
    {}

    public RateChangeInfo(String srNo, String productBatch, String productRetail, String productTrade, String productPurchase,String productQuantities) {
        this.srNo = srNo;
        this.productId = productId;
        this.productName = productName;
        this.productBatch = productBatch;
        this.productRetail = productRetail;
        this.productTrade = productTrade;
        this.productPurchase = productPurchase;
        this.productQuantities = productQuantities;

        this.btnView = new JFXButton();
        this.btnView.setGraphic(GlobalVariables.createViewIcon());
        this.btnView.getStyleClass().add("btn_icon");
        this.operationsPane = new HBox(btnView);

        this.btnView.setOnAction((action)-> {
            try {
                viewClicked();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
    public void viewClicked() throws IOException {
        PurchaseInvoice.changeRatePane.setVisible(false);

        RateChange.prodctsbatch = this.productBatch;
        RateChange.prodctsquantity = this.productQuantities;
        RateChange.retailRate = this.productRetail;
        RateChange.tradeRate = this.productTrade;
        RateChange.purchaseRate = this.productPurchase;

        Parent parent = null;
        try {
            parent = FXMLLoader.load(getClass().getResource("/view/rate_change.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setStyle("-fx-padding: -20 -10 -20 -10;");
        //dialogLayout.setHeading(lblUpdate);
        dialogLayout.setBody(parent);
        PurchaseInvoice.dialog = new JFXDialog(PurchaseInvoice.changeRatePane, dialogLayout, JFXDialog.DialogTransition.CENTER);

        PurchaseInvoice.changeRatePane.setVisible(true);
        PurchaseInvoice.dialog.show();
        PurchaseInvoice.dialog.setOverlayClose(false);

        //RateChange objratechange = new RateChange();
        //objratechange.viewbatchClicked();
    }


    public String getSrNo() {
        return srNo;
    }

    public void setSrNo(String srNo) {
        this.srNo = srNo;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductBatch() {
        return productBatch;
    }

    public void setProductBatch(String productBatch) {
        this.productBatch = productBatch;
    }

    public String getProductRetail() {
        return productRetail;
    }

    public void setProductRetail(String productRetail) {
        this.productRetail = productRetail;
    }

    public String getProductTrade() {
        return productTrade;
    }

    public void setProductTrade(String productTrade) {
        this.productTrade = productTrade;
    }

    public String getProductPurchase() {
        return productPurchase;
    }

    public void setProductPurchase(String productPurchase) {
        this.productPurchase = productPurchase;
    }

    public HBox getOperationsPane() {
        return operationsPane;
    }

    public void setOperationsPane(HBox operationsPane) {
        this.operationsPane = operationsPane;
    }

    public JFXButton getBtnView() {
        return btnView;
    }

    public void setBtnView(JFXButton btnView) {
        this.btnView = btnView;
    }




    private Node createViewIcon() {
        GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
        Glyph graphic = fontAwesome.create(FontAwesome.Glyph.EYE);
        graphic.setFontSize(20.0);
        return graphic;
    }


    String[] prodctRatelist = new String[1];
    public String[] get_prodDetails(Statement stmt3, Connection con3,int product_id, String batchNumber )
    {
        ResultSet rs = null;
        try {
            rs = stmt3.executeQuery("select retail_price,trade_price,purchase_price,quantity from batchwise_stock where batch_no ='"+batchNumber+"' and prod_id = '"+product_id+"'  ");
            while (rs.next())
            {
                prodctRatelist[0] = (rs.getString(1)+"--"+ rs.getString(2)+"--"+ rs.getString(3) +"--"+ rs.getString(4) );
            }

            con3.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return prodctRatelist;
    }

    String prodctname = "";
    public String get_prodNames(Statement stmt3, Connection con3,int product_id )
    {
        ResultSet rs = null;
        try {
            rs = stmt3.executeQuery("select product_name from product_info where  product_id = '"+product_id+"'  ");
            while (rs.next())
            {
                prodctname = rs.getString(1);
            }

            con3.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return prodctname;
    }

    public ArrayList<ArrayList<String>> getprodRateInfo(Statement stmt, Connection con,int prodIDs)
    {
        ArrayList<ArrayList<String>> batchRateData = new ArrayList<ArrayList<String>>();
        ArrayList<String> temp;
        ResultSet rs = null;
        try {
            rs = stmt.executeQuery("SELECT batch_no,retail_price,trade_price,purchase_price,quantity  FROM `batchwise_stock` WHERE prod_id = '"+prodIDs+"' ");
            while (rs.next())
            {
                temp = new ArrayList<String>();
                temp.add(rs.getString(1));
                temp.add(rs.getString(2));
                temp.add(rs.getString(3));
                temp.add(rs.getString(4));
                temp.add(rs.getString(5));
                batchRateData.add(temp);
            }
//            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return batchRateData;
    }
}
