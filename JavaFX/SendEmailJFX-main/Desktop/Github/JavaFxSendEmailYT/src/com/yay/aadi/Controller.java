package com.yay.aadi;

import javafx.event.ActionEvent;
import javafx.scene.control.*;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.*;
import java.util.*;

public class Controller {

    public TextField emailToField;
    public TextField emailFromField;
    public TextArea emailMessageField;
    public TextField emailSubjectField;
    public PasswordField emailPasswordField;
    public Label sentBoolValue;

    public void buttonClicked(ActionEvent actionEvent){
        sendEmail();
    }

    public void sendEmail() {
        String to = "Live.person76@gmail.com"; // emailToField.getText();
        String from = "Server.email0011@gmail.com"; // emailFromField.getText();
        String host = "smtp.gmail.com";
        final String username = "Server.email0011@gmail.com"; // emailFromField.getText();
        final String password = "123saffigujrat456"; // emailPasswordField.getText();

        //setup mail server

        Properties props = System.getProperties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.port", "587");

        Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator(){
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        try {
            //create mail
            MimeMessage m = new MimeMessage(session);
            m.setFrom(new InternetAddress(from));
            m.addRecipient(MimeMessage.RecipientType.TO, new InternetAddress(to));
            m.setSubject("Subject"); // (emailSubjectField.getText());
            m.setText("Message"); // (emailMessageField.getText());

            MimeBodyPart messageBodyPart = new MimeBodyPart();

            Multipart multipart = new MimeMultipart();

            String file = "Data/View_Company.xls";
            String fileName = "View_Company.xls";
            DataSource source = new FileDataSource(file);
            messageBodyPart.setDataHandler(new DataHandler(source));
            messageBodyPart.setFileName(fileName);
            multipart.addBodyPart(messageBodyPart);

            m.setContent(multipart);

            System.out.println("Sending");

            //send mail

            Transport.send(m);
            sentBoolValue.setVisible(true);
            System.out.println("Message sent!");

        }   catch (MessagingException e){
            e.printStackTrace();
        }

    }

}
