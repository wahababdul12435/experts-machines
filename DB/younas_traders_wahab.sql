-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 23, 2020 at 11:28 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `younas_traders`
--

-- --------------------------------------------------------

--
-- Table structure for table `batchwise_stock`
--

DROP TABLE IF EXISTS `batchwise_stock`;
CREATE TABLE IF NOT EXISTS `batchwise_stock` (
  `prod_id` int(11) NOT NULL,
  `batch_no` varchar(50) NOT NULL,
  `quantity` int(11) NOT NULL,
  `bonus` int(11) NOT NULL,
  `batch_expiry` varchar(20) NOT NULL,
  `entry_date` varchar(20) NOT NULL,
  `entry_time` varchar(15) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `batchwise_stock`
--

INSERT INTO `batchwise_stock` (`prod_id`, `batch_no`, `quantity`, `bonus`, `batch_expiry`, `entry_date`, `entry_time`) VALUES
(107, 'PMT139', 0, 0, '29/Apr/2020', '29/Apr/2020', '09:48:56'),
(4, '5AQ157', 370, 0, '29/Apr/2020', '29/Apr/2020', '09:48:56'),
(107, 'PMT376', 0, 0, '03/May/2020', '03/May/2020', '04:42:14'),
(107, 'PMT140', 0, 0, '14/May/2020', '14/May/2020', '12:12:12'),
(107, 'PMT141', 0, 0, '14/May/2020', '14/May/2020', '12:13:13'),
(107, 'PMT142', 140, 10, '14/May/2020', '14/May/2020', '12:14:14');

-- --------------------------------------------------------

--
-- Table structure for table `order_info_detailed`
--

DROP TABLE IF EXISTS `order_info_detailed`;
CREATE TABLE IF NOT EXISTS `order_info_detailed` (
  `order_id` int(8) UNSIGNED NOT NULL,
  `product_id` int(8) UNSIGNED NOT NULL,
  `batch_number` varchar(15) NOT NULL,
  `quantity` int(8) NOT NULL,
  `submission_quantity` int(8) NOT NULL,
  `unit` varchar(30) NOT NULL,
  `submission_unit` varchar(30) NOT NULL,
  `order_price` float NOT NULL,
  `bonus_quant` int(11) NOT NULL,
  `discount` float NOT NULL,
  `final_price` float NOT NULL,
  `cmplt_returned_bit` tinyint(1) NOT NULL,
  `returned_quant` int(11) NOT NULL,
  `returned_bonus_quant` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_info_detailed`
--

INSERT INTO `order_info_detailed` (`order_id`, `product_id`, `batch_number`, `quantity`, `submission_quantity`, `unit`, `submission_unit`, `order_price`, `bonus_quant`, `discount`, `final_price`, `cmplt_returned_bit`, `returned_quant`, `returned_bonus_quant`) VALUES
(1, 107, 'PMT139', 60, 60, 'Packs', 'Packs', 13734.6, 0, 0, 13734.6, 0, 0, 0),
(1, 107, 'PMT376', 90, 90, 'Packs', 'Packs', 20601.9, 0, 0, 20601.9, 0, 0, 0),
(1, 107, 'PMT140', 60, 60, 'Packs', 'Packs', 9156.4, 0, 0, 9156.4, 0, 0, 0),
(2, 4, '5AQ157', 10, 10, 'Packs', 'Packs', 984, 0, 0, 984, 0, 0, 0),
(2, 107, 'PMT141', 10, 10, 'Packs', 'Packs', 1945.7, 0, 0, 1945.7, 0, 0, 0),
(3, 4, '5AQ157', 10, 10, 'Packs', 'Packs', 984, 0, 0, 984, 0, 0, 0),
(3, 107, 'PMT141', 10, 10, 'Packs', 'Packs', 1945.7, 0, 0, 1945.7, 0, 0, 0),
(4, 4, '5AQ157', 10, 10, 'Packs', 'Packs', 885.6, 0, 10, 787.2, 0, 0, 0),
(5, 4, '5AQ157', 10, 10, 'Packs', 'Packs', 885.6, 0, 10, 787.2, 0, 0, 0),
(5, 107, 'PMT141', 10, 10, 'Packs', 'Packs', 1751.13, 0, 10, 1556.56, 0, 0, 0),
(6, 107, 'PMT141', 10, 10, 'Packs', 'Packs', 1556.56, 0, 20, 1167.42, 0, 0, 0),
(6, 4, '5AQ157', 10, 10, 'Packs', 'Packs', 885.6, 0, 10, 787.2, 0, 0, 0),
(7, 107, 'PMT142', 10, 10, 'Packs', 'Packs', 1751.13, 0, 10, 1556.56, 0, 0, 0),
(7, 4, '5AQ157', 10, 10, 'Packs', 'Packs', 885.6, 0, 10, 787.2, 0, 0, 0),
(8, 107, 'PMT142', 10, 10, 'Packs', 'Packs', 1945.7, 0, 0, 1945.7, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `order_return_detail`
--

DROP TABLE IF EXISTS `order_return_detail`;
CREATE TABLE IF NOT EXISTS `order_return_detail` (
  `return_id` int(11) NOT NULL,
  `prod_id` int(11) NOT NULL,
  `prod_batch` varchar(15) NOT NULL,
  `prod_quant` int(11) NOT NULL,
  `bonus_quant` int(11) NOT NULL,
  `discount_amount` float NOT NULL,
  `total_amount` float NOT NULL,
  `Unit` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_info`
--

DROP TABLE IF EXISTS `purchase_info`;
CREATE TABLE IF NOT EXISTS `purchase_info` (
  `purchase_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `comp_id` int(10) NOT NULL,
  `invoice_num` varchar(11) NOT NULL,
  `supplier_id` int(11) UNSIGNED NOT NULL,
  `purchase_date` varchar(20) NOT NULL,
  `gross_amount` float NOT NULL,
  `disc_amount` float NOT NULL,
  `net_amount` float NOT NULL,
  PRIMARY KEY (`purchase_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_info`
--

INSERT INTO `purchase_info` (`purchase_id`, `comp_id`, `invoice_num`, `supplier_id`, `purchase_date`, `gross_amount`, `disc_amount`, `net_amount`) VALUES
(1, 0, '1880', 1, '29/Apr/2020', 226700, 0, 226700),
(2, 0, '2541', 1, '03/May/2020', 18095, 0, 18095),
(3, 0, '4568', 1, '21/May/2020', 18095, 0, 18095);

-- --------------------------------------------------------

--
-- Table structure for table `purchase_info_detail`
--

DROP TABLE IF EXISTS `purchase_info_detail`;
CREATE TABLE IF NOT EXISTS `purchase_info_detail` (
  `invoice_num` varchar(15) NOT NULL,
  `purchase_id` int(10) NOT NULL,
  `prod_id` int(11) NOT NULL,
  `discount` float NOT NULL,
  `bonus_quant` int(11) NOT NULL,
  `recieve_quant` int(11) NOT NULL,
  `batch_no` varchar(20) NOT NULL,
  `expiry_date` varchar(20) NOT NULL,
  `gross_amount` float NOT NULL,
  `disc_amount` float NOT NULL,
  `net_amount` float NOT NULL,
  `invoice_date` varchar(20) NOT NULL,
  `returnBit` tinyint(1) NOT NULL,
  `returned_quant` int(11) NOT NULL,
  `returned_bonus_quant` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_info_detail`
--

INSERT INTO `purchase_info_detail` (`invoice_num`, `purchase_id`, `prod_id`, `discount`, `bonus_quant`, `recieve_quant`, `batch_no`, `expiry_date`, `gross_amount`, `disc_amount`, `net_amount`, `invoice_date`, `returnBit`, `returned_quant`, `returned_bonus_quant`) VALUES
('1880', 1, 107, 0, 0, 1000, 'PMT139', '29/Apr/2020', 180950, 0, 180950, '29/Apr/2020', 0, 0, 0),
('1880', 1, 4, 0, 0, 500, '5AQ157', '29/Apr/2020', 45750, 0, 45750, '29/Apr/2020', 0, 0, 0),
('2541', 2, 107, 0, 0, 100, 'PMT376', '03/May/2020', 18095, 0, 18095, '03/May/2020', 0, 0, 0),
('4568', 3, 107, 0, 10, 100, 'PMT142', '21/May/2020', 18095, 0, 18095, '21/May/2020', 0, 0, 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
