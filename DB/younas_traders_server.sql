-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 06, 2022 at 09:38 AM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `younas_traders`
--

-- --------------------------------------------------------

--
-- Table structure for table `area_info`
--

DROP TABLE IF EXISTS `area_info`;
CREATE TABLE IF NOT EXISTS `area_info` (
  `area_table_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `area_id` int(10) UNSIGNED DEFAULT NULL,
  `area_name` varchar(250) NOT NULL,
  `area_abbrev` varchar(250) NOT NULL,
  `city_table_id` int(10) UNSIGNED NOT NULL,
  `area_status` varchar(50) NOT NULL,
  `creating_user_id` int(10) UNSIGNED NOT NULL,
  `creating_date` varchar(20) NOT NULL,
  `creating_time` varchar(20) NOT NULL,
  `update_user_id` int(10) UNSIGNED NOT NULL,
  `update_date` varchar(20) NOT NULL,
  `update_time` varchar(20) NOT NULL,
  PRIMARY KEY (`area_table_id`),
  UNIQUE KEY `area_id` (`area_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `batchwise_stock`
--

DROP TABLE IF EXISTS `batchwise_stock`;
CREATE TABLE IF NOT EXISTS `batchwise_stock` (
  `batch_stock_id` int(10) NOT NULL AUTO_INCREMENT,
  `product_table_id` int(11) NOT NULL,
  `batch_no` varchar(50) NOT NULL,
  `quantity` int(11) NOT NULL,
  `bonus` int(11) NOT NULL,
  `retail_price` float NOT NULL,
  `trade_price` float NOT NULL,
  `purchase_price` float NOT NULL,
  `batch_expiry` varchar(20) NOT NULL,
  `entry_date` varchar(20) NOT NULL,
  `entry_time` varchar(15) NOT NULL,
  `mobile_sync` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`batch_stock_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bonus_policy`
--

DROP TABLE IF EXISTS `bonus_policy`;
CREATE TABLE IF NOT EXISTS `bonus_policy` (
  `bonus_policyID` int(11) NOT NULL AUTO_INCREMENT,
  `approval_id` varchar(30) NOT NULL,
  `dealer_table_id` int(11) DEFAULT NULL,
  `company_table_id` int(11) UNSIGNED DEFAULT NULL,
  `product_table_id` int(11) UNSIGNED NOT NULL,
  `quantity` int(11) NOT NULL,
  `bonus_quant` int(11) NOT NULL,
  `start_date` varchar(50) NOT NULL,
  `end_date` varchar(50) NOT NULL,
  `policy_status` varchar(50) NOT NULL,
  `entered_by` int(11) NOT NULL,
  `entry_date` varchar(50) NOT NULL,
  `entry_time` varchar(50) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `update_date` varchar(50) NOT NULL,
  `update_time` varchar(50) NOT NULL,
  PRIMARY KEY (`bonus_policyID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cash_collected`
--

DROP TABLE IF EXISTS `cash_collected`;
CREATE TABLE IF NOT EXISTS `cash_collected` (
  `cash_collection_id` int(11) NOT NULL AUTO_INCREMENT,
  `dealer_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `cash_collection_date` varchar(50) NOT NULL,
  `total_payment` float NOT NULL,
  `cash_received` float NOT NULL,
  PRIMARY KEY (`cash_collection_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `city_info`
--

DROP TABLE IF EXISTS `city_info`;
CREATE TABLE IF NOT EXISTS `city_info` (
  `city_table_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `city_id` int(10) UNSIGNED DEFAULT NULL,
  `district_table_id` int(10) UNSIGNED DEFAULT NULL,
  `city_name` varchar(100) NOT NULL,
  `city_status` varchar(20) NOT NULL,
  `creating_user_id` int(11) UNSIGNED NOT NULL,
  `creating_date` varchar(30) NOT NULL,
  `creating_time` varchar(30) NOT NULL,
  `update_user_id` int(10) UNSIGNED NOT NULL,
  `update_date` varchar(20) NOT NULL,
  `update_time` varchar(20) NOT NULL,
  PRIMARY KEY (`city_table_id`),
  UNIQUE KEY `city_id` (`city_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `company_alternate_contacts`
--

DROP TABLE IF EXISTS `company_alternate_contacts`;
CREATE TABLE IF NOT EXISTS `company_alternate_contacts` (
  `contact_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_id` int(10) UNSIGNED NOT NULL,
  `contact_name` varchar(150) NOT NULL,
  `contact_number` varchar(60) NOT NULL,
  PRIMARY KEY (`contact_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `company_info`
--

DROP TABLE IF EXISTS `company_info`;
CREATE TABLE IF NOT EXISTS `company_info` (
  `company_table_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `company_name` varchar(200) NOT NULL,
  `company_address` varchar(300) NOT NULL,
  `company_city` varchar(50) NOT NULL,
  `company_contact` varchar(100) NOT NULL,
  `contact_Person` varchar(100) NOT NULL,
  `company_email` varchar(100) NOT NULL,
  `company_image` varchar(300) DEFAULT NULL,
  `company_status` varchar(50) NOT NULL,
  `creating_user_id` int(11) NOT NULL,
  `creating_date` varchar(30) NOT NULL,
  `creating_time` varchar(30) NOT NULL,
  `update_user_id` int(11) NOT NULL,
  `update_date` varchar(30) NOT NULL,
  `update_time` varchar(30) NOT NULL,
  PRIMARY KEY (`company_table_id`),
  UNIQUE KEY `company_id` (`company_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_info`
--

INSERT INTO `company_info` (`company_table_id`, `company_id`, `company_name`, `company_address`, `company_city`, `company_contact`, `contact_Person`, `company_email`, `company_image`, `company_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`) VALUES
(1, 4, 'High Q Pharmaceuticals', 'Karachi', 'Karachi', '', '', '', NULL, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '30/Nov/2021', '11:39 AM'),
(2, 5, 'Vida Laboratories', 'Karachi', 'Karachi', '021-563658', '', '', NULL, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '30/Nov/2021', '11:47 AM');

-- --------------------------------------------------------

--
-- Table structure for table `company_overall_record`
--

DROP TABLE IF EXISTS `company_overall_record`;
CREATE TABLE IF NOT EXISTS `company_overall_record` (
  `company_overall_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_id` int(10) UNSIGNED NOT NULL,
  `orders_given` int(10) UNSIGNED NOT NULL,
  `successful_orders` int(10) UNSIGNED NOT NULL,
  `ordered_packets` int(10) UNSIGNED NOT NULL,
  `received_packets` int(10) UNSIGNED NOT NULL,
  `ordered_boxes` int(10) UNSIGNED NOT NULL,
  `received_boxes` int(10) UNSIGNED NOT NULL,
  `order_price` float NOT NULL,
  `discount_price` float NOT NULL,
  `invoiced_price` float NOT NULL,
  `cash_sent` float NOT NULL,
  `return_packets` int(10) NOT NULL,
  `return_boxes` int(10) NOT NULL,
  `return_price` float NOT NULL,
  `cash_return` float NOT NULL,
  `waived_off_price` float NOT NULL,
  `pending_payments` float NOT NULL,
  PRIMARY KEY (`company_overall_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_overall_record`
--

INSERT INTO `company_overall_record` (`company_overall_id`, `company_id`, `orders_given`, `successful_orders`, `ordered_packets`, `received_packets`, `ordered_boxes`, `received_boxes`, `order_price`, `discount_price`, `invoiced_price`, `cash_sent`, `return_packets`, `return_boxes`, `return_price`, `cash_return`, `waived_off_price`, `pending_payments`) VALUES
(1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `company_payments`
--

DROP TABLE IF EXISTS `company_payments`;
CREATE TABLE IF NOT EXISTS `company_payments` (
  `payment_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_id` int(10) UNSIGNED NOT NULL,
  `invoice_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `amount` float NOT NULL,
  `cash_type` varchar(10) NOT NULL,
  `pending_payments` float UNSIGNED NOT NULL,
  `date` varchar(30) NOT NULL,
  `day` varchar(15) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `status` varchar(30) NOT NULL,
  PRIMARY KEY (`payment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dealer_day_summary`
--

DROP TABLE IF EXISTS `dealer_day_summary`;
CREATE TABLE IF NOT EXISTS `dealer_day_summary` (
  `dealer_summary_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `date` varchar(20) NOT NULL,
  `day` varchar(15) NOT NULL,
  `booking_order` int(10) UNSIGNED NOT NULL,
  `delivered_order` int(10) UNSIGNED NOT NULL,
  `returned_order` int(10) UNSIGNED NOT NULL,
  `return_quantity` int(10) NOT NULL,
  `return_price` float NOT NULL,
  `cash_collection` float NOT NULL,
  `cash_return` float NOT NULL,
  `cash_waiveoff` float NOT NULL,
  PRIMARY KEY (`dealer_summary_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dealer_gps_location`
--

DROP TABLE IF EXISTS `dealer_gps_location`;
CREATE TABLE IF NOT EXISTS `dealer_gps_location` (
  `dealer_loc_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `latitude` varchar(25) NOT NULL,
  `longitude` varchar(25) NOT NULL,
  `loc_name` varchar(150) NOT NULL,
  `entry_date` varchar(20) NOT NULL,
  `entry_time` varchar(20) NOT NULL,
  `update_date` varchar(20) NOT NULL,
  `update_time` varchar(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`dealer_loc_id`),
  UNIQUE KEY `dealer_id` (`dealer_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dealer_info`
--

DROP TABLE IF EXISTS `dealer_info`;
CREATE TABLE IF NOT EXISTS `dealer_info` (
  `dealer_table_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `dealer_id` int(10) UNSIGNED DEFAULT NULL,
  `dealer_area_id` int(10) UNSIGNED NOT NULL,
  `dealer_name` varchar(200) NOT NULL,
  `dealer_contact_person` varchar(100) NOT NULL,
  `dealer_phone` varchar(50) NOT NULL,
  `dealer_fax` varchar(50) NOT NULL,
  `dealer_address` varchar(250) NOT NULL,
  `dealer_address1` varchar(250) NOT NULL,
  `dealer_type` varchar(30) NOT NULL,
  `dealer_cnic` varchar(30) NOT NULL,
  `dealer_ntn` varchar(70) NOT NULL,
  `dealer_image` varchar(300) DEFAULT NULL,
  `dealer_lic9_num` varchar(80) NOT NULL,
  `dealer_lic9_exp` varchar(50) NOT NULL,
  `dealer_lic10_num` varchar(50) NOT NULL,
  `dealer_lic10_Exp` varchar(50) NOT NULL,
  `dealer_lic11_num` varchar(50) NOT NULL,
  `dealer_lic11_Exp` varchar(50) NOT NULL,
  `dealer_credit_limit` int(11) NOT NULL,
  `dealer_status` varchar(50) NOT NULL,
  `creating_user_id` int(10) UNSIGNED NOT NULL,
  `creating_date` varchar(20) NOT NULL,
  `creating_time` varchar(20) NOT NULL,
  `update_user_id` int(10) UNSIGNED NOT NULL,
  `update_date` varchar(20) NOT NULL,
  `update_time` varchar(20) NOT NULL,
  PRIMARY KEY (`dealer_table_id`),
  UNIQUE KEY `dealer_id` (`dealer_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1759 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dealer_info`
--

INSERT INTO `dealer_info` (`dealer_table_id`, `dealer_id`, `dealer_area_id`, `dealer_name`, `dealer_contact_person`, `dealer_phone`, `dealer_fax`, `dealer_address`, `dealer_address1`, `dealer_type`, `dealer_cnic`, `dealer_ntn`, `dealer_image`, `dealer_lic9_num`, `dealer_lic9_exp`, `dealer_lic10_num`, `dealer_lic10_Exp`, `dealer_lic11_num`, `dealer_lic11_Exp`, `dealer_credit_limit`, `dealer_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`) VALUES
(1, 11, 1, 'ATTIQ MEDICAL STORE', '', '053-3513450', '', 'CIRCULAR ROAD', 'GUJRAT', 'Whole Saler', '3420174263657', '0402645-4', '', '0', '0', '0', '1/1/2017 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(2, 1102, 1, 'DR.ABDUL-MUSTFA', '', '0305-5169708', '', '', 'SHIFA HOSPITAL GUJRAT', 'Doctor', '34201-8773014-9', '1225328', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(3, 13, 1, 'ATTIQ MEDICAL STORE (2)', '', '', '', 'SHAH FASIAL GATE', 'GUJRAT', 'Retailer', '', '', '', '0', '272', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(4, 518, 5, 'DR.ALLAH-DAD CHEEMA', '', '', '', '', 'CHEEMA HOSPITAL GUJRAT', 'Doctor', '', '0401520-7', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(5, 26, 2, 'PHARMACY, ALI CLINIC', '', '', '', 'G.T.ROAD', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(6, 17, 1, 'BUKHARI MEDICAL STORE', '', '', '', 'NEAR CHOWK PAKISTAN', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(7, 48, 4, 'NEW MEHAR MEDICAL STORE', '', '', '', 'AHSAN PLAZA COURT ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(8, 163, 1, 'AL HASSAN MEDICAL STORE', '', '03219626800', '', 'MUSLIM ABAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '0', '12/31/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(9, 18, 1, 'JEDDAH PHARMACIE', '', '053-3510621', '', 'NEAR KABLE GATE', 'GUJRAT', 'Retailer', '34201-6861736-5', '5422145-5', '', '45742', '0', '0', '10/10/2021 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(10, 164, 1, 'NEW SERWER MEDICAL STORE', '', '', '', 'JINAH ROAD', 'GUJRAT', 'Retailer', '34201-0422378-5', '6499775-2', '', '0', '247', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(11, 813, 8, 'DR.M.ZEESHAN ANWAR', '', '', '', 'PMDC# 53380-P', 'FAZEELAT AMIN HOSPITAL GUJRAT', 'Doctor', '', '', '', '53380', '0', '0', '12/31/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(12, 138, 1, 'DR.ASJAD FAROOQ AHMED', '', '0336-6227400', '', 'PMDC# 6837-P', 'CITY HOSPITAL GUJRAT', 'Doctor', '34201-0324706-8', '4089875', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(13, 416, 4, 'PHARMACY,HAIDER MEDICAL COMPLEX', '', '', '', 'JINAH ROAD', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(14, 525, 5, 'DR.TAHIR RASHID', '', '053-3606616', '', 'PMDC# 4222-P', 'POLY CLINIC Hp GUJRAT', 'Doctor', '34201-0434995-9', '8279864-8', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(15, 118, 1, 'WRIACH MEDICAL STORE', '', '', '', 'SABUWAL ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(16, 119, 1, 'FAZAL SONS PHARMACY', '', '', '', 'PRINCE CHOWK', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(17, 165, 1, 'DR.M.ZAMAN MIRZA', '', '', '', '', 'ALI CHILDREN Hp GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(18, 94, 9, 'AL HAIDER MEDICAL STORE', '', '03216291539', '', 'ADJACENT MASTER M ALTAF C/H', 'RALWAY ROAD GUJRAT', 'Retailer', '', '2291312-2', '', '0', '0', '', '12/30/1899 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(19, 529, 5, 'SOBER PHARMACY', '', '03224009080', '', 'NEAR GUJRAT HOSPITAL', 'BHIMBER ROAD GUJRAT', 'Retailer', '34201-0407250-9', '1459895-7', '', '0', '12031', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(20, 123, 1, 'NEW PUNJAB MEDICAL STORE', '', '', '', 'BHIMBER ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(21, 530, 5, 'DR.MIR ZAHID ZAHEER', '', '0333-8445678', '0300 9620732', 'PMDC# 7247-P', 'Gujrat Hospital GUJRAT', 'Doctor', '34201-1692229-9', '0406050-4', '', '7247', '0', '0', '12/31/2023 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(22, 568, 5, 'PHARMACY,DR.IQBAL JAWAD GOHER', '', '', '', 'BHIMBER ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(23, 1101, 1, 'DR.HAIDER ASADULLAH MALIK', '', '', '', 'PMDC# 3032-P', 'Malik Haider Hospital GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(24, 427, 4, 'PHARMACY,MEDICARE HOSPITAL', '', '', '', 'BHIMBER ROAD', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(25, 531, 5, 'DR.SHEHZAD CH.', '', '0315-6200777', '', '', 'NEW FAMILY Hp GUJRAT', 'Doctor', '34201-0573947-9', '0404847-4', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(26, 532, 5, '.RAZA PHARMACY', '', '03009625301', '', 'OPP. CHANAB HOSPITAL', 'GUJRAT', 'Retailer', '', '', '', '1401', '0', '0', '8/1/2017 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(27, 130, 1, 'USMAN MEDICAL STORE', '', '', '', 'SHAH HUSSAIN ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(28, 131, 1, 'SUFIAN MEDICAL STORE', '', '', '', 'JINAH ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(29, 142, 1, 'DR.AZHAR MEHMOOD MIRZA', '', '', '', 'MIRZA HOSPITAL', 'KACHERY CHOWK GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(30, 166, 1, 'CENTRAL MEDICAL STORE', '', '053-3510488', '', 'STREET NO.1,LINK JINNAH ROAD', 'GUJRAT', 'Retailer', '34201-0524154-3', '', '', '37992', '37992', '37992', '12/22/2020 12:00 AM', '12/22/2020 12:00 AM', '12/22/2020 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(31, 143, 1, 'NEW AZIZ BHATTI PHARMACY', '', '03006206991', '', 'COURT ROAD', 'GUJRAT', 'Retailer', '', '', '', '1328', '0', '0', '6/30/2016 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(32, 167, 1, 'SADAAT MEDICAL STORE', '', '03338526249', '', 'KHAWAJGAN ROAD', 'CHOWK PAKISTAN GUJRAT', 'Retailer', '34201-0377578-3', '1285206-6', '', '0', '27022', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(33, 112, 1, 'HM PHARMACY', '', '0321-6217605', '', 'Opp.Zahoor Palace', 'GUJRAT', 'Retailer', '34201-8008819-1', '2266402-5', '', '34643', '0', '0', '6/25/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(34, 144, 1, 'MUQADAS MEDICAL STORE', '', '', '', 'COURT ROAD', 'GUJRAT', 'Retailer', '', '', '', '692', '0', '692', '12/31/2015 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(35, 168, 1, 'LIAQAT PHARMACY', '', '', '', 'NEAR MEEZAN BANK', 'CHOWK PAKISTAN GUJRAT', 'Retailer', '', '', '', '25030', '0', '0', '1/29/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(36, 113, 1, 'ZAHID MEDICAL STORE', '', '', '', 'Near Zahoor Palace', 'East Circular Road GUJRAT', 'Retailer', '34201-0370622-9', '0402530-0', '', '26522', '0', '819', '12/28/2019 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(37, 145, 1, 'DR.ABDUL QAYOOM', '', '', '', 'MARYAM HOSPITAL', 'REHMAN SHAHED ROAD GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(38, 943, 9, 'DR.ZAFAR IQBAL', '', '', '', 'PMDC# 23131-P', 'RIMSHA HOSPITAL GUJRAT', 'Doctor', '', '', '', '23131', '0', '0', '12/31/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(39, 19, 1, 'SHEIKH PHARMACY', '', '03006231941', '', 'NEAR GUJRAT JEWELLERS', 'MUSLIM BAZAR GUJRAT', 'Retailer', '34201-1777581-5', '2434486-9', '', '16943', '0', '0', '10/24/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(40, 115, 1, 'NEW JET PHARMACY', '', '0300-9627028', '', 'EAST CIRCULAR ROAD', 'GUJRAT', 'Retailer', '34201-8115993-9', '1528011-0', '', '15504', '0', '0', '10/24/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(41, 116, 1, '.ROYAL MEDICAL STORE', '', '', '', '', 'GUJRAT', 'Retailer', '34201-0555462-3', '26173000', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(42, 1105, 1, 'DR.M.MOHSIN SANDHU & SONS', '', '053-3533260', '', 'TIMBLE BAZAR', 'GUJRAT', 'Retailer', '', '', '', '0', '5522', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(43, 146, 1, 'AMMAR MEDICAL STORE', '', '', '', 'FAWARA CHOWK', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(44, 117, 1, 'Naseem Medical Store', '', '0321-6205455', '', 'Out Side Asif Hospital', 'Fawara Chowk GUJRAT', 'Retailer', '34201-3096324-5', '1289940-2', '', '0', '9891', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(45, 74, 7, 'KHATANA MEDICAL STORE', '', '0300 6207279', '', 'OOP.AZIZ BHATTI SHAHEED Hp', 'GUJRAT', 'Retailer', '3420178511657', '5409883-1', '', '0', '7548', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(46, 12, 1, 'AL-RIAZ MEDICAL STORE', '', '03006201596', '', 'SHAH FAISAL GATE', 'GUJRAT', 'Retailer', '34201-0932996-3', '1755183-8', '', '0', '721', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(47, 120, 1, 'NAEEM MEDICAL STORE', '', '053-3524205', '', 'SHAHDULA ROAD', 'GUJRAT', 'Retailer', '34201-0357522-9', '1633552-0', '', '403', '0', '403', '12/31/2015 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(48, 95, 9, 'NEW AFTAB MEDICAL STORE', '', '', '', 'NEAR ANSARI CLOTH HOUSE', 'SARGODHA ROAD GUJRAT', 'Retailer', '34201-0472303-1', '1460003-0', '', '0', '7889', '0', '12/31/2015 12:00 AM', '9/29/2019 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(49, 5132, 5, 'AL-AZIZ MEDICAL STORE', '', '', '', 'G.T.ROAD', 'GUJRAT', 'Retailer', '', '', '', '343', '0', '0', '12/31/2013 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(50, 96, 9, 'BILAL MEDICAL STORE', '', '03216213521', '', 'OPP.RAJA BAKERS', 'RAILWAY ROAD GUJRAT', 'Retailer', '34201-0488392-5', '0401630-7', '', '0', '3820', '0', '12/31/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(51, 981, 9, 'JAMEEL MEDICAL STORE', '', '', '', 'RAILWAY ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '0', '12/31/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(52, 960, 9, 'ABDULAH MEDICAL STORE', '', '03009624484', '', 'RAILWAY ROAD', 'GUJRAT', 'Retailer', '34201-0544283-1', '3778567-2', '', '0', '305', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(53, 161, 1, 'HYDER MEDICAL STORE', '', '', '', 'RAILWAY ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(54, 99, 9, 'DR.M.AMIN GUL', '', '0333-8403202', '', 'PMDC NO.10775-P', 'INAYAT HOSPITAL GUJRAT', 'Doctor', '34201-0348034-3', '0401773-7', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(55, 910, 9, '.REHMAN MEDICAL STORE', '', '0332 8387221', '', 'NEAR SITTARA BAKERS', 'SARGODHA ROAD GUJRAT', 'Retailer', '34201-8971173-5', '5630660-8', '', '0', '5685', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(56, 911, 9, 'QAUMI MEDICAL STORE', '', '', '', 'NEAR CH.KHADIM KARYANA STORE', 'STAFF GALA GUJRAT', 'Retailer', '34201-0513717-9', '1632918-0', '', '0', '13498', '0', '5/26/0214 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(57, 912, 9, 'NEW SULMAN MEDICAL STORE', '', '03076231572', '', 'SARGODHA ROAD', 'GUJRAT', 'Retailer', '', '5348687-5', '', '891', '0', '891', '12/31/2015 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(58, 913, 9, 'NASEER PHARMACY', '', '0333-8452091', '', 'STAAF GALA,SARGODHA ROAD', 'GUJRAT', 'Retailer', '34201-2635558-9', 'A322964-8', '', '68107', '0', '0', '3/15/2023 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(59, 21, 2, 'AL-MUSLIM MEDICAL STORE', '', '', '', 'D.H.Q. HOSPITAL', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(60, 22, 2, 'DEWAN MEDICAL STORE', '', '', '', 'D.H.Q HOSPITAL', 'MANDI BHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(61, 23, 2, 'AHSAN MEDICAL STORE', '', '', '', 'D.H.Q. HOSPITAL', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(62, 24, 2, 'ITTIFAQ MEDICAL STORE', '', '', '', 'D.H.Q HOSPITAL', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(63, 25, 2, 'SOHAIL MEDICAL STORE', '', '', '', 'D.H.Q HOSPITAL', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(64, 28, 2, 'AHMED-WAQAS MEDICAL STORE', '', '', '', 'D.H.Q.HOSPITAL', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(65, 29, 2, 'SHAHID MEDICAL STORE', '', '', '', 'KACHARY ROAD', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(66, 212, 2, 'SHAUKET MEDICAL HALL', '', '', '', 'OPP.D.H.Q.', 'MANDI BHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(67, 214, 2, 'AZEEM MEDICAL STORE', '', '', '', '', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(68, 215, 2, 'FAIZ MEDICAL STORE', '', '', '', 'SADDER BAZAR', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(69, 218, 2, 'PANJAB MEDICAL STORE', '', '', '', 'SADDER BAZAR', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(70, 222, 2, 'MIAN MEDICAL STORE', '', '', '', 'COMMETTE BAZAR', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(71, 224, 2, 'AL GONDAL MEDICAL STORE', '', '', '', 'D.H.Q.ROAD', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(72, 225, 2, 'PHARMACY,RAKHAT HOSPITAL', '', '', '', '', 'MANDI BAHAUDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(73, 226, 2, 'MUMTAZ MEDICINE', '', '', '', 'PHALIA ROAD', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(74, 227, 2, 'PAK MEDICAL STORE', '', '', '', 'PHALIA ROAD', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(75, 228, 2, 'ABUBAKAR MEDICAL STORE', '', '', '', 'PHALIA ROAD', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(76, 229, 2, 'KHALEEL MEDICAL STORE', '', '', '', 'PHALIA ROAD', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(77, 230, 2, 'PHARMACY,ALVI HOSPITAL', '', '', '', 'PHALIA ROAD', 'MANDI BAHAUDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(78, 231, 2, 'TAHIR MADICAL STORE', '', '', '', 'KACHARY ROAD', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(79, 233, 2, 'MIAN MEDICINE', '', '', '', 'KEHEHRY ROAD', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(80, 234, 2, 'PHARMACY,NAZEER GONDAL H.S', '', '', '', 'RASOOL ROAD', 'MANDI BAHAUDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(81, 235, 2, 'SHAHEEN MEDICAL STORE', '', '', '', 'RASDDL ROAD', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(82, 236, 2, 'NASIR MADICAL STORE', '', '', '', 'RASOOL ROAD', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(83, 240, 2, 'HASSAN MEDICAL STORE', '', '', '', 'KACHARY ROAD', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(84, 237, 2, 'AL SHAFA MEDICINE CO', '', '', '', 'KACHARY ROAD', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(85, 239, 2, 'STANDERD MEDICAL STORE', '', '', '', 'KING ROAD', 'MANDI BHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(86, 31, 3, 'IMTIAZ MEDICAL STORE', '', '', '', 'T.H.Q.ROAD', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(87, 32, 3, 'NWE MUNIR MADICAL STORE', '', '', '', 'T.H.Q.ROAD', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(88, 33, 3, 'SALEEME MADICAL STORE', '', '', '', 'T.H.Q.ROAD', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(89, 34, 3, 'PUBLIC MADICAL HALL', '', '', '', 'T.H.Q.ROAD', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(90, 35, 3, 'SHARIF MADICAL STORE', '', '', '', 'T.H.Q.ROAD', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(91, 36, 3, 'NISAR MADICAL STORE', '', '', '', 'T.H.Q.ROAD', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(92, 37, 3, 'AL-SHAFA MADICAL STORE', '', '', '', 'T.H.Q.ROAD', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(93, 38, 3, 'ZAMAN MADICAL STORE', '', '', '', 'KHANIAN ROAD', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(94, 39, 3, 'FEIZAN MADICAL STORE', '', '', '', 'MAIN ROAD', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(95, 42, 4, 'MANGET MADICAL STORE', '', '', '', 'MAIN BAZAR', 'PHARIANWALI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(96, 43, 4, 'AL-MUSLIM MADICAL STORE', '', '', '', 'MAIN BAZAR', 'PHARIANWALI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(97, 44, 4, 'SHAHEEN MADICAL STORE', '', '', '', 'MAIN BAZAR', 'PHARIANWALI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(98, 45, 4, 'ANWER MADICAL STORE', '', '', '', 'OPP-B.H.U.', 'PHARIANWALI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(99, 141, 14, 'TARIQ MEDICAL STORE', '', '', '', 'NEAR RAILWAYS', 'SARI ALAMGIR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(100, 147, 14, 'NEW IQBAL MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'SARI ALAMGER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(101, 149, 14, 'ANSAR MEDICAL STORE', '', '', '', 'G.T.ROAD', 'SARI ALAMGER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(102, 81, 8, 'BAIGA MEDICAL STORE', '', '', '', 'SADDER BAZAR', 'KHARIAN CANT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(103, 82, 8, 'HUSSAIN MEDICAL STORE', '', '', '', 'SADDER BAZAR', 'KHARIAN CANT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(104, 93, 9, 'DAR MEDICAL STORE', '', '', '', 'GULYANA ROAD', 'KHARIAN CITY', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(105, 83, 8, 'PUNJEB MEDICAL STORE', '', '', '', 'SADDER BAZAR', 'KHARIAN CANT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(106, 85, 8, 'NEW PAK MEDICAL STORE', '', '', '', 'SADDER BAZAR', 'KHARIAN CANT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(107, 92, 9, 'AMER MEDICAL STORE', '', '', '', 'GULYANA ROAD', 'KHARIAN CITY', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(108, 914, 9, 'TARIQ MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'KHARIAN CITY', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(109, 916, 9, 'DASI DAWAKHANA', '', '', '', 'MAIN BAZAR', 'KHARIAN CITY', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(110, 917, 9, 'KHARIAN MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'KHARIAN CITY', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(111, 71, 7, 'KHALID MEDICAL STORE', '', '', '', 'DINACHAKIAN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(112, 72, 7, 'TARIQ MEDICAL STORE', '', '', '', 'DINACHAKIAN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(113, 73, 7, 'HAFIZ SONS', '', '', '', 'GHALAH MANDI', 'LALAMUSA', 'G', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(114, 75, 7, 'SATHI MEDICAL STORE', '', '', '', 'NEAR GHALA MANDI', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(115, 76, 7, 'AL-REHMAN MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(116, 77, 7, 'CHISTE MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(117, 78, 7, 'MADINA MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(118, 710, 7, 'ABUBAKER MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(119, 712, 7, 'NEW ZAM ZAM MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(120, 713, 7, 'NEW RAFIQ MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(121, 714, 7, 'NEW KAMAL MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(122, 715, 7, 'HAIDER MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(123, 716, 7, 'FAROOQ MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(124, 717, 7, 'FORMASALE MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(125, 720, 7, 'JALAL MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(126, 721, 7, 'NAEEM MEDICAL CENTER', '', '', '', 'G.T.ROAD', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(127, 722, 7, 'FARYAD MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'DEONAMANDI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(128, 723, 7, 'SHAMI MEDICAL STORE', '', '', '', 'G.T.ROAD', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(129, 724, 7, 'AL-MURTAZA MEDICAL STORE', '', '', '', 'G.T.ROAD', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(130, 725, 7, 'BUKHARI MEDICAL STORE', '', '', '', 'G.T.ROAD', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(131, 726, 7, 'LIFE MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(132, 727, 7, 'MEDICAR MEDICAL STORE', '', '', '', 'G.T.ROAD', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(133, 728, 7, 'ZUBAIR MEDICAL STORE', '', '', '', 'G.T.ROAD', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(134, 729, 7, 'MADNI MEDICOS', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(135, 730, 7, 'IQRA MEDICAL STORE', '', '', '', 'G.T.ROAD', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(136, 733, 7, 'PHARMACY,DR.AHMED SHAH', '', '', '', 'RAILWAY HOSPITAL', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(137, 734, 7, 'NEW USAMA MEDICAL STORE', '', '', '', 'CAMPING GROUND', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(138, 735, 7, 'PAK MEDICAL STORE', '', '', '', 'CAMPING GROUND', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(139, 736, 7, 'NAVEED MEDICOS', '', '', '', 'CAMPING GROUND', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(140, 101, 10, 'MUNEER MEDICAL STORE', '', '', '', '', 'JALALPUR JATTAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(141, 102, 10, 'CHOKHAN MEDICAL STORE', '', '', '', '', 'BARILASHARIF', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(142, 103, 10, 'PHARMACY,DR.SHAKIL RAZA CLINIC', '', '', '', '', 'JALALPUR JATTAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(143, 104, 10, 'PHARMACY,AL REHMAN H.S', '', '592777-430777', '431136', 'MAIN BAZAR', 'JALALPUR JATTAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(144, 105, 10, 'NEW WARAICH MADICAL STORE', '', '', '', 'MAIN BAZAR', 'JALALPUR JATTAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(145, 106, 10, 'SHAKH MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'JALALPUR JATTAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(146, 107, 10, 'PHARMACY,SALEEM HOSPITAL', '', '', '', '', 'JALALPUR JATTAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(147, 108, 10, 'RAFIQ MEDICAL STORE', '', '', '', 'NEAR BUS STOP', 'JALALPUR JATTAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(148, 109, 10, 'LATIF MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'FATHAPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(149, 1010, 10, 'PHARMACY,CITY HOSPITAL', '', '', '', 'TANDA ROAD', 'JALALPUR JATTAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(150, 1011, 10, 'PHARMACY,ALLIED MEDICAL CENTRE', '', '', '', 'NEAR CIVIL HOSPATIL', 'JALALPUR JATTAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(151, 1012, 10, 'AL-SHIFA MEDICAL STORE', '', '', '', '', 'JALALPUR JATTAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(152, 111, 11, 'LATIF MEDICAL STORE', '', '', '', 'MAIN ROAD', 'FATEHPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(153, 114, 11, 'SALEEM & SONS MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'KOTLA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(154, 1110, 11, 'AL FAZAL MEDICAL STORE', '', '', '', 'MAIN ROAD', 'KOTLA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(155, 1111, 11, 'HAFIZ SONS MEDICAL STORE', '', '', '', 'BHIMBER ROAD', 'KOTLA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(156, 51, 5, 'WARAICH MEDICAL STORE', '', '', '', 'MAIN ROAD', 'MANGOWAL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(157, 52, 5, 'ARSHED MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'MANGOWAL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(158, 54, 5, 'PHARMACY,DR.MIAN M.ASLAM', '', '', '', 'MAIN RIAD', 'MANGOWAL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(159, 55, 5, 'SHAN MEDICAL STORE', '', '', '', 'MAIN ROAD', 'KUNAH', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(160, 56, 5, 'BHATTI MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'KUNJAH', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(161, 57, 5, 'SHAHID MEDICAL STORE', '', '', '', 'MAIN ROAD', 'KUNJAH', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(162, 58, 5, 'NAVEED MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'MONGOWAL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(163, 59, 5, 'NADEEM MADICAL STORE', '', '', '', 'MAIN BAZAR', 'KUNJAH', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(164, 511, 5, 'PHARMACY,SHAFE HOSPITAL', '', '', '', '', 'KUNJAH', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(165, 512, 5, 'KAMRAN MEDICAL STORE', '', '', '', '', 'KUNJAH', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(166, 61, 6, 'SHABAZ MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(167, 62, 6, 'TARIQ MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(168, 63, 6, 'EJAZ MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(169, 64, 6, 'KHURSHID MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(170, 65, 6, 'PUNJAB MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(171, 66, 6, 'CHISHTIA MEDICAL STORE', '', '', '', '', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(172, 67, 6, 'ZAHEER MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(173, 68, 6, 'KOKHAR MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(174, 69, 6, 'KASHMER MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(175, 610, 6, 'MIAN MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(176, 611, 6, 'DR.SHANAZ LIAQAT', '', '', '', '', 'DINGA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(177, 121, 12, 'AZAD MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(178, 122, 12, 'AWAME MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(179, 124, 12, 'AKBAR KHANAM HOSPITAL', '', '', '', '', 'BHIMBER', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(180, 125, 12, 'TARIQ MEDICAL STORE', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(181, 126, 12, 'IMTIAZI MEDICAL STORE', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(182, 127, 12, 'MOH.DIN MEMORIAL HOSPATIL', '', '', '', '', 'BHIMBER', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(183, 132, 13, 'NOOR MEDICAL STORE', '', '', '', 'MAIN ROAD', 'KOTJAMAL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(184, 133, 13, 'AZIZ MEDICAL STORE', '', '', '', '', 'KOTJAMAL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(185, 134, 13, 'YOUSAF MADICAL STORE', '', '', '', '', 'KOTJAMAL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(186, 1112, 11, 'FAIZ MEDICAL STORE', '', '', '', '', 'KOTLA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(187, 1013, 10, 'FAZAL HOSPATIL', '', '', '', '', 'JALALPUR', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(188, 1014, 10, 'CITY MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'JALALPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(189, 219, 2, 'JAVAID MEDICAL STORE', '', '', '', 'SADDER BAZAR', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(190, 241, 2, 'KHWAJA MEDICAL STORE', '', '', '', 'MANDI ROAD', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(191, 919, 9, 'HASHAMI MEDICAL STORE', '', '', '', '', 'GULIANA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(192, 47, 4, 'AL-QADARI MEDICAL STORE', '', '', '', '', 'PHARIANAWALI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(193, 737, 7, 'BILAL BROTHERS', '', '', '', 'G.T.ROAD', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(194, 1015, 10, 'ALI MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'JALALPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(195, 243, 2, 'NEW MUMTAZ MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'MANDI BHALWDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(196, 1113, 11, 'SADDAT MEDICAL STORE', '', '', '', 'MAIN ROAD', 'FATEHPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(197, 513, 5, 'PHARMACY,DR.ARIF MASOOD', '', '', '', '', 'KUNJAH', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(198, 612, 6, 'AWAMI MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(199, 920, 9, 'DASI MEDICAL STORE', '', '', '', 'GULYANA ROAD', 'KHARIAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(200, 1017, 10, 'PHARMACY,SHRIF-M-HOPITAL', '', '', '', '', 'JALALPUR', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(201, 613, 6, 'HASSAN MEDICAL STORE', '', '', '', '', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(202, 247, 2, 'NEW KAMAL MEDICAL STORE', '', '', '', 'NEAR D.H.Q', 'MANDI BHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM');
INSERT INTO `dealer_info` (`dealer_table_id`, `dealer_id`, `dealer_area_id`, `dealer_name`, `dealer_contact_person`, `dealer_phone`, `dealer_fax`, `dealer_address`, `dealer_address1`, `dealer_type`, `dealer_cnic`, `dealer_ntn`, `dealer_image`, `dealer_lic9_num`, `dealer_lic9_exp`, `dealer_lic10_num`, `dealer_lic10_Exp`, `dealer_lic11_num`, `dealer_lic11_Exp`, `dealer_credit_limit`, `dealer_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`) VALUES
(203, 249, 2, 'PHARMACY,TARIQ MEDICAL COMPLAX', '', '', '', 'DINGA ROAD', 'MANDI BHAUDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(204, 310, 3, 'PHALIA MEDICAL STORE', '', '', '', '', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(205, 922, 9, 'PHARMACY,NEW MINHAS H.S', '', '', '', '', 'KHARIAN CITY', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(206, 514, 5, 'KHAN MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'KUNJAH', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(207, 515, 5, 'BISMILLAH MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'KUNJAH', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(208, 923, 9, 'PHARMACY,IQBAL M.HOSPITAL', '', '', '', 'DR,AFTAB AHMED', 'DINGA ROAD KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(209, 250, 2, 'SANA MEDICAL STORE', '', '', '', 'DINGA ROAD', 'MANDI BHAWLDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(210, 1114, 11, 'IQBAL MEDICAL STORE', '', '', '', 'BHIMBER ROAD', 'KOTLA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(211, 311, 3, 'PANJAB MEDICAL STORE', '', '', '', '', 'PHLIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(212, 251, 2, 'PEOPLE S PHARMACY', '', '', '', 'D.H.Q', 'MANDI BHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(213, 312, 3, 'SHAHEEN MADICAL STORE', '', '', '', ' T.H.Q.ROAD', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(214, 614, 6, 'ASIF MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(215, 252, 2, 'NEW SUPPER MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'MANDI BHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(216, 615, 6, 'AL MUMTAZ HOSPITAL', '', '', '', '', 'DINGA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(217, 616, 6, 'KHALEEL H.S', '', '', '', '', 'DINGA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(218, 617, 6, 'KAME MEDICAL STORE', '', '', '', '', 'CHALIAWALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(219, 1115, 11, 'NEW SAJJAD MEDICAL STORE', '', '', '', 'KOTLA ROAD', 'DOULTA NAGER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(220, 1116, 11, 'AL KARAM MEDICAL STORE', '', '', '', 'MAIN ROAD', 'BOKAN MORE', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(221, 618, 6, 'KHALIQ MEDICAL STORE', '', '', '', '', 'CHALIAWALA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(222, 516, 5, 'FATAH KHAN MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'KUNJAH', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(223, 1020, 10, 'EHSAN MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'JALALPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(224, 313, 3, 'AWAN CLINIC', '', '', '', '', 'PHALIA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(225, 256, 2, 'PHARMACY,CHEEMA HOSPITAL', '', '', '', '', 'MANDI', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(226, 778, 7, 'AL HAFIZ MEDICAL STORE', '', '', '', 'CAMPING GROND', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(227, 1022, 10, 'IQRA MEDICAL STORE', '', '', '', 'JALALPUR JATTAN', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(228, 314, 3, 'ABID MEDICAL STORE', '', '', '', 'PHALIA', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(229, 315, 3, 'MADINA MEDICAL STORE', '', '', '', '', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(230, 87, 8, 'JAN SHAR KHAN', '', '', '', 'SADDER BAZAR', 'KHARIAN CANT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(231, 1117, 11, '.', '', '', '', '', 'KOTLA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(232, 98, 9, 'SHAHBAZ MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'KHARIAN CITY', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(233, 619, 6, 'AJMAL MEDICAL STORE', '', '', '', '', 'CHALIWALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(234, 459, 4, 'DMD', '', '03225991314', '', '', 'GUJRAT', 'Retailer', '', '', '', '414', '0', '0', '12/31/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(235, 169, 1, 'NEW FAROOQ MEDICAL STORE', '', '', '', 'NAZ CHOWK', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(236, 97, 9, 'ZAHID MEDICAL STORE', '', '', '', 'SARGODHA ROAD', 'GUJRAT', 'Retailer', '34201-7621926-1', '5312867-5', '', '1005', '0', '0', '12/31/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(237, 1146, 1, 'GRACE PLUS PHARMACY', '', '0333-8405395', '', 'JALALPUR JATTAN ROAD', 'GUJRAT', 'Retailer', '34104-9204851-3', '8157253-4', '', '45182', '0', '0', '9/5/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(238, 174, 1, 'AL GHANI MEDICAL STORE', '', '', '', 'AHSAN PLAZA', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(239, 831, 8, 'LUCKY PHARMACY', '', '', '', 'KACHARY CHOWK,REHMAN SHAHEED', 'ROAD GUJRAT', 'Retailer', '34201-0487828-1', '4222785-2', '', '13782', '0', '0', '10/11/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(240, 177, 1, 'REHMAN S.R', '', '', '', 'JINAH ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(241, 27, 2, 'NEW NASEEM MEDICAL STORE', '', '', '', 'D.H.Q ROAD', 'MANDI BHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(242, 211, 2, 'ZAFAR MEDICAL STORE', '', '', '', 'KACHARY ROAD', 'MANDI BHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(243, 213, 2, 'MAZHAR MEDICAL STORE', '', '', '', 'KACHARY ROAD', 'MANDI BHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(244, 216, 2, 'ASHEA MEDICAL STORE', '', '', '', 'PHALIA ROAD', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(245, 217, 2, 'ARAIB MEDICAL STORE', '', '', '', 'KACHARY ROAD', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(246, 221, 2, 'AWAME MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'MANDI BAHAUDIN', 'Whole Saler', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(247, 210, 2, '.', '', '', '', '', '', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(248, 79, 7, 'FATHA & SONS MEDICAL STORE', '', '', '', 'RAILWAY ROAD', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(249, 711, 7, 'MAKKAH MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(250, 718, 7, 'ASAD MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(251, 731, 7, 'NEW IQRA MEDICAL STORE', '', '', '', 'G.T ROAD', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(252, 732, 7, 'NEW TALAHA MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(253, 84, 8, 'PHARMACY-NASEEB CLINIC', '', '', '', '', 'KHARIAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(254, 148, 14, 'USMAN MEDICAL STORE', '', '', '', 'G.T.ROAD', 'SARI ALAMGER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(255, 1410, 14, 'YASIR MEDICAL STORE', '', '', '', 'NEAR MADDNI MASJID', 'SARI ALALMGER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(256, 738, 7, 'SAVEN STAR MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(257, 1018, 10, 'ALI MEDICAL STORE', '', '', '', '', 'JALALPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(258, 739, 7, 'DR.SHAHID LATIF', '', '', '', 'RAILWAY ROAD', 'LALAMUSA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(259, 740, 7, 'DR.USMAN GHANI', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(260, 741, 7, 'NIAZI CLINIC', '', '', '', 'G.T.ROAD', 'LALAMUSA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(261, 742, 7, 'KASHMIR MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(262, 743, 7, 'PHARMACY,DR.MUBEEN MALIK', '', '053.7531534', '', 'G.T.ROAD PUNJAN KISSANA', 'KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(263, 744, 7, 'PHARMACY,DR.NADEEM AHMED KHAN', '', '', '', '', 'LALAMUSA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(264, 719, 7, 'ARSHAD MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(265, 91, 9, 'PHARMACY,SUBHAN H.S', '', '', '', 'GULYANA ROAD', 'KHARIAN CITY', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(266, 918, 9, 'PHARMACY,DR.KAMAL ATHAR', '', '', '', 'MAIN BAZAR', 'KHARIAN CITY', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(267, 921, 9, 'PHARMACY,MAQBOL NASEEM H.', '', '', '', '', 'KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(268, 924, 9, 'PHARMACY,DR.SAJJADA HAFIZ CLINIC', '', '', '', 'G.T.ROAD', 'KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(269, 925, 9, 'SHARIF MEDICAL STORE', '', '', '', 'KHARIAN ROAD', 'SOBOR CHOWK', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(270, 49, 4, 'MIAN MEDICAL STORE', '', '', '', 'B.H.U', 'PHARIANWALI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(271, 410, 4, 'AL RIAZ MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'PHARIANWALI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(272, 220, 2, 'KHANNAN PHARMACY', '', '', '', 'NEAR D.H.Q', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(273, 223, 2, 'RAHMAT MEDICAL STORE', '', '', '', 'NEAR D.H.Q', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(274, 232, 2, 'ABAD MEDICAL STORE', '', '', '', 'KING ROAD', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(275, 238, 2, 'MADINA MEDICAL STORE', '', '', '', 'KING ROAD', 'MANDI BHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(276, 242, 2, 'CHAUDRY MEDICAL STORE', '', '', '', 'PHALIA ROAD', 'MANDI BHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(277, 244, 2, 'PHARMACY,DR.YOUNAS PARWAZ', '', '', '', 'KACHARY ROAD', 'MANDI BHAUDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(278, 245, 2, 'PHARMACY,CITY HOSPITAL', '', '', '', 'D.H.Q. ROAD', 'MANDI BHAUDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(279, 246, 2, 'PHARMACY,FAMALY HOSPITAL', '', '', '', 'D.H.Q ROAD', 'MANDI BHAUDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(280, 248, 2, 'ZAHID MEDICAL STORE', '', '', '', 'KACHARY ROAD', 'MANDI BHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(281, 253, 2, 'MANSOR MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'MANDI BHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(282, 254, 2, 'SHAKH NAEEM MEDICAL STORE', '', '', '', '', 'MANDI BHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(283, 255, 2, 'DR.RIZWAN CHEEMA', '', '', '', 'KACHARY ROAD', 'MANDI BHAUDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(284, 480, 4, 'AL HARAM MEDICAL STORE', '', '', '', 'BHIMBER ROAD', 'GUJRAT', 'Retailer', '', '', '', '622', '0', '622', '12/31/2013 12:00 AM', '12/30/1899 12:00 AM', '12/31/2013 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(285, 915, 9, 'FATIMA MEDICAL STORE', '', '', '', 'CHOWK MADINA BAZAR', 'SARGODHA ROAD GUJRAT', 'Retailer', '34201-6740850-5', '6513405-6', '', '7569', '0', '0', '10/30/2019 12:00 AM', '12/31/2015 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(286, 957, 9, 'DR.IRFAN SHAUKAT SHIRAZI', '', '', '03026010011', 'PMDC# 21264-P', 'SHERAZI HOSPITAL GUJRAT', 'Doctor', '', '0997580-2', '', '21264', '0', '0', '12/31/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(287, 185, 1, 'MIRZA MEDICAL STORE', '', '', '', 'SHAHDULA ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(288, 286, 2, 'AL SHAFEE MEDICAL STORE', '', '', '', 'SARGODHA ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(289, 187, 1, 'IQBAL MEDICAL STORE', '', '', '', 'RAILWAY ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(290, 539, 5, 'DR.RIFAT GHAZALA', '', '', '', '', 'PUNJAB MEDICAL CENTER GUJRAT', 'Doctor', '', '2500904-4', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(291, 152, 1, 'DR.SYED MOHSIN ALI', '', '', '', 'PMDC# 29331-P', 'SYED SURGICAL Hp GUJRAT', 'Doctor', '35200-8585177-9', '3909447-2', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(292, 541, 5, 'DR.ZAHIDA AKMAL', '', '', '', 'PMDC# 13988-P', 'KHURSHED HOSPITAL GUJRAT', 'Doctor', '', '1633626-7', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(293, 947, 9, 'ALAM MEDICOS', '', '03216219269', '', 'BANK ROAD CHOWK HAKIM KHAN', 'FATUPURA GUJRAT', 'Retailer', '34201-6097612-9', '2698217-0', '', '0', '6725', '0', '12/31/2015 12:00 AM', '11/2/2019 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(294, 194, 1, 'ROZARY HOSPITAL', '', '', '', '', 'GUJRAT', 'I', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(295, 966, 9, 'DR.IJAZ BASHIR', '', '', '', 'PMDC# 19674-P', 'BASHIR HOSPITAL GUJRAT', 'Doctor', '34201-4693844-9', '0401095-7', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(296, 544, 5, 'DR.SAFDAR HUSSAN', '', '', '', 'BHIMBER ROAD', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(297, 198, 1, 'NASEEB MEDICAL STORE', '', '', '', 'BHIMBER ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(298, 517, 5, 'PHARMACY,AL SULTANIA HOSPITAL', '', '', '', '', 'MONGOWAL', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(299, 519, 5, 'TALIB H.S', '', '', '', 'NEAR BUSS STAND', 'KUNJAH', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(300, 520, 5, 'DR.ZAHID AUOB', '', '', '', 'MAIN ROAD', 'MONGOWAL', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(301, 620, 6, 'ZAHID MEDICAL STORE', '', '', '', 'DINGA ROAD', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(302, 521, 5, 'QUOAM MAHRIA M.S', '', '', '', 'MAIN BAZAR', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(303, 622, 6, 'DR.KHALID ARIBE', '', '', '', '', 'DINGA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(304, 623, 6, 'AMER JAVAID COMPLEX', '', '', '', 'THANA ROAD', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(305, 624, 6, 'MAJEED MEDICAL STORE', '', '', '', '', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(306, 1118, 11, 'FAMILY MEDICAL STORE', '', '', '', 'BHIMBER ROAD', 'KOTLA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(307, 1119, 11, 'SHARIF MEDICAL STORE', '', '', '', 'MAIN ROAD', 'KOTLA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(308, 1120, 11, 'DR.AMANAT ALI', '', '', '', '', 'KOTLA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(309, 1121, 11, 'KHARSHED MEDICAL STORE', '', '', '', 'MAIN ROAD', 'FATEHPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(310, 1016, 10, 'BHATTI MEDICAL STORE', '', '', '', 'TANDA ROAD', 'JALALPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(311, 1021, 10, 'PHARMACY,AZAM HOSPITAL', '', '', '', '', 'FATHAPUR', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(312, 1024, 10, 'PHARMACY,SALEEM H.S', '', '', '', 'MAIN ROAD', 'JALALPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(313, 1025, 10, 'AYOUB MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'FATHAPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(314, 1027, 10, 'FARAZ MEDICAL STORE', '', '', '', 'TANDA ROAD', 'JALALPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(315, 1028, 10, 'PHARMACY,DR.SHABIR HUSSAIN', '', '', '', '', 'JALALPUR', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(316, 257, 2, 'SHAUKAT MEDICAL STORE', '', '', '', 'D.H.Q.HOSPITAL', 'MANDI BHAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(317, 258, 2, 'NAEEM SAKH MEDICAL STORE', '', '', '', 'KACHARY ROAD', 'MANDI BAHAUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(318, 259, 2, 'IMRAN MEDICAL STORE', '', '', '', 'NEAR D.H.Q', 'MANDI BAHAUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(319, 260, 2, 'CITY MEDICAL HALL', '', '', '', '', 'MANDI BAHAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(320, 261, 2, 'BILAL AGENCIES', '', '', '', 'OPP OLD BIJLI GHAR', 'MANDI BAHAUDDIN', 'S', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(321, 46, 4, 'BHUTTA MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'PAHRIANWALI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(322, 411, 4, 'AL QADRI MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'PAHRIANWALI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(323, 316, 3, 'FAZAL HAQ MEDICAL STORE', '', '', '', '', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(324, 317, 3, 'SOHAIL MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(325, 625, 6, 'KAZMI MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'CHELLIANWALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(326, 626, 6, 'JAVAID MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'CHELLIANWALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(327, 199, 1, 'AL-HASSAN MEDICAL STORE', '', '', '', 'BHIMBER ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(328, 1100, 1, 'PAKISTAN MEDICAL STORE', '', '', '', 'SARGODHA ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(329, 948, 9, 'DR.NADEEM AHMED', '', '', '', 'PMDC# 30422-P', 'UMAR ARSHAD Hospital GUJRAT', 'Doctor', '34202-5984302-9', '2805344-3', '', '30422', '0', '0', '12/31/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(330, 4102, 4, 'SHIFA PHARMACY', '', '', '', 'RAZAQ MARKET', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(331, 1103, 1, 'HAIDER MEDICAL STORE', '', '', '', 'RAILWAY ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(332, 1104, 1, 'JAHANGIR MEDICAL STORE', '', '', '', 'G.T.ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(333, 949, 9, 'NEW YOUNAS MEDICAL STORE', '', '03466831055', '', 'OPP LADIES&CHILDERN PARK', 'RAMTALI ROAD GUJRAT', 'Retailer', '', '1166931-3', '', '0', '0', '7586', '12/31/2015 12:00 AM', '12/31/2015 12:00 AM', '10/28/2019 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(334, 2107, 2, 'PHARMACY,ASIF CLINIC', '', '', '', 'RALWAY ROAD', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(335, 1019, 10, 'MITHO MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'JALALPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(336, 1029, 10, 'AKHTAR MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'KARIANWALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(337, 1030, 10, 'AZAM MEDICAL STORE', '', '', '', 'TANDO ROAD', 'KARIANWALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(338, 1031, 10, 'BISMILLAH MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'KARIANWALAL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(339, 1032, 10, 'EJZ MEDICSL STORE', '', '', '', 'MAIN BAZAR', 'KARIANWALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(340, 1033, 10, 'ASHRAF MEDICAL STORE', '', '', '', '', 'JALALPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(341, 1034, 10, 'ALAM MEDICAL HALL', '', '', '', 'MAIN BAZAR', 'JALALPUR SOBTIAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(342, 1035, 10, 'AL HAQ MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'J.P.SOBHTIAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(343, 745, 7, 'RASHID MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(344, 318, 3, 'MUKHTAR MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(345, 263, 2, 'ABID MEDICAL STORE', '', '', '', 'KING ROAD', 'MANDI BHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(346, 265, 2, 'PHARMACY,NAZEER SURGICAL HOSPITAL', '', '', '', 'RASOOL ROAD', 'MANDI BHAUDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(347, 4108, 4, 'PHARMACY,JINAH H.S', '', '', '', 'JINAH ROAD', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(348, 266, 2, 'PHARMACY,SHAFAR HOSPITAL', '', '', '', 'RASOOL ROAD', 'MANDI BHAUDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(349, 1411, 14, 'PHARMACY,ALI NAWAZ HOSPITAL', '', '', '', 'NEAR G.T.ROAD', 'SARI ALAMGER', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(350, 747, 7, 'NEW IQRA MEDICAL STORE', '', '', '', 'G.T.ROAD', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(351, 748, 7, 'PHARMACY,SARGUN HOSPITAL', '', '', '', '', 'LALAMUSA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(352, 749, 7, 'ABBAS MEDICAL STORE', '', '', '', 'G.T.ROAD', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(353, 88, 8, 'NEW JAN SHAR M.S', '', '', '', 'SADDER BAZAR', 'KHARIAN CANT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(354, 750, 7, '.', '', '', '', '', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(355, 1412, 14, 'AL NOOR MEDICAL STORE', '', '', '', '', 'SARI ALAMGIR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(356, 1122, 11, 'CHAUDHARY MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'KAKRALI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(357, 547, 5, 'DR.MEHFOOZ AHMED', '', '', '', '', 'REHMAN CLINIC GUJRAT', 'Doctor', '34201-7307582-5', '0223022-4', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(358, 267, 2, 'GHOREE MEDICAL STORE', '', '', '', 'NEAR.D.H.Q.', 'MANDI BHAUDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(359, 751, 7, 'SANDHU MEDICAL STORE', '', '', '', '', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(360, 412, 4, 'NEW PUNJAB MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'PHARIANWALI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(361, 752, 7, 'PHARMACY,POLY CLINIC', '', '', '', '', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(362, 1413, 14, 'REHAN MEDICAL STORE', '', '', '', '', 'SARI ALAMGER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(363, 268, 2, 'MOON PHARMACY', '', '', '', '', 'MANDI BHAUDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(364, 319, 3, 'NEW SUPPER MEDICAL STORE', '', '', '', 'MAIN ROAD', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(365, 320, 3, 'ANMOL MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(366, 753, 7, 'SIALVI MEDICAL STORE', '', '', '', '', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(367, 754, 7, 'PHARMACY,SAEEDA ZAFAR H.S', '', '', '', '', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(368, 1036, 10, 'BHATTI MEDICAL STORE', '', '', '', 'MAIN ROAD', 'JALALPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(369, 926, 9, 'AL MUSLIM MEDICAL STORE', '', '', '', '', 'KHARIAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(370, 270, 2, 'PHARMACY,TARAR HOSPITAL', '', '', '', '', 'MANDI BHAUDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(371, 927, 9, 'HAMAZA MEDICAL STORE', '', '', '', '', 'KHARIAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(372, 321, 3, 'NEW SALEEME MEDICAL STORE', '', '', '', 'NEAR T.H.Q', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(373, 371, 3, 'AL SHAFA  H.S', '', '', '', '', 'MANDI BHAUDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(374, 271, 2, 'PHARMACY,AL KARAM HOSPITAL', '', '', '', '', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(375, 172, 1, 'NEW LIFE MEDICAL STORE', '', '', '', 'NEAR AYESHA HOSPITAL', 'JINNAH ROAD GUJRAT', 'Retailer', '3420103647219', '4985994-8', '', '12875', '0', '0', '10/2/2019 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(376, 1037, 10, 'PHARMACY,WARAICH H.S', '', '', '', '', 'JALALPUR', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(377, 1038, 10, '.', '', '', '', '', 'JALALPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(378, 1039, 10, 'KHALEEQ MEDICAL STORE', '', '', '', '', 'JALALPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(379, 272, 2, 'NASIR MEDICOSE', '', '', '', 'RASOOL ROAD', 'MANDI BHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(380, 756, 7, 'ARSHAD MEDICOSE', '', '', '', 'CAMPING GROUND', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(381, 757, 7, 'NASIR MEDICAL STORE', '', '', '', '', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(382, 1414, 14, 'KHALID MEDICAL STORE', '', '', '', '', 'SARI ALAMGIR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(383, 322, 3, 'NEW IMTIAZ MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(384, 128, 12, 'QAUAM ARFA MEDICAL STORE', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(385, 129, 12, 'ATTIQ MEDICAL STORE', '', '', '', '', 'BHIMBER', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(386, 1210, 12, 'KHURSHED MEDICAL STORE', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(387, 1211, 12, 'ALTAMAS MEDICAL STORE', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(388, 151, 15, 'TAHIR MEDICAL STORE', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(389, 153, 15, 'YOUSAF MEDICAL STORE', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(390, 154, 15, 'SHAKAR MEDICAL STORE', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(391, 323, 3, 'AL REHMAN CLINIC', '', '', '', '', 'PHALIA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(392, 758, 7, 'PHARMACY,MALIK HOSPITAL', '', '', '', '', 'LALAMUSA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(393, 5114, 5, 'NADEEM MEDICAL STORE', '', '', '', '', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(394, 273, 2, 'NEW NAEEM MEDICAL STORE', '', '', '', '', 'MANDI BHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(395, 155, 15, 'IMRAN MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'RUKAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(396, 1040, 10, 'KHURSHED MEDICAL STORE', '', '', '', '', 'FATHAPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(397, 274, 2, 'PHARMACY,A.T.B.ASSOCIATION', '', '', '', '', 'MANDI BHAUDIN', 'I', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(398, 522, 5, 'PHARMACY,MONGOWAL HOSPITAL', '', '', '', '', 'MONGOWAL', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(399, 523, 5, 'SHAHZAD MEDICAL STORE', '', '', '', '', 'MONGOWAL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(400, 548, 5, 'NOOR PHARMACY', '', '03316342822', '', 'OPP GATE#1 SABOWAL HOSPITAL', 'GUJRAT', 'Retailer', '', '', '', '1124', '0', '0', '10/19/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(401, 156, 15, 'AL SHAFA MEDICAL STORE', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(402, 549, 5, 'DR.MAJ.SARFRAZ', '', '', '', '', 'MAJARS LAB GUJRAT', 'Doctor', '37405-0335522-7', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(403, 275, 2, 'FAREDE MEDICAL STORE', '', '', '', '', 'MANDI MHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(404, 556, 5, 'PHARMACY,HASSAN HOSPITAL', '', '', '', '', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(405, 2117, 2, 'KHALID MEDICAL STORE', '', '', '', '', 'GUJRAT', 'Retailer', '', '', '', '0', '275', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(406, 324, 3, 'PHARMACY,HASSAN CLINIC', '', '', '', '', 'PHALIA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(407, 1041, 10, 'PHARMACY,KISHWAR HOSPITAL', '', '', '', '', '', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(408, 1123, 11, 'HAQBAHO MEDICAL STORE', '', '', '', '', 'KOTLA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(409, 1151, 1, 'USMAN T.CO', '', '', '', '', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(410, 950, 9, 'JAVAID MEDICAL STORE', '', '', '', '', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM');
INSERT INTO `dealer_info` (`dealer_table_id`, `dealer_id`, `dealer_area_id`, `dealer_name`, `dealer_contact_person`, `dealer_phone`, `dealer_fax`, `dealer_address`, `dealer_address1`, `dealer_type`, `dealer_cnic`, `dealer_ntn`, `dealer_image`, `dealer_lic9_num`, `dealer_lic9_exp`, `dealer_lic10_num`, `dealer_lic10_Exp`, `dealer_lic11_num`, `dealer_lic11_Exp`, `dealer_credit_limit`, `dealer_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`) VALUES
(411, 157, 1, 'MEDI PLUS PHARMACY', '', '03344651557', '', 'SABOWAL PLAZA', 'GUJRAT', 'Retailer', '', '', '', '1294', '0', '0', '1/28/2016 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(412, 759, 7, 'USMAN HOSPITAL', '', '', '', 'G.T.ROAD', 'LALAMUSA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(413, 325, 3, 'LUCK MEDICAL STORE', '', '', '', '', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(414, 557, 5, 'DR.MASUD ANWAR KHAN', '', '', '', 'PMDC# 6701-P', 'MADNI HOSPITAL GUJRAT', 'Doctor', '', '1164023-5', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(415, 779, 7, 'NAZIR S.R HOSPITAL', '', '', '', '', 'LALAMUSA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(416, 171, 17, 'ATTIQ MEDICAL STORE', '', '', '', '', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(417, 276, 2, 'SHAFIQ MEDICAL STORE', '', '', '', '', 'MANDI BHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(418, 4120, 4, 'HAMAD MEDICAL STORE', '', '', '', 'OPP D.H.Q', 'GUJRAT', 'Retailer', '', '', '', '987', '0', '0', '12/31/2013 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(419, 762, 7, 'SHAH MEDICOSE', '', '', '', '', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(420, 763, 7, 'IKRAM MEDICIN HOUSE', '', '', '', '', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(421, 764, 7, 'FRIENDS MEDICAL STORE', '', '', '', '', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(422, 765, 7, 'PHARMACY,RAHMET HOSPITAL', '', '', '', '', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(423, 277, 2, 'JAVAID MEDICAL STORE', '', '', '', 'ALVI CHOWK', 'MANDI BHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(424, 197, 1, 'SHUJA ALI MEDICAL STORE', '', '', '', 'KHWAJGON ROAD', 'GUJRAT', 'Retailer', '', '', '', '934', '0', '934', '12/31/2015 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(425, 766, 7, 'PAKISTAN MEDICAL STORE', '', '', '', 'G.T.ROAD', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(426, 1124, 11, 'PHARMACY,HADAYAT ULLAHA HOSPITAL', '', '', '', '', 'DOLATNAGAR', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(427, 5302, 5, 'AL FATHA G.S', '', '', '', 'CHOWK PAKISTAN', 'GUJRAT', 'G', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(428, 1155, 1, 'LATIF SONS', '', '', '', 'NAZ CHOWK', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(429, 558, 5, 'NEW USMAN MEDICAL STORE', '', '0323-4765791', '', 'NEAR NOOR BAKERS,BHIMBER ROAD', 'GUJRAT', 'Retailer', '34201-7640435-1', '1539176-7', '', '0', '29130', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(430, 89, 8, 'AL SHAFI MEDICAL STORE', '', '', '', 'NEAR C.M.H', 'KHARIAN CANT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(431, 1042, 10, 'PHARMACY,DR.NASIM TANVER', '', '', '', '', 'JALALPUR JATTAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(432, 928, 9, 'PHARMACY,ALLIED HOSPITAL', '', '', '', '', 'KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(433, 929, 9, 'AFZAL MEDICAL STORE', '', '', '', 'NIA ARA MOHALLAH', 'KHARIAN CITY', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(434, 930, 9, 'RAFIQ MEDICAL SERVICE', '', '', '', 'GULIANA ADDA', 'GULIANA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(435, 931, 9, 'MAHMOOD MEDICAL STORE', '', '', '', '', 'KHARIAN CITY', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(436, 621, 6, 'NAVEED MEDICAL STORE', '', '', '', '', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(437, 413, 4, 'GADHE MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'PHARIANWALI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(438, 1043, 10, 'PHARMACY,DR.MUBASHAR CLINIC', '', '', '', 'MAIN BAZAR', 'FATHAPUR', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(439, 810, 8, 'LIFE MEDICAL STORE', '', '', '', 'NEAR C.M.H', 'KHARIAN CANT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(440, 278, 2, 'ALI MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'MANDI BHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(441, 279, 2, 'KHURSHID G.S', '', '', '', 'MAIN BAZAR', 'MANDI BHAUDIN', 'G', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(442, 932, 9, 'PHARMACY,IMTIAZ HOSPITAL', '', '', '', '', 'GULIANA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(443, 933, 9, 'HAMAZA MEDICAL STORE', '', '', '', '', 'GULIANA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(444, 934, 9, 'IFTIKHAR MEDICAL STORE', '', '', '', '', 'GULIANA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(445, 935, 9, 'AWAN MEDICAL STORE', '', '', '', '', 'GULIANA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(446, 1212, 12, 'NEW VALLY MEDICAL HALL', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(447, 767, 7, 'PHARMACY,SHAFA HOSPITAL', '', '', '', '', 'LALAMUSA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(448, 280, 2, 'SHAH DIN MEDICAL STORE', '', '', '', '', 'MANDI BHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(449, 936, 9, 'PHARMACY,KHEZAR HOSPITAL', '', '', '', '', 'KHARIAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(450, 768, 7, 'PHARMACY,DR.M.ALTAF', '', '', '', 'FAIZ HOSPITAL', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(451, 769, 7, 'SARGAN HOSPITAL', '', '', '', 'G.T.ROAD', 'LALAMUSA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(452, 770, 7, 'ZAHID MEDICOS', '', '', '', '', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(453, 336, 3, 'PHARMACY, DOCTOR CLINC WARRANTY', 'INDOOR PHARMACY', '', '', 'INDOOR PHARMACY', '', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(454, 281, 2, 'POME MEDICAL STORE', '', '', '', 'D.H.Q ROAD', 'MANDI BHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(455, 1044, 10, 'KASHMIR MEDICAL STORE', '', '', '', '', 'JALALPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(456, 264, 2, 'PHARMACY,DR.AKHTAR JAVAID TUBASAM', '', '', '', '', 'MANDI BHAUDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(457, 937, 9, 'PHARMACY,SULTAN HOSPITAL', '', '', '', '', 'KHARIAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(458, 4154, 4, 'AL SHAFA MEDICAL STORE', '', '', '', 'BHIMBER ROAD', 'GUJRAT', 'Retailer', '', '', '', '858', '0', '858', '12/31/2013 12:00 AM', '12/30/1899 12:00 AM', '12/31/2013 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(459, 982, 9, 'P.T.C.L,DISPENCERY', '', '', '', 'G.T.ROAD', 'GUJRAT', 'I', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(460, 1158, 1, 'DR.WASEEM BUTT', '', '', '', '', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(461, 771, 7, 'BUTT MEDICAL STORE', '', '', '', '', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(462, 269, 2, 'AL-FALAHA MEDICAL STORE', '', '', '', '', 'MANDI BAHAUDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(463, 780, 7, 'JANJUWA MEDICAL STORE', '', '', '', '', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(464, 326, 3, 'DR.IJAZ MUGHAL', '', '', '', '', 'PHALIA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(465, 1415, 14, 'SAKEENA MEDICAL STORE', '', '', '', '', 'SARI ALAMGER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(466, 1023, 10, 'AYOUAB MEDICAL STORE', '', '', '', '', 'JALALPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(467, 327, 3, 'SHAHBAZ MEDICAL STORE', '', '', '', '', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(468, 430, 4, 'NEW CITY MEDICAL STORE', '', '', '', 'OOP.D.H.Q', 'GUJRAT', 'Retailer', '', '', '', '775', '0', '775', '12/31/2013 12:00 AM', '12/30/1899 12:00 AM', '12/31/2013 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(469, 980, 9, 'BAHI JAN MEDICAL STORE', '', '0335-4471456', '', 'NEAR FAZAL ELLAHI G/S', 'MOH.SARDAR PURA GUJRAT', 'Retailer', '34201-3270853-3', '', '', '0', '7197', '0', '12/31/2015 12:00 AM', '10/30/2019 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(470, 969, 9, 'DR.SYED MUZAMAL HUSSAIN', '', '', '', 'PMDC# 9865-P', 'MUZAMIL EYE CLINIC GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(471, 328, 3, 'FAMILY HOSPITAL', '', '', '', '', 'PHALIA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(472, 329, 3, 'DR,PARVAIZ NAZEER TARAR', '', '', '', '', 'PHALIA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(473, 53, 5, 'KARIMI CLINIC', '', '', '', '', 'KUNJAH', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(474, 1416, 14, 'ZAHID MEDICOS', '', '', '', '', 'SARI ALAMGER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(475, 1417, 14, 'AL UMAR MEDICAL STORE', '', '', '', '', 'SARI ALAMGER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(476, 537, 5, '.MALIK PHARMACY', '', '03006209020', '', 'OPP.GUJRAT HOSPITAL', 'BHIMBER ROAD GUJRAT', 'Retailer', '', '', '', '30677', '0', '0', '3/3/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(477, 162, 16, 'AZAM MEDICAL STORE', '', '', '', '', 'KARINWALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(478, 543, 5, 'DR.M.ARSHAD', '', '', '', '', 'CHANAB HOSPITAL GUJRAT', 'Doctor', '', '2528048', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(479, 170, 1, 'DR.USMAN NAEEM', '', '03348064306', '', '', 'HAMEDA BAGAM Hp GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(480, 510, 5, 'PHARMACY,DR.ARSHAD CHISHTE', '', '', '', '', 'KUNJAH', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(481, 173, 17, 'JET MEDICAL STORE', '', '', '', 'MUSLIM BAZAR', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(482, 1750, 17, 'AL RIAZ MEDICAL STORE', '', '', '', '', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(483, 1026, 10, 'BASHARAT MEDICAL & G.S', '', '', '', 'MAIN TANDA ROAD', 'JALALPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(484, 86, 8, 'FARAH MEDICOSE', '', '', '', 'C.M.H', 'KHARIAN CANT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(485, 330, 3, 'ALI MEDICAL STORE', '', '', '', '', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(486, 1130, 1, 'GONDAL M/C', '', '', '', '', 'GUJRANWALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(487, 1418, 14, 'MARSAB MEDICAL STORE', '', '', '', 'G.T.ROAD', 'SARI ALAMGIR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(488, 1045, 10, 'BUTT MEDICAL STORE', '', '', '', '', 'JALALPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(489, 1125, 11, 'IMTIAZI MEDICAL STORE', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(490, 1126, 11, 'KHURSHED MEDICAL STORE', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(491, 1127, 11, 'QAYYUM ARFA MEDICAL STORE', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(492, 1228, 12, 'VALLY MEDICAL STORE', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(493, 1128, 11, 'KASHMIR VALLY MEDICAL STORE', '', '', '', '', 'BHUMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(494, 1129, 11, 'PHARMACY,MOH.DIN MEMORIAL HOSPITAL', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(495, 282, 2, 'PHARMACY,ANWAR FATIMA HOSPITAL', '', '', '', '', 'MANDI BHAUDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(496, 2169, 2, 'PHARMACY.DR.ABDUL SATTAR', '', '', '', 'G.T.ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(497, 772, 7, 'DR,NAEEM AKHTAR JANJUWA', '', '', '', 'G.T.ROAD', 'LALAMUSA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(498, 398, 3, 'KHAN MEDICAL STORE', '', '', '', 'KABLI GATE', 'GUJRAT', 'Retailer', '', '', '', '895', '895', '895', '12/31/2013 12:00 AM', '12/31/2013 12:00 AM', '12/31/2013 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(499, 582, 5, 'AL HAQ MEDICAL STORE', '', '03006233036', '', 'NEAR ALEENA CENTER', 'BHIMBER ROAD GUJRAT', 'Retailer', '', '', '', '0', '27756', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(500, 1419, 14, 'TARIQ MEDICAL STORE', '', '', '', 'VILLAGE', 'KHAMBI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(501, 967, 9, '.WAKEEL MEDICAL STORE', '', '', '', 'SARGODHA ROAD', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '0', '12/31/2017 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(502, 746, 7, 'SIRAJ MEDICAL STORE', '', '', '', 'GALI QULFA WALI', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(503, 3111, 3, 'JET MEDICINE & SURGI', '', '', '', 'EAST CIRCULAR ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(504, 1420, 14, 'PHARMACY,NAZEER BAGUM M.H', '', '', '', '', 'SARI ALALMGIR', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(505, 262, 2, 'MANGET MEDICAL STORE', '', '', '', '', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(506, 627, 6, 'YASIR MEDICAL STORE', '', '', '', '', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(507, 628, 6, 'MAZHAR MEDICAL STORE', '', '', '', '', 'CHELLIANWALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(508, 760, 7, 'PHARMACY,DR.ZAFAR AMIN NIAZI', '', '', '', 'G.T.ROAD', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(509, 2525, 25, 'KAZMI MEDICAL STORE', '', '', '', '', 'CHELLIANWALI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(510, 41, 4, 'BILAL HOSPITAL', '', '', '', '', 'PAHRIANWALI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(511, 414, 4, 'WALLEE SURGCAL HOSPITAL', '', '', '', '', 'PAHRIANWALI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(512, 755, 7, 'GOSEEA MEDICAL STORE', '', '', '', 'KHARIAN ROAD', 'BHADDAR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(513, 158, 15, 'AL GONDAL MEDICAL STORE', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(514, 422, 42, 'IMRAN MEDICAL STORE', '', '', '', '', 'RUKKAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(515, 331, 3, 'PHARMACY,DR.KHALIQ DAD TARAR', '', '', '', '', 'PHALIA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(516, 2710, 27, 'NADEEM MEDICAL STORE', '', '', '', '', 'MALIK WALL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(517, 332, 3, 'IRSHAD TRADAR', '', '', '', '', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(518, 1131, 11, 'QAYYUM BROTHERS', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(519, 2711, 27, 'KHAWAJA MEDICAL STORE', '', '', '', '', 'MALIK WALL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(520, 159, 15, 'SHAKH MEDICAL STORE', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(521, 283, 2, 'SHEIKH MEDICAL STORE', '', '', '', '', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(522, 284, 2, 'PHARAMCY,DR.M,RAMZAN PARVEZ', '', '', '', '', 'MANDI BAHA U DDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(523, 285, 28, 'AL SHAFA MEDICAL STORE', '', '', '', '', 'RUKKAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(524, 288, 28, 'OWAIS MEDICAL STORE', '', '', '', '', 'GOJRA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(525, 289, 28, 'ABDUL MEDICAL STORE', '', '', '', '', 'GOJRA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(526, 2810, 28, 'SHOKAT MEDICAL STORE', '', '', '', '', 'GOJRA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(527, 2811, 28, 'SHEIKH MEDICAL STORE', '', '', '', '', 'GOJRA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(528, 1421, 14, 'PHARMACY,DR.SADHEER SULTAN', '', '', '', '', 'SARI ALAMGIR', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(529, 1132, 11, 'NISAR MEDICAL STORE', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(530, 2812, 28, 'ZOKE MEDICAL STORE', '', '', '', '', 'GOJRA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(531, 2813, 28, 'BISMILLAH MEDICAL STORE', '', '', '', '', 'GOJRA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(532, 1510, 15, 'ADAM ZAADA MEDICAL STORE', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(533, 1511, 15, 'ASLAM MEDICAL STORE', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(534, 761, 7, 'AWAME MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(535, 1512, 15, 'ALI MEDICAL CENTER', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(536, 2814, 28, 'GONDAL MEDICAL STORE', '', '', '', '', 'RUKKAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(537, 2712, 27, 'BHATTI MEDICAL STORE', '', '', '', '', 'MALIK WALL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(538, 176, 1, 'DR.WASEEM BUTT', '', '', '', '', 'SHAH JAHANGIR ROAD GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(539, 2713, 27, 'DASTAGEER MEDICAL STORE', '', '', '', '', 'MALIK WALL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(540, 2815, 28, 'AFTAB SURGICAL HOSPITAL', '', '', '', '', 'GOJRA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(541, 1133, 11, 'WAHAB HOSPITAL', '', '', '', 'BHIMBER ROAD', 'KOTLA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(542, 2714, 27, 'MADINA MEDICAL STORE', '', '', '', '', 'MALIK WALL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(543, 2715, 27, 'GOSEEA MEDICAL STORE', '', '', '', '', 'MALIK WALL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(544, 1513, 15, 'AL-SHAMS MEDICAL STORE', '', '', '', 'THANA ROAD', 'KUTHIALA SHEKHAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(545, 1134, 11, 'RIZWAN MEDICAL STORE', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(546, 938, 9, 'MADANI HOSPITAL', '', '', '', '', 'KHARIAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(547, 2160, 2, 'PHARMACY,DR.ZAMEER HOSPITAL', '', '', '', 'G.T.ROAD', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(548, 1135, 11, 'PHARMACY,AZAM HOSPITAL', '', '', '', '', 'KOTLA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(549, 939, 9, 'PHARMACY.MINHAS HOSPITAL', '', '', '', 'DINGA ROAD DHORIA', 'KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(550, 1046, 10, 'IHSAN MEDICAL HALL', '', '', '', 'TANDA CHOWK', 'JALALPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(551, 2128, 2, 'MEHR BROTHERS', '', '', '', 'SARGODHA ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(552, 773, 7, 'FARRUKH MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(553, 1422, 14, 'SALEEM MEDICAL STORE', '', '', '', '', 'SARI ALAMGIR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(554, 951, 9, 'PHARMACY,AL HAMAD H.S', '', '', '', 'SHADOLA ROAD', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(555, 5172, 5, 'MUSLIM G/S', '', '', '', '', 'GUJRAT', 'G', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(556, 1514, 15, 'NOMAN MEDICAL STORE', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(557, 1515, 15, 'ALI SURGICAL HOSPITAL', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(558, 333, 3, 'PUNJAB MEDICAL STORE', '', '', '', '', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(559, 2816, 28, 'DR,SAGEER HASSAN', '', '', '', '', 'GOJRA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(560, 287, 2, 'PHARMACY,UMAR HOSPITAL', '', '', '', '', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(561, 561, 5, 'DR.HAFEEZ-UR-REHMAN', '', '', '', 'HAFEEZ CLINIC', 'BHIMBER ROAD GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(562, 1136, 11, 'KHALID BAKAR', '', '', '', '', 'KOTLA', 'G', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(563, 811, 8, 'PHARMA CARE MEDICAL STORE', '', '', '', '', 'KHARIAN CANT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(564, 940, 9, 'PHARMACY,AMIN CLINIC', '', '', '', '', 'KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(565, 1137, 11, 'REHMANIA MEDICAL STORE', '', '', '', '', 'BADHAR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(566, 334, 3, 'AL FREED MEDICAL STORE', '', '', '', '', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(567, 816, 8, 'DR.MIRZA NASEER AHMED', '', '', '', 'PMDC# 15417-P', 'NEAR POLICE CHOKI GREEN TOWN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(568, 1423, 14, 'PHARMACY,ZAINAB HOSPITAL', '', '', '', 'G.T.ROAD', 'SARI ALAMGIR', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(569, 774, 7, 'PHARMACY,DR.SHAKILA AKHTAR H/S', '', '', '', '', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(570, 290, 2, 'AL SHAFA HOSPITAL', '', '', '', '', 'MANDI BAHUDDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(571, 941, 9, 'IDEAL BAKAR', '', '', '', '', 'KHARIAN', 'G', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(572, 1424, 14, 'HAMAZA MEDICAL STORE', '', '', '', '', 'SARI ALAMGIR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(573, 775, 7, 'PHARMACY,MAHAM CARE & CURE HOSPITAL', '', '', '', '', 'LALAMUSA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(574, 1138, 11, 'USAMAN GHANI MEDICAL & G.S', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(575, 821, 8, 'DR.SALEEM MALIK', '', '', '', 'JAMAL PUR ROAD', 'GUJRAT', 'Doctor', '34201-6454941-3', '0829243-4', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(576, 942, 9, 'PHARMACY,DR.SAJJADA HAFIZ CLINIC', '', '', '', 'G.T.ROAD', 'KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(577, 291, 2, 'PHARMACY,AL GAHNI HOSPITAL', '', '', '', '', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(578, 1139, 11, 'PHARMACY,SKIN VISION CLINIC', '', '', '', 'DR,SALMAN CH', 'KOTLA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(579, 1140, 11, 'PHARMACY,MUBEEN SURGICAL HOSPITAL', '', '', '', 'BHIMBER ROAD', 'KOTLA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(580, 1141, 11, 'PHARMACY,M.ALAM HOSPITAL', '', 'KOTLA', '', '', 'BHIMBER ROAD', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(581, 5163, 5, 'PHARMACY,DR,SYED MAZAHAR ABBAS', '', '', '', 'PAKISTAN CHOWK', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(582, 812, 8, 'PHARMACY,SAFIA IQBAL CLINIC', '', '', '', '', 'KHARIAN CANT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(583, 776, 7, 'BISMILLAH PHARMACY', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(584, 1164, 1, 'SOCIAL SECURITY HOSPITAL', '', '', '', 'G.T.ROAD', 'GUJRAT', 'I', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(585, 292, 29, 'AL REHMAN MEDICAL STORE', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(586, 293, 29, 'YOUSAF MEDICAL STORE', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(587, 294, 29, 'SHAKAR MEDICAL STORE', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(588, 296, 29, 'AL SHAFA MEDICAL STORE', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(589, 297, 29, 'ASHRAF MEDICAL STORE', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(590, 298, 29, 'AL GONDAL MEDICAL STORE', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(591, 299, 29, 'SHAIKH MEDICAL STORE', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(592, 2910, 29, 'ADAM ZADA MEDICAL STORE', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(593, 2911, 29, 'ASLAM MEDICAL STORE', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(594, 2912, 29, 'ALI MEDICAL CENTER', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(595, 2913, 29, 'AL SHAMAS MEDICAL STORE', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(596, 2914, 29, 'NOMAN MEDICAL STORE', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(597, 2915, 29, 'ALI SURGICAL HOSPITAL', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(598, 2020, 20, 'SHAN MEDICAL STORE', '', '', '', 'DINGA CHAWK', 'KUNJAH', 'Retailer', '', '', '', '443', '0', '0', '12/31/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(599, 205, 20, 'BHATTI MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'KUNJAH', 'Retailer', '34201-0379904-1', '6616816-7', '', '0', '47447', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(600, 2019, 20, 'SHAHID MEDICAL STORE', '', '', '', 'OPP R.H.C', 'KUNJAH', 'Retailer', '', '', '', '682', '0', '683', '12/31/2015 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(601, 207, 20, 'DR.ARSHAD CHISHTE', '', '', '', 'MAIN BAZAR', 'KUNJAH', 'Doctor', '34201-0308343-7', '1698230-4', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(602, 2023, 20, '.DR.ARIF MASOOD', '', '', '', 'MAIN BASAR', 'KUNJAH', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(603, 206, 20, 'BISMILLAH MEDICAL STORE', '', '', '', 'NEAR NAVEED KARYANA STORE', 'MAIN BAZAR KUNJAH', 'Retailer', '34201-0440386-1', '7602732-0', '', '9900', '454', '0', '11/30/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(604, 2014, 20, 'FATAH KHAN MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'KUNJAH', 'Retailer', '', '', '', '0', '1152', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(605, 2013, 20, 'EJAZ MEDICAL STORE', '', '', '', 'NEAR KAKA SWEET HOUSE', 'MAIN BAZAR KUNJAH', 'Retailer', '34201-4014366-3', '8981417-2', '', '0', '9187', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(606, 2717, 27, 'PHARMACY,ROMA SURGICAL HOSPITAL', '', '', '', '', 'MONGOWAL', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(607, 2122, 21, '.WARACIH MEDICAL STORE', '', '', '', 'MANDI ROAD', 'MONGOWAL', 'Retailer', '', '', '', '0', '465', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(608, 2113, 21, 'DR.SYED ZUKI UL HASSAN', '', '', '', 'QUBRA HOSPITAL MONGOWAL', 'PMDC# 29755-P', 'Doctor', '', '', '', '29755', '0', '0', '12/31/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(609, 2112, 21, 'DR.SHERAZ MALIK', '', '', '', '', 'MONGOWAL SURGICAL Hp MONGOWAL', 'Doctor', '34201-0551486-1', '3557958-7', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(610, 2121, 21, 'SHAHZAD LATIF PHARMACY', '', '', '', 'MAIN BAZAR', 'MONGOWAL GHARBI', 'Retailer', '', '', '', '32601', '0', '0', '5/19/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(611, 301, 30, 'TARIQ MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(612, 302, 30, 'EJAZ MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(613, 303, 30, 'PUNJAB MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(614, 304, 30, 'SHAHBAZ MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(615, 305, 30, 'KHURSHID MEDICAL STORE', '', '', '', '', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(616, 306, 30, 'ZAHEER MEDICAL STORE', '', '', '', '', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(617, 307, 30, 'KASHMIR MEDICAL STORE', '', '', '', '', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(618, 308, 30, 'HASSAN MEDICAL STORE', '', '', '', '', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(619, 309, 30, 'NAVEED MEDICAL STORE', '', '', '', '', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(620, 3010, 30, 'MAJEED MEDICAL STORE', '', '', '', '', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM');
INSERT INTO `dealer_info` (`dealer_table_id`, `dealer_id`, `dealer_area_id`, `dealer_name`, `dealer_contact_person`, `dealer_phone`, `dealer_fax`, `dealer_address`, `dealer_address1`, `dealer_type`, `dealer_cnic`, `dealer_ntn`, `dealer_image`, `dealer_lic9_num`, `dealer_lic9_exp`, `dealer_lic10_num`, `dealer_lic10_Exp`, `dealer_lic11_num`, `dealer_lic11_Exp`, `dealer_credit_limit`, `dealer_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`) VALUES
(621, 3011, 30, 'YASIR MEDICAL STORE', '', '', '', '', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(622, 3015, 30, 'PHARMACY,AL MUMTAZ HOSPITAL', '', '', '', '', 'DINGA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(623, 3016, 30, 'PHARMACY,KHALEEL HOSPITAL', '', '', '', '', 'DINGA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(624, 3017, 30, 'PHARMACY,KINZA SURGICAL HOSPITAL', '', '', '', '', 'DINGA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(625, 3018, 30, 'PHARMACY,DR.KHALID ARBBE', '', '', '', '', 'DINGA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(626, 3019, 30, 'PHARMACY,AMER JAVAID COMPLEX', '', '', '', '', 'DINGA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(627, 3020, 30, 'PHARMACY,AL SHEFA HOSPITAL', '', '', '', '', 'DINGA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(628, 3021, 30, 'PHARMACY,DR.SHAHNAZ HOSPITAL', '', '', '', '', 'DINGA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(629, 423, 42, 'MANGAT MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'PHARIANWALI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(630, 421, 42, 'IMTIAZ MEDICAL STORE', '', '', '', 'T.H.Q', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(631, 2410, 24, 'SHARIF MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(632, 2411, 24, 'NISAR MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(633, 2413, 24, 'FEIZAN MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(634, 2414, 24, 'NASEER MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(635, 2415, 24, 'TABASUM MEDICAL STORE', '', '', '', 'MANDI ROAD', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(636, 2416, 24, 'SHAHEEN MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(637, 2417, 24, 'MADINA MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(638, 2418, 24, 'ANMOL MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(639, 2412, 24, 'AL SHAFA MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(640, 2419, 24, 'LUCK MEDICAL STORE', '', '', '', '', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(641, 2420, 24, 'ALI MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(642, 2421, 24, 'IRSHAD TRADAR', '', '', '', '', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(643, 2422, 24, 'PUNJAB MEDICAL STORE', '', '', '', '', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(644, 2423, 24, 'PHARMACY,PHALIA M/C', '', '', '', '', 'PHALIA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(645, 2426, 24, 'PHARMACY,AWAN CLINIC', '', '', '', '', 'PHALIA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(646, 2427, 24, 'PHARMACY,AL REHMAN CLINIC', '', '', '', '', 'PHALIA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(647, 2428, 24, 'ZAFAR MEDICAL STORE', '', '', '', '', 'PHALIA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(648, 2429, 24, 'PHARMACY,FAMILY HOSPITAL', '', '', '', '', 'PHALIA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(649, 2430, 24, 'PHARMACY,DR.PARVAIZ NAZEER TARAR', '', '', '', '', 'PHALIA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(650, 2431, 24, 'PHARMACY,DR.KHALIQ DAD TARAR', '', '', '', '', 'PHALIA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(651, 202, 20, 'AHSAN MEDICAL STORE', '', '', '', 'OPP.DHQ', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(652, 203, 20, 'AL GONDAL MEDICAL STORE', '', '', '', 'OPP.DHQ', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(653, 204, 20, 'AL MUSLIM MEDICAL STORE', '', '', '', 'OPP.DHQ', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(654, 208, 20, 'KHANNAN PHARMACY', '', '', '', 'OPP.DHQ', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(655, 209, 20, 'NOOR MEDICAL STORE', '', '', '', 'OPP.DHQ', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(656, 2051, 20, 'PEOPLE,S PHARMACY', '', '', '', 'OPP.DHQ', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(657, 2010, 20, 'PEOPLE,S PHARMACY', '', '', '', 'OPP.DHQ', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(658, 2011, 20, 'REHMAT MEDICAL STORE', '', '', '', 'OPP.DHQ', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(659, 2012, 20, 'SHAH DIN MEDICAL STORE', '', '', '', 'OPP.DHQ', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(660, 2021, 20, 'PUNJAB MEDICAL STORE', '', '', '', 'SADDAR BAZAR', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(661, 2022, 20, 'MIAN MEDICAL STORE', '', '', '', 'COMMETTE BAZAR', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(662, 2015, 20, 'HAFIZ MEDICAL STORE', '', '', '', 'OPP.DHQ', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(663, 2024, 20, 'AWAME MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(664, 2025, 20, 'NEW SUPPER MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(665, 2026, 20, 'FAIZ MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(666, 2027, 20, 'MANSOOR MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(667, 2028, 20, 'MUMTAZ MEDICAL STORE', '', '', '', 'PHALIA ROAD', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(668, 2029, 20, 'PHARMACY,SALEEM HOSPITAL', '', '', '', 'D.H.Q ROAD', 'MANDI BAHUDDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(669, 2030, 20, 'PAK MEDICAL STORE', '', '', '', 'PHALIA ROAD', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(670, 2035, 20, 'MIAN MEDICINE', '', '', '', 'KACHERY ROAD', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(671, 2036, 20, 'MOON MEDICAL STORE', '', '', '', 'KACHERY ROAD', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(672, 2037, 20, 'ARAIB MEDICAL STORE', '', '', '', 'KACHERY ROAD', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(673, 2038, 20, 'HASSAN MEDICAL STORE', '', '', '', 'KACHERY ROAD', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(674, 2031, 20, 'KHALEEL MEDICAL STORE', '', '', '', 'PHALIA ROAD', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(675, 2039, 20, 'MEDICEN PLUS PHARMACY', '', '', '', 'KACHERY ROAD', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(676, 2040, 20, 'SHAHEEN MEDICAL STORE', '', '', '', 'KACHERY ROAD', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(677, 2041, 20, 'NASIR MEDICAL STORE', '', '', '', 'RASOOL ROAD', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(678, 2016, 20, 'NEW NAEEM MEDICAL STORE', '', '', '', 'OPP.DHQ', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(679, 2042, 20, 'AL GHANI CLINIC', '', '', '', '', 'MANDI BAHUDDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(680, 2043, 20, 'SHAIKH MEDICAL STORE', '', '', '', 'RASOOL ROAD', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(681, 2032, 20, 'ABUBAKAR MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(682, 2044, 20, 'AL SHAFA MEDICAL STORE', '', '', '', 'KACHERY ROAD', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(683, 2045, 20, 'AL FALAHA MEDICAL STORE', '', '', '', 'KACHERY ROAD', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(684, 2046, 20, 'AL REHMAN MEDICAL STORE', '', '', '', '', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(685, 2033, 20, 'ALI MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(686, 2047, 20, 'AZEEM MEDICAL STORE', '', '', '', 'DINGA ROAD', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(687, 2048, 20, 'CITY MEDICAL STORE', '', '', '', 'KACHERY ROAD', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(688, 2055, 20, 'PARADAIZ MEDICAL STORE', '', '', '', 'PHALIA ROAD', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(689, 2056, 20, 'JAVAID MEDICAL STORE', '', '', '', 'OOP GOVT DEGREE COLLEGE', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(690, 2057, 20, 'KHAWAJA MEDICAL STORE', '', '', '', 'PHALIA ROAD', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(691, 2058, 20, 'ABID MEDICAL STORE', '', '', '', 'KING CHOWK', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(692, 2059, 20, 'STANDERD MEDICAL STORE', '', '', '', 'KING CHOWK', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(693, 4165, 41, 'PHARMACY,RAKHAT HOSPITAL', '', '', '', 'PHALIA ROAD', 'MANDI BAHUDDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(694, 4166, 41, 'PHARMACY,TARAR HOSPITAL', '', '', '', 'PHALIA ROAD', 'MANDI BAHUDDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(695, 2067, 20, 'PHARMACY,ALVI HOSPITAL', '', '', '', 'PHALIA ROAD', 'MANDI BAHUDDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(696, 2068, 20, 'PHARMACY,AKHTAR JAVAID TUBASAM', '', '', '', '', 'MANDI BAHUDDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(697, 2069, 20, 'PHARMACY,ARSHAD HRT CENTER', '', '', '', '', 'MANDI BAHUDDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(698, 2070, 20, 'PHARMACY,CITY HOSPITAL', '', '', '', '', 'MANDI BAHUDDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(699, 2071, 20, 'PHARMACY,CHEEMA HOSPITAL', '', '', '', '', 'MANDI BAHUDDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(700, 2072, 20, 'PHARMACY,GENRAL HOSPITAL', '', '', '', '', 'MANDI BAHUDDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(701, 2073, 20, 'GONDAL MEDICAL STORE', '', '', '', 'RASOOL ROAD', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(702, 2074, 20, 'PHARMACY,M.AKRAM HOSPITAL', '', '', '', 'RASOOL ROAD', 'MANDI BAHUDDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(703, 2075, 20, 'PHARMACY,SHATIC HOSPITAL', '', '', '', '', 'MANDI BAHUDDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(704, 2076, 20, 'PHARMACY,TARIQ HOSPITAL', '', '', '', 'DINGA ROAD', 'MANDI BAHUDDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(705, 2077, 20, 'PHARMACY,UMER HOSPITAL', '', '', '', '', 'MANDI BAHUDDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(706, 2078, 20, 'PHARMACY,NABEEL SR,HOSPITAL', '', '', '', '', 'MANDI BAHUDDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(707, 2079, 20, 'PHARMACY,AL KARAM HOSPITAL', '', '', '', '', 'MANDI BAHUDDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(708, 2080, 20, 'PHARMACY,HASHMI HEART CENTER', '', '', '', '', 'MANDI BAHUDDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(709, 2081, 20, 'PHARMACY,FAMALY CARE HOSPITAL', '', '', '', '', 'MANDI BAHUDDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(710, 2082, 20, 'PHARMACY.A.T.B ASSOCIATION', '', '', '', 'D.H.Q ROAD', 'MANDI BAHUDDIN', 'I', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(711, 2017, 20, 'ASIF MEDICAL STORE', '', '', '', 'NEAR DHQ', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(712, 2060, 20, 'ITTFAQ MEDICAL STORE', '', '', '', 'KING CHOWK', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(713, 1330, 13, 'DR.SAMARA MOBEEN', '', '', '0314-4171717', 'PMDC# 3737-AJK', 'LIFE CARE CLINIC KOTLA', 'Doctor', '', '', '', '3737', '0', '0', '12/31/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(714, 1320, 13, 'KHAN PHARMACY', '', '0303-9596018', '', 'MAIN BAZAR', 'KOTLA', 'Retailer', '', '', '', '45340', '0', '0', '9/18/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(715, 1321, 13, 'SALEEM & SONS MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'KOTLA ARAB ALI KHAN', 'Retailer', '34202-0645938-1', '7121274-6', '', '309', '0', '309', '12/31/2017 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(716, 3110, 31, 'HUSSAIN MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'KOTLA', 'Retailer', '', '', '', '4112', '0', '0', '5/8/2012 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(717, 1322, 13, 'HAFIZ MEDICAL & GENERAL STORE', '', '0300-6218617', '', 'BHIMBER ROAD', 'KOTLA ARAB ALI KHAN', 'Retailer', '34202-8343408-5', 'A043241-5', '', '842', '0', '2', '12/31/2017 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(718, 1323, 13, '.IQBAL MEDICAL STORE', '', '0301-6269107', '', 'OPP.MCB BHIMBER ROAD', 'KOTLA', 'Retailer', '34202-0763828-1', '6284024-8', '', '0', '3459', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(719, 1326, 13, 'HAQBAHO MEDICAL STORE', '', '', '', 'BHIMBER ROAD', 'KOTLA ARAB ALI KHAN', 'Retailer', '', '', '', '764', '0', '764', '12/31/2015 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(720, 3130, 31, 'KHALID BAKAR', '', '', '', 'BHIMBER ROAD', 'KOTLA', 'G', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(721, 1334, 13, 'DR.IFTIKHAR', '', '', '', 'ALAM HOSPITAL', 'KOTLA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(722, 1335, 13, 'DR.AMANAT', '', '', '', '', 'SUNNAN HOSPITAL KOTLA', 'Doctor', '34201-6342240-5', '0997941-7', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(723, 1336, 13, 'DR.EHSAAN', '', '', '', '', 'NAWAB HOSPITAL KOTLA', 'Doctor', '3420104062175', '1110679-4', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(724, 1327, 13, 'SARWAR PHARMACY', '', '0331-5201003', '', 'LANGRIAL CHAWK', 'KOTLA ARAB ALI KHAN', 'Retailer', '', '', '', '7361', '0', '0', '9/18/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(725, 3123, 31, 'HAMZA MEDICAL STORE', '', '', '', 'NEAR BHU BANDAHAR', 'T,KHARIAN', 'Retailer', '', '', '', '721', '0', '0', '12/31/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(726, 1337, 13, 'DR.ABDUL WAHAB', '', '', '', 'WAHAB HOSPITAL', 'KOTLA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(727, 1338, 13, 'DR.COL.AZAM', '', '', '', 'AZAM HOSPITAL', 'KOTLA ARAB ALI KHAN', 'Doctor', '34201-6628476-3', '4177580-5', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(728, 1310, 13, 'REHMANIA MEDICAL DTORE', '', '', '', '', 'BADHAR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(729, 1339, 13, 'PHARMACY,ZAINAB HOSPITAL', '', '', '', '', 'KOTLA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(730, 3137, 31, 'TAHIR MEDICAL STORE', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(731, 3131, 31, 'KASHMIR VALLY MEDICAL STORE', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(732, 3132, 31, 'NISAR MEDICAL STORE', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(733, 3141, 31, 'KHURSUHED MEDICAL STORE', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(734, 351, 35, 'SAKEENA MEDICAL STORE', '', '', '', '', 'SARI ALAMGER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(735, 352, 35, 'ANSAR MEDICAL STORE', '', '', '', '', 'SARI ALAMGER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(736, 353, 35, 'REHAN MEDICAL STORE', '', '', '', '', 'SARI ALAMGER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(737, 354, 35, 'MARSAB MEDICAL STORE', '', '', '', '', 'SARI ALAMGER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(738, 355, 35, 'YASIR MEDICAL STORE', '', '', '', '', 'SARI ALAMGER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(739, 356, 35, 'AL SHAFA MEDICOSE', '', '', '', 'MAIN BAZAR', 'SARI ALAMGER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(740, 357, 35, 'JAVAID MEDICAL STORE', '', '', '', '', 'SARI ALAMGER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(741, 358, 35, 'TARIQ MEDICAL STORE', '', '', '', 'NEAR RAILWAY LINE', 'SARI ALAMGER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(742, 359, 35, 'ANWAR MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'SARI ALAMGER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(743, 3510, 35, 'MADINA MEDICAL STORE', '', '', '', 'NEAR RAILWAY LINE', 'SARI ALAMGER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(744, 3511, 35, 'NEW IQBAL MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'SARI ALAMGER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(745, 3512, 35, 'KHALID MEDICAL STORE', '', '', '', '', 'SARI ALAMGER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(746, 3513, 35, 'HAMAZA MEDICAL STORE', '', '', '', '', 'SARI ALAMGER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(747, 3514, 35, 'TARIQ MEDICAL STORE', '', '', '', '', 'KHAMBI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(748, 3520, 35, 'PHARMACY,ZAINAB HOSPITAL', '', '', '', '', 'SARI ALAMGER', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(749, 3521, 35, 'PHARMACY,NAZEER BAGUM HOSPITAL', '', '', '', '', 'SARI ALAMGER', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(750, 401, 40, 'SAKEENA MEDICAL STORE', '', '', '', 'G.T.ROAD', 'SARI ALAMGIR', 'Retailer', '', '', '', '813', '0', '813', '12/31/2015 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(751, 402, 40, 'REHAN MEDICOSE', '', '03215915565', '', 'MAIN BAZAR', 'SARI ALAMGIR', 'Retailer', '', '', '', '638', '0', '638', '12/31/2015 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(752, 403, 40, 'YASIR MEDICAL STORE', '', '03455677657', '', 'MAIN BAZAR', 'SARI ALAMGIR', 'Retailer', '', '', '', '877', '0', '877', '12/31/2015 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(753, 404, 40, 'ANSAR MEDICOSE', '', '0544651841', '', 'SADAT MARKET', 'SARI ALAMGIR', 'Retailer', '', '', '', '211', '211', '211', '12/31/2015 12:00 AM', '12/31/2015 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(754, 405, 40, 'MIRZA PHARMACY', '', '', '', 'NEAR U.B.L', 'SARI ALAMGIR', 'Retailer', '', '', '', '1147', '0', '0', '3/8/2014 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(755, 406, 40, 'AL SHAFA MEDICOSE', '', '', '', '', 'SARI ALAMGIR', 'Retailer', '', '', '', '573', '573', '573', '12/31/2013 12:00 AM', '12/31/2013 12:00 AM', '12/31/2013 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(756, 407, 40, 'JAVAID MEDICAL STORE', '', '03435742386', '', 'RAILWAY CROSSING', 'SARI ALAMGIR', 'Retailer', '', '', '', '0', '337', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(757, 408, 40, 'TARIQ MEDICAL STORE', '', '0544650228', '', 'NEAR RAILWAY LINE', 'SARI ALAMGIR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(758, 409, 40, 'PHARMACY,KHUSHHAL HOSPITAL', '', '', '', '', 'SARI ALAMGIR', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(759, 4010, 40, 'ANWAR MEDICAL STORE', '', '03455696092', '', 'NEAR RAILWAY CROSSING', 'SARI ALAMGIR', 'Retailer', '', '', '', '210', '0', '0', '12/31/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(760, 4011, 40, 'NEW IQBAL MEDICOSE', '', '03455722955', '', 'MAIN BAZAR', 'SARI ALAMGIR', 'Retailer', '', '', '', '698', '0', '698', '12/31/2015 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(761, 4012, 40, 'KHALID MEDICOSE', '', '0544653326', '', 'MAIN BAZAR', 'SARI ALAMGIR', 'Retailer', '', '', '', '696', '696', '696', '12/31/2015 12:00 AM', '12/31/2015 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(762, 4013, 40, 'TARIQ MEDICAL STORE', '', '', '', '', 'KHAMBI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(763, 4020, 40, 'PHARMACY,ZANIB HOSPITAL', '', '', '', '', 'SARI ALAMGIR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(764, 4021, 40, 'PHARMACY,NAZEER BAGUM HOSPITAL', '', '', '', '', 'SARI ALAMGIR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(765, 382, 38, '.', '', '', '', '', 'KHARIAN CANT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(766, 1910, 19, 'NEW PAK MEDICAL STORE', '', '0333-5270928', '', 'SADDAR BAZAR', 'KHARIAN CANT', 'Retailer', '34201-0884214-9', '3005822-8', '', '510', '0', '510', '12/31/2015 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(767, 1911, 19, 'PUNJAB MEDICAL STORE', '', '7610141', '', 'SADDER BAZAR', 'KHARIAN CANT', 'Retailer', '', '', '', '825', '0', '825', '12/31/2015 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(768, 195, 19, 'FATEH MEDICAL STORE', '', '0320-5853870', '', 'NEAR C.M.H CHOWK', 'KHARIAN CANT', 'Retailer', '34202-0766645-1', '2463738-6', '', '5837', '0', '0', '9/15/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(769, 388, 38, 'PHRMA CARE MEDICAL STORE', '', '', '', 'OPP.CMH', 'KHARIAN CANT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(770, 192, 19, 'BAIGA MEDICAL STORE', '', '', '', 'SUF SHIVKAN SHOPING COMPLEX', 'KHARIAN CANT', 'Retailer', '', '', '', '79', '0', '79', '12/31/2015 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(771, 3518, 35, 'PHARMACY,KHALID JAMIL', '', '', '', '', 'KHARIAN CITY', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(772, 3523, 35, 'AMIN MEDICAL HALL', '', '', '', 'MAIN BAZAR', 'KHARIAN CITY', 'Retailer', '', '', '', '207', '0', '0', '12/31/2013 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(773, 3527, 35, 'IDEIAL BAKAR', '', '', '', '', 'KHARIAN CITY', 'G', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(774, 1152, 11, 'DR.KHIZAR HAYAT', '', '', '', 'KHIZAR HOSPITAL', 'DINGA ROAD KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(775, 1153, 11, 'DR.MEHMOD AKHTAR MINHAS', '', '0304-8416369', '', '', 'MINHAS Hp DHORIA', 'Doctor', '3420287913717', '0408340-7', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(776, 1832, 18, 'RAFIQUE PHARMACY', '', '0537578159', '', 'NEAR JS BANK', 'GULYANA', 'Retailer', '', '', '', '69642', '0', '0', '4/29/2023 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(777, 1821, 18, 'DR.M.ABBAS', '', '0346-2041650', '', 'PMDC# 25990-P', 'IMTIAZ HOSPITAL GULIANA', 'Doctor', '34202-9806743-6', '4005077-7', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(778, 1814, 18, 'IFTIKHAR MEDICAL STORE', '', '', '', 'KOTLA ROAD', 'GULIANA', 'Retailer', '', '', '', '101', '0', '0', '12/31/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(779, 1812, 18, '.HASHAME MEDICAL STORE', '', '', '', '', 'GULIANA', 'Retailer', '34202-7660594-9', '7918221-3', '', '881', '0', '881', '12/31/2015 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(780, 184, 18, 'AWAN MEDICAL STORE', '', '0537588120', '', 'GULYANA CHAWK', 'GULIANA', 'Retailer', '', '', '', '924', '0', '924', '12/31/2015 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(781, 183, 18, 'AMARA MEDICAL STORE', '', '0333-8401879', '', 'NEAR HBL,KOTLA ROAD', 'GULYANA', 'Retailer', '', '', '', '0', '40540', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(782, 1830, 18, 'TANVEER MEDICAL STORE', '', '', '', 'MANGILA ROAD', 'AMNA ABAD,TEH.KHARIAN', 'G', '', '', '', '0', '5304', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(783, 3215, 32, 'ZUBAIR MEDICAL STORE', '', '', '', 'G.T.ROAD', 'LALAMUSA', 'Retailer', '', '', '', '619', '0', '619', '12/31/2013 12:00 AM', '12/30/1899 12:00 AM', '12/31/2013 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(784, 3217, 32, 'ITTFAQ PHARMACY', '', '', '', 'CHANDNE CHOWK', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(785, 3220, 32, 'NEW USAMA MEDICAL STORE', '', '', '', 'CAMPING GROUND', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(786, 3225, 32, 'AL HAFIZ MEDICAL STORE', '', '', '', 'CAMPING GROUND', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(787, 3226, 32, 'MIR USAMA MEDICAL STORE', '', '', '', 'CHANDNI CHOWK NO 1', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(788, 3227, 32, 'SHAH MEDICOSE', '', '', '', 'DR.NAILA AYUB', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(789, 3228, 32, 'MOON ENTERPRISES', '', '', '', '', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(790, 3232, 32, 'PHARMACY,SARGUN HOSPITAL', '', '', '', 'G.T.ROAD', 'LALAMUSA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(791, 1068, 10, 'FAROOQ MEDICAL STORE', '', '03226416046', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(792, 3244, 32, 'HAIDER MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '528', '528', '528', '12/31/2013 12:00 AM', '12/31/2013 12:00 AM', '12/31/2013 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(793, 3247, 32, 'NEW ZAM ZAM MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(794, 3248, 32, 'PHARMACY,DR.NADEEM AHMED KHAN', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(795, 3249, 32, 'ABUBAKAR MEDICAL STORT', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(796, 3251, 32, 'MAKKHA MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(797, 3253, 32, 'PHARMACY,CAP,ASLAM BHATTI', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(798, 3255, 32, 'AL REHMAN MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(799, 3257, 32, 'BISMILLAH MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(800, 1047, 10, 'PHARMACELL MEDICAL STORE', '', '03156241614', '', 'OPP.YASIN SWEETS', 'MAIN BAZAR LALAMUSA', 'Retailer', '34202-8901445-3', '7365872-2', '', '0', '0', '13826', '12/31/2015 12:00 AM', '12/30/1899 12:00 AM', '11/16/2019 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(801, 3259, 32, 'FRIENDS MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(802, 3260, 32, 'NEW TALAHA MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(803, 1048, 10, 'JANJUA MEDICAL STORE', '', '03334341019', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '781', '781', '781', '12/31/2015 12:00 AM', '12/31/2015 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(804, 1051, 10, 'BUTT MEDICAL STORE', '', '03216204595', '', 'NEAR MADINA GENERAL STORE', 'EID GAH ROAD LALAMUSA', 'Retailer', '', '', '', '5830', '0', '0', '9/11/2019 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(805, 3268, 32, 'JADDHA MEDICOSE', '', '', '', 'OPP MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '1200', '0', '0', '11/16/2014 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(806, 1052, 10, 'MALIK MEDICAL STORE', '', '03006232793', '', 'BAZAR DINACHAKIAN', 'LALAMUSA', 'Retailer', '34202-0729238-3', '4260162-2', '', '0', '533', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(807, 1053, 10, 'KHALID MEDICAL SERVICES', '', '0537-511912', '', 'DINA CHAKIAN BAZAR', 'LALAMUSA', 'Retailer', '34202-5191279-5', '7232237-8', '', '0', '3608', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(808, 1054, 10, 'TARIQ MEDICAL STORE', '', '03216289993', '', 'DINACHAKIAN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '194', '0', '195', '12/31/2015 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(809, 1055, 10, 'SETHI MEDICAL STORE', '', '', '', 'NEAR PURANI GALLA MANDI', 'LALAMUSA', 'Retailer', '3420208123379', '0996092-9', '', '7293', '0', '0', '9/11/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(810, 3262, 32, 'IKRAM MEDICAL STORE', '', '', '', 'DINACHAKIAN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(811, 3273, 32, 'IKRAM MEDICAL STORE', '', '', '', 'DINACHAKIAN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(812, 3280, 32, 'HAFIZ G/S', '', '', '', 'DINACHAKIAN BAZAR', 'LALAMUSA', 'G', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(813, 3281, 32, 'KASHMIR G/S', '', '', '', '', 'LALAMUSA', 'G', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(814, 3282, 32, 'ARIF  G/S', '', '', '', '', 'LALAMUSA', 'G', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(815, 3283, 32, 'UMAIR G/S', '', '', '', '', 'LALAMUSA', 'G', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(816, 2018, 20, 'SHAFIQ MEDICAL STORE', '', '', '', '', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(817, 2483, 24, 'PHARMACY,AL SHAFA HOSPITAL', '', '', '', '', 'MAINDI BAHUDDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(818, 2083, 20, 'PHARMACY,AL SHAFA HOSPITAL', '', '', '', '', 'MANDI BAHUDDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(819, 191, 19, 'PHARMACY,FAZAL HOSPITAL', '', '', '', '', 'JALALPUR', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(820, 1923, 19, 'ALI MEDICAL STORE', '', '', '', '', 'JALALPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(821, 1926, 19, 'BHATTI MEDICAL STORE', '', '', '', '', 'JALALPUR', 'Retailer', '', '', '', '0', '331', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(822, 1914, 19, 'BUTT MEDICAL STORE', '', '', '', '', 'JALALPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM');
INSERT INTO `dealer_info` (`dealer_table_id`, `dealer_id`, `dealer_area_id`, `dealer_name`, `dealer_contact_person`, `dealer_phone`, `dealer_fax`, `dealer_address`, `dealer_address1`, `dealer_type`, `dealer_cnic`, `dealer_ntn`, `dealer_image`, `dealer_lic9_num`, `dealer_lic9_exp`, `dealer_lic10_num`, `dealer_lic10_Exp`, `dealer_lic11_num`, `dealer_lic11_Exp`, `dealer_credit_limit`, `dealer_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`) VALUES
(823, 1912, 19, 'IQRA MEDICAL STORE', '', '', '', '', 'JALALPUR', 'Retailer', '', '', '', '498', '0', '498', '1/1/2013 12:00 AM', '12/30/1899 12:00 AM', '1/1/2013 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(824, 1913, 19, 'KASHMIR MEDICAL STORE', '', '', '', '', 'JALALPUR', 'Retailer', '', '', '', '0', '478', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(825, 1927, 19, 'PHARMACY,DR.NASEEM TANVER', '', '', '', '', 'JALALPUR', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(826, 1940, 19, 'KHURSHID MEDICAL STORE', '', '', '', '', 'FATHAPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(827, 1356, 13, 'LATIF PHARMACY', '', '', '', 'FATEH PUR', 'GUJRAT', 'Retailer', '34201-0562115-9', '1111020-1', '', '0', '72175', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(828, 1942, 19, 'PHARMACY,AZAM HOSPITAL', '', '', '', '', 'FATHAPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(829, 345, 3, 'PHARMACY,MUBASHAR CLINIC', '', '', '', '', 'FATHAPUR', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(830, 3239, 32, 'GOSEEA MEDICAL STORE', '', '', '', '', 'BADHAR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(831, 3238, 32, 'HAMZA MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '721', '0', '0', '12/31/2013 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(832, 2175, 2, 'PHARMACY,BUTT HOSPITAL', '', '', '', 'G.T.ROAD', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(833, 2424, 24, 'SOHAIL MEDICAL STORE', '', '', '', '', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(834, 3284, 32, '.', '', '', '', '', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(835, 2049, 20, 'FRIENDS MEDICOSE', '', '', '', 'OLD RASOOL ROAD', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(836, 2425, 24, 'AL REHMAN MEDICAL STORE', '', '', '', '', 'LADHER KHERD', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(837, 1333, 13, 'DR.IMRAN', '', '', '', 'IQBAL HOSPITAL', 'KAKRALI', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(838, 3012, 30, 'FAZAL MEDICAL STORE', '', '', '', 'CHOWK DHALIANA', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(839, 954, 9, 'QAZI MEDICAL STORE', '', '03216217080', '', 'GUZAR-E-MADINA ROAD', 'GUJRAT', 'Retailer', '', '', '', '198', '0', '0', '12/31/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(840, 970, 9, 'PHARMACY,DR.SAFADAR IQBAL', '', '', '', 'SARGODHA ROAD', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(841, 2034, 20, 'TAHIR MEDICAL STORE', '', '', '', 'KACHERY ROAD', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(842, 1180, 1, 'FRIENDS M/S', '', '', '', '', 'JEHLUM', 'S', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(843, 1056, 10, 'PHARMACY,BANO CLINIC', '', '', '', 'G.T ROAD', 'LALAMUSA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(844, 2050, 20, 'UNITED MEDICINE TRADER', '', '', '', 'SCHOOL MOHALLAHA', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(845, 4022, 40, 'HAMAZA MEDICAL STORE', '', '', '', '', 'SARI ALAMGIR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(846, 3612, 36, 'NEW ALI ZAIN MEDICAL STORE', '', '', '', 'R.H.C MALKAR', 'MALKAA', 'Retailer', '', '', '', '599', '0', '599', '12/31/2013 12:00 AM', '12/30/1899 12:00 AM', '12/31/2013 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(847, 554, 5, 'AL-SAYED PHARMACY', '', '', '', 'BHIMBER ROAD', 'GUJRAT', 'Retailer', '', '', '', '1245', '0', '0', '5/14/2017 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(848, 1340, 13, 'DR.YAHYA', '', '', '', 'FAMALY HOSPITAL', 'KOTLA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(849, 3812, 38, 'MUSA KHAN PHARMACY', '', '', '', 'SAF SHIKAN PLAZA', 'KHARIAN CANTT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(850, 2084, 20, 'PHARMACY,DR.ANSAR IQBAL MIRZA', '', '', '', '', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(851, 1142, 11, 'SOHAIL MEDICAL STORE', '', '', '', 'ADDA CHANNAN', 'TEHSIL KHARIAN', 'Retailer', '', '', '', '729', '0', '729', '12/31/2013 12:00 AM', '12/30/1899 12:00 AM', '12/31/2013 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(852, 1143, 11, 'SHAHEEN MEDICAL STORE', '', '', '', 'ADDA CHANNAN', 'TEHSIL KHARIAN', 'Retailer', '', '', '', '773', '0', '773', '12/31/2013 12:00 AM', '12/30/1899 12:00 AM', '12/31/2013 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(853, 1057, 10, 'PHARMACY,DR.RIAZ ASLAM JANJUWA', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(854, 1827, 18, 'RAFIQUE MEDICAL STORE', '', '03338459652', '', 'MAIN BAZAR', 'NASEERA', 'Retailer', '', '', '', '415', '0', '415', '12/31/2015 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(855, 1826, 18, 'QURASHI MEDICOSE', '', '03338511679', '', 'MAIN BAZAR', 'NASEERA', 'Retailer', '', '', '', '872', '0', '872', '12/31/2015 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(856, 1828, 18, 'RAJA MEDICAL HALL', '', '03338536612', '', 'MAIN BAZAR', 'NASEERA', 'Retailer', '', '', '', '0', '123', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(857, 1200, 1, 'ASIA MEDICAL STORE', '', '', '', 'KABLI GATE', 'GUJRAT', 'Retailer', '', '', '', '565', '0', '0', '12/31/2013 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(858, 1250, 1, 'AL HAIDER MEDICAL STORE', '', '', '', 'RAILWAY ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(859, 1169, 11, 'PHARMACY,MUSHTAQ M/CLINIC', '', '', '', 'DR,ZAFAR MEHDI', 'NASEERA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(860, 952, 9, 'AL-NOOR PHARMACY', '', '', '', 'GHARI AHMAD ABAD', 'GUJRAT', 'Retailer', '', '', '', '1210', '0', '0', '1/8/2017 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(861, 20100, 20, 'MEHTAB MEDICAL STORE', '', '', '', 'NEAR JAMIA MADRISA', 'TEHSIL & DIST MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(862, 1325, 13, 'AL KARAM PHARMACY', '', '0300-6212376', '', 'NEAR HBL BHIMBER ROAD', 'KOTLA', 'Retailer', '34202-1396949-1', 'A013014-0', '', '7046', '0', '0', '9/8/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(863, 2432, 24, 'AL RASHEED PHARMACY', '', '', '', 'MAIN BAZAR', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(864, 340, 3, 'PHARMACY,SADDIQ CLINIC', '', '', '', 'MAIN BAZAR', 'BAHGOWAL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(865, 4023, 40, 'PHARMACY,DR.NAZAR MOH HOS', '', '', '', '', 'SARI ALAMGIR', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(866, 2085, 20, 'PHARMACY,LAHORE LAB', '', '', '', '', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(867, 3213, 32, 'PHARMACY,DR.SALEEM BUTT', '', '', '', 'G.T.ROAD', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(868, 135, 13, '.IMTIAZ MEDICAL STORE', '', '0346-6872415', '', 'BHAMBER ROAD', 'BAZURAGWAL', 'Retailer', '', '', '', '750', '0', '750', '12/31/2013 12:00 AM', '12/30/1899 12:00 AM', '12/31/2013 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(869, 1159, 11, 'PHARMACY,BASSAT CLINIC', '', '', '', '', 'NASEERA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(870, 20110, 20, 'WARRANTY H.S', '', '', '', '', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(871, 3030, 30, 'HASSAN MEDICAL STORE', '', '', '', 'OPP.RHC', 'CHELLANWALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(872, 3031, 30, 'AL REHMAN MEDICAL STORE', '', '', '', 'OPP RHC', 'CHAILLAWALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(873, 1150, 11, '.ZAHEER PHARMACY', '', '', '', 'NEAR RHC PINDI SULTAN PUR', 'TEHSIL KHARIAN', 'Retailer', '', '', '', '7914', '0', '0', '10/25/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(874, 3140, 3, 'PHARMACY,AMMAN CLINIC', '', '', '', '', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(875, 4014, 40, 'AL-MAKKAH PHARMACY', '', '', '', 'G.T.ROAD', 'SARI ALAMGIR', 'Retailer', '', '', '', '1018', '0', '0', '12/31/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(876, 2086, 20, 'PHARMACY,ANWAR FATIMA HOSPITAL', '', '', '', '', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(877, 1930, 19, 'ATTIQUE  PHARMACY', '', '', '', 'MUHALLAH SHAR SHAHI', 'JALALPUR', 'Retailer', '', '', '', '1301', '0', '0', '3/4/2016 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(878, 2612, 26, 'DAUAD MEDICAL STORE', '', '', '', '', 'KUNJAH', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(879, 968, 9, 'DR.BILAL RASHID PAGGANWALA', '', '', '', 'PMDC# 32837-P', 'M.RASHEED HOSPITAL GUJRAT', 'Doctor', '34201-4252434-3', '3116695-4', '', '32837', '0', '0', '12/31/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(880, 4015, 40, 'BAOO JEE PHARMACY', '', '0544653038', '', 'IN FRONT OF RAWALPINDI', 'SARI ALAMGIR', 'Retailer', '', '', '', '1358', '0', '0', '2/1/2017 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(881, 1324, 13, 'NEW SHARIF PHARMACY', '', '0332-9588433', '', 'LANGRIAL CHOWK', 'KOTLA', 'Retailer', '', '', '', '2130', '0', '0', '8/30/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(882, 2087, 20, 'PHARMACY,DR.M.SHAFIQ', '', '', '', 'DAR-UL-SHIFA HOSPITAL', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(883, 1145, 11, 'SHABEER MEDICAL STORE', '', '', '', 'DINGA ROAD', 'NOONAWALI', 'Retailer', '', '', '', '0', '286', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(884, 3560, 35, 'SHIFA MEDICAL STORE', '', '', '', '', 'KHARIAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(885, 3210, 3, 'PHARMACY, SULTAN CLINIC', '', '', '', '', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(886, 3022, 30, 'ALAM PHARMACY', '', '', '', 'ALAM MARKET', 'DINGA T,KHARIAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(887, 3181, 3, 'MARRYUM PHARMACY', 'MUBARK MARKET', '', '', 'GUJRAT', '', 'Retailer', '', '', '', '1031', '0', '0', '3/3/2014 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(888, 2625, 26, 'AL-JANNAT MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'KUNJAH', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(889, 3023, 30, 'MADDANI PHARMACY', '', '', '', 'BOHAR CHOWK', 'DINGA TEHSIL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(890, 2088, 20, 'PHARMACY,DR.ISHTIAQ HOSPITAL', '', '', '', '', 'MANDI BAHUDDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(891, 4141, 4, 'SHIFA PHARMACY', '', '', '', 'BHIMBER ROAD', 'GUJRAT', 'Retailer', '', '', '', '1173', '0', '0', '6/26/2014 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(892, 1946, 19, 'DAR PHARMACY', '', '', '', 'MAIN BAZAR', 'JALALPUR', 'Retailer', '', '', '', '1216', '0', '0', '1/28/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(893, 3214, 32, 'REHMAN MEDICINE COM', '', '', '', 'DINACHAKIAN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '241', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(894, 196, 19, 'KHALID MEDICAL STORE', '', '03404112781', '', 'OPP C.M.H', 'KHARIAN CANTT', 'Retailer', '', '', '', '0', '1507', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(895, 2099, 20, 'PHARMA PLUS PHARMACY', '', '', '', 'PHALIA ROAD', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(896, 3537, 35, 'JUNAID MEDICAL STORE', '', '', '', 'GULYANA RAOD', 'KHARIAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(897, 2817, 28, 'AL SHIFA  MEDICAL STORE', '', '', '', '', 'GOJRA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(898, 1058, 10, 'KHURSHID MEDICAL STORE', '', '03006246914', '', 'DINA CHAKIAN BAZAR', 'LALAMUSA', 'Retailer', '34202-0719856-1', '4254666-4', '', '0', '241', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(899, 3148, 31, 'PHARMACY,DR.RIZWAN HAIDER', '', '', '', 'KOTLA ROAD', 'SADDA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(900, 3164, 3, 'SHERAZ PHARMACY', '', '', '', 'SHAHDAULA ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(901, 41101, 41, 'YOUNAS SONS', '', '', '', 'MANDI BAHUDDIN', '', 'S', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(902, 965, 9, 'LATIF PHARMACY', '', '03316247291', '', 'STAF GALA SARGODHA ROAD', 'GUJRAT', 'Retailer', '34201-7110462-1', '7491027-3', '', '1064', '0', '0', '2/1/2016 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(903, 3024, 30, 'ADAM HOSPITAL', '', '', '', 'DINGA', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(904, 140, 1, 'BUTT BROTHERS PHARMACY', '', '03008620925', '', '', 'KUTCHERY CHOWK GUJRAT', 'Retailer', '34201-4592273-3', '0402584-9', '', '7999', '0', '0', '9/26/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(905, 3614, 36, 'ALI PHARMACY', '', '', '', '', 'GULIANA', 'Retailer', '', '', '', '1068', '0', '0', '2/1/2013 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(906, 2142, 2, 'PEARMACY,WAPDA DISPENSARY', '', '', '', 'SARGODHA ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(907, 3615, 36, 'HAADI PHARMACY', '', '', '', 'LALAMUSA ROAD', 'GULYANA', 'Retailer', '', '', '', '1258', '0', '0', '7/4/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(908, 3538, 35, 'PHARMACY,DR.FOZIA AMIR HOSPITAL', '', '', '', '', 'KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(909, 3143, 3, 'PHARMACY,AL FAZAL HOSPITAL', '', '', '', 'QAMAR SIALVI ROAD', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(910, 3149, 31, 'PHARMACY, NAZEER HOSPITAL', '', '', '', '', 'BHIMBER', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(911, 1202, 1, 'GHUMAN MEDICINE CO', '', '03218540628', '', 'VANIKAY ROAD', 'HAFIZABAD', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(912, 3554, 35, '.', '', '', '', '', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(913, 1341, 13, 'PHARMACY, SKIN VISION HOSPITAL', '', '', '', 'BHIMBER ROAD', 'KOTLA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(914, 3616, 36, 'PHARMACY,FATAMA HOSPITAL', '', '', '', '', 'LAKHARI KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(915, 1342, 13, 'PHARMACY, ASHRAF CLINIC', '', '', '', '', 'KOTLA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(916, 342, 3, 'QADEER MEDICAL HALL', '', '0300-6208883', '', '', 'BHAGOWAL KALAN', 'Retailer', '', '', '', '682', '0', '682', '12/31/2015 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(917, 189, 18, 'HASSAN MEDICAL STORE', '', '03006208882', '', '', 'BHAGOWAL KALAN', 'Retailer', '', '', '', '422', '0', '422', '12/31/2013 12:00 AM', '12/30/1899 12:00 AM', '12/31/2013 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(918, 4024, 40, 'SUNNY MEDICAL STORE', '', '03129400360', '', 'OLD RAILWAY LINE ROAD', 'SARAI ALAMGIR', 'Retailer', '', '', '', '0', '1083', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(919, 3025, 30, 'DAWOOD PHARMACY', '', '', '', 'DINGA ROAD', 'ATTOWALA DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(920, 1863, 18, 'DAWOOD PHARMACY', '', '0300-9579388', '', 'DINGA ROAD', 'ATTOWALA', 'Retailer', '', '', '', '1082', '0', '0', '12/31/2013 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(921, 3032, 30, 'NEW ZAHEER PHARMACY', '', '', '', 'NEAR GOV,MAT', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(922, 1329, 13, '.SEHR PHARMACY', '', '0347-7339080', '', '', 'KOTLA ARAB ALI KHAN', 'Retailer', '', '', '', '37504', '0', '0', '8/9/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(923, 4025, 40, 'CITY PHARMACY', '', '', '', 'INFRONT OF T.H', 'SARI ALAMGIR', 'Retailer', '', '', '', '1148', '0', '0', '3/8/2014 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(924, 4178, 4, 'PHARMACY, DR. ISHFAQ BUTT', '', '', '', '', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(925, 4026, 40, 'BILAL MEDICAL STORE', '', '', '', 'TAWAKAI MARKET', 'SARAI ALAMGIR', 'Retailer', '', '', '', '0', '1425', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(926, 2116, 21, 'IMRAN PHARMACY', '', '', '', 'MAIN BAZAR', 'MANGOWAL GHARBI', 'Retailer', '34201-8933873-1', '3983880-3', '', '25333', '0', '0', '12/5/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(927, 136, 13, 'FAISAL PHARMACY', '', '0333-8487696', '', 'CHOWK', 'DOULAT NAGAR', 'Retailer', '34201-0463859-7', '7365639-3', '', '63609', '0', '0', '11/17/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(928, 2061, 20, 'SADDAT MEDICAL STORE', '', '', '', 'KING ROAD', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(929, 2052, 20, 'PHARMACY,MUKHTAR CLINIC', '', '', '', 'OPP DHQ', 'MANDI BAHUDDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(930, 2053, 20, 'MADINA MEDICAL STORE', '', '', '', 'KING CHOWK', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(931, 341, 3, 'PHARMACY. FAMALY CLINIC', '', '', '', '', 'KARIANWALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(932, 3263, 32, 'AHMEDDIN & SONS PHARMACY', '', 'LALAMUSA', '', '', 'MAIN BAZAR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(933, 2433, 24, 'HASSAN PHARMACY', '', '', '', '', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(934, 2434, 24, 'GONDAL MEDICAL STORE', '', '', '', '', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(935, 3300, 3, 'SITARA BAKAR', '', '', '', 'SAEGODHA ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(936, 2628, 26, 'PHARMACY,RAZA CLINIC', '', '', '', '', 'KUNJAH', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(937, 2179, 2, 'AMJAD MEDICAL STORE', '', '', '', 'RALWAY ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(938, 961, 9, 'AHMED BROTHERS PHARMACY', '', '', '', 'NEAR SATAF GALLA', 'SARGODHA ROAD GUJRAT', 'Retailer', '34201-8258893-1', '1460305-5', '', '63944', '0', '0', '12/14/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(939, 3154, 31, 'HASHIM MEDICAL STORE', '', '', '', 'LANGRIAL CHOWK', 'KOTLA ARAB', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(940, 3033, 30, 'FATHA PHARMACY', '', '', '', '', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(941, 2090, 20, 'ASHIA MEDICAL STORE', '', '', '', '', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(942, 139, 13, '.MADNI MEDICAL STORE', '', '0345-6923570', '', 'NEAR NAGRIAN EXCHANGE', 'CHOOR CHAK', 'Retailer', '', '', '', '3621', '0', '0', '8/30/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(943, 2062, 20, 'EMAAN  MEDICAL STORE', '', '', '', '', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(944, 1810, 18, 'HAMZA MEDICAL STORE', '', '', '', 'AHMED DIN MARKET', 'GULYANA', 'Retailer', '', '', '', '0', '1104', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(945, 343, 3, 'QAMAR PHARMACY', '', '', '', '', 'KARIANWALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(946, 3156, 31, 'BAJWA PHARMACY', '', '', '', 'NEAR RURAL HEALTH CENTER', 'DAULAT NAGAR', 'Retailer', '', '', '', '1160', '0', '0', '4/5/2014 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(947, 2818, 28, 'RANJHA MEDICAL STORE', '', '', '', '', 'GOJRA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(948, 2063, 20, 'HASSAM MEDICINE', '', '', '', 'OPP D.H.Q', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(949, 2054, 20, 'SALEEM MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(950, 1343, 13, 'DR.SHEHZAD-UL-HASSAN', '', '0305-6277062', '', '', 'SHABIR HOSPITAL KOTLA', 'Doctor', '3420201548923', '0407863-2', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(951, 3158, 31, 'SHAHZAD MEDICAL STORE', '', '', '', 'PINDI HUNJA', 'BHEMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(952, 1186, 1, 'MAJEED SONS PHARMACY', '', '', '', '', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(953, 4187, 4, 'AL HIDAMAT FONDATION', '', '', '', '', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(954, 3237, 32, 'LUCKY SEVEN MEDICAL STORE', '', '', '', 'NEAR FRUIT MANDI', 'LALAMUSA', 'Retailer', '', '', '', '0', '422', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(955, 1819, 18, 'PHARMACY,BASHARAT H/S', '', '', '', '', 'BAHGOWAL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(956, 2630, 26, 'MADINA PHARMACY', '', '', '', 'MAIN BAZAR', 'KUNJAH', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(957, 3159, 31, 'AL-IMRAN MEDICAL STORE', '', '', '', 'BHIMBER ROAD', 'GHUNDRA KALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(958, 3571, 35, 'AFTAB MEDICAL STORE', '', '', '', 'GULINA ROAD', 'KHARIAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(959, 3619, 36, 'PHARMACY,HASHAME W/F/H', '', '', '', '', 'GULIANA', 'Retailer', '', '', '', '881', '0', '881', '12/31/2013 12:00 AM', '12/30/1899 12:00 AM', '12/31/2013 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(960, 3277, 32, 'ZULFIQAR MEDICAL SERVIC', '', '', '', '', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(961, 2916, 29, 'PHARMACY, DR.ROBINA M/H', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(962, 2091, 20, 'AHMED MEDICAL STORE', '', '', '', '', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(963, 2092, 20, 'AL HAMZA MEDICAL STORE', '', '', '', 'COMMITTEE CHOWK', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(964, 2118, 21, 'NEW KASHMIR MEDICAL STORE', '', '', '', 'OPP Govt,Meternity Hospital', 'MANGOWAL GHARBI', 'Retailer', '34201-4240382-5', '5479247-2', '', '0', '64061', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(965, 4027, 40, 'AWAN BROTHERS PHARMACY', '', '', '', 'G.T.ROAD', 'SARI ALAMGIR', 'Retailer', '', '', '', '1136', '0', '0', '1/12/2016 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(966, 1050, 10, 'DR.ATIF MAHMOOD', '', '', '', 'PMDC# 21094-P', 'AMNA HOSPITAL LALAMUSA', 'Doctor', '34202-1151677-5', '0406944-7', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(967, 2119, 21, 'NEW WARRAICH MEDICAL STORE', '', '', '', 'OPP GOVT.MATERNITY HP', 'MONGOWAL GHARBI', 'Retailer', '', '', '', '0', '24488', '0', '3/31/2017 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(968, 2110, 21, 'DR.QAISER NADEEM', '', '', '', 'HADDI CLINIC', 'MONGOWAL', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(969, 4147, 4, 'TAYYAB PHARMACY', '', '', '', 'NEAR AZIZ BHATTI H/S', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(970, 3161, 31, 'UMER MEDICAL STORE', '', '', '', 'ROMLOW TEHSIL', 'BHAMBIR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(971, 2089, 20, 'PHARMACY,DR.SHAFIQ CLINIC', '', '', '', '', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(972, 3265, 32, 'ZULFIQAR MEDICAL STORE', '', '', '', '', 'LALAMUSA', 'Retailer', '', '', '', '1017', '0', '0', '12/31/2013 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(973, 5148, 5, 'PHAENACY, AHMED HOSPITAL', '', '', '', '', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(974, 2093, 20, 'DAWAKHANA RAZIVA', '', '', '', 'CHOWK NOOR MASJID', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(975, 567, 5, 'DR.ZAHIDA BABAR', '', '0300-9623598', '', 'PMDC# 1499-AJK', 'IBRAHIM HOSPITAL GUJRAT', 'Doctor', '34201-0545792-8', '3827529', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(976, 3266, 32, 'KARAM ELLAHI PHARMACY', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(977, 2635, 26, 'ARIF MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'KUNJAH', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(978, 1968, 19, 'SHEIKH BROTHERS PHARMACY', '', '', '', 'MAIN BAZAR', 'JAJALPUR', 'Retailer', '', '', '', '1145', '0', '0', '2/17/2014 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(979, 4028, 40, 'ZAM ZAM PHARMACY', '', '', '', 'NEAR RAILWAY GATE', 'SARAI ALAMGIR', 'Retailer', '', '', '', '1146', '0', '0', '2/20/2014 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(980, 337, 3, 'DR.GULZAAR AHMED', '', '', '', '', 'IRSHAD HOSPITAL KOTLI KOHALA', 'Doctor', '34201-1227947-9', '1156586', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(981, 2094, 20, 'PHARMACY, FAMALY CLINIC', '', '', '', 'JAIL ROAD', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(982, 4029, 40, 'PHARMACY,DR,M.RIAZ BARRA', '', '', '', '', 'SARAI ALAMGIR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(983, 1344, 13, 'PHARMACY, MUSHTAQ HOSPITAL', '', '', '', '', 'KOTLA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(984, 2917, 29, 'PHARMACY, SHAFI HOSPITAL', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(985, 1060, 10, 'HAJI GULZAR SURGICAL PHARMACY', '', '', '', 'MALL ROAD,CHANDNI CHOWK NO.1', 'LALAMUSA', 'Retailer', '', '8239813-7', '', '32391', '0', '0', '4/26/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(986, 962, 9, 'AZHAR MEDICAL STORE', '', '03445306998', '', 'MOHALLAH QUTAB ABAD', 'SARGODHA ROAD GUJRAT', 'Retailer', '34201-0481925-7', '2735177-7', '', '0', '33234', '0', '12/31/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(987, 953, 9, 'NEW ALAM MEDICAL STORE', '', '03009620693', '', 'MOHALLAH KALU PURA', 'GUJRAT', 'Retailer', '34201-8033083-5', '3421428-3', '', '0', '1159', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(988, 1331, 13, 'HASHIM PHARMACY', '', '0300-8529348', '', 'LANGRIAL CHOWK', 'KOTLA', 'Retailer', '', '', '', '6681', '0', '0', '9/18/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(989, 1226, 12, 'USMAN MEDICAL STORE', '', '', '', '', 'BARNALA', 'Retailer', '', '', '', '291', '0', '0', '12/31/2018 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(990, 367, 3, '.MAQSOOD MEDICAL STORE', '', '', '', '', 'JALALPUR SOBATIAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(991, 365, 3, 'TOQEER PHARMACY', '', '0307 6214114', '', '', 'JALALPUR SOBATIAN', 'Retailer', '', '', '', '978', '0', '0', '12/31/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(992, 362, 3, 'PHARMACY,AL HAMAD HOSPITAL', '', '', '', '', 'JALALPUR SOBATIAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(993, 1217, 12, 'PHARMACY,DR MASOOD AKHTAR', '', '', '', '', 'LORAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(994, 175, 1, '.ATIF PHARMACY', '', '03009621843', '', 'KHAWAJGEN ROAD', 'GUJRAT', 'Retailer', '', '', '', '1514', '0', '0', '10/20/2018 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(995, 4188, 4, 'PHARMACY,SADDIQ HOSPITAL', '', '', '', 'REHMAN SHAHEED ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(996, 3562, 35, 'PHARMACY, MAQBOOL NASEEM HOSPITAL', '', '', '', '', 'KHARIAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(997, 4199, 4, 'PHARMACY,AL TARIQ HOSPITAL', '', '', '', '', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(998, 1062, 10, '.HASSAN PHARMACY', '', '', '', 'MAIN BAZAR', 'DEONA MANDI', 'Retailer', '', '', '', '1154', '0', '0', '3/25/2016 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(999, 1345, 13, 'PHARMACY, SAADE CLINIC', '', '', '', 'SOBOR CHOWK', 'KOTLA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1000, 2203, 2, 'JAVAID PHARMACY', '', '', '', 'G.T.ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1001, 360, 3, 'ALAM MEDICAL STORE', '', '', '', '', 'JALALPUR SOBATIAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1002, 1231, 12, 'ALI MEDICAL STORE', '', '', '', 'ABDUL REHMAN MARKET', 'AWAN SHARIF', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1003, 1973, 19, 'PHARMACY,SHILOKH HOSPITAL', '', '', '', '', 'JALALPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1004, 363, 3, 'SABAR MEDICAL STORE', '', '', '', '', 'AWAN SHARIF', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1005, 3618, 36, 'NEW TAMOOR PHARMACY', '', '', '', 'OPP RURAL HEALTH CENTER', 'MALKA', 'Retailer', '', '', '', '1040', '0', '0', '4/4/2014 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1006, 5300, 5, 'RIZWAN G/S', '', '', '', '', 'GUJRAT', 'G', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1007, 2301, 2, 'RAJA B/K', '', '', '', '', 'GUJRAT', 'G', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1008, 3302, 3, 'MOON G/S', '', '', '', '', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1009, 3034, 30, 'AL KUWAIT PHARMACY', '', '', '', 'THANA ROAD', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1010, 3279, 32, 'SAFADAR MEDICAL STORE', '', '', '', 'G.T.ROAD', 'JORA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1011, 4303, 4, 'T.MART G/S', '', '', '', '', 'GUJRAT', 'G', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1012, 1346, 13, 'DR.AMJAD HUSSAIN', '', '', '', 'PMDC# 1889-AJK', 'HASSAN ORTHOPEDIC Hp KOTLA', 'Doctor', '81102-0788768-3', '7363978-7', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1013, 3167, 31, 'PHARMACY,TOKHEED HOSPITAL', '', '', '', '', 'MACHOWAL', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1014, 2095, 20, 'NEW ASIF MEDICAL STORE', '', '', '', 'NEAR AKRAM H/S', 'MANDI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1015, 338, 3, 'PHARMACY,M. KHAN HOSPITAL', '', '', '', '', 'SAGHAR TANDA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1016, 339, 3, 'PHARMACY,MOHSAN CLINIC', '', '', '', '', 'BAGOWALL', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1017, 3304, 3, 'HIGH CLASS B/K', '', '', '', '', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1018, 3168, 31, 'DISCOUNT MEDICAL STORE', '', '', '', 'OPP D.H.Q', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1019, 1818, 18, 'PHARMACY, WALI HOSPITAL', '', '', '', '', 'KARIANWALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1020, 1059, 10, 'NEW KAMAL MEDICAL CENTER', '', '03227380072', '', 'NEAR NISAR HOSPITAL', 'G.T ROAD LALAMUSA', 'Retailer', '34202-5783142-3', '0812303-9', '', '0', '8218', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1021, 3286, 32, 'SANDHU USAMA MEDICAL STORE', '', '', '', '', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1022, 3287, 32, 'AL NOOR MEDICAL STORE', '', '03424660415', '', 'G.T.ROAD', 'LALAMUSA', 'Retailer', '', '', '', '420', '0', '0', '12/31/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1023, 5305, 5, 'G.M BAKAR', '', '', '', '', 'GUJRAT', 'G', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1024, 1221, 12, 'ZESHAN MEDICAL STORE', '', '', '', '', 'BRANALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1025, 3169, 31, 'ABDULLAH MEDICAL STORE', '', '', '', 'SHER JANG COLONY', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1026, 4306, 4, '7/11 BAKAR', '', '', '', 'BHAMBER ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM');
INSERT INTO `dealer_info` (`dealer_table_id`, `dealer_id`, `dealer_area_id`, `dealer_name`, `dealer_contact_person`, `dealer_phone`, `dealer_fax`, `dealer_address`, `dealer_address1`, `dealer_type`, `dealer_cnic`, `dealer_ntn`, `dealer_image`, `dealer_lic9_num`, `dealer_lic9_exp`, `dealer_lic10_num`, `dealer_lic10_Exp`, `dealer_lic11_num`, `dealer_lic11_Exp`, `dealer_credit_limit`, `dealer_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`) VALUES
(1027, 4307, 4, 'CAKE CENTER', '', '', '', '', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1028, 2189, 2, 'PHARMACY, SOCICAL SECURITY HOSPITAL', '', '', '', 'G.T.ROAD', 'GUJRAT', 'I', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1029, 4308, 4, 'BILAL BAKAR', '', '', '', 'JAIL ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1030, 335, 3, 'PHARMACY,HAJI FAZAL HUSSAN', '', '', '', '', 'HAJIWALA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1031, 5162, 5, 'DAWOOD BUTT PHARMACY', '', '03344665705', '', 'NEAR AZIZ BHATTI S.H', 'GUJRAT', 'Retailer', '', '', '', '1188', '0', '0', '8/30/2016 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1032, 2192, 2, 'PHARMACY,SHAMSHAD HOSPITAL', '', '', '', 'NEAR G.T.S ADA', 'GUJRAT', 'Retailer', '', '', '', '0', '1159', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1033, 1225, 12, 'DR.IKRAAM DAR', '', '', '', 'AL BARKAT HOSPITAL', 'BARNALA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1034, 3288, 32, 'MUGHAL MEDICAL STORE', '', '', '', 'GUNJA ROAD ROAD', 'LALAMUSA', 'Retailer', '', '', '', '473', '0', '0', '12/31/2013 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1035, 3564, 35, 'PHARMACY, HUMAYUN MEDICAL /C', '', '', '', 'G.T.ROAD', 'KHARIAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1036, 1063, 10, 'KAMAL MEDICAL STORE', '', '0321-6286123', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '34202-2721651-3', '4177453-1', '', '0', '38674', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1037, 1980, 19, 'MUNIR MEDICAL STORE', '', '', '', '', 'JALALPUR JATTAN', 'Retailer', '', '', '', '711', '0', '0', '12/31/2013 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1038, 4030, 40, 'AL UMAR MEDICAL STORE', '', '', '', '', 'SARI ALAMIGER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1039, 361, 3, 'KHAN MEDICAL STORE', '', '', '', 'GHULAM HAIDER CHOWK', 'AWAN SHARIF', 'Retailer', '', '', '', '0', '14383', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1040, 2220, 2, 'NOOR-UL-HASSAN PHARMACY', 'NEAR BUSS STAND', '', '', 'MAIN BAZAR', 'SHADIWAL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1041, 1064, 10, 'DR.IJAZ HUSSAIN', '', '', '', 'G.T.ROAD', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1042, 1981, 19, 'MUH. ALI PHARMACY', '', '', '', 'MAIN BAZAR', 'FATEH PUR', 'Retailer', '', '', '', '0', '1168', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1043, 0, 0, '', '', '', '', '', '', '', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1044, 4016, 40, 'PHARMACY,DR,IMRAN MAHMOOD', '', '', '', '', 'SARI ALAMGER', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1045, 3573, 35, 'PHARMACY,AL NEMAT CLINIC', '', '', '', '', 'KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1046, 3577, 35, 'SHAHEEN PHARMACY', '', '', '', 'GULYANA ROAD', 'KHARIAN', 'Retailer', '', '', '', '1219', '0', '0', '3/3/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1047, 4031, 40, 'PHARMACY,WAJJID CLINIC', '', '', '', '', 'SARI ALAMGIR', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1048, 3170, 31, 'AKRAM PHARMACY', '', 'GUJRAT', '', '', 'QADIR COLONY', 'Retailer', '', '', '', '1220', '0', '0', '2/8/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1049, 4032, 40, 'TAHIR PHARMACY', '', '', '', 'RAILWAY CROSSING', 'SARAI ALAMGIR', 'Retailer', '', '', '', '1227', '0', '0', '2/26/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1050, 3574, 35, 'PHARMACY, SARDAR HOSPITAL', '', '', '', 'G.T.ROAD', 'KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1051, 1300, 1, 'PREMIER AGENCIES', '', '', '', 'SAMAL IND,STATE NO 1', 'GUJRANWALA', 'S', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1052, 1149, 11, 'IRFAN PHARMACY', '', '', '', 'DINGA ROAD', 'KHARIAN', 'Retailer', '', '', '', '1141', '0', '0', '2/15/2016 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1053, 150, 1, 'WARRAICH PHARMACY', '', '0336-9625178', '', 'SHAH JEHANGIR ROAD', 'NEAR ZAHOOR PALACE GUJRAT', 'Retailer', '34201-2823840-1', '7217691', '', '7951', '0', '0', '9/26/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1054, 482, 4, 'MALIK BROTHER PHARMACY', '', '', '', 'COURT ROAD', 'GUJRAT', 'Retailer', '', '', '', '1254', '0', '0', '6/3/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1055, 3171, 31, 'MIAN JEE MEDICAL STORE', '', '', '', 'R.H.C,', 'DAULAT NAGAR', 'Retailer', '', '', '', '0', '1259', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1056, 3172, 31, 'IMRAN MEDICAL STORE', '', '', '', 'MOHRA SADDA', 'BHIMBER', 'Retailer', '', '', '', '0', '1263', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1057, 3173, 31, 'HAFIZ PHARMACY', '', '', '', 'NEAR H/C', 'DAYLAT NAGAR', 'Retailer', '', '', '', '1260', '0', '0', '7/14/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1058, 366, 3, 'SAQLAIN PHARMACY', '', '0342 6834866', '', 'AWAN SHARIF ROAD', 'JALALPUR SOBTIAN', 'Retailer', '', '', '', '1320', '0', '0', '6/30/2016 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1059, 1983, 19, 'PHARMACY, UNITED HOSPITAL', '', '', '', '', 'JALALPUR', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1060, 3579, 35, 'FAIKA SITAR HOSPITAL', 'G.T.ROAD', '', '', 'G.T. ROAD', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1061, 186, 1, 'DR.ABDUL QADOOS BUTT', '', '', '', 'COURT ROAD', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1062, 1984, 19, 'MEHAR M UZAIR PHARMACY', 'JALALPUR JATTAN', '', '', 'JALALPUR  JATTAN', '', 'Retailer', '', '', '', '1267', '0', '0', '8/21/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1063, 4162, 4, 'IQBAL MOMORIAL HOSPITAL', 'CIVIL LINE', '', '', 'GUJRAT', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1064, 3580, 35, 'BLUE GRASS', 'G.T.ROAD.PUNJANKASAN', '', '', 'G.T.ROAD.PUNJANKASANA', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1065, 3581, 35, 'U-MART', 'G.T.ROAD', '', '', 'G.T.ROAD PUNJANKASANA', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1066, 3621, 36, 'KHADIM MEDICOSE', '', '', '', 'LAMMAY TEHSIL', 'KHARIAN', 'Retailer', '', '', '', '897', '0', '0', '12/31/2013 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1067, 4175, 4, 'PHARMACY, ZARA HOSPITAL', '', '', '', 'COURT ROAD', 'GUJRAT 03226916227', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1068, 636, 6, 'ASLAM MEDICAL STORE', '', '', '', 'MAIN ROAD', 'KIRANWALA', 'Retailer', '', '', '', '486', '486', '486', '12/31/2013 12:00 AM', '12/31/2013 12:00 AM', '12/31/2013 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1069, 399, 3, 'MEDIZONE PHARMACY', '', '', '', 'KABLI GATE NEAR H.B.L', 'GUJRAT', 'Retailer', '', '', '', '1285', '0', '0', '11/24/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1070, 1148, 11, 'ABDULLAH PHARMACY', '', '', '', 'UTTAM CHOWK DINGA ROAD', 'KHARIAN', 'Retailer', '', '', '', '1341', '0', '0', '9/1/2016 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1071, 3174, 31, 'SAJJAD PHARMACY', '', '', '', 'MAIN BAZAR', 'DAULT NAGAR', 'Retailer', '', '', '', '1290', '0', '0', '1/5/2016 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1072, 1061, 10, 'DR.HAFAZ MUDASAR', '', '', '', 'NOOR HOSPITAL', 'MAIN BAZAR LALAMUSA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1073, 3620, 36, 'HAMZA PHARMACY', '', '', '', 'ZIA UL HAQ CHOWK', 'MALKA', 'Retailer', '', '', '', '1297', '0', '0', '2/10/2016 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1074, 346, 3, 'PHARMACY,HAJI REHMAT F/D', '', '', '', '', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1075, 4017, 40, 'AL KOUSAR PHARMACY', '', '', '', 'MOHA ROAD', 'SARAI ALAMGIR', 'Retailer', '', '', '', '1302', '0', '0', '3/4/2016 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1076, 1328, 13, 'CHAUDHARY PHARMACY', '', '', '', 'PURANA KOTLA CHOWK', 'KOTLA ARAB ALI KHAN', 'Retailer', '', '', '', '1357', '0', '0', '2/1/2017 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1077, 4033, 40, 'PHARMACY, AL SHIFA HOSPITAL', '', '', '', '', 'SARI ALAMGIR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1078, 1833, 18, 'PHARMACY, A.D GONDAL', '', '', '', 'KOTLI PRMANAND', 'KOTLI PRMANAND', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1079, 424, 42, 'PHARMACY,ISHTIAQ HOSPITAL', '', '', '', '', '', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1080, 545, 5, 'DR.ZAFAR IQBAL', '', '', '', 'PMDC# 16467-P', 'ZAFAR SURGICAL Hp GUJRAT', 'Doctor', '', '', '', '16467', '0', '0', '12/31/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1081, 4101, 4, 'PHARMACY,FAZAL WELFAIR SOCITY', 'BHIMBER ROAD', '', '', 'GUJRAT', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1082, 3572, 35, 'CARE MEDICAL STORE', '', '', '', 'PINDI SULTANPUR', 'KHARIAN', 'Retailer', '', '', '', '0', '1313', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1083, 15, 1, 'BUTT TRADERS', '', '', '', 'NEAR KIRAN CINMA', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1084, 4018, 40, 'PHARMACY, DR, SIRAJ MUNEER', '', '', '', '', 'SARI ALAMGIR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1085, 344, 3, 'DANIYAL MEDICAL STORE', '', '', '', 'OPP DARA MASTER RIAZ', 'VILLAGE BHANGRANWALA', 'Retailer', '34201-0581480-7', '5575776-6', '', '0', '2706', '0', '8/24/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1086, 4182, 4, 'SHAHID MEDICAL STORE', '', '', '', 'BADSHAHI ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '0', '12/31/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1087, 2611, 26, 'PHARMACY, NATIONAL MEDICAL/C', '', '', '', 'MAIN ROAN', 'SHADIWAL', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1088, 3540, 35, 'PHARMACY, DR. SABA NAZ CLINIC', '', '', '', '', 'KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1089, 137, 13, 'ADNAN PHARMACY', '', '03354421684', '', 'KHARIAN ROAD', 'DAULAT NAGAR', 'Retailer', '', '', '', '1331', '0', '0', '7/14/2016 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1090, 3180, 31, 'PHARMACY ALI  H/P', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1091, 527, 5, 'DR.M.IKRAAM', '', '053-3605379', '', '', 'IKRAAM HOSPITAL GUJRAT', 'Doctor', '34201-0669563-5', '2996394-0', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1092, 3815, 38, 'BOOTS INTERNATIONAL PHARMACY', '', 'KHARIAN CANTT', '', '', 'NEAR C.M.H', 'Retailer', '', '', '', '1330', '0', '0', '6/30/2016 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1093, 533, 5, 'SADAAT MEDICAL STORE', '', '', '', 'MADINA', 'GUJRAT', 'Retailer', '', '', '', '455', '0', '0', '12/31/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1094, 4118, 4, 'PHARMACY, SAAD HOSPITAL', '', '', '', 'ALI PUR ROAD', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1095, 3622, 36, 'PHARMACY, AL SHIFA CLINIC', '', '', '', '', 'MALKA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1096, 3816, 38, 'ALI PHARMACY', 'SAFF SHIKAN PLAZA', '', '', 'KHARIAN CANTT', '', 'Retailer', '', '', '', '0', '0', '0', '8/4/2016 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1097, 4034, 40, 'ANSER PHARMACY', '', '0544651350', '', 'NEAR RAILWAY LINE', 'SARI ALAMGIR', 'Retailer', '', '', '', '1346', '0', '0', '10/1/2016 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1098, 1065, 10, 'DR.ZAFAR AMIN NIAZI', '', '', '', 'NIAZI HOSPITAL', 'G.T.ROAD LALMAUSA', 'Doctor', '34202-8059059-1', '1302903-7', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1099, 4300, 4, '.', '', '', '', '', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1100, 26200, 26, 'TAUSEEF MEDICAL STORE', '', '', '', 'SHADIWAL ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1101, 1066, 10, 'DR.ASIF MALIK', '', '', '', 'MALIK HOSPITAL', 'LALAMUSA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1102, 4106, 4, 'PHRMACY JAWARIYA LIAQAT', 'JAIL ROAD', '', '', 'GUJRAT', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1103, 5301, 5, 'CASH & CARY', '', '', '', 'SARWAR GOLD PLAZA', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1104, 1227, 12, '.AL-ZIA MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'KOTJAMEL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1105, 5304, 5, 'SHAFIQ G/S', '', '', '', '', '', 'G', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1106, 5306, 5, 'GOSIA BAKAR', '', '', '', '', '', 'G', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1107, 5307, 5, 'A.REHMAN BAKAR', '', '0345-5041102', '', '', '', 'G', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1108, 4309, 4, 'T.MART SP/S', '', '', '', '', '', 'G', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1109, 3303, 3, 'FUJI G/S', '', '', '', '', '', 'G', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1110, 2305, 2, 'ZAIQA BAKAR', '', '', '', '', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1111, 3306, 3, 'WATTAN BAKAR', '', '', '', '', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1112, 3307, 3, 'GOSIA B/K', '', '', '', '', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1113, 3301, 3, 'BAO BAKAR', '', '', '', '', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1114, 5310, 5, 'AL FAJAR S/M', '', '', '', '', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1115, 5311, 5, 'GULSHAN B/K', '', '', '', '', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1116, 110, 1, 'ARSHAD PHARMACY', '', '0300-9628726', '', 'NEAR MASJID AL HADEES', 'MUSLIM BAZAR GUJRAT', 'Retailer', '34201-0394115-5', '34201-0394115-5', '', '7037', '0', '0', '10/24/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1117, 4019, 40, 'PHARMACY, KAMRAN CLINIC', '', '', '', '', 'SARI ALAMGIR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1118, 4035, 40, 'PHARMACY, BARRA CLINIC', '', '', '', '', 'SARI ALAMGER', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1119, 4036, 40, 'ZAHEER MEDICAL STORE', '', '', '', 'SHAKRELLA', 'SARAI ALAMGIR', 'Retailer', '', '', '', '911', '0', '0', '12/31/2013 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1120, 824, 8, 'DR.SYED BABAR HUSSNAIN', '', '', '', 'PMDC# 37156-P', 'AL-MUZAMIL CLINIC GUJRAT', 'Doctor', '34201-3272073-3', '3903458-5', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1121, 181, 1, 'GREEN PLUS PHARMACY', '', '03334512167', '', '15-A NEW MARGHZAR COLONY', 'GUJRAT', 'Retailer', '', '', '', '1372', '0', '0', '4/2/2017 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1122, 1311, 13, 'DR.TAYYABA SURAYA', '', '0336-6835837', '', 'PMDC#104274-P', 'AMEER HUSSAIN HOSPITAL KAKRALI', 'Doctor', '34603-5443406-9', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1123, 526, 5, 'GUJRAT PHARMACY', '', '03127629302', '', 'OPP,IKRAM Hp', 'BHIMBER ROAD GUJRAT', 'Retailer', '', '', '', '1379', '0', '0', '4/25/2017 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1124, 964, 9, 'KHALID MEDICAL STORE', '', '', '', 'GALI SIRSYED COLLEGE', 'GUJRAT', 'Retailer', '', '', '', '0', '275', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1125, 3650, 36, 'SITARA BAKARS', '', '', '', 'GULYANA', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1126, 3530, 35, 'AHMED BROTHERS MEDICAL STORE', '', '', '', 'VILLAGE SHAH SARMUST', 'TEHSIL KHARIAN', 'Retailer', '', '', '', '917', '0', '917', '12/31/2015 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1127, 4045, 40, 'IMTIAZ MEDICAL STORE', '', '', '', 'DANDI DARA SARA-I-ALAMGIR 0346', '6107165', 'Retailer', '', '', '', '17396', '0', '0', '12/31/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1128, 40301, 40, 'HABIB BAKERS', '', '', '', 'SARI ALAMGIR', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1129, 3128, 31, 'SHAHEEN BAKER', '', '', '', 'KOTLA', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1130, 3129, 31, 'SHAHEEN BAKER', '', '', '', 'MAIN BAZAR  KOTLA', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1131, 19300, 19, 'PAK BAKERS', '', '', '', 'KHOKHA STOP 0301.3038559', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1132, 19301, 19, 'AHMED BAKERS', '', '', '', 'FATEH PUR.03076262403', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1133, 3649, 36, 'SUPPER AFZAL B/K', '', '', '', '', 'GULIANA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1134, 963, 9, 'DANIYAL MEDICAL STORE', '', '0333-8432553', '', 'NEAR WAHID TRUST HOSPITAL', 'G.T.ROAD GUJRAT', 'Retailer', '34201-2392821-7', '5321437-7', '', '0', '10442', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1135, 32200, 32, 'FRIENDS TREDERS', '', '', '', 'DINACHAKIAN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1136, 1500, 1, 'ACME PHARMACEUTICAL CO', '', '', '', '12 RAJA RAM ST(GWAL MANDI)', 'LAHORE', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1137, 32305, 32, 'CRESANT BAKERS', '', '', '', 'LALAMUSA', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1138, 12300, 12, 'KHALEEL VARITY STORE', '', '', '', 'BRANALA', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1139, 12301, 12, 'RASHID BAKERS', '', '', '', 'KOTJEMEL', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1140, 32102, 32, 'HAFIZ SONS CASH&CARRY', '', '', '', 'DINACHAKIAN LALAMUSA', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1141, 3850, 38, 'C.M.H', '', '', '', 'KHARIAN CANT', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1142, 550, 5, 'AL SHEIKH BAKERI', '', '', '', 'CHOWK PAKISTAN', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1143, 1147, 11, 'PHARMACY DR SABA NAZ', '', '', '', 'G.T.ROAD KHARIAN  CITY', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1144, 1049, 10, '.MERAN ASLAAN PHARMACY', '', '', '', 'QAZI IMAM SHAH ROAD', 'LALAMUSA', 'Retailer', '', '', '', '1443', '0', '0', '12/22/2017 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1145, 1156, 11, 'AMBALA BAKARS', '', '', '', 'DINGA ROAD KHARAIN', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1146, 1332, 13, 'DR.SHOAIB', '', '', '', 'REHMAT MEDICAL & H/C', 'KOTLA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1147, 160, 1, '.NADEEM MEDICAL STORE', '', '', '', 'NEAR CHOWK PAKISTAN', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1148, 1347, 13, 'PHARMACY,RAZA HOSPITAL', '', '', '', '', 'KOTLA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1149, 1154, 11, 'CARE MEDICAL STORE', '', '', '', 'PINDI SULTAN PUR', 'KHARIAN', 'Retailer', '', '', '', '0', '1313', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1150, 945, 9, 'MAHER CLINC', '', '', '', 'CHILDREN PARK', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1151, 829, 8, 'DR.WAZAHAT HUSSAIN WARRAICH', '', '', '', 'PMDC# 26569-P', 'HUSSAIN ORTHOCARE Hp GUJRAT', 'Doctor', '3420103405761', '1367185-5', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1152, 1302, 1, '.DR.SAYDA MEHMOOD', '', '', '', '', 'AL FAZAL Hp GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1153, 817, 8, 'DR.MOONA MIRZA', '', '', '', 'PMDC# 52905-P', 'MEDICARE HOSPITAL GUJRAT', 'Doctor', '38201-5557214-0', '3566464-9', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1154, 971, 9, 'DR.ABDUL SATTAR', '', '', '', 'PMDC# 28197-P', 'WAHIT TRUST HOSPITAL GUJRAT', 'Doctor', '', '', '', '28197', '0', '0', '12/31/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1155, 1517, 15, 'CH. MEDICAL STORE', '', '', '', '', 'SAMAHNI', 'Retailer', '', '', '', '5', '0', '0', '2/17/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1156, 1519, 15, 'SAARAM MEDICAL STORE', '', '', '', 'SAMAHANI', 'DISTRICT BHIMBER A.K.', 'Retailer', '', '', '', '0', '156', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1157, 1522, 15, 'VALLEY MEDICAL STORE', '', '', '', 'NEAR RHC', 'SAMAHNI', 'Retailer', '', '', '', '0', '0', '0', '11/14/2018 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1158, 1822, 18, '.DR.RIFAT ABBAS', '', '', '', '', 'GULYANA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1159, 1348, 13, 'PHARMACY,KASHMIR METERNTY HOME', '', '', '', '', 'KAKRALI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1160, 570, 5, 'DR.AZHAR CHAUDRY', '', '', '', 'SKIN CARE CENTER', 'BHIMBER ROAD GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1161, 956, 9, 'DR.ARIF NAZIR', '', '', '', 'PMDC# 9740-P', 'MEHER CLINIC GUJRAT', 'Doctor', '', '', '', '9740', '0', '0', '12/31/2018 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1162, 1813, 18, 'HMAZA PHARMACY', '', '', '', '', 'MALKA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1163, 1157, 11, 'DR.NAHED NASEEB', '', '', '', 'NASEEB SHAHEED HOSPITAL', 'KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1164, 2211, 22, 'MADINA MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'JOKALIAN', 'Retailer', '', '', '', '0', '38771', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1165, 2114, 21, 'DR.UZMA MUMTAZ', '', '', '', 'MUNGOWAL', 'PMDC NO.41147-S', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1166, 1213, 12, 'ANJUM MEDICAL STORE', '', '', '', 'MOIL CHOWK', 'KOTE JAMEL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1167, 1219, 12, '.UMER MEDICAL STORE', '', '', '', 'MOIL', 'TEHSIL BARNALA', 'Retailer', '', '', '', '208', '0', '0', '6/3/2017 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1168, 1220, 12, 'SHAHID MEDICAL STORE', '', '', '', '', 'KOTJAMEL', 'Retailer', '', '', '', '164', '0', '0', '6/15/2018 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1169, 348, 3, 'DR.ADREES KHAN', '', '', '', 'INAYAT BEGUM MEM. HOSPITAL', 'KARANWALA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1170, 3311, 3, 'PHARMACY,MIRZA KHAN MEM. HOSPITAL', 'FAWARA CHOWK', '', '', 'GUJRAT', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1171, 1173, 11, 'DR.IQBAL ALVY', '', '', '', '', 'MAQBOL NASEEM HOSPITAL KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1172, 1067, 10, 'DR.FAISAL NISAR', '', '', '', 'PMDC# 34518-P', 'NISAAR HOSPITAL LALAMUSA', 'Doctor', '', '', '', '34518', '0', '0', '12/31/2023 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1173, 347, 3, 'DR.Ejaz Ahmed', '', '0300-6224036', '', 'PMDC# 37389-P', 'Al-Fazal Hospital Chak Kmala', 'Doctor', '32203-6389221-5', '7212847', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1174, 349, 3, 'DR.AZMAT ULLAH', '', '0300-6217125', '', 'PMDC# 62688-P', 'ALAM HOSPITAL CHAK SHAMS', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1175, 350, 3, 'AKHTAR MEDICAL STORE', '', '0342-8071294', '', 'NEAR POST OFFICE', 'KARIANWALA', 'Retailer', '34201-1959638-9', '1620777-7', '', '0', '4079', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1176, 2115, 21, 'EIMAAN PHARMACY', '', '', '', 'MAIN BAZAR', 'KHAYAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1177, 827, 8, 'DR.USAMA MAKHDOOM', '', '', '', 'USAMA CLINIC', 'BHIMBER ROAD GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1178, 1462, 14, 'USMAN MEDICAL STORE', '', '', '', 'MAIN ROAD', 'KANGRA TEH&DISTT.BHIMBER', 'Retailer', '', '', '', '192', '0', '0', '11/14/2018 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1179, 1425, 14, 'MADINA MEDICAL STORE', '', '', '', '', 'MOIL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1180, 1230, 12, 'MADINA MEDICAL STORE', '', '', '', 'MOIL', 'TEHSIL BARNALA A.K.', 'Retailer', '', '', '', '227', '0', '0', '11/1/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1181, 1460, 14, '.SALEEM MEDICAL STORE', '', '', '', '', 'SOKSAN BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1182, 1187, 11, 'DR.QUDSIA BANO', '', '0346-6570273', '', 'PMDC# 36358-P', 'FATIMA M/C KHARIAN', 'Doctor', '3420105453934', '4029034-4', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1183, 1523, 15, 'ZAHID MEDICAL STORE', '', '', '', '', 'PONA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1184, 1349, 13, 'DR.MARYAM IKRAAM', '', '', '', 'MARYAM HOSPITAL', 'KOTLA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1185, 1350, 13, '.RAZA PHARMACY', '', '0346-0703330', '', 'MELAD CHOWK', 'CHOUR CHAK TEHSIL KHARIAN', 'Retailer', '', '', '', '2972', '0', '0', '8/30/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1186, 1351, 13, 'DR.SUMERA AZAM', '', '', '', 'REHMAN HOSPITAL', 'KOTLA ARAB ALI KHAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1187, 1430, 14, 'DR.SHUMAILA UMAIR', '', '', '', 'KASHMIR ORTHOPEDIC HOSPITAL', 'BHIMBER', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1188, 1175, 11, 'DR.PERVAIZ AKHTAR', '', '', '', 'AL-NAMET CLINIC', 'KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1189, 193, 1, '.MADINA MEDICAL STORE', '', '', '', '', 'VILLAGE CHAK SADA', 'Retailer', '', '', '', '0', '1163', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1190, 9193, 9, '.SHOAIB AMJAD', '', '', '', '', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1191, 555, 5, 'DR.GOHAR IQBAL', '', '', '', 'GOHAR IQBAL HOSPITAL', 'BHIMBER ROAD GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1192, 955, 9, 'DR.BUNYAD ALI IKAI', '', '', '', 'SHAMSHAD HOSPITAL', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1193, 1446, 14, 'MUHAMMAD HEALTH CARE HOSPITAL', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1194, 1313, 13, 'DR.TARIQ MUSTFA', '', '0312-6200904', '', '', 'HAMEDA TAJAMAL HOSPITAL KOTLA', 'Doctor', '34202-9272322-1', '7747435-1', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1195, 1232, 12, 'FARMAN MEDICAL STORE', '', '', '', '', 'BARNALA', 'Retailer', '', '', '', '199', '0', '0', '6/5/2018 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1196, 1352, 13, 'SHAHZAD MEDICAL STORE', '', '', '', 'NEAR ABID KARYANA STORE', 'VILLAGE SOHAL', 'Retailer', '', '', '', '0', '10243', '0', '12/31/2017 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1197, 560, 5, '.ZARYAB PHARMACY', '', '', '', 'NEAR ASHFAQ BOOK DEPO', 'BHIMBER ROAD GUJRAT', 'Retailer', '', '', '', '6405', '0', '0', '10/18/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1198, 2317, 23, 'MADNI MEDICAL STORE', '', '03144678808', '', 'HARIAWALA CHOWK,SHADIWAL ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '5692', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1199, 659, 6, 'HAMMAD MEDICAL STORE', '', '', '', 'SHADIWAL ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1200, 1069, 10, 'SAFDAR MEDICAL STORE', '', '', '', 'NEAR HASNAT MEDICAL STORE', 'MAIN BAZAR JAURA KARNANA', 'Retailer', '', '', '', '3099', '0', '0', '9/9/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1201, 1194, 11, 'DR.AFTAAB KHAN', '', '', '', 'POLY CLINIC', 'KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1202, 1463, 14, 'REHMAN MEDICAL STORE', '', '', '', 'LIAQAT ABAD', 'DISTT.BHIMBER', 'Retailer', '', '', '', '165', '0', '0', '12/31/2017 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1203, 1233, 12, 'AMJAD MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'BARNALA', 'Retailer', '', '', '', '266', '0', '0', '9/22/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1204, 1353, 13, 'DR.KASHIF MUSHTAQ', '', '', '', '', 'MUSHTAQ MEM. HOSPITAL KOTLA', 'Doctor', '', '7146098-2', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1205, 1070, 10, 'DR.GHULAM SARWAR', '', '', '', 'PMDC# 13253-P', 'JORRA', 'Doctor', '', '', '', '13253', '0', '0', '12/13/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1206, 1071, 10, 'DR.SAIQA SADDIQUE', '', '', '', 'PMDC# 40598-P', 'JORRA', 'Doctor', '', '', '', '40598', '0', '0', '12/31/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1207, 569, 5, 'DIAMOND PHARMACY', '', '0322-5991314', '', 'NEAR FAMILY HOSPIATL', 'BHIMBER ROAD  GUJRAT', 'Retailer', '34201-9635053-3', '', '', '31025', '0', '0', '3/30/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1208, 1461, 14, '.SHAOUR MEDICAL STORE', '', '', '', '', 'SOKASAN BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1209, 1452, 14, 'DR.DAIM IQBAL DAIM', '', '', '', 'NAZIR HOSPITAL', 'BHIMBER', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1210, 1354, 13, 'DR.MUBASHAR', '', '', '', '', 'MUBASHIR CLINIC FATAHPUR', 'Doctor', '34201-3256989-9', '6925517-8', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1211, 1234, 12, 'NISAR MEDICAL STORE', '', '', '', '', 'MOIL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1212, 1824, 18, 'DR.ZAFAR MEHDI', '', '', '', '', 'NASEERA PMDC#45292-P', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1213, 576, 5, '.ATIYA NAVEED CLINIC', '', '', '', '', 'MAKYANA', 'G', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1214, 814, 8, 'DR.MADEHA EJAZ', '', '', '', 'AL-KHIDMAT HOSPITAL GUJRAT', 'PMDC NO.60219-P', 'Doctor', '', '', '', '60219', '0', '0', '12/31/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1215, 662, 6, 'NOOR PHARMACY', '', '', '', 'OPP. SHABIR SHARIF HOSPITAL', 'KUNJA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1216, 815, 8, '.RAFIQ CLINIC', '', '03320401140', '', '', 'JASOKI', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1217, 577, 5, 'DR.MUBASHRA SHARIF', '', '', '', 'PMDC# 58975-P', 'Malik Family Hp GUJRAT', 'Doctor', '', '5006211-6', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1218, 1355, 13, 'AL SADIQ PHARMACY', '', '0300-6292450', '', 'OPP. ASKARI BANK BHIMBHER RD', 'KOTLA ARAB ALI KHAN', 'Retailer', '', '', '', '38923', '0', '0', '1/5/2023 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1219, 1453, 14, 'ATIF MEDICAL STORE', '', '', '', '', 'PINDI JHUNJA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1220, 1196, 11, 'DR.RUKHSANA JABEEN', '', '', '', '', 'AL-REHMAN CLINIC KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1221, 182, 1, 'DR.GULFAM CHEEMA', '', '', '', 'ROSHAN DIABETIC CENTER', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1222, 1829, 18, '.TAMOOR MEDICAL STORE', '', '', '', 'OPP. RHC', 'MALKA', 'Retailer', '3420244715957', '', '', '0', '1381', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1223, 1811, 18, 'HAMZA MEDICAL STORE', '', '', '', '', 'MALKA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1224, 1188, 11, 'BIN DAD PHARMACY', '', '0344-6199399', '', 'SHOUKAT PLAZA,DINGA ROAD', 'KHARIAN', 'Retailer', '', '4313534-0', '', '45624', '0', '0', '10/7/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1225, 1189, 11, 'DR.SHAHID', '', '', '', 'ARHAM MEDICAL &SURGICAL CENTER', 'MARALA KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1226, 11100, 11, '.DR.M.FAROOQ', '', '', '', '', 'MOHREE KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1227, 13102, 13, '.RAJA NAVEED', '', '', '', '', 'KOTLA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1228, 1357, 13, 'CITY MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'DANDI DARA', 'Retailer', '', '', '', '0', '32906', '0', '10/4/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1229, 1358, 13, 'MALIK MEDICAL STORE', '', '', '', 'OPP.B.H.UNIT HOSPITAL', 'DANDI DARA', 'Retailer', '', '', '', '0', '1265', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1230, 1359, 13, 'CHAUDHRY SAFFI MEDICAL STORE', '', '', '', '', 'DANDI DARA', 'Retailer', '', '', '', '800', '0', '0', '12/31/2017 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM');
INSERT INTO `dealer_info` (`dealer_table_id`, `dealer_id`, `dealer_area_id`, `dealer_name`, `dealer_contact_person`, `dealer_phone`, `dealer_fax`, `dealer_address`, `dealer_address1`, `dealer_type`, `dealer_cnic`, `dealer_ntn`, `dealer_image`, `dealer_lic9_num`, `dealer_lic9_exp`, `dealer_lic10_num`, `dealer_lic10_Exp`, `dealer_lic11_num`, `dealer_lic11_Exp`, `dealer_credit_limit`, `dealer_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`) VALUES
(1231, 1360, 13, 'ADIL MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'DANDI DARA', 'Retailer', '', '', '', '1561', '1561', '0', '5/4/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1232, 972, 9, 'DR.SAFDAR IQBAL', '', '', '', 'AL-GHAZALI CLINIC', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1233, 973, 9, 'DR.M.ZAHID AKRAM', '', '', '', 'PMDC NO.37858-S', 'WAHID TRUST Hospital GUJRAT', 'Doctor', '', '', '', '37858', '0', '0', '12/31/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1234, 1525, 15, 'PHARMACY,DR.MAQBOL', '', '', '', '', 'POONA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1235, 1526, 15, '.ZAREEF MEDICAL STORE', '', '', '', 'SANDO CROSS', 'SAMANI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1236, 1527, 15, 'KHUSHNOOD MEDICAL STORE', '', '', '', '', 'SAMAHANI', 'Retailer', '', '', '', '401', '0', '0', '4/15/2017 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1237, 820, 8, 'MADINA MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'JEHEURANWALI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1238, 1361, 13, '.AL-HABIB CLINIC', '', '', '', '', 'DANDI DARA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1239, 1528, 15, '.ATTIQUE MEDICAL STORE', '', '', '', '', 'MALOOT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1240, 1072, 10, '.DR.M.BILAL', '', '', '', 'SARDARPURA WELFARE HOSPITAL', 'LALAMUSA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1241, 1529, 15, '.AL MADINA MEDICAL STORE', '', '', '', '', 'POONA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1242, 578, 5, 'DR.FARYAD HUSSAIN', '', '', '', '', 'FARYAD CLINIC GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1243, 1109, 1, 'DR.JAWAD CH.', '', '', '', 'GUJRAT HAIR TRANCE PLANT', 'REHMAN SHAHED ROAD GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1244, 974, 9, 'DR.SHAMIM AKHTAR', '', '', '', 'KANIZ AKHTAR HOSPITA', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1245, 822, 8, 'DR.SHAZIA TABASAM', '', '', '', 'JALALPUR JATAN ROAD', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1246, 1362, 13, 'DR.SUHAIL AHMED', '', '', '', 'MEDICARE Hp DOULTNAGAR', 'PMDC NO.14418-N', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1247, 1530, 15, '.GILANI MEDICAL STORE', '', '', '', '', 'CHOKI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1248, 1454, 14, '.AL-SHAFI CLINIC', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1249, 1363, 13, 'HAMZA MEDICAL STORE', '', '', '', 'MANDIR', 'KOTLA ARAB ALI KHAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1250, 1455, 14, '.ALI KHAN HOSPITAL', '', '', '', '', 'BHIMBER', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1251, 1364, 13, '.BOLANI MEDICAL STORE', '', '', '', '', 'BOLANI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1252, 11101, 11, 'DR.COL.ZULFIQAR', '', '', '', 'ZULFIQAR NEURO HOSPITAL', 'KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1253, 1456, 14, '.UNITED MEDICAL STORE', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1254, 1365, 13, '.IMRAN MEDICAL HALL', '', '', '', '', 'BHRING', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1255, 975, 9, 'DR.FARHAT-ULLAH', '', '', '', 'NASR U LLAH CLINIC GUJRAT', 'PMDC NO.54652-P', 'Doctor', '', '', '', '54652', '0', '0', '12/31/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1256, 1366, 13, 'IMTIAZ MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'DANDI DARA', 'Retailer', '', '', '', '1066', '0', '0', '9/22/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1257, 5164, 5, '.MARYAM CLINIC', '', '', '', '', 'MACHIWAL', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1258, 1106, 1, 'HUSSNAIN MEDICAL STORE', '', '0313-7805338', '', 'SOBAY DAR PLAZA,REHMAN SHAHEED', 'ROAD GUJRAT', 'Retailer', '3420105640431', '5505447-3', '', '0', '63081', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1259, 1823, 18, '.DR.UMER FAROOQ', '', '', '', 'FATIMA MEDICAL HOSPITAL', 'LEHREE MALKA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1260, 1107, 1, 'PHARMACY,DR.NADIA', '', '', '', 'SHADULA ROAD', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1261, 1457, 14, 'NEW VALLY MEDICAL STORE', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '284', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1262, 976, 9, 'ARIF PHARMACY', '', '', '', 'KALUPURA ROAD', 'GUJRAT', 'Retailer', '', '', '', '1508', '0', '0', '9/8/2018 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1263, 1108, 1, '.SHIFA PHARMACY', '', '', '', 'MOH.ISLAM NAGAR', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1264, 1367, 13, '.SKIN CARE CLINIC', '', '', '', 'DR.AMREIN MUSTFA', 'KOTLA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1265, 564, 5, 'DR.RIAZ MUGHAL', '', '', '', 'BHIMBER ROAD', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1266, 580, 5, '.DR.M.ILYAS', '', '', '', 'MALHO KHOKHAR', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1267, 1080, 10, 'DR.RIAZ ASLAM', '', '', '', 'PMDC# 28983-P', 'HAJI IMAAM DIN CLINIC LALAMUSA', 'Doctor', '34202-4064068-9', '1634138-4', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1268, 839, 8, '.SAJDA CLINIC', '', '', '', '', 'MADINA ROAD GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1269, 2120, 21, 'RAZA MEDICAL STORE', '', '', '', 'NEAR POLICE CHOWKI', 'LARI ADDA MONGOWAL GHARBI', 'Retailer', '', '', '', '0', '10780', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1270, 1816, 18, '.DR.M.USMAN', '', '', '', '', 'CHANAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1271, 1368, 13, '.WAJID DENTEL CLINIC', '', '', '', 'DANDI DARA', '', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1272, 1450, 14, 'DR.IMRAN SIDDIQUE', '', '', '', 'ALI CHILDREN Hp BHIMBER', 'PMDC NO.1632-AJK', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1273, 1073, 10, '.ALI MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'DEWNA MANDI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1274, 180, 1, 'DR.IRFAN AKBAR NAZ', '', '', '', 'PMDC# 27820-P', 'NASEEM AKBAR HOSPITAL GUJRAT', 'Doctor', '34201-1653969-7', '3551498-1', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1275, 1516, 15, 'DR.MADASAR MUNEER', '', '', '', 'JANDALA SAMANI', 'PMDC NO.4522-AJK', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1276, 368, 3, 'DR.M.MOAZAM', '', '0331-6283878', '', 'MOHSIN CLINIC KARIANWALA', 'PMDC# 78863-P', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1277, 1177, 11, 'DR.HUMAYON', '', '', '', 'IQBAL MEMORIAL HOSPITAL', 'MANDHER KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1278, 369, 3, 'HEERA MEDICAL STORE', '', '', '', 'POLICE STATION ROAD,OPP TANDA', 'PARK.TANDA', 'Retailer', '3420194930283', '5809199-5', '', '0', '6997', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1279, 977, 9, '.SHAHBAZ MEDICAL STORE', '', '', '', 'MR.ARSHAD', 'MIANA CHOWK VILLAGE LAGAY', 'Retailer', '', '', '', '15749', '0', '15749', '11/29/2019 12:00 AM', '12/30/1899 12:00 AM', '11/29/2019 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1280, 370, 3, '.DR.FIZZA MUDASSER', '', '0306-6454989', '', '', 'KASAB', 'Doctor', '34201-0319154-7', '5234849-8', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1281, 1395, 13, 'HAMZA MEDICAL STORE', '', '', '', 'NEAR BHU', 'MANDAHAR', 'Retailer', '', '', '', '25322', '0', '0', '12/9/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1282, 2310, 23, '.NATASHA CLINIC', '', '', '', '', 'KHOJANWALI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1283, 2311, 23, 'DR.RIZWAN MAQSOOD', '', '', '', 'MIRZA MEHBOB CLINIC TRIKHA', 'PMDC NO.36466-P', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1284, 1075, 10, 'LUCKY MEDICAL STORE', '', '', '', '', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1285, 1426, 14, '.AKBAR KHANAM HOSPITAL', '', '', '', '', 'BHIMBER', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1286, 1531, 15, 'MUBEEN MEDICAL STORE', '', '', '', '', 'JANDALA SAMAHNI', 'Retailer', '', '', '', '0', '0', '0', '1/1/2018 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1287, 1427, 14, '.MARYAM DENTAL CLINIC', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1288, 1235, 12, 'DR.IMRAN SADDIQUE', '', '', '', 'YOUSAF HOSPITAL', 'KADHALA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1289, 1820, 18, 'PHARMACY,DR.AKHTAR HUSSAIN MIRZA', '', '', '', 'CHORIAN PINDI HASHAM', 'MALKA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1290, 585, 5, '.DR.ZULFIQAR', '', '', '', '', 'MALHO KHOKHAR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1291, 1081, 10, 'HAFIZ PHARMACY', '', '', '', 'NEAR TRUMA CENTER', 'GT ROAD LALAMUSA', 'Retailer', '', '', '', '1544', '0', '0', '3/20/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1292, 2313, 23, '.IMRAN CLINIC', '', '', '', '', 'TRIKHA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1293, 985, 9, 'DR.SALEEM AHMED', '', '', '', 'PMDC NO.45243-P', 'SARGODHA ROAD GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1294, 1825, 18, '.RAZIA METENTY HOME', '', '', '', '', 'MALKA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1295, 1236, 12, 'ZAHID MEDICAL STORE', '', '', '', '', 'KADHALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1296, 1237, 12, 'DR.M.ABDULLAH', '', '', '', 'FAQEER CLINIC', 'KADHALA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1297, 1370, 13, 'DR.AKIF SOHAIL', '', '', '', 'AL-SHIFA HOSPITAL', 'KOTLA ARAB ALI KHAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1298, 1831, 18, '.ADEEL CLINIC', '', '', '', '', 'THOTHA RAYE BAHADER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1299, 1835, 18, '.MUDASER CLINIC', '', '', '', '', 'THOTHA RAYE BAHADER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1300, 2314, 23, '.IRAM CLINIC', '', '', '', '', 'KIRANWALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1301, 1836, 18, '.UMER HOSPITAL', '', '', '', 'DR.UMER', 'GULYANA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1302, 1238, 12, '.SULMAN MEDICAL STORE', '', '', '', '', 'KOTE JAMEL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1303, 1837, 18, '.MUKHTAAR CLINIC', '', '', '', '', 'SANTAL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1304, 586, 5, 'TAIMOOR MEDICAL STORE', 'DR.AKBAR ALI SB', '', '', 'OPP.BABA DERA', 'VILLAGE MALOKHOKHER', 'Retailer', '', '', '', '0', '30702', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1305, 1239, 12, '.ABDULLAH MEDICAL STORE', '', '', '', '', 'KADHALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1306, 1240, 12, '.WAQAS MEDICAL STORE', '', '', '', '', 'MAGHLURA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1307, 1241, 12, '.BILAL MEDICAL STORE', '', '', '', '', 'MAGLURA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1308, 1838, 18, '.BADHAR CLINIC', '', '', '', '', 'BADHAR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1309, 1839, 18, '.RASHEED CLINIC', '', '', '', '', 'SADKLAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1310, 1840, 18, '.ISHTIAQ CLINIC', '', '', '', '', 'JANTLA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1311, 1429, 14, 'DR.ATT-UR-REHMAN', '', '', '', 'ABDULLAH HEART CARE', 'BHIMBER', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1312, 823, 8, 'DR.SOBIA AZHAR', '', '', '', 'MARGHZAR CALONI', 'GUJRAT', 'Doctor', '34201-5982752-0', '7593509-2', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1313, 1532, 15, 'ZAHID MEDICAL STORE', '', '', '', '', 'POONA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1314, 1533, 15, 'ALI MEDICAL HALL', '', '', '', '', 'POONA', 'Retailer', '', '', '', '0', '0', '0', '8/18/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1315, 1534, 15, 'AL-SHAFA MEDICAL STORE', '', '', '', '', 'POONA', 'Retailer', '', '', '', '254', '0', '0', '5/1/2018 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1316, 588, 5, 'DR.SHAGUFTA SHAFIQ', '', '', '', 'BHIMBER ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1317, 589, 5, 'AKRAM PHARMACY', '', '', '', 'QADIR COLONY', 'BHIMBER ROAD GUJRAT', 'Retailer', '', '', '', '1220', '0', '0', '2/8/2017 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1318, 1535, 15, 'RAHEEM MEDICAL STORE', '', '', '', '', 'POONA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1319, 1841, 18, 'DR.AKHTAR HUSSAIN MIRZA', '', '', '', 'YASHFEEN CLINIC SHORRIN', 'PMDC NO.45080-P', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1320, 1369, 13, 'DR.FRAZ', '', '', '', '', 'KOTLA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1321, 1375, 13, 'DR.MAJ.RIZWAN HAIDER', '', '', '', '', 'DOLAT NAGAR', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1322, 1842, 18, '.JABAAR CLINIC', '', '', '', '', 'DOOGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1323, 1843, 18, '.AL-SHIFA CLINIC', '', '', '', '', 'MALKA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1324, 1844, 18, '.NAZIR ORTHO CLINIC', '', '', '', '', 'BADHAR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1325, 1536, 15, '.BILAL NAEEM MEDICAL STORE', '', '', '', '', 'POONA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1326, 1845, 18, 'TAJ ALAM MEDICAL STORE', '', '', '', 'NEAR GOVT.MATERNITY HOSPITAL', 'KOTLI BAJAR TEHSIL KHARIAN', 'Retailer', '23202-0716095-5', '', '', '902', '15032', '0', '12/31/2017 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1327, 1846, 18, 'RAZIYA CLINIC', '', '', '', '', 'MALKA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1328, 1082, 10, '.RIMSHA CLINIC', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1329, 1083, 10, '.KASHIF MEDICAL STORE', '', '0333-8517104', '', 'NEAR TRAUMA CENTER', 'LALAMUSA', 'Retailer', '', '', '', '811', '0', '0', '12/31/2017 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1330, 1245, 12, 'DR.M.ASHRAF', '', '', '', 'SHIFA Hp BARNALA', 'PMDC NO.248-AJK', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1331, 1465, 14, '.SUBHANI MEDICAL STORE', '', '', '', '', 'SUKASON BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1332, 1847, 18, 'SHAKEEL MEDICAL STORE', '', '', '', '', 'CHOODO', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1333, 1371, 13, 'AYUB MEDICAL STORE', '', '', '', 'NEAR PAPPO SHOE SHOP', 'MAIN BAZAR FATEH PUR', 'Retailer', '34201-0521451-5', '', '', '2824', '0', '2824', '8/23/2019 12:00 AM', '12/30/1899 12:00 AM', '8/23/2019 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1334, 1464, 14, '.NAVEED JAFAR MEDICAL STORE', '', '', '', '', 'D.H.Q. BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1335, 1372, 13, 'RAZA PHARMACY', '', '', '', '', 'FATHEPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1336, 372, 3, '.ZAIN CLINIC', '', '', '', 'DR.TANZEEM AKHTAR', 'JALALPUR SOBTIAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1337, 201, 20, '.AHMAD MEDICAL STORE', '', '', '', 'OPP.LARI ADDA', 'KUNJA', 'Retailer', '', '', '', '0', '1555', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1338, 1466, 14, 'BILAL PHARMACY', '', '', '', 'OPP.DHQ HOSPITAL', 'BHIMBER (AK)', 'Retailer', '', '', '', '244', '0', '0', '6/20/2018 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1339, 1467, 14, '.MUHAMMAD HEALTH CARE', '', '', '', 'DR.NAEEM ASLAM', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1340, 1468, 14, '.HUSNA CHILD CLINIC', '', '', '', 'DR.SAMARA MUBEEN', 'PINDI JHUNJA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1341, 1848, 18, '.SOLULAT CLINIC', '', '', '', '', 'MALKA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1342, 1174, 11, 'ADNAN PHARMACY', '', '', '', 'KHARIAN ROAD', 'DOULAT NAGAR', 'Retailer', '', '', '', '1331', '0', '0', '7/14/2018 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1343, 1373, 13, 'ADNAN PHARMACY', '', '0335-4421684', '', 'KHARIAN ROAD', 'DOULAT NAGAR', 'Retailer', '34202-7991869-1', '', '', '32696', '0', '0', '5/19/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1344, 1469, 14, '.LASANI MEDICAL STORE', '', '', '', '', 'SUKASON BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1345, 1470, 14, 'TAYYAB MEDICAL STORE', '', '', '', '', 'SOKASAN BHIMBR', 'Retailer', '', '', '', '239', '0', '0', '11/1/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1346, 1471, 14, '.AL SHIFA MEDICAL STORE', '', '', '', '', 'SUKASON BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1347, 1472, 14, '.NAVEED MEDICAL STORE', '', '', '', '', 'SUKASON BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1348, 1473, 14, '.ABID MEDICAL STORE', '', '', '', '', 'SUKASON BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1349, 534, 5, 'DR.ZOHAIB AHMED', '', '', '', 'BHIMBER ROAD', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1350, 1374, 13, 'NEW RAZA PHARMACY', '', '', '', 'JALALPUR JATTAN ROAD CHOWK', 'FATEH PUR', 'Retailer', '', '', '', '1499', '0', '0', '7/1/2018 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1351, 2315, 23, '.DR.SADEEQ-E-AKBAR', '', '', '', '', 'GOLAKI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1352, 1849, 18, '.AHSAAN CLINIC', '', '', '', '', 'CHOKAAR KHURD', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1353, 373, 3, '.DR.SHAISTA', '', '', '', '', 'BHU BHARAJ', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1354, 1376, 13, 'Saleem Sons Pharmacy', '', '0306-6207263', '', 'Langrial Chowk', 'KOTLA', 'Retailer', '', '5782459-4', '', '7012', '0', '0', '9/18/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1355, 1246, 12, 'MOAZZAM MEDICAL STORE', '', '', '', 'THUB ROAD', 'BARNALA', 'Retailer', '', '', '', '261', '0', '0', '3/20/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1356, 1538, 15, '.DR.HAIDER AZAM', '', '', '', '', 'RHC POONA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1357, 1377, 13, '.ANWAR CLINIC', '', '', '', '', 'KOTLA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1358, 1476, 14, 'BILAL MEDICAL STORE', '', 'BHIMBHER', '', '', 'D.H.Q', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1359, 1539, 15, 'ASLAM MEDICAL STORE', '', '', '', 'KADYALA ROAD', 'CHOWKI', 'Retailer', '', '', '', '224', '0', '0', '11/1/2017 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1360, 2316, 23, '.SUMERA CLINIC', '', '', '', '', 'GOLAKI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1361, 1540, 15, 'ARIF MEDICAL STORE', '', '', '', 'BUS STAND', 'SAMANI', 'Retailer', '', '', '', '149', '0', '0', '4/29/2018 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1362, 1084, 10, 'FAHEEM-UL-ISLAM SHAHEED PHARMACY', '', '', '', 'MALL ROAD', 'LALAMUSA', 'Retailer', '', '', '', '1574', '0', '0', '6/5/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1363, 1085, 10, 'JADDAH MEDICAL STORE', '', '', '', 'OPP.YASIN SWEETS', 'MAIN BAZAR LALAMUSA', 'Retailer', '', '', '', '0', '1476', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1364, 1086, 10, 'DR.M.MUNIR', '', '', '', 'PMDC# 20857-P', 'DIABITIES CLINIC LALAMUSA', 'Doctor', '', '', '', '20857', '0', '0', '12/31/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1365, 374, 3, 'DR.SALMAN SHABBIR', '', '', '', 'PMDC# 80669-P', 'NAZIR CLINIC TANDA', 'Doctor', '34201-7425639-3', '5082217-7', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1366, 375, 3, '.DR.SHABANA', '', '', '', '', 'BHU HANJRA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1367, 376, 3, '.DR.KIRAN', '', '', '', '', 'PEROSHAH', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1368, 1478, 14, 'DR.SAMARA MUBEEN', '', '', '', 'HUSNA CLINIC', 'PINDI JHONJA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1369, 1087, 10, 'DR.SHAHZAD', '', '', '', 'AL-SHIFA MEDICAL HALL', 'DEWNA MANDI GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1370, 1381, 13, '.ANWAR CLINIC', '', '', '', '', 'BUZGWAL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1371, 1541, 15, 'ASAD MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'CHOKI TEHSIL SAMAHNI', 'Retailer', '', '', '', '37', '0', '0', '1/31/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1372, 978, 9, 'DR.ABDUL-GHAFOOR KHOKHAR', '', '', '', 'KHOKHAR CLINIC', 'RAILWAY ROAD GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1373, 1481, 14, 'HUSSAIN MEDICAL STORE', '', '', '', '', 'PINDI JJUNJA', 'Retailer', '', '', '', '265', '0', '0', '3/20/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1374, 1542, 15, '.JANDALA MEDICAL STORE', '', '', '', '', 'JANDALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1375, 1543, 15, '.WAHEED MEDICAL STORE', '', '', '', '', 'CHOKI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1376, 1544, 15, 'AL SYED MEDICOSE', '', '', '', '', 'CHOWKI SAMAHNI', 'Retailer', '', '', '', '16', '0', '0', '11/2/2018 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1377, 1545, 15, '.SIRAJ MEDICAL STORE', '', '', '', '', 'JANDALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1378, 1546, 15, 'ALI MEDICAL STORE', '', '', '', '', 'SAMANI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1379, 834, 8, 'SADAT MEDICAL STORE', '', '', '', '', 'MADINA DIS. GUJRAT', 'Retailer', '342010449737', '5571276-6', '', '455', '0', '0', '12/31/2017 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1380, 377, 3, 'DR.MIRZA M.UMAR', '', '', '', 'lIFE LINE CLINIC CHAK KAMALA', 'PMDC NO.75521-P', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1381, 1850, 18, '.ISLAMI SHIFAKHANA', '', '', '', '', 'BHAROAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1382, 2332, 23, '.NAZIA CLINIC', '', '', '', '', 'KHOJA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1383, 2333, 23, '.SAJID CLINIC', '', '', '', '', 'KHOJA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1384, 841, 8, '.ZAINAB CLINIC', '', '', '', '', 'B.H.U. CHACHIYAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1385, 574, 5, 'DR.SULMAN BASHIR', '', '', '', 'BHIMBER ROAD', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1386, 1163, 11, '.HUMAYON CLINIC', '', '', '', '', 'SITARPURRA KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1387, 2125, 21, '.DR.AKIF SUHAIL', '', '', '', '', 'MONGOWAL', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1388, 838, 8, '.QURATULAIN CLINIC', '', '', '', '', 'JAMALPUR GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1389, 2133, 21, '.UROOJ CLINIC', '', '', '', '', 'KHERANWALI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1390, 378, 3, '.NEW USMAN MEDICAL CENTER', '', '', '', 'TANDA ROAD', 'PEJOKI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1391, 1433, 14, 'DUA MEDICAL STORE', '', '', '', '', 'DANDAR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1392, 1474, 14, 'DUA MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'DANDAR TEH&DISST.BHIMBER', 'Retailer', '', '', '', '275', '0', '0', '11/4/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1393, 1547, 15, '.MODEL HOSPITAL', '', '', '', 'BANDALA', 'CHOKI', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1394, 1088, 10, 'EXPRESS PHARMACY', '', '053-7513399', '', 'NEAR NISAR HOSPITAL', 'G.T ROAD LALAMUSA', 'Retailer', '34202-1797337-1', '7610027-5', '', '2248', '0', '0', '8/21/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1395, 1475, 14, '.ADNAN MEDICAL STORE', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1396, 1477, 14, '.DR.FARHAT AMIN JARRAL', '', '', '', '', 'BHIMBER', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1397, 1851, 18, '.ZAIN CLINIC', '', '', '', '', 'NUNAWALI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1398, 590, 5, '.SHAKILA CLINIC', '', '', '', 'DR.SHAKILA (MIDWIFE)', 'ADOWAL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1399, 1247, 12, 'DR.JAHANZAIB HAMID', '', '', '', 'JAHANZAIB CLINIC MOIL A.K', 'PMDC NO.77699-P', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1400, 840, 8, '.SHIFA CLINIC', '', '', '', 'DR.USMAN', 'MADINA ROAD GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1401, 592, 5, '.SALEEM ABAAS CLINIC', '', '', '', '', 'BHIMBER ROAD GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1402, 1383, 13, '.SADDI HOSPTAL', '', '', '', 'DR.FIAZ', 'SABOOR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1403, 1384, 13, 'DR.SAMRA BIBI', '', '', '', 'PMDC# 63445-P', 'RAZIA BEGHUM HOSPITAL KOTLA', 'Doctor', '34202-7607532-0', '6049719-0', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1404, 1190, 11, 'DR.ARSHIYA ASLAM', '', '', '', '', 'AHMED HOSPITAL JANDALA KHARIAN', 'Doctor', '38403-1256475-6', '5276097-0', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1405, 1479, 14, 'JANJUA MEDICAL STORE', '', '', '', 'SAMAHNI CHOWK', 'BHIMBER AZAD KASHMIR', 'Retailer', '', '', '', '240', '0', '0', '5/13/2018 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1406, 1548, 15, 'ABDULLAH MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'POONA TEHSIL SAMAHNI', 'Retailer', '', '', '', '279', '0', '0', '12/30/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1407, 1385, 13, 'MALIK MEDICAL STORE', '', '', '', 'NEAR ILYAS KARYANA STORE', 'VILLAGE KHARANA', 'Retailer', '', '', '', '0', '3188', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1408, 1248, 12, 'DR.UMAR SALAM MIRZA', '', '', '', 'ISLAM MEDICAL COMPLEX', 'BARNALA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1409, 1386, 13, '.AYYAN CLINIC', '', '', '', '', 'KOTLA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1410, 593, 5, 'DR.M.ASLAM', '', '', '', 'PMDC# 25574-P', 'BHIMBER ROAD GUJRAT', 'Doctor', '', '8264658-3', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1411, 1549, 15, '.AZEEM U LLAH MEDICAL STORE', '', '', '', '', 'BANDALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1412, 1480, 14, 'DR.AZHAR IQBAL', '', '', '', 'SCHOOL ROAD BHIMBER', 'PMDC NO.2302-AJK', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1413, 594, 5, '.DR.ISHAFIQ AHMAD', '', '', '', '', 'MULL KHAKAR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1414, 1853, 18, '.KAZMI MEDICAL STORE', '', '', '', '', 'SEHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1415, 1854, 18, 'DR.FAIZAN MUHAMMAD', '', '', '', 'PMDC# 105447-P', 'FAIZAN CLINIC CHANNAN', 'Doctor', '34202-5781670-9', '5285366-8', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1416, 1482, 14, '.QASIM MEDICAL STORE', '', '', '', '', 'PINDI JHUNJA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1417, 835, 8, 'DR.MASOOD AHMAD AKHTAR', '', '', '', 'PMDC NO.6082-P', 'LORRAN GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1418, 1855, 18, '.REHMAN CLINIC', '', '', '', '', 'NASEERA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1419, 1249, 12, '.CHAINAR WELFAIR SOCITY', '', '', '', '', 'KOTEJAMEL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1420, 2129, 21, '.KIRAN CLINIC', '', '', '', '', 'MONGOWAL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1421, 1550, 15, '.MAJID MEDICAL STORE', '', '', '', '', 'KADYALA SAMANI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1422, 990, 9, 'AL-MADINA PHARMACY', '', '', '', 'NEAR DAR MILL GALLA', 'SARGODHA ROAD GUJRAT', 'Retailer', '34201-3785625-5', '5035343-5', '', '41390', '0', '0', '3/27/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1423, 991, 9, '.DR.AYESHA FAISAL', '', '', '', 'FAISAL HOSPITAL', 'SARGODHA ROAD GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1424, 2130, 21, '.NASREN CLINIC', '', '', '', '', 'MONGOWAL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1425, 379, 3, 'DR.SARFRAZ AHMED', '', '0349-7442901', '', 'PMDC# 81682-P', 'RAZAQ CLINIC BHAGOWAL KALAN', 'Doctor', '34201-8743350-9', '8198390-2', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1426, 380, 3, 'DR.SIRSHAR AHMED', '', '', '', 'HABIB HOSPITAL', 'KARIANWALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1427, 1251, 12, 'AMIR MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'BARHING', 'Retailer', '', '', '', '273', '0', '0', '11/4/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1428, 1485, 14, 'NEW VALLY MEDICAL STORE', '', '', '', 'MIRPUR CHOWK', 'BHIMBER CITY', 'Retailer', '', '', '', '284', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1429, 1856, 18, 'HASSAN PHARMACY', '', '', '', 'OPP.MASJID MUHAMMAD', 'DINGA ROAD CHANNAN', 'Retailer', '', '', '', '10516', '0', '0', '9/28/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1430, 1551, 15, '.AL-KHIDMAT MEDICAL STORE', '', '', '', '', 'DANNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1431, 992, 9, 'DR.ALI SILMAN', '', '', '', 'PMDC NO.41188-P', 'BILAL M/C RAILWAY ROAD GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1432, 1387, 13, 'MIAN JEE MEDICAL STORE', '', '', '', 'BHIMBER ROAD,OPP.RHC', 'DAULAT NAGAR', 'Retailer', '', '', '', '359', '0', '0', '8/24/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1433, 1486, 14, 'ABDULLAH MEDICAL STORE', '', '', '', 'SHER JANG COLONY', 'BHIMBER', 'Retailer', '', '', '', '203', '0', '0', '7/17/2018 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1434, 1388, 13, 'NOOR MEDICAL STORE', '', '0303-6045827', '', 'NEAR RUPAIRY STOP', 'JALALPUR SOBTIAN ROAD', 'Retailer', '', '', '', '0', '26386', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1435, 2334, 23, '.DR.AFZAL', '', '', '', '', 'KHOJA MONGOWAL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1436, 596, 5, 'DR.M.BILAL', '', '', '', '', 'INAYAT BASHIR Hp MACHIWAL', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM');
INSERT INTO `dealer_info` (`dealer_table_id`, `dealer_id`, `dealer_area_id`, `dealer_name`, `dealer_contact_person`, `dealer_phone`, `dealer_fax`, `dealer_address`, `dealer_address1`, `dealer_type`, `dealer_cnic`, `dealer_ntn`, `dealer_image`, `dealer_lic9_num`, `dealer_lic9_exp`, `dealer_lic10_num`, `dealer_lic10_Exp`, `dealer_lic11_num`, `dealer_lic11_Exp`, `dealer_credit_limit`, `dealer_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`) VALUES
(1437, 1195, 11, 'DR.HAFSA ARIF', '', '', '', 'CHARANWALA FREE DISPENSERY', 'PMDC NO.74022-P', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1438, 1252, 12, '.MALIQA MEDICAL STORE', '', '', '', '', 'KOTE JAMEL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1439, 1389, 13, '.HUSSAIN MEDICAL STORE', '', '', '', '', 'DANDI DARA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1440, 2318, 23, '.DR.SHANZA AFZAL', '', '', '', '', 'DHAROWAL', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1441, 1552, 15, '.ZAMAN MEDICAL STORE', '', '', '', '', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1442, 1487, 14, 'BUKHARI MEDICAL STORE', '', '', '', 'OPP.DHQ HOSPITAL', 'BHIMBER', 'Retailer', '', '', '', '204', '0', '0', '4/18/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1443, 381, 3, '.REHMAT HOSPITAL', '', '', '', '', 'RANGRA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1444, 1390, 13, 'DR.RAZWAN ASHRAF', '', '', '0301 4923178', 'PMDC# 70604-P', 'HELATH CARE CLINIC KOTLA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1445, 1815, 18, '.ALI ZAIN MEDICAL STORE', '', '', '', 'NEAR RHC', 'MALKA KHARIAN', 'Retailer', '34202-9346909-0', '', '', '5289', '0', '0', '9/16/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1446, 1391, 13, 'ADIL PHARMACY', '', '', '', 'BULANI BRANCH NEAR BHU', 'BULANI', 'Retailer', '', '', '', '24714', '0', '0', '12/6/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1447, 2210, 22, '.SHAZIA SALEEM CLINIC', '', '', '', '', 'NAWALOK', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1448, 2126, 21, '.DR.SAIRA', '', '', '', 'SAIRA CLINIC', 'MONGOWAL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1449, 837, 8, '.DR.SHAHID MEHMOOD', '', '', '', '', 'MADINA ROAD GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1450, 1090, 10, 'DR.SOHAIB UL HASSAN', '', '', '', 'PMDC# 50939-P', 'CAP.IJAZ CLINIC LALAMUSA', 'Doctor', '', '', '', '50939', '0', '0', '12/31/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1451, 1197, 11, 'DR.JAMMSHED DILAWAR', '', '', '', 'PMDC# 57876-P', 'DILAWAR Hp Dinga Road KHARIAN', 'Doctor', '35201-9938348-1', '7343632-1', '', '57876', '0', '0', '12/31/2024 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1452, 1198, 11, 'DR.SYED MUQADDAS ALI SHAH', '', '', '', 'PMDC# 17415', 'ALI CHILDREN CLINIC KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1453, 383, 3, 'DR.SOBIA AZHAR', '', '', '', '', 'TANDA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1454, 2319, 23, 'RASOOL MEDICAL STORE', '', '', '', 'HARIANWALA CHOWK', 'GUJRAT', 'Retailer', '34201-5679519-3', '5697896-5', '', '0', '31277', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1455, 384, 3, 'DR.NUSRA ULFAT', '', '', '', 'AL-SHIFA CLINIC KARIANWALA', 'PMDC NO.84960-P', 'Retailer', '', '', '', '84960', '0', '0', '12/31/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1456, 1199, 11, 'DR.SALMAN AHMAD', '', '', '', 'PMDC# 46690-P', 'SALMANS SKIN CLINIC KHARIAN', 'Doctor', '', '', '', '46690', '0', '0', '12/31/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1457, 2320, 23, '.YASMIN CLINIC', '', '', '', '', 'SHADIWAL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1458, 2321, 23, 'SHABBIR & SONS MEDICAL STORE', '', '', '', 'NEAR SHADIWAL CHOWKI', 'MAIN BAZAR SHADIWAL', 'Retailer', '', '', '', '0', '32435', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1459, 385, 3, 'DR.Ibrar Ahmed', '', '', '', 'PMDC# 87517-P', 'KOTLI BHAGWAN', 'Retailer', '', '', '', '87517', '0', '0', '12/31/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1460, 1915, 19, 'WAQAR PHARMACY', '', '0345-4303907', '', 'NEAR CMH GATE', 'KHARIAN CANTT', 'Retailer', '1310109806821', '2389800-3', '', '32428', '0', '0', '4/26/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1461, 1091, 10, 'ARSHAD MEDICAL STORE', '', '', '', '', 'LALAMUSA', 'Retailer', '34601-0776477-7', '2264120-3', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1462, 386, 3, '.DR.ABID', '', '', '', '', 'LAKHANWAL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1463, 1393, 13, 'USMAN MEDICAL STORE', '', '0306-3387175', '', 'OPP.UBL BANK CHOWK FATEHPUR', 'GUJRAT', 'Retailer', '35202-1135215-3', '', '', '0', '41934', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1464, 1553, 15, '.DR.WAHEED', '', '', '', '', 'BINDI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1465, 1092, 10, 'DR.FAHAD USMAN', '', '', '', 'PMDC# 56889-P', 'USMAN HOSPITAL LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1466, 1488, 14, 'DR.NASREEN AKHTER', '', '', '', 'CHILDREN Hp BHIMBER', 'PMDC NO.147-AJK', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1467, 1093, 10, 'ZAFAR PHARMACY', '', '', '', 'BEHIND POLICE STATION SADAR', 'MOH.KARIM PURA LALAMUSA', 'Retailer', '34202-2721578-9', '5563668-3', '', '32791', '0', '0', '5/19/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1468, 2213, 22, 'SUFYAN MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'PINDI KALU', 'Retailer', '', '', '', '0', '15206', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1469, 993, 9, 'BASIT MEDICAL STORE', '', '', '', 'MOHALLAH REHMAN PURA', 'LUNDPUR ROAD GUJRAT', 'Retailer', '', '', '', '0', '32725', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1470, 1857, 18, 'ATIF MEDICAL STORE', '', '', '', 'NEAR CH.FAZAL DAD Hp', 'KOTLI BAJAR', 'Retailer', '', '', '', '0', '31126', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1471, 994, 9, 'AKHTAR MEDICAL STORE', '', '', '', 'MOHALLAH QUTAB ABAD', 'SARGODHA ROAD GUJRAT', 'Retailer', '', '', '', '0', '32723', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1472, 1554, 15, 'YASIR MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'CHOWKI', 'Retailer', '', '', '', '294', '0', '0', '4/14/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1473, 1555, 15, 'FAHAD MEDICAL STORE', '', '', '', '', 'CHOWKI', 'Retailer', '', '', '', '254', '0', '0', '5/1/2018 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1474, 1094, 10, 'DR.AFTAB AHMED', '', '', '', 'PMDC# 56878-P', 'NOOR CLINIC PANJAN KISANA', 'Doctor', '', '', '', '56878', '0', '0', '12/31/2018 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1475, 13121, 13, 'DR.RAHEELA RANI', '', '', '', 'PMDC# 92716-P', 'CH.MEHNDI KHAN CLINIC MACHIWAL', 'Doctor', '', '', '', '92716', '0', '0', '9/8/2018 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1476, 825, 8, 'DR.SYED OSAMA TALAT', '', '', '', 'GUJRAT', 'PMDC# 54178-P', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1477, 1394, 13, 'BILAL MEDICAL STORE', '', '', '', '', 'DANDY WALA BULANI', 'Retailer', '', '', '', '0', '32875', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1478, 5167, 5, '.MEDI CARE PHARMACY', '', '', '', 'OPP.IKRAM HOSPITAL', 'BHIMBER ROAD GUJRAT', 'Retailer', '', '', '', '33035', '0', '0', '5/24/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1479, 1396, 13, 'DAWOOD RAZA MEDICAL STORE', '', '', '', 'NEAR WATAR SUPPLYOFFICE', 'MAIN BAZAR FATEHPUR', 'Retailer', '', '', '', '0', '10154', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1480, 1556, 15, 'DR.HAROON KHURSHIED', '', '', '', 'JANDALA', 'PMDC NO.4332-AJK', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1481, 1397, 13, 'UMAR PHARMACY', '', '0348-0451304', '', '', 'CHOWK DOULAT NAGAR', 'Retailer', '34201-0348507-7', '6367178-2', '', '66738', '0', '0', '3/4/2023 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1482, 995, 9, 'DR.MADIHA EJAZ', '', '', '', 'PMDC NO.60219-P', 'LINK RAILWAY ROAD GUJRAT', 'Doctor', '', '', '', '60219', '0', '0', '12/31/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1483, 996, 9, 'DR.ABDUL MUNIR', '', '', '', 'TANVEER CLINIC', 'RAILWAY Rd GUJRAT PMDC#100776', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1484, 2212, 22, '.DR.AQSA', '', '', '', 'RHC JOKALIAN', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1485, 1095, 10, 'DR.ASIF IQBAL', '', '', '', 'PMDC# 25305-P', 'MALIK Hp MAIN BAZAR LALAMUSA', 'Doctor', '34202-0815747-5', '2339525-7', '', '25305', '0', '0', '12/31/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1486, 390, 3, '.MAQSOOD MEDICAL STORE', '', '', '', 'OPP.POLICE CHOWKI', 'MAIN ROAD JALAL PUR SOBTIAN', 'Retailer', '3420105773539', '1111047-3', '', '5181', '0', '0', '10/23/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1487, 1398, 13, 'DR.M.IRSHAD', '', '', '', 'KOTLA', 'PMDC NO.14476-P', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1488, 1399, 13, 'DR.M.UMAR WAQAS', '', '', '', 'HABIB CLINIC DANDIDARA', 'PMDC NO.73244-P', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1489, 1557, 15, 'DR.HAIDER AZIM', '', '', '', 'AL ZAMAN M/C POONA', 'PMDC NO.3440-AJK', 'Doctor', '', '', '', '3440', '0', '0', '12/31/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1490, 997, 9, 'DR.CH.IMRAN SARWAR', '', '', '', 'SARGHODA ROAD GUJRAT', 'PMDC NO.41788-P', 'Doctor', '', '', '', '41788', '0', '0', '12/31/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1491, 2323, 23, '.DR.NABEIL SHAIKH', '', '', '', 'SAMAMOLA', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1492, 5168, 5, 'DR.SADIA IRFAN', '', '', '', 'POLY CLINIC BIMBER', 'ROAD GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1493, 998, 9, 'MASOOD MEDICAL HALL', '', '', '', 'PAKKI GALI', 'RAILWAY ROAD GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '12/30/1899 12:00 AM', '10/10/2019 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1494, 1242, 12, '.SHAKEEL MEDICAL STORE', '', '', '', 'BARNALA ROAD BHRING', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1495, 391, 3, '.DR.UZMA SHAHEEN', '', '', '', 'FATIMA METERNTY HP', 'KARINANWALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1496, 392, 3, 'DR MIRZA M.UMAR', '', '', '', 'NAYAB CLINIC KUNGRA', 'PMDC# 75521-P', 'Doctor', '', '', '', '75521', '0', '0', '12/31/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1497, 11116, 11, 'DR.M.AMIN', '', '', '', 'PMDC# 16026-N', 'SYED MEDICAL CENTER LALAMUSA', 'Doctor', '34202-9315656-5', '5297052-3', '', '16026', '0', '0', '12/31/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1498, 13100, 13, 'UMER PHARMACY', '', '', '', '', 'CHOWK DOULAT NAGAR', 'Retailer', '', '', '', '35107', '0', '0', '7/20/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1499, 2322, 23, '.DR.IBRAHEEM', '', '', '', '', 'SHADIWAL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1500, 393, 3, '.DR.SAIRA IRFAN', '', '', '', 'AL-SHARIF MET. HOME', 'BEHLOL PUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1501, 13101, 13, 'DR.ALI HANIF', '', '', '', 'PMDC# 94742-P', 'MURAD EYE HOSPITAL KOTLA', 'Doctor', '', '', '', '94742', '0', '0', '3/14/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1502, 832, 8, 'RASHID MEDICAL STORE', '', '', '', 'NEAR UNION COUNCIL OFFICE', 'MADINA SYEDAN GUJRAT', 'Retailer', '', '', '', '0', '7056', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1503, 13103, 13, '.DR.ASHFAQ', '', '', '', 'DLAWRPUR', '', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1504, 13104, 13, 'DR.SALMAN AHMAD', '', '', '', 'PMDC# 46690-P', 'KOTLA', 'Doctor', '', '', '', '46690', '0', '0', '12/31/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1505, 1097, 10, 'DUA PHARMACY', '', '', '', 'FAIZ CHOWK OPP. TEHSIL LEVEL', 'HOSPITAL LALAMUSA', 'Retailer', '34202-6411313-5', '', '', '32717', '0', '0', '6/19/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1506, 5170, 5, 'SALEEM MEDICAL STORE', '', '', '', 'LALAZAR COLONY, NEARZAMINDAR', 'COLLEGE, BHIMBER ROAD GUJRAT', 'Retailer', '', '', '', '0', '34458', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1507, 1098, 10, 'MUGHAL MEDICAL STORE', '', '', '', 'MALL ROAD,CAMPING GROUND', 'LALAMUSA', 'Retailer', '34202-0740352-1', '34202-0740352-1', '', '3292', '0', '0', '9/5/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1508, 1099, 10, 'JEDDAH PHARMACY', '', '', '', 'OPPOSITE YASIR SWEETS', 'MAIN BAZAR LALAMUSA', 'Retailer', '', '', '', '32365', '0', '0', '4/25/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1509, 11106, 11, 'DR.KHIZAR HAYAT KHAN', '', '', '', 'PMDC#11671-P', 'KHIZAR Hp DINGA ROAD KHARIAN', 'Doctor', '', '', '', '11671', '0', '0', '12/31/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1510, 1858, 18, '.NAWAZ CLINIC', '', '', '', '', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1511, 1859, 18, 'AWAN MEDICAL STORE', '', '', '', '', 'GLYANA CHOWK', 'Retailer', '', '', '', '0', '3244', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1512, 5171, 5, '.KHAWAJGAN DIALICES CENTER', '', '', '', '', 'BHIMBER ROAD GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1513, 13105, 13, 'DR.SHAHARYAR BASIT', '', '0333-8462426', '', 'PMDC# 92254-P', 'IQBAL CLINIC DOULAT NAGAR', 'Doctor', '34201-0347089-7', '7597334-2', '', '92254', '0', '0', '12/31/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1514, 11107, 11, 'DR.KHALID JAMEEL', '', '', '', 'PMDC# 12302-P', 'HASAN M/C GT ROAD KHARIAN', 'Doctor', '', '', '', '12302', '0', '0', '12/31/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1515, 2123, 21, '.AL-DAIM MEDICAL COMPLEX', '', '', '', 'TAPIALA ROAD', 'MANGOWAL', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1516, 1860, 18, '.AL-SHIFA CLINIC', '', '', '', '', 'BADHAR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1517, 10100, 10, 'DR.IKRAM M.ZAMAN NASEEM', '', '', '', 'PMDC# 82294-P', 'ITTIFAQ HEALTHCARE DEWNA MANDI', 'Doctor', '', '', '', '82294', '0', '0', '12/31/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1518, 1861, 18, 'AL GHANI PHARMACY', '', '0306-5198413', '', 'JALAL PUR JATTAN ROAD', 'GULIANA', 'Retailer', '34202-0693324-1', '', '', '37439', '0', '0', '8/13/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1519, 10101, 10, 'MEDI GREEN PHARMACY', '', '', '', 'NEAR BANK AL HABIB', 'GT ROAD LALAMUSA', 'Retailer', '34202-7767257-7', '5817166-7', '', '38439', '0', '0', '12/5/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1520, 5173, 5, 'DR.ANUM AITZAZ', '', '', '', 'PMDC# 74205-P', 'BASHIR BEGUM HOSPITAL GUJRAT', 'Doctor', '34603-4846556-2', '6895427-5', '', '51109', '0', '0', '12/31/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1521, 394, 3, 'HUSSAIN MEDICAL STORE', '', '0300-6186053', '', 'TANDA ROAD', 'KARIANWALA', 'Retailer', '', '', '', '38078', '0', '0', '11/17/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1522, 999, 9, 'DR.WASEEM SHAFI', '', '', '', 'SHADIWAL ROAD GUJRAT', 'PMDC#27345-P', 'Retailer', '', '', '', '27345', '0', '0', '12/31/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1523, 10102, 10, 'BILAL BROTHERS MEDICAL STORE', '', '', '', 'Adjacent Faizullah Hospital', 'G.T ROAD LALAMUSA', 'Retailer', '34202-0773607-1', '2860562-4', '', '7593', '0', '0', '9/21/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1524, 13106, 13, 'DR.SAEEDA NOREEN UL HASSAN', '', '', '', 'FAIZ E MIRAAN HOSPITAL KOTLA', 'PMDC# 75714-P', 'Doctor', '', '', '', '75714', '0', '0', '12/31/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1525, 9100, 9, '.MOHSIN MS', '', '', '', '', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1526, 11109, 11, 'DR.M.AFZAL', '', '', '', 'PMDC# 26052-S', 'WALEED CLINIC KHARIAN', 'Doctor', '', '', '', '26052', '0', '0', '12/31/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1527, 13107, 13, 'DR.M.ADREES', '', '0347-6514149', '', 'PMDC# 92551-P', 'CITY CLINIC CHOR CHAK', 'Doctor', '34202-0144724-3', '', '', '92551', '0', '0', '9/5/2018 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1528, 13108, 13, '.NABEEL CLINIC', '', '', '', '', 'KOTLA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1529, 13109, 13, 'SIKANDAR MEDICAL STORE', '', '', '', 'SIDWAL', 'KOTLA ARAB ALI KHAN', 'Retailer', '34201-0452816-9', '5971317-6', '', '37603', '0', '0', '11/5/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1530, 1962, 19, 'KHAN MEDICAL STORE', '', '', '', 'NASEERA', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1531, 1862, 18, 'KHAN MEDICAL STORE', '', '', '', '', 'NASEERA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1532, 10103, 10, 'AHMED MEDICAL STORE', '', '', '', 'HAIDERI CHOWK,CAMPING GROUND', 'LALAMUSA', 'Retailer', '34202-0717679-5', '6466120-7', '', '39119', '0', '0', '1/15/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1533, 13110, 13, 'FARHAN PHARMACY', '', '', '', 'CHOWK', 'DOULAT NAGAR', 'Retailer', '', '', '', '35571', '0', '0', '9/13/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1534, 395, 3, 'DR.NASIR IQBAL CHAUDHRY', '', '', '', 'FAMILY CLINIC HAJIWALA ROAD', 'KARIANWALA PMDC#1207-AJK', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1535, 11115, 11, '.MEHER MEDICAL SERVICE', '', '', '', '', 'VILLAGE GANJA', 'Retailer', '', '', '', '33120', '0', '0', '9/11/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1536, 979, 9, 'DR.ALI HASNAIN BHUTTA', '', '', '', 'PMDC# 78777-P', 'BHUTTA CLINIC GUJRAT', 'Doctor', '', '', '', '78777', '0', '0', '12/31/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1537, 396, 3, 'AHMED MEDICAL STORE', '', '', '', 'RANGPUR CHOWK,NEAR', 'JALALPUR SOBTIAN GUJRAT', 'Retailer', '', '', '', '0', '40732', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1538, 13111, 13, 'DR.SYED ASIM AKHTAR', '', '', '', 'PMDC# 49811-P', 'ASHRAF CHILDREN HP KOTLA', 'Doctor', '', '', '', '49811', '0', '0', '12/31/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1539, 2324, 23, '.DR.FARIHA SHAKIR', '', '', '', 'KHOJYAWALI', '', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1540, 10120, 10, 'AITZAZ', '', '', '', 'LALAMUSA', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1541, 13112, 13, '.NOOR MATERNITY HOSPITAL', '', '', '', '', '', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1542, 10105, 10, 'AL SHIFA PHARMACY', '', '', '', 'MOHALLAH RAHMATABAD', 'GT ROAD LALAMUSA', 'Retailer', '', '', '', '39306', '0', '0', '1/26/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1543, 2214, 22, 'ALI AKBAR MEDICAL STORE', '', '', '', '', 'KHOJIANWALI', 'Retailer', '', '', '', '0', '40690', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1544, 11103, 11, 'NEW AL SHIFA MEDICAL STORE', '', '', '', 'NEAR ALLAH WALI MASJID', 'GULYANA ROAD KHARIAN', 'Retailer', '', '', '', '0', '41394', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1545, 13113, 13, 'DR.M.ALAM', '', '', '', 'PMDC# 11207-P', 'KOTLA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1546, 13114, 13, '.DR.IRSHAD AYYAZ', '', '', '', 'AL SHIFA CLINIC', 'KOTLA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1547, 2127, 21, '.DR.TASAWAR', '', '', '', '', 'MONGOWAL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1548, 5121, 5, 'DR.MUHAMMAD ALI', '', '', '', 'PMDC# 57905-P', 'AL-FLAH M/C QADIR COLONY', 'Doctor', '', '', '', '57905', '0', '0', '12/31/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1549, 11111, 11, 'EXPRESS PHARMACY', '', '0344-4544027', '', 'OPPOSITE SESSION COURT', 'GT ROAD KHARIAN', 'Retailer', '', '', '', '41031', '0', '0', '3/4/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1550, 1864, 18, 'MUDASSAR MEDICAL STORE', '', '', '', 'NEAR BHU HOSPITAL', 'THUTHA RAI BHADUR', 'Retailer', '34202-8313443-5', '8176386-3', '', '0', '41680', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1551, 10106, 10, 'DR.ASMA LATIF', '', '', '', 'PMDC# 52716-P', 'CITY M/C LALAMUSA', 'Doctor', '34202-8039925-8', '5665762-1', '', '52716', '0', '0', '12/31/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1552, 1865, 18, 'DR.ASAD ABBAS', '', '', '', 'SHSHZAD CLINIC ALI CHAAK', 'PMDC# 90930-P', 'Doctor', '', '', '', '90930', '0', '0', '12/31/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1553, 1866, 18, 'SHAHEEN MEDICAL STORE', '', '', '', 'CHANNAN ADDA', 'TEHSIL KHARIAN', 'Retailer', '', '', '', '0', '3415', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1554, 1867, 18, 'AMIR PHARMACY', '', '0342-8646208', '', '', 'GULYANA', 'Retailer', '34202-0804070-9', '5311178-8', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1555, 983, 9, 'QAAZI MEDICAL STORE', '', '0331-0141001', '', 'NEAR ADIL MARKET', 'SARGODHA ROAD GUJRAT', 'Retailer', '', '', '', '0', '41936', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1556, 5123, 5, 'DR.SAAD ULLAH', '', '', '', 'PMDC# 84438-P', 'CHANDALA ROAD GUJRAT', 'Doctor', '', '', '', '84438', '0', '0', '12/31/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1557, 2325, 23, '.DR.BABAR', '', '', '', 'DHAROWAL', '', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1558, 1868, 18, 'DR.M.UMER IJAZ', '', '', '', 'PMDC# 69248-P', 'MARYAM CLINIC NASEERA', 'Retailer', '', '', '', '69248', '0', '0', '12/31/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1559, 5109, 5, 'DR.SYED OSAMA TALAT', '', '0303 6804739', '', 'PMDC# 54178-P', 'TALAT HOSPITAL GUJRAT', 'Doctor', '3420190492459', '8919478-1', '', '54178', '0', '0', '12/31/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1560, 984, 9, 'BASHIR PHARMACY', '', '', '', 'INSIDE STAFF GALA,', 'MAIN GALA AMIN ABAD GUJRAT', 'Retailer', '', '', '', '42363', '0', '0', '5/17/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1561, 10107, 10, 'IFA PHARMACY', '', '0537-512145', '', 'SKY WAY BUILDING', 'RAILWAY ROAD LALAMUSA', 'Retailer', '', '', '', '42968', '0', '0', '7/1/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1562, 2326, 23, 'ANWAR MEDICAL STORE', '', '', '', 'HARYANWALA CHOWK', 'SAHDIWAL ROAD GUJRAT', 'Retailer', '', '', '', '0', '39265', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1563, 1869, 18, '.SHABIR CLINIC', '', '', '', '', 'PANJWARRIAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1564, 9105, 9, 'DR.KASHIF MEHMOOD', '', '', '', 'PMDC# 52056', 'IQBAL CLINIC GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1565, 2327, 23, 'NOOR UL HASSAN PHARMACY', '', '', '', '', 'MAIN BAZAR SHADIWAL', 'Retailer', '', '', '', '50377', '0', '0', '4/12/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1566, 10108, 10, 'MUSA MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '42729', '0', '0', '6/19/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1567, 1916, 19, 'MEDASK PHARMACY', '', '', '', 'NEAR CAR PARKING', 'CMH KHARIAN', 'Retailer', '', '7944576-6', '', '42440', '0', '0', '5/22/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1568, 397, 3, 'ABU BAKAR MEDICAL STORE', '', '', '', 'CHAMB ROAD BARILA SHARIF', '', 'Retailer', '', '', '', '0', '43193', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1569, 5124, 5, 'DR.RAQIA NOOR', '', '0300-6286380', '', 'PMDC# 30874-P', 'SHAHZAD Hp GUJRAT', 'Doctor', '37302-5531220-4', '', '', '30874', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1570, 1870, 18, '.FARHAN CLINIC', '', '', '', '', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1571, 1871, 18, '.AYESHA HOSPITAL', '', '', '', '', 'NASEERA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1572, 10109, 10, 'IFA PHARMACY', '', '', '', 'SKY WAY BUILDING RAILWAY ROAD', 'LALAMUSA', 'Retailer', '', '', '', '42968', '0', '0', '7/1/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1573, 1872, 18, '.SHAHZAD MEDICAL STORE', '', '', '', '', 'CHAKPRANA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1574, 11112, 11, '.DR.KHURSHEED ABU ADAN', '', '', '', 'NAJAM HOSPITAL KHARIAN', '', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1575, 11113, 11, '.DENTAL STUDIO', '', '', '', 'NEAR SAJDA HAFEEZ HOSPITAL', 'GT ROAD KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1576, 10110, 10, 'MADNI MEDICOSE', '', '', '', 'NEAR MALIK HOSPITAL', 'MAIN BAZAR LALAMUSA', 'Retailer', '34202-0431998-9', '4233659-7', '', '4200', '0', '0', '9/9/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1577, 1873, 18, 'HASSAN PHARMACY', '', '', '', '', 'DINGA ROAD CHANAN', 'Retailer', '', '', '', '10516', '0', '0', '9/20/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1578, 5125, 5, 'DR.HAFEEZ U REHMAN', '', '', '', 'PMDC# 12413P', 'USAMA CLINIC GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1579, 9101, 9, 'AL JANNAT PHARMACY', '', '', '', 'SERVICE MORR', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1580, 13115, 13, 'AHSAN MEDICAL STORE', '', '0301-8352280', '', 'OPPOSITE GHOSIA MASJID', 'HANJ', 'Retailer', '', '', '', '0', '18114', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1581, 13116, 13, 'DR.BASHARAT MUNEER', '', '', '', 'KOTLA', '', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1582, 10115, 10, 'DR.TAJAMAL HUSSAIN', '', '', '', 'PMDC# 99796-P', 'SHIFA HOSPITAL DEWNA MANDI', 'Retailer', '', '', '', '24997', '0', '0', '12/31/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1583, 10116, 10, 'DAWAI PLUS PHARMACY', '', '', '', 'INFRONT TEHSIL LEVEL HOSPITAL', 'CAMPING GROUND LALAMUSA', 'Retailer', '34202-2853544-9', '5353612-7', '', '43483', '0', '0', '7/22/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1584, 2328, 23, 'DR.AZHAR IHSAN', '', '', '', 'Opp G.M.H Mongowal Gharbi', 'PMDC# 41164-P', 'Doctor', '', '', '', '41164', '0', '0', '12/31/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1585, 2329, 23, 'CHAUDHRARY MEDICAL STORE', '', '', '', 'NEAR PUL REHMANIA MAIN', 'DINGA ROAD SHAHABDIWAL', 'Retailer', '', '', '', '0', '6192', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1586, 2330, 23, 'DAWOOD MEDICAL STORE', '', '', '', 'SAMMA ROAD BUS STAND', 'LANGAY', 'Retailer', '', '', '', '0', '35778', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1587, 1874, 18, '.UMAR HOSPITAL', '', '', '', '', 'GULYANA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1588, 10117, 10, 'DR.SARFRAZ AHMAD RANA', '', '', '', 'PMDC# 22329-P', 'FATIMA HOSPITAL DEWNA MANDI', 'Doctor', '', '', '', '22329', '0', '0', '12/31/2023 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1589, 1875, 18, 'AL FAZAL MEDICAL STORE', '', '', '', 'PINDI HASHIM', '', 'Retailer', '', '', '', '0', '41921', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1590, 10118, 10, 'DR.ABDUL MUNIR', '', '', '', 'PMDC# 100776-P', 'FIRST M/C DEOWNA MANDI', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1591, 826, 8, 'DR.UMAIR WAHEED', '', '', '', 'SHADMAN WELFARE HOSPITAL', 'SHADMAN ROAD PMDC#100109-P', 'Doctor', '', '', '', '100109', '0', '0', '9/16/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1592, 2111, 21, 'DR.SALEEM AHMED', '', '', '', 'AL NOOR SURGICAL HOSPITAL', 'MANGOWAL PMDC#45243-P', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1593, 13117, 13, 'DR.ADNAN RASOOL', '', '', '', 'SHAUKAT DENTAL KOTLA', '', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1594, 13118, 13, 'DR.FAISAL REHMAN', '', '', '', 'KOTLA', 'PMDC# 2288-AJK', 'Doctor', '', '', '', '2288', '0', '0', '12/31/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1595, 2215, 22, '.DR.TAHIR', '', '', '', 'ASAD ULLAH PUR', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1596, 2331, 23, 'SAJID MEDICAL STORE', '', '', '', 'VPO GOLAKI GUJRAT', '', 'Retailer', '', '', '', '0', '35775', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1597, 9102, 9, 'HASEEB MEDICAL STORE', '', '', '', 'MOHALLAH SULTAN PURA', 'BEHIND EID GAH GT ROAD GURJAT', 'Retailer', '', '', '', '5634', '0', '0', '10/4/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1598, 3100, 3, '.DR.IHSAN ULLAH', '', '', '', '', '', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1599, 9103, 9, 'DR.RABAIL', '', '', '', 'PMDC# 63222-P', 'SHADIWAL ROAD GUJRAT', 'Doctor', '', '', '', '6322', '0', '0', '12/31/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1600, 13119, 13, 'DR.ARSLAN AMJAD', '', '', '', 'PMDC# 106878-P', 'AMJAD M/HALL KOTLA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1601, 11114, 11, '.ADEEL MEDICAL STORE', '', '', '', 'GUNJAH ROAD', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1602, 5130, 5, '.DR.NARGIS', '', '', '', '', 'AL HADAM Hp GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1603, 10121, 10, 'DR.ADNAN RASOOL', '', '', '', 'GHULAM RASOOL DENTAL CLINIC', 'DEONA MANDI', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1604, 10112, 10, 'SALAMAT MEMORIAL MEDICAL STORE', '', '', '', 'MAIN BAZAR NEAR SERWAR PLAZA', 'CHIRAGHPURA LALAMUSA', 'Retailer', '', '', '', '499', '0', '0', '10/9/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1605, 3101, 3, 'DR.IMRAN ZAMAN', '', '0331-9616625', '', 'PMDC #87071-P', 'ZAMAN CLINIC KHOKHA STOP', 'Doctor', '', '', '', '87071', '0', '0', '12/31/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1606, 2335, 23, 'DR.FARIAH', '', '', '', 'AHMED MURTAZA DENTAL CLINIC', 'KHOJIANWALI PMDC#13661-D', 'Retailer', '', '', '', '13661', '0', '0', '12/31/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1607, 581, 5, '.ZEESHAN BUTT MS', '', '', '', '', 'GUJRAT', 'Retailer', '34201-8377609-7', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1608, 1876, 18, 'SHABBIR MEDICAL STORE', '', '', '', 'NEAR MB BUILDERS DINGA ROAD', 'NOONAWALI', 'Retailer', '', '', '', '16913', '0', '0', '10/25/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1609, 1877, 18, 'ABDULLAH MEDICAL STORE', '', '', '', 'HOSPITAL CHOWK', 'MALKA', 'Retailer', '3420232467033', '', '', '0', '38241', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1610, 13120, 13, 'DR.SYEDA NEELAM', '', '0300-4255359', '', 'PMDC# 37108-P', 'ZAINAB HOSPITAL KOTLA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1611, 5104, 5, 'DR.SOFIA IJAZ', '', '', '', 'BHIMBER ROAD GUJRAT', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1612, 3102, 3, 'DR.M.ARSHAD IQBAL', '', '', '', 'PMDC# 21334-P', 'Walli Hussain Mem.Hp LAKHANWAL', 'Doctor', '', '', '', '21334', '0', '0', '12/31/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1613, 1878, 18, 'DR.M.AKHTER HUSSAIN MIRZA', '', '', '', 'HASHIM WELFARE TRUST', 'PINDI HASHIM', 'Doctor', '', '889509-6', '', '45080', '0', '0', '12/31/2023 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1614, 9106, 9, 'DR.ABDUL QADEER', '', '', '', 'PMDC# 59669-P', 'MOH.KALOPURA GUJRAT', 'Doctor', '', '', '', '59669', '0', '0', '12/31/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1615, 10113, 10, 'DR.KHALID AHMED', '', '', '', 'PMDC# 12990-N', 'JOORA', 'Doctor', '', '', '', '12990', '0', '0', '12/31/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1616, 10114, 10, '.FIRST MEDICAL HOSPITAL', '', '', '', 'DEWNA MANDI', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1617, 1879, 18, '.NAUMAN MEDICAL STOR', 'KHASEET PURR', '', '', '', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1618, 2134, 21, 'HAFIZ G PHARMACY', '', '', '', 'NEAR MIAN ASLAM HOSPITAL', 'MANGOWAL GHARBI', 'Retailer', '', '', '', '43672', '0', '0', '7/30/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1619, 10122, 10, 'DR.AYESHA AMJAD', '', '', '', 'PMDC# 66844-P', 'UMER BASHIR Hp LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1620, 9107, 9, 'AKRAM & SONS MEDICAL STORE', '', '', '', 'NEAR POLICE SHAHEEN CHOWKI', 'MUH.QUTABABAD SARGODHA RD GRT', 'Retailer', '', '', '', '0', '10219', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1621, 13122, 13, 'DR.NADEEM SHEHZAD', '', '', '', 'PMDC# 74091-P', 'AL-AZAM HOSPITAL KOTLA', 'Doctor', '34202-9233471-5', '', '', '74091', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1622, 2135, 21, 'AFRIDI MEDICAL STORE', '', '', '', 'VILLAGE KANG CHANNAN', 'SARGODHA ROAD GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1623, 1183, 11, 'DR.HUMAYUN RASHEED', '', '', '', 'PMDC# 5799-P', 'HUMAYUN M/C JANDANWALA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1624, 10123, 10, 'NEW RAFIQ MEDICAL STORE', '', '', '', 'NEAR NOONAN HALWAI MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '2658', '0', '0', '8/26/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1625, 13123, 13, 'CHAUDHRY QAISER PHARMACY', '', '0300-6229626', '', 'JALALPUR JATTAN ROAD', 'FATEH PUR', 'Retailer', '', '', '', '45856', '0', '0', '10/22/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1626, 9108, 9, 'JS PHARMACY', '', '', '', 'MAIN SARGODHA ROAD', 'GUJRAT', 'Retailer', '34201-9272623-1', '4250763-4', '', '48706', '0', '0', '11/23/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1627, 13124, 13, 'DR.ANSAR MAHMOOD', '', '0301-6262934', '', 'ISLAMABAD MEDICAL COMPLEX', 'DOULAT NAGAR', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1628, 2136, 21, 'AHMED PHARMACY', '', '', '', '', 'Dinga Road KEERANWALA', 'Retailer', '', '', '', '4316', '0', '0', '10/26/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1629, 13125, 13, 'DR.IRFAN WARIS', '', '', '', 'PMDC# 81692-P', 'ASSA FREE DISPENSARY SADWAL', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1630, 1880, 18, '.USMAN CLINIC', '', '', '', 'LAHREE', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1631, 9109, 9, 'FAZAIA MEDICAL STORE', '', '0331-8878502', '', 'NEAR MAKKI MASJID', 'SHADIWAL ROAD GUJRAT', 'Retailer', '', '', '', '0', '48926', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1632, 11118, 11, 'CARE PHARMACY', '', '', '', 'UTAM CHOWK DHORIA', 'KHARIAN', 'Retailer', '34202-0276542-5', '4880620-1', '', '48738', '0', '0', '11/21/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM');
INSERT INTO `dealer_info` (`dealer_table_id`, `dealer_id`, `dealer_area_id`, `dealer_name`, `dealer_contact_person`, `dealer_phone`, `dealer_fax`, `dealer_address`, `dealer_address1`, `dealer_type`, `dealer_cnic`, `dealer_ntn`, `dealer_image`, `dealer_lic9_num`, `dealer_lic9_exp`, `dealer_lic10_num`, `dealer_lic10_Exp`, `dealer_lic11_num`, `dealer_lic11_Exp`, `dealer_credit_limit`, `dealer_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`) VALUES
(1633, 13126, 13, 'DR.MUHAMMAD QASIM BILAL', '', '', '', 'SADWAL ROAD', 'KOTLA PMDC#5003-AJK', 'Retailer', '', '', '', '0', '5003', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1634, 5105, 5, '.DR.MUSHTAQ WARRAICH', '', '', '', 'GUJRAT', '', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1635, 5106, 5, 'DR.ZAFAR IQBAL SANDHU', '', '', '', 'PMDC# 49535-P', 'NATIONAL ORTHOPADIC Hp GUJRAT', 'Retailer', '34201-5973942-1', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1636, 10124, 10, 'DR.AMNAH WARIS', '', '', '', 'PMDC# 28447-N', 'ITTEFAQ H/C DEWNA MANDI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1637, 3103, 3, 'ALAM&SONS MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'TANA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1638, 3104, 3, '.SAMEER CLINIC', '', '', '', 'QILLA ISLAM', '', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1639, 11119, 11, 'DR.WAQAS ALI', '', '', '', 'PMDC# 49148-P', 'NAJAM Hp KHARIAN', 'Doctor', '34202-0856793-3', '7149352-4', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1640, 9110, 9, 'FAZAL PHARMACY', '', '0307-7224221', '', 'WAHID TRUST,NEAR PAK FAN', 'GT ROAD GUJRAT', 'Retailer', '34201-5343556-7', '3783510-6', '', '48734', '0', '0', '1/2/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1641, 13127, 13, 'DR.M.SHAFIQ', '', '', '', 'PMDC# 7440-9', 'RAZA EYE Hp KOTLA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1642, 13128, 13, 'AHMED MEDICAL STORE', '', '', '', '', 'SARAI DING GUJRAT', 'Retailer', '', '', '', '49233', '0', '0', '12/12/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1643, 3105, 3, 'MALIK MEDICAL STORE', '', '', '', 'NEAR MASJID MAIN BAZAR', 'AWAN SHARIF', 'Retailer', '', '', '', '0', '49318', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1644, 10125, 10, 'DR.MAJID HASSAN', '', '', '', 'PMDC# 65745-P', 'EJAZ Hp LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1645, 9111, 9, '.ASMA CLINIC', '', '', '', 'MADINA BAZAR SARGODHA ROAD', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1646, 10126, 10, 'DR.ASHRAF MIRZA', '', '', '', '', 'LALAMUSA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1647, 3106, 3, '.DR.SHAISTA WARIS', '', '', '', 'SHOAIB CLINIC AWAN SHARIF', '', 'Retailer', '34201-5079156-9', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1648, 3107, 3, 'MUGHAL PHARMACY', '', '', '', '', 'AWAN SHARIF', 'Retailer', '', '', '', '49607', '0', '0', '1/27/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1649, 10127, 10, '.NADIA LHV', '', '', '', '', '', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1650, 1881, 18, '.AYSHA CLINIC', '', '', '', 'BAGWALL', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1651, 10128, 10, 'DR.CH.M.ALI', '', '', '', 'PMDC# 99850-P', 'DEWNA MANDI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1652, 13129, 13, 'DR.FAZEEL SHAHZAD', 'PMDC *63905-P', '', '', '', 'KOTLA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1653, 9112, 9, 'RATHORE MEDICAL STORE', '', '', '', 'MUHALLAH KALUPURA ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '6258', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1654, 9115, 9, 'AL RASHEED MEDICAL STOR', '', '', '', 'MOHALLAH NOOR PUR PADDY', 'NEAR LAL MASJID GUJRAT', 'Retailer', '', '', '', '0', '38205', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1655, 10129, 10, '.DR.ZEESHAN AHAMED', '', '', '', '', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1656, 10130, 10, 'HASSAN PHARMACY DISTRIBUTOR', '', '', '', '', 'MAIN BAZAR DEONA MANDI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '12/30/1899 12:00 AM', '9/25/2021 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1657, 5107, 5, 'DR.SAADIA IRAM', '', '', '', 'PMDC#36701-P', 'IRFAN HP MARGHAZAR COLONEY', 'Doctor', '34201-1377136-2', '34201-1377136-2', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1658, 1882, 18, '.NAWJJID CLINIC', '', '', '', 'KAMMLA', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1659, 1883, 18, '.AL-MANZOOR CLINIC', '', '', '', '', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1660, 3108, 3, '.DR.M.SHAHBAZ', '', '', '', '', 'MUNGOWAL NEAR JPJ', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1661, 3109, 3, 'DR.SAAD ARSHAD', '', '', '', 'HAIDER ALI HOSPITAL', 'TANDA', 'Doctor', '', '', '', '100437', '0', '0', '12/31/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1662, 9113, 9, 'DR.SAJJAD RASOOL CH.', '', '', '', '', 'RASOOL MEDICAL CENTER GUJRAT', 'Doctor', '', '', '', '30235', '0', '0', '12/31/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1663, 10131, 10, 'HAMZA MEDICAL STORE', '', '', '', 'EID GAH ROAD', 'LALAMUSA', 'Retailer', '34202-6155915-3', 'A312392-2', '', '0', '50818', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1664, 1144, 1, 'DR.UMAR ABBAS', '', '', '', 'PMDC#70592-P', 'Qamar Sialvi Road GUJRAT', 'Retailer', '3420159208689', '4905197-4', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1665, 11120, 11, 'DR.FAISAL ABBAS', '', '', '', 'PMDC# 55142-P', 'DINGA ROAD KHARIAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1666, 9114, 9, 'DR.M.AFZAL', '', '', '', 'PMDC# 24466-P', 'LAHORE M/C GUJRAT', 'Doctor', '', '', '', '24466', '0', '0', '12/31/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1667, 9116, 9, 'MOHSIN PHARMACY', '', '', '', 'MUHALLAH KALUPURA, BADSHAHI RD', 'GUJRAT', 'Retailer', '', '', '', '38433', '0', '0', '12/3/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1668, 2216, 22, 'DR.SABTAIN HAIDER', '', '', '', 'PMDC# 102863-P', 'HAIDER CLINIC CHAKRI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1669, 3112, 3, 'AL MADINA MEDICAL STORE', '', '0345-4704286', '', 'TANDA ROAD', 'KARIANWALA', 'Retailer', '34201-4080696-9', '', '', '0', '50658', '0', '4/1/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1670, 3113, 3, 'DR.WAQAS ALI', '', '', '', 'PMDC# 8442-P', 'TANDA', 'Retailer', '34201-0374237-1', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1671, 10132, 10, 'LALAMUSA PHARMACY A Franchise of', '', '', '', 'FAZAL DIN S Pharma Plus', 'MAIN GT ROAD LALAMUSA', 'Retailer', '', '8719538-5', '', '53793', '0', '0', '7/11/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1672, 2336, 23, 'AHMED MEDICAL STORE', '', '', '', 'MAIN LARI ADDA', 'SHADIWAL', 'Retailer', '', '7588142-8', '', '51494', '0', '0', '5/4/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1673, 9117, 9, '.ZAITOON PHARMACY', '', '', '', 'GUEST HOUSE BUILDING', 'Small Industrial Area GUJRAT', 'Retailer', '', '', '', '54106', '0', '0', '7/22/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1674, 3114, 3, '.DR.ABDUL SATTAR', '', '', '', '', 'KOTLI KOHALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1675, 13130, 13, 'DR.M.JAWAD', '', '', '', 'PMDC# 58919-P', 'CITY CLINIC KOTLA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1676, 13131, 13, 'DR.M.AJMAL', '', '', '', 'PMDC# 16455-P', 'Ahmed Poly Clinic DOULATNAGAR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1677, 3115, 3, 'LIFE CARE PHARMACY', '', '', '', 'THANNA ROAD KARIANWALA', 'NEAR PTCL EXCHANGE GUJRAT', 'Retailer', '34201-5282547-3', '', '', '62585', '0', '0', '10/17/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1678, 1884, 18, '.ZUBAIR CLINIC', '', '', '', 'SEHNA', '', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1679, 3116, 3, '.DR.ZAKIA RAZA', '', '', '', 'KARIANWALA', '', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1680, 9118, 9, 'AHAD PHARMACY', '', '', '', 'NEAR AMIR ELECTRONICS', 'SARGODHA ROAD GUJRAT', 'Retailer', '34201-0302202-9', '', '', '58640', '0', '0', '8/26/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1681, 10133, 10, 'ZAINAB PHARMACY', '', '', '', 'Adjacent Tehsil Level Hospital', 'Camping Ground LALAMUSA', 'Retailer', '34202-9771837-9', '', '', '54066', '0', '0', '7/20/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1682, 5110, 5, 'DR.CH SHAFIQ AHMED', '', '', '', 'PMDC# 7080-P', 'AL-SHAAFI HOSPIATL GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1683, 9119, 9, 'AL SHAFI PHARMACY', '', '', '', 'SARGODHA ROAD', 'NEAR KHALIL HOSPITAL GUJRAT', 'Retailer', '', '', '', '56631', '0', '0', '8/26/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1684, 3117, 3, '.DR.QASIM YAQOOB', '', '', '', 'BHARAJ', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1685, 10134, 10, 'DR.AFZA AKRAM', '', '', '', 'PMDC# 71000-P', 'MADNI CLINIC LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1686, 1160, 11, 'HAIDER MEDICAL STORE', '', '', '', 'OPP. RAWAYAT RESTAURANT', 'NEAR MARALA MORE JHANDAWALA', 'Retailer', '', '', '', '0', '58672', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1687, 10135, 10, 'ALI MEDICAL STORE', '', '', '', 'NEAR LALA RAFIQ MOHALLAH', 'RAFIQABAD GT ROAD LALAMUSA', 'Retailer', '', '', '', '0', '54065', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1688, 1888, 18, '.AMINA MEDICAL CENTER', '', '', '', 'BHADDAR', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1689, 1185, 11, 'JAN SHER CHEMIST PHARMACY', '', '', '', 'MURALA PLAZA DINGA ROAD', 'KHARIAN', 'Retailer', '34202-0762247-3', '1412393-2', '', '58936', '0', '0', '8/22/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1690, 10136, 10, 'DR.SYEDA FATIMA TU ZAHIRA', '', '', '', 'PMDC# 99657-P', 'TAHIR Surgical Hp LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1691, 11124, 11, 'DR.TAHIR IQBAL MIRZA', '', '', '', 'PMDC# 23774-P', 'SUBHAN Hp KHARIAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1692, 11125, 11, '.DR.HAFZA ANWAR', '', '', '', 'KHARIAN', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1693, 10137, 10, 'DR.BILAL GHAZANFAR', '', '', '', 'PMDC# 99810-P', 'PASWAL LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1694, 10138, 10, 'ISMAIL MEDICAL STORE', '', '', '', 'NEAR SHAUKAT KARYANA STORE', '', 'Retailer', '', '', '', '0', '54155', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1695, 986, 9, 'AL NOOR MEDICAL STORE', '', '0344-6488459', '', 'Opp.Sain Raja Phatac,', 'GT Road GUJRAT', 'Retailer', '', '', '', '0', '51862', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1696, 3118, 3, 'DR.UMAIR WAHEED', '', '', '', 'PMDC# 100109-P', 'NAZIR AHMED WELFARE TRUST', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1697, 3119, 3, '.DR.M.AFZAL', '', '', '', '', 'SAGGAR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1698, 13132, 13, 'DR.M.BILAL', '', '', '', 'PMDC# 79000-P', 'ANAYAT BASHIR CLINIC KOTLA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1699, 987, 9, 'ZAID MEDICAL STORE', '', '', '', 'MOHALLAH FAIZABAD NEAR HASSAN', 'GENERAL STORE GUJRAT', 'Retailer', '', '3351420-8', '', '0', '6640', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1700, 1889, 18, 'AMIR JAVED PHARMACY', '', '', '', 'VILAGE BHADAR TAHSIL KHARIAN', '', 'Retailer', '34202-4088857-5', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1701, 1890, 18, '.AL SYED MEDICAL STORE', '', '', '', 'PANJAN KASNA', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1702, 988, 9, 'DR.AAMARA SHARIQ', '', '', '', 'PMDC#87604-P', 'SADDIQUE M/C GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1703, 10139, 10, 'DR.NABEEL MASOOD', 'PMDC 99701-P', '', '', 'ALI HEALTH CENTER SEDHRI', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1704, 989, 9, 'DR.NASRULLAH ALTAF CH', '', '', '', 'LIFE CARE MEDICAL CENTER', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1705, 13133, 13, 'DR.FAREEDA YASMIN', '', '', '', 'PMDC# 39225-P', 'NOOR CLINIC KOTLA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1706, 3120, 3, 'BILAL MEDICAL STORE', '', '', '', 'ADDA MARI WARRAICHAN', '', 'Retailer', '', '', '', '0', '63680', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1707, 9155, 9, 'DR.UMER HUSSAIN', 'WAHID TRUST HOSPITAL', '', '', 'PMDC 76898-N', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1708, 13134, 13, 'QAISER MEDICAL STORE', '', '', '', 'AWAN SHRIF ROAD,MALIK CHOWK', 'GUJRAT', 'Retailer', '', '', '', '0', '62880', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1709, 11126, 11, '.DR.MUHAMMAD AFZAL', '', '', '', 'JANDANWALA', 'TAJAMAL MEDICAL CENTER', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1710, 9157, 9, 'ASIF PHARMACY', '', '', '', 'KALUPURA BAZAR NEAR ALLAMA', 'PLAZA GUJRAT', 'Retailer', '', '', '', '64068', '0', '0', '12/3/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1711, 11127, 11, 'ALI PHARMACY', '', '', '', 'GULYANA MORR', 'VILLAGE THEKRIYAN', 'Retailer', '', '', '', '0', '0', '0', '11/28/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1712, 10142, 10, 'HASNAT MEDICAL STORE', '', '', '', 'JOURA KAMANA OPP.HUZAIMA SWEET', 'BUS STOP JOURA', 'Retailer', '', '', '', '0', '48542', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1713, 3121, 3, 'YOUSAF MEDICAL STORE', '', '', '', 'MAIN BAZAR TANDA NEAR P.O.', 'TANDA', 'Retailer', '', '', '', '0', '63957', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1714, 2337, 23, '.SAJID MS', '', '', '', 'GOLAYKI', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1715, 10143, 10, 'DR.CH.M.SHAHZAD', '', '', '', 'PMDC# 51216-P', 'BASHIR CHILD Hp LALAMUSA', 'Doctor', '', '7493398-7', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1716, 11128, 11, 'TAJAMAL MEDICAL CENTRE', '', '', '', 'KHARIAN', '', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1717, 10144, 10, 'OXYGEN PHARMACY', '', '', '', 'CITY MALL,G.T ROAD', 'LALAMUSA', 'Retailer', '', '', '', '64330', '0', '0', '12/16/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1718, 1891, 18, '.SUHAIL MS', '', '', '', '', 'CHANAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1719, 3122, 3, 'DR.NAUMAN ALI', '', '', '', 'PMDC# 96666-P', 'RAJA SABIR CLINC SAGGAR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1720, 3124, 3, 'DR.HUSSAIN HUMAYON', '', '', '', 'PMDC# 74381-P', 'MUSA Hp KARIANWALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1721, 13135, 13, 'NEW HAFIZ BROTHER PHARMACY', '', '', '', 'VILLAGE PYARA FATAH PUR RD', 'GUJRAT', 'Retailer', '', '', '', '45759', '0', '0', '10/5/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1722, 13137, 13, 'AL SYED PHARMACY', '', '', '', 'PURANA KOTLA ARAB ALI KHAN', 'ROAD KOTLA', 'Retailer', '3420250335707', '', '', '64996', '0', '0', '1/2/2023 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1723, 9158, 9, 'DR.EJAZ BASHIR', '', '', '', 'PMDC# 19674-P', 'CLEFT Hp GUJRAT', 'Retailer', '34201-3976712-3', '7358793-6', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1724, 1892, 18, 'ABDULLAH MEDICAL STORE', '', '', '', '', 'BHADDAR CHOWK', 'Retailer', '3420249097723', '', '', '0', '64512', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1725, 1893, 18, '.MANZOOR CLINIC', '', '', '', 'SUMALLA', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1726, 13139, 13, 'HAMZA MEDICAL STORE', 'VILLAGE LANGARIL NEA', '', '', 'R UBL BANK', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1727, 2217, 22, '.LHV SADIYA', '', '', '', 'JOKALIYAN', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1728, 2218, 22, '.LHV UZMA', '', '', '', 'KOT ALLAH BAKHSH', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1729, 10145, 10, 'DR.SHAKILA AKHTAR', '', '', '', 'PMDC# 284-N', 'BANO MEM.CLINIC LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1730, 958, 9, 'DANISH HEALTHCARE PHARMACY(A FRANCHISE', '', '', '', 'OF FAZAL DINS PHARMA PLUS)', 'PAGGAN WALA PLAZA GUJRAT', 'Retailer', '34201-6499258-5', 'A340614', '', '65695', '0', '0', '2/12/2023 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1731, 10146, 10, 'REHMAT PHARMACY', '', '', '', 'MAIN BAZAR', 'DEONA MANDI', 'Retailer', '', '', '', '65210', '0', '0', '1/26/2023 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1732, 528, 5, 'DR.ALI HANIF', '', '', '', 'PMDC# 94742-P', 'Family Health Clinic GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1733, 3125, 3, 'DR.AFZAAL HUSSAIN', '', '0346-3661987', '', 'PMDC# 78102-P', 'AL-SUGRAH Hp TANDA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1734, 1894, 18, '.AL SHIFA HEALTH CENTER', 'GULYNA', '', '', '', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1735, 1895, 18, '.ISLAM WELFARE TRUST', 'DHONNI', '', '', '', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1736, 10147, 10, 'DR.IRFAN ARSHAD RANA', '', '', '', 'GT ROAD LALAMUSA PMDC#47290-P', 'NATIONAL NEURO MEDICAL CENTER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1737, 13141, 13, 'M.ARSAL PHARMACY', '', '', '', '', 'FATAH PUR CHOWK', 'Retailer', '34201-2451453-3', '', '', '66728', '0', '0', '3/4/2023 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1738, 9159, 9, 'KHALID MEDICAL STORE', '', '', '', 'RAILWAY ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1739, 1896, 18, 'DR.KHALID REHMAN', '', '', '', 'PMDC# 17143', 'JANDANWALA', 'Retailer', '', '1057392-6', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1740, 1917, 19, 'NEW JAN SHER PHARMACY AND MEDICAL STORE', '', '', '', 'CB PLAZA SADDAR BAZAR', 'KHARIAN CANTT', 'Retailer', '34202-0779831-9', '4127856-9', '', '71909', '0', '0', '4/10/2023 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1741, 16, 1, 'AL-MADINA PHARMACY', '', '', '', 'DHAKI CHOWK', 'GUJRAT', 'Retailer', '34101-2773558-7', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1742, 9121, 9, 'DR.HASSAN ZIA', '', '', '', 'HASSAN HEALTH CARE', 'GULZAR MADINA ROAD GUJRAT', 'Retailer', '34201-6826814-7', '4110731-4', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1743, 944, 9, 'DR.M. IHSAN ULLAH', '', '', '', 'SARGODHA ROAD', 'RAUF HOSPITAL', 'Doctor', '', '', '', '104774', '0', '0', '6/10/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1744, 1918, 19, 'AL FAZAL PHARMACY & COSMETICS', '', '', '', 'SUF SHIVKAN SHOPPING PLAZA', 'KHARIAN CANTT', 'Retailer', '', '', '', '71930', '0', '0', '4/10/2023 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1745, 9161, 9, 'DR.SYED WAQAS HAIDER', '', '', '', 'PMDC# 57587-P', 'SABAR H/C KHATALA', 'Retailer', '', '3935827-5', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1746, 1162, 1, 'DR.QASIM SALMAN', 'AYSHA HOSPITAL JINAH', '', '', 'ROAD GUJRAT', 'PMDC 37679-P', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1747, 2340, 23, 'NEW MAJID PHARMACY', '', '', '', 'HOSPITAL CHOWK', 'SHADIWAL', 'Retailer', '3420127704435', '2926669-6', '', '72876', '0', '0', '7/7/2023 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1748, 14, 1, 'DR.ALI RAUF', '', '', '', 'TAIMOOR CHOWK  PMDC#79879-P', 'GUJRAT KIDNEY CENTER', 'Retailer', '34201-4078295-5', '5572760-5', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1749, 13142, 13, 'DR.KAINAT ASHRAF', '', '', '', 'PMDC# 5673-AJK', 'NATIONAL H/C KOTLA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1750, 946, 9, 'FAMILY CARE PHARMACY', '', '', '', 'NEAR SHELL PETROL PUMP', 'SHADIWAL ROAD GUJRAT', 'Retailer', '', '', '', '78017', '0', '0', '7/24/2023 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1751, 1312, 13, 'ZAIN PHARMACY', '', '', '', 'NEAR AMIR BAKERY MARJAN CHOWK', 'KHARANA', 'Retailer', '34201-1232726-1', '', '', '77775', '0', '0', '7/14/2023 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1752, 2124, 21, 'ESSA MEDICAL STORE', '', '', '', 'OPP. POLICE CHOKI', 'MANGOWAL GHARBI', 'Retailer', '34201-4716171-7', 'A094284-0', '', '0', '77948', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1753, 2341, 23, 'ATTA & SONS PHARMACY', '', '', '', 'NEAR POLICE CHOKI', 'SHADIWAL', 'Retailer', '3420180162415', '', '', '78000', '0', '0', '7/24/2023 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1754, 3126, 3, 'DR.UMAR SOBAN', '', '', '', 'PMDC# 92824-P', 'MUMTAZ CLINIC KARIANWALA', 'Retailer', '3420164459173', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1755, 1897, 18, 'DR.AFSHAN IRAM', '', '', '', 'PMDC# 58649-P', 'HASSAN CLINIC GULANA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1756, 10148, 10, 'DR.HASNAIN ASHRAF', '', '', '', 'PMDC# 86026-P', 'DEWNA MANDI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1757, 13143, 13, 'DR.BASIT ALI', '', '', '', 'SHAAFI HOSPITAL', 'PMDC#1067763-P', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(1758, 2342, 23, 'HASEEB MEDICAL STORE', '', '', '', 'VILLAGE LANGAY', 'TEH. & DISTT. GUJRAT', 'Retailer', '', '', '', '0', '40685', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM');

-- --------------------------------------------------------

--
-- Table structure for table `dealer_overall_record`
--

DROP TABLE IF EXISTS `dealer_overall_record`;
CREATE TABLE IF NOT EXISTS `dealer_overall_record` (
  `dealer_overall_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `orders_given` int(10) UNSIGNED NOT NULL,
  `successful_orders` int(10) UNSIGNED NOT NULL,
  `ordered_packets` int(10) UNSIGNED NOT NULL,
  `submitted_packets` int(10) UNSIGNED NOT NULL,
  `ordered_boxes` int(10) UNSIGNED NOT NULL,
  `submitted_boxes` int(10) UNSIGNED NOT NULL,
  `order_price` float NOT NULL,
  `discount_price` float NOT NULL,
  `invoiced_price` float NOT NULL,
  `cash_collected` float NOT NULL,
  `return_packets` int(10) NOT NULL,
  `return_boxes` int(10) NOT NULL,
  `return_price` float NOT NULL,
  `cash_return` float NOT NULL,
  `waived_off_price` float NOT NULL,
  `pending_payments` float NOT NULL,
  PRIMARY KEY (`dealer_overall_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dealer_payments`
--

DROP TABLE IF EXISTS `dealer_payments`;
CREATE TABLE IF NOT EXISTS `dealer_payments` (
  `payment_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `amount` float NOT NULL,
  `cash_type` varchar(10) NOT NULL,
  `pending_payments` float NOT NULL,
  `date` varchar(30) NOT NULL,
  `day` varchar(15) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `status` varchar(30) NOT NULL,
  PRIMARY KEY (`payment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dealer_types`
--

DROP TABLE IF EXISTS `dealer_types`;
CREATE TABLE IF NOT EXISTS `dealer_types` (
  `dealer_type_ID` int(11) NOT NULL AUTO_INCREMENT,
  `typeID` int(11) NOT NULL,
  `dealerType_name` varchar(100) NOT NULL,
  `typeStatus` varchar(50) NOT NULL,
  `DeleteStatus` int(11) NOT NULL,
  PRIMARY KEY (`dealer_type_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `discount_policy`
--

DROP TABLE IF EXISTS `discount_policy`;
CREATE TABLE IF NOT EXISTS `discount_policy` (
  `disc_policyID` int(11) NOT NULL AUTO_INCREMENT,
  `approval_id` int(11) NOT NULL,
  `dealer_table_id` int(11) DEFAULT NULL,
  `company_table_id` int(11) UNSIGNED DEFAULT NULL,
  `product_table_id` int(11) UNSIGNED DEFAULT NULL,
  `sale_amount` float NOT NULL,
  `discount_percent` float NOT NULL,
  `start_date` varchar(50) NOT NULL,
  `end_date` varchar(50) NOT NULL,
  `policy_status` varchar(50) NOT NULL,
  `entered_by` int(11) NOT NULL,
  `entry_date` varchar(50) NOT NULL,
  `entry_time` varchar(50) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `update_date` varchar(50) NOT NULL,
  `update_time` varchar(50) NOT NULL,
  PRIMARY KEY (`disc_policyID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `district_info`
--

DROP TABLE IF EXISTS `district_info`;
CREATE TABLE IF NOT EXISTS `district_info` (
  `district_table_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `district_id` int(10) UNSIGNED DEFAULT NULL,
  `district_name` varchar(100) NOT NULL,
  `district_status` varchar(20) NOT NULL,
  `creating_user_id` int(10) UNSIGNED NOT NULL,
  `creating_date` varchar(20) NOT NULL,
  `creating_time` varchar(20) NOT NULL,
  `update_user_id` int(10) UNSIGNED NOT NULL,
  `update_date` varchar(20) NOT NULL,
  `update_time` varchar(20) NOT NULL,
  PRIMARY KEY (`district_table_id`),
  UNIQUE KEY `district_id` (`district_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `groups_info`
--

DROP TABLE IF EXISTS `groups_info`;
CREATE TABLE IF NOT EXISTS `groups_info` (
  `group_table_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `group_id` int(10) UNSIGNED DEFAULT NULL,
  `company_id` int(10) UNSIGNED NOT NULL,
  `group_name` varchar(200) NOT NULL,
  `group_status` varchar(20) NOT NULL,
  `creating_user_id` int(10) UNSIGNED NOT NULL,
  `creating_date` varchar(20) NOT NULL,
  `creating_time` varchar(20) NOT NULL,
  `update_user_id` int(10) UNSIGNED NOT NULL,
  `update_date` varchar(20) NOT NULL,
  `update_time` varchar(20) NOT NULL,
  PRIMARY KEY (`group_table_id`),
  UNIQUE KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mobile_curr_locations`
--

DROP TABLE IF EXISTS `mobile_curr_locations`;
CREATE TABLE IF NOT EXISTS `mobile_curr_locations` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `latitude` varchar(25) NOT NULL,
  `longitude` varchar(25) NOT NULL,
  `location_name` varchar(150) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mobile_gps_location`
--

DROP TABLE IF EXISTS `mobile_gps_location`;
CREATE TABLE IF NOT EXISTS `mobile_gps_location` (
  `mob_loc_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `latitude` varchar(20) NOT NULL,
  `longitude` varchar(20) NOT NULL,
  `loc_name` varchar(150) NOT NULL,
  `date` varchar(30) NOT NULL,
  `time` varchar(30) NOT NULL,
  PRIMARY KEY (`mob_loc_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_gps_location`
--

DROP TABLE IF EXISTS `order_gps_location`;
CREATE TABLE IF NOT EXISTS `order_gps_location` (
  `order_loc_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` int(10) UNSIGNED NOT NULL,
  `latitude` varchar(20) NOT NULL,
  `longitude` varchar(20) NOT NULL,
  `loc_name` varchar(150) NOT NULL,
  `date` varchar(30) NOT NULL,
  `time` varchar(30) NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `price` float NOT NULL,
  `type` varchar(20) NOT NULL,
  PRIMARY KEY (`order_loc_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_info`
--

DROP TABLE IF EXISTS `order_info`;
CREATE TABLE IF NOT EXISTS `order_info` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `dealer_id` int(11) DEFAULT NULL,
  `product_id` varchar(500) DEFAULT NULL,
  `quantity` varchar(500) DEFAULT NULL,
  `unit` varchar(500) NOT NULL,
  `order_price` float NOT NULL,
  `bonus` varchar(500) NOT NULL,
  `discount` varchar(500) NOT NULL,
  `waive_off_price` float NOT NULL,
  `final_price` float NOT NULL,
  `order_success` tinyint(1) NOT NULL,
  `comments` varchar(500) NOT NULL,
  `booking_latitude` varchar(25) NOT NULL,
  `booking_longitude` varchar(25) NOT NULL,
  `booking_area` varchar(300) NOT NULL,
  `booking_date` varchar(50) NOT NULL,
  `booking_time` varchar(50) NOT NULL,
  `booking_user_id` int(10) UNSIGNED NOT NULL,
  `delivered_latitude` varchar(25) DEFAULT NULL,
  `delivered_longitude` varchar(25) DEFAULT NULL,
  `delivered_area` varchar(300) DEFAULT NULL,
  `delivered_date` varchar(20) DEFAULT NULL,
  `delivered_time` varchar(20) DEFAULT NULL,
  `delivered_user_id` int(10) UNSIGNED DEFAULT NULL,
  `update_dates` varchar(15) DEFAULT NULL,
  `update_times` varchar(15) DEFAULT NULL,
  `update_user_id` int(10) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_info_detailed`
--

DROP TABLE IF EXISTS `order_info_detailed`;
CREATE TABLE IF NOT EXISTS `order_info_detailed` (
  `detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(8) UNSIGNED NOT NULL,
  `product_table_id` int(8) UNSIGNED NOT NULL,
  `batch_id` int(10) UNSIGNED NOT NULL,
  `quantity` int(8) NOT NULL,
  `submission_quantity` int(8) NOT NULL,
  `unit` varchar(30) NOT NULL,
  `submission_unit` varchar(30) NOT NULL,
  `order_price` float NOT NULL,
  `bonus_quant` int(11) NOT NULL,
  `discount` float NOT NULL,
  `final_price` float NOT NULL,
  `cmplt_returned_bit` tinyint(1) NOT NULL,
  `returned_quant` int(11) NOT NULL,
  `returned_bonus_quant` int(11) NOT NULL,
  PRIMARY KEY (`detail_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_return`
--

DROP TABLE IF EXISTS `order_return`;
CREATE TABLE IF NOT EXISTS `order_return` (
  `return_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `dealer_id` int(11) NOT NULL,
  `return_total_price` float NOT NULL,
  `return_gross_price` float NOT NULL,
  `order_id` int(11) NOT NULL,
  `entry_date` varchar(20) NOT NULL,
  `entry_time` varchar(20) NOT NULL,
  `update_date` varchar(20) NOT NULL,
  `update_time` varchar(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`return_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_return_detail`
--

DROP TABLE IF EXISTS `order_return_detail`;
CREATE TABLE IF NOT EXISTS `order_return_detail` (
  `return_detail_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `return_id` int(11) NOT NULL,
  `prod_id` int(11) NOT NULL,
  `prod_batch` varchar(15) NOT NULL,
  `prod_quant` int(11) NOT NULL,
  `bonus_quant` int(11) NOT NULL,
  `discount_amount` float NOT NULL,
  `total_amount` float NOT NULL,
  `unit` varchar(20) NOT NULL,
  `return_reason` varchar(100) NOT NULL,
  PRIMARY KEY (`return_detail_id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_status_info`
--

DROP TABLE IF EXISTS `order_status_info`;
CREATE TABLE IF NOT EXISTS `order_status_info` (
  `order_status_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` int(10) UNSIGNED NOT NULL,
  `booking_user_id` int(10) UNSIGNED NOT NULL,
  `booking_date` varchar(20) NOT NULL,
  `booking_latitude` varchar(25) NOT NULL,
  `booking_longitude` varchar(25) NOT NULL,
  `booking_area` varchar(200) NOT NULL,
  `delivered_user_id` int(10) UNSIGNED DEFAULT NULL,
  `delivered_date` varchar(20) DEFAULT NULL,
  `delivered_latitude` varchar(25) DEFAULT NULL,
  `delivered_longitude` varchar(25) DEFAULT NULL,
  `delivered_area` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`order_status_id`),
  UNIQUE KEY `order_id` (`order_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `products_stock`
--

DROP TABLE IF EXISTS `products_stock`;
CREATE TABLE IF NOT EXISTS `products_stock` (
  `ProdStockID` int(11) NOT NULL AUTO_INCREMENT,
  `product_table_id` int(10) NOT NULL,
  `in_stock` int(10) NOT NULL,
  `min_stock` int(10) NOT NULL,
  `max_stock` int(10) NOT NULL,
  `bonus_quant` int(11) NOT NULL,
  `status` varchar(30) NOT NULL,
  PRIMARY KEY (`ProdStockID`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products_stock`
--

INSERT INTO `products_stock` (`ProdStockID`, `product_table_id`, `in_stock`, `min_stock`, `max_stock`, `bonus_quant`, `status`) VALUES
(1, 1, 0, 0, 0, 0, 'Active'),
(2, 2, 0, 0, 0, 0, 'Active'),
(3, 3, 0, 0, 0, 0, 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `product_info`
--

DROP TABLE IF EXISTS `product_info`;
CREATE TABLE IF NOT EXISTS `product_info` (
  `product_table_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `company_table_id` int(10) UNSIGNED NOT NULL,
  `group_table_id` int(10) UNSIGNED DEFAULT NULL,
  `report_prodID` varchar(50) NOT NULL,
  `old_prodID` varchar(20) NOT NULL,
  `product_name` varchar(200) NOT NULL,
  `product_type` varchar(30) NOT NULL,
  `packSize` varchar(30) NOT NULL,
  `carton_size` varchar(15) NOT NULL,
  `retail_price` float NOT NULL,
  `trade_price` float NOT NULL,
  `purchase_price` float NOT NULL,
  `purchase_discount` float NOT NULL,
  `sales_tax` float NOT NULL,
  `hold_stock` int(11) NOT NULL,
  `product_status` varchar(50) NOT NULL,
  `creating_user_id` int(10) UNSIGNED NOT NULL,
  `creating_date` varchar(20) NOT NULL,
  `creating_time` varchar(20) NOT NULL,
  `update_user_id` int(10) UNSIGNED NOT NULL,
  `update_date` varchar(20) NOT NULL,
  `update_time` varchar(20) NOT NULL,
  PRIMARY KEY (`product_table_id`),
  UNIQUE KEY `product_id` (`product_id`)
) ENGINE=MyISAM AUTO_INCREMENT=211 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_info`
--

INSERT INTO `product_info` (`product_table_id`, `product_id`, `company_table_id`, `group_table_id`, `report_prodID`, `old_prodID`, `product_name`, `product_type`, `packSize`, `carton_size`, `retail_price`, `trade_price`, `purchase_price`, `purchase_discount`, `sales_tax`, `hold_stock`, `product_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`) VALUES
(1, 11, 1, 0, '00100062', '1', 'ALPHACOLINE 250 INJ', 'Tablet', '10S', '200', 2586.24, 2198.3, 2066.4, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(2, 42, 4, 0, '00200003', '2', 'APIZYT SYP', 'Tablet', '120ML', '200', 280, 238, 223.72, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(3, 13, 1, 0, '00100019', '3', 'ADLOXIN 400MG INJ', 'Tablet', '250ML', '200', 1000, 850, 790.5, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(4, 14, 1, 0, '00100053', '4', 'AMLO-Q 5MG TAB', 'Tablet', '20S', '200', 123.86, 105.28, 98.97, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(5, 15, 1, 0, '00100054', '5', 'AMLO-Q 10MG TAB', 'Tablet', '20S', '200', 211.85, 180.07, 169.26, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(6, 145, 1, 0, '00100007', '45', 'FAMOPSIN 20MG TAB', 'Tablet', '20S', '200', 200, 170, 159.8, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(7, 153, 1, 0, '00100008', '53', 'FAMOPSIN 40MG TAB', 'Tablet', '10S', '200', 200, 170, 159.8, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(8, 111, 1, 0, '00100063', '11', 'BIOCOBAL 500MG INJ', 'Tablet', '10S', '200', 660, 561, 527.34, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(9, 415, 4, 0, '00200001', '15', 'CALDREE 600 TAB', 'Tablet', '30S', '200', 560, 476, 447.44, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(10, 416, 4, 0, '00200005', '16', 'CARNEEL 500MG TAB', 'Tablet', '30S', '200', 2400, 2040, 1917.6, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(11, 117, 1, 0, '', '17', 'CEFAPEZONE 1GM INJ', 'Tablet', 'VIAL', '200', 296, 252, 232, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(12, 418, 4, 0, '00200008', '18', 'CQ10 100MG CAP', 'Tablet', '30S', '200', 1920, 1632, 1534.08, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(13, 119, 1, 0, '00100013', '19', 'CYCIN 250MG TAB', 'Tablet', '10S', '200', 199.6, 169.66, 159.48, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(14, 120, 1, 0, '00100014', '20', 'CYCIN 500MG TAB', 'Tablet', '10S', '200', 385.9, 328, 308.32, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(15, 121, 1, 0, '00100015', '21', 'CYCIN 200MG INJ', 'Tablet', '100ML', '200', 340, 289, 271.66, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(16, 126, 1, 0, '00100070', '26', 'DAYLINE 250MG INJ (IM)', 'Tablet', 'VIAL', '200', 129.9, 110.4, 103.78, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(17, 127, 1, 0, '00100072', '27', 'DAYLINE 500MG INJ (IM)', 'Tablet', 'VIAL', '200', 218.64, 185.84, 174.69, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(18, 128, 1, 0, '00100073', '28', 'DAYLINE 1GM INJ', 'Tablet', 'VIAL', '200', 346, 294.1, 276.45, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(19, 129, 1, 0, '00100069', '29', 'DAYLINE 250MG INJ (IV)', 'Tablet', 'VIAL', '200', 129.9, 110.4, 103.78, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(20, 130, 1, 0, '00100071', '30', 'DAYLINE 500MG INJ (IV)', 'Tablet', 'VIAL', '200', 218.64, 185.84, 174.69, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(21, 131, 1, 0, '00100077', '31', 'DAYTAXIME 1G INJ', 'Tablet', '1S', '200', 240, 204, 191.76, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(22, 132, 1, 0, '00100075', '32', 'DAYTAXIME 250MG INJ', 'Tablet', '1S', '200', 95.21, 80.93, 76.06, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(23, 133, 1, 0, '00100076', '33', 'DAYTAXIME 500MG INJ', 'Tablet', '1S', '200', 152, 129.2, 121.45, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(24, 134, 1, 0, '00100078', '34', 'DAYPIME 500MG INJ', 'Tablet', 'VIAL', '200', 407.34, 346.24, 325.46, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(25, 135, 1, 0, '00100079', '35', 'DAYPIME 1GM INJ', 'Tablet', 'VIAL', '200', 681.37, 579.17, 544.42, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(26, 136, 1, 0, '00100074', '36', 'DAYFORT 1GM INJ', 'Tablet', 'VIAL', '200', 319.57, 271.63, 255.33, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(27, 140, 1, 0, '00100018', '40', 'EM-FLOX 400MG TAB', 'Tablet', '5S', '200', 589.11, 500.74, 470.7, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(28, 141, 1, 0, '00100005', '41', 'ESKEM 20MG CAP', 'Tablet', '14S', '200', 248.37, 211.12, 198.45, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(29, 142, 1, 0, '00100006', '42', 'ESKEM 40MG CAP', 'Tablet', '14S', '200', 399.67, 339.72, 319.33, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(30, 146, 1, 0, '00100012', '46', 'FUGACIN 200MG TAB', 'Tablet', '10S', '200', 258.83, 219.75, 206.8, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(31, 147, 1, 0, '00100044', '47', 'FREEHALE 5MG TAB', 'Tablet', '14S', '200', 256, 217.6, 204.54, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(32, 148, 1, 0, '00100045', '48', 'FREEHALE 10MG TAB', 'Tablet', '14S', '200', 360, 306, 287.64, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(33, 149, 1, 0, '00100046', '49', 'FREEHALE SACHET 4MG', 'Tablet', '14S', '200', 280, 238, 223.72, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(34, 154, 1, 0, '', '54', 'GARLISH* CAP', 'Tablet', '30S', '200', 547.4, 476, 476, 8, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(35, 159, 1, 0, '00100083', '59', 'HIZONE IM 250MG INJ', 'Tablet', 'VIAL', '200', 134.57, 114.38, 107.51, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(36, 160, 1, 0, '', '60', 'HIZONE IV 250MG INJ', 'Tablet', 'VIAL', '200', 125.55, 112, 100, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(37, 161, 1, 0, '00100082', '61', 'HIZONE 1GM INJ', 'Tablet', 'VIAL', '200', 535.04, 454.78, 427.49, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(38, 162, 1, 0, '00100064', '62', 'HIFER 100MG INJ', 'Tablet', '5S', '200', 1680, 1428, 1342.32, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(39, 166, 1, 0, '00100049', '66', 'JARDIN 10MG TAB', 'Tablet', '10S', '200', 99.09, 84.23, 79.17, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(40, 167, 1, 0, '00100051', '67', 'JARDIN D  TAB', 'Tablet', '10S', '200', 259.22, 220.34, 207.12, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(41, 168, 1, 0, '00100050', '68', 'JARDIN D SYP', 'Tablet', '120ML', '200', 259.22, 220.34, 207.11, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(42, 472, 4, 0, '00200004', '72', 'KROFF SYP', 'Tablet', '120ML', '200', 198, 168.3, 158.2, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(43, 173, 1, 0, '', '73', 'KERT 8MG TAB', 'Tablet', '30S', '200', 214, 182, 167, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(44, 174, 1, 0, '', '74', 'KERT 16MG TAB', 'Tablet', '30S', '200', 312, 266, 245, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(45, 179, 1, 0, '00100009', '79', 'LCYN 250MG TAB', 'Tablet', '10S', '200', 220, 187, 175.78, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(46, 180, 1, 0, '00100010', '80', 'LCYN 500MG TAB', 'Tablet', '10S', '200', 332, 282.2, 265.27, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(47, 181, 1, 0, '00100011', '81', 'LCYN 750MG TAB', 'Tablet', '10S', '200', 440, 374, 351.56, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(48, 485, 4, 0, '00200009', '85', 'MAXFOL 400mcg TAB', 'Tablet', '30S', '200', 280, 238, 223.72, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(49, 186, 1, 0, '00100065', '86', 'MIXEL 200MG CAP', 'Tablet', '5S', '200', 234.49, 199.32, 187.36, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(50, 187, 1, 0, '00100066', '87', 'MIXEL 400MG CAP', 'Tablet', '5S', '200', 394.51, 335.33, 315.21, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(51, 188, 1, 0, '00100067', '88', 'MIXEL 100MG SYP', 'Tablet', '30ML', '200', 220.18, 187.15, 175.92, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(52, 189, 1, 0, '00100068', '89', 'MIXEL DS 200MG SYP', 'Tablet', '30ML', '200', 277, 235.45, 221.32, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(53, 193, 1, 0, '00100037', '93', 'NIMODIN 30MG TAB', 'Tablet', '30S', '200', 560, 476, 447.44, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(54, 497, 4, 0, '00200007', '97', 'OXIVA TAB', 'Tablet', '20S', '200', 1600, 1360, 1278.4, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(55, 198, 1, 0, '00100023', '98', 'ONE-3 20/120MG TAB', 'Tablet', '16S', '200', 411.55, 349.82, 328.83, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(56, 199, 1, 0, '00100024', '99', 'ONE-3 40/240MG TAB', 'Tablet', '8S', '200', 339.53, 288.6, 271.28, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(57, 1100, 1, 0, '00100025', '100', 'ONE-3 80/480MG TAB', 'Tablet', '6S', '200', 424.41, 360.75, 339.1, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(58, 1104, 1, 0, '00100058', '104', 'PYTEX 20MG TAB', 'Tablet', '20S', '200', 259.21, 220.33, 207.11, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(59, 1105, 1, 0, '00100047', '105', 'PYLOCLAR 250MG TAB', 'Tablet', '10S', '200', 350.65, 298.05, 280.17, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(60, 1106, 1, 0, '00100048', '106', 'PYLOCLAR 500MG TAB', 'Tablet', '10S', '200', 612.43, 520.57, 489.33, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(61, 1107, 1, 0, '00100020', '107', 'POLYMALT 100MG TAB', 'Tablet', '20S', '200', 251.81, 214.04, 201.2, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(62, 1108, 1, 0, '00100022', '108', 'POLYMALT  SYP', 'Tablet', '120ML', '200', 197.01, 167.46, 157.41, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(63, 1109, 1, 0, '00100021', '109', 'POLYMALT F TAB', 'Tablet', '30S', '200', 334.76, 284.54, 267.47, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(64, 1110, 1, 0, '00100052', '110', 'PLATLO 75MG TAB', 'Tablet', '14S', '200', 265.1, 225.33, 211.81, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(65, 1114, 1, 0, '00100016', '114', 'QUPRON 250MG TAB', 'Tablet', '10S', '200', 159.03, 135.17, 125.71, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(66, 1115, 1, 0, '00100017', '115', 'QUPRON 500MG TAB', 'Tablet', '10S', '200', 220, 187, 174, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(67, 1123, 1, 0, '00100055', '123', 'REMETHAN 50MG TAB', 'Tablet', '20S', '200', 120, 102, 95.88, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(68, 1124, 1, 0, '00100056', '124', 'REMETHAN 100R TAB', 'Tablet', '30S', '200', 360, 306, 287.64, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(69, 1118, 1, 0, '00100059', '118', 'QSARTAN 25MG TAB', 'Tablet', '20S', '200', 158.48, 134.71, 126.62, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(70, 1119, 1, 0, '00100060', '119', 'QSARTAN 50MG TAB', 'Tablet', '20S', '200', 279.05, 237.19, 222.96, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(71, 1120, 1, 0, '00100061', '120', 'QSARTAN PLUS 50/12.5MG TAB', 'Tablet', '20S', '200', 444.35, 377.7, 355.04, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(72, 1121, 1, 0, '00100080', '121', 'Q-BACT 1GM INJ', 'Tablet', 'VIAL', '200', 370.31, 314.76, 295.87, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(73, 1122, 1, 0, '00100081', '122', 'Q-BACT 2GM INJ', 'Tablet', 'VIAL', '200', 518.43, 440.67, 414.23, 8, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(74, 4126, 4, 0, '00200006', '126', 'ROXVITA TAB', 'Tablet', '30S', '200', 480, 408, 383.52, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(75, 1127, 1, 0, '00100035', '127', 'REMENT 10MG TAB', 'Tablet', '30S', '200', 870, 739.5, 695.13, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(76, 1128, 1, 0, '00100036', '128', 'REMENT 20MG TAB', 'Tablet', '10S', '200', 472, 401.2, 377.13, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(77, 1129, 1, 0, '00100001', '129', 'RULING 20MG CAP', 'Tablet', '14S', '200', 198.18, 168.46, 158.35, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(78, 1130, 1, 0, '00100002', '130', 'RULING 40MG CAP', 'Tablet', '14S', '200', 344, 292.4, 274.86, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(79, 1131, 1, 0, '00100004', '131', 'RULING INJ 40MG', 'Tablet', 'VIAL', '200', 357.82, 304.15, 285.9, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(80, 1132, 1, 0, '00100003', '132', 'RULING+ SACHET 40MG', 'Tablet', '10S', '200', 269.58, 229.14, 215.4, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(81, 1137, 1, 0, '00100042', '137', 'SULVORID 25MG TAB', 'Tablet', '20S', '200', 225.15, 191.38, 179.89, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(82, 1138, 1, 0, '00100043', '138', 'SULVORID 50MG TAB', 'Tablet', '20S', '200', 374.75, 318.54, 299.43, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(83, 1139, 1, 0, '00100084', '139', 'SUNVIT INJ', 'Tablet', '1S', '200', 187.5, 151.73, 142.62, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(84, 1140, 1, 0, '00100041', '140', 'STOMACOL 10MG TAB', 'Tablet', '50S', '200', 314.1, 266.99, 250.97, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(85, 1141, 1, 0, '00100038', '141', 'STAT A 10MG TAB', 'Tablet', '14S', '200', 252, 214.2, 201.35, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(86, 1142, 1, 0, '00100039', '142', 'STAT A 20MG TAB', 'Tablet', '14S', '200', 340, 289, 271.66, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(87, 1143, 1, 0, '00100040', '143', 'STAT A 40MG TAB', 'Tablet', '14S', '200', 416.5, 354, 325, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(88, 1147, 1, 0, '00100033', '147', 'UFRIM 10MG TAB', 'Tablet', '14S', '200', 312, 265.2, 249.28, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(89, 1148, 1, 0, '00100034', '148', 'UFRIM 20MG TAB', 'Tablet', '14S', '200', 380, 323, 303.62, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(90, 1452, 1, 0, '', '452', 'VILDOS 50MG TAB', 'Tablet', '28S', '200', 672, 571.25, 525.55, 8, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(91, 1153, 1, 0, '00100031', '153', 'VILDOMET 50/850MG TAB', 'Tablet', '14S', '200', 396, 336.6, 316.4, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(92, 1154, 1, 0, '00100032', '154', 'VILDOMET 50/1000MG TAB', 'Tablet', '14S', '200', 396, 336.6, 316.4, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(93, 1155, 1, 0, '', '155', 'VITA JUNIOR* SACHET', 'Tablet', '10S', '200', 222, 189, 174, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(94, 1160, 1, 0, '00100026', '160', 'ZEENARYL 1MG TAB', 'Tablet', '20S', '200', 120, 102, 95.88, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(95, 1161, 1, 0, '00100027', '161', 'ZEENARYL 2MG TAB', 'Tablet', '20S', '200', 184, 156.4, 147.01, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(96, 1162, 1, 0, '00100028', '162', 'ZEENARYL 3MG TAB', 'Tablet', '20S', '200', 253, 215.05, 202.15, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(97, 1163, 1, 0, '00100029', '163', 'ZEENARYL 4MG TAB', 'Tablet', '20S', '200', 360, 306, 287.64, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(98, 1151, 1, 0, '00100030', '151', 'VILDOS 50MG TAB', 'Tablet', '28S', '200', 672, 571.2, 536.93, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(99, 114, 1, 0, '', '14', 'CQ10 50MG CAP', 'Tablet', '30S', '200', 1192, 1014, 932, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(100, 4103, 4, 0, '00200015', '103', 'PROHALE TAB', 'Tablet', '30S', '200', 1860, 1581, 1470.33, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(101, 1125, 1, 0, '00100057', '125', 'REMETHAN INJ 75MG', 'Tablet', '10S', '200', 342, 290.7, 273.25, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(102, 490, 4, 0, '00200010', '90', 'Maxfol 600mcg Tab', 'Tablet', '30S', '200', 342, 290.7, 273.25, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(103, 143, 1, 0, '00100085', '43', 'ENTUM 80MG INJ', 'Tablet', '6S', '200', 864, 734.4, 690.33, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(104, 196, 1, 0, '00100086', '96', 'ONAMED 150MG CAP', 'Tablet', '1S', '200', 221.35, 188.15, 176.86, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(105, 175, 1, 0, '', '75', 'KETOLAC 30MG INJ', 'Tablet', '5S', '200', 430, 365.5, 337, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(106, 125, 1, 0, '00100089', '25', 'DAYZONE 1G INJ (IV)', 'Tablet', 'VIAL', '200', 280, 238, 223.72, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(107, 1144, 1, 0, '', '144', 'SOFONIL 400MG TAB', 'Tablet', '28S', '200', 5868, 4000, 3800, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(108, 150, 1, 0, '00100091', '50', 'FORTIUS 5MG TAB', 'Tablet', '10S', '200', 144, 122.4, 115.06, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(109, 151, 1, 0, '00100092', '51', 'FORTIUS 10MG TAB', 'Tablet', '10S', '200', 240, 204, 191.76, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(110, 152, 1, 0, '00100093', '52', 'FORTIUS 20MG TAB', 'Tablet', '10S', '200', 412, 350.2, 329.18, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(111, 1145, 1, 0, '00100090', '145', 'SOLOSIN SR 0.4MG CAP', 'Tablet', '20S', '200', 782, 664.7, 624.81, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(112, 422, 4, 0, '00200012', '22', 'CRANTOP SACHET', 'Tablet', '10S', '200', 360, 306, 287.64, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(113, 124, 1, 0, '', '24', 'DACLAHEP-60 TAB', 'Tablet', '28S', '200', 4552, 4000, 3869, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(114, 1156, 1, 0, '00100098', '156', 'VANCOTIC 1G INJ', 'Tablet', 'VIAL', '200', 1564.63, 1329.94, 1249.64, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(115, 1157, 1, 0, '00100099', '157', 'VINZAT 2.25G IV INJ', 'Tablet', 'VIAL', '200', 474.4, 403.24, 379.04, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(116, 1158, 1, 0, '00100100', '158', 'VINZAT 4.5G IV INJ', 'Tablet', 'VIAL', '200', 976.7, 830.2, 780.39, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(117, 413, 4, 0, '00200002', '13', 'Caldree DS Tab', 'Tablet', '30S', '200', 960, 816, 767.04, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(118, 4101, 4, 0, '00200013', '101', 'OSLIA SOFTGEL', 'Tablet', '1S', '200', 240, 204, 191.76, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(119, 4159, 4, 0, '00200014', '159', 'VIDA-Z TAB', 'Tablet', '30S', '200', 592, 503.2, 473, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(120, 210, 2, 0, '', '10', 'CLOLAM 500MG VIGINAL TAB', 'Tablet', '1S', '200', 73.6, 62.56, 56.3, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(121, 211, 2, 0, '', '11', 'CALTIS 125MG DRY SUSP', 'Tablet', '60ML', '200', 256.45, 217.98, 196.18, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(122, 212, 2, 0, '', '12', 'CALTIS 250MG DRY SUSP', 'Tablet', '60ML', '200', 423.2, 359.72, 323.75, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(123, 219, 2, 0, '', '19', 'ESPHARM 20MG CAP', 'Tablet', '14S', '200', 136.85, 116.32, 104.69, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(124, 220, 2, 0, '', '20', 'ESPHARM 40MG CAP', 'Tablet', '14S', '200', 227.7, 193.55, 174.19, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(125, 221, 2, 0, '', '21', 'ERCOPD 150MG CAP', 'Tablet', '20S', '200', 155.25, 131.96, 118.77, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(126, 222, 2, 0, '', '22', 'ERCOPD 175MG DRY SUSP', 'Tablet', '100ML', '200', 155.25, 131.96, 118.77, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(127, 231, 2, 0, '', '31', 'HIPOUR 40MG TAB', 'Tablet', '20S', '200', 360, 306, 275.4, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(128, 232, 2, 0, '', '32', 'HIPOUR 80MG TAB', 'Tablet', '20S', '200', 680, 578, 520.2, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(129, 240, 2, 0, '', '40', 'ISFLO 150MG CAP', 'Tablet', '1S', '200', 158.7, 134.9, 121.41, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(130, 248, 2, 0, '', '48', 'LUFTHER 15/90MG DRY SUSP', 'Tablet', '1S', '200', 103.5, 87.97, 79.18, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(131, 255, 2, 0, '', '55', 'MOZOLE 20MG CAP', 'Tablet', '14S', '200', 165.6, 140.76, 126.68, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(132, 256, 2, 0, '', '56', 'MOZOLE 40MG CAP', 'Tablet', '14S', '200', 287.5, 244.38, 219.94, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(133, 257, 2, 0, '', '57', 'MENOP 2GM SACHETS', 'Tablet', '7S', '200', 885.5, 752.68, 677.41, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(134, 258, 2, 0, '', '58', 'MOLAM 400MG TAB', 'Tablet', '5S', '200', 546.25, 464.31, 417.88, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(135, 265, 2, 0, '', '65', 'PITRIN 20MG TAB', 'Tablet', '20S', '200', 201.25, 171.06, 153.96, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(136, 266, 2, 0, '', '66', 'PRIDET 50MG TAB', 'Tablet', '10S', '200', 207, 175.95, 158.36, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(137, 280, 2, 0, '', '80', 'TAMIS 0.4MG CAP', 'Tablet', '10S', '200', 601.45, 511.23, 460.11, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(138, 281, 2, 0, '', '81', 'TRANIC 250MG CAP', 'Tablet', '100S', '200', 747.5, 635.38, 571.84, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(139, 282, 2, 0, '', '82', 'TRANIC 500MG CAP', 'Tablet', '20S', '200', 287.5, 244.38, 219.94, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(140, 310, 3, 0, '', '10', 'BROPHYL SYP', 'Tablet', '120ML', '200', 67, 56.95, 56.95, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(141, 311, 3, 0, '', '11', 'BROPHYL D SYP', 'Tablet', '120ML', '200', 65, 55.25, 55.25, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(142, 313, 3, 0, '', '13', 'BENDAZOLE SUSP', 'Tablet', '10ML', '200', 26.8, 22.78, 22.78, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(143, 314, 3, 0, '', '14', 'BROXOL SYP', 'Tablet', '120ML', '200', 50, 42.5, 42.5, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(144, 315, 3, 0, '', '15', 'BROXOL DM SYP', 'Tablet', '120ML', '200', 60, 51, 51, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(145, 320, 3, 0, '', '20', 'CALSID SYP', 'Tablet', '120ML', '200', 69, 58.65, 58.65, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(146, 329, 3, 0, '', '29', 'DYMIN 50MG TAB', 'Tablet', '100S', '200', 100.28, 85.24, 85.24, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(147, 330, 3, 0, '', '30', 'DYMIN SYP', 'Tablet', '60ML', '200', 31.61, 26.87, 26.87, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(148, 341, 3, 0, '', '41', 'FENBRO 100MG SYP', 'Tablet', '90ML', '200', 46.95, 39.91, 39.91, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(149, 342, 3, 0, '', '42', 'FENBRO 8PLUS SYP', 'Tablet', '90ML', '200', 73, 62.05, 62.05, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(150, 344, 3, 0, '', '44', 'FURAMID SYP', 'Tablet', '60ML', '200', 42, 35.7, 35.7, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(151, 346, 3, 0, '', '46', 'FERVIT LIQ.', 'Tablet', '120ML', '200', 58.5, 49.73, 49.73, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(152, 355, 3, 0, '', '55', 'HEX 50MG TAB', 'Tablet', '10S', '200', 200, 170, 170, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(153, 370, 3, 0, '', '70', 'KAMIC 250MG TAB', 'Tablet', '200S', '200', 300, 255, 255, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(154, 371, 3, 0, '', '71', 'KAMIC FORTE TAB', 'Tablet', '100S', '200', 264, 224.4, 224.4, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(155, 380, 3, 0, '', '80', 'MANACID SYP', 'Tablet', '120ML', '200', 46, 39.1, 39.1, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(156, 381, 3, 0, '', '81', 'MANACID FORTE SYP', 'Tablet', '120ML', '200', 65, 55.25, 55.25, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(157, 390, 3, 0, '', '90', 'NAXID 250MG SUSP', 'Tablet', '60ML', '200', 80, 68, 68, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(158, 3100, 3, 0, '', '100', 'PEDROL 500MG TAB', 'Tablet', '200S', '200', 264.71, 225, 225, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(159, 3102, 3, 0, '', '102', 'PEDROL SYP', 'Tablet', '60ML', '200', 35, 29.75, 29.75, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(160, 3103, 3, 0, '', '103', 'PEDROL FORTE 60ML SYP', 'Tablet', '60ML', '200', 44, 37.4, 37.4, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(161, 3104, 3, 0, '', '104', 'PEDROL FORTE 90ML SYP', 'Tablet', '90ML', '200', 60, 51, 51, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(162, 3105, 3, 0, '', '105', 'PEDROL TOTAL SYP', 'Tablet', '60ML', '200', 46, 39.1, 39.1, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(163, 3106, 3, 0, '', '106', 'PEDROL DROPS', 'Tablet', '20ML', '200', 40, 34, 34, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(164, 3107, 3, 0, '', '107', 'PERL 20MG CAP', 'Tablet', '14S', '200', 168, 142.8, 142.8, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(165, 3120, 3, 0, '', '120', 'RIDOX 100MG CAP', 'Tablet', '100S', '200', 300, 255, 255, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(166, 3121, 3, 0, '', '121', 'ROZID 250MG TAB', 'Tablet', '10S', '200', 100, 85, 85, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(167, 3122, 3, 0, '', '122', 'ROZID 500MG TAB', 'Tablet', '10S', '200', 190, 161.5, 161.5, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(168, 3124, 3, 0, '', '124', 'RIAM SYP', 'Tablet', '90ML', '200', 60, 51, 51, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(169, 3126, 3, 0, '', '126', 'RID-ALL FORTE TAB', 'Tablet', '30S', '200', 136.85, 116.32, 116.32, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(170, 3127, 3, 0, '', '127', 'RINTAL SYP', 'Tablet', '120ML', '200', 48.5, 41.23, 41.23, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(171, 3135, 3, 0, '', '135', 'SANID 100MG TAB', 'Tablet', '30S', '200', 213, 181.05, 181.05, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(172, 3145, 3, 0, '', '145', 'VERIL 500MCG TAB', 'Tablet', '20S', '200', 130, 110.5, 110.5, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(173, 3146, 3, 0, '', '146', 'VASDEX-L SYP', 'Tablet', '120ML', '200', 80, 68, 68, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(174, 3165, 3, 0, '', '165', 'ZOLT TAB', 'Tablet', '30S', '200', 74, 62.9, 62.9, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(175, 3166, 3, 0, '', '166', 'ZOLT DS TAB', 'Tablet', '15S', '200', 90, 76.5, 76.5, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(176, 3167, 3, 0, '', '167', 'ZOLT SUSP', 'Tablet', '90ML', '200', 65, 55.25, 55.25, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(177, 3136, 3, 0, '', '136', 'STENZAR SYRUP', 'Tablet', '120ML', '200', 45, 38.25, 38.25, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(178, 312, 3, 0, '', '12', 'BENDAZOL TABLET', 'Tablet', '2S', '200', 20.9, 17.77, 17.77, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(179, 343, 3, 0, '', '43', 'FERFOLIC CAPSULES', 'Tablet', '30S', '200', 45, 38.25, 38.25, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(180, 3101, 3, 0, '', '101', 'PEDROL EXTRA TABLET', 'Tablet', '100S', '200', 150, 127.5, 127.5, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(181, 345, 3, 0, '', '45', 'FERVIT CAPSULES', 'Tablet', '30S', '200', 99, 84.15, 84.15, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(182, 3123, 3, 0, '', '123', 'RIAM TABLET', 'Tablet', '100S', '200', 200, 170, 170, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(183, 382, 3, 0, '', '82', 'MULTONIC SYRUP', 'Tablet', '120ML', '200', 60, 51, 51, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(184, 3125, 3, 0, '', '125', 'RID-ALL', 'Tablet', '100S', '200', 350, 297.5, 297.5, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(185, 24, 2, 0, '', '4', 'ANTEM TABLET', 'Tablet', '30S', '200', 158.7, 134.9, 121.41, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(186, 246, 2, 0, '', '46', 'LAMCIT 250MG TAB', 'Tablet', '10S', '200', 149.5, 127.08, 114.37, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(187, 247, 2, 0, '', '47', 'LAMCIT 500MG TAB', 'Tablet', '10S', '200', 247.25, 210.16, 189.15, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(188, 230, 2, 0, '', '30', 'HEMIFOL TAB', 'Tablet', '20S', '200', 109.25, 92.86, 83.58, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(189, 249, 2, 0, '', '49', 'LAMLOX 250MG TAB', 'Tablet', '10S', '200', 180, 153, 137.7, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(190, 250, 2, 0, '', '50', 'LAMLOX 500MG TAB', 'Tablet', '10S', '200', 310, 263.5, 237.15, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(191, 283, 2, 0, '', '83', 'TRAPAM 50MG  CAP', 'Tablet', '10S', '200', 120.75, 102.64, 92.37, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(192, 182, 1, 0, '00100125', '82', 'LETOCOR 2.5MG TAB', 'Tablet', '10S', '200', 2390, 2031.5, 1909.61, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(193, 191, 1, 0, '00100126', '91', 'MONAK INJ 30MG', 'Tablet', '5S', '200', 637, 541.45, 508.91, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(194, 163, 1, 0, '00100134', '63', 'ITOPER 50MG TAB', 'Tablet', '10S', '200', 225.81, 191.94, 180.42, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(195, 4150, 4, 0, '00200017', '150', 'VIDACAL C SACHET', 'Tablet', '10S', '200', 380, 323, 303.62, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(196, 1152, 1, 0, '00100135', '152', 'VILDOMET 50/500mg Tab', 'Tablet', '14S', '200', 396, 336.6, 316.4, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(197, 1164, 1, 0, '00100136', '164', 'ZYTO 250MG TAB', 'Tablet', '6S', '200', 222.19, 188.87, 177.53, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(198, 1165, 1, 0, '00100137', '165', 'ZYTO 500MG TAB', 'Tablet', '6S', '200', 407.34, 346.24, 325.46, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(199, 1166, 1, 0, '00100138', '166', 'ZYTO SUSP 200MG', 'Tablet', '1S', '200', 230.22, 195.69, 183.94, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(200, 1167, 1, 0, '00100143', '167', 'X-LOX 400MG/250ML I.V.', 'Tablet', 'VIAL', '200', 1200, 1020, 958.8, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(201, 122, 1, 0, '00100101', '22', 'COLIATE INJ', 'Tablet', '1S', '200', 1600, 1360, 1278.4, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(202, 1133, 1, 0, '00100139', '133', 'RETERIC 50MG CAP', 'Tablet', '14S', '200', 284, 241.4, 226.91, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(203, 1134, 1, 0, '00100140', '134', 'RETERIC 75MG CAP', 'Tablet', '14S', '200', 321.31, 273.12, 256.73, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(204, 1135, 1, 0, '00100141', '135', 'RETERIC 100MG CAP', 'Tablet', '14S', '200', 392.03, 333.23, 313.23, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(205, 1136, 1, 0, '00100142', '136', 'RETERIC 150mg CAP', 'Tablet', '14S', '200', 480, 408, 383.52, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(206, 137, 1, 0, '00100144', '37', 'DAYLINE 2G INJ', 'Tablet', '1S', '200', 468, 397.8, 373.93, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(207, 138, 1, 0, '00100145', '38', 'EMPAGLI 10MG TAB', 'Tablet', '14S', '200', 280, 238, 223.72, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(208, 139, 1, 0, '00100146', '39', 'EMPAGLI 25MG TAB', 'Tablet', '14S', '200', 398, 338.3, 318, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(209, 484, 4, 0, '', '84', 'MEDITON CAP', 'Tablet', '30S', '200', 1600, 1360, 1278.4, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM'),
(210, 144, 1, 0, '', '44', 'EBAK 10MG TAB', 'Tablet', '', '200', 140.72, 119.61, 112.43, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM');

-- --------------------------------------------------------

--
-- Table structure for table `product_overall_record`
--

DROP TABLE IF EXISTS `product_overall_record`;
CREATE TABLE IF NOT EXISTS `product_overall_record` (
  `overall_record_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_table_id` int(11) NOT NULL,
  `prod_name` varchar(100) NOT NULL,
  `prod_batch` varchar(50) NOT NULL,
  `quantity_sold` int(11) NOT NULL,
  `quantity_return` int(11) NOT NULL,
  `trade_rate` float NOT NULL,
  `purch_rate` float NOT NULL,
  `retail_rate` float NOT NULL,
  `expired_quantity` int(11) NOT NULL,
  `damaged_quantity` int(11) NOT NULL,
  `prod_status` varchar(50) NOT NULL,
  PRIMARY KEY (`overall_record_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_info`
--

DROP TABLE IF EXISTS `purchase_info`;
CREATE TABLE IF NOT EXISTS `purchase_info` (
  `purchase_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `comp_id` int(10) NOT NULL,
  `invoice_num` varchar(11) NOT NULL,
  `supplier_id` int(11) UNSIGNED NOT NULL,
  `purchase_date` varchar(20) NOT NULL,
  `gross_amount` float NOT NULL,
  `disc_amount` float NOT NULL,
  `net_amount` float NOT NULL,
  `entry_date` varchar(20) NOT NULL,
  `entry_time` varchar(20) NOT NULL,
  `update_date` varchar(20) NOT NULL,
  `update_time` varchar(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`purchase_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_info_detail`
--

DROP TABLE IF EXISTS `purchase_info_detail`;
CREATE TABLE IF NOT EXISTS `purchase_info_detail` (
  `purchase_info_detail_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `invoice_num` varchar(15) NOT NULL,
  `purchase_id` int(10) NOT NULL,
  `prod_id` int(11) NOT NULL,
  `discount` float NOT NULL,
  `bonus_quant` int(11) NOT NULL,
  `recieve_quant` int(11) NOT NULL,
  `batch_no` varchar(20) NOT NULL,
  `expiry_date` varchar(20) NOT NULL,
  `gross_amount` float NOT NULL,
  `disc_amount` float NOT NULL,
  `net_amount` float NOT NULL,
  `invoice_date` varchar(20) NOT NULL,
  `returnBit` tinyint(1) NOT NULL,
  `returned_quant` int(11) NOT NULL,
  `returned_bonus_quant` int(11) NOT NULL,
  PRIMARY KEY (`purchase_info_detail_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order_detail`
--

DROP TABLE IF EXISTS `purchase_order_detail`;
CREATE TABLE IF NOT EXISTS `purchase_order_detail` (
  `order_detail_tableID` int(11) NOT NULL AUTO_INCREMENT,
  `product_table_id` int(11) NOT NULL,
  `quantity_sold` int(11) NOT NULL,
  `quantity_instock` int(11) NOT NULL,
  `quantity_ordered` int(11) NOT NULL,
  `purch_order_id` int(11) NOT NULL,
  `net_amount` float NOT NULL,
  PRIMARY KEY (`order_detail_tableID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order_info`
--

DROP TABLE IF EXISTS `purchase_order_info`;
CREATE TABLE IF NOT EXISTS `purchase_order_info` (
  `purch_order_table_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `purch_order_id` int(11) NOT NULL,
  `purch_order_date` varchar(150) NOT NULL,
  `purch_order_amount` float NOT NULL,
  `numb_of_prod` int(11) NOT NULL,
  `inventoryDays` int(11) NOT NULL,
  `comp_table_id` int(11) NOT NULL,
  `order_status` varchar(100) NOT NULL,
  `creating_user_id` int(11) NOT NULL,
  `creating_date` varchar(30) NOT NULL,
  `creating_time` varchar(30) NOT NULL,
  `update_user_id` int(11) NOT NULL,
  `update_date` varchar(30) NOT NULL,
  `update_time` varchar(30) NOT NULL,
  PRIMARY KEY (`purch_order_table_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_return`
--

DROP TABLE IF EXISTS `purchase_return`;
CREATE TABLE IF NOT EXISTS `purchase_return` (
  `returnTable_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `return_id` int(11) NOT NULL,
  `purchInvoNumb` varchar(30) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `return_total_price` float NOT NULL,
  `return_gross_price` float NOT NULL,
  `entry_date` varchar(20) NOT NULL,
  `entry_time` varchar(20) NOT NULL,
  `update_date` varchar(20) NOT NULL,
  `update_time` varchar(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`returnTable_id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_return_detail`
--

DROP TABLE IF EXISTS `purchase_return_detail`;
CREATE TABLE IF NOT EXISTS `purchase_return_detail` (
  `return_detail_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `return_id` int(11) NOT NULL,
  `prod_id` int(11) NOT NULL,
  `prod_batch` varchar(15) NOT NULL,
  `prod_quant` int(11) NOT NULL,
  `bonus_quant` int(11) NOT NULL,
  `discount_amount` float NOT NULL,
  `total_amount` float NOT NULL,
  `unit` varchar(20) NOT NULL,
  `return_reason` varchar(100) NOT NULL,
  PRIMARY KEY (`return_detail_id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_return_info_detail`
--

DROP TABLE IF EXISTS `purchase_return_info_detail`;
CREATE TABLE IF NOT EXISTS `purchase_return_info_detail` (
  `return_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `purch_invo_num` varchar(20) NOT NULL,
  `return_date` varchar(20) NOT NULL,
  `prod_id` int(11) NOT NULL,
  `return_quant` int(11) NOT NULL,
  `discount_percent` float NOT NULL,
  `bonus_quantity` int(11) NOT NULL,
  `total_amount` float NOT NULL,
  `batch_no` varchar(20) NOT NULL,
  `gross_amount` float NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purch_return_info`
--

DROP TABLE IF EXISTS `purch_return_info`;
CREATE TABLE IF NOT EXISTS `purch_return_info` (
  `purch_return_no` int(11) NOT NULL,
  `purch_invo_num` varchar(20) NOT NULL,
  `return_date` varchar(20) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `total_amount` float NOT NULL,
  `discount_amount` float NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `salesman_designated_cities`
--

DROP TABLE IF EXISTS `salesman_designated_cities`;
CREATE TABLE IF NOT EXISTS `salesman_designated_cities` (
  `salesman_designated_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `salesman_table_id` int(10) UNSIGNED NOT NULL,
  `designated_city_id` int(10) UNSIGNED NOT NULL,
  `start_date` varchar(20) NOT NULL,
  `end_date` varchar(20) NOT NULL,
  `designated_status` varchar(50) NOT NULL,
  PRIMARY KEY (`salesman_designated_id`)
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sale_invoice_print_info`
--

DROP TABLE IF EXISTS `sale_invoice_print_info`;
CREATE TABLE IF NOT EXISTS `sale_invoice_print_info` (
  `sale_print_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `header_1` varchar(100) NOT NULL,
  `header_2` varchar(100) NOT NULL,
  `header_3` varchar(100) NOT NULL,
  `header_4` varchar(100) NOT NULL,
  `header_5` varchar(100) NOT NULL,
  `owner_ids` int(11) NOT NULL,
  `owner_name` varchar(50) NOT NULL,
  `father_name` varchar(50) NOT NULL,
  `owner_cnic` varchar(20) NOT NULL,
  `owner_country` varchar(50) NOT NULL,
  `business_name` varchar(100) NOT NULL,
  `business_address` varchar(250) NOT NULL,
  `business_city` varchar(150) NOT NULL,
  `footer_1` varchar(1500) NOT NULL,
  `footer_2` varchar(200) NOT NULL,
  `footer_3` varchar(200) NOT NULL,
  `footer_4` varchar(200) NOT NULL,
  `creating_user_id` int(11) NOT NULL,
  `creating_date` varchar(20) NOT NULL,
  `creating_time` varchar(20) NOT NULL,
  `update_user_id` int(11) NOT NULL,
  `update_date` varchar(20) NOT NULL,
  `update_time` varchar(20) NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`sale_print_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sale_invoice_print_info`
--

INSERT INTO `sale_invoice_print_info` (`sale_print_id`, `header_1`, `header_2`, `header_3`, `header_4`, `header_5`, `owner_ids`, `owner_name`, `father_name`, `owner_cnic`, `owner_country`, `business_name`, `business_address`, `business_city`, `footer_1`, `footer_2`, `footer_3`, `footer_4`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`, `status`) VALUES
(2, '[yguh]', '[yguh]', '[yguh]', '[yguh]', '[yguh]', 1, 'fyguhij', 'tfgyuhji', 'tfgyhujitfygvbhuj', 'tfgyhujtfgyhu', 'tfgyhu', 'tfgyuh', '[tyg]', 'tfgyhu', 'tfgyuh', 'tyg', 'tyg', 1, '28/Aug/2021', '12:08 PM', 1, '28/Aug/2021', '12:08 PM', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `subarea_info`
--

DROP TABLE IF EXISTS `subarea_info`;
CREATE TABLE IF NOT EXISTS `subarea_info` (
  `subarea_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `area_id` int(10) UNSIGNED NOT NULL,
  `sub_area_name` varchar(250) NOT NULL,
  `subarea_status` varchar(50) NOT NULL,
  `entry_date` varchar(20) NOT NULL,
  `entry_time` varchar(20) NOT NULL,
  `update_date` varchar(20) NOT NULL,
  `update_time` varchar(20) NOT NULL,
  `user_id` int(10) NOT NULL,
  PRIMARY KEY (`subarea_id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `supplier_company`
--

DROP TABLE IF EXISTS `supplier_company`;
CREATE TABLE IF NOT EXISTS `supplier_company` (
  `supplier_company_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `supplier_id` int(10) UNSIGNED NOT NULL,
  `company_id` int(10) UNSIGNED NOT NULL,
  `updated_date` varchar(20) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`supplier_company_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier_company`
--

INSERT INTO `supplier_company` (`supplier_company_id`, `supplier_id`, `company_id`, `updated_date`, `user_id`, `status`) VALUES
(3, 1, 1, '30/Nov/2021', 1, 'Active'),
(2, 2, 2, '29/Nov/2021', 1, 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `supplier_info`
--

DROP TABLE IF EXISTS `supplier_info`;
CREATE TABLE IF NOT EXISTS `supplier_info` (
  `supplier_table_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `supplier_id` int(10) UNSIGNED DEFAULT NULL,
  `supplier_name` varchar(100) NOT NULL,
  `supplier_email` varchar(100) NOT NULL,
  `supplier_contact` varchar(60) NOT NULL,
  `contact_person` varchar(200) NOT NULL,
  `supplier_address` varchar(200) NOT NULL,
  `supplier_city` varchar(200) NOT NULL,
  `supplier_status` varchar(20) NOT NULL,
  `creating_user_id` int(10) UNSIGNED NOT NULL,
  `creating_date` varchar(30) NOT NULL,
  `creating_time` varchar(30) NOT NULL,
  `update_user_id` int(10) UNSIGNED NOT NULL,
  `update_date` varchar(30) NOT NULL,
  `update_time` varchar(30) NOT NULL,
  PRIMARY KEY (`supplier_table_id`),
  UNIQUE KEY `supplier_id` (`supplier_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier_info`
--

INSERT INTO `supplier_info` (`supplier_table_id`, `supplier_id`, `supplier_name`, `supplier_email`, `supplier_contact`, `contact_person`, `supplier_address`, `supplier_city`, `supplier_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`) VALUES
(1, 1, 'High Q', '', '', '', 'Karachi', 'Karachi', 'Active', 1, '29/Nov/2021', '11:52 AM', 1, '30/Nov/2021', '11:50 AM'),
(2, 2, 'Vida Laboratories', '', '', '', '', '', 'Active', 1, '29/Nov/2021', '11:52 AM', 1, '29/Nov/2021', '11:52 AM');

-- --------------------------------------------------------

--
-- Table structure for table `sync_tracking`
--

DROP TABLE IF EXISTS `sync_tracking`;
CREATE TABLE IF NOT EXISTS `sync_tracking` (
  `data_id` int(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `data_name` varchar(30) NOT NULL,
  `last_modified` varchar(50) NOT NULL,
  PRIMARY KEY (`data_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `system_settings`
--

DROP TABLE IF EXISTS `system_settings`;
CREATE TABLE IF NOT EXISTS `system_settings` (
  `setting_id` int(10) NOT NULL AUTO_INCREMENT,
  `setting_name` varchar(50) NOT NULL,
  `setting_value` varchar(1500) NOT NULL,
  PRIMARY KEY (`setting_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `system_settings`
--

INSERT INTO `system_settings` (`setting_id`, `setting_name`, `setting_value`) VALUES
(1, 'batch_policy', 'Entry FIFO');

-- --------------------------------------------------------

--
-- Table structure for table `user_accounts`
--

DROP TABLE IF EXISTS `user_accounts`;
CREATE TABLE IF NOT EXISTS `user_accounts` (
  `account_id` int(10) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(100) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_password` varchar(300) NOT NULL,
  `user_rights` int(10) NOT NULL,
  `reg_date` varchar(50) NOT NULL,
  `current_status` varchar(30) NOT NULL,
  `user_status` varchar(50) NOT NULL,
  PRIMARY KEY (`account_id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_accounts`
--

INSERT INTO `user_accounts` (`account_id`, `user_name`, `user_id`, `user_password`, `user_rights`, `reg_date`, `current_status`, `user_status`) VALUES
(1, 'Saffi', 1, '123', 1, '10/Feb/2020', 'off', 'Approved');

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

DROP TABLE IF EXISTS `user_info`;
CREATE TABLE IF NOT EXISTS `user_info` (
  `user_table_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `user_name` varchar(200) NOT NULL,
  `user_contact` varchar(60) NOT NULL,
  `user_address` varchar(250) NOT NULL,
  `user_cnic` varchar(50) NOT NULL,
  `user_type` varchar(20) NOT NULL,
  `user_image` varchar(300) NOT NULL,
  `user_status` varchar(50) NOT NULL,
  `creating_user_id` int(10) UNSIGNED NOT NULL,
  `creating_date` varchar(20) NOT NULL,
  `creating_time` varchar(20) NOT NULL,
  `update_user_id` int(10) UNSIGNED NOT NULL,
  `update_date` varchar(20) NOT NULL,
  `update_time` varchar(20) NOT NULL,
  PRIMARY KEY (`user_table_id`),
  UNIQUE KEY `salesman_id` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` (`user_table_id`, `user_id`, `user_name`, `user_contact`, `user_address`, `user_cnic`, `user_type`, `user_image`, `user_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`) VALUES
(1, 2, 'Saffi', '03526555', 'Gujrat', '34201-5525252-5', 'Admin', '', 'Active', 1, '08/Feb/2021', '03:35 PM', 1, '08/Feb/2021', '03:35 PM');

-- --------------------------------------------------------

--
-- Table structure for table `user_rights`
--

DROP TABLE IF EXISTS `user_rights`;
CREATE TABLE IF NOT EXISTS `user_rights` (
  `rights_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(20) NOT NULL,
  `create_record` tinyint(4) NOT NULL,
  `read_record` tinyint(4) NOT NULL,
  `edit_record` tinyint(4) NOT NULL,
  `delete_record` tinyint(4) NOT NULL,
  `manage_stock` tinyint(4) NOT NULL,
  `manage_cash` tinyint(4) NOT NULL,
  `manage_settings` tinyint(4) NOT NULL,
  `mobile_app` tinyint(4) NOT NULL,
  PRIMARY KEY (`rights_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
