-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 16, 2022 at 05:12 PM
-- Server version: 10.5.12-MariaDB
-- PHP Version: 7.3.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id18297883_profit_flow`
--

-- --------------------------------------------------------

--
-- Table structure for table `area_info`
--

CREATE TABLE `area_info` (
  `area_table_id` int(10) UNSIGNED NOT NULL,
  `software_area_id` int(10) UNSIGNED NOT NULL,
  `area_id` int(10) UNSIGNED DEFAULT NULL,
  `area_name` varchar(250) NOT NULL,
  `area_abbrev` varchar(250) NOT NULL,
  `city_table_id` int(10) UNSIGNED NOT NULL,
  `area_status` varchar(50) NOT NULL,
  `creating_user_id` int(10) UNSIGNED NOT NULL,
  `creating_date` varchar(20) NOT NULL,
  `creating_time` varchar(20) NOT NULL,
  `update_user_id` int(10) UNSIGNED NOT NULL,
  `update_date` varchar(20) NOT NULL,
  `update_time` varchar(20) NOT NULL,
  `software_lic_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `area_info`
--

INSERT INTO `area_info` (`area_table_id`, `software_area_id`, `area_id`, `area_name`, `area_abbrev`, `city_table_id`, `area_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`, `software_lic_id`) VALUES
(20, 2, 0, 'Test 1', 'Test 1', 0, 'Active', 1, '27/Jan/2022', '12:17 AM', 1, '27/Feb/2022', '12:17 AM', 1),
(31, 1, 1, 'GJT-1', 'GJT-1', 1, 'Active', 1, '15/Feb/2022', '10:59 AM', 1, '16/Feb/2022', '10:59 AM', 1),
(33, 3, 9, 'GJT-9', 'GJT-9', 1, 'Active', 1, '16/Feb/2022', '10:59 AM', 1, '16/Feb/2022', '10:59 AM', 1),
(34, 4, 7, 'GJT-7', 'GJT-7', 1, 'Active', 1, '16/Feb/2022', '10:59 AM', 1, '16/Feb/2022', '10:59 AM', 1),
(35, 5, 11, 'Kharian City', 'Kharian City', 2, 'Active', 1, '16/Feb/2022', '10:59 AM', 1, '16/Feb/2022', '10:59 AM', 1),
(36, 6, 19, 'Kharian Cantt', 'Kharian Cantt', 2, 'Active', 1, '16/Feb/2022', '10:59 AM', 1, '16/Feb/2022', '10:59 AM', 1),
(37, 7, 18, 'Gulyana', 'Gulyana', 2, 'Active', 1, '16/Feb/2022', '10:59 AM', 1, '16/Feb/2022', '10:59 AM', 1),
(38, 8, 10, 'Lalamusa City', 'Lalamusa City', 3, 'Active', 1, '16/Feb/2022', '10:59 AM', 1, '16/Feb/2022', '10:59 AM', 1),
(39, 9, 13, 'Kotla City', 'Kotla City', 6, 'Active', 1, '16/Feb/2022', '10:59 AM', 1, '16/Feb/2022', '10:59 AM', 1);

-- --------------------------------------------------------

--
-- Table structure for table `batchwise_stock`
--

CREATE TABLE `batchwise_stock` (
  `batch_stock_id` int(10) NOT NULL,
  `software_batch_stock_id` int(10) UNSIGNED DEFAULT NULL,
  `product_table_id` int(11) NOT NULL,
  `batch_no` varchar(50) NOT NULL,
  `quantity` int(11) NOT NULL,
  `bonus` int(11) NOT NULL,
  `retail_price` float NOT NULL,
  `trade_price` float NOT NULL,
  `purchase_price` float NOT NULL,
  `batch_expiry` varchar(20) NOT NULL,
  `entry_date` varchar(20) NOT NULL,
  `entry_time` varchar(15) NOT NULL,
  `update_date` varchar(20) DEFAULT NULL,
  `update_time` varchar(20) DEFAULT NULL,
  `software_lic_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `batchwise_stock`
--

INSERT INTO `batchwise_stock` (`batch_stock_id`, `software_batch_stock_id`, `product_table_id`, `batch_no`, `quantity`, `bonus`, `retail_price`, `trade_price`, `purchase_price`, `batch_expiry`, `entry_date`, `entry_time`, `update_date`, `update_time`, `software_lic_id`) VALUES
(1, 1, 4, '5AQ254', 97, 0, 123.86, 105.28, 98.97, '23/Feb/2022', '23/Feb/2022', '12:33:02.295', NULL, NULL, 1),
(2, 2, 4, '5AQ215', 500, 0, 123.86, 105.28, 98.97, '23/Feb/2022', '23/Feb/2022', '12:38:18.477', NULL, NULL, 1),
(3, 3, 5, '1AQ264', 699, 0, 211.85, 180.07, 169.26, '26/Feb/2022', '26/Feb/2022', '12:46:56.887', NULL, NULL, 1),
(4, 4, 109, '1FT265', 2000, 0, 240, 204, 191.76, '27/Feb/2022', '27/Feb/2022', '11:02:32.204', NULL, NULL, 1),
(5, 5, 110, '2FT365', 600, 0, 412, 350.2, 329.18, '27/Feb/2022', '27/Feb/2022', '11:06:12.813', NULL, NULL, 1),
(6, 6, 109, '1FT264', 900, 0, 240, 204, 191.76, '27/Feb/2022', '27/Feb/2022', '11:45:40.235', NULL, NULL, 1),
(7, 7, 109, '1FT977', 900, 0, 240, 204, 191.76, '27/Feb/2022', '27/Feb/2022', '11:48:16.275', NULL, NULL, 1),
(8, 8, 111, 'BLI-512', 240, 0, 660, 561, 527.34, '07/Mar/2022', '07/Mar/2022', '11:26:00.225', NULL, NULL, 1),
(9, 9, 111, 'BLI-0321', 240, 0, 660, 561, 527.34, '07/Mar/2022', '07/Mar/2022', '11:31:52.100', NULL, NULL, 1),
(10, 10, 117, '2514', 20, 0, 296, 252, 232, '07/Mar/2022', '07/Mar/2022', '11:34:35.691', NULL, NULL, 1),
(11, 11, 119, '2CY264', 1000, 0, 199.6, 169.66, 159.48, '07/Mar/2022', '07/Mar/2022', '11:37:11.456', NULL, NULL, 1),
(12, 12, 120, '5CY264', 1000, 0, 385.9, 328, 308.32, '07/Mar/2022', '07/Mar/2022', '11:51:24.474', NULL, NULL, 1),
(13, 13, 13, '2CY854', 1000, 0, 1000, 850, 790.5, '07/Mar/2022', '07/Mar/2022', '11:59:47.118', NULL, NULL, 1),
(14, 14, 11, '5210', 30, 0, 2586.24, 2198.3, 2066.4, '07/Mar/2022', '07/Mar/2022', '12:38:38.974', NULL, NULL, 1),
(15, 15, 13, '2CY145', 100, 0, 1000, 850, 790.5, '07/Mar/2022', '07/Mar/2022', '12:40:41.379', NULL, NULL, 1),
(16, 16, 14, '5CY214', 198, 0, 123.86, 105.28, 98.97, '07/Mar/2022', '07/Mar/2022', '12:40:48.802', NULL, NULL, 1),
(17, 17, 13, '2CY456', 1000, 0, 1000, 850, 790.5, '07/Mar/2022', '07/Mar/2022', '12:45:44.796', NULL, NULL, 1),
(18, 18, 11, '1224', 20, 0, 2586.24, 2198.3, 2066.4, '07/Mar/2022', '07/Mar/2022', '12:51:47.705', NULL, NULL, 1),
(19, 19, 14, '5CY26', 200, 0, 123.86, 105.28, 98.97, '07/Mar/2022', '07/Mar/2022', '12:51:51.005', NULL, NULL, 1),
(20, 20, 106, '1DX361', 1000, 0, 280, 238, 223.72, '07/Mar/2022', '07/Mar/2022', '01:01:12.860', NULL, NULL, 1),
(21, 21, 18, '1DL264', 1000, 0, 346, 294.1, 276.45, '07/Mar/2022', '07/Mar/2022', '01:01:13.064', NULL, NULL, 1),
(22, 22, 20, '5DY255', 400, 0, 218.64, 185.84, 174.69, '08/Mar/2022', '08/Mar/2022', '12:06:04.972', NULL, NULL, 1),
(23, 23, 109, '1FT251', 1000, 0, 240, 204, 191.76, '11/Mar/2022', '11/Mar/2022', '06:34:58.944', NULL, NULL, 1),
(24, 24, 38, 'HFR-362', 400, 0, 1680, 1428, 1342.32, '11/Mar/2022', '11/Mar/2022', '06:34:59.093', NULL, NULL, 1),
(25, 25, 109, '', 1000, 0, 240, 204, 191.76, '15/Mar/2022', '15/Mar/2022', '11:14:51.323', NULL, NULL, 1),
(26, 26, 108, '', 1000, 0, 144, 122.4, 115.06, '15/Mar/2022', '15/Mar/2022', '11:14:51.417', NULL, NULL, 1),
(27, 27, 36, '', 200, 0, 125.55, 112, 100, '15/Mar/2022', '15/Mar/2022', '11:14:51.517', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `bonus_policy`
--

CREATE TABLE `bonus_policy` (
  `bonus_policyID` int(11) NOT NULL,
  `software_bonus_id` int(10) UNSIGNED NOT NULL,
  `approval_id` varchar(30) NOT NULL,
  `dealer_table_id` int(11) DEFAULT NULL,
  `company_table_id` int(11) UNSIGNED DEFAULT NULL,
  `product_table_id` int(11) UNSIGNED NOT NULL,
  `quantity` int(11) NOT NULL,
  `bonus_quant` int(11) NOT NULL,
  `start_date` varchar(50) NOT NULL,
  `end_date` varchar(50) NOT NULL,
  `policy_status` varchar(50) NOT NULL,
  `entered_by` int(11) NOT NULL,
  `entry_date` varchar(50) NOT NULL,
  `entry_time` varchar(50) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `update_date` varchar(50) NOT NULL,
  `update_time` varchar(50) NOT NULL,
  `software_lic_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cash_collected`
--

CREATE TABLE `cash_collected` (
  `cash_collection_id` int(11) NOT NULL,
  `software_cash_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `cash_collection_date` varchar(50) NOT NULL,
  `total_payment` float NOT NULL,
  `cash_received` float NOT NULL,
  `software_lic_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `city_info`
--

CREATE TABLE `city_info` (
  `city_table_id` int(11) UNSIGNED NOT NULL,
  `software_city_id` int(10) UNSIGNED NOT NULL,
  `city_id` int(10) UNSIGNED DEFAULT NULL,
  `district_table_id` int(10) UNSIGNED DEFAULT NULL,
  `city_name` varchar(100) NOT NULL,
  `city_status` varchar(20) NOT NULL,
  `creating_user_id` int(11) UNSIGNED NOT NULL,
  `creating_date` varchar(30) NOT NULL,
  `creating_time` varchar(30) NOT NULL,
  `update_user_id` int(10) UNSIGNED NOT NULL,
  `update_date` varchar(20) NOT NULL,
  `update_time` varchar(20) NOT NULL,
  `software_lic_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city_info`
--

INSERT INTO `city_info` (`city_table_id`, `software_city_id`, `city_id`, `district_table_id`, `city_name`, `city_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`, `software_lic_id`) VALUES
(5, 1, 1, 1, 'Gujrat', 'Active', 1, '16/Feb/2022', '10:46 AM', 1, '16/Feb/2022', '10:46 AM', 1),
(6, 2, 2, 1, 'Kharian', 'Active', 1, '16/Feb/2022', '10:46 AM', 1, '16/Feb/2022', '10:46 AM', 1),
(7, 3, 3, 1, 'Lalamusa', 'Active', 1, '16/Feb/2022', '10:46 AM', 1, '16/Feb/2022', '10:46 AM', 1),
(8, 4, 4, 1, 'Kunjah', 'Active', 1, '16/Feb/2022', '10:46 AM', 1, '16/Feb/2022', '10:46 AM', 1),
(9, 5, 5, 1, 'Mangowal', 'Active', 1, '16/Feb/2022', '10:46 AM', 1, '16/Feb/2022', '10:46 AM', 1),
(10, 6, 6, 1, 'Kotla', 'Active', 1, '16/Feb/2022', '10:46 AM', 1, '16/Feb/2022', '10:46 AM', 1),
(11, 7, 7, 1, 'Jalalpur Jattan', 'Active', 1, '16/Feb/2022', '10:46 AM', 1, '16/Feb/2022', '10:46 AM', 1);

-- --------------------------------------------------------

--
-- Table structure for table `company_alternate_contacts`
--

CREATE TABLE `company_alternate_contacts` (
  `contact_id` int(10) UNSIGNED NOT NULL,
  `software_contact_id` int(10) UNSIGNED NOT NULL,
  `company_id` int(10) UNSIGNED NOT NULL,
  `contact_name` varchar(150) NOT NULL,
  `contact_number` varchar(60) NOT NULL,
  `software_lic_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `company_info`
--

CREATE TABLE `company_info` (
  `company_table_id` int(11) NOT NULL,
  `software_company_id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `company_name` varchar(200) NOT NULL,
  `company_address` varchar(300) NOT NULL,
  `company_city` varchar(50) NOT NULL,
  `company_contact` varchar(100) NOT NULL,
  `contact_Person` varchar(100) NOT NULL,
  `company_email` varchar(100) NOT NULL,
  `company_image` varchar(300) DEFAULT NULL,
  `company_status` varchar(50) NOT NULL,
  `creating_user_id` int(11) NOT NULL,
  `creating_date` varchar(30) NOT NULL,
  `creating_time` varchar(30) NOT NULL,
  `update_user_id` int(11) NOT NULL,
  `update_date` varchar(30) NOT NULL,
  `update_time` varchar(30) NOT NULL,
  `software_lic_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_info`
--

INSERT INTO `company_info` (`company_table_id`, `software_company_id`, `company_id`, `company_name`, `company_address`, `company_city`, `company_contact`, `contact_Person`, `company_email`, `company_image`, `company_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`, `software_lic_id`) VALUES
(43, 1, 4, 'High Q Pharmaceuticals', 'Karachi', 'Karachi', '', '', '', '', 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '30/Nov/2021', '11:39 AM', 1),
(44, 2, 5, 'Abbot Laboratories', 'Karachi', 'Karachi', '021-563658', '', '', '', 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '19/Jan/2022', '11:10 AM', 1),
(45, 3, 6, 'Wyeth', 'Karachi', 'Karachi', '', '', '', '', 'Active', 1, '19/Jan/2022', '11:11 AM', 1, '19/Jan/2022', '11:11 AM', 1),
(46, 4, 8, 'Vida Laboratories', 'Karachi', 'Karachi', '', '', '', '', 'Active', 1, '19/Jan/2022', '11:11 AM', 1, '19/Jan/2022', '11:11 AM', 1);

-- --------------------------------------------------------

--
-- Table structure for table `company_overall_record`
--

CREATE TABLE `company_overall_record` (
  `company_overall_id` int(10) UNSIGNED NOT NULL,
  `software_company_overall_id` int(10) UNSIGNED NOT NULL,
  `company_id` int(10) UNSIGNED NOT NULL,
  `orders_given` int(10) UNSIGNED NOT NULL,
  `successful_orders` int(10) UNSIGNED NOT NULL,
  `ordered_packets` int(10) UNSIGNED NOT NULL,
  `received_packets` int(10) UNSIGNED NOT NULL,
  `ordered_boxes` int(10) UNSIGNED NOT NULL,
  `received_boxes` int(10) UNSIGNED NOT NULL,
  `order_price` float NOT NULL,
  `discount_price` float NOT NULL,
  `invoiced_price` float NOT NULL,
  `cash_sent` float NOT NULL,
  `return_packets` int(10) NOT NULL,
  `return_boxes` int(10) NOT NULL,
  `return_price` float NOT NULL,
  `cash_return` float NOT NULL,
  `waived_off_price` float NOT NULL,
  `pending_payments` float NOT NULL,
  `software_lic_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_overall_record`
--

INSERT INTO `company_overall_record` (`company_overall_id`, `software_company_overall_id`, `company_id`, `orders_given`, `successful_orders`, `ordered_packets`, `received_packets`, `ordered_boxes`, `received_boxes`, `order_price`, `discount_price`, `invoiced_price`, `cash_sent`, `return_packets`, `return_boxes`, `return_price`, `cash_return`, `waived_off_price`, `pending_payments`, `software_lic_id`) VALUES
(31, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1),
(32, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1),
(33, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1),
(34, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `company_payments`
--

CREATE TABLE `company_payments` (
  `payment_id` int(10) UNSIGNED NOT NULL,
  `software_payment_id` int(10) UNSIGNED NOT NULL,
  `company_id` int(10) UNSIGNED NOT NULL,
  `invoice_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `amount` float NOT NULL,
  `cash_type` varchar(10) NOT NULL,
  `pending_payments` float NOT NULL,
  `date` varchar(30) NOT NULL,
  `day` varchar(15) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `comments` varchar(200) DEFAULT NULL,
  `status` varchar(30) NOT NULL,
  `software_lic_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_payments`
--

INSERT INTO `company_payments` (`payment_id`, `software_payment_id`, `company_id`, `invoice_id`, `amount`, `cash_type`, `pending_payments`, `date`, `day`, `user_id`, `comments`, `status`, `software_lic_id`) VALUES
(15, 2, 1, 0, 5000, 'Sent', -5000, '30/Jan/2022', 'Sunday', 1, '', 'Cash', 1);

-- --------------------------------------------------------

--
-- Table structure for table `dealer_day_summary`
--

CREATE TABLE `dealer_day_summary` (
  `dealer_summary_id` int(10) UNSIGNED NOT NULL,
  `software_dealer_summary_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `date` varchar(20) NOT NULL,
  `day` varchar(15) NOT NULL,
  `booking_order` int(10) UNSIGNED NOT NULL,
  `delivered_order` int(10) UNSIGNED NOT NULL,
  `returned_order` int(10) UNSIGNED NOT NULL,
  `return_quantity` int(10) NOT NULL,
  `return_price` float NOT NULL,
  `cash_collection` float NOT NULL,
  `cash_return` float NOT NULL,
  `cash_waiveoff` float NOT NULL,
  `software_lic_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dealer_gps_location`
--

CREATE TABLE `dealer_gps_location` (
  `dealer_loc_id` int(10) UNSIGNED NOT NULL,
  `software_dealer_loc_id` int(11) UNSIGNED DEFAULT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `latitude` varchar(25) NOT NULL,
  `longitude` varchar(25) NOT NULL,
  `loc_name` varchar(150) NOT NULL,
  `entry_date` varchar(20) NOT NULL,
  `entry_time` varchar(20) NOT NULL,
  `update_date` varchar(20) NOT NULL,
  `update_time` varchar(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `software_lic_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dealer_info`
--

CREATE TABLE `dealer_info` (
  `dealer_table_id` int(10) UNSIGNED NOT NULL,
  `software_dealer_id` int(10) UNSIGNED NOT NULL,
  `dealer_id` int(10) UNSIGNED DEFAULT NULL,
  `dealer_area_id` int(10) UNSIGNED NOT NULL,
  `dealer_name` varchar(200) NOT NULL,
  `dealer_contact_person` varchar(100) NOT NULL,
  `dealer_phone` varchar(50) NOT NULL,
  `dealer_fax` varchar(50) NOT NULL,
  `dealer_address` varchar(250) NOT NULL,
  `dealer_address1` varchar(250) NOT NULL,
  `dealer_type` varchar(30) NOT NULL,
  `dealer_cnic` varchar(30) NOT NULL,
  `dealer_ntn` varchar(70) NOT NULL,
  `dealer_image` varchar(300) DEFAULT NULL,
  `dealer_lic9_num` varchar(80) NOT NULL,
  `dealer_lic9_exp` varchar(50) NOT NULL,
  `dealer_lic10_num` varchar(50) NOT NULL,
  `dealer_lic10_Exp` varchar(50) NOT NULL,
  `dealer_lic11_num` varchar(50) NOT NULL,
  `dealer_lic11_Exp` varchar(50) NOT NULL,
  `dealer_credit_limit` int(11) NOT NULL,
  `dealer_status` varchar(50) NOT NULL,
  `creating_user_id` int(10) UNSIGNED NOT NULL,
  `creating_date` varchar(20) NOT NULL,
  `creating_time` varchar(20) NOT NULL,
  `update_user_id` int(10) UNSIGNED NOT NULL,
  `update_date` varchar(20) NOT NULL,
  `update_time` varchar(20) NOT NULL,
  `software_lic_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dealer_info`
--

INSERT INTO `dealer_info` (`dealer_table_id`, `software_dealer_id`, `dealer_id`, `dealer_area_id`, `dealer_name`, `dealer_contact_person`, `dealer_phone`, `dealer_fax`, `dealer_address`, `dealer_address1`, `dealer_type`, `dealer_cnic`, `dealer_ntn`, `dealer_image`, `dealer_lic9_num`, `dealer_lic9_exp`, `dealer_lic10_num`, `dealer_lic10_Exp`, `dealer_lic11_num`, `dealer_lic11_Exp`, `dealer_credit_limit`, `dealer_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`, `software_lic_id`) VALUES
(26371, 1, 11, 1, 'ATTIQ MEDICAL STORE', '', '053-3513450', '', 'CIRCULAR ROAD', 'GUJRAT', 'Whole Saler', '3420174263657', '0402645-4', '', '0', '0', '0', '1/1/2017 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26372, 2, 1102, 1, 'DR.ABDUL-MUSTFA', '', '0305-5169708', '', '', 'SHIFA HOSPITAL GUJRAT', 'Doctor', '34201-8773014-9', '1225328', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26373, 3, 13, 1, 'ATTIQ MEDICAL STORE (2)', '', '', '', 'SHAH FASIAL GATE', 'GUJRAT', 'Retailer', '', '', '', '0', '272', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26374, 4, 518, 5, 'DR.ALLAH-DAD CHEEMA', '', '', '', '', 'CHEEMA HOSPITAL GUJRAT', 'Doctor', '', '0401520-7', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26375, 5, 26, 2, 'PHARMACY, ALI CLINIC', '', '', '', 'G.T.ROAD', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26376, 6, 17, 1, 'BUKHARI MEDICAL STORE', '', '', '', 'NEAR CHOWK PAKISTAN', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26377, 7, 48, 4, 'NEW MEHAR MEDICAL STORE', '', '', '', 'AHSAN PLAZA COURT ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26378, 8, 163, 1, 'AL HASSAN MEDICAL STORE', '', '03219626800', '', 'MUSLIM ABAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '0', '12/31/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26379, 9, 18, 1, 'JEDDAH PHARMACIE', '', '053-3510621', '', 'NEAR KABLE GATE', 'GUJRAT', 'Retailer', '34201-6861736-5', '5422145-5', '', '45742', '0', '0', '10/10/2021 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26380, 10, 164, 1, 'NEW SERWER MEDICAL STORE', '', '', '', 'JINAH ROAD', 'GUJRAT', 'Retailer', '34201-0422378-5', '6499775-2', '', '0', '247', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26381, 11, 813, 8, 'DR.M.ZEESHAN ANWAR', '', '', '', 'PMDC# 53380-P', 'FAZEELAT AMIN HOSPITAL GUJRAT', 'Doctor', '', '', '', '53380', '0', '0', '12/31/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26382, 12, 138, 1, 'DR.ASJAD FAROOQ AHMED', '', '0336-6227400', '', 'PMDC# 6837-P', 'CITY HOSPITAL GUJRAT', 'Doctor', '34201-0324706-8', '4089875', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26383, 13, 416, 4, 'PHARMACY,HAIDER MEDICAL COMPLEX', '', '', '', 'JINAH ROAD', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26384, 14, 525, 5, 'DR.TAHIR RASHID', '', '053-3606616', '', 'PMDC# 4222-P', 'POLY CLINIC Hp GUJRAT', 'Doctor', '34201-0434995-9', '8279864-8', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26385, 15, 118, 1, 'WRIACH MEDICAL STORE', '', '', '', 'SABUWAL ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26386, 16, 119, 1, 'FAZAL SONS PHARMACY', '', '', '', 'PRINCE CHOWK', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26387, 17, 165, 1, 'DR.M.ZAMAN MIRZA', '', '', '', '', 'ALI CHILDREN Hp GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26388, 18, 94, 9, 'AL HAIDER MEDICAL STORE', '', '03216291539', '', 'ADJACENT MASTER M ALTAF C/H', 'RALWAY ROAD GUJRAT', 'Retailer', '', '2291312-2', '', '0', '0', '', '12/30/1899 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26389, 19, 529, 5, 'SOBER PHARMACY', '', '03224009080', '', 'NEAR GUJRAT HOSPITAL', 'BHIMBER ROAD GUJRAT', 'Retailer', '34201-0407250-9', '1459895-7', '', '0', '12031', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26390, 20, 123, 1, 'NEW PUNJAB MEDICAL STORE', '', '', '', 'BHIMBER ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26391, 21, 530, 5, 'DR.MIR ZAHID ZAHEER', '', '0333-8445678', '0300 9620732', 'PMDC# 7247-P', 'Gujrat Hospital GUJRAT', 'Doctor', '34201-1692229-9', '0406050-4', '', '7247', '0', '0', '12/31/2023 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26392, 22, 568, 5, 'PHARMACY,DR.IQBAL JAWAD GOHER', '', '', '', 'BHIMBER ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26393, 23, 1101, 1, 'DR.HAIDER ASADULLAH MALIK', '', '', '', 'PMDC# 3032-P', 'Malik Haider Hospital GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26394, 24, 427, 4, 'PHARMACY,MEDICARE HOSPITAL', '', '', '', 'BHIMBER ROAD', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26395, 25, 531, 5, 'DR.SHEHZAD CH.', '', '0315-6200777', '', '', 'NEW FAMILY Hp GUJRAT', 'Doctor', '34201-0573947-9', '0404847-4', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26396, 26, 532, 5, '.RAZA PHARMACY', '', '03009625301', '', 'OPP. CHANAB HOSPITAL', 'GUJRAT', 'Retailer', '', '', '', '1401', '0', '0', '8/1/2017 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26397, 27, 130, 1, 'USMAN MEDICAL STORE', '', '', '', 'SHAH HUSSAIN ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26398, 28, 131, 1, 'SUFIAN MEDICAL STORE', '', '', '', 'JINAH ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26399, 29, 142, 1, 'DR.AZHAR MEHMOOD MIRZA', '', '', '', 'MIRZA HOSPITAL', 'KACHERY CHOWK GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26400, 30, 166, 1, 'CENTRAL MEDICAL STORE', '', '053-3510488', '', 'STREET NO.1,LINK JINNAH ROAD', 'GUJRAT', 'Retailer', '34201-0524154-3', '', '', '37992', '37992', '37992', '12/22/2020 12:00 AM', '12/22/2020 12:00 AM', '12/22/2020 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26401, 31, 143, 1, 'NEW AZIZ BHATTI PHARMACY', '', '03006206991', '', 'COURT ROAD', 'GUJRAT', 'Retailer', '', '', '', '1328', '0', '0', '6/30/2016 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26402, 32, 167, 1, 'SADAAT MEDICAL STORE', '', '03338526249', '', 'KHAWAJGAN ROAD', 'CHOWK PAKISTAN GUJRAT', 'Retailer', '34201-0377578-3', '1285206-6', '', '0', '27022', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26403, 33, 112, 1, 'HM PHARMACY', '', '0321-6217605', '', 'Opp.Zahoor Palace', 'GUJRAT', 'Retailer', '34201-8008819-1', '2266402-5', '', '34643', '0', '0', '6/25/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26404, 34, 144, 1, 'MUQADAS MEDICAL STORE', '', '', '', 'COURT ROAD', 'GUJRAT', 'Retailer', '', '', '', '692', '0', '692', '12/31/2015 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26405, 35, 168, 1, 'LIAQAT PHARMACY', '', '', '', 'NEAR MEEZAN BANK', 'CHOWK PAKISTAN GUJRAT', 'Retailer', '', '', '', '25030', '0', '0', '1/29/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26406, 36, 113, 1, 'ZAHID MEDICAL STORE', '', '', '', 'Near Zahoor Palace', 'East Circular Road GUJRAT', 'Retailer', '34201-0370622-9', '0402530-0', '', '26522', '0', '819', '12/28/2019 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26407, 37, 145, 1, 'DR.ABDUL QAYOOM', '', '', '', 'MARYAM HOSPITAL', 'REHMAN SHAHED ROAD GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26408, 38, 943, 9, 'DR.ZAFAR IQBAL', '', '', '', 'PMDC# 23131-P', 'RIMSHA HOSPITAL GUJRAT', 'Doctor', '', '', '', '23131', '0', '0', '12/31/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26409, 39, 19, 1, 'SHEIKH PHARMACY', '', '03006231941', '', 'NEAR GUJRAT JEWELLERS', 'MUSLIM BAZAR GUJRAT', 'Retailer', '34201-1777581-5', '2434486-9', '', '16943', '0', '0', '10/24/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26410, 40, 115, 1, 'NEW JET PHARMACY', '', '0300-9627028', '', 'EAST CIRCULAR ROAD', 'GUJRAT', 'Retailer', '34201-8115993-9', '1528011-0', '', '15504', '0', '0', '10/24/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26411, 41, 116, 1, '.ROYAL MEDICAL STORE', '', '', '', '', 'GUJRAT', 'Retailer', '34201-0555462-3', '26173000', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26412, 42, 1105, 1, 'DR.M.MOHSIN SANDHU & SONS', '', '053-3533260', '', 'TIMBLE BAZAR', 'GUJRAT', 'Retailer', '', '', '', '0', '5522', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26413, 43, 146, 1, 'AMMAR MEDICAL STORE', '', '', '', 'FAWARA CHOWK', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26414, 44, 117, 1, 'Naseem Medical Store', '', '0321-6205455', '', 'Out Side Asif Hospital', 'Fawara Chowk GUJRAT', 'Retailer', '34201-3096324-5', '1289940-2', '', '0', '9891', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26415, 45, 74, 7, 'KHATANA MEDICAL STORE', '', '0300 6207279', '', 'OOP.AZIZ BHATTI SHAHEED Hp', 'GUJRAT', 'Retailer', '3420178511657', '5409883-1', '', '0', '7548', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26416, 46, 12, 1, 'AL-RIAZ MEDICAL STORE', '', '03006201596', '', 'SHAH FAISAL GATE', 'GUJRAT', 'Retailer', '34201-0932996-3', '1755183-8', '', '0', '721', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26417, 47, 120, 1, 'NAEEM MEDICAL STORE', '', '053-3524205', '', 'SHAHDULA ROAD', 'GUJRAT', 'Retailer', '34201-0357522-9', '1633552-0', '', '403', '0', '403', '12/31/2015 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26418, 48, 95, 9, 'NEW AFTAB MEDICAL STORE', '', '', '', 'NEAR ANSARI CLOTH HOUSE', 'SARGODHA ROAD GUJRAT', 'Retailer', '34201-0472303-1', '1460003-0', '', '0', '7889', '0', '12/31/2015 12:00 AM', '9/29/2019 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26419, 49, 5132, 5, 'AL-AZIZ MEDICAL STORE', '', '', '', 'G.T.ROAD', 'GUJRAT', 'Retailer', '', '', '', '343', '0', '0', '12/31/2013 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26420, 50, 96, 9, 'BILAL MEDICAL STORE', '', '03216213521', '', 'OPP.RAJA BAKERS', 'RAILWAY ROAD GUJRAT', 'Retailer', '34201-0488392-5', '0401630-7', '', '0', '3820', '0', '12/31/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26421, 51, 981, 9, 'JAMEEL MEDICAL STORE', '', '', '', 'RAILWAY ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '0', '12/31/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26422, 52, 960, 9, 'ABDULAH MEDICAL STORE', '', '03009624484', '', 'RAILWAY ROAD', 'GUJRAT', 'Retailer', '34201-0544283-1', '3778567-2', '', '0', '305', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26423, 53, 161, 1, 'HYDER MEDICAL STORE', '', '', '', 'RAILWAY ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26424, 54, 99, 9, 'DR.M.AMIN GUL', '', '0333-8403202', '', 'PMDC NO.10775-P', 'INAYAT HOSPITAL GUJRAT', 'Doctor', '34201-0348034-3', '0401773-7', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26425, 55, 910, 9, '.REHMAN MEDICAL STORE', '', '0332 8387221', '', 'NEAR SITTARA BAKERS', 'SARGODHA ROAD GUJRAT', 'Retailer', '34201-8971173-5', '5630660-8', '', '0', '5685', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26426, 56, 911, 9, 'QAUMI MEDICAL STORE', '', '', '', 'NEAR CH.KHADIM KARYANA STORE', 'STAFF GALA GUJRAT', 'Retailer', '34201-0513717-9', '1632918-0', '', '0', '13498', '0', '5/26/0214 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26427, 57, 912, 9, 'NEW SULMAN MEDICAL STORE', '', '03076231572', '', 'SARGODHA ROAD', 'GUJRAT', 'Retailer', '', '5348687-5', '', '891', '0', '891', '12/31/2015 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26428, 58, 913, 9, 'NASEER PHARMACY', '', '0333-8452091', '', 'STAAF GALA,SARGODHA ROAD', 'GUJRAT', 'Retailer', '34201-2635558-9', 'A322964-8', '', '68107', '0', '0', '3/15/2023 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26429, 59, 21, 2, 'AL-MUSLIM MEDICAL STORE', '', '', '', 'D.H.Q. HOSPITAL', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26430, 60, 22, 2, 'DEWAN MEDICAL STORE', '', '', '', 'D.H.Q HOSPITAL', 'MANDI BHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26431, 61, 23, 2, 'AHSAN MEDICAL STORE', '', '', '', 'D.H.Q. HOSPITAL', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26432, 62, 24, 2, 'ITTIFAQ MEDICAL STORE', '', '', '', 'D.H.Q HOSPITAL', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26433, 63, 25, 2, 'SOHAIL MEDICAL STORE', '', '', '', 'D.H.Q HOSPITAL', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26434, 64, 28, 2, 'AHMED-WAQAS MEDICAL STORE', '', '', '', 'D.H.Q.HOSPITAL', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26435, 65, 29, 2, 'SHAHID MEDICAL STORE', '', '', '', 'KACHARY ROAD', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26436, 66, 212, 2, 'SHAUKET MEDICAL HALL', '', '', '', 'OPP.D.H.Q.', 'MANDI BHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26437, 67, 214, 2, 'AZEEM MEDICAL STORE', '', '', '', '', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26438, 68, 215, 2, 'FAIZ MEDICAL STORE', '', '', '', 'SADDER BAZAR', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26439, 69, 218, 2, 'PANJAB MEDICAL STORE', '', '', '', 'SADDER BAZAR', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26440, 70, 222, 2, 'MIAN MEDICAL STORE', '', '', '', 'COMMETTE BAZAR', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26441, 71, 224, 2, 'AL GONDAL MEDICAL STORE', '', '', '', 'D.H.Q.ROAD', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26442, 72, 225, 2, 'PHARMACY,RAKHAT HOSPITAL', '', '', '', '', 'MANDI BAHAUDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26443, 73, 226, 2, 'MUMTAZ MEDICINE', '', '', '', 'PHALIA ROAD', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26444, 74, 227, 2, 'PAK MEDICAL STORE', '', '', '', 'PHALIA ROAD', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26445, 75, 228, 2, 'ABUBAKAR MEDICAL STORE', '', '', '', 'PHALIA ROAD', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26446, 76, 229, 2, 'KHALEEL MEDICAL STORE', '', '', '', 'PHALIA ROAD', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26447, 77, 230, 2, 'PHARMACY,ALVI HOSPITAL', '', '', '', 'PHALIA ROAD', 'MANDI BAHAUDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26448, 78, 231, 2, 'TAHIR MADICAL STORE', '', '', '', 'KACHARY ROAD', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26449, 79, 233, 2, 'MIAN MEDICINE', '', '', '', 'KEHEHRY ROAD', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26450, 80, 234, 2, 'PHARMACY,NAZEER GONDAL H.S', '', '', '', 'RASOOL ROAD', 'MANDI BAHAUDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26451, 81, 235, 2, 'SHAHEEN MEDICAL STORE', '', '', '', 'RASDDL ROAD', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26452, 82, 236, 2, 'NASIR MADICAL STORE', '', '', '', 'RASOOL ROAD', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26453, 83, 240, 2, 'HASSAN MEDICAL STORE', '', '', '', 'KACHARY ROAD', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26454, 84, 237, 2, 'AL SHAFA MEDICINE CO', '', '', '', 'KACHARY ROAD', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26455, 85, 239, 2, 'STANDERD MEDICAL STORE', '', '', '', 'KING ROAD', 'MANDI BHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26456, 86, 31, 3, 'IMTIAZ MEDICAL STORE', '', '', '', 'T.H.Q.ROAD', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26457, 87, 32, 3, 'NWE MUNIR MADICAL STORE', '', '', '', 'T.H.Q.ROAD', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26458, 88, 33, 3, 'SALEEME MADICAL STORE', '', '', '', 'T.H.Q.ROAD', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26459, 89, 34, 3, 'PUBLIC MADICAL HALL', '', '', '', 'T.H.Q.ROAD', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26460, 90, 35, 3, 'SHARIF MADICAL STORE', '', '', '', 'T.H.Q.ROAD', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26461, 91, 36, 3, 'NISAR MADICAL STORE', '', '', '', 'T.H.Q.ROAD', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26462, 92, 37, 3, 'AL-SHAFA MADICAL STORE', '', '', '', 'T.H.Q.ROAD', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26463, 93, 38, 3, 'ZAMAN MADICAL STORE', '', '', '', 'KHANIAN ROAD', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26464, 94, 39, 3, 'FEIZAN MADICAL STORE', '', '', '', 'MAIN ROAD', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26465, 95, 42, 4, 'MANGET MADICAL STORE', '', '', '', 'MAIN BAZAR', 'PHARIANWALI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26466, 96, 43, 4, 'AL-MUSLIM MADICAL STORE', '', '', '', 'MAIN BAZAR', 'PHARIANWALI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26467, 97, 44, 4, 'SHAHEEN MADICAL STORE', '', '', '', 'MAIN BAZAR', 'PHARIANWALI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26468, 98, 45, 4, 'ANWER MADICAL STORE', '', '', '', 'OPP-B.H.U.', 'PHARIANWALI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26469, 99, 141, 14, 'TARIQ MEDICAL STORE', '', '', '', 'NEAR RAILWAYS', 'SARI ALAMGIR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26470, 100, 147, 14, 'NEW IQBAL MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'SARI ALAMGER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26471, 101, 149, 14, 'ANSAR MEDICAL STORE', '', '', '', 'G.T.ROAD', 'SARI ALAMGER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26472, 102, 81, 8, 'BAIGA MEDICAL STORE', '', '', '', 'SADDER BAZAR', 'KHARIAN CANT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26473, 103, 82, 8, 'HUSSAIN MEDICAL STORE', '', '', '', 'SADDER BAZAR', 'KHARIAN CANT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26474, 104, 93, 9, 'DAR MEDICAL STORE', '', '', '', 'GULYANA ROAD', 'KHARIAN CITY', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26475, 105, 83, 8, 'PUNJEB MEDICAL STORE', '', '', '', 'SADDER BAZAR', 'KHARIAN CANT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26476, 106, 85, 8, 'NEW PAK MEDICAL STORE', '', '', '', 'SADDER BAZAR', 'KHARIAN CANT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26477, 107, 92, 9, 'AMER MEDICAL STORE', '', '', '', 'GULYANA ROAD', 'KHARIAN CITY', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26478, 108, 914, 9, 'TARIQ MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'KHARIAN CITY', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26479, 109, 916, 9, 'DASI DAWAKHANA', '', '', '', 'MAIN BAZAR', 'KHARIAN CITY', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26480, 110, 917, 9, 'KHARIAN MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'KHARIAN CITY', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26481, 111, 71, 7, 'KHALID MEDICAL STORE', '', '', '', 'DINACHAKIAN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26482, 112, 72, 7, 'TARIQ MEDICAL STORE', '', '', '', 'DINACHAKIAN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26483, 113, 73, 7, 'HAFIZ SONS', '', '', '', 'GHALAH MANDI', 'LALAMUSA', 'G', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26484, 114, 75, 7, 'SATHI MEDICAL STORE', '', '', '', 'NEAR GHALA MANDI', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26485, 115, 76, 7, 'AL-REHMAN MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26486, 116, 77, 7, 'CHISTE MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26487, 117, 78, 7, 'MADINA MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26488, 118, 710, 7, 'ABUBAKER MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26489, 119, 712, 7, 'NEW ZAM ZAM MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26490, 120, 713, 7, 'NEW RAFIQ MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26491, 121, 714, 7, 'NEW KAMAL MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26492, 122, 715, 7, 'HAIDER MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26493, 123, 716, 7, 'FAROOQ MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26494, 124, 717, 7, 'FORMASALE MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26495, 125, 720, 7, 'JALAL MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26496, 126, 721, 7, 'NAEEM MEDICAL CENTER', '', '', '', 'G.T.ROAD', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26497, 127, 722, 7, 'FARYAD MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'DEONAMANDI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26498, 128, 723, 7, 'SHAMI MEDICAL STORE', '', '', '', 'G.T.ROAD', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26499, 129, 724, 7, 'AL-MURTAZA MEDICAL STORE', '', '', '', 'G.T.ROAD', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26500, 130, 725, 7, 'BUKHARI MEDICAL STORE', '', '', '', 'G.T.ROAD', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26501, 131, 726, 7, 'LIFE MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26502, 132, 727, 7, 'MEDICAR MEDICAL STORE', '', '', '', 'G.T.ROAD', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26503, 133, 728, 7, 'ZUBAIR MEDICAL STORE', '', '', '', 'G.T.ROAD', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26504, 134, 729, 7, 'MADNI MEDICOS', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26505, 135, 730, 7, 'IQRA MEDICAL STORE', '', '', '', 'G.T.ROAD', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26506, 136, 733, 7, 'PHARMACY,DR.AHMED SHAH', '', '', '', 'RAILWAY HOSPITAL', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26507, 137, 734, 7, 'NEW USAMA MEDICAL STORE', '', '', '', 'CAMPING GROUND', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26508, 138, 735, 7, 'PAK MEDICAL STORE', '', '', '', 'CAMPING GROUND', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26509, 139, 736, 7, 'NAVEED MEDICOS', '', '', '', 'CAMPING GROUND', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26510, 140, 101, 10, 'MUNEER MEDICAL STORE', '', '', '', '', 'JALALPUR JATTAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26511, 141, 102, 10, 'CHOKHAN MEDICAL STORE', '', '', '', '', 'BARILASHARIF', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26512, 142, 103, 10, 'PHARMACY,DR.SHAKIL RAZA CLINIC', '', '', '', '', 'JALALPUR JATTAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26513, 143, 104, 10, 'PHARMACY,AL REHMAN H.S', '', '592777-430777', '431136', 'MAIN BAZAR', 'JALALPUR JATTAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26514, 144, 105, 10, 'NEW WARAICH MADICAL STORE', '', '', '', 'MAIN BAZAR', 'JALALPUR JATTAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26515, 145, 106, 10, 'SHAKH MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'JALALPUR JATTAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26516, 146, 107, 10, 'PHARMACY,SALEEM HOSPITAL', '', '', '', '', 'JALALPUR JATTAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26517, 147, 108, 10, 'RAFIQ MEDICAL STORE', '', '', '', 'NEAR BUS STOP', 'JALALPUR JATTAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26518, 148, 109, 10, 'LATIF MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'FATHAPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26519, 149, 1010, 10, 'PHARMACY,CITY HOSPITAL', '', '', '', 'TANDA ROAD', 'JALALPUR JATTAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26520, 150, 1011, 10, 'PHARMACY,ALLIED MEDICAL CENTRE', '', '', '', 'NEAR CIVIL HOSPATIL', 'JALALPUR JATTAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26521, 151, 1012, 10, 'AL-SHIFA MEDICAL STORE', '', '', '', '', 'JALALPUR JATTAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26522, 152, 111, 11, 'LATIF MEDICAL STORE', '', '', '', 'MAIN ROAD', 'FATEHPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26523, 153, 114, 11, 'SALEEM & SONS MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'KOTLA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26524, 154, 1110, 11, 'AL FAZAL MEDICAL STORE', '', '', '', 'MAIN ROAD', 'KOTLA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26525, 155, 1111, 11, 'HAFIZ SONS MEDICAL STORE', '', '', '', 'BHIMBER ROAD', 'KOTLA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26526, 156, 51, 5, 'WARAICH MEDICAL STORE', '', '', '', 'MAIN ROAD', 'MANGOWAL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26527, 157, 52, 5, 'ARSHED MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'MANGOWAL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26528, 158, 54, 5, 'PHARMACY,DR.MIAN M.ASLAM', '', '', '', 'MAIN RIAD', 'MANGOWAL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26529, 159, 55, 5, 'SHAN MEDICAL STORE', '', '', '', 'MAIN ROAD', 'KUNAH', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26530, 160, 56, 5, 'BHATTI MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'KUNJAH', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26531, 161, 57, 5, 'SHAHID MEDICAL STORE', '', '', '', 'MAIN ROAD', 'KUNJAH', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26532, 162, 58, 5, 'NAVEED MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'MONGOWAL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26533, 163, 59, 5, 'NADEEM MADICAL STORE', '', '', '', 'MAIN BAZAR', 'KUNJAH', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26534, 164, 511, 5, 'PHARMACY,SHAFE HOSPITAL', '', '', '', '', 'KUNJAH', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26535, 165, 512, 5, 'KAMRAN MEDICAL STORE', '', '', '', '', 'KUNJAH', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26536, 166, 61, 6, 'SHABAZ MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26537, 167, 62, 6, 'TARIQ MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26538, 168, 63, 6, 'EJAZ MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26539, 169, 64, 6, 'KHURSHID MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26540, 170, 65, 6, 'PUNJAB MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26541, 171, 66, 6, 'CHISHTIA MEDICAL STORE', '', '', '', '', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26542, 172, 67, 6, 'ZAHEER MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26543, 173, 68, 6, 'KOKHAR MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26544, 174, 69, 6, 'KASHMER MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26545, 175, 610, 6, 'MIAN MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26546, 176, 611, 6, 'DR.SHANAZ LIAQAT', '', '', '', '', 'DINGA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26547, 177, 121, 12, 'AZAD MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26548, 178, 122, 12, 'AWAME MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26549, 179, 124, 12, 'AKBAR KHANAM HOSPITAL', '', '', '', '', 'BHIMBER', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26550, 180, 125, 12, 'TARIQ MEDICAL STORE', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26551, 181, 126, 12, 'IMTIAZI MEDICAL STORE', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26552, 182, 127, 12, 'MOH.DIN MEMORIAL HOSPATIL', '', '', '', '', 'BHIMBER', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26553, 183, 132, 13, 'NOOR MEDICAL STORE', '', '', '', 'MAIN ROAD', 'KOTJAMAL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26554, 184, 133, 13, 'AZIZ MEDICAL STORE', '', '', '', '', 'KOTJAMAL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26555, 185, 134, 13, 'YOUSAF MADICAL STORE', '', '', '', '', 'KOTJAMAL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26556, 186, 1112, 11, 'FAIZ MEDICAL STORE', '', '', '', '', 'KOTLA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26557, 187, 1013, 10, 'FAZAL HOSPATIL', '', '', '', '', 'JALALPUR', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26558, 188, 1014, 10, 'CITY MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'JALALPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26559, 189, 219, 2, 'JAVAID MEDICAL STORE', '', '', '', 'SADDER BAZAR', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26560, 190, 241, 2, 'KHWAJA MEDICAL STORE', '', '', '', 'MANDI ROAD', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26561, 191, 919, 9, 'HASHAMI MEDICAL STORE', '', '', '', '', 'GULIANA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26562, 192, 47, 4, 'AL-QADARI MEDICAL STORE', '', '', '', '', 'PHARIANAWALI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26563, 193, 737, 7, 'BILAL BROTHERS', '', '', '', 'G.T.ROAD', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26564, 194, 1015, 10, 'ALI MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'JALALPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1);
INSERT INTO `dealer_info` (`dealer_table_id`, `software_dealer_id`, `dealer_id`, `dealer_area_id`, `dealer_name`, `dealer_contact_person`, `dealer_phone`, `dealer_fax`, `dealer_address`, `dealer_address1`, `dealer_type`, `dealer_cnic`, `dealer_ntn`, `dealer_image`, `dealer_lic9_num`, `dealer_lic9_exp`, `dealer_lic10_num`, `dealer_lic10_Exp`, `dealer_lic11_num`, `dealer_lic11_Exp`, `dealer_credit_limit`, `dealer_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`, `software_lic_id`) VALUES
(26565, 195, 243, 2, 'NEW MUMTAZ MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'MANDI BHALWDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26566, 196, 1113, 11, 'SADDAT MEDICAL STORE', '', '', '', 'MAIN ROAD', 'FATEHPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26567, 197, 513, 5, 'PHARMACY,DR.ARIF MASOOD', '', '', '', '', 'KUNJAH', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26568, 198, 612, 6, 'AWAMI MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26569, 199, 920, 9, 'DASI MEDICAL STORE', '', '', '', 'GULYANA ROAD', 'KHARIAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26570, 200, 1017, 10, 'PHARMACY,SHRIF-M-HOPITAL', '', '', '', '', 'JALALPUR', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26571, 201, 613, 6, 'HASSAN MEDICAL STORE', '', '', '', '', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26572, 202, 247, 2, 'NEW KAMAL MEDICAL STORE', '', '', '', 'NEAR D.H.Q', 'MANDI BHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26573, 203, 249, 2, 'PHARMACY,TARIQ MEDICAL COMPLAX', '', '', '', 'DINGA ROAD', 'MANDI BHAUDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26574, 204, 310, 3, 'PHALIA MEDICAL STORE', '', '', '', '', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26575, 205, 922, 9, 'PHARMACY,NEW MINHAS H.S', '', '', '', '', 'KHARIAN CITY', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26576, 206, 514, 5, 'KHAN MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'KUNJAH', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26577, 207, 515, 5, 'BISMILLAH MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'KUNJAH', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26578, 208, 923, 9, 'PHARMACY,IQBAL M.HOSPITAL', '', '', '', 'DR,AFTAB AHMED', 'DINGA ROAD KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26579, 209, 250, 2, 'SANA MEDICAL STORE', '', '', '', 'DINGA ROAD', 'MANDI BHAWLDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26580, 210, 1114, 11, 'IQBAL MEDICAL STORE', '', '', '', 'BHIMBER ROAD', 'KOTLA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26581, 211, 311, 3, 'PANJAB MEDICAL STORE', '', '', '', '', 'PHLIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26582, 212, 251, 2, 'PEOPLE S PHARMACY', '', '', '', 'D.H.Q', 'MANDI BHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26583, 213, 312, 3, 'SHAHEEN MADICAL STORE', '', '', '', ' T.H.Q.ROAD', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26584, 214, 614, 6, 'ASIF MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26585, 215, 252, 2, 'NEW SUPPER MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'MANDI BHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26586, 216, 615, 6, 'AL MUMTAZ HOSPITAL', '', '', '', '', 'DINGA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26587, 217, 616, 6, 'KHALEEL H.S', '', '', '', '', 'DINGA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26588, 218, 617, 6, 'KAME MEDICAL STORE', '', '', '', '', 'CHALIAWALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26589, 219, 1115, 11, 'NEW SAJJAD MEDICAL STORE', '', '', '', 'KOTLA ROAD', 'DOULTA NAGER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26590, 220, 1116, 11, 'AL KARAM MEDICAL STORE', '', '', '', 'MAIN ROAD', 'BOKAN MORE', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26591, 221, 618, 6, 'KHALIQ MEDICAL STORE', '', '', '', '', 'CHALIAWALA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26592, 222, 516, 5, 'FATAH KHAN MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'KUNJAH', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26593, 223, 1020, 10, 'EHSAN MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'JALALPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26594, 224, 313, 3, 'AWAN CLINIC', '', '', '', '', 'PHALIA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26595, 225, 256, 2, 'PHARMACY,CHEEMA HOSPITAL', '', '', '', '', 'MANDI', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26596, 226, 778, 7, 'AL HAFIZ MEDICAL STORE', '', '', '', 'CAMPING GROND', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26597, 227, 1022, 10, 'IQRA MEDICAL STORE', '', '', '', 'JALALPUR JATTAN', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26598, 228, 314, 3, 'ABID MEDICAL STORE', '', '', '', 'PHALIA', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26599, 229, 315, 3, 'MADINA MEDICAL STORE', '', '', '', '', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26600, 230, 87, 8, 'JAN SHAR KHAN', '', '', '', 'SADDER BAZAR', 'KHARIAN CANT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26601, 231, 1117, 11, '.', '', '', '', '', 'KOTLA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26602, 232, 98, 9, 'SHAHBAZ MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'KHARIAN CITY', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26603, 233, 619, 6, 'AJMAL MEDICAL STORE', '', '', '', '', 'CHALIWALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26604, 234, 459, 4, 'DMD', '', '03225991314', '', '', 'GUJRAT', 'Retailer', '', '', '', '414', '0', '0', '12/31/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26605, 235, 169, 1, 'NEW FAROOQ MEDICAL STORE', '', '', '', 'NAZ CHOWK', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26606, 236, 97, 9, 'ZAHID MEDICAL STORE', '', '', '', 'SARGODHA ROAD', 'GUJRAT', 'Retailer', '34201-7621926-1', '5312867-5', '', '1005', '0', '0', '12/31/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26607, 237, 1146, 1, 'GRACE PLUS PHARMACY', '', '0333-8405395', '', 'JALALPUR JATTAN ROAD', 'GUJRAT', 'Retailer', '34104-9204851-3', '8157253-4', '', '45182', '0', '0', '9/5/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26608, 238, 174, 1, 'AL GHANI MEDICAL STORE', '', '', '', 'AHSAN PLAZA', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26609, 239, 831, 8, 'LUCKY PHARMACY', '', '', '', 'KACHARY CHOWK,REHMAN SHAHEED', 'ROAD GUJRAT', 'Retailer', '34201-0487828-1', '4222785-2', '', '13782', '0', '0', '10/11/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26610, 240, 177, 1, 'REHMAN S.R', '', '', '', 'JINAH ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26611, 241, 27, 2, 'NEW NASEEM MEDICAL STORE', '', '', '', 'D.H.Q ROAD', 'MANDI BHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26612, 242, 211, 2, 'ZAFAR MEDICAL STORE', '', '', '', 'KACHARY ROAD', 'MANDI BHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26613, 243, 213, 2, 'MAZHAR MEDICAL STORE', '', '', '', 'KACHARY ROAD', 'MANDI BHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26614, 244, 216, 2, 'ASHEA MEDICAL STORE', '', '', '', 'PHALIA ROAD', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26615, 245, 217, 2, 'ARAIB MEDICAL STORE', '', '', '', 'KACHARY ROAD', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26616, 246, 221, 2, 'AWAME MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'MANDI BAHAUDIN', 'Whole Saler', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26617, 247, 210, 2, '.', '', '', '', '', '', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26618, 248, 79, 7, 'FATHA & SONS MEDICAL STORE', '', '', '', 'RAILWAY ROAD', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26619, 249, 711, 7, 'MAKKAH MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26620, 250, 718, 7, 'ASAD MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26621, 251, 731, 7, 'NEW IQRA MEDICAL STORE', '', '', '', 'G.T ROAD', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26622, 252, 732, 7, 'NEW TALAHA MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26623, 253, 84, 8, 'PHARMACY-NASEEB CLINIC', '', '', '', '', 'KHARIAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26624, 254, 148, 14, 'USMAN MEDICAL STORE', '', '', '', 'G.T.ROAD', 'SARI ALAMGER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26625, 255, 1410, 14, 'YASIR MEDICAL STORE', '', '', '', 'NEAR MADDNI MASJID', 'SARI ALALMGER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26626, 256, 738, 7, 'SAVEN STAR MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26627, 257, 1018, 10, 'ALI MEDICAL STORE', '', '', '', '', 'JALALPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26628, 258, 739, 7, 'DR.SHAHID LATIF', '', '', '', 'RAILWAY ROAD', 'LALAMUSA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26629, 259, 740, 7, 'DR.USMAN GHANI', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26630, 260, 741, 7, 'NIAZI CLINIC', '', '', '', 'G.T.ROAD', 'LALAMUSA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26631, 261, 742, 7, 'KASHMIR MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26632, 262, 743, 7, 'PHARMACY,DR.MUBEEN MALIK', '', '053.7531534', '', 'G.T.ROAD PUNJAN KISSANA', 'KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26633, 263, 744, 7, 'PHARMACY,DR.NADEEM AHMED KHAN', '', '', '', '', 'LALAMUSA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26634, 264, 719, 7, 'ARSHAD MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26635, 265, 91, 9, 'PHARMACY,SUBHAN H.S', '', '', '', 'GULYANA ROAD', 'KHARIAN CITY', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26636, 266, 918, 9, 'PHARMACY,DR.KAMAL ATHAR', '', '', '', 'MAIN BAZAR', 'KHARIAN CITY', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26637, 267, 921, 9, 'PHARMACY,MAQBOL NASEEM H.', '', '', '', '', 'KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26638, 268, 924, 9, 'PHARMACY,DR.SAJJADA HAFIZ CLINIC', '', '', '', 'G.T.ROAD', 'KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26639, 269, 925, 9, 'SHARIF MEDICAL STORE', '', '', '', 'KHARIAN ROAD', 'SOBOR CHOWK', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26640, 270, 49, 4, 'MIAN MEDICAL STORE', '', '', '', 'B.H.U', 'PHARIANWALI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26641, 271, 410, 4, 'AL RIAZ MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'PHARIANWALI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26642, 272, 220, 2, 'KHANNAN PHARMACY', '', '', '', 'NEAR D.H.Q', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26643, 273, 223, 2, 'RAHMAT MEDICAL STORE', '', '', '', 'NEAR D.H.Q', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26644, 274, 232, 2, 'ABAD MEDICAL STORE', '', '', '', 'KING ROAD', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26645, 275, 238, 2, 'MADINA MEDICAL STORE', '', '', '', 'KING ROAD', 'MANDI BHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26646, 276, 242, 2, 'CHAUDRY MEDICAL STORE', '', '', '', 'PHALIA ROAD', 'MANDI BHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26647, 277, 244, 2, 'PHARMACY,DR.YOUNAS PARWAZ', '', '', '', 'KACHARY ROAD', 'MANDI BHAUDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26648, 278, 245, 2, 'PHARMACY,CITY HOSPITAL', '', '', '', 'D.H.Q. ROAD', 'MANDI BHAUDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26649, 279, 246, 2, 'PHARMACY,FAMALY HOSPITAL', '', '', '', 'D.H.Q ROAD', 'MANDI BHAUDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26650, 280, 248, 2, 'ZAHID MEDICAL STORE', '', '', '', 'KACHARY ROAD', 'MANDI BHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26651, 281, 253, 2, 'MANSOR MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'MANDI BHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26652, 282, 254, 2, 'SHAKH NAEEM MEDICAL STORE', '', '', '', '', 'MANDI BHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26653, 283, 255, 2, 'DR.RIZWAN CHEEMA', '', '', '', 'KACHARY ROAD', 'MANDI BHAUDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26654, 284, 480, 4, 'AL HARAM MEDICAL STORE', '', '', '', 'BHIMBER ROAD', 'GUJRAT', 'Retailer', '', '', '', '622', '0', '622', '12/31/2013 12:00 AM', '12/30/1899 12:00 AM', '12/31/2013 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26655, 285, 915, 9, 'FATIMA MEDICAL STORE', '', '', '', 'CHOWK MADINA BAZAR', 'SARGODHA ROAD GUJRAT', 'Retailer', '34201-6740850-5', '6513405-6', '', '7569', '0', '0', '10/30/2019 12:00 AM', '12/31/2015 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26656, 286, 957, 9, 'DR.IRFAN SHAUKAT SHIRAZI', '', '', '03026010011', 'PMDC# 21264-P', 'SHERAZI HOSPITAL GUJRAT', 'Doctor', '', '0997580-2', '', '21264', '0', '0', '12/31/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26657, 287, 185, 1, 'MIRZA MEDICAL STORE', '', '', '', 'SHAHDULA ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26658, 288, 286, 2, 'AL SHAFEE MEDICAL STORE', '', '', '', 'SARGODHA ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26659, 289, 187, 1, 'IQBAL MEDICAL STORE', '', '', '', 'RAILWAY ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26660, 290, 539, 5, 'DR.RIFAT GHAZALA', '', '', '', '', 'PUNJAB MEDICAL CENTER GUJRAT', 'Doctor', '', '2500904-4', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26661, 291, 152, 1, 'DR.SYED MOHSIN ALI', '', '', '', 'PMDC# 29331-P', 'SYED SURGICAL Hp GUJRAT', 'Doctor', '35200-8585177-9', '3909447-2', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26662, 292, 541, 5, 'DR.ZAHIDA AKMAL', '', '', '', 'PMDC# 13988-P', 'KHURSHED HOSPITAL GUJRAT', 'Doctor', '', '1633626-7', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26663, 293, 947, 9, 'ALAM MEDICOS', '', '03216219269', '', 'BANK ROAD CHOWK HAKIM KHAN', 'FATUPURA GUJRAT', 'Retailer', '34201-6097612-9', '2698217-0', '', '0', '6725', '0', '12/31/2015 12:00 AM', '11/2/2019 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26664, 294, 194, 1, 'ROZARY HOSPITAL', '', '', '', '', 'GUJRAT', 'I', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26665, 295, 966, 9, 'DR.IJAZ BASHIR', '', '', '', 'PMDC# 19674-P', 'BASHIR HOSPITAL GUJRAT', 'Doctor', '34201-4693844-9', '0401095-7', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26666, 296, 544, 5, 'DR.SAFDAR HUSSAN', '', '', '', 'BHIMBER ROAD', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26667, 297, 198, 1, 'NASEEB MEDICAL STORE', '', '', '', 'BHIMBER ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26668, 298, 517, 5, 'PHARMACY,AL SULTANIA HOSPITAL', '', '', '', '', 'MONGOWAL', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26669, 299, 519, 5, 'TALIB H.S', '', '', '', 'NEAR BUSS STAND', 'KUNJAH', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26670, 300, 520, 5, 'DR.ZAHID AUOB', '', '', '', 'MAIN ROAD', 'MONGOWAL', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26671, 301, 620, 6, 'ZAHID MEDICAL STORE', '', '', '', 'DINGA ROAD', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26672, 302, 521, 5, 'QUOAM MAHRIA M.S', '', '', '', 'MAIN BAZAR', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26673, 303, 622, 6, 'DR.KHALID ARIBE', '', '', '', '', 'DINGA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26674, 304, 623, 6, 'AMER JAVAID COMPLEX', '', '', '', 'THANA ROAD', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26675, 305, 624, 6, 'MAJEED MEDICAL STORE', '', '', '', '', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26676, 306, 1118, 11, 'FAMILY MEDICAL STORE', '', '', '', 'BHIMBER ROAD', 'KOTLA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26677, 307, 1119, 11, 'SHARIF MEDICAL STORE', '', '', '', 'MAIN ROAD', 'KOTLA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26678, 308, 1120, 11, 'DR.AMANAT ALI', '', '', '', '', 'KOTLA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26679, 309, 1121, 11, 'KHARSHED MEDICAL STORE', '', '', '', 'MAIN ROAD', 'FATEHPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26680, 310, 1016, 10, 'BHATTI MEDICAL STORE', '', '', '', 'TANDA ROAD', 'JALALPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26681, 311, 1021, 10, 'PHARMACY,AZAM HOSPITAL', '', '', '', '', 'FATHAPUR', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26682, 312, 1024, 10, 'PHARMACY,SALEEM H.S', '', '', '', 'MAIN ROAD', 'JALALPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26683, 313, 1025, 10, 'AYOUB MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'FATHAPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26684, 314, 1027, 10, 'FARAZ MEDICAL STORE', '', '', '', 'TANDA ROAD', 'JALALPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26685, 315, 1028, 10, 'PHARMACY,DR.SHABIR HUSSAIN', '', '', '', '', 'JALALPUR', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26686, 316, 257, 2, 'SHAUKAT MEDICAL STORE', '', '', '', 'D.H.Q.HOSPITAL', 'MANDI BHAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26687, 317, 258, 2, 'NAEEM SAKH MEDICAL STORE', '', '', '', 'KACHARY ROAD', 'MANDI BAHAUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26688, 318, 259, 2, 'IMRAN MEDICAL STORE', '', '', '', 'NEAR D.H.Q', 'MANDI BAHAUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26689, 319, 260, 2, 'CITY MEDICAL HALL', '', '', '', '', 'MANDI BAHAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26690, 320, 261, 2, 'BILAL AGENCIES', '', '', '', 'OPP OLD BIJLI GHAR', 'MANDI BAHAUDDIN', 'S', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26691, 321, 46, 4, 'BHUTTA MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'PAHRIANWALI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26692, 322, 411, 4, 'AL QADRI MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'PAHRIANWALI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26693, 323, 316, 3, 'FAZAL HAQ MEDICAL STORE', '', '', '', '', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26694, 324, 317, 3, 'SOHAIL MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26695, 325, 625, 6, 'KAZMI MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'CHELLIANWALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26696, 326, 626, 6, 'JAVAID MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'CHELLIANWALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26697, 327, 199, 1, 'AL-HASSAN MEDICAL STORE', '', '', '', 'BHIMBER ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26698, 328, 1100, 1, 'PAKISTAN MEDICAL STORE', '', '', '', 'SARGODHA ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26699, 329, 948, 9, 'DR.NADEEM AHMED', '', '', '', 'PMDC# 30422-P', 'UMAR ARSHAD Hospital GUJRAT', 'Doctor', '34202-5984302-9', '2805344-3', '', '30422', '0', '0', '12/31/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26700, 330, 4102, 4, 'SHIFA PHARMACY', '', '', '', 'RAZAQ MARKET', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26701, 331, 1103, 1, 'HAIDER MEDICAL STORE', '', '', '', 'RAILWAY ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26702, 332, 1104, 1, 'JAHANGIR MEDICAL STORE', '', '', '', 'G.T.ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26703, 333, 949, 9, 'NEW YOUNAS MEDICAL STORE', '', '03466831055', '', 'OPP LADIES&CHILDERN PARK', 'RAMTALI ROAD GUJRAT', 'Retailer', '', '1166931-3', '', '0', '0', '7586', '12/31/2015 12:00 AM', '12/31/2015 12:00 AM', '10/28/2019 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26704, 334, 2107, 2, 'PHARMACY,ASIF CLINIC', '', '', '', 'RALWAY ROAD', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26705, 335, 1019, 10, 'MITHO MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'JALALPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26706, 336, 1029, 10, 'AKHTAR MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'KARIANWALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26707, 337, 1030, 10, 'AZAM MEDICAL STORE', '', '', '', 'TANDO ROAD', 'KARIANWALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26708, 338, 1031, 10, 'BISMILLAH MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'KARIANWALAL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26709, 339, 1032, 10, 'EJZ MEDICSL STORE', '', '', '', 'MAIN BAZAR', 'KARIANWALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26710, 340, 1033, 10, 'ASHRAF MEDICAL STORE', '', '', '', '', 'JALALPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26711, 341, 1034, 10, 'ALAM MEDICAL HALL', '', '', '', 'MAIN BAZAR', 'JALALPUR SOBTIAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26712, 342, 1035, 10, 'AL HAQ MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'J.P.SOBHTIAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26713, 343, 745, 7, 'RASHID MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26714, 344, 318, 3, 'MUKHTAR MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26715, 345, 263, 2, 'ABID MEDICAL STORE', '', '', '', 'KING ROAD', 'MANDI BHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26716, 346, 265, 2, 'PHARMACY,NAZEER SURGICAL HOSPITAL', '', '', '', 'RASOOL ROAD', 'MANDI BHAUDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26717, 347, 4108, 4, 'PHARMACY,JINAH H.S', '', '', '', 'JINAH ROAD', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26718, 348, 266, 2, 'PHARMACY,SHAFAR HOSPITAL', '', '', '', 'RASOOL ROAD', 'MANDI BHAUDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26719, 349, 1411, 14, 'PHARMACY,ALI NAWAZ HOSPITAL', '', '', '', 'NEAR G.T.ROAD', 'SARI ALAMGER', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26720, 350, 747, 7, 'NEW IQRA MEDICAL STORE', '', '', '', 'G.T.ROAD', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26721, 351, 748, 7, 'PHARMACY,SARGUN HOSPITAL', '', '', '', '', 'LALAMUSA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26722, 352, 749, 7, 'ABBAS MEDICAL STORE', '', '', '', 'G.T.ROAD', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26723, 353, 88, 8, 'NEW JAN SHAR M.S', '', '', '', 'SADDER BAZAR', 'KHARIAN CANT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26724, 354, 750, 7, '.', '', '', '', '', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26725, 355, 1412, 14, 'AL NOOR MEDICAL STORE', '', '', '', '', 'SARI ALAMGIR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26726, 356, 1122, 11, 'CHAUDHARY MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'KAKRALI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26727, 357, 547, 5, 'DR.MEHFOOZ AHMED', '', '', '', '', 'REHMAN CLINIC GUJRAT', 'Doctor', '34201-7307582-5', '0223022-4', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26728, 358, 267, 2, 'GHOREE MEDICAL STORE', '', '', '', 'NEAR.D.H.Q.', 'MANDI BHAUDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26729, 359, 751, 7, 'SANDHU MEDICAL STORE', '', '', '', '', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26730, 360, 412, 4, 'NEW PUNJAB MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'PHARIANWALI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26731, 361, 752, 7, 'PHARMACY,POLY CLINIC', '', '', '', '', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26732, 362, 1413, 14, 'REHAN MEDICAL STORE', '', '', '', '', 'SARI ALAMGER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26733, 363, 268, 2, 'MOON PHARMACY', '', '', '', '', 'MANDI BHAUDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26734, 364, 319, 3, 'NEW SUPPER MEDICAL STORE', '', '', '', 'MAIN ROAD', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26735, 365, 320, 3, 'ANMOL MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26736, 366, 753, 7, 'SIALVI MEDICAL STORE', '', '', '', '', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26737, 367, 754, 7, 'PHARMACY,SAEEDA ZAFAR H.S', '', '', '', '', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26738, 368, 1036, 10, 'BHATTI MEDICAL STORE', '', '', '', 'MAIN ROAD', 'JALALPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26739, 369, 926, 9, 'AL MUSLIM MEDICAL STORE', '', '', '', '', 'KHARIAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26740, 370, 270, 2, 'PHARMACY,TARAR HOSPITAL', '', '', '', '', 'MANDI BHAUDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26741, 371, 927, 9, 'HAMAZA MEDICAL STORE', '', '', '', '', 'KHARIAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26742, 372, 321, 3, 'NEW SALEEME MEDICAL STORE', '', '', '', 'NEAR T.H.Q', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26743, 373, 371, 3, 'AL SHAFA  H.S', '', '', '', '', 'MANDI BHAUDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26744, 374, 271, 2, 'PHARMACY,AL KARAM HOSPITAL', '', '', '', '', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26745, 375, 172, 1, 'NEW LIFE MEDICAL STORE', '', '', '', 'NEAR AYESHA HOSPITAL', 'JINNAH ROAD GUJRAT', 'Retailer', '3420103647219', '4985994-8', '', '12875', '0', '0', '10/2/2019 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26746, 376, 1037, 10, 'PHARMACY,WARAICH H.S', '', '', '', '', 'JALALPUR', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26747, 377, 1038, 10, '.', '', '', '', '', 'JALALPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26748, 378, 1039, 10, 'KHALEEQ MEDICAL STORE', '', '', '', '', 'JALALPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26749, 379, 272, 2, 'NASIR MEDICOSE', '', '', '', 'RASOOL ROAD', 'MANDI BHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26750, 380, 756, 7, 'ARSHAD MEDICOSE', '', '', '', 'CAMPING GROUND', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26751, 381, 757, 7, 'NASIR MEDICAL STORE', '', '', '', '', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26752, 382, 1414, 14, 'KHALID MEDICAL STORE', '', '', '', '', 'SARI ALAMGIR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26753, 383, 322, 3, 'NEW IMTIAZ MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26754, 384, 128, 12, 'QAUAM ARFA MEDICAL STORE', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26755, 385, 129, 12, 'ATTIQ MEDICAL STORE', '', '', '', '', 'BHIMBER', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26756, 386, 1210, 12, 'KHURSHED MEDICAL STORE', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26757, 387, 1211, 12, 'ALTAMAS MEDICAL STORE', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26758, 388, 151, 15, 'TAHIR MEDICAL STORE', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26759, 389, 153, 15, 'YOUSAF MEDICAL STORE', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26760, 390, 154, 15, 'SHAKAR MEDICAL STORE', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26761, 391, 323, 3, 'AL REHMAN CLINIC', '', '', '', '', 'PHALIA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26762, 392, 758, 7, 'PHARMACY,MALIK HOSPITAL', '', '', '', '', 'LALAMUSA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26763, 393, 5114, 5, 'NADEEM MEDICAL STORE', '', '', '', '', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26764, 394, 273, 2, 'NEW NAEEM MEDICAL STORE', '', '', '', '', 'MANDI BHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1);
INSERT INTO `dealer_info` (`dealer_table_id`, `software_dealer_id`, `dealer_id`, `dealer_area_id`, `dealer_name`, `dealer_contact_person`, `dealer_phone`, `dealer_fax`, `dealer_address`, `dealer_address1`, `dealer_type`, `dealer_cnic`, `dealer_ntn`, `dealer_image`, `dealer_lic9_num`, `dealer_lic9_exp`, `dealer_lic10_num`, `dealer_lic10_Exp`, `dealer_lic11_num`, `dealer_lic11_Exp`, `dealer_credit_limit`, `dealer_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`, `software_lic_id`) VALUES
(26765, 395, 155, 15, 'IMRAN MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'RUKAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26766, 396, 1040, 10, 'KHURSHED MEDICAL STORE', '', '', '', '', 'FATHAPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26767, 397, 274, 2, 'PHARMACY,A.T.B.ASSOCIATION', '', '', '', '', 'MANDI BHAUDIN', 'I', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26768, 398, 522, 5, 'PHARMACY,MONGOWAL HOSPITAL', '', '', '', '', 'MONGOWAL', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26769, 399, 523, 5, 'SHAHZAD MEDICAL STORE', '', '', '', '', 'MONGOWAL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26770, 400, 548, 5, 'NOOR PHARMACY', '', '03316342822', '', 'OPP GATE#1 SABOWAL HOSPITAL', 'GUJRAT', 'Retailer', '', '', '', '1124', '0', '0', '10/19/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26771, 401, 156, 15, 'AL SHAFA MEDICAL STORE', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26772, 402, 549, 5, 'DR.MAJ.SARFRAZ', '', '', '', '', 'MAJARS LAB GUJRAT', 'Doctor', '37405-0335522-7', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26773, 403, 275, 2, 'FAREDE MEDICAL STORE', '', '', '', '', 'MANDI MHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26774, 404, 556, 5, 'PHARMACY,HASSAN HOSPITAL', '', '', '', '', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26775, 405, 2117, 2, 'KHALID MEDICAL STORE', '', '', '', '', 'GUJRAT', 'Retailer', '', '', '', '0', '275', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26776, 406, 324, 3, 'PHARMACY,HASSAN CLINIC', '', '', '', '', 'PHALIA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26777, 407, 1041, 10, 'PHARMACY,KISHWAR HOSPITAL', '', '', '', '', '', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26778, 408, 1123, 11, 'HAQBAHO MEDICAL STORE', '', '', '', '', 'KOTLA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26779, 409, 1151, 1, 'USMAN T.CO', '', '', '', '', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26780, 410, 950, 9, 'JAVAID MEDICAL STORE', '', '', '', '', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26781, 411, 157, 1, 'MEDI PLUS PHARMACY', '', '03344651557', '', 'SABOWAL PLAZA', 'GUJRAT', 'Retailer', '', '', '', '1294', '0', '0', '1/28/2016 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26782, 412, 759, 7, 'USMAN HOSPITAL', '', '', '', 'G.T.ROAD', 'LALAMUSA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26783, 413, 325, 3, 'LUCK MEDICAL STORE', '', '', '', '', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26784, 414, 557, 5, 'DR.MASUD ANWAR KHAN', '', '', '', 'PMDC# 6701-P', 'MADNI HOSPITAL GUJRAT', 'Doctor', '', '1164023-5', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26785, 415, 779, 7, 'NAZIR S.R HOSPITAL', '', '', '', '', 'LALAMUSA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26786, 416, 171, 17, 'ATTIQ MEDICAL STORE', '', '', '', '', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26787, 417, 276, 2, 'SHAFIQ MEDICAL STORE', '', '', '', '', 'MANDI BHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26788, 418, 4120, 4, 'HAMAD MEDICAL STORE', '', '', '', 'OPP D.H.Q', 'GUJRAT', 'Retailer', '', '', '', '987', '0', '0', '12/31/2013 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26789, 419, 762, 7, 'SHAH MEDICOSE', '', '', '', '', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26790, 420, 763, 7, 'IKRAM MEDICIN HOUSE', '', '', '', '', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26791, 421, 764, 7, 'FRIENDS MEDICAL STORE', '', '', '', '', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26792, 422, 765, 7, 'PHARMACY,RAHMET HOSPITAL', '', '', '', '', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26793, 423, 277, 2, 'JAVAID MEDICAL STORE', '', '', '', 'ALVI CHOWK', 'MANDI BHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26794, 424, 197, 1, 'SHUJA ALI MEDICAL STORE', '', '', '', 'KHWAJGON ROAD', 'GUJRAT', 'Retailer', '', '', '', '934', '0', '934', '12/31/2015 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26795, 425, 766, 7, 'PAKISTAN MEDICAL STORE', '', '', '', 'G.T.ROAD', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26796, 426, 1124, 11, 'PHARMACY,HADAYAT ULLAHA HOSPITAL', '', '', '', '', 'DOLATNAGAR', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26797, 427, 5302, 5, 'AL FATHA G.S', '', '', '', 'CHOWK PAKISTAN', 'GUJRAT', 'G', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26798, 428, 1155, 1, 'LATIF SONS', '', '', '', 'NAZ CHOWK', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26799, 429, 558, 5, 'NEW USMAN MEDICAL STORE', '', '0323-4765791', '', 'NEAR NOOR BAKERS,BHIMBER ROAD', 'GUJRAT', 'Retailer', '34201-7640435-1', '1539176-7', '', '0', '29130', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26800, 430, 89, 8, 'AL SHAFI MEDICAL STORE', '', '', '', 'NEAR C.M.H', 'KHARIAN CANT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26801, 431, 1042, 10, 'PHARMACY,DR.NASIM TANVER', '', '', '', '', 'JALALPUR JATTAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26802, 432, 928, 9, 'PHARMACY,ALLIED HOSPITAL', '', '', '', '', 'KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26803, 433, 929, 9, 'AFZAL MEDICAL STORE', '', '', '', 'NIA ARA MOHALLAH', 'KHARIAN CITY', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26804, 434, 930, 9, 'RAFIQ MEDICAL SERVICE', '', '', '', 'GULIANA ADDA', 'GULIANA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26805, 435, 931, 9, 'MAHMOOD MEDICAL STORE', '', '', '', '', 'KHARIAN CITY', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26806, 436, 621, 6, 'NAVEED MEDICAL STORE', '', '', '', '', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26807, 437, 413, 4, 'GADHE MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'PHARIANWALI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26808, 438, 1043, 10, 'PHARMACY,DR.MUBASHAR CLINIC', '', '', '', 'MAIN BAZAR', 'FATHAPUR', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26809, 439, 810, 8, 'LIFE MEDICAL STORE', '', '', '', 'NEAR C.M.H', 'KHARIAN CANT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26810, 440, 278, 2, 'ALI MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'MANDI BHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26811, 441, 279, 2, 'KHURSHID G.S', '', '', '', 'MAIN BAZAR', 'MANDI BHAUDIN', 'G', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26812, 442, 932, 9, 'PHARMACY,IMTIAZ HOSPITAL', '', '', '', '', 'GULIANA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26813, 443, 933, 9, 'HAMAZA MEDICAL STORE', '', '', '', '', 'GULIANA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26814, 444, 934, 9, 'IFTIKHAR MEDICAL STORE', '', '', '', '', 'GULIANA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26815, 445, 935, 9, 'AWAN MEDICAL STORE', '', '', '', '', 'GULIANA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26816, 446, 1212, 12, 'NEW VALLY MEDICAL HALL', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26817, 447, 767, 7, 'PHARMACY,SHAFA HOSPITAL', '', '', '', '', 'LALAMUSA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26818, 448, 280, 2, 'SHAH DIN MEDICAL STORE', '', '', '', '', 'MANDI BHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26819, 449, 936, 9, 'PHARMACY,KHEZAR HOSPITAL', '', '', '', '', 'KHARIAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26820, 450, 768, 7, 'PHARMACY,DR.M.ALTAF', '', '', '', 'FAIZ HOSPITAL', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26821, 451, 769, 7, 'SARGAN HOSPITAL', '', '', '', 'G.T.ROAD', 'LALAMUSA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26822, 452, 770, 7, 'ZAHID MEDICOS', '', '', '', '', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26823, 453, 336, 3, 'PHARMACY, DOCTOR CLINC WARRANTY', 'INDOOR PHARMACY', '', '', 'INDOOR PHARMACY', '', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26824, 454, 281, 2, 'POME MEDICAL STORE', '', '', '', 'D.H.Q ROAD', 'MANDI BHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26825, 455, 1044, 10, 'KASHMIR MEDICAL STORE', '', '', '', '', 'JALALPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26826, 456, 264, 2, 'PHARMACY,DR.AKHTAR JAVAID TUBASAM', '', '', '', '', 'MANDI BHAUDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26827, 457, 937, 9, 'PHARMACY,SULTAN HOSPITAL', '', '', '', '', 'KHARIAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26828, 458, 4154, 4, 'AL SHAFA MEDICAL STORE', '', '', '', 'BHIMBER ROAD', 'GUJRAT', 'Retailer', '', '', '', '858', '0', '858', '12/31/2013 12:00 AM', '12/30/1899 12:00 AM', '12/31/2013 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26829, 459, 982, 9, 'P.T.C.L,DISPENCERY', '', '', '', 'G.T.ROAD', 'GUJRAT', 'I', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26830, 460, 1158, 1, 'DR.WASEEM BUTT', '', '', '', '', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26831, 461, 771, 7, 'BUTT MEDICAL STORE', '', '', '', '', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26832, 462, 269, 2, 'AL-FALAHA MEDICAL STORE', '', '', '', '', 'MANDI BAHAUDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26833, 463, 780, 7, 'JANJUWA MEDICAL STORE', '', '', '', '', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26834, 464, 326, 3, 'DR.IJAZ MUGHAL', '', '', '', '', 'PHALIA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26835, 465, 1415, 14, 'SAKEENA MEDICAL STORE', '', '', '', '', 'SARI ALAMGER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26836, 466, 1023, 10, 'AYOUAB MEDICAL STORE', '', '', '', '', 'JALALPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26837, 467, 327, 3, 'SHAHBAZ MEDICAL STORE', '', '', '', '', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26838, 468, 430, 4, 'NEW CITY MEDICAL STORE', '', '', '', 'OOP.D.H.Q', 'GUJRAT', 'Retailer', '', '', '', '775', '0', '775', '12/31/2013 12:00 AM', '12/30/1899 12:00 AM', '12/31/2013 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26839, 469, 980, 9, 'BAHI JAN MEDICAL STORE', '', '0335-4471456', '', 'NEAR FAZAL ELLAHI G/S', 'MOH.SARDAR PURA GUJRAT', 'Retailer', '34201-3270853-3', '', '', '0', '7197', '0', '12/31/2015 12:00 AM', '10/30/2019 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26840, 470, 969, 9, 'DR.SYED MUZAMAL HUSSAIN', '', '', '', 'PMDC# 9865-P', 'MUZAMIL EYE CLINIC GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26841, 471, 328, 3, 'FAMILY HOSPITAL', '', '', '', '', 'PHALIA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26842, 472, 329, 3, 'DR,PARVAIZ NAZEER TARAR', '', '', '', '', 'PHALIA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26843, 473, 53, 5, 'KARIMI CLINIC', '', '', '', '', 'KUNJAH', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26844, 474, 1416, 14, 'ZAHID MEDICOS', '', '', '', '', 'SARI ALAMGER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26845, 475, 1417, 14, 'AL UMAR MEDICAL STORE', '', '', '', '', 'SARI ALAMGER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26846, 476, 537, 5, '.MALIK PHARMACY', '', '03006209020', '', 'OPP.GUJRAT HOSPITAL', 'BHIMBER ROAD GUJRAT', 'Retailer', '', '', '', '30677', '0', '0', '3/3/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26847, 477, 162, 16, 'AZAM MEDICAL STORE', '', '', '', '', 'KARINWALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26848, 478, 543, 5, 'DR.M.ARSHAD', '', '', '', '', 'CHANAB HOSPITAL GUJRAT', 'Doctor', '', '2528048', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26849, 479, 170, 1, 'DR.USMAN NAEEM', '', '03348064306', '', '', 'HAMEDA BAGAM Hp GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26850, 480, 510, 5, 'PHARMACY,DR.ARSHAD CHISHTE', '', '', '', '', 'KUNJAH', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26851, 481, 173, 17, 'JET MEDICAL STORE', '', '', '', 'MUSLIM BAZAR', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26852, 482, 1750, 17, 'AL RIAZ MEDICAL STORE', '', '', '', '', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26853, 483, 1026, 10, 'BASHARAT MEDICAL & G.S', '', '', '', 'MAIN TANDA ROAD', 'JALALPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26854, 484, 86, 8, 'FARAH MEDICOSE', '', '', '', 'C.M.H', 'KHARIAN CANT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26855, 485, 330, 3, 'ALI MEDICAL STORE', '', '', '', '', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26856, 486, 1130, 1, 'GONDAL M/C', '', '', '', '', 'GUJRANWALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26857, 487, 1418, 14, 'MARSAB MEDICAL STORE', '', '', '', 'G.T.ROAD', 'SARI ALAMGIR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26858, 488, 1045, 10, 'BUTT MEDICAL STORE', '', '', '', '', 'JALALPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26859, 489, 1125, 11, 'IMTIAZI MEDICAL STORE', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26860, 490, 1126, 11, 'KHURSHED MEDICAL STORE', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26861, 491, 1127, 11, 'QAYYUM ARFA MEDICAL STORE', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26862, 492, 1228, 12, 'VALLY MEDICAL STORE', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26863, 493, 1128, 11, 'KASHMIR VALLY MEDICAL STORE', '', '', '', '', 'BHUMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26864, 494, 1129, 11, 'PHARMACY,MOH.DIN MEMORIAL HOSPITAL', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26865, 495, 282, 2, 'PHARMACY,ANWAR FATIMA HOSPITAL', '', '', '', '', 'MANDI BHAUDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26866, 496, 2169, 2, 'PHARMACY.DR.ABDUL SATTAR', '', '', '', 'G.T.ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26867, 497, 772, 7, 'DR,NAEEM AKHTAR JANJUWA', '', '', '', 'G.T.ROAD', 'LALAMUSA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26868, 498, 398, 3, 'KHAN MEDICAL STORE', '', '', '', 'KABLI GATE', 'GUJRAT', 'Retailer', '', '', '', '895', '895', '895', '12/31/2013 12:00 AM', '12/31/2013 12:00 AM', '12/31/2013 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26869, 499, 582, 5, 'AL HAQ MEDICAL STORE', '', '03006233036', '', 'NEAR ALEENA CENTER', 'BHIMBER ROAD GUJRAT', 'Retailer', '', '', '', '0', '27756', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26870, 500, 1419, 14, 'TARIQ MEDICAL STORE', '', '', '', 'VILLAGE', 'KHAMBI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26871, 501, 967, 9, '.WAKEEL MEDICAL STORE', '', '', '', 'SARGODHA ROAD', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '0', '12/31/2017 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26872, 502, 746, 7, 'SIRAJ MEDICAL STORE', '', '', '', 'GALI QULFA WALI', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26873, 503, 3111, 3, 'JET MEDICINE & SURGI', '', '', '', 'EAST CIRCULAR ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26874, 504, 1420, 14, 'PHARMACY,NAZEER BAGUM M.H', '', '', '', '', 'SARI ALALMGIR', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26875, 505, 262, 2, 'MANGET MEDICAL STORE', '', '', '', '', 'MANDI BAHAUDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26876, 506, 627, 6, 'YASIR MEDICAL STORE', '', '', '', '', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26877, 507, 628, 6, 'MAZHAR MEDICAL STORE', '', '', '', '', 'CHELLIANWALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26878, 508, 760, 7, 'PHARMACY,DR.ZAFAR AMIN NIAZI', '', '', '', 'G.T.ROAD', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26879, 509, 2525, 25, 'KAZMI MEDICAL STORE', '', '', '', '', 'CHELLIANWALI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26880, 510, 41, 4, 'BILAL HOSPITAL', '', '', '', '', 'PAHRIANWALI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26881, 511, 414, 4, 'WALLEE SURGCAL HOSPITAL', '', '', '', '', 'PAHRIANWALI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26882, 512, 755, 7, 'GOSEEA MEDICAL STORE', '', '', '', 'KHARIAN ROAD', 'BHADDAR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26883, 513, 158, 15, 'AL GONDAL MEDICAL STORE', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26884, 514, 422, 42, 'IMRAN MEDICAL STORE', '', '', '', '', 'RUKKAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26885, 515, 331, 3, 'PHARMACY,DR.KHALIQ DAD TARAR', '', '', '', '', 'PHALIA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26886, 516, 2710, 27, 'NADEEM MEDICAL STORE', '', '', '', '', 'MALIK WALL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26887, 517, 332, 3, 'IRSHAD TRADAR', '', '', '', '', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26888, 518, 1131, 11, 'QAYYUM BROTHERS', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26889, 519, 2711, 27, 'KHAWAJA MEDICAL STORE', '', '', '', '', 'MALIK WALL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26890, 520, 159, 15, 'SHAKH MEDICAL STORE', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26891, 521, 283, 2, 'SHEIKH MEDICAL STORE', '', '', '', '', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26892, 522, 284, 2, 'PHARAMCY,DR.M,RAMZAN PARVEZ', '', '', '', '', 'MANDI BAHA U DDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26893, 523, 285, 28, 'AL SHAFA MEDICAL STORE', '', '', '', '', 'RUKKAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26894, 524, 288, 28, 'OWAIS MEDICAL STORE', '', '', '', '', 'GOJRA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26895, 525, 289, 28, 'ABDUL MEDICAL STORE', '', '', '', '', 'GOJRA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26896, 526, 2810, 28, 'SHOKAT MEDICAL STORE', '', '', '', '', 'GOJRA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26897, 527, 2811, 28, 'SHEIKH MEDICAL STORE', '', '', '', '', 'GOJRA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26898, 528, 1421, 14, 'PHARMACY,DR.SADHEER SULTAN', '', '', '', '', 'SARI ALAMGIR', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26899, 529, 1132, 11, 'NISAR MEDICAL STORE', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26900, 530, 2812, 28, 'ZOKE MEDICAL STORE', '', '', '', '', 'GOJRA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26901, 531, 2813, 28, 'BISMILLAH MEDICAL STORE', '', '', '', '', 'GOJRA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26902, 532, 1510, 15, 'ADAM ZAADA MEDICAL STORE', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26903, 533, 1511, 15, 'ASLAM MEDICAL STORE', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26904, 534, 761, 7, 'AWAME MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26905, 535, 1512, 15, 'ALI MEDICAL CENTER', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26906, 536, 2814, 28, 'GONDAL MEDICAL STORE', '', '', '', '', 'RUKKAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26907, 537, 2712, 27, 'BHATTI MEDICAL STORE', '', '', '', '', 'MALIK WALL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26908, 538, 176, 1, 'DR.WASEEM BUTT', '', '', '', '', 'SHAH JAHANGIR ROAD GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26909, 539, 2713, 27, 'DASTAGEER MEDICAL STORE', '', '', '', '', 'MALIK WALL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26910, 540, 2815, 28, 'AFTAB SURGICAL HOSPITAL', '', '', '', '', 'GOJRA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26911, 541, 1133, 11, 'WAHAB HOSPITAL', '', '', '', 'BHIMBER ROAD', 'KOTLA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26912, 542, 2714, 27, 'MADINA MEDICAL STORE', '', '', '', '', 'MALIK WALL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26913, 543, 2715, 27, 'GOSEEA MEDICAL STORE', '', '', '', '', 'MALIK WALL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26914, 544, 1513, 15, 'AL-SHAMS MEDICAL STORE', '', '', '', 'THANA ROAD', 'KUTHIALA SHEKHAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26915, 545, 1134, 11, 'RIZWAN MEDICAL STORE', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26916, 546, 938, 9, 'MADANI HOSPITAL', '', '', '', '', 'KHARIAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26917, 547, 2160, 2, 'PHARMACY,DR.ZAMEER HOSPITAL', '', '', '', 'G.T.ROAD', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26918, 548, 1135, 11, 'PHARMACY,AZAM HOSPITAL', '', '', '', '', 'KOTLA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26919, 549, 939, 9, 'PHARMACY.MINHAS HOSPITAL', '', '', '', 'DINGA ROAD DHORIA', 'KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26920, 550, 1046, 10, 'IHSAN MEDICAL HALL', '', '', '', 'TANDA CHOWK', 'JALALPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26921, 551, 2128, 2, 'MEHR BROTHERS', '', '', '', 'SARGODHA ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26922, 552, 773, 7, 'FARRUKH MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26923, 553, 1422, 14, 'SALEEM MEDICAL STORE', '', '', '', '', 'SARI ALAMGIR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26924, 554, 951, 9, 'PHARMACY,AL HAMAD H.S', '', '', '', 'SHADOLA ROAD', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26925, 555, 5172, 5, 'MUSLIM G/S', '', '', '', '', 'GUJRAT', 'G', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26926, 556, 1514, 15, 'NOMAN MEDICAL STORE', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26927, 557, 1515, 15, 'ALI SURGICAL HOSPITAL', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26928, 558, 333, 3, 'PUNJAB MEDICAL STORE', '', '', '', '', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26929, 559, 2816, 28, 'DR,SAGEER HASSAN', '', '', '', '', 'GOJRA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26930, 560, 287, 2, 'PHARMACY,UMAR HOSPITAL', '', '', '', '', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26931, 561, 561, 5, 'DR.HAFEEZ-UR-REHMAN', '', '', '', 'HAFEEZ CLINIC', 'BHIMBER ROAD GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26932, 562, 1136, 11, 'KHALID BAKAR', '', '', '', '', 'KOTLA', 'G', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26933, 563, 811, 8, 'PHARMA CARE MEDICAL STORE', '', '', '', '', 'KHARIAN CANT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26934, 564, 940, 9, 'PHARMACY,AMIN CLINIC', '', '', '', '', 'KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26935, 565, 1137, 11, 'REHMANIA MEDICAL STORE', '', '', '', '', 'BADHAR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26936, 566, 334, 3, 'AL FREED MEDICAL STORE', '', '', '', '', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26937, 567, 816, 8, 'DR.MIRZA NASEER AHMED', '', '', '', 'PMDC# 15417-P', 'NEAR POLICE CHOKI GREEN TOWN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26938, 568, 1423, 14, 'PHARMACY,ZAINAB HOSPITAL', '', '', '', 'G.T.ROAD', 'SARI ALAMGIR', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26939, 569, 774, 7, 'PHARMACY,DR.SHAKILA AKHTAR H/S', '', '', '', '', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26940, 570, 290, 2, 'AL SHAFA HOSPITAL', '', '', '', '', 'MANDI BAHUDDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26941, 571, 941, 9, 'IDEAL BAKAR', '', '', '', '', 'KHARIAN', 'G', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26942, 572, 1424, 14, 'HAMAZA MEDICAL STORE', '', '', '', '', 'SARI ALAMGIR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26943, 573, 775, 7, 'PHARMACY,MAHAM CARE & CURE HOSPITAL', '', '', '', '', 'LALAMUSA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26944, 574, 1138, 11, 'USAMAN GHANI MEDICAL & G.S', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26945, 575, 821, 8, 'DR.SALEEM MALIK', '', '', '', 'JAMAL PUR ROAD', 'GUJRAT', 'Doctor', '34201-6454941-3', '0829243-4', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26946, 576, 942, 9, 'PHARMACY,DR.SAJJADA HAFIZ CLINIC', '', '', '', 'G.T.ROAD', 'KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26947, 577, 291, 2, 'PHARMACY,AL GAHNI HOSPITAL', '', '', '', '', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26948, 578, 1139, 11, 'PHARMACY,SKIN VISION CLINIC', '', '', '', 'DR,SALMAN CH', 'KOTLA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26949, 579, 1140, 11, 'PHARMACY,MUBEEN SURGICAL HOSPITAL', '', '', '', 'BHIMBER ROAD', 'KOTLA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26950, 580, 1141, 11, 'PHARMACY,M.ALAM HOSPITAL', '', 'KOTLA', '', '', 'BHIMBER ROAD', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26951, 581, 5163, 5, 'PHARMACY,DR,SYED MAZAHAR ABBAS', '', '', '', 'PAKISTAN CHOWK', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26952, 582, 812, 8, 'PHARMACY,SAFIA IQBAL CLINIC', '', '', '', '', 'KHARIAN CANT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26953, 583, 776, 7, 'BISMILLAH PHARMACY', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26954, 584, 1164, 1, 'SOCIAL SECURITY HOSPITAL', '', '', '', 'G.T.ROAD', 'GUJRAT', 'I', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26955, 585, 292, 29, 'AL REHMAN MEDICAL STORE', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26956, 586, 293, 29, 'YOUSAF MEDICAL STORE', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26957, 587, 294, 29, 'SHAKAR MEDICAL STORE', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26958, 588, 296, 29, 'AL SHAFA MEDICAL STORE', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26959, 589, 297, 29, 'ASHRAF MEDICAL STORE', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26960, 590, 298, 29, 'AL GONDAL MEDICAL STORE', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26961, 591, 299, 29, 'SHAIKH MEDICAL STORE', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26962, 592, 2910, 29, 'ADAM ZADA MEDICAL STORE', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26963, 593, 2911, 29, 'ASLAM MEDICAL STORE', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26964, 594, 2912, 29, 'ALI MEDICAL CENTER', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26965, 595, 2913, 29, 'AL SHAMAS MEDICAL STORE', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26966, 596, 2914, 29, 'NOMAN MEDICAL STORE', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26967, 597, 2915, 29, 'ALI SURGICAL HOSPITAL', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1);
INSERT INTO `dealer_info` (`dealer_table_id`, `software_dealer_id`, `dealer_id`, `dealer_area_id`, `dealer_name`, `dealer_contact_person`, `dealer_phone`, `dealer_fax`, `dealer_address`, `dealer_address1`, `dealer_type`, `dealer_cnic`, `dealer_ntn`, `dealer_image`, `dealer_lic9_num`, `dealer_lic9_exp`, `dealer_lic10_num`, `dealer_lic10_Exp`, `dealer_lic11_num`, `dealer_lic11_Exp`, `dealer_credit_limit`, `dealer_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`, `software_lic_id`) VALUES
(26968, 598, 2020, 20, 'SHAN MEDICAL STORE', '', '', '', 'DINGA CHAWK', 'KUNJAH', 'Retailer', '', '', '', '443', '0', '0', '12/31/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26969, 599, 205, 20, 'BHATTI MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'KUNJAH', 'Retailer', '34201-0379904-1', '6616816-7', '', '0', '47447', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26970, 600, 2019, 20, 'SHAHID MEDICAL STORE', '', '', '', 'OPP R.H.C', 'KUNJAH', 'Retailer', '', '', '', '682', '0', '683', '12/31/2015 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26971, 601, 207, 20, 'DR.ARSHAD CHISHTE', '', '', '', 'MAIN BAZAR', 'KUNJAH', 'Doctor', '34201-0308343-7', '1698230-4', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26972, 602, 2023, 20, '.DR.ARIF MASOOD', '', '', '', 'MAIN BASAR', 'KUNJAH', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26973, 603, 206, 20, 'BISMILLAH MEDICAL STORE', '', '', '', 'NEAR NAVEED KARYANA STORE', 'MAIN BAZAR KUNJAH', 'Retailer', '34201-0440386-1', '7602732-0', '', '9900', '454', '0', '11/30/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26974, 604, 2014, 20, 'FATAH KHAN MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'KUNJAH', 'Retailer', '', '', '', '0', '1152', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26975, 605, 2013, 20, 'EJAZ MEDICAL STORE', '', '', '', 'NEAR KAKA SWEET HOUSE', 'MAIN BAZAR KUNJAH', 'Retailer', '34201-4014366-3', '8981417-2', '', '0', '9187', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26976, 606, 2717, 27, 'PHARMACY,ROMA SURGICAL HOSPITAL', '', '', '', '', 'MONGOWAL', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26977, 607, 2122, 21, '.WARACIH MEDICAL STORE', '', '', '', 'MANDI ROAD', 'MONGOWAL', 'Retailer', '', '', '', '0', '465', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26978, 608, 2113, 21, 'DR.SYED ZUKI UL HASSAN', '', '', '', 'QUBRA HOSPITAL MONGOWAL', 'PMDC# 29755-P', 'Doctor', '', '', '', '29755', '0', '0', '12/31/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26979, 609, 2112, 21, 'DR.SHERAZ MALIK', '', '', '', '', 'MONGOWAL SURGICAL Hp MONGOWAL', 'Doctor', '34201-0551486-1', '3557958-7', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26980, 610, 2121, 21, 'SHAHZAD LATIF PHARMACY', '', '', '', 'MAIN BAZAR', 'MONGOWAL GHARBI', 'Retailer', '', '', '', '32601', '0', '0', '5/19/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26981, 611, 301, 30, 'TARIQ MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26982, 612, 302, 30, 'EJAZ MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26983, 613, 303, 30, 'PUNJAB MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26984, 614, 304, 30, 'SHAHBAZ MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26985, 615, 305, 30, 'KHURSHID MEDICAL STORE', '', '', '', '', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26986, 616, 306, 30, 'ZAHEER MEDICAL STORE', '', '', '', '', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26987, 617, 307, 30, 'KASHMIR MEDICAL STORE', '', '', '', '', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26988, 618, 308, 30, 'HASSAN MEDICAL STORE', '', '', '', '', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26989, 619, 309, 30, 'NAVEED MEDICAL STORE', '', '', '', '', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26990, 620, 3010, 30, 'MAJEED MEDICAL STORE', '', '', '', '', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26991, 621, 3011, 30, 'YASIR MEDICAL STORE', '', '', '', '', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26992, 622, 3015, 30, 'PHARMACY,AL MUMTAZ HOSPITAL', '', '', '', '', 'DINGA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26993, 623, 3016, 30, 'PHARMACY,KHALEEL HOSPITAL', '', '', '', '', 'DINGA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26994, 624, 3017, 30, 'PHARMACY,KINZA SURGICAL HOSPITAL', '', '', '', '', 'DINGA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26995, 625, 3018, 30, 'PHARMACY,DR.KHALID ARBBE', '', '', '', '', 'DINGA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26996, 626, 3019, 30, 'PHARMACY,AMER JAVAID COMPLEX', '', '', '', '', 'DINGA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26997, 627, 3020, 30, 'PHARMACY,AL SHEFA HOSPITAL', '', '', '', '', 'DINGA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26998, 628, 3021, 30, 'PHARMACY,DR.SHAHNAZ HOSPITAL', '', '', '', '', 'DINGA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(26999, 629, 423, 42, 'MANGAT MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'PHARIANWALI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27000, 630, 421, 42, 'IMTIAZ MEDICAL STORE', '', '', '', 'T.H.Q', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27001, 631, 2410, 24, 'SHARIF MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27002, 632, 2411, 24, 'NISAR MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27003, 633, 2413, 24, 'FEIZAN MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27004, 634, 2414, 24, 'NASEER MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27005, 635, 2415, 24, 'TABASUM MEDICAL STORE', '', '', '', 'MANDI ROAD', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27006, 636, 2416, 24, 'SHAHEEN MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27007, 637, 2417, 24, 'MADINA MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27008, 638, 2418, 24, 'ANMOL MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27009, 639, 2412, 24, 'AL SHAFA MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27010, 640, 2419, 24, 'LUCK MEDICAL STORE', '', '', '', '', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27011, 641, 2420, 24, 'ALI MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27012, 642, 2421, 24, 'IRSHAD TRADAR', '', '', '', '', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27013, 643, 2422, 24, 'PUNJAB MEDICAL STORE', '', '', '', '', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27014, 644, 2423, 24, 'PHARMACY,PHALIA M/C', '', '', '', '', 'PHALIA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27015, 645, 2426, 24, 'PHARMACY,AWAN CLINIC', '', '', '', '', 'PHALIA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27016, 646, 2427, 24, 'PHARMACY,AL REHMAN CLINIC', '', '', '', '', 'PHALIA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27017, 647, 2428, 24, 'ZAFAR MEDICAL STORE', '', '', '', '', 'PHALIA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27018, 648, 2429, 24, 'PHARMACY,FAMILY HOSPITAL', '', '', '', '', 'PHALIA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27019, 649, 2430, 24, 'PHARMACY,DR.PARVAIZ NAZEER TARAR', '', '', '', '', 'PHALIA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27020, 650, 2431, 24, 'PHARMACY,DR.KHALIQ DAD TARAR', '', '', '', '', 'PHALIA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27021, 651, 202, 20, 'AHSAN MEDICAL STORE', '', '', '', 'OPP.DHQ', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27022, 652, 203, 20, 'AL GONDAL MEDICAL STORE', '', '', '', 'OPP.DHQ', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27023, 653, 204, 20, 'AL MUSLIM MEDICAL STORE', '', '', '', 'OPP.DHQ', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27024, 654, 208, 20, 'KHANNAN PHARMACY', '', '', '', 'OPP.DHQ', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27025, 655, 209, 20, 'NOOR MEDICAL STORE', '', '', '', 'OPP.DHQ', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27026, 656, 2051, 20, 'PEOPLE,S PHARMACY', '', '', '', 'OPP.DHQ', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27027, 657, 2010, 20, 'PEOPLE,S PHARMACY', '', '', '', 'OPP.DHQ', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27028, 658, 2011, 20, 'REHMAT MEDICAL STORE', '', '', '', 'OPP.DHQ', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27029, 659, 2012, 20, 'SHAH DIN MEDICAL STORE', '', '', '', 'OPP.DHQ', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27030, 660, 2021, 20, 'PUNJAB MEDICAL STORE', '', '', '', 'SADDAR BAZAR', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27031, 661, 2022, 20, 'MIAN MEDICAL STORE', '', '', '', 'COMMETTE BAZAR', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27032, 662, 2015, 20, 'HAFIZ MEDICAL STORE', '', '', '', 'OPP.DHQ', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27033, 663, 2024, 20, 'AWAME MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27034, 664, 2025, 20, 'NEW SUPPER MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27035, 665, 2026, 20, 'FAIZ MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27036, 666, 2027, 20, 'MANSOOR MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27037, 667, 2028, 20, 'MUMTAZ MEDICAL STORE', '', '', '', 'PHALIA ROAD', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27038, 668, 2029, 20, 'PHARMACY,SALEEM HOSPITAL', '', '', '', 'D.H.Q ROAD', 'MANDI BAHUDDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27039, 669, 2030, 20, 'PAK MEDICAL STORE', '', '', '', 'PHALIA ROAD', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27040, 670, 2035, 20, 'MIAN MEDICINE', '', '', '', 'KACHERY ROAD', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27041, 671, 2036, 20, 'MOON MEDICAL STORE', '', '', '', 'KACHERY ROAD', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27042, 672, 2037, 20, 'ARAIB MEDICAL STORE', '', '', '', 'KACHERY ROAD', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27043, 673, 2038, 20, 'HASSAN MEDICAL STORE', '', '', '', 'KACHERY ROAD', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27044, 674, 2031, 20, 'KHALEEL MEDICAL STORE', '', '', '', 'PHALIA ROAD', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27045, 675, 2039, 20, 'MEDICEN PLUS PHARMACY', '', '', '', 'KACHERY ROAD', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27046, 676, 2040, 20, 'SHAHEEN MEDICAL STORE', '', '', '', 'KACHERY ROAD', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27047, 677, 2041, 20, 'NASIR MEDICAL STORE', '', '', '', 'RASOOL ROAD', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27048, 678, 2016, 20, 'NEW NAEEM MEDICAL STORE', '', '', '', 'OPP.DHQ', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27049, 679, 2042, 20, 'AL GHANI CLINIC', '', '', '', '', 'MANDI BAHUDDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27050, 680, 2043, 20, 'SHAIKH MEDICAL STORE', '', '', '', 'RASOOL ROAD', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27051, 681, 2032, 20, 'ABUBAKAR MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27052, 682, 2044, 20, 'AL SHAFA MEDICAL STORE', '', '', '', 'KACHERY ROAD', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27053, 683, 2045, 20, 'AL FALAHA MEDICAL STORE', '', '', '', 'KACHERY ROAD', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27054, 684, 2046, 20, 'AL REHMAN MEDICAL STORE', '', '', '', '', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27055, 685, 2033, 20, 'ALI MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27056, 686, 2047, 20, 'AZEEM MEDICAL STORE', '', '', '', 'DINGA ROAD', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27057, 687, 2048, 20, 'CITY MEDICAL STORE', '', '', '', 'KACHERY ROAD', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27058, 688, 2055, 20, 'PARADAIZ MEDICAL STORE', '', '', '', 'PHALIA ROAD', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27059, 689, 2056, 20, 'JAVAID MEDICAL STORE', '', '', '', 'OOP GOVT DEGREE COLLEGE', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27060, 690, 2057, 20, 'KHAWAJA MEDICAL STORE', '', '', '', 'PHALIA ROAD', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27061, 691, 2058, 20, 'ABID MEDICAL STORE', '', '', '', 'KING CHOWK', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27062, 692, 2059, 20, 'STANDERD MEDICAL STORE', '', '', '', 'KING CHOWK', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27063, 693, 4165, 41, 'PHARMACY,RAKHAT HOSPITAL', '', '', '', 'PHALIA ROAD', 'MANDI BAHUDDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27064, 694, 4166, 41, 'PHARMACY,TARAR HOSPITAL', '', '', '', 'PHALIA ROAD', 'MANDI BAHUDDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27065, 695, 2067, 20, 'PHARMACY,ALVI HOSPITAL', '', '', '', 'PHALIA ROAD', 'MANDI BAHUDDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27066, 696, 2068, 20, 'PHARMACY,AKHTAR JAVAID TUBASAM', '', '', '', '', 'MANDI BAHUDDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27067, 697, 2069, 20, 'PHARMACY,ARSHAD HRT CENTER', '', '', '', '', 'MANDI BAHUDDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27068, 698, 2070, 20, 'PHARMACY,CITY HOSPITAL', '', '', '', '', 'MANDI BAHUDDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27069, 699, 2071, 20, 'PHARMACY,CHEEMA HOSPITAL', '', '', '', '', 'MANDI BAHUDDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27070, 700, 2072, 20, 'PHARMACY,GENRAL HOSPITAL', '', '', '', '', 'MANDI BAHUDDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27071, 701, 2073, 20, 'GONDAL MEDICAL STORE', '', '', '', 'RASOOL ROAD', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27072, 702, 2074, 20, 'PHARMACY,M.AKRAM HOSPITAL', '', '', '', 'RASOOL ROAD', 'MANDI BAHUDDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27073, 703, 2075, 20, 'PHARMACY,SHATIC HOSPITAL', '', '', '', '', 'MANDI BAHUDDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27074, 704, 2076, 20, 'PHARMACY,TARIQ HOSPITAL', '', '', '', 'DINGA ROAD', 'MANDI BAHUDDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27075, 705, 2077, 20, 'PHARMACY,UMER HOSPITAL', '', '', '', '', 'MANDI BAHUDDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27076, 706, 2078, 20, 'PHARMACY,NABEEL SR,HOSPITAL', '', '', '', '', 'MANDI BAHUDDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27077, 707, 2079, 20, 'PHARMACY,AL KARAM HOSPITAL', '', '', '', '', 'MANDI BAHUDDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27078, 708, 2080, 20, 'PHARMACY,HASHMI HEART CENTER', '', '', '', '', 'MANDI BAHUDDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27079, 709, 2081, 20, 'PHARMACY,FAMALY CARE HOSPITAL', '', '', '', '', 'MANDI BAHUDDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27080, 710, 2082, 20, 'PHARMACY.A.T.B ASSOCIATION', '', '', '', 'D.H.Q ROAD', 'MANDI BAHUDDIN', 'I', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27081, 711, 2017, 20, 'ASIF MEDICAL STORE', '', '', '', 'NEAR DHQ', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27082, 712, 2060, 20, 'ITTFAQ MEDICAL STORE', '', '', '', 'KING CHOWK', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27083, 713, 1330, 13, 'DR.SAMARA MOBEEN', '', '', '0314-4171717', 'PMDC# 3737-AJK', 'LIFE CARE CLINIC KOTLA', 'Doctor', '', '', '', '3737', '0', '0', '12/31/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27084, 714, 1320, 13, 'KHAN PHARMACY', '', '0303-9596018', '', 'MAIN BAZAR', 'KOTLA', 'Retailer', '', '', '', '45340', '0', '0', '9/18/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27085, 715, 1321, 13, 'SALEEM & SONS MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'KOTLA ARAB ALI KHAN', 'Retailer', '34202-0645938-1', '7121274-6', '', '309', '0', '309', '12/31/2017 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27086, 716, 3110, 31, 'HUSSAIN MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'KOTLA', 'Retailer', '', '', '', '4112', '0', '0', '5/8/2012 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27087, 717, 1322, 13, 'HAFIZ MEDICAL & GENERAL STORE', '', '0300-6218617', '', 'BHIMBER ROAD', 'KOTLA ARAB ALI KHAN', 'Retailer', '34202-8343408-5', 'A043241-5', '', '842', '0', '2', '12/31/2017 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27088, 718, 1323, 13, '.IQBAL MEDICAL STORE', '', '0301-6269107', '', 'OPP.MCB BHIMBER ROAD', 'KOTLA', 'Retailer', '34202-0763828-1', '6284024-8', '', '0', '3459', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27089, 719, 1326, 13, 'HAQBAHO MEDICAL STORE', '', '', '', 'BHIMBER ROAD', 'KOTLA ARAB ALI KHAN', 'Retailer', '', '', '', '764', '0', '764', '12/31/2015 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27090, 720, 3130, 31, 'KHALID BAKAR', '', '', '', 'BHIMBER ROAD', 'KOTLA', 'G', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27091, 721, 1334, 13, 'DR.IFTIKHAR', '', '', '', 'ALAM HOSPITAL', 'KOTLA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27092, 722, 1335, 13, 'DR.AMANAT', '', '', '', '', 'SUNNAN HOSPITAL KOTLA', 'Doctor', '34201-6342240-5', '0997941-7', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27093, 723, 1336, 13, 'DR.EHSAAN', '', '', '', '', 'NAWAB HOSPITAL KOTLA', 'Doctor', '3420104062175', '1110679-4', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27094, 724, 1327, 13, 'SARWAR PHARMACY', '', '0331-5201003', '', 'LANGRIAL CHAWK', 'KOTLA ARAB ALI KHAN', 'Retailer', '', '', '', '7361', '0', '0', '9/18/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27095, 725, 3123, 31, 'HAMZA MEDICAL STORE', '', '', '', 'NEAR BHU BANDAHAR', 'T,KHARIAN', 'Retailer', '', '', '', '721', '0', '0', '12/31/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27096, 726, 1337, 13, 'DR.ABDUL WAHAB', '', '', '', 'WAHAB HOSPITAL', 'KOTLA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27097, 727, 1338, 13, 'DR.COL.AZAM', '', '', '', 'AZAM HOSPITAL', 'KOTLA ARAB ALI KHAN', 'Doctor', '34201-6628476-3', '4177580-5', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27098, 728, 1310, 13, 'REHMANIA MEDICAL DTORE', '', '', '', '', 'BADHAR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27099, 729, 1339, 13, 'PHARMACY,ZAINAB HOSPITAL', '', '', '', '', 'KOTLA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27100, 730, 3137, 31, 'TAHIR MEDICAL STORE', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27101, 731, 3131, 31, 'KASHMIR VALLY MEDICAL STORE', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27102, 732, 3132, 31, 'NISAR MEDICAL STORE', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27103, 733, 3141, 31, 'KHURSUHED MEDICAL STORE', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27104, 734, 351, 35, 'SAKEENA MEDICAL STORE', '', '', '', '', 'SARI ALAMGER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27105, 735, 352, 35, 'ANSAR MEDICAL STORE', '', '', '', '', 'SARI ALAMGER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27106, 736, 353, 35, 'REHAN MEDICAL STORE', '', '', '', '', 'SARI ALAMGER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27107, 737, 354, 35, 'MARSAB MEDICAL STORE', '', '', '', '', 'SARI ALAMGER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27108, 738, 355, 35, 'YASIR MEDICAL STORE', '', '', '', '', 'SARI ALAMGER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27109, 739, 356, 35, 'AL SHAFA MEDICOSE', '', '', '', 'MAIN BAZAR', 'SARI ALAMGER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27110, 740, 357, 35, 'JAVAID MEDICAL STORE', '', '', '', '', 'SARI ALAMGER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27111, 741, 358, 35, 'TARIQ MEDICAL STORE', '', '', '', 'NEAR RAILWAY LINE', 'SARI ALAMGER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27112, 742, 359, 35, 'ANWAR MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'SARI ALAMGER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27113, 743, 3510, 35, 'MADINA MEDICAL STORE', '', '', '', 'NEAR RAILWAY LINE', 'SARI ALAMGER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27114, 744, 3511, 35, 'NEW IQBAL MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'SARI ALAMGER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27115, 745, 3512, 35, 'KHALID MEDICAL STORE', '', '', '', '', 'SARI ALAMGER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27116, 746, 3513, 35, 'HAMAZA MEDICAL STORE', '', '', '', '', 'SARI ALAMGER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27117, 747, 3514, 35, 'TARIQ MEDICAL STORE', '', '', '', '', 'KHAMBI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27118, 748, 3520, 35, 'PHARMACY,ZAINAB HOSPITAL', '', '', '', '', 'SARI ALAMGER', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27119, 749, 3521, 35, 'PHARMACY,NAZEER BAGUM HOSPITAL', '', '', '', '', 'SARI ALAMGER', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27120, 750, 401, 40, 'SAKEENA MEDICAL STORE', '', '', '', 'G.T.ROAD', 'SARI ALAMGIR', 'Retailer', '', '', '', '813', '0', '813', '12/31/2015 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27121, 751, 402, 40, 'REHAN MEDICOSE', '', '03215915565', '', 'MAIN BAZAR', 'SARI ALAMGIR', 'Retailer', '', '', '', '638', '0', '638', '12/31/2015 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27122, 752, 403, 40, 'YASIR MEDICAL STORE', '', '03455677657', '', 'MAIN BAZAR', 'SARI ALAMGIR', 'Retailer', '', '', '', '877', '0', '877', '12/31/2015 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27123, 753, 404, 40, 'ANSAR MEDICOSE', '', '0544651841', '', 'SADAT MARKET', 'SARI ALAMGIR', 'Retailer', '', '', '', '211', '211', '211', '12/31/2015 12:00 AM', '12/31/2015 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27124, 754, 405, 40, 'MIRZA PHARMACY', '', '', '', 'NEAR U.B.L', 'SARI ALAMGIR', 'Retailer', '', '', '', '1147', '0', '0', '3/8/2014 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27125, 755, 406, 40, 'AL SHAFA MEDICOSE', '', '', '', '', 'SARI ALAMGIR', 'Retailer', '', '', '', '573', '573', '573', '12/31/2013 12:00 AM', '12/31/2013 12:00 AM', '12/31/2013 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27126, 756, 407, 40, 'JAVAID MEDICAL STORE', '', '03435742386', '', 'RAILWAY CROSSING', 'SARI ALAMGIR', 'Retailer', '', '', '', '0', '337', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27127, 757, 408, 40, 'TARIQ MEDICAL STORE', '', '0544650228', '', 'NEAR RAILWAY LINE', 'SARI ALAMGIR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27128, 758, 409, 40, 'PHARMACY,KHUSHHAL HOSPITAL', '', '', '', '', 'SARI ALAMGIR', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27129, 759, 4010, 40, 'ANWAR MEDICAL STORE', '', '03455696092', '', 'NEAR RAILWAY CROSSING', 'SARI ALAMGIR', 'Retailer', '', '', '', '210', '0', '0', '12/31/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27130, 760, 4011, 40, 'NEW IQBAL MEDICOSE', '', '03455722955', '', 'MAIN BAZAR', 'SARI ALAMGIR', 'Retailer', '', '', '', '698', '0', '698', '12/31/2015 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27131, 761, 4012, 40, 'KHALID MEDICOSE', '', '0544653326', '', 'MAIN BAZAR', 'SARI ALAMGIR', 'Retailer', '', '', '', '696', '696', '696', '12/31/2015 12:00 AM', '12/31/2015 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27132, 762, 4013, 40, 'TARIQ MEDICAL STORE', '', '', '', '', 'KHAMBI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27133, 763, 4020, 40, 'PHARMACY,ZANIB HOSPITAL', '', '', '', '', 'SARI ALAMGIR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27134, 764, 4021, 40, 'PHARMACY,NAZEER BAGUM HOSPITAL', '', '', '', '', 'SARI ALAMGIR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27135, 765, 382, 38, '.', '', '', '', '', 'KHARIAN CANT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27136, 766, 1910, 19, 'NEW PAK MEDICAL STORE', '', '0333-5270928', '', 'SADDAR BAZAR', 'KHARIAN CANT', 'Retailer', '34201-0884214-9', '3005822-8', '', '510', '0', '510', '12/31/2015 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27137, 767, 1911, 19, 'PUNJAB MEDICAL STORE', '', '7610141', '', 'SADDER BAZAR', 'KHARIAN CANT', 'Retailer', '', '', '', '825', '0', '825', '12/31/2015 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27138, 768, 195, 19, 'FATEH MEDICAL STORE', '', '0320-5853870', '', 'NEAR C.M.H CHOWK', 'KHARIAN CANT', 'Retailer', '34202-0766645-1', '2463738-6', '', '5837', '0', '0', '9/15/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27139, 769, 388, 38, 'PHRMA CARE MEDICAL STORE', '', '', '', 'OPP.CMH', 'KHARIAN CANT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27140, 770, 192, 19, 'BAIGA MEDICAL STORE', '', '', '', 'SUF SHIVKAN SHOPING COMPLEX', 'KHARIAN CANT', 'Retailer', '', '', '', '79', '0', '79', '12/31/2015 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27141, 771, 3518, 35, 'PHARMACY,KHALID JAMIL', '', '', '', '', 'KHARIAN CITY', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27142, 772, 3523, 35, 'AMIN MEDICAL HALL', '', '', '', 'MAIN BAZAR', 'KHARIAN CITY', 'Retailer', '', '', '', '207', '0', '0', '12/31/2013 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27143, 773, 3527, 35, 'IDEIAL BAKAR', '', '', '', '', 'KHARIAN CITY', 'G', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27144, 774, 1152, 11, 'DR.KHIZAR HAYAT', '', '', '', 'KHIZAR HOSPITAL', 'DINGA ROAD KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27145, 775, 1153, 11, 'DR.MEHMOD AKHTAR MINHAS', '', '0304-8416369', '', '', 'MINHAS Hp DHORIA', 'Doctor', '3420287913717', '0408340-7', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27146, 776, 1832, 18, 'RAFIQUE PHARMACY', '', '0537578159', '', 'NEAR JS BANK', 'GULYANA', 'Retailer', '', '', '', '69642', '0', '0', '4/29/2023 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27147, 777, 1821, 18, 'DR.M.ABBAS', '', '0346-2041650', '', 'PMDC# 25990-P', 'IMTIAZ HOSPITAL GULIANA', 'Doctor', '34202-9806743-6', '4005077-7', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27148, 778, 1814, 18, 'IFTIKHAR MEDICAL STORE', '', '', '', 'KOTLA ROAD', 'GULIANA', 'Retailer', '', '', '', '101', '0', '0', '12/31/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27149, 779, 1812, 18, '.HASHAME MEDICAL STORE', '', '', '', '', 'GULIANA', 'Retailer', '34202-7660594-9', '7918221-3', '', '881', '0', '881', '12/31/2015 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27150, 780, 184, 18, 'AWAN MEDICAL STORE', '', '0537588120', '', 'GULYANA CHAWK', 'GULIANA', 'Retailer', '', '', '', '924', '0', '924', '12/31/2015 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27151, 781, 183, 18, 'AMARA MEDICAL STORE', '', '0333-8401879', '', 'NEAR HBL,KOTLA ROAD', 'GULYANA', 'Retailer', '', '', '', '0', '40540', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27152, 782, 1830, 18, 'TANVEER MEDICAL STORE', '', '', '', 'MANGILA ROAD', 'AMNA ABAD,TEH.KHARIAN', 'G', '', '', '', '0', '5304', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27153, 783, 3215, 32, 'ZUBAIR MEDICAL STORE', '', '', '', 'G.T.ROAD', 'LALAMUSA', 'Retailer', '', '', '', '619', '0', '619', '12/31/2013 12:00 AM', '12/30/1899 12:00 AM', '12/31/2013 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27154, 784, 3217, 32, 'ITTFAQ PHARMACY', '', '', '', 'CHANDNE CHOWK', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27155, 785, 3220, 32, 'NEW USAMA MEDICAL STORE', '', '', '', 'CAMPING GROUND', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27156, 786, 3225, 32, 'AL HAFIZ MEDICAL STORE', '', '', '', 'CAMPING GROUND', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27157, 787, 3226, 32, 'MIR USAMA MEDICAL STORE', '', '', '', 'CHANDNI CHOWK NO 1', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27158, 788, 3227, 32, 'SHAH MEDICOSE', '', '', '', 'DR.NAILA AYUB', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27159, 789, 3228, 32, 'MOON ENTERPRISES', '', '', '', '', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27160, 790, 3232, 32, 'PHARMACY,SARGUN HOSPITAL', '', '', '', 'G.T.ROAD', 'LALAMUSA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27161, 791, 1068, 10, 'FAROOQ MEDICAL STORE', '', '03226416046', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1);
INSERT INTO `dealer_info` (`dealer_table_id`, `software_dealer_id`, `dealer_id`, `dealer_area_id`, `dealer_name`, `dealer_contact_person`, `dealer_phone`, `dealer_fax`, `dealer_address`, `dealer_address1`, `dealer_type`, `dealer_cnic`, `dealer_ntn`, `dealer_image`, `dealer_lic9_num`, `dealer_lic9_exp`, `dealer_lic10_num`, `dealer_lic10_Exp`, `dealer_lic11_num`, `dealer_lic11_Exp`, `dealer_credit_limit`, `dealer_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`, `software_lic_id`) VALUES
(27162, 792, 3244, 32, 'HAIDER MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '528', '528', '528', '12/31/2013 12:00 AM', '12/31/2013 12:00 AM', '12/31/2013 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27163, 793, 3247, 32, 'NEW ZAM ZAM MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27164, 794, 3248, 32, 'PHARMACY,DR.NADEEM AHMED KHAN', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27165, 795, 3249, 32, 'ABUBAKAR MEDICAL STORT', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27166, 796, 3251, 32, 'MAKKHA MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27167, 797, 3253, 32, 'PHARMACY,CAP,ASLAM BHATTI', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27168, 798, 3255, 32, 'AL REHMAN MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27169, 799, 3257, 32, 'BISMILLAH MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27170, 800, 1047, 10, 'PHARMACELL MEDICAL STORE', '', '03156241614', '', 'OPP.YASIN SWEETS', 'MAIN BAZAR LALAMUSA', 'Retailer', '34202-8901445-3', '7365872-2', '', '0', '0', '13826', '12/31/2015 12:00 AM', '12/30/1899 12:00 AM', '11/16/2019 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27171, 801, 3259, 32, 'FRIENDS MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27172, 802, 3260, 32, 'NEW TALAHA MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27173, 803, 1048, 10, 'JANJUA MEDICAL STORE', '', '03334341019', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '781', '781', '781', '12/31/2015 12:00 AM', '12/31/2015 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27174, 804, 1051, 10, 'BUTT MEDICAL STORE', '', '03216204595', '', 'NEAR MADINA GENERAL STORE', 'EID GAH ROAD LALAMUSA', 'Retailer', '', '', '', '5830', '0', '0', '9/11/2019 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27175, 805, 3268, 32, 'JADDHA MEDICOSE', '', '', '', 'OPP MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '1200', '0', '0', '11/16/2014 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27176, 806, 1052, 10, 'MALIK MEDICAL STORE', '', '03006232793', '', 'BAZAR DINACHAKIAN', 'LALAMUSA', 'Retailer', '34202-0729238-3', '4260162-2', '', '0', '533', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27177, 807, 1053, 10, 'KHALID MEDICAL SERVICES', '', '0537-511912', '', 'DINA CHAKIAN BAZAR', 'LALAMUSA', 'Retailer', '34202-5191279-5', '7232237-8', '', '0', '3608', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27178, 808, 1054, 10, 'TARIQ MEDICAL STORE', '', '03216289993', '', 'DINACHAKIAN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '194', '0', '195', '12/31/2015 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27179, 809, 1055, 10, 'SETHI MEDICAL STORE', '', '', '', 'NEAR PURANI GALLA MANDI', 'LALAMUSA', 'Retailer', '3420208123379', '0996092-9', '', '7293', '0', '0', '9/11/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27180, 810, 3262, 32, 'IKRAM MEDICAL STORE', '', '', '', 'DINACHAKIAN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27181, 811, 3273, 32, 'IKRAM MEDICAL STORE', '', '', '', 'DINACHAKIAN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27182, 812, 3280, 32, 'HAFIZ G/S', '', '', '', 'DINACHAKIAN BAZAR', 'LALAMUSA', 'G', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27183, 813, 3281, 32, 'KASHMIR G/S', '', '', '', '', 'LALAMUSA', 'G', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27184, 814, 3282, 32, 'ARIF  G/S', '', '', '', '', 'LALAMUSA', 'G', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27185, 815, 3283, 32, 'UMAIR G/S', '', '', '', '', 'LALAMUSA', 'G', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27186, 816, 2018, 20, 'SHAFIQ MEDICAL STORE', '', '', '', '', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27187, 817, 2483, 24, 'PHARMACY,AL SHAFA HOSPITAL', '', '', '', '', 'MAINDI BAHUDDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27188, 818, 2083, 20, 'PHARMACY,AL SHAFA HOSPITAL', '', '', '', '', 'MANDI BAHUDDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27189, 819, 191, 19, 'PHARMACY,FAZAL HOSPITAL', '', '', '', '', 'JALALPUR', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27190, 820, 1923, 19, 'ALI MEDICAL STORE', '', '', '', '', 'JALALPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27191, 821, 1926, 19, 'BHATTI MEDICAL STORE', '', '', '', '', 'JALALPUR', 'Retailer', '', '', '', '0', '331', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27192, 822, 1914, 19, 'BUTT MEDICAL STORE', '', '', '', '', 'JALALPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27193, 823, 1912, 19, 'IQRA MEDICAL STORE', '', '', '', '', 'JALALPUR', 'Retailer', '', '', '', '498', '0', '498', '1/1/2013 12:00 AM', '12/30/1899 12:00 AM', '1/1/2013 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27194, 824, 1913, 19, 'KASHMIR MEDICAL STORE', '', '', '', '', 'JALALPUR', 'Retailer', '', '', '', '0', '478', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27195, 825, 1927, 19, 'PHARMACY,DR.NASEEM TANVER', '', '', '', '', 'JALALPUR', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27196, 826, 1940, 19, 'KHURSHID MEDICAL STORE', '', '', '', '', 'FATHAPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27197, 827, 1356, 13, 'LATIF PHARMACY', '', '', '', 'FATEH PUR', 'GUJRAT', 'Retailer', '34201-0562115-9', '1111020-1', '', '0', '72175', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27198, 828, 1942, 19, 'PHARMACY,AZAM HOSPITAL', '', '', '', '', 'FATHAPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27199, 829, 345, 3, 'PHARMACY,MUBASHAR CLINIC', '', '', '', '', 'FATHAPUR', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27200, 830, 3239, 32, 'GOSEEA MEDICAL STORE', '', '', '', '', 'BADHAR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27201, 831, 3238, 32, 'HAMZA MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '721', '0', '0', '12/31/2013 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27202, 832, 2175, 2, 'PHARMACY,BUTT HOSPITAL', '', '', '', 'G.T.ROAD', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27203, 833, 2424, 24, 'SOHAIL MEDICAL STORE', '', '', '', '', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27204, 834, 3284, 32, '.', '', '', '', '', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27205, 835, 2049, 20, 'FRIENDS MEDICOSE', '', '', '', 'OLD RASOOL ROAD', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27206, 836, 2425, 24, 'AL REHMAN MEDICAL STORE', '', '', '', '', 'LADHER KHERD', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27207, 837, 1333, 13, 'DR.IMRAN', '', '', '', 'IQBAL HOSPITAL', 'KAKRALI', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27208, 838, 3012, 30, 'FAZAL MEDICAL STORE', '', '', '', 'CHOWK DHALIANA', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27209, 839, 954, 9, 'QAZI MEDICAL STORE', '', '03216217080', '', 'GUZAR-E-MADINA ROAD', 'GUJRAT', 'Retailer', '', '', '', '198', '0', '0', '12/31/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27210, 840, 970, 9, 'PHARMACY,DR.SAFADAR IQBAL', '', '', '', 'SARGODHA ROAD', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27211, 841, 2034, 20, 'TAHIR MEDICAL STORE', '', '', '', 'KACHERY ROAD', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27212, 842, 1180, 1, 'FRIENDS M/S', '', '', '', '', 'JEHLUM', 'S', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27213, 843, 1056, 10, 'PHARMACY,BANO CLINIC', '', '', '', 'G.T ROAD', 'LALAMUSA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27214, 844, 2050, 20, 'UNITED MEDICINE TRADER', '', '', '', 'SCHOOL MOHALLAHA', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27215, 845, 4022, 40, 'HAMAZA MEDICAL STORE', '', '', '', '', 'SARI ALAMGIR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27216, 846, 3612, 36, 'NEW ALI ZAIN MEDICAL STORE', '', '', '', 'R.H.C MALKAR', 'MALKAA', 'Retailer', '', '', '', '599', '0', '599', '12/31/2013 12:00 AM', '12/30/1899 12:00 AM', '12/31/2013 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27217, 847, 554, 5, 'AL-SAYED PHARMACY', '', '', '', 'BHIMBER ROAD', 'GUJRAT', 'Retailer', '', '', '', '1245', '0', '0', '5/14/2017 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27218, 848, 1340, 13, 'DR.YAHYA', '', '', '', 'FAMALY HOSPITAL', 'KOTLA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27219, 849, 3812, 38, 'MUSA KHAN PHARMACY', '', '', '', 'SAF SHIKAN PLAZA', 'KHARIAN CANTT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27220, 850, 2084, 20, 'PHARMACY,DR.ANSAR IQBAL MIRZA', '', '', '', '', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27221, 851, 1142, 11, 'SOHAIL MEDICAL STORE', '', '', '', 'ADDA CHANNAN', 'TEHSIL KHARIAN', 'Retailer', '', '', '', '729', '0', '729', '12/31/2013 12:00 AM', '12/30/1899 12:00 AM', '12/31/2013 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27222, 852, 1143, 11, 'SHAHEEN MEDICAL STORE', '', '', '', 'ADDA CHANNAN', 'TEHSIL KHARIAN', 'Retailer', '', '', '', '773', '0', '773', '12/31/2013 12:00 AM', '12/30/1899 12:00 AM', '12/31/2013 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27223, 853, 1057, 10, 'PHARMACY,DR.RIAZ ASLAM JANJUWA', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27224, 854, 1827, 18, 'RAFIQUE MEDICAL STORE', '', '03338459652', '', 'MAIN BAZAR', 'NASEERA', 'Retailer', '', '', '', '415', '0', '415', '12/31/2015 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27225, 855, 1826, 18, 'QURASHI MEDICOSE', '', '03338511679', '', 'MAIN BAZAR', 'NASEERA', 'Retailer', '', '', '', '872', '0', '872', '12/31/2015 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27226, 856, 1828, 18, 'RAJA MEDICAL HALL', '', '03338536612', '', 'MAIN BAZAR', 'NASEERA', 'Retailer', '', '', '', '0', '123', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27227, 857, 1200, 1, 'ASIA MEDICAL STORE', '', '', '', 'KABLI GATE', 'GUJRAT', 'Retailer', '', '', '', '565', '0', '0', '12/31/2013 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27228, 858, 1250, 1, 'AL HAIDER MEDICAL STORE', '', '', '', 'RAILWAY ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27229, 859, 1169, 11, 'PHARMACY,MUSHTAQ M/CLINIC', '', '', '', 'DR,ZAFAR MEHDI', 'NASEERA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27230, 860, 952, 9, 'AL-NOOR PHARMACY', '', '', '', 'GHARI AHMAD ABAD', 'GUJRAT', 'Retailer', '', '', '', '1210', '0', '0', '1/8/2017 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27231, 861, 20100, 20, 'MEHTAB MEDICAL STORE', '', '', '', 'NEAR JAMIA MADRISA', 'TEHSIL & DIST MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27232, 862, 1325, 13, 'AL KARAM PHARMACY', '', '0300-6212376', '', 'NEAR HBL BHIMBER ROAD', 'KOTLA', 'Retailer', '34202-1396949-1', 'A013014-0', '', '7046', '0', '0', '9/8/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27233, 863, 2432, 24, 'AL RASHEED PHARMACY', '', '', '', 'MAIN BAZAR', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27234, 864, 340, 3, 'PHARMACY,SADDIQ CLINIC', '', '', '', 'MAIN BAZAR', 'BAHGOWAL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27235, 865, 4023, 40, 'PHARMACY,DR.NAZAR MOH HOS', '', '', '', '', 'SARI ALAMGIR', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27236, 866, 2085, 20, 'PHARMACY,LAHORE LAB', '', '', '', '', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27237, 867, 3213, 32, 'PHARMACY,DR.SALEEM BUTT', '', '', '', 'G.T.ROAD', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27238, 868, 135, 13, '.IMTIAZ MEDICAL STORE', '', '0346-6872415', '', 'BHAMBER ROAD', 'BAZURAGWAL', 'Retailer', '', '', '', '750', '0', '750', '12/31/2013 12:00 AM', '12/30/1899 12:00 AM', '12/31/2013 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27239, 869, 1159, 11, 'PHARMACY,BASSAT CLINIC', '', '', '', '', 'NASEERA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27240, 870, 20110, 20, 'WARRANTY H.S', '', '', '', '', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27241, 871, 3030, 30, 'HASSAN MEDICAL STORE', '', '', '', 'OPP.RHC', 'CHELLANWALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27242, 872, 3031, 30, 'AL REHMAN MEDICAL STORE', '', '', '', 'OPP RHC', 'CHAILLAWALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27243, 873, 1150, 11, '.ZAHEER PHARMACY', '', '', '', 'NEAR RHC PINDI SULTAN PUR', 'TEHSIL KHARIAN', 'Retailer', '', '', '', '7914', '0', '0', '10/25/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27244, 874, 3140, 3, 'PHARMACY,AMMAN CLINIC', '', '', '', '', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27245, 875, 4014, 40, 'AL-MAKKAH PHARMACY', '', '', '', 'G.T.ROAD', 'SARI ALAMGIR', 'Retailer', '', '', '', '1018', '0', '0', '12/31/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27246, 876, 2086, 20, 'PHARMACY,ANWAR FATIMA HOSPITAL', '', '', '', '', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27247, 877, 1930, 19, 'ATTIQUE  PHARMACY', '', '', '', 'MUHALLAH SHAR SHAHI', 'JALALPUR', 'Retailer', '', '', '', '1301', '0', '0', '3/4/2016 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27248, 878, 2612, 26, 'DAUAD MEDICAL STORE', '', '', '', '', 'KUNJAH', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27249, 879, 968, 9, 'DR.BILAL RASHID PAGGANWALA', '', '', '', 'PMDC# 32837-P', 'M.RASHEED HOSPITAL GUJRAT', 'Doctor', '34201-4252434-3', '3116695-4', '', '32837', '0', '0', '12/31/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27250, 880, 4015, 40, 'BAOO JEE PHARMACY', '', '0544653038', '', 'IN FRONT OF RAWALPINDI', 'SARI ALAMGIR', 'Retailer', '', '', '', '1358', '0', '0', '2/1/2017 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27251, 881, 1324, 13, 'NEW SHARIF PHARMACY', '', '0332-9588433', '', 'LANGRIAL CHOWK', 'KOTLA', 'Retailer', '', '', '', '2130', '0', '0', '8/30/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27252, 882, 2087, 20, 'PHARMACY,DR.M.SHAFIQ', '', '', '', 'DAR-UL-SHIFA HOSPITAL', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27253, 883, 1145, 11, 'SHABEER MEDICAL STORE', '', '', '', 'DINGA ROAD', 'NOONAWALI', 'Retailer', '', '', '', '0', '286', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27254, 884, 3560, 35, 'SHIFA MEDICAL STORE', '', '', '', '', 'KHARIAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27255, 885, 3210, 3, 'PHARMACY, SULTAN CLINIC', '', '', '', '', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27256, 886, 3022, 30, 'ALAM PHARMACY', '', '', '', 'ALAM MARKET', 'DINGA T,KHARIAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27257, 887, 3181, 3, 'MARRYUM PHARMACY', 'MUBARK MARKET', '', '', 'GUJRAT', '', 'Retailer', '', '', '', '1031', '0', '0', '3/3/2014 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27258, 888, 2625, 26, 'AL-JANNAT MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'KUNJAH', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27259, 889, 3023, 30, 'MADDANI PHARMACY', '', '', '', 'BOHAR CHOWK', 'DINGA TEHSIL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27260, 890, 2088, 20, 'PHARMACY,DR.ISHTIAQ HOSPITAL', '', '', '', '', 'MANDI BAHUDDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27261, 891, 4141, 4, 'SHIFA PHARMACY', '', '', '', 'BHIMBER ROAD', 'GUJRAT', 'Retailer', '', '', '', '1173', '0', '0', '6/26/2014 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27262, 892, 1946, 19, 'DAR PHARMACY', '', '', '', 'MAIN BAZAR', 'JALALPUR', 'Retailer', '', '', '', '1216', '0', '0', '1/28/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27263, 893, 3214, 32, 'REHMAN MEDICINE COM', '', '', '', 'DINACHAKIAN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '241', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27264, 894, 196, 19, 'KHALID MEDICAL STORE', '', '03404112781', '', 'OPP C.M.H', 'KHARIAN CANTT', 'Retailer', '', '', '', '0', '1507', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27265, 895, 2099, 20, 'PHARMA PLUS PHARMACY', '', '', '', 'PHALIA ROAD', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27266, 896, 3537, 35, 'JUNAID MEDICAL STORE', '', '', '', 'GULYANA RAOD', 'KHARIAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27267, 897, 2817, 28, 'AL SHIFA  MEDICAL STORE', '', '', '', '', 'GOJRA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27268, 898, 1058, 10, 'KHURSHID MEDICAL STORE', '', '03006246914', '', 'DINA CHAKIAN BAZAR', 'LALAMUSA', 'Retailer', '34202-0719856-1', '4254666-4', '', '0', '241', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27269, 899, 3148, 31, 'PHARMACY,DR.RIZWAN HAIDER', '', '', '', 'KOTLA ROAD', 'SADDA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27270, 900, 3164, 3, 'SHERAZ PHARMACY', '', '', '', 'SHAHDAULA ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27271, 901, 41101, 41, 'YOUNAS SONS', '', '', '', 'MANDI BAHUDDIN', '', 'S', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27272, 902, 965, 9, 'LATIF PHARMACY', '', '03316247291', '', 'STAF GALA SARGODHA ROAD', 'GUJRAT', 'Retailer', '34201-7110462-1', '7491027-3', '', '1064', '0', '0', '2/1/2016 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27273, 903, 3024, 30, 'ADAM HOSPITAL', '', '', '', 'DINGA', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27274, 904, 140, 1, 'BUTT BROTHERS PHARMACY', '', '03008620925', '', '', 'KUTCHERY CHOWK GUJRAT', 'Retailer', '34201-4592273-3', '0402584-9', '', '7999', '0', '0', '9/26/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27275, 905, 3614, 36, 'ALI PHARMACY', '', '', '', '', 'GULIANA', 'Retailer', '', '', '', '1068', '0', '0', '2/1/2013 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27276, 906, 2142, 2, 'PEARMACY,WAPDA DISPENSARY', '', '', '', 'SARGODHA ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27277, 907, 3615, 36, 'HAADI PHARMACY', '', '', '', 'LALAMUSA ROAD', 'GULYANA', 'Retailer', '', '', '', '1258', '0', '0', '7/4/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27278, 908, 3538, 35, 'PHARMACY,DR.FOZIA AMIR HOSPITAL', '', '', '', '', 'KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27279, 909, 3143, 3, 'PHARMACY,AL FAZAL HOSPITAL', '', '', '', 'QAMAR SIALVI ROAD', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27280, 910, 3149, 31, 'PHARMACY, NAZEER HOSPITAL', '', '', '', '', 'BHIMBER', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27281, 911, 1202, 1, 'GHUMAN MEDICINE CO', '', '03218540628', '', 'VANIKAY ROAD', 'HAFIZABAD', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27282, 912, 3554, 35, '.', '', '', '', '', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27283, 913, 1341, 13, 'PHARMACY, SKIN VISION HOSPITAL', '', '', '', 'BHIMBER ROAD', 'KOTLA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27284, 914, 3616, 36, 'PHARMACY,FATAMA HOSPITAL', '', '', '', '', 'LAKHARI KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27285, 915, 1342, 13, 'PHARMACY, ASHRAF CLINIC', '', '', '', '', 'KOTLA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27286, 916, 342, 3, 'QADEER MEDICAL HALL', '', '0300-6208883', '', '', 'BHAGOWAL KALAN', 'Retailer', '', '', '', '682', '0', '682', '12/31/2015 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27287, 917, 189, 18, 'HASSAN MEDICAL STORE', '', '03006208882', '', '', 'BHAGOWAL KALAN', 'Retailer', '', '', '', '422', '0', '422', '12/31/2013 12:00 AM', '12/30/1899 12:00 AM', '12/31/2013 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27288, 918, 4024, 40, 'SUNNY MEDICAL STORE', '', '03129400360', '', 'OLD RAILWAY LINE ROAD', 'SARAI ALAMGIR', 'Retailer', '', '', '', '0', '1083', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27289, 919, 3025, 30, 'DAWOOD PHARMACY', '', '', '', 'DINGA ROAD', 'ATTOWALA DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27290, 920, 1863, 18, 'DAWOOD PHARMACY', '', '0300-9579388', '', 'DINGA ROAD', 'ATTOWALA', 'Retailer', '', '', '', '1082', '0', '0', '12/31/2013 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27291, 921, 3032, 30, 'NEW ZAHEER PHARMACY', '', '', '', 'NEAR GOV,MAT', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27292, 922, 1329, 13, '.SEHR PHARMACY', '', '0347-7339080', '', '', 'KOTLA ARAB ALI KHAN', 'Retailer', '', '', '', '37504', '0', '0', '8/9/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27293, 923, 4025, 40, 'CITY PHARMACY', '', '', '', 'INFRONT OF T.H', 'SARI ALAMGIR', 'Retailer', '', '', '', '1148', '0', '0', '3/8/2014 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27294, 924, 4178, 4, 'PHARMACY, DR. ISHFAQ BUTT', '', '', '', '', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27295, 925, 4026, 40, 'BILAL MEDICAL STORE', '', '', '', 'TAWAKAI MARKET', 'SARAI ALAMGIR', 'Retailer', '', '', '', '0', '1425', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27296, 926, 2116, 21, 'IMRAN PHARMACY', '', '', '', 'MAIN BAZAR', 'MANGOWAL GHARBI', 'Retailer', '34201-8933873-1', '3983880-3', '', '25333', '0', '0', '12/5/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27297, 927, 136, 13, 'FAISAL PHARMACY', '', '0333-8487696', '', 'CHOWK', 'DOULAT NAGAR', 'Retailer', '34201-0463859-7', '7365639-3', '', '63609', '0', '0', '11/17/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27298, 928, 2061, 20, 'SADDAT MEDICAL STORE', '', '', '', 'KING ROAD', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27299, 929, 2052, 20, 'PHARMACY,MUKHTAR CLINIC', '', '', '', 'OPP DHQ', 'MANDI BAHUDDIN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27300, 930, 2053, 20, 'MADINA MEDICAL STORE', '', '', '', 'KING CHOWK', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27301, 931, 341, 3, 'PHARMACY. FAMALY CLINIC', '', '', '', '', 'KARIANWALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27302, 932, 3263, 32, 'AHMEDDIN & SONS PHARMACY', '', 'LALAMUSA', '', '', 'MAIN BAZAR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27303, 933, 2433, 24, 'HASSAN PHARMACY', '', '', '', '', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27304, 934, 2434, 24, 'GONDAL MEDICAL STORE', '', '', '', '', 'PHALIA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27305, 935, 3300, 3, 'SITARA BAKAR', '', '', '', 'SAEGODHA ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27306, 936, 2628, 26, 'PHARMACY,RAZA CLINIC', '', '', '', '', 'KUNJAH', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27307, 937, 2179, 2, 'AMJAD MEDICAL STORE', '', '', '', 'RALWAY ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27308, 938, 961, 9, 'AHMED BROTHERS PHARMACY', '', '', '', 'NEAR SATAF GALLA', 'SARGODHA ROAD GUJRAT', 'Retailer', '34201-8258893-1', '1460305-5', '', '63944', '0', '0', '12/14/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27309, 939, 3154, 31, 'HASHIM MEDICAL STORE', '', '', '', 'LANGRIAL CHOWK', 'KOTLA ARAB', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27310, 940, 3033, 30, 'FATHA PHARMACY', '', '', '', '', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27311, 941, 2090, 20, 'ASHIA MEDICAL STORE', '', '', '', '', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27312, 942, 139, 13, '.MADNI MEDICAL STORE', '', '0345-6923570', '', 'NEAR NAGRIAN EXCHANGE', 'CHOOR CHAK', 'Retailer', '', '', '', '3621', '0', '0', '8/30/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27313, 943, 2062, 20, 'EMAAN  MEDICAL STORE', '', '', '', '', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27314, 944, 1810, 18, 'HAMZA MEDICAL STORE', '', '', '', 'AHMED DIN MARKET', 'GULYANA', 'Retailer', '', '', '', '0', '1104', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27315, 945, 343, 3, 'QAMAR PHARMACY', '', '', '', '', 'KARIANWALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27316, 946, 3156, 31, 'BAJWA PHARMACY', '', '', '', 'NEAR RURAL HEALTH CENTER', 'DAULAT NAGAR', 'Retailer', '', '', '', '1160', '0', '0', '4/5/2014 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27317, 947, 2818, 28, 'RANJHA MEDICAL STORE', '', '', '', '', 'GOJRA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27318, 948, 2063, 20, 'HASSAM MEDICINE', '', '', '', 'OPP D.H.Q', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27319, 949, 2054, 20, 'SALEEM MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27320, 950, 1343, 13, 'DR.SHEHZAD-UL-HASSAN', '', '0305-6277062', '', '', 'SHABIR HOSPITAL KOTLA', 'Doctor', '3420201548923', '0407863-2', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27321, 951, 3158, 31, 'SHAHZAD MEDICAL STORE', '', '', '', 'PINDI HUNJA', 'BHEMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27322, 952, 1186, 1, 'MAJEED SONS PHARMACY', '', '', '', '', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27323, 953, 4187, 4, 'AL HIDAMAT FONDATION', '', '', '', '', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27324, 954, 3237, 32, 'LUCKY SEVEN MEDICAL STORE', '', '', '', 'NEAR FRUIT MANDI', 'LALAMUSA', 'Retailer', '', '', '', '0', '422', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27325, 955, 1819, 18, 'PHARMACY,BASHARAT H/S', '', '', '', '', 'BAHGOWAL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27326, 956, 2630, 26, 'MADINA PHARMACY', '', '', '', 'MAIN BAZAR', 'KUNJAH', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27327, 957, 3159, 31, 'AL-IMRAN MEDICAL STORE', '', '', '', 'BHIMBER ROAD', 'GHUNDRA KALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27328, 958, 3571, 35, 'AFTAB MEDICAL STORE', '', '', '', 'GULINA ROAD', 'KHARIAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27329, 959, 3619, 36, 'PHARMACY,HASHAME W/F/H', '', '', '', '', 'GULIANA', 'Retailer', '', '', '', '881', '0', '881', '12/31/2013 12:00 AM', '12/30/1899 12:00 AM', '12/31/2013 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27330, 960, 3277, 32, 'ZULFIQAR MEDICAL SERVIC', '', '', '', '', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27331, 961, 2916, 29, 'PHARMACY, DR.ROBINA M/H', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27332, 962, 2091, 20, 'AHMED MEDICAL STORE', '', '', '', '', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27333, 963, 2092, 20, 'AL HAMZA MEDICAL STORE', '', '', '', 'COMMITTEE CHOWK', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27334, 964, 2118, 21, 'NEW KASHMIR MEDICAL STORE', '', '', '', 'OPP Govt,Meternity Hospital', 'MANGOWAL GHARBI', 'Retailer', '34201-4240382-5', '5479247-2', '', '0', '64061', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27335, 965, 4027, 40, 'AWAN BROTHERS PHARMACY', '', '', '', 'G.T.ROAD', 'SARI ALAMGIR', 'Retailer', '', '', '', '1136', '0', '0', '1/12/2016 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27336, 966, 1050, 10, 'DR.ATIF MAHMOOD', '', '', '', 'PMDC# 21094-P', 'AMNA HOSPITAL LALAMUSA', 'Doctor', '34202-1151677-5', '0406944-7', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27337, 967, 2119, 21, 'NEW WARRAICH MEDICAL STORE', '', '', '', 'OPP GOVT.MATERNITY HP', 'MONGOWAL GHARBI', 'Retailer', '', '', '', '0', '24488', '0', '3/31/2017 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27338, 968, 2110, 21, 'DR.QAISER NADEEM', '', '', '', 'HADDI CLINIC', 'MONGOWAL', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27339, 969, 4147, 4, 'TAYYAB PHARMACY', '', '', '', 'NEAR AZIZ BHATTI H/S', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27340, 970, 3161, 31, 'UMER MEDICAL STORE', '', '', '', 'ROMLOW TEHSIL', 'BHAMBIR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27341, 971, 2089, 20, 'PHARMACY,DR.SHAFIQ CLINIC', '', '', '', '', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27342, 972, 3265, 32, 'ZULFIQAR MEDICAL STORE', '', '', '', '', 'LALAMUSA', 'Retailer', '', '', '', '1017', '0', '0', '12/31/2013 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27343, 973, 5148, 5, 'PHAENACY, AHMED HOSPITAL', '', '', '', '', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27344, 974, 2093, 20, 'DAWAKHANA RAZIVA', '', '', '', 'CHOWK NOOR MASJID', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27345, 975, 567, 5, 'DR.ZAHIDA BABAR', '', '0300-9623598', '', 'PMDC# 1499-AJK', 'IBRAHIM HOSPITAL GUJRAT', 'Doctor', '34201-0545792-8', '3827529', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27346, 976, 3266, 32, 'KARAM ELLAHI PHARMACY', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27347, 977, 2635, 26, 'ARIF MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'KUNJAH', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27348, 978, 1968, 19, 'SHEIKH BROTHERS PHARMACY', '', '', '', 'MAIN BAZAR', 'JAJALPUR', 'Retailer', '', '', '', '1145', '0', '0', '2/17/2014 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27349, 979, 4028, 40, 'ZAM ZAM PHARMACY', '', '', '', 'NEAR RAILWAY GATE', 'SARAI ALAMGIR', 'Retailer', '', '', '', '1146', '0', '0', '2/20/2014 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27350, 980, 337, 3, 'DR.GULZAAR AHMED', '', '', '', '', 'IRSHAD HOSPITAL KOTLI KOHALA', 'Doctor', '34201-1227947-9', '1156586', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27351, 981, 2094, 20, 'PHARMACY, FAMALY CLINIC', '', '', '', 'JAIL ROAD', 'MANDI BAHUDDIN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27352, 982, 4029, 40, 'PHARMACY,DR,M.RIAZ BARRA', '', '', '', '', 'SARAI ALAMGIR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27353, 983, 1344, 13, 'PHARMACY, MUSHTAQ HOSPITAL', '', '', '', '', 'KOTLA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27354, 984, 2917, 29, 'PHARMACY, SHAFI HOSPITAL', '', '', '', '', 'KTHALA SHAKHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27355, 985, 1060, 10, 'HAJI GULZAR SURGICAL PHARMACY', '', '', '', 'MALL ROAD,CHANDNI CHOWK NO.1', 'LALAMUSA', 'Retailer', '', '8239813-7', '', '32391', '0', '0', '4/26/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1);
INSERT INTO `dealer_info` (`dealer_table_id`, `software_dealer_id`, `dealer_id`, `dealer_area_id`, `dealer_name`, `dealer_contact_person`, `dealer_phone`, `dealer_fax`, `dealer_address`, `dealer_address1`, `dealer_type`, `dealer_cnic`, `dealer_ntn`, `dealer_image`, `dealer_lic9_num`, `dealer_lic9_exp`, `dealer_lic10_num`, `dealer_lic10_Exp`, `dealer_lic11_num`, `dealer_lic11_Exp`, `dealer_credit_limit`, `dealer_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`, `software_lic_id`) VALUES
(27356, 986, 962, 9, 'AZHAR MEDICAL STORE', '', '03445306998', '', 'MOHALLAH QUTAB ABAD', 'SARGODHA ROAD GUJRAT', 'Retailer', '34201-0481925-7', '2735177-7', '', '0', '33234', '0', '12/31/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27357, 987, 953, 9, 'NEW ALAM MEDICAL STORE', '', '03009620693', '', 'MOHALLAH KALU PURA', 'GUJRAT', 'Retailer', '34201-8033083-5', '3421428-3', '', '0', '1159', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27358, 988, 1331, 13, 'HASHIM PHARMACY', '', '0300-8529348', '', 'LANGRIAL CHOWK', 'KOTLA', 'Retailer', '', '', '', '6681', '0', '0', '9/18/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27359, 989, 1226, 12, 'USMAN MEDICAL STORE', '', '', '', '', 'BARNALA', 'Retailer', '', '', '', '291', '0', '0', '12/31/2018 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27360, 990, 367, 3, '.MAQSOOD MEDICAL STORE', '', '', '', '', 'JALALPUR SOBATIAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27361, 991, 365, 3, 'TOQEER PHARMACY', '', '0307 6214114', '', '', 'JALALPUR SOBATIAN', 'Retailer', '', '', '', '978', '0', '0', '12/31/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27362, 992, 362, 3, 'PHARMACY,AL HAMAD HOSPITAL', '', '', '', '', 'JALALPUR SOBATIAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27363, 993, 1217, 12, 'PHARMACY,DR MASOOD AKHTAR', '', '', '', '', 'LORAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27364, 994, 175, 1, '.ATIF PHARMACY', '', '03009621843', '', 'KHAWAJGEN ROAD', 'GUJRAT', 'Retailer', '', '', '', '1514', '0', '0', '10/20/2018 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27365, 995, 4188, 4, 'PHARMACY,SADDIQ HOSPITAL', '', '', '', 'REHMAN SHAHEED ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27366, 996, 3562, 35, 'PHARMACY, MAQBOOL NASEEM HOSPITAL', '', '', '', '', 'KHARIAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27367, 997, 4199, 4, 'PHARMACY,AL TARIQ HOSPITAL', '', '', '', '', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27368, 998, 1062, 10, '.HASSAN PHARMACY', '', '', '', 'MAIN BAZAR', 'DEONA MANDI', 'Retailer', '', '', '', '1154', '0', '0', '3/25/2016 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27369, 999, 1345, 13, 'PHARMACY, SAADE CLINIC', '', '', '', 'SOBOR CHOWK', 'KOTLA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27370, 1000, 2203, 2, 'JAVAID PHARMACY', '', '', '', 'G.T.ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27371, 1001, 360, 3, 'ALAM MEDICAL STORE', '', '', '', '', 'JALALPUR SOBATIAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27372, 1002, 1231, 12, 'ALI MEDICAL STORE', '', '', '', 'ABDUL REHMAN MARKET', 'AWAN SHARIF', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27373, 1003, 1973, 19, 'PHARMACY,SHILOKH HOSPITAL', '', '', '', '', 'JALALPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27374, 1004, 363, 3, 'SABAR MEDICAL STORE', '', '', '', '', 'AWAN SHARIF', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27375, 1005, 3618, 36, 'NEW TAMOOR PHARMACY', '', '', '', 'OPP RURAL HEALTH CENTER', 'MALKA', 'Retailer', '', '', '', '1040', '0', '0', '4/4/2014 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27376, 1006, 5300, 5, 'RIZWAN G/S', '', '', '', '', 'GUJRAT', 'G', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27377, 1007, 2301, 2, 'RAJA B/K', '', '', '', '', 'GUJRAT', 'G', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27378, 1008, 3302, 3, 'MOON G/S', '', '', '', '', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27379, 1009, 3034, 30, 'AL KUWAIT PHARMACY', '', '', '', 'THANA ROAD', 'DINGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27380, 1010, 3279, 32, 'SAFADAR MEDICAL STORE', '', '', '', 'G.T.ROAD', 'JORA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27381, 1011, 4303, 4, 'T.MART G/S', '', '', '', '', 'GUJRAT', 'G', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27382, 1012, 1346, 13, 'DR.AMJAD HUSSAIN', '', '', '', 'PMDC# 1889-AJK', 'HASSAN ORTHOPEDIC Hp KOTLA', 'Doctor', '81102-0788768-3', '7363978-7', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27383, 1013, 3167, 31, 'PHARMACY,TOKHEED HOSPITAL', '', '', '', '', 'MACHOWAL', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27384, 1014, 2095, 20, 'NEW ASIF MEDICAL STORE', '', '', '', 'NEAR AKRAM H/S', 'MANDI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27385, 1015, 338, 3, 'PHARMACY,M. KHAN HOSPITAL', '', '', '', '', 'SAGHAR TANDA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27386, 1016, 339, 3, 'PHARMACY,MOHSAN CLINIC', '', '', '', '', 'BAGOWALL', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27387, 1017, 3304, 3, 'HIGH CLASS B/K', '', '', '', '', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27388, 1018, 3168, 31, 'DISCOUNT MEDICAL STORE', '', '', '', 'OPP D.H.Q', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27389, 1019, 1818, 18, 'PHARMACY, WALI HOSPITAL', '', '', '', '', 'KARIANWALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27390, 1020, 1059, 10, 'NEW KAMAL MEDICAL CENTER', '', '03227380072', '', 'NEAR NISAR HOSPITAL', 'G.T ROAD LALAMUSA', 'Retailer', '34202-5783142-3', '0812303-9', '', '0', '8218', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27391, 1021, 3286, 32, 'SANDHU USAMA MEDICAL STORE', '', '', '', '', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27392, 1022, 3287, 32, 'AL NOOR MEDICAL STORE', '', '03424660415', '', 'G.T.ROAD', 'LALAMUSA', 'Retailer', '', '', '', '420', '0', '0', '12/31/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27393, 1023, 5305, 5, 'G.M BAKAR', '', '', '', '', 'GUJRAT', 'G', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27394, 1024, 1221, 12, 'ZESHAN MEDICAL STORE', '', '', '', '', 'BRANALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27395, 1025, 3169, 31, 'ABDULLAH MEDICAL STORE', '', '', '', 'SHER JANG COLONY', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27396, 1026, 4306, 4, '7/11 BAKAR', '', '', '', 'BHAMBER ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27397, 1027, 4307, 4, 'CAKE CENTER', '', '', '', '', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27398, 1028, 2189, 2, 'PHARMACY, SOCICAL SECURITY HOSPITAL', '', '', '', 'G.T.ROAD', 'GUJRAT', 'I', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27399, 1029, 4308, 4, 'BILAL BAKAR', '', '', '', 'JAIL ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27400, 1030, 335, 3, 'PHARMACY,HAJI FAZAL HUSSAN', '', '', '', '', 'HAJIWALA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27401, 1031, 5162, 5, 'DAWOOD BUTT PHARMACY', '', '03344665705', '', 'NEAR AZIZ BHATTI S.H', 'GUJRAT', 'Retailer', '', '', '', '1188', '0', '0', '8/30/2016 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27402, 1032, 2192, 2, 'PHARMACY,SHAMSHAD HOSPITAL', '', '', '', 'NEAR G.T.S ADA', 'GUJRAT', 'Retailer', '', '', '', '0', '1159', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27403, 1033, 1225, 12, 'DR.IKRAAM DAR', '', '', '', 'AL BARKAT HOSPITAL', 'BARNALA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27404, 1034, 3288, 32, 'MUGHAL MEDICAL STORE', '', '', '', 'GUNJA ROAD ROAD', 'LALAMUSA', 'Retailer', '', '', '', '473', '0', '0', '12/31/2013 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27405, 1035, 3564, 35, 'PHARMACY, HUMAYUN MEDICAL /C', '', '', '', 'G.T.ROAD', 'KHARIAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27406, 1036, 1063, 10, 'KAMAL MEDICAL STORE', '', '0321-6286123', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '34202-2721651-3', '4177453-1', '', '0', '38674', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27407, 1037, 1980, 19, 'MUNIR MEDICAL STORE', '', '', '', '', 'JALALPUR JATTAN', 'Retailer', '', '', '', '711', '0', '0', '12/31/2013 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27408, 1038, 4030, 40, 'AL UMAR MEDICAL STORE', '', '', '', '', 'SARI ALAMIGER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27409, 1039, 361, 3, 'KHAN MEDICAL STORE', '', '', '', 'GHULAM HAIDER CHOWK', 'AWAN SHARIF', 'Retailer', '', '', '', '0', '14383', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27410, 1040, 2220, 2, 'NOOR-UL-HASSAN PHARMACY', 'NEAR BUSS STAND', '', '', 'MAIN BAZAR', 'SHADIWAL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27411, 1041, 1064, 10, 'DR.IJAZ HUSSAIN', '', '', '', 'G.T.ROAD', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27412, 1042, 1981, 19, 'MUH. ALI PHARMACY', '', '', '', 'MAIN BAZAR', 'FATEH PUR', 'Retailer', '', '', '', '0', '1168', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27413, 1043, 0, 0, '', '', '', '', '', '', '', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27414, 1044, 4016, 40, 'PHARMACY,DR,IMRAN MAHMOOD', '', '', '', '', 'SARI ALAMGER', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27415, 1045, 3573, 35, 'PHARMACY,AL NEMAT CLINIC', '', '', '', '', 'KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27416, 1046, 3577, 35, 'SHAHEEN PHARMACY', '', '', '', 'GULYANA ROAD', 'KHARIAN', 'Retailer', '', '', '', '1219', '0', '0', '3/3/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27417, 1047, 4031, 40, 'PHARMACY,WAJJID CLINIC', '', '', '', '', 'SARI ALAMGIR', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27418, 1048, 3170, 31, 'AKRAM PHARMACY', '', 'GUJRAT', '', '', 'QADIR COLONY', 'Retailer', '', '', '', '1220', '0', '0', '2/8/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27419, 1049, 4032, 40, 'TAHIR PHARMACY', '', '', '', 'RAILWAY CROSSING', 'SARAI ALAMGIR', 'Retailer', '', '', '', '1227', '0', '0', '2/26/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27420, 1050, 3574, 35, 'PHARMACY, SARDAR HOSPITAL', '', '', '', 'G.T.ROAD', 'KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27421, 1051, 1300, 1, 'PREMIER AGENCIES', '', '', '', 'SAMAL IND,STATE NO 1', 'GUJRANWALA', 'S', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27422, 1052, 1149, 11, 'IRFAN PHARMACY', '', '', '', 'DINGA ROAD', 'KHARIAN', 'Retailer', '', '', '', '1141', '0', '0', '2/15/2016 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27423, 1053, 150, 1, 'WARRAICH PHARMACY', '', '0336-9625178', '', 'SHAH JEHANGIR ROAD', 'NEAR ZAHOOR PALACE GUJRAT', 'Retailer', '34201-2823840-1', '7217691', '', '7951', '0', '0', '9/26/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27424, 1054, 482, 4, 'MALIK BROTHER PHARMACY', '', '', '', 'COURT ROAD', 'GUJRAT', 'Retailer', '', '', '', '1254', '0', '0', '6/3/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27425, 1055, 3171, 31, 'MIAN JEE MEDICAL STORE', '', '', '', 'R.H.C,', 'DAULAT NAGAR', 'Retailer', '', '', '', '0', '1259', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27426, 1056, 3172, 31, 'IMRAN MEDICAL STORE', '', '', '', 'MOHRA SADDA', 'BHIMBER', 'Retailer', '', '', '', '0', '1263', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27427, 1057, 3173, 31, 'HAFIZ PHARMACY', '', '', '', 'NEAR H/C', 'DAYLAT NAGAR', 'Retailer', '', '', '', '1260', '0', '0', '7/14/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27428, 1058, 366, 3, 'SAQLAIN PHARMACY', '', '0342 6834866', '', 'AWAN SHARIF ROAD', 'JALALPUR SOBTIAN', 'Retailer', '', '', '', '1320', '0', '0', '6/30/2016 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27429, 1059, 1983, 19, 'PHARMACY, UNITED HOSPITAL', '', '', '', '', 'JALALPUR', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27430, 1060, 3579, 35, 'FAIKA SITAR HOSPITAL', 'G.T.ROAD', '', '', 'G.T. ROAD', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27431, 1061, 186, 1, 'DR.ABDUL QADOOS BUTT', '', '', '', 'COURT ROAD', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27432, 1062, 1984, 19, 'MEHAR M UZAIR PHARMACY', 'JALALPUR JATTAN', '', '', 'JALALPUR  JATTAN', '', 'Retailer', '', '', '', '1267', '0', '0', '8/21/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27433, 1063, 4162, 4, 'IQBAL MOMORIAL HOSPITAL', 'CIVIL LINE', '', '', 'GUJRAT', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27434, 1064, 3580, 35, 'BLUE GRASS', 'G.T.ROAD.PUNJANKASAN', '', '', 'G.T.ROAD.PUNJANKASANA', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27435, 1065, 3581, 35, 'U-MART', 'G.T.ROAD', '', '', 'G.T.ROAD PUNJANKASANA', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27436, 1066, 3621, 36, 'KHADIM MEDICOSE', '', '', '', 'LAMMAY TEHSIL', 'KHARIAN', 'Retailer', '', '', '', '897', '0', '0', '12/31/2013 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27437, 1067, 4175, 4, 'PHARMACY, ZARA HOSPITAL', '', '', '', 'COURT ROAD', 'GUJRAT 03226916227', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27438, 1068, 636, 6, 'ASLAM MEDICAL STORE', '', '', '', 'MAIN ROAD', 'KIRANWALA', 'Retailer', '', '', '', '486', '486', '486', '12/31/2013 12:00 AM', '12/31/2013 12:00 AM', '12/31/2013 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27439, 1069, 399, 3, 'MEDIZONE PHARMACY', '', '', '', 'KABLI GATE NEAR H.B.L', 'GUJRAT', 'Retailer', '', '', '', '1285', '0', '0', '11/24/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27440, 1070, 1148, 11, 'ABDULLAH PHARMACY', '', '', '', 'UTTAM CHOWK DINGA ROAD', 'KHARIAN', 'Retailer', '', '', '', '1341', '0', '0', '9/1/2016 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27441, 1071, 3174, 31, 'SAJJAD PHARMACY', '', '', '', 'MAIN BAZAR', 'DAULT NAGAR', 'Retailer', '', '', '', '1290', '0', '0', '1/5/2016 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27442, 1072, 1061, 10, 'DR.HAFAZ MUDASAR', '', '', '', 'NOOR HOSPITAL', 'MAIN BAZAR LALAMUSA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27443, 1073, 3620, 36, 'HAMZA PHARMACY', '', '', '', 'ZIA UL HAQ CHOWK', 'MALKA', 'Retailer', '', '', '', '1297', '0', '0', '2/10/2016 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27444, 1074, 346, 3, 'PHARMACY,HAJI REHMAT F/D', '', '', '', '', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27445, 1075, 4017, 40, 'AL KOUSAR PHARMACY', '', '', '', 'MOHA ROAD', 'SARAI ALAMGIR', 'Retailer', '', '', '', '1302', '0', '0', '3/4/2016 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27446, 1076, 1328, 13, 'CHAUDHARY PHARMACY', '', '', '', 'PURANA KOTLA CHOWK', 'KOTLA ARAB ALI KHAN', 'Retailer', '', '', '', '1357', '0', '0', '2/1/2017 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27447, 1077, 4033, 40, 'PHARMACY, AL SHIFA HOSPITAL', '', '', '', '', 'SARI ALAMGIR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27448, 1078, 1833, 18, 'PHARMACY, A.D GONDAL', '', '', '', 'KOTLI PRMANAND', 'KOTLI PRMANAND', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27449, 1079, 424, 42, 'PHARMACY,ISHTIAQ HOSPITAL', '', '', '', '', '', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27450, 1080, 545, 5, 'DR.ZAFAR IQBAL', '', '', '', 'PMDC# 16467-P', 'ZAFAR SURGICAL Hp GUJRAT', 'Doctor', '', '', '', '16467', '0', '0', '12/31/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27451, 1081, 4101, 4, 'PHARMACY,FAZAL WELFAIR SOCITY', 'BHIMBER ROAD', '', '', 'GUJRAT', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27452, 1082, 3572, 35, 'CARE MEDICAL STORE', '', '', '', 'PINDI SULTANPUR', 'KHARIAN', 'Retailer', '', '', '', '0', '1313', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27453, 1083, 15, 1, 'BUTT TRADERS', '', '', '', 'NEAR KIRAN CINMA', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27454, 1084, 4018, 40, 'PHARMACY, DR, SIRAJ MUNEER', '', '', '', '', 'SARI ALAMGIR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27455, 1085, 344, 3, 'DANIYAL MEDICAL STORE', '', '', '', 'OPP DARA MASTER RIAZ', 'VILLAGE BHANGRANWALA', 'Retailer', '34201-0581480-7', '5575776-6', '', '0', '2706', '0', '8/24/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27456, 1086, 4182, 4, 'SHAHID MEDICAL STORE', '', '', '', 'BADSHAHI ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '0', '12/31/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27457, 1087, 2611, 26, 'PHARMACY, NATIONAL MEDICAL/C', '', '', '', 'MAIN ROAN', 'SHADIWAL', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27458, 1088, 3540, 35, 'PHARMACY, DR. SABA NAZ CLINIC', '', '', '', '', 'KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27459, 1089, 137, 13, 'ADNAN PHARMACY', '', '03354421684', '', 'KHARIAN ROAD', 'DAULAT NAGAR', 'Retailer', '', '', '', '1331', '0', '0', '7/14/2016 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27460, 1090, 3180, 31, 'PHARMACY ALI  H/P', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27461, 1091, 527, 5, 'DR.M.IKRAAM', '', '053-3605379', '', '', 'IKRAAM HOSPITAL GUJRAT', 'Doctor', '34201-0669563-5', '2996394-0', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27462, 1092, 3815, 38, 'BOOTS INTERNATIONAL PHARMACY', '', 'KHARIAN CANTT', '', '', 'NEAR C.M.H', 'Retailer', '', '', '', '1330', '0', '0', '6/30/2016 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27463, 1093, 533, 5, 'SADAAT MEDICAL STORE', '', '', '', 'MADINA', 'GUJRAT', 'Retailer', '', '', '', '455', '0', '0', '12/31/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27464, 1094, 4118, 4, 'PHARMACY, SAAD HOSPITAL', '', '', '', 'ALI PUR ROAD', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27465, 1095, 3622, 36, 'PHARMACY, AL SHIFA CLINIC', '', '', '', '', 'MALKA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27466, 1096, 3816, 38, 'ALI PHARMACY', 'SAFF SHIKAN PLAZA', '', '', 'KHARIAN CANTT', '', 'Retailer', '', '', '', '0', '0', '0', '8/4/2016 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27467, 1097, 4034, 40, 'ANSER PHARMACY', '', '0544651350', '', 'NEAR RAILWAY LINE', 'SARI ALAMGIR', 'Retailer', '', '', '', '1346', '0', '0', '10/1/2016 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27468, 1098, 1065, 10, 'DR.ZAFAR AMIN NIAZI', '', '', '', 'NIAZI HOSPITAL', 'G.T.ROAD LALMAUSA', 'Doctor', '34202-8059059-1', '1302903-7', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27469, 1099, 4300, 4, '.', '', '', '', '', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27470, 1100, 26200, 26, 'TAUSEEF MEDICAL STORE', '', '', '', 'SHADIWAL ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27471, 1101, 1066, 10, 'DR.ASIF MALIK', '', '', '', 'MALIK HOSPITAL', 'LALAMUSA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27472, 1102, 4106, 4, 'PHRMACY JAWARIYA LIAQAT', 'JAIL ROAD', '', '', 'GUJRAT', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27473, 1103, 5301, 5, 'CASH & CARY', '', '', '', 'SARWAR GOLD PLAZA', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27474, 1104, 1227, 12, '.AL-ZIA MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'KOTJAMEL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27475, 1105, 5304, 5, 'SHAFIQ G/S', '', '', '', '', '', 'G', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27476, 1106, 5306, 5, 'GOSIA BAKAR', '', '', '', '', '', 'G', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27477, 1107, 5307, 5, 'A.REHMAN BAKAR', '', '0345-5041102', '', '', '', 'G', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27478, 1108, 4309, 4, 'T.MART SP/S', '', '', '', '', '', 'G', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27479, 1109, 3303, 3, 'FUJI G/S', '', '', '', '', '', 'G', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27480, 1110, 2305, 2, 'ZAIQA BAKAR', '', '', '', '', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27481, 1111, 3306, 3, 'WATTAN BAKAR', '', '', '', '', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27482, 1112, 3307, 3, 'GOSIA B/K', '', '', '', '', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27483, 1113, 3301, 3, 'BAO BAKAR', '', '', '', '', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27484, 1114, 5310, 5, 'AL FAJAR S/M', '', '', '', '', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27485, 1115, 5311, 5, 'GULSHAN B/K', '', '', '', '', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27486, 1116, 110, 1, 'ARSHAD PHARMACY', '', '0300-9628726', '', 'NEAR MASJID AL HADEES', 'MUSLIM BAZAR GUJRAT', 'Retailer', '34201-0394115-5', '34201-0394115-5', '', '7037', '0', '0', '10/24/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27487, 1117, 4019, 40, 'PHARMACY, KAMRAN CLINIC', '', '', '', '', 'SARI ALAMGIR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27488, 1118, 4035, 40, 'PHARMACY, BARRA CLINIC', '', '', '', '', 'SARI ALAMGER', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27489, 1119, 4036, 40, 'ZAHEER MEDICAL STORE', '', '', '', 'SHAKRELLA', 'SARAI ALAMGIR', 'Retailer', '', '', '', '911', '0', '0', '12/31/2013 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27490, 1120, 824, 8, 'DR.SYED BABAR HUSSNAIN', '', '', '', 'PMDC# 37156-P', 'AL-MUZAMIL CLINIC GUJRAT', 'Doctor', '34201-3272073-3', '3903458-5', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27491, 1121, 181, 1, 'GREEN PLUS PHARMACY', '', '03334512167', '', '15-A NEW MARGHZAR COLONY', 'GUJRAT', 'Retailer', '', '', '', '1372', '0', '0', '4/2/2017 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27492, 1122, 1311, 13, 'DR.TAYYABA SURAYA', '', '0336-6835837', '', 'PMDC#104274-P', 'AMEER HUSSAIN HOSPITAL KAKRALI', 'Doctor', '34603-5443406-9', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27493, 1123, 526, 5, 'GUJRAT PHARMACY', '', '03127629302', '', 'OPP,IKRAM Hp', 'BHIMBER ROAD GUJRAT', 'Retailer', '', '', '', '1379', '0', '0', '4/25/2017 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27494, 1124, 964, 9, 'KHALID MEDICAL STORE', '', '', '', 'GALI SIRSYED COLLEGE', 'GUJRAT', 'Retailer', '', '', '', '0', '275', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27495, 1125, 3650, 36, 'SITARA BAKARS', '', '', '', 'GULYANA', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27496, 1126, 3530, 35, 'AHMED BROTHERS MEDICAL STORE', '', '', '', 'VILLAGE SHAH SARMUST', 'TEHSIL KHARIAN', 'Retailer', '', '', '', '917', '0', '917', '12/31/2015 12:00 AM', '12/30/1899 12:00 AM', '12/31/2015 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27497, 1127, 4045, 40, 'IMTIAZ MEDICAL STORE', '', '', '', 'DANDI DARA SARA-I-ALAMGIR 0346', '6107165', 'Retailer', '', '', '', '17396', '0', '0', '12/31/2015 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27498, 1128, 40301, 40, 'HABIB BAKERS', '', '', '', 'SARI ALAMGIR', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27499, 1129, 3128, 31, 'SHAHEEN BAKER', '', '', '', 'KOTLA', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27500, 1130, 3129, 31, 'SHAHEEN BAKER', '', '', '', 'MAIN BAZAR  KOTLA', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27501, 1131, 19300, 19, 'PAK BAKERS', '', '', '', 'KHOKHA STOP 0301.3038559', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27502, 1132, 19301, 19, 'AHMED BAKERS', '', '', '', 'FATEH PUR.03076262403', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27503, 1133, 3649, 36, 'SUPPER AFZAL B/K', '', '', '', '', 'GULIANA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27504, 1134, 963, 9, 'DANIYAL MEDICAL STORE', '', '0333-8432553', '', 'NEAR WAHID TRUST HOSPITAL', 'G.T.ROAD GUJRAT', 'Retailer', '34201-2392821-7', '5321437-7', '', '0', '10442', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27505, 1135, 32200, 32, 'FRIENDS TREDERS', '', '', '', 'DINACHAKIAN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27506, 1136, 1500, 1, 'ACME PHARMACEUTICAL CO', '', '', '', '12 RAJA RAM ST(GWAL MANDI)', 'LAHORE', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27507, 1137, 32305, 32, 'CRESANT BAKERS', '', '', '', 'LALAMUSA', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27508, 1138, 12300, 12, 'KHALEEL VARITY STORE', '', '', '', 'BRANALA', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27509, 1139, 12301, 12, 'RASHID BAKERS', '', '', '', 'KOTJEMEL', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27510, 1140, 32102, 32, 'HAFIZ SONS CASH&CARRY', '', '', '', 'DINACHAKIAN LALAMUSA', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27511, 1141, 3850, 38, 'C.M.H', '', '', '', 'KHARIAN CANT', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27512, 1142, 550, 5, 'AL SHEIKH BAKERI', '', '', '', 'CHOWK PAKISTAN', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27513, 1143, 1147, 11, 'PHARMACY DR SABA NAZ', '', '', '', 'G.T.ROAD KHARIAN  CITY', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27514, 1144, 1049, 10, '.MERAN ASLAAN PHARMACY', '', '', '', 'QAZI IMAM SHAH ROAD', 'LALAMUSA', 'Retailer', '', '', '', '1443', '0', '0', '12/22/2017 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27515, 1145, 1156, 11, 'AMBALA BAKARS', '', '', '', 'DINGA ROAD KHARAIN', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27516, 1146, 1332, 13, 'DR.SHOAIB', '', '', '', 'REHMAT MEDICAL & H/C', 'KOTLA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27517, 1147, 160, 1, '.NADEEM MEDICAL STORE', '', '', '', 'NEAR CHOWK PAKISTAN', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27518, 1148, 1347, 13, 'PHARMACY,RAZA HOSPITAL', '', '', '', '', 'KOTLA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27519, 1149, 1154, 11, 'CARE MEDICAL STORE', '', '', '', 'PINDI SULTAN PUR', 'KHARIAN', 'Retailer', '', '', '', '0', '1313', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27520, 1150, 945, 9, 'MAHER CLINC', '', '', '', 'CHILDREN PARK', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27521, 1151, 829, 8, 'DR.WAZAHAT HUSSAIN WARRAICH', '', '', '', 'PMDC# 26569-P', 'HUSSAIN ORTHOCARE Hp GUJRAT', 'Doctor', '3420103405761', '1367185-5', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27522, 1152, 1302, 1, '.DR.SAYDA MEHMOOD', '', '', '', '', 'AL FAZAL Hp GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27523, 1153, 817, 8, 'DR.MOONA MIRZA', '', '', '', 'PMDC# 52905-P', 'MEDICARE HOSPITAL GUJRAT', 'Doctor', '38201-5557214-0', '3566464-9', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27524, 1154, 971, 9, 'DR.ABDUL SATTAR', '', '', '', 'PMDC# 28197-P', 'WAHIT TRUST HOSPITAL GUJRAT', 'Doctor', '', '', '', '28197', '0', '0', '12/31/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27525, 1155, 1517, 15, 'CH. MEDICAL STORE', '', '', '', '', 'SAMAHNI', 'Retailer', '', '', '', '5', '0', '0', '2/17/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27526, 1156, 1519, 15, 'SAARAM MEDICAL STORE', '', '', '', 'SAMAHANI', 'DISTRICT BHIMBER A.K.', 'Retailer', '', '', '', '0', '156', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27527, 1157, 1522, 15, 'VALLEY MEDICAL STORE', '', '', '', 'NEAR RHC', 'SAMAHNI', 'Retailer', '', '', '', '0', '0', '0', '11/14/2018 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27528, 1158, 1822, 18, '.DR.RIFAT ABBAS', '', '', '', '', 'GULYANA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27529, 1159, 1348, 13, 'PHARMACY,KASHMIR METERNTY HOME', '', '', '', '', 'KAKRALI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27530, 1160, 570, 5, 'DR.AZHAR CHAUDRY', '', '', '', 'SKIN CARE CENTER', 'BHIMBER ROAD GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27531, 1161, 956, 9, 'DR.ARIF NAZIR', '', '', '', 'PMDC# 9740-P', 'MEHER CLINIC GUJRAT', 'Doctor', '', '', '', '9740', '0', '0', '12/31/2018 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27532, 1162, 1813, 18, 'HMAZA PHARMACY', '', '', '', '', 'MALKA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27533, 1163, 1157, 11, 'DR.NAHED NASEEB', '', '', '', 'NASEEB SHAHEED HOSPITAL', 'KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27534, 1164, 2211, 22, 'MADINA MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'JOKALIAN', 'Retailer', '', '', '', '0', '38771', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27535, 1165, 2114, 21, 'DR.UZMA MUMTAZ', '', '', '', 'MUNGOWAL', 'PMDC NO.41147-S', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27536, 1166, 1213, 12, 'ANJUM MEDICAL STORE', '', '', '', 'MOIL CHOWK', 'KOTE JAMEL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27537, 1167, 1219, 12, '.UMER MEDICAL STORE', '', '', '', 'MOIL', 'TEHSIL BARNALA', 'Retailer', '', '', '', '208', '0', '0', '6/3/2017 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27538, 1168, 1220, 12, 'SHAHID MEDICAL STORE', '', '', '', '', 'KOTJAMEL', 'Retailer', '', '', '', '164', '0', '0', '6/15/2018 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27539, 1169, 348, 3, 'DR.ADREES KHAN', '', '', '', 'INAYAT BEGUM MEM. HOSPITAL', 'KARANWALA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27540, 1170, 3311, 3, 'PHARMACY,MIRZA KHAN MEM. HOSPITAL', 'FAWARA CHOWK', '', '', 'GUJRAT', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27541, 1171, 1173, 11, 'DR.IQBAL ALVY', '', '', '', '', 'MAQBOL NASEEM HOSPITAL KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27542, 1172, 1067, 10, 'DR.FAISAL NISAR', '', '', '', 'PMDC# 34518-P', 'NISAAR HOSPITAL LALAMUSA', 'Doctor', '', '', '', '34518', '0', '0', '12/31/2023 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27543, 1173, 347, 3, 'DR.Ejaz Ahmed', '', '0300-6224036', '', 'PMDC# 37389-P', 'Al-Fazal Hospital Chak Kmala', 'Doctor', '32203-6389221-5', '7212847', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27544, 1174, 349, 3, 'DR.AZMAT ULLAH', '', '0300-6217125', '', 'PMDC# 62688-P', 'ALAM HOSPITAL CHAK SHAMS', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27545, 1175, 350, 3, 'AKHTAR MEDICAL STORE', '', '0342-8071294', '', 'NEAR POST OFFICE', 'KARIANWALA', 'Retailer', '34201-1959638-9', '1620777-7', '', '0', '4079', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27546, 1176, 2115, 21, 'EIMAAN PHARMACY', '', '', '', 'MAIN BAZAR', 'KHAYAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27547, 1177, 827, 8, 'DR.USAMA MAKHDOOM', '', '', '', 'USAMA CLINIC', 'BHIMBER ROAD GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27548, 1178, 1462, 14, 'USMAN MEDICAL STORE', '', '', '', 'MAIN ROAD', 'KANGRA TEH&DISTT.BHIMBER', 'Retailer', '', '', '', '192', '0', '0', '11/14/2018 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27549, 1179, 1425, 14, 'MADINA MEDICAL STORE', '', '', '', '', 'MOIL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27550, 1180, 1230, 12, 'MADINA MEDICAL STORE', '', '', '', 'MOIL', 'TEHSIL BARNALA A.K.', 'Retailer', '', '', '', '227', '0', '0', '11/1/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27551, 1181, 1460, 14, '.SALEEM MEDICAL STORE', '', '', '', '', 'SOKSAN BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27552, 1182, 1187, 11, 'DR.QUDSIA BANO', '', '0346-6570273', '', 'PMDC# 36358-P', 'FATIMA M/C KHARIAN', 'Doctor', '3420105453934', '4029034-4', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1);
INSERT INTO `dealer_info` (`dealer_table_id`, `software_dealer_id`, `dealer_id`, `dealer_area_id`, `dealer_name`, `dealer_contact_person`, `dealer_phone`, `dealer_fax`, `dealer_address`, `dealer_address1`, `dealer_type`, `dealer_cnic`, `dealer_ntn`, `dealer_image`, `dealer_lic9_num`, `dealer_lic9_exp`, `dealer_lic10_num`, `dealer_lic10_Exp`, `dealer_lic11_num`, `dealer_lic11_Exp`, `dealer_credit_limit`, `dealer_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`, `software_lic_id`) VALUES
(27553, 1183, 1523, 15, 'ZAHID MEDICAL STORE', '', '', '', '', 'PONA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27554, 1184, 1349, 13, 'DR.MARYAM IKRAAM', '', '', '', 'MARYAM HOSPITAL', 'KOTLA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27555, 1185, 1350, 13, '.RAZA PHARMACY', '', '0346-0703330', '', 'MELAD CHOWK', 'CHOUR CHAK TEHSIL KHARIAN', 'Retailer', '', '', '', '2972', '0', '0', '8/30/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27556, 1186, 1351, 13, 'DR.SUMERA AZAM', '', '', '', 'REHMAN HOSPITAL', 'KOTLA ARAB ALI KHAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27557, 1187, 1430, 14, 'DR.SHUMAILA UMAIR', '', '', '', 'KASHMIR ORTHOPEDIC HOSPITAL', 'BHIMBER', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27558, 1188, 1175, 11, 'DR.PERVAIZ AKHTAR', '', '', '', 'AL-NAMET CLINIC', 'KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27559, 1189, 193, 1, '.MADINA MEDICAL STORE', '', '', '', '', 'VILLAGE CHAK SADA', 'Retailer', '', '', '', '0', '1163', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27560, 1190, 9193, 9, '.SHOAIB AMJAD', '', '', '', '', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27561, 1191, 555, 5, 'DR.GOHAR IQBAL', '', '', '', 'GOHAR IQBAL HOSPITAL', 'BHIMBER ROAD GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27562, 1192, 955, 9, 'DR.BUNYAD ALI IKAI', '', '', '', 'SHAMSHAD HOSPITAL', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27563, 1193, 1446, 14, 'MUHAMMAD HEALTH CARE HOSPITAL', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27564, 1194, 1313, 13, 'DR.TARIQ MUSTFA', '', '0312-6200904', '', '', 'HAMEDA TAJAMAL HOSPITAL KOTLA', 'Doctor', '34202-9272322-1', '7747435-1', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27565, 1195, 1232, 12, 'FARMAN MEDICAL STORE', '', '', '', '', 'BARNALA', 'Retailer', '', '', '', '199', '0', '0', '6/5/2018 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27566, 1196, 1352, 13, 'SHAHZAD MEDICAL STORE', '', '', '', 'NEAR ABID KARYANA STORE', 'VILLAGE SOHAL', 'Retailer', '', '', '', '0', '10243', '0', '12/31/2017 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27567, 1197, 560, 5, '.ZARYAB PHARMACY', '', '', '', 'NEAR ASHFAQ BOOK DEPO', 'BHIMBER ROAD GUJRAT', 'Retailer', '', '', '', '6405', '0', '0', '10/18/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27568, 1198, 2317, 23, 'MADNI MEDICAL STORE', '', '03144678808', '', 'HARIAWALA CHOWK,SHADIWAL ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '5692', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27569, 1199, 659, 6, 'HAMMAD MEDICAL STORE', '', '', '', 'SHADIWAL ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27570, 1200, 1069, 10, 'SAFDAR MEDICAL STORE', '', '', '', 'NEAR HASNAT MEDICAL STORE', 'MAIN BAZAR JAURA KARNANA', 'Retailer', '', '', '', '3099', '0', '0', '9/9/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27571, 1201, 1194, 11, 'DR.AFTAAB KHAN', '', '', '', 'POLY CLINIC', 'KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27572, 1202, 1463, 14, 'REHMAN MEDICAL STORE', '', '', '', 'LIAQAT ABAD', 'DISTT.BHIMBER', 'Retailer', '', '', '', '165', '0', '0', '12/31/2017 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27573, 1203, 1233, 12, 'AMJAD MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'BARNALA', 'Retailer', '', '', '', '266', '0', '0', '9/22/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27574, 1204, 1353, 13, 'DR.KASHIF MUSHTAQ', '', '', '', '', 'MUSHTAQ MEM. HOSPITAL KOTLA', 'Doctor', '', '7146098-2', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27575, 1205, 1070, 10, 'DR.GHULAM SARWAR', '', '', '', 'PMDC# 13253-P', 'JORRA', 'Doctor', '', '', '', '13253', '0', '0', '12/13/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27576, 1206, 1071, 10, 'DR.SAIQA SADDIQUE', '', '', '', 'PMDC# 40598-P', 'JORRA', 'Doctor', '', '', '', '40598', '0', '0', '12/31/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27577, 1207, 569, 5, 'DIAMOND PHARMACY', '', '0322-5991314', '', 'NEAR FAMILY HOSPIATL', 'BHIMBER ROAD  GUJRAT', 'Retailer', '34201-9635053-3', '', '', '31025', '0', '0', '3/30/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27578, 1208, 1461, 14, '.SHAOUR MEDICAL STORE', '', '', '', '', 'SOKASAN BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27579, 1209, 1452, 14, 'DR.DAIM IQBAL DAIM', '', '', '', 'NAZIR HOSPITAL', 'BHIMBER', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27580, 1210, 1354, 13, 'DR.MUBASHAR', '', '', '', '', 'MUBASHIR CLINIC FATAHPUR', 'Doctor', '34201-3256989-9', '6925517-8', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27581, 1211, 1234, 12, 'NISAR MEDICAL STORE', '', '', '', '', 'MOIL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27582, 1212, 1824, 18, 'DR.ZAFAR MEHDI', '', '', '', '', 'NASEERA PMDC#45292-P', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27583, 1213, 576, 5, '.ATIYA NAVEED CLINIC', '', '', '', '', 'MAKYANA', 'G', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27584, 1214, 814, 8, 'DR.MADEHA EJAZ', '', '', '', 'AL-KHIDMAT HOSPITAL GUJRAT', 'PMDC NO.60219-P', 'Doctor', '', '', '', '60219', '0', '0', '12/31/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27585, 1215, 662, 6, 'NOOR PHARMACY', '', '', '', 'OPP. SHABIR SHARIF HOSPITAL', 'KUNJA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27586, 1216, 815, 8, '.RAFIQ CLINIC', '', '03320401140', '', '', 'JASOKI', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27587, 1217, 577, 5, 'DR.MUBASHRA SHARIF', '', '', '', 'PMDC# 58975-P', 'Malik Family Hp GUJRAT', 'Doctor', '', '5006211-6', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27588, 1218, 1355, 13, 'AL SADIQ PHARMACY', '', '0300-6292450', '', 'OPP. ASKARI BANK BHIMBHER RD', 'KOTLA ARAB ALI KHAN', 'Retailer', '', '', '', '38923', '0', '0', '1/5/2023 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27589, 1219, 1453, 14, 'ATIF MEDICAL STORE', '', '', '', '', 'PINDI JHUNJA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27590, 1220, 1196, 11, 'DR.RUKHSANA JABEEN', '', '', '', '', 'AL-REHMAN CLINIC KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27591, 1221, 182, 1, 'DR.GULFAM CHEEMA', '', '', '', 'ROSHAN DIABETIC CENTER', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27592, 1222, 1829, 18, '.TAMOOR MEDICAL STORE', '', '', '', 'OPP. RHC', 'MALKA', 'Retailer', '3420244715957', '', '', '0', '1381', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27593, 1223, 1811, 18, 'HAMZA MEDICAL STORE', '', '', '', '', 'MALKA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27594, 1224, 1188, 11, 'BIN DAD PHARMACY', '', '0344-6199399', '', 'SHOUKAT PLAZA,DINGA ROAD', 'KHARIAN', 'Retailer', '', '4313534-0', '', '45624', '0', '0', '10/7/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27595, 1225, 1189, 11, 'DR.SHAHID', '', '', '', 'ARHAM MEDICAL &SURGICAL CENTER', 'MARALA KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27596, 1226, 11100, 11, '.DR.M.FAROOQ', '', '', '', '', 'MOHREE KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27597, 1227, 13102, 13, '.RAJA NAVEED', '', '', '', '', 'KOTLA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27598, 1228, 1357, 13, 'CITY MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'DANDI DARA', 'Retailer', '', '', '', '0', '32906', '0', '10/4/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27599, 1229, 1358, 13, 'MALIK MEDICAL STORE', '', '', '', 'OPP.B.H.UNIT HOSPITAL', 'DANDI DARA', 'Retailer', '', '', '', '0', '1265', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27600, 1230, 1359, 13, 'CHAUDHRY SAFFI MEDICAL STORE', '', '', '', '', 'DANDI DARA', 'Retailer', '', '', '', '800', '0', '0', '12/31/2017 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27601, 1231, 1360, 13, 'ADIL MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'DANDI DARA', 'Retailer', '', '', '', '1561', '1561', '0', '5/4/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27602, 1232, 972, 9, 'DR.SAFDAR IQBAL', '', '', '', 'AL-GHAZALI CLINIC', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27603, 1233, 973, 9, 'DR.M.ZAHID AKRAM', '', '', '', 'PMDC NO.37858-S', 'WAHID TRUST Hospital GUJRAT', 'Doctor', '', '', '', '37858', '0', '0', '12/31/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27604, 1234, 1525, 15, 'PHARMACY,DR.MAQBOL', '', '', '', '', 'POONA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27605, 1235, 1526, 15, '.ZAREEF MEDICAL STORE', '', '', '', 'SANDO CROSS', 'SAMANI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27606, 1236, 1527, 15, 'KHUSHNOOD MEDICAL STORE', '', '', '', '', 'SAMAHANI', 'Retailer', '', '', '', '401', '0', '0', '4/15/2017 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27607, 1237, 820, 8, 'MADINA MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'JEHEURANWALI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27608, 1238, 1361, 13, '.AL-HABIB CLINIC', '', '', '', '', 'DANDI DARA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27609, 1239, 1528, 15, '.ATTIQUE MEDICAL STORE', '', '', '', '', 'MALOOT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27610, 1240, 1072, 10, '.DR.M.BILAL', '', '', '', 'SARDARPURA WELFARE HOSPITAL', 'LALAMUSA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27611, 1241, 1529, 15, '.AL MADINA MEDICAL STORE', '', '', '', '', 'POONA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27612, 1242, 578, 5, 'DR.FARYAD HUSSAIN', '', '', '', '', 'FARYAD CLINIC GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27613, 1243, 1109, 1, 'DR.JAWAD CH.', '', '', '', 'GUJRAT HAIR TRANCE PLANT', 'REHMAN SHAHED ROAD GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27614, 1244, 974, 9, 'DR.SHAMIM AKHTAR', '', '', '', 'KANIZ AKHTAR HOSPITA', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27615, 1245, 822, 8, 'DR.SHAZIA TABASAM', '', '', '', 'JALALPUR JATAN ROAD', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27616, 1246, 1362, 13, 'DR.SUHAIL AHMED', '', '', '', 'MEDICARE Hp DOULTNAGAR', 'PMDC NO.14418-N', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27617, 1247, 1530, 15, '.GILANI MEDICAL STORE', '', '', '', '', 'CHOKI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27618, 1248, 1454, 14, '.AL-SHAFI CLINIC', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27619, 1249, 1363, 13, 'HAMZA MEDICAL STORE', '', '', '', 'MANDIR', 'KOTLA ARAB ALI KHAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27620, 1250, 1455, 14, '.ALI KHAN HOSPITAL', '', '', '', '', 'BHIMBER', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27621, 1251, 1364, 13, '.BOLANI MEDICAL STORE', '', '', '', '', 'BOLANI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27622, 1252, 11101, 11, 'DR.COL.ZULFIQAR', '', '', '', 'ZULFIQAR NEURO HOSPITAL', 'KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27623, 1253, 1456, 14, '.UNITED MEDICAL STORE', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27624, 1254, 1365, 13, '.IMRAN MEDICAL HALL', '', '', '', '', 'BHRING', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27625, 1255, 975, 9, 'DR.FARHAT-ULLAH', '', '', '', 'NASR U LLAH CLINIC GUJRAT', 'PMDC NO.54652-P', 'Doctor', '', '', '', '54652', '0', '0', '12/31/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27626, 1256, 1366, 13, 'IMTIAZ MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'DANDI DARA', 'Retailer', '', '', '', '1066', '0', '0', '9/22/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27627, 1257, 5164, 5, '.MARYAM CLINIC', '', '', '', '', 'MACHIWAL', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27628, 1258, 1106, 1, 'HUSSNAIN MEDICAL STORE', '', '0313-7805338', '', 'SOBAY DAR PLAZA,REHMAN SHAHEED', 'ROAD GUJRAT', 'Retailer', '3420105640431', '5505447-3', '', '0', '63081', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27629, 1259, 1823, 18, '.DR.UMER FAROOQ', '', '', '', 'FATIMA MEDICAL HOSPITAL', 'LEHREE MALKA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27630, 1260, 1107, 1, 'PHARMACY,DR.NADIA', '', '', '', 'SHADULA ROAD', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27631, 1261, 1457, 14, 'NEW VALLY MEDICAL STORE', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '284', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27632, 1262, 976, 9, 'ARIF PHARMACY', '', '', '', 'KALUPURA ROAD', 'GUJRAT', 'Retailer', '', '', '', '1508', '0', '0', '9/8/2018 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27633, 1263, 1108, 1, '.SHIFA PHARMACY', '', '', '', 'MOH.ISLAM NAGAR', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27634, 1264, 1367, 13, '.SKIN CARE CLINIC', '', '', '', 'DR.AMREIN MUSTFA', 'KOTLA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27635, 1265, 564, 5, 'DR.RIAZ MUGHAL', '', '', '', 'BHIMBER ROAD', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27636, 1266, 580, 5, '.DR.M.ILYAS', '', '', '', 'MALHO KHOKHAR', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27637, 1267, 1080, 10, 'DR.RIAZ ASLAM', '', '', '', 'PMDC# 28983-P', 'HAJI IMAAM DIN CLINIC LALAMUSA', 'Doctor', '34202-4064068-9', '1634138-4', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27638, 1268, 839, 8, '.SAJDA CLINIC', '', '', '', '', 'MADINA ROAD GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27639, 1269, 2120, 21, 'RAZA MEDICAL STORE', '', '', '', 'NEAR POLICE CHOWKI', 'LARI ADDA MONGOWAL GHARBI', 'Retailer', '', '', '', '0', '10780', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27640, 1270, 1816, 18, '.DR.M.USMAN', '', '', '', '', 'CHANAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27641, 1271, 1368, 13, '.WAJID DENTEL CLINIC', '', '', '', 'DANDI DARA', '', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27642, 1272, 1450, 14, 'DR.IMRAN SIDDIQUE', '', '', '', 'ALI CHILDREN Hp BHIMBER', 'PMDC NO.1632-AJK', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27643, 1273, 1073, 10, '.ALI MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'DEWNA MANDI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27644, 1274, 180, 1, 'DR.IRFAN AKBAR NAZ', '', '', '', 'PMDC# 27820-P', 'NASEEM AKBAR HOSPITAL GUJRAT', 'Doctor', '34201-1653969-7', '3551498-1', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27645, 1275, 1516, 15, 'DR.MADASAR MUNEER', '', '', '', 'JANDALA SAMANI', 'PMDC NO.4522-AJK', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27646, 1276, 368, 3, 'DR.M.MOAZAM', '', '0331-6283878', '', 'MOHSIN CLINIC KARIANWALA', 'PMDC# 78863-P', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27647, 1277, 1177, 11, 'DR.HUMAYON', '', '', '', 'IQBAL MEMORIAL HOSPITAL', 'MANDHER KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27648, 1278, 369, 3, 'HEERA MEDICAL STORE', '', '', '', 'POLICE STATION ROAD,OPP TANDA', 'PARK.TANDA', 'Retailer', '3420194930283', '5809199-5', '', '0', '6997', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27649, 1279, 977, 9, '.SHAHBAZ MEDICAL STORE', '', '', '', 'MR.ARSHAD', 'MIANA CHOWK VILLAGE LAGAY', 'Retailer', '', '', '', '15749', '0', '15749', '11/29/2019 12:00 AM', '12/30/1899 12:00 AM', '11/29/2019 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27650, 1280, 370, 3, '.DR.FIZZA MUDASSER', '', '0306-6454989', '', '', 'KASAB', 'Doctor', '34201-0319154-7', '5234849-8', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27651, 1281, 1395, 13, 'HAMZA MEDICAL STORE', '', '', '', 'NEAR BHU', 'MANDAHAR', 'Retailer', '', '', '', '25322', '0', '0', '12/9/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27652, 1282, 2310, 23, '.NATASHA CLINIC', '', '', '', '', 'KHOJANWALI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27653, 1283, 2311, 23, 'DR.RIZWAN MAQSOOD', '', '', '', 'MIRZA MEHBOB CLINIC TRIKHA', 'PMDC NO.36466-P', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27654, 1284, 1075, 10, 'LUCKY MEDICAL STORE', '', '', '', '', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27655, 1285, 1426, 14, '.AKBAR KHANAM HOSPITAL', '', '', '', '', 'BHIMBER', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27656, 1286, 1531, 15, 'MUBEEN MEDICAL STORE', '', '', '', '', 'JANDALA SAMAHNI', 'Retailer', '', '', '', '0', '0', '0', '1/1/2018 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27657, 1287, 1427, 14, '.MARYAM DENTAL CLINIC', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27658, 1288, 1235, 12, 'DR.IMRAN SADDIQUE', '', '', '', 'YOUSAF HOSPITAL', 'KADHALA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27659, 1289, 1820, 18, 'PHARMACY,DR.AKHTAR HUSSAIN MIRZA', '', '', '', 'CHORIAN PINDI HASHAM', 'MALKA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27660, 1290, 585, 5, '.DR.ZULFIQAR', '', '', '', '', 'MALHO KHOKHAR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27661, 1291, 1081, 10, 'HAFIZ PHARMACY', '', '', '', 'NEAR TRUMA CENTER', 'GT ROAD LALAMUSA', 'Retailer', '', '', '', '1544', '0', '0', '3/20/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27662, 1292, 2313, 23, '.IMRAN CLINIC', '', '', '', '', 'TRIKHA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27663, 1293, 985, 9, 'DR.SALEEM AHMED', '', '', '', 'PMDC NO.45243-P', 'SARGODHA ROAD GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27664, 1294, 1825, 18, '.RAZIA METENTY HOME', '', '', '', '', 'MALKA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27665, 1295, 1236, 12, 'ZAHID MEDICAL STORE', '', '', '', '', 'KADHALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27666, 1296, 1237, 12, 'DR.M.ABDULLAH', '', '', '', 'FAQEER CLINIC', 'KADHALA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27667, 1297, 1370, 13, 'DR.AKIF SOHAIL', '', '', '', 'AL-SHIFA HOSPITAL', 'KOTLA ARAB ALI KHAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27668, 1298, 1831, 18, '.ADEEL CLINIC', '', '', '', '', 'THOTHA RAYE BAHADER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27669, 1299, 1835, 18, '.MUDASER CLINIC', '', '', '', '', 'THOTHA RAYE BAHADER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27670, 1300, 2314, 23, '.IRAM CLINIC', '', '', '', '', 'KIRANWALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27671, 1301, 1836, 18, '.UMER HOSPITAL', '', '', '', 'DR.UMER', 'GULYANA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27672, 1302, 1238, 12, '.SULMAN MEDICAL STORE', '', '', '', '', 'KOTE JAMEL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27673, 1303, 1837, 18, '.MUKHTAAR CLINIC', '', '', '', '', 'SANTAL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27674, 1304, 586, 5, 'TAIMOOR MEDICAL STORE', 'DR.AKBAR ALI SB', '', '', 'OPP.BABA DERA', 'VILLAGE MALOKHOKHER', 'Retailer', '', '', '', '0', '30702', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27675, 1305, 1239, 12, '.ABDULLAH MEDICAL STORE', '', '', '', '', 'KADHALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27676, 1306, 1240, 12, '.WAQAS MEDICAL STORE', '', '', '', '', 'MAGHLURA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27677, 1307, 1241, 12, '.BILAL MEDICAL STORE', '', '', '', '', 'MAGLURA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27678, 1308, 1838, 18, '.BADHAR CLINIC', '', '', '', '', 'BADHAR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27679, 1309, 1839, 18, '.RASHEED CLINIC', '', '', '', '', 'SADKLAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27680, 1310, 1840, 18, '.ISHTIAQ CLINIC', '', '', '', '', 'JANTLA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27681, 1311, 1429, 14, 'DR.ATT-UR-REHMAN', '', '', '', 'ABDULLAH HEART CARE', 'BHIMBER', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27682, 1312, 823, 8, 'DR.SOBIA AZHAR', '', '', '', 'MARGHZAR CALONI', 'GUJRAT', 'Doctor', '34201-5982752-0', '7593509-2', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27683, 1313, 1532, 15, 'ZAHID MEDICAL STORE', '', '', '', '', 'POONA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27684, 1314, 1533, 15, 'ALI MEDICAL HALL', '', '', '', '', 'POONA', 'Retailer', '', '', '', '0', '0', '0', '8/18/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27685, 1315, 1534, 15, 'AL-SHAFA MEDICAL STORE', '', '', '', '', 'POONA', 'Retailer', '', '', '', '254', '0', '0', '5/1/2018 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27686, 1316, 588, 5, 'DR.SHAGUFTA SHAFIQ', '', '', '', 'BHIMBER ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27687, 1317, 589, 5, 'AKRAM PHARMACY', '', '', '', 'QADIR COLONY', 'BHIMBER ROAD GUJRAT', 'Retailer', '', '', '', '1220', '0', '0', '2/8/2017 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27688, 1318, 1535, 15, 'RAHEEM MEDICAL STORE', '', '', '', '', 'POONA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27689, 1319, 1841, 18, 'DR.AKHTAR HUSSAIN MIRZA', '', '', '', 'YASHFEEN CLINIC SHORRIN', 'PMDC NO.45080-P', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27690, 1320, 1369, 13, 'DR.FRAZ', '', '', '', '', 'KOTLA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27691, 1321, 1375, 13, 'DR.MAJ.RIZWAN HAIDER', '', '', '', '', 'DOLAT NAGAR', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27692, 1322, 1842, 18, '.JABAAR CLINIC', '', '', '', '', 'DOOGA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27693, 1323, 1843, 18, '.AL-SHIFA CLINIC', '', '', '', '', 'MALKA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27694, 1324, 1844, 18, '.NAZIR ORTHO CLINIC', '', '', '', '', 'BADHAR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27695, 1325, 1536, 15, '.BILAL NAEEM MEDICAL STORE', '', '', '', '', 'POONA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27696, 1326, 1845, 18, 'TAJ ALAM MEDICAL STORE', '', '', '', 'NEAR GOVT.MATERNITY HOSPITAL', 'KOTLI BAJAR TEHSIL KHARIAN', 'Retailer', '23202-0716095-5', '', '', '902', '15032', '0', '12/31/2017 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27697, 1327, 1846, 18, 'RAZIYA CLINIC', '', '', '', '', 'MALKA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27698, 1328, 1082, 10, '.RIMSHA CLINIC', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27699, 1329, 1083, 10, '.KASHIF MEDICAL STORE', '', '0333-8517104', '', 'NEAR TRAUMA CENTER', 'LALAMUSA', 'Retailer', '', '', '', '811', '0', '0', '12/31/2017 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27700, 1330, 1245, 12, 'DR.M.ASHRAF', '', '', '', 'SHIFA Hp BARNALA', 'PMDC NO.248-AJK', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27701, 1331, 1465, 14, '.SUBHANI MEDICAL STORE', '', '', '', '', 'SUKASON BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27702, 1332, 1847, 18, 'SHAKEEL MEDICAL STORE', '', '', '', '', 'CHOODO', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27703, 1333, 1371, 13, 'AYUB MEDICAL STORE', '', '', '', 'NEAR PAPPO SHOE SHOP', 'MAIN BAZAR FATEH PUR', 'Retailer', '34201-0521451-5', '', '', '2824', '0', '2824', '8/23/2019 12:00 AM', '12/30/1899 12:00 AM', '8/23/2019 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27704, 1334, 1464, 14, '.NAVEED JAFAR MEDICAL STORE', '', '', '', '', 'D.H.Q. BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27705, 1335, 1372, 13, 'RAZA PHARMACY', '', '', '', '', 'FATHEPUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27706, 1336, 372, 3, '.ZAIN CLINIC', '', '', '', 'DR.TANZEEM AKHTAR', 'JALALPUR SOBTIAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27707, 1337, 201, 20, '.AHMAD MEDICAL STORE', '', '', '', 'OPP.LARI ADDA', 'KUNJA', 'Retailer', '', '', '', '0', '1555', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27708, 1338, 1466, 14, 'BILAL PHARMACY', '', '', '', 'OPP.DHQ HOSPITAL', 'BHIMBER (AK)', 'Retailer', '', '', '', '244', '0', '0', '6/20/2018 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27709, 1339, 1467, 14, '.MUHAMMAD HEALTH CARE', '', '', '', 'DR.NAEEM ASLAM', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27710, 1340, 1468, 14, '.HUSNA CHILD CLINIC', '', '', '', 'DR.SAMARA MUBEEN', 'PINDI JHUNJA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27711, 1341, 1848, 18, '.SOLULAT CLINIC', '', '', '', '', 'MALKA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27712, 1342, 1174, 11, 'ADNAN PHARMACY', '', '', '', 'KHARIAN ROAD', 'DOULAT NAGAR', 'Retailer', '', '', '', '1331', '0', '0', '7/14/2018 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27713, 1343, 1373, 13, 'ADNAN PHARMACY', '', '0335-4421684', '', 'KHARIAN ROAD', 'DOULAT NAGAR', 'Retailer', '34202-7991869-1', '', '', '32696', '0', '0', '5/19/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27714, 1344, 1469, 14, '.LASANI MEDICAL STORE', '', '', '', '', 'SUKASON BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27715, 1345, 1470, 14, 'TAYYAB MEDICAL STORE', '', '', '', '', 'SOKASAN BHIMBR', 'Retailer', '', '', '', '239', '0', '0', '11/1/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27716, 1346, 1471, 14, '.AL SHIFA MEDICAL STORE', '', '', '', '', 'SUKASON BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27717, 1347, 1472, 14, '.NAVEED MEDICAL STORE', '', '', '', '', 'SUKASON BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27718, 1348, 1473, 14, '.ABID MEDICAL STORE', '', '', '', '', 'SUKASON BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27719, 1349, 534, 5, 'DR.ZOHAIB AHMED', '', '', '', 'BHIMBER ROAD', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27720, 1350, 1374, 13, 'NEW RAZA PHARMACY', '', '', '', 'JALALPUR JATTAN ROAD CHOWK', 'FATEH PUR', 'Retailer', '', '', '', '1499', '0', '0', '7/1/2018 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27721, 1351, 2315, 23, '.DR.SADEEQ-E-AKBAR', '', '', '', '', 'GOLAKI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27722, 1352, 1849, 18, '.AHSAAN CLINIC', '', '', '', '', 'CHOKAAR KHURD', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27723, 1353, 373, 3, '.DR.SHAISTA', '', '', '', '', 'BHU BHARAJ', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27724, 1354, 1376, 13, 'Saleem Sons Pharmacy', '', '0306-6207263', '', 'Langrial Chowk', 'KOTLA', 'Retailer', '', '5782459-4', '', '7012', '0', '0', '9/18/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27725, 1355, 1246, 12, 'MOAZZAM MEDICAL STORE', '', '', '', 'THUB ROAD', 'BARNALA', 'Retailer', '', '', '', '261', '0', '0', '3/20/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27726, 1356, 1538, 15, '.DR.HAIDER AZAM', '', '', '', '', 'RHC POONA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27727, 1357, 1377, 13, '.ANWAR CLINIC', '', '', '', '', 'KOTLA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27728, 1358, 1476, 14, 'BILAL MEDICAL STORE', '', 'BHIMBHER', '', '', 'D.H.Q', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27729, 1359, 1539, 15, 'ASLAM MEDICAL STORE', '', '', '', 'KADYALA ROAD', 'CHOWKI', 'Retailer', '', '', '', '224', '0', '0', '11/1/2017 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27730, 1360, 2316, 23, '.SUMERA CLINIC', '', '', '', '', 'GOLAKI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27731, 1361, 1540, 15, 'ARIF MEDICAL STORE', '', '', '', 'BUS STAND', 'SAMANI', 'Retailer', '', '', '', '149', '0', '0', '4/29/2018 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27732, 1362, 1084, 10, 'FAHEEM-UL-ISLAM SHAHEED PHARMACY', '', '', '', 'MALL ROAD', 'LALAMUSA', 'Retailer', '', '', '', '1574', '0', '0', '6/5/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27733, 1363, 1085, 10, 'JADDAH MEDICAL STORE', '', '', '', 'OPP.YASIN SWEETS', 'MAIN BAZAR LALAMUSA', 'Retailer', '', '', '', '0', '1476', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27734, 1364, 1086, 10, 'DR.M.MUNIR', '', '', '', 'PMDC# 20857-P', 'DIABITIES CLINIC LALAMUSA', 'Doctor', '', '', '', '20857', '0', '0', '12/31/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27735, 1365, 374, 3, 'DR.SALMAN SHABBIR', '', '', '', 'PMDC# 80669-P', 'NAZIR CLINIC TANDA', 'Doctor', '34201-7425639-3', '5082217-7', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27736, 1366, 375, 3, '.DR.SHABANA', '', '', '', '', 'BHU HANJRA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27737, 1367, 376, 3, '.DR.KIRAN', '', '', '', '', 'PEROSHAH', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27738, 1368, 1478, 14, 'DR.SAMARA MUBEEN', '', '', '', 'HUSNA CLINIC', 'PINDI JHONJA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27739, 1369, 1087, 10, 'DR.SHAHZAD', '', '', '', 'AL-SHIFA MEDICAL HALL', 'DEWNA MANDI GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27740, 1370, 1381, 13, '.ANWAR CLINIC', '', '', '', '', 'BUZGWAL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27741, 1371, 1541, 15, 'ASAD MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'CHOKI TEHSIL SAMAHNI', 'Retailer', '', '', '', '37', '0', '0', '1/31/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27742, 1372, 978, 9, 'DR.ABDUL-GHAFOOR KHOKHAR', '', '', '', 'KHOKHAR CLINIC', 'RAILWAY ROAD GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27743, 1373, 1481, 14, 'HUSSAIN MEDICAL STORE', '', '', '', '', 'PINDI JJUNJA', 'Retailer', '', '', '', '265', '0', '0', '3/20/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27744, 1374, 1542, 15, '.JANDALA MEDICAL STORE', '', '', '', '', 'JANDALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27745, 1375, 1543, 15, '.WAHEED MEDICAL STORE', '', '', '', '', 'CHOKI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27746, 1376, 1544, 15, 'AL SYED MEDICOSE', '', '', '', '', 'CHOWKI SAMAHNI', 'Retailer', '', '', '', '16', '0', '0', '11/2/2018 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27747, 1377, 1545, 15, '.SIRAJ MEDICAL STORE', '', '', '', '', 'JANDALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27748, 1378, 1546, 15, 'ALI MEDICAL STORE', '', '', '', '', 'SAMANI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27749, 1379, 834, 8, 'SADAT MEDICAL STORE', '', '', '', '', 'MADINA DIS. GUJRAT', 'Retailer', '342010449737', '5571276-6', '', '455', '0', '0', '12/31/2017 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1);
INSERT INTO `dealer_info` (`dealer_table_id`, `software_dealer_id`, `dealer_id`, `dealer_area_id`, `dealer_name`, `dealer_contact_person`, `dealer_phone`, `dealer_fax`, `dealer_address`, `dealer_address1`, `dealer_type`, `dealer_cnic`, `dealer_ntn`, `dealer_image`, `dealer_lic9_num`, `dealer_lic9_exp`, `dealer_lic10_num`, `dealer_lic10_Exp`, `dealer_lic11_num`, `dealer_lic11_Exp`, `dealer_credit_limit`, `dealer_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`, `software_lic_id`) VALUES
(27750, 1380, 377, 3, 'DR.MIRZA M.UMAR', '', '', '', 'lIFE LINE CLINIC CHAK KAMALA', 'PMDC NO.75521-P', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27751, 1381, 1850, 18, '.ISLAMI SHIFAKHANA', '', '', '', '', 'BHAROAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27752, 1382, 2332, 23, '.NAZIA CLINIC', '', '', '', '', 'KHOJA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27753, 1383, 2333, 23, '.SAJID CLINIC', '', '', '', '', 'KHOJA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27754, 1384, 841, 8, '.ZAINAB CLINIC', '', '', '', '', 'B.H.U. CHACHIYAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27755, 1385, 574, 5, 'DR.SULMAN BASHIR', '', '', '', 'BHIMBER ROAD', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27756, 1386, 1163, 11, '.HUMAYON CLINIC', '', '', '', '', 'SITARPURRA KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27757, 1387, 2125, 21, '.DR.AKIF SUHAIL', '', '', '', '', 'MONGOWAL', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27758, 1388, 838, 8, '.QURATULAIN CLINIC', '', '', '', '', 'JAMALPUR GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27759, 1389, 2133, 21, '.UROOJ CLINIC', '', '', '', '', 'KHERANWALI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27760, 1390, 378, 3, '.NEW USMAN MEDICAL CENTER', '', '', '', 'TANDA ROAD', 'PEJOKI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27761, 1391, 1433, 14, 'DUA MEDICAL STORE', '', '', '', '', 'DANDAR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27762, 1392, 1474, 14, 'DUA MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'DANDAR TEH&DISST.BHIMBER', 'Retailer', '', '', '', '275', '0', '0', '11/4/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27763, 1393, 1547, 15, '.MODEL HOSPITAL', '', '', '', 'BANDALA', 'CHOKI', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27764, 1394, 1088, 10, 'EXPRESS PHARMACY', '', '053-7513399', '', 'NEAR NISAR HOSPITAL', 'G.T ROAD LALAMUSA', 'Retailer', '34202-1797337-1', '7610027-5', '', '2248', '0', '0', '8/21/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27765, 1395, 1475, 14, '.ADNAN MEDICAL STORE', '', '', '', '', 'BHIMBER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27766, 1396, 1477, 14, '.DR.FARHAT AMIN JARRAL', '', '', '', '', 'BHIMBER', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27767, 1397, 1851, 18, '.ZAIN CLINIC', '', '', '', '', 'NUNAWALI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27768, 1398, 590, 5, '.SHAKILA CLINIC', '', '', '', 'DR.SHAKILA (MIDWIFE)', 'ADOWAL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27769, 1399, 1247, 12, 'DR.JAHANZAIB HAMID', '', '', '', 'JAHANZAIB CLINIC MOIL A.K', 'PMDC NO.77699-P', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27770, 1400, 840, 8, '.SHIFA CLINIC', '', '', '', 'DR.USMAN', 'MADINA ROAD GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27771, 1401, 592, 5, '.SALEEM ABAAS CLINIC', '', '', '', '', 'BHIMBER ROAD GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27772, 1402, 1383, 13, '.SADDI HOSPTAL', '', '', '', 'DR.FIAZ', 'SABOOR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27773, 1403, 1384, 13, 'DR.SAMRA BIBI', '', '', '', 'PMDC# 63445-P', 'RAZIA BEGHUM HOSPITAL KOTLA', 'Doctor', '34202-7607532-0', '6049719-0', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27774, 1404, 1190, 11, 'DR.ARSHIYA ASLAM', '', '', '', '', 'AHMED HOSPITAL JANDALA KHARIAN', 'Doctor', '38403-1256475-6', '5276097-0', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27775, 1405, 1479, 14, 'JANJUA MEDICAL STORE', '', '', '', 'SAMAHNI CHOWK', 'BHIMBER AZAD KASHMIR', 'Retailer', '', '', '', '240', '0', '0', '5/13/2018 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27776, 1406, 1548, 15, 'ABDULLAH MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'POONA TEHSIL SAMAHNI', 'Retailer', '', '', '', '279', '0', '0', '12/30/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27777, 1407, 1385, 13, 'MALIK MEDICAL STORE', '', '', '', 'NEAR ILYAS KARYANA STORE', 'VILLAGE KHARANA', 'Retailer', '', '', '', '0', '3188', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27778, 1408, 1248, 12, 'DR.UMAR SALAM MIRZA', '', '', '', 'ISLAM MEDICAL COMPLEX', 'BARNALA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27779, 1409, 1386, 13, '.AYYAN CLINIC', '', '', '', '', 'KOTLA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27780, 1410, 593, 5, 'DR.M.ASLAM', '', '', '', 'PMDC# 25574-P', 'BHIMBER ROAD GUJRAT', 'Doctor', '', '8264658-3', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27781, 1411, 1549, 15, '.AZEEM U LLAH MEDICAL STORE', '', '', '', '', 'BANDALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27782, 1412, 1480, 14, 'DR.AZHAR IQBAL', '', '', '', 'SCHOOL ROAD BHIMBER', 'PMDC NO.2302-AJK', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27783, 1413, 594, 5, '.DR.ISHAFIQ AHMAD', '', '', '', '', 'MULL KHAKAR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27784, 1414, 1853, 18, '.KAZMI MEDICAL STORE', '', '', '', '', 'SEHNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27785, 1415, 1854, 18, 'DR.FAIZAN MUHAMMAD', '', '', '', 'PMDC# 105447-P', 'FAIZAN CLINIC CHANNAN', 'Doctor', '34202-5781670-9', '5285366-8', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27786, 1416, 1482, 14, '.QASIM MEDICAL STORE', '', '', '', '', 'PINDI JHUNJA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27787, 1417, 835, 8, 'DR.MASOOD AHMAD AKHTAR', '', '', '', 'PMDC NO.6082-P', 'LORRAN GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27788, 1418, 1855, 18, '.REHMAN CLINIC', '', '', '', '', 'NASEERA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27789, 1419, 1249, 12, '.CHAINAR WELFAIR SOCITY', '', '', '', '', 'KOTEJAMEL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27790, 1420, 2129, 21, '.KIRAN CLINIC', '', '', '', '', 'MONGOWAL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27791, 1421, 1550, 15, '.MAJID MEDICAL STORE', '', '', '', '', 'KADYALA SAMANI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27792, 1422, 990, 9, 'AL-MADINA PHARMACY', '', '', '', 'NEAR DAR MILL GALLA', 'SARGODHA ROAD GUJRAT', 'Retailer', '34201-3785625-5', '5035343-5', '', '41390', '0', '0', '3/27/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27793, 1423, 991, 9, '.DR.AYESHA FAISAL', '', '', '', 'FAISAL HOSPITAL', 'SARGODHA ROAD GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27794, 1424, 2130, 21, '.NASREN CLINIC', '', '', '', '', 'MONGOWAL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27795, 1425, 379, 3, 'DR.SARFRAZ AHMED', '', '0349-7442901', '', 'PMDC# 81682-P', 'RAZAQ CLINIC BHAGOWAL KALAN', 'Doctor', '34201-8743350-9', '8198390-2', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27796, 1426, 380, 3, 'DR.SIRSHAR AHMED', '', '', '', 'HABIB HOSPITAL', 'KARIANWALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27797, 1427, 1251, 12, 'AMIR MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'BARHING', 'Retailer', '', '', '', '273', '0', '0', '11/4/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27798, 1428, 1485, 14, 'NEW VALLY MEDICAL STORE', '', '', '', 'MIRPUR CHOWK', 'BHIMBER CITY', 'Retailer', '', '', '', '284', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27799, 1429, 1856, 18, 'HASSAN PHARMACY', '', '', '', 'OPP.MASJID MUHAMMAD', 'DINGA ROAD CHANNAN', 'Retailer', '', '', '', '10516', '0', '0', '9/28/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27800, 1430, 1551, 15, '.AL-KHIDMAT MEDICAL STORE', '', '', '', '', 'DANNA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27801, 1431, 992, 9, 'DR.ALI SILMAN', '', '', '', 'PMDC NO.41188-P', 'BILAL M/C RAILWAY ROAD GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27802, 1432, 1387, 13, 'MIAN JEE MEDICAL STORE', '', '', '', 'BHIMBER ROAD,OPP.RHC', 'DAULAT NAGAR', 'Retailer', '', '', '', '359', '0', '0', '8/24/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27803, 1433, 1486, 14, 'ABDULLAH MEDICAL STORE', '', '', '', 'SHER JANG COLONY', 'BHIMBER', 'Retailer', '', '', '', '203', '0', '0', '7/17/2018 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27804, 1434, 1388, 13, 'NOOR MEDICAL STORE', '', '0303-6045827', '', 'NEAR RUPAIRY STOP', 'JALALPUR SOBTIAN ROAD', 'Retailer', '', '', '', '0', '26386', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27805, 1435, 2334, 23, '.DR.AFZAL', '', '', '', '', 'KHOJA MONGOWAL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27806, 1436, 596, 5, 'DR.M.BILAL', '', '', '', '', 'INAYAT BASHIR Hp MACHIWAL', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27807, 1437, 1195, 11, 'DR.HAFSA ARIF', '', '', '', 'CHARANWALA FREE DISPENSERY', 'PMDC NO.74022-P', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27808, 1438, 1252, 12, '.MALIQA MEDICAL STORE', '', '', '', '', 'KOTE JAMEL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27809, 1439, 1389, 13, '.HUSSAIN MEDICAL STORE', '', '', '', '', 'DANDI DARA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27810, 1440, 2318, 23, '.DR.SHANZA AFZAL', '', '', '', '', 'DHAROWAL', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27811, 1441, 1552, 15, '.ZAMAN MEDICAL STORE', '', '', '', '', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27812, 1442, 1487, 14, 'BUKHARI MEDICAL STORE', '', '', '', 'OPP.DHQ HOSPITAL', 'BHIMBER', 'Retailer', '', '', '', '204', '0', '0', '4/18/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27813, 1443, 381, 3, '.REHMAT HOSPITAL', '', '', '', '', 'RANGRA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27814, 1444, 1390, 13, 'DR.RAZWAN ASHRAF', '', '', '0301 4923178', 'PMDC# 70604-P', 'HELATH CARE CLINIC KOTLA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27815, 1445, 1815, 18, '.ALI ZAIN MEDICAL STORE', '', '', '', 'NEAR RHC', 'MALKA KHARIAN', 'Retailer', '34202-9346909-0', '', '', '5289', '0', '0', '9/16/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27816, 1446, 1391, 13, 'ADIL PHARMACY', '', '', '', 'BULANI BRANCH NEAR BHU', 'BULANI', 'Retailer', '', '', '', '24714', '0', '0', '12/6/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27817, 1447, 2210, 22, '.SHAZIA SALEEM CLINIC', '', '', '', '', 'NAWALOK', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27818, 1448, 2126, 21, '.DR.SAIRA', '', '', '', 'SAIRA CLINIC', 'MONGOWAL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27819, 1449, 837, 8, '.DR.SHAHID MEHMOOD', '', '', '', '', 'MADINA ROAD GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27820, 1450, 1090, 10, 'DR.SOHAIB UL HASSAN', '', '', '', 'PMDC# 50939-P', 'CAP.IJAZ CLINIC LALAMUSA', 'Doctor', '', '', '', '50939', '0', '0', '12/31/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27821, 1451, 1197, 11, 'DR.JAMMSHED DILAWAR', '', '', '', 'PMDC# 57876-P', 'DILAWAR Hp Dinga Road KHARIAN', 'Doctor', '35201-9938348-1', '7343632-1', '', '57876', '0', '0', '12/31/2024 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27822, 1452, 1198, 11, 'DR.SYED MUQADDAS ALI SHAH', '', '', '', 'PMDC# 17415', 'ALI CHILDREN CLINIC KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27823, 1453, 383, 3, 'DR.SOBIA AZHAR', '', '', '', '', 'TANDA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27824, 1454, 2319, 23, 'RASOOL MEDICAL STORE', '', '', '', 'HARIANWALA CHOWK', 'GUJRAT', 'Retailer', '34201-5679519-3', '5697896-5', '', '0', '31277', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27825, 1455, 384, 3, 'DR.NUSRA ULFAT', '', '', '', 'AL-SHIFA CLINIC KARIANWALA', 'PMDC NO.84960-P', 'Retailer', '', '', '', '84960', '0', '0', '12/31/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27826, 1456, 1199, 11, 'DR.SALMAN AHMAD', '', '', '', 'PMDC# 46690-P', 'SALMANS SKIN CLINIC KHARIAN', 'Doctor', '', '', '', '46690', '0', '0', '12/31/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27827, 1457, 2320, 23, '.YASMIN CLINIC', '', '', '', '', 'SHADIWAL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27828, 1458, 2321, 23, 'SHABBIR & SONS MEDICAL STORE', '', '', '', 'NEAR SHADIWAL CHOWKI', 'MAIN BAZAR SHADIWAL', 'Retailer', '', '', '', '0', '32435', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27829, 1459, 385, 3, 'DR.Ibrar Ahmed', '', '', '', 'PMDC# 87517-P', 'KOTLI BHAGWAN', 'Retailer', '', '', '', '87517', '0', '0', '12/31/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27830, 1460, 1915, 19, 'WAQAR PHARMACY', '', '0345-4303907', '', 'NEAR CMH GATE', 'KHARIAN CANTT', 'Retailer', '1310109806821', '2389800-3', '', '32428', '0', '0', '4/26/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27831, 1461, 1091, 10, 'ARSHAD MEDICAL STORE', '', '', '', '', 'LALAMUSA', 'Retailer', '34601-0776477-7', '2264120-3', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27832, 1462, 386, 3, '.DR.ABID', '', '', '', '', 'LAKHANWAL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27833, 1463, 1393, 13, 'USMAN MEDICAL STORE', '', '0306-3387175', '', 'OPP.UBL BANK CHOWK FATEHPUR', 'GUJRAT', 'Retailer', '35202-1135215-3', '', '', '0', '41934', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27834, 1464, 1553, 15, '.DR.WAHEED', '', '', '', '', 'BINDI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27835, 1465, 1092, 10, 'DR.FAHAD USMAN', '', '', '', 'PMDC# 56889-P', 'USMAN HOSPITAL LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27836, 1466, 1488, 14, 'DR.NASREEN AKHTER', '', '', '', 'CHILDREN Hp BHIMBER', 'PMDC NO.147-AJK', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27837, 1467, 1093, 10, 'ZAFAR PHARMACY', '', '', '', 'BEHIND POLICE STATION SADAR', 'MOH.KARIM PURA LALAMUSA', 'Retailer', '34202-2721578-9', '5563668-3', '', '32791', '0', '0', '5/19/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27838, 1468, 2213, 22, 'SUFYAN MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'PINDI KALU', 'Retailer', '', '', '', '0', '15206', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27839, 1469, 993, 9, 'BASIT MEDICAL STORE', '', '', '', 'MOHALLAH REHMAN PURA', 'LUNDPUR ROAD GUJRAT', 'Retailer', '', '', '', '0', '32725', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27840, 1470, 1857, 18, 'ATIF MEDICAL STORE', '', '', '', 'NEAR CH.FAZAL DAD Hp', 'KOTLI BAJAR', 'Retailer', '', '', '', '0', '31126', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27841, 1471, 994, 9, 'AKHTAR MEDICAL STORE', '', '', '', 'MOHALLAH QUTAB ABAD', 'SARGODHA ROAD GUJRAT', 'Retailer', '', '', '', '0', '32723', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27842, 1472, 1554, 15, 'YASIR MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'CHOWKI', 'Retailer', '', '', '', '294', '0', '0', '4/14/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27843, 1473, 1555, 15, 'FAHAD MEDICAL STORE', '', '', '', '', 'CHOWKI', 'Retailer', '', '', '', '254', '0', '0', '5/1/2018 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27844, 1474, 1094, 10, 'DR.AFTAB AHMED', '', '', '', 'PMDC# 56878-P', 'NOOR CLINIC PANJAN KISANA', 'Doctor', '', '', '', '56878', '0', '0', '12/31/2018 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27845, 1475, 13121, 13, 'DR.RAHEELA RANI', '', '', '', 'PMDC# 92716-P', 'CH.MEHNDI KHAN CLINIC MACHIWAL', 'Doctor', '', '', '', '92716', '0', '0', '9/8/2018 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27846, 1476, 825, 8, 'DR.SYED OSAMA TALAT', '', '', '', 'GUJRAT', 'PMDC# 54178-P', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27847, 1477, 1394, 13, 'BILAL MEDICAL STORE', '', '', '', '', 'DANDY WALA BULANI', 'Retailer', '', '', '', '0', '32875', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27848, 1478, 5167, 5, '.MEDI CARE PHARMACY', '', '', '', 'OPP.IKRAM HOSPITAL', 'BHIMBER ROAD GUJRAT', 'Retailer', '', '', '', '33035', '0', '0', '5/24/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27849, 1479, 1396, 13, 'DAWOOD RAZA MEDICAL STORE', '', '', '', 'NEAR WATAR SUPPLYOFFICE', 'MAIN BAZAR FATEHPUR', 'Retailer', '', '', '', '0', '10154', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27850, 1480, 1556, 15, 'DR.HAROON KHURSHIED', '', '', '', 'JANDALA', 'PMDC NO.4332-AJK', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27851, 1481, 1397, 13, 'UMAR PHARMACY', '', '0348-0451304', '', '', 'CHOWK DOULAT NAGAR', 'Retailer', '34201-0348507-7', '6367178-2', '', '66738', '0', '0', '3/4/2023 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27852, 1482, 995, 9, 'DR.MADIHA EJAZ', '', '', '', 'PMDC NO.60219-P', 'LINK RAILWAY ROAD GUJRAT', 'Doctor', '', '', '', '60219', '0', '0', '12/31/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27853, 1483, 996, 9, 'DR.ABDUL MUNIR', '', '', '', 'TANVEER CLINIC', 'RAILWAY Rd GUJRAT PMDC#100776', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27854, 1484, 2212, 22, '.DR.AQSA', '', '', '', 'RHC JOKALIAN', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27855, 1485, 1095, 10, 'DR.ASIF IQBAL', '', '', '', 'PMDC# 25305-P', 'MALIK Hp MAIN BAZAR LALAMUSA', 'Doctor', '34202-0815747-5', '2339525-7', '', '25305', '0', '0', '12/31/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27856, 1486, 390, 3, '.MAQSOOD MEDICAL STORE', '', '', '', 'OPP.POLICE CHOWKI', 'MAIN ROAD JALAL PUR SOBTIAN', 'Retailer', '3420105773539', '1111047-3', '', '5181', '0', '0', '10/23/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27857, 1487, 1398, 13, 'DR.M.IRSHAD', '', '', '', 'KOTLA', 'PMDC NO.14476-P', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27858, 1488, 1399, 13, 'DR.M.UMAR WAQAS', '', '', '', 'HABIB CLINIC DANDIDARA', 'PMDC NO.73244-P', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27859, 1489, 1557, 15, 'DR.HAIDER AZIM', '', '', '', 'AL ZAMAN M/C POONA', 'PMDC NO.3440-AJK', 'Doctor', '', '', '', '3440', '0', '0', '12/31/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27860, 1490, 997, 9, 'DR.CH.IMRAN SARWAR', '', '', '', 'SARGHODA ROAD GUJRAT', 'PMDC NO.41788-P', 'Doctor', '', '', '', '41788', '0', '0', '12/31/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27861, 1491, 2323, 23, '.DR.NABEIL SHAIKH', '', '', '', 'SAMAMOLA', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27862, 1492, 5168, 5, 'DR.SADIA IRFAN', '', '', '', 'POLY CLINIC BIMBER', 'ROAD GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27863, 1493, 998, 9, 'MASOOD MEDICAL HALL', '', '', '', 'PAKKI GALI', 'RAILWAY ROAD GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '12/30/1899 12:00 AM', '10/10/2019 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27864, 1494, 1242, 12, '.SHAKEEL MEDICAL STORE', '', '', '', 'BARNALA ROAD BHRING', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27865, 1495, 391, 3, '.DR.UZMA SHAHEEN', '', '', '', 'FATIMA METERNTY HP', 'KARINANWALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27866, 1496, 392, 3, 'DR MIRZA M.UMAR', '', '', '', 'NAYAB CLINIC KUNGRA', 'PMDC# 75521-P', 'Doctor', '', '', '', '75521', '0', '0', '12/31/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27867, 1497, 11116, 11, 'DR.M.AMIN', '', '', '', 'PMDC# 16026-N', 'SYED MEDICAL CENTER LALAMUSA', 'Doctor', '34202-9315656-5', '5297052-3', '', '16026', '0', '0', '12/31/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27868, 1498, 13100, 13, 'UMER PHARMACY', '', '', '', '', 'CHOWK DOULAT NAGAR', 'Retailer', '', '', '', '35107', '0', '0', '7/20/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27869, 1499, 2322, 23, '.DR.IBRAHEEM', '', '', '', '', 'SHADIWAL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27870, 1500, 393, 3, '.DR.SAIRA IRFAN', '', '', '', 'AL-SHARIF MET. HOME', 'BEHLOL PUR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27871, 1501, 13101, 13, 'DR.ALI HANIF', '', '', '', 'PMDC# 94742-P', 'MURAD EYE HOSPITAL KOTLA', 'Doctor', '', '', '', '94742', '0', '0', '3/14/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27872, 1502, 832, 8, 'RASHID MEDICAL STORE', '', '', '', 'NEAR UNION COUNCIL OFFICE', 'MADINA SYEDAN GUJRAT', 'Retailer', '', '', '', '0', '7056', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27873, 1503, 13103, 13, '.DR.ASHFAQ', '', '', '', 'DLAWRPUR', '', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27874, 1504, 13104, 13, 'DR.SALMAN AHMAD', '', '', '', 'PMDC# 46690-P', 'KOTLA', 'Doctor', '', '', '', '46690', '0', '0', '12/31/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27875, 1505, 1097, 10, 'DUA PHARMACY', '', '', '', 'FAIZ CHOWK OPP. TEHSIL LEVEL', 'HOSPITAL LALAMUSA', 'Retailer', '34202-6411313-5', '', '', '32717', '0', '0', '6/19/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27876, 1506, 5170, 5, 'SALEEM MEDICAL STORE', '', '', '', 'LALAZAR COLONY, NEARZAMINDAR', 'COLLEGE, BHIMBER ROAD GUJRAT', 'Retailer', '', '', '', '0', '34458', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27877, 1507, 1098, 10, 'MUGHAL MEDICAL STORE', '', '', '', 'MALL ROAD,CAMPING GROUND', 'LALAMUSA', 'Retailer', '34202-0740352-1', '34202-0740352-1', '', '3292', '0', '0', '9/5/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27878, 1508, 1099, 10, 'JEDDAH PHARMACY', '', '', '', 'OPPOSITE YASIR SWEETS', 'MAIN BAZAR LALAMUSA', 'Retailer', '', '', '', '32365', '0', '0', '4/25/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27879, 1509, 11106, 11, 'DR.KHIZAR HAYAT KHAN', '', '', '', 'PMDC#11671-P', 'KHIZAR Hp DINGA ROAD KHARIAN', 'Doctor', '', '', '', '11671', '0', '0', '12/31/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27880, 1510, 1858, 18, '.NAWAZ CLINIC', '', '', '', '', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27881, 1511, 1859, 18, 'AWAN MEDICAL STORE', '', '', '', '', 'GLYANA CHOWK', 'Retailer', '', '', '', '0', '3244', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27882, 1512, 5171, 5, '.KHAWAJGAN DIALICES CENTER', '', '', '', '', 'BHIMBER ROAD GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27883, 1513, 13105, 13, 'DR.SHAHARYAR BASIT', '', '0333-8462426', '', 'PMDC# 92254-P', 'IQBAL CLINIC DOULAT NAGAR', 'Doctor', '34201-0347089-7', '7597334-2', '', '92254', '0', '0', '12/31/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27884, 1514, 11107, 11, 'DR.KHALID JAMEEL', '', '', '', 'PMDC# 12302-P', 'HASAN M/C GT ROAD KHARIAN', 'Doctor', '', '', '', '12302', '0', '0', '12/31/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27885, 1515, 2123, 21, '.AL-DAIM MEDICAL COMPLEX', '', '', '', 'TAPIALA ROAD', 'MANGOWAL', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27886, 1516, 1860, 18, '.AL-SHIFA CLINIC', '', '', '', '', 'BADHAR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27887, 1517, 10100, 10, 'DR.IKRAM M.ZAMAN NASEEM', '', '', '', 'PMDC# 82294-P', 'ITTIFAQ HEALTHCARE DEWNA MANDI', 'Doctor', '', '', '', '82294', '0', '0', '12/31/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27888, 1518, 1861, 18, 'AL GHANI PHARMACY', '', '0306-5198413', '', 'JALAL PUR JATTAN ROAD', 'GULIANA', 'Retailer', '34202-0693324-1', '', '', '37439', '0', '0', '8/13/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27889, 1519, 10101, 10, 'MEDI GREEN PHARMACY', '', '', '', 'NEAR BANK AL HABIB', 'GT ROAD LALAMUSA', 'Retailer', '34202-7767257-7', '5817166-7', '', '38439', '0', '0', '12/5/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27890, 1520, 5173, 5, 'DR.ANUM AITZAZ', '', '', '', 'PMDC# 74205-P', 'BASHIR BEGUM HOSPITAL GUJRAT', 'Doctor', '34603-4846556-2', '6895427-5', '', '51109', '0', '0', '12/31/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27891, 1521, 394, 3, 'HUSSAIN MEDICAL STORE', '', '0300-6186053', '', 'TANDA ROAD', 'KARIANWALA', 'Retailer', '', '', '', '38078', '0', '0', '11/17/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27892, 1522, 999, 9, 'DR.WASEEM SHAFI', '', '', '', 'SHADIWAL ROAD GUJRAT', 'PMDC#27345-P', 'Retailer', '', '', '', '27345', '0', '0', '12/31/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27893, 1523, 10102, 10, 'BILAL BROTHERS MEDICAL STORE', '', '', '', 'Adjacent Faizullah Hospital', 'G.T ROAD LALAMUSA', 'Retailer', '34202-0773607-1', '2860562-4', '', '7593', '0', '0', '9/21/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27894, 1524, 13106, 13, 'DR.SAEEDA NOREEN UL HASSAN', '', '', '', 'FAIZ E MIRAAN HOSPITAL KOTLA', 'PMDC# 75714-P', 'Doctor', '', '', '', '75714', '0', '0', '12/31/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27895, 1525, 9100, 9, '.MOHSIN MS', '', '', '', '', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27896, 1526, 11109, 11, 'DR.M.AFZAL', '', '', '', 'PMDC# 26052-S', 'WALEED CLINIC KHARIAN', 'Doctor', '', '', '', '26052', '0', '0', '12/31/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27897, 1527, 13107, 13, 'DR.M.ADREES', '', '0347-6514149', '', 'PMDC# 92551-P', 'CITY CLINIC CHOR CHAK', 'Doctor', '34202-0144724-3', '', '', '92551', '0', '0', '9/5/2018 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27898, 1528, 13108, 13, '.NABEEL CLINIC', '', '', '', '', 'KOTLA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27899, 1529, 13109, 13, 'SIKANDAR MEDICAL STORE', '', '', '', 'SIDWAL', 'KOTLA ARAB ALI KHAN', 'Retailer', '34201-0452816-9', '5971317-6', '', '37603', '0', '0', '11/5/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27900, 1530, 1962, 19, 'KHAN MEDICAL STORE', '', '', '', 'NASEERA', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27901, 1531, 1862, 18, 'KHAN MEDICAL STORE', '', '', '', '', 'NASEERA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27902, 1532, 10103, 10, 'AHMED MEDICAL STORE', '', '', '', 'HAIDERI CHOWK,CAMPING GROUND', 'LALAMUSA', 'Retailer', '34202-0717679-5', '6466120-7', '', '39119', '0', '0', '1/15/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27903, 1533, 13110, 13, 'FARHAN PHARMACY', '', '', '', 'CHOWK', 'DOULAT NAGAR', 'Retailer', '', '', '', '35571', '0', '0', '9/13/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27904, 1534, 395, 3, 'DR.NASIR IQBAL CHAUDHRY', '', '', '', 'FAMILY CLINIC HAJIWALA ROAD', 'KARIANWALA PMDC#1207-AJK', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27905, 1535, 11115, 11, '.MEHER MEDICAL SERVICE', '', '', '', '', 'VILLAGE GANJA', 'Retailer', '', '', '', '33120', '0', '0', '9/11/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27906, 1536, 979, 9, 'DR.ALI HASNAIN BHUTTA', '', '', '', 'PMDC# 78777-P', 'BHUTTA CLINIC GUJRAT', 'Doctor', '', '', '', '78777', '0', '0', '12/31/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27907, 1537, 396, 3, 'AHMED MEDICAL STORE', '', '', '', 'RANGPUR CHOWK,NEAR', 'JALALPUR SOBTIAN GUJRAT', 'Retailer', '', '', '', '0', '40732', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27908, 1538, 13111, 13, 'DR.SYED ASIM AKHTAR', '', '', '', 'PMDC# 49811-P', 'ASHRAF CHILDREN HP KOTLA', 'Doctor', '', '', '', '49811', '0', '0', '12/31/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27909, 1539, 2324, 23, '.DR.FARIHA SHAKIR', '', '', '', 'KHOJYAWALI', '', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27910, 1540, 10120, 10, 'AITZAZ', '', '', '', 'LALAMUSA', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27911, 1541, 13112, 13, '.NOOR MATERNITY HOSPITAL', '', '', '', '', '', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27912, 1542, 10105, 10, 'AL SHIFA PHARMACY', '', '', '', 'MOHALLAH RAHMATABAD', 'GT ROAD LALAMUSA', 'Retailer', '', '', '', '39306', '0', '0', '1/26/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27913, 1543, 2214, 22, 'ALI AKBAR MEDICAL STORE', '', '', '', '', 'KHOJIANWALI', 'Retailer', '', '', '', '0', '40690', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27914, 1544, 11103, 11, 'NEW AL SHIFA MEDICAL STORE', '', '', '', 'NEAR ALLAH WALI MASJID', 'GULYANA ROAD KHARIAN', 'Retailer', '', '', '', '0', '41394', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27915, 1545, 13113, 13, 'DR.M.ALAM', '', '', '', 'PMDC# 11207-P', 'KOTLA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27916, 1546, 13114, 13, '.DR.IRSHAD AYYAZ', '', '', '', 'AL SHIFA CLINIC', 'KOTLA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27917, 1547, 2127, 21, '.DR.TASAWAR', '', '', '', '', 'MONGOWAL', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27918, 1548, 5121, 5, 'DR.MUHAMMAD ALI', '', '', '', 'PMDC# 57905-P', 'AL-FLAH M/C QADIR COLONY', 'Doctor', '', '', '', '57905', '0', '0', '12/31/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27919, 1549, 11111, 11, 'EXPRESS PHARMACY', '', '0344-4544027', '', 'OPPOSITE SESSION COURT', 'GT ROAD KHARIAN', 'Retailer', '', '', '', '41031', '0', '0', '3/4/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27920, 1550, 1864, 18, 'MUDASSAR MEDICAL STORE', '', '', '', 'NEAR BHU HOSPITAL', 'THUTHA RAI BHADUR', 'Retailer', '34202-8313443-5', '8176386-3', '', '0', '41680', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27921, 1551, 10106, 10, 'DR.ASMA LATIF', '', '', '', 'PMDC# 52716-P', 'CITY M/C LALAMUSA', 'Doctor', '34202-8039925-8', '5665762-1', '', '52716', '0', '0', '12/31/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27922, 1552, 1865, 18, 'DR.ASAD ABBAS', '', '', '', 'SHSHZAD CLINIC ALI CHAAK', 'PMDC# 90930-P', 'Doctor', '', '', '', '90930', '0', '0', '12/31/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27923, 1553, 1866, 18, 'SHAHEEN MEDICAL STORE', '', '', '', 'CHANNAN ADDA', 'TEHSIL KHARIAN', 'Retailer', '', '', '', '0', '3415', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27924, 1554, 1867, 18, 'AMIR PHARMACY', '', '0342-8646208', '', '', 'GULYANA', 'Retailer', '34202-0804070-9', '5311178-8', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27925, 1555, 983, 9, 'QAAZI MEDICAL STORE', '', '0331-0141001', '', 'NEAR ADIL MARKET', 'SARGODHA ROAD GUJRAT', 'Retailer', '', '', '', '0', '41936', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27926, 1556, 5123, 5, 'DR.SAAD ULLAH', '', '', '', 'PMDC# 84438-P', 'CHANDALA ROAD GUJRAT', 'Doctor', '', '', '', '84438', '0', '0', '12/31/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27927, 1557, 2325, 23, '.DR.BABAR', '', '', '', 'DHAROWAL', '', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27928, 1558, 1868, 18, 'DR.M.UMER IJAZ', '', '', '', 'PMDC# 69248-P', 'MARYAM CLINIC NASEERA', 'Retailer', '', '', '', '69248', '0', '0', '12/31/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27929, 1559, 5109, 5, 'DR.SYED OSAMA TALAT', '', '0303 6804739', '', 'PMDC# 54178-P', 'TALAT HOSPITAL GUJRAT', 'Doctor', '3420190492459', '8919478-1', '', '54178', '0', '0', '12/31/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27930, 1560, 984, 9, 'BASHIR PHARMACY', '', '', '', 'INSIDE STAFF GALA,', 'MAIN GALA AMIN ABAD GUJRAT', 'Retailer', '', '', '', '42363', '0', '0', '5/17/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27931, 1561, 10107, 10, 'IFA PHARMACY', '', '0537-512145', '', 'SKY WAY BUILDING', 'RAILWAY ROAD LALAMUSA', 'Retailer', '', '', '', '42968', '0', '0', '7/1/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27932, 1562, 2326, 23, 'ANWAR MEDICAL STORE', '', '', '', 'HARYANWALA CHOWK', 'SAHDIWAL ROAD GUJRAT', 'Retailer', '', '', '', '0', '39265', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27933, 1563, 1869, 18, '.SHABIR CLINIC', '', '', '', '', 'PANJWARRIAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27934, 1564, 9105, 9, 'DR.KASHIF MEHMOOD', '', '', '', 'PMDC# 52056', 'IQBAL CLINIC GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27935, 1565, 2327, 23, 'NOOR UL HASSAN PHARMACY', '', '', '', '', 'MAIN BAZAR SHADIWAL', 'Retailer', '', '', '', '50377', '0', '0', '4/12/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27936, 1566, 10108, 10, 'MUSA MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '42729', '0', '0', '6/19/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27937, 1567, 1916, 19, 'MEDASK PHARMACY', '', '', '', 'NEAR CAR PARKING', 'CMH KHARIAN', 'Retailer', '', '7944576-6', '', '42440', '0', '0', '5/22/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27938, 1568, 397, 3, 'ABU BAKAR MEDICAL STORE', '', '', '', 'CHAMB ROAD BARILA SHARIF', '', 'Retailer', '', '', '', '0', '43193', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27939, 1569, 5124, 5, 'DR.RAQIA NOOR', '', '0300-6286380', '', 'PMDC# 30874-P', 'SHAHZAD Hp GUJRAT', 'Doctor', '37302-5531220-4', '', '', '30874', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27940, 1570, 1870, 18, '.FARHAN CLINIC', '', '', '', '', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1);
INSERT INTO `dealer_info` (`dealer_table_id`, `software_dealer_id`, `dealer_id`, `dealer_area_id`, `dealer_name`, `dealer_contact_person`, `dealer_phone`, `dealer_fax`, `dealer_address`, `dealer_address1`, `dealer_type`, `dealer_cnic`, `dealer_ntn`, `dealer_image`, `dealer_lic9_num`, `dealer_lic9_exp`, `dealer_lic10_num`, `dealer_lic10_Exp`, `dealer_lic11_num`, `dealer_lic11_Exp`, `dealer_credit_limit`, `dealer_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`, `software_lic_id`) VALUES
(27941, 1571, 1871, 18, '.AYESHA HOSPITAL', '', '', '', '', 'NASEERA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27942, 1572, 10109, 10, 'IFA PHARMACY', '', '', '', 'SKY WAY BUILDING RAILWAY ROAD', 'LALAMUSA', 'Retailer', '', '', '', '42968', '0', '0', '7/1/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27943, 1573, 1872, 18, '.SHAHZAD MEDICAL STORE', '', '', '', '', 'CHAKPRANA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27944, 1574, 11112, 11, '.DR.KHURSHEED ABU ADAN', '', '', '', 'NAJAM HOSPITAL KHARIAN', '', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27945, 1575, 11113, 11, '.DENTAL STUDIO', '', '', '', 'NEAR SAJDA HAFEEZ HOSPITAL', 'GT ROAD KHARIAN', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27946, 1576, 10110, 10, 'MADNI MEDICOSE', '', '', '', 'NEAR MALIK HOSPITAL', 'MAIN BAZAR LALAMUSA', 'Retailer', '34202-0431998-9', '4233659-7', '', '4200', '0', '0', '9/9/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27947, 1577, 1873, 18, 'HASSAN PHARMACY', '', '', '', '', 'DINGA ROAD CHANAN', 'Retailer', '', '', '', '10516', '0', '0', '9/20/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27948, 1578, 5125, 5, 'DR.HAFEEZ U REHMAN', '', '', '', 'PMDC# 12413P', 'USAMA CLINIC GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27949, 1579, 9101, 9, 'AL JANNAT PHARMACY', '', '', '', 'SERVICE MORR', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27950, 1580, 13115, 13, 'AHSAN MEDICAL STORE', '', '0301-8352280', '', 'OPPOSITE GHOSIA MASJID', 'HANJ', 'Retailer', '', '', '', '0', '18114', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27951, 1581, 13116, 13, 'DR.BASHARAT MUNEER', '', '', '', 'KOTLA', '', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27952, 1582, 10115, 10, 'DR.TAJAMAL HUSSAIN', '', '', '', 'PMDC# 99796-P', 'SHIFA HOSPITAL DEWNA MANDI', 'Retailer', '', '', '', '24997', '0', '0', '12/31/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27953, 1583, 10116, 10, 'DAWAI PLUS PHARMACY', '', '', '', 'INFRONT TEHSIL LEVEL HOSPITAL', 'CAMPING GROUND LALAMUSA', 'Retailer', '34202-2853544-9', '5353612-7', '', '43483', '0', '0', '7/22/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27954, 1584, 2328, 23, 'DR.AZHAR IHSAN', '', '', '', 'Opp G.M.H Mongowal Gharbi', 'PMDC# 41164-P', 'Doctor', '', '', '', '41164', '0', '0', '12/31/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27955, 1585, 2329, 23, 'CHAUDHRARY MEDICAL STORE', '', '', '', 'NEAR PUL REHMANIA MAIN', 'DINGA ROAD SHAHABDIWAL', 'Retailer', '', '', '', '0', '6192', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27956, 1586, 2330, 23, 'DAWOOD MEDICAL STORE', '', '', '', 'SAMMA ROAD BUS STAND', 'LANGAY', 'Retailer', '', '', '', '0', '35778', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27957, 1587, 1874, 18, '.UMAR HOSPITAL', '', '', '', '', 'GULYANA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27958, 1588, 10117, 10, 'DR.SARFRAZ AHMAD RANA', '', '', '', 'PMDC# 22329-P', 'FATIMA HOSPITAL DEWNA MANDI', 'Doctor', '', '', '', '22329', '0', '0', '12/31/2023 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27959, 1589, 1875, 18, 'AL FAZAL MEDICAL STORE', '', '', '', 'PINDI HASHIM', '', 'Retailer', '', '', '', '0', '41921', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27960, 1590, 10118, 10, 'DR.ABDUL MUNIR', '', '', '', 'PMDC# 100776-P', 'FIRST M/C DEOWNA MANDI', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27961, 1591, 826, 8, 'DR.UMAIR WAHEED', '', '', '', 'SHADMAN WELFARE HOSPITAL', 'SHADMAN ROAD PMDC#100109-P', 'Doctor', '', '', '', '100109', '0', '0', '9/16/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27962, 1592, 2111, 21, 'DR.SALEEM AHMED', '', '', '', 'AL NOOR SURGICAL HOSPITAL', 'MANGOWAL PMDC#45243-P', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27963, 1593, 13117, 13, 'DR.ADNAN RASOOL', '', '', '', 'SHAUKAT DENTAL KOTLA', '', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27964, 1594, 13118, 13, 'DR.FAISAL REHMAN', '', '', '', 'KOTLA', 'PMDC# 2288-AJK', 'Doctor', '', '', '', '2288', '0', '0', '12/31/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27965, 1595, 2215, 22, '.DR.TAHIR', '', '', '', 'ASAD ULLAH PUR', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27966, 1596, 2331, 23, 'SAJID MEDICAL STORE', '', '', '', 'VPO GOLAKI GUJRAT', '', 'Retailer', '', '', '', '0', '35775', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27967, 1597, 9102, 9, 'HASEEB MEDICAL STORE', '', '', '', 'MOHALLAH SULTAN PURA', 'BEHIND EID GAH GT ROAD GURJAT', 'Retailer', '', '', '', '5634', '0', '0', '10/4/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27968, 1598, 3100, 3, '.DR.IHSAN ULLAH', '', '', '', '', '', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27969, 1599, 9103, 9, 'DR.RABAIL', '', '', '', 'PMDC# 63222-P', 'SHADIWAL ROAD GUJRAT', 'Doctor', '', '', '', '6322', '0', '0', '12/31/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27970, 1600, 13119, 13, 'DR.ARSLAN AMJAD', '', '', '', 'PMDC# 106878-P', 'AMJAD M/HALL KOTLA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27971, 1601, 11114, 11, '.ADEEL MEDICAL STORE', '', '', '', 'GUNJAH ROAD', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27972, 1602, 5130, 5, '.DR.NARGIS', '', '', '', '', 'AL HADAM Hp GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27973, 1603, 10121, 10, 'DR.ADNAN RASOOL', '', '', '', 'GHULAM RASOOL DENTAL CLINIC', 'DEONA MANDI', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27974, 1604, 10112, 10, 'SALAMAT MEMORIAL MEDICAL STORE', '', '', '', 'MAIN BAZAR NEAR SERWAR PLAZA', 'CHIRAGHPURA LALAMUSA', 'Retailer', '', '', '', '499', '0', '0', '10/9/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27975, 1605, 3101, 3, 'DR.IMRAN ZAMAN', '', '0331-9616625', '', 'PMDC #87071-P', 'ZAMAN CLINIC KHOKHA STOP', 'Doctor', '', '', '', '87071', '0', '0', '12/31/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27976, 1606, 2335, 23, 'DR.FARIAH', '', '', '', 'AHMED MURTAZA DENTAL CLINIC', 'KHOJIANWALI PMDC#13661-D', 'Retailer', '', '', '', '13661', '0', '0', '12/31/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27977, 1607, 581, 5, '.ZEESHAN BUTT MS', '', '', '', '', 'GUJRAT', 'Retailer', '34201-8377609-7', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27978, 1608, 1876, 18, 'SHABBIR MEDICAL STORE', '', '', '', 'NEAR MB BUILDERS DINGA ROAD', 'NOONAWALI', 'Retailer', '', '', '', '16913', '0', '0', '10/25/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27979, 1609, 1877, 18, 'ABDULLAH MEDICAL STORE', '', '', '', 'HOSPITAL CHOWK', 'MALKA', 'Retailer', '3420232467033', '', '', '0', '38241', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27980, 1610, 13120, 13, 'DR.SYEDA NEELAM', '', '0300-4255359', '', 'PMDC# 37108-P', 'ZAINAB HOSPITAL KOTLA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27981, 1611, 5104, 5, 'DR.SOFIA IJAZ', '', '', '', 'BHIMBER ROAD GUJRAT', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27982, 1612, 3102, 3, 'DR.M.ARSHAD IQBAL', '', '', '', 'PMDC# 21334-P', 'Walli Hussain Mem.Hp LAKHANWAL', 'Doctor', '', '', '', '21334', '0', '0', '12/31/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27983, 1613, 1878, 18, 'DR.M.AKHTER HUSSAIN MIRZA', '', '', '', 'HASHIM WELFARE TRUST', 'PINDI HASHIM', 'Doctor', '', '889509-6', '', '45080', '0', '0', '12/31/2023 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27984, 1614, 9106, 9, 'DR.ABDUL QADEER', '', '', '', 'PMDC# 59669-P', 'MOH.KALOPURA GUJRAT', 'Doctor', '', '', '', '59669', '0', '0', '12/31/2019 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27985, 1615, 10113, 10, 'DR.KHALID AHMED', '', '', '', 'PMDC# 12990-N', 'JOORA', 'Doctor', '', '', '', '12990', '0', '0', '12/31/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27986, 1616, 10114, 10, '.FIRST MEDICAL HOSPITAL', '', '', '', 'DEWNA MANDI', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27987, 1617, 1879, 18, '.NAUMAN MEDICAL STOR', 'KHASEET PURR', '', '', '', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27988, 1618, 2134, 21, 'HAFIZ G PHARMACY', '', '', '', 'NEAR MIAN ASLAM HOSPITAL', 'MANGOWAL GHARBI', 'Retailer', '', '', '', '43672', '0', '0', '7/30/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27989, 1619, 10122, 10, 'DR.AYESHA AMJAD', '', '', '', 'PMDC# 66844-P', 'UMER BASHIR Hp LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27990, 1620, 9107, 9, 'AKRAM & SONS MEDICAL STORE', '', '', '', 'NEAR POLICE SHAHEEN CHOWKI', 'MUH.QUTABABAD SARGODHA RD GRT', 'Retailer', '', '', '', '0', '10219', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27991, 1621, 13122, 13, 'DR.NADEEM SHEHZAD', '', '', '', 'PMDC# 74091-P', 'AL-AZAM HOSPITAL KOTLA', 'Doctor', '34202-9233471-5', '', '', '74091', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27992, 1622, 2135, 21, 'AFRIDI MEDICAL STORE', '', '', '', 'VILLAGE KANG CHANNAN', 'SARGODHA ROAD GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27993, 1623, 1183, 11, 'DR.HUMAYUN RASHEED', '', '', '', 'PMDC# 5799-P', 'HUMAYUN M/C JANDANWALA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27994, 1624, 10123, 10, 'NEW RAFIQ MEDICAL STORE', '', '', '', 'NEAR NOONAN HALWAI MAIN BAZAR', 'LALAMUSA', 'Retailer', '', '', '', '2658', '0', '0', '8/26/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27995, 1625, 13123, 13, 'CHAUDHRY QAISER PHARMACY', '', '0300-6229626', '', 'JALALPUR JATTAN ROAD', 'FATEH PUR', 'Retailer', '', '', '', '45856', '0', '0', '10/22/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27996, 1626, 9108, 9, 'JS PHARMACY', '', '', '', 'MAIN SARGODHA ROAD', 'GUJRAT', 'Retailer', '34201-9272623-1', '4250763-4', '', '48706', '0', '0', '11/23/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27997, 1627, 13124, 13, 'DR.ANSAR MAHMOOD', '', '0301-6262934', '', 'ISLAMABAD MEDICAL COMPLEX', 'DOULAT NAGAR', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27998, 1628, 2136, 21, 'AHMED PHARMACY', '', '', '', '', 'Dinga Road KEERANWALA', 'Retailer', '', '', '', '4316', '0', '0', '10/26/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(27999, 1629, 13125, 13, 'DR.IRFAN WARIS', '', '', '', 'PMDC# 81692-P', 'ASSA FREE DISPENSARY SADWAL', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28000, 1630, 1880, 18, '.USMAN CLINIC', '', '', '', 'LAHREE', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28001, 1631, 9109, 9, 'FAZAIA MEDICAL STORE', '', '0331-8878502', '', 'NEAR MAKKI MASJID', 'SHADIWAL ROAD GUJRAT', 'Retailer', '', '', '', '0', '48926', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28002, 1632, 11118, 11, 'CARE PHARMACY', '', '', '', 'UTAM CHOWK DHORIA', 'KHARIAN', 'Retailer', '34202-0276542-5', '4880620-1', '', '48738', '0', '0', '11/21/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28003, 1633, 13126, 13, 'DR.MUHAMMAD QASIM BILAL', '', '', '', 'SADWAL ROAD', 'KOTLA PMDC#5003-AJK', 'Retailer', '', '', '', '0', '5003', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28004, 1634, 5105, 5, '.DR.MUSHTAQ WARRAICH', '', '', '', 'GUJRAT', '', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28005, 1635, 5106, 5, 'DR.ZAFAR IQBAL SANDHU', '', '', '', 'PMDC# 49535-P', 'NATIONAL ORTHOPADIC Hp GUJRAT', 'Retailer', '34201-5973942-1', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28006, 1636, 10124, 10, 'DR.AMNAH WARIS', '', '', '', 'PMDC# 28447-N', 'ITTEFAQ H/C DEWNA MANDI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28007, 1637, 3103, 3, 'ALAM&SONS MEDICAL STORE', '', '', '', 'MAIN BAZAR', 'TANA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28008, 1638, 3104, 3, '.SAMEER CLINIC', '', '', '', 'QILLA ISLAM', '', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28009, 1639, 11119, 11, 'DR.WAQAS ALI', '', '', '', 'PMDC# 49148-P', 'NAJAM Hp KHARIAN', 'Doctor', '34202-0856793-3', '7149352-4', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28010, 1640, 9110, 9, 'FAZAL PHARMACY', '', '0307-7224221', '', 'WAHID TRUST,NEAR PAK FAN', 'GT ROAD GUJRAT', 'Retailer', '34201-5343556-7', '3783510-6', '', '48734', '0', '0', '1/2/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28011, 1641, 13127, 13, 'DR.M.SHAFIQ', '', '', '', 'PMDC# 7440-9', 'RAZA EYE Hp KOTLA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28012, 1642, 13128, 13, 'AHMED MEDICAL STORE', '', '', '', '', 'SARAI DING GUJRAT', 'Retailer', '', '', '', '49233', '0', '0', '12/12/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28013, 1643, 3105, 3, 'MALIK MEDICAL STORE', '', '', '', 'NEAR MASJID MAIN BAZAR', 'AWAN SHARIF', 'Retailer', '', '', '', '0', '49318', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28014, 1644, 10125, 10, 'DR.MAJID HASSAN', '', '', '', 'PMDC# 65745-P', 'EJAZ Hp LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28015, 1645, 9111, 9, '.ASMA CLINIC', '', '', '', 'MADINA BAZAR SARGODHA ROAD', 'GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28016, 1646, 10126, 10, 'DR.ASHRAF MIRZA', '', '', '', '', 'LALAMUSA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28017, 1647, 3106, 3, '.DR.SHAISTA WARIS', '', '', '', 'SHOAIB CLINIC AWAN SHARIF', '', 'Retailer', '34201-5079156-9', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28018, 1648, 3107, 3, 'MUGHAL PHARMACY', '', '', '', '', 'AWAN SHARIF', 'Retailer', '', '', '', '49607', '0', '0', '1/27/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28019, 1649, 10127, 10, '.NADIA LHV', '', '', '', '', '', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28020, 1650, 1881, 18, '.AYSHA CLINIC', '', '', '', 'BAGWALL', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28021, 1651, 10128, 10, 'DR.CH.M.ALI', '', '', '', 'PMDC# 99850-P', 'DEWNA MANDI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28022, 1652, 13129, 13, 'DR.FAZEEL SHAHZAD', 'PMDC *63905-P', '', '', '', 'KOTLA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28023, 1653, 9112, 9, 'RATHORE MEDICAL STORE', '', '', '', 'MUHALLAH KALUPURA ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '6258', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28024, 1654, 9115, 9, 'AL RASHEED MEDICAL STOR', '', '', '', 'MOHALLAH NOOR PUR PADDY', 'NEAR LAL MASJID GUJRAT', 'Retailer', '', '', '', '0', '38205', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28025, 1655, 10129, 10, '.DR.ZEESHAN AHAMED', '', '', '', '', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28026, 1656, 10130, 10, 'HASSAN PHARMACY DISTRIBUTOR', '', '', '', '', 'MAIN BAZAR DEONA MANDI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '12/30/1899 12:00 AM', '9/25/2021 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28027, 1657, 5107, 5, 'DR.SAADIA IRAM', '', '', '', 'PMDC#36701-P', 'IRFAN HP MARGHAZAR COLONEY', 'Doctor', '34201-1377136-2', '34201-1377136-2', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28028, 1658, 1882, 18, '.NAWJJID CLINIC', '', '', '', 'KAMMLA', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28029, 1659, 1883, 18, '.AL-MANZOOR CLINIC', '', '', '', '', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28030, 1660, 3108, 3, '.DR.M.SHAHBAZ', '', '', '', '', 'MUNGOWAL NEAR JPJ', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28031, 1661, 3109, 3, 'DR.SAAD ARSHAD', '', '', '', 'HAIDER ALI HOSPITAL', 'TANDA', 'Doctor', '', '', '', '100437', '0', '0', '12/31/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28032, 1662, 9113, 9, 'DR.SAJJAD RASOOL CH.', '', '', '', '', 'RASOOL MEDICAL CENTER GUJRAT', 'Doctor', '', '', '', '30235', '0', '0', '12/31/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28033, 1663, 10131, 10, 'HAMZA MEDICAL STORE', '', '', '', 'EID GAH ROAD', 'LALAMUSA', 'Retailer', '34202-6155915-3', 'A312392-2', '', '0', '50818', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28034, 1664, 1144, 1, 'DR.UMAR ABBAS', '', '', '', 'PMDC#70592-P', 'Qamar Sialvi Road GUJRAT', 'Retailer', '3420159208689', '4905197-4', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28035, 1665, 11120, 11, 'DR.FAISAL ABBAS', '', '', '', 'PMDC# 55142-P', 'DINGA ROAD KHARIAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28036, 1666, 9114, 9, 'DR.M.AFZAL', '', '', '', 'PMDC# 24466-P', 'LAHORE M/C GUJRAT', 'Doctor', '', '', '', '24466', '0', '0', '12/31/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28037, 1667, 9116, 9, 'MOHSIN PHARMACY', '', '', '', 'MUHALLAH KALUPURA, BADSHAHI RD', 'GUJRAT', 'Retailer', '', '', '', '38433', '0', '0', '12/3/2020 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28038, 1668, 2216, 22, 'DR.SABTAIN HAIDER', '', '', '', 'PMDC# 102863-P', 'HAIDER CLINIC CHAKRI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28039, 1669, 3112, 3, 'AL MADINA MEDICAL STORE', '', '0345-4704286', '', 'TANDA ROAD', 'KARIANWALA', 'Retailer', '34201-4080696-9', '', '', '0', '50658', '0', '4/1/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28040, 1670, 3113, 3, 'DR.WAQAS ALI', '', '', '', 'PMDC# 8442-P', 'TANDA', 'Retailer', '34201-0374237-1', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28041, 1671, 10132, 10, 'LALAMUSA PHARMACY A Franchise of', '', '', '', 'FAZAL DIN S Pharma Plus', 'MAIN GT ROAD LALAMUSA', 'Retailer', '', '8719538-5', '', '53793', '0', '0', '7/11/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28042, 1672, 2336, 23, 'AHMED MEDICAL STORE', '', '', '', 'MAIN LARI ADDA', 'SHADIWAL', 'Retailer', '', '7588142-8', '', '51494', '0', '0', '5/4/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28043, 1673, 9117, 9, '.ZAITOON PHARMACY', '', '', '', 'GUEST HOUSE BUILDING', 'Small Industrial Area GUJRAT', 'Retailer', '', '', '', '54106', '0', '0', '7/22/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28044, 1674, 3114, 3, '.DR.ABDUL SATTAR', '', '', '', '', 'KOTLI KOHALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28045, 1675, 13130, 13, 'DR.M.JAWAD', '', '', '', 'PMDC# 58919-P', 'CITY CLINIC KOTLA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28046, 1676, 13131, 13, 'DR.M.AJMAL', '', '', '', 'PMDC# 16455-P', 'Ahmed Poly Clinic DOULATNAGAR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28047, 1677, 3115, 3, 'LIFE CARE PHARMACY', '', '', '', 'THANNA ROAD KARIANWALA', 'NEAR PTCL EXCHANGE GUJRAT', 'Retailer', '34201-5282547-3', '', '', '62585', '0', '0', '10/17/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28048, 1678, 1884, 18, '.ZUBAIR CLINIC', '', '', '', 'SEHNA', '', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28049, 1679, 3116, 3, '.DR.ZAKIA RAZA', '', '', '', 'KARIANWALA', '', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28050, 1680, 9118, 9, 'AHAD PHARMACY', '', '', '', 'NEAR AMIR ELECTRONICS', 'SARGODHA ROAD GUJRAT', 'Retailer', '34201-0302202-9', '', '', '58640', '0', '0', '8/26/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28051, 1681, 10133, 10, 'ZAINAB PHARMACY', '', '', '', 'Adjacent Tehsil Level Hospital', 'Camping Ground LALAMUSA', 'Retailer', '34202-9771837-9', '', '', '54066', '0', '0', '7/20/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28052, 1682, 5110, 5, 'DR.CH SHAFIQ AHMED', '', '', '', 'PMDC# 7080-P', 'AL-SHAAFI HOSPIATL GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28053, 1683, 9119, 9, 'AL SHAFI PHARMACY', '', '', '', 'SARGODHA ROAD', 'NEAR KHALIL HOSPITAL GUJRAT', 'Retailer', '', '', '', '56631', '0', '0', '8/26/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28054, 1684, 3117, 3, '.DR.QASIM YAQOOB', '', '', '', 'BHARAJ', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28055, 1685, 10134, 10, 'DR.AFZA AKRAM', '', '', '', 'PMDC# 71000-P', 'MADNI CLINIC LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28056, 1686, 1160, 11, 'HAIDER MEDICAL STORE', '', '', '', 'OPP. RAWAYAT RESTAURANT', 'NEAR MARALA MORE JHANDAWALA', 'Retailer', '', '', '', '0', '58672', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28057, 1687, 10135, 10, 'ALI MEDICAL STORE', '', '', '', 'NEAR LALA RAFIQ MOHALLAH', 'RAFIQABAD GT ROAD LALAMUSA', 'Retailer', '', '', '', '0', '54065', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28058, 1688, 1888, 18, '.AMINA MEDICAL CENTER', '', '', '', 'BHADDAR', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28059, 1689, 1185, 11, 'JAN SHER CHEMIST PHARMACY', '', '', '', 'MURALA PLAZA DINGA ROAD', 'KHARIAN', 'Retailer', '34202-0762247-3', '1412393-2', '', '58936', '0', '0', '8/22/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28060, 1690, 10136, 10, 'DR.SYEDA FATIMA TU ZAHIRA', '', '', '', 'PMDC# 99657-P', 'TAHIR Surgical Hp LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28061, 1691, 11124, 11, 'DR.TAHIR IQBAL MIRZA', '', '', '', 'PMDC# 23774-P', 'SUBHAN Hp KHARIAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28062, 1692, 11125, 11, '.DR.HAFZA ANWAR', '', '', '', 'KHARIAN', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28063, 1693, 10137, 10, 'DR.BILAL GHAZANFAR', '', '', '', 'PMDC# 99810-P', 'PASWAL LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28064, 1694, 10138, 10, 'ISMAIL MEDICAL STORE', '', '', '', 'NEAR SHAUKAT KARYANA STORE', '', 'Retailer', '', '', '', '0', '54155', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28065, 1695, 986, 9, 'AL NOOR MEDICAL STORE', '', '0344-6488459', '', 'Opp.Sain Raja Phatac,', 'GT Road GUJRAT', 'Retailer', '', '', '', '0', '51862', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28066, 1696, 3118, 3, 'DR.UMAIR WAHEED', '', '', '', 'PMDC# 100109-P', 'NAZIR AHMED WELFARE TRUST', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28067, 1697, 3119, 3, '.DR.M.AFZAL', '', '', '', '', 'SAGGAR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28068, 1698, 13132, 13, 'DR.M.BILAL', '', '', '', 'PMDC# 79000-P', 'ANAYAT BASHIR CLINIC KOTLA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28069, 1699, 987, 9, 'ZAID MEDICAL STORE', '', '', '', 'MOHALLAH FAIZABAD NEAR HASSAN', 'GENERAL STORE GUJRAT', 'Retailer', '', '3351420-8', '', '0', '6640', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28070, 1700, 1889, 18, 'AMIR JAVED PHARMACY', '', '', '', 'VILAGE BHADAR TAHSIL KHARIAN', '', 'Retailer', '34202-4088857-5', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28071, 1701, 1890, 18, '.AL SYED MEDICAL STORE', '', '', '', 'PANJAN KASNA', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28072, 1702, 988, 9, 'DR.AAMARA SHARIQ', '', '', '', 'PMDC#87604-P', 'SADDIQUE M/C GUJRAT', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28073, 1703, 10139, 10, 'DR.NABEEL MASOOD', 'PMDC 99701-P', '', '', 'ALI HEALTH CENTER SEDHRI', 'LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28074, 1704, 989, 9, 'DR.NASRULLAH ALTAF CH', '', '', '', 'LIFE CARE MEDICAL CENTER', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28075, 1705, 13133, 13, 'DR.FAREEDA YASMIN', '', '', '', 'PMDC# 39225-P', 'NOOR CLINIC KOTLA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28076, 1706, 3120, 3, 'BILAL MEDICAL STORE', '', '', '', 'ADDA MARI WARRAICHAN', '', 'Retailer', '', '', '', '0', '63680', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28077, 1707, 9155, 9, 'DR.UMER HUSSAIN', 'WAHID TRUST HOSPITAL', '', '', 'PMDC 76898-N', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28078, 1708, 13134, 13, 'QAISER MEDICAL STORE', '', '', '', 'AWAN SHRIF ROAD,MALIK CHOWK', 'GUJRAT', 'Retailer', '', '', '', '0', '62880', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28079, 1709, 11126, 11, '.DR.MUHAMMAD AFZAL', '', '', '', 'JANDANWALA', 'TAJAMAL MEDICAL CENTER', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28080, 1710, 9157, 9, 'ASIF PHARMACY', '', '', '', 'KALUPURA BAZAR NEAR ALLAMA', 'PLAZA GUJRAT', 'Retailer', '', '', '', '64068', '0', '0', '12/3/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28081, 1711, 11127, 11, 'ALI PHARMACY', '', '', '', 'GULYANA MORR', 'VILLAGE THEKRIYAN', 'Retailer', '', '', '', '0', '0', '0', '11/28/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28082, 1712, 10142, 10, 'HASNAT MEDICAL STORE', '', '', '', 'JOURA KAMANA OPP.HUZAIMA SWEET', 'BUS STOP JOURA', 'Retailer', '', '', '', '0', '48542', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28083, 1713, 3121, 3, 'YOUSAF MEDICAL STORE', '', '', '', 'MAIN BAZAR TANDA NEAR P.O.', 'TANDA', 'Retailer', '', '', '', '0', '63957', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28084, 1714, 2337, 23, '.SAJID MS', '', '', '', 'GOLAYKI', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28085, 1715, 10143, 10, 'DR.CH.M.SHAHZAD', '', '', '', 'PMDC# 51216-P', 'BASHIR CHILD Hp LALAMUSA', 'Doctor', '', '7493398-7', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28086, 1716, 11128, 11, 'TAJAMAL MEDICAL CENTRE', '', '', '', 'KHARIAN', '', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28087, 1717, 10144, 10, 'OXYGEN PHARMACY', '', '', '', 'CITY MALL,G.T ROAD', 'LALAMUSA', 'Retailer', '', '', '', '64330', '0', '0', '12/16/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28088, 1718, 1891, 18, '.SUHAIL MS', '', '', '', '', 'CHANAN', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28089, 1719, 3122, 3, 'DR.NAUMAN ALI', '', '', '', 'PMDC# 96666-P', 'RAJA SABIR CLINC SAGGAR', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28090, 1720, 3124, 3, 'DR.HUSSAIN HUMAYON', '', '', '', 'PMDC# 74381-P', 'MUSA Hp KARIANWALA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28091, 1721, 13135, 13, 'NEW HAFIZ BROTHER PHARMACY', '', '', '', 'VILLAGE PYARA FATAH PUR RD', 'GUJRAT', 'Retailer', '', '', '', '45759', '0', '0', '10/5/2021 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28092, 1722, 13137, 13, 'AL SYED PHARMACY', '', '', '', 'PURANA KOTLA ARAB ALI KHAN', 'ROAD KOTLA', 'Retailer', '3420250335707', '', '', '64996', '0', '0', '1/2/2023 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28093, 1723, 9158, 9, 'DR.EJAZ BASHIR', '', '', '', 'PMDC# 19674-P', 'CLEFT Hp GUJRAT', 'Retailer', '34201-3976712-3', '7358793-6', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28094, 1724, 1892, 18, 'ABDULLAH MEDICAL STORE', '', '', '', '', 'BHADDAR CHOWK', 'Retailer', '3420249097723', '', '', '0', '64512', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28095, 1725, 1893, 18, '.MANZOOR CLINIC', '', '', '', 'SUMALLA', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28096, 1726, 13139, 13, 'HAMZA MEDICAL STORE', 'VILLAGE LANGARIL NEA', '', '', 'R UBL BANK', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28097, 1727, 2217, 22, '.LHV SADIYA', '', '', '', 'JOKALIYAN', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28098, 1728, 2218, 22, '.LHV UZMA', '', '', '', 'KOT ALLAH BAKHSH', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28099, 1729, 10145, 10, 'DR.SHAKILA AKHTAR', '', '', '', 'PMDC# 284-N', 'BANO MEM.CLINIC LALAMUSA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28100, 1730, 958, 9, 'DANISH HEALTHCARE PHARMACY(A FRANCHISE', '', '', '', 'OF FAZAL DINS PHARMA PLUS)', 'PAGGAN WALA PLAZA GUJRAT', 'Retailer', '34201-6499258-5', 'A340614', '', '65695', '0', '0', '2/12/2023 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28101, 1731, 10146, 10, 'REHMAT PHARMACY', '', '', '', 'MAIN BAZAR', 'DEONA MANDI', 'Retailer', '', '', '', '65210', '0', '0', '1/26/2023 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28102, 1732, 528, 5, 'DR.ALI HANIF', '', '', '', 'PMDC# 94742-P', 'Family Health Clinic GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28103, 1733, 3125, 3, 'DR.AFZAAL HUSSAIN', '', '0346-3661987', '', 'PMDC# 78102-P', 'AL-SUGRAH Hp TANDA', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28104, 1734, 1894, 18, '.AL SHIFA HEALTH CENTER', 'GULYNA', '', '', '', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28105, 1735, 1895, 18, '.ISLAM WELFARE TRUST', 'DHONNI', '', '', '', '', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28106, 1736, 10147, 10, 'DR.IRFAN ARSHAD RANA', '', '', '', 'GT ROAD LALAMUSA PMDC#47290-P', 'NATIONAL NEURO MEDICAL CENTER', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28107, 1737, 13141, 13, 'M.ARSAL PHARMACY', '', '', '', '', 'FATAH PUR CHOWK', 'Retailer', '34201-2451453-3', '', '', '66728', '0', '0', '3/4/2023 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28108, 1738, 9159, 9, 'KHALID MEDICAL STORE', '', '', '', 'RAILWAY ROAD', 'GUJRAT', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28109, 1739, 1896, 18, 'DR.KHALID REHMAN', '', '', '', 'PMDC# 17143', 'JANDANWALA', 'Retailer', '', '1057392-6', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28110, 1740, 1917, 19, 'NEW JAN SHER PHARMACY AND MEDICAL STORE', '', '', '', 'CB PLAZA SADDAR BAZAR', 'KHARIAN CANTT', 'Retailer', '34202-0779831-9', '4127856-9', '', '71909', '0', '0', '4/10/2023 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28111, 1741, 16, 1, 'AL-MADINA PHARMACY', '', '', '', 'DHAKI CHOWK', 'GUJRAT', 'Retailer', '34101-2773558-7', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28112, 1742, 9121, 9, 'DR.HASSAN ZIA', '', '', '', 'HASSAN HEALTH CARE', 'GULZAR MADINA ROAD GUJRAT', 'Retailer', '34201-6826814-7', '4110731-4', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28113, 1743, 944, 9, 'DR.M. IHSAN ULLAH', '', '', '', 'SARGODHA ROAD', 'RAUF HOSPITAL', 'Doctor', '', '', '', '104774', '0', '0', '6/10/2022 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28114, 1744, 1918, 19, 'AL FAZAL PHARMACY & COSMETICS', '', '', '', 'SUF SHIVKAN SHOPPING PLAZA', 'KHARIAN CANTT', 'Retailer', '', '', '', '71930', '0', '0', '4/10/2023 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28115, 1745, 9161, 9, 'DR.SYED WAQAS HAIDER', '', '', '', 'PMDC# 57587-P', 'SABAR H/C KHATALA', 'Retailer', '', '3935827-5', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28116, 1746, 1162, 1, 'DR.QASIM SALMAN', 'AYSHA HOSPITAL JINAH', '', '', 'ROAD GUJRAT', 'PMDC 37679-P', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28117, 1747, 2340, 23, 'NEW MAJID PHARMACY', '', '', '', 'HOSPITAL CHOWK', 'SHADIWAL', 'Retailer', '3420127704435', '2926669-6', '', '72876', '0', '0', '7/7/2023 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28118, 1748, 14, 1, 'DR.ALI RAUF', '', '', '', 'TAIMOOR CHOWK  PMDC#79879-P', 'GUJRAT KIDNEY CENTER', 'Retailer', '34201-4078295-5', '5572760-5', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28119, 1749, 13142, 13, 'DR.KAINAT ASHRAF', '', '', '', 'PMDC# 5673-AJK', 'NATIONAL H/C KOTLA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28120, 1750, 946, 9, 'FAMILY CARE PHARMACY', '', '', '', 'NEAR SHELL PETROL PUMP', 'SHADIWAL ROAD GUJRAT', 'Retailer', '', '', '', '78017', '0', '0', '7/24/2023 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28121, 1751, 1312, 13, 'ZAIN PHARMACY', '', '', '', 'NEAR AMIR BAKERY MARJAN CHOWK', 'KHARANA', 'Retailer', '34201-1232726-1', '', '', '77775', '0', '0', '7/14/2023 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28122, 1752, 2124, 21, 'ESSA MEDICAL STORE', '', '', '', 'OPP. POLICE CHOKI', 'MANGOWAL GHARBI', 'Retailer', '34201-4716171-7', 'A094284-0', '', '0', '77948', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28123, 1753, 2341, 23, 'ATTA & SONS PHARMACY', '', '', '', 'NEAR POLICE CHOKI', 'SHADIWAL', 'Retailer', '3420180162415', '', '', '78000', '0', '0', '7/24/2023 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28124, 1754, 3126, 3, 'DR.UMAR SOBAN', '', '', '', 'PMDC# 92824-P', 'MUMTAZ CLINIC KARIANWALA', 'Retailer', '3420164459173', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28125, 1755, 1897, 18, 'DR.AFSHAN IRAM', '', '', '', 'PMDC# 58649-P', 'HASSAN CLINIC GULANA', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28126, 1756, 10148, 10, 'DR.HASNAIN ASHRAF', '', '', '', 'PMDC# 86026-P', 'DEWNA MANDI', 'Retailer', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28127, 1757, 13143, 13, 'DR.BASIT ALI', '', '', '', 'SHAAFI HOSPITAL', 'PMDC#1067763-P', 'Doctor', '', '', '', '0', '0', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(28128, 1758, 2342, 23, 'HASEEB MEDICAL STORE', '', '', '', 'VILLAGE LANGAY', 'TEH. & DISTT. GUJRAT', 'Retailer', '', '', '', '0', '40685', '', '12/30/1899 12:00 AM', '', '12/30/1899 12:00 AM', 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1);

-- --------------------------------------------------------

--
-- Table structure for table `dealer_overall_record`
--

CREATE TABLE `dealer_overall_record` (
  `dealer_overall_id` int(10) UNSIGNED NOT NULL,
  `software_dealer_overall_id` int(11) UNSIGNED DEFAULT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `orders_given` int(10) UNSIGNED NOT NULL,
  `successful_orders` int(10) UNSIGNED NOT NULL,
  `ordered_packets` int(10) UNSIGNED NOT NULL,
  `submitted_packets` int(10) UNSIGNED NOT NULL,
  `ordered_boxes` int(10) UNSIGNED NOT NULL,
  `submitted_boxes` int(10) UNSIGNED NOT NULL,
  `order_price` float NOT NULL,
  `discount_price` float NOT NULL,
  `invoiced_price` float NOT NULL,
  `cash_collected` float NOT NULL,
  `return_packets` int(10) NOT NULL,
  `return_boxes` int(10) NOT NULL,
  `return_price` float NOT NULL,
  `cash_return` float NOT NULL,
  `waived_off_price` float NOT NULL,
  `pending_payments` float NOT NULL,
  `software_lic_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dealer_payments`
--

CREATE TABLE `dealer_payments` (
  `payment_id` int(10) UNSIGNED NOT NULL,
  `software_payment_id` int(11) UNSIGNED DEFAULT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `amount` float NOT NULL,
  `cash_type` varchar(10) NOT NULL,
  `pending_payments` float NOT NULL,
  `date` varchar(30) NOT NULL,
  `day` varchar(15) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `comments` varchar(200) DEFAULT NULL,
  `status` varchar(30) NOT NULL,
  `send_status_computer` varchar(20) DEFAULT NULL,
  `software_lic_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dealer_payments`
--

INSERT INTO `dealer_payments` (`payment_id`, `software_payment_id`, `dealer_id`, `order_id`, `amount`, `cash_type`, `pending_payments`, `date`, `day`, `user_id`, `comments`, `status`, `send_status_computer`, `software_lic_id`) VALUES
(1, 1, 1, 0, 1000, 'Collection', -1000, '29/Jan/2022', 'Saturday', 1, '', 'Cash', NULL, 1),
(2, 2, 2, 0, 500, 'Collection', -500, '29/Jan/2022', 'Saturday', 1, '', 'Cash', NULL, 1),
(3, 3, 4, 0, 2000, 'Collection', -2000, '29/Jan/2022', 'Saturday', 1, '', 'Cash', NULL, 1),
(4, 4, 5, 0, 3000, 'Return', 3000, '29/Jan/2022', 'Saturday', 1, '', 'Cash', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `dealer_types`
--

CREATE TABLE `dealer_types` (
  `dealer_type_ID` int(11) NOT NULL,
  `software_dealer_type_id` int(10) UNSIGNED NOT NULL,
  `typeID` int(11) NOT NULL,
  `dealerType_name` varchar(100) NOT NULL,
  `typeStatus` varchar(50) NOT NULL,
  `DeleteStatus` int(11) NOT NULL,
  `software_lic_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `discount_policy`
--

CREATE TABLE `discount_policy` (
  `disc_policyID` int(11) NOT NULL,
  `software_discount_id` int(10) UNSIGNED NOT NULL,
  `approval_id` int(11) NOT NULL,
  `dealer_table_id` int(11) DEFAULT NULL,
  `company_table_id` int(11) UNSIGNED DEFAULT NULL,
  `product_table_id` int(11) UNSIGNED DEFAULT NULL,
  `sale_amount` float NOT NULL,
  `discount_percent` float NOT NULL,
  `start_date` varchar(50) NOT NULL,
  `end_date` varchar(50) NOT NULL,
  `policy_status` varchar(50) NOT NULL,
  `entered_by` int(11) NOT NULL,
  `entry_date` varchar(50) NOT NULL,
  `entry_time` varchar(50) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `update_date` varchar(50) NOT NULL,
  `update_time` varchar(50) NOT NULL,
  `software_lic_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `district_info`
--

CREATE TABLE `district_info` (
  `district_table_id` int(10) UNSIGNED NOT NULL,
  `software_district_id` int(10) UNSIGNED NOT NULL,
  `district_id` int(10) UNSIGNED DEFAULT NULL,
  `district_name` varchar(100) NOT NULL,
  `district_status` varchar(20) NOT NULL,
  `creating_user_id` int(10) UNSIGNED NOT NULL,
  `creating_date` varchar(20) NOT NULL,
  `creating_time` varchar(20) NOT NULL,
  `update_user_id` int(10) UNSIGNED NOT NULL,
  `update_date` varchar(20) NOT NULL,
  `update_time` varchar(20) NOT NULL,
  `software_lic_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `district_info`
--

INSERT INTO `district_info` (`district_table_id`, `software_district_id`, `district_id`, `district_name`, `district_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`, `software_lic_id`) VALUES
(1, 1, 0, 'Gujrat', 'Active', 1, '29/Jan/2022', '01:53 AM', 1, '29/Jan/2022', '01:53 AM', 1),
(10, 2, 2, 'Gujranwala', 'Active', 1, '16/Feb/2022', '10:42 AM', 1, '16/Feb/2022', '10:42 AM', 1);

-- --------------------------------------------------------

--
-- Table structure for table `groups_info`
--

CREATE TABLE `groups_info` (
  `group_table_id` int(10) UNSIGNED NOT NULL,
  `software_group_id` int(10) UNSIGNED NOT NULL,
  `group_id` int(10) UNSIGNED DEFAULT NULL,
  `company_id` int(10) UNSIGNED NOT NULL,
  `group_name` varchar(200) NOT NULL,
  `group_status` varchar(20) NOT NULL,
  `creating_user_id` int(10) UNSIGNED NOT NULL,
  `creating_date` varchar(20) NOT NULL,
  `creating_time` varchar(20) NOT NULL,
  `update_user_id` int(10) UNSIGNED NOT NULL,
  `update_date` varchar(20) NOT NULL,
  `update_time` varchar(20) NOT NULL,
  `software_lic_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mobile_curr_locations`
--

CREATE TABLE `mobile_curr_locations` (
  `curr_loc_id` int(10) UNSIGNED NOT NULL,
  `software_user_id` int(10) UNSIGNED NOT NULL,
  `latitude` varchar(25) NOT NULL,
  `longitude` varchar(25) NOT NULL,
  `location_name` varchar(150) NOT NULL,
  `update_date` varchar(20) DEFAULT NULL,
  `update_time` varchar(20) DEFAULT NULL,
  `software_lic_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mobile_curr_locations`
--

INSERT INTO `mobile_curr_locations` (`curr_loc_id`, `software_user_id`, `latitude`, `longitude`, `location_name`, `update_date`, `update_time`, `software_lic_id`) VALUES
(1, 1, '32.587898333333335', '74.14539833333333', 'null, Gujrat', '16/Apr/2022', '12:15 AM', 1),
(2, 16, '32.551740', '74.072403', 'Shadiwal Road, Gujrat', '20/Mar/2021', '05:00 PM', 1),
(78, 18, '31.561650289222598', '74.30024592205882', 'Rajgarh, Lahore', '10/Apr/2022', '09:52 pm', 1);

-- --------------------------------------------------------

--
-- Table structure for table `mobile_gps_location`
--

CREATE TABLE `mobile_gps_location` (
  `mob_loc_id` int(10) UNSIGNED NOT NULL,
  `software_user_id` int(10) UNSIGNED NOT NULL,
  `latitude` varchar(20) NOT NULL,
  `longitude` varchar(20) NOT NULL,
  `loc_name` varchar(150) NOT NULL,
  `date` varchar(30) NOT NULL,
  `time` varchar(30) NOT NULL,
  `send_status_computer` varchar(20) DEFAULT 'Pending',
  `software_lic_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mobile_gps_location`
--

INSERT INTO `mobile_gps_location` (`mob_loc_id`, `software_user_id`, `latitude`, `longitude`, `loc_name`, `date`, `time`, `send_status_computer`, `software_lic_id`) VALUES
(1, 1, '32.551307', '74.071403', 'Shadiwal Road, Gujrat', '20/Mar/2021', '4:05 PM', NULL, 1),
(6, 18, '31.560440000000003', '74.299785', 'Rajgarh, Lahore', '02/Mar/2022', '01:31 AM', 'Sent', 1);

-- --------------------------------------------------------

--
-- Table structure for table `order_gps_location`
--

CREATE TABLE `order_gps_location` (
  `order_loc_id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED DEFAULT 0,
  `user_id` int(10) UNSIGNED NOT NULL,
  `latitude` varchar(20) NOT NULL,
  `longitude` varchar(20) NOT NULL,
  `loc_name` varchar(150) NOT NULL,
  `date` varchar(30) NOT NULL,
  `time` varchar(30) NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `price` float NOT NULL,
  `type` varchar(20) NOT NULL,
  `send_status_computer` varchar(20) DEFAULT 'Pending',
  `software_lic_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_info`
--

CREATE TABLE `order_info` (
  `order_id` int(11) NOT NULL,
  `software_order_id` int(11) UNSIGNED DEFAULT NULL,
  `dealer_id` int(11) DEFAULT NULL,
  `product_id` varchar(500) DEFAULT NULL,
  `quantity` varchar(500) DEFAULT NULL,
  `unit` varchar(500) NOT NULL,
  `order_price` float NOT NULL,
  `bonus` varchar(500) NOT NULL,
  `discount` float NOT NULL,
  `waive_off_price` float NOT NULL,
  `final_price` varchar(11) NOT NULL,
  `order_success` tinyint(1) NOT NULL,
  `booking_latitude` varchar(25) NOT NULL,
  `booking_longitude` varchar(25) NOT NULL,
  `booking_area` varchar(300) NOT NULL,
  `booking_date` varchar(50) NOT NULL,
  `booking_time` varchar(50) NOT NULL,
  `booking_user_id` int(10) UNSIGNED DEFAULT NULL,
  `delivered_latitude` varchar(25) DEFAULT NULL,
  `delivered_longitude` varchar(25) DEFAULT NULL,
  `delivered_area` varchar(300) DEFAULT NULL,
  `delivered_date` varchar(20) DEFAULT NULL,
  `delivered_time` varchar(20) DEFAULT NULL,
  `delivered_user_id` int(10) UNSIGNED DEFAULT NULL,
  `update_dates` varchar(15) DEFAULT NULL,
  `update_times` varchar(15) DEFAULT NULL,
  `update_user_id` int(10) DEFAULT NULL,
  `comments` varchar(200) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `send_status_computer` varchar(20) NOT NULL,
  `software_lic_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_info`
--

INSERT INTO `order_info` (`order_id`, `software_order_id`, `dealer_id`, `product_id`, `quantity`, `unit`, `order_price`, `bonus`, `discount`, `waive_off_price`, `final_price`, `order_success`, `booking_latitude`, `booking_longitude`, `booking_area`, `booking_date`, `booking_time`, `booking_user_id`, `delivered_latitude`, `delivered_longitude`, `delivered_area`, `delivered_date`, `delivered_time`, `delivered_user_id`, `update_dates`, `update_times`, `update_user_id`, `comments`, `status`, `send_status_computer`, `software_lic_id`) VALUES
(1, 1, 9, '2_-_4_-_6_-_8_-_9', '1_-_1_-_1_-_1_-_1', 'Packets_-_Packets_-_Packets_-_Packets_-_Packets', 1550.28, '0_-_0_-_0_-_0_-_0', 0, 0, '1550.28', 1, '32.55265927', '74.07456351', 'Unnamed Road, Green Town, Gujrat, Punjab, Pakistan', '15/Mar/2022', '11:33 AM', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Pending', 'Sent', 1),
(4, 3, 10, '17_-_28_-_14_-_52_-_57', '1_-_1_-_1_-_1_-_1', 'Packets_-_Packets_-_Packets_-_Packets_-_Packets', 1321.16, '0_-_0_-_0_-_0_-_0', 0, 0, '1321.16', 1, '32.55263527', '74.07454467', 'Unnamed Road, Green Town, Gujrat, Punjab, Pakistan', '15/Mar/2022', '11:37 AM', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Pending', 'Sent', 1),
(5, 11, 1, '2_-_4_-_5_-_6', '1_-_1_-_1_-_1', 'Packets_-_Packets_-_Packets_-_Packets', 693.35, '0_-_0_-_0_-_0', 0, 0, '693.35', 1, '32.55264228', '74.07449615', 'Unnamed Road, Green Town, Gujrat, Punjab, Pakistan', '16/Mar/2022', '12:05 PM', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Pending', 'Sent', 1);

-- --------------------------------------------------------

--
-- Table structure for table `order_info_detailed`
--

CREATE TABLE `order_info_detailed` (
  `detail_id` int(11) NOT NULL,
  `software_detail_id` int(11) UNSIGNED DEFAULT NULL,
  `software_order_id` int(11) UNSIGNED DEFAULT NULL,
  `order_id` int(8) UNSIGNED NOT NULL,
  `product_table_id` int(8) UNSIGNED NOT NULL,
  `batch_id` int(10) UNSIGNED NOT NULL,
  `quantity` int(8) NOT NULL,
  `submission_quantity` int(8) NOT NULL,
  `unit` varchar(30) NOT NULL,
  `submission_unit` varchar(30) NOT NULL,
  `order_price` float NOT NULL,
  `bonus_quant` int(11) NOT NULL,
  `discount` float NOT NULL,
  `final_price` float NOT NULL,
  `cmplt_returned_bit` tinyint(1) NOT NULL,
  `returned_quant` int(11) NOT NULL,
  `returned_bonus_quant` int(11) NOT NULL,
  `software_lic_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_info_detailed`
--

INSERT INTO `order_info_detailed` (`detail_id`, `software_detail_id`, `software_order_id`, `order_id`, `product_table_id`, `batch_id`, `quantity`, `submission_quantity`, `unit`, `submission_unit`, `order_price`, `bonus_quant`, `discount`, `final_price`, `cmplt_returned_bit`, `returned_quant`, `returned_bonus_quant`, `software_lic_id`) VALUES
(1, NULL, NULL, 1, 2, 0, 1, 1, 'Packets', 'Packets', 238, 0, 0, 238, 0, 0, 0, 1),
(2, NULL, NULL, 1, 4, 1, 1, 1, 'Packets', 'Packets', 105.28, 0, 0, 105.28, 0, 0, 0, 1),
(3, NULL, NULL, 1, 6, 0, 1, 1, 'Packets', 'Packets', 170, 0, 0, 170, 0, 0, 0, 1),
(4, NULL, NULL, 1, 8, 0, 1, 1, 'Packets', 'Packets', 561, 0, 0, 561, 0, 0, 0, 1),
(5, NULL, NULL, 1, 9, 0, 1, 1, 'Packets', 'Packets', 476, 0, 0, 476, 0, 0, 0, 1),
(11, NULL, NULL, 3, 17, 0, 1, 1, 'Packets', 'Packets', 185.84, 0, 0, 185.84, 0, 0, 0, 1),
(12, NULL, NULL, 3, 28, 0, 1, 1, 'Packets', 'Packets', 211.12, 0, 0, 211.12, 0, 0, 0, 1),
(13, NULL, NULL, 3, 14, 16, 1, 1, 'Packets', 'Packets', 105.28, 0, 0, 105.28, 0, 0, 0, 1),
(14, NULL, NULL, 3, 52, 0, 1, 1, 'Packets', 'Packets', 235.45, 0, 0, 235.45, 0, 0, 0, 1),
(15, NULL, NULL, 3, 57, 0, 1, 1, 'Packets', 'Packets', 360.75, 0, 0, 360.75, 0, 0, 0, 1),
(16, NULL, NULL, 3, 17, 0, 1, 1, 'Packets', 'Packets', 185.84, 0, 0, 185.84, 0, 0, 0, 1),
(17, NULL, NULL, 3, 28, 0, 1, 1, 'Packets', 'Packets', 211.12, 0, 0, 211.12, 0, 0, 0, 1),
(18, NULL, NULL, 3, 14, 16, 1, 1, 'Packets', 'Packets', 105.28, 0, 0, 105.28, 0, 0, 0, 1),
(19, NULL, NULL, 3, 52, 0, 1, 1, 'Packets', 'Packets', 235.45, 0, 0, 235.45, 0, 0, 0, 1),
(20, NULL, NULL, 3, 57, 0, 1, 1, 'Packets', 'Packets', 360.75, 0, 0, 360.75, 0, 0, 0, 1),
(21, NULL, NULL, 5, 2, 0, 1, 1, 'Packets', 'Packets', 238, 0, 0, 238, 0, 0, 0, 1),
(22, NULL, NULL, 5, 4, 1, 1, 1, 'Packets', 'Packets', 105.28, 0, 0, 105.28, 0, 0, 0, 1),
(23, NULL, NULL, 5, 5, 3, 1, 1, 'Packets', 'Packets', 180.07, 0, 0, 180.07, 0, 0, 0, 1),
(24, NULL, NULL, 5, 6, 0, 1, 1, 'Packets', 'Packets', 170, 0, 0, 170, 0, 0, 0, 1),
(25, 1, 1, 1, 2, 0, 1, 1, 'Packets', 'Packets', 238, 0, 0, 238, 0, 0, 0, 1),
(26, 2, 1, 1, 4, 1, 1, 1, 'Packets', 'Packets', 105.28, 0, 0, 105.28, 0, 0, 0, 1),
(27, 3, 1, 1, 6, 0, 1, 1, 'Packets', 'Packets', 170, 0, 0, 170, 0, 0, 0, 1),
(28, 4, 1, 1, 8, 0, 1, 1, 'Packets', 'Packets', 561, 0, 0, 561, 0, 0, 0, 1),
(29, 5, 1, 1, 9, 0, 1, 1, 'Packets', 'Packets', 476, 0, 0, 476, 0, 0, 0, 1),
(30, 6, 1, 1, 2, 0, 1, 1, 'Packets', 'Packets', 238, 0, 0, 238, 0, 0, 0, 1),
(31, 7, 1, 1, 4, 1, 1, 1, 'Packets', 'Packets', 105.28, 0, 0, 105.28, 0, 0, 0, 1),
(32, 8, 1, 1, 6, 0, 1, 1, 'Packets', 'Packets', 170, 0, 0, 170, 0, 0, 0, 1),
(33, 9, 1, 1, 8, 0, 1, 1, 'Packets', 'Packets', 561, 0, 0, 561, 0, 0, 0, 1),
(34, 10, 1, 1, 9, 0, 1, 1, 'Packets', 'Packets', 476, 0, 0, 476, 0, 0, 0, 1),
(35, 11, 3, 3, 17, 0, 1, 1, 'Packets', 'Packets', 185.84, 0, 0, 185.84, 0, 0, 0, 1),
(36, 12, 3, 3, 28, 0, 1, 1, 'Packets', 'Packets', 211.12, 0, 0, 211.12, 0, 0, 0, 1),
(37, 13, 3, 3, 14, 16, 1, 1, 'Packets', 'Packets', 105.28, 0, 0, 105.28, 0, 0, 0, 1),
(38, 14, 3, 3, 52, 0, 1, 1, 'Packets', 'Packets', 235.45, 0, 0, 235.45, 0, 0, 0, 1),
(39, 15, 3, 3, 57, 0, 1, 1, 'Packets', 'Packets', 360.75, 0, 0, 360.75, 0, 0, 0, 1),
(40, 16, 11, 11, 2, 0, 1, 1, 'Packets', 'Packets', 238, 0, 0, 238, 0, 0, 0, 1),
(41, 17, 11, 11, 4, 1, 1, 1, 'Packets', 'Packets', 105.28, 0, 0, 105.28, 0, 0, 0, 1),
(42, 18, 11, 11, 5, 3, 1, 1, 'Packets', 'Packets', 180.07, 0, 0, 180.07, 0, 0, 0, 1),
(43, 19, 11, 11, 6, 0, 1, 1, 'Packets', 'Packets', 170, 0, 0, 170, 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `order_return`
--

CREATE TABLE `order_return` (
  `return_id` int(10) UNSIGNED NOT NULL,
  `software_return_id` int(11) UNSIGNED DEFAULT NULL,
  `dealer_id` int(11) NOT NULL,
  `return_total_price` varchar(10) NOT NULL,
  `return_gross_price` varchar(10) NOT NULL,
  `order_id` int(11) NOT NULL,
  `entry_date` varchar(20) NOT NULL,
  `entry_time` varchar(20) NOT NULL,
  `enter_user_id` int(10) DEFAULT NULL,
  `update_date` varchar(20) DEFAULT NULL,
  `update_time` varchar(20) DEFAULT NULL,
  `update_user_id` int(11) DEFAULT NULL,
  `comments` varchar(200) DEFAULT NULL,
  `status` varchar(20) NOT NULL,
  `send_status_computer` varchar(20) DEFAULT NULL,
  `software_lic_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_return_detail`
--

CREATE TABLE `order_return_detail` (
  `return_detail_id` int(10) UNSIGNED NOT NULL,
  `software_return_detail_id` int(10) UNSIGNED DEFAULT NULL,
  `return_id` int(11) NOT NULL,
  `prod_id` int(11) NOT NULL,
  `prod_batch` varchar(15) NOT NULL,
  `prod_quant` int(11) NOT NULL,
  `bonus_quant` int(11) NOT NULL,
  `discount_amount` float NOT NULL,
  `total_amount` float NOT NULL,
  `unit` varchar(20) NOT NULL,
  `return_reason` varchar(100) NOT NULL,
  `software_lic_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_status_info`
--

CREATE TABLE `order_status_info` (
  `order_status_id` int(10) UNSIGNED NOT NULL,
  `software_order_id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `booking_user_id` int(10) UNSIGNED NOT NULL,
  `booking_date` varchar(20) NOT NULL,
  `booking_latitude` varchar(25) NOT NULL,
  `booking_longitude` varchar(25) NOT NULL,
  `booking_area` varchar(200) NOT NULL,
  `delivered_user_id` int(10) UNSIGNED DEFAULT NULL,
  `delivered_date` varchar(20) DEFAULT NULL,
  `delivered_latitude` varchar(25) DEFAULT NULL,
  `delivered_longitude` varchar(25) DEFAULT NULL,
  `delivered_area` varchar(200) DEFAULT NULL,
  `software_lic_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `products_stock`
--

CREATE TABLE `products_stock` (
  `ProdStockID` int(11) NOT NULL,
  `software_product_stock_id` int(10) UNSIGNED NOT NULL,
  `product_table_id` int(10) NOT NULL,
  `in_stock` int(10) NOT NULL,
  `min_stock` int(10) NOT NULL,
  `max_stock` int(10) NOT NULL,
  `bonus_quant` int(11) NOT NULL,
  `status` varchar(30) NOT NULL,
  `software_lic_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products_stock`
--

INSERT INTO `products_stock` (`ProdStockID`, `software_product_stock_id`, `product_table_id`, `in_stock`, `min_stock`, `max_stock`, `bonus_quant`, `status`, `software_lic_id`) VALUES
(64, 1, 1, 0, 0, 0, 0, 'Active', 1),
(65, 2, 2, 0, 0, 0, 0, 'Active', 1),
(66, 3, 3, 0, 0, 0, 0, 'Active', 1),
(67, 4, 14, 498, 0, 0, 0, 'Active', 1),
(68, 5, 15, 700, 0, 0, 0, 'Active', 1),
(69, 6, 151, 2000, 0, 0, 0, 'Active', 1),
(70, 7, 152, 600, 0, 0, 0, 'Active', 1),
(71, 8, 109, 1800, 0, 0, 0, 'Active', 1),
(72, 9, 117, 20, 0, 0, 0, 'Active', 1),
(73, 10, 119, 1000, 0, 0, 0, 'Active', 1),
(74, 12, 13, 1000, 0, 0, 0, 'Active', 1),
(75, 13, 11, 40, 0, 0, 0, 'Active', 1),
(76, 14, 106, 1000, 0, 0, 0, 'Active', 1),
(77, 15, 18, 1000, 0, 0, 0, 'Active', 1),
(78, 16, 20, 400, 0, 0, 0, 'Active', 1),
(79, 17, 38, 400, 0, 0, 0, 'Active', 1),
(80, 18, 108, 1000, 0, 0, 0, 'Active', 1),
(81, 19, 36, 200, 0, 0, 0, 'Active', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_info`
--

CREATE TABLE `product_info` (
  `product_table_id` int(11) NOT NULL,
  `software_product_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `company_table_id` int(10) UNSIGNED NOT NULL,
  `group_table_id` int(10) UNSIGNED DEFAULT NULL,
  `report_prodID` varchar(50) NOT NULL,
  `old_prodID` varchar(20) NOT NULL,
  `product_name` varchar(200) NOT NULL,
  `product_type` varchar(30) NOT NULL,
  `packSize` varchar(30) NOT NULL,
  `carton_size` varchar(15) NOT NULL,
  `retail_price` float NOT NULL,
  `trade_price` float NOT NULL,
  `purchase_price` float NOT NULL,
  `purchase_discount` float NOT NULL,
  `sales_tax` float NOT NULL,
  `hold_stock` int(11) NOT NULL,
  `product_status` varchar(50) NOT NULL,
  `creating_user_id` int(10) UNSIGNED NOT NULL,
  `creating_date` varchar(20) NOT NULL,
  `creating_time` varchar(20) NOT NULL,
  `update_user_id` int(10) UNSIGNED NOT NULL,
  `update_date` varchar(20) NOT NULL,
  `update_time` varchar(20) NOT NULL,
  `software_lic_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_info`
--

INSERT INTO `product_info` (`product_table_id`, `software_product_id`, `product_id`, `company_table_id`, `group_table_id`, `report_prodID`, `old_prodID`, `product_name`, `product_type`, `packSize`, `carton_size`, `retail_price`, `trade_price`, `purchase_price`, `purchase_discount`, `sales_tax`, `hold_stock`, `product_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`, `software_lic_id`) VALUES
(4201, 1, 11, 1, 0, '00100062', '1', 'ALPHACOLINE 250 INJ', 'Tablet', '10S', '200', 2586.24, 2198.3, 2066.4, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4202, 2, 42, 4, 0, '00200003', '2', 'APIZYT SYP', 'Tablet', '120ML', '200', 280, 238, 223.72, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4203, 3, 13, 1, 0, '00100019', '3', 'ADLOXIN 400MG INJ', 'Tablet', '250ML', '200', 1000, 850, 790.5, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4204, 4, 14, 1, 0, '00100053', '4', 'AMLO-Q 5MG TAB', 'Tablet', '20S', '200', 123.86, 105.28, 98.97, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4205, 5, 15, 1, 0, '00100054', '5', 'AMLO-Q 10MG TAB', 'Tablet', '20S', '200', 211.85, 180.07, 169.26, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4206, 6, 145, 1, 0, '00100007', '45', 'FAMOPSIN 20MG TAB', 'Tablet', '20S', '200', 200, 170, 159.8, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4207, 7, 153, 1, 0, '00100008', '53', 'FAMOPSIN 40MG TAB', 'Tablet', '10S', '200', 200, 170, 159.8, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4208, 8, 111, 1, 0, '00100063', '11', 'BIOCOBAL 500MG INJ', 'Tablet', '10S', '200', 660, 561, 527.34, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4209, 9, 415, 4, 0, '00200001', '15', 'CALDREE 600 TAB', 'Tablet', '30S', '200', 560, 476, 447.44, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4210, 10, 416, 4, 0, '00200005', '16', 'CARNEEL 500MG TAB', 'Tablet', '30S', '200', 2400, 2040, 1917.6, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4211, 11, 117, 1, 0, '', '17', 'CEFAPEZONE 1GM INJ', 'Tablet', 'VIAL', '200', 296, 252, 232, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4212, 12, 418, 4, 0, '00200008', '18', 'CQ10 100MG CAP', 'Tablet', '30S', '200', 1920, 1632, 1534.08, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4213, 13, 119, 1, 0, '00100013', '19', 'CYCIN 250MG TAB', 'Tablet', '10S', '200', 199.6, 169.66, 159.48, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4214, 14, 120, 1, 0, '00100014', '20', 'CYCIN 500MG TAB', 'Tablet', '10S', '200', 385.9, 328, 308.32, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4215, 15, 121, 1, 0, '00100015', '21', 'CYCIN 200MG INJ', 'Tablet', '100ML', '200', 340, 289, 271.66, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4216, 16, 126, 1, 0, '00100070', '26', 'DAYLINE 250MG INJ (IM)', 'Tablet', 'VIAL', '200', 129.9, 110.4, 103.78, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4217, 17, 127, 1, 0, '00100072', '27', 'DAYLINE 500MG INJ (IM)', 'Tablet', 'VIAL', '200', 218.64, 185.84, 174.69, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4218, 18, 128, 1, 0, '00100073', '28', 'DAYLINE 1GM INJ', 'Tablet', 'VIAL', '200', 346, 294.1, 276.45, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4219, 19, 129, 1, 0, '00100069', '29', 'DAYLINE 250MG INJ (IV)', 'Tablet', 'VIAL', '200', 129.9, 110.4, 103.78, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4220, 20, 130, 1, 0, '00100071', '30', 'DAYLINE 500MG INJ (IV)', 'Tablet', 'VIAL', '200', 218.64, 185.84, 174.69, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4221, 21, 131, 1, 0, '00100077', '31', 'DAYTAXIME 1G INJ', 'Tablet', '1S', '200', 240, 204, 191.76, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4222, 22, 132, 1, 0, '00100075', '32', 'DAYTAXIME 250MG INJ', 'Tablet', '1S', '200', 95.21, 80.93, 76.06, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4223, 23, 133, 1, 0, '00100076', '33', 'DAYTAXIME 500MG INJ', 'Tablet', '1S', '200', 152, 129.2, 121.45, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4224, 24, 134, 1, 0, '00100078', '34', 'DAYPIME 500MG INJ', 'Tablet', 'VIAL', '200', 407.34, 346.24, 325.46, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4225, 25, 135, 1, 0, '00100079', '35', 'DAYPIME 1GM INJ', 'Tablet', 'VIAL', '200', 681.37, 579.17, 544.42, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4226, 26, 136, 1, 0, '00100074', '36', 'DAYFORT 1GM INJ', 'Tablet', 'VIAL', '200', 319.57, 271.63, 255.33, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4227, 27, 140, 1, 0, '00100018', '40', 'EM-FLOX 400MG TAB', 'Tablet', '5S', '200', 589.11, 500.74, 470.7, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4228, 28, 141, 1, 0, '00100005', '41', 'ESKEM 20MG CAP', 'Tablet', '14S', '200', 248.37, 211.12, 198.45, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4229, 29, 142, 1, 0, '00100006', '42', 'ESKEM 40MG CAP', 'Tablet', '14S', '200', 399.67, 339.72, 319.33, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4230, 30, 146, 1, 0, '00100012', '46', 'FUGACIN 200MG TAB', 'Tablet', '10S', '200', 258.83, 219.75, 206.8, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4231, 31, 147, 1, 0, '00100044', '47', 'FREEHALE 5MG TAB', 'Tablet', '14S', '200', 256, 217.6, 204.54, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4232, 32, 148, 1, 0, '00100045', '48', 'FREEHALE 10MG TAB', 'Tablet', '14S', '200', 360, 306, 287.64, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4233, 33, 149, 1, 0, '00100046', '49', 'FREEHALE SACHET 4MG', 'Tablet', '14S', '200', 280, 238, 223.72, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4234, 34, 154, 1, 0, '', '54', 'GARLISH* CAP', 'Tablet', '30S', '200', 547.4, 476, 476, 8, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4235, 35, 159, 1, 0, '00100083', '59', 'HIZONE IM 250MG INJ', 'Tablet', 'VIAL', '200', 134.57, 114.38, 107.51, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4236, 36, 160, 1, 0, '', '60', 'HIZONE IV 250MG INJ', 'Tablet', 'VIAL', '200', 125.55, 112, 100, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4237, 37, 161, 1, 0, '00100082', '61', 'HIZONE 1GM INJ', 'Tablet', 'VIAL', '200', 535.04, 454.78, 427.49, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4238, 38, 162, 1, 0, '00100064', '62', 'HIFER 100MG INJ', 'Tablet', '5S', '200', 1680, 1428, 1342.32, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4239, 39, 166, 1, 0, '00100049', '66', 'JARDIN 10MG TAB', 'Tablet', '10S', '200', 99.09, 84.23, 79.17, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4240, 40, 167, 1, 0, '00100051', '67', 'JARDIN D  TAB', 'Tablet', '10S', '200', 259.22, 220.34, 207.12, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4241, 41, 168, 1, 0, '00100050', '68', 'JARDIN D SYP', 'Tablet', '120ML', '200', 259.22, 220.34, 207.11, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4242, 42, 472, 4, 0, '00200004', '72', 'KROFF SYP', 'Tablet', '120ML', '200', 198, 168.3, 158.2, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4243, 43, 173, 1, 0, '', '73', 'KERT 8MG TAB', 'Tablet', '30S', '200', 214, 182, 167, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4244, 44, 174, 1, 0, '', '74', 'KERT 16MG TAB', 'Tablet', '30S', '200', 312, 266, 245, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4245, 45, 179, 1, 0, '00100009', '79', 'LCYN 250MG TAB', 'Tablet', '10S', '200', 220, 187, 175.78, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4246, 46, 180, 1, 0, '00100010', '80', 'LCYN 500MG TAB', 'Tablet', '10S', '200', 332, 282.2, 265.27, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4247, 47, 181, 1, 0, '00100011', '81', 'LCYN 750MG TAB', 'Tablet', '10S', '200', 440, 374, 351.56, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4248, 48, 485, 4, 0, '00200009', '85', 'MAXFOL 400mcg TAB', 'Tablet', '30S', '200', 280, 238, 223.72, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4249, 49, 186, 1, 0, '00100065', '86', 'MIXEL 200MG CAP', 'Tablet', '5S', '200', 234.49, 199.32, 187.36, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4250, 50, 187, 1, 0, '00100066', '87', 'MIXEL 400MG CAP', 'Tablet', '5S', '200', 394.51, 335.33, 315.21, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4251, 51, 188, 1, 0, '00100067', '88', 'MIXEL 100MG SYP', 'Tablet', '30ML', '200', 220.18, 187.15, 175.92, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4252, 52, 189, 1, 0, '00100068', '89', 'MIXEL DS 200MG SYP', 'Tablet', '30ML', '200', 277, 235.45, 221.32, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4253, 53, 193, 1, 0, '00100037', '93', 'NIMODIN 30MG TAB', 'Tablet', '30S', '200', 560, 476, 447.44, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4254, 54, 497, 4, 0, '00200007', '97', 'OXIVA TAB', 'Tablet', '20S', '200', 1600, 1360, 1278.4, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4255, 55, 198, 1, 0, '00100023', '98', 'ONE-3 20/120MG TAB', 'Tablet', '16S', '200', 411.55, 349.82, 328.83, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4256, 56, 199, 1, 0, '00100024', '99', 'ONE-3 40/240MG TAB', 'Tablet', '8S', '200', 339.53, 288.6, 271.28, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4257, 57, 1100, 1, 0, '00100025', '100', 'ONE-3 80/480MG TAB', 'Tablet', '6S', '200', 424.41, 360.75, 339.1, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4258, 58, 1104, 1, 0, '00100058', '104', 'PYTEX 20MG TAB', 'Tablet', '20S', '200', 259.21, 220.33, 207.11, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4259, 59, 1105, 1, 0, '00100047', '105', 'PYLOCLAR 250MG TAB', 'Tablet', '10S', '200', 350.65, 298.05, 280.17, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4260, 60, 1106, 1, 0, '00100048', '106', 'PYLOCLAR 500MG TAB', 'Tablet', '10S', '200', 612.43, 520.57, 489.33, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4261, 61, 1107, 1, 0, '00100020', '107', 'POLYMALT 100MG TAB', 'Tablet', '20S', '200', 251.81, 214.04, 201.2, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4262, 62, 1108, 1, 0, '00100022', '108', 'POLYMALT  SYP', 'Tablet', '120ML', '200', 197.01, 167.46, 157.41, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4263, 63, 1109, 1, 0, '00100021', '109', 'POLYMALT F TAB', 'Tablet', '30S', '200', 334.76, 284.54, 267.47, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4264, 64, 1110, 1, 0, '00100052', '110', 'PLATLO 75MG TAB', 'Tablet', '14S', '200', 265.1, 225.33, 211.81, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4265, 65, 1114, 1, 0, '00100016', '114', 'QUPRON 250MG TAB', 'Tablet', '10S', '200', 159.03, 135.17, 125.71, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4266, 66, 1115, 1, 0, '00100017', '115', 'QUPRON 500MG TAB', 'Tablet', '10S', '200', 220, 187, 174, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4267, 67, 1123, 1, 0, '00100055', '123', 'REMETHAN 50MG TAB', 'Tablet', '20S', '200', 120, 102, 95.88, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4268, 68, 1124, 1, 0, '00100056', '124', 'REMETHAN 100R TAB', 'Tablet', '30S', '200', 360, 306, 287.64, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4269, 69, 1118, 1, 0, '00100059', '118', 'QSARTAN 25MG TAB', 'Tablet', '20S', '200', 158.48, 134.71, 126.62, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4270, 70, 1119, 1, 0, '00100060', '119', 'QSARTAN 50MG TAB', 'Tablet', '20S', '200', 279.05, 237.19, 222.96, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4271, 71, 1120, 1, 0, '00100061', '120', 'QSARTAN PLUS 50/12.5MG TAB', 'Tablet', '20S', '200', 444.35, 377.7, 355.04, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4272, 72, 1121, 1, 0, '00100080', '121', 'Q-BACT 1GM INJ', 'Tablet', 'VIAL', '200', 370.31, 314.76, 295.87, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4273, 73, 1122, 1, 0, '00100081', '122', 'Q-BACT 2GM INJ', 'Tablet', 'VIAL', '200', 518.43, 440.67, 414.23, 8, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4274, 74, 4126, 4, 0, '00200006', '126', 'ROXVITA TAB', 'Tablet', '30S', '200', 480, 408, 383.52, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4275, 75, 1127, 1, 0, '00100035', '127', 'REMENT 10MG TAB', 'Tablet', '30S', '200', 870, 739.5, 695.13, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4276, 76, 1128, 1, 0, '00100036', '128', 'REMENT 20MG TAB', 'Tablet', '10S', '200', 472, 401.2, 377.13, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4277, 77, 1129, 1, 0, '00100001', '129', 'RULING 20MG CAP', 'Tablet', '14S', '200', 198.18, 168.46, 158.35, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4278, 78, 1130, 1, 0, '00100002', '130', 'RULING 40MG CAP', 'Tablet', '14S', '200', 344, 292.4, 274.86, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4279, 79, 1131, 1, 0, '00100004', '131', 'RULING INJ 40MG', 'Tablet', 'VIAL', '200', 357.82, 304.15, 285.9, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4280, 80, 1132, 1, 0, '00100003', '132', 'RULING+ SACHET 40MG', 'Tablet', '10S', '200', 269.58, 229.14, 215.4, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4281, 81, 1137, 1, 0, '00100042', '137', 'SULVORID 25MG TAB', 'Tablet', '20S', '200', 225.15, 191.38, 179.89, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4282, 82, 1138, 1, 0, '00100043', '138', 'SULVORID 50MG TAB', 'Tablet', '20S', '200', 374.75, 318.54, 299.43, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4283, 83, 1139, 1, 0, '00100084', '139', 'SUNVIT INJ', 'Tablet', '1S', '200', 187.5, 151.73, 142.62, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4284, 84, 1140, 1, 0, '00100041', '140', 'STOMACOL 10MG TAB', 'Tablet', '50S', '200', 314.1, 266.99, 250.97, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4285, 85, 1141, 1, 0, '00100038', '141', 'STAT A 10MG TAB', 'Tablet', '14S', '200', 252, 214.2, 201.35, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4286, 86, 1142, 1, 0, '00100039', '142', 'STAT A 20MG TAB', 'Tablet', '14S', '200', 340, 289, 271.66, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4287, 87, 1143, 1, 0, '00100040', '143', 'STAT A 40MG TAB', 'Tablet', '14S', '200', 416.5, 354, 325, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4288, 88, 1147, 1, 0, '00100033', '147', 'UFRIM 10MG TAB', 'Tablet', '14S', '200', 312, 265.2, 249.28, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4289, 89, 1148, 1, 0, '00100034', '148', 'UFRIM 20MG TAB', 'Tablet', '14S', '200', 380, 323, 303.62, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4290, 90, 1452, 1, 0, '', '452', 'VILDOS 50MG TAB', 'Tablet', '28S', '200', 672, 571.25, 525.55, 8, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4291, 91, 1153, 1, 0, '00100031', '153', 'VILDOMET 50/850MG TAB', 'Tablet', '14S', '200', 396, 336.6, 316.4, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4292, 92, 1154, 1, 0, '00100032', '154', 'VILDOMET 50/1000MG TAB', 'Tablet', '14S', '200', 396, 336.6, 316.4, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4293, 93, 1155, 1, 0, '', '155', 'VITA JUNIOR* SACHET', 'Tablet', '10S', '200', 222, 189, 174, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4294, 94, 1160, 1, 0, '00100026', '160', 'ZEENARYL 1MG TAB', 'Tablet', '20S', '200', 120, 102, 95.88, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4295, 95, 1161, 1, 0, '00100027', '161', 'ZEENARYL 2MG TAB', 'Tablet', '20S', '200', 184, 156.4, 147.01, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4296, 96, 1162, 1, 0, '00100028', '162', 'ZEENARYL 3MG TAB', 'Tablet', '20S', '200', 253, 215.05, 202.15, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4297, 97, 1163, 1, 0, '00100029', '163', 'ZEENARYL 4MG TAB', 'Tablet', '20S', '200', 360, 306, 287.64, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4298, 98, 1151, 1, 0, '00100030', '151', 'VILDOS 50MG TAB', 'Tablet', '28S', '200', 672, 571.2, 536.93, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4299, 99, 114, 1, 0, '', '14', 'CQ10 50MG CAP', 'Tablet', '30S', '200', 1192, 1014, 932, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4300, 100, 4103, 4, 0, '00200015', '103', 'PROHALE TAB', 'Tablet', '30S', '200', 1860, 1581, 1470.33, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4301, 101, 1125, 1, 0, '00100057', '125', 'REMETHAN INJ 75MG', 'Tablet', '10S', '200', 342, 290.7, 273.25, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4302, 102, 490, 4, 0, '00200010', '90', 'Maxfol 600mcg Tab', 'Tablet', '30S', '200', 342, 290.7, 273.25, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4303, 103, 143, 1, 0, '00100085', '43', 'ENTUM 80MG INJ', 'Tablet', '6S', '200', 864, 734.4, 690.33, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4304, 104, 196, 1, 0, '00100086', '96', 'ONAMED 150MG CAP', 'Tablet', '1S', '200', 221.35, 188.15, 176.86, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4305, 105, 175, 1, 0, '', '75', 'KETOLAC 30MG INJ', 'Tablet', '5S', '200', 430, 365.5, 337, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4306, 106, 125, 1, 0, '00100089', '25', 'DAYZONE 1G INJ (IV)', 'Tablet', 'VIAL', '200', 280, 238, 223.72, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4307, 107, 1144, 1, 0, '', '144', 'SOFONIL 400MG TAB', 'Tablet', '28S', '200', 5868, 4000, 3800, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4308, 108, 150, 1, 0, '00100091', '50', 'FORTIUS 5MG TAB', 'Tablet', '10S', '200', 144, 122.4, 115.06, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4309, 109, 151, 1, 0, '00100092', '51', 'FORTIUS 10MG TAB', 'Tablet', '10S', '200', 240, 204, 191.76, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4310, 110, 152, 1, 0, '00100093', '52', 'FORTIUS 20MG TAB', 'Tablet', '10S', '200', 412, 350.2, 329.18, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4311, 111, 1145, 1, 0, '00100090', '145', 'SOLOSIN SR 0.4MG CAP', 'Tablet', '20S', '200', 782, 664.7, 624.81, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4312, 112, 422, 4, 0, '00200012', '22', 'CRANTOP SACHET', 'Tablet', '10S', '200', 360, 306, 287.64, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4313, 113, 124, 1, 0, '', '24', 'DACLAHEP-60 TAB', 'Tablet', '28S', '200', 4552, 4000, 3869, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4314, 114, 1156, 1, 0, '00100098', '156', 'VANCOTIC 1G INJ', 'Tablet', 'VIAL', '200', 1564.63, 1329.94, 1249.64, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4315, 115, 1157, 1, 0, '00100099', '157', 'VINZAT 2.25G IV INJ', 'Tablet', 'VIAL', '200', 474.4, 403.24, 379.04, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4316, 116, 1158, 1, 0, '00100100', '158', 'VINZAT 4.5G IV INJ', 'Tablet', 'VIAL', '200', 976.7, 830.2, 780.39, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4317, 117, 413, 4, 0, '00200002', '13', 'Caldree DS Tab', 'Tablet', '30S', '200', 960, 816, 767.04, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4318, 118, 4101, 4, 0, '00200013', '101', 'OSLIA SOFTGEL', 'Tablet', '1S', '200', 240, 204, 191.76, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4319, 119, 4159, 4, 0, '00200014', '159', 'VIDA-Z TAB', 'Tablet', '30S', '200', 592, 503.2, 473, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4320, 120, 210, 2, 0, '', '10', 'CLOLAM 500MG VIGINAL TAB', 'Tablet', '1S', '200', 73.6, 62.56, 56.3, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4321, 121, 211, 2, 0, '', '11', 'CALTIS 125MG DRY SUSP', 'Tablet', '60ML', '200', 256.45, 217.98, 196.18, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4322, 122, 212, 2, 0, '', '12', 'CALTIS 250MG DRY SUSP', 'Tablet', '60ML', '200', 423.2, 359.72, 323.75, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4323, 123, 219, 2, 0, '', '19', 'ESPHARM 20MG CAP', 'Tablet', '14S', '200', 136.85, 116.32, 104.69, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4324, 124, 220, 2, 0, '', '20', 'ESPHARM 40MG CAP', 'Tablet', '14S', '200', 227.7, 193.55, 174.19, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4325, 125, 221, 2, 0, '', '21', 'ERCOPD 150MG CAP', 'Tablet', '20S', '200', 155.25, 131.96, 118.77, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4326, 126, 222, 2, 0, '', '22', 'ERCOPD 175MG DRY SUSP', 'Tablet', '100ML', '200', 155.25, 131.96, 118.77, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4327, 127, 231, 2, 0, '', '31', 'HIPOUR 40MG TAB', 'Tablet', '20S', '200', 360, 306, 275.4, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4328, 128, 232, 2, 0, '', '32', 'HIPOUR 80MG TAB', 'Tablet', '20S', '200', 680, 578, 520.2, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4329, 129, 240, 2, 0, '', '40', 'ISFLO 150MG CAP', 'Tablet', '1S', '200', 158.7, 134.9, 121.41, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4330, 130, 248, 2, 0, '', '48', 'LUFTHER 15/90MG DRY SUSP', 'Tablet', '1S', '200', 103.5, 87.97, 79.18, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4331, 131, 255, 2, 0, '', '55', 'MOZOLE 20MG CAP', 'Tablet', '14S', '200', 165.6, 140.76, 126.68, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4332, 132, 256, 2, 0, '', '56', 'MOZOLE 40MG CAP', 'Tablet', '14S', '200', 287.5, 244.38, 219.94, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4333, 133, 257, 2, 0, '', '57', 'MENOP 2GM SACHETS', 'Tablet', '7S', '200', 885.5, 752.68, 677.41, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4334, 134, 258, 2, 0, '', '58', 'MOLAM 400MG TAB', 'Tablet', '5S', '200', 546.25, 464.31, 417.88, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4335, 135, 265, 2, 0, '', '65', 'PITRIN 20MG TAB', 'Tablet', '20S', '200', 201.25, 171.06, 153.96, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4336, 136, 266, 2, 0, '', '66', 'PRIDET 50MG TAB', 'Tablet', '10S', '200', 207, 175.95, 158.36, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4337, 137, 280, 2, 0, '', '80', 'TAMIS 0.4MG CAP', 'Tablet', '10S', '200', 601.45, 511.23, 460.11, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4338, 138, 281, 2, 0, '', '81', 'TRANIC 250MG CAP', 'Tablet', '100S', '200', 747.5, 635.38, 571.84, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4339, 139, 282, 2, 0, '', '82', 'TRANIC 500MG CAP', 'Tablet', '20S', '200', 287.5, 244.38, 219.94, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4340, 140, 310, 3, 0, '', '10', 'BROPHYL SYP', 'Tablet', '120ML', '200', 67, 56.95, 56.95, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4341, 141, 311, 3, 0, '', '11', 'BROPHYL D SYP', 'Tablet', '120ML', '200', 65, 55.25, 55.25, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4342, 142, 313, 3, 0, '', '13', 'BENDAZOLE SUSP', 'Tablet', '10ML', '200', 26.8, 22.78, 22.78, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4343, 143, 314, 3, 0, '', '14', 'BROXOL SYP', 'Tablet', '120ML', '200', 50, 42.5, 42.5, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4344, 144, 315, 3, 0, '', '15', 'BROXOL DM SYP', 'Tablet', '120ML', '200', 60, 51, 51, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4345, 145, 320, 3, 0, '', '20', 'CALSID SYP', 'Tablet', '120ML', '200', 69, 58.65, 58.65, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4346, 146, 329, 3, 0, '', '29', 'DYMIN 50MG TAB', 'Tablet', '100S', '200', 100.28, 85.24, 85.24, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4347, 147, 330, 3, 0, '', '30', 'DYMIN SYP', 'Tablet', '60ML', '200', 31.61, 26.87, 26.87, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4348, 148, 341, 3, 0, '', '41', 'FENBRO 100MG SYP', 'Tablet', '90ML', '200', 46.95, 39.91, 39.91, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4349, 149, 342, 3, 0, '', '42', 'FENBRO 8PLUS SYP', 'Tablet', '90ML', '200', 73, 62.05, 62.05, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4350, 150, 344, 3, 0, '', '44', 'FURAMID SYP', 'Tablet', '60ML', '200', 42, 35.7, 35.7, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4351, 151, 346, 3, 0, '', '46', 'FERVIT LIQ.', 'Tablet', '120ML', '200', 58.5, 49.73, 49.73, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4352, 152, 355, 3, 0, '', '55', 'HEX 50MG TAB', 'Tablet', '10S', '200', 200, 170, 170, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4353, 153, 370, 3, 0, '', '70', 'KAMIC 250MG TAB', 'Tablet', '200S', '200', 300, 255, 255, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4354, 154, 371, 3, 0, '', '71', 'KAMIC FORTE TAB', 'Tablet', '100S', '200', 264, 224.4, 224.4, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4355, 155, 380, 3, 0, '', '80', 'MANACID SYP', 'Tablet', '120ML', '200', 46, 39.1, 39.1, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4356, 156, 381, 3, 0, '', '81', 'MANACID FORTE SYP', 'Tablet', '120ML', '200', 65, 55.25, 55.25, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4357, 157, 390, 3, 0, '', '90', 'NAXID 250MG SUSP', 'Tablet', '60ML', '200', 80, 68, 68, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4358, 158, 3100, 3, 0, '', '100', 'PEDROL 500MG TAB', 'Tablet', '200S', '200', 264.71, 225, 225, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4359, 159, 3102, 3, 0, '', '102', 'PEDROL SYP', 'Tablet', '60ML', '200', 35, 29.75, 29.75, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4360, 160, 3103, 3, 0, '', '103', 'PEDROL FORTE 60ML SYP', 'Tablet', '60ML', '200', 44, 37.4, 37.4, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4361, 161, 3104, 3, 0, '', '104', 'PEDROL FORTE 90ML SYP', 'Tablet', '90ML', '200', 60, 51, 51, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4362, 162, 3105, 3, 0, '', '105', 'PEDROL TOTAL SYP', 'Tablet', '60ML', '200', 46, 39.1, 39.1, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4363, 163, 3106, 3, 0, '', '106', 'PEDROL DROPS', 'Tablet', '20ML', '200', 40, 34, 34, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4364, 164, 3107, 3, 0, '', '107', 'PERL 20MG CAP', 'Tablet', '14S', '200', 168, 142.8, 142.8, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4365, 165, 3120, 3, 0, '', '120', 'RIDOX 100MG CAP', 'Tablet', '100S', '200', 300, 255, 255, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4366, 166, 3121, 3, 0, '', '121', 'ROZID 250MG TAB', 'Tablet', '10S', '200', 100, 85, 85, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4367, 167, 3122, 3, 0, '', '122', 'ROZID 500MG TAB', 'Tablet', '10S', '200', 190, 161.5, 161.5, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4368, 168, 3124, 3, 0, '', '124', 'RIAM SYP', 'Tablet', '90ML', '200', 60, 51, 51, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4369, 169, 3126, 3, 0, '', '126', 'RID-ALL FORTE TAB', 'Tablet', '30S', '200', 136.85, 116.32, 116.32, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4370, 170, 3127, 3, 0, '', '127', 'RINTAL SYP', 'Tablet', '120ML', '200', 48.5, 41.23, 41.23, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4371, 171, 3135, 3, 0, '', '135', 'SANID 100MG TAB', 'Tablet', '30S', '200', 213, 181.05, 181.05, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4372, 172, 3145, 3, 0, '', '145', 'VERIL 500MCG TAB', 'Tablet', '20S', '200', 130, 110.5, 110.5, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4373, 173, 3146, 3, 0, '', '146', 'VASDEX-L SYP', 'Tablet', '120ML', '200', 80, 68, 68, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4374, 174, 3165, 3, 0, '', '165', 'ZOLT TAB', 'Tablet', '30S', '200', 74, 62.9, 62.9, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4375, 175, 3166, 3, 0, '', '166', 'ZOLT DS TAB', 'Tablet', '15S', '200', 90, 76.5, 76.5, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4376, 176, 3167, 3, 0, '', '167', 'ZOLT SUSP', 'Tablet', '90ML', '200', 65, 55.25, 55.25, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4377, 177, 3136, 3, 0, '', '136', 'STENZAR SYRUP', 'Tablet', '120ML', '200', 45, 38.25, 38.25, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4378, 178, 312, 3, 0, '', '12', 'BENDAZOL TABLET', 'Tablet', '2S', '200', 20.9, 17.77, 17.77, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4379, 179, 343, 3, 0, '', '43', 'FERFOLIC CAPSULES', 'Tablet', '30S', '200', 45, 38.25, 38.25, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4380, 180, 3101, 3, 0, '', '101', 'PEDROL EXTRA TABLET', 'Tablet', '100S', '200', 150, 127.5, 127.5, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4381, 181, 345, 3, 0, '', '45', 'FERVIT CAPSULES', 'Tablet', '30S', '200', 99, 84.15, 84.15, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4382, 182, 3123, 3, 0, '', '123', 'RIAM TABLET', 'Tablet', '100S', '200', 200, 170, 170, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4383, 183, 382, 3, 0, '', '82', 'MULTONIC SYRUP', 'Tablet', '120ML', '200', 60, 51, 51, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4384, 184, 3125, 3, 0, '', '125', 'RID-ALL', 'Tablet', '100S', '200', 350, 297.5, 297.5, 10, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4385, 185, 24, 2, 0, '', '4', 'ANTEM TABLET', 'Tablet', '30S', '200', 158.7, 134.9, 121.41, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4386, 186, 246, 2, 0, '', '46', 'LAMCIT 250MG TAB', 'Tablet', '10S', '200', 149.5, 127.08, 114.37, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4387, 187, 247, 2, 0, '', '47', 'LAMCIT 500MG TAB', 'Tablet', '10S', '200', 247.25, 210.16, 189.15, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4388, 188, 230, 2, 0, '', '30', 'HEMIFOL TAB', 'Tablet', '20S', '200', 109.25, 92.86, 83.58, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4389, 189, 249, 2, 0, '', '49', 'LAMLOX 250MG TAB', 'Tablet', '10S', '200', 180, 153, 137.7, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4390, 190, 250, 2, 0, '', '50', 'LAMLOX 500MG TAB', 'Tablet', '10S', '200', 310, 263.5, 237.15, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4391, 191, 283, 2, 0, '', '83', 'TRAPAM 50MG  CAP', 'Tablet', '10S', '200', 120.75, 102.64, 92.37, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4392, 192, 182, 1, 0, '00100125', '82', 'LETOCOR 2.5MG TAB', 'Tablet', '10S', '200', 2390, 2031.5, 1909.61, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4393, 193, 191, 1, 0, '00100126', '91', 'MONAK INJ 30MG', 'Tablet', '5S', '200', 637, 541.45, 508.91, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4394, 194, 163, 1, 0, '00100134', '63', 'ITOPER 50MG TAB', 'Tablet', '10S', '200', 225.81, 191.94, 180.42, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4395, 195, 4150, 4, 0, '00200017', '150', 'VIDACAL C SACHET', 'Tablet', '10S', '200', 380, 323, 303.62, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4396, 196, 1152, 1, 0, '00100135', '152', 'VILDOMET 50/500mg Tab', 'Tablet', '14S', '200', 396, 336.6, 316.4, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4397, 197, 1164, 1, 0, '00100136', '164', 'ZYTO 250MG TAB', 'Tablet', '6S', '200', 222.19, 188.87, 177.53, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4398, 198, 1165, 1, 0, '00100137', '165', 'ZYTO 500MG TAB', 'Tablet', '6S', '200', 407.34, 346.24, 325.46, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4399, 199, 1166, 1, 0, '00100138', '166', 'ZYTO SUSP 200MG', 'Tablet', '1S', '200', 230.22, 195.69, 183.94, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4400, 200, 1167, 1, 0, '00100143', '167', 'X-LOX 400MG/250ML I.V.', 'Tablet', 'VIAL', '200', 1200, 1020, 958.8, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4401, 201, 122, 1, 0, '00100101', '22', 'COLIATE INJ', 'Tablet', '1S', '200', 1600, 1360, 1278.4, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4402, 202, 1133, 1, 0, '00100139', '133', 'RETERIC 50MG CAP', 'Tablet', '14S', '200', 284, 241.4, 226.91, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4403, 203, 1134, 1, 0, '00100140', '134', 'RETERIC 75MG CAP', 'Tablet', '14S', '200', 321.31, 273.12, 256.73, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4404, 204, 1135, 1, 0, '00100141', '135', 'RETERIC 100MG CAP', 'Tablet', '14S', '200', 392.03, 333.23, 313.23, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4405, 205, 1136, 1, 0, '00100142', '136', 'RETERIC 150mg CAP', 'Tablet', '14S', '200', 480, 408, 383.52, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4406, 206, 137, 1, 0, '00100144', '37', 'DAYLINE 2G INJ', 'Tablet', '1S', '200', 468, 397.8, 373.93, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4407, 207, 138, 1, 0, '00100145', '38', 'EMPAGLI 10MG TAB', 'Tablet', '14S', '200', 280, 238, 223.72, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4408, 208, 139, 1, 0, '00100146', '39', 'EMPAGLI 25MG TAB', 'Tablet', '14S', '200', 398, 338.3, 318, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4409, 209, 484, 4, 0, '', '84', 'MEDITON CAP', 'Tablet', '30S', '200', 1600, 1360, 1278.4, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1),
(4410, 210, 144, 1, 0, '', '44', 'EBAK 10MG TAB', 'Tablet', '', '200', 140.72, 119.61, 112.43, 0, 0, 0, 'Active', 1, '29/Nov/2021', '11:51 AM', 1, '29/Nov/2021', '11:51 AM', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_overall_record`
--

CREATE TABLE `product_overall_record` (
  `overall_record_id` int(11) NOT NULL,
  `software_overall_record_id` int(10) UNSIGNED NOT NULL,
  `product_table_id` int(11) NOT NULL,
  `prod_name` varchar(100) NOT NULL,
  `prod_batch` varchar(50) NOT NULL,
  `quantity_sold` int(11) NOT NULL,
  `quantity_return` int(11) NOT NULL,
  `trade_rate` float NOT NULL,
  `purch_rate` float NOT NULL,
  `retail_rate` float NOT NULL,
  `expired_quantity` int(11) NOT NULL,
  `damaged_quantity` int(11) NOT NULL,
  `prod_status` varchar(50) NOT NULL,
  `software_lic_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_info`
--

CREATE TABLE `purchase_info` (
  `purchase_id` int(11) UNSIGNED NOT NULL,
  `software_purchase_id` int(10) UNSIGNED NOT NULL,
  `comp_id` int(10) NOT NULL,
  `invoice_num` varchar(11) NOT NULL,
  `supplier_id` int(11) UNSIGNED NOT NULL,
  `purchase_date` varchar(20) NOT NULL,
  `gross_amount` float NOT NULL,
  `disc_amount` float NOT NULL,
  `net_amount` float NOT NULL,
  `enter_user_id` int(11) NOT NULL,
  `entry_date` varchar(20) NOT NULL,
  `entry_time` varchar(20) NOT NULL,
  `update_user_id` int(10) UNSIGNED DEFAULT NULL,
  `update_date` varchar(20) NOT NULL,
  `update_time` varchar(20) NOT NULL,
  `comments` varchar(200) DEFAULT NULL,
  `status` varchar(20) NOT NULL,
  `software_lic_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_info`
--

INSERT INTO `purchase_info` (`purchase_id`, `software_purchase_id`, `comp_id`, `invoice_num`, `supplier_id`, `purchase_date`, `gross_amount`, `disc_amount`, `net_amount`, `enter_user_id`, `entry_date`, `entry_time`, `update_user_id`, `update_date`, `update_time`, `comments`, `status`, `software_lic_id`) VALUES
(1, 1, 1, '25', 1, '23/Feb/2022', 49485, 0, 49485, 1, '23/Feb/2022', '12:38 PM', 1, '23/Feb/2022', '12:38 PM', '', 'Active', 1),
(2, 2, 1, '587', 1, '26/Feb/2022', 118482, 0, 118482, 1, '26/Feb/2022', '12:47 PM', 1, '26/Feb/2022', '12:47 PM', '', 'Active', 1),
(3, 3, 1, '648', 1, '27/Feb/2022', 383520, 0, 383520, 1, '27/Feb/2022', '11:02 AM', 1, '27/Feb/2022', '11:02 AM', '', 'Active', 1),
(4, 4, 1, '6558', 1, '27/Feb/2022', 197508, 0, 197508, 1, '27/Feb/2022', '11:09 AM', 1, '27/Feb/2022', '11:09 AM', '', 'Active', 1),
(5, 5, 1, '2548', 1, '27/Feb/2022', 172584, 0, 172584, 1, '27/Feb/2022', '11:45 AM', 1, '27/Feb/2022', '11:45 AM', '', 'Active', 1),
(6, 6, 1, '6578', 1, '27/Feb/2022', 172584, 0, 172584, 1, '27/Feb/2022', '11:48 AM', 1, '27/Feb/2022', '11:48 AM', '', 'Active', 1);

-- --------------------------------------------------------

--
-- Table structure for table `purchase_info_detail`
--

CREATE TABLE `purchase_info_detail` (
  `purchase_info_detail_id` int(10) UNSIGNED NOT NULL,
  `software_purhchase_info_detail_id` int(10) UNSIGNED NOT NULL,
  `invoice_num` varchar(15) NOT NULL,
  `purchase_id` int(10) NOT NULL,
  `prod_id` int(11) NOT NULL,
  `discount` float NOT NULL,
  `bonus_quant` int(11) NOT NULL,
  `recieve_quant` int(11) NOT NULL,
  `batch_no` varchar(20) NOT NULL,
  `expiry_date` varchar(20) NOT NULL,
  `gross_amount` float NOT NULL,
  `disc_amount` float NOT NULL,
  `net_amount` float NOT NULL,
  `invoice_date` varchar(20) NOT NULL,
  `returnBit` tinyint(1) NOT NULL,
  `returned_quant` int(11) NOT NULL,
  `returned_bonus_quant` int(11) NOT NULL,
  `software_lic_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_info_detail`
--

INSERT INTO `purchase_info_detail` (`purchase_info_detail_id`, `software_purhchase_info_detail_id`, `invoice_num`, `purchase_id`, `prod_id`, `discount`, `bonus_quant`, `recieve_quant`, `batch_no`, `expiry_date`, `gross_amount`, `disc_amount`, `net_amount`, `invoice_date`, `returnBit`, `returned_quant`, `returned_bonus_quant`, `software_lic_id`) VALUES
(1, 1, '3565', 1, 14, 0, 0, 500, '5AQ251', '19/Feb/2022', 49485, 0, 49485, '19/Feb/2022', 0, 0, 0, 1),
(2, 2, '2548', 1, 14, 0, 0, 500, '5AQ251', '19/Feb/2022', 49485, 0, 49485, '19/Feb/2022', 0, 0, 0, 1),
(3, 3, '5244', 1, 14, 0, 0, 500, '5AQ265', '19/Feb/2022', 49485, 0, 49485, '19/Feb/2022', 0, 0, 0, 1),
(4, 4, '25', 1, 14, 0, 0, 500, '55', '19/Feb/2022', 49485, 0, 49485, '19/Feb/2022', 0, 0, 0, 1),
(5, 5, '25', 1, 14, 0, 0, 500, '55', '19/Feb/2022', 49485, 0, 49485, '19/Feb/2022', 0, 0, 0, 1),
(6, 6, '155', 1, 14, 0, 0, 500, '552', '19/Feb/2022', 49485, 0, 49485, '19/Feb/2022', 0, 0, 0, 1),
(7, 7, '254', 1, 14, 0, 0, 500, '52', '19/Feb/2022', 49485, 0, 49485, '19/Feb/2022', 0, 0, 0, 1),
(8, 8, '54', 1, 14, 0, 0, 100, '5AQ254', '23/Feb/2022', 9897, 0, 9897, '23/Feb/2022', 0, 0, 0, 1),
(9, 9, '25', 1, 14, 0, 0, 500, '5AQ215', '23/Feb/2022', 49485, 0, 49485, '23/Feb/2022', 0, 0, 0, 1),
(10, 10, '587', 2, 15, 0, 0, 700, '1AQ264', '26/Feb/2022', 118482, 0, 118482, '26/Feb/2022', 0, 0, 0, 1),
(11, 11, '648', 3, 151, 0, 0, 2000, '1FT265', '27/Feb/2022', 383520, 0, 383520, '27/Feb/2022', 0, 0, 0, 1),
(12, 12, '6558', 4, 152, 0, 0, 600, '2FT365', '27/Feb/2022', 197508, 0, 197508, '27/Feb/2022', 0, 0, 0, 1),
(13, 13, '2548', 5, 109, 0, 0, 900, '1FT264', '27/Feb/2022', 172584, 0, 172584, '27/Feb/2022', 0, 0, 0, 1),
(14, 14, '6578', 6, 109, 0, 0, 900, '1FT977', '27/Feb/2022', 172584, 0, 172584, '27/Feb/2022', 0, 0, 0, 1),
(15, 15, '6955', 7, 111, 0, 0, 240, 'BLI-512', '07/Mar/2022', 126562, 0, 126562, '07/Mar/2022', 0, 0, 0, 1),
(16, 16, '6957', 7, 111, 0, 0, 240, 'BLI-0321', '07/Mar/2022', 126562, 0, 126562, '07/Mar/2022', 0, 0, 0, 1),
(17, 17, '3214', 8, 117, 0, 0, 20, '2514', '07/Mar/2022', 4640, 0, 4640, '07/Mar/2022', 0, 0, 0, 1),
(18, 18, '6254', 8, 119, 0, 0, 1000, '2CY264', '07/Mar/2022', 159480, 0, 159480, '07/Mar/2022', 0, 0, 0, 1),
(19, 19, '6254', 8, 119, 0, 0, 1000, '2CY264', '07/Mar/2022', 159480, 0, 159480, '07/Mar/2022', 0, 0, 0, 1),
(20, 20, '3625', 8, 120, 0, 0, 1000, '5CY264', '07/Mar/2022', 308320, 0, 308320, '07/Mar/2022', 0, 0, 0, 1),
(21, 21, '6325', 8, 13, 0, 0, 1000, '2CY854', '07/Mar/2022', 159480, 0, 159480, '07/Mar/2022', 0, 0, 0, 1),
(22, 22, '2634', 8, 11, 0, 0, 30, '5210', '07/Mar/2022', 6960, 0, 6960, '07/Mar/2022', 0, 0, 0, 1),
(23, 23, '2634', 8, 13, 0, 0, 100, '2CY145', '07/Mar/2022', 15948, 0, 15948, '07/Mar/2022', 0, 0, 0, 1),
(24, 24, '2634', 8, 14, 0, 0, 200, '5CY214', '07/Mar/2022', 61664, 0, 61664, '07/Mar/2022', 0, 0, 0, 1),
(25, 25, '367', 9, 13, 0, 0, 1000, '2CY456', '07/Mar/2022', 159480, 0, 159480, '07/Mar/2022', 0, 0, 0, 1),
(26, 26, '367', 9, 11, 0, 0, 20, '1224', '07/Mar/2022', 4640, 0, 4640, '07/Mar/2022', 0, 0, 0, 1),
(27, 27, '367', 9, 14, 0, 0, 200, '5CY26', '07/Mar/2022', 61664, 0, 61664, '07/Mar/2022', 0, 0, 0, 1),
(28, 28, '637', 10, 106, 0, 0, 2000, '5DZ264', '07/Mar/2022', 447440, 0, 447440, '07/Mar/2022', 0, 0, 0, 1),
(29, 29, '3697', 10, 106, 0, 0, 2000, '1DZ254', '07/Mar/2022', 447440, 0, 447440, '07/Mar/2022', 0, 0, 0, 1),
(30, 30, '3254', 10, 106, 0, 0, 2000, '1DZ265', '07/Mar/2022', 447440, 0, 447440, '07/Mar/2022', 0, 0, 0, 1),
(31, 31, '2314', 10, 106, 0, 0, 1000, '1DX361', '07/Mar/2022', 223720, 0, 223720, '07/Mar/2022', 0, 0, 0, 1),
(32, 32, '2314', 10, 18, 0, 0, 1000, '1DL264', '07/Mar/2022', 276450, 0, 276450, '07/Mar/2022', 0, 0, 0, 1),
(33, 33, '2647', 11, 20, 0, 0, 400, '5DY255', '08/Mar/2022', 69876, 0, 69876, '08/Mar/2022', 0, 0, 0, 1),
(34, 34, '2648', 12, 109, 0, 0, 1000, '1FT251', '11/Mar/2022', 191760, 0, 191760, '11/Mar/2022', 0, 0, 0, 1),
(35, 35, '2648', 12, 38, 0, 0, 400, 'HFR-362', '11/Mar/2022', 536928, 0, 536928, '11/Mar/2022', 0, 0, 0, 1),
(36, 36, '3674', 13, 109, 0, 0, 1000, '', '15/Mar/2022', 191760, 0, 191760, '15/Mar/2022', 0, 0, 0, 1),
(37, 37, '3674', 13, 108, 0, 0, 1000, '', '15/Mar/2022', 115060, 0, 115060, '15/Mar/2022', 0, 0, 0, 1),
(38, 38, '3674', 13, 36, 0, 0, 200, '', '15/Mar/2022', 20000, 0, 20000, '15/Mar/2022', 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order_detail`
--

CREATE TABLE `purchase_order_detail` (
  `order_detail_tableID` int(11) NOT NULL,
  `software_order_detail_id` int(10) UNSIGNED NOT NULL,
  `product_table_id` int(11) NOT NULL,
  `quantity_sold` int(11) NOT NULL,
  `quantity_instock` int(11) NOT NULL,
  `quantity_ordered` int(11) NOT NULL,
  `purch_order_id` int(11) NOT NULL,
  `net_amount` float NOT NULL,
  `software_lic_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order_info`
--

CREATE TABLE `purchase_order_info` (
  `purch_order_table_id` int(11) UNSIGNED NOT NULL,
  `software_purchase_order_id` int(10) UNSIGNED NOT NULL,
  `purch_order_id` int(11) NOT NULL,
  `purch_order_date` varchar(150) NOT NULL,
  `purch_order_amount` float NOT NULL,
  `numb_of_prod` int(11) NOT NULL,
  `inventoryDays` int(11) NOT NULL,
  `comp_table_id` int(11) NOT NULL,
  `order_status` varchar(100) NOT NULL,
  `creating_user_id` int(11) NOT NULL,
  `creating_date` varchar(30) NOT NULL,
  `creating_time` varchar(30) NOT NULL,
  `update_user_id` int(11) NOT NULL,
  `update_date` varchar(30) NOT NULL,
  `update_time` varchar(30) NOT NULL,
  `software_lic_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_return`
--

CREATE TABLE `purchase_return` (
  `returnTable_id` int(10) UNSIGNED NOT NULL,
  `software_purchase_return_id` int(10) UNSIGNED NOT NULL,
  `return_id` int(11) NOT NULL,
  `purchInvoNumb` varchar(30) NOT NULL,
  `company_table_id` int(11) UNSIGNED DEFAULT NULL,
  `supplier_id` int(11) NOT NULL,
  `return_total_price` float NOT NULL,
  `return_gross_price` float NOT NULL,
  `entry_date` varchar(20) NOT NULL,
  `entry_time` varchar(20) NOT NULL,
  `update_date` varchar(20) NOT NULL,
  `update_time` varchar(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comments` varchar(200) DEFAULT NULL,
  `status` varchar(20) NOT NULL,
  `software_lic_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_return_detail`
--

CREATE TABLE `purchase_return_detail` (
  `return_detail_id` int(10) UNSIGNED NOT NULL,
  `software_return_detail_id` int(10) UNSIGNED NOT NULL,
  `return_id` int(11) NOT NULL,
  `prod_id` int(11) NOT NULL,
  `prod_batch` varchar(15) NOT NULL,
  `prod_quant` int(11) NOT NULL,
  `bonus_quant` int(11) NOT NULL,
  `discount_amount` float NOT NULL,
  `total_amount` float NOT NULL,
  `unit` varchar(20) NOT NULL,
  `return_reason` varchar(100) NOT NULL,
  `software_lic_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_return_info_detail`
--

CREATE TABLE `purchase_return_info_detail` (
  `return_id` int(11) NOT NULL,
  `software_return_id` int(10) UNSIGNED NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `purch_invo_num` varchar(20) NOT NULL,
  `return_date` varchar(20) NOT NULL,
  `prod_id` int(11) NOT NULL,
  `return_quant` int(11) NOT NULL,
  `discount_percent` float NOT NULL,
  `bonus_quantity` int(11) NOT NULL,
  `total_amount` float NOT NULL,
  `batch_no` varchar(20) NOT NULL,
  `gross_amount` float NOT NULL,
  `software_lic_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purch_return_info`
--

CREATE TABLE `purch_return_info` (
  `purch_return_no` int(11) NOT NULL,
  `software_purch_return_no` int(10) UNSIGNED NOT NULL,
  `purch_invo_num` varchar(20) NOT NULL,
  `return_date` varchar(20) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `total_amount` float NOT NULL,
  `discount_amount` float NOT NULL,
  `software_lic_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `salesman_designated_cities`
--

CREATE TABLE `salesman_designated_cities` (
  `salesman_designated_id` int(10) UNSIGNED NOT NULL,
  `software_salesman_designated_id` int(10) UNSIGNED NOT NULL,
  `salesman_table_id` int(10) UNSIGNED NOT NULL,
  `designated_city_id` int(10) UNSIGNED NOT NULL,
  `start_date` varchar(20) NOT NULL,
  `end_date` varchar(20) NOT NULL,
  `designated_status` varchar(50) NOT NULL,
  `software_lic_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `salesman_designated_cities`
--

INSERT INTO `salesman_designated_cities` (`salesman_designated_id`, `software_salesman_designated_id`, `salesman_table_id`, `designated_city_id`, `start_date`, `end_date`, `designated_status`, `software_lic_id`) VALUES
(37, 37, 16, 1, '30/Jan/2022', '', 'Active', 1),
(56, 38, 12346, 1, '22/Feb/2022', '', 'Active', 1),
(58, 39, 18, 1, '27/Feb/2022', '', 'Active', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sale_invoice_print_info`
--

CREATE TABLE `sale_invoice_print_info` (
  `sale_print_id` int(10) UNSIGNED NOT NULL,
  `software_sale_print_id` int(10) UNSIGNED NOT NULL,
  `header_1` varchar(100) NOT NULL,
  `header_2` varchar(100) NOT NULL,
  `header_3` varchar(100) NOT NULL,
  `header_4` varchar(100) NOT NULL,
  `header_5` varchar(100) NOT NULL,
  `owner_ids` int(11) NOT NULL,
  `owner_name` varchar(50) NOT NULL,
  `father_name` varchar(50) NOT NULL,
  `owner_cnic` varchar(20) NOT NULL,
  `owner_country` varchar(50) NOT NULL,
  `business_name` varchar(100) NOT NULL,
  `business_address` varchar(250) NOT NULL,
  `business_city` varchar(150) NOT NULL,
  `footer_1` varchar(1500) NOT NULL,
  `footer_2` varchar(200) NOT NULL,
  `footer_3` varchar(200) NOT NULL,
  `footer_4` varchar(200) NOT NULL,
  `creating_user_id` int(11) NOT NULL,
  `creating_date` varchar(20) NOT NULL,
  `creating_time` varchar(20) NOT NULL,
  `update_user_id` int(11) NOT NULL,
  `update_date` varchar(20) NOT NULL,
  `update_time` varchar(20) NOT NULL,
  `status` varchar(20) NOT NULL,
  `software_lic_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sale_invoice_print_info`
--

INSERT INTO `sale_invoice_print_info` (`sale_print_id`, `software_sale_print_id`, `header_1`, `header_2`, `header_3`, `header_4`, `header_5`, `owner_ids`, `owner_name`, `father_name`, `owner_cnic`, `owner_country`, `business_name`, `business_address`, `business_city`, `footer_1`, `footer_2`, `footer_3`, `footer_4`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`, `status`, `software_lic_id`) VALUES
(22, 2, '[yguh]', '[yguh]', '[yguh]', '[yguh]', '[yguh]', 1, 'fyguhij', 'tfgyuhji', 'tfgyhujitfygvbhuj', 'tfgyhujtfgyhu', 'tfgyhu', 'tfgyuh', '[tyg]', 'tfgyhu', 'tfgyuh', 'tyg', 'tyg', 1, '28/Aug/2021', '12:08 PM', 1, '28/Aug/2021', '12:08 PM', 'Active', 1);

-- --------------------------------------------------------

--
-- Table structure for table `software_accounts`
--

CREATE TABLE `software_accounts` (
  `software_lic_id` int(10) UNSIGNED NOT NULL,
  `license_no` varchar(30) NOT NULL,
  `owner_name` varchar(200) NOT NULL,
  `owner_contact` varchar(20) NOT NULL,
  `owner_cnic` varchar(20) NOT NULL,
  `owner_address` varchar(250) NOT NULL,
  `registration_date` varchar(20) NOT NULL,
  `license_renew_on` varchar(20) NOT NULL,
  `valid_till` varchar(20) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subarea_info`
--

CREATE TABLE `subarea_info` (
  `subarea_id` int(10) UNSIGNED NOT NULL,
  `software_subarea_id` int(10) UNSIGNED NOT NULL,
  `area_id` int(10) UNSIGNED NOT NULL,
  `sub_area_name` varchar(250) NOT NULL,
  `subarea_status` varchar(50) NOT NULL,
  `entry_date` varchar(20) NOT NULL,
  `entry_time` varchar(20) NOT NULL,
  `update_date` varchar(20) NOT NULL,
  `update_time` varchar(20) NOT NULL,
  `user_id` int(10) NOT NULL,
  `software_lic_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `supplier_company`
--

CREATE TABLE `supplier_company` (
  `supplier_company_id` int(10) UNSIGNED NOT NULL,
  `software_supplier_company_id` int(10) UNSIGNED NOT NULL,
  `supplier_id` int(10) UNSIGNED NOT NULL,
  `company_id` int(10) UNSIGNED NOT NULL,
  `updated_date` varchar(20) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `status` varchar(20) NOT NULL,
  `software_lic_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier_company`
--

INSERT INTO `supplier_company` (`supplier_company_id`, `software_supplier_company_id`, `supplier_id`, `company_id`, `updated_date`, `user_id`, `status`, `software_lic_id`) VALUES
(42, 3, 1, 1, '30/Nov/2021', 1, 'Active', 1),
(43, 2, 2, 2, '29/Nov/2021', 1, 'Active', 1);

-- --------------------------------------------------------

--
-- Table structure for table `supplier_info`
--

CREATE TABLE `supplier_info` (
  `supplier_table_id` int(10) UNSIGNED NOT NULL,
  `software_supplier_id` int(10) UNSIGNED NOT NULL,
  `supplier_id` int(10) UNSIGNED DEFAULT NULL,
  `supplier_name` varchar(100) NOT NULL,
  `supplier_email` varchar(100) NOT NULL,
  `supplier_contact` varchar(60) NOT NULL,
  `contact_person` varchar(200) NOT NULL,
  `supplier_address` varchar(200) NOT NULL,
  `supplier_city` varchar(200) NOT NULL,
  `supplier_status` varchar(20) NOT NULL,
  `creating_user_id` int(10) UNSIGNED NOT NULL,
  `creating_date` varchar(30) NOT NULL,
  `creating_time` varchar(30) NOT NULL,
  `update_user_id` int(10) UNSIGNED NOT NULL,
  `update_date` varchar(30) NOT NULL,
  `update_time` varchar(30) NOT NULL,
  `software_lic_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier_info`
--

INSERT INTO `supplier_info` (`supplier_table_id`, `software_supplier_id`, `supplier_id`, `supplier_name`, `supplier_email`, `supplier_contact`, `contact_person`, `supplier_address`, `supplier_city`, `supplier_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`, `software_lic_id`) VALUES
(41, 1, 1, 'High Q', '', '', '', 'Karachi', 'Karachi', 'Active', 1, '29/Nov/2021', '11:52 AM', 1, '30/Nov/2021', '11:50 AM', 1),
(42, 2, 2, 'Vida Laboratories', '', '', '', '', '', 'Active', 1, '29/Nov/2021', '11:52 AM', 1, '29/Nov/2021', '11:52 AM', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sync_tracking`
--

CREATE TABLE `sync_tracking` (
  `sync_id` int(11) UNSIGNED NOT NULL,
  `software_sync_id` int(11) DEFAULT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `dealer_info` int(5) DEFAULT NULL,
  `area_info` int(5) DEFAULT NULL,
  `product_info` int(5) DEFAULT NULL,
  `discount_info` int(5) DEFAULT NULL,
  `bonus_info` int(5) DEFAULT NULL,
  `software_lic_id` int(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `system_settings`
--

CREATE TABLE `system_settings` (
  `setting_id` int(10) NOT NULL,
  `software_setting_id` int(10) UNSIGNED NOT NULL,
  `setting_name` varchar(50) NOT NULL,
  `setting_value` varchar(1500) NOT NULL,
  `software_lic_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `system_settings`
--

INSERT INTO `system_settings` (`setting_id`, `software_setting_id`, `setting_name`, `setting_value`, `software_lic_id`) VALUES
(29, 1, 'batch_policy', 'Entry FIFO', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_accounts`
--

CREATE TABLE `user_accounts` (
  `account_id` int(10) NOT NULL,
  `software_account_id` int(10) UNSIGNED NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_password` varchar(300) NOT NULL,
  `user_rights` int(10) NOT NULL,
  `reg_date` varchar(50) NOT NULL,
  `current_status` varchar(30) NOT NULL,
  `user_status` varchar(50) NOT NULL,
  `send_status_computer` varchar(20) DEFAULT NULL,
  `software_lic_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_accounts`
--

INSERT INTO `user_accounts` (`account_id`, `software_account_id`, `user_name`, `user_id`, `user_password`, `user_rights`, `reg_date`, `current_status`, `user_status`, `send_status_computer`, `software_lic_id`) VALUES
(64, 26, 'Saffi', 16, '123', 7, '27/Feb/2022', 'off', 'Pending', 'Sent', 1),
(71, 27, 'Saffi', 18, '123', 7, '27/Feb/2022', 'off', 'Approved', NULL, 1),
(72, 12, 'Ali Hassan', 1, '123', 6, '03/Mar/2022', 'off', 'Pending', 'Sent', 1),
(73, 1, 'Saffi', 1, '123', 1, '10/Feb/2020', 'off', 'Approved', NULL, 1),
(74, 13, 'abdul', 16, '123', 7, '15/Mar/2022', 'off', 'Pending', 'Sent', 1),
(75, 14, 'Tahir', 1, '123', 6, '15/Mar/2022', 'off', 'Pending', 'Sent', 1),
(76, 16, 'use', 18, '123', 32, '15/Mar/2022', 'off', 'Pending', 'Sent', 1),
(77, 17, 'Saffi', 18, '123', 32, '29/Mar/2022', 'off', 'Pending', 'Sent', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE `user_info` (
  `user_table_id` int(10) UNSIGNED NOT NULL,
  `software_user_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `user_name` varchar(200) NOT NULL,
  `user_contact` varchar(60) NOT NULL,
  `user_address` varchar(250) NOT NULL,
  `user_cnic` varchar(50) NOT NULL,
  `user_type` varchar(20) NOT NULL,
  `user_image` varchar(300) NOT NULL,
  `user_status` varchar(50) NOT NULL,
  `creating_user_id` int(10) UNSIGNED NOT NULL,
  `creating_date` varchar(20) NOT NULL,
  `creating_time` varchar(20) NOT NULL,
  `update_user_id` int(10) UNSIGNED NOT NULL,
  `update_date` varchar(20) NOT NULL,
  `update_time` varchar(20) NOT NULL,
  `software_lic_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` (`user_table_id`, `software_user_id`, `user_id`, `user_name`, `user_contact`, `user_address`, `user_cnic`, `user_type`, `user_image`, `user_status`, `creating_user_id`, `creating_date`, `creating_time`, `update_user_id`, `update_date`, `update_time`, `software_lic_id`) VALUES
(16, 1, 2, 'Saffi', '03526555', 'Gujrat', '34201-5525252-5', 'Admin', '', 'Active', 1, '08/Feb/2021', '03:35 PM', 1, '08/Feb/2021', '03:35 PM', 1),
(17, 16, 0, 'Ali', '123456', 'Address of Ali', '34201-5389259-3', 'Data Manager', 'C:/Users/Safi Ullah/Desktop/53110532_2074856822591867_7000092936318746624_n.jpg', 'Active', 1, '30/Jan/2022', '07:17 PM', 1, '30/Jan/2022', '09:04 PM', 1),
(48, 12346, 0, 'Android Test 1', '1234', 'Android Address 1', '4343324', 'Data Manager', '', 'Active', 1, '22/Feb/2022', '04:08 PM', 1, '22/Feb/2022', '04:08 PM', 1),
(49, 17, 0, 'Android Test 1', '35443', 'afwefe', '6565', 'Data Manager', 'null', 'Active', 1, '23/Feb/2022', '12:03 AM', 1, '23/Feb/2022', '12:06 AM', 1),
(50, 18, 0, 'Saffi Test', '123456', 'Saffi Address', '4234231151', 'Data Manager', 'null', 'Active', 1, '27/Feb/2022', '12:32 AM', 1, '27/Feb/2022', '12:34 AM', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_rights`
--

CREATE TABLE `user_rights` (
  `rights_id` int(11) NOT NULL,
  `software_rights_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` varchar(20) NOT NULL,
  `create_record` tinyint(4) NOT NULL,
  `read_record` tinyint(4) NOT NULL,
  `edit_record` tinyint(4) NOT NULL,
  `delete_record` tinyint(4) NOT NULL,
  `manage_stock` tinyint(4) NOT NULL,
  `manage_cash` tinyint(4) NOT NULL,
  `manage_settings` tinyint(4) NOT NULL,
  `mobile_app` tinyint(4) NOT NULL,
  `software_lic_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_rights`
--

INSERT INTO `user_rights` (`rights_id`, `software_rights_id`, `user_id`, `create_record`, `read_record`, `edit_record`, `delete_record`, `manage_stock`, `manage_cash`, `manage_settings`, `mobile_app`, `software_lic_id`) VALUES
(6, 1, '1', 0, 0, 0, 0, 0, 0, 0, 1, 1),
(7, 6, '16', 1, 1, 1, 1, 0, 0, 0, 1, 1),
(24, 0, '12345', 0, 0, 0, 0, 0, 0, 0, 1, 1),
(26, 0, '12346', 0, 0, 0, 0, 0, 0, 0, 1, 1),
(27, 0, '17', 0, 0, 0, 0, 0, 0, 0, 1, 1),
(32, 7, '18', 0, 0, 0, 0, 0, 0, 0, 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `area_info`
--
ALTER TABLE `area_info`
  ADD PRIMARY KEY (`area_table_id`);

--
-- Indexes for table `batchwise_stock`
--
ALTER TABLE `batchwise_stock`
  ADD PRIMARY KEY (`batch_stock_id`);

--
-- Indexes for table `bonus_policy`
--
ALTER TABLE `bonus_policy`
  ADD PRIMARY KEY (`bonus_policyID`);

--
-- Indexes for table `cash_collected`
--
ALTER TABLE `cash_collected`
  ADD PRIMARY KEY (`cash_collection_id`);

--
-- Indexes for table `city_info`
--
ALTER TABLE `city_info`
  ADD PRIMARY KEY (`city_table_id`);

--
-- Indexes for table `company_alternate_contacts`
--
ALTER TABLE `company_alternate_contacts`
  ADD PRIMARY KEY (`contact_id`);

--
-- Indexes for table `company_info`
--
ALTER TABLE `company_info`
  ADD PRIMARY KEY (`company_table_id`);

--
-- Indexes for table `company_overall_record`
--
ALTER TABLE `company_overall_record`
  ADD PRIMARY KEY (`company_overall_id`);

--
-- Indexes for table `company_payments`
--
ALTER TABLE `company_payments`
  ADD PRIMARY KEY (`payment_id`);

--
-- Indexes for table `dealer_day_summary`
--
ALTER TABLE `dealer_day_summary`
  ADD PRIMARY KEY (`dealer_summary_id`);

--
-- Indexes for table `dealer_gps_location`
--
ALTER TABLE `dealer_gps_location`
  ADD PRIMARY KEY (`dealer_loc_id`);

--
-- Indexes for table `dealer_info`
--
ALTER TABLE `dealer_info`
  ADD PRIMARY KEY (`dealer_table_id`);

--
-- Indexes for table `dealer_overall_record`
--
ALTER TABLE `dealer_overall_record`
  ADD PRIMARY KEY (`dealer_overall_id`);

--
-- Indexes for table `dealer_payments`
--
ALTER TABLE `dealer_payments`
  ADD PRIMARY KEY (`payment_id`);

--
-- Indexes for table `dealer_types`
--
ALTER TABLE `dealer_types`
  ADD PRIMARY KEY (`dealer_type_ID`);

--
-- Indexes for table `discount_policy`
--
ALTER TABLE `discount_policy`
  ADD PRIMARY KEY (`disc_policyID`);

--
-- Indexes for table `district_info`
--
ALTER TABLE `district_info`
  ADD PRIMARY KEY (`district_table_id`);

--
-- Indexes for table `groups_info`
--
ALTER TABLE `groups_info`
  ADD PRIMARY KEY (`group_table_id`);

--
-- Indexes for table `mobile_curr_locations`
--
ALTER TABLE `mobile_curr_locations`
  ADD PRIMARY KEY (`curr_loc_id`);

--
-- Indexes for table `mobile_gps_location`
--
ALTER TABLE `mobile_gps_location`
  ADD PRIMARY KEY (`mob_loc_id`);

--
-- Indexes for table `order_gps_location`
--
ALTER TABLE `order_gps_location`
  ADD PRIMARY KEY (`order_loc_id`);

--
-- Indexes for table `order_info`
--
ALTER TABLE `order_info`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `order_info_detailed`
--
ALTER TABLE `order_info_detailed`
  ADD PRIMARY KEY (`detail_id`);

--
-- Indexes for table `order_return`
--
ALTER TABLE `order_return`
  ADD PRIMARY KEY (`return_id`);

--
-- Indexes for table `order_return_detail`
--
ALTER TABLE `order_return_detail`
  ADD PRIMARY KEY (`return_detail_id`);

--
-- Indexes for table `order_status_info`
--
ALTER TABLE `order_status_info`
  ADD PRIMARY KEY (`order_status_id`);

--
-- Indexes for table `products_stock`
--
ALTER TABLE `products_stock`
  ADD PRIMARY KEY (`ProdStockID`);

--
-- Indexes for table `product_info`
--
ALTER TABLE `product_info`
  ADD PRIMARY KEY (`product_table_id`);

--
-- Indexes for table `product_overall_record`
--
ALTER TABLE `product_overall_record`
  ADD PRIMARY KEY (`overall_record_id`);

--
-- Indexes for table `purchase_info`
--
ALTER TABLE `purchase_info`
  ADD PRIMARY KEY (`purchase_id`);

--
-- Indexes for table `purchase_info_detail`
--
ALTER TABLE `purchase_info_detail`
  ADD PRIMARY KEY (`purchase_info_detail_id`);

--
-- Indexes for table `purchase_order_detail`
--
ALTER TABLE `purchase_order_detail`
  ADD PRIMARY KEY (`order_detail_tableID`);

--
-- Indexes for table `purchase_order_info`
--
ALTER TABLE `purchase_order_info`
  ADD PRIMARY KEY (`purch_order_table_id`);

--
-- Indexes for table `purchase_return`
--
ALTER TABLE `purchase_return`
  ADD PRIMARY KEY (`returnTable_id`);

--
-- Indexes for table `purchase_return_detail`
--
ALTER TABLE `purchase_return_detail`
  ADD PRIMARY KEY (`return_detail_id`);

--
-- Indexes for table `salesman_designated_cities`
--
ALTER TABLE `salesman_designated_cities`
  ADD PRIMARY KEY (`salesman_designated_id`);

--
-- Indexes for table `sale_invoice_print_info`
--
ALTER TABLE `sale_invoice_print_info`
  ADD PRIMARY KEY (`sale_print_id`);

--
-- Indexes for table `software_accounts`
--
ALTER TABLE `software_accounts`
  ADD PRIMARY KEY (`software_lic_id`);

--
-- Indexes for table `subarea_info`
--
ALTER TABLE `subarea_info`
  ADD PRIMARY KEY (`subarea_id`);

--
-- Indexes for table `supplier_company`
--
ALTER TABLE `supplier_company`
  ADD PRIMARY KEY (`supplier_company_id`);

--
-- Indexes for table `supplier_info`
--
ALTER TABLE `supplier_info`
  ADD PRIMARY KEY (`supplier_table_id`);

--
-- Indexes for table `sync_tracking`
--
ALTER TABLE `sync_tracking`
  ADD PRIMARY KEY (`sync_id`);

--
-- Indexes for table `system_settings`
--
ALTER TABLE `system_settings`
  ADD PRIMARY KEY (`setting_id`);

--
-- Indexes for table `user_accounts`
--
ALTER TABLE `user_accounts`
  ADD PRIMARY KEY (`account_id`);

--
-- Indexes for table `user_info`
--
ALTER TABLE `user_info`
  ADD PRIMARY KEY (`user_table_id`);

--
-- Indexes for table `user_rights`
--
ALTER TABLE `user_rights`
  ADD PRIMARY KEY (`rights_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `area_info`
--
ALTER TABLE `area_info`
  MODIFY `area_table_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `batchwise_stock`
--
ALTER TABLE `batchwise_stock`
  MODIFY `batch_stock_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `bonus_policy`
--
ALTER TABLE `bonus_policy`
  MODIFY `bonus_policyID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cash_collected`
--
ALTER TABLE `cash_collected`
  MODIFY `cash_collection_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `city_info`
--
ALTER TABLE `city_info`
  MODIFY `city_table_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `company_alternate_contacts`
--
ALTER TABLE `company_alternate_contacts`
  MODIFY `contact_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `company_info`
--
ALTER TABLE `company_info`
  MODIFY `company_table_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `company_overall_record`
--
ALTER TABLE `company_overall_record`
  MODIFY `company_overall_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `company_payments`
--
ALTER TABLE `company_payments`
  MODIFY `payment_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `dealer_day_summary`
--
ALTER TABLE `dealer_day_summary`
  MODIFY `dealer_summary_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `dealer_gps_location`
--
ALTER TABLE `dealer_gps_location`
  MODIFY `dealer_loc_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `dealer_info`
--
ALTER TABLE `dealer_info`
  MODIFY `dealer_table_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29887;

--
-- AUTO_INCREMENT for table `dealer_overall_record`
--
ALTER TABLE `dealer_overall_record`
  MODIFY `dealer_overall_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `dealer_payments`
--
ALTER TABLE `dealer_payments`
  MODIFY `payment_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `dealer_types`
--
ALTER TABLE `dealer_types`
  MODIFY `dealer_type_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `discount_policy`
--
ALTER TABLE `discount_policy`
  MODIFY `disc_policyID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `district_info`
--
ALTER TABLE `district_info`
  MODIFY `district_table_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `groups_info`
--
ALTER TABLE `groups_info`
  MODIFY `group_table_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mobile_curr_locations`
--
ALTER TABLE `mobile_curr_locations`
  MODIFY `curr_loc_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT for table `mobile_gps_location`
--
ALTER TABLE `mobile_gps_location`
  MODIFY `mob_loc_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `order_gps_location`
--
ALTER TABLE `order_gps_location`
  MODIFY `order_loc_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_info`
--
ALTER TABLE `order_info`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `order_info_detailed`
--
ALTER TABLE `order_info_detailed`
  MODIFY `detail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `order_return`
--
ALTER TABLE `order_return`
  MODIFY `return_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_return_detail`
--
ALTER TABLE `order_return_detail`
  MODIFY `return_detail_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `order_status_info`
--
ALTER TABLE `order_status_info`
  MODIFY `order_status_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products_stock`
--
ALTER TABLE `products_stock`
  MODIFY `ProdStockID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `product_info`
--
ALTER TABLE `product_info`
  MODIFY `product_table_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4621;

--
-- AUTO_INCREMENT for table `product_overall_record`
--
ALTER TABLE `product_overall_record`
  MODIFY `overall_record_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `purchase_info`
--
ALTER TABLE `purchase_info`
  MODIFY `purchase_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `purchase_info_detail`
--
ALTER TABLE `purchase_info_detail`
  MODIFY `purchase_info_detail_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `purchase_order_detail`
--
ALTER TABLE `purchase_order_detail`
  MODIFY `order_detail_tableID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `purchase_order_info`
--
ALTER TABLE `purchase_order_info`
  MODIFY `purch_order_table_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `purchase_return`
--
ALTER TABLE `purchase_return`
  MODIFY `returnTable_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `purchase_return_detail`
--
ALTER TABLE `purchase_return_detail`
  MODIFY `return_detail_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `salesman_designated_cities`
--
ALTER TABLE `salesman_designated_cities`
  MODIFY `salesman_designated_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `sale_invoice_print_info`
--
ALTER TABLE `sale_invoice_print_info`
  MODIFY `sale_print_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `software_accounts`
--
ALTER TABLE `software_accounts`
  MODIFY `software_lic_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `subarea_info`
--
ALTER TABLE `subarea_info`
  MODIFY `subarea_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `supplier_company`
--
ALTER TABLE `supplier_company`
  MODIFY `supplier_company_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `supplier_info`
--
ALTER TABLE `supplier_info`
  MODIFY `supplier_table_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `sync_tracking`
--
ALTER TABLE `sync_tracking`
  MODIFY `sync_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `system_settings`
--
ALTER TABLE `system_settings`
  MODIFY `setting_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `user_accounts`
--
ALTER TABLE `user_accounts`
  MODIFY `account_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- AUTO_INCREMENT for table `user_info`
--
ALTER TABLE `user_info`
  MODIFY `user_table_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `user_rights`
--
ALTER TABLE `user_rights`
  MODIFY `rights_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
