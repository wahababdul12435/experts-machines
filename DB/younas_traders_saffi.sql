-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 23, 2020 at 11:29 AM
-- Server version: 5.7.19
-- PHP Version: 7.0.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `younas_traders`
--

-- --------------------------------------------------------

--
-- Table structure for table `area_info`
--

DROP TABLE IF EXISTS `area_info`;
CREATE TABLE IF NOT EXISTS `area_info` (
  `area_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `area_name` varchar(250) NOT NULL,
  `area_status` varchar(50) NOT NULL,
  PRIMARY KEY (`area_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `area_info`
--

INSERT INTO `area_info` (`area_id`, `area_name`, `area_status`) VALUES
(1, 'Gujrat Center', 'Active'),
(2, 'Main Area', 'Active'),
(4, 'Gujrat Center 1 Test', 'In Active');

-- --------------------------------------------------------

--
-- Table structure for table `batchwise_stock`
--

DROP TABLE IF EXISTS `batchwise_stock`;
CREATE TABLE IF NOT EXISTS `batchwise_stock` (
  `prod_id` int(11) NOT NULL,
  `batch_no` varchar(50) NOT NULL,
  `quantity` int(11) NOT NULL,
  `bonus` int(11) NOT NULL,
  `batch_expiry` varchar(20) NOT NULL,
  `entry_date` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `batchwise_stock`
--

INSERT INTO `batchwise_stock` (`prod_id`, `batch_no`, `quantity`, `bonus`, `batch_expiry`, `entry_date`) VALUES
(2, 'RAW', 200, 20, '2020-03-19', '2020-03-19'),
(456, 'YAT', 100, 5, '2020-03-19', '2020-03-19'),
(3, 'IOP', 40, 4, '2020-03-19', '2020-03-19'),
(1, 'LKJ', 30, 3, '2020-03-19', '2020-03-19'),
(1, 'UYT', 20, 2, '2020-03-21', '2020-03-21'),
(1, 'FAS', 50, 0, '2020-03-21', '2020-03-21'),
(3, 'BCVX', 10, 0, '2020-03-21', '2020-03-21'),
(3, 'BXCC', 10, 0, '2020-03-21', '2020-03-21');

-- --------------------------------------------------------

--
-- Table structure for table `company_alternate_contacts`
--

DROP TABLE IF EXISTS `company_alternate_contacts`;
CREATE TABLE IF NOT EXISTS `company_alternate_contacts` (
  `contact_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_id` int(10) UNSIGNED NOT NULL,
  `contact_name` varchar(150) NOT NULL,
  `contact_number` varchar(60) NOT NULL,
  PRIMARY KEY (`contact_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `company_groups`
--

DROP TABLE IF EXISTS `company_groups`;
CREATE TABLE IF NOT EXISTS `company_groups` (
  `group_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_id` int(10) UNSIGNED NOT NULL,
  `group_name` varchar(200) NOT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_groups`
--

INSERT INTO `company_groups` (`group_id`, `company_id`, `group_name`) VALUES
(1, 2, 'Group 2 1'),
(2, 2, 'Group 2 2'),
(3, 3, 'Group 3 1'),
(4, 3, 'Group 3 2'),
(5, 2, 'Group 2 3');

-- --------------------------------------------------------

--
-- Table structure for table `company_info`
--

DROP TABLE IF EXISTS `company_info`;
CREATE TABLE IF NOT EXISTS `company_info` (
  `company_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_name` varchar(200) NOT NULL,
  `company_address` varchar(300) NOT NULL,
  `company_contact` varchar(100) NOT NULL,
  `company_email` varchar(100) NOT NULL,
  `company_status` varchar(50) NOT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=MyISAM AUTO_INCREMENT=340 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_info`
--

INSERT INTO `company_info` (`company_id`, `company_name`, `company_address`, `company_contact`, `company_email`, `company_status`) VALUES
(1, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(2, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(3, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(11, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(5, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(6, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(7, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(8, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(9, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(12, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(13, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(14, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(15, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(16, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(17, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(18, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(19, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(20, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(21, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(22, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(23, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(24, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(25, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(26, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(27, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(28, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(29, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(30, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(31, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(32, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(33, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(34, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(35, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(36, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(37, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(38, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(39, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(40, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(41, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(42, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(43, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(44, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(45, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(46, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(47, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(48, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(49, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(50, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(51, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(52, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(53, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(54, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(55, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(56, 'Company Name 6', 'Company Address 6 Test of 56', 'Company Contact 6', 'Company Email 6', 'Active'),
(57, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(58, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(59, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(60, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(61, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(62, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(63, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(64, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(65, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(66, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(67, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(68, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(69, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(70, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(71, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(72, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(73, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(74, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(75, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(76, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(77, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(78, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(79, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(80, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(81, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(82, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(83, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(84, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(85, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(86, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(87, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(88, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(89, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(90, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(91, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(92, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(93, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(94, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(95, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(96, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(97, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(98, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(99, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(100, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(101, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(102, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(103, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(104, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(105, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(106, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(107, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(108, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(109, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(110, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(111, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(112, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(113, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(114, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(115, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(116, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(117, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(118, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(119, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(120, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(121, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(122, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(123, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(124, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(125, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(126, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(127, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(128, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(129, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(130, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(131, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(132, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(133, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(134, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(135, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(136, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(137, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(138, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(139, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(140, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(141, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(142, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(143, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(144, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(145, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(146, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(147, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(148, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(149, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(150, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(151, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(152, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(153, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(154, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(155, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(156, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(157, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(158, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(159, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(160, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(161, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(162, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(163, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(164, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(165, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(166, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(167, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(168, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(169, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(170, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(171, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(172, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(173, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(174, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(175, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(176, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(177, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(178, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(179, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(180, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(181, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(182, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(183, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(184, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(185, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(186, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(187, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(188, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(189, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(190, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(191, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(192, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(193, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(194, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(195, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(196, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(197, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(198, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(199, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(200, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(201, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(202, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(203, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(204, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(205, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(206, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(207, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(208, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(209, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(210, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(211, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(212, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(213, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(214, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(215, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(216, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(217, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(218, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(219, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(220, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(221, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(222, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(223, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(224, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(225, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(226, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(227, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(228, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(229, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(230, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(231, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(232, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(233, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(234, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(235, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(236, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(237, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(238, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(239, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(240, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(241, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(242, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(243, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(244, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(245, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(246, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(247, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(248, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(249, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(250, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(251, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(252, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', ''),
(253, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(254, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(255, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(256, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(257, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(258, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(259, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(260, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(261, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(262, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(263, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(264, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(265, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(266, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(267, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(268, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(269, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(270, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(271, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(272, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(273, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(274, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(275, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(276, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(277, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(278, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(279, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(280, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(281, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(282, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(283, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(284, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(285, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(286, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(287, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(288, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(289, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(290, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(291, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(292, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(293, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(294, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(295, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(296, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(297, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(298, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(299, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(300, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(301, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(302, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(303, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(304, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(305, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(306, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(307, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(308, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(309, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(310, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(311, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(312, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(313, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(314, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(315, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(316, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(317, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(318, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(319, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(320, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(321, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(322, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(323, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(324, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(325, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(326, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(327, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(328, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(329, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(330, 'Company Name 5', 'Company Address 5', 'Company Contact 5', 'Company Email 5', 'Active'),
(331, 'Company Name 6', 'Company Address 6 Test', 'Company Contact 6', 'Company Email 6', 'Active'),
(332, 'Company Name 7', 'Company Address 7', 'Company Contact 7', 'Company Email 7', 'Active'),
(333, 'Company Name 8', 'Company Address 8', 'Company Contact 8', 'Company Email 8', 'Active'),
(334, 'Company Name 9', 'Company Address 9', 'Company Contact 9', 'Company Email 9', 'Active'),
(335, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', 'Active'),
(336, 'Company Name 2', 'Company Address 2', 'Company Contact 2', 'Company Email 2', 'Active'),
(337, 'Company Name 3', 'Company Address 3', 'Company Contact 3', 'Company Email 3', 'In Active'),
(338, 'Company Name 1', 'Company Address 1', 'Company Contact 1', 'Company Email 1', ''),
(339, '', '', '', '', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `dealer_gps_location`
--

DROP TABLE IF EXISTS `dealer_gps_location`;
CREATE TABLE IF NOT EXISTS `dealer_gps_location` (
  `dealer_loc_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `latitude` varchar(25) NOT NULL,
  `longitude` varchar(25) NOT NULL,
  `loc_name` varchar(150) NOT NULL,
  PRIMARY KEY (`dealer_loc_id`),
  UNIQUE KEY `dealer_id` (`dealer_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dealer_gps_location`
--

INSERT INTO `dealer_gps_location` (`dealer_loc_id`, `dealer_id`, `latitude`, `longitude`, `loc_name`) VALUES
(1, 1, '32.7879', '74.17539833333333', 'Gujrat, Punjab, Pakistan'),
(2, 2, '32.587898333333335', '74.14539833333333', 'Sook Kalan Rd, Sook Kalan, Gujrat, Punjab 50770, Pakistan');

-- --------------------------------------------------------

--
-- Table structure for table `dealer_info`
--

DROP TABLE IF EXISTS `dealer_info`;
CREATE TABLE IF NOT EXISTS `dealer_info` (
  `dealer_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `dealer_area_id` int(10) UNSIGNED NOT NULL,
  `dealer_name` varchar(200) NOT NULL,
  `dealer_contact` varchar(100) NOT NULL,
  `dealer_address` varchar(250) NOT NULL,
  `dealer_type` varchar(30) NOT NULL,
  `dealer_cnic` varchar(30) NOT NULL,
  `dealer_lic_num` varchar(80) NOT NULL,
  `dealer_lic_exp` varchar(50) NOT NULL,
  `dealer_status` varchar(50) NOT NULL,
  PRIMARY KEY (`dealer_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dealer_info`
--

INSERT INTO `dealer_info` (`dealer_id`, `dealer_area_id`, `dealer_name`, `dealer_contact`, `dealer_address`, `dealer_type`, `dealer_cnic`, `dealer_lic_num`, `dealer_lic_exp`, `dealer_status`) VALUES
(1, 2, 'Dealer Name 1 Test', 'Dealer Contact 1 Test', 'Dealer Address 1 Test', 'Retailer', 'Dealer CNIC 1 Test', 'Dealer Lic Num 1 Test', 'Dealer Lic Exp 1 Test', 'In Active'),
(2, 3, 'Dealer Name 1', 'Dealer Contact 1', 'Dealer Address 1', 'Doctor', 'Dealer CNIC 1', 'Dealer Lic Num 1', 'Dealer Lic Exp 1', 'Active'),
(3, 3, 'Dealer Name 1', 'Dealer Contact 1', 'Dealer Address 1', 'Doctor', 'Dealer CNIC 1', 'Dealer Lic Num 1', 'Dealer Lic Exp 1', 'Active'),
(4, 3, 'Dealer Name 1', 'Dealer Contact 1', 'Dealer Address 1', 'Doctor', 'Dealer CNIC 1', 'Dealer Lic Num 1', 'Dealer Lic Exp 1', 'Active'),
(5, 3, 'Dealer Name 1', 'Dealer Contact 1', 'Dealer Address 1', 'Doctor', 'Dealer CNIC 1', 'Dealer Lic Num 1', 'Dealer Lic Exp 1', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `dealer_overall_record`
--

DROP TABLE IF EXISTS `dealer_overall_record`;
CREATE TABLE IF NOT EXISTS `dealer_overall_record` (
  `dealer_overall_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `orders_given` int(10) UNSIGNED NOT NULL,
  `successful_orders` int(10) UNSIGNED NOT NULL,
  `ordered_packets` int(10) UNSIGNED NOT NULL,
  `submitted_packets` int(10) UNSIGNED NOT NULL,
  `ordered_boxes` int(10) UNSIGNED NOT NULL,
  `submitted_boxes` int(10) UNSIGNED NOT NULL,
  `order_price` float NOT NULL,
  `discount_price` float NOT NULL,
  `invoiced_price` float NOT NULL,
  `cash_collected` float NOT NULL,
  `waived_off_price` float NOT NULL,
  `pending_payments` float NOT NULL,
  PRIMARY KEY (`dealer_overall_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dealer_overall_record`
--

INSERT INTO `dealer_overall_record` (`dealer_overall_id`, `dealer_id`, `orders_given`, `successful_orders`, `ordered_packets`, `submitted_packets`, `ordered_boxes`, `submitted_boxes`, `order_price`, `discount_price`, `invoiced_price`, `cash_collected`, `waived_off_price`, `pending_payments`) VALUES
(1, 7, 2, 0, 30, 0, 0, 0, 1547.5, 0, 0, 0, 0, 0),
(2, 1, 8, 1, 141, 50, 0, 0, 16760, 0, 10000, 9200, 0, 800);

-- --------------------------------------------------------

--
-- Table structure for table `dealer_payments`
--

DROP TABLE IF EXISTS `dealer_payments`;
CREATE TABLE IF NOT EXISTS `dealer_payments` (
  `payment_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `cash_collected` float NOT NULL,
  `date` varchar(30) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `status` varchar(30) NOT NULL,
  PRIMARY KEY (`payment_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dealer_payments`
--

INSERT INTO `dealer_payments` (`payment_id`, `dealer_id`, `order_id`, `cash_collected`, `date`, `user_id`, `status`) VALUES
(1, 1, 0, 500, '15/Nov/2019', 4, 'Cash'),
(3, 1, 0, 500, '15/Nov/2019', 4, 'Cash'),
(5, 1, 0, 500, '18/Apr/2020', 1, 'Cash'),
(6, 1, 0, 500, '18/Apr/2020', 1, 'Cash'),
(7, 1, 0, 200, '25/Apr/2020', 1, 'Cash'),
(8, 1, 0, 500, '03/May/2020', 1, 'Cash'),
(9, 1, 0, 500, '04/May/2020', 1, 'Cash');

-- --------------------------------------------------------

--
-- Table structure for table `mobile_curr_locations`
--

DROP TABLE IF EXISTS `mobile_curr_locations`;
CREATE TABLE IF NOT EXISTS `mobile_curr_locations` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `latitude` varchar(25) NOT NULL,
  `longitude` varchar(25) NOT NULL,
  `location_name` varchar(150) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mobile_curr_locations`
--

INSERT INTO `mobile_curr_locations` (`user_id`, `latitude`, `longitude`, `location_name`) VALUES
(1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan'),
(2, '32.651158', '74.010952', 'Younas Traders, Gujrat');

-- --------------------------------------------------------

--
-- Table structure for table `mobile_gps_location`
--

DROP TABLE IF EXISTS `mobile_gps_location`;
CREATE TABLE IF NOT EXISTS `mobile_gps_location` (
  `mob_loc_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `latitude` varchar(20) NOT NULL,
  `longitude` varchar(20) NOT NULL,
  `loc_name` varchar(150) NOT NULL,
  `date` varchar(30) NOT NULL,
  `time` varchar(30) NOT NULL,
  PRIMARY KEY (`mob_loc_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1611 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mobile_gps_location`
--

INSERT INTO `mobile_gps_location` (`mob_loc_id`, `user_id`, `latitude`, `longitude`, `loc_name`, `date`, `time`) VALUES
(1610, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '23/May/2020', '03:39 PM'),
(1609, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '23/May/2020', '03:39 PM'),
(1608, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '23/May/2020', '03:39 PM'),
(1607, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '23/May/2020', '03:38 PM'),
(1606, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '23/May/2020', '03:38 PM'),
(1605, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '23/May/2020', '03:38 PM'),
(1604, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '23/May/2020', '03:38 PM'),
(1603, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '23/May/2020', '03:38 PM'),
(1602, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '23/May/2020', '03:38 PM'),
(1601, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '23/May/2020', '03:37 PM'),
(1600, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '23/May/2020', '03:37 PM'),
(1599, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '23/May/2020', '03:37 PM'),
(1598, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '23/May/2020', '03:35 PM'),
(1597, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '23/May/2020', '03:35 PM'),
(1596, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '23/May/2020', '03:35 PM'),
(1595, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '23/May/2020', '03:35 PM'),
(1594, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '23/May/2020', '03:35 PM'),
(1593, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '23/May/2020', '03:35 PM'),
(1592, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '23/May/2020', '03:34 PM'),
(1591, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '23/May/2020', '03:34 PM'),
(1590, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '23/May/2020', '03:34 PM'),
(1589, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '23/May/2020', '03:34 PM'),
(1587, 1, '0.0', '0.0', '', '23/May/2020', '03:34 PM'),
(1588, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '23/May/2020', '03:34 PM'),
(1586, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '23/May/2020', '03:09 PM'),
(1585, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '23/May/2020', '03:09 PM'),
(1584, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '23/May/2020', '03:09 PM'),
(1583, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '23/May/2020', '03:09 PM'),
(1582, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '23/May/2020', '03:09 PM'),
(1581, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '23/May/2020', '03:08 PM'),
(1580, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '23/May/2020', '03:08 PM'),
(1579, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '23/May/2020', '03:08 PM'),
(1578, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '23/May/2020', '03:08 PM'),
(1577, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '23/May/2020', '03:08 PM'),
(1576, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '23/May/2020', '03:07 PM'),
(1575, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '23/May/2020', '03:07 PM'),
(1574, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '08:04 PM'),
(1573, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '08:03 PM'),
(1572, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '08:03 PM'),
(1571, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '08:03 PM'),
(1570, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '08:03 PM'),
(1569, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '08:03 PM'),
(1568, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '08:03 PM'),
(1567, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '08:02 PM'),
(1566, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '08:01 PM'),
(1565, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '08:01 PM'),
(1564, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '07:58 PM'),
(1563, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '07:56 PM'),
(1562, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '07:56 PM'),
(1561, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '07:56 PM'),
(1560, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '07:56 PM'),
(1559, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '05:36 PM'),
(1558, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '05:36 PM'),
(1557, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '05:36 PM'),
(1556, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '05:35 PM'),
(1555, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '05:35 PM'),
(1554, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '05:35 PM'),
(1553, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '05:35 PM'),
(1552, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '05:34 PM'),
(1551, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '05:34 PM'),
(1550, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '05:30 PM'),
(1549, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '05:30 PM'),
(1548, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '05:29 PM'),
(1547, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '05:29 PM'),
(1546, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '05:29 PM'),
(1545, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '05:29 PM'),
(1544, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '05:29 PM'),
(1543, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '05:29 PM'),
(1542, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '05:28 PM'),
(1541, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '05:28 PM'),
(1540, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '05:16 PM'),
(1539, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '02:41 PM'),
(1538, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '02:41 PM'),
(1537, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '02:41 PM'),
(1536, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '02:41 PM'),
(1535, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '02:41 PM'),
(1534, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '12:57 AM'),
(1533, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '12:56 AM'),
(1532, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '12:56 AM'),
(1531, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '12:56 AM'),
(1530, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '12:56 AM'),
(1529, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '12:56 AM'),
(1528, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '12:56 AM'),
(1527, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '12:55 AM'),
(1526, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '12:55 AM'),
(1525, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '12:55 AM'),
(1524, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '12:55 AM'),
(1523, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '12:55 AM'),
(1522, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '12:50 AM'),
(1521, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '12:49 AM'),
(1520, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '12:49 AM'),
(1519, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '12:49 AM'),
(1518, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '12:49 AM'),
(1517, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '12:49 AM'),
(1516, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '12:49 AM'),
(1515, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '17/May/2020', '12:48 AM'),
(1514, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '11:29 PM'),
(1513, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '11:29 PM'),
(1512, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '11:28 PM'),
(1511, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '11:28 PM'),
(1510, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '11:28 PM'),
(1509, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '10:15 PM'),
(1508, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '10:15 PM'),
(1507, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '10:15 PM'),
(1506, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '10:14 PM'),
(1505, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '10:14 PM'),
(1504, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '10:14 PM'),
(1503, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '10:14 PM'),
(1502, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '10:14 PM'),
(1501, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '10:14 PM'),
(1500, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '10:13 PM'),
(1499, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '10:13 PM'),
(1498, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '10:13 PM'),
(1497, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '10:11 PM'),
(1496, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '10:11 PM'),
(1495, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '10:10 PM'),
(1494, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '10:10 PM'),
(1493, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '10:10 PM'),
(1492, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '10:10 PM'),
(1491, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '10:10 PM'),
(1490, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '10:10 PM'),
(1489, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '09:57 PM'),
(1488, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '09:57 PM'),
(1487, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '09:57 PM'),
(1486, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '09:56 PM'),
(1485, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '09:56 PM'),
(1484, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '09:56 PM'),
(1483, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '09:56 PM'),
(1482, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '09:56 PM'),
(1481, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '09:56 PM'),
(1480, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '09:55 PM'),
(1479, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '09:55 PM'),
(1478, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '09:55 PM'),
(1477, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '09:54 PM'),
(1476, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '09:53 PM'),
(1475, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '09:53 PM'),
(1474, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '09:53 PM'),
(1473, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '09:53 PM'),
(1472, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '09:53 PM'),
(1471, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '09:52 PM'),
(1470, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '09:52 PM'),
(1469, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '09:52 PM'),
(1468, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '09:52 PM'),
(1467, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '09:52 PM'),
(1466, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '09:52 PM'),
(1465, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '09:51 PM'),
(1464, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '09:50 PM'),
(1463, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '09:50 PM'),
(1462, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '09:50 PM'),
(1461, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '09:50 PM'),
(1460, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '09:50 PM'),
(1459, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '09:50 PM'),
(1458, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '09:49 PM'),
(1457, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '09:49 PM'),
(1456, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '09:49 PM'),
(1455, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '09:49 PM'),
(1454, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '09:49 PM'),
(1453, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '08:19 PM'),
(1452, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '08:19 PM'),
(1451, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '08:19 PM'),
(1450, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '08:19 PM'),
(1449, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '08:16 PM'),
(1448, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '08:16 PM'),
(1447, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '08:16 PM'),
(1446, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '08:16 PM'),
(1445, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '08:14 PM'),
(1444, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '08:12 PM'),
(1443, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '08:12 PM'),
(1441, 1, '0.0', '0.0', '', '16/May/2020', '02:58 AM'),
(1442, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '02:58 AM'),
(1440, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '02:58 AM'),
(1439, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '02:58 AM'),
(1438, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '02:58 AM'),
(1437, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '02:58 AM'),
(1436, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '02:57 AM'),
(1435, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '02:57 AM'),
(1434, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '02:57 AM'),
(1433, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '02:30 AM'),
(1432, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '02:30 AM'),
(1431, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '02:30 AM'),
(1430, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '02:30 AM'),
(1429, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '02:29 AM'),
(1428, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '02:29 AM'),
(1427, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '02:29 AM'),
(1426, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '02:29 AM'),
(1425, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '02:29 AM'),
(1424, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '02:28 AM'),
(1423, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '02:28 AM'),
(1422, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '02:28 AM'),
(1421, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '02:12 AM'),
(1420, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '02:11 AM'),
(1419, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '02:11 AM'),
(1418, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '12:53 AM'),
(1417, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '12:53 AM'),
(1416, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '12:53 AM'),
(1415, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '12:53 AM'),
(1414, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '12:53 AM'),
(1413, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '12:53 AM'),
(1412, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '12:52 AM'),
(1411, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '12:52 AM'),
(1410, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '12:49 AM'),
(1409, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '16/May/2020', '12:49 AM'),
(1408, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '15/May/2020', '11:28 PM'),
(1407, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '15/May/2020', '11:28 PM'),
(1406, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '15/May/2020', '11:28 PM'),
(1405, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '15/May/2020', '11:28 PM'),
(1404, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '15/May/2020', '11:27 PM'),
(1403, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '15/May/2020', '11:27 PM'),
(1402, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '15/May/2020', '11:27 PM'),
(1401, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '15/May/2020', '11:27 PM'),
(1400, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '15/May/2020', '11:27 PM'),
(1399, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '15/May/2020', '11:27 PM'),
(1398, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '15/May/2020', '11:26 PM'),
(1397, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '15/May/2020', '11:26 PM'),
(1396, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '15/May/2020', '11:21 PM'),
(1395, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '15/May/2020', '11:21 PM'),
(1394, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '15/May/2020', '11:21 PM'),
(1393, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '15/May/2020', '11:20 PM'),
(1392, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '15/May/2020', '11:20 PM'),
(1391, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '15/May/2020', '11:02 PM'),
(1390, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '15/May/2020', '11:02 PM'),
(1389, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '15/May/2020', '11:02 PM'),
(1388, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '15/May/2020', '11:02 PM'),
(1387, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '15/May/2020', '11:02 PM'),
(1386, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '15/May/2020', '11:01 PM'),
(1385, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '15/May/2020', '11:01 PM'),
(1384, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '15/May/2020', '11:01 PM'),
(1382, 1, '0.0', '0.0', '', '15/May/2020', '11:01 PM'),
(1383, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '15/May/2020', '11:01 PM'),
(1381, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '15/May/2020', '11:01 PM'),
(1380, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '15/May/2020', '11:01 PM'),
(1379, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '15/May/2020', '11:00 PM'),
(1378, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '15/May/2020', '11:00 PM'),
(1377, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '15/May/2020', '11:00 PM'),
(1376, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '14/May/2020', '12:18 AM'),
(1375, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '14/May/2020', '12:18 AM'),
(1373, 1, '0.0', '0.0', '', '14/May/2020', '12:02 AM'),
(1374, 1, '0.0', '0.0', '', '14/May/2020', '12:03 AM'),
(1371, 1, '0.0', '0.0', '', '14/May/2020', '12:02 AM'),
(1372, 1, '0.0', '0.0', '', '14/May/2020', '12:02 AM'),
(1369, 1, '0.0', '0.0', '', '14/May/2020', '12:02 AM'),
(1370, 1, '0.0', '0.0', '', '14/May/2020', '12:02 AM'),
(1367, 1, '0.0', '0.0', '', '14/May/2020', '12:01 AM'),
(1368, 1, '0.0', '0.0', '', '14/May/2020', '12:02 AM'),
(1365, 1, '0.0', '0.0', '', '14/May/2020', '12:01 AM'),
(1366, 1, '0.0', '0.0', '', '14/May/2020', '12:01 AM'),
(1363, 1, '0.0', '0.0', '', '14/May/2020', '12:01 AM'),
(1364, 1, '0.0', '0.0', '', '14/May/2020', '12:01 AM'),
(1362, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '13/May/2020', '11:31 PM'),
(1361, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '13/May/2020', '11:31 PM'),
(1360, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '13/May/2020', '11:30 PM'),
(1359, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '13/May/2020', '11:30 PM'),
(1358, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '13/May/2020', '11:30 PM'),
(1357, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '13/May/2020', '11:30 PM'),
(1356, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '13/May/2020', '11:30 PM'),
(1355, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '13/May/2020', '11:30 PM'),
(1354, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '13/May/2020', '11:29 PM'),
(1353, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '13/May/2020', '11:29 PM'),
(1352, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '13/May/2020', '11:29 PM'),
(1351, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '13/May/2020', '11:29 PM'),
(1350, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '13/May/2020', '11:08 PM'),
(1349, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '13/May/2020', '11:08 PM'),
(1348, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '13/May/2020', '11:08 PM'),
(1347, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '13/May/2020', '11:08 PM'),
(1346, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '13/May/2020', '11:08 PM'),
(1345, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '13/May/2020', '11:07 PM'),
(1344, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '13/May/2020', '11:07 PM'),
(1343, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '13/May/2020', '11:07 PM'),
(1342, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '13/May/2020', '11:07 PM'),
(1341, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '13/May/2020', '11:07 PM'),
(1340, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '13/May/2020', '11:07 PM'),
(1339, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '12/May/2020', '11:06 PM'),
(1338, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '12/May/2020', '11:06 PM'),
(1337, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '12/May/2020', '11:06 PM'),
(1336, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '12/May/2020', '11:06 PM'),
(1335, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '12/May/2020', '11:06 PM'),
(1334, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '12/May/2020', '11:05 PM'),
(1333, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '12/May/2020', '11:05 PM'),
(1332, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '12/May/2020', '11:05 PM'),
(1331, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '12/May/2020', '11:05 PM'),
(1330, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '12/May/2020', '11:05 PM'),
(1329, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '12/May/2020', '11:05 PM'),
(1328, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '12/May/2020', '11:04 PM'),
(1327, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '12/May/2020', '10:56 PM'),
(1326, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '12/May/2020', '10:55 PM'),
(1325, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '12/May/2020', '10:55 PM'),
(1324, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '12/May/2020', '10:55 PM'),
(1323, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '12/May/2020', '10:55 PM'),
(1322, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '12/May/2020', '10:55 PM'),
(1321, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '12/May/2020', '10:55 PM'),
(1320, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '12/May/2020', '10:54 PM'),
(1319, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '12/May/2020', '10:54 PM'),
(1318, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '12/May/2020', '10:54 PM'),
(1317, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '12/May/2020', '10:54 PM'),
(1316, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '12/May/2020', '10:54 PM'),
(1315, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '12/May/2020', '10:52 PM'),
(1314, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '12/May/2020', '10:52 PM'),
(1313, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '12/May/2020', '10:52 PM'),
(1312, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '12/May/2020', '10:52 PM'),
(1311, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '12/May/2020', '10:52 PM'),
(1310, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '12/May/2020', '10:52 PM'),
(1309, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '12/May/2020', '10:52 PM'),
(1307, 1, '0.0', '0.0', '', '12/May/2020', '10:51 PM'),
(1308, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '12/May/2020', '10:51 PM'),
(1306, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '12/May/2020', '10:51 PM'),
(1305, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '12/May/2020', '10:51 PM'),
(1304, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '12/May/2020', '10:51 PM'),
(1303, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '12/May/2020', '10:51 PM'),
(1302, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '12/May/2020', '10:50 PM'),
(1301, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '12/May/2020', '10:50 PM'),
(1300, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '12/May/2020', '10:50 PM'),
(1299, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '12/May/2020', '10:50 PM'),
(1298, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '12/May/2020', '10:50 PM'),
(1297, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '12/May/2020', '10:50 PM'),
(1296, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '12/May/2020', '10:49 PM'),
(1294, 1, '0.0', '0.0', '', '11/May/2020', '11:56 PM'),
(1295, 1, '0.0', '0.0', '', '11/May/2020', '11:57 PM'),
(1293, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '06/May/2020', '11:16 PM'),
(1292, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '06/May/2020', '11:16 PM'),
(1291, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '06/May/2020', '11:16 PM'),
(1290, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '06/May/2020', '11:15 PM'),
(1289, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '06/May/2020', '11:15 PM'),
(1288, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '06/May/2020', '11:15 PM'),
(1287, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '06/May/2020', '11:15 PM'),
(1286, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '06/May/2020', '11:15 PM'),
(1285, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '06/May/2020', '11:15 PM'),
(1284, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '06/May/2020', '11:14 PM'),
(1283, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '06/May/2020', '11:14 PM'),
(1282, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '06/May/2020', '11:14 PM'),
(1281, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '06/May/2020', '11:11 PM'),
(1280, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '06/May/2020', '11:11 PM'),
(1278, 1, '0.0', '0.0', '', '06/May/2020', '11:11 PM'),
(1279, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '06/May/2020', '11:11 PM'),
(1277, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '06/May/2020', '11:11 PM'),
(1276, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '06/May/2020', '11:11 PM'),
(1275, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '06/May/2020', '11:11 PM'),
(1274, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '06/May/2020', '10:57 PM'),
(1273, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '06/May/2020', '10:57 PM'),
(1272, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '06/May/2020', '10:57 PM'),
(1271, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '06/May/2020', '10:57 PM'),
(1270, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '06/May/2020', '10:57 PM'),
(1269, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '06/May/2020', '10:57 PM'),
(1268, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '06/May/2020', '10:57 PM'),
(1267, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '06/May/2020', '10:57 PM'),
(1266, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '04/May/2020', '11:23 PM'),
(1265, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '04/May/2020', '11:23 PM'),
(1264, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '04/May/2020', '11:23 PM'),
(1263, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '04/May/2020', '11:22 PM'),
(1262, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '04/May/2020', '11:22 PM'),
(1261, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '04/May/2020', '11:22 PM'),
(1260, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '04/May/2020', '11:22 PM'),
(1259, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '04/May/2020', '11:22 PM'),
(1258, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '04/May/2020', '11:22 PM'),
(1257, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '04/May/2020', '11:22 PM'),
(1255, 1, '0.0', '0.0', '', '04/May/2020', '11:22 PM'),
(1256, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '04/May/2020', '11:22 PM'),
(1254, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '04/May/2020', '11:22 PM'),
(1253, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '04/May/2020', '11:21 PM'),
(1252, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '04/May/2020', '11:21 PM'),
(1251, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '04/May/2020', '11:21 PM'),
(1250, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '04/May/2020', '11:21 PM'),
(1249, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '04/May/2020', '11:21 PM'),
(1248, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '04/May/2020', '11:21 PM'),
(1247, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '04/May/2020', '11:20 PM'),
(1246, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '04/May/2020', '11:20 PM'),
(1245, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '04/May/2020', '11:20 PM'),
(1244, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '04/May/2020', '11:20 PM'),
(1243, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '04/May/2020', '11:20 PM'),
(1242, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '04/May/2020', '11:20 PM'),
(1241, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '04/May/2020', '11:19 PM'),
(1240, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '04/May/2020', '11:19 PM'),
(1239, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '04/May/2020', '11:19 PM'),
(1238, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '04/May/2020', '11:19 PM'),
(1237, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '04/May/2020', '11:19 PM'),
(1236, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '04/May/2020', '11:19 PM'),
(1235, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '04/May/2020', '01:56 AM'),
(1234, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '04/May/2020', '01:56 AM'),
(1233, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '04/May/2020', '01:56 AM'),
(1232, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '04/May/2020', '01:56 AM'),
(1231, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '04/May/2020', '01:56 AM'),
(1230, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '04/May/2020', '01:56 AM'),
(1229, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '04/May/2020', '01:56 AM'),
(1228, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '04/May/2020', '01:56 AM'),
(1227, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '04/May/2020', '01:56 AM'),
(1226, 1, '0.0', '0.0', '', '04/May/2020', '01:56 AM'),
(1225, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '04/May/2020', '01:55 AM'),
(1224, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '04/May/2020', '01:55 AM'),
(1223, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '11:59 PM'),
(1222, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '11:58 PM'),
(1221, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '11:58 PM'),
(1220, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '11:58 PM'),
(1219, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '11:58 PM'),
(1218, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '11:58 PM'),
(1217, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '11:58 PM'),
(1216, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '11:57 PM'),
(1215, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '11:57 PM'),
(1214, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '11:57 PM'),
(1213, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '11:57 PM'),
(1212, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '11:57 PM'),
(1211, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '02:44 PM'),
(1210, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '02:44 PM'),
(1209, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '02:44 PM'),
(1208, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '02:44 PM'),
(1207, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '02:44 PM'),
(1206, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '02:44 PM'),
(1205, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '02:43 PM'),
(1204, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '02:43 PM'),
(1203, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '02:43 PM'),
(1202, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '02:43 PM'),
(1201, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '02:43 PM'),
(1200, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '02:43 PM'),
(1199, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '02:40 PM'),
(1198, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '02:40 PM'),
(1197, 1, '0.0', '0.0', '', '03/May/2020', '02:40 PM'),
(1196, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '02:40 PM'),
(1195, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '02:40 PM'),
(1194, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '02:39 PM'),
(1193, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '02:39 PM'),
(1192, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '02:39 PM'),
(1190, 1, '0.0', '0.0', '', '03/May/2020', '02:39 PM'),
(1191, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '02:39 PM'),
(1189, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '02:39 PM'),
(1188, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '02:39 PM'),
(1187, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '02:39 PM'),
(1186, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '02:38 PM'),
(1185, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '02:35 PM'),
(1184, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '02:35 PM'),
(1183, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '12:09 AM'),
(1182, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '12:09 AM'),
(1181, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '12:09 AM'),
(1180, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '12:09 AM'),
(1179, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '12:09 AM'),
(1178, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '12:09 AM'),
(1177, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '12:08 AM'),
(1176, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '12:08 AM'),
(1175, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '12:07 AM'),
(1174, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '12:07 AM'),
(1173, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '12:07 AM'),
(1172, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '12:07 AM'),
(1171, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '12:04 AM'),
(1170, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '12:04 AM'),
(1169, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '12:04 AM'),
(1168, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '12:04 AM'),
(1166, 1, '0.0', '0.0', '', '03/May/2020', '12:04 AM'),
(1167, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '12:04 AM'),
(1165, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '12:04 AM'),
(1163, 1, '0.0', '0.0', '', '03/May/2020', '12:04 AM'),
(1164, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '12:04 AM'),
(1162, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '12:04 AM'),
(1161, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '12:04 AM'),
(1160, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '12:03 AM'),
(1159, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '12:03 AM'),
(1158, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:54 PM'),
(1157, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:54 PM'),
(1156, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:54 PM'),
(1155, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:54 PM'),
(1154, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:54 PM'),
(1153, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:54 PM'),
(1152, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:54 PM'),
(1151, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:54 PM'),
(1150, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:54 PM'),
(1148, 1, '0.0', '0.0', '', '02/May/2020', '11:54 PM'),
(1149, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:54 PM'),
(1147, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:53 PM'),
(1146, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:52 PM'),
(1145, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:52 PM');
INSERT INTO `mobile_gps_location` (`mob_loc_id`, `user_id`, `latitude`, `longitude`, `loc_name`, `date`, `time`) VALUES
(1144, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:51 PM'),
(1143, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:51 PM'),
(1142, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:51 PM'),
(1141, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:51 PM'),
(1140, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:51 PM'),
(1139, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:51 PM'),
(1138, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:50 PM'),
(1137, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:50 PM'),
(1136, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:50 PM'),
(1135, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:50 PM'),
(1134, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:48 PM'),
(1133, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:48 PM'),
(1132, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:48 PM'),
(1131, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:48 PM'),
(1130, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:48 PM'),
(1129, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:48 PM'),
(1128, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:48 PM'),
(1127, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:48 PM'),
(1125, 1, '0.0', '0.0', '', '02/May/2020', '11:48 PM'),
(1126, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:48 PM'),
(1124, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:47 PM'),
(1123, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:47 PM'),
(1122, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:47 PM'),
(1121, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:47 PM'),
(1120, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:47 PM'),
(1119, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:47 PM'),
(1118, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:41 PM'),
(1117, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:41 PM'),
(1116, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:41 PM'),
(1115, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:41 PM'),
(1114, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:41 PM'),
(1113, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:40 PM'),
(1112, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:40 PM'),
(1111, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:40 PM'),
(1110, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:38 PM'),
(1109, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:38 PM'),
(1108, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:38 PM'),
(1107, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:38 PM'),
(1106, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:38 PM'),
(1105, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:38 PM'),
(1104, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:37 PM'),
(1103, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:37 PM'),
(1102, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:37 PM'),
(1101, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:37 PM'),
(1100, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:37 PM'),
(1099, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:37 PM'),
(1098, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:36 PM'),
(1097, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '02/May/2020', '11:36 PM'),
(1096, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '30/Apr/2020', '01:36 AM'),
(1095, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '30/Apr/2020', '01:36 AM'),
(1094, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '30/Apr/2020', '01:36 AM'),
(1093, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '30/Apr/2020', '01:36 AM'),
(1092, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '30/Apr/2020', '01:36 AM'),
(1091, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '30/Apr/2020', '01:36 AM'),
(1090, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '30/Apr/2020', '01:35 AM'),
(1089, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '30/Apr/2020', '01:35 AM'),
(1088, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '29/Apr/2020', '10:57 PM'),
(1087, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '29/Apr/2020', '10:57 PM'),
(1086, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '29/Apr/2020', '10:56 PM'),
(1085, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '29/Apr/2020', '10:56 PM'),
(1084, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '29/Apr/2020', '10:56 PM'),
(1083, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '29/Apr/2020', '10:56 PM'),
(1082, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '29/Apr/2020', '10:56 PM'),
(1081, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '29/Apr/2020', '10:56 PM'),
(1080, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '29/Apr/2020', '10:55 PM'),
(1079, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '29/Apr/2020', '10:55 PM'),
(1078, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '29/Apr/2020', '10:55 PM'),
(1077, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '29/Apr/2020', '10:55 PM'),
(1076, 2, '32.582898333333335', '74.15539833333333', 'Sook Kalan Road, Sook Kalan', '26/Apr/2020', '11:33 PM'),
(1075, 2, '32.583898333333335', '74.16539833333333', 'Sook Kalan Road, Sook Kalan', '26/Apr/2020', '11:33 PM'),
(1074, 2, '32.584898333333335', '74.17539833333333', 'Sook Kalan Road, Sook Kalan', '26/Apr/2020', '11:33 PM'),
(1073, 2, '32.585898333333335', '74.18539833333333', 'Sook Kalan Road, Sook Kalan', '26/Apr/2020', '11:33 PM'),
(1072, 2, '32.586898333333335', '74.19539833333333', 'Sook Kalan Road, Sook Kalan', '26/Apr/2020', '11:33 PM'),
(1067, 1, '32.590898333333335', '74.13539833333333', 'Sook Kalan Road, Sook Kalan', '26/Apr/2020', '11:33 PM'),
(1068, 1, '32.591898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '26/Apr/2020', '11:33 PM'),
(1069, 1, '32.592898333333335', '74.15539833333333', 'Sook Kalan Road, Sook Kalan', '26/Apr/2020', '11:33 PM'),
(1070, 1, '32.593898333333335', '74.16539833333333', 'Sook Kalan Road, Sook Kalan', '26/Apr/2020', '11:33 PM'),
(1071, 1, '32.594898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '26/Apr/2020', '11:33 PM');

-- --------------------------------------------------------

--
-- Table structure for table `order_gps_location`
--

DROP TABLE IF EXISTS `order_gps_location`;
CREATE TABLE IF NOT EXISTS `order_gps_location` (
  `order_loc_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` int(10) UNSIGNED NOT NULL,
  `latitude` varchar(20) NOT NULL,
  `longitude` varchar(20) NOT NULL,
  `loc_name` varchar(150) NOT NULL,
  `date` varchar(30) NOT NULL,
  `time` varchar(30) NOT NULL,
  `dealer_id` int(10) UNSIGNED NOT NULL,
  `price` float NOT NULL,
  `type` varchar(20) NOT NULL,
  PRIMARY KEY (`order_loc_id`)
) ENGINE=MyISAM AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_gps_location`
--

INSERT INTO `order_gps_location` (`order_loc_id`, `order_id`, `user_id`, `latitude`, `longitude`, `loc_name`, `date`, `time`, `dealer_id`, `price`, `type`) VALUES
(1, 0, 1, '32.501158', '74.070952', 'Fawara Chowk ÙÙˆØ§Ø±Û Ú†ÙˆÚ©, Fawara Chowk, Dhaki Bazaar Area, Gujrat, Punjab, Pakistan', '25/Mar/2020', '01:20 PM', 1, 2400, 'Booking'),
(2, 0, 1, '32.511158', '74.070952', 'Dhakki Chowk Gujrat Pakistan', '25/Mar/2020', '01:18 pm', 1, 1500, 'Booking'),
(3, 0, 1, '32.521158', '74.070952', 'Dhakki Chowk Gujrat Pakistan', '25/Mar/2020', '01:18 pm', 1, 1500, 'Collection'),
(4, 0, 1, '32.531158', '74.070952', 'Dhakki Chowk Gujrat Pakistan', '25/Mar/2020', '01:18 pm', 1, 1500, 'Collection'),
(5, 0, 1, '32.541158', '74.070952', 'Fawara Chowk ÙÙˆØ§Ø±Û Ú†ÙˆÚ©, Fawara Chowk, Dhaki Bazaar Area, Gujrat, Punjab, Pakistan', '25/Mar/2020', '01:27 PM', 6, 2092.5, 'Booking'),
(6, 0, 1, '32.551158', '74.070952', 'Fawara Chowk ÙÙˆØ§Ø±Û Ú†ÙˆÚ©, Fawara Chowk, Dhaki Bazaar Area, Gujrat, Punjab, Pakistan', '25/Mar/2020', '02:50 PM', 1, 3300, 'Delivered'),
(7, 0, 1, '32.561158', '74.070952', 'Fawara Chowk ÙÙˆØ§Ø±Û Ú†ÙˆÚ©, Fawara Chowk, Dhaki Bazaar Area, Gujrat, Punjab, Pakistan', '25/Mar/2020', '02:51 PM', 1, 1614, 'Delivered'),
(8, 0, 1, '32.571158', '74.070952', 'Dhakki Chowk Gujrat Pakistan', '25/Mar/2020', '01:18 pm', 1, 1500, 'Booking'),
(9, 0, 2, '32.501158', '74.010952', 'Fawara Chowk ÙÙˆØ§Ø±Û Ú†ÙˆÚ©, Fawara Chowk, Dhaki Bazaar Area, Gujrat, Punjab, Pakistan', '25/Mar/2020', '01:20 PM', 1, 2400, 'Booking'),
(10, 0, 2, '32.511158', '74.020952', 'Dhakki Chowk Gujrat Pakistan', '25/Mar/2020', '01:18 pm', 1, 1500, 'Booking'),
(11, 0, 2, '32.521158', '74.030952', 'Dhakki Chowk Gujrat Pakistan', '25/Mar/2020', '01:18 pm', 1, 1500, 'Collection'),
(12, 0, 2, '32.531158', '74.040952', 'Dhakki Chowk Gujrat Pakistan', '25/Mar/2020', '01:18 pm', 1, 1500, 'Collection'),
(13, 0, 2, '32.541158', '74.050952', 'Fawara Chowk ÙÙˆØ§Ø±Û Ú†ÙˆÚ©, Fawara Chowk, Dhaki Bazaar Area, Gujrat, Punjab, Pakistan', '25/Mar/2020', '01:27 PM', 6, 2092.5, 'Booking'),
(14, 0, 2, '32.551158', '74.060952', 'Fawara Chowk ÙÙˆØ§Ø±Û Ú†ÙˆÚ©, Fawara Chowk, Dhaki Bazaar Area, Gujrat, Punjab, Pakistan', '25/Mar/2020', '02:50 PM', 1, 3300, 'Delivered'),
(15, 0, 2, '32.561158', '74.070952', 'Fawara Chowk ÙÙˆØ§Ø±Û Ú†ÙˆÚ©, Fawara Chowk, Dhaki Bazaar Area, Gujrat, Punjab, Pakistan', '25/Mar/2020', '02:51 PM', 1, 1614, 'Delivered'),
(16, 0, 2, '32.571158', '74.080952', 'Dhakki Chowk Gujrat Pakistan', '25/Mar/2020', '01:18 pm', 1, 1500, 'Booking'),
(17, 2, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '12/Apr/2020', '02:13 PM', 3, 5000, 'Delivered'),
(18, 2, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '13/Apr/2020', '02:53 AM', 3, 5000, 'Delivered'),
(19, 2, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '13/Apr/2020', '9:28 PM', 3, 5000, 'Delivered'),
(20, 0, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '18/Apr/2020', '01:27 AM', 1, 500, 'Collection'),
(21, 0, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '18/Apr/2020', '02:25 AM', 1, 500, 'Collection'),
(22, 0, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '25/Apr/2020', '09:53 PM', 1, 200, 'Collection'),
(23, 0, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Rd, Sook Kalan, Gujrat, Punjab 50770, Pakistan', '25/Apr/2020', '10:48 PM', 2, 960, 'Booking'),
(24, 0, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Rd, Sook Kalan, Gujrat, Punjab 50770, Pakistan', '25/Apr/2020', '11:38 PM', 1, 60, 'Booking'),
(25, 0, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Rd, Sook Kalan, Gujrat, Punjab 50770, Pakistan', '25/Apr/2020', '11:39 PM', 1, 1200, 'Booking'),
(26, 0, 1, '32.5879', '74.1454', 'Sook Kalan Rd, Sook Kalan, Gujrat, Punjab 50770, Pakistan', '25/Apr/2020', '11:01 PM', 1, 1000, 'Booking'),
(27, 0, 1, '32.5879', '74.1454', 'Sook Kalan Rd, Sook Kalan, Gujrat, Punjab 50770, Pakistan', '25/Apr/2020', '11:01 PM', 1, 1000, 'Booking'),
(28, 0, 1, '32.5879', '74.1454', 'Sook Kalan Rd, Sook Kalan, Gujrat, Punjab 50770, Pakistan', '25/Apr/2020', '11:01 PM', 1, 1000, 'Booking'),
(29, 2, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '26/Apr/2020', '04:52 PM', 3, 500, 'Delivered'),
(30, 0, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '02:41 PM', 1, 500, 'Collection'),
(31, 2, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '03/May/2020', '02:42 PM', 3, 1000, 'Delivered'),
(32, 0, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '04/May/2020', '01:55 AM', 1, 500, 'Collection'),
(33, 2, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '04/May/2020', '11:20 PM', 3, 3000, 'Delivered'),
(34, 2, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Road, Sook Kalan', '04/May/2020', '11:22 PM', 3, 3000, 'Delivered'),
(35, 0, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Rd, Sook Kalan, Gujrat, Punjab 50770, Pakistan', '23/May/2020', '03:33 PM', 1, 2500, 'Booking');

-- --------------------------------------------------------

--
-- Table structure for table `order_info`
--

DROP TABLE IF EXISTS `order_info`;
CREATE TABLE IF NOT EXISTS `order_info` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `dealer_id` int(11) DEFAULT NULL,
  `product_id` varchar(500) DEFAULT NULL,
  `quantity` varchar(500) DEFAULT NULL,
  `unit` varchar(500) NOT NULL,
  `order_price` float NOT NULL,
  `bonus` varchar(500) NOT NULL,
  `discount` varchar(500) NOT NULL,
  `final_price` float NOT NULL,
  `order_success` tinyint(1) NOT NULL,
  `latitude` varchar(25) NOT NULL,
  `logitude` varchar(25) NOT NULL,
  `order_place_area` varchar(300) NOT NULL,
  `date` varchar(50) NOT NULL,
  `time` varchar(50) NOT NULL,
  `user_id` int(10) NOT NULL,
  `status` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=MyISAM AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_info`
--

INSERT INTO `order_info` (`order_id`, `dealer_id`, `product_id`, `quantity`, `unit`, `order_price`, `bonus`, `discount`, `final_price`, `order_success`, `latitude`, `logitude`, `order_place_area`, `date`, `time`, `user_id`, `status`) VALUES
(1, 1, '123', '5', 'Box', 1000, '0', '0', 1000, 0, '0', '0', '', '01/Oct/2019', '09:58 PM', 1, 'Pending'),
(2, 3, '123_-_456', '5_-_10', 'Box_-_Packets', 5800, '0', '0', 5800, 0, '0', '0', '', '10/Nov/2019', '09:58 PM', 1, 'Invoiced'),
(3, 1, '456', '8', 'Packets', 960, '0', '30', 672, 0, '0', '0', '', '10/Nov/2019', '10:01 am', 4, 'Pending'),
(4, 3, '1', '1', 'Packets', 2500, '0', '0', 2500, 0, '0', '0', '', '11/Nov/2019', '12:05 AM', 2, 'Pending'),
(21, 2, '123', '10', 'Packets', 150, '', '0', 150, 0, '0', '0', '', '19/Jan/2020', '09:18 PM', 1, 'Pending'),
(20, 1, '3', '1', 'Packets', 60, '', '0', 60, 0, '0', '0', '', '19/Jan/2020', '09:17 PM', 1, 'Delivered'),
(7, 4, '4', '4', 'Packets', 0, '0', '0', 0, 0, '0', '0', '', '11/Nov/2019', '02:09 AM', 1, 'Pending'),
(8, 5, '5', '5', 'Box', 0, '0', '0', 0, 0, '0', '0', '', '11/Nov/2019', '02:15 AM', 3, 'Pending'),
(9, 5, '6', '6', 'Box', 0, '0', '0', 0, 0, '0', '0', '', '11/Nov/2019', '02:17 AM', 1, 'Pending'),
(10, 5, '7', '7', 'Box', 0, '0', '0', 0, 0, '0', '0', '', '11/Nov/2019', '02:19 AM', 1, 'Pending'),
(11, 5, '5', '5', 'Packets', 0, '0', '0', 0, 0, '0', '0', '', '11/Nov/2019', '03:53 AM', 1, 'Pending'),
(12, 4, '4', '4', 'Box', 0, '0', '0', 0, 0, '0', '0', '', '11/Nov/2019', '09:11 AM', 1, 'Pending'),
(13, 5, '5', '5', 'Box', 0, '0', '0', 0, 0, '0', '0', '', '11/Nov/2019', '09:11 AM', 1, 'Pending'),
(14, 5, '5', '5', 'Box', 0, '0', '0', 0, 0, '0', '0', '', '11/Nov/2019', '09:12 AM', 1, 'Pending'),
(15, 4, '4', '4', 'Box', 0, '0', '0', 0, 0, '0', '0', '', '11/Nov/2019', '09:15 AM', 1, 'Pending'),
(16, 2, '2', '2', 'Box', 0, '0', '0', 0, 0, '0', '0', '', '11/Nov/2019', '09:39 AM', 1, 'Pending'),
(17, 3, '3', '3', 'Box', 0, '0', '0', 0, 0, '0', '0', '', '11/Nov/2019', '09:40 AM', 1, 'Pending'),
(18, 4, '4', '4', 'Box', 0, '0', '0', 0, 0, '0', '0', '', '17/Nov/2019', '12:34 AM', 1, 'Pending'),
(19, 4441, '4441', '4441', 'Packets', 0, '0', '0', 0, 0, '0', '0', '', '17/Nov/2019', '12:45 AM', 1, 'Pending'),
(22, 4, '2', '1', 'Packets', 80, '', '0', 80, 0, '0', '0', '', '19/Jan/2020', '09:19 PM', 1, 'Pending'),
(23, 5, '456_-_3_-_123_-_1', '1_-_2_-_3_-_4', 'Packets_-_Packets_-_Packets_-_Packets', 1090, '', '0', 1090, 0, '0', '0', '', '19/Jan/2020', '09:21 PM', 1, 'Pending'),
(24, 1, '2_-_3', '1_-_1', 'Packets_-_Packets', 140, '', '0', 140, 0, '0', '0', '', '19/Jan/2020', '09:29 PM', 1, 'Pending'),
(25, 3, '123_-_456', '1_-_1', 'Packets_-_Packets', 270, '', '0', 270, 0, '0', '0', '', '19/Jan/2020', '09:31 PM', 1, 'Pending'),
(26, 2, '3_-_123', '1_-_1', 'Packets_-_Packets', 210, '', '0', 210, 0, '0', '0', '', '19/Jan/2020', '09:35 PM', 1, 'Pending'),
(27, 2, '123', '15', 'Packets', 2250, '', '0', 2250, 0, '37.422', '-122.084', '1600 Amphitheatre Pkwy, Mountain View, CA 94043, USA', '10/Feb/2020', '10:55 PM', 1, 'Pending'),
(30, 1, '2_-_3', '10_-_10', 'Packs_-_Packs', 32320, '0_-_0', '0_-_0', 32320, 1, '1.021', '2.01', 'Shadiwal', '2020-03-12', '12:53:37.241', 1, '1'),
(31, 1, '2_-_3_-_123', '10_-_10_-_10', 'Packs_-_Packs_-_Packs', 32640, '0_-_0_-_0', '10_-_10_-_10', 29376, 1, '1.021', '2.01', 'Shadiwal', '2020-03-14', '10:14:46.903', 1, 'Pending'),
(32, 1, '458_-_460_-_123_-_456_-_1', '10_-_10_-_10_-_10_-_10', 'Packs_-_Packs_-_Packs_-_Packs_-_Packs', 1260, '1_-_1_-_1_-_1_-_1', '0_-_0_-_0_-_0_-_0', 1260, 1, '1.021', '2.01', 'Shadiwal', '2020-03-16', '12:56:59.743', 1, 'Pending'),
(33, 1, '2_-_1', '10_-_10', 'Packs_-_Packs', 640, '0_-_0', '0_-_0', 640, 1, '1.021', '2.01', 'Shadiwal', '2020-03-28', '14:19:56.123', 1, 'Pending'),
(34, 1, '2_-_456', '10_-_10', 'Packs_-_Packs', 640, '0_-_0', '0_-_0', 640, 1, '1.021', '2.01', 'Shadiwal', '2020-03-28', '14:23:30.983', 1, 'Pending'),
(35, 1, '2', '10', 'Packs', 320, '0', '10', 288, 1, '1.021', '2.01', 'Shadiwal', '2020-03-28', '14:35:00.036', 1, 'Pending'),
(36, 1, '2', '10', 'Packs', 320, '0', '10', 288, 1, '1.021', '2.01', 'Shadiwal', '2020-03-28', '14:36:19.340', 1, 'Pending'),
(37, 1, '2', '10', 'Packs', 320, '0', '10', 288, 1, '1.021', '2.01', 'Shadiwal', '2020-03-28', '14:37:01.634', 1, 'Pending'),
(38, 1, '2', '10', 'Packs', 320, '0', '10', 288, 1, '1.021', '2.01', 'Shadiwal', '2020-03-28', '14:44:31.037', 1, 'Pending'),
(39, 1, '2', '10', 'Packs', 320, '0', '10', 288, 1, '1.021', '2.01', 'Shadiwal', '2020-03-28', '14:45:22.513', 1, 'Pending'),
(40, 1, '2_-_456_-_1', '10_-_10_-_10', 'Packs_-_Packs_-_Packs', 960, '0_-_0_-_0', '0_-_0_-_10', 928, 1, '1.021', '2.01', 'Shadiwal', '2020-03-28', '14:46:05.412', 1, 'Pending'),
(41, 1, '2_-_456', '3_-_3', 'Packs_-_Packs', 192, '0_-_0', '10_-_1', 181.44, 1, '1.021', '2.01', 'Shadiwal', '2020-03-30', '10:04:28.204', 1, 'Pending'),
(42, 1, '2_-_456', '10_-_10', 'Packs_-_Packs', 640, '0_-_0', '0_-_0', 640, 1, '1.021', '2.01', 'Shadiwal', '2020-03-30', '11:11:45.449', 1, 'Pending'),
(43, 1, '2_-_456', '10_-_10', 'Packs_-_Packs', 640, '0_-_0', '0_-_0', 640, 1, '1.021', '2.01', 'Shadiwal', '2020-03-30', '11:17:11.552', 1, 'Pending'),
(44, 1, '2_-_3', '10_-_10', 'Packs_-_Packs', 32320, '0_-_0', '10_-_0', 32288, 1, '1.021', '2.01', 'Shadiwal', '2020-03-30', '11:19:20.762', 1, 'Invoiced'),
(45, 2, '456', '8', 'Packets', 960, '', '0.00', 960, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Rd, Sook Kalan, Gujrat, Punjab 50770, Pakistan', '25/Apr/2020', '10:48 PM', 1, 'Pending'),
(46, 1, '3', '1', 'Packets', 60, '', '0.00', 60, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Rd, Sook Kalan, Gujrat, Punjab 50770, Pakistan', '25/Apr/2020', '11:38 PM', 1, 'Pending'),
(47, 1, '456', '10', 'Packets', 1200, '', '0.00', 1200, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Rd, Sook Kalan, Gujrat, Punjab 50770, Pakistan', '25/Apr/2020', '11:39 PM', 1, 'Pending'),
(48, 1, '1', '10', 'Packets', 1000, '', '0.00', 1000, 1, '32.5879', '74.1454', 'Sook Kalan Rd, Sook Kalan, Gujrat, Punjab 50770, Pakistan', '25/Apr/2020', '11:01 PM', 1, 'Pending'),
(49, 1, '1', '10', 'Packets', 1000, '', '0.00', 1000, 1, '32.5879', '74.1454', 'Sook Kalan Rd, Sook Kalan, Gujrat, Punjab 50770, Pakistan', '25/Apr/2020', '11:01 PM', 1, 'Pending'),
(50, 1, '1', '10', 'Packets', 1000, '', '0.00', 1000, 1, '32.5879', '74.1454', 'Sook Kalan Rd, Sook Kalan, Gujrat, Punjab 50770, Pakistan', '25/Apr/2020', '11:01 PM', 1, 'Pending'),
(51, 1, '1_-_123', '10_-_10', 'Packets_-_Packets', 2500, '', '0.00_-_0.00', 2500, 1, '32.587898333333335', '74.14539833333333', 'Sook Kalan Rd, Sook Kalan, Gujrat, Punjab 50770, Pakistan', '23/May/2020', '03:33 PM', 1, 'Pending');

-- --------------------------------------------------------

--
-- Table structure for table `order_info_detailed`
--

DROP TABLE IF EXISTS `order_info_detailed`;
CREATE TABLE IF NOT EXISTS `order_info_detailed` (
  `order_id` int(8) UNSIGNED NOT NULL,
  `product_id` int(8) UNSIGNED NOT NULL,
  `batch_number` varchar(15) NOT NULL,
  `quantity` int(8) NOT NULL,
  `submission_quantity` int(8) NOT NULL,
  `unit` varchar(30) NOT NULL,
  `submission_unit` varchar(30) NOT NULL,
  `order_price` float NOT NULL,
  `bonus_quant` int(11) NOT NULL,
  `discount` float NOT NULL,
  `final_price` float NOT NULL,
  `returned_bit` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_info_detailed`
--

INSERT INTO `order_info_detailed` (`order_id`, `product_id`, `batch_number`, `quantity`, `submission_quantity`, `unit`, `submission_unit`, `order_price`, `bonus_quant`, `discount`, `final_price`, `returned_bit`) VALUES
(1, 123, '', 5, 5, 'Packets', 'Packets', 1000, 0, 0, 1000, 0),
(2, 123, '', 5, 5, 'Packets', 'Packets', 1000, 0, 0, 1000, 0),
(3, 456, '', 8, 8, 'Packets', 'Packets', 960, 0, 30, 672, 0),
(38, 2, '', 10, 10, 'Packs', 'Packs', 320, 0, 10, 288, 0),
(39, 2, '', 10, 10, 'Packs', 'Packs', 320, 0, 10, 288, 0),
(40, 2, '', 10, 10, 'Packs', 'Packs', 320, 0, 0, 320, 0),
(40, 456, '', 10, 10, 'Packs', 'Packs', 320, 0, 0, 320, 0),
(40, 1, '', 10, 10, 'Packs', 'Packs', 320, 0, 10, 288, 0),
(41, 2, '', 3, 3, 'Packs', 'Packs', 96, 0, 10, 86.4, 0),
(41, 456, '', 3, 3, 'Packs', 'Packs', 96, 0, 1, 95.04, 0),
(44, 2, 'RAW', 10, 10, 'Packs', 'Packs', 320, 0, 10, 288, 1),
(44, 3, 'BXCC', 10, 10, 'Packs', 'Packs', 32000, 0, 0, 32000, 1),
(2, 456, 'abc', 10, 6, 'Packets', 'Packets', 4800, 0, 0, 4800, 0);

-- --------------------------------------------------------

--
-- Table structure for table `order_return`
--

DROP TABLE IF EXISTS `order_return`;
CREATE TABLE IF NOT EXISTS `order_return` (
  `return_id` int(11) NOT NULL AUTO_INCREMENT,
  `dealer_id` int(11) NOT NULL,
  `return_total_price` float NOT NULL,
  `return_gross_price` float NOT NULL,
  `order_id` int(11) NOT NULL,
  `current_date` varchar(20) NOT NULL,
  PRIMARY KEY (`return_id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_return`
--

INSERT INTO `order_return` (`return_id`, `dealer_id`, `return_total_price`, `return_gross_price`, `order_id`, `current_date`) VALUES
(1, 1, 32, 32, 32, '2020-03-29'),
(3, 1, 320, 320, 0, '2020-03-29'),
(4, 1, 32288, 32320, 44, '2020-04-02'),
(5, 1, 32288, 32320, 44, '2020-04-02'),
(6, 1, 288, 320, 44, '2020-04-02'),
(7, 1, 288, 320, 44, '2020-04-02'),
(8, 1, 288, 320, 44, '2020-04-02'),
(9, 1, 288, 320, 44, '2020-04-02'),
(10, 1, 288, 320, 44, '2020-04-02'),
(11, 1, 32000, 32000, 44, '2020-04-05');

-- --------------------------------------------------------

--
-- Table structure for table `order_return_detail`
--

DROP TABLE IF EXISTS `order_return_detail`;
CREATE TABLE IF NOT EXISTS `order_return_detail` (
  `return_id` int(11) NOT NULL,
  `prod_id` int(11) NOT NULL,
  `prod_batch` varchar(15) NOT NULL,
  `prod_quant` int(11) NOT NULL,
  `bonus_quant` int(11) NOT NULL,
  `discount_amount` float NOT NULL,
  `total_amount` float NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_return_detail`
--

INSERT INTO `order_return_detail` (`return_id`, `prod_id`, `prod_batch`, `prod_quant`, `bonus_quant`, `discount_amount`, `total_amount`) VALUES
(3, 2, '', 10, 0, 320, 0),
(3, 1, '', 10, 0, 320, 0),
(3, 456, '', 10, 0, 320, 0),
(8, 2, 'RAW', 10, 0, 288, 32),
(9, 2, 'RAW', 10, 0, 288, 32),
(10, 2, 'RAW', 10, 0, 288, 32),
(11, 3, 'BXCC', 10, 0, 32000, 0);

-- --------------------------------------------------------

--
-- Table structure for table `products_stock`
--

DROP TABLE IF EXISTS `products_stock`;
CREATE TABLE IF NOT EXISTS `products_stock` (
  `product_id` int(10) NOT NULL,
  `in_stock` int(10) NOT NULL,
  `bonus_quant` int(11) NOT NULL,
  `status` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products_stock`
--

INSERT INTO `products_stock` (`product_id`, `in_stock`, `bonus_quant`, `status`) VALUES
(2, 169, 20, 'Active'),
(456, 67, 10, 'Active'),
(3, 80, 5, 'Active'),
(1, 0, 1, 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `product_info`
--

DROP TABLE IF EXISTS `product_info`;
CREATE TABLE IF NOT EXISTS `product_info` (
  `product_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_id` int(10) UNSIGNED NOT NULL,
  `group_id` int(10) UNSIGNED NOT NULL,
  `product_name` varchar(200) NOT NULL,
  `retail_price` float NOT NULL,
  `trade_price` float NOT NULL,
  `purchase_price` float NOT NULL,
  `purchase_discount` float NOT NULL,
  `final_price` float NOT NULL,
  `product_status` varchar(50) NOT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=MyISAM AUTO_INCREMENT=461 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_info`
--

INSERT INTO `product_info` (`product_id`, `company_id`, `group_id`, `product_name`, `retail_price`, `trade_price`, `purchase_price`, `purchase_discount`, `final_price`, `product_status`) VALUES
(1, 3, 3, 'Name 1', 34, 32, 43, 2, 100, 'Active'),
(2, 3, 4, 'Name 1', 34, 32, 43, 2, 80, 'Active'),
(3, 2, 2, 'Name 3333', 3400, 3200, 4300, 20, 60, 'Active'),
(123, 3, 4, 'Name 123', 34, 32, 43, 2, 150, 'Active'),
(456, 3, 4, 'Name 456', 34, 32, 43, 2, 120, 'Active'),
(457, 3, 4, 'Name 456', 34, 32, 43, 2, 120, 'Active'),
(458, 1, 1, 'Abc 1', 20, 15, 10, 5, 9.5, 'Active'),
(459, 1, 1, 'Abc 1', 20, 15, 10, 5, 9.5, 'Active'),
(460, 1, 1, 'Abc 2', 20, 15, 10, 10, 9, 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_info`
--

DROP TABLE IF EXISTS `purchase_info`;
CREATE TABLE IF NOT EXISTS `purchase_info` (
  `purchase_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `invoice_num` varchar(11) NOT NULL,
  `supplier_id` int(11) UNSIGNED NOT NULL,
  `purchase_date` varchar(20) NOT NULL,
  `gross_amount` float NOT NULL,
  `disc_amount` float NOT NULL,
  `net_amount` float NOT NULL,
  PRIMARY KEY (`purchase_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_info`
--

INSERT INTO `purchase_info` (`purchase_id`, `invoice_num`, `supplier_id`, `purchase_date`, `gross_amount`, `disc_amount`, `net_amount`) VALUES
(1, '1', 1, '2020-03-19', 0, 0, 4300),
(2, 'tgrr', 1, '2020-03-19', 0, 0, 4300),
(3, 'jhgfd', 1, '2020-03-19', 0, 0, 175440),
(4, 'dasdc', 1, '2020-03-19', 0, 0, 2150),
(5, 'RWQE', 1, '2020-03-21', 0, 0, 860),
(6, 'vzC', 1, '2020-03-21', 0, 0, 2150),
(7, 'vzc', 1, '2020-03-21', 0, 0, 43000),
(8, 'bxc', 1, '2020-03-21', 0, 0, 43000);

-- --------------------------------------------------------

--
-- Table structure for table `purchase_info_detail`
--

DROP TABLE IF EXISTS `purchase_info_detail`;
CREATE TABLE IF NOT EXISTS `purchase_info_detail` (
  `invoice_num` varchar(15) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `purchase_id` int(10) NOT NULL,
  `comp_id` int(11) NOT NULL,
  `prod_id` int(11) NOT NULL,
  `discount` float NOT NULL,
  `bonus_quant` int(11) NOT NULL,
  `recieve_quant` int(11) NOT NULL,
  `batch_no` varchar(20) NOT NULL,
  `expiry_date` varchar(20) NOT NULL,
  `gross_amount` float NOT NULL,
  `disc_amount` float NOT NULL,
  `net_amount` float NOT NULL,
  `invoice_date` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_info_detail`
--

INSERT INTO `purchase_info_detail` (`invoice_num`, `supplier_id`, `purchase_id`, `comp_id`, `prod_id`, `discount`, `bonus_quant`, `recieve_quant`, `batch_no`, `expiry_date`, `gross_amount`, `disc_amount`, `net_amount`, `invoice_date`) VALUES
('jhgfd', 1, 3, 3, 456, 0, 5, 50, 'YAT', '2020-03-19', 0, 0, 175440, '2020-03-19'),
('jhgfd', 1, 3, 2, 3, 0, 4, 40, 'IOP', '2020-03-19', 0, 0, 175440, '2020-03-19'),
('jhgfd', 1, 3, 3, 1, 0, 3, 30, 'LKJ', '2020-03-19', 0, 0, 175440, '2020-03-19'),
('dasdc', 1, 4, 3, 456, 0, 0, 50, 'YAT', '2020-03-19', 0, 0, 2150, '2020-03-19'),
('RWQE', 1, 5, 3, 1, 0, 2, 20, 'UYT', '2020-03-21', 0, 0, 860, '2020-03-21'),
('vzC', 1, 6, 3, 1, 0, 0, 50, 'FAS', '2020-03-21', 0, 0, 2150, '2020-03-21'),
('vzc', 1, 7, 2, 3, 0, 0, 10, 'BCVX', '2020-03-21', 0, 0, 43000, '2020-03-21'),
('bxc', 1, 8, 2, 3, 0, 0, 10, 'BXCC', '2020-03-21', 0, 0, 43000, '2020-03-21');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_return_info_detail`
--

DROP TABLE IF EXISTS `purchase_return_info_detail`;
CREATE TABLE IF NOT EXISTS `purchase_return_info_detail` (
  `return_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `purch_invo_num` varchar(20) NOT NULL,
  `return_date` varchar(20) NOT NULL,
  `prod_id` int(11) NOT NULL,
  `return_quant` int(11) NOT NULL,
  `discount_percent` float NOT NULL,
  `bonus_quantity` int(11) NOT NULL,
  `total_amount` float NOT NULL,
  `batch_no` varchar(20) NOT NULL,
  `gross_amount` float NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purch_return_info`
--

DROP TABLE IF EXISTS `purch_return_info`;
CREATE TABLE IF NOT EXISTS `purch_return_info` (
  `purch_return_no` int(11) NOT NULL,
  `purch_invo_num` varchar(20) NOT NULL,
  `return_date` varchar(20) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `total_amount` float NOT NULL,
  `discount_amount` float NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `salesman_designated_areas`
--

DROP TABLE IF EXISTS `salesman_designated_areas`;
CREATE TABLE IF NOT EXISTS `salesman_designated_areas` (
  `salesman_designated_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `salesman_id` int(10) UNSIGNED NOT NULL,
  `designated_area_id` int(10) UNSIGNED NOT NULL,
  `designated_status` varchar(50) NOT NULL,
  PRIMARY KEY (`salesman_designated_id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `salesman_designated_areas`
--

INSERT INTO `salesman_designated_areas` (`salesman_designated_id`, `salesman_id`, `designated_area_id`, `designated_status`) VALUES
(1, 2, 3, 'Active'),
(2, 3, 3, 'Active'),
(3, 3, 2, 'Active'),
(6, 4, 3, 'In Active'),
(7, 4, 2, 'In Active'),
(8, 4, 3, 'In Active'),
(9, 4, 2, 'In Active'),
(10, 1, 3, 'In Active'),
(11, 1, 5, 'In Active'),
(12, 2, 5, 'Active'),
(13, 1, 6, 'In Active');

-- --------------------------------------------------------

--
-- Table structure for table `salesman_info`
--

DROP TABLE IF EXISTS `salesman_info`;
CREATE TABLE IF NOT EXISTS `salesman_info` (
  `salesman_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `salesman_name` varchar(200) NOT NULL,
  `salesman_contact` varchar(60) NOT NULL,
  `salesman_address` varchar(250) NOT NULL,
  `salesman_cnic` varchar(50) NOT NULL,
  `salesman_status` varchar(50) NOT NULL,
  PRIMARY KEY (`salesman_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `salesman_info`
--

INSERT INTO `salesman_info` (`salesman_id`, `salesman_name`, `salesman_contact`, `salesman_address`, `salesman_cnic`, `salesman_status`) VALUES
(1, 'Salesman Name 1 Test', 'Salesman Contact 1 Test', 'Salesman Address 1 Test', 'Salesman CNIC 1 Test', 'In Active'),
(2, 'wdwqd', 'dwdqwwd', 'wdqwd', 'dwq', 'Active'),
(3, 'dwdw', 'swqdwq', 'dwqd', 'eweqwd', 'Active'),
(4, 'Salesman 4', 'Salesman 4', 'Salesman 4', 'Salesman 4', 'In Active');

-- --------------------------------------------------------

--
-- Table structure for table `subarea_info`
--

DROP TABLE IF EXISTS `subarea_info`;
CREATE TABLE IF NOT EXISTS `subarea_info` (
  `subarea_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `area_id` int(10) UNSIGNED NOT NULL,
  `sub_area_name` varchar(250) NOT NULL,
  `subarea_status` varchar(50) NOT NULL,
  PRIMARY KEY (`subarea_id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subarea_info`
--

INSERT INTO `subarea_info` (`subarea_id`, `area_id`, `sub_area_name`, `subarea_status`) VALUES
(2, 1, 'Area ABC', 'Active'),
(3, 1, 'Area XYZ', 'Active'),
(4, 2, 'Sub 1', 'Active'),
(5, 2, 'Sub 2', 'Active'),
(6, 2, 'Sub 3', 'Active'),
(7, 2, 'Sub 4', 'Active'),
(9, 2, 'Sub 6', 'Active'),
(10, 2, 'Sub 7 Test', 'In Active');

-- --------------------------------------------------------

--
-- Table structure for table `supplier_info`
--

DROP TABLE IF EXISTS `supplier_info`;
CREATE TABLE IF NOT EXISTS `supplier_info` (
  `supplier_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_id` int(10) UNSIGNED NOT NULL,
  `supplier_name` varchar(100) NOT NULL,
  `supplier_contact` varchar(60) NOT NULL,
  `supplier_address` varchar(200) NOT NULL,
  `supplier_status` varchar(50) NOT NULL,
  PRIMARY KEY (`supplier_id`)
) ENGINE=MyISAM AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier_info`
--

INSERT INTO `supplier_info` (`supplier_id`, `company_id`, `supplier_name`, `supplier_contact`, `supplier_address`, `supplier_status`) VALUES
(1, 2, 'Supplier Name 1', 'Supplier Contact 1', 'Supplier Address 1', 'Active'),
(2, 2, 'TCS', 'Supplier Contact 1', 'Supplier Address 1', 'Active'),
(4, 2, 'Leopard', 'Supplier Contact 1', 'Supplier Address 1', 'Active'),
(50, 1, 'abc Name', '243432', 'abc address', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `sync_tracking`
--

DROP TABLE IF EXISTS `sync_tracking`;
CREATE TABLE IF NOT EXISTS `sync_tracking` (
  `data_id` int(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `data_name` varchar(30) NOT NULL,
  `last_modified` varchar(50) NOT NULL,
  PRIMARY KEY (`data_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sync_tracking`
--

INSERT INTO `sync_tracking` (`data_id`, `data_name`, `last_modified`) VALUES
(1, 'subarea_info', '15/Mar/2020'),
(2, 'dealer_info', '15/Mar/2020'),
(3, 'product_info', '15/Mar/2020'),
(4, 'invoices', '15/Mar/2020'),
(5, 'dealer_pending_payments', '15/Mar/2020'),
(6, 'dealer_gps_locations', '15/Mar/2020');

-- --------------------------------------------------------

--
-- Table structure for table `user_accounts`
--

DROP TABLE IF EXISTS `user_accounts`;
CREATE TABLE IF NOT EXISTS `user_accounts` (
  `account_id` int(10) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(100) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_password` varchar(300) NOT NULL,
  `user_power` varchar(50) NOT NULL,
  `reg_date` varchar(50) NOT NULL,
  `current_status` varchar(30) NOT NULL,
  `user_status` varchar(50) NOT NULL,
  PRIMARY KEY (`account_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_accounts`
--

INSERT INTO `user_accounts` (`account_id`, `user_name`, `user_id`, `user_password`, `user_power`, `reg_date`, `current_status`, `user_status`) VALUES
(2, 'Saffi', 123, '123', 'Salesman', '10/Feb/2020', 'on', 'Approved'),
(6, 'Saffi', 456, '123', 'Salesman', '10/Feb/2020', 'off', 'Declined'),
(7, 'Saffi Test', 1, '1234', 'Salesman', '10/Feb/2020', 'on', 'Approved'),
(8, 'Saffi', 2, '123', 'Salesman', '10/Feb/2020', 'on', 'Approved');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
