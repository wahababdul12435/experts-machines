package com.example.younastraders;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.widget.Toast;

import androidx.annotation.Nullable;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class SyncMysql extends Service{

    boolean check;



    final class ThreadClass implements Runnable {
        String stDate;
        String stTime;
        String userId;
        SendData objSendData;
        DatabaseSync objDatabaseSync;
        GetData objGetData;
        GlobalVariables objGlobal = new GlobalVariables(SyncMysql.this);

        int serviceId;

        ThreadClass(int serviceId) {
            this.serviceId = serviceId;
        }

        public void run() {
            synchronized (this) {
                objDatabaseSync = new DatabaseSync(SyncMysql.this);
                objGetData = new GetData(SyncMysql.this);
                Cursor res = objGetData.getAreaIds();
                while(res.moveToNext())
                {
                    DatabaseSync.preAreaIds.add(res.getString(0));
                }
                res = objGetData.getDealerIds();
                while(res.moveToNext())
                {
                    DatabaseSync.preDealerIds.add(res.getString(0));
                }
                res = objGetData.getProductIds();
                while(res.moveToNext())
                {
                    DatabaseSync.preProductIds.add(res.getString(0));
                }
                check = objDatabaseSync.insertAllRecord();
                objGlobal.getPending();
                stopSelf(serviceId);
            }
        }

    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //        serviceStatus = true;
//        Toast.makeText(this, "Service Started", Toast.LENGTH_LONG).show();
        Thread thread = new Thread(new ThreadClass(startId));
        thread.start();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        //        serviceStatus = false;
//        Toast.makeText(this, "Service Stopped", Toast.LENGTH_LONG).show();
        super.onDestroy();
//        if(check)
//        {
//            Toast.makeText(getApplicationContext(), "Data Synced", Toast.LENGTH_SHORT).show();
//        }
//        else
//        {
//            Toast.makeText(getApplicationContext(), "Data Not Synced", Toast.LENGTH_SHORT).show();
//        }
//        Intent dialogIntent = new Intent(this, Home.class);
//        dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        startActivity(dialogIntent);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
