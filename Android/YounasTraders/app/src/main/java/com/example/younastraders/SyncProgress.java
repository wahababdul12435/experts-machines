package com.example.younastraders;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

public class SyncProgress extends Activity {

    boolean check;
    DatabaseSync objDatabaseSync;
    Handler handler;
    public static int syncedComplete = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync_progress);
        syncedComplete = 0;

        Intent intent = new Intent(this, SyncMysql.class);
        startService(intent);

        handler=new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent=new Intent(SyncProgress.this, Home.class);
                startActivity(intent);
                finish();
            }
        },12000);

    }

//    @Override
//    protected void onStart() {
//        super.onStart();
//        objDatabaseSync = new DatabaseSync(this);
//        check = objDatabaseSync.insertAllRecord();
//
//        while(syncedComplete < 6)
//        {
//
//        }
//
//        Toast.makeText(getApplicationContext(), "Done", Toast.LENGTH_SHORT).show();
//
//
//    }
}
