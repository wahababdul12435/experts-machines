package com.example.younastraders;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import java.io.File;

public class DatabaseSQLite extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "YounasTraders.db";

    public static final String TABLE_NAME = "order_info";
    public static final String COL_1 = "id";
    public static final String COL_2 = "dealer_id";
    public static final String COL_3 = "product_id";
    public static final String COL_4 = "quantity";
    public static final String COL_5 = "unit";
    public static final String COL_6 = "order_price";
    public static final String COL_7 = "bonus";
    public static final String COL_8 = "discount";
    public static final String COL_9 = "final_price";
    public static final String COL_10 = "latitude";
    public static final String COL_11 = "longitude";
    public static final String COL_12 = "order_place_area";
    public static final String COL_13 = "date";
    public static final String COL_14 = "time";
    public static final String COL_15 = "salesman_id";
    public static final String COL_16 = "status";

    public static final String DEALER_INFO_TABLE = "dealer_info";
    public static final String DEALER_COL_1 = "dealer_id";
    public static final String DEALER_COL_2 = "dealer_area_id";
    public static final String DEALER_COL_3 = "dealer_name";
    public static final String DEALER_COL_4 = "dealer_contact";
    public static final String DEALER_COL_5 = "dealer_address";
    public static final String DEALER_COL_6 = "dealer_type";
    public static final String DEALER_COL_7 = "dealer_cnic";
    public static final String DEALER_COL_8 = "dealer_lic_num";
    public static final String DEALER_COL_9 = "dealer_lic_exp";
    public static final String DEALER_COL_10 = "dealer_status";

    public static final String AREA_INFO_TABLE = "area_info";
    public static final String AREA_COL_1 = "area_id";
    public static final String AREA_COL_2 = "city_id";
    public static final String AREA_COL_3 = "area_name";
    public static final String AREA_COL_4 = "area_status";

    public static final String PRODUCT_INFO_TABLE = "product_info";
    public static final String PRODUCT_COL_1 = "product_id";
    public static final String PRODUCT_COL_2 = "product_name";
    public static final String PRODUCT_COL_3 = "final_price";
    public static final String PRODUCT_COL_4 = "product_status";
    public static final String PRODUCT_COL_5 = "company_name";

    public static final String USER_ACCOUNTS_TABLE = "user_accounts";
    public static final String ACCOUNTS_COL_1 = "account_id";
    public static final String ACCOUNTS_COL_2 = "user_name";
    public static final String ACCOUNTS_COL_3 = "software_id";
    public static final String ACCOUNTS_COL_4 = "user_id";
    public static final String ACCOUNTS_COL_5 = "user_password";
    public static final String ACCOUNTS_COL_6 = "user_power";
    public static final String ACCOUNTS_COL_7 = "reg_date";
    public static final String ACCOUNTS_COL_8 = "auto_send";
    public static final String ACCOUNTS_COL_9 = "user_status";

    public static final String INVOICES_TABLE = "invoices_data";
    public static final String INVOICES_COL_1 = "invoices_id";
    public static final String INVOICES_COL_2 = "dealer_id";
    public static final String INVOICES_COL_3 = "product_id";
    public static final String INVOICES_COL_4 = "quantity";
    public static final String INVOICES_COL_5 = "unit";
    public static final String INVOICES_COL_6 = "invoice_price";
    public static final String INVOICES_COL_7 = "invoice_status";

    public static final String SYNC_TRACKING_TABLE = "sync_tracking";
    public static final String SYNCTRACK_COL_1 = "data_id";
    public static final String SYNCTRACK_COL_2 = "data_name";
    public static final String SYNCTRACK_COL_3 = "last_update_date";
    public static final String SYNCTRACK_COL_4 = "last_update_time";

    public static final String MOBILE_LOCATION_TABLE = "mobile_location";
    public static final String MOBLOC_COL_1 = "mob_loc_id";
    public static final String MOBLOC_COL_2 = "user_id";
    public static final String MOBLOC_COL_3 = "latitude";
    public static final String MOBLOC_COL_4 = "longitude";
    public static final String MOBLOC_COL_5 = "loc_name";
    public static final String MOBLOC_COL_6 = "date";
    public static final String MOBLOC_COL_7 = "time";

    public static final String ORDER_GPS_LOCATION_TABLE = "order_gps_location";
    public static final String ORDLOC_COL_1 = "order_loc_id";
    public static final String ORDLOC_COL_2 = "order_id";
    public static final String ORDLOC_COL_3 = "user_id";
    public static final String ORDLOC_COL_4 = "latitude";
    public static final String ORDLOC_COL_5 = "longitude";
    public static final String ORDLOC_COL_6 = "loc_name";
    public static final String ORDLOC_COL_7 = "date";
    public static final String ORDLOC_COL_8 = "time";
    public static final String ORDLOC_COL_9 = "dealer_id";
    public static final String ORDLOC_COL_10 = "price";
    public static final String ORDLOC_COL_11 = "type";

    public static final String DEALER_GPS_LOC_TABLE = "dealer_gps_location";
    public static final String DEALGPS_COL_1 = "dealer_loc_id";
    public static final String DEALGPS_COL_2 = "dealer_id";
    public static final String DEALGPS_COL_3 = "latitude";
    public static final String DEALGPS_COL_4 = "longitude";
    public static final String DEALGPS_COL_5 = "loc_name";
    public static final String DEALGPS_COL_6 = "date";
    public static final String DEALGPS_COL_7 = "time";
    public static final String DEALGPS_COL_8 = "user_id";

    public static final String DEALER_PENDINGS_TABLE = "dealer_pending_payments";
    public static final String DEALPENDING_COL_1 = "dealer_id";
    public static final String DEALPENDING_COL_2 = "payments";

    public static final String DELIVERED_TABLE = "invoice_delivered";
    public static final String DELIVERED_COL_1 = "order_id";
    public static final String DELIVERED_COL_2 = "dealer_id";
    public static final String DELIVERED_COL_3 = "cash_collected";
    public static final String DELIVERED_COL_4 = "return_bit";
    public static final String DELIVERED_COL_5 = "latitude";
    public static final String DELIVERED_COL_6 = "longitude";
    public static final String DELIVERED_COL_7 = "delivered_location";
    public static final String DELIVERED_COL_8 = "date";
    public static final String DELIVERED_COL_9 = "time";
    public static final String DELIVERED_COL_10 = "user_id";
    public static final String DELIVERED_COL_11 = "status";

    public static final String DEALERS_PAYMENTS_TABLE = "dealer_payments";
    public static final String DEALPAYMENTS_COL_1 = "payment_id";
    public static final String DEALPAYMENTS_COL_2 = "dealer_id";
    public static final String DEALPAYMENTS_COL_3 = "order_id";
    public static final String DEALPAYMENTS_COL_4 = "cash";
    public static final String DEALPAYMENTS_COL_5 = "cash_type";
    public static final String DEALPAYMENTS_COL_6 = "latitude";
    public static final String DEALPAYMENTS_COL_7 = "longitude";
    public static final String DEALPAYMENTS_COL_8 = "payments_location";
    public static final String DEALPAYMENTS_COL_9 = "date";
    public static final String DEALPAYMENTS_COL_10 = "time";
    public static final String DEALPAYMENTS_COL_11 = "user_id";
    public static final String DEALPAYMENTS_COL_12 = "status";

    public static final String BATCHWISE_STOCK_TABLE = "batchwise_stock";
    public static final String BATCHSTOCK_COL_1 = "batchstock_id";
    public static final String BATCHSTOCK_COL_2 = "product_id";
    public static final String BATCHSTOCK_COL_3 = "batch_no";
    public static final String BATCHSTOCK_COL_4 = "quantity";
    public static final String BATCHSTOCK_COL_5 = "expiry_date";

    public static final String RETURN_TABLE = "order_return";
    public static final String RETURN_COL_1 = "return_id";
    public static final String RETURN_COL_2 = "order_id";
    public static final String RETURN_COL_3 = "dealer_id";
    public static final String RETURN_COL_4 = "product_id";
    public static final String RETURN_COL_5 = "quantity";
    public static final String RETURN_COL_6 = "unit";
    public static final String RETURN_COL_7 = "batch_no";
    public static final String RETURN_COL_8 = "return_price";
    public static final String RETURN_COL_9 = "total_return_price";
    public static final String RETURN_COL_10 = "return_reason";
    public static final String RETURN_COL_11 = "cash_return";
    public static final String RETURN_COL_12 = "latitude";
    public static final String RETURN_COL_13 = "longitude";
    public static final String RETURN_COL_14 = "return_location";
    public static final String RETURN_COL_15 = "date";
    public static final String RETURN_COL_16 = "time";
    public static final String RETURN_COL_17 = "user_id";
    public static final String RETURN_COL_18 = "status";

    public static final String DISCOUNT_TABLE = "discount_info";
    public static final String DISCOUNT_COL_1 = "approval_id";
    public static final String DISCOUNT_COL_2 = "dealer_id";
    public static final String DISCOUNT_COL_3 = "company_name";
    public static final String DISCOUNT_COL_4 = "product_id";
    public static final String DISCOUNT_COL_5 = "sale_amount";
    public static final String DISCOUNT_COL_6 = "discount";
    public static final String DISCOUNT_COL_7 = "start_date";
    public static final String DISCOUNT_COL_8 = "end_date";

    public static final String BONUS_TABLE = "bonus_info";
    public static final String BONUS_COL_1 = "approval_id";
    public static final String BONUS_COL_2 = "dealer_id";
    public static final String BONUS_COL_3 = "company_name";
    public static final String BONUS_COL_4 = "product_id";
    public static final String BONUS_COL_5 = "quantity";
    public static final String BONUS_COL_6 = "bonus";
    public static final String BONUS_COL_7 = "start_date";
    public static final String BONUS_COL_8 = "end_date";

    public static SQLiteDatabase db;

    private Context ctx;

    public DatabaseSQLite(Context context) {
        super(context, DATABASE_NAME, null, 1);
        this.ctx = context;
        db = this.getWritableDatabase();
        checkTablesExist();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table "+TABLE_NAME+"("+COL_1+" INTEGER PRIMARY KEY AUTOINCREMENT, "+COL_2+" VARCHAR(100), "+COL_3+" VARCHAR(700), "+COL_4+" VARCHAR(300), "+COL_5+" VARCHAR(800), "+COL_6+" VARCHAR(500), "+COL_7+" VARCHAR(500), "+COL_8+" VARCHAR(400), "+COL_9+" VARCHAR(500), "+COL_10+" VARCHAR(20), "+COL_11+" VARCHAR(20), "+COL_12+" VARCHAR(300), "+COL_13+" VARCHAR(30), "+COL_14+" VARCHAR(30), "+COL_15+" VARCHAR(50), "+COL_16+" VARCHAR(30))");
        db.execSQL("create table "+AREA_INFO_TABLE+"("+AREA_COL_1+" INTEGER PRIMARY KEY, "+AREA_COL_2+" INTEGER, "+AREA_COL_3+" VARCHAR(200), "+AREA_COL_4+" VARCHAR(50))");
        db.execSQL("create table "+DEALER_INFO_TABLE+"("+DEALER_COL_1+" INTEGER PRIMARY KEY, "+DEALER_COL_2+" INTEGER, "+DEALER_COL_3+" VARCHAR(150), "+DEALER_COL_4+" VARCHAR(50), "+DEALER_COL_5+" VARCHAR(200), "+DEALER_COL_6+" VARCHAR(50), "+DEALER_COL_7+" VARCHAR(50), "+DEALER_COL_8+" VARCHAR(100), "+DEALER_COL_9+" VARCHAR(50), "+DEALER_COL_10+" VARCHAR(50))");
        db.execSQL("create table "+PRODUCT_INFO_TABLE+"("+PRODUCT_COL_1+" INTEGER PRIMARY KEY, "+PRODUCT_COL_2+" VARCHAR(200), "+PRODUCT_COL_3+" FLOAT, "+PRODUCT_COL_4+" VARCHAR(50), "+PRODUCT_COL_5+" VARCHAR(100))");
        db.execSQL("create table "+USER_ACCOUNTS_TABLE+"("+ACCOUNTS_COL_1+" INTEGER PRIMARY KEY, "+ACCOUNTS_COL_2+" VARCHAR(100), "+ACCOUNTS_COL_3+" INTEGER, "+ACCOUNTS_COL_4+" INTEGER, "+ACCOUNTS_COL_5+" VARCHAR(300), "+ACCOUNTS_COL_6+" VARCHAR(50), "+ACCOUNTS_COL_7+" VARCHAR(50), "+ACCOUNTS_COL_8+" VARCHAR(5), "+ACCOUNTS_COL_9+" VARCHAR(50))");
        db.execSQL("create table "+INVOICES_TABLE+"("+INVOICES_COL_1+" INTEGER(10), "+INVOICES_COL_2+" INTEGER(10), "+INVOICES_COL_3+" VARCHAR(800), "+INVOICES_COL_4+" VARCHAR(600), "+INVOICES_COL_5+" VARCHAR(800), "+INVOICES_COL_6+" FLOAT(10), "+INVOICES_COL_7+" VARCHAR(30))");
        db.execSQL("create table "+SYNC_TRACKING_TABLE+"("+SYNCTRACK_COL_1+" INTEGER PRIMARY KEY AUTOINCREMENT, "+SYNCTRACK_COL_2+" VARCHAR(30), "+SYNCTRACK_COL_3+" VARCHAR(20), "+SYNCTRACK_COL_4+" VARCHAR(20))");
        db.execSQL("create table "+MOBILE_LOCATION_TABLE+"("+MOBLOC_COL_1+" INTEGER PRIMARY KEY AUTOINCREMENT, "+MOBLOC_COL_2+" VARCHAR(10), "+MOBLOC_COL_3+" VARCHAR(20), "+MOBLOC_COL_4+" VARCHAR(20), "+MOBLOC_COL_5+" VARCHAR(150), "+MOBLOC_COL_6+" VARCHAR(30), "+MOBLOC_COL_7+" VARCHAR(30))");
        db.execSQL("create table "+ORDER_GPS_LOCATION_TABLE+"("+ORDLOC_COL_1+" INTEGER PRIMARY KEY AUTOINCREMENT, "+ORDLOC_COL_2+" INTEGER(10) DEFAULT '0', "+ORDLOC_COL_3+" VARCHAR(10), "+ORDLOC_COL_4+" VARCHAR(20), "+ORDLOC_COL_5+" VARCHAR(20), "+ORDLOC_COL_6+" VARCHAR(150), "+ORDLOC_COL_7+" VARCHAR(30), "+ORDLOC_COL_8+" VARCHAR(30), "+ORDLOC_COL_9+" INTEGER(10), "+ORDLOC_COL_10+" FLOAT(10), "+ORDLOC_COL_11+" VARCHAR(20))");
        db.execSQL("create table "+DEALER_GPS_LOC_TABLE+"("+DEALGPS_COL_1+" INTEGER PRIMARY KEY AUTOINCREMENT, "+DEALGPS_COL_2+" INTEGER(10) UNIQUE, "+DEALGPS_COL_3+" VARCHAR(25), "+DEALGPS_COL_4+" VARCHAR(25), "+DEALGPS_COL_5+" VARCHAR(150), "+DEALGPS_COL_6+" VARCHAR(20), "+DEALGPS_COL_7+" VARCHAR(20), "+DEALGPS_COL_8+" VARCHAR(10))");
        db.execSQL("create table "+DEALER_PENDINGS_TABLE+"("+DEALPENDING_COL_1+" INTEGER(10) UNIQUE, "+DEALPENDING_COL_2+" FLOAT(10))");
        db.execSQL("create table "+DELIVERED_TABLE+"("+DELIVERED_COL_1+" INTEGER(10) UNIQUE, "+DELIVERED_COL_2+" INTEGER(10), "+DELIVERED_COL_3+" FLOAT(10), "+DELIVERED_COL_4+" BOOLEAN, "+DELIVERED_COL_5+" VARCHAR(25), "+DELIVERED_COL_6+" VARCHAR(25), "+DELIVERED_COL_7+" VARCHAR(150), "+DELIVERED_COL_8+" VARCHAR(20), "+DELIVERED_COL_9+" VARCHAR(20), "+DELIVERED_COL_10+" INTEGER(10), "+DELIVERED_COL_11+" VARCHAR(20))");
        db.execSQL("create table "+DEALERS_PAYMENTS_TABLE+"("+DEALPAYMENTS_COL_1+" INTEGER PRIMARY KEY AUTOINCREMENT, "+DEALPAYMENTS_COL_2+" INTEGER(10), "+DEALPAYMENTS_COL_3+" INTEGER(10) DEFAULT '0', "+DEALPAYMENTS_COL_4+" FLOAT(10), "+DEALPAYMENTS_COL_5+" VARCHAR(20), "+DEALPAYMENTS_COL_6+" VARCHAR(25), "+DEALPAYMENTS_COL_7+" VARCHAR(25), "+DEALPAYMENTS_COL_8+" VARCHAR(150), "+DEALPAYMENTS_COL_9+" VARCHAR(20), "+DEALPAYMENTS_COL_10+" VARCHAR(20), "+DEALPAYMENTS_COL_11+" INTEGER(10), "+DEALPAYMENTS_COL_12+" VARCHAR(30))");
        db.execSQL("create table "+BATCHWISE_STOCK_TABLE+"("+BATCHSTOCK_COL_1+" INTEGER(10), "+BATCHSTOCK_COL_2+" INTEGER(10), "+BATCHSTOCK_COL_3+" VARCHAR(50), "+BATCHSTOCK_COL_4+" INTEGER(10), "+BATCHSTOCK_COL_5+" VARCHAR(30))");
        db.execSQL("create table "+RETURN_TABLE+"("+RETURN_COL_1+" INTEGER PRIMARY KEY AUTOINCREMENT, "+RETURN_COL_2+" INTEGER(10), "+RETURN_COL_3+" INTEGER(10), "+RETURN_COL_4+" VARCHAR(500), "+RETURN_COL_5+" VARCHAR(300), "+RETURN_COL_6+" VARCHAR(500), "+RETURN_COL_7+" VARCHAR(500), "+RETURN_COL_8+" VARCHAR(300), "+RETURN_COL_9+" FLOAT(10), "+RETURN_COL_10+" VARCHAR(600), "+RETURN_COL_11+" FLOAT(10), "+RETURN_COL_12+" VARCHAR(20), "+RETURN_COL_13+" VARCHAR(20), "+RETURN_COL_14+" VARCHAR(150), "+RETURN_COL_15+" VARCHAR(20), "+RETURN_COL_16+" VARCHAR(20), "+RETURN_COL_17+" INTEGER(10), "+RETURN_COL_18+" VARCHAR(20))");
        db.execSQL("create table "+DISCOUNT_TABLE+"("+DISCOUNT_COL_1+" INTEGER(10), "+DISCOUNT_COL_2+" INTEGER(10), "+DISCOUNT_COL_3+" VARCHAR(100), "+DISCOUNT_COL_4+" INTEGER(10), "+DISCOUNT_COL_5+" FLOAT(10), "+DISCOUNT_COL_6+" FLOAT(10), "+DISCOUNT_COL_7+" VARCHAR(20), "+DISCOUNT_COL_8+" VARCHAR(20))");
        db.execSQL("create table "+BONUS_TABLE+"("+BONUS_COL_1+" INTEGER(10), "+BONUS_COL_2+" INTEGER(10), "+BONUS_COL_3+" VARCHAR(100), "+BONUS_COL_4+" INTEGER(10), "+BONUS_COL_5+" INTEGER(10), "+BONUS_COL_6+" INTEGER(10), "+BONUS_COL_7+" VARCHAR(20), "+BONUS_COL_8+" VARCHAR(20))");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS "+AREA_INFO_TABLE);
        db.execSQL("DROP TABLE IF EXISTS "+DEALER_INFO_TABLE);
        db.execSQL("DROP TABLE IF EXISTS "+PRODUCT_INFO_TABLE);
        db.execSQL("DROP TABLE IF EXISTS "+USER_ACCOUNTS_TABLE);
        db.execSQL("DROP TABLE IF EXISTS "+INVOICES_TABLE);
        db.execSQL("DROP TABLE IF EXISTS "+SYNC_TRACKING_TABLE);
        db.execSQL("DROP TABLE IF EXISTS "+MOBILE_LOCATION_TABLE);
        db.execSQL("DROP TABLE IF EXISTS "+ORDER_GPS_LOCATION_TABLE);
        db.execSQL("DROP TABLE IF EXISTS "+DEALER_GPS_LOC_TABLE);
        db.execSQL("DROP TABLE IF EXISTS "+DEALER_PENDINGS_TABLE);
        db.execSQL("DROP TABLE IF EXISTS "+DELIVERED_TABLE);
        db.execSQL("DROP TABLE IF EXISTS "+DEALER_PENDINGS_TABLE);
        db.execSQL("DROP TABLE IF EXISTS "+DEALERS_PAYMENTS_TABLE);
        db.execSQL("DROP TABLE IF EXISTS "+BATCHWISE_STOCK_TABLE);
        db.execSQL("DROP TABLE IF EXISTS "+RETURN_TABLE);
        db.execSQL("DROP TABLE IF EXISTS "+DISCOUNT_TABLE);
        db.execSQL("DROP TABLE IF EXISTS "+BONUS_TABLE);
        onCreate(db);
    }

    public void createOrderTable(SQLiteDatabase db)
    {
        db.execSQL("create table "+TABLE_NAME+"("+COL_1+" INTEGER PRIMARY KEY AUTOINCREMENT, "+COL_2+" VARCHAR(100), "+COL_3+" VARCHAR(700), "+COL_4+" VARCHAR(300), "+COL_5+" VARCHAR(800), "+COL_6+" FLOAT, "+COL_7+" VARCHAR(500), "+COL_8+" FLOAT, "+COL_9+" FLOAT, "+COL_10+" FLOAT, "+COL_11+" FLOAT, "+COL_12+" VARCHAR(300), "+COL_13+" VARCHAR(30), "+COL_14+" VARCHAR(30), "+COL_15+" VARCHAR(50), "+COL_16+" VARCHAR(30))");
    }
    public void createSubareaTable(SQLiteDatabase db)
    {
        db.execSQL("create table "+AREA_INFO_TABLE+"("+AREA_COL_1+" INTEGER PRIMARY KEY, "+AREA_COL_2+" INTEGER, "+AREA_COL_3+" VARCHAR(200), "+AREA_COL_4+" VARCHAR(50))");
    }
    public void createDealerTable(SQLiteDatabase db)
    {
        db.execSQL("create table "+DEALER_INFO_TABLE+"("+DEALER_COL_1+" INTEGER PRIMARY KEY, "+DEALER_COL_2+" INTEGER, "+DEALER_COL_3+" VARCHAR(150), "+DEALER_COL_4+" VARCHAR(50), "+DEALER_COL_5+" VARCHAR(200), "+DEALER_COL_6+" VARCHAR(50), "+DEALER_COL_7+" VARCHAR(50), "+DEALER_COL_8+" VARCHAR(100), "+DEALER_COL_9+" VARCHAR(50), "+DEALER_COL_10+" VARCHAR(50))");
    }
    public void createProductTable(SQLiteDatabase db)
    {
        db.execSQL("create table "+PRODUCT_INFO_TABLE+"("+PRODUCT_COL_1+" INTEGER PRIMARY KEY, "+PRODUCT_COL_2+" VARCHAR(200), "+PRODUCT_COL_3+" FLOAT, "+PRODUCT_COL_4+" VARCHAR(50), "+PRODUCT_COL_5+" VARCHAR(100))");
    }
    public void createUserAccountsTable(SQLiteDatabase db)
    {
        db.execSQL("create table "+USER_ACCOUNTS_TABLE+"("+ACCOUNTS_COL_1+" INTEGER PRIMARY KEY, "+ACCOUNTS_COL_2+" VARCHAR(100), "+ACCOUNTS_COL_3+" INTEGER, "+ACCOUNTS_COL_4+" INTEGER, "+ACCOUNTS_COL_5+" VARCHAR(300), "+ACCOUNTS_COL_6+" VARCHAR(50), "+ACCOUNTS_COL_7+" VARCHAR(50), "+ACCOUNTS_COL_8+" VARCHAR(5), "+ACCOUNTS_COL_9+" VARCHAR(50))");
    }
    public void createInvoicesTable(SQLiteDatabase db)
    {
        db.execSQL("create table "+INVOICES_TABLE+"("+INVOICES_COL_1+" INTEGER(10), "+INVOICES_COL_2+" INTEGER(10), "+INVOICES_COL_3+" VARCHAR(800), "+INVOICES_COL_4+" VARCHAR(600), "+INVOICES_COL_5+" VARCHAR(800), "+INVOICES_COL_6+" FLOAT(10), "+INVOICES_COL_7+" VARCHAR(30))");
    }
    public void createSyncTrackTable(SQLiteDatabase db)
    {
        db.execSQL("create table "+SYNC_TRACKING_TABLE+"("+SYNCTRACK_COL_1+" INTEGER(5), "+SYNCTRACK_COL_2+" VARCHAR(30), "+SYNCTRACK_COL_3+" VARCHAR(20), "+SYNCTRACK_COL_4+" VARCHAR(20))");
    }
    public void createMobileLocationTable(SQLiteDatabase db)
    {
        db.execSQL("create table "+MOBILE_LOCATION_TABLE+"("+MOBLOC_COL_1+" INTEGER(5), "+MOBLOC_COL_2+" VARCHAR(10), "+MOBLOC_COL_3+" VARCHAR(20), "+MOBLOC_COL_4+" VARCHAR(20), "+MOBLOC_COL_5+" VARCHAR(150), "+MOBLOC_COL_6+" VARCHAR(30), "+MOBLOC_COL_7+" VARCHAR(30))");
    }
    public void createOrderLocationTable(SQLiteDatabase db)
    {
        db.execSQL("create table "+ORDER_GPS_LOCATION_TABLE+"("+ORDLOC_COL_1+" INTEGER PRIMARY KEY AUTOINCREMENT, "+ORDLOC_COL_2+" INTEGER(10) DEFAULT '0', "+ORDLOC_COL_3+" VARCHAR(10), "+ORDLOC_COL_4+" VARCHAR(20), "+ORDLOC_COL_5+" VARCHAR(20), "+ORDLOC_COL_6+" VARCHAR(150), "+ORDLOC_COL_7+" VARCHAR(30), "+ORDLOC_COL_8+" VARCHAR(30), "+ORDLOC_COL_9+" INTEGER(10), "+ORDLOC_COL_10+" FLOAT(10), "+ORDLOC_COL_11+" VARCHAR(20))");
    }
    public void createDealerGPSTable(SQLiteDatabase db)
    {
        db.execSQL("create table "+DEALER_GPS_LOC_TABLE+"("+DEALGPS_COL_1+" INTEGER PRIMARY KEY AUTOINCREMENT, "+DEALGPS_COL_2+" INTEGER(10) UNIQUE, "+DEALGPS_COL_3+" VARCHAR(25), "+DEALGPS_COL_4+" VARCHAR(25), "+DEALGPS_COL_5+" VARCHAR(150), "+DEALGPS_COL_6+" VARCHAR(20), "+DEALGPS_COL_7+" VARCHAR(20), "+DEALGPS_COL_8+" VARCHAR(10))");
    }
    public void createDealerPendingsTable(SQLiteDatabase db)
    {
        db.execSQL("create table "+DEALER_PENDINGS_TABLE+"("+DEALPENDING_COL_1+" INTEGER(10) UNIQUE, "+DEALPENDING_COL_2+" FLOAT(10))");
    }
    public void createDeliveryTable(SQLiteDatabase db)
    {
        db.execSQL("create table "+DELIVERED_TABLE+"("+DELIVERED_COL_1+" INTEGER(10) UNIQUE, "+DELIVERED_COL_2+" INTEGER(10), "+DELIVERED_COL_3+" FLOAT(10), "+DELIVERED_COL_4+" BOOLEAN, "+DELIVERED_COL_5+" VARCHAR(25), "+DELIVERED_COL_6+" VARCHAR(25), "+DELIVERED_COL_7+" VARCHAR(150), "+DELIVERED_COL_8+" VARCHAR(20), "+DELIVERED_COL_9+" VARCHAR(20), "+DELIVERED_COL_10+" INTEGER(10), "+DELIVERED_COL_11+" VARCHAR(20))");
    }
    public void createDealersPaymentsTable(SQLiteDatabase db)
    {
        db.execSQL("create table "+DEALERS_PAYMENTS_TABLE+"("+DEALPAYMENTS_COL_1+" INTEGER PRIMARY KEY AUTOINCREMENT, "+DEALPAYMENTS_COL_2+" INTEGER(10), "+DEALPAYMENTS_COL_3+" INTEGER(10) DEFAULT '0', "+DEALPAYMENTS_COL_4+" FLOAT(10), "+DEALPAYMENTS_COL_5+" VARCHAR(20), "+DEALPAYMENTS_COL_6+" VARCHAR(25), "+DEALPAYMENTS_COL_7+" VARCHAR(25), "+DEALPAYMENTS_COL_8+" VARCHAR(150), "+DEALPAYMENTS_COL_9+" VARCHAR(20), "+DEALPAYMENTS_COL_10+" VARCHAR(20), "+DEALPAYMENTS_COL_11+" INTEGER(10), "+DEALPAYMENTS_COL_12+" VARCHAR(30))");
    }
    public void createBatchwiseStockTable(SQLiteDatabase db)
    {
        db.execSQL("create table "+BATCHWISE_STOCK_TABLE+"("+BATCHSTOCK_COL_1+" INTEGER(10), "+BATCHSTOCK_COL_2+" INTEGER(10), "+BATCHSTOCK_COL_3+" VARCHAR(50), "+BATCHSTOCK_COL_4+" INTEGER(10), "+BATCHSTOCK_COL_5+" VARCHAR(30))");
    }
    public void createReturnTable(SQLiteDatabase db)
    {
        db.execSQL("create table "+RETURN_TABLE+"("+RETURN_COL_1+" INTEGER PRIMARY KEY AUTOINCREMENT, "+RETURN_COL_2+" INTEGER(10), "+RETURN_COL_3+" INTEGER(10), "+RETURN_COL_4+" VARCHAR(500), "+RETURN_COL_5+" VARCHAR(300), "+RETURN_COL_6+" VARCHAR(500), "+RETURN_COL_7+" VARCHAR(500), "+RETURN_COL_8+" VARCHAR(300), "+RETURN_COL_9+" FLOAT(10), "+RETURN_COL_10+" VARCHAR(600), "+RETURN_COL_11+" FLOAT(10), "+RETURN_COL_12+" VARCHAR(20), "+RETURN_COL_13+" VARCHAR(20), "+RETURN_COL_14+" VARCHAR(150), "+RETURN_COL_15+" VARCHAR(20), "+RETURN_COL_16+" VARCHAR(20), "+RETURN_COL_17+" INTEGER(10), "+RETURN_COL_18+" VARCHAR(20))");
    }
    public void createDiscountTable(SQLiteDatabase db)
    {
        db.execSQL("create table "+DISCOUNT_TABLE+"("+DISCOUNT_COL_1+" INTEGER(10), "+DISCOUNT_COL_2+" INTEGER(10), "+DISCOUNT_COL_3+" VARCHAR(100), "+DISCOUNT_COL_4+" INTEGER(10), "+DISCOUNT_COL_5+" FLOAT(10), "+DISCOUNT_COL_6+" FLOAT(10), "+DISCOUNT_COL_7+" VARCHAR(20), "+DISCOUNT_COL_8+" VARCHAR(20))");
    }
    public void createBonusTable(SQLiteDatabase db)
    {
        db.execSQL("create table "+BONUS_TABLE+"("+BONUS_COL_1+" INTEGER(10), "+BONUS_COL_2+" INTEGER(10), "+BONUS_COL_3+" VARCHAR(100), "+BONUS_COL_4+" INTEGER(10), "+BONUS_COL_5+" INTEGER(10), "+BONUS_COL_6+" INTEGER(10), "+BONUS_COL_7+" VARCHAR(20), "+BONUS_COL_8+" VARCHAR(20))");
    }

    public void flushTables()
    {
        
        onUpgrade(db,0,0);
    }

    public boolean insertData(String dealerID, String productID, String quantity, String unit, String orderPrice, String bonus, String discount, String finalPrice, String latitude, String longitude, String order_place_area, String date, String time, String salesmanId, String status)
    {
        
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_2, dealerID);
        contentValues.put(COL_3, productID);
        contentValues.put(COL_4, quantity);
        contentValues.put(COL_5, unit);
        contentValues.put(COL_6, orderPrice);
        contentValues.put(COL_7, bonus);
        contentValues.put(COL_8, discount);
        contentValues.put(COL_9, finalPrice);
        contentValues.put(COL_10, latitude);
        contentValues.put(COL_11, longitude);
        contentValues.put(COL_12, order_place_area);
        contentValues.put(COL_13, date);
        contentValues.put(COL_14, time);
        contentValues.put(COL_15, salesmanId);
        contentValues.put(COL_16, status);
        long result = db.insert(TABLE_NAME, null, contentValues);
        if(result == -1)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean registerUser(String userName, String softwareId, String userId, String password, String userPower, String stDate, String autoSend, String status)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ACCOUNTS_COL_2, userName);
        contentValues.put(ACCOUNTS_COL_3, softwareId);
        contentValues.put(ACCOUNTS_COL_4, userId);
        contentValues.put(ACCOUNTS_COL_5, password);
        contentValues.put(ACCOUNTS_COL_6, userPower);
        contentValues.put(ACCOUNTS_COL_7, stDate);
        contentValues.put(ACCOUNTS_COL_8, autoSend);
        contentValues.put(ACCOUNTS_COL_9, status);
        long result = db.insert(USER_ACCOUNTS_TABLE, null, contentValues);
        if(result == -1)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

//    public boolean updateUser(String userName, String userId, String password, String userPower, String stDate, String status)
//    {
//        
//        ContentValues contentValues = new ContentValues();
//        contentValues.put(ACCOUNTS_COL_2, userName);
//        contentValues.put(ACCOUNTS_COL_3, userId);
//        contentValues.put(ACCOUNTS_COL_4, password);
//        contentValues.put(ACCOUNTS_COL_5, userPower);
//        contentValues.put(ACCOUNTS_COL_6, stDate);
//        contentValues.put(ACCOUNTS_COL_7, status);
//        long result = db.update(USER_ACCOUNTS_TABLE, contentValues, "user_id="+userId, null);
//        if(result == -1)
//        {
//            return false;
//        }
//        else
//        {
//            return true;
//        }
//    }

    public boolean updateUser(String userId, String... info)
    {
        ContentValues contentValues = new ContentValues();
        if(info[0].equals("status"))
        {
            contentValues.put(ACCOUNTS_COL_9, info[1]);
        }
        else
        {
            contentValues.put(ACCOUNTS_COL_2, info[1]);
            contentValues.put(ACCOUNTS_COL_4, info[2]);
            contentValues.put(ACCOUNTS_COL_7, info[3]);
        }

        long result = db.update(USER_ACCOUNTS_TABLE, contentValues, "user_id=?", new String[]{userId});
        if(result == -1)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean updateBookingData(String orderId, String dealerID, String productID, String quantity, String unit, String orderPrice, String bonus, String discount, String finalPrice, String latitude, String longitude, String order_place_area, String date, String time, String salesmanId, String status)
    {
        
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_2, dealerID);
        contentValues.put(COL_3, productID);
        contentValues.put(COL_4, quantity);
        contentValues.put(COL_5, unit);
        contentValues.put(COL_6, orderPrice);
        contentValues.put(COL_7, bonus);
        contentValues.put(COL_8, discount);
        contentValues.put(COL_9, finalPrice);
        contentValues.put(COL_10, latitude);
        contentValues.put(COL_11, longitude);
        contentValues.put(COL_12, order_place_area);
        contentValues.put(COL_13, date);
        contentValues.put(COL_14, time);
        contentValues.put(COL_15, salesmanId);
        contentValues.put(COL_16, status);
        int result = db.update(TABLE_NAME, contentValues, "id="+orderId, null);
        if(result <= 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean updateCurrentStatus(String orderId, String status)
    {
        
        ContentValues contentValues = new ContentValues();
        String[] orderIdArr = orderId.split("_--_");
        int result = 0;
        for(int i=0; i<orderIdArr.length; i++)
        {
            contentValues.put(COL_16, status);
            result = db.update(TABLE_NAME, contentValues, "id="+orderIdArr[i], null);
        }

        if(result <= 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean updateCashStatus(String paymentId, String status)
    {
        
        ContentValues contentValues = new ContentValues();
        contentValues.put(DEALPAYMENTS_COL_12, status);
        int result = db.update(DEALERS_PAYMENTS_TABLE, contentValues, "payment_id="+paymentId, null);
        if(result <= 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean updateReturnStatus(String returnId, String status)
    {
        
        ContentValues contentValues = new ContentValues();
        contentValues.put(RETURN_COL_18, status);
        int result = db.update(RETURN_TABLE, contentValues, "return_id="+returnId, null);
        if(result <= 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean updateDealerGPSLoc(String dealerID, String latitude, String longitude, String locationName, String date, String time, String userId)
    {
        
        ContentValues contentValues = new ContentValues();
        contentValues.put(DEALGPS_COL_3, latitude);
        contentValues.put(DEALGPS_COL_4, longitude);
        contentValues.put(DEALGPS_COL_5, locationName);
        contentValues.put(DEALGPS_COL_6, date);
        contentValues.put(DEALGPS_COL_7, time);
        contentValues.put(DEALGPS_COL_8, userId);
        int result = db.update(DEALER_GPS_LOC_TABLE, contentValues, "dealer_id="+dealerID, null);
        if(result <= 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public Cursor getAllData()
    {
        
        Cursor res = db.rawQuery("SELECT * FROM "+TABLE_NAME, null);
        return res;

    }

    public Cursor getAllPendingBookings()
    {
        
        Cursor res = db.rawQuery("SELECT * FROM "+TABLE_NAME+" WHERE status = 'Pending'", null);
        return res;

    }

    public void checkTablesExist()
    {
        

        if(!doesTableExist(db, TABLE_NAME))
        {
            createOrderTable(db);
        }

        if(!doesTableExist(db, AREA_INFO_TABLE))
        {
            createSubareaTable(db);
        }

        if(!doesTableExist(db, DEALER_INFO_TABLE))
        {
            createDealerTable(db);
        }

        if(!doesTableExist(db, PRODUCT_INFO_TABLE))
        {
            createProductTable(db);
        }

        if(!doesTableExist(db, USER_ACCOUNTS_TABLE))
        {
            createUserAccountsTable(db);
        }

        if(!doesTableExist(db, INVOICES_TABLE))
        {
            createInvoicesTable(db);
        }

        if(!doesTableExist(db, SYNC_TRACKING_TABLE))
        {
            createSyncTrackTable(db);
        }

        if(!doesTableExist(db, MOBILE_LOCATION_TABLE))
        {
            createMobileLocationTable(db);
        }

        if(!doesTableExist(db, ORDER_GPS_LOCATION_TABLE))
        {
            createOrderLocationTable(db);
        }

        if(!doesTableExist(db, DEALER_GPS_LOC_TABLE))
        {
            createDealerGPSTable(db);
        }

        if(!doesTableExist(db, DEALER_PENDINGS_TABLE))
        {
            createDealerPendingsTable(db);
        }

        if(!doesTableExist(db, DELIVERED_TABLE))
        {
            createDeliveryTable(db);
        }

        if(!doesTableExist(db, DEALERS_PAYMENTS_TABLE))
        {
            createDealersPaymentsTable(db);
        }

        if(!doesTableExist(db, BATCHWISE_STOCK_TABLE))
        {
            createBatchwiseStockTable(db);
        }

        if(!doesTableExist(db, RETURN_TABLE))
        {
            createReturnTable(db);
        }

        if(!doesTableExist(db, DISCOUNT_TABLE))
        {
            createDiscountTable(db);
        }

        if(!doesTableExist(db, BONUS_TABLE))
        {
            createBonusTable(db);
        }
    }

    public Cursor getPendingInvoices()
    {
        
        Cursor res = db.rawQuery("SELECT invoices_data.invoices_id, dealer_info.dealer_name, SUM(invoices_data.invoice_price) FROM invoices_data INNER JOIN dealer_info ON dealer_info.dealer_id=invoices_data.dealer_id WHERE invoices_data.invoice_status = 'Invoiced' GROUP BY invoices_data.invoices_id", null);
        return res;
    }

    public Cursor getDealersDiscount()
    {
        String date;
        String[] dateArr = GlobalVariables.getStDate().split("/");
        date = dateArr[2]+getMonthNum(dateArr[1])+dateArr[0];
        
        Cursor res = db.rawQuery("SELECT `dealer_info`.`dealer_name`, `dealer_info`.`dealer_address`, `discount_info`.`sale_amount`, `discount_info`.`discount` FROM `discount_info` LEFT OUTER JOIN `dealer_info` ON `dealer_info`.`dealer_id` = `discount_info`.`dealer_id` WHERE `discount_info`.`dealer_id` != '0' AND `start_date` <= '"+date+"' AND `end_date` >= '"+date+"' ORDER BY `dealer_info`.`dealer_name`", null);
        return res;
    }

    public Cursor getCompanyDiscount()
    {
        String date;
        String[] dateArr = GlobalVariables.getStDate().split("/");
        date = dateArr[2]+getMonthNum(dateArr[1])+dateArr[0];
        
        Cursor res = db.rawQuery("SELECT `company_name`, `sale_amount`,`discount` FROM `discount_info` WHERE `dealer_id` = '0' AND `product_id` = 0 AND `start_date` <= '"+date+"' AND `end_date` >= '"+date+"' ORDER BY `company_name`", null);
        return res;
    }

    public Cursor getProductsDiscount()
    {
        String date;
        String[] dateArr = GlobalVariables.getStDate().split("/");
        date = dateArr[2]+getMonthNum(dateArr[1])+dateArr[0];
        
        Cursor res = db.rawQuery("SELECT product_info.product_name, discount_info.sale_amount, discount_info.discount FROM `discount_info` LEFT OUTER JOIN product_info ON product_info.product_id = discount_info.product_id WHERE discount_info.dealer_id = '0' AND (discount_info.company_name = 0 OR discount_info.company_name = 'null') AND `start_date` <= '"+date+"' AND `end_date` >= '"+date+"' ORDER BY product_info.product_name", null);
        return res;
    }

    public Cursor getDealersBonus()
    {
        String date;
        String[] dateArr = GlobalVariables.getStDate().split("/");
        date = dateArr[2]+getMonthNum(dateArr[1])+dateArr[0];
        
        Cursor res = db.rawQuery("SELECT `dealer_info`.`dealer_name`, `dealer_info`.`dealer_address`, `bonus_info`.`quantity`, `bonus_info`.`bonus` FROM `bonus_info` LEFT OUTER JOIN `dealer_info` ON `dealer_info`.`dealer_id` = `bonus_info`.`dealer_id` WHERE `bonus_info`.`dealer_id` != '0' AND `start_date` <= '"+date+"' AND `end_date` >= '"+date+"' ORDER BY `dealer_info`.`dealer_name`", null);
        return res;
    }

    public Cursor getCompanyBonus()
    {
        String date;
        String[] dateArr = GlobalVariables.getStDate().split("/");
        date = dateArr[2]+getMonthNum(dateArr[1])+dateArr[0];
        
        Cursor res = db.rawQuery("SELECT `company_name`, `quantity`, `bonus` FROM `bonus_info` WHERE `dealer_id` = '0' AND `product_id` = 0 AND `start_date` <= '"+date+"' AND `end_date` >= '"+date+"' ORDER BY `company_name`", null);
        return res;
    }

    public Cursor getProductsBonus()
    {
        String date;
        String[] dateArr = GlobalVariables.getStDate().split("/");
        date = dateArr[2]+getMonthNum(dateArr[1])+dateArr[0];
        
        Cursor res = db.rawQuery("SELECT product_info.product_name, bonus_info.quantity, bonus_info.bonus FROM `bonus_info` LEFT OUTER JOIN product_info ON product_info.product_id = bonus_info.product_id WHERE bonus_info.dealer_id = '0' AND (bonus_info.company_name = 0 OR bonus_info.company_name = 'null') AND `start_date` <= '"+date+"' AND `end_date` >= '"+date+"' ORDER BY product_info.product_name", null);
        return res;
    }

    public Cursor getCurrentBonusInfo()
    {
        String date;
        String[] dateArr = GlobalVariables.getStDate().split("/");
        date = dateArr[2]+getMonthNum(dateArr[1])+dateArr[0];
        
        Cursor res = db.rawQuery("SELECT dealer_id, company_name, product_id, quantity, bonus FROM bonus_info WHERE`start_date` <= '"+date+"' AND `end_date` >= '"+date+"'", null);
        return res;
    }

    public Cursor getCurrentDiscountInfo()
    {
        String date;
        String[] dateArr = GlobalVariables.getStDate().split("/");
        date = dateArr[2]+getMonthNum(dateArr[1])+dateArr[0];
        
        Cursor res = db.rawQuery("SELECT dealer_id, company_name, product_id, sale_amount, discount FROM discount_info WHERE`start_date` <= '"+date+"' AND `end_date` >= '"+date+"'", null);
        return res;
    }

    public Cursor getProductsCompaniesInfo()
    {
        
        Cursor res = db.rawQuery("SELECT product_id, company_name FROM product_info WHERE product_status = 'Active'", null);
        return res;
    }

    public Cursor getOrderInfo(String orderId)
    {
        
        Cursor res = db.rawQuery("SELECT order_info.*, dealer_info.dealer_name, dealer_info.dealer_address FROM "+TABLE_NAME+" INNER JOIN dealer_info ON dealer_info.dealer_id=order_info.dealer_id WHERE order_info.id = "+orderId, null);
        return res;
    }

    public Cursor getProductInfo()
    {
        
        Cursor res = db.rawQuery("SELECT * FROM product_info WHERE product_status = 'Active'", null);
        return res;
    }

    public Cursor getRegistration()
    {
        
        Cursor res = db.rawQuery("SELECT * FROM "+USER_ACCOUNTS_TABLE, null);
        return res;
    }

    public Cursor getLastSynced()
    {
        Cursor res = db.rawQuery("SELECT `data_name`, `last_update_date`, `last_update_time` FROM "+SYNC_TRACKING_TABLE, null);
        return res;
    }

    public void deleteAccount()
    {
        
        db.delete(DatabaseSQLite.USER_ACCOUNTS_TABLE, null, null);
    }



    public boolean insertCurrentLocation(String userId, String latitude, String longitude, String locationName, String stDate, String stTime)
    {
        
        ContentValues contentValues = new ContentValues();
        contentValues.put(MOBLOC_COL_2, userId);
        contentValues.put(MOBLOC_COL_3, latitude);
        contentValues.put(MOBLOC_COL_4, longitude);
        contentValues.put(MOBLOC_COL_5, locationName);
        contentValues.put(MOBLOC_COL_6, stDate);
        contentValues.put(MOBLOC_COL_7, stTime);
        long result = db.insert(MOBILE_LOCATION_TABLE, null, contentValues);
        if(result == -1)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean insertDelivery(String orderId, String dealerId, String cashCollected, String returnBit, String latitude, String longitude, String locationName, String stDate, String stTime, String userId, String status)
    {
        
        ContentValues contentValues = new ContentValues();
        contentValues.put(DELIVERED_COL_1, orderId);
        contentValues.put(DELIVERED_COL_2, dealerId);
        contentValues.put(DELIVERED_COL_3, cashCollected);
        contentValues.put(DELIVERED_COL_4, returnBit);
        contentValues.put(DELIVERED_COL_5, latitude);
        contentValues.put(DELIVERED_COL_6, longitude);
        contentValues.put(DELIVERED_COL_7, locationName);
        contentValues.put(DELIVERED_COL_8, stDate);
        contentValues.put(DELIVERED_COL_9, stTime);
        contentValues.put(DELIVERED_COL_10, userId);
        contentValues.put(DELIVERED_COL_11, status);
        long result = db.insert(DELIVERED_TABLE, null, contentValues);
        if(result == -1)
        {
            return false;
        }
        else
        {
            updateInvoice(orderId, "Delivered");
            return true;
        }
    }

    public void deleteDelivery(String orderId)
    {
        
        db.delete(DatabaseSQLite.DELIVERED_TABLE, "order_id="+orderId, null);
    }

    public void deleteDealerPayment(String orderId)
    {
        
        db.delete(DatabaseSQLite.DEALERS_PAYMENTS_TABLE, "order_id="+orderId, null);
    }

    public boolean updateInvoice(String orderId, String status)
    {
        
        ContentValues contentValues = new ContentValues();
        contentValues.put(INVOICES_COL_7, status);
        int result = db.update(INVOICES_TABLE, contentValues, "invoices_id="+orderId, null);
        if(result <= 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean updateDelivered(String orderId, String status)
    {
        
        ContentValues contentValues = new ContentValues();
        contentValues.put(DELIVERED_COL_11, status);
        int result = db.update(DELIVERED_TABLE, contentValues, "order_id="+orderId, null);
        if(result <= 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }




    public boolean insertOrderGPSLocation(String orderId, String dealerId, String cashCollected, String latitude, String longitude, String locationName, String stDate, String stTime, String userId, String type)
    {
        
        ContentValues contentValues = new ContentValues();
        contentValues.put(ORDLOC_COL_2, orderId);
        contentValues.put(ORDLOC_COL_3, userId);
        contentValues.put(ORDLOC_COL_4, latitude);
        contentValues.put(ORDLOC_COL_5, longitude);
        contentValues.put(ORDLOC_COL_6, locationName);
        contentValues.put(ORDLOC_COL_7, stDate);
        contentValues.put(ORDLOC_COL_8, stTime);
        contentValues.put(ORDLOC_COL_9, dealerId);
        contentValues.put(ORDLOC_COL_10, cashCollected);
        contentValues.put(ORDLOC_COL_11, type);
        long result = db.insert(ORDER_GPS_LOCATION_TABLE, null, contentValues);
        if(result == -1)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public void updateDealerPending(String dealerId, String cashCollected)
    {
        
        db.execSQL("UPDATE "+DEALER_PENDINGS_TABLE+" SET payments = payments - "+cashCollected+" WHERE dealer_id = "+dealerId);
    }

    public boolean insertCashCollection(String dealerId, String orderId, String cashCollected, String cashType, String latitude, String longitude, String locationName, String stDate, String stTime, String userId, String status)
    {
        
        ContentValues contentValues = new ContentValues();
        contentValues.put(DEALPAYMENTS_COL_2, dealerId);
        contentValues.put(DEALPAYMENTS_COL_3, orderId);
        contentValues.put(DEALPAYMENTS_COL_4, cashCollected);
        contentValues.put(DEALPAYMENTS_COL_5, cashType);
        contentValues.put(DEALPAYMENTS_COL_6, latitude);
        contentValues.put(DEALPAYMENTS_COL_7, longitude);
        contentValues.put(DEALPAYMENTS_COL_8, locationName);
        contentValues.put(DEALPAYMENTS_COL_9, stDate);
        contentValues.put(DEALPAYMENTS_COL_10, stTime);
        contentValues.put(DEALPAYMENTS_COL_11, userId);
        contentValues.put(DEALPAYMENTS_COL_12, status);
        long result = db.insert(DEALERS_PAYMENTS_TABLE, null, contentValues);
        if(result == -1)
        {
            return false;
        }
        else
        {
            insertOrderGPSLocation(orderId, dealerId, cashCollected, latitude, longitude, locationName, stDate, stTime, userId, "Collection");
            if(cashType.equals("Return"))
            {
                cashCollected = String.valueOf(-1 * Float.parseFloat(cashCollected));
            }
            updateDealerPending(dealerId, cashCollected);
            return true;
        }
    }

    public boolean insertReturn(String orderId, String dealerId, String productId, String quantity, String unit, String batchNo, String returnPrice, String totalReturnPrice, String returnReason, String cashReturn, String latitude, String longitude, String return_area, String stDate, String stTime, String userId, String status)
    {
        
        ContentValues contentValues = new ContentValues();
        contentValues.put(RETURN_COL_2, orderId);
        contentValues.put(RETURN_COL_3, dealerId);
        contentValues.put(RETURN_COL_4, productId);
        contentValues.put(RETURN_COL_5, quantity);
        contentValues.put(RETURN_COL_6, unit);
        contentValues.put(RETURN_COL_7, batchNo);
        contentValues.put(RETURN_COL_8, returnPrice);
        contentValues.put(RETURN_COL_9, totalReturnPrice);
        contentValues.put(RETURN_COL_10, returnReason);
        contentValues.put(RETURN_COL_11, cashReturn);
        contentValues.put(RETURN_COL_12, latitude);
        contentValues.put(RETURN_COL_13, longitude);
        contentValues.put(RETURN_COL_14, return_area);
        contentValues.put(RETURN_COL_15, stDate);
        contentValues.put(RETURN_COL_16, stTime);
        contentValues.put(RETURN_COL_17, userId);
        contentValues.put(RETURN_COL_18, status);
        long result = db.insert(RETURN_TABLE, null, contentValues);
        if(result == -1)
        {
            return false;
        }
        else
        {
            insertOrderGPSLocation(orderId, dealerId, cashReturn, latitude, longitude, return_area, stDate, stTime, userId, "Return");
            totalReturnPrice = String.valueOf(-1*Float.parseFloat(totalReturnPrice));
            updateDealerPending(dealerId, totalReturnPrice);
            return true;
        }
    }

    public boolean updateReturn(String returnId, String orderId, String dealerId, String productId, String quantity, String unit, String batchNo, String returnPrice, String totalReturnPrice, String returnReason, String cashReturn, String latitude, String longitude, String return_area, String stDate, String stTime, String userId, String status)
    {
        
        ContentValues contentValues = new ContentValues();
        contentValues.put(RETURN_COL_2, orderId);
        contentValues.put(RETURN_COL_3, dealerId);
        contentValues.put(RETURN_COL_4, productId);
        contentValues.put(RETURN_COL_5, quantity);
        contentValues.put(RETURN_COL_6, unit);
        contentValues.put(RETURN_COL_7, batchNo);
        contentValues.put(RETURN_COL_8, returnPrice);
        contentValues.put(RETURN_COL_9, totalReturnPrice);
        contentValues.put(RETURN_COL_10, returnReason);
        contentValues.put(RETURN_COL_11, cashReturn);
        contentValues.put(RETURN_COL_12, latitude);
        contentValues.put(RETURN_COL_13, longitude);
        contentValues.put(RETURN_COL_14, return_area);
        contentValues.put(RETURN_COL_15, stDate);
        contentValues.put(RETURN_COL_16, stTime);
        contentValues.put(RETURN_COL_17, userId);
        contentValues.put(RETURN_COL_18, status);
        int result = db.update(RETURN_TABLE, contentValues, "return_id="+returnId, null);
        if(result <= 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public void updateCashCollection(String paymentId, String dealerId, String preCashCollected, String newCashCollected)
    {
        
        db.execSQL("UPDATE "+DEALERS_PAYMENTS_TABLE+" SET cash = "+newCashCollected+" WHERE payment_id = "+paymentId);
        db.execSQL("UPDATE "+DEALER_PENDINGS_TABLE+" SET payments = payments + "+preCashCollected+" - "+newCashCollected+" WHERE dealer_id = "+dealerId);
    }

    public Cursor getPendingLocations()
    {
        
        Cursor res = db.rawQuery("SELECT * FROM mobile_location", null);
        return res;
    }

    public void deleteGPSLocation(String mobLocId)
    {
        
        db.delete(DatabaseSQLite.MOBILE_LOCATION_TABLE, "mob_loc_id="+mobLocId, null);
    }


    private static boolean doesDatabaseExist(Context context, String dbName) {
        File dbFile = context.getDatabasePath(dbName);
        return dbFile.exists();
    }

    public boolean doesTableExist(SQLiteDatabase db, String tableName) {
        Cursor cursor = db.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '" + tableName + "'", null);

        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.close();
                return true;
            }
            cursor.close();
        }
        return false;
    }


    private String getMonthNum(String month)
    {
        if(month.equals("Jan"))
            return "01";
        else if(month.equals("Feb"))
            month = "02";
        else if(month.equals("Mar"))
            month = "03";
        else if(month.equals("Apr"))
            return "04";
        else if(month.equals("May"))
            return "05";
        else if(month.equals("Jun"))
            return "06";
        else if(month.equals("Jul"))
            return "07";
        else if(month.equals("Aug"))
            return "08";
        else if(month.equals("Sep"))
            return "09";
        else if(month.equals("Oct"))
            return "10";
        else if(month.equals("Nov"))
            return "11";
        else if(month.equals("Dec"))
            return "12";

        return "00";
    }
}
