package com.example.younastraders.ui.dealerpin;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class DealerPinViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public DealerPinViewModel() {
        mText = new MutableLiveData<>();
    }

    public LiveData<String> getText() {
        return mText;
    }
}