package com.example.younastraders;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class GetData extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "YounasTraders.db";
    public String[] dealersId;
    public String[] dealersName;
    public String[] dealersAddress;
    public String[] dealersIdPendingPayments;
    public String[] dealersPendingPayments;
    public ArrayList<ArrayList<String>> batchwiseStock;
    public ArrayList<ArrayList<String>> returnInfoDetail;
    public ArrayList<String> batchProductIds;

    public GetData(Context context, String... data) {
        super(context, DATABASE_NAME, null, 1);
        if(data.length > 0 && data[0].equals("Pending Payments"))
        {
            dealersData();
            dealerPendingPayments();
        }
        else if(data.length > 0 && data[0].equals("Pending Cash"))
        {

        }
        else if(data.length > 0 && data[0].equals("Batchwise Stock"))
        {
            Cursor res = batchwiseStockDetail();
            int num = res.getCount();
            if(num == 0)
            {
                batchwiseStock = null;
                batchProductIds = null;
            }
            else
            {
                batchwiseStock = new ArrayList<ArrayList<String>>();
                batchProductIds = new ArrayList<String>();
                ArrayList<String> temp = new ArrayList<String>();
                String preProductId = "";
                boolean check = false;
                while(res.moveToNext())
                {
                    if(preProductId.equals(res.getString(0)))
                    {
                        temp.add(res.getString(1));
                    }
                    else
                    {
                        if(check)
                        {
                            batchwiseStock.add(temp);
                        }
                        check = true;
                        temp = new ArrayList<String>();
                        preProductId = res.getString(0);
                        batchProductIds.add(res.getString(0));
                        temp.add(res.getString(1));
                    }
                }
                batchwiseStock.add(temp);
            }
        }
        else if(data.length > 0 && data[0].equals("View Return"))
        {

        }
        else if(data.length > 0 && data[0].equals("Dealer Pin"))
        {
            dealersGPSData();
        }

    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public int dealersGPSData()
    {
        Cursor res = DatabaseSQLite.db.rawQuery("SELECT dealer_gps_location.dealer_id, dealer_info.dealer_name, dealer_info.dealer_address FROM dealer_gps_location LEFT OUTER JOIN dealer_info ON dealer_info.dealer_id = dealer_gps_location.dealer_id" , null);
        if(res.getCount() == 0)
        {
            dealersId = new String[res.getCount()+1];
            dealersName = new String[res.getCount()+1];
            dealersId[0] = "0";
            dealersName[0] = "Select Dealer";
            return 0;
        }
        else
        {
            dealersId = new String[res.getCount()+1];
            dealersName = new String[res.getCount()+1];
            dealersId[0] = "0";
            dealersName[0] = "Select Dealer";
            int i = 1;
            while(res.moveToNext())
            {
                dealersId[i] = res.getString(0);
                dealersName[i] = res.getString(1)+" ("+res.getString(2)+")";
                i++;
            }
            return 1;
        }
    }

    public Cursor getInvoicedInfo(String orderId)
    {
        Cursor res = DatabaseSQLite.db.rawQuery("SELECT invoices_data.*, dealer_info.dealer_name, dealer_info.dealer_contact, dealer_info.dealer_address, product_info.product_name FROM invoices_data LEFT OUTER JOIN dealer_info ON dealer_info.dealer_id=invoices_data.dealer_id LEFT OUTER JOIN product_info ON product_info.product_id = invoices_data.product_id WHERE invoices_data.invoices_id = "+orderId, null);
        return res;
    }

    public Cursor getDeliveryInfo(String orderId)
    {
        Cursor res = DatabaseSQLite.db.rawQuery("SELECT invoices_data.*, dealer_info.dealer_name, dealer_info.dealer_contact, dealer_info.dealer_address, product_info.product_name, invoice_delivered.cash_collected FROM invoices_data LEFT OUTER JOIN dealer_info ON dealer_info.dealer_id=invoice_delivered.dealer_id LEFT OUTER JOIN product_info ON product_info.product_id = invoices_data.product_id LEFT OUTER JOIN invoice_delivered ON invoice_delivered.order_id = invoices_data.invoices_id WHERE invoices_data.invoices_id = "+orderId, null);
        return res;
    }

    public Cursor getDealerPending(String dealerId)
    {
        Cursor res = DatabaseSQLite.db.rawQuery("SELECT payments FROM dealer_pending_payments WHERE dealer_id = "+dealerId, null);
        return res;
    }

    public Cursor getEditCashCollected(String paymentId)
    {
        Cursor res = DatabaseSQLite.db.rawQuery("SELECT dealer_payments.dealer_id, dealer_payments.cash, dealer_pending_payments.payments FROM dealer_payments LEFT OUTER JOIN dealer_pending_payments ON dealer_payments.dealer_id = dealer_pending_payments.dealer_id WHERE dealer_payments.payment_id = "+paymentId, null);
        return res;
    }

    public Cursor batchwiseStockDetail()
    {
        Cursor res = DatabaseSQLite.db.rawQuery("SELECT product_id, batch_no FROM `batchwise_stock` ORDER BY `product_id`", null);
        return res;
    }

    public Cursor getReturnDetail()
    {
        Cursor res = DatabaseSQLite.db.rawQuery("SELECT order_return.return_id, order_return.dealer_id, dealer_info.dealer_name, order_return.total_return_price, order_return.date, order_return.status FROM order_return LEFT OUTER JOIN dealer_info ON dealer_info.dealer_id=order_return.dealer_id ORDER BY order_return.return_id DESC", null);
        return res;
    }

    public Cursor getPendingReturnDetail(String returnId)
    {
        Cursor res = DatabaseSQLite.db.rawQuery("SELECT * FROM order_return WHERE return_id = "+returnId, null);
        return res;
    }

    public Cursor getAreaIds()
    {
        Cursor res = DatabaseSQLite.db.rawQuery("SELECT area_id FROM area_info", null);
        return res;
    }

    public Cursor getDealerIds()
    {
        Cursor res = DatabaseSQLite.db.rawQuery("SELECT dealer_id FROM dealer_info", null);
        return res;
    }

    public Cursor getProductIds()
    {
        Cursor res = DatabaseSQLite.db.rawQuery("SELECT product_id FROM product_info", null);
        return res;
    }

    public int dealersData()
    {
        Cursor res = DatabaseSQLite.db.rawQuery("SELECT dealer_id, dealer_name, dealer_address FROM "+DatabaseSQLite.DEALER_INFO_TABLE, null);
        if(res.getCount() == 0)
        {
            dealersId = new String[res.getCount()+1];
            dealersName = new String[res.getCount()+1];
            dealersAddress = new String[res.getCount()+1];
            dealersId[0] = "0";
            dealersName[0] = "Select Dealer";
            dealersAddress[0] = "NULL";
            return 0;
        }
        else
        {
            dealersId = new String[res.getCount()+1];
            dealersName = new String[res.getCount()+1];
            dealersAddress = new String[res.getCount()+1];
            dealersId[0] = "0";
            dealersName[0] = "Select Dealer";
            dealersAddress[0] = "NULL";
            int i = 1;
            while(res.moveToNext())
            {
                dealersId[i] = res.getString(0);
                dealersName[i] = res.getString(1)+" ("+res.getString(2)+")";
                dealersAddress[i] = res.getString(2);
                i++;
            }
            return 1;
        }
    }

    public int dealerPendingPayments()
    {
        
        Cursor res = DatabaseSQLite.db.rawQuery("SELECT dealer_id, payments FROM dealer_pending_payments", null);
        if(res.getCount() == 0)
        {
            dealersIdPendingPayments = new String[res.getCount()];
            dealersPendingPayments = new String[res.getCount()];
            return 0;
        }
        else
        {
            dealersIdPendingPayments = new String[res.getCount()];
            dealersPendingPayments = new String[res.getCount()];
            int i = 0;
            while(res.moveToNext())
            {
                dealersIdPendingPayments[i] = res.getString(0);
                dealersPendingPayments[i] = res.getString(1);
                i++;
            }
            return 1;
        }
    }



    public Cursor getBookedOrders()
    {
        
        Cursor res = DatabaseSQLite.db.rawQuery("SELECT order_info.id, dealer_info.dealer_name, order_info.final_price, order_info.date, order_info.status FROM "+DatabaseSQLite.TABLE_NAME+" LEFT OUTER JOIN dealer_info ON dealer_info.dealer_id=order_info.dealer_id ORDER BY order_info.id DESC", null);
        return res;
    }

    public Cursor getCashDetails()
    {
        
        Cursor res = DatabaseSQLite.db.rawQuery("SELECT dealer_payments.*, dealer_info.dealer_name FROM dealer_payments LEFT OUTER JOIN dealer_info ON dealer_payments.dealer_id = dealer_info.dealer_id ORDER BY dealer_payments.payment_id DESC", null);
        return res;
    }

    public Cursor getDeliveryDetails()
    {
        
        Cursor res = DatabaseSQLite.db.rawQuery("SELECT invoice_delivered.order_id, invoice_delivered.dealer_id, dealer_info.dealer_name, invoice_delivered.date, invoice_delivered.status FROM invoice_delivered LEFT OUTER JOIN dealer_info ON invoice_delivered.dealer_id = dealer_info.dealer_id ORDER BY invoice_delivered.order_id DESC", null);
        return res;
    }

    public Cursor getPendingBookingCount()
    {
        
        Cursor res = DatabaseSQLite.db.rawQuery("SELECT COUNT("+DatabaseSQLite.COL_1+") FROM "+DatabaseSQLite.TABLE_NAME+" WHERE status = 'Pending'", null);
        return res;
    }

    public Cursor getPendingDeliveredCount()
    {
        
        Cursor res = DatabaseSQLite.db.rawQuery("SELECT COUNT("+DatabaseSQLite.DELIVERED_TABLE+"."+DatabaseSQLite.DELIVERED_COL_1+") FROM "+DatabaseSQLite.DELIVERED_TABLE+" LEFT OUTER JOIN dealer_info ON invoice_delivered.dealer_id = dealer_info.dealer_id WHERE "+DatabaseSQLite.DELIVERED_TABLE+".status = 'Pending'", null);
        return res;
    }

    public Cursor getPendingCashCount()
    {
        
        Cursor res = DatabaseSQLite.db.rawQuery("SELECT COUNT("+DatabaseSQLite.DEALERS_PAYMENTS_TABLE+"."+DatabaseSQLite.DEALPAYMENTS_COL_1+") FROM "+DatabaseSQLite.DEALERS_PAYMENTS_TABLE+" LEFT OUTER JOIN dealer_info ON dealer_payments.dealer_id = dealer_info.dealer_id WHERE "+DatabaseSQLite.DEALERS_PAYMENTS_TABLE+".status = 'Pending'", null);
        return res;
    }

    public Cursor getPendingReturnCount()
    {
        
        Cursor res = DatabaseSQLite.db.rawQuery("SELECT COUNT("+DatabaseSQLite.RETURN_TABLE+"."+DatabaseSQLite.RETURN_COL_1+") FROM "+DatabaseSQLite.RETURN_TABLE+" LEFT OUTER JOIN dealer_info ON dealer_info.dealer_id=order_return.dealer_id WHERE "+DatabaseSQLite.RETURN_TABLE+".status = 'Pending'", null);
        return res;
    }

    public Cursor getBookingCount(String... dateVal)
    {
        if(dateVal.length > 0)
        {
            
            Cursor res = DatabaseSQLite.db.rawQuery("SELECT COUNT("+DatabaseSQLite.COL_1+") FROM "+DatabaseSQLite.TABLE_NAME+" WHERE date = '"+dateVal[0]+"' AND status = 'Sent'", null);
            return res;
        }
        else
        {
            
            Cursor res = DatabaseSQLite.db.rawQuery("SELECT COUNT("+DatabaseSQLite.COL_1+") FROM "+DatabaseSQLite.TABLE_NAME+" WHERE status = 'Sent'", null);
            return res;
        }

    }

    public Cursor getDeliverCount()
    {
        
        Cursor res = DatabaseSQLite.db.rawQuery("SELECT COUNT("+DatabaseSQLite.DELIVERED_COL_1+") FROM "+DatabaseSQLite.DELIVERED_TABLE+" WHERE status = 'Sent'", null);
        return res;
    }

    public Cursor getPendingDelivered()
    {
        
        Cursor res = DatabaseSQLite.db.rawQuery("SELECT * FROM invoice_delivered WHERE status = 'Pending'", null);
        return res;
    }

    public Cursor getPendingPayments()
    {
        
        Cursor res = DatabaseSQLite.db.rawQuery("SELECT * FROM dealer_payments WHERE status = 'Pending'", null);
        return res;
    }

    public Cursor getPendingReturns()
    {
        
        Cursor res = DatabaseSQLite.db.rawQuery("SELECT * FROM order_return WHERE status = 'Pending'", null);
        return res;
    }

    public String[] getDealersId() {
        return dealersId;
    }

    public String[] getDealersName() {
        return dealersName;
    }

    public String[] getDealersIdPendingPayments() {
        return dealersIdPendingPayments;
    }

    public String[] getDealersPendingPayments() {
        return dealersPendingPayments;
    }

    public ArrayList<ArrayList<String>> getBatchwiseStock() {
        return batchwiseStock;
    }

    public ArrayList<String> getBatchProductIds() {
        return batchProductIds;
    }
}
