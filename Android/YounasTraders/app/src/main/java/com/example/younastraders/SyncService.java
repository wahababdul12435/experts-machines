package com.example.younastraders;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

public class SyncService extends Service{
    //    public static boolean serviceStatus;
    public static int tenCounts = 1;
    public int timeStampInterval = 600;
    public int dataUpdateInterval = 30;
    public int currLocInterval = 5;

    final class ThreadClass implements Runnable, LocationListener {
        double lat;
        double lon;
        String currAreaNameHalf;
        String currAreaNameFull;
        String preLocation = "";
        protected LocationManager locationManager;
        String stDate;
        String stTime;
        String userId;
        String preLatitude = "";
        String preLongitude = "";
        SendData objSendData;
        DatabaseSync objDatabaseSync;
        GlobalVariables objGlobal = new GlobalVariables(SyncService.this);

        int serviceId;

        ThreadClass(int serviceId) {
            this.serviceId = serviceId;
        }

        public void run() {
            synchronized (this) {
                objDatabaseSync = new DatabaseSync(SyncService.this);
                objDatabaseSync.insertAllRecord(GlobalVariables.areaLastSyncedDate, GlobalVariables.areaLastSyncedTime, GlobalVariables.dealerLastSyncedDate, GlobalVariables.dealerLastSyncedTime, GlobalVariables.productLastSyncedDate, GlobalVariables.productLastSyncedTime, GlobalVariables.dealerGPSLastSyncedDate, GlobalVariables.dealerGPSLastSyncedTime, GlobalVariables.dealerPendingLastSyncedDate, GlobalVariables.dealerPendingLastSyncedTime, GlobalVariables.batchLastSyncedDate, GlobalVariables.batchLastSyncedTime, GlobalVariables.discountLastSyncedDate, GlobalVariables.discountLastSyncedTime, GlobalVariables.bonusLastSyncedDate, GlobalVariables.bonusLastSyncedTime);
                objGlobal.getPending();
                while (tenCounts <= 12) {
                    lat = 0;
                    lon = 0;
                    currAreaNameHalf = "";
                    HandlerThread handlerThread = new HandlerThread("MyHandlerThread");
                    handlerThread.start();
                    Looper looper = handlerThread.getLooper();

                    try {
                        wait(currLocInterval * 1000);
                        dataUpdateInterval -= currLocInterval;
                        timeStampInterval -= currLocInterval;
                        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    Activity#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for Activity#requestPermissions for more details.
                            return;
                        }
                        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this, looper);
                        GlobalVariables.setLatitude(String.valueOf(getLat()));
                        GlobalVariables.setLongitude(String.valueOf(getLon()));
                        GlobalVariables.setLocationNameHalf(getcurrAreaNameHalf());
                        GlobalVariables.setLocationNameFull(getCurrAreaNameFull());
                        Date objDate = Calendar.getInstance().getTime();
                        SimpleDateFormat df = new SimpleDateFormat("dd/MMM/yyyy");
                        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
                        stDate = df.format(objDate);
                        stTime = sdf.format(objDate);
                        objSendData = new SendData(SyncService.this);
                        if(timeStampInterval <= 0)
                        {
                            statusCheck();
                            if(!GlobalVariables.getLatitude().equals("0.0") && !GlobalVariables.getLongitude().equals("0.0") && !preLocation.equals(GlobalVariables.getLocationNameHalf()))
                            {
                                preLocation = GlobalVariables.getLocationNameHalf();
                                objSendData.sendTimeStampLocation(GlobalVariables.userId, GlobalVariables.getLatitude(), GlobalVariables.getLongitude(), GlobalVariables.getLocationNameHalf(), stDate, stTime);
                            }
                            tenCounts++;
                            timeStampInterval = 600;
                        }
                        if(dataUpdateInterval <= 0)
                        {
//                            objDatabaseSync = new DatabaseSync(SyncService.this);
//                            objDatabaseSync.insertAllRecord();
                            objGlobal.getPending();
                            dataUpdateInterval = 30;
                        }
                        if((!preLatitude.equals(GlobalVariables.getLatitude()) || !preLongitude.equals(GlobalVariables.getLongitude())) && !GlobalVariables.getLatitude().equals("0.0") && !GlobalVariables.getLongitude().equals("0.0"))
                        {
                            preLatitude = GlobalVariables.getLatitude();
                            preLongitude = GlobalVariables.getLongitude();
                            objSendData.sendCurrentLocation(GlobalVariables.userId, GlobalVariables.getLatitude(), GlobalVariables.getLongitude(), GlobalVariables.getLocationNameHalf());
                        }
                        else
                        {
                            objSendData.sendCurrentLocationTime(GlobalVariables.userId);
                        }

                    }
                    catch (InterruptedException e)
                    {
                        e.printStackTrace();
                    }
                }
                stopSelf(serviceId);
            }
        }

        @Override
        public void onLocationChanged(Location location) {
            getCurrentLocation(location);
        }

        public void getCurrentLocation(Location location)
        {
            try {
                String addr = getAddress(location.getLatitude(), location.getLongitude());
                lat = location.getLatitude();
                lon = location.getLongitude();
                currAreaNameHalf = addr;
                GlobalVariables.setLatitude(String.valueOf(lat));
                GlobalVariables.setLongitude(String.valueOf(lon));
                GlobalVariables.setLocationNameHalf(currAreaNameHalf);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }

        public String getAddress(double lat, double lng) throws IOException {
            Geocoder geocoder = new Geocoder(SyncService.this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            Address obj = addresses.get(0);
            //            String add = obj.getAddressLine(0);
            String add;
            currAreaNameFull = obj.getAddressLine(0);
            if(obj.getSubLocality() != null)
            {
                add = obj.getSubLocality()+", "+obj.getLocality();
            }
            else if(obj.getSubThoroughfare() != null)
            {
                add = obj.getSubThoroughfare()+", "+obj.getLocality();
            }
            else
            {
                add = obj.getThoroughfare()+", "+obj.getLocality();
            }

            return add;
        }

        public double getLat() {
            return lat;
        }

        public double getLon() {
            return lon;
        }

        public String getcurrAreaNameHalf() {
            return currAreaNameHalf;
        }

        public String getCurrAreaNameFull() {
            return currAreaNameFull;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //        serviceStatus = true;
//        Toast.makeText(this, "Service Started", Toast.LENGTH_LONG).show();
        Thread thread = new Thread(new ThreadClass(startId));
        thread.start();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        //        serviceStatus = false;
//        Toast.makeText(this, "Service Stopped", Toast.LENGTH_LONG).show();
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void statusCheck() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            System.exit(0);
        }
    }
}
