package com.example.younastraders.tabsbonus;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.younastraders.DatabaseSQLite;
import com.example.younastraders.InvoicedOrderDetail;
import com.example.younastraders.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link BonusDealerFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BonusDealerFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    LinearLayout lv;
//    SearchableSpinner edDealerName;
//    ArrayAdapter<String> dealerAdapter;
//    public String[] dealersId;
//    public String[] dealersName;
    LinearLayout[] subLayout;
    View[] viewLine;
    int bonusCount;
    String stInvoiceDetail;

    DatabaseSQLite objSqlLite;
    ArrayAdapter<String> invoicesNumAdapter;
//    Button btnGetDetail;

    String[] stOrderIds;
    String[] stOrderIdsList;
    TextView[] txtSrNo;
    TextView[] txtDealerName;
    TextView[] txtDealerAddress;
    TextView[] txtQuantity;
    TextView[] txtBonus;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public BonusDealerFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ExploreFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BonusDealerFragment newInstance(String param1, String param2) {
        BonusDealerFragment fragment = new BonusDealerFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    private int findIndexInArray(String query, String[] arr) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i].equals(query)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_bonus_dealer, container, false);
        objSqlLite = new DatabaseSQLite(getActivity());
        bonusCount = 0;
        lv = rootView.findViewById(R.id.linear_layout);
//        GetData objGetData = new GetData(getActivity());
//        dealersId = objGetData.getDealersId();
//        dealersName = objGetData.getDealersName();
//        edDealerName = rootView.findViewById(R.id.ed_dealer_name);
//        if(dealersName == null)
//        {
//            dealersName = new String[1];
//            dealersName[0] = "No Dealer Record";
//        }
//        dealerAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, dealersName);
//        edDealerName.setAdapter(dealerAdapter);

//        btnGetDetail = rootView.findViewById(R.id.btn_getdetail);
        viewAll();
        return rootView;
    }

    public void viewAll()
    {
        Cursor res = objSqlLite.getDealersBonus();
        int num = res.getCount();

        if(num == 0)
        {
            txtDealerName = new TextView[1];
            lv.addView(createEmptyText());

            subLayout = new LinearLayout[1];
            stOrderIdsList = new String[1];
            stOrderIds = new String[1];
            txtSrNo = new TextView[1];
            txtDealerName = new TextView[1];
            txtQuantity = new TextView[1];
            txtBonus = new Button[1];
            stOrderIdsList[0] = "Select Invoice";
        }
        else
        {
            subLayout = new LinearLayout[res.getCount()];
            viewLine = new View[res.getCount()];
            stOrderIdsList = new String[res.getCount()+1];
            stOrderIds = new String[res.getCount()];
            txtSrNo = new TextView[res.getCount()];
            txtDealerName = new TextView[res.getCount()];
            txtQuantity = new TextView[res.getCount()];
            txtBonus = new Button[res.getCount()];
            res.moveToNext();
            stOrderIdsList[0] = "Dealer Name";
//            if(res.getString(0) == null)
////            {
////                txtDealerName = new TextView[1];
////                lv.addView(createEmptyText());
////            }
////            else
////            {
////                do {
////                    bonusCount++;
////                    stOrderIds[bonusCount-1] = res.getString(0);
////                    stOrderIdsList[bonusCount] = res.getString(0);
////                    lv.addView(createLinearLayout(res.getString(0), res.getString(1), res.getString(2), res.getString(3)));
////                    addListenerToButtons();
////                }while (res.moveToNext());
////            }
            do {
                bonusCount++;
                stOrderIds[bonusCount-1] = res.getString(0);
                stOrderIdsList[bonusCount] = res.getString(0);
                lv.addView(createLinearLayout(String.valueOf(bonusCount), res.getString(0)+"\n"+res.getString(1), res.getString(2), res.getString(3)));
                lv.addView(createLineView());
                addListenerToButtons();
            }while (res.moveToNext());
        }
    }


    public TextView createEmptyText() {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        txtDealerName[0] = new TextView(getActivity());
        txtDealerName[0].setLayoutParams(lparams);
        txtDealerName[0].setText("No Record to Show");
        txtDealerName[0].setTextSize(TypedValue.COMPLEX_UNIT_SP,18);
        txtDealerName[0].setGravity(Gravity.CENTER);
        return txtDealerName[0];
    }

    public TextView createSrNoText(String text) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(Math.round(getResources().getDimension(R.dimen.discount_bonus_1)), LinearLayout.LayoutParams.WRAP_CONTENT);
        txtSrNo[bonusCount-1] = new TextView(getActivity());
        txtSrNo[bonusCount-1].setLayoutParams(lparams);
        txtSrNo[bonusCount-1].setText(text);
        txtSrNo[bonusCount-1].setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
//        txtSrNo[bonusCount-1].setBackgroundColor(Color.BLUE);
        return txtSrNo[bonusCount-1];
    }

    public TextView createDealerText(String text) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(Math.round(getResources().getDimension(R.dimen.discount_bonus_2)), LinearLayout.LayoutParams.WRAP_CONTENT);
        txtDealerName[bonusCount-1] = new TextView(getActivity());
        txtDealerName[bonusCount-1].setLayoutParams(lparams);
        txtDealerName[bonusCount-1].setText(text);
//        txtDealerName[bonusCount-1].setBackgroundColor(Color.YELLOW);
        return txtDealerName[bonusCount-1];
    }

    public TextView createQuantityText(String text) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(Math.round(getResources().getDimension(R.dimen.discount_bonus_3)), LinearLayout.LayoutParams.WRAP_CONTENT);
        txtQuantity[bonusCount-1] = new TextView(getActivity());
        txtQuantity[bonusCount-1].setLayoutParams(lparams);
        txtQuantity[bonusCount-1].setText(text);
        txtQuantity[bonusCount-1].setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
//        txtQuantity[bonusCount-1].setBackgroundColor(Color.GREEN);
        return txtQuantity[bonusCount-1];
    }

    public TextView createBonusText(String text) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        txtBonus[bonusCount-1] = new Button(getActivity());
        txtBonus[bonusCount-1].setLayoutParams(lparams);
        txtBonus[bonusCount-1].setText(text);
//        txtBonus[bonusCount-1].setBackgroundColor(Color.RED);
        txtBonus[bonusCount-1].setBackgroundColor(Color.TRANSPARENT);
        return txtBonus[bonusCount-1];
    }

    public LinearLayout createLinearLayout(String srNo, String dealerName, String quantity, String bonus) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        subLayout[bonusCount-1] = new LinearLayout(getActivity());
        subLayout[bonusCount-1].setOrientation(LinearLayout.HORIZONTAL);
        subLayout[bonusCount-1].setLayoutParams(lparams);
        subLayout[bonusCount-1].addView(createSrNoText(srNo));
        subLayout[bonusCount-1].addView(createDealerText(dealerName));
        subLayout[bonusCount-1].addView(createQuantityText(quantity));
        subLayout[bonusCount-1].addView(createBonusText(bonus));
        subLayout[bonusCount-1].setPadding(0,0,0,10);
        return subLayout[bonusCount-1];
    }

    public View createLineView() {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,  Math.round(getResources().getDimension(R.dimen.line)));
        viewLine[bonusCount-1] = new View(getActivity());
        viewLine[bonusCount-1].setLayoutParams(lparams);
        viewLine[bonusCount-1].setBackgroundColor(Color.BLACK);
        return viewLine[bonusCount-1];
    }

    public void addListenerToButtons()
    {
        for(int i=0; i<bonusCount; i++)
        {
            final int finalI = i;
            txtBonus[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), InvoicedOrderDetail.class);
                    intent.putExtra("ORDER_ID", stOrderIds[finalI]);
                    startActivity(intent);
                }
            });
        }
    }
}