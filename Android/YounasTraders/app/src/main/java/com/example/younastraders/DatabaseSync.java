package com.example.younastraders;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

public class DatabaseSync {
    String[][] newDataUpdate;
    String[][] areaInfo;
    String[][] dealerInfo;
    String[][] productInfo;
    String[] dealerGPSData;
    String[][] invoicesData;
    String[][] dealersPendingPayments;
    String[][] batchwiseStock;
    String[][] discountInfo;
    String[][] bonusInfo;

    public static ArrayList<String> preAreaIds = new ArrayList<>();
    public static ArrayList<String> preDealerIds = new ArrayList<>();
    public static ArrayList<String> preProductIds = new ArrayList<>();

    static int allRecordCheck = 0;

    boolean syncCheck = false;

    public static final String DATABASE_NAME = "YounasTraders.DatabaseSQLite.db";


    public Context context;

    public DatabaseSync(Context context) {
        this.context = context;
        allRecordCheck = 0;
//        DatabaseSQLite.db.delete(DatabaseSQLite.DEALER_GPS_LOC_TABLE, null, null);
//        DatabaseSQLite.db.delete(DatabaseSQLite.DEALER_PENDINGS_TABLE, null, null);
//        syncCheck = insertAllRecord();
//        int x = 0;
////        while(allRecordCheck < 6)
////        {
////            x++;
////        }
//        if(syncCheck)
//        {
//            Toast.makeText(context, "Data Synced Successfully", Toast.LENGTH_SHORT).show();
//        }
//        else
//        {
//            Toast.makeText(context, "Data Unable to Synced", Toast.LENGTH_SHORT).show();
//        }
//        Intent intent=new Intent(context, Home.class);
//        context.startActivity(intent);
    }

    public void deletaAllData()
    {
        DatabaseSQLite.db.delete(DatabaseSQLite.DEALER_GPS_LOC_TABLE, null, null);
        DatabaseSQLite.db.delete(DatabaseSQLite.DEALER_PENDINGS_TABLE, null, null);
        DatabaseSQLite.db.delete(DatabaseSQLite.AREA_INFO_TABLE, null, null);
        DatabaseSQLite.db.delete(DatabaseSQLite.DEALER_INFO_TABLE, null, null);
        DatabaseSQLite.db.delete(DatabaseSQLite.PRODUCT_INFO_TABLE, null, null);
        DatabaseSQLite.db.delete(DatabaseSQLite.BATCHWISE_STOCK_TABLE, null, null);
        DatabaseSQLite.db.delete(DatabaseSQLite.DISCOUNT_TABLE, null, null);
        DatabaseSQLite.db.delete(DatabaseSQLite.BONUS_TABLE, null, null);
    }

    public boolean insertArea(String areaId, String cityId, String areaName, String status)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseSQLite.AREA_COL_1, areaId);
        contentValues.put(DatabaseSQLite.AREA_COL_2, cityId);
        contentValues.put(DatabaseSQLite.AREA_COL_3, areaName);
        contentValues.put(DatabaseSQLite.AREA_COL_4, status);
        long result = DatabaseSQLite.db.insert(DatabaseSQLite.AREA_INFO_TABLE, null, contentValues);
        if(result == -1)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean insertDealer(String dealerId, String dealerAreaId, String dealerName, String dealerContact, String dealerAddress, String dealerType, String dealerCnic, String dealerLicNum, String dealerLicExp, String status)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseSQLite.DEALER_COL_1, dealerId);
        contentValues.put(DatabaseSQLite.DEALER_COL_2, dealerAreaId);
        contentValues.put(DatabaseSQLite.DEALER_COL_3, dealerName);
        contentValues.put(DatabaseSQLite.DEALER_COL_4, dealerContact);
        contentValues.put(DatabaseSQLite.DEALER_COL_5, dealerAddress);
        contentValues.put(DatabaseSQLite.DEALER_COL_6, dealerType);
        contentValues.put(DatabaseSQLite.DEALER_COL_7, dealerCnic);
        contentValues.put(DatabaseSQLite.DEALER_COL_8, dealerLicNum);
        contentValues.put(DatabaseSQLite.DEALER_COL_9, dealerLicExp);
        contentValues.put(DatabaseSQLite.DEALER_COL_10, status);
        long result = DatabaseSQLite.db.insert(DatabaseSQLite.DEALER_INFO_TABLE, null, contentValues);
        if(result == -1)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean insertProduct(String productId, String productName, String finalPrice, String status, String companyName)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseSQLite.PRODUCT_COL_1, productId);
        contentValues.put(DatabaseSQLite.PRODUCT_COL_2, productName);
        contentValues.put(DatabaseSQLite.PRODUCT_COL_3, finalPrice);
        contentValues.put(DatabaseSQLite.PRODUCT_COL_4, status);
        contentValues.put(DatabaseSQLite.PRODUCT_COL_5, companyName);
        long result = DatabaseSQLite.db.insert(DatabaseSQLite.PRODUCT_INFO_TABLE, null, contentValues);
        if(result == -1)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean insertDealerGPS(String dealerId)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseSQLite.DEALGPS_COL_2, dealerId);
        contentValues.put(DatabaseSQLite.DEALGPS_COL_3, "NULL");
        contentValues.put(DatabaseSQLite.DEALGPS_COL_4, "NULL");
        contentValues.put(DatabaseSQLite.DEALGPS_COL_5, "NULL");

        int result = DatabaseSQLite.db.update(DatabaseSQLite.DEALER_GPS_LOC_TABLE, contentValues, "dealer_id="+dealerId, null);
        if(result == -1)
        {
            long results = DatabaseSQLite.db.insert(DatabaseSQLite.DEALER_GPS_LOC_TABLE, null, contentValues);
            if(results == -1)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        else
        {
            return true;
        }
    }

    public boolean insertInvoice(String invoiceId, String dealerId, String productId, String quantity, String unit, String invoicePrice, String invoiceStatus)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseSQLite.INVOICES_COL_1, invoiceId);
        contentValues.put(DatabaseSQLite.INVOICES_COL_2, dealerId);
        contentValues.put(DatabaseSQLite.INVOICES_COL_3, productId);
        contentValues.put(DatabaseSQLite.INVOICES_COL_4, quantity);
        contentValues.put(DatabaseSQLite.INVOICES_COL_5, unit);
        contentValues.put(DatabaseSQLite.INVOICES_COL_6, invoicePrice);
        contentValues.put(DatabaseSQLite.INVOICES_COL_7, invoiceStatus);
        long result = DatabaseSQLite.db.insert(DatabaseSQLite.INVOICES_TABLE, null, contentValues);
        if(result == -1)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean insertDealerPending(String dealerId, String pendingPayments)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseSQLite.DEALPENDING_COL_1, dealerId);
        contentValues.put(DatabaseSQLite.DEALPENDING_COL_2, pendingPayments);
        int result = DatabaseSQLite.db.update(DatabaseSQLite.DEALER_PENDINGS_TABLE, contentValues, "dealer_id="+dealerId, null);
        if(result == -1)
        {
            long results = DatabaseSQLite.db.insert(DatabaseSQLite.DEALER_PENDINGS_TABLE, null, contentValues);
            if(results == -1)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        else
        {
            return true;
        }
    }

    public boolean insertBatchwiseStock(String batchstockId, String productId, String batchNo, String quantity, String expiryDate)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseSQLite.BATCHSTOCK_COL_1, batchstockId);
        contentValues.put(DatabaseSQLite.BATCHSTOCK_COL_2, productId);
        contentValues.put(DatabaseSQLite.BATCHSTOCK_COL_3, batchNo);
        contentValues.put(DatabaseSQLite.BATCHSTOCK_COL_4, quantity);
        contentValues.put(DatabaseSQLite.BATCHSTOCK_COL_5, expiryDate);
        long result = DatabaseSQLite.db.insert(DatabaseSQLite.BATCHWISE_STOCK_TABLE, null, contentValues);
        if(result == -1)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean insertDiscountInfo(String approvalId, String dealerId, String companyName, String productId, String saleAmount, String discount, String startDate, String endDate)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseSQLite.DISCOUNT_COL_1, approvalId);
        contentValues.put(DatabaseSQLite.DISCOUNT_COL_2, dealerId);
        contentValues.put(DatabaseSQLite.DISCOUNT_COL_3, companyName);
        contentValues.put(DatabaseSQLite.DISCOUNT_COL_4, productId);
        contentValues.put(DatabaseSQLite.DISCOUNT_COL_5, saleAmount);
        contentValues.put(DatabaseSQLite.DISCOUNT_COL_6, discount);
        contentValues.put(DatabaseSQLite.DISCOUNT_COL_7, startDate);
        contentValues.put(DatabaseSQLite.DISCOUNT_COL_8, endDate);
        long result = DatabaseSQLite.db.insert(DatabaseSQLite.DISCOUNT_TABLE, null, contentValues);
        if(result == -1)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean insertBonusInfo(String approvalId, String dealerId, String companyName, String productId, String quantity, String bonus, String startDate, String endDate)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseSQLite.BONUS_COL_1, approvalId);
        contentValues.put(DatabaseSQLite.BONUS_COL_2, dealerId);
        contentValues.put(DatabaseSQLite.BONUS_COL_3, companyName);
        contentValues.put(DatabaseSQLite.BONUS_COL_4, productId);
        contentValues.put(DatabaseSQLite.BONUS_COL_5, quantity);
        contentValues.put(DatabaseSQLite.BONUS_COL_6, bonus);
        contentValues.put(DatabaseSQLite.BONUS_COL_7, startDate);
        contentValues.put(DatabaseSQLite.BONUS_COL_8, endDate);
        long result = DatabaseSQLite.db.insert(DatabaseSQLite.BONUS_TABLE, null, contentValues);
        if(result == -1)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean insertDataBaseSync(String dataName, String lastUpdateDate, String lastUpdateTime)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseSQLite.SYNCTRACK_COL_2, dataName);
        contentValues.put(DatabaseSQLite.SYNCTRACK_COL_3, lastUpdateDate);
        contentValues.put(DatabaseSQLite.SYNCTRACK_COL_4, lastUpdateTime);
        long result = DatabaseSQLite.db.insert(DatabaseSQLite.SYNC_TRACKING_TABLE, null, contentValues);
        if(result == -1)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public void checkNewDataUpdate()
    {
        DatabaseSQLite objsqlite = new DatabaseSQLite(context);
        Cursor res = objsqlite.getLastSynced();

        if(res.getCount() == 0)
        {
            deletaAllData();
            insertAllRecord();
        }
        else
        {

        }
    }

    public boolean insertAllRecord(final String... str)
    {
        final boolean[] completeInsertion = {false};
        final boolean[] areaResponse = {false};
        final boolean[] dealerResponse = {false};
        final boolean[] productResponse = {false};
        final boolean[] dealerGPSResponse = {false};
        final boolean[] dealerPendings = {false};
        final boolean[] batchstock = {false};
        final boolean[] discount = {false};
        final boolean[] bonus = {false};

        final FetchDataMysql objfetchmysql1;
        final FetchDataMysql objfetchmysql2;
        final FetchDataMysql objfetchmysql3;
        final FetchDataMysql objfetchmysql4;
        final FetchDataMysql objfetchmysql5;
        final FetchDataMysql objfetchmysql6;
        final FetchDataMysql objfetchmysql7;
        final FetchDataMysql objfetchmysql8;

        if(str.length > 0)
        {
            if(!str[0].equals(""))
            {
                objfetchmysql1 = new FetchDataMysql(context,"area_info", str[0], str[1]);
            }
            else
            {
                objfetchmysql1 = new FetchDataMysql(context,"area_info");
            }
            if(!str[2].equals(""))
            {
                objfetchmysql2 = new FetchDataMysql(context,"dealer_info", str[2], str[3]);
            }
            else
            {
                objfetchmysql2 = new FetchDataMysql(context,"dealer_info");
            }
            if(!str[4].equals(""))
            {
                objfetchmysql3 = new FetchDataMysql(context,"product_info", str[4], str[5]);
            }
            else
            {
                objfetchmysql3 = new FetchDataMysql(context,"product_info");
            }
            if(!str[6].equals(""))
            {
                objfetchmysql4 = new FetchDataMysql(context,"dealer_gps_location", str[6], str[7]);
            }
            else
            {
                objfetchmysql4 = new FetchDataMysql(context,"dealer_gps_location");
            }
            if(!str[8].equals(""))
            {
                objfetchmysql5 = new FetchDataMysql(context,"dealer_pendings", str[8], str[9]);
            }
            else
            {
                objfetchmysql5 = new FetchDataMysql(context,"dealer_pendings");
            }
            if(!str[10].equals(""))
            {
                objfetchmysql6 = new FetchDataMysql(context,"batchwise_stock", str[10], str[11]);
            }
            else
            {
                objfetchmysql6 = new FetchDataMysql(context,"batchwise_stock");
            }
            if(!str[12].equals(""))
            {
                objfetchmysql7 = new FetchDataMysql(context,"discount", str[12], str[13]);
            }
            else
            {
                objfetchmysql7 = new FetchDataMysql(context,"discount");
            }
            if(!str[14].equals(""))
            {
                objfetchmysql8 = new FetchDataMysql(context,"bonus", str[14], str[15]);
            }
            else
            {
                objfetchmysql8 = new FetchDataMysql(context,"bonus");
            }
        }
        else
        {
            objfetchmysql1 = new FetchDataMysql(context,"area_info");
            objfetchmysql2 = new FetchDataMysql(context,"dealer_info");
            objfetchmysql3 = new FetchDataMysql(context,"product_info");
            objfetchmysql4 = new FetchDataMysql(context,"dealer_gps_location");
            objfetchmysql5 = new FetchDataMysql(context,"dealer_pendings");
            objfetchmysql6 = new FetchDataMysql(context,"batchwise_stock");
            objfetchmysql7 = new FetchDataMysql(context,"discount");
            objfetchmysql8 = new FetchDataMysql(context,"bonus");
        }


        objfetchmysql1.setCallBack(new FetchDataMysql.CallBack() {
            public void onResult(String value) {
                allRecordCheck++;
                areaInfo = objfetchmysql1.getAreaInfo();
                if(areaInfo != null)
                {
                    completeInsertion[0] = true;
                    for(int i=0; i<areaInfo.length; i++)
                    {
                        if(areaInfo[4].equals("Update"))
                        {
                            areaResponse[0] = updateArea(areaInfo[i][0], areaInfo[i][1], areaInfo[i][2], areaInfo[i][3]);
                        }
                        else if(areaInfo[4].equals("Delete"))
                        {
                            areaResponse[0] = deleteArea(areaInfo[i][0]);
                        }
                        else
                        {
                            if(!preAreaIds.contains(areaInfo[i][0]))
                            {
                                areaResponse[0] = insertArea(areaInfo[i][0], areaInfo[i][1], areaInfo[i][2], areaInfo[i][3]);
                                preAreaIds.add(areaInfo[i][0]);
                            }

                        }
                    }
                    if(str.length > 0 && !str[0].equals(""))
                    {
                        updateDataBaseSync("area_info", GlobalVariables.getStDate(), GlobalVariables.getStTime());
                    }
                    else
                    {
                        insertDataBaseSync("area_info", GlobalVariables.getStDate(), GlobalVariables.getStTime());
                    }
//                    Toast.makeText(context, "Synced From Server", Toast.LENGTH_LONG).show();
                }
                else
                {
//                    completeInsertion[0] = false;
//                    Toast.makeText(context, "Unable to Connect to Server", Toast.LENGTH_LONG).show();
                }
            }
        });

        objfetchmysql2.setCallBack(new FetchDataMysql.CallBack() {
            @Override
            public void onResult(String value) {
                allRecordCheck++;
                dealerInfo = objfetchmysql2.getDealerInfo();
                if(dealerInfo != null)
                {
                    completeInsertion[0] = true;
                    for(int i=0; i<dealerInfo.length; i++)
                    {
                        if(dealerInfo[10].equals("Update"))
                        {
                            dealerResponse[0] = updateDealer(dealerInfo[i][0], dealerInfo[i][1], dealerInfo[i][2], dealerInfo[i][3], dealerInfo[i][4], dealerInfo[i][5], dealerInfo[i][6], dealerInfo[i][7], dealerInfo[i][8], dealerInfo[i][9]);
                        }
                        else if(dealerInfo[10].equals("Delete"))
                        {
                            dealerResponse[0] = deleteDealer(dealerInfo[i][0]);
                        }
                        else
                        {
                            if(!preDealerIds.contains(dealerInfo[i][0]))
                            {
                                dealerResponse[0] = insertDealer(dealerInfo[i][0], dealerInfo[i][1], dealerInfo[i][2], dealerInfo[i][3], dealerInfo[i][4], dealerInfo[i][5], dealerInfo[i][6], dealerInfo[i][7], dealerInfo[i][8], dealerInfo[i][9]);
                                preDealerIds.add(dealerInfo[i][0]);
                            }
                        }

                    }
                    if(str.length > 0 && !str[2].equals(""))
                    {
                        updateDataBaseSync("dealer_info", GlobalVariables.getStDate(), GlobalVariables.getStTime());
                    }
                    else
                    {
                        insertDataBaseSync("dealer_info", GlobalVariables.getStDate(), GlobalVariables.getStTime());
                    }
                }

            }
        });


        objfetchmysql3.setCallBack(new FetchDataMysql.CallBack() {
            @Override
            public void onResult(String value) {
                allRecordCheck++;
                productInfo = objfetchmysql3.getProductInfo();
                if(productInfo != null)
                {
                    completeInsertion[0] = true;
                    for(int i=0; i<productInfo.length; i++)
                    {
                        if(productInfo[5].equals("Update"))
                        {
                            productResponse[0] = updateProduct(productInfo[i][0], productInfo[i][1], productInfo[i][2], productInfo[i][3], productInfo[i][4]);
                        }
                        else if(productInfo[5].equals("Delete"))
                        {
                            productResponse[0] = deleteProduct(productInfo[i][0]);
                        }
                        else
                        {
                            if(!preProductIds.contains(productInfo[i][0]))
                            {
                                productResponse[0] = insertProduct(productInfo[i][0], productInfo[i][1], productInfo[i][2], productInfo[i][3], productInfo[i][4]);
                                preProductIds.add(productInfo[i][0]);
                            }
                        }
                    }
                    if(str.length > 0 && !str[4].equals(""))
                    {
                        updateDataBaseSync("product_info", GlobalVariables.getStDate(), GlobalVariables.getStTime());
                    }
                    else
                    {
                        insertDataBaseSync("product_info", GlobalVariables.getStDate(), GlobalVariables.getStTime());
                    }
                }
            }
        });

        objfetchmysql4.setCallBack(new FetchDataMysql.CallBack() {
            @Override
            public void onResult(String value) {
                dealerGPSData = objfetchmysql4.getDealerGPSData();
                if(dealerGPSData != null)
                {
                    completeInsertion[0] = true;
                    for(int i=0; i<dealerGPSData.length; i++)
                    {
                        dealerGPSResponse[0] = insertDealerGPS(dealerGPSData[i]);
                    }
                    if(str.length > 0 && !str[6].equals(""))
                    {
                        updateDataBaseSync("dealer_gps", GlobalVariables.getStDate(), GlobalVariables.getStTime());
                    }
                    else
                    {
                        insertDataBaseSync("dealer_gps", GlobalVariables.getStDate(), GlobalVariables.getStTime());
                    }
                }
            }
        });

        objfetchmysql5.setCallBack(new FetchDataMysql.CallBack() {
            @Override
            public void onResult(String value) {
                dealersPendingPayments = objfetchmysql5.getDealerPendingPayments();
                if(dealersPendingPayments != null)
                {
                    completeInsertion[0] = true;
                    for(int i=0; i<dealersPendingPayments.length; i++)
                    {
                        dealerPendings[0] = insertDealerPending(dealersPendingPayments[i][0], dealersPendingPayments[i][1]);
                    }
                    if(str.length > 0 && !str[8].equals(""))
                    {
                        updateDataBaseSync("dealer_pending", GlobalVariables.getStDate(), GlobalVariables.getStTime());
                    }
                    else
                    {
                        insertDataBaseSync("dealer_pending", GlobalVariables.getStDate(), GlobalVariables.getStTime());
                    }
                }
            }
        });

        objfetchmysql6.setCallBack(new FetchDataMysql.CallBack() {
            @Override
            public void onResult(String value) {
                allRecordCheck++;
                batchwiseStock = objfetchmysql6.getBatchwiseStock();
                if(batchwiseStock != null)
                {
                    completeInsertion[0] = true;
                    for(int i=0; i<batchwiseStock.length; i++)
                    {
                        if(batchwiseStock[5].equals("Update"))
                        {
                            batchstock[0] = updateBatchWiseStock(batchwiseStock[i][0], batchwiseStock[i][1], batchwiseStock[i][2], batchwiseStock[i][3], batchwiseStock[i][4]);
                        }
                        else if(batchwiseStock[5].equals("Delete"))
                        {
                            batchstock[0] = deleteBatchWiseStock(batchwiseStock[i][0]);
                        }
                        else
                        {
                            batchstock[0] = insertBatchwiseStock(batchwiseStock[i][0], batchwiseStock[i][1], batchwiseStock[i][2], batchwiseStock[i][3], batchwiseStock[i][4]);
                        }
                    }
                    if(str.length > 0 && !str[10].equals(""))
                    {
                        updateDataBaseSync("batchwise_stock", GlobalVariables.getStDate(), GlobalVariables.getStTime());
                    }
                    else
                    {
                        insertDataBaseSync("batchwise_stock", GlobalVariables.getStDate(), GlobalVariables.getStTime());
                    }
                }
            }
        });

        objfetchmysql7.setCallBack(new FetchDataMysql.CallBack() {
            @Override
            public void onResult(String value) {
                allRecordCheck++;
                discountInfo = objfetchmysql7.getDiscountInfo();
                if(discountInfo != null)
                {
                    completeInsertion[0] = true;
                    for(int i=0; i<discountInfo.length; i++)
                    {
                        if(discountInfo[8].equals("Update"))
                        {
                            discount[0] = updateDiscount(discountInfo[i][0], discountInfo[i][1], discountInfo[i][2], discountInfo[i][3], discountInfo[i][4], discountInfo[i][5], discountInfo[i][6], discountInfo[i][7]);
                        }
                        else if(discountInfo[8].equals("Delete"))
                        {
                            discount[0] = deleteDiscount(discountInfo[i][0]);
                        }
                        else
                        {
                            discount[0] = insertDiscountInfo(discountInfo[i][0], discountInfo[i][1], discountInfo[i][2], discountInfo[i][3], discountInfo[i][4], discountInfo[i][5], discountInfo[i][6], discountInfo[i][7]);
                        }
                    }
                    if(str.length > 0 && !str[12].equals(""))
                    {
                        updateDataBaseSync("discount", GlobalVariables.getStDate(), GlobalVariables.getStTime());
                    }
                    else
                    {
                        insertDataBaseSync("discount", GlobalVariables.getStDate(), GlobalVariables.getStTime());
                    }
                }
            }
        });

        objfetchmysql8.setCallBack(new FetchDataMysql.CallBack() {
            @Override
            public void onResult(String value) {
                allRecordCheck++;
                bonusInfo = objfetchmysql8.getBonusInfo();
                if(bonusInfo != null)
                {
                    completeInsertion[0] = true;
                    for(int i=0; i<bonusInfo.length; i++)
                    {
                        if(bonusInfo[8].equals("Update"))
                        {
                            bonus[0] = updateBonus(bonusInfo[i][0], bonusInfo[i][1], bonusInfo[i][2], bonusInfo[i][3], bonusInfo[i][4], bonusInfo[i][5], bonusInfo[i][6], bonusInfo[i][7]);
                        }
                        else if(bonusInfo[8].equals("Delete"))
                        {
                            bonus[0] = deleteBonus(bonusInfo[i][0]);
                        }
                        else
                        {
                            bonus[0] = insertBonusInfo(bonusInfo[i][0], bonusInfo[i][1], bonusInfo[i][2], bonusInfo[i][3], bonusInfo[i][4], bonusInfo[i][5], bonusInfo[i][6], bonusInfo[i][7]);
                        }
                    }
                    if(str.length > 0 && !str[14].equals(""))
                    {
                        updateDataBaseSync("bonus", GlobalVariables.getStDate(), GlobalVariables.getStTime());
                    }
                    else
                    {
                        insertDataBaseSync("bonus", GlobalVariables.getStDate(), GlobalVariables.getStTime());
                    }
                }
            }
        });

        refreshInvoicesData();

        return completeInsertion[0];
    }

    public void refreshInvoicesData()
    {
        DatabaseSQLite.db.delete(DatabaseSQLite.INVOICES_TABLE, null, null);

        final boolean[] invoiceResponse = {false};

        final FetchDataMysql objfetchmysql= new FetchDataMysql(context, "invoices_data");
        objfetchmysql.setCallBack(new FetchDataMysql.CallBack() {

            public void onResult(String value) {
                invoicesData = objfetchmysql.getInvoicesData();
                if(invoicesData != null)
                {
                    for(int i=0; i<invoicesData.length; i++)
                    {
                        invoiceResponse[0] = insertInvoice(invoicesData[i][0], invoicesData[i][1], invoicesData[i][2], invoicesData[i][3], invoicesData[i][4], invoicesData[i][5], invoicesData[i][6]);
                    }
//                    Toast.makeText(context, "Synced Invoices From Server", Toast.LENGTH_LONG).show();
                }
                else
                {
                    Toast.makeText(context, "Unable to Sync Invoices to Server", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public boolean updateArea(String areaId, String cityId, String areaName, String areaStatus)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseSQLite.AREA_COL_2, cityId);
        contentValues.put(DatabaseSQLite.AREA_COL_3, areaName);
        contentValues.put(DatabaseSQLite.AREA_COL_4, areaStatus);
        int result = DatabaseSQLite.db.update(DatabaseSQLite.AREA_INFO_TABLE, contentValues, "area_id="+areaId, null);
        if(result <= 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean updateDealer(String dealerId, String areaId, String dealerName, String dealerContact, String dealerAddress, String dealerType, String dealerCnic,  String dealerLicNum, String dealerLicExp, String dealerStatus)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseSQLite.DEALER_COL_2, areaId);
        contentValues.put(DatabaseSQLite.DEALER_COL_3, dealerName);
        contentValues.put(DatabaseSQLite.DEALER_COL_4, dealerContact);
        contentValues.put(DatabaseSQLite.DEALER_COL_5, dealerAddress);
        contentValues.put(DatabaseSQLite.DEALER_COL_6, dealerType);
        contentValues.put(DatabaseSQLite.DEALER_COL_7, dealerCnic);
        contentValues.put(DatabaseSQLite.DEALER_COL_8, dealerLicNum);
        contentValues.put(DatabaseSQLite.DEALER_COL_9, dealerLicExp);
        contentValues.put(DatabaseSQLite.DEALER_COL_10, dealerStatus);
        int result = DatabaseSQLite.db.update(DatabaseSQLite.DEALER_INFO_TABLE, contentValues, "dealer_id="+dealerId, null);
        if(result <= 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean updateProduct(String productId, String productName, String finalPrice, String productStatus, String companyName)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseSQLite.PRODUCT_COL_2, productName);
        contentValues.put(DatabaseSQLite.PRODUCT_COL_3, finalPrice);
        contentValues.put(DatabaseSQLite.PRODUCT_COL_4, productStatus);
        contentValues.put(DatabaseSQLite.PRODUCT_COL_5, companyName);
        int result = DatabaseSQLite.db.update(DatabaseSQLite.PRODUCT_INFO_TABLE, contentValues, "product_id="+productId, null);
        if(result <= 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean updateInvoice(String invoiceId, String dealerId, String productIds, String submissionQty, String submissionUnit, String invoicePrice, String orderStatus)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseSQLite.INVOICES_COL_2, dealerId);
        contentValues.put(DatabaseSQLite.INVOICES_COL_3, productIds);
        contentValues.put(DatabaseSQLite.INVOICES_COL_4, submissionQty);
        contentValues.put(DatabaseSQLite.INVOICES_COL_5, submissionUnit);
        contentValues.put(DatabaseSQLite.INVOICES_COL_6, invoicePrice);
        contentValues.put(DatabaseSQLite.INVOICES_COL_7, orderStatus);
        int result = DatabaseSQLite.db.update(DatabaseSQLite.INVOICES_TABLE, contentValues, "invoices_id="+invoiceId, null);
        if(result <= 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean updateBatchWiseStock(String batchStockId, String productId, String batchNo, String quantity, String batchExpiry)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseSQLite.BATCHSTOCK_COL_2, productId);
        contentValues.put(DatabaseSQLite.BATCHSTOCK_COL_3, batchNo);
        contentValues.put(DatabaseSQLite.BATCHSTOCK_COL_4, quantity);
        contentValues.put(DatabaseSQLite.BATCHSTOCK_COL_5, batchExpiry);
        int result = DatabaseSQLite.db.update(DatabaseSQLite.BATCHWISE_STOCK_TABLE, contentValues, "batchstock_id="+batchStockId, null);
        if(result <= 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean updateDiscount(String approvalId, String dealerId, String companyName, String productId, String saleAmount, String discount, String startDate, String endDate)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseSQLite.DISCOUNT_COL_2, dealerId);
        contentValues.put(DatabaseSQLite.DISCOUNT_COL_3, companyName);
        contentValues.put(DatabaseSQLite.DISCOUNT_COL_4, productId);
        contentValues.put(DatabaseSQLite.DISCOUNT_COL_5, saleAmount);
        contentValues.put(DatabaseSQLite.DISCOUNT_COL_6, discount);
        contentValues.put(DatabaseSQLite.DISCOUNT_COL_7, startDate);
        contentValues.put(DatabaseSQLite.DISCOUNT_COL_8, endDate);
        int result = DatabaseSQLite.db.update(DatabaseSQLite.DISCOUNT_TABLE, contentValues, "approval_id="+approvalId, null);
        if(result <= 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean updateBonus(String approvalId, String dealerId, String companyName, String productId, String quantity, String bonus, String startDate, String endDate)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseSQLite.BONUS_COL_2, dealerId);
        contentValues.put(DatabaseSQLite.BONUS_COL_3, companyName);
        contentValues.put(DatabaseSQLite.BONUS_COL_4, productId);
        contentValues.put(DatabaseSQLite.BONUS_COL_5, quantity);
        contentValues.put(DatabaseSQLite.BONUS_COL_6, bonus);
        contentValues.put(DatabaseSQLite.BONUS_COL_7, startDate);
        contentValues.put(DatabaseSQLite.BONUS_COL_8, endDate);
        int result = DatabaseSQLite.db.update(DatabaseSQLite.BONUS_TABLE, contentValues, "approval_id="+approvalId, null);
        if(result <= 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean updateDataBaseSync(String dataName, String updateDate, String updateTime)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseSQLite.SYNCTRACK_COL_3, updateDate);
        contentValues.put(DatabaseSQLite.SYNCTRACK_COL_4, updateTime);
        int result = DatabaseSQLite.db.update(DatabaseSQLite.SYNC_TRACKING_TABLE, contentValues, "data_name='" + dataName + "'", null);
        if(result <= 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean deleteArea(String id)
    {
        int result = DatabaseSQLite.db.delete(DatabaseSQLite.AREA_INFO_TABLE, "area_id="+id, null);
        if(result <= 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean deleteDealer(String id)
    {
        int result = DatabaseSQLite.db.delete(DatabaseSQLite.DEALER_INFO_TABLE, "dealer_id="+id, null);
        if(result <= 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean deleteProduct(String id)
    {
        int result = DatabaseSQLite.db.delete(DatabaseSQLite.PRODUCT_INFO_TABLE, "product_id="+id, null);
        if(result <= 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean deleteInvoice(String id)
    {
        int result = DatabaseSQLite.db.delete(DatabaseSQLite.INVOICES_TABLE, "invoices_id="+id, null);
        if(result <= 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean deleteBatchWiseStock(String id)
    {
        int result = DatabaseSQLite.db.delete(DatabaseSQLite.BATCHWISE_STOCK_TABLE, "batchstock_id="+id, null);
        if(result <= 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean deleteDiscount(String id)
    {
        int result = DatabaseSQLite.db.delete(DatabaseSQLite.DISCOUNT_TABLE, "approval_id="+id, null);
        if(result <= 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean deleteBonus(String id)
    {
        int result = DatabaseSQLite.db.delete(DatabaseSQLite.BONUS_TABLE, "approval_id="+id, null);
        if(result <= 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}
