package com.example.younastraders.tabsdiscount;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.younastraders.DatabaseSQLite;
import com.example.younastraders.GlobalVariables;
import com.example.younastraders.R;
import com.example.younastraders.ui.delivery.DeliveryViewModel;
import com.example.younastraders.ui.viewpending.ViewPendingOrdersFragment;
import com.google.android.material.badge.BadgeDrawable;
import com.google.android.material.tabs.TabLayout;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.util.ArrayList;
import java.util.List;

public class DiscountTabActivity extends Fragment{

    private Toolbar toolbar;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    ViewPagerAdapter viewPagerAdapter;

    private DiscountDealerFragment discountDealerFragment;
    private DiscountCompanyFragment discountCompanyFragment;
    private DiscountProductFragment discountProductFragment;
//    private TravelFragment travelFragment;

    private DeliveryViewModel objDeliveryViewModel;
    LinearLayout lv;
    SearchableSpinner edInvoiceNum;
    LinearLayout[] subLayout;
    int ordersCount;
    String stInvoiceDetail;

    DatabaseSQLite objSqlLite;
    ArrayAdapter<String> invoicesNumAdapter;
    Button btnGetDetail;

    String[] stOrderIds;
    String[] stOrderIdsList;
    TextView[] txOrderId;
    TextView[] txDealerName;
    TextView[] txFinalPrice;
    Button[] btnOps;

    private static final String FRAGMENT_NAME = ViewPendingOrdersFragment.class.getSimpleName();
    private static final String TAG = FRAGMENT_NAME;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        objDeliveryViewModel = ViewModelProviders.of(this).get(DeliveryViewModel.class);
        View root = inflater.inflate(R.layout.activity_tabs_discount, container, false);

        viewPager = root.findViewById(R.id.view_pager);
        tabLayout = root.findViewById(R.id.tab_layout);

        discountDealerFragment = new DiscountDealerFragment();
        discountCompanyFragment = new DiscountCompanyFragment();
        discountProductFragment = new DiscountProductFragment();
//        travelFragment = new TravelFragment();

        tabLayout.setupWithViewPager(viewPager);

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager(), 0);
        viewPagerAdapter.addFragment(discountDealerFragment, "Dealer");
        viewPagerAdapter.addFragment(discountCompanyFragment, "Company");
        viewPagerAdapter.addFragment(discountProductFragment, "Product");
//        viewPagerAdapter.addFragment(travelFragment, "Travel");
        viewPager.setAdapter(viewPagerAdapter);

        tabLayout.getTabAt(0).setIcon(R.drawable.ic_baseline_explore_24);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_baseline_flight_24);
        tabLayout.getTabAt(2).setIcon(R.drawable.ic_baseline_flight_24);
//        tabLayout.getTabAt(2).setIcon(R.drawable.ic_baseline_card_travel_24);

        if(!GlobalVariables.pendingDelivered.equals("0"))
        {
            BadgeDrawable badgeDrawable = tabLayout.getTabAt(1).getOrCreateBadge();
            badgeDrawable.setVisible(true);
            badgeDrawable.setBackgroundColor(Color.parseColor("#1e4355"));
            badgeDrawable.setNumber(Integer.parseInt(GlobalVariables.pendingDelivered));
        }

        return root;
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {

        private List<Fragment> fragments = new ArrayList<>();
        private List<String> fragmentTitle = new ArrayList<>();

        public ViewPagerAdapter(@NonNull FragmentManager fm, int behavior) {
            super(fm, behavior);
        }

        public void addFragment(Fragment fragment, String title) {
            fragments.add(fragment);
            fragmentTitle.add(title);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitle.get(position);
        }
    }
}
