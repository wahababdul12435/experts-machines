package com.example.younastraders;

import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class DeliveredOrderDetail extends AppCompatActivity {
    String orderId;
    GetData objGetData;
    TextView txtInvoiceNum;
    TextView txtDealerName;
    TextView txtDealerContact;
    TextView txtDealerAddress;
    TextView txtInvoicePrice;
    TextView txtPreviousPrice;
    TextView txtNetTotal;
    TextView txtCashCollected;
    Button btnSaveDelivered;
    Button btnSendDelivered;
    Button btnEditDelivered;
    Button btnCancelDelivered;
    public static ProgressBar edProgress;


    LinearLayout lv;
    int itemsCount;
    float invoicePrice;
    String dealerId;
    float pendingPrice;
    float netTotalPrice;
    String stCashCollected;
    SendData objSendData;

    LinearLayout[] subLayout;
    TextView[] txSrNo;
    TextView[] txProductName;
    TextView[] txQuantity;
    TextView[] txUnit;
    Button[] opBtns;
    boolean returnBit = false;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.delivered_order_detail);
        txtInvoiceNum = findViewById(R.id.txt_invoice_num);
        txtDealerName = findViewById(R.id.txt_dealer_name);
        txtDealerContact = findViewById(R.id.txt_dealer_contact);
        txtDealerAddress = findViewById(R.id.txt_dealer_address);
        txtInvoicePrice = findViewById(R.id.txt_invoicers);
        txtPreviousPrice = findViewById(R.id.txt_previousrs);
        txtNetTotal = findViewById(R.id.txt_nettotal);
        txtCashCollected = findViewById(R.id.txt_cash_collected);
        btnSaveDelivered = findViewById(R.id.btn_save_delivered);
        btnSendDelivered = findViewById(R.id.btn_send_delivered);
        btnEditDelivered = findViewById(R.id.btn_edit_delivered);
        btnCancelDelivered = findViewById(R.id.btn_cancel_delivered);
        objSendData = new SendData(this);
        edProgress = findViewById(R.id.ed_progress);
        lv = findViewById(R.id.linear_layout);
        orderId = getIntent().getStringExtra("ORDER_ID");
        txtInvoiceNum.setText(orderId);
        itemsCount = 0;
        invoicePrice = 0;
        objGetData = new GetData(this);
        Cursor res = objGetData.getDeliveryInfo(orderId);
        int num = res.getCount();

        if(num == 0)
        {
            txProductName = new TextView[1];
            lv.addView(createEmptyText());
        }
        else
        {
            subLayout = new LinearLayout[res.getCount()];
            txSrNo = new TextView[res.getCount()];
            txProductName = new TextView[res.getCount()];
            txQuantity = new TextView[res.getCount()];
            txUnit = new TextView[res.getCount()];
            opBtns = new Button[res.getCount()];
            res.moveToNext();
            itemsCount++;
            dealerId = res.getString(1);
            txtDealerName.setText(res.getString(7));
            txtDealerContact.setText(res.getString(8));
            txtDealerAddress.setText(res.getString(9));
            invoicePrice += Float.parseFloat(res.getString(5));
            txtCashCollected.setText(res.getString(11));
            stCashCollected = res.getString(11);
            lv.addView(createLinearLayout(String.valueOf(itemsCount), res.getString(10), res.getString(3), res.getString(4)));
            while(res.moveToNext())
            {
                itemsCount++;
//                txOrderId[itemsCount-1] = res.getString(0);
                lv.addView(createLinearLayout(String.valueOf(itemsCount), res.getString(10), res.getString(3), res.getString(4)));
                invoicePrice += Float.parseFloat(res.getString(5));
            }
            txtInvoicePrice.setText(String.valueOf(invoicePrice));
            res = objGetData.getDealerPending(dealerId);
            num = res.getCount();

            if(num == 0)
            {
                txtPreviousPrice.setText("0.0");
                pendingPrice = 0;
            }
            else
            {
                res.moveToNext();
                txtPreviousPrice.setText(res.getString(0));
                pendingPrice = Float.parseFloat(res.getString(0));
            }
            netTotalPrice = invoicePrice + pendingPrice;
            txtNetTotal.setText(String.valueOf(netTotalPrice));
        }
        btnSaveDelivered.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stCashCollected = txtCashCollected.getText().toString();
                objSendData.saveDelivered(orderId, dealerId, stCashCollected, String.valueOf(returnBit), GlobalVariables.getLatitude(), GlobalVariables.getLongitude(), GlobalVariables.getLocationNameHalf(), GlobalVariables.getStDate(), GlobalVariables.getStTime(), GlobalVariables.getUserId());
            }
        });
        btnSendDelivered.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stCashCollected = txtCashCollected.getText().toString();
                objSendData.sendDelivered(orderId, dealerId, stCashCollected, String.valueOf(returnBit), GlobalVariables.getLatitude(), GlobalVariables.getLongitude(), GlobalVariables.getLocationNameHalf(), GlobalVariables.getStDate(), GlobalVariables.getStTime(), GlobalVariables.getUserId());
            }
        });
        btnCancelDelivered.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Float cashDiff = -1 * (Float.parseFloat(stCashCollected) - invoicePrice);
                objSendData.cancelDelivered(orderId, dealerId,String.valueOf(cashDiff));
            }
        });
    }


    public TextView createEmptyText() {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        txProductName[0] = new TextView(this);
        txProductName[0].setLayoutParams(lparams);
        txProductName[0].setText("No Record to Show");
        txProductName[0].setTextSize(TypedValue.COMPLEX_UNIT_SP,18);
        txProductName[0].setGravity(Gravity.CENTER);
        return txProductName[0];
    }

    public TextView createSrNoText(String text) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(Math.round(getResources().getDimension(R.dimen.invoice_srno)), ViewGroup.LayoutParams.WRAP_CONTENT);
        txSrNo[itemsCount-1] = new TextView(this);
        txSrNo[itemsCount-1].setLayoutParams(lparams);
//        txSrNo[itemsCount-1].setBackgroundColor(Color.BLUE);
        txSrNo[itemsCount-1].setText(text);
        return txSrNo[itemsCount-1];
    }

    public TextView createProductNameText(String text) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(Math.round(getResources().getDimension(R.dimen.invoice_productname)), ViewGroup.LayoutParams.WRAP_CONTENT);
        txProductName[itemsCount-1] = new TextView(this);
        txProductName[itemsCount-1].setLayoutParams(lparams);
//        txProductName[itemsCount-1].setBackgroundColor(Color.RED);
        txProductName[itemsCount-1].setText(text);
        return txProductName[itemsCount-1];
    }

    public TextView createQuantityText(String text) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(Math.round(getResources().getDimension(R.dimen.invoice_quantity)), ViewGroup.LayoutParams.WRAP_CONTENT);
        txQuantity[itemsCount-1] = new TextView(this);
        txQuantity[itemsCount-1].setLayoutParams(lparams);
//        txQuantity[itemsCount-1].setBackgroundColor(Color.GREEN);
        txQuantity[itemsCount-1].setText(text);
        return txQuantity[itemsCount-1];
    }
    public TextView createUnitText(String text) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(Math.round(getResources().getDimension(R.dimen.invoice_unit)), ViewGroup.LayoutParams.WRAP_CONTENT);
        txUnit[itemsCount-1] = new TextView(this);
        txUnit[itemsCount-1].setLayoutParams(lparams);
//        txUnit[itemsCount-1].setBackgroundColor(Color.YELLOW);
        txUnit[itemsCount-1].setText(text);
        return txUnit[itemsCount-1];
    }

    public Button createOpButton() {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        opBtns[itemsCount-1] = new Button(DeliveredOrderDetail.this);
        opBtns[itemsCount-1].setLayoutParams(lparams);
        opBtns[itemsCount-1].setText("X");
        opBtns[itemsCount-1].setTextColor(Color.RED);
        opBtns[itemsCount-1].setBackgroundColor(Color.TRANSPARENT);
        return opBtns[itemsCount-1];
    }

    public LinearLayout createLinearLayout(String SrNo, String productName, String quantity, String unit) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        subLayout[itemsCount-1] = new LinearLayout(this);
        subLayout[itemsCount-1].setOrientation(LinearLayout.HORIZONTAL);
        subLayout[itemsCount-1].setLayoutParams(lparams);
        subLayout[itemsCount-1].addView(createSrNoText(SrNo));
        subLayout[itemsCount-1].addView(createProductNameText(productName));
        subLayout[itemsCount-1].addView(createQuantityText(quantity));
        subLayout[itemsCount-1].addView(createUnitText(unit));
        subLayout[itemsCount-1].addView(createOpButton());
        return subLayout[itemsCount-1];
    }
}