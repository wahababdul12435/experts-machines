package com.example.younastraders.tabsdelivery;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.example.younastraders.DatabaseSQLite;
import com.example.younastraders.DeliveredOrderDetail;
import com.example.younastraders.GetData;
import com.example.younastraders.Home;
import com.example.younastraders.InputFilterMinMax;
import com.example.younastraders.InvoicedOrderDetail;
import com.example.younastraders.MainActivity;
import com.example.younastraders.PendingOrderDetail;
import com.example.younastraders.R;
import com.example.younastraders.SendData;
import com.google.android.material.tabs.TabLayout;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ViewDeliveryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ViewDeliveryFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    LinearLayout lv;
    ProgressBar progressSendAllPending;
    Button btnSendAllPendings;
    LinearLayout[] subLayout;
    GetData objGetData;
    SendData objSendData;

    String[] strOrderId;
    TextView[] txtSrNo;
    TextView[] txtDealerName;
    TextView[] txtInvoiceNum;
    Button[] btnOps;
    String[] strStatus;
    TextView[] txtDate;
    int deliveryCount;
    String strPreDate;
//    SendMessage SM;
    View rootView;
    View[] viewLine;
    boolean setColor = false;

    public ViewDeliveryFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FlightsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ViewDeliveryFragment newInstance(String param1, String param2) {
        ViewDeliveryFragment fragment = new ViewDeliveryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_view_delivery, container, false);
        lv = rootView.findViewById(R.id.linear_layout);
        progressSendAllPending = rootView.findViewById(R.id.progress_sendallpending);
        btnSendAllPendings = rootView.findViewById(R.id.btn_delivery_sendpending);
        objGetData = new GetData(getActivity(), "Pending Cash");
        objSendData = new SendData(getActivity());
        deliveryCount = 0;
        getPendingCashData();
        btnSendAllPendings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressSendAllPending.setVisibility(View.VISIBLE);
//                Cursor res = objGetData.getPendingDelivered();
//                int num = res.getCount();
//                boolean response = false;
//
//                if(num > 0)
//                {
//                    while(res.moveToNext())
//                    {
//                        response = objSendData.sendSavedDelivered(res.getString(0),res.getString(1),res.getString(2),res.getString(3),res.getString(4),res.getString(5),res.getString(6),res.getString(7),res.getString(8),res.getString(9));
//                    }
//                }
//                if(response)
//                {
//                    Toast.makeText(getActivity(), "Delivered Orders Sent", Toast.LENGTH_LONG).show();
//                }
                String response = objSendData.sendAllPendingDelivered();
                Toast.makeText(getActivity(), response, Toast.LENGTH_LONG).show();

                Intent intent = new Intent(getActivity(), Home.class);
                startActivity(intent);
                getActivity().finish();
            }
        });
        return rootView;
    }

    public void getPendingCashData()
    {
        Cursor res = objGetData.getDeliveryDetails();
        int num = res.getCount();

        if(num <= 0)
        {
            txtDealerName = new TextView[1];
            lv.addView(createEmptyText());
        }
        else
        {
            subLayout = new LinearLayout[num];
            txtSrNo = new TextView[num];
            txtDealerName = new TextView[num];
            txtInvoiceNum = new TextView[num];
            txtDate = new TextView[num];
            btnOps = new Button[num];
            strStatus = new String[num];
            strOrderId = new String[num];
            viewLine = new View[num];
            strPreDate = "";
            while(res.moveToNext())
            {
                deliveryCount++;
                strOrderId[deliveryCount-1] = res.getString(0);
                strStatus[deliveryCount-1] = res.getString(4);
                if(!strPreDate.equals(res.getString(3)))
                {
                    strPreDate = res.getString(3);
                    lv.addView(createDateText(strPreDate));
                }
                lv.addView(createLinearLayout(String.valueOf(deliveryCount), res.getString(2), res.getString(0), res.getString(4)));
                lv.addView(createLineView());
//                addListenerToButtons();
            }
        }
    }


    public TextView createEmptyText() {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        txtDealerName[0] = new TextView(getActivity());
        txtDealerName[0].setLayoutParams(lparams);
        txtDealerName[0].setText("No Record to Show");
        txtDealerName[0].setTextSize(TypedValue.COMPLEX_UNIT_SP,18);
        txtDealerName[0].setGravity(Gravity.CENTER);
        return txtDealerName[0];
    }

    public TextView createSrNoText(String text) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(Math.round(getResources().getDimension(R.dimen.invoice_srno)), ViewGroup.LayoutParams.WRAP_CONTENT);
        txtSrNo[deliveryCount-1] = new TextView(getActivity());
        txtSrNo[deliveryCount-1].setLayoutParams(lparams);
//        txtSrNo[deliveryCount-1].setBackgroundColor(Color.RED);
        txtSrNo[deliveryCount-1].setText(text);
        txtSrNo[deliveryCount-1].setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        return txtSrNo[deliveryCount-1];
    }

    public TextView createDealerText(String text) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(Math.round(getResources().getDimension(R.dimen.dealer_pending_cash)), ViewGroup.LayoutParams.WRAP_CONTENT);
        txtDealerName[deliveryCount-1] = new TextView(getActivity());
        txtDealerName[deliveryCount-1].setLayoutParams(lparams);
//        txtDealerName[deliveryCount-1].setBackgroundColor(Color.RED);
        txtDealerName[deliveryCount-1].setText(text);
        return txtDealerName[deliveryCount-1];
    }

    public TextView createinvoiceNumText(String text) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        txtInvoiceNum[deliveryCount-1] = new TextView(getActivity());
        txtInvoiceNum[deliveryCount-1].setLayoutParams(lparams);
//        txtInvoiceNum[deliveryCount-1].setBackgroundColor(Color.GREEN);
        txtInvoiceNum[deliveryCount-1].setText(text);
        txtInvoiceNum[deliveryCount-1].setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        return txtInvoiceNum[deliveryCount-1];
    }

    public View createLineView() {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,  Math.round(getResources().getDimension(R.dimen.line)));
        viewLine[deliveryCount-1] = new View(getActivity());
        viewLine[deliveryCount-1].setLayoutParams(lparams);
        viewLine[deliveryCount-1].setBackgroundColor(Color.BLACK);
        return viewLine[deliveryCount-1];
    }

    public TextView createDateText(String text) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        txtDate[deliveryCount-1] = new TextView(getActivity());
        txtDate[deliveryCount-1].setLayoutParams(lparams);
//        txtDate[deliveryCount-1].setBackgroundColor(Color.YELLOW);
        txtDate[deliveryCount-1].setText(text);
        txtDate[deliveryCount-1].setGravity(Gravity.CENTER);
        txtDate[deliveryCount-1].setTextColor(Color.BLACK);
        return txtDate[deliveryCount-1];
    }

//    public Button createOpButton(String status) {
//        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//        btnOps[deliveryCount-1] = new Button(getActivity());
//        btnOps[deliveryCount-1].setLayoutParams(lparams);
//        if(status.equals("Pending"))
//        {
//            btnOps[deliveryCount-1].setText("ACTION");
//        }
//        else
//        {
//            btnOps[deliveryCount-1].setText("SENT");
//        }
//        btnOps[deliveryCount-1].setBackgroundColor(Color.TRANSPARENT);
//        return btnOps[deliveryCount-1];
//    }

    public LinearLayout createLinearLayout(String srNo, String dealerName, String invoiceNum, String status) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        subLayout[deliveryCount-1] = new LinearLayout(getActivity());
        subLayout[deliveryCount-1].setOrientation(LinearLayout.HORIZONTAL);
        subLayout[deliveryCount-1].setLayoutParams(lparams);
        subLayout[deliveryCount-1].addView(createSrNoText(srNo));
        subLayout[deliveryCount-1].addView(createDealerText(dealerName));
        subLayout[deliveryCount-1].addView(createinvoiceNumText(invoiceNum));
//        subLayout[deliveryCount-1].addView(createDateText(date));
//        subLayout[deliveryCount-1].addView(createOpButton(status));
        if(setColor)
        {
            subLayout[deliveryCount-1].setBackgroundColor(Color.parseColor("#E1E1E1"));
            setColor = false;
        }
        else
        {
            setColor = true;
        }
        if(status.equals("Pending"))
        {
            subLayout[deliveryCount-1].setBackgroundColor(Color.parseColor("#FFA8A8"));
            subLayout[deliveryCount-1].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(strStatus[deliveryCount-1].equals("Pending"))
                    {
                        Intent intent = new Intent(getActivity(), DeliveredOrderDetail.class);
                        intent.putExtra("ORDER_ID", strOrderId[deliveryCount-1]);
                        startActivity(intent);
                    }
                }
            });
        }
        else
        {
//            subLayout[deliveryCount-1].setBackgroundColor(Color.parseColor("#B0FFA8"));
        }
        return subLayout[deliveryCount-1];
    }

//    public void addListenerToButtons()
//    {
//        for(int i=0; i<deliveryCount; i++)
//        {
//            final int finalI = i;
//            btnOps[i].setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if(strStatus[finalI].equals("Pending"))
//                    {
//                        Intent intent = new Intent(getActivity(), DeliveredOrderDetail.class);
//                        intent.putExtra("ORDER_ID", strOrderId[finalI]);
//                        startActivity(intent);
//                    }
//                }
//            });
//        }
//    }

//    interface SendMessage {
//        void sendData(String message);
//    }
//
//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//
//        try {
//            SM = (SendMessage) getActivity();
//        } catch (ClassCastException e) {
//            throw new ClassCastException("Error in retrieving data. Please try again");
//        }
//    }
}