package com.example.younastraders;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class GlobalVariables {
    public static String softwareId = "0";
    public static String userId = "1";
    public static String autoSend = "false";
    public static String latitude;
    public static String longitude;
    public static String locationNameHalf;
    public static String locationNameFull;
    public static String stDate;
    public static String stTime;
    public static String pendingBookings = "0";
    public static String pendingDelivered = "0";
    public static String pendingCash = "0";
    public static String pendingReturn = "0";
    public static String areaLastSyncedDate = "";
    public static String areaLastSyncedTime = "";
    public static String dealerLastSyncedDate = "";
    public static String dealerLastSyncedTime = "";
    public static String productLastSyncedDate = "";
    public static String productLastSyncedTime = "";
    public static String dealerGPSLastSyncedDate = "";
    public static String dealerGPSLastSyncedTime = "";
    public static String dealerPendingLastSyncedDate = "";
    public static String dealerPendingLastSyncedTime = "";
    public static String batchLastSyncedDate = "";
    public static String batchLastSyncedTime = "";
    public static String discountLastSyncedDate = "";
    public static String discountLastSyncedTime = "";
    public static String bonusLastSyncedDate = "";
    public static String bonusLastSyncedTime = "";
    public Context ctx;

    public GlobalVariables(Context ctx)
    {
        this.ctx = ctx;
    }

    public static String getUserId() {
        return userId;
    }

    public static void setUserId(String userId) {
        GlobalVariables.userId = userId;
    }

    public static String getLatitude() {
        return latitude;
    }

    public static void setLatitude(String latitude) {
        GlobalVariables.latitude = latitude;
    }

    public static String getLongitude() {
        return longitude;
    }

    public static void setLongitude(String longitude) {
        GlobalVariables.longitude = longitude;
    }

    public static String getLocationNameHalf() {
        return locationNameHalf;
    }

    public static void setLocationNameHalf(String locationNameHalf) {
        GlobalVariables.locationNameHalf = locationNameHalf;
    }

    public static String getLocationNameFull() {
        return locationNameFull;
    }

    public static void setLocationNameFull(String locationNameFull) {
        GlobalVariables.locationNameFull = locationNameFull;
    }

    public static String getStDate() {
        Date objDate = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd/MMM/yyyy");
        stDate = df.format(objDate);
        return stDate;
    }

    public static String getStTime() {
        Date objDate = Calendar.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        stTime = sdf.format(objDate);
        return stTime;
    }

    public static String getAutoSend() {
        return autoSend;
    }

    public void getPending()
    {
        GetData objGetData = new GetData(ctx);
        Cursor res = objGetData.getPendingBookingCount();
        res.moveToNext();
        pendingBookings = res.getString(0);

        res = objGetData.getPendingDeliveredCount();
        res.moveToNext();
        pendingDelivered = res.getString(0);

        res = objGetData.getPendingCashCount();
        res.moveToNext();
        pendingCash = res.getString(0);

        res = objGetData.getPendingReturnCount();
        res.moveToNext();
        pendingReturn = res.getString(0);
    }
}
