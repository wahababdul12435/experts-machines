package com.example.younastraders;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import com.example.younastraders.ui.home.HomeFragment;
import com.example.younastraders.ui.viewpending.ViewPendingOrdersFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import android.view.MenuItem;
import android.view.View;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import android.content.DialogInterface;

public class Home extends AppCompatActivity implements LocationListener {

    private AppBarConfiguration mAppBarConfiguration;
    GetData objGetData;
    NavigationView navigationView;

    protected LocationManager locationManager;
    double lat;
    double lon;
    String orderPlaceArea;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
//        FragmentManager manager = getSupportFragmentManager();
//        ViewPendingOrdersFragment objPendingFragment = new ViewPendingOrdersFragment();
//        FragmentTransaction transaction = manager.beginTransaction();
//        transaction.remove(objPendingFragment).commit();
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        lat = 0;
        lon = 0;
        orderPlaceArea = "";
        if(!checkLocationPermission())
        {
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }
        if(checkLocationPermission())
        {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        }

//        FloatingActionButton fab = findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_viewpendingorders,
                R.id.nav_delivery, R.id.nav_collection, R.id.nav_return, R.id.nav_settings, R.id.dealer_pin)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        Intent intent = new Intent(this, SyncService.class);
        startService(intent);

        objGetData = new GetData(this);
        getPending();
        statusCheck();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

//    @Override
//    public void onResume(){
//        super.onResume();
//        Intent intent = getIntent();
//        Bundle extras = intent.getExtras();
//        if (extras != null) {
//            if (extras.containsKey("frag")) {
//                String frag = intent.getExtras().getString("frag");
//                intent.removeExtra("frag");
//                switch(frag){
//                    case "ViewPendingOrdersFragment":
//                        //here you can set Fragment B to your activity as usual;
//                        NavigationView navigationView = (NavigationView) this.findViewById(R.id.nav_view);
//                        navigationView.getMenu().getItem(3).setChecked(true);
//                        break;
//                }
//            }
//        }
//        else
//        {
//            FragmentManager manager = getSupportFragmentManager();
//            ViewPendingOrdersFragment objPendingFragment = new ViewPendingOrdersFragment();
//            FragmentTransaction transaction = manager.beginTransaction();
//            transaction.remove(objPendingFragment).commit();
//            clearStack();
//        }
//    }

    public void getPending()
    {
        // on Button Click
        Menu menu = navigationView.getMenu();
        MenuItem menu_navBooking = menu.findItem(R.id.nav_gallery);
        MenuItem menu_navDelivered = menu.findItem(R.id.nav_delivery);
        MenuItem menu_navCash = menu.findItem(R.id.nav_collection);
        MenuItem menu_navReturn = menu.findItem(R.id.nav_return);
        GlobalVariables objGlobalVariables = new GlobalVariables(this);
        objGlobalVariables.getPending();
        if(!GlobalVariables.pendingBookings.equals("0"))
        {
            menu_navBooking.setTitle("Send Pending Orders ("+GlobalVariables.pendingBookings+")");
        }

        if(!GlobalVariables.pendingDelivered.equals("0"))
        {
            menu_navDelivered.setTitle("Delivery and Collection ("+GlobalVariables.pendingDelivered+")");
        }

        if(!GlobalVariables.pendingCash.equals("0"))
        {
            menu_navCash.setTitle("Cash Collection ("+GlobalVariables.pendingCash+")");
        }

        if(!GlobalVariables.pendingReturn.equals("0"))
        {
            menu_navReturn.setTitle("Returns ("+GlobalVariables.pendingReturn+")");
        }
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        getCurrentLocation(location);
    }

    public void getCurrentLocation(Location location)
    {
        try {
            String addr = getAddress(location.getLatitude(), location.getLongitude());
            lat = location.getLatitude();
            lon = location.getLongitude();
            orderPlaceArea = addr;
            GlobalVariables.setLatitude(String.valueOf(lat));
            GlobalVariables.setLongitude(String.valueOf(lon));
            GlobalVariables.setLocationNameHalf(orderPlaceArea);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public String getAddress(double lat, double lng) throws IOException {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
        Address obj = addresses.get(0);
        //            String add = obj.getAddressLine(0);
        String add;
        orderPlaceArea = obj.getAddressLine(0);
        if(obj.getSubLocality() != null)
        {
            add = obj.getSubLocality()+", "+obj.getLocality();
        }
        else if(obj.getSubThoroughfare() != null)
        {
            add = obj.getSubThoroughfare()+", "+obj.getLocality();
        }
        else
        {
            add = obj.getThoroughfare()+", "+obj.getLocality();
        }

        return add;
    }

    public boolean checkLocationPermission()
    {
        String permission = "android.permission.ACCESS_FINE_LOCATION";
        int res = this.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    public void statusCheck() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                        System.exit(0);
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }
}
