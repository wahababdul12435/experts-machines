package com.example.younastraders;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public class FetchDataMysql {

    String[] newDataUpdate;
    String[][] areaInfo;
    String[][] dealerInfo;
    String[][] productInfo;
    String[][] invoicesData;
    String[][] dealerPendingPayments;
    String[][] batchwiseStock;
    String[][] discountInfo;
    String[][] bonusInfo;
    String[] dealerGPSData;
    String dataTableName;
    Context ctx;

    public FetchDataMysql(Context ctx, String dataTableName) {
        this.ctx = ctx;
        this.dataTableName = dataTableName;
//        getJSON("http://10.0.2.2/GitKraken/distribution/experts-machines/PHP/YounasTraders/AndroidDataGet/getData.php?tablename="+dataTableName);

//        ----------------------------------------- LAPTOP ID ---------------------------------
//        getJSON("http://192.168.43.176/GitKraken/distribution/experts-machines/PHP/YounasTraders/AndroidDataGet/getData.php?tablename="+dataTableName);

//        ----------------------------------------- OFFICE SYSTEM ID ---------------------------------
//        getJSON("http://192.168.10.2/GitKraken/distribution/experts-machines/PHP/YounasTraders/AndroidDataGet/getData.php?tablename="+dataTableName);

//        ----------------------------------------- LIVE SERVER ID ---------------------------------
        getJSON("https://tuberichy.com/ProfitFlow/AndroidDataGet/getData.php?softwareid="+GlobalVariables.softwareId+"&tablename="+dataTableName);
    }

    public FetchDataMysql(Context ctx, String dataTableName, String updatedDate, String updatedTime) {
        this.ctx = ctx;
        this.dataTableName = dataTableName;
//        getJSON("http://10.0.2.2/GitKraken/distribution/experts-machines/PHP/YounasTraders/AndroidDataGet/getData.php?tablename="+dataTableName);

//        ----------------------------------------- LAPTOP ID ---------------------------------
//        getJSON("http://192.168.43.176/GitKraken/distribution/experts-machines/PHP/YounasTraders/AndroidDataGet/getData.php?tablename="+dataTableName);

//        ----------------------------------------- OFFICE SYSTEM ID ---------------------------------
//        getJSON("http://192.168.10.2/GitKraken/distribution/experts-machines/PHP/YounasTraders/AndroidDataGet/getData.php?tablename="+dataTableName);

//        ----------------------------------------- LIVE SERVER ID ---------------------------------
        getJSON("https://tuberichy.com/ProfitFlow/AndroidDataGet/getData.php?softwareid="+GlobalVariables.softwareId+"&lastupdatedate="+updatedDate+"&lastupdatetime="+updatedTime+"&tablename="+dataTableName);
    }

    private CallBack callBack;

    public CallBack getCallBack() {
        return callBack;
    }

    public void setCallBack(CallBack callBack) {
        this.callBack = callBack;
    }


    public void getJSON(final String urlWebService) {

        class GetJSON extends AsyncTask<Void, Void, String> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }


            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                if(callBack != null && s != null)
                {
                    try {
                        loadIntoListView(s);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                callBack.onResult(s);
            }

            @Override
            protected String doInBackground(Void... voids) {
                try
                {
                    URL url;
                    if(dataTableName.equals("discount") || dataTableName.equals("bonus"))
                    {
                        url = new URL(urlWebService+"&date="+GlobalVariables.getStDate());
                    }
                    else if(dataTableName.equals("new_data_update"))
                    {
                        url = new URL(urlWebService+"&user_id="+GlobalVariables.getUserId());
                    }
                    else
                    {
                        url = new URL(urlWebService);
                    }

                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    con.setConnectTimeout(2 * 1000);
                    con.connect();
                    if (con.getResponseCode() == 200)
                    {
                        StringBuilder sb = new StringBuilder();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));
                        String json;
                        while ((json = bufferedReader.readLine()) != null)
                        {
                            sb.append(json + "\n");
                        }
                        return sb.toString().trim();
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception e)
                {
                    return null;
                }
            }
        }
        GetJSON getJSON = new GetJSON();
        getJSON.execute();
    }

    private void loadIntoListView(String json) throws JSONException {
        JSONArray jsonArray = new JSONArray(json);
        if(dataTableName.equals("new_data_update"))
        {
            newDataUpdate = new String[5];
            JSONObject obj = jsonArray.getJSONObject(0);
            newDataUpdate[0] = obj.getString("dealer_info");
            newDataUpdate[1] = obj.getString("area_info");
            newDataUpdate[2] = obj.getString("product_info");
            newDataUpdate[3] = obj.getString("discount_info");
            newDataUpdate[4] = obj.getString("bonus_info");
        }
        else if(dataTableName.equals("area_info"))
        {
            areaInfo = new String[jsonArray.length()][5];
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                areaInfo[i][0] = obj.getString("id");
                areaInfo[i][1] = obj.getString("city_id");
                areaInfo[i][2] = obj.getString("area_name");
                areaInfo[i][3] = obj.getString("area_status");
                areaInfo[i][4] = obj.getString("ops");
            }
        }
        else if(dataTableName.equals("dealer_info"))
        {
            dealerInfo = new String[jsonArray.length()][11];
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                dealerInfo[i][0] = obj.getString("id");
                dealerInfo[i][1] = obj.getString("areaid");
                dealerInfo[i][2] = obj.getString("dealer_name");
                dealerInfo[i][3] = obj.getString("dealer_contact");
                dealerInfo[i][4] = obj.getString("dealer_address");
                dealerInfo[i][5] = obj.getString("dealer_type");
                dealerInfo[i][6] = obj.getString("dealer_cnic");
                dealerInfo[i][7] = obj.getString("dealer_licnum");
                dealerInfo[i][8] = obj.getString("dealer_licexp");
                dealerInfo[i][9] = obj.getString("dealer_status");
                dealerInfo[i][10] = obj.getString("ops");
            }
        }
        else if(dataTableName.equals("product_info"))
        {
            productInfo = new String[jsonArray.length()][6];
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                productInfo[i][0] = obj.getString("id");
                productInfo[i][1] = obj.getString("product_name");
                productInfo[i][2] = obj.getString("final_price");
                productInfo[i][3] = obj.getString("product_status");
                productInfo[i][4] = obj.getString("company_name");
                productInfo[i][5] = obj.getString("ops");
            }
        }
        else if(dataTableName.equals("invoices_data"))
        {
            invoicesData = new String[jsonArray.length()][8];
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                invoicesData[i][0] = obj.getString("id");
                invoicesData[i][1] = obj.getString("dealer_id");
                invoicesData[i][2] = obj.getString("product_ids");
                invoicesData[i][3] = obj.getString("submission_qty");
                invoicesData[i][4] = obj.getString("submission_unit");
                invoicesData[i][5] = obj.getString("invoice_price");
                invoicesData[i][6] = obj.getString("order_status");
                invoicesData[i][7] = obj.getString("ops");
            }
        }
        else if(dataTableName.equals("dealer_gps_location"))
        {
            dealerGPSData = new String[jsonArray.length()];
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                dealerGPSData[i] = obj.getString("dealer_id");
            }
        }
        else if(dataTableName.equals("dealer_pendings"))
        {
            dealerPendingPayments = new String[jsonArray.length()][2];
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                dealerPendingPayments[i][0] = obj.getString("dealer_id");
                dealerPendingPayments[i][1] = obj.getString("pending_payments");
            }
        }
        else if(dataTableName.equals("batchwise_stock"))
        {
            batchwiseStock = new String[jsonArray.length()][6];
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                batchwiseStock[i][0] = obj.getString("batchstock_id");
                batchwiseStock[i][1] = obj.getString("product_id");
                batchwiseStock[i][2] = obj.getString("batch_no");
                batchwiseStock[i][3] = obj.getString("quantity");
                batchwiseStock[i][4] = obj.getString("batch_expiry");
                batchwiseStock[i][5] = obj.getString("ops");
            }
        }
        else if(dataTableName.equals("discount"))
        {
            discountInfo = new String[jsonArray.length()][9];
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                discountInfo[i][0] = obj.getString("approval_id");
                discountInfo[i][1] = obj.getString("dealer_id");
                discountInfo[i][2] = obj.getString("company_name");
                discountInfo[i][3] = obj.getString("product_id");
                discountInfo[i][4] = obj.getString("sale_amount");
                discountInfo[i][5] = obj.getString("discount");
                String startDate = obj.getString("start_date");
                if(!startDate.equals(null) && !startDate.equals(""))
                {
                    String[] startDateArr = startDate.split("/");
                    startDate = startDateArr[2]+getMonthNum(startDateArr[1])+startDateArr[0];
                    discountInfo[i][6] = startDate;
                }
                else
                {
                    discountInfo[i][6] = "0";
                }
                String endDate = obj.getString("end_date");
                if(!endDate.equals(null) && !endDate.equals(""))
                {
                    String[] endtDateArr = endDate.split("/");
                    endDate = endtDateArr[2]+getMonthNum(endtDateArr[1])+endtDateArr[0];
                    discountInfo[i][7] = endDate;
                }
                else
                {
                    discountInfo[i][7] = "0";
                }
                discountInfo[i][8] = obj.getString("ops");
            }
        }
        else if(dataTableName.equals("bonus"))
        {
            bonusInfo = new String[jsonArray.length()][9];
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                bonusInfo[i][0] = obj.getString("approval_id");
                bonusInfo[i][1] = obj.getString("dealer_id");
                bonusInfo[i][2] = obj.getString("company_name");
                bonusInfo[i][3] = obj.getString("product_id");
                bonusInfo[i][4] = obj.getString("quantity");
                bonusInfo[i][5] = obj.getString("bonus");
                String startDate = obj.getString("start_date");
                if(!startDate.equals(null) && !startDate.equals(""))
                {
                    String[] startDateArr = startDate.split("/");
                    
                    startDate = startDateArr[2]+getMonthNum(startDateArr[1])+startDateArr[0];
                    bonusInfo[i][6] = startDate;
                }
                else
                {
                    bonusInfo[i][6] = "0";
                }
                String endDate = obj.getString("end_date");
                if(!endDate.equals(null) && !endDate.equals(""))
                {
                    String[] endtDateArr = endDate.split("/");
                    endDate = endtDateArr[2]+getMonthNum(endtDateArr[1])+endtDateArr[0];
                    bonusInfo[i][7] = endDate;
                }
                else
                {
                    bonusInfo[i][7] = "0";
                }
                bonusInfo[i][8] = obj.getString("ops");
            }
        }

    }
    
    private String getMonthNum(String month)
    {
        if(month.equals("Jan"))
            return "01";
        else if(month.equals("Feb"))
            month = "02";
        else if(month.equals("Mar"))
            month = "03";
        else if(month.equals("Apr"))
            return "04";
        else if(month.equals("May"))
            return "05";
        else if(month.equals("Jun"))
            return "06";
        else if(month.equals("Jul"))
            return "07";
        else if(month.equals("Aug"))
            return "08";
        else if(month.equals("Sep"))
            return "09";
        else if(month.equals("Oct"))
            return "10";
        else if(month.equals("Nov"))
            return "11";
        else if(month.equals("Dec"))
            return "12";

        return "00";
    }

    public String[] getNewDataUpdate() {
        return newDataUpdate;
    }

    public String[][] getAreaInfo() {
        return areaInfo;
    }

    public String[][] getDealerInfo() {
        return dealerInfo;
    }

    public String[][] getProductInfo() {
        return productInfo;
    }

    public String[] getDealerGPSData() {
        return dealerGPSData;
    }

    public String[][] getInvoicesData() {
        return invoicesData;
    }

    public String[][] getDealerPendingPayments() {
        return dealerPendingPayments;
    }

    public String[][] getBatchwiseStock() {
        return batchwiseStock;
    }

    public String[][] getDiscountInfo() {
        return discountInfo;
    }

    public String[][] getBonusInfo() {
        return bonusInfo;
    }

    public interface CallBack{
        void onResult(String value);
    }
}