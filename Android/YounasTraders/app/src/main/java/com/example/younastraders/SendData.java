package com.example.younastraders;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.younastraders.ui.dealerpin.DealerPinFragment;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

public class SendData {
    String status;
    BackgroundTask objBackground;
    BackgroundTask2 objBackground2;
    LiveLocationSend objLiveLocationSend;
    MysqlDataSend objMysql;
    DatabaseSQLite objDatabaseSqlite;
    Context ctx;
    String responseStatus;
    String nextBackgroudTask;
    GetData objGetData;
    int sendPendingBookingTrack;

    public SendData(Context ctx)
    {
        this.ctx = ctx;
        this.objDatabaseSqlite = new DatabaseSQLite(ctx);
        this.objBackground = new BackgroundTask(ctx);
        this.objBackground2 = new BackgroundTask2(ctx);
        this.objLiveLocationSend = new LiveLocationSend(ctx);
        this.objMysql = new MysqlDataSend(ctx);
        this.status = "Pending";
        this.responseStatus = "";
        this.objGetData = new GetData(ctx);
        sendPendingBookingTrack = 0;
    }

    public String saveBooking(final String dealerId, final String productId, final String quantity, final String unit, final String orderPrice, final String bonus, final String discount, final String finalPrice, final String latitude, final String longitude, final String order_place_area, final String stDate, final String stTime, final String salesmanId)
    {
        status = "Pending";
        boolean result = objDatabaseSqlite.insertData(dealerId, productId, quantity, unit, orderPrice, bonus, discount, finalPrice, latitude, longitude, order_place_area, stDate, stTime, salesmanId, status);
        if(result)
        {
            responseStatus = "Order Saved";
        }
        else
        {
            responseStatus = "Order Not Saved";
        }
        Toast.makeText(ctx, responseStatus, Toast.LENGTH_LONG).show();
        Intent intent = new Intent(ctx, Home.class);
        ctx.startActivity(intent);
        ((Activity)ctx).finish();
        return responseStatus;
    }

    public String sendBooking(final String dealerId, final String productId, final String quantity, final String unit, final String orderPrice, final String bonus, final String discount, final String finalPrice, final String latitude, final String longitude, final String order_place_area, final String stDate, final String stTime, final String salesmanId)
    {
        objMysql.setCallBack(new MysqlDataSend.CallBack() {
            @Override
            public void onResult(String value) {
                if(value.equals("Order Sent"))
                {
                    status = "Sent";
                    responseStatus = "Order Sent ";
                }
                else
                {
                    status = "Pending";
                    responseStatus = "Order Not Sent ";
                }
                boolean result = objDatabaseSqlite.insertData(dealerId, productId, quantity, unit, orderPrice, bonus, discount, finalPrice, latitude, longitude, order_place_area, stDate, stTime, salesmanId, status);
                if(result == true)
                {
                    responseStatus += " and Saved";
                }
                else
                {
                    responseStatus += " and Not Saved";
                }
                Toast.makeText(ctx, responseStatus, Toast.LENGTH_LONG).show();
                Intent intent = new Intent(ctx, Home.class);
                ctx.startActivity(intent);
                ((Activity)ctx).finish();
            }
        });
        if(!MysqlDataSend.inProgress)
        {
            MysqlDataSend.inProgress = true;
            status = "Pending";
            objMysql.execute("order", dealerId, productId, quantity, unit, orderPrice, bonus, discount, finalPrice, latitude, longitude, order_place_area, stDate, stTime, salesmanId, status);
        }
        return responseStatus;
    }

    public Boolean sendSavedBooking(final String orderId, final String dealerId, final String productId, final String quantity, final String unit, final String orderPrice, final String bonus, final String discount, final String finalPrice, final String latitude, final String longitude, final String order_place_area, final String stDate, final String stTime, final String salesmanId, final String orderStatus)
    {
        final boolean[] result = {false};
        objMysql.setCallBack(new MysqlDataSend.CallBack() {
            @Override
            public void onResult(String value) {
                if(value.equals("Order Sent"))
                {
                    status = "Sent";
                    Toast.makeText(ctx, "All Pending Orders Submitted", Toast.LENGTH_LONG).show();
                    result[0] = objDatabaseSqlite.updateCurrentStatus(orderId, status);
                }
                else
                {
                    result[0] = false;
                }
                Intent intent = new Intent(ctx, Home.class);
                ctx.startActivity(intent);
                ((Activity)ctx).finish();
            }
        });
        if(!MysqlDataSend.inProgress)
        {
            MysqlDataSend.inProgress = true;
            status = "Pending";
            objMysql.execute("order", dealerId, productId, quantity, unit, orderPrice, bonus, discount, finalPrice, latitude, longitude, order_place_area, stDate, stTime, salesmanId, orderStatus);
        }
        return result[0];
    }

    public String updateBooking(final String orderId, final String dealerId, final String productId, final String quantity, final String unit, final String orderPrice, final String bonus, final String discount, final String finalPrice, final String latitude, final String longitude, final String order_place_area, final String stDate, final String stTime, final String salesmanId)
    {
        status = "Pending";
        boolean result = objDatabaseSqlite.updateBookingData(orderId, dealerId, productId, quantity, unit, orderPrice, bonus, discount, finalPrice, latitude, longitude, order_place_area, stDate, stTime, salesmanId, status);
        if(result)
        {
            responseStatus = "Order Updated";
        }
        else
        {
            responseStatus = "Order Not Updated";
        }
        Toast.makeText(ctx, responseStatus, Toast.LENGTH_LONG).show();
        return responseStatus;
    }

    public String registerUser(final String userName, final String softwareId, final String userId, final String password, final String userPower, final String stDate, final String autoSend)
    {
        objMysql.setCallBack(new MysqlDataSend.CallBack() {
            @Override
            public void onResult(String value) {
                if(value == "Registration Request Sent Successfully")
                {
                    status = "Pending";
                    responseStatus = "Registration Request Sent";
                }
                else
                {
                    status = "Not Sent";
                    responseStatus = "Registration Request Not Sent";
                }
                boolean result = objDatabaseSqlite.registerUser(userName, softwareId, userId, password, userPower, stDate, autoSend, status);
                if(result)
                {
                    responseStatus += " and Saved";
                }
                else
                {
                    responseStatus += " and unable to Save";
                }
                Toast.makeText(ctx, responseStatus, Toast.LENGTH_LONG).show();
                Intent intent = new Intent(ctx, LoginActivity.class);
                ctx.startActivity(intent);
                ((Activity)ctx).finish();
            }
        });
        if(!MysqlDataSend.inProgress)
        {
            MysqlDataSend.inProgress = true;
            objMysql.execute("register", userName, softwareId, userId, password, userPower, stDate,  status);
        }
        return responseStatus;
    }

    public String reSendRegisterUser(final String userName, final String softwareId, final String userId, final String password, final String userPower, final String stDate)
    {
        objMysql.setCallBack(new MysqlDataSend.CallBack() {
            @Override
            public void onResult(String value) {
                boolean result = false;
                if(value == "Registration Request Sent Successfully")
                {
                    status = "Pending";
                    responseStatus = "Registration Request Sent";
//                    result = objDatabaseSqlite.updateUser(userName, userId, password, userPower, stDate, status);
                    result = objDatabaseSqlite.updateUser(userId, "status", status);
                }
                else
                {
                    status = "Not Sent";
                    responseStatus = "Registration Request Not Sent";
                }
                Toast.makeText(ctx, responseStatus, Toast.LENGTH_LONG).show();
                Intent intent = new Intent(ctx, LoginActivity.class);
                ctx.startActivity(intent);
                ((Activity)ctx).finish();
            }
        });
        if(!MysqlDataSend.inProgress)
        {
            MysqlDataSend.inProgress = true;
            objMysql.execute("register", userName, softwareId, userId, password, userPower, stDate,  status);
        }
        return responseStatus;
    }

    public String checkRegistrationsStatus(final String userId)
    {
        objMysql.setCallBack(new MysqlDataSend.CallBack() {
            @Override
            public void onResult(String value) {
                if(value.equals("1"))
                {
                    status = "Approved";
                    boolean result = objDatabaseSqlite.updateUser(userId, "status", status);
                    if(result)
                    {
                        Intent intent = new Intent(ctx, LoginActivity.class);
                        ctx.startActivity(intent);
                        ((Activity)ctx).finish();
                    }
                }
            }
        });
        if(!MysqlDataSend.inProgress)
        {
            MysqlDataSend.inProgress = true;
            objMysql.execute("registerStatus", userId);
        }
        return responseStatus;
    }

    public String sendAllPendingBookings()
    {
        String orderId = "";
        String dealerId = "";
        String productId = "";
        String quantity = "";
        String unit = "";
        String orderPrice = "";
        String bonus = "";
        String discount = "";
        String finalPrice = "";
        String latitude = "";
        String longitude = "";
        String order_place_area = "";
        String salessmanId = "";
        String date = "";
        String time = "";
        String status = "";
        Boolean returnResponse = false;
        int i = 0;
        int num = 0;
        Cursor res = null;
        boolean iter = false;
        // on Button Click

        res = objDatabaseSqlite.getAllPendingBookings();
        num = res.getCount();

        if(num <= 0)
        {
            return "Orders already Submitted";
        }
        else
        {
            StringBuffer objBuffer = new StringBuffer();
            while(res.moveToNext())
            {
                if(!orderId.equals(""))
                {
                    orderId += "_--_"+res.getString(0);
                    dealerId += "_--_"+res.getString(1);
                    productId += "_--_"+res.getString(2);
                    quantity += "_--_"+res.getString(3);
                    unit += "_--_"+res.getString(4);
                    orderPrice += "_--_"+res.getString(5);
                    bonus += "_--_"+res.getString(6);
                    discount += "_--_"+res.getString(7);
                    finalPrice += "_--_"+res.getString(8);
                    latitude += "_--_"+res.getString(9);
                    longitude += "_--_"+res.getString(10);
                    order_place_area += "_--_"+res.getString(11);
                    date += "_--_"+res.getString(12);
                    time += "_--_"+res.getString(13);
                    salessmanId += "_--_"+res.getString(14);
                    status += "_--_"+"Pending";
                }
                else
                {
                    orderId = res.getString(0);
                    dealerId = res.getString(1);
                    productId = res.getString(2);
                    quantity = res.getString(3);
                    unit = res.getString(4);
                    orderPrice = res.getString(5);
                    bonus = res.getString(6);
                    discount = res.getString(7);
                    finalPrice = res.getString(8);
                    latitude = res.getString(9);
                    longitude = res.getString(10);
                    order_place_area = res.getString(11);
                    date = res.getString(12);
                    time = res.getString(13);
                    salessmanId = res.getString(14);
                    status = "Pending";
                }

            }
            if(!MysqlDataSend.inProgress)
            {
                returnResponse = sendSavedBooking(orderId, dealerId, productId, quantity, unit, orderPrice, bonus, discount, finalPrice, latitude, longitude, order_place_area, date, time, salessmanId, status);
            }
            else
            {
                returnResponse = false;
            }

            return "";
        }
    }

    public String sendAllPendingDelivered()
    {
        String orderId;
        String dealerId;
        String cashCollected;
        String returnBit;
        String latitude;
        String longitude;
        String deliveredLocation;
        String date;
        String time;
        String userId;
        String status;
        Boolean returnResponse = false;
        int i = 0;
        Cursor res = objGetData.getPendingDelivered();

        if(res.getCount() == 0)
        {
            return "Orders already Submitted";
        }
        else
        {
            StringBuffer objBuffer = new StringBuffer();
            while(res.moveToNext())
            {
                orderId = res.getString(0);
                dealerId = res.getString(1);
                cashCollected = res.getString(2);
                returnBit = res.getString(3);
                latitude = res.getString(9);
                longitude = res.getString(10);
                deliveredLocation = res.getString(11);
                date = res.getString(12);
                time = res.getString(13);
                userId = res.getString(14);
                status = "Delivered";
                returnResponse = sendSavedDelivered(orderId, dealerId, cashCollected, returnBit, latitude, longitude, deliveredLocation, date, time, userId);
                if(returnResponse)
                {
                    i++;
                }
            }
            if(i >= res.getCount())
            {
                return "All Pending Delivered Submitted";
            }
            else
            {
                return "Incomplete Delivered Submission";
            }
        }
    }

    public String sendAllPendingCash()
    {
        String paymentId;
        String dealerId;
        String orderId;
        String cash;
        String cashType;
        String latitude;
        String longitude;
        String paymentLocation;
        String date;
        String time;
        String userId;
        String status;
        Boolean returnResponse = false;
        int i = 0;
        Cursor res = objGetData.getPendingPayments();

        if(res.getCount() == 0)
        {
            return "Payments already Submitted";
        }
        else
        {
            StringBuffer objBuffer = new StringBuffer();
            while(res.moveToNext())
            {
                paymentId = res.getString(0);
                dealerId = res.getString(1);
                orderId = res.getString(2);
                cash = res.getString(3);
                cashType = res.getString(4);
                latitude = res.getString(5);
                longitude = res.getString(6);
                paymentLocation = res.getString(7);
                date = res.getString(8);
                time = res.getString(9);
                userId = res.getString(10);
                status = "Cash";
                returnResponse = sendSavedCash(paymentId, dealerId, orderId, cash, cashType, latitude, longitude, paymentLocation, date, time, userId);
                if(returnResponse)
                {
                    i++;
                }
            }
            if(i >= res.getCount())
            {
                return "All Pending Payments Submitted";
            }
            else
            {
                return "Incomplete Payments Submission";
            }
        }
    }

    public String sendAllPendingReturn()
    {
        String returnId;
        String orderId;
        String dealerId;
        String productId;
        String quantity;
        String unit;
        String batchNo;
        String returnPrice;
        String totalReturnPrice;
        String returnReason;
        String cashReturn;
        String latitude;
        String longitude;
        String returnLocation;
        String date;
        String time;
        String userId;
        String status;
        Boolean returnResponse = false;
        int i = 0;
        Cursor res = objGetData.getPendingReturns();
        int totalCount = res.getCount();
        if(totalCount == 0)
        {
            return "Returns already Submitted";
        }
        else
        {
            StringBuffer objBuffer = new StringBuffer();
            while(res.moveToNext())
            {
                returnId = res.getString(0);
                orderId = res.getString(1);
                dealerId = res.getString(2);
                productId = res.getString(3);
                quantity = res.getString(4);
                unit = res.getString(5);
                batchNo = res.getString(6);
                returnPrice = res.getString(7);
                totalReturnPrice = res.getString(8);
                returnReason = res.getString(9);
                cashReturn = res.getString(10);
                latitude = res.getString(11);
                longitude = res.getString(12);
                returnLocation = res.getString(13);
                date = res.getString(14);
                time = res.getString(15);
                userId = res.getString(16);
                status = "Return";
                returnResponse = sendSavedReturn(totalCount, returnId, orderId, dealerId, productId, quantity, unit, batchNo, returnPrice, totalReturnPrice, returnReason, cashReturn, latitude, longitude, returnLocation, date, time, userId);
                if(returnResponse)
                {
                    i++;
                }
            }

//            if(i >= totalCount)
//            {
//                return "All Pending Returns Submitted";
//            }
//            else
//            {
//                return "Incomplete Returns Submission";
//            }

            return null;
        }
    }

    public void sendCurrentLocation(final String userId, final String latitude, final String longitude, final String locationName)
    {
        objLiveLocationSend.setCallBack(new LiveLocationSend.CallBack() {
            @Override
            public void onResult(String value) {

            }
        });
        if(!LiveLocationSend.inProgress)
        {
            LiveLocationSend.inProgress = true;
            objLiveLocationSend.execute("currentlocation", userId, latitude, longitude, locationName);
        }

    }

    public void sendCurrentLocationTime(final String userId)
    {
        objLiveLocationSend.setCallBack(new LiveLocationSend.CallBack() {
            @Override
            public void onResult(String value) {

            }
        });
        if(!LiveLocationSend.inProgress)
        {
            LiveLocationSend.inProgress = true;
            objLiveLocationSend.execute("currentlocationtime", userId);
        }

    }

    public void sendTimeStampLocation(final String userId, final String latitude, final String longitude, final String locationName, final String stDate, final String stTime)
    {
        objBackground.setCallBack(new BackgroundTask.CallBack() {
            @Override
            public void onResult(String value) {
                if(!value.equals("Location Sent Successfully"))
                {
                    objDatabaseSqlite.insertCurrentLocation(userId, latitude, longitude, locationName, stDate, stTime);
                }
                else
                {
                    sendPendingTimeStampLocations();
                }
            }
        });
        if(!BackgroundTask.inProgress)
        {
            BackgroundTask.inProgress = true;
            objBackground.execute("timestamplocation", userId, latitude, longitude, locationName, stDate, stTime);
        }
    }

    public void sendPendingTimeStampLocations()
    {
        String mobLocId;
        String userId;
        String latitude;
        String longitude;
        String locationName;
        String stDate;
        String stTime;
        int i = 0;
        Cursor res = objDatabaseSqlite.getPendingLocations();

        if(res.getCount() == 0)
        {

        }
        else
        {
            while(res.moveToNext())
            {
                mobLocId = res.getString(0);
                userId = res.getString(1);
                latitude = res.getString(2);
                longitude = res.getString(3);
                locationName = res.getString(4);
                stDate = res.getString(5);
                stTime = res.getString(6);

                final String finalMobLocId = mobLocId;
//                BackgroundTask objback1 = new BackgroundTask(ctx);
                objBackground2.setCallBack(new BackgroundTask2.CallBack() {
                    @Override
                    public void onResult(String value) {
                        if(value.equals("Location Sent"))
                        {
                            objDatabaseSqlite.deleteGPSLocation(finalMobLocId);
                        }
                    }
                });
                if(!BackgroundTask2.inProgress)
                {
                    BackgroundTask2.inProgress = true;
                    objBackground2.execute("resendtimestamplocation", userId, latitude, longitude, locationName, stDate, stTime);
                }
            }
        }
    }

    public String sendDealerPin(final String dealerId, final String latitude, final String longitude, final String dealer_pin_loc, final String date, final String time, final String userId)
    {
        DealerPinFragment.edProgressDealerPin.setVisibility(View.VISIBLE);
        objMysql.setCallBack(new MysqlDataSend.CallBack() {
            @Override
            public void onResult(String value) {
                if(!value.equals("Location Sent Successfully"))
                {
                    boolean result = objDatabaseSqlite.updateDealerGPSLoc(dealerId, latitude, longitude, dealer_pin_loc, date, time, userId);
                    if(result)
                    {
                        responseStatus += "Location Saved";
                    }
                    else
                    {
                        responseStatus += "Location Not Sent, Please Connect to Internet";
                    }
                }
                else
                {
                    responseStatus = "Location Sent Successfully";
                }

                Toast.makeText(ctx, responseStatus, Toast.LENGTH_LONG).show();
                Intent intent = new Intent(ctx, Home.class);
                ctx.startActivity(intent);
                ((Activity)ctx).finish();
            }
        });
        if(!MysqlDataSend.inProgress)
        {
            MysqlDataSend.inProgress = true;
            objMysql.execute("dealerpinloc", dealerId, latitude, longitude, dealer_pin_loc, date, time, userId);
        }
        return responseStatus;
    }

    public String saveDelivered(final String orderId, final String dealerId, final String cashCollected, final String returnBit, final String latitude, final String longitude, final String delivered_area, final String stDate, final String stTime, final String userId)
    {
        status = "Pending";
        boolean result = objDatabaseSqlite.insertDelivery(orderId, dealerId, cashCollected, returnBit, latitude, longitude, delivered_area, stDate, stTime, userId, status);
        if(result)
        {
            responseStatus += "Delivered Order Saved";
        }
        else
        {
            responseStatus += "Delivered Order Not Saved";
        }
        Toast.makeText(ctx, responseStatus, Toast.LENGTH_LONG).show();
        Intent intent = new Intent(ctx, Home.class);
        ctx.startActivity(intent);
        ((Activity)ctx).finish();
        return responseStatus;
    }

    public String sendDelivered(final String orderId, final String dealerId, final String cashCollected, final String returnBit, final String latitude, final String longitude, final String delivered_area, final String stDate, final String stTime, final String userId)
    {
        objMysql.setCallBack(new MysqlDataSend.CallBack() {
            @Override
            public void onResult(String value) {
                if(value.equals("Delivery Sent Successfully"))
                {
                    status = "Sent";
                    responseStatus = "Delivery Sent ";
                }
                else
                {
                    status = "Pending";
                    responseStatus = "Delivery Not Sent ";
                }
                boolean result = objDatabaseSqlite.insertDelivery(orderId, dealerId, cashCollected, returnBit, latitude, longitude, delivered_area, stDate, stTime, userId, status);
                if(result)
                {
                    responseStatus += " and Saved";
                }
                else
                {
                    responseStatus += " and Not Saved";
                }
                Toast.makeText(ctx, responseStatus, Toast.LENGTH_LONG).show();
                Intent intent = new Intent(ctx, Home.class);
                ctx.startActivity(intent);
                ((Activity)ctx).finish();
            }
        });
        if(!MysqlDataSend.inProgress)
        {
            MysqlDataSend.inProgress = true;
            status = "Delivered";
            objMysql.execute("delivered", orderId, dealerId, cashCollected, String.valueOf(returnBit), latitude, longitude, delivered_area, stDate, stTime, userId, status);
        }
        return responseStatus;
    }

    public boolean sendSavedDelivered(final String orderId, final String dealerId, final String cashCollected, final String returnBit, final String latitude, final String longitude, final String delivered_area, final String stDate, final String stTime, final String userId)
    {
        final boolean[] result = {false};
        objMysql.setCallBack(new MysqlDataSend.CallBack() {
            @Override
            public void onResult(String value) {
                if(value.equals("Delivery Sent Successfully"))
                {
                    status = "Sent";
                    result[0] = objDatabaseSqlite.updateDelivered(orderId, status);
                }
                else
                {
                    result[0] = false;
                }
                Intent intent = new Intent(ctx, Home.class);
                ctx.startActivity(intent);
                ((Activity)ctx).finish();
            }
        });
        if(!MysqlDataSend.inProgress)
        {
            MysqlDataSend.inProgress = true;
            status = "Delivered";
            objMysql.execute("delivered", orderId, dealerId, cashCollected, latitude, longitude, delivered_area, stDate, stTime, userId, status);
        }
        return result[0];
    }

    public String cancelDelivered(final String orderId, final String dealerId, final String cashDifference)
    {
        status = "Pending";
        objDatabaseSqlite.deleteDelivery(orderId);
        objDatabaseSqlite.updateInvoice(orderId, "Invoiced");
//        objDatabaseSqlite.deleteDealerPayment(orderId);
        objDatabaseSqlite.updateDealerPending(dealerId, cashDifference);

        Toast.makeText(ctx, responseStatus, Toast.LENGTH_LONG).show();
        Intent intent = new Intent(ctx, Home.class);
        ctx.startActivity(intent);
        ((Activity)ctx).finish();
        return responseStatus;
    }

    public String updateDelivered(final String orderId, final String dealerId, final String productId, final String quantity, final String unit, final String orderPrice, final String bonus, final String discount, final String finalPrice, final String latitude, final String longitude, final String order_place_area, final String stDate, final String stTime, final String salesmanId)
    {
        status = "Pending";
        boolean result = objDatabaseSqlite.updateBookingData(orderId, dealerId, productId, quantity, unit, orderPrice, bonus, discount, finalPrice, latitude, longitude, order_place_area, stDate, stTime, salesmanId, status);
        if(result)
        {
            responseStatus = "Order Updated";
        }
        else
        {
            responseStatus = "Order Not Updated";
        }
        Toast.makeText(ctx, responseStatus, Toast.LENGTH_LONG).show();
        return responseStatus;
    }

    public void saveCash(final String dealerId, final String orderId, final String cashCollected, final String cashType, final String latitude, final String longitude, final String delivered_area, final String stDate, final String stTime, final String userId)
    {
        status = "Pending";
        boolean result = objDatabaseSqlite.insertCashCollection(dealerId, orderId, cashCollected, cashType, latitude, longitude, delivered_area, stDate, stTime, userId, status);
        if(result)
        {
            responseStatus += " Cash Collection Saved";
        }
        else
        {
            responseStatus += " Cash Collection Not Saved";
        }
        Toast.makeText(ctx, responseStatus, Toast.LENGTH_LONG).show();
        Intent intent = new Intent(ctx, Home.class);
        ctx.startActivity(intent);
        ((Activity)ctx).finish();
    }

    public String sendCash(final String dealerId, final String orderId, final String cashCollected, final String cashType, final String latitude, final String longitude, final String delivered_area, final String stDate, final String stTime, final String userId)
    {
//        InvoicedOrderDetail.edProgress.setVisibility(View.VISIBLE);
        objMysql.setCallBack(new MysqlDataSend.CallBack() {
            @Override
            public void onResult(String value) {
                if(value.equals("Collection Sent Successfully"))
                {
                    status = "Sent";
                    responseStatus = "Cash Collection Sent";
                }
                else
                {
                    status = "Pending";
                    responseStatus = "Cash Collection Not Sent ";
                }
                boolean result = objDatabaseSqlite.insertCashCollection(dealerId, orderId, cashCollected, cashType, latitude, longitude, delivered_area, stDate, stTime, userId, status);
                if(result)
                {
                    responseStatus += " and Saved";
                }
                else
                {
                    responseStatus += " and Not Saved";
                }
                Toast.makeText(ctx, responseStatus, Toast.LENGTH_LONG).show();
                Intent intent = new Intent(ctx, Home.class);
                ctx.startActivity(intent);
                ((Activity)ctx).finish();
            }
        });
        if(!MysqlDataSend.inProgress)
        {
            MysqlDataSend.inProgress = true;
            status = "Cash";
            objMysql.execute("cash_collected", dealerId, cashCollected, cashType, latitude, longitude, delivered_area, stDate, stTime, userId, status);
        }
        return responseStatus;
    }

    public boolean sendSavedCash(final String paymentId, final String dealerId, final String orderId, final String cashCollected, final String cashType, final String latitude, final String longitude, final String payment_area, final String stDate, final String stTime, final String userId)
    {
        final boolean[] result = {false};
        objMysql.setCallBack(new MysqlDataSend.CallBack() {
            @Override
            public void onResult(String value) {
                if(value.equals("Collection Sent Successfully"))
                {
                    status = "Sent";
                    result[0] = objDatabaseSqlite.updateCashStatus(paymentId, status);
                }
                else
                {
                    result[0] = false;
                }
                Intent intent = new Intent(ctx, Home.class);
                ctx.startActivity(intent);
                ((Activity)ctx).finish();
            }
        });
        if(!MysqlDataSend.inProgress)
        {
            MysqlDataSend.inProgress = true;
            status = "Cash";
            objMysql.execute("cash_collected", dealerId, cashCollected, cashType, latitude, longitude, payment_area, stDate, stTime, userId, status);
        }
        return result[0];
    }

    public void updateCash(final String paymentId, final String dealerId, final String preCashCollectedNew, final String newCashCollected)
    {
        status = "Pending";
        objDatabaseSqlite.updateCashCollection(paymentId, dealerId, preCashCollectedNew, newCashCollected);
        responseStatus = "Cash Collection Updated";
        Toast.makeText(ctx, responseStatus, Toast.LENGTH_LONG).show();
    }

    public String updateUserDetails(final String userId, final String stName, final String stPass, final String autoSend)
    {
        objMysql.setCallBack(new MysqlDataSend.CallBack() {
            @Override
            public void onResult(String value) {
                if(value.equals("User Updated Successfully"))
                {
                    responseStatus = "User Updated ";
                }
                else
                {
                    responseStatus = "User Not Updated ";
                }
                boolean result = objDatabaseSqlite.updateUser(userId, "user_detail", stName, stPass, autoSend);
                if(result)
                {
                    GlobalVariables.autoSend = autoSend;
                    responseStatus += " and Saved";
                }
                else
                {
                    responseStatus += " and Not Saved";
                }
                Toast.makeText(ctx, responseStatus, Toast.LENGTH_LONG).show();
                Intent intent = new Intent(ctx, Home.class);
                ctx.startActivity(intent);
                ((Activity)ctx).finish();
            }
        });
        if(!MysqlDataSend.inProgress)
        {
            MysqlDataSend.inProgress = true;
            objMysql.execute("UserDetails", userId, stName, stPass);
        }
        return responseStatus;
    }

    public void saveReturn(final String orderId, final String dealerId, final String productId, final String quantity, final String unit, final String batchNo, final String returnPrice, final String totalReturnPrice, final String returnReason, final String cashReturn, final String latitude, final String longitude, final String return_area, final String stDate, final String stTime, final String userId)
    {
        status = "Pending";
        boolean result = objDatabaseSqlite.insertReturn(orderId, dealerId, productId, quantity, unit, batchNo, returnPrice, totalReturnPrice, returnReason, cashReturn, latitude, longitude, return_area, stDate, stTime, userId, status);
        if(result)
        {
            responseStatus += "Return Saved";
        }
        else
        {
            responseStatus += "Return Not Saved";
        }
        Toast.makeText(ctx, responseStatus, Toast.LENGTH_LONG).show();
        Intent intent = new Intent(ctx, Home.class);
        ctx.startActivity(intent);
        ((Activity)ctx).finish();
    }

    public void updateReturn(final String returnId, final String orderId, final String dealerId, final String productId, final String quantity, final String unit, final String batchNo, final String returnPrice, final String totalReturnPrice, final String returnReason, final String cashReturn, final String latitude, final String longitude, final String return_area, final String stDate, final String stTime, final String userId)
    {
        status = "Pending";
        boolean result = objDatabaseSqlite.updateReturn(returnId, orderId, dealerId, productId, quantity, unit, batchNo, returnPrice, totalReturnPrice, returnReason, cashReturn, latitude, longitude, return_area, stDate, stTime, userId, status);
        if(result)
        {
            responseStatus += "Return Saved";
        }
        else
        {
            responseStatus += "Return Not Saved";
        }
        Toast.makeText(ctx, responseStatus, Toast.LENGTH_LONG).show();
        Intent intent = new Intent(ctx, Home.class);
        ctx.startActivity(intent);
        ((Activity)ctx).finish();
    }

    public String sendReturn(final String orderId, final String dealerId, final String productId, final String quantity, final String unit, final String batchNo, final String returnPrice, final String totalReturnPrice, final String returnReason, final String cashReturn, final String latitude, final String longitude, final String return_area, final String stDate, final String stTime, final String userId)
    {
        objMysql.setCallBack(new MysqlDataSend.CallBack() {
            @Override
            public void onResult(String value) {
                if(value.equals("Return Sent"))
                {
                    responseStatus = "Return Sent ";
                    status = "Sent";
                }
                else
                {
                    responseStatus = "Return Not Sent ";
                    status = "Pending";
                }
                boolean result = objDatabaseSqlite.insertReturn(orderId, dealerId, productId, quantity, unit, batchNo, returnPrice, totalReturnPrice, returnReason, cashReturn, latitude, longitude, return_area, stDate, stTime, userId, status);
                if(result)
                {
                    responseStatus += "and Saved";
                }
                else
                {
                    responseStatus += "and Not Saved";
                }
                Toast.makeText(ctx, responseStatus, Toast.LENGTH_LONG).show();
                Intent intent = new Intent(ctx, Home.class);
                ctx.startActivity(intent);
                ((Activity)ctx).finish();
            }
        });
        if(!MysqlDataSend.inProgress)
        {
            MysqlDataSend.inProgress = true;
            status = "Return";
            objMysql.execute("Return", orderId, dealerId, productId, quantity, unit, batchNo, returnPrice, totalReturnPrice, returnReason, cashReturn, latitude, longitude, return_area, stDate, stTime, userId, status);
        }
        return responseStatus;
    }

    public boolean sendSavedReturn(final int count, final String returnId, final String orderId, final String dealerId, final String productId, final String quantity, final String unit, final String batchNo, final String returnPrice, final String totalReturnPrice, final String returnReason, final String cashReturn, final String latitude, final String longitude, final String returnLocation, final String stDate, final String stTime, final String userId)
    {
        final boolean[] result = {false};
        objMysql.setCallBack(new MysqlDataSend.CallBack() {
            @Override
            public void onResult(String value) {
                if(value.equals("Return Sent"))
                {
                    if(count == 1)
                    {
                        Toast.makeText(ctx, "Return Sent", Toast.LENGTH_LONG).show();
                    }
                    status = "Sent";
                    result[0] = objDatabaseSqlite.updateReturnStatus(returnId, status);
                }
                else
                {
                    result[0] = false;
                }
                Intent intent = new Intent(ctx, Home.class);
                ctx.startActivity(intent);
                ((Activity)ctx).finish();
            }
        });
        if(!MysqlDataSend.inProgress)
        {
            MysqlDataSend.inProgress = true;
            status = "Return";
            objMysql.execute("Return", orderId, dealerId, productId, quantity, unit, batchNo, returnPrice, totalReturnPrice, returnReason, cashReturn, latitude, longitude, returnLocation, stDate, stTime, userId, status);
        }
        return result[0];
    }
}
