package com.example.younastraders;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements LocationListener {

    int productsCount;
    int maxExtraProducts = 50;
    float totalOrderPrice = 0;
    String bonus = "0";
    String discountSingle = "0";
    String strBonus = "";
    float totalDiscount = 0;
    float totalFinalPrice = 0;
    Button btnSubmitOrder;
    Button btnSaveOrder;
    SearchableSpinner edDealerName;
    SearchableSpinner edProductName;
    EditText edQuantity;
    EditText edBonus;
    EditText edDiscount;

    SearchableSpinner[] orgProducts;
    EditText[] orgqQuantity;
    Spinner[] orgUnit;

    LinearLayout[] subLayout;
    LinearLayout[] subLayoutDetail;
    ArrayList<LinearLayout> subLayoutTable;
    public static ProgressBar edProgress;
    SendData objSendData;
    Button btnAdd;
    EditText edOrderPrice;
    EditText edFinalPrice;

    String strOrderPrice;
    String strDiscount;
    String strFinalPrice;

    ArrayAdapter<String> dealerAdapter;
    ArrayAdapter<String> productAdapter;
    ArrayAdapter<String> unitAdapter;
    LinearLayout lv;

    DatabaseSQLite objDatabaseSqlite;

    public String[] dealersId;
    public String[] dealersName;
    public String[] dealersAddress;
    public String[] productsIds;
    public String[] productsName;
    public float[] productsPrice;
    public ArrayList<ArrayList<String>> bonusDetail;
    public ArrayList<ArrayList<String>> discountDetail;
    public ArrayList<ArrayList<String>> productsCompaniesDetail;

    String stDate;
    String stTime;

    String send_dealerId;
    String send_productIds = "";
    String send_quantities = "";
    String send_discount = "";
    String send_unit = "";
    String send_salesmanId = "1";

    String[] strPaymentId;
    ArrayList<TextView> txtSrNo;
    ArrayList<TextView> txtDealerName;
    ArrayList<TextView> txtCash;
    ArrayList<Button> opBtn;
    ArrayList<View> viewLine;
    boolean setColor = false;

    ArrayList<String> selectedProductId = new ArrayList<>();
    ArrayList<String> selectedProductName = new ArrayList<>();
    ArrayList<String> selectedQty = new ArrayList<>();
    ArrayList<String> selectedPrice = new ArrayList<>();
    ArrayList<String> selectedDiscount = new ArrayList<>();
    ArrayList<String> selectedBonus = new ArrayList<>();
    ArrayList<String> selectedFinalPrice = new ArrayList<>();


    double lat;
    double lon;
    String orderPlaceArea;
    protected LocationManager locationManager;

    @SuppressLint("MissingPermission")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lat = 0;
        lon = 0;
        orderPlaceArea = "";
        if(!checkLocationPermission())
        {
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }
        if(checkLocationPermission())
        {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        }

        Date objDate = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        this.stDate = df.format(objDate);
        this.stTime = sdf.format(objDate);
        DealersAndProductsInfo objdealandproinfo = new DealersAndProductsInfo(this);
        dealersId = objdealandproinfo.getDealersId();
        dealersName = objdealandproinfo.getDealersName();
        dealersAddress = objdealandproinfo.getDealersAddress();
        productsIds = objdealandproinfo.getProductsIds();
        productsName = objdealandproinfo.getProductsName();
        productsPrice = objdealandproinfo.getProductsPrice();
        orgProducts = new SearchableSpinner[1];
        orgqQuantity = new EditText[1];
        orgUnit = new Spinner[1];
        subLayoutTable = new ArrayList<>();
        txtSrNo = new ArrayList<>();
        txtDealerName = new ArrayList<>();
        txtCash = new ArrayList<>();
        opBtn = new ArrayList<>();
        viewLine = new ArrayList<>();
        edOrderPrice = findViewById(R.id.edorderprice);
        edDiscount = findViewById(R.id.eddiscount);
        edFinalPrice = findViewById(R.id.edfinalprice);

        edOrderPrice.setText(String.valueOf(totalOrderPrice));
        edDiscount.setText(String.valueOf(totalDiscount));
        edFinalPrice.setText(String.valueOf(totalFinalPrice));

        subLayout = new LinearLayout[maxExtraProducts];
        subLayoutDetail = new LinearLayout[maxExtraProducts];

        objSendData = new SendData(this);
        productsCount = 0;
        btnAdd = findViewById(R.id.button_add);
        edDealerName = findViewById(R.id.ed_dealerName);
        edProductName = findViewById(R.id.ed_productName);
        edQuantity = findViewById(R.id.ed_quantity);
        edDiscount = findViewById(R.id.ed_discount);
        edBonus = findViewById(R.id.ed_bonus);
        lv = findViewById(R.id.linear_layout);

        dealerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, dealersName);
        edDealerName.setAdapter(dealerAdapter);

        productAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, productsName);
        edProductName.setAdapter(productAdapter);

        String[] unitItems = new String[]{"Packets", "Box"};
        unitAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, unitItems);
        objDatabaseSqlite = new DatabaseSQLite(this);
        btnSubmitOrder = findViewById(R.id.btn_send);
        btnSaveOrder = findViewById(R.id.btn_save);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                lv.addView(createLinearLayout());
                String productName = edProductName.getSelectedItem().toString();
                String qty = edQuantity.getText().toString();
                String bonus = edBonus.getText().toString();
                String totalQty = qty+(bonus.equals("") || bonus.equals("0") ? "" : "+"+bonus);
                selectedProductId.add(productsIds[edProductName.getSelectedItemPosition()]);
                selectedProductName.add(productName);
                qty = qty.equals("") ? "0" : qty;
                bonus = bonus.equals("") ? "0" : bonus;
                selectedQty.add(qty);
                selectedBonus.add(bonus);
                float productPrice = productsPrice[edProductName.getSelectedItemPosition()] * Integer.parseInt(qty);
                String discount = getDiscount(dealersId[edDealerName.getSelectedItemPosition()], productsIds[edProductName.getSelectedItemPosition()], String.valueOf(productsPrice[edProductName.getSelectedItemPosition()] * Integer.parseInt(qty)));
                selectedPrice.add(String.valueOf(productPrice));
                selectedDiscount.add(discount);
                selectedFinalPrice.add(String.valueOf(productPrice - Float.parseFloat(discount)));
                totalOrderPrice += productPrice;
                totalDiscount += Float.parseFloat(discount);
                totalFinalPrice += productPrice - Float.parseFloat(discount);
                lv.addView(createLinearLayout(String.valueOf(productsCount+1), productName, totalQty));
                edDiscount.setText(String.format("%.2f", totalDiscount));
                totalFinalPrice = totalOrderPrice - totalDiscount;
                edOrderPrice.setText(String.valueOf(totalOrderPrice));
                edFinalPrice.setText(String.valueOf(totalFinalPrice));
                productsCount++;
                addListenerToButtons();
                edProductName.setSelection(1);
                edQuantity.setText("");
                edBonus.setText("");
                edDiscount.setText("");
            }
        });

        bonusDetail = initializeBonusInfo();
        discountDetail = initializeDiscountInfo();
        productsCompaniesDetail = initializeProductsCompaniesInfo();

        btnSaveOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                compileOrder();
                objSendData.saveBooking(send_dealerId, send_productIds, send_quantities, send_unit, strOrderPrice, bonus, send_discount, strFinalPrice, String.valueOf(lat), String.valueOf(lon), orderPlaceArea, stDate, stTime, send_salesmanId);
            }
        });


        edProgress = findViewById(R.id.ed_progress);
        btnSubmitOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                compileOrder();
                objSendData.sendBooking(send_dealerId, send_productIds, send_quantities, send_unit, strOrderPrice, bonus, send_discount, strFinalPrice, String.valueOf(lat), String.valueOf(lon), orderPlaceArea, stDate, stTime, send_salesmanId);
            }
        });
    }

    public void compileOrder()
    {
        float orderPrice = 0;
        float finalPrice = 0;
        MainActivity.edProgress.setVisibility(View.VISIBLE);
        boolean check = false;
        if(edDealerName.getSelectedItemPosition() > 0)
        {
            send_dealerId = dealersId[edDealerName.getSelectedItemPosition()];
        }
        else
        {
            return;
        }
        for(int j=0; j<productsCount; j++)
        {
            if(check)
            {
                send_productIds = send_productIds+"_-_"+selectedProductId.get(j);
                send_quantities = send_quantities+"_-_"+selectedQty.get(j);
                bonus = bonus+"_-_"+selectedBonus.get(j); // Problem as Discount structure in mysql is changed
                send_discount = send_discount+"_-_"+selectedDiscount.get(j); // Problem as Discount structure in mysql is changed
                send_unit = send_unit+"_-_Packets";
            }
            else
            {
                send_productIds = selectedProductId.get(j);
                send_quantities = selectedQty.get(j);
                bonus = selectedBonus.get(j);
                send_discount = selectedDiscount.get(j);
                send_unit = "Packets";
                check = true;
            }
        }
        SyncService.tenCounts = 1;
        Intent intent = new Intent(this, SyncService.class);
        startService(intent);
    }


    public TextView createSrNoText(String text) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(Math.round(getResources().getDimension(R.dimen.invoice_srno)), ViewGroup.LayoutParams.WRAP_CONTENT);
        txtSrNo.add(new TextView(MainActivity.this));
        txtSrNo.get(productsCount).setLayoutParams(lparams);
//        txtSrNo.get(productsCount).setBackgroundColor(Color.RED);
        txtSrNo.get(productsCount).setText(text);
        txtSrNo.get(productsCount).setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        return txtSrNo.get(productsCount);
    }

    public TextView createProductText(String text) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(Math.round(getResources().getDimension(R.dimen._200dp)), ViewGroup.LayoutParams.WRAP_CONTENT);
        txtDealerName.add(new TextView(MainActivity.this));
        txtDealerName.get(productsCount).setLayoutParams(lparams);
//        txtDealerName.get(productsCount).setBackgroundColor(Color.RED);
        txtDealerName.get(productsCount).setText(text);
        return txtDealerName.get(productsCount);
    }

    public TextView createQtyText(String text) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(Math.round(getResources().getDimension(R.dimen._60dp)), ViewGroup.LayoutParams.WRAP_CONTENT);
        txtCash.add(new TextView(MainActivity.this));
        txtCash.get(productsCount).setLayoutParams(lparams);
//        txtCash.get(productsCount).setBackgroundColor(Color.GREEN);
        txtCash.get(productsCount).setText(text);
        return txtCash.get(productsCount);
    }

    public Button createOpButton() {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        opBtn.add(new Button(MainActivity.this));
        opBtn.get(productsCount).setLayoutParams(lparams);
        opBtn.get(productsCount).setText("X");
        opBtn.get(productsCount).setTextColor(Color.RED);
        opBtn.get(productsCount).setBackgroundColor(Color.TRANSPARENT);
        return opBtn.get(productsCount);
    }

    public View createLineView() {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,  Math.round(getResources().getDimension(R.dimen.line)));
        viewLine.add(new View(MainActivity.this));
        viewLine.get(productsCount).setLayoutParams(lparams);
        viewLine.get(productsCount).setBackgroundColor(Color.BLACK);
        return viewLine.get(productsCount);
    }

    public LinearLayout createLinearLayout(String srNo, String dealerName, String cash) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        subLayoutTable.add(new LinearLayout(MainActivity.this));
        subLayoutTable.get(productsCount).setOrientation(LinearLayout.HORIZONTAL);
        subLayoutTable.get(productsCount).setLayoutParams(lparams);
        subLayoutTable.get(productsCount).addView(createSrNoText(srNo));
        subLayoutTable.get(productsCount).addView(createProductText(dealerName));
        subLayoutTable.get(productsCount).addView(createQtyText(cash));
        subLayoutTable.get(productsCount).addView(createOpButton());
        if(setColor)
        {
            subLayoutTable.get(productsCount).setBackgroundColor(Color.parseColor("#E1E1E1"));
            setColor = false;
        }
        else
        {
            setColor = true;
        }
        return subLayoutTable.get(productsCount);
    }

    public void addListenerToButtons()
    {
        for(int i=0; i<productsCount; i++)
        {
            final int finalI = i;
            opBtn.get(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    subLayoutTable.get(finalI).setVisibility(View.GONE);
                    totalOrderPrice -= Float.parseFloat(selectedPrice.get(finalI));
                    totalDiscount -= Float.parseFloat(selectedDiscount.get(finalI));
                    totalFinalPrice -= Float.parseFloat(selectedFinalPrice.get(finalI));
                    selectedPrice.remove(finalI);
                    selectedDiscount.remove(finalI);
                    selectedFinalPrice.remove(finalI);
                    txtSrNo.remove(finalI);
                    txtDealerName.remove(finalI);
                    txtCash.remove(finalI);
                    opBtn.remove(finalI);
                    subLayoutTable.remove(finalI);
                    edDiscount.setText(String.format("%.2f", totalDiscount));
                    totalFinalPrice = totalOrderPrice - totalDiscount;
                    edOrderPrice.setText(String.valueOf(totalOrderPrice));
                    edFinalPrice.setText(String.valueOf(totalFinalPrice));
                    productsCount--;
                    reviseSrNo();
                    // I AM HERE, UPON DELETION SETTING UP OF A TOTAL PRICE, TOTAL DISCOUNT AND TOTAL FINAL PRICE AGAIN.

                }
            });

//            edProductName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                @Override
//                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                    calculatePrice();
//                }
//
//                @Override
//                public void onNothingSelected(AdapterView<?> parent) {
//
//                }
//            });
//
//            edQuantity.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//                @Override
//                public void onFocusChange(View view, boolean hasFocus) {
//                    if (!hasFocus) {
//                        if(edQuantity.getText().toString().equals(""))
//                        {
//                            edQuantity.setText("1");
//                        }
//                        calculatePrice();
//                    }
//                }
//            });
//
//            edQuantity.addTextChangedListener(new TextWatcher() {
//                @Override
//                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//                    calculatePrice();
//                }
//
//                @Override
//                public void onTextChanged(CharSequence s, int start, int before, int count) {
//                    calculatePrice();
//                }
//
//                @Override
//                public void afterTextChanged(Editable s) {
//                    calculatePrice();
//                }
//            });
        }
        calculatePrice();
    }

    private void reviseSrNo()
    {
        for(int i=0; i<productsCount; i++)
        {
            txtSrNo.get(i).setText(String.valueOf(i+1));
        }
    }

    public void calculatePrice()
    {
        strOrderPrice = "";
        bonus = "0";
        discountSingle = "0";
        strDiscount = "";
        strFinalPrice = "";
        boolean iter = false;
        String stQan = "0";
        for(int j=0; j<productsCount; j++)
        {
            if(edProductName.getSelectedItemPosition() > 0)
            {
                stQan = edQuantity.getText().toString();
                bonus = edBonus.getText().toString();
                discountSingle = edDiscount.getText().toString();
                if(stQan.equals(""))
                {
                    stQan = "0";
                }
                if(iter)
                {
                    strOrderPrice = strOrderPrice+"_-_"+String.valueOf(productsPrice[edProductName.getSelectedItemPosition()] * Integer.parseInt(stQan));
                    strBonus = strBonus+"_-_"+bonus;
                    strDiscount = strDiscount+"_-_"+discountSingle;
                    strFinalPrice = strFinalPrice+"_-_"+String.valueOf((productsPrice[edProductName.getSelectedItemPosition()] * Integer.parseInt(stQan)) - Float.parseFloat(discountSingle));
                }
                else
                {
                    iter = true;
                    strOrderPrice = String.valueOf(productsPrice[edProductName.getSelectedItemPosition()] * Integer.parseInt(stQan));
                    strBonus = bonus;
                    strDiscount = discountSingle;
                    strFinalPrice = String.valueOf((productsPrice[edProductName.getSelectedItemPosition()] * Integer.parseInt(stQan)) - Float.parseFloat(discountSingle));
                }
                totalOrderPrice = totalOrderPrice + (productsPrice[edProductName.getSelectedItemPosition()] * Integer.parseInt(stQan));
            }

            if(edDealerName.getSelectedItemPosition() > 0 && edProductName.getSelectedItemPosition() > 0)
            {
                String bonus = getBonus(dealersId[edDealerName.getSelectedItemPosition()], productsIds[edProductName.getSelectedItemPosition()], edQuantity.getText().toString());
                edBonus.setText(bonus);
                String discount = getDiscount(dealersId[edDealerName.getSelectedItemPosition()], productsIds[edProductName.getSelectedItemPosition()], String.valueOf(productsPrice[edProductName.getSelectedItemPosition()] * Integer.parseInt(stQan)));
                float discountPrice = (productsPrice[edProductName.getSelectedItemPosition()] * Integer.parseInt(stQan))/100 * Float.parseFloat(discount);
                totalDiscount += discountPrice;
                edDiscount.setText(String.format("%.2f", totalDiscount));
            }
            else
            {
                edBonus.setText("0");
                edDiscount.setText("0");
            }
        }

        edDiscount.setText(String.format("%.2f", totalDiscount));
        totalFinalPrice = totalOrderPrice - totalDiscount;
        edOrderPrice.setText(String.valueOf(totalOrderPrice));
        edFinalPrice.setText(String.valueOf(totalFinalPrice));
    }

    public void viewAll()
    {
        // on Button Click
        Cursor res = objDatabaseSqlite.getAllData();

        if(res.getCount() == 0)
        {
            showMessage("Error", "Nothing Found");
            return;
        }
        else
        {
            StringBuffer objBuffer = new StringBuffer();
            int i = 1;
            while(res.moveToNext())
            {
                objBuffer.append("id: "+res.getString(0)+"\n");
                objBuffer.append("dealer_id: "+res.getString(1)+"\n");
                objBuffer.append("product_id: "+res.getString(2)+"\n");
                objBuffer.append("quantity: "+res.getString(3)+"\n");
                objBuffer.append("status: "+res.getString(4)+"\n");
//                i++;
            }
            showMessage("Data", objBuffer.toString());
        }
    }

    private ArrayList<ArrayList<String>> initializeBonusInfo()
    {
        ArrayList<ArrayList<String>> bonusData = new ArrayList<>();
        Cursor res = objDatabaseSqlite.getCurrentBonusInfo();
        int num = res.getCount();

        if(num > 0)
        {
            res.moveToNext();
            ArrayList<String> temp;
            do {
                temp = new ArrayList<>();
                temp.add(res.getString(0));
                temp.add(res.getString(1));
                temp.add(res.getString(2));
                temp.add(res.getString(3));
                temp.add(res.getString(4));
                bonusData.add(temp);
            }while (res.moveToNext());
        }

        return bonusData;
    }

    private ArrayList<ArrayList<String>> initializeDiscountInfo()
    {
        ArrayList<ArrayList<String>> discountData = new ArrayList<>();
        Cursor res = objDatabaseSqlite.getCurrentDiscountInfo();
        int num = res.getCount();

        if(num > 0)
        {
            res.moveToNext();
            ArrayList<String> temp;
            do {
                temp = new ArrayList<>();
                temp.add(res.getString(0));
                temp.add(res.getString(1));
                temp.add(res.getString(2));
                temp.add(res.getString(3));
                temp.add(res.getString(4));
                discountData.add(temp);
            }while (res.moveToNext());
        }

        return discountData;
    }

    private ArrayList<ArrayList<String>> initializeProductsCompaniesInfo()
    {
        ArrayList<ArrayList<String>> productsCompaniesData = new ArrayList<>();
        Cursor res = objDatabaseSqlite.getProductsCompaniesInfo();
        int num = res.getCount();

        if(num > 0)
        {
            res.moveToNext();
            ArrayList<String> temp;
            do {
                temp = new ArrayList<>();
                temp.add(res.getString(0));
                temp.add(res.getString(1));
                productsCompaniesData.add(temp);
            }while (res.moveToNext());
        }

        return productsCompaniesData;
    }

    private String getBonus(String dealerId, String productId, String quantity)
    {
        String bonus = "0";
        if(quantity.equals(""))
        {
            quantity = "0";
        }

        for(int i=0; i<bonusDetail.size(); i++)
        {
            if(dealerId.equals(bonusDetail.get(i).get(0)) && (bonusDetail.get(i).get(1).equals("0") || bonusDetail.get(i).get(1).equals("null")) && (bonusDetail.get(i).get(2).equals("0") || bonusDetail.get(i).get(2).equals("null")))
            {
                if(bonusDetail.get(i).get(3).equals("0"))
                {
                    return bonusDetail.get(i).get(4);
                }
                else 
                {
                    int num = Integer.valueOf(quantity)/Integer.valueOf(bonusDetail.get(i).get(3));
                    num = num*Integer.valueOf(bonusDetail.get(i).get(4));
                    return String.valueOf(num);
                }
            }
            else if(dealerId.equals(bonusDetail.get(i).get(0)) && (bonusDetail.get(i).get(1).equals("0") || bonusDetail.get(i).get(1).equals("null")) && productId.equals(bonusDetail.get(i).get(2)))
            {
                if(bonusDetail.get(i).get(3).equals("0"))
                {
                    return bonusDetail.get(i).get(4);
                }
                else 
                {
                    int num = Integer.valueOf(quantity)/Integer.valueOf(bonusDetail.get(i).get(3));
                    num = num*Integer.valueOf(bonusDetail.get(i).get(4));
                    return String.valueOf(num);
                }
            }
            else if((bonusDetail.get(i).get(0).equals("0") || bonusDetail.get(i).get(0).equals("null")) && (bonusDetail.get(i).get(1).equals("0") || bonusDetail.get(i).get(1).equals("null")) && productId.equals(bonusDetail.get(i).get(2)))
            {
                if(bonusDetail.get(i).get(3).equals("0"))
                {
                    return bonusDetail.get(i).get(4);
                }
                else 
                {
                    int num = Integer.valueOf(quantity)/Integer.valueOf(bonusDetail.get(i).get(3));
                    num = num*Integer.valueOf(bonusDetail.get(i).get(4));
                    return String.valueOf(num);
                }
            }
            else if((bonusDetail.get(i).get(0).equals("0") || bonusDetail.get(i).get(0).equals("null")) && !(bonusDetail.get(i).get(1).equals("0") || bonusDetail.get(i).get(1).equals("null")) && (bonusDetail.get(i).get(2).equals("0") || bonusDetail.get(i).get(2).equals("null")))
            {
                for(int j=0; j<productsCompaniesDetail.size(); j++)
                {
                    if(productsCompaniesDetail.get(j).get(1).equals(bonusDetail.get(i).get(1)))
                    {
                        if(bonusDetail.get(i).get(3).equals("0"))
                        {
                            return bonusDetail.get(i).get(4);
                        }
                        else 
                        {
                            int num = Integer.valueOf(quantity)/Integer.valueOf(bonusDetail.get(i).get(3));
                            num = num*Integer.valueOf(bonusDetail.get(i).get(4));
                            return String.valueOf(num);
                        }
                    }
                }
            }
            else if(dealerId.equals(bonusDetail.get(i).get(0)) && !(bonusDetail.get(i).get(1).equals("0") || bonusDetail.get(i).get(1).equals("null")) && (bonusDetail.get(i).get(2).equals("0") || bonusDetail.get(i).get(2).equals("null")))
            {
                for(int j=0; j<productsCompaniesDetail.size(); j++)
                {
                    if(productsCompaniesDetail.get(j).get(1).equals(bonusDetail.get(i).get(1)))
                    {
                        if(bonusDetail.get(i).get(3).equals("0"))
                        {
                            return bonusDetail.get(i).get(4);
                        }
                        else 
                        {
                            int num = Integer.valueOf(quantity)/Integer.valueOf(bonusDetail.get(i).get(3));
                            num = num*Integer.valueOf(bonusDetail.get(i).get(4));
                            return String.valueOf(num);
                        }
                    }
                }
            }
            else if((bonusDetail.get(i).get(0).equals("0") || bonusDetail.get(i).get(0).equals("null")) && !(bonusDetail.get(i).get(1).equals("0") || bonusDetail.get(i).get(1).equals("null")) && productId.equals(bonusDetail.get(i).get(2)))
            {
                for(int j=0; j<productsCompaniesDetail.size(); j++)
                {
                    if(productsCompaniesDetail.get(j).get(1).equals(bonusDetail.get(i).get(1)))
                    {
                        if(bonusDetail.get(i).get(3).equals("0"))
                        {
                            return bonusDetail.get(i).get(4);
                        }
                        else 
                        {
                            int num = Integer.valueOf(quantity)/Integer.valueOf(bonusDetail.get(i).get(3));
                            num = num*Integer.valueOf(bonusDetail.get(i).get(4));
                            return String.valueOf(num);
                        }
                    }
                }
            }
            else if((bonusDetail.get(i).get(0).equals("0") || bonusDetail.get(i).get(0).equals("null")) && (bonusDetail.get(i).get(1).equals("0") || bonusDetail.get(i).get(1).equals("null")) && (bonusDetail.get(i).get(2).equals("0") || bonusDetail.get(i).get(2).equals("null")))
            {
                if(bonusDetail.get(i).get(3).equals("0"))
                {
                    return bonusDetail.get(i).get(4);
                }
                else 
                {
                    int num = Integer.valueOf(quantity)/Integer.valueOf(bonusDetail.get(i).get(3));
                    num = num*Integer.valueOf(bonusDetail.get(i).get(4));
                    return String.valueOf(num);
                }
            }
        }
        return bonus;
    }





    private String getDiscount(String dealerId, String productId, String saleAmount)
    {
        String discount = "0";
        if(saleAmount.equals(""))
        {
            saleAmount = "0";
        }

        for(int i=0; i<discountDetail.size(); i++)
        {
            if(dealerId.equals(discountDetail.get(i).get(0)) && (discountDetail.get(i).get(1).equals("0") || discountDetail.get(i).get(1).equals("null")) && (discountDetail.get(i).get(2).equals("0") || discountDetail.get(i).get(2).equals("null")))
            {
                if(Float.parseFloat(discountDetail.get(i).get(3)) == 0)
                {
                    return discountDetail.get(i).get(4);
                }
                else
                {
                    float num = Float.valueOf(saleAmount)/Float.valueOf(discountDetail.get(i).get(3));
                    num = num*Float.valueOf(discountDetail.get(i).get(4));
                    return String.valueOf(num);
                }
            }
            else if(dealerId.equals(discountDetail.get(i).get(0)) && (discountDetail.get(i).get(1).equals("0") || discountDetail.get(i).get(1).equals("null")) && productId.equals(discountDetail.get(i).get(2)))
            {
                if(Float.parseFloat(discountDetail.get(i).get(3)) == 0)
                {
                    return discountDetail.get(i).get(4);
                }
                else
                {
                    float num = Float.valueOf(saleAmount)/Float.valueOf(discountDetail.get(i).get(3));
                    num = num*Float.valueOf(discountDetail.get(i).get(4));
                    return String.valueOf(num);
                }
            }
            else if((discountDetail.get(i).get(0).equals("0") || discountDetail.get(i).get(0).equals("null")) && (discountDetail.get(i).get(1).equals("0") || discountDetail.get(i).get(1).equals("null")) && productId.equals(discountDetail.get(i).get(2)))
            {
                if(Float.parseFloat(discountDetail.get(i).get(3)) == 0)
                {
                    return discountDetail.get(i).get(4);
                }
                else
                {
                    float num = Float.valueOf(saleAmount)/Float.valueOf(discountDetail.get(i).get(3));
                    num = num*Float.valueOf(discountDetail.get(i).get(4));
                    return String.valueOf(num);
                }
            }
            else if((discountDetail.get(i).get(0).equals("0") || discountDetail.get(i).get(0).equals("null")) && !(discountDetail.get(i).get(1).equals("0") || discountDetail.get(i).get(1).equals("null")) && (discountDetail.get(i).get(2).equals("0") || discountDetail.get(i).get(2).equals("null")))
            {
                for(int j=0; j<productsCompaniesDetail.size(); j++)
                {
                    if(productsCompaniesDetail.get(j).get(1).equals(discountDetail.get(i).get(1)))
                    {
                        if(Float.parseFloat(discountDetail.get(i).get(3)) == 0)
                        {
                            return discountDetail.get(i).get(4);
                        }
                        else
                        {
                            float num = Float.valueOf(saleAmount)/Float.valueOf(discountDetail.get(i).get(3));
                            num = num*Float.valueOf(discountDetail.get(i).get(4));
                            return String.valueOf(num);
                        }
                    }
                }
            }
            else if(dealerId.equals(discountDetail.get(i).get(0)) && !(discountDetail.get(i).get(1).equals("0") || discountDetail.get(i).get(1).equals("null")) && (discountDetail.get(i).get(2).equals("0") || discountDetail.get(i).get(2).equals("null")))
            {
                for(int j=0; j<productsCompaniesDetail.size(); j++)
                {
                    if(productsCompaniesDetail.get(j).get(1).equals(discountDetail.get(i).get(1)))
                    {
                        if(Float.parseFloat(discountDetail.get(i).get(3)) == 0)
                        {
                            return discountDetail.get(i).get(4);
                        }
                        else
                        {
                            float num = Float.valueOf(saleAmount)/Float.valueOf(discountDetail.get(i).get(3));
                            num = num*Float.valueOf(discountDetail.get(i).get(4));
                            return String.valueOf(num);
                        }
                    }
                }
            }
            else if((discountDetail.get(i).get(0).equals("0") || discountDetail.get(i).get(0).equals("null")) && !(discountDetail.get(i).get(1).equals("0") || discountDetail.get(i).get(1).equals("null")) && productId.equals(discountDetail.get(i).get(2)))
            {
                for(int j=0; j<productsCompaniesDetail.size(); j++)
                {
                    if(productsCompaniesDetail.get(j).get(1).equals(discountDetail.get(i).get(1)))
                    {
                        if(Float.parseFloat(discountDetail.get(i).get(3)) == 0)
                        {
                            return discountDetail.get(i).get(4);
                        }
                        else
                        {
                            float num = Float.valueOf(saleAmount)/Float.valueOf(discountDetail.get(i).get(3));
                            num = num*Float.valueOf(discountDetail.get(i).get(4));
                            return String.valueOf(num);
                        }
                    }
                }
            }
            else if((discountDetail.get(i).get(0).equals("0") || discountDetail.get(i).get(0).equals("null")) && (discountDetail.get(i).get(1).equals("0") || discountDetail.get(i).get(1).equals("null")) && (discountDetail.get(i).get(2).equals("0") || discountDetail.get(i).get(2).equals("null")))
            {
                if(Float.parseFloat(discountDetail.get(i).get(3)) == 0)
                {
                    return discountDetail.get(i).get(4);
                }
                else
                {
                    float num = Float.valueOf(saleAmount)/Float.valueOf(discountDetail.get(i).get(3));
                    num = num*Float.valueOf(discountDetail.get(i).get(4));
                    return String.valueOf(num);
                }
            }
        }
        return discount;
    }

    public void showMessage(String title, String message)
    {
        AlertDialog.Builder objBuilder = new AlertDialog.Builder(this);
        objBuilder.setCancelable(true);
        objBuilder.setTitle(title);
        objBuilder.setMessage(message);
        objBuilder.show();
    }

    @Override
    public void onLocationChanged(Location location) {
        if(checkLocationPermission())
        {
            try {
                String addr = getAddress(location.getLatitude(), location.getLongitude());
                this.lat = location.getLatitude();
                this.lon = location.getLongitude();
                this.orderPlaceArea = addr;

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public String getAddress(double lat, double lng) throws IOException {
        String areaName = "";
        Geocoder geocoder = new Geocoder(MainActivity.this, Locale.getDefault());
        List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
        Address obj = addresses.get(0);
        String add = obj.getAddressLine(0);
        add = "\n" + obj.getCountryName();
        add = "\n" + obj.getCountryCode();
        add = "\n" + obj.getAdminArea();
        add = "\n" + obj.getSubAdminArea();
        add = "\n" + obj.getPostalCode();
        add = "\n" + obj.getSubAdminArea();
        add = "\n" + obj.getLocality();
        add = "\n" + obj.getSubLocality();
        add = "\n" + obj.getSubThoroughfare();

        return obj.getAddressLine(0);
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    public boolean checkLocationPermission()
    {
        String permission = "android.permission.ACCESS_FINE_LOCATION";
        int res = this.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }
}

