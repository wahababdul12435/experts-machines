package com.example.younastraders;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class Settings extends AppCompatActivity {
    EditText edId;
    EditText edName;
    EditText edPass;
    EditText edCurrPass;
    EditText edNewPass;
    EditText edReNewPass;
    TextView txtNameChange;
    TextView txtPassChange;
    Button btnSaveChanges;
    Switch switchAuto;

    String stName;
    String stPass;
    String autoSend;
    SendData objSendData;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);
        edId = findViewById(R.id.ed_settings_id);
        edName = findViewById(R.id.ed_settings_name);
        edPass = findViewById(R.id.ed_settings_password);
        edCurrPass = findViewById(R.id.ed_settings_currpass);
        edNewPass = findViewById(R.id.ed_settings_newpass);
        edReNewPass = findViewById(R.id.ed_settings_renewpass);
        txtNameChange = findViewById(R.id.txt_settings_name);
        txtPassChange = findViewById(R.id.txt_settings_password);
        btnSaveChanges = findViewById(R.id.btn_settings_save);
        switchAuto = findViewById(R.id.switch_settings_autosend);
        if(GlobalVariables.getAutoSend().equals("true"))
        {
            switchAuto.setChecked(true);
        }
        else
        {
            switchAuto.setChecked(false);
        }
        stName = edName.getText().toString();
        stPass = edPass.getText().toString();
        objSendData = new SendData(this);

        txtNameChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edName.setEnabled(true);
                btnSaveChanges.setVisibility(View.VISIBLE);
            }
        });

        txtPassChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edCurrPass.setVisibility(View.VISIBLE);
                edNewPass.setVisibility(View.VISIBLE);
                edReNewPass.setVisibility(View.VISIBLE);
                btnSaveChanges.setVisibility(View.VISIBLE);
                edPass.setVisibility(View.GONE);
                txtPassChange.setVisibility(View.GONE);
            }
        });

        switchAuto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnSaveChanges.setVisibility(View.VISIBLE);
            }
        });

        btnSaveChanges.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edName.isEnabled() && !(edName.getText().toString().equals("")))
                {
                    stName = edName.getText().toString();
                }
                if(edCurrPass.getVisibility() == View.VISIBLE)
                {
                    if(edCurrPass.getText().toString().equals(stPass))
                    {
                        if(edNewPass.getText().toString().equals(edReNewPass.getText().toString()))
                        {
                            stPass = edNewPass.getText().toString();
                        }
                        else
                        {
                            Toast.makeText(Settings.this, "New Password Not matched", Toast.LENGTH_LONG).show();
                            return;
                        }
                    }
                    else
                    {
                        Toast.makeText(Settings.this, "Incorrect Password", Toast.LENGTH_LONG).show();
                        return;
                    }
                }
                if(switchAuto.isChecked())
                {
                    autoSend = "true";
                }
                else
                {
                    autoSend = "false";
                }
                objSendData.updateUserDetails(GlobalVariables.getUserId(), stName, stPass, autoSend);
            }
        });
    }
}
