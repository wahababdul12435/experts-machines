package com.example.younastraders.tabs;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.example.younastraders.DatabaseSQLite;
import com.example.younastraders.GetData;
import com.example.younastraders.Home;
import com.example.younastraders.InputFilterMinMax;
import com.example.younastraders.MainActivity;
import com.example.younastraders.PendingOrderDetail;
import com.example.younastraders.R;
import com.example.younastraders.SendData;
import com.google.android.material.tabs.TabLayout;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PendingCashFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PendingCashFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    LinearLayout lv;
    ProgressBar progressSendAllPending;
    Button btnSendAllPendings;
    SendData objSendData;
    LinearLayout[] subLayout;
    GetData objGetData;

    String[] strPaymentId;
    TextView[] txtSrNo;
    TextView[] txtDealerName;
    TextView[] txtCash;
    Button[] btnOps;
    String[] strStatus;
    TextView[] txtDate;
    int cashCount;
    String strPreDate;
    SendMessage SM;
    View rootView;
    View[] viewLine;
    boolean setColor = false;

    public PendingCashFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FlightsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PendingCashFragment newInstance(String param1, String param2) {
        PendingCashFragment fragment = new PendingCashFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_pending_cash, container, false);
        lv = rootView.findViewById(R.id.linear_layout);

        progressSendAllPending = rootView.findViewById(R.id.progress_sendallpending);
        btnSendAllPendings = rootView.findViewById(R.id.btn_delivery_sendpending);
        objSendData = new SendData(getActivity());
        objGetData = new GetData(getActivity(), "Pending Cash");
        cashCount = 0;
        getPendingCashData();
        btnSendAllPendings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressSendAllPending.setVisibility(View.VISIBLE);
//                Cursor res = objGetData.getPendingDelivered();
//                int num = res.getCount();
//                boolean response = false;
//
//                if(num > 0)
//                {
//                    while(res.moveToNext())
//                    {
//                        response = objSendData.sendSavedDelivered(res.getString(0),res.getString(1),res.getString(2),res.getString(3),res.getString(4),res.getString(5),res.getString(6),res.getString(7),res.getString(8),res.getString(9));
//                    }
//                }
//                if(response)
//                {
//                    Toast.makeText(getActivity(), "Delivered Orders Sent", Toast.LENGTH_LONG).show();
//                }
                String response = objSendData.sendAllPendingCash();
                Toast.makeText(getActivity(), response, Toast.LENGTH_LONG).show();

                Intent intent = new Intent(getActivity(), Home.class);
                startActivity(intent);
                getActivity().finish();
            }
        });
        return rootView;
    }

    public void getPendingCashData()
    {
        Cursor res = objGetData.getCashDetails();
        int num = res.getCount();

        if(num <= 0)
        {
            txtDealerName = new TextView[1];
            lv.addView(createEmptyText());
        }
        else
        {
            subLayout = new LinearLayout[num];
            strPaymentId = new String[num];
            txtSrNo = new TextView[num];
            txtDealerName = new TextView[num];
            txtCash = new TextView[num];
            txtDate = new TextView[num];
            btnOps = new Button[num];
            strStatus = new String[num];
            viewLine = new View[num];
            strPreDate = "";
            while(res.moveToNext())
            {
                cashCount++;
                strPaymentId[cashCount-1] = res.getString(0);
                strStatus[cashCount-1] = res.getString(11);
                if(!strPreDate.equals(res.getString(8)))
                {
                    strPreDate = res.getString(8);
                    lv.addView(createDateText(strPreDate));
                }
                if(res.getString(4).equals("Return"))
                {
                    lv.addView(createLinearLayout(String.valueOf(cashCount), res.getString(12), res.getString(3)+" ("+res.getString(4)+")", res.getString(11)));
                    lv.addView(createLineView());
                }
                else
                {
                    lv.addView(createLinearLayout(String.valueOf(cashCount), res.getString(12), res.getString(3), res.getString(11)));
                    lv.addView(createLineView());
                }
//                addListenerToButtons();
            }
        }
    }


    public TextView createEmptyText() {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        txtDealerName[0] = new TextView(getActivity());
        txtDealerName[0].setLayoutParams(lparams);
        txtDealerName[0].setText("No Record to Show");
        txtDealerName[0].setTextSize(TypedValue.COMPLEX_UNIT_SP,18);
        txtDealerName[0].setGravity(Gravity.CENTER);
        return txtDealerName[0];
    }

    public TextView createSrNoText(String text) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(Math.round(getResources().getDimension(R.dimen.invoice_srno)), ViewGroup.LayoutParams.WRAP_CONTENT);
        txtSrNo[cashCount-1] = new TextView(getActivity());
        txtSrNo[cashCount-1].setLayoutParams(lparams);
//        txtSrNo[cashCount-1].setBackgroundColor(Color.RED);
        txtSrNo[cashCount-1].setText(text);
        txtSrNo[cashCount-1].setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        return txtSrNo[cashCount-1];
    }

    public TextView createDealerText(String text) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(Math.round(getResources().getDimension(R.dimen.dealer_pending_cash)), ViewGroup.LayoutParams.WRAP_CONTENT);
        txtDealerName[cashCount-1] = new TextView(getActivity());
        txtDealerName[cashCount-1].setLayoutParams(lparams);
//        txtDealerName[cashCount-1].setBackgroundColor(Color.RED);
        txtDealerName[cashCount-1].setText(text);
        return txtDealerName[cashCount-1];
    }

    public TextView createCashText(String text) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        txtCash[cashCount-1] = new TextView(getActivity());
        txtCash[cashCount-1].setLayoutParams(lparams);
//        txtCash[cashCount-1].setBackgroundColor(Color.GREEN);
        txtCash[cashCount-1].setText(text);
        return txtCash[cashCount-1];
    }

    public View createLineView() {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,  Math.round(getResources().getDimension(R.dimen.line)));
        viewLine[cashCount-1] = new View(getActivity());
        viewLine[cashCount-1].setLayoutParams(lparams);
        viewLine[cashCount-1].setBackgroundColor(Color.BLACK);
        return viewLine[cashCount-1];
    }

    public TextView createDateText(String text) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        txtDate[cashCount-1] = new TextView(getActivity());
        txtDate[cashCount-1].setLayoutParams(lparams);
//        txtDate[cashCount-1].setBackgroundColor(Color.YELLOW);
        txtDate[cashCount-1].setText(text);
        txtDate[cashCount-1].setGravity(Gravity.CENTER);
        txtDate[cashCount-1].setTextColor(Color.BLACK);
        return txtDate[cashCount-1];
    }

//    public Button createOpButton(String status) {
//        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//        btnOps[cashCount-1] = new Button(getActivity());
//        btnOps[cashCount-1].setLayoutParams(lparams);
//        if(status.equals("Pending"))
//        {
//            btnOps[cashCount-1].setText("Edit");
//        }
//        else
//        {
//            btnOps[cashCount-1].setText("SENT");
//        }
//        btnOps[cashCount-1].setBackgroundColor(Color.TRANSPARENT);
//        return btnOps[cashCount-1];
//    }

    public LinearLayout createLinearLayout(String srNo, String dealerName, String cash, String status) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        subLayout[cashCount-1] = new LinearLayout(getActivity());
        subLayout[cashCount-1].setOrientation(LinearLayout.HORIZONTAL);
        subLayout[cashCount-1].setLayoutParams(lparams);
        subLayout[cashCount-1].addView(createSrNoText(srNo));
        subLayout[cashCount-1].addView(createDealerText(dealerName));
        subLayout[cashCount-1].addView(createCashText(cash));
//        subLayout[cashCount-1].addView(createDateText(date));
//        subLayout[cashCount-1].addView(createOpButton(status));
        if(setColor)
        {
            subLayout[cashCount-1].setBackgroundColor(Color.parseColor("#E1E1E1"));
            setColor = false;
        }
        else
        {
            setColor = true;
        }
        if(status.equals("Pending"))
        {
            final String paymentId = strPaymentId[cashCount-1];
            subLayout[cashCount-1].setBackgroundColor(Color.parseColor("#FFA8A8"));
            subLayout[cashCount-1].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SM.sendData(paymentId);
                    TabLayout tabhost = (TabLayout) getActivity().findViewById(R.id.tab_layout);
                    tabhost.getTabAt(0).select();
                }
            });
        }
        else
        {
//            subLayout[cashCount-1].setBackgroundColor(Color.parseColor("#B0FFA8"));
        }
        return subLayout[cashCount-1];
    }

//    public void addListenerToButtons()
//    {
//        for(int i=0; i<cashCount; i++)
//        {
//            final int finalI = i;
//            btnOps[i].setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if(strStatus[finalI].equals("Pending"))
//                    {
//                        SM.sendData(strPaymentId[finalI]);
//                        TabLayout tabhost = (TabLayout) getActivity().findViewById(R.id.tab_layout);
//                        tabhost.getTabAt(0).select();
//                    }
//                }
//            });
//        }
//    }

    interface SendMessage {
        void sendData(String message);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            SM = (SendMessage) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException("Error in retrieving data. Please try again");
        }
    }
}