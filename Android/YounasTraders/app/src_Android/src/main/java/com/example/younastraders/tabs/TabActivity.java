package com.example.younastraders.tabs;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.viewpager.widget.ViewPager;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.younastraders.GetData;
import com.example.younastraders.GlobalVariables;
import com.example.younastraders.R;
import com.example.younastraders.tabsdelivery.AddDeliveryFragment;
import com.example.younastraders.tabsdelivery.DeliveryTabActivity;
import com.example.younastraders.tabsdelivery.ViewDeliveryFragment;
import com.example.younastraders.ui.delivery.DeliveryViewModel;
import com.google.android.material.badge.BadgeDrawable;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

public class TabActivity  extends AppCompatActivity implements PendingCashFragment.SendMessage {

    private Toolbar toolbar;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    ViewPagerAdapter viewPagerAdapter;

    private AddCashFragment addCashFragment;
    private PendingCashFragment pendingCashFragment;
//    private TravelFragment travelFragment;

    private AppBarConfiguration mAppBarConfiguration;
    GetData objGetData;
    NavigationView navigationView;

    public static String paymentId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabs);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        viewPager = findViewById(R.id.view_pager);
        tabLayout = findViewById(R.id.tab_layout);

        addCashFragment = new AddCashFragment();
        pendingCashFragment = new PendingCashFragment();
//        travelFragment = new TravelFragment();

        tabLayout.setupWithViewPager(viewPager);

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), 0);
        viewPagerAdapter.addFragment(addCashFragment, "Add Cash");
        viewPagerAdapter.addFragment(pendingCashFragment, "Pending Cash");
//        viewPagerAdapter.addFragment(travelFragment, "Travel");
        viewPager.setAdapter(viewPagerAdapter);

        tabLayout.getTabAt(0).setIcon(R.drawable.ic_baseline_explore_24);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_baseline_flight_24);
//        tabLayout.getTabAt(2).setIcon(R.drawable.ic_baseline_card_travel_24);

        if(!GlobalVariables.pendingCash.equals("0"))
        {
            BadgeDrawable badgeDrawable = tabLayout.getTabAt(1).getOrCreateBadge();
            badgeDrawable.setVisible(true);
            badgeDrawable.setNumber(Integer.parseInt(GlobalVariables.pendingCash));
        }


    }

    @Override
    public void sendData(String message) {
        String tag = "android:switcher:" + R.id.view_pager + ":" + 0;
        AddCashFragment f = (AddCashFragment) getSupportFragmentManager().findFragmentByTag(tag);
        f.displayReceivedData(message);
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {

        private List<Fragment> fragments = new ArrayList<>();
        private List<String> fragmentTitle = new ArrayList<>();

        public ViewPagerAdapter(@NonNull FragmentManager fm, int behavior) {
            super(fm, behavior);
        }

        public void addFragment(Fragment fragment, String title) {
            fragments.add(fragment);
            fragmentTitle.add(title);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitle.get(position);
        }
    }
}
