package com.example.younastraders.ui.home;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.younastraders.GetData;
import com.example.younastraders.GlobalVariables;
import com.example.younastraders.Home;
import com.example.younastraders.MainActivity;
import com.example.younastraders.R;
import com.example.younastraders.ui.viewpending.ViewPendingOrdersFragment;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.DefaultAxisValueFormatter;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    PieChart objPieChar1;
    BarChart objBarChar2;
    TextView txtTotalBooking;
    TextView txtTotalDelivered;
    String bookingCount = "0";
    String deliverCount = "0";
    GetData objGetData;
    String[][] bookingDatesData;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
        objGetData = new GetData(getActivity());
        bookingDatesData = new String[7][2];
        Calendar cal = Calendar.getInstance();

        Date objDate = cal.getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd/MMM/yyyy");
        bookingDatesData[0][0] = df.format(objDate);
        bookingDatesData[0][1] = "0";

        for(int i=1; i<=6; i++)
        {
            cal.add(Calendar.DATE, -1);
            objDate = cal.getTime();
            df = new SimpleDateFormat("dd/MMM/yyyy");
            bookingDatesData[i][0] = df.format(objDate);
            bookingDatesData[i][1] = "0";
        }

        getCountData();
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        homeViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
            }
        });

        objPieChar1 = root.findViewById(R.id.piechart1);
        objBarChar2 = root.findViewById(R.id.barchart2);
//        txtTotalBooking = root.findViewById(R.id.txt_dash_bookings);
//        txtTotalDelivered = root.findViewById(R.id.txt_dash_delivered);

        objPieChar1.setUsePercentValues(false);
        objPieChar1.getDescription().setEnabled(false);
        objPieChar1.setExtraOffsets(5,10,5,5);
        objPieChar1.setDragDecelerationFrictionCoef(0.99f);
        objPieChar1.setDrawHoleEnabled(false);
        objPieChar1.setHoleColor(Color.WHITE);
        objPieChar1.setTransparentCircleRadius(61f);

        objBarChar2.getDescription().setEnabled(false);

//        txtTotalBooking.setText("Total Bookings: 10");
//        txtTotalDelivered.setText("Total Delivered: 8");

        setData1();
        setData2();

        return root;
    }

    private void setData1()
    {
        ArrayList<PieEntry> yVals = new ArrayList<>();
        yVals.add(new PieEntry(1, "Bookings"));
        yVals.add(new PieEntry(0, "Delivered"));

        PieDataSet dataSet = new PieDataSet(yVals, "");
        dataSet.setValueFormatter(new DefaultAxisValueFormatter(0));
        dataSet.setSliceSpace(3f);
        dataSet.setSelectionShift(5f);
        dataSet.setColors(ColorTemplate.MATERIAL_COLORS);

        PieData data = new PieData(dataSet);
        data.setValueTextSize(10f);
        data.setValueTextColor(Color.YELLOW);

        objPieChar1.setData(data);
    }

    private void setData2()
    {
        ArrayList<BarEntry> yVals = new ArrayList<>();
        final ArrayList<String> xLabel = new ArrayList<>();
        for(int i=0; i<bookingDatesData.length; i++)
        {
//            float radians = (float)Math.toRadians(i);
//            float val = (float) Math.sin(radians);
//            entries.add(new BarEntry(i, val));
            float value = (float)(Math.random()*100);
            yVals.add(new BarEntry(i, Integer.parseInt(bookingDatesData[i][1])));
            xLabel.add(bookingDatesData[i][0]);
        }

        BarDataSet set;
        set = new BarDataSet(yVals, "");
        set.setColors(ColorTemplate.MATERIAL_COLORS);
        set.setDrawValues(true);

        objBarChar2.getXAxis().setValueFormatter(new IndexAxisValueFormatter(xLabel));


        BarData data = new BarData(set);
//        data.setDrawValues(false);
//        data.setBarWidth(0.8f);

        objBarChar2.setData(data);
        objBarChar2.invalidate();
        objBarChar2.animateY(500);
    }

    private void getCountData()
    {
        Cursor res = objGetData.getBookingCount();
        res.moveToNext();
        bookingCount = res.getString(0);

        res = objGetData.getDeliverCount();
        res.moveToNext();
        deliverCount = res.getString(0);

        for(int i=0; i<bookingDatesData.length; i++)
        {
            res = objGetData.getBookingCount(bookingDatesData[i][0]);
            res.moveToNext();
            bookingDatesData[i][1] = res.getString(0);
        }
    }
}