package com.example.younastraders;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

public class SyncProgress extends Activity {

    boolean syncCheck = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync_progress);

        DatabaseSync objDatabaseSync = new DatabaseSync(this);
        syncCheck = objDatabaseSync.insertAllRecord();

        if(syncCheck)
        {
            Toast.makeText(this, "Data Synced Successfully", Toast.LENGTH_SHORT).show();
        }
        else
        {
            Toast.makeText(this, "Data Unable to Synced", Toast.LENGTH_SHORT).show();
        }

        Intent intent=new Intent(SyncProgress.this, Home.class);
        startActivity(intent);
        finish();

    }
}
