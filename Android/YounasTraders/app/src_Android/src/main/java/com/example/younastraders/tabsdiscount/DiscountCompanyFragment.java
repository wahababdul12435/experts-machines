package com.example.younastraders.tabsdiscount;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.younastraders.DatabaseSQLite;
import com.example.younastraders.GetData;
import com.example.younastraders.InvoicedOrderDetail;
import com.example.younastraders.R;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DiscountCompanyFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DiscountCompanyFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    LinearLayout lv;
    //    SearchableSpinner edCompanyName;
//    ArrayAdapter<String> companyAdapter;
//    public String[] companysId;
//    public String[] companysName;
    LinearLayout[] subLayout;
    View[] viewLine;
    int discountCount;
    String stInvoiceDetail;

    DatabaseSQLite objSqlLite;
    ArrayAdapter<String> invoicesNumAdapter;
//    Button btnGetDetail;

    String[] stOrderIds;
    String[] stOrderIdsList;
    TextView[] txtSrNo;
    TextView[] txtCompanyName;
    TextView[] txtCompanyAddress;
    TextView[] txtSaleAmount;
    TextView[] txtDiscount;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public DiscountCompanyFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ExploreFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DiscountCompanyFragment newInstance(String param1, String param2) {
        DiscountCompanyFragment fragment = new DiscountCompanyFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    private int findIndexInArray(String query, String[] arr) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i].equals(query)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_discount_company, container, false);
        objSqlLite = new DatabaseSQLite(getActivity());
        discountCount = 0;
        lv = rootView.findViewById(R.id.linear_layout);
//        GetData objGetData = new GetData(getActivity());
//        companysId = objGetData.getCompanysId();
//        companysName = objGetData.getCompanysName();
//        edCompanyName = rootView.findViewById(R.id.ed_company_name);
//        if(companysName == null)
//        {
//            companysName = new String[1];
//            companysName[0] = "No Company Record";
//        }
//        companyAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, companysName);
//        edCompanyName.setAdapter(companyAdapter);

//        btnGetDetail = rootView.findViewById(R.id.btn_getdetail);
        viewAll();
        return rootView;
    }

    public void viewAll()
    {
        Cursor res = objSqlLite.getCompanyDiscount();
        int num = res.getCount();

        if(num == 0)
        {
            txtCompanyName = new TextView[1];
            lv.addView(createEmptyText());

            subLayout = new LinearLayout[1];
            stOrderIdsList = new String[1];
            stOrderIds = new String[1];
            txtSrNo = new TextView[1];
            txtCompanyName = new TextView[1];
            txtSaleAmount = new TextView[1];
            txtDiscount = new Button[1];
            stOrderIdsList[0] = "Select Invoice";
        }
        else
        {
            subLayout = new LinearLayout[res.getCount()];
            viewLine = new View[res.getCount()];
            stOrderIdsList = new String[res.getCount()+1];
            stOrderIds = new String[res.getCount()];
            txtSrNo = new TextView[res.getCount()];
            txtCompanyName = new TextView[res.getCount()];
            txtSaleAmount = new TextView[res.getCount()];
            txtDiscount = new Button[res.getCount()];
            res.moveToNext();
            stOrderIdsList[0] = "Company Name";
//            if(res.getString(0) == null)
////            {
////                txtCompanyName = new TextView[1];
////                lv.addView(createEmptyText());
////            }
////            else
////            {
////                do {
////                    discountCount++;
////                    stOrderIds[discountCount-1] = res.getString(0);
////                    stOrderIdsList[discountCount] = res.getString(0);
////                    lv.addView(createLinearLayout(res.getString(0), res.getString(1), res.getString(2), res.getString(3)));
////                    addListenerToButtons();
////                }while (res.moveToNext());
////            }
            do {
                discountCount++;
                stOrderIds[discountCount-1] = res.getString(0);
                stOrderIdsList[discountCount] = res.getString(0);
                lv.addView(createLinearLayout(String.valueOf(discountCount), res.getString(0), res.getString(1), res.getString(2)));
                lv.addView(createLineView());
                addListenerToButtons();
            }while (res.moveToNext());
        }
    }


    public TextView createEmptyText() {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        txtCompanyName[0] = new TextView(getActivity());
        txtCompanyName[0].setLayoutParams(lparams);
        txtCompanyName[0].setText("No Record to Show");
        txtCompanyName[0].setTextSize(TypedValue.COMPLEX_UNIT_SP,18);
        txtCompanyName[0].setGravity(Gravity.CENTER);
        return txtCompanyName[0];
    }

    public TextView createSrNoText(String text) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(Math.round(getResources().getDimension(R.dimen.discount_bonus_1)), LinearLayout.LayoutParams.WRAP_CONTENT);
        txtSrNo[discountCount-1] = new TextView(getActivity());
        txtSrNo[discountCount-1].setLayoutParams(lparams);
        txtSrNo[discountCount-1].setText(text);
        txtSrNo[discountCount-1].setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
//        txtSrNo[discountCount-1].setBackgroundColor(Color.BLUE);
        return txtSrNo[discountCount-1];
    }

    public TextView createCompanyText(String text) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(Math.round(getResources().getDimension(R.dimen.discount_bonus_2)), LinearLayout.LayoutParams.WRAP_CONTENT);
        txtCompanyName[discountCount-1] = new TextView(getActivity());
        txtCompanyName[discountCount-1].setLayoutParams(lparams);
        txtCompanyName[discountCount-1].setText(text);
//        txtCompanyName[discountCount-1].setBackgroundColor(Color.YELLOW);
        return txtCompanyName[discountCount-1];
    }

    public TextView createSaleAmountText(String text) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(Math.round(getResources().getDimension(R.dimen.discount_bonus_3)), LinearLayout.LayoutParams.WRAP_CONTENT);
        txtSaleAmount[discountCount-1] = new TextView(getActivity());
        txtSaleAmount[discountCount-1].setLayoutParams(lparams);
        txtSaleAmount[discountCount-1].setText(text);
        txtSaleAmount[discountCount-1].setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
//        txtSaleAmount[discountCount-1].setBackgroundColor(Color.GREEN);
        return txtSaleAmount[discountCount-1];
    }

    public TextView createDiscountText(String text) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        txtDiscount[discountCount-1] = new Button(getActivity());
        txtDiscount[discountCount-1].setLayoutParams(lparams);
        txtDiscount[discountCount-1].setText(text);
//        txtDiscount[discountCount-1].setBackgroundColor(Color.RED);
        txtDiscount[discountCount-1].setBackgroundColor(Color.TRANSPARENT);
        return txtDiscount[discountCount-1];
    }

    public LinearLayout createLinearLayout(String srNo, String companyName, String saleAmount, String discount) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        subLayout[discountCount-1] = new LinearLayout(getActivity());
        subLayout[discountCount-1].setOrientation(LinearLayout.HORIZONTAL);
        subLayout[discountCount-1].setLayoutParams(lparams);
        subLayout[discountCount-1].addView(createSrNoText(srNo));
        subLayout[discountCount-1].addView(createCompanyText(companyName));
        subLayout[discountCount-1].addView(createSaleAmountText(saleAmount));
        subLayout[discountCount-1].addView(createDiscountText(discount));
        subLayout[discountCount-1].setPadding(0,0,0,10);
        return subLayout[discountCount-1];
    }

    public View createLineView() {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,  Math.round(getResources().getDimension(R.dimen.line)));
        viewLine[discountCount-1] = new View(getActivity());
        viewLine[discountCount-1].setLayoutParams(lparams);
        viewLine[discountCount-1].setBackgroundColor(Color.BLACK);
        return viewLine[discountCount-1];
    }

    public void addListenerToButtons()
    {
        for(int i=0; i<discountCount; i++)
        {
            final int finalI = i;
            txtDiscount[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), InvoicedOrderDetail.class);
                    intent.putExtra("ORDER_ID", stOrderIds[finalI]);
                    startActivity(intent);
                }
            });
        }
    }
}