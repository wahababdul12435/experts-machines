package com.example.younastraders;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.ListView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

public class BackgroundTask extends AsyncTask<String,Void,String>{

    private CallBack callBack;
    public ListView listView;

    public CallBack getCallBack() {
        return callBack;
    }

    public void setCallBack(CallBack callBack) {
        this.callBack = callBack;
    }

    private String response = "";
//    private ProgressDialog dialog;
    Context ctx;
    public static boolean inProgress;

    BackgroundTask(Context ctx)
    {
        this.ctx = ctx;
    }

    @Override
    protected void onPreExecute() {
//        dialog = new ProgressDialog(ctx);
//        dialog.setMessage("Progress start");
//        dialog.show();
    }

    protected String doInBackground(String... params) {
//        String sendInvoice = "http://10.0.2.2/GitKraken/distribution/experts-machines/PHP/YounasTraders/SendInvoice.php?type=";
//        String sendLocation = "http://10.0.2.2/GitKraken/distribution/experts-machines/PHP/YounasTraders/SendLocation.php?type=";
//        String sendRegistration = "http://10.0.2.2/GitKraken/distribution/experts-machines/PHP/YounasTraders/AndroidDataGet/UserAuthentication.php?method=register";
//        String checkRegStatus = "http://10.0.2.2/GitKraken/distribution/experts-machines/PHP/YounasTraders/AndroidDataGet/UserAuthentication.php?method=registerStatus&userID=";

//         ------------------------------------- LAPTOP ID -----------------------------------------
//        String sendInvoice = "http://192.168.43.176/GitKraken/distribution/experts-machines/PHP/YounasTraders/SendInvoice.php?type=";
//        String sendLocation = "http://192.168.43.176/GitKraken/distribution/experts-machines/PHP/YounasTraders/SendLocation.php?type=";
//        String sendRegistration = "http://192.168.43.176/GitKraken/distribution/experts-machines/PHP/YounasTraders/AndroidDataGet/UserAuthentication.php?method=register";
//        String checkRegStatus = "http://192.168.43.176/GitKraken/distribution/experts-machines/PHP/YounasTraders/AndroidDataGet/UserAuthentication.php?method=registerStatus&userID=";

//        --------------------------------------- OFFICE SYSTEM ID -----------------------------------
//        String sendInvoice = "http://192.168.10.2/GitKraken/distribution/experts-machines/PHP/YounasTraders/SendInvoice.php?type=";
//        String sendLocation = "http://192.168.10.2/GitKraken/distribution/experts-machines/PHP/YounasTraders/SendLocation.php?type=";
//        String sendRegistration = "http://192.168.10.2/GitKraken/distribution/experts-machines/PHP/YounasTraders/AndroidDataGet/UserAuthentication.php?method=";
//        String checkRegStatus = "http://192.168.10.2/GitKraken/distribution/experts-machines/PHP/YounasTraders/AndroidDataGet/UserAuthentication.php?method=registerStatus&userID=";

//        --------------------------------------- LIVE SERVER ID -----------------------------------
        String sendInvoice = "https://tuberichy.com/ProfitFlow/SendInvoice.php?softwareid="+GlobalVariables.softwareId+"&type=";
        String sendLocation = "https://tuberichy.com/ProfitFlow/SendLocation.php?softwareid="+GlobalVariables.softwareId+"&type=";
        String sendRegistration = "https://tuberichy.com/ProfitFlow/AndroidDataGet/UserAuthentication.php?softwareid="+GlobalVariables.softwareId+"&method=register";
        String checkRegStatus = "https://tuberichy.com/ProfitFlow/AndroidDataGet/UserAuthentication.php?softwareid="+GlobalVariables.softwareId+"&method=registerStatus&userID=";

        String tableData = params[0];
        if(tableData.equals("order"))
        {
            String type = "Booking";
            sendInvoice += type;
            String dealerId = params[1];
            String productId = params[2];
            String quantity = params[3];
            String unit = params[4];
            String orderPrice = params[5];
            String bonus = params[6];
            String discount = params[7];
            String finalPrice = params[8];
            String latitude = params[9];
            String longitude = params[10];
            String order_place_area = params[11];
            String date = params[12];
            String time = params[13];
            String salesmanId = params[14];
            String status = params[15];
            try {
                URL objurl = new URL(sendInvoice);
                HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setConnectTimeout(2 * 1000);
                objhttpurlconnection.connect();
                if (objhttpurlconnection.getResponseCode() == 200)
                {
                    objhttpurlconnection.disconnect();
                    objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                    objhttpurlconnection.setRequestMethod("POST");
                    objhttpurlconnection.setDoOutput(true);
                    objhttpurlconnection.setDoInput(true);
                    OutputStream objos = objhttpurlconnection.getOutputStream();
                    BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                    String data = URLEncoder.encode("dealer_id", "UTF-8") +"="+URLEncoder.encode(dealerId, "UTF-8")+"&"+
                            URLEncoder.encode("product_id", "UTF-8")+"="+URLEncoder.encode(productId, "UTF-8")+"&"+
                            URLEncoder.encode("quantity", "UTF-8")+"="+URLEncoder.encode(quantity, "UTF-8")+"&"+
                            URLEncoder.encode("unit", "UTF-8")+"="+URLEncoder.encode(unit, "UTF-8")+"&"+
                            URLEncoder.encode("orderprice", "UTF-8")+"="+URLEncoder.encode(orderPrice, "UTF-8")+"&"+
                            URLEncoder.encode("bonus", "UTF-8")+"="+URLEncoder.encode(bonus, "UTF-8")+"&"+
                            URLEncoder.encode("discount", "UTF-8")+"="+URLEncoder.encode(discount, "UTF-8")+"&"+
                            URLEncoder.encode("finalprice", "UTF-8")+"="+URLEncoder.encode(finalPrice, "UTF-8")+"&"+
                            URLEncoder.encode("latitude", "UTF-8")+"="+URLEncoder.encode(latitude, "UTF-8")+"&"+
                            URLEncoder.encode("longitude", "UTF-8")+"="+URLEncoder.encode(longitude, "UTF-8")+"&"+
                            URLEncoder.encode("order_place_area", "UTF-8")+"="+URLEncoder.encode(order_place_area, "UTF-8")+"&"+
                            URLEncoder.encode("date", "UTF-8")+"="+URLEncoder.encode(date, "UTF-8")+"&"+
                            URLEncoder.encode("time", "UTF-8")+"="+URLEncoder.encode(time, "UTF-8")+"&"+
                            URLEncoder.encode("salesman_id", "UTF-8")+"="+URLEncoder.encode(salesmanId, "UTF-8")+"&"+
                            URLEncoder.encode("status", "UTF-8")+"="+URLEncoder.encode(status, "UTF-8");
                    objbuffwrite.write(data);
                    objbuffwrite.flush();
                    objbuffwrite.close();
                    objos.close();

                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(objhttpurlconnection.getInputStream()));
                    String orderResponse;
                    while ((orderResponse = bufferedReader.readLine()) != null)
                    {
                        response = orderResponse;
                    }
                    objhttpurlconnection.disconnect();
                }
                else
                {
                    response = "Unable to Connect To Server";
                }

                return response;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if(tableData.equals("register"))
        {
            String userName = params[1];
            String userID = params[2];
            String password = params[3];
            String userPower = params[4];
            String regDate = params[5];
            String status = params[6];
            try {
                URL objurl = new URL(sendRegistration);
                HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                objhttpurlconnection.setRequestMethod("POST");
                objhttpurlconnection.setDoOutput(true);
                objhttpurlconnection.setDoInput(true);
                OutputStream objos = objhttpurlconnection.getOutputStream();
                BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                String data = URLEncoder.encode("user_name", "UTF-8") +"="+URLEncoder.encode(userName, "UTF-8")+"&"+
                        URLEncoder.encode("user_id", "UTF-8")+"="+URLEncoder.encode(userID, "UTF-8")+"&"+
                        URLEncoder.encode("password", "UTF-8")+"="+URLEncoder.encode(password, "UTF-8")+"&"+
                        URLEncoder.encode("user_power", "UTF-8")+"="+URLEncoder.encode(userPower, "UTF-8")+"&"+
                        URLEncoder.encode("reg_date", "UTF-8")+"="+URLEncoder.encode(regDate, "UTF-8")+"&"+
                        URLEncoder.encode("status", "UTF-8")+"="+URLEncoder.encode(status, "UTF-8");
                objbuffwrite.write(data);
                objbuffwrite.flush();
                objbuffwrite.close();
                objos.close();

                InputStream objis = objhttpurlconnection.getInputStream();
                objis.close();
                objhttpurlconnection.disconnect();

                response = "Registration Request Sent Successfully";

                return response;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            response = "Registration Request Not Sent";
        }
        else if(tableData.equals("registerStatus"))
        {
            String userID = params[1];
            checkRegStatus += userID;
            try {
                URL objurl = new URL(checkRegStatus);
                HttpURLConnection con = (HttpURLConnection) objurl.openConnection();
                con.setConnectTimeout(2 * 1000);
                con.connect();
                if (con.getResponseCode() == 200)
                {
                    StringBuilder sb = new StringBuilder();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));
                    String regStatus;
                    while ((regStatus = bufferedReader.readLine()) != null)
                    {
                        response = regStatus;
                    }
                    return response;
                }
                else
                {
                    return response;
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if(tableData.equals("timestamplocation"))
        {
            String type = "TimeStamp";
            String userID = params[1];
            String latitude = params[2];
            String longitude = params[3];
            String locationName = params[4];
            String date = params[5];
            String time = params[6];
            sendLocation += type;
            try {
                URL objurl = new URL(sendLocation);
                HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();

                objhttpurlconnection.setConnectTimeout(2 * 1000);
                objhttpurlconnection.connect();
                if (objhttpurlconnection.getResponseCode() == 200)
                {
                    objhttpurlconnection.disconnect();
                    objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                    objhttpurlconnection.setRequestMethod("POST");
                    objhttpurlconnection.setDoOutput(true);
                    objhttpurlconnection.setDoInput(true);
                    OutputStream objos = objhttpurlconnection.getOutputStream();
                    BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                    String data = URLEncoder.encode("user_id", "UTF-8")+"="+URLEncoder.encode(userID, "UTF-8")+"&"+
                            URLEncoder.encode("latitude", "UTF-8")+"="+URLEncoder.encode(latitude, "UTF-8")+"&"+
                            URLEncoder.encode("longitude", "UTF-8")+"="+URLEncoder.encode(longitude, "UTF-8")+"&"+
                            URLEncoder.encode("location_name", "UTF-8")+"="+URLEncoder.encode(locationName, "UTF-8")+"&"+
                            URLEncoder.encode("date", "UTF-8")+"="+URLEncoder.encode(date, "UTF-8")+"&"+
                            URLEncoder.encode("time", "UTF-8")+"="+URLEncoder.encode(time, "UTF-8");
                    objbuffwrite.write(data);
                    objbuffwrite.flush();
                    objbuffwrite.close();
                    objos.close();

                    InputStream objis = objhttpurlconnection.getInputStream();
                    objis.close();
                    objhttpurlconnection.disconnect();
                    response = "Location Sent Successfully";

                }
                else
                {
                    response = "Location Not Sent";
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if(tableData.equals("resendtimestamplocation"))
        {
            String type = "TimeStamp";
            String userID = params[1];
            String latitude = params[2];
            String longitude = params[3];
            String locationName = params[4];
            String date = params[5];
            String time = params[6];
            sendLocation += type;
            try {
                URL objurl = new URL(sendLocation);
                HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();

                objhttpurlconnection.setConnectTimeout(2 * 1000);
                objhttpurlconnection.connect();
                if (objhttpurlconnection.getResponseCode() == 200)
                {
                    objhttpurlconnection.disconnect();
                    objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                    objhttpurlconnection.setRequestMethod("POST");
                    objhttpurlconnection.setDoOutput(true);
                    objhttpurlconnection.setDoInput(true);
                    OutputStream objos = objhttpurlconnection.getOutputStream();
                    BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                    String data = URLEncoder.encode("user_id", "UTF-8")+"="+URLEncoder.encode(userID, "UTF-8")+"&"+
                            URLEncoder.encode("latitude", "UTF-8")+"="+URLEncoder.encode(latitude, "UTF-8")+"&"+
                            URLEncoder.encode("longitude", "UTF-8")+"="+URLEncoder.encode(longitude, "UTF-8")+"&"+
                            URLEncoder.encode("location_name", "UTF-8")+"="+URLEncoder.encode(locationName, "UTF-8")+"&"+
                            URLEncoder.encode("date", "UTF-8")+"="+URLEncoder.encode(date, "UTF-8")+"&"+
                            URLEncoder.encode("time", "UTF-8")+"="+URLEncoder.encode(time, "UTF-8");
                    objbuffwrite.write(data);
                    objbuffwrite.flush();
                    objbuffwrite.close();
                    objos.close();

                    InputStream objis = objhttpurlconnection.getInputStream();
                    objis.close();
                    objhttpurlconnection.disconnect();
                    response = "Location Sent";

                }
                else
                {
                    response = "Location Not Sent";
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if(tableData.equals("currentlocation"))
        {
            String type = "CurrLocation";
            String userID = params[1];
            String latitude = params[2];
            String longitude = params[3];
            String locationName = params[4];
            sendLocation += type;
            try {
                URL objurl = new URL(sendLocation);
                HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();

                objhttpurlconnection.setConnectTimeout(2 * 1000);
                objhttpurlconnection.connect();
                if (objhttpurlconnection.getResponseCode() == 200)
                {
                    objhttpurlconnection.disconnect();
                    objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                    objhttpurlconnection.setRequestMethod("POST");
                    objhttpurlconnection.setDoOutput(true);
                    objhttpurlconnection.setDoInput(true);
                    OutputStream objos = objhttpurlconnection.getOutputStream();
                    BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                    String data = URLEncoder.encode("user_id", "UTF-8")+"="+URLEncoder.encode(userID, "UTF-8")+"&"+
                            URLEncoder.encode("latitude", "UTF-8")+"="+URLEncoder.encode(latitude, "UTF-8")+"&"+
                            URLEncoder.encode("longitude", "UTF-8")+"="+URLEncoder.encode(longitude, "UTF-8")+"&"+
                            URLEncoder.encode("location_name", "UTF-8")+"="+URLEncoder.encode(locationName, "UTF-8");
                    objbuffwrite.write(data);
                    objbuffwrite.flush();
                    objbuffwrite.close();
                    objos.close();
                    InputStream objis = objhttpurlconnection.getInputStream();
                    objis.close();
                    objhttpurlconnection.disconnect();
                    response = "Location Sent Successfully";

                }
                else
                {
                    response = "Location Not Sent";
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if(tableData.equals("dealerpinloc"))
        {
            String type = "DealerPinLoc";
            String dealerId = params[1];
            String latitude = params[2];
            String longitude = params[3];
            String locationName = params[4];
            sendLocation += type;
            try {
                URL objurl = new URL(sendInvoice);
                HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();

                objhttpurlconnection.setConnectTimeout(2 * 1000);
                objhttpurlconnection.connect();
                if (objhttpurlconnection.getResponseCode() == 200)
                {
                    objhttpurlconnection.disconnect();
                    objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                    objhttpurlconnection.setRequestMethod("POST");
                    objhttpurlconnection.setDoOutput(true);
                    objhttpurlconnection.setDoInput(true);
                    OutputStream objos = objhttpurlconnection.getOutputStream();
                    BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                    String data = URLEncoder.encode("dealer_id", "UTF-8")+"="+URLEncoder.encode(dealerId, "UTF-8")+"&"+
                            URLEncoder.encode("latitude", "UTF-8")+"="+URLEncoder.encode(latitude, "UTF-8")+"&"+
                            URLEncoder.encode("longitude", "UTF-8")+"="+URLEncoder.encode(longitude, "UTF-8")+"&"+
                            URLEncoder.encode("location_name", "UTF-8")+"="+URLEncoder.encode(locationName, "UTF-8");
                    objbuffwrite.write(data);
                    objbuffwrite.flush();
                    objbuffwrite.close();
                    objos.close();
                    InputStream objis = objhttpurlconnection.getInputStream();
                    objis.close();
                    objhttpurlconnection.disconnect();
                    response = "Location Sent Successfully";

                }
                else
                {
                    response = "Location Not Sent";
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if(tableData.equals("delivered"))
        {
            String type = "Delivered";
            String orderId = params[1];
            String dealerId = params[2];
            String cashCollected = params[3];
            String latitude = params[4];
            String longitude = params[5];
            String deliveredLocation = params[6];
            String stDate = params[7];
            String stTime = params[8];
            String userId = params[9];
            String status = params[10];
            sendInvoice += type;
            try {
                URL objurl = new URL(sendInvoice);
                HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();

                objhttpurlconnection.setConnectTimeout(2 * 1000);
                objhttpurlconnection.connect();
                if (objhttpurlconnection.getResponseCode() == 200)
                {
                    objhttpurlconnection.disconnect();
                    objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                    objhttpurlconnection.setRequestMethod("POST");
                    objhttpurlconnection.setDoOutput(true);
                    objhttpurlconnection.setDoInput(true);
                    OutputStream objos = objhttpurlconnection.getOutputStream();
                    BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                    String data = URLEncoder.encode("order_id", "UTF-8")+"="+URLEncoder.encode(orderId, "UTF-8")+"&"+
                            URLEncoder.encode("dealer_id", "UTF-8")+"="+URLEncoder.encode(dealerId, "UTF-8")+"&"+
                            URLEncoder.encode("cash_collected", "UTF-8")+"="+URLEncoder.encode(cashCollected, "UTF-8")+"&"+
                            URLEncoder.encode("latitude", "UTF-8")+"="+URLEncoder.encode(latitude, "UTF-8")+"&"+
                            URLEncoder.encode("longitude", "UTF-8")+"="+URLEncoder.encode(longitude, "UTF-8")+"&"+
                            URLEncoder.encode("location_name", "UTF-8")+"="+URLEncoder.encode(deliveredLocation, "UTF-8")+"&"+
                            URLEncoder.encode("date", "UTF-8")+"="+URLEncoder.encode(stDate, "UTF-8")+"&"+
                            URLEncoder.encode("time", "UTF-8")+"="+URLEncoder.encode(stTime, "UTF-8")+"&"+
                            URLEncoder.encode("user_id", "UTF-8")+"="+URLEncoder.encode(userId, "UTF-8")+"&"+
                            URLEncoder.encode("status", "UTF-8")+"="+URLEncoder.encode(status, "UTF-8");
                    objbuffwrite.write(data);
                    objbuffwrite.flush();
                    objbuffwrite.close();
                    objos.close();
                    InputStream objis = objhttpurlconnection.getInputStream();
                    objis.close();
                    objhttpurlconnection.disconnect();
                    response = "Delivery Sent Successfully";

                }
                else
                {
                    response = "Delivery Not Sent";
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return response;
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
//        dialog.show();
    }

    @Override
    protected void onPostExecute(String value) {
//        dialog.dismiss();
        if(callBack != null)
        {
            inProgress = false;
            callBack.onResult(value);
        }
    }

    public interface CallBack{
        void onResult(String value);
    }
}
