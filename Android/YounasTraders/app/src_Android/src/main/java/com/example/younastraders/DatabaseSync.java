package com.example.younastraders;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.view.View;
import android.widget.Toast;

public class DatabaseSync extends SQLiteOpenHelper {
    String[][] newDataUpdate;
    String[][] areaInfo;
    String[][] dealerInfo;
    String[][] productInfo;
    String[] dealerGPSData;
    String[][] invoicesData;
    String[][] dealersPendingPayments;
    String[][] batchwiseStock;
    String[][] discountInfo;
    String[][] bonusInfo;

    public static final String DATABASE_NAME = "YounasTraders.db";


    public Context context;

    public DatabaseSync(Context context) {
        super(context, DATABASE_NAME, null, 1);
        this.context = context;
        SQLiteDatabase db = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void deletaAllData()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(DatabaseSQLite.AREA_INFO_TABLE, null, null);
        db.delete(DatabaseSQLite.DEALER_INFO_TABLE, null, null);
        db.delete(DatabaseSQLite.PRODUCT_INFO_TABLE, null, null);
        db.delete(DatabaseSQLite.DEALER_GPS_LOC_TABLE, null, null);
        db.delete(DatabaseSQLite.DEALER_PENDINGS_TABLE, null, null);
        db.delete(DatabaseSQLite.BATCHWISE_STOCK_TABLE, null, null);
        db.delete(DatabaseSQLite.DISCOUNT_TABLE, null, null);
        db.delete(DatabaseSQLite.BONUS_TABLE, null, null);
    }

    public boolean insertarea(String areaId, String cityId, String areaName, String status)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseSQLite.AREA_COL_1, areaId);
        contentValues.put(DatabaseSQLite.AREA_COL_2, cityId);
        contentValues.put(DatabaseSQLite.AREA_COL_3, areaName);
        contentValues.put(DatabaseSQLite.AREA_COL_4, status);
        long result = db.insert(DatabaseSQLite.AREA_INFO_TABLE, null, contentValues);
        if(result == -1)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean insertDealer(String dealerId, String dealerAreaId, String dealerName, String dealerContact, String dealerAddress, String dealerType, String dealerCnic, String dealerLicNum, String dealerLicExp, String status)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseSQLite.DEALER_COL_1, dealerId);
        contentValues.put(DatabaseSQLite.DEALER_COL_2, dealerAreaId);
        contentValues.put(DatabaseSQLite.DEALER_COL_3, dealerName);
        contentValues.put(DatabaseSQLite.DEALER_COL_4, dealerContact);
        contentValues.put(DatabaseSQLite.DEALER_COL_5, dealerAddress);
        contentValues.put(DatabaseSQLite.DEALER_COL_6, dealerType);
        contentValues.put(DatabaseSQLite.DEALER_COL_7, dealerCnic);
        contentValues.put(DatabaseSQLite.DEALER_COL_8, dealerLicNum);
        contentValues.put(DatabaseSQLite.DEALER_COL_9, dealerLicExp);
        contentValues.put(DatabaseSQLite.DEALER_COL_10, status);
        long result = db.insert(DatabaseSQLite.DEALER_INFO_TABLE, null, contentValues);
        if(result == -1)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean insertProduct(String productId, String productName, String finalPrice, String status, String companyName)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseSQLite.PRODUCT_COL_1, productId);
        contentValues.put(DatabaseSQLite.PRODUCT_COL_2, productName);
        contentValues.put(DatabaseSQLite.PRODUCT_COL_3, finalPrice);
        contentValues.put(DatabaseSQLite.PRODUCT_COL_4, status);
        contentValues.put(DatabaseSQLite.PRODUCT_COL_5, companyName);
        long result = db.insert(DatabaseSQLite.PRODUCT_INFO_TABLE, null, contentValues);
        if(result == -1)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean insertDealerGPS(String dealerId)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseSQLite.DEALGPS_COL_2, dealerId);
        contentValues.put(DatabaseSQLite.DEALGPS_COL_3, "NULL");
        contentValues.put(DatabaseSQLite.DEALGPS_COL_4, "NULL");
        contentValues.put(DatabaseSQLite.DEALGPS_COL_5, "NULL");
        long result = db.insert(DatabaseSQLite.DEALER_GPS_LOC_TABLE, null, contentValues);
        if(result == -1)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean insertInvoice(String invoiceId, String dealerId, String productId, String quantity, String unit, String invoicePrice, String invoiceStatus)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseSQLite.INVOICES_COL_1, invoiceId);
        contentValues.put(DatabaseSQLite.INVOICES_COL_2, dealerId);
        contentValues.put(DatabaseSQLite.INVOICES_COL_3, productId);
        contentValues.put(DatabaseSQLite.INVOICES_COL_4, quantity);
        contentValues.put(DatabaseSQLite.INVOICES_COL_5, unit);
        contentValues.put(DatabaseSQLite.INVOICES_COL_6, invoicePrice);
        contentValues.put(DatabaseSQLite.INVOICES_COL_7, invoiceStatus);
        long result = db.insert(DatabaseSQLite.INVOICES_TABLE, null, contentValues);
        if(result == -1)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean insertDealerPending(String dealerId, String pendingPayments)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseSQLite.DEALPENDING_COL_1, dealerId);
        contentValues.put(DatabaseSQLite.DEALPENDING_COL_2, pendingPayments);
        long result = db.insert(DatabaseSQLite.DEALER_PENDINGS_TABLE, null, contentValues);
        if(result == -1)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean insertBatchwiseStock(String batchstockId, String productId, String batchNo, String quantity, String expiryDate)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseSQLite.BATCHSTOCK_COL_1, batchstockId);
        contentValues.put(DatabaseSQLite.BATCHSTOCK_COL_2, productId);
        contentValues.put(DatabaseSQLite.BATCHSTOCK_COL_3, batchNo);
        contentValues.put(DatabaseSQLite.BATCHSTOCK_COL_4, quantity);
        contentValues.put(DatabaseSQLite.BATCHSTOCK_COL_5, expiryDate);
        long result = db.insert(DatabaseSQLite.BATCHWISE_STOCK_TABLE, null, contentValues);
        if(result == -1)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean insertDiscountInfo(String approvalId, String dealerId, String companyName, String productId, String saleAmount, String discount, String startDate, String endDate)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseSQLite.DISCOUNT_COL_1, approvalId);
        contentValues.put(DatabaseSQLite.DISCOUNT_COL_2, dealerId);
        contentValues.put(DatabaseSQLite.DISCOUNT_COL_3, companyName);
        contentValues.put(DatabaseSQLite.DISCOUNT_COL_4, productId);
        contentValues.put(DatabaseSQLite.DISCOUNT_COL_5, saleAmount);
        contentValues.put(DatabaseSQLite.DISCOUNT_COL_6, discount);
        contentValues.put(DatabaseSQLite.DISCOUNT_COL_7, startDate);
        contentValues.put(DatabaseSQLite.DISCOUNT_COL_8, endDate);
        long result = db.insert(DatabaseSQLite.DISCOUNT_TABLE, null, contentValues);
        if(result == -1)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean insertBonusInfo(String approvalId, String dealerId, String companyName, String productId, String quantity, String bonus, String startDate, String endDate)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseSQLite.BONUS_COL_1, approvalId);
        contentValues.put(DatabaseSQLite.BONUS_COL_2, dealerId);
        contentValues.put(DatabaseSQLite.BONUS_COL_3, companyName);
        contentValues.put(DatabaseSQLite.BONUS_COL_4, productId);
        contentValues.put(DatabaseSQLite.BONUS_COL_5, quantity);
        contentValues.put(DatabaseSQLite.BONUS_COL_6, bonus);
        contentValues.put(DatabaseSQLite.BONUS_COL_7, startDate);
        contentValues.put(DatabaseSQLite.BONUS_COL_8, endDate);
        long result = db.insert(DatabaseSQLite.BONUS_TABLE, null, contentValues);
        if(result == -1)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean insertDataBaseSync(String dataName, String lastUpdateDate, String lastUpdateTime)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseSQLite.SYNCTRACK_COL_2, dataName);
        contentValues.put(DatabaseSQLite.SYNCTRACK_COL_3, lastUpdateDate);
        contentValues.put(DatabaseSQLite.SYNCTRACK_COL_4, lastUpdateTime);
        long result = db.insert(DatabaseSQLite.SYNC_TRACKING_TABLE, null, contentValues);
        if(result == -1)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public void checkNewDataUpdate()
    {
        DatabaseSQLite objsqlite = new DatabaseSQLite(context);
        Cursor res = objsqlite.getLastSynced();

        if(res.getCount() == 0)
        {
            deletaAllData();
            insertAllRecord();
        }
        else
        {

        }
    }

    public boolean insertAllRecord(String... str)
    {
        final boolean[] completeInsertion = {false};
        final boolean[] areaResponse = {false};
        final boolean[] dealerResponse = {false};
        final boolean[] productResponse = {false};
        final boolean[] dealerGPSResponse = {false};
        final boolean[] dealerPendings = {false};
        final boolean[] batchstock = {false};
        final boolean[] discount = {false};
        final boolean[] bonus = {false};

        final FetchDataMysql objfetchmysql1;
        final FetchDataMysql objfetchmysql2;
        final FetchDataMysql objfetchmysql3;
        final FetchDataMysql objfetchmysql4;
        final FetchDataMysql objfetchmysql5;
        final FetchDataMysql objfetchmysql6;
        final FetchDataMysql objfetchmysql7;
        final FetchDataMysql objfetchmysql8;
        
        if(str.length > 0)
        {
            objfetchmysql1 = new FetchDataMysql(context, "area_info", str[0], str[1]);
            objfetchmysql2 = new FetchDataMysql(context,"dealer_info", str[0], str[1]);
            objfetchmysql3 = new FetchDataMysql(context,"product_info", str[0], str[1]);
            objfetchmysql4 = new FetchDataMysql(context,"dealer_gps_location", str[0], str[1]);
            objfetchmysql5 = new FetchDataMysql(context,"dealer_pendings", str[0], str[1]);
            objfetchmysql6 = new FetchDataMysql(context,"batchwise_stock", str[0], str[1]);
            objfetchmysql7 = new FetchDataMysql(context,"discount", str[0], str[1]);
            objfetchmysql8 = new FetchDataMysql(context,"bonus", str[0], str[1]);
        }
        else
        {
            objfetchmysql1= new FetchDataMysql(context, "area_info");
            objfetchmysql2 = new FetchDataMysql(context,"dealer_info");
            objfetchmysql3 = new FetchDataMysql(context,"product_info");
            objfetchmysql4 = new FetchDataMysql(context,"dealer_gps_location");
            objfetchmysql5 = new FetchDataMysql(context,"dealer_pendings");
            objfetchmysql6 = new FetchDataMysql(context,"batchwise_stock");
            objfetchmysql7 = new FetchDataMysql(context,"discount");
            objfetchmysql8 = new FetchDataMysql(context,"bonus");
        }

        objfetchmysql1.setCallBack(new FetchDataMysql.CallBack() {
            public void onResult(String value) {
                areaInfo = objfetchmysql1.getAreaInfo();
                if(areaInfo != null)
                {
                    completeInsertion[0] = true;
                    for(int i=0; i<areaInfo.length; i++)
                    {
                        if(areaInfo[4].equals("Update"))
                        {
                            areaResponse[0] = updateArea(areaInfo[i][0], areaInfo[i][1], areaInfo[i][2], areaInfo[i][3]);
                        }
                        else if(areaInfo[4].equals("Delete"))
                        {
                            areaResponse[0] = deleteArea(areaInfo[i][0]);
                        }
                        else
                        {
                            areaResponse[0] = insertarea(areaInfo[i][0], areaInfo[i][1], areaInfo[i][2], areaInfo[i][3]);
                        }
                    }
//                    Toast.makeText(context, "Synced From Server", Toast.LENGTH_LONG).show();
                }
                else
                {
//                    completeInsertion[0] = false;
//                    Toast.makeText(context, "Unable to Connect to Server", Toast.LENGTH_LONG).show();
                }
            }
        });

        objfetchmysql2.setCallBack(new FetchDataMysql.CallBack() {
            @Override
            public void onResult(String value) {
                dealerInfo = objfetchmysql2.getDealerInfo();
                if(dealerInfo != null)
                {
                    completeInsertion[0] = true;
                    for(int i=0; i<dealerInfo.length; i++)
                    {
                        if(dealerInfo[10].equals("Update"))
                        {
                            dealerResponse[0] = updateDealer(dealerInfo[i][0], dealerInfo[i][1], dealerInfo[i][2], dealerInfo[i][3], dealerInfo[i][4], dealerInfo[i][5], dealerInfo[i][6], dealerInfo[i][7], dealerInfo[i][8], dealerInfo[i][9]);
                        }
                        else if(dealerInfo[10].equals("Delete"))
                        {
                            dealerResponse[0] = deleteDealer(dealerInfo[i][0]);
                        }
                        else
                        {
                            dealerResponse[0] = insertDealer(dealerInfo[i][0], dealerInfo[i][1], dealerInfo[i][2], dealerInfo[i][3], dealerInfo[i][4], dealerInfo[i][5], dealerInfo[i][6], dealerInfo[i][7], dealerInfo[i][8], dealerInfo[i][9]);
                        }

                    }
                }

            }
        });


        objfetchmysql3.setCallBack(new FetchDataMysql.CallBack() {
            @Override
            public void onResult(String value) {
                productInfo = objfetchmysql3.getProductInfo();
                if(productInfo != null)
                {
                    completeInsertion[0] = true;
                    for(int i=0; i<productInfo.length; i++)
                    {
                        if(productInfo[5].equals("Update"))
                        {
                            productResponse[0] = updateProduct(productInfo[i][0], productInfo[i][1], productInfo[i][2], productInfo[i][3], productInfo[i][4]);
                        }
                        else if(productInfo[5].equals("Delete"))
                        {
                            productResponse[0] = deleteProduct(productInfo[i][0]);
                        }
                        else
                        {
                            productResponse[0] = insertProduct(productInfo[i][0], productInfo[i][1], productInfo[i][2], productInfo[i][3], productInfo[i][4]);
                        }
                    }
                }
            }
        });

        objfetchmysql4.setCallBack(new FetchDataMysql.CallBack() {
            @Override
            public void onResult(String value) {
                dealerGPSData = objfetchmysql4.getDealerGPSData();
                if(dealerGPSData != null)
                {
                    completeInsertion[0] = true;
                    for(int i=0; i<dealerGPSData.length; i++)
                    {
                        dealerGPSResponse[0] = insertDealerGPS(dealerGPSData[i]);
                    }
                }
            }
        });

        objfetchmysql5.setCallBack(new FetchDataMysql.CallBack() {
            @Override
            public void onResult(String value) {
                dealersPendingPayments = objfetchmysql5.getDealerPendingPayments();
                if(dealersPendingPayments != null)
                {
                    completeInsertion[0] = true;
                    for(int i=0; i<dealersPendingPayments.length; i++)
                    {
                        dealerPendings[0] = insertDealerPending(dealersPendingPayments[i][0], dealersPendingPayments[i][1]);
                    }
                }
            }
        });

        objfetchmysql6.setCallBack(new FetchDataMysql.CallBack() {
            @Override
            public void onResult(String value) {
                batchwiseStock = objfetchmysql6.getBatchwiseStock();
                if(batchwiseStock != null)
                {
                    completeInsertion[0] = true;
                    for(int i=0; i<batchwiseStock.length; i++)
                    {
                        if(batchwiseStock[5].equals("Update"))
                        {
                            batchstock[0] = updateBatchWiseStock(batchwiseStock[i][0], batchwiseStock[i][1], batchwiseStock[i][2], batchwiseStock[i][3], batchwiseStock[i][4]);
                        }
                        else if(batchwiseStock[5].equals("Delete"))
                        {
                            batchstock[0] = deleteBatchWiseStock(batchwiseStock[i][0]);
                        }
                        else
                        {
                            batchstock[0] = insertBatchwiseStock(batchwiseStock[i][0], batchwiseStock[i][1], batchwiseStock[i][2], batchwiseStock[i][3], batchwiseStock[i][4]);
                        }
                    }
                }
            }
        });

        objfetchmysql7.setCallBack(new FetchDataMysql.CallBack() {
            @Override
            public void onResult(String value) {
                discountInfo = objfetchmysql7.getDiscountInfo();
                if(discountInfo != null)
                {
                    completeInsertion[0] = true;
                    for(int i=0; i<discountInfo.length; i++)
                    {
                        if(discountInfo[8].equals("Update"))
                        {
                            discount[0] = updateDiscount(discountInfo[i][0], discountInfo[i][1], discountInfo[i][2], discountInfo[i][3], discountInfo[i][4], discountInfo[i][5], discountInfo[i][6], discountInfo[i][7]);
                        }
                        else if(discountInfo[8].equals("Delete"))
                        {
                            discount[0] = deleteDiscount(discountInfo[i][0]);
                        }
                        else
                        {
                            discount[0] = insertDiscountInfo(discountInfo[i][0], discountInfo[i][1], discountInfo[i][2], discountInfo[i][3], discountInfo[i][4], discountInfo[i][5], discountInfo[i][6], discountInfo[i][7]);
                        }
                    }
                }
            }
        });

        objfetchmysql8.setCallBack(new FetchDataMysql.CallBack() {
            @Override
            public void onResult(String value) {
                bonusInfo = objfetchmysql8.getBonusInfo();
                if(bonusInfo != null)
                {
                    completeInsertion[0] = true;
                    for(int i=0; i<bonusInfo.length; i++)
                    {
                        if(bonusInfo[8].equals("Update"))
                        {
                            bonus[0] = updateBonus(bonusInfo[i][0], bonusInfo[i][1], bonusInfo[i][2], bonusInfo[i][3], bonusInfo[i][4], bonusInfo[i][5], bonusInfo[i][6], bonusInfo[i][7]);
                        }
                        else if(bonusInfo[8].equals("Delete"))
                        {
                            bonus[0] = deleteBonus(bonusInfo[i][0]);
                        }
                        else
                        {
                            bonus[0] = insertBonusInfo(bonusInfo[i][0], bonusInfo[i][1], bonusInfo[i][2], bonusInfo[i][3], bonusInfo[i][4], bonusInfo[i][5], bonusInfo[i][6], bonusInfo[i][7]);
                        }
                    }
                }
            }
        });

        refreshInvoicesData();

        if(str != null)
        {
            updateDataBaseSync(GlobalVariables.getStDate(), GlobalVariables.getStTime());
        }
        else
        {
            insertDataBaseSync("", GlobalVariables.getStDate(), GlobalVariables.getStTime());
        }

        return completeInsertion[0];
    }

    public void refreshInvoicesData()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(DatabaseSQLite.INVOICES_TABLE, null, null);

        final boolean[] invoiceResponse = {false};

        final FetchDataMysql objfetchmysql= new FetchDataMysql(context, "invoices_data");
        objfetchmysql.setCallBack(new FetchDataMysql.CallBack() {

            public void onResult(String value) {
                invoicesData = objfetchmysql.getInvoicesData();
                if(invoicesData != null)
                {
                    for(int i=0; i<invoicesData.length; i++)
                    {
                        invoiceResponse[0] = insertInvoice(invoicesData[i][0], invoicesData[i][1], invoicesData[i][2], invoicesData[i][3], invoicesData[i][4], invoicesData[i][5], invoicesData[i][6]);
                    }
//                    Toast.makeText(context, "Synced Invoices From Server", Toast.LENGTH_LONG).show();
                }
                else
                {
                    Toast.makeText(context, "Unable to Sync Invoices to Server", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public boolean updateArea(String areaId, String cityId, String areaName, String areaStatus)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseSQLite.AREA_COL_2, cityId);
        contentValues.put(DatabaseSQLite.AREA_COL_3, areaName);
        contentValues.put(DatabaseSQLite.AREA_COL_4, areaStatus);
        int result = db.update(DatabaseSQLite.AREA_INFO_TABLE, contentValues, "area_id="+areaId, null);
        if(result <= 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean updateDealer(String dealerId, String areaId, String dealerName, String dealerContact, String dealerAddress, String dealerType, String dealerCnic,  String dealerLicNum, String dealerLicExp, String dealerStatus)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseSQLite.DEALER_COL_2, areaId);
        contentValues.put(DatabaseSQLite.DEALER_COL_3, dealerName);
        contentValues.put(DatabaseSQLite.DEALER_COL_4, dealerContact);
        contentValues.put(DatabaseSQLite.DEALER_COL_5, dealerAddress);
        contentValues.put(DatabaseSQLite.DEALER_COL_6, dealerType);
        contentValues.put(DatabaseSQLite.DEALER_COL_7, dealerCnic);
        contentValues.put(DatabaseSQLite.DEALER_COL_8, dealerLicNum);
        contentValues.put(DatabaseSQLite.DEALER_COL_9, dealerLicExp);
        contentValues.put(DatabaseSQLite.DEALER_COL_10, dealerStatus);
        int result = db.update(DatabaseSQLite.DEALER_INFO_TABLE, contentValues, "dealer_id="+dealerId, null);
        if(result <= 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean updateProduct(String productId, String productName, String finalPrice, String productStatus, String companyName)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseSQLite.PRODUCT_COL_2, productName);
        contentValues.put(DatabaseSQLite.PRODUCT_COL_3, finalPrice);
        contentValues.put(DatabaseSQLite.PRODUCT_COL_4, productStatus);
        contentValues.put(DatabaseSQLite.PRODUCT_COL_5, companyName);
        int result = db.update(DatabaseSQLite.PRODUCT_INFO_TABLE, contentValues, "product_id="+productId, null);
        if(result <= 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean updateInvoice(String invoiceId, String dealerId, String productIds, String submissionQty, String submissionUnit, String invoicePrice, String orderStatus)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseSQLite.INVOICES_COL_2, dealerId);
        contentValues.put(DatabaseSQLite.INVOICES_COL_3, productIds);
        contentValues.put(DatabaseSQLite.INVOICES_COL_4, submissionQty);
        contentValues.put(DatabaseSQLite.INVOICES_COL_5, submissionUnit);
        contentValues.put(DatabaseSQLite.INVOICES_COL_6, invoicePrice);
        contentValues.put(DatabaseSQLite.INVOICES_COL_7, orderStatus);
        int result = db.update(DatabaseSQLite.INVOICES_TABLE, contentValues, "invoices_id="+invoiceId, null);
        if(result <= 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean updateBatchWiseStock(String batchStockId, String productId, String batchNo, String quantity, String batchExpiry)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseSQLite.BATCHSTOCK_COL_2, productId);
        contentValues.put(DatabaseSQLite.BATCHSTOCK_COL_3, batchNo);
        contentValues.put(DatabaseSQLite.BATCHSTOCK_COL_4, quantity);
        contentValues.put(DatabaseSQLite.BATCHSTOCK_COL_5, batchExpiry);
        int result = db.update(DatabaseSQLite.BATCHWISE_STOCK_TABLE, contentValues, "batchstock_id="+batchStockId, null);
        if(result <= 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean updateDiscount(String approvalId, String dealerId, String companyName, String productId, String saleAmount, String discount, String startDate, String endDate)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseSQLite.DISCOUNT_COL_2, dealerId);
        contentValues.put(DatabaseSQLite.DISCOUNT_COL_3, companyName);
        contentValues.put(DatabaseSQLite.DISCOUNT_COL_4, productId);
        contentValues.put(DatabaseSQLite.DISCOUNT_COL_5, saleAmount);
        contentValues.put(DatabaseSQLite.DISCOUNT_COL_6, discount);
        contentValues.put(DatabaseSQLite.DISCOUNT_COL_7, startDate);
        contentValues.put(DatabaseSQLite.DISCOUNT_COL_8, endDate);
        int result = db.update(DatabaseSQLite.DISCOUNT_TABLE, contentValues, "approval_id="+approvalId, null);
        if(result <= 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean updateBonus(String approvalId, String dealerId, String companyName, String productId, String quantity, String bonus, String startDate, String endDate)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseSQLite.BONUS_COL_2, dealerId);
        contentValues.put(DatabaseSQLite.BONUS_COL_3, companyName);
        contentValues.put(DatabaseSQLite.BONUS_COL_4, productId);
        contentValues.put(DatabaseSQLite.BONUS_COL_5, quantity);
        contentValues.put(DatabaseSQLite.BONUS_COL_6, bonus);
        contentValues.put(DatabaseSQLite.BONUS_COL_7, startDate);
        contentValues.put(DatabaseSQLite.BONUS_COL_8, endDate);
        int result = db.update(DatabaseSQLite.BONUS_TABLE, contentValues, "approval_id="+approvalId, null);
        if(result <= 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean updateDataBaseSync(String updateDate, String updateTime)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseSQLite.SYNCTRACK_COL_3, updateDate);
        contentValues.put(DatabaseSQLite.SYNCTRACK_COL_4, updateTime);
        int result = db.update(DatabaseSQLite.SYNC_TRACKING_TABLE, contentValues, "1", null);
        if(result <= 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean deleteArea(String id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        int result = db.delete(DatabaseSQLite.AREA_INFO_TABLE, "area_id="+id, null);
        if(result <= 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean deleteDealer(String id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        int result = db.delete(DatabaseSQLite.DEALER_INFO_TABLE, "dealer_id="+id, null);
        if(result <= 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean deleteProduct(String id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        int result = db.delete(DatabaseSQLite.PRODUCT_INFO_TABLE, "product_id="+id, null);
        if(result <= 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean deleteInvoice(String id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        int result = db.delete(DatabaseSQLite.INVOICES_TABLE, "invoices_id="+id, null);
        if(result <= 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean deleteBatchWiseStock(String id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        int result = db.delete(DatabaseSQLite.BATCHWISE_STOCK_TABLE, "batchstock_id="+id, null);
        if(result <= 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean deleteDiscount(String id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        int result = db.delete(DatabaseSQLite.DISCOUNT_TABLE, "approval_id="+id, null);
        if(result <= 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public boolean deleteBonus(String id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        int result = db.delete(DatabaseSQLite.BONUS_TABLE, "approval_id="+id, null);
        if(result <= 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}
