package com.example.younastraders;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class PendingOrderDetail extends AppCompatActivity {

    int itemsCount;
    int maxExtraProducts = 50;
    float orderPrice = 0;
    String bonus = "";
    float discount = 0;
    float finalPrice = 0;
    Button btnSubmitOrder;
    Button btnSaveOrder;
    SearchableSpinner edDealerID;

    SearchableSpinner[] orgProducts;
    EditText[] orgqQuantity;
    Spinner[] orgUnit;

    LinearLayout[] subLayout;
    LinearLayout[] subLayoutDetail;
    SearchableSpinner[] extraProducts;
    EditText[] extraQuantity;
    EditText[] extraDiscount;
    Spinner[] extraUnits;
    Button[] extraOpBtn;
    ProgressBar edProgress;
    SendData objSendData;
    Button btnAdd;
    Button btnEdit;
    EditText edOrderPrice;
    EditText edDiscount;
    EditText edFinalPrice;

    String strOrderPrice;
    String strDiscount;
    String strFinalPrice;

    ArrayAdapter<String> dealerAdapter;
    ArrayAdapter<String> productAdapter;
    ArrayAdapter<String> unitAdapter;
    LinearLayout lv;

    DatabaseSQLite objDatabaseSqlite;

    public String[] dealersId;
    public String[] dealersName;
    public String[] dealersAddress;
    public String[] productsIds;
    public String[] productsName;
    public float[] productsPrice;

    String orderID;
    String orderStatus;

    String stDate;
    String stTime;

    String getDealerName;
//    String getDealerId;
    String getProductId;
    String getQuantity;
    String getUnit;
    String getOrderPrice;
    String getBonus;
    String getDiscount;
    String getFinalPrice;
    String getDate;
    String getTime;


    ArrayList<String> allProductsIds;
    ArrayList<String> allProductsNames;
    ArrayList<String> allProductsPrices;

    String lat;
    String lon;
    String orderPlaceArea;

    String send_dealerId;
    String send_productIds = "";
    String send_quantities = "";
    String send_discount = "";
    String send_unit = "";
    String send_salesmanId = "1";

    boolean priceCal = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pending_order_detail);
        edOrderPrice = findViewById(R.id.edorderprice);
        edDiscount = findViewById(R.id.eddiscount);
        edDiscount.setText("0.0");
        edFinalPrice = findViewById(R.id.edfinalprice);

        orderID = getIntent().getStringExtra("ORDER_ID");
        orderStatus = getIntent().getStringExtra("STATUS");
        objDatabaseSqlite = new DatabaseSQLite(this);

        Date objDate = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        this.stDate = df.format(objDate);
        this.stTime = sdf.format(objDate);
        DealersAndProductsInfo objdealandproinfo = new DealersAndProductsInfo(this);
        dealersId = objdealandproinfo.getDealersId();
        dealersName = objdealandproinfo.getDealersName();
        dealersAddress = objdealandproinfo.getDealersAddress();
        productsIds = objdealandproinfo.getProductsIds();
        productsName = objdealandproinfo.getProductsName();
        productsPrice = objdealandproinfo.getProductsPrice();
        orgProducts = new SearchableSpinner[4];
        orgqQuantity = new EditText[4];
        orgUnit = new Spinner[4];

        subLayout = new LinearLayout[maxExtraProducts];
        subLayoutDetail = new LinearLayout[maxExtraProducts];
        extraProducts = new SearchableSpinner[maxExtraProducts];
        extraQuantity = new EditText[maxExtraProducts];
        extraDiscount = new EditText[maxExtraProducts];
        extraUnits = new Spinner[maxExtraProducts];
        extraOpBtn = new Button[maxExtraProducts];

        objSendData = new SendData(this);
        itemsCount = 0;
        btnAdd = findViewById(R.id.button_add);
        btnEdit = findViewById(R.id.btn_edit);
        edDealerID = findViewById(R.id.ed_dealerName);
        lv = findViewById(R.id.linear_layout);

        dealerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, dealersName);
        edDealerID.setAdapter(dealerAdapter);

        productAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, productsName);

        String[] unitItems = new String[]{"Packets", "Box"};
        unitAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, unitItems);

        btnSaveOrder = findViewById(R.id.btn_save_pending);
        btnSubmitOrder = findViewById(R.id.btn_send_pending);


        Cursor res = objDatabaseSqlite.getProductInfo();
        if(res.getCount() == 0)
        {

        }
        else
        {
            allProductsIds = new ArrayList<String>(res.getCount());
            allProductsNames = new ArrayList<String>(res.getCount());
            allProductsPrices = new ArrayList<String>(res.getCount());
            while(res.moveToNext())
            {
                allProductsIds.add(res.getString(0));
                allProductsNames.add(res.getString(1));
                allProductsPrices.add(res.getString(2));
            }
        }


        Cursor res1 = objDatabaseSqlite.getOrderInfo(orderID);
        if(res1.getCount() == 0)
        {

        }
        else
        {
            float totalOrderPrice = 0.0f;
            float totalDiscount = 0.0f;
            float totalFinalPrice = 0.0f;
            while(res1.moveToNext())
            {
                getDealerName = res1.getString(16)+" ("+res1.getString(17)+")";
                int dealerPos = dealerAdapter.getPosition(getDealerName);
                edDealerID.setSelection(dealerPos);
                edDealerID.setEnabled(false);
//                getDealerId = res1.getString(1);
                getProductId = res1.getString(2);
                getQuantity = res1.getString(3);
                getUnit = res1.getString(4);
                getOrderPrice = res1.getString(5);
                getBonus = res1.getString(6);
                getDiscount = res1.getString(7);
                getFinalPrice = res1.getString(8);
                lat = res1.getString(9);
                lon = res1.getString(10);
                orderPlaceArea = res1.getString(11);
                getDate = res1.getString(12);
                getTime = res1.getString(13);
                String[] productArr = getProductId.split("_-_");
                String[] quantityArr = getQuantity.split("_-_");
                String[] unitArr = getUnit.split("_-_");
                String[] orderPriceArr = getOrderPrice.split("_-_");
                String[] discountArr = getDiscount.split("_-_");
                String[] finalPriceArr = getFinalPrice.split("_-_");
                for(int j=0; j<productArr.length; j++)
                {
                    int proIndex = allProductsIds.indexOf(productArr[j]);
                    String proName = allProductsNames.get(proIndex);
                    totalOrderPrice = totalFinalPrice + Float.parseFloat(orderPriceArr[j]);
                    totalDiscount = totalDiscount + Float.parseFloat(discountArr[j]);
                    totalFinalPrice = totalFinalPrice + Float.parseFloat(finalPriceArr[j]);
                    itemsCount++;
                    lv.addView(createLinearLayout(proName, quantityArr[j], unitArr[j]));
                }
                addListenerToButtons();
            }
            edOrderPrice.setText(String.valueOf(totalOrderPrice));
            edDiscount.setText(String.valueOf(totalDiscount));
            edFinalPrice.setText(String.valueOf(totalFinalPrice));
        }

        if(orderStatus.equals("Sent"))
        {
            btnAdd.setEnabled(false);
            btnEdit.setEnabled(false);
            btnSaveOrder.setEnabled(false);
            btnSubmitOrder.setEnabled(false);
        }

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemsCount++;
                lv.addView(createLinearLayout("Select Item", "1", "Packets"));
                addListenerToButtons();
            }
        });

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = btnEdit.getText().toString();
                if(text.equals("Edit"))
                {
                    btnEdit.setText("Cancel");
                    edDealerID.setEnabled(true);
                    btnAdd.setEnabled(true);
                    for(int i=0; i<itemsCount; i++)
                    {
                        extraProducts[i].setEnabled(true);
                        extraQuantity[i].setEnabled(true);
                        extraUnits[i].setEnabled(true);
                        extraOpBtn[i].setEnabled(true);
                        priceCal = true;
                    }
                }
                else
                {
                    finish();
                }

            }
        });


        edProgress = findViewById(R.id.ed_progress);

        btnSaveOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(compileOrder())
                {
                    String returnStatus = objSendData.updateBooking(orderID, send_dealerId, send_productIds, send_quantities, send_unit, strOrderPrice, bonus, strDiscount, strFinalPrice, lat, lon, orderPlaceArea, getDate, getTime, send_salesmanId);
                    Intent intent = new Intent(PendingOrderDetail.this, Home.class);
                    startActivity(intent);
                    finish();
                }
            }
        });

        btnSubmitOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(compileOrder())
                {
                    objSendData.sendSavedBooking(orderID, send_dealerId, send_productIds, send_quantities, send_unit, strOrderPrice, bonus, strDiscount, strFinalPrice, lat, lon, orderPlaceArea, getDate, getTime, send_salesmanId, "Pending");
                    Intent intent = new Intent(PendingOrderDetail.this, Home.class);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    public boolean compileOrder()
    {
        edProgress.setVisibility(View.VISIBLE);
        boolean check = false;
        strOrderPrice = "";
        strDiscount = "";
        strFinalPrice = "";
        String stQan;
        if(edDealerID.getSelectedItemPosition() > 0)
        {
            send_dealerId = dealersId[edDealerID.getSelectedItemPosition()];
        }
        else
        {
            Toast.makeText(PendingOrderDetail.this, "Please Select Dealer", Toast.LENGTH_LONG).show();
            return false;
        }
        for(int j=0; j<itemsCount; j++)
        {
            if(!(subLayout[j].getVisibility() == View.GONE) && extraProducts[j].getSelectedItemPosition() > 0)
            {
                stQan = extraQuantity[j].getText().toString();
                if(stQan.equals(""))
                {
                    stQan = "0";
                }
                if(check)
                {
                    send_productIds = send_productIds+"_-_"+productsIds[extraProducts[j].getSelectedItemPosition()];
                    send_quantities = send_quantities+"_-_"+extraQuantity[j].getText().toString();
                    send_discount = send_discount+"_-_"+extraDiscount[j].getText().toString();
                    send_unit = send_unit+"_-_Packets";
                    strOrderPrice = strOrderPrice+"_-_"+String.valueOf(productsPrice[extraProducts[j].getSelectedItemPosition()] * Integer.parseInt(stQan));
                    bonus = bonus+"_-_0";
                    strDiscount = strDiscount+"_-_0.0";
                    strFinalPrice = strFinalPrice+"_-_"+String.valueOf(productsPrice[extraProducts[j].getSelectedItemPosition()] * Integer.parseInt(stQan));
                }
                else
                {
                    send_productIds = productsIds[extraProducts[j].getSelectedItemPosition()];
                    send_quantities = extraQuantity[j].getText().toString();
                    send_discount = extraDiscount[j].getText().toString();
                    send_unit = "Packets";
                    strOrderPrice = String.valueOf(productsPrice[extraProducts[j].getSelectedItemPosition()] * Integer.parseInt(stQan));
                    bonus = "0";
                    strDiscount = "0.0";
                    strFinalPrice = String.valueOf(productsPrice[extraProducts[j].getSelectedItemPosition()] * Integer.parseInt(stQan));
                    check = true;
                }

            }
        }
        orderPrice = Float.parseFloat(edOrderPrice.getText().toString());
        discount = Float.parseFloat(edDiscount.getText().toString());
        finalPrice = Float.parseFloat(edFinalPrice.getText().toString());
        edProgress = findViewById(R.id.ed_progress);
        edProgress.setVisibility(View.VISIBLE);
        return true;
    }

    public SearchableSpinner createProductSpinner(String text) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, Math.round(getResources().getDimension(R.dimen.control_height)));
        extraProducts[itemsCount-1] = new SearchableSpinner(PendingOrderDetail.this);
        extraProducts[itemsCount-1].setLayoutParams(lparams);
        extraProducts[itemsCount-1].setAdapter(productAdapter);
        int pos = productAdapter.getPosition(text);
        extraProducts[itemsCount-1].setSelection(pos);
        if(!priceCal)
        {
            extraProducts[itemsCount-1].setEnabled(false);
        }
        return extraProducts[itemsCount-1];
    }
    public EditText createQuantityEditText(String text) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(Math.round(getResources().getDimension(R.dimen.quantity_width)), Math.round(getResources().getDimension(R.dimen.control_height)));
        extraQuantity[itemsCount-1] = new EditText(PendingOrderDetail.this);
        extraQuantity[itemsCount-1].setLayoutParams(lparams);
        extraQuantity[itemsCount-1].setMovementMethod(new ScrollingMovementMethod());
        extraQuantity[itemsCount-1].setText(text);
        extraQuantity[itemsCount-1].setInputType(InputType.TYPE_CLASS_NUMBER);
        extraQuantity[itemsCount-1].setFilters(new InputFilter[]{ new InputFilterMinMax("1", "100")});
        if(!priceCal)
        {
            extraQuantity[itemsCount-1].setEnabled(false);
        }
        return extraQuantity[itemsCount-1];
    }
    public EditText createDiscountEditText(String text) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(Math.round(getResources().getDimension(R.dimen.quantity_width)), Math.round(getResources().getDimension(R.dimen.control_height)));
        extraDiscount[itemsCount-1] = new EditText(PendingOrderDetail.this);
        extraDiscount[itemsCount-1].setLayoutParams(lparams);
        extraDiscount[itemsCount-1].setMovementMethod(new ScrollingMovementMethod());
        extraDiscount[itemsCount-1].setText(text);
        extraDiscount[itemsCount-1].setInputType(InputType.TYPE_CLASS_NUMBER);
        extraDiscount[itemsCount-1].setFilters(new InputFilter[]{ new InputFilterMinMax("1", "100")});
        extraDiscount[itemsCount-1].setEnabled(false);
        return extraDiscount[itemsCount-1];
    }
    public Spinner createUnitSpinner(String text) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(Math.round(getResources().getDimension(R.dimen.unit_width)), Math.round(getResources().getDimension(R.dimen.control_height)));
        extraUnits[itemsCount-1] = new Spinner(PendingOrderDetail.this);
        extraUnits[itemsCount-1].setLayoutParams(lparams);
        extraUnits[itemsCount-1].setAdapter(unitAdapter);
        int pos = unitAdapter.getPosition(text);
        extraUnits[itemsCount-1].setSelection(pos);
        if(!priceCal)
        {
            extraUnits[itemsCount-1].setEnabled(false);
        }
        return extraUnits[itemsCount-1];
    }

    public Button createOpButton() {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        extraOpBtn[itemsCount-1] = new Button(PendingOrderDetail.this);
        extraOpBtn[itemsCount-1].setLayoutParams(lparams);
        extraOpBtn[itemsCount-1].setText("X");
        extraOpBtn[itemsCount-1].setTextColor(Color.RED);
        extraOpBtn[itemsCount-1].setBackgroundColor(Color.TRANSPARENT);
        if(!priceCal)
        {
            extraOpBtn[itemsCount-1].setEnabled(false);
        }
        return extraOpBtn[itemsCount-1];
    }

    public LinearLayout createDetailLinearLayout(String quantity, String unit) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        subLayoutDetail[itemsCount-1] = new LinearLayout(PendingOrderDetail.this);
        subLayoutDetail[itemsCount-1].setOrientation(LinearLayout.HORIZONTAL);
        subLayoutDetail[itemsCount-1].setLayoutParams(lparams);
        subLayoutDetail[itemsCount-1].addView(createQuantityEditText(quantity));
        subLayoutDetail[itemsCount-1].addView(createUnitSpinner(unit));
        subLayoutDetail[itemsCount-1].addView(createDiscountEditText("0.00"));
        subLayoutDetail[itemsCount-1].addView(createOpButton());
        return subLayoutDetail[itemsCount-1];
    }

    public LinearLayout createLinearLayout(String productName, String quantity, String unit) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        subLayout[itemsCount-1] = new LinearLayout(PendingOrderDetail.this);
        subLayout[itemsCount-1].setOrientation(LinearLayout.VERTICAL);
        subLayout[itemsCount-1].setLayoutParams(lparams);
        subLayout[itemsCount-1].addView(createProductSpinner(productName));
        subLayout[itemsCount-1].addView(createDetailLinearLayout(quantity, unit));
        return subLayout[itemsCount-1];
    }

    public void addListenerToButtons()
    {
        for(int i=0; i<itemsCount; i++)
        {
            final int finalI = i;
            extraOpBtn[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    subLayout[finalI].setVisibility(View.GONE);
                    calculatePrice();
                }
            });

            extraProducts[i].setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    calculatePrice();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            extraQuantity[i].setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean hasFocus) {
                    if (!hasFocus) {
                        if(extraQuantity[finalI].getText().toString().equals(""))
                        {
                            extraQuantity[finalI].setText("1");
                        }
                        calculatePrice();
                    }
                }
            });

            extraQuantity[i].addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    calculatePrice();
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    calculatePrice();
                }

                @Override
                public void afterTextChanged(Editable s) {
                    calculatePrice();
                }
            });
        }
        calculatePrice();
    }

    public void calculatePrice()
    {
        if(priceCal)
        {
            orderPrice = 0f;
            finalPrice = 0f;
            strOrderPrice = "";
            strDiscount = "";
            strFinalPrice = "";
            boolean iter = false;
            for(int j=0; j<itemsCount; j++)
            {
                if(!(subLayout[j].getVisibility() == View.GONE) && extraProducts[j].getSelectedItemPosition() > 0)
                {
                    String stQan = extraQuantity[j].getText().toString();
                    if(stQan.equals(""))
                    {
                        stQan = "0";
                    }
                    if(iter)
                    {
                        strOrderPrice = strOrderPrice+"_-_"+String.valueOf(productsPrice[extraProducts[j].getSelectedItemPosition()] * Integer.parseInt(stQan));
                        bonus = bonus+"_-_0";
                        strDiscount = strDiscount+"_-_0.0";
                        strFinalPrice = strFinalPrice+"_-_"+String.valueOf(productsPrice[extraProducts[j].getSelectedItemPosition()] * Integer.parseInt(stQan));
                    }
                    else
                    {
                        iter = true;
                        strOrderPrice = String.valueOf(productsPrice[extraProducts[j].getSelectedItemPosition()] * Integer.parseInt(stQan));
                        bonus = "0";
                        strDiscount = "0.0";
                        strFinalPrice = String.valueOf(productsPrice[extraProducts[j].getSelectedItemPosition()] * Integer.parseInt(stQan));
                    }
                    orderPrice = orderPrice + (productsPrice[extraProducts[j].getSelectedItemPosition()] * Integer.parseInt(stQan));
                }
            }
            discount = Float.valueOf(edDiscount.getText().toString());
            finalPrice = orderPrice - ((orderPrice/100)*discount);
            edOrderPrice.setText(String.valueOf(orderPrice));
            edFinalPrice.setText(String.valueOf(finalPrice));
        }

    }
}
