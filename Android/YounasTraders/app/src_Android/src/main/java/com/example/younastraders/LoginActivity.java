package com.example.younastraders;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LoginActivity";
    private static final int REQUEST_SIGNUP = 0;
    TextView newSignUp;
    DatabaseSQLite objsqlite;
    TextView txtUserName;
    TextView txtUserId;
    TextView txtUserStatus;
    Button btnLogin;
    Button btnReSend;
    SendData objSendData;
    ProgressBar progressReg;

    String stUserName;
    String stSoftwareId;
    String stUserId;
    String stUserPass;
    String stUserPower;
    String stRegDate;
    String stUserStatus;
    String autoSend;

    EditText edGetUserId;
    EditText edGetUserPass;
    String getUserId;
    String getUserPass;

    boolean removeAccount = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        objsqlite = new DatabaseSQLite(this);
        txtUserName = findViewById(R.id.txt_username);
        txtUserId = findViewById(R.id.txt_userid);
        txtUserStatus = findViewById(R.id.txt_userstatus);
        newSignUp = findViewById(R.id.new_sign_up);
        btnReSend = findViewById(R.id.btnReSend);
        progressReg = findViewById(R.id.progressReg);
        btnLogin = findViewById(R.id.btn_login);
        edGetUserId = findViewById(R.id.ed_userid);
        edGetUserPass = findViewById(R.id.ed_userpass);
        objSendData = new SendData(this);
        viewReg();
        newSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(removeAccount)
                {
                    showMessage(LoginActivity.this, "Info", "Remove Account", true);
                }
                else
                {
                    Intent intent = new Intent(LoginActivity.this, SignupActivity.class);
                    startActivity(intent);
                    finish();
                }

            }
        });

        btnReSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressReg.setVisibility(View.VISIBLE);
                String responseStatus = objSendData.reSendRegisterUser(stUserName, stSoftwareId, stUserId, stUserPass, stUserPower, stRegDate);
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(stUserStatus.equals("Approved"))
                {
                    getUserId = edGetUserId.getText().toString();
                    getUserPass = edGetUserPass.getText().toString();
                    if(stUserId.equals(getUserId) && stUserPass.equals(getUserPass))
                    {
                        GlobalVariables.userId = stUserId;
                        GlobalVariables.autoSend = autoSend;
                        GlobalVariables objGlobal = new GlobalVariables(LoginActivity.this);
                        objGlobal.getPending();

                        DatabaseSQLite objsqlite = new DatabaseSQLite(LoginActivity.this);
                        Cursor res = objsqlite.getLastSynced();

                        if(res.getCount() == 0)
                        {
                            Intent intent = new Intent(LoginActivity.this, SyncProgress.class);
                            startActivity(intent);
                            finish();
                        }
                        else
                        {
                            Intent intent = new Intent(LoginActivity.this, Home.class);
                            startActivity(intent);
                            finish();
                        }
                    }
                    else
                    {
                        Toast.makeText(LoginActivity.this, "Incorrect User id or Password", Toast.LENGTH_LONG).show();
                    }
                }
                else
                {
                    Toast.makeText(LoginActivity.this, "Account is Not Registered", Toast.LENGTH_LONG).show();
                }
            }
        });

//        edGetUserId.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View view, boolean hasFocus) {
//                if (hasFocus) {
//                    findViewById(R.id.txt_regheader).setVisibility(View.GONE);
//                    findViewById(R.id.txt_username_fix).setVisibility(View.GONE);
//                    findViewById(R.id.txt_userid_fix).setVisibility(View.GONE);
//                    findViewById(R.id.txt_userstatus_fix).setVisibility(View.GONE);
//                    findViewById(R.id.txt_username).setVisibility(View.GONE);
//                    findViewById(R.id.txt_userid).setVisibility(View.GONE);
//                    findViewById(R.id.txt_userstatus).setVisibility(View.GONE);
//                    btnReSend.setVisibility(View.GONE);
//                }
//            }
//        });

//        edGetUserId.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//                Toast.makeText(LoginActivity.this, "Before Text Changed", Toast.LENGTH_LONG).show();
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                Toast.makeText(LoginActivity.this, "On Text Changed", Toast.LENGTH_LONG).show();
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                Toast.makeText(LoginActivity.this, "After Text Changed", Toast.LENGTH_LONG).show();
//            }
//        });

//        edGetUserPass.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View view, boolean hasFocus) {
//                if (hasFocus) {
//                    findViewById(R.id.txt_regheader).setVisibility(View.GONE);
//                    findViewById(R.id.txt_username_fix).setVisibility(View.GONE);
//                    findViewById(R.id.txt_userid_fix).setVisibility(View.GONE);
//                    findViewById(R.id.txt_userstatus_fix).setVisibility(View.GONE);
//                    findViewById(R.id.txt_username).setVisibility(View.GONE);
//                    findViewById(R.id.txt_userid).setVisibility(View.GONE);
//                    findViewById(R.id.txt_userstatus).setVisibility(View.GONE);
//                    btnReSend.setVisibility(View.GONE);
//                }
//            }
//        });

    }

    public void viewReg()
    {
        Cursor res = objsqlite.getRegistration();

        if(res.getCount() == 0)
        {
            removeAccount = false;
            findViewById(R.id.txt_regheader).setVisibility(View.GONE);
            findViewById(R.id.txt_username_fix).setVisibility(View.GONE);
            findViewById(R.id.txt_userid_fix).setVisibility(View.GONE);
            findViewById(R.id.txt_userstatus_fix).setVisibility(View.GONE);
            stUserStatus = "Empty";
            btnReSend.setVisibility(View.GONE);
        }
        else
        {
            newSignUp.setText("Remove Account and Sign Up");
            removeAccount = true;
            res.moveToNext();
            stUserName = res.getString(1);
            stSoftwareId = res.getString(2);
            stUserId = res.getString(3);
            stUserPass = res.getString(4);
            stUserPower = res.getString(5);
            stRegDate = res.getString(6);
            autoSend = res.getString(7);
            stUserStatus = res.getString(8);
            txtUserName.setText(stUserName);
            txtUserId.setText(stUserId);
            txtUserStatus.setText(stUserStatus);
            if(stUserStatus.equals("Pending"))
            {
                btnReSend.setVisibility(View.GONE);
                objSendData.checkRegistrationsStatus(stUserId);
            }
            else if (stUserStatus.equals("Approved"))
            {
                btnReSend.setVisibility(View.GONE);
            }
        }
    }

    public static void setKeyboardVisibilityListener(final Activity activity, final KeyboardVisibilityListener keyboardVisibilityListener) {
        final View contentView = activity.findViewById(android.R.id.content);
        contentView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            private int mPreviousHeight;

            @Override
            public void onGlobalLayout() {
                int newHeight = contentView.getHeight();
                if (mPreviousHeight != 0) {
                    if (mPreviousHeight > newHeight) {
                        Toast.makeText(activity, "Keyboard Shown", Toast.LENGTH_LONG).show();
                        keyboardVisibilityListener.onKeyboardVisibilityChanged(true);
                    } else if (mPreviousHeight < newHeight) {
                        Toast.makeText(activity, "Keyboard Not Shown", Toast.LENGTH_LONG).show();
                        keyboardVisibilityListener.onKeyboardVisibilityChanged(false);
                    } else {
                        Toast.makeText(activity, "No Change", Toast.LENGTH_LONG).show();
                    }
                }
                mPreviousHeight = newHeight;
            }
        });
    }

    public interface KeyboardVisibilityListener {
        void onKeyboardVisibilityChanged(boolean keyboardVisible);
    }

    public void showMessage(Context ctx, String title, String message, boolean YesNo)
    {
        AlertDialog.Builder objBuilder = new AlertDialog.Builder(ctx);

        if(YesNo)
        {
            objBuilder.setCancelable(false);

            objBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    //  Action for 'YES' Button
                    DatabaseSQLite objDatabaseSQLite = new DatabaseSQLite(LoginActivity.this);
                    objDatabaseSQLite.onUpgrade(objDatabaseSQLite.db, 0, 0);
                    Intent intent = new Intent(LoginActivity.this, SignupActivity.class);
                    startActivity(intent);
                    finish();
                }
            });
            objBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    //  Action for 'NO' Button
                    dialog.cancel();
                }
            });
        }
        else
        {
            objBuilder.setCancelable(true);
        }


        objBuilder.setTitle(title);
        objBuilder.setMessage(message);
        objBuilder.show();
    }
}


