package com.example.younastraders.ui.dealerpin;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.younastraders.GetData;
import com.example.younastraders.GlobalVariables;
import com.example.younastraders.R;
import com.example.younastraders.SendData;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

public class DealerPinFragment extends AppCompatActivity {
    public String[] dealersId;
    public String[] dealersName;
    ArrayAdapter<String> dealerAdapter;
    SearchableSpinner edDealerID;
    Button btnSendLoc;
    public static ProgressBar edProgressDealerPin;
    SendData objSendData;
    String send_dealerId;

    private DealerPinViewModel dealerpinViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_dealerpin);
//        final TextView textView = findViewById(R.id.text_send);
//        dealerpinViewModel.getText().observe(this, new Observer<String>() {
//            @Override
//            public void onChanged(@Nullable String s) {
//                textView.setText(s);
//            }
//        });

        GetData objGetData = new GetData(this, "Dealer Pin");
        dealersId = objGetData.getDealersId();
        dealersName = objGetData.getDealersName();
//
        edDealerID = findViewById(R.id.ed_dealerName);
        btnSendLoc = findViewById(R.id.btn_send_dealerpin);
        edProgressDealerPin = findViewById(R.id.ed_progress);
        objSendData = new SendData(this);
//
        dealerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, dealersName);
        edDealerID.setAdapter(dealerAdapter);


        btnSendLoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edDealerID.getSelectedItemPosition() != 0)
                {
                    send_dealerId = dealersId[edDealerID.getSelectedItemPosition()];
                    objSendData.sendDealerPin(send_dealerId, GlobalVariables.getLatitude(), GlobalVariables.getLongitude(), GlobalVariables.getLocationNameFull(), GlobalVariables.getStDate(), GlobalVariables.getStTime(), GlobalVariables.getUserId());
                }
                else
                {
                    Toast.makeText(DealerPinFragment.this, "Please Select a Dealer", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}