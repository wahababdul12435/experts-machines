package com.example.younastraders;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.ListView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

public class BackgroundTask2 extends AsyncTask<String,Void,String>{

    private CallBack callBack;
    public ListView listView;

    public CallBack getCallBack() {
        return callBack;
    }

    public void setCallBack(CallBack callBack) {
        this.callBack = callBack;
    }

    private String response = "";
//    private ProgressDialog dialog;
    Context ctx;
    public static boolean inProgress;

    BackgroundTask2(Context ctx)
    {
        this.ctx = ctx;
    }

    @Override
    protected void onPreExecute() {
//        dialog = new ProgressDialog(ctx);
//        dialog.setMessage("Progress start");
//        dialog.show();
    }

    protected String doInBackground(String... params) {
//        String sendLocation = "http://10.0.2.2/GitKraken/distribution/experts-machines/PHP/YounasTraders/SendLocation.php?type=";

//         ------------------------------------- LAPTOP ID -----------------------------------------
//        String sendLocation = "http://192.168.43.176/GitKraken/distribution/experts-machines/PHP/YounasTraders/SendLocation.php?type=";

//        --------------------------------------- OFFICE SYSTEM ID -----------------------------------
//        String sendLocation = "http://192.168.10.2/GitKraken/distribution/experts-machines/PHP/YounasTraders/SendLocation.php?type=";

//        --------------------------------------- LIVE SERVER ID -----------------------------------
        String sendLocation = "https://tuberichy.com/ProfitFlow/SendLocation.php?softwareid="+GlobalVariables.softwareId+"&type=";

        String tableData = params[0];
        if(tableData.equals("resendtimestamplocation"))
        {
            String type = "TimeStamp";
            String userID = params[1];
            String latitude = params[2];
            String longitude = params[3];
            String locationName = params[4];
            String date = params[5];
            String time = params[6];
            sendLocation += type;
            try {
                URL objurl = new URL(sendLocation);
                HttpURLConnection objhttpurlconnection = (HttpURLConnection) objurl.openConnection();

                objhttpurlconnection.setConnectTimeout(2 * 1000);
                objhttpurlconnection.connect();
                if (objhttpurlconnection.getResponseCode() == 200)
                {
                    objhttpurlconnection.disconnect();
                    objhttpurlconnection = (HttpURLConnection) objurl.openConnection();
                    objhttpurlconnection.setRequestMethod("POST");
                    objhttpurlconnection.setDoOutput(true);
                    objhttpurlconnection.setDoInput(true);
                    OutputStream objos = objhttpurlconnection.getOutputStream();
                    BufferedWriter objbuffwrite = new BufferedWriter(new OutputStreamWriter(objos, "UTF-8"));

                    String data = URLEncoder.encode("user_id", "UTF-8")+"="+URLEncoder.encode(userID, "UTF-8")+"&"+
                            URLEncoder.encode("latitude", "UTF-8")+"="+URLEncoder.encode(latitude, "UTF-8")+"&"+
                            URLEncoder.encode("longitude", "UTF-8")+"="+URLEncoder.encode(longitude, "UTF-8")+"&"+
                            URLEncoder.encode("location_name", "UTF-8")+"="+URLEncoder.encode(locationName, "UTF-8")+"&"+
                            URLEncoder.encode("date", "UTF-8")+"="+URLEncoder.encode(date, "UTF-8")+"&"+
                            URLEncoder.encode("time", "UTF-8")+"="+URLEncoder.encode(time, "UTF-8");
                    objbuffwrite.write(data);
                    objbuffwrite.flush();
                    objbuffwrite.close();
                    objos.close();

                    InputStream objis = objhttpurlconnection.getInputStream();
                    objis.close();
                    objhttpurlconnection.disconnect();
                    response = "Location Sent";

                }
                else
                {
                    response = "Location Not Sent";
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return response;
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
//        dialog.show();
    }

    @Override
    protected void onPostExecute(String value) {
//        dialog.dismiss();
        if(callBack != null)
        {
            inProgress = false;
            callBack.onResult(value);
        }
    }

    public interface CallBack{
        void onResult(String value);
    }
}
