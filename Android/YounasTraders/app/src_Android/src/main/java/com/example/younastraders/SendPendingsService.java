package com.example.younastraders;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

public class SendPendingsService extends Service {
    public static int tenCounts = 1;
    public int sendDelay = 10;

    final class ThreadClass implements Runnable
    {
        SendData objSendData;

        int serviceId;
        ThreadClass(int serviceId)
        {
            this.serviceId = serviceId;
        }

        @SuppressLint("MissingPermission")
        public void run()
        {
            synchronized (this)
            {
                while (tenCounts<=12)
                {
                    HandlerThread handlerThread = new HandlerThread("MyHandlerThread");
                    handlerThread.start();
                    Looper looper = handlerThread.getLooper();

                    try {
                        wait(sendDelay*1000);
                        objSendData = new SendData(SendPendingsService.this);
                        objSendData.sendAllPendingBookings();
                        objSendData.sendAllPendingDelivered();
                        objSendData.sendAllPendingCash();
                        objSendData.sendAllPendingReturn();
                        tenCounts++;
                    }
                    catch (InterruptedException e)
                    {
                        e.printStackTrace();
                    }
                }
                stopSelf(serviceId);
            }
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //        serviceStatus = true;
        Toast.makeText(this, "Send Pendings Service Started", Toast.LENGTH_LONG).show();
        Thread thread = new Thread(new ThreadClass(startId));
        thread.start();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        //        serviceStatus = false;
        Toast.makeText(this, "Send Pendings Service Stopped", Toast.LENGTH_LONG).show();
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
