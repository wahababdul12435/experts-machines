package com.example.younastraders;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;

import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements LocationListener {

    int extraCount;
    int maxExtraProducts = 50;
    float orderPrice = 0;
    String bonus = "0";
    String discountSingle = "0";
    String strBonus = "";
    float discount = 0;
    float finalPrice = 0;
    Button btnSubmitOrder;
    Button btnSaveOrder;
    SearchableSpinner edDealerID;

    SearchableSpinner[] orgProducts;
    EditText[] orgqQuantity;
    Spinner[] orgUnit;

    LinearLayout[] subLayout;
    LinearLayout[] subLayoutDetail;
    SearchableSpinner[] extraProducts;
    EditText[] extraQuantity;
    EditText[] extraBonus;
    EditText[] extraDiscount;
    Spinner[] extraUnits;
    Button[] extraOpBtn;
    public static ProgressBar edProgress;
    SendData objSendData;
    Button btnAdd;
    EditText edOrderPrice;
    EditText edDiscount;
    EditText edFinalPrice;

    String strOrderPrice;
    String strDiscount;
    String strFinalPrice;

    ArrayAdapter<String> dealerAdapter;
    ArrayAdapter<String> productAdapter;
    ArrayAdapter<String> unitAdapter;
    LinearLayout lv;

    DatabaseSQLite objDatabaseSqlite;

    public String[] dealersId;
    public String[] dealersName;
    public String[] dealersAddress;
    public String[] productsIds;
    public String[] productsName;
    public float[] productsPrice;
    public ArrayList<ArrayList<String>> bonusDetail;
    public ArrayList<ArrayList<String>> discountDetail;
    public ArrayList<ArrayList<String>> productsCompaniesDetail;

    String stDate;
    String stTime;

    String send_dealerId;
    String send_productIds = "";
    String send_quantities = "";
    String send_discount = "";
    String send_unit = "";
    String send_salesmanId = "1";

    double lat;
    double lon;
    String orderPlaceArea;
    protected LocationManager locationManager;

    @SuppressLint("MissingPermission")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lat = 0;
        lon = 0;
        orderPlaceArea = "";
        if(!checkLocationPermission())
        {
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }
        if(checkLocationPermission())
        {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        }

        Date objDate = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        this.stDate = df.format(objDate);
        this.stTime = sdf.format(objDate);
        DealersAndProductsInfo objdealandproinfo = new DealersAndProductsInfo(this);
        dealersId = objdealandproinfo.getDealersId();
        dealersName = objdealandproinfo.getDealersName();
        dealersAddress = objdealandproinfo.getDealersAddress();
        productsIds = objdealandproinfo.getProductsIds();
        productsName = objdealandproinfo.getProductsName();
        productsPrice = objdealandproinfo.getProductsPrice();
        orgProducts = new SearchableSpinner[4];
        orgqQuantity = new EditText[4];
        orgUnit = new Spinner[4];
        edOrderPrice = findViewById(R.id.edorderprice);
        edDiscount = findViewById(R.id.eddiscount);
        edFinalPrice = findViewById(R.id.edfinalprice);

        edOrderPrice.setText(String.valueOf(orderPrice));
        edDiscount.setText(String.valueOf(discount));
        edFinalPrice.setText(String.valueOf(finalPrice));

        subLayout = new LinearLayout[maxExtraProducts];
        subLayoutDetail = new LinearLayout[maxExtraProducts];
        extraProducts = new SearchableSpinner[maxExtraProducts];
        extraQuantity = new EditText[maxExtraProducts];
        extraBonus = new EditText[maxExtraProducts];
        extraDiscount = new EditText[maxExtraProducts];
        extraUnits = new Spinner[maxExtraProducts];
        extraOpBtn = new Button[maxExtraProducts];

        objSendData = new SendData(this);
        extraCount = 0;
        btnAdd = findViewById(R.id.button_add);
        edDealerID = findViewById(R.id.ed_dealerName);
        lv = findViewById(R.id.linear_layout);

        dealerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, dealersName);
        edDealerID.setAdapter(dealerAdapter);

        productAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, productsName);

        String[] unitItems = new String[]{"Packets", "Box"};
        unitAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, unitItems);
        objDatabaseSqlite = new DatabaseSQLite(this);
        btnSubmitOrder = findViewById(R.id.btn_send);
        btnSaveOrder = findViewById(R.id.btn_save);

        for(int i=0; i<4; i++)
        {
            extraCount++;
            lv.addView(createLinearLayout());
            addListenerToButtons();
        }
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                extraCount++;
                lv.addView(createLinearLayout());
                addListenerToButtons();
            }
        });

        bonusDetail = initializeBonusInfo();
        discountDetail = initializeDiscountInfo();
        productsCompaniesDetail = initializeProductsCompaniesInfo();

        btnSaveOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                compileOrder();
                objSendData.saveBooking(send_dealerId, send_productIds, send_quantities, send_unit, strOrderPrice, bonus, send_discount, strFinalPrice, String.valueOf(lat), String.valueOf(lon), orderPlaceArea, stDate, stTime, send_salesmanId);
            }
        });


        edProgress = findViewById(R.id.ed_progress);
        btnSubmitOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                compileOrder();
                objSendData.sendBooking(send_dealerId, send_productIds, send_quantities, send_unit, strOrderPrice, bonus, send_discount, strFinalPrice, String.valueOf(lat), String.valueOf(lon), orderPlaceArea, stDate, stTime, send_salesmanId);
            }
        });
    }

    public void compileOrder()
    {
        float orderPrice = 0;
        float finalPrice = 0;
        MainActivity.edProgress.setVisibility(View.VISIBLE);
        boolean check = false;
        if(edDealerID.getSelectedItemPosition() > 0)
        {
            send_dealerId = dealersId[edDealerID.getSelectedItemPosition()];
        }
        else
        {
            return;
        }
        for(int j=0; j<extraCount; j++)
        {
            if(!(subLayout[j].getVisibility() == View.GONE) && extraProducts[j].getSelectedItemPosition() > 0)
            {
                if(check)
                {
                    send_productIds = send_productIds+"_-_"+productsIds[extraProducts[j].getSelectedItemPosition()];
                    send_quantities = send_quantities+"_-_"+extraQuantity[j].getText().toString();
                    bonus = bonus+"_-_"+extraBonus[j].getText().toString(); // Problem as Discount structure in mysql is changed
                    send_discount = send_discount+"_-_"+extraDiscount[j].getText().toString(); // Problem as Discount structure in mysql is changed
                    send_unit = send_unit+"_-_Packets";
                }
                else
                {
                    send_productIds = productsIds[extraProducts[j].getSelectedItemPosition()];
                    send_quantities = extraQuantity[j].getText().toString();
                    bonus = extraBonus[j].getText().toString();
                    send_discount = extraDiscount[j].getText().toString();
                    send_unit = "Packets";
                    check = true;
                }

            }
        }
        SyncService.tenCounts = 1;
        Intent intent = new Intent(this, SyncService.class);
        startService(intent);
    }

    public SearchableSpinner createProductSpinner() {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, Math.round(getResources().getDimension(R.dimen.control_height)));
        extraProducts[extraCount-1] = new SearchableSpinner(MainActivity.this);
        extraProducts[extraCount-1].setLayoutParams(lparams);
        extraProducts[extraCount-1].setAdapter(productAdapter);
        return extraProducts[extraCount-1];
    }
    public EditText createQuantityEditText() {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(Math.round(getResources().getDimension(R.dimen.quantity_width)), Math.round(getResources().getDimension(R.dimen.control_height)));
        extraQuantity[extraCount-1] = new EditText(MainActivity.this);
        extraQuantity[extraCount-1].setLayoutParams(lparams);
        extraQuantity[extraCount-1].setMovementMethod(new ScrollingMovementMethod());
        extraQuantity[extraCount-1].setText("1");
        extraQuantity[extraCount-1].setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        extraQuantity[extraCount-1].setInputType(InputType.TYPE_CLASS_NUMBER);
//        extraQuantity[extraCount-1].setFilters(new InputFilter[]{ new InputFilterMinMax("1", "100")});
        return extraQuantity[extraCount-1];
    }

    public EditText createBonusEditText() {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(Math.round(getResources().getDimension(R.dimen.bonus_width)), Math.round(getResources().getDimension(R.dimen.control_height)));
        extraBonus[extraCount-1] = new EditText(MainActivity.this);
        extraBonus[extraCount-1].setLayoutParams(lparams);
        extraBonus[extraCount-1].setMovementMethod(new ScrollingMovementMethod());
        extraBonus[extraCount-1].setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        extraBonus[extraCount-1].setEnabled(false);
        return extraBonus[extraCount-1];
    }

    public EditText createDiscountEditText() {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(Math.round(getResources().getDimension(R.dimen.discount__width)), Math.round(getResources().getDimension(R.dimen.control_height)));
        extraDiscount[extraCount-1] = new EditText(MainActivity.this);
        extraDiscount[extraCount-1].setLayoutParams(lparams);
        extraDiscount[extraCount-1].setMovementMethod(new ScrollingMovementMethod());
        extraDiscount[extraCount-1].setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        extraDiscount[extraCount-1].setEnabled(false);
        return extraDiscount[extraCount-1];
    }

//    public Spinner createUnitSpinner() {
//        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(Math.round(getResources().getDimension(R.dimen.unit_width)), Math.round(getResources().getDimension(R.dimen.control_height)));
//        extraUnits[extraCount-1] = new Spinner(MainActivity.this);
//        extraUnits[extraCount-1].setLayoutParams(lparams);
//        extraUnits[extraCount-1].setAdapter(unitAdapter);
//        return extraUnits[extraCount-1];
//    }

    public Button createOpButton() {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        extraOpBtn[extraCount-1] = new Button(MainActivity.this);
        extraOpBtn[extraCount-1].setLayoutParams(lparams);
        extraOpBtn[extraCount-1].setText("X");
        extraOpBtn[extraCount-1].setTextColor(Color.RED);
        extraOpBtn[extraCount-1].setBackgroundColor(Color.TRANSPARENT);
        return extraOpBtn[extraCount-1];
    }

    public LinearLayout createDetailLinearLayout() {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        subLayoutDetail[extraCount-1] = new LinearLayout(MainActivity.this);
        subLayoutDetail[extraCount-1].setOrientation(LinearLayout.HORIZONTAL);
        subLayoutDetail[extraCount-1].setLayoutParams(lparams);
        subLayoutDetail[extraCount-1].addView(createQuantityEditText());
//        subLayoutDetail[extraCount-1].addView(createUnitSpinner());
        subLayoutDetail[extraCount-1].addView(createBonusEditText());
        subLayoutDetail[extraCount-1].addView(createDiscountEditText());
        subLayoutDetail[extraCount-1].addView(createOpButton());
        return subLayoutDetail[extraCount-1];
    }

    public LinearLayout createLinearLayout() {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        subLayout[extraCount-1] = new LinearLayout(MainActivity.this);
        subLayout[extraCount-1].setOrientation(LinearLayout.VERTICAL);
        subLayout[extraCount-1].setLayoutParams(lparams);
        subLayout[extraCount-1].addView(createProductSpinner());
        subLayout[extraCount-1].addView(createDetailLinearLayout());
        return subLayout[extraCount-1];
    }

    public void addListenerToButtons()
    {
        for(int i=0; i<extraCount; i++)
        {
            final int finalI = i;
            extraOpBtn[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    subLayout[finalI].setVisibility(View.GONE);
                    calculatePrice();
                }
            });

            extraProducts[i].setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    calculatePrice();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            extraQuantity[i].setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean hasFocus) {
                    if (!hasFocus) {
                        if(extraQuantity[finalI].getText().toString().equals(""))
                        {
                            extraQuantity[finalI].setText("1");
                        }
                        calculatePrice();
                    }
                }
            });

            extraQuantity[i].addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    calculatePrice();
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    calculatePrice();
                }

                @Override
                public void afterTextChanged(Editable s) {
                    calculatePrice();
                }
            });
        }
        calculatePrice();
    }

    public void calculatePrice()
    {
        orderPrice = 0f;
        finalPrice = 0f;
        strOrderPrice = "";
        bonus = "0";
        discountSingle = "0";
        strDiscount = "";
        strFinalPrice = "";
        boolean iter = false;
        String stQan = "0";
        float totalDiscount = 0;
        for(int j=0; j<extraCount; j++)
        {
            if(!(subLayout[j].getVisibility() == View.GONE) && extraProducts[j].getSelectedItemPosition() > 0)
            {
                stQan = extraQuantity[j].getText().toString();
                bonus = extraBonus[j].getText().toString();
                discountSingle = extraDiscount[j].getText().toString();
                if(stQan.equals(""))
                {
                    stQan = "0";
                }
                if(iter)
                {
                    strOrderPrice = strOrderPrice+"_-_"+String.valueOf(productsPrice[extraProducts[j].getSelectedItemPosition()] * Integer.parseInt(stQan));
                    strBonus = strBonus+"_-_"+bonus;
                    strDiscount = strDiscount+"_-_"+discountSingle;
                    strFinalPrice = strFinalPrice+"_-_"+String.valueOf((productsPrice[extraProducts[j].getSelectedItemPosition()] * Integer.parseInt(stQan)) - Float.parseFloat(discountSingle));
                }
                else
                {
                    iter = true;
                    strOrderPrice = String.valueOf(productsPrice[extraProducts[j].getSelectedItemPosition()] * Integer.parseInt(stQan));
                    strBonus = bonus;
                    strDiscount = discountSingle;
                    strFinalPrice = String.valueOf((productsPrice[extraProducts[j].getSelectedItemPosition()] * Integer.parseInt(stQan)) - Float.parseFloat(discountSingle));
                }
                orderPrice = orderPrice + (productsPrice[extraProducts[j].getSelectedItemPosition()] * Integer.parseInt(stQan));
            }

            if(edDealerID.getSelectedItemPosition() > 0 && extraProducts[j].getSelectedItemPosition() > 0)
            {
                String bonus = getBonus(dealersId[edDealerID.getSelectedItemPosition()], productsIds[extraProducts[j].getSelectedItemPosition()], extraQuantity[j].getText().toString());
                extraBonus[j].setText(bonus);
                String discount = getDiscount(dealersId[edDealerID.getSelectedItemPosition()], productsIds[extraProducts[j].getSelectedItemPosition()], String.valueOf(productsPrice[extraProducts[j].getSelectedItemPosition()] * Integer.parseInt(stQan)));
                float discountPrice = (productsPrice[extraProducts[j].getSelectedItemPosition()] * Integer.parseInt(stQan))/100 * Float.parseFloat(discount);
                totalDiscount += discountPrice;
                extraDiscount[j].setText(String.format("%.2f", discountPrice));
            }
            else
            {
                extraBonus[j].setText("0");
                extraDiscount[j].setText("0");
            }
        }
        discount = Float.valueOf(edDiscount.getText().toString());

        edDiscount.setText(String.format("%.2f", totalDiscount));
        finalPrice = orderPrice - totalDiscount;
        edOrderPrice.setText(String.valueOf(orderPrice));
        edFinalPrice.setText(String.valueOf(finalPrice));
    }

    public void viewAll()
    {
        // on Button Click
        Cursor res = objDatabaseSqlite.getAllData();

        if(res.getCount() == 0)
        {
            showMessage("Error", "Nothing Found");
            return;
        }
        else
        {
            StringBuffer objBuffer = new StringBuffer();
            int i = 1;
            while(res.moveToNext())
            {
                objBuffer.append("id: "+res.getString(0)+"\n");
                objBuffer.append("dealer_id: "+res.getString(1)+"\n");
                objBuffer.append("product_id: "+res.getString(2)+"\n");
                objBuffer.append("quantity: "+res.getString(3)+"\n");
                objBuffer.append("status: "+res.getString(4)+"\n");
//                i++;
            }
            showMessage("Data", objBuffer.toString());
        }
    }

    private ArrayList<ArrayList<String>> initializeBonusInfo()
    {
        ArrayList<ArrayList<String>> bonusData = new ArrayList<>();
        Cursor res = objDatabaseSqlite.getCurrentBonusInfo();
        int num = res.getCount();

        if(num > 0)
        {
            res.moveToNext();
            ArrayList<String> temp;
            do {
                temp = new ArrayList<>();
                temp.add(res.getString(0));
                temp.add(res.getString(1));
                temp.add(res.getString(2));
                temp.add(res.getString(3));
                temp.add(res.getString(4));
                bonusData.add(temp);
            }while (res.moveToNext());
        }

        return bonusData;
    }

    private ArrayList<ArrayList<String>> initializeDiscountInfo()
    {
        ArrayList<ArrayList<String>> discountData = new ArrayList<>();
        Cursor res = objDatabaseSqlite.getCurrentDiscountInfo();
        int num = res.getCount();

        if(num > 0)
        {
            res.moveToNext();
            ArrayList<String> temp;
            do {
                temp = new ArrayList<>();
                temp.add(res.getString(0));
                temp.add(res.getString(1));
                temp.add(res.getString(2));
                temp.add(res.getString(3));
                temp.add(res.getString(4));
                discountData.add(temp);
            }while (res.moveToNext());
        }

        return discountData;
    }

    private ArrayList<ArrayList<String>> initializeProductsCompaniesInfo()
    {
        ArrayList<ArrayList<String>> productsCompaniesData = new ArrayList<>();
        Cursor res = objDatabaseSqlite.getProductsCompaniesInfo();
        int num = res.getCount();

        if(num > 0)
        {
            res.moveToNext();
            ArrayList<String> temp;
            do {
                temp = new ArrayList<>();
                temp.add(res.getString(0));
                temp.add(res.getString(1));
                productsCompaniesData.add(temp);
            }while (res.moveToNext());
        }

        return productsCompaniesData;
    }

    private String getBonus(String dealerId, String productId, String quantity)
    {
        String bonus = "0";
        if(quantity.equals(""))
        {
            quantity = "0";
        }

        for(int i=0; i<bonusDetail.size(); i++)
        {
            if(dealerId.equals(bonusDetail.get(i).get(0)) && (bonusDetail.get(i).get(1).equals("0") || bonusDetail.get(i).get(1).equals("null")) && (bonusDetail.get(i).get(2).equals("0") || bonusDetail.get(i).get(2).equals("null")))
            {
                if(bonusDetail.get(i).get(3).equals("0"))
                {
                    return bonusDetail.get(i).get(4);
                }
                else 
                {
                    int num = Integer.valueOf(quantity)/Integer.valueOf(bonusDetail.get(i).get(3));
                    num = num*Integer.valueOf(bonusDetail.get(i).get(4));
                    return String.valueOf(num);
                }
            }
            else if(dealerId.equals(bonusDetail.get(i).get(0)) && (bonusDetail.get(i).get(1).equals("0") || bonusDetail.get(i).get(1).equals("null")) && productId.equals(bonusDetail.get(i).get(2)))
            {
                if(bonusDetail.get(i).get(3).equals("0"))
                {
                    return bonusDetail.get(i).get(4);
                }
                else 
                {
                    int num = Integer.valueOf(quantity)/Integer.valueOf(bonusDetail.get(i).get(3));
                    num = num*Integer.valueOf(bonusDetail.get(i).get(4));
                    return String.valueOf(num);
                }
            }
            else if((bonusDetail.get(i).get(0).equals("0") || bonusDetail.get(i).get(0).equals("null")) && (bonusDetail.get(i).get(1).equals("0") || bonusDetail.get(i).get(1).equals("null")) && productId.equals(bonusDetail.get(i).get(2)))
            {
                if(bonusDetail.get(i).get(3).equals("0"))
                {
                    return bonusDetail.get(i).get(4);
                }
                else 
                {
                    int num = Integer.valueOf(quantity)/Integer.valueOf(bonusDetail.get(i).get(3));
                    num = num*Integer.valueOf(bonusDetail.get(i).get(4));
                    return String.valueOf(num);
                }
            }
            else if((bonusDetail.get(i).get(0).equals("0") || bonusDetail.get(i).get(0).equals("null")) && !(bonusDetail.get(i).get(1).equals("0") || bonusDetail.get(i).get(1).equals("null")) && (bonusDetail.get(i).get(2).equals("0") || bonusDetail.get(i).get(2).equals("null")))
            {
                for(int j=0; j<productsCompaniesDetail.size(); j++)
                {
                    if(productsCompaniesDetail.get(j).get(1).equals(bonusDetail.get(i).get(1)))
                    {
                        if(bonusDetail.get(i).get(3).equals("0"))
                        {
                            return bonusDetail.get(i).get(4);
                        }
                        else 
                        {
                            int num = Integer.valueOf(quantity)/Integer.valueOf(bonusDetail.get(i).get(3));
                            num = num*Integer.valueOf(bonusDetail.get(i).get(4));
                            return String.valueOf(num);
                        }
                    }
                }
            }
            else if(dealerId.equals(bonusDetail.get(i).get(0)) && !(bonusDetail.get(i).get(1).equals("0") || bonusDetail.get(i).get(1).equals("null")) && (bonusDetail.get(i).get(2).equals("0") || bonusDetail.get(i).get(2).equals("null")))
            {
                for(int j=0; j<productsCompaniesDetail.size(); j++)
                {
                    if(productsCompaniesDetail.get(j).get(1).equals(bonusDetail.get(i).get(1)))
                    {
                        if(bonusDetail.get(i).get(3).equals("0"))
                        {
                            return bonusDetail.get(i).get(4);
                        }
                        else 
                        {
                            int num = Integer.valueOf(quantity)/Integer.valueOf(bonusDetail.get(i).get(3));
                            num = num*Integer.valueOf(bonusDetail.get(i).get(4));
                            return String.valueOf(num);
                        }
                    }
                }
            }
            else if((bonusDetail.get(i).get(0).equals("0") || bonusDetail.get(i).get(0).equals("null")) && !(bonusDetail.get(i).get(1).equals("0") || bonusDetail.get(i).get(1).equals("null")) && productId.equals(bonusDetail.get(i).get(2)))
            {
                for(int j=0; j<productsCompaniesDetail.size(); j++)
                {
                    if(productsCompaniesDetail.get(j).get(1).equals(bonusDetail.get(i).get(1)))
                    {
                        if(bonusDetail.get(i).get(3).equals("0"))
                        {
                            return bonusDetail.get(i).get(4);
                        }
                        else 
                        {
                            int num = Integer.valueOf(quantity)/Integer.valueOf(bonusDetail.get(i).get(3));
                            num = num*Integer.valueOf(bonusDetail.get(i).get(4));
                            return String.valueOf(num);
                        }
                    }
                }
            }
            else if((bonusDetail.get(i).get(0).equals("0") || bonusDetail.get(i).get(0).equals("null")) && (bonusDetail.get(i).get(1).equals("0") || bonusDetail.get(i).get(1).equals("null")) && (bonusDetail.get(i).get(2).equals("0") || bonusDetail.get(i).get(2).equals("null")))
            {
                if(bonusDetail.get(i).get(3).equals("0"))
                {
                    return bonusDetail.get(i).get(4);
                }
                else 
                {
                    int num = Integer.valueOf(quantity)/Integer.valueOf(bonusDetail.get(i).get(3));
                    num = num*Integer.valueOf(bonusDetail.get(i).get(4));
                    return String.valueOf(num);
                }
            }
        }
        return bonus;
    }





    private String getDiscount(String dealerId, String productId, String saleAmount)
    {
        String discount = "0";
        if(saleAmount.equals(""))
        {
            saleAmount = "0";
        }

        for(int i=0; i<discountDetail.size(); i++)
        {
            if(dealerId.equals(discountDetail.get(i).get(0)) && (discountDetail.get(i).get(1).equals("0") || discountDetail.get(i).get(1).equals("null")) && (discountDetail.get(i).get(2).equals("0") || discountDetail.get(i).get(2).equals("null")))
            {
                if(Float.parseFloat(discountDetail.get(i).get(3)) == 0)
                {
                    return discountDetail.get(i).get(4);
                }
                else
                {
                    float num = Float.valueOf(saleAmount)/Float.valueOf(discountDetail.get(i).get(3));
                    num = num*Float.valueOf(discountDetail.get(i).get(4));
                    return String.valueOf(num);
                }
            }
            else if(dealerId.equals(discountDetail.get(i).get(0)) && (discountDetail.get(i).get(1).equals("0") || discountDetail.get(i).get(1).equals("null")) && productId.equals(discountDetail.get(i).get(2)))
            {
                if(Float.parseFloat(discountDetail.get(i).get(3)) == 0)
                {
                    return discountDetail.get(i).get(4);
                }
                else
                {
                    float num = Float.valueOf(saleAmount)/Float.valueOf(discountDetail.get(i).get(3));
                    num = num*Float.valueOf(discountDetail.get(i).get(4));
                    return String.valueOf(num);
                }
            }
            else if((discountDetail.get(i).get(0).equals("0") || discountDetail.get(i).get(0).equals("null")) && (discountDetail.get(i).get(1).equals("0") || discountDetail.get(i).get(1).equals("null")) && productId.equals(discountDetail.get(i).get(2)))
            {
                if(Float.parseFloat(discountDetail.get(i).get(3)) == 0)
                {
                    return discountDetail.get(i).get(4);
                }
                else
                {
                    float num = Float.valueOf(saleAmount)/Float.valueOf(discountDetail.get(i).get(3));
                    num = num*Float.valueOf(discountDetail.get(i).get(4));
                    return String.valueOf(num);
                }
            }
            else if((discountDetail.get(i).get(0).equals("0") || discountDetail.get(i).get(0).equals("null")) && !(discountDetail.get(i).get(1).equals("0") || discountDetail.get(i).get(1).equals("null")) && (discountDetail.get(i).get(2).equals("0") || discountDetail.get(i).get(2).equals("null")))
            {
                for(int j=0; j<productsCompaniesDetail.size(); j++)
                {
                    if(productsCompaniesDetail.get(j).get(1).equals(discountDetail.get(i).get(1)))
                    {
                        if(Float.parseFloat(discountDetail.get(i).get(3)) == 0)
                        {
                            return discountDetail.get(i).get(4);
                        }
                        else
                        {
                            float num = Float.valueOf(saleAmount)/Float.valueOf(discountDetail.get(i).get(3));
                            num = num*Float.valueOf(discountDetail.get(i).get(4));
                            return String.valueOf(num);
                        }
                    }
                }
            }
            else if(dealerId.equals(discountDetail.get(i).get(0)) && !(discountDetail.get(i).get(1).equals("0") || discountDetail.get(i).get(1).equals("null")) && (discountDetail.get(i).get(2).equals("0") || discountDetail.get(i).get(2).equals("null")))
            {
                for(int j=0; j<productsCompaniesDetail.size(); j++)
                {
                    if(productsCompaniesDetail.get(j).get(1).equals(discountDetail.get(i).get(1)))
                    {
                        if(Float.parseFloat(discountDetail.get(i).get(3)) == 0)
                        {
                            return discountDetail.get(i).get(4);
                        }
                        else
                        {
                            float num = Float.valueOf(saleAmount)/Float.valueOf(discountDetail.get(i).get(3));
                            num = num*Float.valueOf(discountDetail.get(i).get(4));
                            return String.valueOf(num);
                        }
                    }
                }
            }
            else if((discountDetail.get(i).get(0).equals("0") || discountDetail.get(i).get(0).equals("null")) && !(discountDetail.get(i).get(1).equals("0") || discountDetail.get(i).get(1).equals("null")) && productId.equals(discountDetail.get(i).get(2)))
            {
                for(int j=0; j<productsCompaniesDetail.size(); j++)
                {
                    if(productsCompaniesDetail.get(j).get(1).equals(discountDetail.get(i).get(1)))
                    {
                        if(Float.parseFloat(discountDetail.get(i).get(3)) == 0)
                        {
                            return discountDetail.get(i).get(4);
                        }
                        else
                        {
                            float num = Float.valueOf(saleAmount)/Float.valueOf(discountDetail.get(i).get(3));
                            num = num*Float.valueOf(discountDetail.get(i).get(4));
                            return String.valueOf(num);
                        }
                    }
                }
            }
            else if((discountDetail.get(i).get(0).equals("0") || discountDetail.get(i).get(0).equals("null")) && (discountDetail.get(i).get(1).equals("0") || discountDetail.get(i).get(1).equals("null")) && (discountDetail.get(i).get(2).equals("0") || discountDetail.get(i).get(2).equals("null")))
            {
                if(Float.parseFloat(discountDetail.get(i).get(3)) == 0)
                {
                    return discountDetail.get(i).get(4);
                }
                else
                {
                    float num = Float.valueOf(saleAmount)/Float.valueOf(discountDetail.get(i).get(3));
                    num = num*Float.valueOf(discountDetail.get(i).get(4));
                    return String.valueOf(num);
                }
            }
        }
        return discount;
    }

    public void showMessage(String title, String message)
    {
        AlertDialog.Builder objBuilder = new AlertDialog.Builder(this);
        objBuilder.setCancelable(true);
        objBuilder.setTitle(title);
        objBuilder.setMessage(message);
        objBuilder.show();
    }

    @Override
    public void onLocationChanged(Location location) {
        if(checkLocationPermission())
        {
            try {
                String addr = getAddress(location.getLatitude(), location.getLongitude());
                this.lat = location.getLatitude();
                this.lon = location.getLongitude();
                this.orderPlaceArea = addr;

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public String getAddress(double lat, double lng) throws IOException {
        String areaName = "";
        Geocoder geocoder = new Geocoder(MainActivity.this, Locale.getDefault());
        List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
        Address obj = addresses.get(0);
        String add = obj.getAddressLine(0);
        add = "\n" + obj.getCountryName();
        add = "\n" + obj.getCountryCode();
        add = "\n" + obj.getAdminArea();
        add = "\n" + obj.getSubAdminArea();
        add = "\n" + obj.getPostalCode();
        add = "\n" + obj.getSubAdminArea();
        add = "\n" + obj.getLocality();
        add = "\n" + obj.getSubLocality();
        add = "\n" + obj.getSubThoroughfare();

        return obj.getAddressLine(0);
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    public boolean checkLocationPermission()
    {
        String permission = "android.permission.ACCESS_FINE_LOCATION";
        int res = this.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }
}

