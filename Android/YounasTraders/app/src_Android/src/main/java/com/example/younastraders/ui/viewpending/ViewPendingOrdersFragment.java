package com.example.younastraders.ui.viewpending;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.younastraders.DatabaseSQLite;
import com.example.younastraders.GetData;
import com.example.younastraders.PendingOrderDetail;
import com.example.younastraders.R;
import com.google.android.material.tabs.TabLayout;

public class ViewPendingOrdersFragment extends AppCompatActivity {

    private ViewPendingOrdersViewModel objViewPendingViewModel;
    LinearLayout lv;
    LinearLayout[] subLayout;
    int ordersCount;

    GetData objGetData;

    String[] txOrderId;
    TextView[] txtSrNo;
    TextView[] txtDealerName;
    TextView[] txtFinalPrice;
    TextView[] txtDate;
    View[] viewLine;
    Button[] btnOps;
    String[] strStatus;
    String strPreDate;
    boolean setColor = false;

    private static final String FRAGMENT_NAME = ViewPendingOrdersFragment.class.getSimpleName();
    private static final String TAG = FRAGMENT_NAME;

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_viewpendingorders);
        objGetData = new GetData(this);
        ordersCount = 0;
        lv = findViewById(R.id.linear_layout);
        viewAll();
//        objViewPendingViewModel.getText().observe(this, new Observer<String>() {
//            @Override
//            public void onChanged(@Nullable String s) {
//            }
//        });
    }


    public void viewAll()
    {
        Cursor res = objGetData.getBookedOrders();
        int num = res.getCount();

        if(num == 0)
        {
            txtDealerName = new TextView[1];
            lv.addView(createEmptyText());
        }
        else
        {
            subLayout = new LinearLayout[res.getCount()];
            txOrderId = new String[res.getCount()];
            txtSrNo = new TextView[res.getCount()];
            txtDealerName = new TextView[res.getCount()];
            txtFinalPrice = new TextView[res.getCount()];
            txtDate = new TextView[res.getCount()];
            viewLine = new View[res.getCount()];
            btnOps = new Button[res.getCount()];
            strStatus = new String[res.getCount()];
            strPreDate = "";
            while(res.moveToNext())
            {
                ordersCount++;
                txOrderId[ordersCount-1] = res.getString(0);
                strStatus[ordersCount-1] = res.getString(4);
                if(!strPreDate.equals(res.getString(3)))
                {
                    strPreDate = res.getString(3);
                    lv.addView(createDateText(strPreDate));
                }
                String[] finalPriceArr = res.getString(2).split("_-_");
                float totalFinalPrice = 0.0f;
                for(int i=0; i<finalPriceArr.length; i++)
                {
                    totalFinalPrice += Float.parseFloat(finalPriceArr[i]);
                }
                lv.addView(createLinearLayout(String.valueOf(ordersCount), res.getString(1), String.valueOf(totalFinalPrice), res.getString(4)));
                lv.addView(createLineView());
//                addListenerToButtons();
            }
        }
    }


    public TextView createEmptyText() {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        txtDealerName[0] = new TextView(this);
        txtDealerName[0].setLayoutParams(lparams);
        txtDealerName[0].setText("No Record to Show");
        txtDealerName[0].setTextSize(TypedValue.COMPLEX_UNIT_SP,18);
        txtDealerName[0].setGravity(Gravity.CENTER);
        return txtDealerName[0];
    }

    public TextView createSrNoText(String text) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(Math.round(getResources().getDimension(R.dimen.invoice_srno)), ViewGroup.LayoutParams.WRAP_CONTENT);
        txtSrNo[ordersCount-1] = new TextView(this);
        txtSrNo[ordersCount-1].setLayoutParams(lparams);
        txtSrNo[ordersCount-1].setText(text);
        txtSrNo[ordersCount-1].setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        return txtSrNo[ordersCount-1];
    }

    public TextView createDealerText(String text) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(Math.round(getResources().getDimension(R.dimen.pending_dealer_width)), ViewGroup.LayoutParams.WRAP_CONTENT);
        txtDealerName[ordersCount-1] = new TextView(this);
        txtDealerName[ordersCount-1].setLayoutParams(lparams);
        txtDealerName[ordersCount-1].setText(text);
        return txtDealerName[ordersCount-1];
    }

    public TextView createFinalPriceText(String text) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        txtFinalPrice[ordersCount-1] = new TextView(this);
        txtFinalPrice[ordersCount-1].setLayoutParams(lparams);
//        txtFinalPrice[ordersCount-1].setBackgroundColor(Color.GREEN);
        txtFinalPrice[ordersCount-1].setText(text);
        txtFinalPrice[ordersCount-1].setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        return txtFinalPrice[ordersCount-1];
    }

    public View createLineView() {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,  Math.round(getResources().getDimension(R.dimen.line)));
        viewLine[ordersCount-1] = new View(this);
        viewLine[ordersCount-1].setLayoutParams(lparams);
        viewLine[ordersCount-1].setBackgroundColor(Color.BLACK);
        return viewLine[ordersCount-1];
    }

    public TextView createDateText(String text) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        txtDate[ordersCount-1] = new TextView(this);
        txtDate[ordersCount-1].setLayoutParams(lparams);
//        txtDate[ordersCount-1].setBackgroundColor(Color.YELLOW);
        txtDate[ordersCount-1].setText(text);
        txtDate[ordersCount-1].setGravity(Gravity.CENTER);
        txtDate[ordersCount-1].setTextColor(Color.BLACK);
        return txtDate[ordersCount-1];
    }

//    public Button createOpButton(String status) {
//        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//        btnOps[ordersCount-1] = new Button(this);
//        btnOps[ordersCount-1].setLayoutParams(lparams);
//        if(status.equals("Pending"))
//        {
//            btnOps[ordersCount-1].setText("DETAIL");
//        }
//        else
//        {
//            btnOps[ordersCount-1].setText("SENT");
//        }
//        btnOps[ordersCount-1].setBackgroundColor(Color.TRANSPARENT);
//        return btnOps[ordersCount-1];
//    }

    public LinearLayout createLinearLayout(String srNo, String dealerName, String finalPrice, String status) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        subLayout[ordersCount-1] = new LinearLayout(this);
        subLayout[ordersCount-1].setOrientation(LinearLayout.HORIZONTAL);
        subLayout[ordersCount-1].setLayoutParams(lparams);
        subLayout[ordersCount-1].addView(createSrNoText(srNo));
        subLayout[ordersCount-1].addView(createDealerText(dealerName));
        subLayout[ordersCount-1].addView(createFinalPriceText(finalPrice));
//        subLayout[ordersCount-1].addView(createOpButton(status));
        if(setColor)
        {
            subLayout[ordersCount-1].setBackgroundColor(Color.parseColor("#E1E1E1"));
            setColor = false;
        }
        else
        {
            setColor = true;
        }
        if(status.equals("Pending"))
        {
            final String orderId = txOrderId[ordersCount-1];
            subLayout[ordersCount-1].setBackgroundColor(Color.parseColor("#FFA8A8"));
            subLayout[ordersCount-1].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ViewPendingOrdersFragment.this, PendingOrderDetail.class);
                    intent.putExtra("ORDER_ID", orderId);
                    intent.putExtra("STATUS", "Pending");
                    startActivity(intent);
                }
            });
        }
        else
        {
//            subLayout[ordersCount-1].setBackgroundColor(Color.parseColor("#B0FFA8"));
            subLayout[ordersCount-1].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ViewPendingOrdersFragment.this, PendingOrderDetail.class);
                    intent.putExtra("ORDER_ID", txOrderId[ordersCount-1]);
                    intent.putExtra("STATUS", "Sent");
                    startActivity(intent);
                }
            });
        }
        return subLayout[ordersCount-1];
    }

//    public void addListenerToButtons()
//    {
//        for(int i=0; i<ordersCount; i++)
//        {
//            final int finalI = i;
//            btnOps[i].setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if(strStatus[finalI].equals("Pending"))
//                    {
//                        Intent intent = new Intent(ViewPendingOrdersFragment.this, PendingOrderDetail.class);
//                        intent.putExtra("ORDER_ID", txOrderId[finalI]);
//                        startActivity(intent);
//                    }
//                }
//            });
//        }
//    }
}