package com.example.younastraders;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

public class Test extends AppCompatActivity {
    SearchableSpinner edDealerID;
    Button btnAdd;
    ArrayAdapter<String> dealerAdapter;
    public String[] dealersName;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_add_return);
        dealersName = new String[5];
        dealersName[0] = "Dealer 1";
        dealersName[1] = "Dealer 2";
        dealersName[2] = "Dealer 3";
        dealersName[3] = "Dealer 4";
        dealersName[4] = "Dealer 5";
        btnAdd = findViewById(R.id.button_add);
        edDealerID = findViewById(R.id.ed_dealerName);

        dealerAdapter = new ArrayAdapter<>(Test.this, android.R.layout.simple_spinner_dropdown_item, dealersName);
        edDealerID.setAdapter(dealerAdapter);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edDealerID.setSelection(3);
            }
        });
    }
}