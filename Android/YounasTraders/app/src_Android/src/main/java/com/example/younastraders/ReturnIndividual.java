package com.example.younastraders;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDialogFragment;

public class ReturnIndividual extends AppCompatDialogFragment {
    private EditText editTextRetQty;
    private TextView txtItemName;
    private TextView txtInvoicedQty;
    public static String itemName = "";
    public static String invoiceQty = "";
    public static String returnedQty = "";
    private ReturnIndividualDialogListener listener;
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.return_individual, null);
        builder.setView(view)
                .setTitle("Set Return")
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                })
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String retQty = editTextRetQty.getText().toString();
                        listener.applyTexts(retQty);
                    }
                });
        editTextRetQty = view.findViewById(R.id.edit_return_returnqty);
        txtItemName = view.findViewById(R.id.txt_return_item);
        txtInvoicedQty = view.findViewById(R.id.txt_return_invoicedqty);
        txtItemName.setText(itemName);
        txtInvoicedQty.setText(invoiceQty);
        if(!returnedQty.equals("0"))
        {
            editTextRetQty.setText(returnedQty);
        }

        return builder.create();
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (ReturnIndividualDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() +
                    "must implement ReturnIndividualDialogListener");
        }
    }
    public interface ReturnIndividualDialogListener {
        void applyTexts(String retQty);
    }
}
