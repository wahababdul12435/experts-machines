package com.example.younastraders.tabsdelivery;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.younastraders.DatabaseSQLite;
import com.example.younastraders.GetData;
import com.example.younastraders.GlobalVariables;
import com.example.younastraders.InvoicedOrderDetail;
import com.example.younastraders.R;
import com.example.younastraders.SendData;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AddDeliveryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddDeliveryFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    LinearLayout lv;
    SearchableSpinner edInvoiceNum;
    LinearLayout[] subLayout;
    int ordersCount;
    String stInvoiceDetail;

    DatabaseSQLite objSqlLite;
    ArrayAdapter<String> invoicesNumAdapter;
    Button btnGetDetail;

    String[] stOrderIds;
    String[] stOrderIdsList;
    TextView[] txOrderId;
    TextView[] txDealerName;
    TextView[] txFinalPrice;
    Button[] btnOps;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public AddDeliveryFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ExploreFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AddDeliveryFragment newInstance(String param1, String param2) {
        AddDeliveryFragment fragment = new AddDeliveryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    private int findIndexInArray(String query, String[] arr) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i].equals(query)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_add_delivery, container, false);
        objSqlLite = new DatabaseSQLite(getActivity());
        ordersCount = 0;
        lv = rootView.findViewById(R.id.linear_layout);
        edInvoiceNum = rootView.findViewById(R.id.ed_invoicenum);
        btnGetDetail = rootView.findViewById(R.id.btn_getdetail);
        viewAll();
        if(stOrderIds.length == 0)
        {
            edInvoiceNum.setVisibility(View.GONE);
            btnGetDetail.setVisibility(View.GONE);
        }
        invoicesNumAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, stOrderIdsList);
        edInvoiceNum.setAdapter(invoicesNumAdapter);

        btnGetDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), String.valueOf(edInvoiceNum.getSelectedItemPosition()), Toast.LENGTH_LONG).show();
                if(edInvoiceNum.getSelectedItemPosition() > 0 )
                {
                    stInvoiceDetail = stOrderIdsList[edInvoiceNum.getSelectedItemPosition()];
                    Intent intent = new Intent(getActivity(), InvoicedOrderDetail.class);
                    intent.putExtra("ORDER_ID", stInvoiceDetail);
                    startActivity(intent);
                }
                else
                {
                    Toast.makeText(getActivity(), "Please Enter Invoice Num", Toast.LENGTH_LONG).show();
                }
            }
        });
        return rootView;
    }

    public void viewAll()
    {
        Cursor res = objSqlLite.getPendingInvoices();
        int num = res.getCount();

        if(num == 0)
        {
            txDealerName = new TextView[1];
            lv.addView(createEmptyText());

            subLayout = new LinearLayout[1];
            stOrderIdsList = new String[1];
            stOrderIds = new String[1];
            txOrderId = new TextView[1];
            txDealerName = new TextView[1];
            txFinalPrice = new TextView[1];
            btnOps = new Button[1];
            stOrderIdsList[0] = "Select Invoice";
        }
        else
        {
            subLayout = new LinearLayout[res.getCount()];
            stOrderIdsList = new String[res.getCount()+1];
            stOrderIds = new String[res.getCount()];
            txOrderId = new TextView[res.getCount()];
            txDealerName = new TextView[res.getCount()];
            txFinalPrice = new TextView[res.getCount()];
            btnOps = new Button[res.getCount()];
            res.moveToNext();
            stOrderIdsList[0] = "Select Invoice";
            if(res.getString(0) == null)
            {
                txDealerName = new TextView[1];
                lv.addView(createEmptyText());
            }
            else
            {
                do {
                    ordersCount++;
                    stOrderIds[ordersCount-1] = res.getString(0);
                    stOrderIdsList[ordersCount] = res.getString(0);
                    lv.addView(createLinearLayout(res.getString(0), res.getString(1), res.getString(2)));
                    addListenerToButtons();
                }while (res.moveToNext());
            }
        }
    }


    public TextView createEmptyText() {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        txDealerName[0] = new TextView(getActivity());
        txDealerName[0].setLayoutParams(lparams);
        txDealerName[0].setText("No Record to Show");
        txDealerName[0].setTextSize(TypedValue.COMPLEX_UNIT_SP,18);
        txDealerName[0].setGravity(Gravity.CENTER);
        return txDealerName[0];
    }

    public TextView createInvoiceNoText(String text) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(Math.round(getResources().getDimension(R.dimen.order_id)), LinearLayout.LayoutParams.WRAP_CONTENT);
        txOrderId[ordersCount-1] = new TextView(getActivity());
        txOrderId[ordersCount-1].setLayoutParams(lparams);
        txOrderId[ordersCount-1].setText(text);
//        txOrderId[ordersCount-1].setBackgroundColor(Color.BLUE);
        return txOrderId[ordersCount-1];
    }

    public TextView createDealerText(String text) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(Math.round(getResources().getDimension(R.dimen.dealer_150)), LinearLayout.LayoutParams.WRAP_CONTENT);
        txDealerName[ordersCount-1] = new TextView(getActivity());
        txDealerName[ordersCount-1].setLayoutParams(lparams);
        txDealerName[ordersCount-1].setText(text);
//        txDealerName[ordersCount-1].setBackgroundColor(Color.YELLOW);
        return txDealerName[ordersCount-1];
    }

    public TextView createFinalPriceText(String text) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(Math.round(getResources().getDimension(R.dimen.pending_final_price)), LinearLayout.LayoutParams.WRAP_CONTENT);
        txFinalPrice[ordersCount-1] = new TextView(getActivity());
        txFinalPrice[ordersCount-1].setLayoutParams(lparams);
        txFinalPrice[ordersCount-1].setText(text);
//        txFinalPrice[ordersCount-1].setBackgroundColor(Color.GREEN);
        return txFinalPrice[ordersCount-1];
    }

    public Button createOpButton() {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        btnOps[ordersCount-1] = new Button(getActivity());
        btnOps[ordersCount-1].setLayoutParams(lparams);
        btnOps[ordersCount-1].setText("Detail");
//        btnOps[ordersCount-1].setBackgroundColor(Color.RED);
        btnOps[ordersCount-1].setBackgroundColor(Color.TRANSPARENT);
        return btnOps[ordersCount-1];
    }

    public LinearLayout createLinearLayout(String invoiceNo, String dealerName, String finalPrice) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        subLayout[ordersCount-1] = new LinearLayout(getActivity());
        subLayout[ordersCount-1].setOrientation(LinearLayout.HORIZONTAL);
        subLayout[ordersCount-1].setLayoutParams(lparams);
        subLayout[ordersCount-1].addView(createInvoiceNoText(invoiceNo));
        subLayout[ordersCount-1].addView(createDealerText(dealerName));
        subLayout[ordersCount-1].addView(createFinalPriceText(finalPrice));
        subLayout[ordersCount-1].addView(createOpButton());
        return subLayout[ordersCount-1];
    }

    public void addListenerToButtons()
    {
        for(int i=0; i<ordersCount; i++)
        {
            final int finalI = i;
            btnOps[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), InvoicedOrderDetail.class);
                    intent.putExtra("ORDER_ID", stOrderIds[finalI]);
                    startActivity(intent);
                }
            });
        }
    }
}