package com.example.younastraders;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DealersAndProductsInfo extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "YounasTraders.db";
    public static final String SUBAREA_INFO_TABLE = "subarea_info";
    public static final String DEALER_INFO_TABLE = "dealer_info";
    public static final String PRODUCT_INFO_TABLE = "product_info";
    public String[] dealersId;
    public String[] dealersName;
    public String[] dealersAddress;
    public String[] productsIds;
    public String[] productsName;
    public float[] productsPrice;

    public DealersAndProductsInfo(Context context) {
        super(context, DATABASE_NAME, null, 1);
        dealersData();
        productsData();
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public int dealersData()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("SELECT dealer_id, dealer_name, dealer_address FROM "+DEALER_INFO_TABLE, null);
        if(res.getCount() == 0)
        {
            dealersId = new String[res.getCount()+1];
            dealersName = new String[res.getCount()+1];
            dealersAddress = new String[res.getCount()+1];
            dealersId[0] = "0";
            dealersName[0] = "Select Dealer";
            dealersAddress[0] = "NULL";
            return 0;
        }
        else
        {
            dealersId = new String[res.getCount()+1];
            dealersName = new String[res.getCount()+1];
            dealersAddress = new String[res.getCount()+1];
            dealersId[0] = "0";
            dealersName[0] = "Select Dealer";
            dealersAddress[0] = "NULL";
            int i = 1;
            while(res.moveToNext())
            {
                dealersId[i] = res.getString(0);
                dealersName[i] = res.getString(1)+" ("+res.getString(2)+")";
                dealersAddress[i] = res.getString(2);
                i++;
            }
            return 1;
        }
    }

    public int productsData()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("SELECT product_id, product_name, final_price FROM "+PRODUCT_INFO_TABLE, null);
        if(res.getCount() == 0)
        {
            productsIds = new String[res.getCount()+1];
            productsName = new String[res.getCount()+1];
            productsPrice = new float[res.getCount()+1];
            productsIds[0] = "0";
            productsName[0] = "Select Item";
            productsPrice[0] = 0.0f;
            return 0;
        }
        else
        {
            productsIds = new String[res.getCount()+1];
            productsName = new String[res.getCount()+1];
            productsPrice = new float[res.getCount()+1];
            productsIds[0] = "0";
            productsName[0] = "Select Item";
            productsPrice[0] = 0.0f;
            int i = 1;
            while(res.moveToNext())
            {
                productsIds[i] = res.getString(0);
                productsName[i] = res.getString(1);
                productsPrice[i] = Float.parseFloat(res.getString(2));
                i++;
            }
            return 1;
        }
    }

    public String[] getDealersId() {
        return dealersId;
    }

    public String[] getDealersName() {
        return dealersName;
    }

    public String[] getDealersAddress() {
        return dealersAddress;
    }

    public String[] getProductsIds() {
        return productsIds;
    }

    public String[] getProductsName() {
        return productsName;
    }

    public float[] getProductsPrice() {
        return productsPrice;
    }
}
