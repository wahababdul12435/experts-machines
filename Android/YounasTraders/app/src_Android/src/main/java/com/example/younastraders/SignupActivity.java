package com.example.younastraders;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class SignupActivity extends AppCompatActivity {
    private static final String TAG = "SignupActivity";
    TextView existLogin;
    EditText edUserName;
    EditText edSoftwareId;
    EditText edUserId;
    EditText edPassword;
    EditText edConfirmPassword;
    Button btnRegister;
    SendData objSendData;
    String stDate;
    ProgressBar progressReg;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        existLogin = findViewById(R.id.txt_login);
        edUserName = findViewById(R.id.ed_username);
        edSoftwareId = findViewById(R.id.ed_software_id);
        edUserId = findViewById(R.id.ed_userid);
        edPassword = findViewById(R.id.ed_pass);
        edConfirmPassword = findViewById(R.id.ed_repass);
        btnRegister = findViewById(R.id.btn_register);
        progressReg = findViewById(R.id.progressReg);
        objSendData = new SendData(this);
        Date objDate = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd/MMM/yyyy");
        this.stDate = df.format(objDate);


        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressReg.setVisibility(View.VISIBLE);
                String stUserName = edUserName.getText().toString();
                String stSoftwareId = edSoftwareId.getText().toString();
                String stUserId = edUserId.getText().toString();
                String stPass = edPassword.getText().toString();
                String stUserPower = "Salesman";
                String autoSend = "false";
                String stConPass = edConfirmPassword.getText().toString();
                if(!stPass.equals(stConPass))
                {
                    Toast.makeText(SignupActivity.this, "Password Not Matched", Toast.LENGTH_LONG).show();
                }
                else
                {
                    String responseStatus = objSendData.registerUser(stUserName, stSoftwareId, stUserId, stPass, stUserPower, stDate, autoSend);
//                    Intent intent = new Intent(SignupActivity.this, LoginActivity.class);
//                    startActivity(intent);
//                    finish();
                }
            }
        });
        existLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignupActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
