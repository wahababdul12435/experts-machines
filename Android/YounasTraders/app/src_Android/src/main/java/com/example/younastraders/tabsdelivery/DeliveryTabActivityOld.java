package com.example.younastraders.tabsdelivery;

import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.example.younastraders.GlobalVariables;
import com.example.younastraders.R;
import com.google.android.material.badge.BadgeDrawable;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

public class DeliveryTabActivityOld extends AppCompatActivity{

    private Toolbar toolbar;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    ViewPagerAdapter viewPagerAdapter;

    private AddDeliveryFragment addDeliveryFragment;
    private ViewDeliveryFragment viewDeliveryFragment;
//    private TravelFragment travelFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabs_delivery);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        viewPager = findViewById(R.id.view_pager);
        tabLayout = findViewById(R.id.tab_layout);

        addDeliveryFragment = new AddDeliveryFragment();
        viewDeliveryFragment = new ViewDeliveryFragment();
//        travelFragment = new TravelFragment();

        tabLayout.setupWithViewPager(viewPager);

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), 0);
        viewPagerAdapter.addFragment(addDeliveryFragment, "Add");
        viewPagerAdapter.addFragment(viewDeliveryFragment, "View");
//        viewPagerAdapter.addFragment(travelFragment, "Travel");
        viewPager.setAdapter(viewPagerAdapter);

        tabLayout.getTabAt(0).setIcon(R.drawable.ic_baseline_explore_24);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_baseline_flight_24);
//        tabLayout.getTabAt(2).setIcon(R.drawable.ic_baseline_card_travel_24);

        if(!GlobalVariables.pendingDelivered.equals("0"))
        {
            BadgeDrawable badgeDrawable = tabLayout.getTabAt(1).getOrCreateBadge();
            badgeDrawable.setVisible(true);
            badgeDrawable.setBackgroundColor(Color.parseColor("#1e4355"));
            badgeDrawable.setNumber(Integer.parseInt(GlobalVariables.pendingDelivered));
        }

    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {

        private List<Fragment> fragments = new ArrayList<>();
        private List<String> fragmentTitle = new ArrayList<>();

        public ViewPagerAdapter(@NonNull FragmentManager fm, int behavior) {
            super(fm, behavior);
        }

        public void addFragment(Fragment fragment, String title) {
            fragments.add(fragment);
            fragmentTitle.add(title);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitle.get(position);
        }
    }
}
