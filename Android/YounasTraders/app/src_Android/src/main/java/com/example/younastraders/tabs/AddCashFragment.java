package com.example.younastraders.tabs;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.younastraders.GetData;
import com.example.younastraders.GlobalVariables;
import com.example.younastraders.Home;
import com.example.younastraders.R;
import com.example.younastraders.SendData;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AddCashFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddCashFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    SearchableSpinner edDealerID;
    TextView txtPendingPayment;
    EditText edCollectedPayments;
    Spinner spinnerCashType;
    Button btnSave;
    Button btnSend;

    ArrayAdapter<String> dealerAdapter;
    ArrayAdapter<String> cashAdapter;
    public String[] dealersId;
    public String[] dealersName;
    public String[] dealersIdPendingPayments;
    public String[] dealersPendingPayments;
    SendData objSendData;
    Boolean editPrevious = false;
    String editPaymentId;
    String editDealerId;
    String editCash;
    String editPendingPayments;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public AddCashFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ExploreFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AddCashFragment newInstance(String param1, String param2) {
        AddCashFragment fragment = new AddCashFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    private int findIndexInArray(String query, String[] arr) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i].equals(query)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_add_cash, container, false);
        edDealerID = rootView.findViewById(R.id.ed_dealerName);
        txtPendingPayment = rootView.findViewById(R.id.txt_pending_payments);
        edCollectedPayments = rootView.findViewById(R.id.ed_cash_collected);
        spinnerCashType = rootView.findViewById(R.id.spinner_cash_type);
        btnSave = rootView.findViewById(R.id.btn_save_collection);
        btnSend = rootView.findViewById(R.id.btn_send_collection);

        GetData objGetData = new GetData(getActivity(), "Pending Payments");
        dealersId = objGetData.getDealersId();
        dealersName = objGetData.getDealersName();
        dealersIdPendingPayments = objGetData.getDealersIdPendingPayments();
        dealersPendingPayments = objGetData.getDealersPendingPayments();
        objSendData = new SendData(getActivity());
        String[] cashType = new String[]{"Collection", "Return"};
        cashAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, cashType);
        spinnerCashType.setAdapter(cashAdapter);

        dealerAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_dropdown_item, dealersName);
        edDealerID.setAdapter(dealerAdapter);

        if(!TabActivity.paymentId.equals(""))
        {
            displayReceivedData(TabActivity.paymentId);
        }

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edDealerID.getSelectedItemPosition() > 0)
                {
                    if(editPrevious)
                    {
                        String cashCollected = "0.0";
                        cashCollected = edCollectedPayments.getText().toString();
                        objSendData.updateCash(editPaymentId, editDealerId, editCash, cashCollected);
                        Intent intent = new Intent(getActivity(), Home.class);
                        startActivity(intent);
                        getActivity().finish();
                    }
                    else
                    {
                        String cashCollected = "0.0";
                        cashCollected = edCollectedPayments.getText().toString();
                        String cashType = spinnerCashType.getSelectedItem().toString();
                        String selectedDealerId = dealersId[edDealerID.getSelectedItemPosition()];
                        objSendData.saveCash(selectedDealerId, "0", cashCollected, cashType, GlobalVariables.getLatitude(), GlobalVariables.getLongitude(), GlobalVariables.getLocationNameHalf(), GlobalVariables.getStDate(), GlobalVariables.getStTime(), GlobalVariables.getUserId());
                    }
                }
                else
                {
                    Toast.makeText(getActivity(), "Please Select Dealer", Toast.LENGTH_LONG).show();
                }
            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edDealerID.getSelectedItemPosition() > 0)
                {
                    if(editPrevious)
                    {
                        String cashCollected = "0.0";
                        cashCollected = edCollectedPayments.getText().toString();
                        objSendData.updateCash(editPaymentId, editDealerId, editCash, cashCollected);
                        String selectedDealerId = dealersId[edDealerID.getSelectedItemPosition()];
                        String cashType = spinnerCashType.getSelectedItem().toString();
                        objSendData.sendSavedCash(editPaymentId, selectedDealerId, "0", cashCollected, cashType, GlobalVariables.getLatitude(), GlobalVariables.getLongitude(), GlobalVariables.getLocationNameHalf(), GlobalVariables.getStDate(), GlobalVariables.getStTime(), GlobalVariables.getUserId());
                    }
                    else
                    {
                        String cashCollected = "0.0";
                        cashCollected = edCollectedPayments.getText().toString();
                        String selectedDealerId = dealersId[edDealerID.getSelectedItemPosition()];
                        String cashType = spinnerCashType.getSelectedItem().toString();
                        objSendData.sendCash(selectedDealerId, "0", cashCollected, cashType, GlobalVariables.getLatitude(), GlobalVariables.getLongitude(), GlobalVariables.getLocationNameHalf(), GlobalVariables.getStDate(), GlobalVariables.getStTime(), GlobalVariables.getUserId());
                    }

                }
                else
                {
                    Toast.makeText(getActivity(), "Please Select Dealer", Toast.LENGTH_LONG).show();
                }
            }
        });

        edDealerID.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int index = findIndexInArray(dealersId[edDealerID.getSelectedItemPosition()], dealersIdPendingPayments);
                String pendingAmount = "0.0";
                if(index!=-1){
                    pendingAmount = dealersPendingPayments[index];
                }
                txtPendingPayment.setText(String.valueOf(Float.parseFloat(pendingAmount)));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        return rootView;
    }

    protected void displayReceivedData(String message)
    {
        editPrevious = true;
        editPaymentId = message;
        GetData objGetData = new GetData(getActivity());
        Cursor res = objGetData.getEditCashCollected(editPaymentId);
        res.moveToNext();
        editDealerId = res.getString(0);
        editCash = res.getString(1);
        if(res.getString(2) == null || res.getString(2).equals(""))
        {
            editPendingPayments = "0.0";
        }
        else
        {
            editPendingPayments = res.getString(2);
        }

        txtPendingPayment.setText(String.valueOf(Float.parseFloat(editPendingPayments) + Float.parseFloat(editCash)));
        edCollectedPayments.setText(editCash);
        int dealerIndex = findIndexInArray(editDealerId, dealersId);
        edDealerID.setSelection(dealerIndex);
//        edDealerID.setEnabled(false);
    }


}